﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Collections.Generic.IEqualityComparer`1<ZXing.Aztec.Internal.Decoder/Table>
struct IEqualityComparer_1_t1798512917;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.Decoder/Table>
struct ICollection_1_t1902068500;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>[]
struct KeyValuePair_2U5BU5D_t3675014586;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>
struct IEnumerator_1_t273767124;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct KeyCollection_t89880820;
// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct ValueCollection_t1458694378;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4075412057.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m660172606_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m660172606(__this, method) ((  void (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2__ctor_m660172606_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3644457653_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3644457653(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3644457653_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2482255759_gshared (Dictionary_2_t2758088665 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2482255759(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2758088665 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2482255759_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2726107007_gshared (Dictionary_2_t2758088665 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2726107007(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2758088665 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2726107007_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m23628186_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m23628186(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m23628186_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1192801020_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1192801020(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1192801020_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1893066986_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1893066986(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1893066986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m876635423_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m876635423(__this, method) ((  bool (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m876635423_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3391781370_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3391781370(__this, method) ((  bool (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3391781370_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1973522624_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1973522624(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1973522624_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m832295599_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m832295599(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m832295599_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1288649282_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1288649282(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1288649282_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1382396336_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1382396336(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1382396336_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3327101677_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3327101677(__this, ___key0, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3327101677_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1956699044_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1956699044(__this, method) ((  bool (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1956699044_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m114368214_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m114368214(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m114368214_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1847869160_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1847869160(__this, method) ((  bool (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1847869160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3439533315_gshared (Dictionary_2_t2758088665 * __this, KeyValuePair_2_t2656869371  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3439533315(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2758088665 *, KeyValuePair_2_t2656869371 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3439533315_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3250507619_gshared (Dictionary_2_t2758088665 * __this, KeyValuePair_2_t2656869371  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3250507619(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2758088665 *, KeyValuePair_2_t2656869371 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3250507619_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m896660007_gshared (Dictionary_2_t2758088665 * __this, KeyValuePair_2U5BU5D_t3675014586* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m896660007(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2758088665 *, KeyValuePair_2U5BU5D_t3675014586*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m896660007_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1752134472_gshared (Dictionary_2_t2758088665 * __this, KeyValuePair_2_t2656869371  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1752134472(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2758088665 *, KeyValuePair_2_t2656869371 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1752134472_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2089166790_gshared (Dictionary_2_t2758088665 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2089166790(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2089166790_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1775989205_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1775989205(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1775989205_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2014068492_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2014068492(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2014068492_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2631832089_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2631832089(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2631832089_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3907071838_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3907071838(__this, method) ((  int32_t (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_get_Count_m3907071838_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3523920617_gshared (Dictionary_2_t2758088665 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3523920617(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3523920617_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2646810750_gshared (Dictionary_2_t2758088665 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2646810750(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2758088665 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2646810750_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m485840566_gshared (Dictionary_2_t2758088665 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m485840566(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2758088665 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m485840566_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3097579457_gshared (Dictionary_2_t2758088665 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3097579457(__this, ___size0, method) ((  void (*) (Dictionary_2_t2758088665 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3097579457_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3399026173_gshared (Dictionary_2_t2758088665 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3399026173(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3399026173_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2656869371  Dictionary_2_make_pair_m3793390993_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3793390993(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2656869371  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3793390993_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3235151885_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3235151885(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3235151885_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m4020067241_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m4020067241(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m4020067241_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2473343794_gshared (Dictionary_2_t2758088665 * __this, KeyValuePair_2U5BU5D_t3675014586* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2473343794(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2758088665 *, KeyValuePair_2U5BU5D_t3675014586*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2473343794_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3343278266_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3343278266(__this, method) ((  void (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_Resize_m3343278266_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2057571127_gshared (Dictionary_2_t2758088665 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2057571127(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2758088665 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2057571127_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2361273193_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2361273193(__this, method) ((  void (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_Clear_m2361273193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4121581139_gshared (Dictionary_2_t2758088665 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4121581139(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2758088665 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m4121581139_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3313256659_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3313256659(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3313256659_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m162164700_gshared (Dictionary_2_t2758088665 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m162164700(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2758088665 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m162164700_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3900142600_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3900142600(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3900142600_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m674757309_gshared (Dictionary_2_t2758088665 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m674757309(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2758088665 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m674757309_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3400575916_gshared (Dictionary_2_t2758088665 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3400575916(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2758088665 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3400575916_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Keys()
extern "C"  KeyCollection_t89880820 * Dictionary_2_get_Keys_m2366994159_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2366994159(__this, method) ((  KeyCollection_t89880820 * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_get_Keys_m2366994159_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Values()
extern "C"  ValueCollection_t1458694378 * Dictionary_2_get_Values_m2794686411_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2794686411(__this, method) ((  ValueCollection_t1458694378 * (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_get_Values_m2794686411_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2685010792_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2685010792(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2685010792_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2332381380_gshared (Dictionary_2_t2758088665 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2332381380(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t2758088665 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2332381380_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2481242272_gshared (Dictionary_2_t2758088665 * __this, KeyValuePair_2_t2656869371  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2481242272(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2758088665 *, KeyValuePair_2_t2656869371 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2481242272_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::GetEnumerator()
extern "C"  Enumerator_t4075412057  Dictionary_2_GetEnumerator_m981334537_gshared (Dictionary_2_t2758088665 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m981334537(__this, method) ((  Enumerator_t4075412057  (*) (Dictionary_2_t2758088665 *, const MethodInfo*))Dictionary_2_GetEnumerator_m981334537_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m2615038078_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m2615038078(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m2615038078_gshared)(__this /* static, unused */, ___key0, ___value1, method)
