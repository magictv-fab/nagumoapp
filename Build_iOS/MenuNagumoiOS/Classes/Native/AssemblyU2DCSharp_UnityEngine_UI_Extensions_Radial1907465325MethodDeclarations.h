﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.RadialLayout
struct RadialLayout_t1907465325;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.RadialLayout::.ctor()
extern "C"  void RadialLayout__ctor_m773452251 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.RadialLayout::OnEnable()
extern "C"  void RadialLayout_OnEnable_m1488045003 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.RadialLayout::SetLayoutHorizontal()
extern "C"  void RadialLayout_SetLayoutHorizontal_m2791988329 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.RadialLayout::SetLayoutVertical()
extern "C"  void RadialLayout_SetLayoutVertical_m3589532731 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.RadialLayout::CalculateLayoutInputVertical()
extern "C"  void RadialLayout_CalculateLayoutInputVertical_m387524889 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.RadialLayout::CalculateLayoutInputHorizontal()
extern "C"  void RadialLayout_CalculateLayoutInputHorizontal_m859036103 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.RadialLayout::CalculateRadial()
extern "C"  void RadialLayout_CalculateRadial_m3746447582 (RadialLayout_t1907465325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
