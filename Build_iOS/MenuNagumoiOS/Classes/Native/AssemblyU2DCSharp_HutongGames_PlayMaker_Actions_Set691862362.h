﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetSkybox
struct  SetSkybox_t691862362  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetSkybox::skybox
	FsmMaterial_t924399665 * ___skybox_9;
	// System.Boolean HutongGames.PlayMaker.Actions.SetSkybox::everyFrame
	bool ___everyFrame_10;

public:
	inline static int32_t get_offset_of_skybox_9() { return static_cast<int32_t>(offsetof(SetSkybox_t691862362, ___skybox_9)); }
	inline FsmMaterial_t924399665 * get_skybox_9() const { return ___skybox_9; }
	inline FsmMaterial_t924399665 ** get_address_of_skybox_9() { return &___skybox_9; }
	inline void set_skybox_9(FsmMaterial_t924399665 * value)
	{
		___skybox_9 = value;
		Il2CppCodeGenWriteBarrier(&___skybox_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(SetSkybox_t691862362, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
