﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetComponent
struct GetComponent_t2168373359;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetComponent::.ctor()
extern "C"  void GetComponent__ctor_m682621655 (GetComponent_t2168373359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::Reset()
extern "C"  void GetComponent_Reset_m2624021892 (GetComponent_t2168373359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::OnEnter()
extern "C"  void GetComponent_OnEnter_m2556110062 (GetComponent_t2168373359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::OnUpdate()
extern "C"  void GetComponent_OnUpdate_m1063559957 (GetComponent_t2168373359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::DoGetComponent()
extern "C"  void GetComponent_DoGetComponent_m1989697151 (GetComponent_t2168373359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
