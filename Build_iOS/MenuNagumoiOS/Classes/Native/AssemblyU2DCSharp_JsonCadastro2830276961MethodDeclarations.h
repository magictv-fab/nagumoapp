﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonCadastro
struct JsonCadastro_t2830276961;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonCadastro::.ctor()
extern "C"  void JsonCadastro__ctor_m4224013914 (JsonCadastro_t2830276961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
