﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.result.ReturnResultVO
struct  ReturnResultVO_t972950402  : public Il2CppObject
{
public:
	// System.Boolean MagicTV.vo.result.ReturnResultVO::success
	bool ___success_0;
	// System.String MagicTV.vo.result.ReturnResultVO::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(ReturnResultVO_t972950402, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(ReturnResultVO_t972950402, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
