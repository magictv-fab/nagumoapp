﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CameraFadeOut
struct CameraFadeOut_t513845787;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::.ctor()
extern "C"  void CameraFadeOut__ctor_m811390331 (CameraFadeOut_t513845787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::Reset()
extern "C"  void CameraFadeOut_Reset_m2752790568 (CameraFadeOut_t513845787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::OnEnter()
extern "C"  void CameraFadeOut_OnEnter_m1748756114 (CameraFadeOut_t513845787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::OnUpdate()
extern "C"  void CameraFadeOut_OnUpdate_m1805391345 (CameraFadeOut_t513845787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::OnGUI()
extern "C"  void CameraFadeOut_OnGUI_m306788981 (CameraFadeOut_t513845787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
