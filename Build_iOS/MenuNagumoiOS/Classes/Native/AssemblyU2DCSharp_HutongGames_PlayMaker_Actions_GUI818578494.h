﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
struct  GUILayoutBeginScrollView_t818578494  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::scrollPosition
	FsmVector2_t533912881 * ___scrollPosition_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::horizontalScrollbar
	FsmBool_t1075959796 * ___horizontalScrollbar_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::verticalScrollbar
	FsmBool_t1075959796 * ___verticalScrollbar_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::useCustomStyle
	FsmBool_t1075959796 * ___useCustomStyle_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::horizontalStyle
	FsmString_t952858651 * ___horizontalStyle_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::verticalStyle
	FsmString_t952858651 * ___verticalStyle_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::backgroundStyle
	FsmString_t952858651 * ___backgroundStyle_17;

public:
	inline static int32_t get_offset_of_scrollPosition_11() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___scrollPosition_11)); }
	inline FsmVector2_t533912881 * get_scrollPosition_11() const { return ___scrollPosition_11; }
	inline FsmVector2_t533912881 ** get_address_of_scrollPosition_11() { return &___scrollPosition_11; }
	inline void set_scrollPosition_11(FsmVector2_t533912881 * value)
	{
		___scrollPosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___scrollPosition_11, value);
	}

	inline static int32_t get_offset_of_horizontalScrollbar_12() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___horizontalScrollbar_12)); }
	inline FsmBool_t1075959796 * get_horizontalScrollbar_12() const { return ___horizontalScrollbar_12; }
	inline FsmBool_t1075959796 ** get_address_of_horizontalScrollbar_12() { return &___horizontalScrollbar_12; }
	inline void set_horizontalScrollbar_12(FsmBool_t1075959796 * value)
	{
		___horizontalScrollbar_12 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollbar_12, value);
	}

	inline static int32_t get_offset_of_verticalScrollbar_13() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___verticalScrollbar_13)); }
	inline FsmBool_t1075959796 * get_verticalScrollbar_13() const { return ___verticalScrollbar_13; }
	inline FsmBool_t1075959796 ** get_address_of_verticalScrollbar_13() { return &___verticalScrollbar_13; }
	inline void set_verticalScrollbar_13(FsmBool_t1075959796 * value)
	{
		___verticalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier(&___verticalScrollbar_13, value);
	}

	inline static int32_t get_offset_of_useCustomStyle_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___useCustomStyle_14)); }
	inline FsmBool_t1075959796 * get_useCustomStyle_14() const { return ___useCustomStyle_14; }
	inline FsmBool_t1075959796 ** get_address_of_useCustomStyle_14() { return &___useCustomStyle_14; }
	inline void set_useCustomStyle_14(FsmBool_t1075959796 * value)
	{
		___useCustomStyle_14 = value;
		Il2CppCodeGenWriteBarrier(&___useCustomStyle_14, value);
	}

	inline static int32_t get_offset_of_horizontalStyle_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___horizontalStyle_15)); }
	inline FsmString_t952858651 * get_horizontalStyle_15() const { return ___horizontalStyle_15; }
	inline FsmString_t952858651 ** get_address_of_horizontalStyle_15() { return &___horizontalStyle_15; }
	inline void set_horizontalStyle_15(FsmString_t952858651 * value)
	{
		___horizontalStyle_15 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalStyle_15, value);
	}

	inline static int32_t get_offset_of_verticalStyle_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___verticalStyle_16)); }
	inline FsmString_t952858651 * get_verticalStyle_16() const { return ___verticalStyle_16; }
	inline FsmString_t952858651 ** get_address_of_verticalStyle_16() { return &___verticalStyle_16; }
	inline void set_verticalStyle_16(FsmString_t952858651 * value)
	{
		___verticalStyle_16 = value;
		Il2CppCodeGenWriteBarrier(&___verticalStyle_16, value);
	}

	inline static int32_t get_offset_of_backgroundStyle_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___backgroundStyle_17)); }
	inline FsmString_t952858651 * get_backgroundStyle_17() const { return ___backgroundStyle_17; }
	inline FsmString_t952858651 ** get_address_of_backgroundStyle_17() { return &___backgroundStyle_17; }
	inline void set_backgroundStyle_17(FsmString_t952858651 * value)
	{
		___backgroundStyle_17 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundStyle_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
