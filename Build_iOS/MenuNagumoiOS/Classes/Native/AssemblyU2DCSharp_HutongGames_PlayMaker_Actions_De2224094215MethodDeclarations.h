﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DeviceVibrate
struct DeviceVibrate_t2224094215;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::.ctor()
extern "C"  void DeviceVibrate__ctor_m3907775759 (DeviceVibrate_t2224094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::Reset()
extern "C"  void DeviceVibrate_Reset_m1554208700 (DeviceVibrate_t2224094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::OnEnter()
extern "C"  void DeviceVibrate_OnEnter_m962816294 (DeviceVibrate_t2224094215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
