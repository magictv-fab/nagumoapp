﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Correios.Net.Http.Request
struct Request_t788181;
// System.String
struct String_t;
// Correios.Net.Http.Response
struct Response_t724041629;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Correios.Net.Http.Request::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void Request__ctor_m3814120383 (Request_t788181 * __this, String_t* ___url0, String_t* ___dataToSend1, String_t* ___method2, String_t* ___contentType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Http.Request::get_Url()
extern "C"  String_t* Request_get_Url_m3814722782 (Request_t788181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Http.Request::set_Url(System.String)
extern "C"  void Request_set_Url_m2371841563 (Request_t788181 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Http.Request::get_DataToSend()
extern "C"  String_t* Request_get_DataToSend_m1771502784 (Request_t788181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Http.Request::set_DataToSend(System.String)
extern "C"  void Request_set_DataToSend_m2021464875 (Request_t788181 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Http.Request::get_Method()
extern "C"  String_t* Request_get_Method_m151840948 (Request_t788181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Http.Request::set_Method(System.String)
extern "C"  void Request_set_Method_m2356156471 (Request_t788181 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Http.Request::get_ContentType()
extern "C"  String_t* Request_get_ContentType_m277521346 (Request_t788181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Http.Request::set_ContentType(System.String)
extern "C"  void Request_set_ContentType_m748749495 (Request_t788181 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Correios.Net.Http.Response Correios.Net.Http.Request::Send(System.Int32)
extern "C"  Response_t724041629 * Request_Send_m893011429 (Request_t788181 * __this, int32_t ___timeout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
