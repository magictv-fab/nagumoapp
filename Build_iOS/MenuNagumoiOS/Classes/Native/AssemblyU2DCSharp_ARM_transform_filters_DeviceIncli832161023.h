﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.DeviceInclinationCameraFilter
struct  DeviceInclinationCameraFilter_t832161023  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// System.Int32 ARM.transform.filters.DeviceInclinationCameraFilter::sizeFilter
	int32_t ___sizeFilter_15;
	// UnityEngine.Vector3[] ARM.transform.filters.DeviceInclinationCameraFilter::_filters
	Vector3U5BU5D_t215400611* ____filters_16;
	// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::_filtersum
	Vector3_t4282066566  ____filtersum_17;
	// System.Int32 ARM.transform.filters.DeviceInclinationCameraFilter::posFilter
	int32_t ___posFilter_18;
	// System.Int32 ARM.transform.filters.DeviceInclinationCameraFilter::countSamples
	int32_t ___countSamples_19;
	// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::lastSample
	Vector3_t4282066566  ___lastSample_20;
	// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::calibration
	Vector3_t4282066566  ___calibration_21;
	// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::DebugRotationLast
	Vector3_t4282066566  ___DebugRotationLast_22;
	// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::DebugRotationNext
	Vector3_t4282066566  ___DebugRotationNext_23;
	// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::DebugRotationOut
	Vector3_t4282066566  ___DebugRotationOut_24;

public:
	inline static int32_t get_offset_of_sizeFilter_15() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___sizeFilter_15)); }
	inline int32_t get_sizeFilter_15() const { return ___sizeFilter_15; }
	inline int32_t* get_address_of_sizeFilter_15() { return &___sizeFilter_15; }
	inline void set_sizeFilter_15(int32_t value)
	{
		___sizeFilter_15 = value;
	}

	inline static int32_t get_offset_of__filters_16() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ____filters_16)); }
	inline Vector3U5BU5D_t215400611* get__filters_16() const { return ____filters_16; }
	inline Vector3U5BU5D_t215400611** get_address_of__filters_16() { return &____filters_16; }
	inline void set__filters_16(Vector3U5BU5D_t215400611* value)
	{
		____filters_16 = value;
		Il2CppCodeGenWriteBarrier(&____filters_16, value);
	}

	inline static int32_t get_offset_of__filtersum_17() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ____filtersum_17)); }
	inline Vector3_t4282066566  get__filtersum_17() const { return ____filtersum_17; }
	inline Vector3_t4282066566 * get_address_of__filtersum_17() { return &____filtersum_17; }
	inline void set__filtersum_17(Vector3_t4282066566  value)
	{
		____filtersum_17 = value;
	}

	inline static int32_t get_offset_of_posFilter_18() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___posFilter_18)); }
	inline int32_t get_posFilter_18() const { return ___posFilter_18; }
	inline int32_t* get_address_of_posFilter_18() { return &___posFilter_18; }
	inline void set_posFilter_18(int32_t value)
	{
		___posFilter_18 = value;
	}

	inline static int32_t get_offset_of_countSamples_19() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___countSamples_19)); }
	inline int32_t get_countSamples_19() const { return ___countSamples_19; }
	inline int32_t* get_address_of_countSamples_19() { return &___countSamples_19; }
	inline void set_countSamples_19(int32_t value)
	{
		___countSamples_19 = value;
	}

	inline static int32_t get_offset_of_lastSample_20() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___lastSample_20)); }
	inline Vector3_t4282066566  get_lastSample_20() const { return ___lastSample_20; }
	inline Vector3_t4282066566 * get_address_of_lastSample_20() { return &___lastSample_20; }
	inline void set_lastSample_20(Vector3_t4282066566  value)
	{
		___lastSample_20 = value;
	}

	inline static int32_t get_offset_of_calibration_21() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___calibration_21)); }
	inline Vector3_t4282066566  get_calibration_21() const { return ___calibration_21; }
	inline Vector3_t4282066566 * get_address_of_calibration_21() { return &___calibration_21; }
	inline void set_calibration_21(Vector3_t4282066566  value)
	{
		___calibration_21 = value;
	}

	inline static int32_t get_offset_of_DebugRotationLast_22() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___DebugRotationLast_22)); }
	inline Vector3_t4282066566  get_DebugRotationLast_22() const { return ___DebugRotationLast_22; }
	inline Vector3_t4282066566 * get_address_of_DebugRotationLast_22() { return &___DebugRotationLast_22; }
	inline void set_DebugRotationLast_22(Vector3_t4282066566  value)
	{
		___DebugRotationLast_22 = value;
	}

	inline static int32_t get_offset_of_DebugRotationNext_23() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___DebugRotationNext_23)); }
	inline Vector3_t4282066566  get_DebugRotationNext_23() const { return ___DebugRotationNext_23; }
	inline Vector3_t4282066566 * get_address_of_DebugRotationNext_23() { return &___DebugRotationNext_23; }
	inline void set_DebugRotationNext_23(Vector3_t4282066566  value)
	{
		___DebugRotationNext_23 = value;
	}

	inline static int32_t get_offset_of_DebugRotationOut_24() { return static_cast<int32_t>(offsetof(DeviceInclinationCameraFilter_t832161023, ___DebugRotationOut_24)); }
	inline Vector3_t4282066566  get_DebugRotationOut_24() const { return ___DebugRotationOut_24; }
	inline Vector3_t4282066566 * get_address_of_DebugRotationOut_24() { return &___DebugRotationOut_24; }
	inline void set_DebugRotationOut_24(Vector3_t4282066566  value)
	{
		___DebugRotationOut_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
