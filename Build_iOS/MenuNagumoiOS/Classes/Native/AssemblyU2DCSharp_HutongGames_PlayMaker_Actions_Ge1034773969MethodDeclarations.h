﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAngleToTarget
struct GetAngleToTarget_t1034773969;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::.ctor()
extern "C"  void GetAngleToTarget__ctor_m1451390901 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::Reset()
extern "C"  void GetAngleToTarget_Reset_m3392791138 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::OnLateUpdate()
extern "C"  void GetAngleToTarget_OnLateUpdate_m3267609533 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::DoGetAngleToTarget()
extern "C"  void GetAngleToTarget_DoGetAngleToTarget_m677929539 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
