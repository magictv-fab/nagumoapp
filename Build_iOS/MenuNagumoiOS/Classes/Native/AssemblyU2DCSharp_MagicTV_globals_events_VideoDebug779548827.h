﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler
struct OnFloatEventHandler_t1243657927;
// MagicTV.globals.events.VideoDebugEvents/OnStringEventHandler
struct OnStringEventHandler_t1157926502;
// MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler
struct OnMetaEventHandler_t4203203482;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.VideoDebugEvents
struct  VideoDebugEvents_t779548827  : public Il2CppObject
{
public:

public:
};

struct VideoDebugEvents_t779548827_StaticFields
{
public:
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::event1
	OnFloatEventHandler_t1243657927 * ___event1_0;
	// MagicTV.globals.events.VideoDebugEvents/OnStringEventHandler MagicTV.globals.events.VideoDebugEvents::stringEvent
	OnStringEventHandler_t1157926502 * ___stringEvent_1;
	// MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler MagicTV.globals.events.VideoDebugEvents::onSaveEvent
	OnMetaEventHandler_t4203203482 * ___onSaveEvent_2;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::event2
	OnFloatEventHandler_t1243657927 * ___event2_3;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::event3
	OnFloatEventHandler_t1243657927 * ___event3_4;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::event4
	OnFloatEventHandler_t1243657927 * ___event4_5;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::change1
	OnFloatEventHandler_t1243657927 * ___change1_6;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::change2
	OnFloatEventHandler_t1243657927 * ___change2_7;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::change3
	OnFloatEventHandler_t1243657927 * ___change3_8;
	// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler MagicTV.globals.events.VideoDebugEvents::change4
	OnFloatEventHandler_t1243657927 * ___change4_9;

public:
	inline static int32_t get_offset_of_event1_0() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___event1_0)); }
	inline OnFloatEventHandler_t1243657927 * get_event1_0() const { return ___event1_0; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_event1_0() { return &___event1_0; }
	inline void set_event1_0(OnFloatEventHandler_t1243657927 * value)
	{
		___event1_0 = value;
		Il2CppCodeGenWriteBarrier(&___event1_0, value);
	}

	inline static int32_t get_offset_of_stringEvent_1() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___stringEvent_1)); }
	inline OnStringEventHandler_t1157926502 * get_stringEvent_1() const { return ___stringEvent_1; }
	inline OnStringEventHandler_t1157926502 ** get_address_of_stringEvent_1() { return &___stringEvent_1; }
	inline void set_stringEvent_1(OnStringEventHandler_t1157926502 * value)
	{
		___stringEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___stringEvent_1, value);
	}

	inline static int32_t get_offset_of_onSaveEvent_2() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___onSaveEvent_2)); }
	inline OnMetaEventHandler_t4203203482 * get_onSaveEvent_2() const { return ___onSaveEvent_2; }
	inline OnMetaEventHandler_t4203203482 ** get_address_of_onSaveEvent_2() { return &___onSaveEvent_2; }
	inline void set_onSaveEvent_2(OnMetaEventHandler_t4203203482 * value)
	{
		___onSaveEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___onSaveEvent_2, value);
	}

	inline static int32_t get_offset_of_event2_3() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___event2_3)); }
	inline OnFloatEventHandler_t1243657927 * get_event2_3() const { return ___event2_3; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_event2_3() { return &___event2_3; }
	inline void set_event2_3(OnFloatEventHandler_t1243657927 * value)
	{
		___event2_3 = value;
		Il2CppCodeGenWriteBarrier(&___event2_3, value);
	}

	inline static int32_t get_offset_of_event3_4() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___event3_4)); }
	inline OnFloatEventHandler_t1243657927 * get_event3_4() const { return ___event3_4; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_event3_4() { return &___event3_4; }
	inline void set_event3_4(OnFloatEventHandler_t1243657927 * value)
	{
		___event3_4 = value;
		Il2CppCodeGenWriteBarrier(&___event3_4, value);
	}

	inline static int32_t get_offset_of_event4_5() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___event4_5)); }
	inline OnFloatEventHandler_t1243657927 * get_event4_5() const { return ___event4_5; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_event4_5() { return &___event4_5; }
	inline void set_event4_5(OnFloatEventHandler_t1243657927 * value)
	{
		___event4_5 = value;
		Il2CppCodeGenWriteBarrier(&___event4_5, value);
	}

	inline static int32_t get_offset_of_change1_6() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___change1_6)); }
	inline OnFloatEventHandler_t1243657927 * get_change1_6() const { return ___change1_6; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_change1_6() { return &___change1_6; }
	inline void set_change1_6(OnFloatEventHandler_t1243657927 * value)
	{
		___change1_6 = value;
		Il2CppCodeGenWriteBarrier(&___change1_6, value);
	}

	inline static int32_t get_offset_of_change2_7() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___change2_7)); }
	inline OnFloatEventHandler_t1243657927 * get_change2_7() const { return ___change2_7; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_change2_7() { return &___change2_7; }
	inline void set_change2_7(OnFloatEventHandler_t1243657927 * value)
	{
		___change2_7 = value;
		Il2CppCodeGenWriteBarrier(&___change2_7, value);
	}

	inline static int32_t get_offset_of_change3_8() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___change3_8)); }
	inline OnFloatEventHandler_t1243657927 * get_change3_8() const { return ___change3_8; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_change3_8() { return &___change3_8; }
	inline void set_change3_8(OnFloatEventHandler_t1243657927 * value)
	{
		___change3_8 = value;
		Il2CppCodeGenWriteBarrier(&___change3_8, value);
	}

	inline static int32_t get_offset_of_change4_9() { return static_cast<int32_t>(offsetof(VideoDebugEvents_t779548827_StaticFields, ___change4_9)); }
	inline OnFloatEventHandler_t1243657927 * get_change4_9() const { return ___change4_9; }
	inline OnFloatEventHandler_t1243657927 ** get_address_of_change4_9() { return &___change4_9; }
	inline void set_change4_9(OnFloatEventHandler_t1243657927 * value)
	{
		___change4_9 = value;
		Il2CppCodeGenWriteBarrier(&___change4_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
