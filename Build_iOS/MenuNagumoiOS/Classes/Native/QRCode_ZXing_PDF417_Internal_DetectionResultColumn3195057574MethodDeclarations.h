﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.DetectionResultColumn
struct DetectionResultColumn_t3195057574;
// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;
// ZXing.PDF417.Internal.Codeword[]
struct CodewordU5BU5D_t399100150;
// ZXing.PDF417.Internal.Codeword
struct Codeword_t1124156527;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_BoundingBox242606069.h"
#include "QRCode_ZXing_PDF417_Internal_Codeword1124156527.h"

// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResultColumn::get_Box()
extern "C"  BoundingBox_t242606069 * DetectionResultColumn_get_Box_m3658852009 (DetectionResultColumn_t3195057574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultColumn::set_Box(ZXing.PDF417.Internal.BoundingBox)
extern "C"  void DetectionResultColumn_set_Box_m3984400044 (DetectionResultColumn_t3195057574 * __this, BoundingBox_t242606069 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.Codeword[] ZXing.PDF417.Internal.DetectionResultColumn::get_Codewords()
extern "C"  CodewordU5BU5D_t399100150* DetectionResultColumn_get_Codewords_m3470021384 (DetectionResultColumn_t3195057574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultColumn::set_Codewords(ZXing.PDF417.Internal.Codeword[])
extern "C"  void DetectionResultColumn_set_Codewords_m653224855 (DetectionResultColumn_t3195057574 * __this, CodewordU5BU5D_t399100150* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultColumn::.ctor(ZXing.PDF417.Internal.BoundingBox)
extern "C"  void DetectionResultColumn__ctor_m1907034616 (DetectionResultColumn_t3195057574 * __this, BoundingBox_t242606069 * ___box0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::IndexForRow(System.Int32)
extern "C"  int32_t DetectionResultColumn_IndexForRow_m1270507976 (DetectionResultColumn_t3195057574 * __this, int32_t ___imageRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.DetectionResultColumn::getCodeword(System.Int32)
extern "C"  Codeword_t1124156527 * DetectionResultColumn_getCodeword_m221347989 (DetectionResultColumn_t3195057574 * __this, int32_t ___imageRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.DetectionResultColumn::getCodewordNearby(System.Int32)
extern "C"  Codeword_t1124156527 * DetectionResultColumn_getCodewordNearby_m841283252 (DetectionResultColumn_t3195057574 * __this, int32_t ___imageRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::imageRowToCodewordIndex(System.Int32)
extern "C"  int32_t DetectionResultColumn_imageRowToCodewordIndex_m1970978438 (DetectionResultColumn_t3195057574 * __this, int32_t ___imageRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultColumn::setCodeword(System.Int32,ZXing.PDF417.Internal.Codeword)
extern "C"  void DetectionResultColumn_setCodeword_m3358378826 (DetectionResultColumn_t3195057574 * __this, int32_t ___imageRow0, Codeword_t1124156527 * ___codeword1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.DetectionResultColumn::ToString()
extern "C"  String_t* DetectionResultColumn_ToString_m4194455525 (DetectionResultColumn_t3195057574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
