﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_Network1492793700.h"
#include "UnityEngine_UnityEngine_BitStream239125475.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_HostData3270478838.h"
#include "UnityEngine_UnityEngine_MasterServer2117519177.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties624110065.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker4185719096.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "UnityEngine_UnityEngine_TextAsset3836129977.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_SortingLayer3376264497.h"
#include "UnityEngine_UnityEngine_SpriteMeshType2570694128.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility1448936472.h"
#include "UnityEngine_UnityEngine_Hash128346790303.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "UnityEngine_UnityEngine_WWWTranscoder609724394.h"
#include "UnityEngine_UnityEngine_UnityString3369712284.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "UnityEngine_UnityEngine_Application2856536070.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback1945583101.h"
#include "UnityEngine_UnityEngine_DebugLogHandler2406589519.h"
#include "UnityEngine_UnityEngine_Debug4195163081.h"
#include "UnityEngine_UnityEngine_Display1321072632.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDele581305515.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"
#include "UnityEngine_UnityEngine_IMECompositionMode3300198960.h"
#include "UnityEngine_UnityEngine_TouchType970257423.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_DeviceOrientation1141857680.h"
#include "UnityEngine_UnityEngine_Gyroscope932197883.h"
#include "UnityEngine_UnityEngine_LocationInfo3215517959.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus1153071304.h"
#include "UnityEngine_UnityEngine_LocationService3853025142.h"
#include "UnityEngine_UnityEngine_Compass599792712.h"
#include "UnityEngine_UnityEngine_Input4200062272.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Light4202674828.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846.h"
#include "UnityEngine_UnityEngine_Time4241968337.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147.h"
#include "UnityEngine_UnityEngine_jvalue3862675307.h"
#include "UnityEngine_UnityEngine_AndroidJNIHelper1095326088.h"
#include "UnityEngine_UnityEngine_AndroidJNI3901305722.h"
#include "UnityEngine_UnityEngine_Motion3026528250.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView2392257822.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3510840818.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3604098244.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai2244784594.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd3393793052.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inter3068494210.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inters507319425.h"
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration572715224.h"
#include "UnityEngine_UnityEngine_iOS_Device1621710112.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier514511185.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit3375281836.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1344855248.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1652317979.h"
#include "UnityEngine_UnityEngine_iOS_NotificationType3233079183.h"
#include "UnityEngine_UnityEngine_iOS_NotificationServices2332829491.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2782999289.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1315979427.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel4269758102.h"
#include "UnityEngine_UnityEngine_ParticleCollisionEvent2700926194.h"
#include "UnityEngine_UnityEngine_ParticleSystemExtensionsIm1014071565.h"
#include "UnityEngine_UnityEngine_ParticlePhysicsExtensions3791554251.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (NetworkViewID_t3400394436)+ sizeof (Il2CppObject), sizeof(NetworkViewID_t3400394436_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[3] = 
{
	NetworkViewID_t3400394436::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3400394436::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3400394436::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (NetworkView_t3656680617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (Network_t1492793700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (BitStream_t239125475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	BitStream_t239125475::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (RPC_t3134615963), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (HostData_t3270478838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[10] = 
{
	HostData_t3270478838::get_offset_of_m_Nat_0(),
	HostData_t3270478838::get_offset_of_m_GameType_1(),
	HostData_t3270478838::get_offset_of_m_GameName_2(),
	HostData_t3270478838::get_offset_of_m_ConnectedPlayers_3(),
	HostData_t3270478838::get_offset_of_m_PlayerLimit_4(),
	HostData_t3270478838::get_offset_of_m_IP_5(),
	HostData_t3270478838::get_offset_of_m_Port_6(),
	HostData_t3270478838::get_offset_of_m_PasswordProtected_7(),
	HostData_t3270478838::get_offset_of_m_Comment_8(),
	HostData_t3270478838::get_offset_of_m_GUID_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (MasterServer_t2117519177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (NetworkMessageInfo_t3807997963)+ sizeof (Il2CppObject), sizeof(NetworkMessageInfo_t3807997963_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2107[3] = 
{
	NetworkMessageInfo_t3807997963::get_offset_of_m_TimeStamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t3807997963::get_offset_of_m_Sender_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t3807997963::get_offset_of_m_ViewID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (DrivenTransformProperties_t624110065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[26] = 
{
	DrivenTransformProperties_t624110065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (DrivenRectTransformTracker_t4185719096)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t4185719096_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (RectTransform_t972643934), -1, sizeof(RectTransform_t972643934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2110[1] = 
{
	RectTransform_t972643934_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Edge_t1676794651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[5] = 
{
	Edge_t1676794651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (Axis_t1676694783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	Axis_t1676694783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (ReapplyDrivenProperties_t779639188), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (ResourceRequest_t3731857623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[2] = 
{
	ResourceRequest_t3731857623::get_offset_of_m_Path_1(),
	ResourceRequest_t3731857623::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (Resources_t2918352667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (TextAsset_t3836129977), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (SerializePrivateVariables_t2835496234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (SerializeField_t3754825534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (Shader_t3191267369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (Material_t3870600107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (SortingLayer_t3376264497)+ sizeof (Il2CppObject), sizeof(SortingLayer_t3376264497_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[1] = 
{
	SortingLayer_t3376264497::get_offset_of_m_Id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (SpriteMeshType_t2570694128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2123[3] = 
{
	SpriteMeshType_t2570694128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (Sprite_t3199167241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (SpriteRenderer_t2548470764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (DataUtility_t1448936472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (Hash128_t346790303)+ sizeof (Il2CppObject), sizeof(Hash128_t346790303_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2127[4] = 
{
	Hash128_t346790303::get_offset_of_m_u32_0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (WWW_t3134621005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[1] = 
{
	WWW_t3134621005::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (WWWForm_t461342257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[6] = 
{
	WWWForm_t461342257::get_offset_of_formData_0(),
	WWWForm_t461342257::get_offset_of_fieldNames_1(),
	WWWForm_t461342257::get_offset_of_fileNames_2(),
	WWWForm_t461342257::get_offset_of_types_3(),
	WWWForm_t461342257::get_offset_of_boundary_4(),
	WWWForm_t461342257::get_offset_of_containsFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (WWWTranscoder_t609724394), -1, sizeof(WWWTranscoder_t609724394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[8] = 
{
	WWWTranscoder_t609724394_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlForbidden_4(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpEscapeChar_5(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpSpace_6(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpForbidden_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (UnityString_t3369712284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (AsyncOperation_t3699081103), sizeof(AsyncOperation_t3699081103_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2132[1] = 
{
	AsyncOperation_t3699081103::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (NetworkReachability_t612403035)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[4] = 
{
	NetworkReachability_t612403035::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (Application_t2856536070), -1, sizeof(Application_t2856536070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2134[2] = 
{
	Application_t2856536070_StaticFields::get_offset_of_s_LogCallbackHandler_0(),
	Application_t2856536070_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (LogCallback_t2984951347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (Behaviour_t200106419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (Camera_t2727095145), -1, sizeof(Camera_t2727095145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[3] = 
{
	Camera_t2727095145_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t2727095145_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t2727095145_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (CameraCallback_t1945583101), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (DebugLogHandler_t2406589519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (Debug_t4195163081), -1, sizeof(Debug_t4195163081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2140[1] = 
{
	Debug_t4195163081_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (Display_t1321072632), -1, sizeof(Display_t1321072632_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2141[4] = 
{
	Display_t1321072632::get_offset_of_nativeDisplay_0(),
	Display_t1321072632_StaticFields::get_offset_of_displays_1(),
	Display_t1321072632_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t1321072632_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (DisplaysUpdatedDelegate_t581305515), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (MonoBehaviour_t667441552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (TouchPhase_t1567063616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2144[6] = 
{
	TouchPhase_t1567063616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (IMECompositionMode_t3300198960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2145[4] = 
{
	IMECompositionMode_t3300198960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (TouchType_t970257423)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2146[4] = 
{
	TouchType_t970257423::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (Touch_t4210255029)+ sizeof (Il2CppObject), sizeof(Touch_t4210255029_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2147[14] = 
{
	Touch_t4210255029::get_offset_of_m_FingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_RawPosition_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_PositionDelta_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_TimeDelta_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_TapCount_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Phase_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Type_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Pressure_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_maximumPossiblePressure_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Radius_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_RadiusVariance_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_AltitudeAngle_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_AzimuthAngle_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (DeviceOrientation_t1141857680)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2148[8] = 
{
	DeviceOrientation_t1141857680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (Gyroscope_t932197883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	Gyroscope_t932197883::get_offset_of_m_GyroIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (LocationInfo_t3215517959)+ sizeof (Il2CppObject), sizeof(LocationInfo_t3215517959_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[6] = 
{
	LocationInfo_t3215517959::get_offset_of_m_Timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_Latitude_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_Longitude_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_Altitude_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_HorizontalAccuracy_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_VerticalAccuracy_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (LocationServiceStatus_t1153071304)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2151[5] = 
{
	LocationServiceStatus_t1153071304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (LocationService_t3853025142), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (Compass_t599792712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (Input_t4200062272), -1, sizeof(Input_t4200062272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[3] = 
{
	Input_t4200062272_StaticFields::get_offset_of_m_MainGyro_0(),
	Input_t4200062272_StaticFields::get_offset_of_locationServiceInstance_1(),
	Input_t4200062272_StaticFields::get_offset_of_compassInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (HideFlags_t1436803931)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[10] = 
{
	HideFlags_t1436803931::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (Object_t3071478659), sizeof(Object_t3071478659_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[2] = 
{
	Object_t3071478659::get_offset_of_m_InstanceID_0(),
	Object_t3071478659::get_offset_of_m_CachedPtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (Component_t3501516275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (Light_t4202674828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (GameObject_t3674682005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (Transform_t1659122786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (Enumerator_t3875554846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[2] = 
{
	Enumerator_t3875554846::get_offset_of_outer_0(),
	Enumerator_t3875554846::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (Time_t4241968337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (Random_t3156561159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (YieldInstruction_t2048002629), sizeof(YieldInstruction_t2048002629_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (PlayerPrefsException_t3680716996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (PlayerPrefs_t1845493509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (AndroidJavaObject_t2362096582), -1, sizeof(AndroidJavaObject_t2362096582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2167[5] = 
{
	AndroidJavaObject_t2362096582_StaticFields::get_offset_of_enableDebugPrints_0(),
	AndroidJavaObject_t2362096582::get_offset_of_m_disposed_1(),
	AndroidJavaObject_t2362096582::get_offset_of_m_jobject_2(),
	AndroidJavaObject_t2362096582::get_offset_of_m_jclass_3(),
	AndroidJavaObject_t2362096582_StaticFields::get_offset_of_s_JavaLangClass_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (AndroidJavaClass_t1816259147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (jvalue_t3862675307)+ sizeof (Il2CppObject), sizeof(jvalue_t3862675307_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2169[9] = 
{
	jvalue_t3862675307::get_offset_of_z_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_s_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_i_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_j_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_f_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_d_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	jvalue_t3862675307::get_offset_of_l_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (AndroidJNIHelper_t1095326088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (AndroidJNI_t3901305722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (Motion_t3026528250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ADBannerView_t2392257822), -1, sizeof(ADBannerView_t2392257822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2173[4] = 
{
	ADBannerView_t2392257822::get_offset_of__bannerView_0(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasClicked_1(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasLoaded_2(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerFailedToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (BannerWasClickedDelegate_t3510840818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (BannerWasLoadedDelegate_t3604098244), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (BannerFailedToLoadDelegate_t2244784594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (ADInterstitialAd_t3393793052), -1, sizeof(ADInterstitialAd_t3393793052_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2177[3] = 
{
	ADInterstitialAd_t3393793052::get_offset_of_interstitialView_0(),
	ADInterstitialAd_t3393793052_StaticFields::get_offset_of_onInterstitialWasLoaded_1(),
	ADInterstitialAd_t3393793052_StaticFields::get_offset_of_onInterstitialWasViewed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (InterstitialWasLoadedDelegate_t3068494210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (InterstitialWasViewedDelegate_t507319425), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (DeviceGeneration_t572715224)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2180[38] = 
{
	DeviceGeneration_t572715224::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (Device_t1621710112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (CalendarIdentifier_t514511185)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2182[12] = 
{
	CalendarIdentifier_t514511185::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (CalendarUnit_t3375281836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2183[12] = 
{
	CalendarUnit_t3375281836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (LocalNotification_t1344855248), -1, sizeof(LocalNotification_t1344855248_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[2] = 
{
	LocalNotification_t1344855248::get_offset_of_notificationWrapper_0(),
	LocalNotification_t1344855248_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (RemoteNotification_t1652317979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[1] = 
{
	RemoteNotification_t1652317979::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (NotificationType_t3233079183)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2186[5] = 
{
	NotificationType_t3233079183::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (NotificationServices_t2332829491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (DirectorPlayer_t2782999289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (PlayState_t1315979427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	PlayState_t1315979427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (Playable_t70832698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[2] = 
{
	Playable_t70832698::get_offset_of_m_Ptr_0(),
	Playable_t70832698::get_offset_of_m_UniqueId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (LoadSceneMode_t3067001883)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2191[3] = 
{
	LoadSceneMode_t3067001883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (SceneManager_t2940962239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (Scene_t1080795294)+ sizeof (Il2CppObject), sizeof(Scene_t1080795294_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[1] = 
{
	Scene_t1080795294::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (ParticleSystem_t381473177), -1, sizeof(ParticleSystem_t381473177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2194[5] = 
{
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (IteratorDelegate_t4269758102), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ParticleCollisionEvent_t2700926194)+ sizeof (Il2CppObject), sizeof(ParticleCollisionEvent_t2700926194_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2196[4] = 
{
	ParticleCollisionEvent_t2700926194::get_offset_of_m_Intersection_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParticleCollisionEvent_t2700926194::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParticleCollisionEvent_t2700926194::get_offset_of_m_Velocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParticleCollisionEvent_t2700926194::get_offset_of_m_ColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (ParticleSystemExtensionsImpl_t1014071565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (ParticlePhysicsExtensions_t3791554251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (ForceMode_t2134283300)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[5] = 
{
	ForceMode_t2134283300::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
