﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetHostData
struct MasterServerGetHostData_t2799670129;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::.ctor()
extern "C"  void MasterServerGetHostData__ctor_m1766034149 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::Reset()
extern "C"  void MasterServerGetHostData_Reset_m3707434386 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::OnEnter()
extern "C"  void MasterServerGetHostData_OnEnter_m38463868 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::GetHostData()
extern "C"  void MasterServerGetHostData_GetHostData_m1729793899 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
