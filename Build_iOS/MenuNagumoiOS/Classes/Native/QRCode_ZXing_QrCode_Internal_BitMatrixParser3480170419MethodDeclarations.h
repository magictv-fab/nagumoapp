﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.BitMatrixParser
struct BitMatrixParser_t3480170419;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.QrCode.Internal.FormatInformation
struct FormatInformation_t3156512315;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.QrCode.Internal.BitMatrixParser ZXing.QrCode.Internal.BitMatrixParser::createBitMatrixParser(ZXing.Common.BitMatrix)
extern "C"  BitMatrixParser_t3480170419 * BitMatrixParser_createBitMatrixParser_m3672390644 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___bitMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern "C"  void BitMatrixParser__ctor_m671400996 (BitMatrixParser_t3480170419 * __this, BitMatrix_t1058711404 * ___bitMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.BitMatrixParser::readFormatInformation()
extern "C"  FormatInformation_t3156512315 * BitMatrixParser_readFormatInformation_m2788133039 (BitMatrixParser_t3480170419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.BitMatrixParser::readVersion()
extern "C"  Version_t1953509534 * BitMatrixParser_readVersion_m2081098383 (BitMatrixParser_t3480170419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.BitMatrixParser::copyBit(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t BitMatrixParser_copyBit_m3168836942 (BitMatrixParser_t3480170419 * __this, int32_t ___i0, int32_t ___j1, int32_t ___versionBits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.QrCode.Internal.BitMatrixParser::readCodewords()
extern "C"  ByteU5BU5D_t4260760469* BitMatrixParser_readCodewords_m3424439879 (BitMatrixParser_t3480170419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.BitMatrixParser::remask()
extern "C"  void BitMatrixParser_remask_m4066574890 (BitMatrixParser_t3480170419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.BitMatrixParser::setMirror(System.Boolean)
extern "C"  void BitMatrixParser_setMirror_m3361398127 (BitMatrixParser_t3480170419 * __this, bool ___mirror0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.BitMatrixParser::mirror()
extern "C"  void BitMatrixParser_mirror_m3356238410 (BitMatrixParser_t3480170419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
