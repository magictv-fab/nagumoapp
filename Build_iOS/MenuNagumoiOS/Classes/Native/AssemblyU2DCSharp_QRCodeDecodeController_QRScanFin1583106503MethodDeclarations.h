﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QRCodeDecodeController/QRScanFinished
struct QRScanFinished_t1583106503;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void QRCodeDecodeController/QRScanFinished::.ctor(System.Object,System.IntPtr)
extern "C"  void QRScanFinished__ctor_m2912328222 (QRScanFinished_t1583106503 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController/QRScanFinished::Invoke(System.String)
extern "C"  void QRScanFinished_Invoke_m3755778570 (QRScanFinished_t1583106503 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult QRCodeDecodeController/QRScanFinished::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * QRScanFinished_BeginInvoke_m3255314967 (QRScanFinished_t1583106503 * __this, String_t* ___str0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController/QRScanFinished::EndInvoke(System.IAsyncResult)
extern "C"  void QRScanFinished_EndInvoke_m806865710 (QRScanFinished_t1583106503 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
