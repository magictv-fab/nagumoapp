﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.BenchmarkAbstract
struct BenchmarkAbstract_t3666376745;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.BenchmarkAbstract
struct  BenchmarkAbstract_t3666376745  : public GenericStartableComponentControllAbstract_t1794600275
{
public:

public:
};

struct BenchmarkAbstract_t3666376745_StaticFields
{
public:
	// MagicTV.abstracts.BenchmarkAbstract MagicTV.abstracts.BenchmarkAbstract::Instance
	BenchmarkAbstract_t3666376745 * ___Instance_8;

public:
	inline static int32_t get_offset_of_Instance_8() { return static_cast<int32_t>(offsetof(BenchmarkAbstract_t3666376745_StaticFields, ___Instance_8)); }
	inline BenchmarkAbstract_t3666376745 * get_Instance_8() const { return ___Instance_8; }
	inline BenchmarkAbstract_t3666376745 ** get_address_of_Instance_8() { return &___Instance_8; }
	inline void set_Instance_8(BenchmarkAbstract_t3666376745 * value)
	{
		___Instance_8 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
