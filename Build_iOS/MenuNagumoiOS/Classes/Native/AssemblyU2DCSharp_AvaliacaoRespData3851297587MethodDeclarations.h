﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AvaliacaoRespData
struct AvaliacaoRespData_t3851297587;

#include "codegen/il2cpp-codegen.h"

// System.Void AvaliacaoRespData::.ctor()
extern "C"  void AvaliacaoRespData__ctor_m3340661656 (AvaliacaoRespData_t3851297587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
