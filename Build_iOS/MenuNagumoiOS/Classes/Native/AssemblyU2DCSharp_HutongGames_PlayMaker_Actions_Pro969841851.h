﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Pr4287178604.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ProjectLocationToMap
struct  ProjectLocationToMap_t969841851  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ProjectLocationToMap::GPSLocation
	FsmVector3_t533912882 * ___GPSLocation_9;
	// HutongGames.PlayMaker.Actions.ProjectLocationToMap/MapProjection HutongGames.PlayMaker.Actions.ProjectLocationToMap::mapProjection
	int32_t ___mapProjection_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minLongitude
	FsmFloat_t2134102846 * ___minLongitude_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::maxLongitude
	FsmFloat_t2134102846 * ___maxLongitude_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minLatitude
	FsmFloat_t2134102846 * ___minLatitude_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::maxLatitude
	FsmFloat_t2134102846 * ___maxLatitude_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minX
	FsmFloat_t2134102846 * ___minX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minY
	FsmFloat_t2134102846 * ___minY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::width
	FsmFloat_t2134102846 * ___width_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::height
	FsmFloat_t2134102846 * ___height_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::projectedX
	FsmFloat_t2134102846 * ___projectedX_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::projectedY
	FsmFloat_t2134102846 * ___projectedY_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ProjectLocationToMap::normalized
	FsmBool_t1075959796 * ___normalized_21;
	// System.Boolean HutongGames.PlayMaker.Actions.ProjectLocationToMap::everyFrame
	bool ___everyFrame_22;
	// System.Single HutongGames.PlayMaker.Actions.ProjectLocationToMap::x
	float ___x_23;
	// System.Single HutongGames.PlayMaker.Actions.ProjectLocationToMap::y
	float ___y_24;

public:
	inline static int32_t get_offset_of_GPSLocation_9() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___GPSLocation_9)); }
	inline FsmVector3_t533912882 * get_GPSLocation_9() const { return ___GPSLocation_9; }
	inline FsmVector3_t533912882 ** get_address_of_GPSLocation_9() { return &___GPSLocation_9; }
	inline void set_GPSLocation_9(FsmVector3_t533912882 * value)
	{
		___GPSLocation_9 = value;
		Il2CppCodeGenWriteBarrier(&___GPSLocation_9, value);
	}

	inline static int32_t get_offset_of_mapProjection_10() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___mapProjection_10)); }
	inline int32_t get_mapProjection_10() const { return ___mapProjection_10; }
	inline int32_t* get_address_of_mapProjection_10() { return &___mapProjection_10; }
	inline void set_mapProjection_10(int32_t value)
	{
		___mapProjection_10 = value;
	}

	inline static int32_t get_offset_of_minLongitude_11() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___minLongitude_11)); }
	inline FsmFloat_t2134102846 * get_minLongitude_11() const { return ___minLongitude_11; }
	inline FsmFloat_t2134102846 ** get_address_of_minLongitude_11() { return &___minLongitude_11; }
	inline void set_minLongitude_11(FsmFloat_t2134102846 * value)
	{
		___minLongitude_11 = value;
		Il2CppCodeGenWriteBarrier(&___minLongitude_11, value);
	}

	inline static int32_t get_offset_of_maxLongitude_12() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___maxLongitude_12)); }
	inline FsmFloat_t2134102846 * get_maxLongitude_12() const { return ___maxLongitude_12; }
	inline FsmFloat_t2134102846 ** get_address_of_maxLongitude_12() { return &___maxLongitude_12; }
	inline void set_maxLongitude_12(FsmFloat_t2134102846 * value)
	{
		___maxLongitude_12 = value;
		Il2CppCodeGenWriteBarrier(&___maxLongitude_12, value);
	}

	inline static int32_t get_offset_of_minLatitude_13() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___minLatitude_13)); }
	inline FsmFloat_t2134102846 * get_minLatitude_13() const { return ___minLatitude_13; }
	inline FsmFloat_t2134102846 ** get_address_of_minLatitude_13() { return &___minLatitude_13; }
	inline void set_minLatitude_13(FsmFloat_t2134102846 * value)
	{
		___minLatitude_13 = value;
		Il2CppCodeGenWriteBarrier(&___minLatitude_13, value);
	}

	inline static int32_t get_offset_of_maxLatitude_14() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___maxLatitude_14)); }
	inline FsmFloat_t2134102846 * get_maxLatitude_14() const { return ___maxLatitude_14; }
	inline FsmFloat_t2134102846 ** get_address_of_maxLatitude_14() { return &___maxLatitude_14; }
	inline void set_maxLatitude_14(FsmFloat_t2134102846 * value)
	{
		___maxLatitude_14 = value;
		Il2CppCodeGenWriteBarrier(&___maxLatitude_14, value);
	}

	inline static int32_t get_offset_of_minX_15() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___minX_15)); }
	inline FsmFloat_t2134102846 * get_minX_15() const { return ___minX_15; }
	inline FsmFloat_t2134102846 ** get_address_of_minX_15() { return &___minX_15; }
	inline void set_minX_15(FsmFloat_t2134102846 * value)
	{
		___minX_15 = value;
		Il2CppCodeGenWriteBarrier(&___minX_15, value);
	}

	inline static int32_t get_offset_of_minY_16() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___minY_16)); }
	inline FsmFloat_t2134102846 * get_minY_16() const { return ___minY_16; }
	inline FsmFloat_t2134102846 ** get_address_of_minY_16() { return &___minY_16; }
	inline void set_minY_16(FsmFloat_t2134102846 * value)
	{
		___minY_16 = value;
		Il2CppCodeGenWriteBarrier(&___minY_16, value);
	}

	inline static int32_t get_offset_of_width_17() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___width_17)); }
	inline FsmFloat_t2134102846 * get_width_17() const { return ___width_17; }
	inline FsmFloat_t2134102846 ** get_address_of_width_17() { return &___width_17; }
	inline void set_width_17(FsmFloat_t2134102846 * value)
	{
		___width_17 = value;
		Il2CppCodeGenWriteBarrier(&___width_17, value);
	}

	inline static int32_t get_offset_of_height_18() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___height_18)); }
	inline FsmFloat_t2134102846 * get_height_18() const { return ___height_18; }
	inline FsmFloat_t2134102846 ** get_address_of_height_18() { return &___height_18; }
	inline void set_height_18(FsmFloat_t2134102846 * value)
	{
		___height_18 = value;
		Il2CppCodeGenWriteBarrier(&___height_18, value);
	}

	inline static int32_t get_offset_of_projectedX_19() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___projectedX_19)); }
	inline FsmFloat_t2134102846 * get_projectedX_19() const { return ___projectedX_19; }
	inline FsmFloat_t2134102846 ** get_address_of_projectedX_19() { return &___projectedX_19; }
	inline void set_projectedX_19(FsmFloat_t2134102846 * value)
	{
		___projectedX_19 = value;
		Il2CppCodeGenWriteBarrier(&___projectedX_19, value);
	}

	inline static int32_t get_offset_of_projectedY_20() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___projectedY_20)); }
	inline FsmFloat_t2134102846 * get_projectedY_20() const { return ___projectedY_20; }
	inline FsmFloat_t2134102846 ** get_address_of_projectedY_20() { return &___projectedY_20; }
	inline void set_projectedY_20(FsmFloat_t2134102846 * value)
	{
		___projectedY_20 = value;
		Il2CppCodeGenWriteBarrier(&___projectedY_20, value);
	}

	inline static int32_t get_offset_of_normalized_21() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___normalized_21)); }
	inline FsmBool_t1075959796 * get_normalized_21() const { return ___normalized_21; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_21() { return &___normalized_21; }
	inline void set_normalized_21(FsmBool_t1075959796 * value)
	{
		___normalized_21 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_21, value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}

	inline static int32_t get_offset_of_x_23() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___x_23)); }
	inline float get_x_23() const { return ___x_23; }
	inline float* get_address_of_x_23() { return &___x_23; }
	inline void set_x_23(float value)
	{
		___x_23 = value;
	}

	inline static int32_t get_offset_of_y_24() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t969841851, ___y_24)); }
	inline float get_y_24() const { return ___y_24; }
	inline float* get_address_of_y_24() { return &___y_24; }
	inline void set_y_24(float value)
	{
		___y_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
