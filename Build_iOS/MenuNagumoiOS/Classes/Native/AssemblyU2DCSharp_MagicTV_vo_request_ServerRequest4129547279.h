﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequestPa47984066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.request.ServerRequestParamsDeviceVO
struct  ServerRequestParamsDeviceVO_t4129547279  : public ServerRequestParamsUpdateVO_t47984066
{
public:
	// System.String MagicTV.vo.request.ServerRequestParamsDeviceVO::token
	String_t* ___token_6;

public:
	inline static int32_t get_offset_of_token_6() { return static_cast<int32_t>(offsetof(ServerRequestParamsDeviceVO_t4129547279, ___token_6)); }
	inline String_t* get_token_6() const { return ___token_6; }
	inline String_t** get_address_of_token_6() { return &___token_6; }
	inline void set_token_6(String_t* value)
	{
		___token_6 = value;
		Il2CppCodeGenWriteBarrier(&___token_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
