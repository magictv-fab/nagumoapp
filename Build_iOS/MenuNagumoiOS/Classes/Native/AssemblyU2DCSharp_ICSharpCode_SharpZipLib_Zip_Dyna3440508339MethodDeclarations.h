﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.DynamicDiskDataSource
struct DynamicDiskDataSource_t3440508339;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.DynamicDiskDataSource::.ctor()
extern "C"  void DynamicDiskDataSource__ctor_m1075024424 (DynamicDiskDataSource_t3440508339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.DynamicDiskDataSource::GetSource(ICSharpCode.SharpZipLib.Zip.ZipEntry,System.String)
extern "C"  Stream_t1561764144 * DynamicDiskDataSource_GetSource_m2649833752 (DynamicDiskDataSource_t3440508339 * __this, ZipEntry_t3141689087 * ___entry0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
