﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_UPCEANWriter3682575931.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.EAN13Writer
struct  EAN13Writer_t2474944089  : public UPCEANWriter_t3682575931
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
