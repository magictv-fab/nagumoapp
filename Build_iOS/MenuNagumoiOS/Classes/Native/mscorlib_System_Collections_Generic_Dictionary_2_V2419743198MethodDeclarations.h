﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>
struct ValueCollection_t2419743198;
// System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>
struct Dictionary_2_t3719137485;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1650970893.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m484743145_gshared (ValueCollection_t2419743198 * __this, Dictionary_2_t3719137485 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m484743145(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2419743198 *, Dictionary_2_t3719137485 *, const MethodInfo*))ValueCollection__ctor_m484743145_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3902964521_gshared (ValueCollection_t2419743198 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3902964521(__this, ___item0, method) ((  void (*) (ValueCollection_t2419743198 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3902964521_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m874671218_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m874671218(__this, method) ((  void (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m874671218_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1862569057_gshared (ValueCollection_t2419743198 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1862569057(__this, ___item0, method) ((  bool (*) (ValueCollection_t2419743198 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1862569057_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m883378502_gshared (ValueCollection_t2419743198 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m883378502(__this, ___item0, method) ((  bool (*) (ValueCollection_t2419743198 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m883378502_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2752282674_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2752282674(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2752282674_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m119121654_gshared (ValueCollection_t2419743198 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m119121654(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2419743198 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m119121654_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3419404933_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3419404933(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3419404933_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m137744916_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m137744916(__this, method) ((  bool (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m137744916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2650248692_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2650248692(__this, method) ((  bool (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2650248692_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m439125094_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m439125094(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m439125094_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3276138992_gshared (ValueCollection_t2419743198 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3276138992(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2419743198 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3276138992_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1650970893  ValueCollection_GetEnumerator_m2908842841_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2908842841(__this, method) ((  Enumerator_t1650970893  (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_GetEnumerator_m2908842841_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4050680622_gshared (ValueCollection_t2419743198 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4050680622(__this, method) ((  int32_t (*) (ValueCollection_t2419743198 *, const MethodInfo*))ValueCollection_get_Count_m4050680622_gshared)(__this, method)
