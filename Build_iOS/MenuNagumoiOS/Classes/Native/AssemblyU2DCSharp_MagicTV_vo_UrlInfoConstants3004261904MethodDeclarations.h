﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.UrlInfoConstants
struct UrlInfoConstants_t3004261904;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.vo.UrlInfoConstants::.ctor()
extern "C"  void UrlInfoConstants__ctor_m2590020163 (UrlInfoConstants_t3004261904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MagicTV.vo.UrlInfoConstants::getQualityIntByType(System.String)
extern "C"  int32_t UrlInfoConstants_getQualityIntByType_m4189471292 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
