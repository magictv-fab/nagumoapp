﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.AppRootEvents/OnVoidEventHandler
struct OnVoidEventHandler_t622442222;
// MagicTV.globals.events.AppRootEvents
struct AppRootEvents_t2849877622;
// System.Collections.Generic.Dictionary`2<System.String,MagicTV.in_apps.InAppEventsChannel>
struct Dictionary_2_t2218131856;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.AppRootEvents
struct  AppRootEvents_t2849877622  : public MonoBehaviour_t667441552
{
public:
	// MagicTV.globals.events.AppRootEvents/OnVoidEventHandler MagicTV.globals.events.AppRootEvents::onDownloadAllCalled
	OnVoidEventHandler_t622442222 * ___onDownloadAllCalled_2;
	// MagicTV.globals.events.AppRootEvents/OnVoidEventHandler MagicTV.globals.events.AppRootEvents::onRaiseStopPresentation
	OnVoidEventHandler_t622442222 * ___onRaiseStopPresentation_3;
	// System.Collections.Generic.Dictionary`2<System.String,MagicTV.in_apps.InAppEventsChannel> MagicTV.globals.events.AppRootEvents::_channels
	Dictionary_2_t2218131856 * ____channels_5;

public:
	inline static int32_t get_offset_of_onDownloadAllCalled_2() { return static_cast<int32_t>(offsetof(AppRootEvents_t2849877622, ___onDownloadAllCalled_2)); }
	inline OnVoidEventHandler_t622442222 * get_onDownloadAllCalled_2() const { return ___onDownloadAllCalled_2; }
	inline OnVoidEventHandler_t622442222 ** get_address_of_onDownloadAllCalled_2() { return &___onDownloadAllCalled_2; }
	inline void set_onDownloadAllCalled_2(OnVoidEventHandler_t622442222 * value)
	{
		___onDownloadAllCalled_2 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadAllCalled_2, value);
	}

	inline static int32_t get_offset_of_onRaiseStopPresentation_3() { return static_cast<int32_t>(offsetof(AppRootEvents_t2849877622, ___onRaiseStopPresentation_3)); }
	inline OnVoidEventHandler_t622442222 * get_onRaiseStopPresentation_3() const { return ___onRaiseStopPresentation_3; }
	inline OnVoidEventHandler_t622442222 ** get_address_of_onRaiseStopPresentation_3() { return &___onRaiseStopPresentation_3; }
	inline void set_onRaiseStopPresentation_3(OnVoidEventHandler_t622442222 * value)
	{
		___onRaiseStopPresentation_3 = value;
		Il2CppCodeGenWriteBarrier(&___onRaiseStopPresentation_3, value);
	}

	inline static int32_t get_offset_of__channels_5() { return static_cast<int32_t>(offsetof(AppRootEvents_t2849877622, ____channels_5)); }
	inline Dictionary_2_t2218131856 * get__channels_5() const { return ____channels_5; }
	inline Dictionary_2_t2218131856 ** get_address_of__channels_5() { return &____channels_5; }
	inline void set__channels_5(Dictionary_2_t2218131856 * value)
	{
		____channels_5 = value;
		Il2CppCodeGenWriteBarrier(&____channels_5, value);
	}
};

struct AppRootEvents_t2849877622_StaticFields
{
public:
	// MagicTV.globals.events.AppRootEvents MagicTV.globals.events.AppRootEvents::_instance
	AppRootEvents_t2849877622 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(AppRootEvents_t2849877622_StaticFields, ____instance_4)); }
	inline AppRootEvents_t2849877622 * get__instance_4() const { return ____instance_4; }
	inline AppRootEvents_t2849877622 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(AppRootEvents_t2849877622 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
