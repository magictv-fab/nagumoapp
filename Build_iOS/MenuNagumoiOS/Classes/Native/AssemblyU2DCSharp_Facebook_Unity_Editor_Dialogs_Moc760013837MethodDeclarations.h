﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct MockLoginDialog_t760013837;
// System.String
struct String_t;
// Facebook.Unity.IGraphResult
struct IGraphResult_t873401058;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::.ctor()
extern "C"  void MockLoginDialog__ctor_m2299122023 (MockLoginDialog_t760013837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::get_DialogTitle()
extern "C"  String_t* MockLoginDialog_get_DialogTitle_m1999539311 (MockLoginDialog_t760013837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::DoGui()
extern "C"  void MockLoginDialog_DoGui_m651527989 (MockLoginDialog_t760013837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::SendSuccessResult()
extern "C"  void MockLoginDialog_SendSuccessResult_m4150730205 (MockLoginDialog_t760013837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::<SendSuccessResult>m__10(Facebook.Unity.IGraphResult)
extern "C"  void MockLoginDialog_U3CSendSuccessResultU3Em__10_m1912459944 (MockLoginDialog_t760013837 * __this, Il2CppObject * ___graphResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
