﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMovieTexture
struct  PlayMovieTexture_t187608383  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject PlayMovieTexture::playTarget
	GameObject_t3674682005 * ___playTarget_2;
	// System.Boolean PlayMovieTexture::playNow
	bool ___playNow_3;
	// System.Boolean PlayMovieTexture::playAudioNow
	bool ___playAudioNow_4;
	// System.Boolean PlayMovieTexture::isLoop
	bool ___isLoop_5;
	// System.Single PlayMovieTexture::delayAudio
	float ___delayAudio_6;
	// UnityEngine.AudioSource PlayMovieTexture::audioSource
	AudioSource_t1740077639 * ___audioSource_7;

public:
	inline static int32_t get_offset_of_playTarget_2() { return static_cast<int32_t>(offsetof(PlayMovieTexture_t187608383, ___playTarget_2)); }
	inline GameObject_t3674682005 * get_playTarget_2() const { return ___playTarget_2; }
	inline GameObject_t3674682005 ** get_address_of_playTarget_2() { return &___playTarget_2; }
	inline void set_playTarget_2(GameObject_t3674682005 * value)
	{
		___playTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___playTarget_2, value);
	}

	inline static int32_t get_offset_of_playNow_3() { return static_cast<int32_t>(offsetof(PlayMovieTexture_t187608383, ___playNow_3)); }
	inline bool get_playNow_3() const { return ___playNow_3; }
	inline bool* get_address_of_playNow_3() { return &___playNow_3; }
	inline void set_playNow_3(bool value)
	{
		___playNow_3 = value;
	}

	inline static int32_t get_offset_of_playAudioNow_4() { return static_cast<int32_t>(offsetof(PlayMovieTexture_t187608383, ___playAudioNow_4)); }
	inline bool get_playAudioNow_4() const { return ___playAudioNow_4; }
	inline bool* get_address_of_playAudioNow_4() { return &___playAudioNow_4; }
	inline void set_playAudioNow_4(bool value)
	{
		___playAudioNow_4 = value;
	}

	inline static int32_t get_offset_of_isLoop_5() { return static_cast<int32_t>(offsetof(PlayMovieTexture_t187608383, ___isLoop_5)); }
	inline bool get_isLoop_5() const { return ___isLoop_5; }
	inline bool* get_address_of_isLoop_5() { return &___isLoop_5; }
	inline void set_isLoop_5(bool value)
	{
		___isLoop_5 = value;
	}

	inline static int32_t get_offset_of_delayAudio_6() { return static_cast<int32_t>(offsetof(PlayMovieTexture_t187608383, ___delayAudio_6)); }
	inline float get_delayAudio_6() const { return ___delayAudio_6; }
	inline float* get_address_of_delayAudio_6() { return &___delayAudio_6; }
	inline void set_delayAudio_6(float value)
	{
		___delayAudio_6 = value;
	}

	inline static int32_t get_offset_of_audioSource_7() { return static_cast<int32_t>(offsetof(PlayMovieTexture_t187608383, ___audioSource_7)); }
	inline AudioSource_t1740077639 * get_audioSource_7() const { return ___audioSource_7; }
	inline AudioSource_t1740077639 ** get_address_of_audioSource_7() { return &___audioSource_7; }
	inline void set_audioSource_7(AudioSource_t1740077639 * value)
	{
		___audioSource_7 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
