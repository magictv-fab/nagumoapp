﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.StateMachine>
struct DefaultComparer_t3110739833;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.StateMachine>::.ctor()
extern "C"  void DefaultComparer__ctor_m2911889407_gshared (DefaultComparer_t3110739833 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2911889407(__this, method) ((  void (*) (DefaultComparer_t3110739833 *, const MethodInfo*))DefaultComparer__ctor_m2911889407_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.StateMachine>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2406673428_gshared (DefaultComparer_t3110739833 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2406673428(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3110739833 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2406673428_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.StateMachine>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3140585876_gshared (DefaultComparer_t3110739833 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3140585876(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3110739833 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3140585876_gshared)(__this, ___x0, ___y1, method)
