﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey
struct  PlayerPrefsDeleteKey_t3492146765  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey::key
	FsmString_t952858651 * ___key_9;

public:
	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(PlayerPrefsDeleteKey_t3492146765, ___key_9)); }
	inline FsmString_t952858651 * get_key_9() const { return ___key_9; }
	inline FsmString_t952858651 ** get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(FsmString_t952858651 * value)
	{
		___key_9 = value;
		Il2CppCodeGenWriteBarrier(&___key_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
