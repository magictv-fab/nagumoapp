﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayRandomSound
struct PlayRandomSound_t3980109638;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::.ctor()
extern "C"  void PlayRandomSound__ctor_m2057002672 (PlayRandomSound_t3980109638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::Reset()
extern "C"  void PlayRandomSound_Reset_m3998402909 (PlayRandomSound_t3980109638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::OnEnter()
extern "C"  void PlayRandomSound_OnEnter_m486340231 (PlayRandomSound_t3980109638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::DoPlayRandomClip()
extern "C"  void PlayRandomSound_DoPlayRandomClip_m3674964486 (PlayRandomSound_t3980109638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
