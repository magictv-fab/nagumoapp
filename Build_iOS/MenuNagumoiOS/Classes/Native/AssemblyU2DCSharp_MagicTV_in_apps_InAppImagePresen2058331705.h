﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ARM.utils.request.ARMSingleRequest
struct ARMSingleRequest_t410726329;

#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppImagePresentation
struct  InAppImagePresentation_t2058331705  : public InAppAbstract_t382673128
{
public:
	// System.Boolean MagicTV.in_apps.InAppImagePresentation::_doPrepareAlreadyCalled
	bool ____doPrepareAlreadyCalled_15;
	// MagicTV.vo.UrlInfoVO MagicTV.in_apps.InAppImagePresentation::_urlInfoVO
	UrlInfoVO_t1761987528 * ____urlInfoVO_16;
	// System.Boolean MagicTV.in_apps.InAppImagePresentation::resetOnStop
	bool ___resetOnStop_17;
	// System.Boolean MagicTV.in_apps.InAppImagePresentation::_componentIsReady
	bool ____componentIsReady_18;
	// System.Boolean MagicTV.in_apps.InAppImagePresentation::_useCache
	bool ____useCache_19;
	// System.String MagicTV.in_apps.InAppImagePresentation::externalUrl
	String_t* ___externalUrl_20;
	// System.Boolean MagicTV.in_apps.InAppImagePresentation::_debugLoadNow
	bool ____debugLoadNow_21;
	// UnityEngine.GameObject MagicTV.in_apps.InAppImagePresentation::_plane
	GameObject_t3674682005 * ____plane_22;
	// ARM.utils.request.ARMSingleRequest MagicTV.in_apps.InAppImagePresentation::_fileRequest
	ARMSingleRequest_t410726329 * ____fileRequest_23;

public:
	inline static int32_t get_offset_of__doPrepareAlreadyCalled_15() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____doPrepareAlreadyCalled_15)); }
	inline bool get__doPrepareAlreadyCalled_15() const { return ____doPrepareAlreadyCalled_15; }
	inline bool* get_address_of__doPrepareAlreadyCalled_15() { return &____doPrepareAlreadyCalled_15; }
	inline void set__doPrepareAlreadyCalled_15(bool value)
	{
		____doPrepareAlreadyCalled_15 = value;
	}

	inline static int32_t get_offset_of__urlInfoVO_16() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____urlInfoVO_16)); }
	inline UrlInfoVO_t1761987528 * get__urlInfoVO_16() const { return ____urlInfoVO_16; }
	inline UrlInfoVO_t1761987528 ** get_address_of__urlInfoVO_16() { return &____urlInfoVO_16; }
	inline void set__urlInfoVO_16(UrlInfoVO_t1761987528 * value)
	{
		____urlInfoVO_16 = value;
		Il2CppCodeGenWriteBarrier(&____urlInfoVO_16, value);
	}

	inline static int32_t get_offset_of_resetOnStop_17() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ___resetOnStop_17)); }
	inline bool get_resetOnStop_17() const { return ___resetOnStop_17; }
	inline bool* get_address_of_resetOnStop_17() { return &___resetOnStop_17; }
	inline void set_resetOnStop_17(bool value)
	{
		___resetOnStop_17 = value;
	}

	inline static int32_t get_offset_of__componentIsReady_18() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____componentIsReady_18)); }
	inline bool get__componentIsReady_18() const { return ____componentIsReady_18; }
	inline bool* get_address_of__componentIsReady_18() { return &____componentIsReady_18; }
	inline void set__componentIsReady_18(bool value)
	{
		____componentIsReady_18 = value;
	}

	inline static int32_t get_offset_of__useCache_19() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____useCache_19)); }
	inline bool get__useCache_19() const { return ____useCache_19; }
	inline bool* get_address_of__useCache_19() { return &____useCache_19; }
	inline void set__useCache_19(bool value)
	{
		____useCache_19 = value;
	}

	inline static int32_t get_offset_of_externalUrl_20() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ___externalUrl_20)); }
	inline String_t* get_externalUrl_20() const { return ___externalUrl_20; }
	inline String_t** get_address_of_externalUrl_20() { return &___externalUrl_20; }
	inline void set_externalUrl_20(String_t* value)
	{
		___externalUrl_20 = value;
		Il2CppCodeGenWriteBarrier(&___externalUrl_20, value);
	}

	inline static int32_t get_offset_of__debugLoadNow_21() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____debugLoadNow_21)); }
	inline bool get__debugLoadNow_21() const { return ____debugLoadNow_21; }
	inline bool* get_address_of__debugLoadNow_21() { return &____debugLoadNow_21; }
	inline void set__debugLoadNow_21(bool value)
	{
		____debugLoadNow_21 = value;
	}

	inline static int32_t get_offset_of__plane_22() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____plane_22)); }
	inline GameObject_t3674682005 * get__plane_22() const { return ____plane_22; }
	inline GameObject_t3674682005 ** get_address_of__plane_22() { return &____plane_22; }
	inline void set__plane_22(GameObject_t3674682005 * value)
	{
		____plane_22 = value;
		Il2CppCodeGenWriteBarrier(&____plane_22, value);
	}

	inline static int32_t get_offset_of__fileRequest_23() { return static_cast<int32_t>(offsetof(InAppImagePresentation_t2058331705, ____fileRequest_23)); }
	inline ARMSingleRequest_t410726329 * get__fileRequest_23() const { return ____fileRequest_23; }
	inline ARMSingleRequest_t410726329 ** get_address_of__fileRequest_23() { return &____fileRequest_23; }
	inline void set__fileRequest_23(ARMSingleRequest_t410726329 * value)
	{
		____fileRequest_23 = value;
		Il2CppCodeGenWriteBarrier(&____fileRequest_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
