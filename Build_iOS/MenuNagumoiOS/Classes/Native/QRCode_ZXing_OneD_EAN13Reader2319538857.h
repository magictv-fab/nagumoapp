﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "QRCode_ZXing_OneD_UPCEANReader3527170699.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.EAN13Reader
struct  EAN13Reader_t2319538857  : public UPCEANReader_t3527170699
{
public:
	// System.Int32[] ZXing.OneD.EAN13Reader::decodeMiddleCounters
	Int32U5BU5D_t3230847821* ___decodeMiddleCounters_12;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_12() { return static_cast<int32_t>(offsetof(EAN13Reader_t2319538857, ___decodeMiddleCounters_12)); }
	inline Int32U5BU5D_t3230847821* get_decodeMiddleCounters_12() const { return ___decodeMiddleCounters_12; }
	inline Int32U5BU5D_t3230847821** get_address_of_decodeMiddleCounters_12() { return &___decodeMiddleCounters_12; }
	inline void set_decodeMiddleCounters_12(Int32U5BU5D_t3230847821* value)
	{
		___decodeMiddleCounters_12 = value;
		Il2CppCodeGenWriteBarrier(&___decodeMiddleCounters_12, value);
	}
};

struct EAN13Reader_t2319538857_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.EAN13Reader::FIRST_DIGIT_ENCODINGS
	Int32U5BU5D_t3230847821* ___FIRST_DIGIT_ENCODINGS_11;

public:
	inline static int32_t get_offset_of_FIRST_DIGIT_ENCODINGS_11() { return static_cast<int32_t>(offsetof(EAN13Reader_t2319538857_StaticFields, ___FIRST_DIGIT_ENCODINGS_11)); }
	inline Int32U5BU5D_t3230847821* get_FIRST_DIGIT_ENCODINGS_11() const { return ___FIRST_DIGIT_ENCODINGS_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_FIRST_DIGIT_ENCODINGS_11() { return &___FIRST_DIGIT_ENCODINGS_11; }
	inline void set_FIRST_DIGIT_ENCODINGS_11(Int32U5BU5D_t3230847821* value)
	{
		___FIRST_DIGIT_ENCODINGS_11 = value;
		Il2CppCodeGenWriteBarrier(&___FIRST_DIGIT_ENCODINGS_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
