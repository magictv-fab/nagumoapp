﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.EAN8Writer
struct EAN8Writer_t1797613783;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// ZXing.Common.BitMatrix ZXing.OneD.EAN8Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * EAN8Writer_encode_m3435359674 (EAN8Writer_t1797613783 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] ZXing.OneD.EAN8Writer::encode(System.String)
extern "C"  BooleanU5BU5D_t3456302923* EAN8Writer_encode_m3479522118 (EAN8Writer_t1797613783 * __this, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.EAN8Writer::.ctor()
extern "C"  void EAN8Writer__ctor_m1419320620 (EAN8Writer_t1797613783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
