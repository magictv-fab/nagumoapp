﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.InvertedLuminanceSource
struct InvertedLuminanceSource_t1075320672;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"

// System.Void ZXing.InvertedLuminanceSource::.ctor(ZXing.LuminanceSource)
extern "C"  void InvertedLuminanceSource__ctor_m3940870450 (InvertedLuminanceSource_t1075320672 * __this, LuminanceSource_t1231523093 * ___delegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.InvertedLuminanceSource::getRow(System.Int32,System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* InvertedLuminanceSource_getRow_m3597911195 (InvertedLuminanceSource_t1075320672 * __this, int32_t ___y0, ByteU5BU5D_t4260760469* ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.InvertedLuminanceSource::get_Matrix()
extern "C"  ByteU5BU5D_t4260760469* InvertedLuminanceSource_get_Matrix_m4228743821 (InvertedLuminanceSource_t1075320672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.InvertedLuminanceSource::get_RotateSupported()
extern "C"  bool InvertedLuminanceSource_get_RotateSupported_m2689561651 (InvertedLuminanceSource_t1075320672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.InvertedLuminanceSource::invert()
extern "C"  LuminanceSource_t1231523093 * InvertedLuminanceSource_invert_m2552524921 (InvertedLuminanceSource_t1075320672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.InvertedLuminanceSource::rotateCounterClockwise()
extern "C"  LuminanceSource_t1231523093 * InvertedLuminanceSource_rotateCounterClockwise_m2979704948 (InvertedLuminanceSource_t1075320672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
