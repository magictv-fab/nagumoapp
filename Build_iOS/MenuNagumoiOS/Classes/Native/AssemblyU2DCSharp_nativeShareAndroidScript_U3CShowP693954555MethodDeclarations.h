﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// nativeShareAndroidScript/<ShowPanelOk>c__Iterator26
struct U3CShowPanelOkU3Ec__Iterator26_t693954555;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void nativeShareAndroidScript/<ShowPanelOk>c__Iterator26::.ctor()
extern "C"  void U3CShowPanelOkU3Ec__Iterator26__ctor_m3560003024 (U3CShowPanelOkU3Ec__Iterator26_t693954555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object nativeShareAndroidScript/<ShowPanelOk>c__Iterator26::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowPanelOkU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1067695106 (U3CShowPanelOkU3Ec__Iterator26_t693954555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object nativeShareAndroidScript/<ShowPanelOk>c__Iterator26::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowPanelOkU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m2608989078 (U3CShowPanelOkU3Ec__Iterator26_t693954555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean nativeShareAndroidScript/<ShowPanelOk>c__Iterator26::MoveNext()
extern "C"  bool U3CShowPanelOkU3Ec__Iterator26_MoveNext_m4129347620 (U3CShowPanelOkU3Ec__Iterator26_t693954555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void nativeShareAndroidScript/<ShowPanelOk>c__Iterator26::Dispose()
extern "C"  void U3CShowPanelOkU3Ec__Iterator26_Dispose_m2267221517 (U3CShowPanelOkU3Ec__Iterator26_t693954555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void nativeShareAndroidScript/<ShowPanelOk>c__Iterator26::Reset()
extern "C"  void U3CShowPanelOkU3Ec__Iterator26_Reset_m1206435965 (U3CShowPanelOkU3Ec__Iterator26_t693954555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
