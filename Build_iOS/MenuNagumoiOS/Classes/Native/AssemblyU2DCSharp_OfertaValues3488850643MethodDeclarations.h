﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertaValues
struct OfertaValues_t3488850643;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertaValues::.ctor()
extern "C"  void OfertaValues__ctor_m3411201320 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::Start()
extern "C"  void OfertaValues_Start_m2358339112 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::Update()
extern "C"  void OfertaValues_Update_m99920613 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::GoLink()
extern "C"  void OfertaValues_GoLink_m4197958878 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::SaveImage()
extern "C"  void OfertaValues_SaveImage_m3551815204 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::RemoverOferta()
extern "C"  void OfertaValues_RemoverOferta_m3616371141 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::Favoritar()
extern "C"  void OfertaValues_Favoritar_m3321389856 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::BtnAtivarDesativar()
extern "C"  void OfertaValues_BtnAtivarDesativar_m492020210 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::DesFavoritar(System.String)
extern "C"  void OfertaValues_DesFavoritar_m2548094334 (OfertaValues_t3488850643 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertaValues::ExcluirFavorito(System.String)
extern "C"  Il2CppObject * OfertaValues_ExcluirFavorito_m2593531836 (OfertaValues_t3488850643 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertaValues::IFavoritar(System.String)
extern "C"  Il2CppObject * OfertaValues_IFavoritar_m1413245645 (OfertaValues_t3488850643 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues::Compartilhar()
extern "C"  void OfertaValues_Compartilhar_m1466653118 (OfertaValues_t3488850643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
