﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.ToggleOnOffAbstract/TurnChange
struct TurnChange_t3311189389;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.abstracts.ToggleOnOffAbstract/TurnChange::.ctor(System.Object,System.IntPtr)
extern "C"  void TurnChange__ctor_m4254956468 (TurnChange_t3311189389 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract/TurnChange::Invoke()
extern "C"  void TurnChange_Invoke_m1638883598 (TurnChange_t3311189389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.abstracts.ToggleOnOffAbstract/TurnChange::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TurnChange_BeginInvoke_m757585461 (TurnChange_t3311189389 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract/TurnChange::EndInvoke(System.IAsyncResult)
extern "C"  void TurnChange_EndInvoke_m2127226820 (TurnChange_t3311189389 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
