﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenShotME
struct  ScreenShotME_t2769423806  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.RectTransform ScreenShotME::rectT
	RectTransform_t972643934 * ___rectT_2;
	// UnityEngine.UI.Image ScreenShotME::img
	Image_t538875265 * ___img_3;
	// System.Int32 ScreenShotME::width
	int32_t ___width_4;
	// System.Int32 ScreenShotME::height
	int32_t ___height_5;

public:
	inline static int32_t get_offset_of_rectT_2() { return static_cast<int32_t>(offsetof(ScreenShotME_t2769423806, ___rectT_2)); }
	inline RectTransform_t972643934 * get_rectT_2() const { return ___rectT_2; }
	inline RectTransform_t972643934 ** get_address_of_rectT_2() { return &___rectT_2; }
	inline void set_rectT_2(RectTransform_t972643934 * value)
	{
		___rectT_2 = value;
		Il2CppCodeGenWriteBarrier(&___rectT_2, value);
	}

	inline static int32_t get_offset_of_img_3() { return static_cast<int32_t>(offsetof(ScreenShotME_t2769423806, ___img_3)); }
	inline Image_t538875265 * get_img_3() const { return ___img_3; }
	inline Image_t538875265 ** get_address_of_img_3() { return &___img_3; }
	inline void set_img_3(Image_t538875265 * value)
	{
		___img_3 = value;
		Il2CppCodeGenWriteBarrier(&___img_3, value);
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(ScreenShotME_t2769423806, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(ScreenShotME_t2769423806, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
