﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertaCRMData
struct OfertaCRMData_t637956887;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertaCRMData::.ctor()
extern "C"  void OfertaCRMData__ctor_m400455732 (OfertaCRMData_t637956887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
