﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Security_Cryptography_Aes2466798581.h"
#include "System_Core_System_Security_Cryptography_AesManaged23175804.h"
#include "System_Core_System_Security_Cryptography_AesTransf1787635017.h"
#include "System_Core_System_Action3771233898.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615769.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615732.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676616792.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A435478332.h"
#include "UnityEngine_U3CModuleU3E86524790.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1416890373.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2154290273.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_PrimitiveType1035833655.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "UnityEngine_UnityEngine_DeviceType3959528308.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910.h"
#include "UnityEngine_UnityEngine_WaitUntil3918096351.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_Caching189518581.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"
#include "UnityEngine_UnityEngine_Cursor2745727898.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Flare4197217604.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumen4047764288.h"
#include "UnityEngine_UnityEngine_Graphics3672240399.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_GL2267613321.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent2820176033.h"
#include "UnityEngine_UnityEngine_CullingGroup1868862003.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2578300556.h"
#include "UnityEngine_UnityEngine_Gradient3661184436.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Interna705488572.h"
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode3302654991.h"
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode4213044537.h"
#include "UnityEngine_UnityEngine_Handheld3573483176.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType2604324130.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard1858258760.h"
#include "UnityEngine_UnityEngine_Gizmos2849394813.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2733244747.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (Aes_t2466798581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (AesManaged_t23175804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (AesTransform_t1787635017), -1, sizeof(AesTransform_t1787635017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2013[14] = 
{
	AesTransform_t1787635017::get_offset_of_expandedKey_12(),
	AesTransform_t1787635017::get_offset_of_Nk_13(),
	AesTransform_t1787635017::get_offset_of_Nr_14(),
	AesTransform_t1787635017_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t1787635017_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T0_18(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T1_19(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T2_20(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T3_21(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Action_t3771233898), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[12] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (U24ArrayTypeU24136_t1676615770)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1676615770_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U24ArrayTypeU24120_t1676615733)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1676615733_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U24ArrayTypeU24256_t1676616795)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616795_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U24ArrayTypeU241024_t435478333)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t435478333_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (U3CModuleU3E_t86524798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (AssetBundleCreateRequest_t1416890373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (AssetBundleRequest_t2154290273), sizeof(AssetBundleRequest_t2154290273_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (AssetBundle_t2070959688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (SendMessageOptions_t3856946179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2030[3] = 
{
	SendMessageOptions_t3856946179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (PrimitiveType_t1035833655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[7] = 
{
	PrimitiveType_t1035833655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (Space_t4209342076)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2032[3] = 
{
	Space_t4209342076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (RuntimePlatform_t3050318497)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[32] = 
{
	RuntimePlatform_t3050318497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (LogType_t4286006228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[6] = 
{
	LogType_t4286006228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (DeviceType_t3959528308)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2035[5] = 
{
	DeviceType_t3959528308::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (SystemInfo_t3820892225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (WaitForSeconds_t1615819279), sizeof(WaitForSeconds_t1615819279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2037[1] = 
{
	WaitForSeconds_t1615819279::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (WaitForFixedUpdate_t2130080621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (WaitForEndOfFrame_t2372756133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (CustomYieldInstruction_t2666549910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (WaitUntil_t3918096351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	WaitUntil_t3918096351::get_offset_of_m_Predicate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (Coroutine_t3621161934), sizeof(Coroutine_t3621161934_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	Coroutine_t3621161934::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (ScriptableObject_t2970544072), sizeof(ScriptableObject_t2970544072_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (Caching_t189518581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (UnhandledExceptionHandler_t1700300692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (CursorLockMode_t1155278888)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2046[4] = 
{
	CursorLockMode_t1155278888::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (Cursor_t2745727898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (GameCenterPlatform_t3570684786), -1, sizeof(GameCenterPlatform_t3570684786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2048[15] = 
{
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AuthenticateCallback_0(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_FriendsCallback_1(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AchievementDescriptionLoaderCallback_2(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AchievementLoaderCallback_3(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ProgressCallback_4(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ScoreCallback_5(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ScoreLoaderCallback_6(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_LeaderboardCallback_7(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_UsersCallback_8(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_adCache_9(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_friends_10(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_users_11(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ResetAchievements_12(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_m_LocalUser_13(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_m_GcBoards_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (GcLeaderboard_t1820874799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[2] = 
{
	GcLeaderboard_t1820874799::get_offset_of_m_InternalLeaderboard_0(),
	GcLeaderboard_t1820874799::get_offset_of_m_GenericLeaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (RenderSettings_t425127197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (QualitySettings_t719345784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (MeshFilter_t3839065225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (Mesh_t4241756145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Flare_t4197217604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (Renderer_t3076687687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (InternalDrawTextureArguments_t4047764288)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[9] = 
{
	InternalDrawTextureArguments_t4047764288::get_offset_of_screenRect_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_texture_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_sourceRect_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_leftBorder_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_rightBorder_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_topBorder_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_bottomBorder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_color_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_mat_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (Graphics_t3672240399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (Screen_t3187157168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (GL_t2267613321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (MeshRenderer_t2804666580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (RectOffset_t3056157787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[2] = 
{
	RectOffset_t3056157787::get_offset_of_m_Ptr_0(),
	RectOffset_t3056157787::get_offset_of_m_SourceStyle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (GUIElement_t3775428101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (GUITexture_t4020448292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (GUILayer_t2983897946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (Texture_t2526458961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (Texture2D_t3884108195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (RenderTexture_t1963041563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (CullingGroupEvent_t2820176033)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t2820176033_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2068[3] = 
{
	CullingGroupEvent_t2820176033::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t2820176033::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t2820176033::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (CullingGroup_t1868862003), sizeof(CullingGroup_t1868862003_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[2] = 
{
	CullingGroup_t1868862003::get_offset_of_m_Ptr_0(),
	CullingGroup_t1868862003::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (StateChanged_t2578300556), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Gradient_t3661184436), sizeof(Gradient_t3661184436_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2071[1] = 
{
	Gradient_t3661184436::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572)+ sizeof (Il2CppObject), sizeof(TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2072[5] = 
{
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_keyboardType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_autocorrection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_multiline_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_secure_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_alert_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (FullScreenMovieControlMode_t3302654991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2073[5] = 
{
	FullScreenMovieControlMode_t3302654991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (FullScreenMovieScalingMode_t4213044537)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2074[5] = 
{
	FullScreenMovieScalingMode_t4213044537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (Handheld_t3573483176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (TouchScreenKeyboardType_t2604324130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2076[10] = 
{
	TouchScreenKeyboardType_t2604324130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (TouchScreenKeyboard_t1858258760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[1] = 
{
	TouchScreenKeyboard_t1858258760::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (Gizmos_t2849394813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (LayerMask_t3236759763)+ sizeof (Il2CppObject), sizeof(LayerMask_t3236759763_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	LayerMask_t3236759763::get_offset_of_m_Mask_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (Vector2_t4282066565)+ sizeof (Il2CppObject), sizeof(Vector2_t4282066565_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[3] = 
{
	0,
	Vector2_t4282066565::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t4282066565::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (Vector3_t4282066566)+ sizeof (Il2CppObject), sizeof(Vector3_t4282066566_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2081[4] = 
{
	0,
	Vector3_t4282066566::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t4282066566::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t4282066566::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (Color32_t598853688)+ sizeof (Il2CppObject), sizeof(Color32_t598853688_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2082[4] = 
{
	Color32_t598853688::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (Quaternion_t1553702882)+ sizeof (Il2CppObject), sizeof(Quaternion_t1553702882_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[5] = 
{
	0,
	Quaternion_t1553702882::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (Rect_t4241904616)+ sizeof (Il2CppObject), sizeof(Rect_t4241904616_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2084[4] = 
{
	Rect_t4241904616::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (Matrix4x4_t1651859333)+ sizeof (Il2CppObject), sizeof(Matrix4x4_t1651859333_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[16] = 
{
	Matrix4x4_t1651859333::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m30_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m01_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m21_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m31_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m02_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m12_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m32_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m03_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m23_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (Bounds_t2711641849)+ sizeof (Il2CppObject), sizeof(Bounds_t2711641849_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2086[2] = 
{
	Bounds_t2711641849::get_offset_of_m_Center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Bounds_t2711641849::get_offset_of_m_Extents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (Vector4_t4282066567)+ sizeof (Il2CppObject), sizeof(Vector4_t4282066567_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2087[5] = 
{
	0,
	Vector4_t4282066567::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (Ray_t3134616544)+ sizeof (Il2CppObject), sizeof(Ray_t3134616544_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2088[2] = 
{
	Ray_t3134616544::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t3134616544::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (Plane_t4206452690)+ sizeof (Il2CppObject), sizeof(Plane_t4206452690_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2089[2] = 
{
	Plane_t4206452690::get_offset_of_m_Normal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Plane_t4206452690::get_offset_of_m_Distance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (MathfInternal_t4096243933)+ sizeof (Il2CppObject), sizeof(MathfInternal_t4096243933_marshaled_pinvoke), sizeof(MathfInternal_t4096243933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2090[3] = 
{
	MathfInternal_t4096243933_StaticFields::get_offset_of_FloatMinNormal_0(),
	MathfInternal_t4096243933_StaticFields::get_offset_of_FloatMinDenormal_1(),
	MathfInternal_t4096243933_StaticFields::get_offset_of_IsFlushToZeroEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (Mathf_t4203372500)+ sizeof (Il2CppObject), sizeof(Mathf_t4203372500_marshaled_pinvoke), sizeof(Mathf_t4203372500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2091[1] = 
{
	Mathf_t4203372500_StaticFields::get_offset_of_Epsilon_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (Keyframe_t4079056114)+ sizeof (Il2CppObject), sizeof(Keyframe_t4079056114_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[4] = 
{
	Keyframe_t4079056114::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_InTangent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_OutTangent_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (AnimationCurve_t3667593487), sizeof(AnimationCurve_t3667593487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2093[1] = 
{
	AnimationCurve_t3667593487::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (NetworkConnectionError_t1049203712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[16] = 
{
	NetworkConnectionError_t1049203712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (NetworkDisconnection_t468395618)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[3] = 
{
	NetworkDisconnection_t468395618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (MasterServerEvent_t2733244747)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2096[6] = 
{
	MasterServerEvent_t2733244747::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (NetworkPeerType_t796297792)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	NetworkPeerType_t796297792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (NetworkLogLevel_t2722760996)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[4] = 
{
	NetworkLogLevel_t2722760996::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (NetworkPlayer_t3231273765)+ sizeof (Il2CppObject), sizeof(NetworkPlayer_t3231273765_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[1] = 
{
	NetworkPlayer_t3231273765::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
