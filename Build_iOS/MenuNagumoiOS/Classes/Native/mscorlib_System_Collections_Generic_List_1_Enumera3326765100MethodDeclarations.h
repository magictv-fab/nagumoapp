﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3554963703(__this, ___l0, method) ((  void (*) (Enumerator_t3326765100 *, List_1_t3307092330 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1330095355(__this, method) ((  void (*) (Enumerator_t3326765100 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3665351793(__this, method) ((  Il2CppObject * (*) (Enumerator_t3326765100 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::Dispose()
#define Enumerator_Dispose_m2078764188(__this, method) ((  void (*) (Enumerator_t3326765100 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::VerifyState()
#define Enumerator_VerifyState_m2484829653(__this, method) ((  void (*) (Enumerator_t3326765100 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::MoveNext()
#define Enumerator_MoveNext_m2830620843(__this, method) ((  bool (*) (Enumerator_t3326765100 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.DelayedEvent>::get_Current()
#define Enumerator_get_Current_m3234720558(__this, method) ((  DelayedEvent_t1938906778 * (*) (Enumerator_t3326765100 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
