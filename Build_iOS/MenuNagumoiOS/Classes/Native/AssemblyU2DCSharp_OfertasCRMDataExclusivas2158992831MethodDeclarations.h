﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasCRMDataExclusivas
struct OfertasCRMDataExclusivas_t2158992831;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasCRMDataExclusivas::.ctor()
extern "C"  void OfertasCRMDataExclusivas__ctor_m2728261564 (OfertasCRMDataExclusivas_t2158992831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
