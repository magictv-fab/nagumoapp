﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Datamatrix.Internal.Version
struct Version_t2313761970;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.BitMatrixParser
struct  BitMatrixParser_t1222713543  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::mappingBitMatrix
	BitMatrix_t1058711404 * ___mappingBitMatrix_0;
	// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::readMappingMatrix
	BitMatrix_t1058711404 * ___readMappingMatrix_1;
	// ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::version
	Version_t2313761970 * ___version_2;

public:
	inline static int32_t get_offset_of_mappingBitMatrix_0() { return static_cast<int32_t>(offsetof(BitMatrixParser_t1222713543, ___mappingBitMatrix_0)); }
	inline BitMatrix_t1058711404 * get_mappingBitMatrix_0() const { return ___mappingBitMatrix_0; }
	inline BitMatrix_t1058711404 ** get_address_of_mappingBitMatrix_0() { return &___mappingBitMatrix_0; }
	inline void set_mappingBitMatrix_0(BitMatrix_t1058711404 * value)
	{
		___mappingBitMatrix_0 = value;
		Il2CppCodeGenWriteBarrier(&___mappingBitMatrix_0, value);
	}

	inline static int32_t get_offset_of_readMappingMatrix_1() { return static_cast<int32_t>(offsetof(BitMatrixParser_t1222713543, ___readMappingMatrix_1)); }
	inline BitMatrix_t1058711404 * get_readMappingMatrix_1() const { return ___readMappingMatrix_1; }
	inline BitMatrix_t1058711404 ** get_address_of_readMappingMatrix_1() { return &___readMappingMatrix_1; }
	inline void set_readMappingMatrix_1(BitMatrix_t1058711404 * value)
	{
		___readMappingMatrix_1 = value;
		Il2CppCodeGenWriteBarrier(&___readMappingMatrix_1, value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(BitMatrixParser_t1222713543, ___version_2)); }
	inline Version_t2313761970 * get_version_2() const { return ___version_2; }
	inline Version_t2313761970 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(Version_t2313761970 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier(&___version_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
