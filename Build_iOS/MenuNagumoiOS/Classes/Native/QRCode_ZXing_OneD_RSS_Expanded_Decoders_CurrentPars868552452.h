﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_CurrentPar2006037904.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState
struct  CurrentParsingState_t868552452  : public Il2CppObject
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::position
	int32_t ___position_0;
	// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState/State ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::encoding
	int32_t ___encoding_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(CurrentParsingState_t868552452, ___position_0)); }
	inline int32_t get_position_0() const { return ___position_0; }
	inline int32_t* get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(int32_t value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(CurrentParsingState_t868552452, ___encoding_1)); }
	inline int32_t get_encoding_1() const { return ___encoding_1; }
	inline int32_t* get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(int32_t value)
	{
		___encoding_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
