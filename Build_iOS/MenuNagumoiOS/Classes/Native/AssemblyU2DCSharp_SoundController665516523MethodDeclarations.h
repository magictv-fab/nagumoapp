﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundController
struct SoundController_t665516523;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundController::.ctor()
extern "C"  void SoundController__ctor_m1479525856 (SoundController_t665516523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundController SoundController::GetInstance()
extern "C"  SoundController_t665516523 * SoundController_GetInstance_m2972233745 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource SoundController::GetInstanceAudioSource()
extern "C"  AudioSource_t1740077639 * SoundController_GetInstanceAudioSource_m3909278577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
