﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.gui.screens.InAppGUIScreen
struct InAppGUIScreen_t1999655547;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.gui.screens.InAppGUIScreen::.ctor()
extern "C"  void InAppGUIScreen__ctor_m2115479799 (InAppGUIScreen_t1999655547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.InAppGUIScreen::prepare()
extern "C"  void InAppGUIScreen_prepare_m2383536252 (InAppGUIScreen_t1999655547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.InAppGUIScreen::UpdateMagicTVContent()
extern "C"  void InAppGUIScreen_UpdateMagicTVContent_m4125832800 (InAppGUIScreen_t1999655547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.InAppGUIScreen::UpdateComplete()
extern "C"  void InAppGUIScreen_UpdateComplete_m3588630287 (InAppGUIScreen_t1999655547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.InAppGUIScreen::hide()
extern "C"  void InAppGUIScreen_hide_m1179595279 (InAppGUIScreen_t1999655547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.InAppGUIScreen::show()
extern "C"  void InAppGUIScreen_show_m1493937418 (InAppGUIScreen_t1999655547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
