﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMDebug
struct  ARMDebug_t3644730743  : public Il2CppObject
{
public:

public:
};

struct ARMDebug_t3644730743_StaticFields
{
public:
	// System.Int64 ARMDebug::startTime
	int64_t ___startTime_0;
	// System.Int64 ARMDebug::lastTotal
	int64_t ___lastTotal_1;

public:
	inline static int32_t get_offset_of_startTime_0() { return static_cast<int32_t>(offsetof(ARMDebug_t3644730743_StaticFields, ___startTime_0)); }
	inline int64_t get_startTime_0() const { return ___startTime_0; }
	inline int64_t* get_address_of_startTime_0() { return &___startTime_0; }
	inline void set_startTime_0(int64_t value)
	{
		___startTime_0 = value;
	}

	inline static int32_t get_offset_of_lastTotal_1() { return static_cast<int32_t>(offsetof(ARMDebug_t3644730743_StaticFields, ___lastTotal_1)); }
	inline int64_t get_lastTotal_1() const { return ___lastTotal_1; }
	inline int64_t* get_address_of_lastTotal_1() { return &___lastTotal_1; }
	inline void set_lastTotal_1(int64_t value)
	{
		___lastTotal_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
