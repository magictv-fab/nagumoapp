﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>
struct KeyCollection_t1619369899;
// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>
struct Dictionary_2_t4287577744;
// System.Collections.Generic.IEnumerator`1<InApp.InAppEventDispatcher/EventNames>
struct IEnumerator_1_t3526500895;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// InApp.InAppEventDispatcher/EventNames[]
struct EventNamesU5BU5D_t3350345955;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke607546502.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4128360304_gshared (KeyCollection_t1619369899 * __this, Dictionary_2_t4287577744 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4128360304(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1619369899 *, Dictionary_2_t4287577744 *, const MethodInfo*))KeyCollection__ctor_m4128360304_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3905989606_gshared (KeyCollection_t1619369899 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3905989606(__this, ___item0, method) ((  void (*) (KeyCollection_t1619369899 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3905989606_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m888967901_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m888967901(__this, method) ((  void (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m888967901_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1383295204_gshared (KeyCollection_t1619369899 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1383295204(__this, ___item0, method) ((  bool (*) (KeyCollection_t1619369899 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1383295204_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m596427593_gshared (KeyCollection_t1619369899 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m596427593(__this, ___item0, method) ((  bool (*) (KeyCollection_t1619369899 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m596427593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4173551769_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4173551769(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4173551769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4231009231_gshared (KeyCollection_t1619369899 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m4231009231(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1619369899 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4231009231_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4128769802_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4128769802(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4128769802_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m787887941_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m787887941(__this, method) ((  bool (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m787887941_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m412262967_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m412262967(__this, method) ((  bool (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m412262967_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3401403171_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3401403171(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3401403171_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1970859557_gshared (KeyCollection_t1619369899 * __this, EventNamesU5BU5D_t3350345955* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1970859557(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1619369899 *, EventNamesU5BU5D_t3350345955*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1970859557_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::GetEnumerator()
extern "C"  Enumerator_t607546502  KeyCollection_GetEnumerator_m3315239560_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3315239560(__this, method) ((  Enumerator_t607546502  (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_GetEnumerator_m3315239560_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3602700541_gshared (KeyCollection_t1619369899 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3602700541(__this, method) ((  int32_t (*) (KeyCollection_t1619369899 *, const MethodInfo*))KeyCollection_get_Count_m3602700541_gshared)(__this, method)
