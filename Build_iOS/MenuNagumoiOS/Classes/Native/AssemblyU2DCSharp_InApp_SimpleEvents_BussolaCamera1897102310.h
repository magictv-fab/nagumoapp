﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InApp.SimpleEvents.BussolaCameraBehavior
struct BussolaCameraBehavior_t1897102310;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract
struct TransformPositionAngleFilterAbstract_t4158746314;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.BussolaCameraBehavior
struct  BussolaCameraBehavior_t1897102310  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// UnityEngine.GameObject InApp.SimpleEvents.BussolaCameraBehavior::objectToMove
	GameObject_t3674682005 * ___objectToMove_3;
	// System.Boolean InApp.SimpleEvents.BussolaCameraBehavior::DebugIsOn
	bool ___DebugIsOn_4;
	// System.Boolean InApp.SimpleEvents.BussolaCameraBehavior::screenButtonToDebug
	bool ___screenButtonToDebug_5;
	// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract InApp.SimpleEvents.BussolaCameraBehavior::filterToOn
	TransformPositionAngleFilterAbstract_t4158746314 * ___filterToOn_6;

public:
	inline static int32_t get_offset_of_objectToMove_3() { return static_cast<int32_t>(offsetof(BussolaCameraBehavior_t1897102310, ___objectToMove_3)); }
	inline GameObject_t3674682005 * get_objectToMove_3() const { return ___objectToMove_3; }
	inline GameObject_t3674682005 ** get_address_of_objectToMove_3() { return &___objectToMove_3; }
	inline void set_objectToMove_3(GameObject_t3674682005 * value)
	{
		___objectToMove_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectToMove_3, value);
	}

	inline static int32_t get_offset_of_DebugIsOn_4() { return static_cast<int32_t>(offsetof(BussolaCameraBehavior_t1897102310, ___DebugIsOn_4)); }
	inline bool get_DebugIsOn_4() const { return ___DebugIsOn_4; }
	inline bool* get_address_of_DebugIsOn_4() { return &___DebugIsOn_4; }
	inline void set_DebugIsOn_4(bool value)
	{
		___DebugIsOn_4 = value;
	}

	inline static int32_t get_offset_of_screenButtonToDebug_5() { return static_cast<int32_t>(offsetof(BussolaCameraBehavior_t1897102310, ___screenButtonToDebug_5)); }
	inline bool get_screenButtonToDebug_5() const { return ___screenButtonToDebug_5; }
	inline bool* get_address_of_screenButtonToDebug_5() { return &___screenButtonToDebug_5; }
	inline void set_screenButtonToDebug_5(bool value)
	{
		___screenButtonToDebug_5 = value;
	}

	inline static int32_t get_offset_of_filterToOn_6() { return static_cast<int32_t>(offsetof(BussolaCameraBehavior_t1897102310, ___filterToOn_6)); }
	inline TransformPositionAngleFilterAbstract_t4158746314 * get_filterToOn_6() const { return ___filterToOn_6; }
	inline TransformPositionAngleFilterAbstract_t4158746314 ** get_address_of_filterToOn_6() { return &___filterToOn_6; }
	inline void set_filterToOn_6(TransformPositionAngleFilterAbstract_t4158746314 * value)
	{
		___filterToOn_6 = value;
		Il2CppCodeGenWriteBarrier(&___filterToOn_6, value);
	}
};

struct BussolaCameraBehavior_t1897102310_StaticFields
{
public:
	// InApp.SimpleEvents.BussolaCameraBehavior InApp.SimpleEvents.BussolaCameraBehavior::_instance
	BussolaCameraBehavior_t1897102310 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(BussolaCameraBehavior_t1897102310_StaticFields, ____instance_2)); }
	inline BussolaCameraBehavior_t1897102310 * get__instance_2() const { return ____instance_2; }
	inline BussolaCameraBehavior_t1897102310 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(BussolaCameraBehavior_t1897102310 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
