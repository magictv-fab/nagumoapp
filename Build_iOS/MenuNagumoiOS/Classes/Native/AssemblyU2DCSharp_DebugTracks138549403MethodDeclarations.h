﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugTracks
struct DebugTracks_t138549403;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// System.Void DebugTracks::.ctor()
extern "C"  void DebugTracks__ctor_m1077666096 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::OnGUI()
extern "C"  void DebugTracks_OnGUI_m573064746 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::drawMenu()
extern "C"  void DebugTracks_drawMenu_m3851917431 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::drawShowButton()
extern "C"  void DebugTracks_drawShowButton_m2046195303 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::drawBackButton()
extern "C"  void DebugTracks_drawBackButton_m2118325841 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DebugTracks::getUniqueName()
extern "C"  String_t* DebugTracks_getUniqueName_m2435257987 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::turnOn()
extern "C"  void DebugTracks_turnOn_m3816556912 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::turnOff()
extern "C"  void DebugTracks_turnOff_m2348968544 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugTracks::ResetOnOff()
extern "C"  void DebugTracks_ResetOnOff_m3386712725 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DebugTracks::isActive()
extern "C"  bool DebugTracks_isActive_m3842804752 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject DebugTracks::getGameObjectReference()
extern "C"  GameObject_t3674682005 * DebugTracks_getGameObjectReference_m4185751011 (DebugTracks_t138549403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
