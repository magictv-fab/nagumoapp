﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetIntValue
struct  SetIntValue_t2127530962  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetIntValue::intVariable
	FsmInt_t1596138449 * ___intVariable_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetIntValue::intValue
	FsmInt_t1596138449 * ___intValue_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetIntValue::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_intVariable_9() { return static_cast<int32_t>(offsetof(SetIntValue_t2127530962, ___intVariable_9)); }
	inline FsmInt_t1596138449 * get_intVariable_9() const { return ___intVariable_9; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_9() { return &___intVariable_9; }
	inline void set_intVariable_9(FsmInt_t1596138449 * value)
	{
		___intVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_9, value);
	}

	inline static int32_t get_offset_of_intValue_10() { return static_cast<int32_t>(offsetof(SetIntValue_t2127530962, ___intValue_10)); }
	inline FsmInt_t1596138449 * get_intValue_10() const { return ___intValue_10; }
	inline FsmInt_t1596138449 ** get_address_of_intValue_10() { return &___intValue_10; }
	inline void set_intValue_10(FsmInt_t1596138449 * value)
	{
		___intValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___intValue_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetIntValue_t2127530962, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
