﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat
struct PlayerPrefsSetFloat_t844658711;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::.ctor()
extern "C"  void PlayerPrefsSetFloat__ctor_m1774662911 (PlayerPrefsSetFloat_t844658711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::Reset()
extern "C"  void PlayerPrefsSetFloat_Reset_m3716063148 (PlayerPrefsSetFloat_t844658711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::OnEnter()
extern "C"  void PlayerPrefsSetFloat_OnEnter_m4035736854 (PlayerPrefsSetFloat_t844658711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
