﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct  GetAnimatorBody_t3376544941  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBody::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorBody::everyFrame
	bool ___everyFrame_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyPosition
	FsmVector3_t533912882 * ___bodyPosition_11;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyRotation
	FsmQuaternion_t3871136040 * ___bodyRotation_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyGameObject
	FsmGameObject_t1697147867 * ___bodyGameObject_13;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorBody::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBody::_animator
	Animator_t2776330603 * ____animator_15;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorBody::_transform
	Transform_t1659122786 * ____transform_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}

	inline static int32_t get_offset_of_bodyPosition_11() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___bodyPosition_11)); }
	inline FsmVector3_t533912882 * get_bodyPosition_11() const { return ___bodyPosition_11; }
	inline FsmVector3_t533912882 ** get_address_of_bodyPosition_11() { return &___bodyPosition_11; }
	inline void set_bodyPosition_11(FsmVector3_t533912882 * value)
	{
		___bodyPosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPosition_11, value);
	}

	inline static int32_t get_offset_of_bodyRotation_12() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___bodyRotation_12)); }
	inline FsmQuaternion_t3871136040 * get_bodyRotation_12() const { return ___bodyRotation_12; }
	inline FsmQuaternion_t3871136040 ** get_address_of_bodyRotation_12() { return &___bodyRotation_12; }
	inline void set_bodyRotation_12(FsmQuaternion_t3871136040 * value)
	{
		___bodyRotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___bodyRotation_12, value);
	}

	inline static int32_t get_offset_of_bodyGameObject_13() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___bodyGameObject_13)); }
	inline FsmGameObject_t1697147867 * get_bodyGameObject_13() const { return ___bodyGameObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_bodyGameObject_13() { return &___bodyGameObject_13; }
	inline void set_bodyGameObject_13(FsmGameObject_t1697147867 * value)
	{
		___bodyGameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___bodyGameObject_13, value);
	}

	inline static int32_t get_offset_of__animatorProxy_14() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ____animatorProxy_14)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_14() const { return ____animatorProxy_14; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_14() { return &____animatorProxy_14; }
	inline void set__animatorProxy_14(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_14 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_14, value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ____animator_15)); }
	inline Animator_t2776330603 * get__animator_15() const { return ____animator_15; }
	inline Animator_t2776330603 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t2776330603 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier(&____animator_15, value);
	}

	inline static int32_t get_offset_of__transform_16() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ____transform_16)); }
	inline Transform_t1659122786 * get__transform_16() const { return ____transform_16; }
	inline Transform_t1659122786 ** get_address_of__transform_16() { return &____transform_16; }
	inline void set__transform_16(Transform_t1659122786 * value)
	{
		____transform_16 = value;
		Il2CppCodeGenWriteBarrier(&____transform_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
