﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.BarcodeRow[]
struct BarcodeRowU5BU5D_t1319297543;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BarcodeMatrix
struct  BarcodeMatrix_t425107471  : public Il2CppObject
{
public:
	// ZXing.PDF417.Internal.BarcodeRow[] ZXing.PDF417.Internal.BarcodeMatrix::matrix
	BarcodeRowU5BU5D_t1319297543* ___matrix_0;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMatrix::currentRow
	int32_t ___currentRow_1;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMatrix::height
	int32_t ___height_2;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMatrix::width
	int32_t ___width_3;

public:
	inline static int32_t get_offset_of_matrix_0() { return static_cast<int32_t>(offsetof(BarcodeMatrix_t425107471, ___matrix_0)); }
	inline BarcodeRowU5BU5D_t1319297543* get_matrix_0() const { return ___matrix_0; }
	inline BarcodeRowU5BU5D_t1319297543** get_address_of_matrix_0() { return &___matrix_0; }
	inline void set_matrix_0(BarcodeRowU5BU5D_t1319297543* value)
	{
		___matrix_0 = value;
		Il2CppCodeGenWriteBarrier(&___matrix_0, value);
	}

	inline static int32_t get_offset_of_currentRow_1() { return static_cast<int32_t>(offsetof(BarcodeMatrix_t425107471, ___currentRow_1)); }
	inline int32_t get_currentRow_1() const { return ___currentRow_1; }
	inline int32_t* get_address_of_currentRow_1() { return &___currentRow_1; }
	inline void set_currentRow_1(int32_t value)
	{
		___currentRow_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(BarcodeMatrix_t425107471, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(BarcodeMatrix_t425107471, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
