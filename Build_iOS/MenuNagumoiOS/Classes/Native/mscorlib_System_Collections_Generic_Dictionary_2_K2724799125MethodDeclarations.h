﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct KeyCollection_t2724799125;
// System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct Dictionary_2_t1098039674;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t479520291;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1712975728.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1410693776_gshared (KeyCollection_t2724799125 * __this, Dictionary_2_t1098039674 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1410693776(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2724799125 *, Dictionary_2_t1098039674 *, const MethodInfo*))KeyCollection__ctor_m1410693776_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4291921094_gshared (KeyCollection_t2724799125 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4291921094(__this, ___item0, method) ((  void (*) (KeyCollection_t2724799125 *, Il2CppChar, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4291921094_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m236856253_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m236856253(__this, method) ((  void (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m236856253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3657062344_gshared (KeyCollection_t2724799125 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3657062344(__this, ___item0, method) ((  bool (*) (KeyCollection_t2724799125 *, Il2CppChar, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3657062344_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m352983853_gshared (KeyCollection_t2724799125 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m352983853(__this, ___item0, method) ((  bool (*) (KeyCollection_t2724799125 *, Il2CppChar, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m352983853_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1529887887_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1529887887(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1529887887_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2451526831_gshared (KeyCollection_t2724799125 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2451526831(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2724799125 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2451526831_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2940553214_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2940553214(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2940553214_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4034723113_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4034723113(__this, method) ((  bool (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4034723113_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3914904859_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3914904859(__this, method) ((  bool (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3914904859_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3817146637_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3817146637(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3817146637_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2115051845_gshared (KeyCollection_t2724799125 * __this, CharU5BU5D_t3324145743* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2115051845(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2724799125 *, CharU5BU5D_t3324145743*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2115051845_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::GetEnumerator()
extern "C"  Enumerator_t1712975728  KeyCollection_GetEnumerator_m610527890_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m610527890(__this, method) ((  Enumerator_t1712975728  (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_GetEnumerator_m610527890_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3063772373_gshared (KeyCollection_t2724799125 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3063772373(__this, method) ((  int32_t (*) (KeyCollection_t2724799125 *, const MethodInfo*))KeyCollection_get_Count_m3063772373_gshared)(__this, method)
