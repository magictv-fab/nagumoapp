﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationsManager
struct NotificationsManager_t3392303557;
// OSSubscriptionStateChanges
struct OSSubscriptionStateChanges_t52612275;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// OSPermissionStateChanges
struct OSPermissionStateChanges_t1134260069;
// OSEmailSubscriptionStateChanges
struct OSEmailSubscriptionStateChanges_t721307367;
// OSNotification
struct OSNotification_t892481775;
// OSNotificationOpenedResult
struct OSNotificationOpenedResult_t158818421;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OSSubscriptionStateChanges52612275.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_OSPermissionStateChanges1134260069.h"
#include "AssemblyU2DCSharp_OSEmailSubscriptionStateChanges721307367.h"
#include "AssemblyU2DCSharp_OSNotification892481775.h"
#include "AssemblyU2DCSharp_OSNotificationOpenedResult158818421.h"

// System.Void NotificationsManager::.ctor()
extern "C"  void NotificationsManager__ctor_m212407926 (NotificationsManager_t3392303557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::Start()
extern "C"  void NotificationsManager_Start_m3454513014 (NotificationsManager_t3392303557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::OneSignal_subscriptionObserver(OSSubscriptionStateChanges)
extern "C"  void NotificationsManager_OneSignal_subscriptionObserver_m3752190783 (NotificationsManager_t3392303557 * __this, OSSubscriptionStateChanges_t52612275 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::AtualizaToken(System.String)
extern "C"  void NotificationsManager_AtualizaToken_m73974872 (NotificationsManager_t3392303557 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NotificationsManager::IAtualizaToken(System.String)
extern "C"  Il2CppObject * NotificationsManager_IAtualizaToken_m2200846623 (NotificationsManager_t3392303557 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::OneSignal_permissionObserver(OSPermissionStateChanges)
extern "C"  void NotificationsManager_OneSignal_permissionObserver_m4064470207 (NotificationsManager_t3392303557 * __this, OSPermissionStateChanges_t1134260069 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::OneSignal_emailSubscriptionObserver(OSEmailSubscriptionStateChanges)
extern "C"  void NotificationsManager_OneSignal_emailSubscriptionObserver_m2831598553 (NotificationsManager_t3392303557 * __this, OSEmailSubscriptionStateChanges_t721307367 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::HandleNotificationReceived(OSNotification)
extern "C"  void NotificationsManager_HandleNotificationReceived_m1234080435 (Il2CppObject * __this /* static, unused */, OSNotification_t892481775 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager::HandleNotificationOpened(OSNotificationOpenedResult)
extern "C"  void NotificationsManager_HandleNotificationOpened_m352989429 (Il2CppObject * __this /* static, unused */, OSNotificationOpenedResult_t158818421 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NotificationsManager::ClearString(System.String)
extern "C"  String_t* NotificationsManager_ClearString_m2225866195 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
