﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform
struct PkzipClassicEncryptCryptoTransform_t1919402088;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::.ctor(System.Byte[])
extern "C"  void PkzipClassicEncryptCryptoTransform__ctor_m3280366090 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, ByteU5BU5D_t4260760469* ___keyBlock0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* PkzipClassicEncryptCryptoTransform_TransformFinalBlock_m1411548159 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t PkzipClassicEncryptCryptoTransform_TransformBlock_m1225536483 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4260760469* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_CanReuseTransform()
extern "C"  bool PkzipClassicEncryptCryptoTransform_get_CanReuseTransform_m1591550928 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_InputBlockSize()
extern "C"  int32_t PkzipClassicEncryptCryptoTransform_get_InputBlockSize_m417708484 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_OutputBlockSize()
extern "C"  int32_t PkzipClassicEncryptCryptoTransform_get_OutputBlockSize_m3104294799 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_CanTransformMultipleBlocks()
extern "C"  bool PkzipClassicEncryptCryptoTransform_get_CanTransformMultipleBlocks_m1363600172 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::Dispose()
extern "C"  void PkzipClassicEncryptCryptoTransform_Dispose_m949969052 (PkzipClassicEncryptCryptoTransform_t1919402088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
