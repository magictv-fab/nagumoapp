﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::.ctor()
#define Stack_1__ctor_m1327177070(__this, method) ((  void (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::.ctor(System.Int32)
#define Stack_1__ctor_m1269866270(__this, ___count0, method) ((  void (*) (Stack_1_t330706054 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3444823400(__this, method) ((  bool (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3599613190(__this, method) ((  Il2CppObject * (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m907054998(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t330706054 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m491952888(__this, method) ((  Il2CppObject* (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m425286545(__this, method) ((  Il2CppObject * (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Clear()
#define Stack_1_Clear_m3028277657(__this, method) ((  void (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Contains(T)
#define Stack_1_Contains_m2922526495(__this, ___t0, method) ((  bool (*) (Stack_1_t330706054 *, Fsm_t1527112426 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::CopyTo(T[],System.Int32)
#define Stack_1_CopyTo_m2039812865(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t330706054 *, FsmU5BU5D_t3357880879*, int32_t, const MethodInfo*))Stack_1_CopyTo_m2694759063_gshared)(__this, ___dest0, ___idx1, method)
// T System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Peek()
#define Stack_1_Peek_m826061760(__this, method) ((  Fsm_t1527112426 * (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Pop()
#define Stack_1_Pop_m858237550(__this, method) ((  Fsm_t1527112426 * (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Push(T)
#define Stack_1_Push_m4207680162(__this, ___t0, method) ((  void (*) (Stack_1_t330706054 *, Fsm_t1527112426 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// T[] System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::ToArray()
#define Stack_1_ToArray_m2084763015(__this, method) ((  FsmU5BU5D_t3357880879* (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_ToArray_m3752136753_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::get_Count()
#define Stack_1_get_Count_m2465306147(__this, method) ((  int32_t (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::GetEnumerator()
#define Stack_1_GetEnumerator_m2424631528(__this, method) ((  Enumerator_t4183459376  (*) (Stack_1_t330706054 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
