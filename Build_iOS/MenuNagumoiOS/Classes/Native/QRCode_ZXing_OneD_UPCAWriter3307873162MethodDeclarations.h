﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCAWriter
struct UPCAWriter_t3307873162;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// ZXing.Common.BitMatrix ZXing.OneD.UPCAWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * UPCAWriter_encode_m3640512941 (UPCAWriter_t3307873162 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.UPCAWriter::preencode(System.String)
extern "C"  String_t* UPCAWriter_preencode_m1769464367 (Il2CppObject * __this /* static, unused */, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCAWriter::.ctor()
extern "C"  void UPCAWriter__ctor_m715111385 (UPCAWriter_t3307873162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
