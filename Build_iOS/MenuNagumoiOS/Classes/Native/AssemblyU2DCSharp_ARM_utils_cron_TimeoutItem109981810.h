﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2357615493.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.cron.TimeoutItem
struct  TimeoutItem_t109981810  : public ViewTimeControllAbstract_t2357615493
{
public:
	// System.Int32 ARM.utils.cron.TimeoutItem::id
	int32_t ___id_6;
	// System.Single ARM.utils.cron.TimeoutItem::delay
	float ___delay_7;

public:
	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(TimeoutItem_t109981810, ___id_6)); }
	inline int32_t get_id_6() const { return ___id_6; }
	inline int32_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(int32_t value)
	{
		___id_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(TimeoutItem_t109981810, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
