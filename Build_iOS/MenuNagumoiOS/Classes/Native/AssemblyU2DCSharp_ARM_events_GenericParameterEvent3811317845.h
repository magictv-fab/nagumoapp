﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.events.GenericParameterEventDispatcher/GenericEvent
struct GenericEvent_t3373847142;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.GenericParameterEventDispatcher
struct  GenericParameterEventDispatcher_t3811317845  : public Il2CppObject
{
public:
	// ARM.events.GenericParameterEventDispatcher/GenericEvent ARM.events.GenericParameterEventDispatcher::_genericEvent
	GenericEvent_t3373847142 * ____genericEvent_0;

public:
	inline static int32_t get_offset_of__genericEvent_0() { return static_cast<int32_t>(offsetof(GenericParameterEventDispatcher_t3811317845, ____genericEvent_0)); }
	inline GenericEvent_t3373847142 * get__genericEvent_0() const { return ____genericEvent_0; }
	inline GenericEvent_t3373847142 ** get_address_of__genericEvent_0() { return &____genericEvent_0; }
	inline void set__genericEvent_0(GenericEvent_t3373847142 * value)
	{
		____genericEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&____genericEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
