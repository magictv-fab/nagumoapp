﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager/<ISendStatus>c__Iterator52
struct U3CISendStatusU3Ec__Iterator52_t2468455590;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FavoritosManager/<ISendStatus>c__Iterator52::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator52__ctor_m612110725 (U3CISendStatusU3Ec__Iterator52_t2468455590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ISendStatus>c__Iterator52::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator52_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3769664109 (U3CISendStatusU3Ec__Iterator52_t2468455590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ISendStatus>c__Iterator52::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator52_System_Collections_IEnumerator_get_Current_m3607560193 (U3CISendStatusU3Ec__Iterator52_t2468455590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FavoritosManager/<ISendStatus>c__Iterator52::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator52_MoveNext_m2337804367 (U3CISendStatusU3Ec__Iterator52_t2468455590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ISendStatus>c__Iterator52::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator52_Dispose_m4021137538 (U3CISendStatusU3Ec__Iterator52_t2468455590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ISendStatus>c__Iterator52::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator52_Reset_m2553510962 (U3CISendStatusU3Ec__Iterator52_t2468455590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
