﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// UnityEngine.Component
struct Component_t3501516275;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2054899105;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct NullPremiumObjectFactory_t3790484746;
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t2057130681;
// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t55743383;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t3715610867;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t785513668;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t159784161;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3956694567;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3297864633;
// System.Func`3<System.Single,System.Single,System.Single>
struct Func_3_t1552338692;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t864200549;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3207823784.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi660379442.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo1354080954.h"
#include "mscorlib_System_Reflection_Emit_Label2268465130.h"
#include "mscorlib_System_Reflection_Emit_MonoResource1505432149.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS3880501745.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC2113902833.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4013605874.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2420703430.h"
#include "mscorlib_System_SByte1161769777.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_TermInfoStrings2002624446.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt6424668076.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3658211563.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope1749213747.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine_UnityEngine_jvalue3862675307.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "UnityEngine_UnityEngine_ParticleCollisionEvent2700926194.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy2662964855.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E3010462567.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXE354375056.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Rectangle2265684451.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3609410114.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan526813605.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa3919795561.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1737958143.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan459380335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1548845516.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1826866697.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro1961690790.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "mscorlib_System_Threading_Interlocked373807572.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen1152456458.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2704060668.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility4196051482.h"
#include "UnityEngine_UnityEngine_JsonUtility4196051482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown4201779933.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumOb3790484746.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr3365905913.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2057130681.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaWr2202637939MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru3107149249MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumOb1788967734MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru3107149249.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMa2355365335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMana55743383.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac455954211.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTra4028259784.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrack3721656949.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2067565974.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRu1069467038MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TypeMappi2898721118MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac243160803MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTra1934199560MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrack2086340917MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr1058218966MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac243160803.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTra1934199560.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrack2086340917.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr1058218966.h"
#include "QRCode_ZXing_SupportClass1360542943.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Predicate_1_gen3781873254.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData2955630591.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "mscorlib_System_Converter_2_gen3715610867.h"
#include "mscorlib_System_Converter_2_gen3715610867MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "System_Core_System_Linq_Check10677726MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "System_Core_System_Linq_Enumerable_Fallback3964967226.h"
#include "System_Core_System_Func_2_gen785513668.h"
#include "System_Core_System_Func_2_gen785513668MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3956694567.h"
#include "System_Core_System_Func_3_gen3956694567MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1552338692.h"
#include "System_Core_System_Func_3_gen1552338692MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEven864200549.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2704060668MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3027308338MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3027308338.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"

// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t3059612989  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667(__this, ___index0, method) ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t3059612989_m3008899942_gshared (Il2CppArray * __this, int32_t p0, CustomAttributeNamedArgument_t3059612989 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t3059612989_m3008899942(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, CustomAttributeNamedArgument_t3059612989 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t3059612989_m3008899942_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t3301293422  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746(__this, ___index0, method) ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t3301293422_m2846288151_gshared (Il2CppArray * __this, int32_t p0, CustomAttributeTypedArgument_t3301293422 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t3301293422_m2846288151(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, CustomAttributeTypedArgument_t3301293422 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t3301293422_m2846288151_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t3207823784  Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434(__this, ___index0, method) ((  LabelData_t3207823784  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabelData_t3207823784_m4249583967_gshared (Il2CppArray * __this, int32_t p0, LabelData_t3207823784 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelData_t3207823784_m4249583967(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, LabelData_t3207823784 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelData_t3207823784_m4249583967_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t660379442  Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500(__this, ___index0, method) ((  LabelFixup_t660379442  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabelFixup_t660379442_m482634139_gshared (Il2CppArray * __this, int32_t p0, LabelFixup_t660379442 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelFixup_t660379442_m482634139(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, LabelFixup_t660379442 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelFixup_t660379442_m482634139_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t1354080954  Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117(__this, ___index0, method) ((  ILTokenInfo_t1354080954  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisILTokenInfo_t1354080954_m3036294916_gshared (Il2CppArray * __this, int32_t p0, ILTokenInfo_t1354080954 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisILTokenInfo_t1354080954_m3036294916(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ILTokenInfo_t1354080954 *, const MethodInfo*))Array_GetGenericValueImpl_TisILTokenInfo_t1354080954_m3036294916_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern "C"  Label_t2268465130  Array_InternalArray__get_Item_TisLabel_t2268465130_m1753959877_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabel_t2268465130_m1753959877(__this, ___index0, method) ((  Label_t2268465130  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabel_t2268465130_m1753959877_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.Label>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabel_t2268465130_m3421628788_gshared (Il2CppArray * __this, int32_t p0, Label_t2268465130 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabel_t2268465130_m3421628788(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Label_t2268465130 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabel_t2268465130_m3421628788_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern "C"  MonoResource_t1505432149  Array_InternalArray__get_Item_TisMonoResource_t1505432149_m358920598_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMonoResource_t1505432149_m358920598(__this, ___index0, method) ((  MonoResource_t1505432149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMonoResource_t1505432149_m358920598_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.MonoResource>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMonoResource_t1505432149_m2129286481_gshared (Il2CppArray * __this, int32_t p0, MonoResource_t1505432149 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMonoResource_t1505432149_m2129286481(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, MonoResource_t1505432149 *, const MethodInfo*))Array_GetGenericValueImpl_TisMonoResource_t1505432149_m2129286481_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern "C"  RefEmitPermissionSet_t3880501745  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3880501745_m113952890_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3880501745_m113952890(__this, ___index0, method) ((  RefEmitPermissionSet_t3880501745  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3880501745_m113952890_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRefEmitPermissionSet_t3880501745_m2461304301_gshared (Il2CppArray * __this, int32_t p0, RefEmitPermissionSet_t3880501745 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRefEmitPermissionSet_t3880501745_m2461304301(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RefEmitPermissionSet_t3880501745 *, const MethodInfo*))Array_GetGenericValueImpl_TisRefEmitPermissionSet_t3880501745_m2461304301_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t741930026  Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410(__this, ___index0, method) ((  ParameterModifier_t741930026  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.ParameterModifier>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisParameterModifier_t741930026_m950388357_gshared (Il2CppArray * __this, int32_t p0, ParameterModifier_t741930026 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisParameterModifier_t741930026_m950388357(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ParameterModifier_t741930026 *, const MethodInfo*))Array_GetGenericValueImpl_TisParameterModifier_t741930026_m950388357_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t2113902833  Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445(__this, ___index0, method) ((  ResourceCacheItem_t2113902833  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResourceCacheItem_t2113902833_m911034714_gshared (Il2CppArray * __this, int32_t p0, ResourceCacheItem_t2113902833 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceCacheItem_t2113902833_m911034714(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ResourceCacheItem_t2113902833 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceCacheItem_t2113902833_m911034714_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t4013605874  Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776(__this, ___index0, method) ((  ResourceInfo_t4013605874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResourceInfo_t4013605874_m4149525161_gshared (Il2CppArray * __this, int32_t p0, ResourceInfo_t4013605874 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceInfo_t4013605874_m4149525161(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ResourceInfo_t4013605874 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceInfo_t4013605874_m4149525161_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397(__this, ___index0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTypeTag_t2420703430_m1311091868_gshared (Il2CppArray * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTypeTag_t2420703430_m1311091868(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTypeTag_t2420703430_m1311091868_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230(__this, ___index0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSByte_t1161769777_m1632182611_gshared (Il2CppArray * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSByte_t1161769777_m1632182611(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSByte_t1161769777_m1632182611_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t766901931  Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218(__this, ___index0, method) ((  X509ChainStatus_t766901931  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisX509ChainStatus_t766901931_m1175622045_gshared (Il2CppArray * __this, int32_t p0, X509ChainStatus_t766901931 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisX509ChainStatus_t766901931_m1175622045(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, X509ChainStatus_t766901931 *, const MethodInfo*))Array_GetGenericValueImpl_TisX509ChainStatus_t766901931_m1175622045_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775(__this, ___index0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Single>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSingle_t4291918972_m1387272912_gshared (Il2CppArray * __this, int32_t p0, float* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSingle_t4291918972_m1387272912(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, float*, const MethodInfo*))Array_GetGenericValueImpl_TisSingle_t4291918972_m1387272912_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t2002624446_m1248113113_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTermInfoStrings_t2002624446_m1248113113(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTermInfoStrings_t2002624446_m1248113113_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TermInfoStrings>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTermInfoStrings_t2002624446_m2810016928_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTermInfoStrings_t2002624446_m2810016928(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTermInfoStrings_t2002624446_m2810016928_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3811539797  Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3811539797_m160824484(__this, ___index0, method) ((  Mark_t3811539797  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Text.RegularExpressions.Mark>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMark_t3811539797_m2494808835_gshared (Il2CppArray * __this, int32_t p0, Mark_t3811539797 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMark_t3811539797_m2494808835(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Mark_t3811539797 *, const MethodInfo*))Array_GetGenericValueImpl_TisMark_t3811539797_m2494808835_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t413522987  Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760(__this, ___index0, method) ((  TimeSpan_t413522987  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTimeSpan_t413522987_m2245336767_gshared (Il2CppArray * __this, int32_t p0, TimeSpan_t413522987 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t413522987_m2245336767(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TimeSpan_t413522987 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t413522987_m2245336767_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256(__this, ___index0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt16_t24667923_m1140249575_gshared (Il2CppArray * __this, int32_t p0, uint16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt16_t24667923_m1140249575(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt16_t24667923_m1140249575_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062(__this, ___index0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt32_t24667981_m513700641_gshared (Il2CppArray * __this, int32_t p0, uint32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt32_t24667981_m513700641(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt32_t24667981_m513700641_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503(__this, ___index0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt64_t24668076_m3412168192_gshared (Il2CppArray * __this, int32_t p0, uint64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt64_t24668076_m3412168192(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt64_t24668076_m3412168192_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t1290668975  Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123(__this, ___index0, method) ((  UriScheme_t1290668975  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Uri/UriScheme>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUriScheme_t1290668975_m4227749606_gshared (Il2CppArray * __this, int32_t p0, UriScheme_t1290668975 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUriScheme_t1290668975_m4227749606(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UriScheme_t1290668975 *, const MethodInfo*))Array_GetGenericValueImpl_TisUriScheme_t1290668975_m4227749606_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t3658211563  Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851(__this, ___index0, method) ((  NsDecl_t3658211563  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNsDecl_t3658211563_m806024660_gshared (Il2CppArray * __this, int32_t p0, NsDecl_t3658211563 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNsDecl_t3658211563_m806024660(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NsDecl_t3658211563 *, const MethodInfo*))Array_GetGenericValueImpl_TisNsDecl_t3658211563_m806024660_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t1749213747  Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815(__this, ___index0, method) ((  NsScope_t1749213747  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XmlNamespaceManager/NsScope>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNsScope_t1749213747_m2130078634_gshared (Il2CppArray * __this, int32_t p0, NsScope_t1749213747 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNsScope_t1749213747_m2130078634(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NsScope_t1749213747 *, const MethodInfo*))Array_GetGenericValueImpl_TisNsScope_t1749213747_m2130078634_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t4194546905  Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850(__this, ___index0, method) ((  Color_t4194546905  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor_t4194546905_m855411061_gshared (Il2CppArray * __this, int32_t p0, Color_t4194546905 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor_t4194546905_m855411061(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color_t4194546905 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor_t4194546905_m855411061_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t598853688  Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403(__this, ___index0, method) ((  Color32_t598853688  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor32_t598853688_m3930958100_gshared (Il2CppArray * __this, int32_t p0, Color32_t598853688 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor32_t598853688_m3930958100(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color32_t598853688 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor32_t598853688_m3930958100_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t243083348  Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859(__this, ___index0, method) ((  ContactPoint_t243083348  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint_t243083348_m2683850686_gshared (Il2CppArray * __this, int32_t p0, ContactPoint_t243083348 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint_t243083348_m2683850686(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint_t243083348 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint_t243083348_m2683850686_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t4288432358  Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017(__this, ___index0, method) ((  ContactPoint2D_t4288432358  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint2D_t4288432358_m1672251792_gshared (Il2CppArray * __this, int32_t p0, ContactPoint2D_t4288432358 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint2D_t4288432358_m1672251792(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint2D_t4288432358 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint2D_t4288432358_m1672251792_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t3762661364  Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905(__this, ___index0, method) ((  RaycastResult_t3762661364  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.EventSystems.RaycastResult>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastResult_t3762661364_m1415359984_gshared (Il2CppArray * __this, int32_t p0, RaycastResult_t3762661364 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastResult_t3762661364_m1415359984(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastResult_t3762661364 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastResult_t3762661364_m1415359984_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.jvalue>(System.Int32)
extern "C"  jvalue_t3862675307  Array_InternalArray__get_Item_Tisjvalue_t3862675307_m2663906116_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_Tisjvalue_t3862675307_m2663906116(__this, ___index0, method) ((  jvalue_t3862675307  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_Tisjvalue_t3862675307_m2663906116_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.jvalue>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_Tisjvalue_t3862675307_m1163186837_gshared (Il2CppArray * __this, int32_t p0, jvalue_t3862675307 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_Tisjvalue_t3862675307_m1163186837(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, jvalue_t3862675307 *, const MethodInfo*))Array_GetGenericValueImpl_Tisjvalue_t3862675307_m1163186837_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t4079056114  Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013(__this, ___index0, method) ((  Keyframe_t4079056114  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Keyframe>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyframe_t4079056114_m3275637980_gshared (Il2CppArray * __this, int32_t p0, Keyframe_t4079056114 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyframe_t4079056114_m3275637980(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Keyframe_t4079056114 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyframe_t4079056114_m3275637980_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.NetworkPlayer>(System.Int32)
extern "C"  NetworkPlayer_t3231273765  Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030(__this, ___index0, method) ((  NetworkPlayer_t3231273765  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.NetworkPlayer>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNetworkPlayer_t3231273765_m663564481_gshared (Il2CppArray * __this, int32_t p0, NetworkPlayer_t3231273765 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNetworkPlayer_t3231273765_m663564481(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NetworkPlayer_t3231273765 *, const MethodInfo*))Array_GetGenericValueImpl_TisNetworkPlayer_t3231273765_m663564481_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.NetworkReachability>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisNetworkReachability_t612403035_m3947396976_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNetworkReachability_t612403035_m3947396976(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNetworkReachability_t612403035_m3947396976_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.NetworkReachability>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNetworkReachability_t612403035_m162460471_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNetworkReachability_t612403035_m162460471(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisNetworkReachability_t612403035_m162460471_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ParticleCollisionEvent>(System.Int32)
extern "C"  ParticleCollisionEvent_t2700926194  Array_InternalArray__get_Item_TisParticleCollisionEvent_t2700926194_m2185485021_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParticleCollisionEvent_t2700926194_m2185485021(__this, ___index0, method) ((  ParticleCollisionEvent_t2700926194  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParticleCollisionEvent_t2700926194_m2185485021_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ParticleCollisionEvent>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisParticleCollisionEvent_t2700926194_m2473941148_gshared (Il2CppArray * __this, int32_t p0, ParticleCollisionEvent_t2700926194 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisParticleCollisionEvent_t2700926194_m2473941148(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ParticleCollisionEvent_t2700926194 *, const MethodInfo*))Array_GetGenericValueImpl_TisParticleCollisionEvent_t2700926194_m2473941148_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Quaternion>(System.Int32)
extern "C"  Quaternion_t1553702882  Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941(__this, ___index0, method) ((  Quaternion_t1553702882  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Quaternion>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisQuaternion_t1553702882_m3893612044_gshared (Il2CppArray * __this, int32_t p0, Quaternion_t1553702882 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisQuaternion_t1553702882_m3893612044(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Quaternion_t1553702882 *, const MethodInfo*))Array_GetGenericValueImpl_TisQuaternion_t1553702882_m3893612044_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t4003175726  Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793(__this, ___index0, method) ((  RaycastHit_t4003175726  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit_t4003175726_m1822174552_gshared (Il2CppArray * __this, int32_t p0, RaycastHit_t4003175726 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit_t4003175726_m1822174552(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit_t4003175726 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit_t4003175726_m1822174552_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t1374744384  Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143(__this, ___index0, method) ((  RaycastHit2D_t1374744384  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit2D_t1374744384_m2530175146_gshared (Il2CppArray * __this, int32_t p0, RaycastHit2D_t1374744384 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit2D_t1374744384_m2530175146(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit2D_t1374744384 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit2D_t1374744384_m2530175146_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Rect>(System.Int32)
extern "C"  Rect_t4241904616  Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRect_t4241904616_m261192103(__this, ___index0, method) ((  Rect_t4241904616  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Rect>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRect_t4241904616_m4039766098_gshared (Il2CppArray * __this, int32_t p0, Rect_t4241904616 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRect_t4241904616_m4039766098(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Rect_t4241904616 *, const MethodInfo*))Array_GetGenericValueImpl_TisRect_t4241904616_m4039766098_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t3209134097  Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997(__this, ___index0, method) ((  HitInfo_t3209134097  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SendMouseEvents/HitInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisHitInfo_t3209134097_m3860792378_gshared (Il2CppArray * __this, int32_t p0, HitInfo_t3209134097 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisHitInfo_t3209134097_m3860792378(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, HitInfo_t3209134097 *, const MethodInfo*))Array_GetGenericValueImpl_TisHitInfo_t3209134097_m3860792378_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t3481375915  Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226(__this, ___index0, method) ((  GcAchievementData_t3481375915  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcAchievementData_t3481375915_m989090367_gshared (Il2CppArray * __this, int32_t p0, GcAchievementData_t3481375915 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcAchievementData_t3481375915_m989090367(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcAchievementData_t3481375915 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcAchievementData_t3481375915_m989090367_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2181296590  Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207(__this, ___index0, method) ((  GcScoreData_t2181296590  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcScoreData_t2181296590_m4057734434_gshared (Il2CppArray * __this, int32_t p0, GcScoreData_t2181296590 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcScoreData_t2181296590_m4057734434(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcScoreData_t2181296590 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcScoreData_t2181296590_m4057734434_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.TextEditor/TextEditOp>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTextEditOp_t4145961110_m4219827839_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTextEditOp_t4145961110_m4219827839(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTextEditOp_t4145961110_m4219827839_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Touch>(System.Int32)
extern "C"  Touch_t4210255029  Array_InternalArray__get_Item_TisTouch_t4210255029_m2563763030_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTouch_t4210255029_m2563763030(__this, ___index0, method) ((  Touch_t4210255029  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTouch_t4210255029_m2563763030_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Touch>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTouch_t4210255029_m3383866193_gshared (Il2CppArray * __this, int32_t p0, Touch_t4210255029 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTouch_t4210255029_m3383866193(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Touch_t4210255029 *, const MethodInfo*))Array_GetGenericValueImpl_TisTouch_t4210255029_m3383866193_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t2662964855_m696167015_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t2662964855_m696167015(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t2662964855_m696167015_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UI.InputField/ContentType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContentType_t2662964855_m2904454496_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContentType_t2662964855_m2904454496(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisContentType_t2662964855_m2904454496_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t65807484  Array_InternalArray__get_Item_TisUICharInfo_t65807484_m2153093395_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t65807484_m2153093395(__this, ___index0, method) ((  UICharInfo_t65807484  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t65807484_m2153093395_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UICharInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUICharInfo_t65807484_m972947878_gshared (Il2CppArray * __this, int32_t p0, UICharInfo_t65807484 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUICharInfo_t65807484_m972947878(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UICharInfo_t65807484 *, const MethodInfo*))Array_GetGenericValueImpl_TisUICharInfo_t65807484_m972947878_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t4113875482  Array_InternalArray__get_Item_TisUILineInfo_t4113875482_m2200325045_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t4113875482_m2200325045(__this, ___index0, method) ((  UILineInfo_t4113875482  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t4113875482_m2200325045_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UILineInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUILineInfo_t4113875482_m1279108164_gshared (Il2CppArray * __this, int32_t p0, UILineInfo_t4113875482 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUILineInfo_t4113875482_m1279108164(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UILineInfo_t4113875482 *, const MethodInfo*))Array_GetGenericValueImpl_TisUILineInfo_t4113875482_m1279108164_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t4244065212  Array_InternalArray__get_Item_TisUIVertex_t4244065212_m2640447059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t4244065212_m2640447059(__this, ___index0, method) ((  UIVertex_t4244065212  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t4244065212_m2640447059_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UIVertex>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUIVertex_t4244065212_m1585369766_gshared (Il2CppArray * __this, int32_t p0, UIVertex_t4244065212 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUIVertex_t4244065212_m1585369766(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UIVertex_t4244065212 *, const MethodInfo*))Array_GetGenericValueImpl_TisUIVertex_t4244065212_m1585369766_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t4282066565  Array_InternalArray__get_Item_TisVector2_t4282066565_m1844444166_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t4282066565_m1844444166(__this, ___index0, method) ((  Vector2_t4282066565  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t4282066565_m1844444166_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector2>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector2_t4282066565_m2608717537_gshared (Il2CppArray * __this, int32_t p0, Vector2_t4282066565 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector2_t4282066565_m2608717537(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector2_t4282066565 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector2_t4282066565_m2608717537_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t4282066566  Array_InternalArray__get_Item_TisVector3_t4282066566_m1333909989_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t4282066566_m1333909989(__this, ___index0, method) ((  Vector3_t4282066566  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t4282066566_m1333909989_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector3>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector3_t4282066566_m2820068450_gshared (Il2CppArray * __this, int32_t p0, Vector3_t4282066566 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector3_t4282066566_m2820068450(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector3_t4282066566 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector3_t4282066566_m2820068450_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t4282066567  Array_InternalArray__get_Item_TisVector4_t4282066567_m823375812_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector4_t4282066567_m823375812(__this, ___index0, method) ((  Vector4_t4282066567  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector4_t4282066567_m823375812_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector4>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector4_t4282066567_m3031419363_gshared (Il2CppArray * __this, int32_t p0, Vector4_t4282066567 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector4_t4282066567_m3031419363(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector4_t4282066567 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector4_t4282066567_m3031419363_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern "C"  WebCamDevice_t3274004757  Array_InternalArray__get_Item_TisWebCamDevice_t3274004757_m3923092186_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWebCamDevice_t3274004757_m3923092186(__this, ___index0, method) ((  WebCamDevice_t3274004757  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWebCamDevice_t3274004757_m3923092186_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.WebCamDevice>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisWebCamDevice_t3274004757_m2650379775_gshared (Il2CppArray * __this, int32_t p0, WebCamDevice_t3274004757 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisWebCamDevice_t3274004757_m2650379775(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, WebCamDevice_t3274004757 *, const MethodInfo*))Array_GetGenericValueImpl_TisWebCamDevice_t3274004757_m2650379775_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.Eyewear/EyewearCalibrationReading>(System.Int32)
extern "C"  EyewearCalibrationReading_t3010462567  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3010462567_m3646914679_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3010462567_m3646914679(__this, ___index0, method) ((  EyewearCalibrationReading_t3010462567  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3010462567_m3646914679_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.Eyewear/EyewearCalibrationReading>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisEyewearCalibrationReading_t3010462567_m2375923152_gshared (Il2CppArray * __this, int32_t p0, EyewearCalibrationReading_t3010462567 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisEyewearCalibrationReading_t3010462567_m2375923152(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, EyewearCalibrationReading_t3010462567 *, const MethodInfo*))Array_GetGenericValueImpl_TisEyewearCalibrationReading_t3010462567_m2375923152_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t354375056_m2801720466_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPIXEL_FORMAT_t354375056_m2801720466(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPIXEL_FORMAT_t354375056_m2801720466_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.Image/PIXEL_FORMAT>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPIXEL_FORMAT_t354375056_m4075936007_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPIXEL_FORMAT_t354375056_m4075936007(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisPIXEL_FORMAT_t354375056_m4075936007_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern "C"  RectangleData_t2265684451  Array_InternalArray__get_Item_TisRectangleData_t2265684451_m28733777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRectangleData_t2265684451_m28733777(__this, ___index0, method) ((  RectangleData_t2265684451  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRectangleData_t2265684451_m28733777_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.RectangleData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRectangleData_t2265684451_m2238103222_gshared (Il2CppArray * __this, int32_t p0, RectangleData_t2265684451 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRectangleData_t2265684451_m2238103222(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RectangleData_t2265684451 *, const MethodInfo*))Array_GetGenericValueImpl_TisRectangleData_t2265684451_m2238103222_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern "C"  TargetSearchResult_t3609410114  Array_InternalArray__get_Item_TisTargetSearchResult_t3609410114_m610339772_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTargetSearchResult_t3609410114_m610339772(__this, ___index0, method) ((  TargetSearchResult_t3609410114  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTargetSearchResult_t3609410114_m610339772_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.TargetFinder/TargetSearchResult>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTargetSearchResult_t3609410114_m4164177771_gshared (Il2CppArray * __this, int32_t p0, TargetSearchResult_t3609410114 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTargetSearchResult_t3609410114_m4164177771(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TargetSearchResult_t3609410114 *, const MethodInfo*))Array_GetGenericValueImpl_TisTargetSearchResult_t3609410114_m4164177771_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern "C"  PropData_t526813605  Array_InternalArray__get_Item_TisPropData_t526813605_m3474501881_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropData_t526813605_m3474501881(__this, ___index0, method) ((  PropData_t526813605  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropData_t526813605_m3474501881_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/PropData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPropData_t526813605_m2848816078_gshared (Il2CppArray * __this, int32_t p0, PropData_t526813605 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPropData_t526813605_m2848816078(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, PropData_t526813605 *, const MethodInfo*))Array_GetGenericValueImpl_TisPropData_t526813605_m2848816078_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern "C"  SmartTerrainRevisionData_t3919795561  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t3919795561_m2436319413_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t3919795561_m2436319413(__this, ___index0, method) ((  SmartTerrainRevisionData_t3919795561  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t3919795561_m2436319413_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSmartTerrainRevisionData_t3919795561_m400460690_gshared (Il2CppArray * __this, int32_t p0, SmartTerrainRevisionData_t3919795561 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSmartTerrainRevisionData_t3919795561_m400460690(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, SmartTerrainRevisionData_t3919795561 *, const MethodInfo*))Array_GetGenericValueImpl_TisSmartTerrainRevisionData_t3919795561_m400460690_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern "C"  SurfaceData_t1737958143  Array_InternalArray__get_Item_TisSurfaceData_t1737958143_m251254851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSurfaceData_t1737958143_m251254851(__this, ___index0, method) ((  SurfaceData_t1737958143  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSurfaceData_t1737958143_m251254851_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSurfaceData_t1737958143_m3549318774_gshared (Il2CppArray * __this, int32_t p0, SurfaceData_t1737958143 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSurfaceData_t1737958143_m3549318774(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, SurfaceData_t1737958143 *, const MethodInfo*))Array_GetGenericValueImpl_TisSurfaceData_t1737958143_m3549318774_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern "C"  TrackableResultData_t395876724  Array_InternalArray__get_Item_TisTrackableResultData_t395876724_m1372804910_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTrackableResultData_t395876724_m1372804910(__this, ___index0, method) ((  TrackableResultData_t395876724  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTrackableResultData_t395876724_m1372804910_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTrackableResultData_t395876724_m3760337259_gshared (Il2CppArray * __this, int32_t p0, TrackableResultData_t395876724 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTrackableResultData_t395876724_m3760337259(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TrackableResultData_t395876724 *, const MethodInfo*))Array_GetGenericValueImpl_TisTrackableResultData_t395876724_m3760337259_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern "C"  VirtualButtonData_t459380335  Array_InternalArray__get_Item_TisVirtualButtonData_t459380335_m3281838419_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVirtualButtonData_t459380335_m3281838419(__this, ___index0, method) ((  VirtualButtonData_t459380335  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVirtualButtonData_t459380335_m3281838419_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVirtualButtonData_t459380335_m881749158_gshared (Il2CppArray * __this, int32_t p0, VirtualButtonData_t459380335 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVirtualButtonData_t459380335_m881749158(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, VirtualButtonData_t459380335 *, const MethodInfo*))Array_GetGenericValueImpl_TisVirtualButtonData_t459380335_m881749158_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern "C"  WordData_t1548845516  Array_InternalArray__get_Item_TisWordData_t1548845516_m2367129074_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordData_t1548845516_m2367129074(__this, ___index0, method) ((  WordData_t1548845516  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordData_t1548845516_m2367129074_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/WordData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisWordData_t1548845516_m1725553269_gshared (Il2CppArray * __this, int32_t p0, WordData_t1548845516 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisWordData_t1548845516_m1725553269(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, WordData_t1548845516 *, const MethodInfo*))Array_GetGenericValueImpl_TisWordData_t1548845516_m1725553269_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern "C"  WordResultData_t1826866697  Array_InternalArray__get_Item_TisWordResultData_t1826866697_m4093502869_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordResultData_t1826866697_m4093502869(__this, ___index0, method) ((  WordResultData_t1826866697  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordResultData_t1826866697_m4093502869_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisWordResultData_t1826866697_m4066890610_gshared (Il2CppArray * __this, int32_t p0, WordResultData_t1826866697 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisWordResultData_t1826866697_m4066890610(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, WordResultData_t1826866697 *, const MethodInfo*))Array_GetGenericValueImpl_TisWordResultData_t1826866697_m4066890610_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern "C"  ProfileData_t1961690790  Array_InternalArray__get_Item_TisProfileData_t1961690790_m2854602072_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisProfileData_t1961690790_m2854602072(__this, ___index0, method) ((  ProfileData_t1961690790  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisProfileData_t1961690790_m2854602072_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.WebCamProfile/ProfileData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisProfileData_t1961690790_m1187019919_gshared (Il2CppArray * __this, int32_t p0, ProfileData_t1961690790 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisProfileData_t1961690790_m1187019919(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ProfileData_t1961690790 *, const MethodInfo*))Array_GetGenericValueImpl_TisProfileData_t1961690790_m1187019919_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ZXing.Aztec.Internal.Decoder/Table>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTable_t1007478513_m745704209_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTable_t1007478513_m745704209(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTable_t1007478513_m745704209_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ZXing.Aztec.Internal.Decoder/Table>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTable_t1007478513_m3283134696_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTable_t1007478513_m3283134696(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTable_t1007478513_m3283134696_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ZXing.BarcodeFormat>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisBarcodeFormat_t4201805817_m2470603185_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBarcodeFormat_t4201805817_m2470603185(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBarcodeFormat_t4201805817_m2470603185_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ZXing.BarcodeFormat>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisBarcodeFormat_t4201805817_m494158870_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisBarcodeFormat_t4201805817_m494158870(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisBarcodeFormat_t4201805817_m494158870_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ZXing.DecodeHintType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisDecodeHintType_t2095781349_m2325404329_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecodeHintType_t2095781349_m2325404329(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecodeHintType_t2095781349_m2325404329_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ZXing.DecodeHintType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDecodeHintType_t2095781349_m1248543760_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDecodeHintType_t2095781349_m1248543760(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisDecodeHintType_t2095781349_m1248543760_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ZXing.EncodeHintType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisEncodeHintType_t3244562957_m725877121_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEncodeHintType_t3244562957_m725877121(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEncodeHintType_t3244562957_m725877121_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ZXing.EncodeHintType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisEncodeHintType_t3244562957_m4095779896_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisEncodeHintType_t3244562957_m4095779896(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisEncodeHintType_t3244562957_m4095779896_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<ZXing.ResultMetadataType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisResultMetadataType_t2923366972_m4183422066_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResultMetadataType_t2923366972_m4183422066(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResultMetadataType_t2923366972_m4183422066_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<ZXing.ResultMetadataType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResultMetadataType_t2923366972_m3705701223_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResultMetadataType_t2923366972_m3705701223(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisResultMetadataType_t2923366972_m3705701223_gshared)(__this, p0, p1, method)
// T System.Threading.Interlocked::CompareExchange<System.Object>(T&,T,T)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___location10, Il2CppObject * ___value1, Il2CppObject * ___comparand2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m1732656505(__this /* static, unused */, ___location10, ___value1, ___comparand2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, ___location10, ___value1, ___comparand2, method)
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m329627835(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m1157584366_gshared (Component_t3501516275 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m1157584366(__this, p0, method) ((  Il2CppObject * (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m1157584366_gshared)(__this, p0, method)
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m1297875695(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t2054899105 * ___data0, const MethodInfo* method);
#define ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176(__this /* static, unused */, ___data0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, BaseEventData_t2054899105 *, const MethodInfo*))ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared)(__this /* static, unused */, ___data0, method)
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m955200616_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m955200616(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m955200616_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2925956997(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m256617435_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m256617435(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m256617435_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t3674682005 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411(__this, p0, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, bool, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared)(__this, p0, method)
// T UnityEngine.GameObject::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInParent_TisIl2CppObject_m4281527380_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentInParent_TisIl2CppObject_m4281527380(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m4281527380_gshared)(__this, method)
// T UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m173634649(__this /* static, unused */, ___json0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, ___json0, method)
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m594707695_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m594707695(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m594707695_gshared)(__this /* static, unused */, method)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, ___original0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, ___original0, method)
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared)(__this /* static, unused */, method)
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method);
#define Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared)(__this /* static, unused */, ___go0, method)
// T Vuforia.IPremiumObjectFactory::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * IPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2962174551_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2962174551(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2962174551_gshared)(__this, method)
// T Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2360300286_gshared (NullPremiumObjectFactory_t3790484746 * __this, const MethodInfo* method);
#define NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2360300286(__this, method) ((  Il2CppObject * (*) (NullPremiumObjectFactory_t3790484746 *, const MethodInfo*))NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2360300286_gshared)(__this, method)
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * SmartTerrainBuilder_CreateReconstruction_TisIl2CppObject_m1171856581_gshared (SmartTerrainBuilder_t3365905913 * __this, const MethodInfo* method);
#define SmartTerrainBuilder_CreateReconstruction_TisIl2CppObject_m1171856581(__this, method) ((  Il2CppObject * (*) (SmartTerrainBuilder_t3365905913 *, const MethodInfo*))SmartTerrainBuilder_CreateReconstruction_TisIl2CppObject_m1171856581_gshared)(__this, method)
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1830079109_gshared (SmartTerrainBuilderImpl_t2057130681 * __this, const MethodInfo* method);
#define SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1830079109(__this, method) ((  Il2CppObject * (*) (SmartTerrainBuilderImpl_t2057130681 *, const MethodInfo*))SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1830079109_gshared)(__this, method)
// T Vuforia.TrackerManager::GetTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManager_GetTracker_TisIl2CppObject_m2486054351_gshared (TrackerManager_t2355365335 * __this, const MethodInfo* method);
#define TrackerManager_GetTracker_TisIl2CppObject_m2486054351(__this, method) ((  Il2CppObject * (*) (TrackerManager_t2355365335 *, const MethodInfo*))TrackerManager_GetTracker_TisIl2CppObject_m2486054351_gshared)(__this, method)
// T Vuforia.TrackerManager::InitTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManager_InitTracker_TisIl2CppObject_m3678094753_gshared (TrackerManager_t2355365335 * __this, const MethodInfo* method);
#define TrackerManager_InitTracker_TisIl2CppObject_m3678094753(__this, method) ((  Il2CppObject * (*) (TrackerManager_t2355365335 *, const MethodInfo*))TrackerManager_InitTracker_TisIl2CppObject_m3678094753_gshared)(__this, method)
// T Vuforia.TrackerManagerImpl::GetTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManagerImpl_GetTracker_TisIl2CppObject_m2502595983_gshared (TrackerManagerImpl_t55743383 * __this, const MethodInfo* method);
#define TrackerManagerImpl_GetTracker_TisIl2CppObject_m2502595983(__this, method) ((  Il2CppObject * (*) (TrackerManagerImpl_t55743383 *, const MethodInfo*))TrackerManagerImpl_GetTracker_TisIl2CppObject_m2502595983_gshared)(__this, method)
// T Vuforia.TrackerManagerImpl::InitTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManagerImpl_InitTracker_TisIl2CppObject_m4190885345_gshared (TrackerManagerImpl_t55743383 * __this, const MethodInfo* method);
#define TrackerManagerImpl_InitTracker_TisIl2CppObject_m4190885345(__this, method) ((  Il2CppObject * (*) (TrackerManagerImpl_t55743383 *, const MethodInfo*))TrackerManagerImpl_InitTracker_TisIl2CppObject_m4190885345_gshared)(__this, method)
// T ZXing.SupportClass::GetValue<System.Boolean>(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,ZXing.DecodeHintType,T)
extern "C"  bool SupportClass_GetValue_TisBoolean_t476798718_m904382831_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___hints0, int32_t ___hintType1, bool ___default2, const MethodInfo* method);
#define SupportClass_GetValue_TisBoolean_t476798718_m904382831(__this /* static, unused */, ___hints0, ___hintType1, ___default2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, bool, const MethodInfo*))SupportClass_GetValue_TisBoolean_t476798718_m904382831_gshared)(__this /* static, unused */, ___hints0, ___hintType1, ___default2, method)
// T ZXing.SupportClass::GetValue<System.Object>(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,ZXing.DecodeHintType,T)
extern "C"  Il2CppObject * SupportClass_GetValue_TisIl2CppObject_m3350635894_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___hints0, int32_t ___hintType1, Il2CppObject * ___default2, const MethodInfo* method);
#define SupportClass_GetValue_TisIl2CppObject_m3350635894(__this /* static, unused */, ___hints0, ___hintType1, ___default2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, Il2CppObject *, const MethodInfo*))SupportClass_GetValue_TisIl2CppObject_m3350635894_gshared)(__this /* static, unused */, ___hints0, ___hintType1, ___default2, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C"  ObjectU5BU5D_t1108656482* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___array0, Predicate_1_t3781873254 * ___match1, const MethodInfo* method);
#define Array_FindAll_TisIl2CppObject_m3670613038(__this /* static, unused */, ___array0, ___match1, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Predicate_1_t3781873254 *, const MethodInfo*))Array_FindAll_TisIl2CppObject_m3670613038_gshared)(__this /* static, unused */, ___array0, ___match1, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m1039640932_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m1039640932(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m1039640932_gshared)(__this /* static, unused */, p0, p1, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t1108656482* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542(__this /* static, unused */, ___values0, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, const MethodInfo*))CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t1983528240* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t3059612989_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t3059612989_m2964992489(__this /* static, unused */, ___values0, method) ((  CustomAttributeNamedArgumentU5BU5D_t1983528240* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t3059612989_m2964992489_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t2088020251* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t3301293422_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t3301293422_m3125716954(__this /* static, unused */, ___values0, method) ((  CustomAttributeTypedArgumentU5BU5D_t2088020251* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t3301293422_m3125716954_gshared)(__this /* static, unused */, ___values0, method)
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponents_TisIl2CppObject_m4290235696_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m4290235696(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m4290235696_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponents_TisIl2CppObject_m1118944320_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m1118944320(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m1118944320_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInChildren_TisIl2CppObject_m3030706502_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3030706502(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3030706502_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared (Component_t3501516275 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m230616125(__this, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_gshared (GameObject_t3674682005 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677(__this, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, bool, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_gshared)(__this, p0, method)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1228840236(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t3501516275 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1225053603(__this, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t3674682005 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637(__this, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, bool, const MethodInfo*))GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared)(__this, p0, method)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258_gshared)(__this, method)
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Object_FindObjectsOfType_TisIl2CppObject_m1311522708_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m1311522708(__this /* static, unused */, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1311522708_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t1108656482* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1015136018* p0, const MethodInfo* method);
#define Resources_ConvertObjects_TisIl2CppObject_m1537961554(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1015136018*, const MethodInfo*))Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared)(__this /* static, unused */, p0, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t1108656482* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___array0, Converter_2_t3715610867 * ___converter1, const MethodInfo* method);
#define Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410(__this /* static, unused */, ___array0, ___converter1, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Converter_2_t3715610867 *, const MethodInfo*))Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared)(__this /* static, unused */, ___array0, ___converter1, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3148122120_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m3148122120(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m3148122120_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3431128615_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t785513668 * ___predicate1, int32_t ___fallback2, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m3431128615(__this /* static, unused */, ___source0, ___predicate1, ___fallback2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t785513668 *, int32_t, const MethodInfo*))Enumerable_First_TisIl2CppObject_m3431128615_gshared)(__this /* static, unused */, ___source0, ___predicate1, ___fallback2, method)
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t785513668 * ___predicate1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016(__this /* static, unused */, ___source0, ___predicate1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t785513668 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared)(__this /* static, unused */, ___source0, ___predicate1, method)
// TSource System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m1543967606_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_Last_TisIl2CppObject_m1543967606(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Last_TisIl2CppObject_m1543967606_gshared)(__this /* static, unused */, ___source0, method)
// TSource System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m3651905832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_Single_TisIl2CppObject_m3651905832(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t3230847821* Enumerable_ToArray_TisInt32_t1153838500_m166960461_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisInt32_t1153838500_m166960461(__this /* static, unused */, ___source0, method) ((  Int32U5BU5D_t3230847821* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisInt32_t1153838500_m166960461_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t1108656482* Enumerable_ToArray_TisIl2CppObject_m3738150552_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3738150552(__this /* static, unused */, ___source0, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3738150552_gshared)(__this /* static, unused */, ___source0, method)
// U System.Linq.Enumerable::Iterate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern "C"  Il2CppObject * Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3128324272_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___initValue1, Func_3_t3956694567 * ___selector2, const MethodInfo* method);
#define Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3128324272(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, Func_3_t3956694567 *, const MethodInfo*))Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3128324272_gshared)(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method)
// U System.Linq.Enumerable::Iterate<System.Single,System.Single>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern "C"  float Enumerable_Iterate_TisSingle_t4291918972_TisSingle_t4291918972_m441885142_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, float ___initValue1, Func_3_t1552338692 * ___selector2, const MethodInfo* method);
#define Enumerable_Iterate_TisSingle_t4291918972_TisSingle_t4291918972_m441885142(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method) ((  float (*) (Il2CppObject * /* static, unused */, Il2CppObject*, float, Func_3_t1552338692 *, const MethodInfo*))Enumerable_Iterate_TisSingle_t4291918972_TisSingle_t4291918972_m441885142_gshared)(__this /* static, unused */, ___source0, ___initValue1, ___selector2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t3674682005 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___root0, BaseEventData_t2054899105 * ___eventData1, EventFunction_1_t864200549 * ___callbackFunction2, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method) ((  GameObject_t3674682005 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, BaseEventData_t2054899105 *, EventFunction_1_t864200549 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared)(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C"  bool ExecuteEvents_Execute_TisIl2CppObject_m1533897725_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * p0, BaseEventData_t2054899105 * p1, EventFunction_1_t864200549 * p2, const MethodInfo* method);
#define ExecuteEvents_Execute_TisIl2CppObject_m1533897725(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, BaseEventData_t2054899105 *, EventFunction_1_t864200549 *, const MethodInfo*))ExecuteEvents_Execute_TisIl2CppObject_m1533897725_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___root0, const MethodInfo* method);
#define ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506(__this /* static, unused */, ___root0, method) ((  GameObject_t3674682005 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared)(__this /* static, unused */, ___root0, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::CanHandleEvent<System.Object>(UnityEngine.GameObject)
extern "C"  bool ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * p0, const MethodInfo* method);
#define ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177_gshared)(__this /* static, unused */, p0, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgument_t3059612989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeNamedArgument_t3059612989 *)(&V_0));
		CustomAttributeNamedArgument_t3059612989  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgument_t3301293422  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeTypedArgument_t3301293422 *)(&V_0));
		CustomAttributeTypedArgument_t3301293422  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_MetadataUsageId;
extern "C"  LabelData_t3207823784  Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LabelData_t3207823784  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelData_t3207823784 *)(&V_0));
		LabelData_t3207823784  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_MetadataUsageId;
extern "C"  LabelFixup_t660379442  Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LabelFixup_t660379442  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelFixup_t660379442 *)(&V_0));
		LabelFixup_t660379442  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_MetadataUsageId;
extern "C"  ILTokenInfo_t1354080954  Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ILTokenInfo_t1354080954  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ILTokenInfo_t1354080954 *)(&V_0));
		ILTokenInfo_t1354080954  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLabel_t2268465130_m1753959877_MetadataUsageId;
extern "C"  Label_t2268465130  Array_InternalArray__get_Item_TisLabel_t2268465130_m1753959877_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabel_t2268465130_m1753959877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Label_t2268465130  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Label_t2268465130 *)(&V_0));
		Label_t2268465130  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisMonoResource_t1505432149_m358920598_MetadataUsageId;
extern "C"  MonoResource_t1505432149  Array_InternalArray__get_Item_TisMonoResource_t1505432149_m358920598_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMonoResource_t1505432149_m358920598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MonoResource_t1505432149  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (MonoResource_t1505432149 *)(&V_0));
		MonoResource_t1505432149  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3880501745_m113952890_MetadataUsageId;
extern "C"  RefEmitPermissionSet_t3880501745  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3880501745_m113952890_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3880501745_m113952890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RefEmitPermissionSet_t3880501745  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RefEmitPermissionSet_t3880501745 *)(&V_0));
		RefEmitPermissionSet_t3880501745  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_MetadataUsageId;
extern "C"  ParameterModifier_t741930026  Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParameterModifier_t741930026  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ParameterModifier_t741930026 *)(&V_0));
		ParameterModifier_t741930026  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_MetadataUsageId;
extern "C"  ResourceCacheItem_t2113902833  Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResourceCacheItem_t2113902833  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceCacheItem_t2113902833 *)(&V_0));
		ResourceCacheItem_t2113902833  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_MetadataUsageId;
extern "C"  ResourceInfo_t4013605874  Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResourceInfo_t4013605874  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceInfo_t4013605874 *)(&V_0));
		ResourceInfo_t4013605874  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_MetadataUsageId;
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_MetadataUsageId;
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_MetadataUsageId;
extern "C"  X509ChainStatus_t766901931  Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	X509ChainStatus_t766901931  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (X509ChainStatus_t766901931 *)(&V_0));
		X509ChainStatus_t766901931  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_MetadataUsageId;
extern "C"  float Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (float*)(&V_0));
		float L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTermInfoStrings_t2002624446_m1248113113_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t2002624446_m1248113113_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTermInfoStrings_t2002624446_m1248113113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_MetadataUsageId;
extern "C"  Mark_t3811539797  Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mark_t3811539797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Mark_t3811539797 *)(&V_0));
		Mark_t3811539797  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_MetadataUsageId;
extern "C"  TimeSpan_t413522987  Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t413522987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeSpan_t413522987 *)(&V_0));
		TimeSpan_t413522987  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_MetadataUsageId;
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_MetadataUsageId;
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint32_t*)(&V_0));
		uint32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_MetadataUsageId;
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint64_t*)(&V_0));
		uint64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_MetadataUsageId;
extern "C"  UriScheme_t1290668975  Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UriScheme_t1290668975  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t1290668975 *)(&V_0));
		UriScheme_t1290668975  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_MetadataUsageId;
extern "C"  NsDecl_t3658211563  Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NsDecl_t3658211563  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsDecl_t3658211563 *)(&V_0));
		NsDecl_t3658211563  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_MetadataUsageId;
extern "C"  NsScope_t1749213747  Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NsScope_t1749213747  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsScope_t1749213747 *)(&V_0));
		NsScope_t1749213747  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_MetadataUsageId;
extern "C"  Color_t4194546905  Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color_t4194546905 *)(&V_0));
		Color_t4194546905  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_MetadataUsageId;
extern "C"  Color32_t598853688  Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color32_t598853688  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color32_t598853688 *)(&V_0));
		Color32_t598853688  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_MetadataUsageId;
extern "C"  ContactPoint_t243083348  Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint_t243083348  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint_t243083348 *)(&V_0));
		ContactPoint_t243083348  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_MetadataUsageId;
extern "C"  ContactPoint2D_t4288432358  Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint2D_t4288432358  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint2D_t4288432358 *)(&V_0));
		ContactPoint2D_t4288432358  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_MetadataUsageId;
extern "C"  RaycastResult_t3762661364  Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastResult_t3762661364  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastResult_t3762661364 *)(&V_0));
		RaycastResult_t3762661364  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.jvalue>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_Tisjvalue_t3862675307_m2663906116_MetadataUsageId;
extern "C"  jvalue_t3862675307  Array_InternalArray__get_Item_Tisjvalue_t3862675307_m2663906116_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_Tisjvalue_t3862675307_m2663906116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	jvalue_t3862675307  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (jvalue_t3862675307 *)(&V_0));
		jvalue_t3862675307  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_MetadataUsageId;
extern "C"  Keyframe_t4079056114  Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Keyframe_t4079056114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Keyframe_t4079056114 *)(&V_0));
		Keyframe_t4079056114  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.NetworkPlayer>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_MetadataUsageId;
extern "C"  NetworkPlayer_t3231273765  Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NetworkPlayer_t3231273765 *)(&V_0));
		NetworkPlayer_t3231273765  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.NetworkReachability>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisNetworkReachability_t612403035_m3947396976_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisNetworkReachability_t612403035_m3947396976_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNetworkReachability_t612403035_m3947396976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ParticleCollisionEvent>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisParticleCollisionEvent_t2700926194_m2185485021_MetadataUsageId;
extern "C"  ParticleCollisionEvent_t2700926194  Array_InternalArray__get_Item_TisParticleCollisionEvent_t2700926194_m2185485021_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisParticleCollisionEvent_t2700926194_m2185485021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParticleCollisionEvent_t2700926194  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ParticleCollisionEvent_t2700926194 *)(&V_0));
		ParticleCollisionEvent_t2700926194  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Quaternion>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_MetadataUsageId;
extern "C"  Quaternion_t1553702882  Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Quaternion_t1553702882 *)(&V_0));
		Quaternion_t1553702882  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_MetadataUsageId;
extern "C"  RaycastHit_t4003175726  Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit_t4003175726 *)(&V_0));
		RaycastHit_t4003175726  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit2D_t1374744384 *)(&V_0));
		RaycastHit2D_t1374744384  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Rect>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_MetadataUsageId;
extern "C"  Rect_t4241904616  Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Rect_t4241904616 *)(&V_0));
		Rect_t4241904616  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_MetadataUsageId;
extern "C"  HitInfo_t3209134097  Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t3209134097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (HitInfo_t3209134097 *)(&V_0));
		HitInfo_t3209134097  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_MetadataUsageId;
extern "C"  GcAchievementData_t3481375915  Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcAchievementData_t3481375915  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcAchievementData_t3481375915 *)(&V_0));
		GcAchievementData_t3481375915  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_MetadataUsageId;
extern "C"  GcScoreData_t2181296590  Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcScoreData_t2181296590  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcScoreData_t2181296590 *)(&V_0));
		GcScoreData_t2181296590  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Touch>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTouch_t4210255029_m2563763030_MetadataUsageId;
extern "C"  Touch_t4210255029  Array_InternalArray__get_Item_TisTouch_t4210255029_m2563763030_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTouch_t4210255029_m2563763030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t4210255029  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Touch_t4210255029 *)(&V_0));
		Touch_t4210255029  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContentType_t2662964855_m696167015_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t2662964855_m696167015_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContentType_t2662964855_m696167015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUICharInfo_t65807484_m2153093395_MetadataUsageId;
extern "C"  UICharInfo_t65807484  Array_InternalArray__get_Item_TisUICharInfo_t65807484_m2153093395_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUICharInfo_t65807484_m2153093395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UICharInfo_t65807484  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UICharInfo_t65807484 *)(&V_0));
		UICharInfo_t65807484  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUILineInfo_t4113875482_m2200325045_MetadataUsageId;
extern "C"  UILineInfo_t4113875482  Array_InternalArray__get_Item_TisUILineInfo_t4113875482_m2200325045_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUILineInfo_t4113875482_m2200325045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UILineInfo_t4113875482  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UILineInfo_t4113875482 *)(&V_0));
		UILineInfo_t4113875482  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUIVertex_t4244065212_m2640447059_MetadataUsageId;
extern "C"  UIVertex_t4244065212  Array_InternalArray__get_Item_TisUIVertex_t4244065212_m2640447059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUIVertex_t4244065212_m2640447059_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t4244065212  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UIVertex_t4244065212 *)(&V_0));
		UIVertex_t4244065212  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector2_t4282066565_m1844444166_MetadataUsageId;
extern "C"  Vector2_t4282066565  Array_InternalArray__get_Item_TisVector2_t4282066565_m1844444166_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector2_t4282066565_m1844444166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector2_t4282066565 *)(&V_0));
		Vector2_t4282066565  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector3_t4282066566_m1333909989_MetadataUsageId;
extern "C"  Vector3_t4282066566  Array_InternalArray__get_Item_TisVector3_t4282066566_m1333909989_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3_t4282066566_m1333909989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3_t4282066566 *)(&V_0));
		Vector3_t4282066566  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector4_t4282066567_m823375812_MetadataUsageId;
extern "C"  Vector4_t4282066567  Array_InternalArray__get_Item_TisVector4_t4282066567_m823375812_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector4_t4282066567_m823375812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector4_t4282066567 *)(&V_0));
		Vector4_t4282066567  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisWebCamDevice_t3274004757_m3923092186_MetadataUsageId;
extern "C"  WebCamDevice_t3274004757  Array_InternalArray__get_Item_TisWebCamDevice_t3274004757_m3923092186_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWebCamDevice_t3274004757_m3923092186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebCamDevice_t3274004757  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WebCamDevice_t3274004757 *)(&V_0));
		WebCamDevice_t3274004757  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.Eyewear/EyewearCalibrationReading>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3010462567_m3646914679_MetadataUsageId;
extern "C"  EyewearCalibrationReading_t3010462567  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3010462567_m3646914679_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3010462567_m3646914679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EyewearCalibrationReading_t3010462567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (EyewearCalibrationReading_t3010462567 *)(&V_0));
		EyewearCalibrationReading_t3010462567  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t354375056_m2801720466_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t354375056_m2801720466_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPIXEL_FORMAT_t354375056_m2801720466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRectangleData_t2265684451_m28733777_MetadataUsageId;
extern "C"  RectangleData_t2265684451  Array_InternalArray__get_Item_TisRectangleData_t2265684451_m28733777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRectangleData_t2265684451_m28733777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectangleData_t2265684451  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RectangleData_t2265684451 *)(&V_0));
		RectangleData_t2265684451  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTargetSearchResult_t3609410114_m610339772_MetadataUsageId;
extern "C"  TargetSearchResult_t3609410114  Array_InternalArray__get_Item_TisTargetSearchResult_t3609410114_m610339772_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTargetSearchResult_t3609410114_m610339772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TargetSearchResult_t3609410114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TargetSearchResult_t3609410114 *)(&V_0));
		TargetSearchResult_t3609410114  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisPropData_t526813605_m3474501881_MetadataUsageId;
extern "C"  PropData_t526813605  Array_InternalArray__get_Item_TisPropData_t526813605_m3474501881_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPropData_t526813605_m3474501881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PropData_t526813605  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (PropData_t526813605 *)(&V_0));
		PropData_t526813605  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t3919795561_m2436319413_MetadataUsageId;
extern "C"  SmartTerrainRevisionData_t3919795561  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t3919795561_m2436319413_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t3919795561_m2436319413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainRevisionData_t3919795561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SmartTerrainRevisionData_t3919795561 *)(&V_0));
		SmartTerrainRevisionData_t3919795561  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSurfaceData_t1737958143_m251254851_MetadataUsageId;
extern "C"  SurfaceData_t1737958143  Array_InternalArray__get_Item_TisSurfaceData_t1737958143_m251254851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSurfaceData_t1737958143_m251254851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SurfaceData_t1737958143  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SurfaceData_t1737958143 *)(&V_0));
		SurfaceData_t1737958143  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTrackableResultData_t395876724_m1372804910_MetadataUsageId;
extern "C"  TrackableResultData_t395876724  Array_InternalArray__get_Item_TisTrackableResultData_t395876724_m1372804910_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTrackableResultData_t395876724_m1372804910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TrackableResultData_t395876724  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TrackableResultData_t395876724 *)(&V_0));
		TrackableResultData_t395876724  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVirtualButtonData_t459380335_m3281838419_MetadataUsageId;
extern "C"  VirtualButtonData_t459380335  Array_InternalArray__get_Item_TisVirtualButtonData_t459380335_m3281838419_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVirtualButtonData_t459380335_m3281838419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VirtualButtonData_t459380335  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (VirtualButtonData_t459380335 *)(&V_0));
		VirtualButtonData_t459380335  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisWordData_t1548845516_m2367129074_MetadataUsageId;
extern "C"  WordData_t1548845516  Array_InternalArray__get_Item_TisWordData_t1548845516_m2367129074_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWordData_t1548845516_m2367129074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WordData_t1548845516  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WordData_t1548845516 *)(&V_0));
		WordData_t1548845516  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisWordResultData_t1826866697_m4093502869_MetadataUsageId;
extern "C"  WordResultData_t1826866697  Array_InternalArray__get_Item_TisWordResultData_t1826866697_m4093502869_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWordResultData_t1826866697_m4093502869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WordResultData_t1826866697  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WordResultData_t1826866697 *)(&V_0));
		WordResultData_t1826866697  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisProfileData_t1961690790_m2854602072_MetadataUsageId;
extern "C"  ProfileData_t1961690790  Array_InternalArray__get_Item_TisProfileData_t1961690790_m2854602072_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisProfileData_t1961690790_m2854602072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ProfileData_t1961690790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ProfileData_t1961690790 *)(&V_0));
		ProfileData_t1961690790  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ZXing.Aztec.Internal.Decoder/Table>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTable_t1007478513_m745704209_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTable_t1007478513_m745704209_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTable_t1007478513_m745704209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ZXing.BarcodeFormat>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisBarcodeFormat_t4201805817_m2470603185_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisBarcodeFormat_t4201805817_m2470603185_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBarcodeFormat_t4201805817_m2470603185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ZXing.DecodeHintType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisDecodeHintType_t2095781349_m2325404329_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisDecodeHintType_t2095781349_m2325404329_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDecodeHintType_t2095781349_m2325404329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ZXing.EncodeHintType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisEncodeHintType_t3244562957_m725877121_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisEncodeHintType_t3244562957_m725877121_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisEncodeHintType_t3244562957_m725877121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<ZXing.ResultMetadataType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisResultMetadataType_t2923366972_m4183422066_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisResultMetadataType_t2923366972_m4183422066_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResultMetadataType_t2923366972_m4183422066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Component::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t1152456458_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t1152456458  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t1152456458_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((Component_t3501516275 *)__this);
		Component_GetComponentFastPath_m1455568887((Component_t3501516275 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m329627835_gshared (Component_t3501516275 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((Component_t3501516275 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Component_t3501516275 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t3501516275 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInChildren_TisIl2CppObject_m1157584366_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m1157584366_gshared (Component_t3501516275 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInChildren_TisIl2CppObject_m1157584366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((Component_t3501516275 *)__this);
		Component_t3501516275 * L_2 = Component_GetComponentInChildren_m1899663946((Component_t3501516275 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInParent_TisIl2CppObject_m1297875695_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t3501516275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInParent_TisIl2CppObject_m1297875695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Component_t3501516275 *)__this);
		Component_t3501516275 * L_1 = Component_GetComponentInParent_m1953645192((Component_t3501516275 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1477400244;
extern const uint32_t ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_MetadataUsageId;
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t2054899105 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseEventData_t2054899105 * L_0 = ___data0;
		if (((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003a;
		}
	}
	{
		BaseEventData_t2054899105 * L_1 = ___data0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m2022236990((Il2CppObject *)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral1477400244, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_5 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003a:
	{
		BaseEventData_t2054899105 * L_6 = ___data0;
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_AddComponent_TisIl2CppObject_m955200616_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m955200616_gshared (GameObject_t3674682005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_AddComponent_TisIl2CppObject_m955200616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)__this);
		Component_t3501516275 * L_1 = GameObject_AddComponent_m2208780168((GameObject_t3674682005 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t1152456458_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponent_TisIl2CppObject_m2925956997_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared (GameObject_t3674682005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponent_TisIl2CppObject_m2925956997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t1152456458  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t1152456458_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)__this);
		GameObject_GetComponentFastPath_m2905716663((GameObject_t3674682005 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m256617435_gshared (GameObject_t3674682005 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((GameObject_t3674682005 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t3674682005 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t3674682005 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t3674682005 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t3674682005 *)__this);
		Component_t3501516275 * L_2 = GameObject_GetComponentInChildren_m1490154500((GameObject_t3674682005 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.GameObject::GetComponentInParent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentInParent_TisIl2CppObject_m4281527380_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponentInParent_TisIl2CppObject_m4281527380_gshared (GameObject_t3674682005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInParent_TisIl2CppObject_m4281527380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)__this);
		Component_t3501516275 * L_1 = GameObject_GetComponentInParent_m434474382((GameObject_t3674682005 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonUtility_FromJson_TisIl2CppObject_m173634649_MetadataUsageId;
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonUtility_FromJson_TisIl2CppObject_m173634649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_2 = JsonUtility_FromJson_m3245538665(NULL /*static, unused*/, (String_t*)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_TisIl2CppObject_m594707695_MetadataUsageId;
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m594707695_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_TisIl2CppObject_m594707695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m3133387403_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m3133387403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3473406, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___original0;
		Object_t3071478659 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, (Object_t3071478659 *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_MetadataUsageId;
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ScriptableObject_t2970544072 * L_1 = ScriptableObject_CreateInstance_m3255479417(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___go0;
		NullCheck((GameObject_t3674682005 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t3674682005 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, (Object_t3071478659 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		GameObject_t3674682005 * L_4 = ___go0;
		NullCheck((GameObject_t3674682005 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((GameObject_t3674682005 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001e:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::CreateReconstruction<System.Object>()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2360300286_MetadataUsageId;
extern "C"  Il2CppObject * NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2360300286_gshared (NullPremiumObjectFactory_t3790484746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2360300286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<System.Object>()
extern const Il2CppType* ReconstructionFromTarget_t41970945_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t2202637939_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2793608676_il2cpp_TypeInfo_var;
extern Il2CppClass* ReconstructionFromTargetImpl_t3107149249_il2cpp_TypeInfo_var;
extern const uint32_t SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1830079109_MetadataUsageId;
extern "C"  Il2CppObject * SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1830079109_gshared (SmartTerrainBuilderImpl_t2057130681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1830079109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(ReconstructionFromTarget_t41970945_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t2202637939_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m2545616589(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		IntPtr_t L_3 = InterfaceFuncInvoker0< IntPtr_t >::Invoke(78 /* System.IntPtr Vuforia.IVuforiaWrapper::SmartTerrainBuilderCreateReconstructionFromTarget() */, IVuforiaWrapper_t2793608676_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		ReconstructionFromTargetImpl_t3107149249 * L_4 = (ReconstructionFromTargetImpl_t3107149249 *)il2cpp_codegen_object_new(ReconstructionFromTargetImpl_t3107149249_il2cpp_TypeInfo_var);
		ReconstructionFromTargetImpl__ctor_m616861192(L_4, (IntPtr_t)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0030:
	{
		Il2CppObject * L_5 = PremiumObjectFactory_get_Instance_m1591768685(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2), (Il2CppObject *)L_5);
		return L_6;
	}
}
// T Vuforia.TrackerManagerImpl::GetTracker<System.Object>()
extern const Il2CppType* ObjectTracker_t455954211_0_0_0_var;
extern const Il2CppType* MarkerTracker_t4028259784_0_0_0_var;
extern const Il2CppType* TextTracker_t3721656949_0_0_0_var;
extern const Il2CppType* SmartTerrainTracker_t2067565974_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2887163856;
extern const uint32_t TrackerManagerImpl_GetTracker_TisIl2CppObject_m2502595983_MetadataUsageId;
extern "C"  Il2CppObject * TrackerManagerImpl_GetTracker_TisIl2CppObject_m2502595983_gshared (TrackerManagerImpl_t55743383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackerManagerImpl_GetTracker_TisIl2CppObject_m2502595983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(ObjectTracker_t455954211_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t455954211 * L_2 = (ObjectTracker_t455954211 *)__this->get_mObjectTracker_1();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(MarkerTracker_t4028259784_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_004e;
		}
	}
	{
		MarkerTracker_t4028259784 * L_5 = (MarkerTracker_t4028259784 *)__this->get_mMarkerTracker_2();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(TextTracker_t3721656949_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_0075;
		}
	}
	{
		TextTracker_t3721656949 * L_8 = (TextTracker_t3721656949 *)__this->get_mTextTracker_3();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(SmartTerrainTracker_t2067565974_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_009c;
		}
	}
	{
		SmartTerrainTracker_t2067565974 * L_11 = (SmartTerrainTracker_t2067565974 *)__this->get_mSmartTerrainTracker_4();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_11, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2887163856, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_12 = V_0;
		return L_12;
	}
}
// T Vuforia.TrackerManagerImpl::InitTracker<System.Object>()
extern const Il2CppType* ObjectTracker_t455954211_0_0_0_var;
extern const Il2CppType* MarkerTracker_t4028259784_0_0_0_var;
extern const Il2CppType* TextTracker_t3721656949_0_0_0_var;
extern const Il2CppType* SmartTerrainTracker_t2067565974_0_0_0_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t2202637939_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeMapping_t2898721118_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2793608676_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectTrackerImpl_t243160803_il2cpp_TypeInfo_var;
extern Il2CppClass* MarkerTrackerImpl_t1934199560_il2cpp_TypeInfo_var;
extern Il2CppClass* TextTrackerImpl_t2086340917_il2cpp_TypeInfo_var;
extern Il2CppClass* SmartTerrainTrackerImpl_t1058218966_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2444913673;
extern Il2CppCodeGenString* _stringLiteral1568033648;
extern const uint32_t TrackerManagerImpl_InitTracker_TisIl2CppObject_m4190885345_MetadataUsageId;
extern "C"  Il2CppObject * TrackerManagerImpl_InitTracker_TisIl2CppObject_m4190885345_gshared (TrackerManagerImpl_t55743383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackerManagerImpl_InitTracker_TisIl2CppObject_m4190885345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1069467038_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3074936826(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t2202637939_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m2545616589(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t2898721118_il2cpp_TypeInfo_var);
		uint16_t L_4 = TypeMapping_GetTypeID_m510557172(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		int32_t L_5 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(123 /* System.Int32 Vuforia.IVuforiaWrapper::TrackerManagerInitTracker(System.Int32) */, IVuforiaWrapper_t2793608676_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_4);
		if (L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2444913673, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_6 = V_1;
		return L_6;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(ObjectTracker_t455954211_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_7) == ((Il2CppObject*)(Type_t *)L_8))))
		{
			goto IL_007a;
		}
	}
	{
		ObjectTracker_t455954211 * L_9 = (ObjectTracker_t455954211 *)__this->get_mObjectTracker_1();
		if (L_9)
		{
			goto IL_0069;
		}
	}
	{
		ObjectTrackerImpl_t243160803 * L_10 = (ObjectTrackerImpl_t243160803 *)il2cpp_codegen_object_new(ObjectTrackerImpl_t243160803_il2cpp_TypeInfo_var);
		ObjectTrackerImpl__ctor_m3044980802(L_10, /*hidden argument*/NULL);
		__this->set_mObjectTracker_1(L_10);
	}

IL_0069:
	{
		ObjectTracker_t455954211 * L_11 = (ObjectTracker_t455954211 *)__this->get_mObjectTracker_1();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_11, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(MarkerTracker_t4028259784_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13))))
		{
			goto IL_00b4;
		}
	}
	{
		MarkerTracker_t4028259784 * L_14 = (MarkerTracker_t4028259784 *)__this->get_mMarkerTracker_2();
		if (L_14)
		{
			goto IL_00a3;
		}
	}
	{
		MarkerTrackerImpl_t1934199560 * L_15 = (MarkerTrackerImpl_t1934199560 *)il2cpp_codegen_object_new(MarkerTrackerImpl_t1934199560_il2cpp_TypeInfo_var);
		MarkerTrackerImpl__ctor_m3198901693(L_15, /*hidden argument*/NULL);
		__this->set_mMarkerTracker_2(L_15);
	}

IL_00a3:
	{
		MarkerTracker_t4028259784 * L_16 = (MarkerTracker_t4028259784 *)__this->get_mMarkerTracker_2();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_16, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(TextTracker_t3721656949_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18))))
		{
			goto IL_00ee;
		}
	}
	{
		TextTracker_t3721656949 * L_19 = (TextTracker_t3721656949 *)__this->get_mTextTracker_3();
		if (L_19)
		{
			goto IL_00dd;
		}
	}
	{
		TextTrackerImpl_t2086340917 * L_20 = (TextTrackerImpl_t2086340917 *)il2cpp_codegen_object_new(TextTrackerImpl_t2086340917_il2cpp_TypeInfo_var);
		TextTrackerImpl__ctor_m2680704304(L_20, /*hidden argument*/NULL);
		__this->set_mTextTracker_3(L_20);
	}

IL_00dd:
	{
		TextTracker_t3721656949 * L_21 = (TextTracker_t3721656949 *)__this->get_mTextTracker_3();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_21, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(SmartTerrainTracker_t2067565974_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_22) == ((Il2CppObject*)(Type_t *)L_23))))
		{
			goto IL_0128;
		}
	}
	{
		SmartTerrainTracker_t2067565974 * L_24 = (SmartTerrainTracker_t2067565974 *)__this->get_mSmartTerrainTracker_4();
		if (L_24)
		{
			goto IL_0117;
		}
	}
	{
		SmartTerrainTrackerImpl_t1058218966 * L_25 = (SmartTerrainTrackerImpl_t1058218966 *)il2cpp_codegen_object_new(SmartTerrainTrackerImpl_t1058218966_il2cpp_TypeInfo_var);
		SmartTerrainTrackerImpl__ctor_m3418811311(L_25, /*hidden argument*/NULL);
		__this->set_mSmartTerrainTracker_4(L_25);
	}

IL_0117:
	{
		SmartTerrainTracker_t2067565974 * L_26 = (SmartTerrainTracker_t2067565974 *)__this->get_mSmartTerrainTracker_4();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_26, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0128:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1568033648, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_27 = V_2;
		return L_27;
	}
}
// T ZXing.SupportClass::GetValue<System.Boolean>(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,ZXing.DecodeHintType,T)
extern Il2CppClass* IDictionary_2_t759989846_il2cpp_TypeInfo_var;
extern const uint32_t SupportClass_GetValue_TisBoolean_t476798718_m904382831_MetadataUsageId;
extern "C"  bool SupportClass_GetValue_TisBoolean_t476798718_m904382831_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___hints0, int32_t ___hintType1, bool ___default2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SupportClass_GetValue_TisBoolean_t476798718_m904382831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___hints0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		bool L_1 = ___default2;
		return L_1;
	}

IL_0005:
	{
		Il2CppObject* L_2 = ___hints0;
		int32_t L_3 = ___hintType1;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(!0) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_2, (int32_t)L_3);
		if (L_4)
		{
			goto IL_0010;
		}
	}
	{
		bool L_5 = ___default2;
		return L_5;
	}

IL_0010:
	{
		Il2CppObject* L_6 = ___hints0;
		int32_t L_7 = ___hintType1;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::get_Item(!0) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_6, (int32_t)L_7);
		return ((*(bool*)((bool*)UnBox (L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))));
	}
}
// T ZXing.SupportClass::GetValue<System.Object>(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,ZXing.DecodeHintType,T)
extern Il2CppClass* IDictionary_2_t759989846_il2cpp_TypeInfo_var;
extern const uint32_t SupportClass_GetValue_TisIl2CppObject_m3350635894_MetadataUsageId;
extern "C"  Il2CppObject * SupportClass_GetValue_TisIl2CppObject_m3350635894_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___hints0, int32_t ___hintType1, Il2CppObject * ___default2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SupportClass_GetValue_TisIl2CppObject_m3350635894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___hints0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		Il2CppObject * L_1 = ___default2;
		return L_1;
	}

IL_0005:
	{
		Il2CppObject* L_2 = ___hints0;
		int32_t L_3 = ___hintType1;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(!0) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_2, (int32_t)L_3);
		if (L_4)
		{
			goto IL_0010;
		}
	}
	{
		Il2CppObject * L_5 = ___default2;
		return L_5;
	}

IL_0010:
	{
		Il2CppObject* L_6 = ___hints0;
		int32_t L_7 = ___hintType1;
		NullCheck((Il2CppObject*)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::get_Item(!0) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_6, (int32_t)L_7);
		return ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral103668165;
extern const uint32_t Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1108656482* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___array0, Predicate_1_t3781873254 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t1108656482* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t1108656482* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t3781873254 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral103668165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t1108656482* L_4 = ___array0;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ObjectU5BU5D_t1108656482* L_5 = ___array0;
		V_3 = (ObjectU5BU5D_t1108656482*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t1108656482* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_2 = (Il2CppObject *)L_9;
		Predicate_1_t3781873254 * L_10 = ___match1;
		Il2CppObject * L_11 = V_2;
		NullCheck((Predicate_1_t3781873254 *)L_10);
		bool L_12 = ((  bool (*) (Predicate_1_t3781873254 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Predicate_1_t3781873254 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_13 = V_1;
		int32_t L_14 = V_0;
		int32_t L_15 = (int32_t)L_14;
		V_0 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		Il2CppObject * L_16 = V_2;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_16);
	}

IL_0058:
	{
		int32_t L_17 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_18 = V_4;
		ObjectU5BU5D_t1108656482* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_20 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482**)(&V_1), (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_21 = V_1;
		return L_21;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t1108656482* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method)
{
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t1108656482* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t1108656482* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_10 = V_0;
		return L_10;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t1983528240* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t3059612989_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t1983528240* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)((CustomAttributeNamedArgumentU5BU5D_t1983528240*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t1108656482* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t1108656482* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_10 = V_0;
		return L_10;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t2088020251* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t3301293422_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2088020251* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)((CustomAttributeTypedArgumentU5BU5D_t2088020251*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t1108656482* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t1108656482* L_9 = ___values0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_10 = V_0;
		return L_10;
	}
}
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponents_TisIl2CppObject_m4290235696_gshared (Component_t3501516275 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t3501516275 *)__this);
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899((Component_t3501516275 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)L_0);
		ObjectU5BU5D_t1108656482* L_1 = ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t3674682005 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInChildren_TisIl2CppObject_m3030706502_gshared (Component_t3501516275 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t3501516275 *)__this);
		ObjectU5BU5D_t1108656482* L_0 = ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t3501516275 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInChildren_TisIl2CppObject_m230616125_gshared (Component_t3501516275 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	{
		NullCheck((Component_t3501516275 *)__this);
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899((Component_t3501516275 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t3674682005 *)L_0);
		ObjectU5BU5D_t1108656482* L_2 = ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t3674682005 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t3501516275 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t3501516275 *)__this);
		ObjectU5BU5D_t1108656482* L_0 = ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((Component_t3501516275 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t3501516275 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	{
		NullCheck((Component_t3501516275 *)__this);
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899((Component_t3501516275 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t3674682005 *)L_0);
		ObjectU5BU5D_t1108656482* L_2 = ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t3674682005 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m1118944320_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponents_TisIl2CppObject_m1118944320_gshared (GameObject_t3674682005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m1118944320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m181453881((GameObject_t3674682005 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t1108656482*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInChildren_TisIl2CppObject_m1339828258_gshared (GameObject_t3674682005 * __this, const MethodInfo* method)
{
	{
		NullCheck((GameObject_t3674682005 *)__this);
		ObjectU5BU5D_t1108656482* L_0 = ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((GameObject_t3674682005 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_gshared (GameObject_t3674682005 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t3674682005 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t3674682005 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t1108656482*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t3674682005 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t3674682005 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t3674682005 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t1108656482*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m1311522708_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1108656482* Object_FindObjectsOfType_TisIl2CppObject_m1311522708_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m1311522708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ObjectU5BU5D_t1015136018* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_2 = ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1015136018*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1015136018*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t1108656482* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1015136018* ___rawObjects0, const MethodInfo* method)
{
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t1015136018* L_0 = ___rawObjects0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t1108656482*)NULL;
	}

IL_0008:
	{
		ObjectU5BU5D_t1015136018* L_1 = ___rawObjects0;
		NullCheck(L_1);
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_002b;
	}

IL_0018:
	{
		ObjectU5BU5D_t1108656482* L_2 = V_0;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t1015136018* L_4 = ___rawObjects0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Object_t3071478659 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_9 = V_1;
		ObjectU5BU5D_t1108656482* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_11 = V_0;
		return L_11;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral3945236896;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1108656482* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___array0, Converter_2_t3715610867 * ___converter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t3715610867 * L_2 = ___converter1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral3945236896, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t1108656482* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t1108656482* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t3715610867 * L_7 = ___converter1;
		ObjectU5BU5D_t1108656482* L_8 = ___array0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck((Converter_2_t3715610867 *)L_7);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (Converter_2_t3715610867 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((Converter_2_t3715610867 *)L_7, (Il2CppObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_14 = V_1;
		ObjectU5BU5D_t1108656482* L_15 = ___array0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_16 = V_0;
		return L_16;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m3148122120_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3148122120_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m3148122120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source0;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005c:
	{
		InvalidOperationException_t1589641621 * L_16 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		Il2CppObject * L_17 = V_2;
		return L_17;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m3431128615_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3431128615_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t785513668 * ___predicate1, int32_t ___fallback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m3431128615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Func_2_t785513668 * L_4 = ___predicate1;
			Il2CppObject * L_5 = V_0;
			NullCheck((Func_2_t785513668 *)L_4);
			bool L_6 = ((  bool (*) (Func_2_t785513668 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_2_t785513668 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			if (!L_6)
			{
				goto IL_0026;
			}
		}

IL_001f:
		{
			Il2CppObject * L_7 = V_0;
			V_2 = (Il2CppObject *)L_7;
			IL2CPP_LEAVE(0x58, FINALLY_0036);
		}

IL_0026:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000c;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			if (L_10)
			{
				goto IL_003a;
			}
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(54)
		}

IL_003a:
		{
			Il2CppObject* L_11 = V_1;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(54)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0041:
	{
		int32_t L_12 = ___fallback2;
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_13 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_004e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_14 = V_3;
		return L_14;
	}

IL_0058:
	{
		Il2CppObject * L_15 = V_2;
		return L_15;
	}
}
// TSource System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2050003016_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Func_2_t785513668 * ___predicate1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___source0;
		Func_2_t785513668 * L_1 = ___predicate1;
		Check_SourceAndPredicate_m2252398949(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		Il2CppObject* L_2 = ___source0;
		Func_2_t785513668 * L_3 = ___predicate1;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t785513668 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Func_2_t785513668 *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// TSource System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Last_TisIl2CppObject_m1543967606_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m1543967606_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Last_TisIl2CppObject_m1543967606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0024:
	{
		Il2CppObject* L_6 = ___source0;
		V_1 = (Il2CppObject*)((Il2CppObject*)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		Il2CppObject* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject* L_8 = V_1;
		Il2CppObject* L_9 = V_1;
		NullCheck((Il2CppObject*)L_9);
		int32_t L_10 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_9);
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_11 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_8, (int32_t)((int32_t)((int32_t)L_10-(int32_t)1)));
		return L_11;
	}

IL_0040:
	{
		V_2 = (bool)1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_6));
		Il2CppObject * L_12 = V_6;
		V_3 = (Il2CppObject *)L_12;
		Il2CppObject* L_13 = ___source0;
		NullCheck((Il2CppObject*)L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_13);
		V_5 = (Il2CppObject*)L_14;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_005a:
		{
			Il2CppObject* L_15 = V_5;
			NullCheck((Il2CppObject*)L_15);
			Il2CppObject * L_16 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_15);
			V_4 = (Il2CppObject *)L_16;
			Il2CppObject * L_17 = V_4;
			V_3 = (Il2CppObject *)L_17;
			V_2 = (bool)0;
		}

IL_0068:
		{
			Il2CppObject* L_18 = V_5;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_005a;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_20 = V_5;
			if (L_20)
			{
				goto IL_007e;
			}
		}

IL_007d:
		{
			IL2CPP_END_FINALLY(121)
		}

IL_007e:
		{
			Il2CppObject* L_21 = V_5;
			NullCheck((Il2CppObject *)L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_21);
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0086:
	{
		bool L_22 = V_2;
		if (L_22)
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_23 = V_3;
		return L_23;
	}

IL_008e:
	{
		InvalidOperationException_t1589641621 * L_24 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}
}
// TSource System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Single_TisIl2CppObject_m3651905832_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m3651905832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Single_TisIl2CppObject_m3651905832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		V_0 = (bool)0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_1 = V_4;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject* L_2 = ___source0;
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_2);
		V_3 = (Il2CppObject*)L_3;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0036;
		}

IL_001f:
		{
			Il2CppObject* L_4 = V_3;
			NullCheck((Il2CppObject*)L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_4);
			V_2 = (Il2CppObject *)L_5;
			bool L_6 = V_0;
			if (!L_6)
			{
				goto IL_0032;
			}
		}

IL_002c:
		{
			InvalidOperationException_t1589641621 * L_7 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m355676978(L_7, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0032:
		{
			V_0 = (bool)1;
			Il2CppObject * L_8 = V_2;
			V_1 = (Il2CppObject *)L_8;
		}

IL_0036:
		{
			Il2CppObject* L_9 = V_3;
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_001f;
			}
		}

IL_0041:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0046);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_11 = V_3;
			if (L_11)
			{
				goto IL_004a;
			}
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(70)
		}

IL_004a:
		{
			Il2CppObject* L_12 = V_3;
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(70)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0051:
	{
		bool L_13 = V_0;
		if (L_13)
		{
			goto IL_005d;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_14 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_005d:
	{
		Il2CppObject * L_15 = V_1;
		return L_15;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t3230847821* Enumerable_ToArray_TisInt32_t1153838500_m166960461_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Int32U5BU5D_t3230847821* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Int32U5BU5D_t3230847821*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Int32U5BU5D_t3230847821*)L_6, (int32_t)0);
		Int32U5BU5D_t3230847821* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t2522024052 * L_9 = (List_1_t2522024052 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t2522024052 *)L_9);
		Int32U5BU5D_t3230847821* L_10 = ((  Int32U5BU5D_t3230847821* (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t2522024052 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t1108656482* Enumerable_ToArray_TisIl2CppObject_m3738150552_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	ObjectU5BU5D_t1108656482* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t1108656482* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (ObjectU5BU5D_t1108656482*)L_6, (int32_t)0);
		ObjectU5BU5D_t1108656482* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t1244034627 * L_9 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t1244034627 *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = ((  ObjectU5BU5D_t1108656482* (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)((List_1_t1244034627 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// U System.Linq.Enumerable::Iterate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3128324272_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3128324272_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___initValue1, Func_3_t3956694567 * ___selector2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Iterate_TisIl2CppObject_TisIl2CppObject_m3128324272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_2 = (Il2CppObject*)L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0021;
		}

IL_000e:
		{
			Il2CppObject* L_2 = V_2;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_1 = (Il2CppObject *)L_3;
			Func_3_t3956694567 * L_4 = ___selector2;
			Il2CppObject * L_5 = V_1;
			Il2CppObject * L_6 = ___initValue1;
			NullCheck((Func_3_t3956694567 *)L_4);
			Il2CppObject * L_7 = ((  Il2CppObject * (*) (Func_3_t3956694567 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t3956694567 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			___initValue1 = (Il2CppObject *)L_7;
			V_0 = (bool)0;
		}

IL_0021:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000e;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (L_10)
			{
				goto IL_0035;
			}
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(49)
		}

IL_0035:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003c:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0048;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_13 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0048:
	{
		Il2CppObject * L_14 = ___initValue1;
		return L_14;
	}
}
// U System.Linq.Enumerable::Iterate<System.Single,System.Single>(System.Collections.Generic.IEnumerable`1<T>,U,System.Func`3<T,U,U>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_Iterate_TisSingle_t4291918972_TisSingle_t4291918972_m441885142_MetadataUsageId;
extern "C"  float Enumerable_Iterate_TisSingle_t4291918972_TisSingle_t4291918972_m441885142_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, float ___initValue1, Func_3_t1552338692 * ___selector2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_Iterate_TisSingle_t4291918972_TisSingle_t4291918972_m441885142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	Il2CppObject* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		Il2CppObject* L_0 = ___source0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_0);
		V_2 = (Il2CppObject*)L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0021;
		}

IL_000e:
		{
			Il2CppObject* L_2 = V_2;
			NullCheck((Il2CppObject*)L_2);
			float L_3 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_2);
			V_1 = (float)L_3;
			Func_3_t1552338692 * L_4 = ___selector2;
			float L_5 = V_1;
			float L_6 = ___initValue1;
			NullCheck((Func_3_t1552338692 *)L_4);
			float L_7 = ((  float (*) (Func_3_t1552338692 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((Func_3_t1552338692 *)L_4, (float)L_5, (float)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
			___initValue1 = (float)L_7;
			V_0 = (bool)0;
		}

IL_0021:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_000e;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (L_10)
			{
				goto IL_0035;
			}
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(49)
		}

IL_0035:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003c:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0048;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_13 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0048:
	{
		float L_14 = ___initValue1;
		return L_14;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern Il2CppClass* ExecuteEvents_t2704060668_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2511824965_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2055567731_MethodInfo_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_MetadataUsageId;
extern "C"  GameObject_t3674682005 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___root0, BaseEventData_t2054899105 * ___eventData1, EventFunction_1_t864200549 * ___callbackFunction2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Transform_t1659122786 * V_1 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t2704060668_il2cpp_TypeInfo_var);
		List_1_t3027308338 * L_1 = ((ExecuteEvents_t2704060668_StaticFields*)ExecuteEvents_t2704060668_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m1321751930(NULL /*static, unused*/, (GameObject_t3674682005 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_003b;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t2704060668_il2cpp_TypeInfo_var);
		List_1_t3027308338 * L_2 = ((ExecuteEvents_t2704060668_StaticFields*)ExecuteEvents_t2704060668_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t3027308338 *)L_2);
		Transform_t1659122786 * L_4 = List_1_get_Item_m2511824965((List_1_t3027308338 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_m2511824965_MethodInfo_var);
		V_1 = (Transform_t1659122786 *)L_4;
		Transform_t1659122786 * L_5 = V_1;
		NullCheck((Component_t3501516275 *)L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899((Component_t3501516275 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t2054899105 * L_7 = ___eventData1;
		EventFunction_1_t864200549 * L_8 = ___callbackFunction2;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, BaseEventData_t2054899105 *, EventFunction_1_t864200549 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t3674682005 *)L_6, (BaseEventData_t2054899105 *)L_7, (EventFunction_1_t864200549 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1659122786 * L_10 = V_1;
		NullCheck((Component_t3501516275 *)L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899((Component_t3501516275 *)L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0037:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t2704060668_il2cpp_TypeInfo_var);
		List_1_t3027308338 * L_14 = ((ExecuteEvents_t2704060668_StaticFields*)ExecuteEvents_t2704060668_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		NullCheck((List_1_t3027308338 *)L_14);
		int32_t L_15 = List_1_get_Count_m2055567731((List_1_t3027308338 *)L_14, /*hidden argument*/List_1_get_Count_m2055567731_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0012;
		}
	}
	{
		return (GameObject_t3674682005 *)NULL;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* ExecuteEvents_t2704060668_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_MetadataUsageId;
extern "C"  GameObject_t3674682005 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___root0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (GameObject_t3674682005 *)NULL;
	}

IL_000e:
	{
		GameObject_t3674682005 * L_2 = ___root0;
		NullCheck((GameObject_t3674682005 *)L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159((GameObject_t3674682005 *)L_2, /*hidden argument*/NULL);
		V_0 = (Transform_t1659122786 *)L_3;
		goto IL_0038;
	}

IL_001a:
	{
		Transform_t1659122786 * L_4 = V_0;
		NullCheck((Component_t3501516275 *)L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899((Component_t3501516275 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t2704060668_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t3674682005 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1659122786 * L_7 = V_0;
		NullCheck((Component_t3501516275 *)L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899((Component_t3501516275 *)L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0031:
	{
		Transform_t1659122786 * L_9 = V_0;
		NullCheck((Transform_t1659122786 *)L_9);
		Transform_t1659122786 * L_10 = Transform_get_parent_m2236876972((Transform_t1659122786 *)L_9, /*hidden argument*/NULL);
		V_0 = (Transform_t1659122786 *)L_10;
	}

IL_0038:
	{
		Transform_t1659122786 * L_11 = V_0;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_001a;
		}
	}
	{
		return (GameObject_t3674682005 *)NULL;
	}
}
