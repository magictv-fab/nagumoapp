﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code39Reader
struct  Code39Reader_t1346536404  : public OneDReader_t3436042911
{
public:
	// System.Boolean ZXing.OneD.Code39Reader::usingCheckDigit
	bool ___usingCheckDigit_6;
	// System.Boolean ZXing.OneD.Code39Reader::extendedMode
	bool ___extendedMode_7;
	// System.Text.StringBuilder ZXing.OneD.Code39Reader::decodeRowResult
	StringBuilder_t243639308 * ___decodeRowResult_8;
	// System.Int32[] ZXing.OneD.Code39Reader::counters
	Int32U5BU5D_t3230847821* ___counters_9;

public:
	inline static int32_t get_offset_of_usingCheckDigit_6() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404, ___usingCheckDigit_6)); }
	inline bool get_usingCheckDigit_6() const { return ___usingCheckDigit_6; }
	inline bool* get_address_of_usingCheckDigit_6() { return &___usingCheckDigit_6; }
	inline void set_usingCheckDigit_6(bool value)
	{
		___usingCheckDigit_6 = value;
	}

	inline static int32_t get_offset_of_extendedMode_7() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404, ___extendedMode_7)); }
	inline bool get_extendedMode_7() const { return ___extendedMode_7; }
	inline bool* get_address_of_extendedMode_7() { return &___extendedMode_7; }
	inline void set_extendedMode_7(bool value)
	{
		___extendedMode_7 = value;
	}

	inline static int32_t get_offset_of_decodeRowResult_8() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404, ___decodeRowResult_8)); }
	inline StringBuilder_t243639308 * get_decodeRowResult_8() const { return ___decodeRowResult_8; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowResult_8() { return &___decodeRowResult_8; }
	inline void set_decodeRowResult_8(StringBuilder_t243639308 * value)
	{
		___decodeRowResult_8 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowResult_8, value);
	}

	inline static int32_t get_offset_of_counters_9() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404, ___counters_9)); }
	inline Int32U5BU5D_t3230847821* get_counters_9() const { return ___counters_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_counters_9() { return &___counters_9; }
	inline void set_counters_9(Int32U5BU5D_t3230847821* value)
	{
		___counters_9 = value;
		Il2CppCodeGenWriteBarrier(&___counters_9, value);
	}
};

struct Code39Reader_t1346536404_StaticFields
{
public:
	// System.String ZXing.OneD.Code39Reader::ALPHABET_STRING
	String_t* ___ALPHABET_STRING_2;
	// System.Char[] ZXing.OneD.Code39Reader::ALPHABET
	CharU5BU5D_t3324145743* ___ALPHABET_3;
	// System.Int32[] ZXing.OneD.Code39Reader::CHARACTER_ENCODINGS
	Int32U5BU5D_t3230847821* ___CHARACTER_ENCODINGS_4;
	// System.Int32 ZXing.OneD.Code39Reader::ASTERISK_ENCODING
	int32_t ___ASTERISK_ENCODING_5;

public:
	inline static int32_t get_offset_of_ALPHABET_STRING_2() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404_StaticFields, ___ALPHABET_STRING_2)); }
	inline String_t* get_ALPHABET_STRING_2() const { return ___ALPHABET_STRING_2; }
	inline String_t** get_address_of_ALPHABET_STRING_2() { return &___ALPHABET_STRING_2; }
	inline void set_ALPHABET_STRING_2(String_t* value)
	{
		___ALPHABET_STRING_2 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHABET_STRING_2, value);
	}

	inline static int32_t get_offset_of_ALPHABET_3() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404_StaticFields, ___ALPHABET_3)); }
	inline CharU5BU5D_t3324145743* get_ALPHABET_3() const { return ___ALPHABET_3; }
	inline CharU5BU5D_t3324145743** get_address_of_ALPHABET_3() { return &___ALPHABET_3; }
	inline void set_ALPHABET_3(CharU5BU5D_t3324145743* value)
	{
		___ALPHABET_3 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHABET_3, value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_4() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404_StaticFields, ___CHARACTER_ENCODINGS_4)); }
	inline Int32U5BU5D_t3230847821* get_CHARACTER_ENCODINGS_4() const { return ___CHARACTER_ENCODINGS_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_CHARACTER_ENCODINGS_4() { return &___CHARACTER_ENCODINGS_4; }
	inline void set_CHARACTER_ENCODINGS_4(Int32U5BU5D_t3230847821* value)
	{
		___CHARACTER_ENCODINGS_4 = value;
		Il2CppCodeGenWriteBarrier(&___CHARACTER_ENCODINGS_4, value);
	}

	inline static int32_t get_offset_of_ASTERISK_ENCODING_5() { return static_cast<int32_t>(offsetof(Code39Reader_t1346536404_StaticFields, ___ASTERISK_ENCODING_5)); }
	inline int32_t get_ASTERISK_ENCODING_5() const { return ___ASTERISK_ENCODING_5; }
	inline int32_t* get_address_of_ASTERISK_ENCODING_5() { return &___ASTERISK_ENCODING_5; }
	inline void set_ASTERISK_ENCODING_5(int32_t value)
	{
		___ASTERISK_ENCODING_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
