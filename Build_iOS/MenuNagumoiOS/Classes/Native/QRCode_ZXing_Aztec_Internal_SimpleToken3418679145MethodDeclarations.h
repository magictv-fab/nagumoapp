﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.SimpleToken
struct SimpleToken_t3418679145;
// ZXing.Aztec.Internal.Token
struct Token_t3066207355;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.Aztec.Internal.SimpleToken::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32)
extern "C"  void SimpleToken__ctor_m839093663 (SimpleToken_t3418679145 * __this, Token_t3066207355 * ___previous0, int32_t ___value1, int32_t ___bitCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.SimpleToken::appendTo(ZXing.Common.BitArray,System.Byte[])
extern "C"  void SimpleToken_appendTo_m1752133149 (SimpleToken_t3418679145 * __this, BitArray_t4163851164 * ___bitArray0, ByteU5BU5D_t4260760469* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Aztec.Internal.SimpleToken::ToString()
extern "C"  String_t* SimpleToken_ToString_m2830885869 (SimpleToken_t3418679145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
