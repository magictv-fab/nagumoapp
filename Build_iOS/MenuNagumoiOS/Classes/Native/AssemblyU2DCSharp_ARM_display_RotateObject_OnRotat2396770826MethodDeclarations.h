﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.display.RotateObject/OnRotateComplete
struct OnRotateComplete_t2396770826;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.display.RotateObject/OnRotateComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRotateComplete__ctor_m670675169 (OnRotateComplete_t2396770826 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject/OnRotateComplete::Invoke()
extern "C"  void OnRotateComplete_Invoke_m1291400315 (OnRotateComplete_t2396770826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.display.RotateObject/OnRotateComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnRotateComplete_BeginInvoke_m1664390576 (OnRotateComplete_t2396770826 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject/OnRotateComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnRotateComplete_EndInvoke_m1438825329 (OnRotateComplete_t2396770826 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
