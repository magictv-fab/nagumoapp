﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsHasKey
struct PlayerPrefsHasKey_t2385409762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::.ctor()
extern "C"  void PlayerPrefsHasKey__ctor_m4118267028 (PlayerPrefsHasKey_t2385409762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::Reset()
extern "C"  void PlayerPrefsHasKey_Reset_m1764699969 (PlayerPrefsHasKey_t2385409762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::OnEnter()
extern "C"  void PlayerPrefsHasKey_OnEnter_m1381462891 (PlayerPrefsHasKey_t2385409762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
