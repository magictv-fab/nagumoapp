﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertasCRMDataExclusivas
struct OfertasCRMDataExclusivas_t2158992831;
// OfertasManagerCRM_Exclusivas
struct OfertasManagerCRM_Exclusivas_t295598117;
// CRM_ScrollViewsManager
struct CRM_ScrollViewsManager_t2923429133;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<OfertasValuesCRM>
struct List_1_t4225135114;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t957326451;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// System.String
struct String_t;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM_Exclusivas
struct  OfertasManagerCRM_Exclusivas_t295598117  : public MonoBehaviour_t667441552
{
public:
	// CRM_ScrollViewsManager OfertasManagerCRM_Exclusivas::_CRM_ScrollViewManager
	CRM_ScrollViewsManager_t2923429133 * ____CRM_ScrollViewManager_5;
	// System.Collections.Generic.List`1<OfertasValuesCRM> OfertasManagerCRM_Exclusivas::ofertaValuesLst
	List_1_t4225135114 * ___ofertaValuesLst_13;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::loadingObj
	GameObject_t3674682005 * ___loadingObj_17;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::popup
	GameObject_t3674682005 * ___popup_18;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_19;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::containerItens
	GameObject_t3674682005 * ___containerItens_20;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::favoritouPrimeiraVezObj
	GameObject_t3674682005 * ___favoritouPrimeiraVezObj_22;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::popupSemConexao
	GameObject_t3674682005 * ___popupSemConexao_23;
	// UnityEngine.UI.ScrollRect OfertasManagerCRM_Exclusivas::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_24;
	// System.Int32 OfertasManagerCRM_Exclusivas::ofertaCounter
	int32_t ___ofertaCounter_25;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::selectedItem
	GameObject_t3674682005 * ___selectedItem_27;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap OfertasManagerCRM_Exclusivas::horizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnap_28;
	// UnityEngine.RectTransform OfertasManagerCRM_Exclusivas::rectContainer
	RectTransform_t972643934 * ___rectContainer_31;
	// System.Boolean OfertasManagerCRM_Exclusivas::onRectUpdate
	bool ___onRectUpdate_32;
	// System.Globalization.CultureInfo OfertasManagerCRM_Exclusivas::myCulture
	CultureInfo_t1065375142 * ___myCulture_34;

public:
	inline static int32_t get_offset_of__CRM_ScrollViewManager_5() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ____CRM_ScrollViewManager_5)); }
	inline CRM_ScrollViewsManager_t2923429133 * get__CRM_ScrollViewManager_5() const { return ____CRM_ScrollViewManager_5; }
	inline CRM_ScrollViewsManager_t2923429133 ** get_address_of__CRM_ScrollViewManager_5() { return &____CRM_ScrollViewManager_5; }
	inline void set__CRM_ScrollViewManager_5(CRM_ScrollViewsManager_t2923429133 * value)
	{
		____CRM_ScrollViewManager_5 = value;
		Il2CppCodeGenWriteBarrier(&____CRM_ScrollViewManager_5, value);
	}

	inline static int32_t get_offset_of_ofertaValuesLst_13() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___ofertaValuesLst_13)); }
	inline List_1_t4225135114 * get_ofertaValuesLst_13() const { return ___ofertaValuesLst_13; }
	inline List_1_t4225135114 ** get_address_of_ofertaValuesLst_13() { return &___ofertaValuesLst_13; }
	inline void set_ofertaValuesLst_13(List_1_t4225135114 * value)
	{
		___ofertaValuesLst_13 = value;
		Il2CppCodeGenWriteBarrier(&___ofertaValuesLst_13, value);
	}

	inline static int32_t get_offset_of_loadingObj_17() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___loadingObj_17)); }
	inline GameObject_t3674682005 * get_loadingObj_17() const { return ___loadingObj_17; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_17() { return &___loadingObj_17; }
	inline void set_loadingObj_17(GameObject_t3674682005 * value)
	{
		___loadingObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_17, value);
	}

	inline static int32_t get_offset_of_popup_18() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___popup_18)); }
	inline GameObject_t3674682005 * get_popup_18() const { return ___popup_18; }
	inline GameObject_t3674682005 ** get_address_of_popup_18() { return &___popup_18; }
	inline void set_popup_18(GameObject_t3674682005 * value)
	{
		___popup_18 = value;
		Il2CppCodeGenWriteBarrier(&___popup_18, value);
	}

	inline static int32_t get_offset_of_itemPrefab_19() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___itemPrefab_19)); }
	inline GameObject_t3674682005 * get_itemPrefab_19() const { return ___itemPrefab_19; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_19() { return &___itemPrefab_19; }
	inline void set_itemPrefab_19(GameObject_t3674682005 * value)
	{
		___itemPrefab_19 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_19, value);
	}

	inline static int32_t get_offset_of_containerItens_20() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___containerItens_20)); }
	inline GameObject_t3674682005 * get_containerItens_20() const { return ___containerItens_20; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_20() { return &___containerItens_20; }
	inline void set_containerItens_20(GameObject_t3674682005 * value)
	{
		___containerItens_20 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_20, value);
	}

	inline static int32_t get_offset_of_favoritouPrimeiraVezObj_22() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___favoritouPrimeiraVezObj_22)); }
	inline GameObject_t3674682005 * get_favoritouPrimeiraVezObj_22() const { return ___favoritouPrimeiraVezObj_22; }
	inline GameObject_t3674682005 ** get_address_of_favoritouPrimeiraVezObj_22() { return &___favoritouPrimeiraVezObj_22; }
	inline void set_favoritouPrimeiraVezObj_22(GameObject_t3674682005 * value)
	{
		___favoritouPrimeiraVezObj_22 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouPrimeiraVezObj_22, value);
	}

	inline static int32_t get_offset_of_popupSemConexao_23() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___popupSemConexao_23)); }
	inline GameObject_t3674682005 * get_popupSemConexao_23() const { return ___popupSemConexao_23; }
	inline GameObject_t3674682005 ** get_address_of_popupSemConexao_23() { return &___popupSemConexao_23; }
	inline void set_popupSemConexao_23(GameObject_t3674682005 * value)
	{
		___popupSemConexao_23 = value;
		Il2CppCodeGenWriteBarrier(&___popupSemConexao_23, value);
	}

	inline static int32_t get_offset_of_scrollRect_24() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___scrollRect_24)); }
	inline ScrollRect_t3606982749 * get_scrollRect_24() const { return ___scrollRect_24; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_24() { return &___scrollRect_24; }
	inline void set_scrollRect_24(ScrollRect_t3606982749 * value)
	{
		___scrollRect_24 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_24, value);
	}

	inline static int32_t get_offset_of_ofertaCounter_25() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___ofertaCounter_25)); }
	inline int32_t get_ofertaCounter_25() const { return ___ofertaCounter_25; }
	inline int32_t* get_address_of_ofertaCounter_25() { return &___ofertaCounter_25; }
	inline void set_ofertaCounter_25(int32_t value)
	{
		___ofertaCounter_25 = value;
	}

	inline static int32_t get_offset_of_selectedItem_27() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___selectedItem_27)); }
	inline GameObject_t3674682005 * get_selectedItem_27() const { return ___selectedItem_27; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_27() { return &___selectedItem_27; }
	inline void set_selectedItem_27(GameObject_t3674682005 * value)
	{
		___selectedItem_27 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_27, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnap_28() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___horizontalScrollSnap_28)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnap_28() const { return ___horizontalScrollSnap_28; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnap_28() { return &___horizontalScrollSnap_28; }
	inline void set_horizontalScrollSnap_28(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnap_28 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnap_28, value);
	}

	inline static int32_t get_offset_of_rectContainer_31() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___rectContainer_31)); }
	inline RectTransform_t972643934 * get_rectContainer_31() const { return ___rectContainer_31; }
	inline RectTransform_t972643934 ** get_address_of_rectContainer_31() { return &___rectContainer_31; }
	inline void set_rectContainer_31(RectTransform_t972643934 * value)
	{
		___rectContainer_31 = value;
		Il2CppCodeGenWriteBarrier(&___rectContainer_31, value);
	}

	inline static int32_t get_offset_of_onRectUpdate_32() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___onRectUpdate_32)); }
	inline bool get_onRectUpdate_32() const { return ___onRectUpdate_32; }
	inline bool* get_address_of_onRectUpdate_32() { return &___onRectUpdate_32; }
	inline void set_onRectUpdate_32(bool value)
	{
		___onRectUpdate_32 = value;
	}

	inline static int32_t get_offset_of_myCulture_34() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117, ___myCulture_34)); }
	inline CultureInfo_t1065375142 * get_myCulture_34() const { return ___myCulture_34; }
	inline CultureInfo_t1065375142 ** get_address_of_myCulture_34() { return &___myCulture_34; }
	inline void set_myCulture_34(CultureInfo_t1065375142 * value)
	{
		___myCulture_34 = value;
		Il2CppCodeGenWriteBarrier(&___myCulture_34, value);
	}
};

struct OfertasManagerCRM_Exclusivas_t295598117_StaticFields
{
public:
	// OfertasCRMDataExclusivas OfertasManagerCRM_Exclusivas::ofertasDataServer
	OfertasCRMDataExclusivas_t2158992831 * ___ofertasDataServer_3;
	// OfertasManagerCRM_Exclusivas OfertasManagerCRM_Exclusivas::instance
	OfertasManagerCRM_Exclusivas_t295598117 * ___instance_4;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::titleLst
	List_1_t1375417109 * ___titleLst_6;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::valueLst
	List_1_t1375417109 * ___valueLst_7;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::infoLst
	List_1_t1375417109 * ___infoLst_8;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::idLst
	List_1_t1375417109 * ___idLst_9;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::idcrmLst
	List_1_t1375417109 * ___idcrmLst_10;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::validadeLst
	List_1_t1375417109 * ___validadeLst_11;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_Exclusivas::linkLst
	List_1_t1375417109 * ___linkLst_12;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> OfertasManagerCRM_Exclusivas::textureLst
	List_1_t957326451 * ___textureLst_14;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> OfertasManagerCRM_Exclusivas::imgLst
	List_1_t272385497 * ___imgLst_15;
	// System.Collections.Generic.List`1<System.Boolean> OfertasManagerCRM_Exclusivas::favoritouLst
	List_1_t1844984270 * ___favoritouLst_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> OfertasManagerCRM_Exclusivas::itensList
	List_1_t747900261 * ___itensList_21;
	// UnityEngine.GameObject OfertasManagerCRM_Exclusivas::currentItem
	GameObject_t3674682005 * ___currentItem_26;
	// System.String OfertasManagerCRM_Exclusivas::currentJsonTxt
	String_t* ___currentJsonTxt_29;
	// System.Int32 OfertasManagerCRM_Exclusivas::loadedItensCount
	int32_t ___loadedItensCount_30;
	// System.Int32 OfertasManagerCRM_Exclusivas::loadedImgsCount
	int32_t ___loadedImgsCount_33;

public:
	inline static int32_t get_offset_of_ofertasDataServer_3() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___ofertasDataServer_3)); }
	inline OfertasCRMDataExclusivas_t2158992831 * get_ofertasDataServer_3() const { return ___ofertasDataServer_3; }
	inline OfertasCRMDataExclusivas_t2158992831 ** get_address_of_ofertasDataServer_3() { return &___ofertasDataServer_3; }
	inline void set_ofertasDataServer_3(OfertasCRMDataExclusivas_t2158992831 * value)
	{
		___ofertasDataServer_3 = value;
		Il2CppCodeGenWriteBarrier(&___ofertasDataServer_3, value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___instance_4)); }
	inline OfertasManagerCRM_Exclusivas_t295598117 * get_instance_4() const { return ___instance_4; }
	inline OfertasManagerCRM_Exclusivas_t295598117 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(OfertasManagerCRM_Exclusivas_t295598117 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}

	inline static int32_t get_offset_of_titleLst_6() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___titleLst_6)); }
	inline List_1_t1375417109 * get_titleLst_6() const { return ___titleLst_6; }
	inline List_1_t1375417109 ** get_address_of_titleLst_6() { return &___titleLst_6; }
	inline void set_titleLst_6(List_1_t1375417109 * value)
	{
		___titleLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_6, value);
	}

	inline static int32_t get_offset_of_valueLst_7() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___valueLst_7)); }
	inline List_1_t1375417109 * get_valueLst_7() const { return ___valueLst_7; }
	inline List_1_t1375417109 ** get_address_of_valueLst_7() { return &___valueLst_7; }
	inline void set_valueLst_7(List_1_t1375417109 * value)
	{
		___valueLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_7, value);
	}

	inline static int32_t get_offset_of_infoLst_8() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___infoLst_8)); }
	inline List_1_t1375417109 * get_infoLst_8() const { return ___infoLst_8; }
	inline List_1_t1375417109 ** get_address_of_infoLst_8() { return &___infoLst_8; }
	inline void set_infoLst_8(List_1_t1375417109 * value)
	{
		___infoLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_8, value);
	}

	inline static int32_t get_offset_of_idLst_9() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___idLst_9)); }
	inline List_1_t1375417109 * get_idLst_9() const { return ___idLst_9; }
	inline List_1_t1375417109 ** get_address_of_idLst_9() { return &___idLst_9; }
	inline void set_idLst_9(List_1_t1375417109 * value)
	{
		___idLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_9, value);
	}

	inline static int32_t get_offset_of_idcrmLst_10() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___idcrmLst_10)); }
	inline List_1_t1375417109 * get_idcrmLst_10() const { return ___idcrmLst_10; }
	inline List_1_t1375417109 ** get_address_of_idcrmLst_10() { return &___idcrmLst_10; }
	inline void set_idcrmLst_10(List_1_t1375417109 * value)
	{
		___idcrmLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___idcrmLst_10, value);
	}

	inline static int32_t get_offset_of_validadeLst_11() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___validadeLst_11)); }
	inline List_1_t1375417109 * get_validadeLst_11() const { return ___validadeLst_11; }
	inline List_1_t1375417109 ** get_address_of_validadeLst_11() { return &___validadeLst_11; }
	inline void set_validadeLst_11(List_1_t1375417109 * value)
	{
		___validadeLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___validadeLst_11, value);
	}

	inline static int32_t get_offset_of_linkLst_12() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___linkLst_12)); }
	inline List_1_t1375417109 * get_linkLst_12() const { return ___linkLst_12; }
	inline List_1_t1375417109 ** get_address_of_linkLst_12() { return &___linkLst_12; }
	inline void set_linkLst_12(List_1_t1375417109 * value)
	{
		___linkLst_12 = value;
		Il2CppCodeGenWriteBarrier(&___linkLst_12, value);
	}

	inline static int32_t get_offset_of_textureLst_14() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___textureLst_14)); }
	inline List_1_t957326451 * get_textureLst_14() const { return ___textureLst_14; }
	inline List_1_t957326451 ** get_address_of_textureLst_14() { return &___textureLst_14; }
	inline void set_textureLst_14(List_1_t957326451 * value)
	{
		___textureLst_14 = value;
		Il2CppCodeGenWriteBarrier(&___textureLst_14, value);
	}

	inline static int32_t get_offset_of_imgLst_15() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___imgLst_15)); }
	inline List_1_t272385497 * get_imgLst_15() const { return ___imgLst_15; }
	inline List_1_t272385497 ** get_address_of_imgLst_15() { return &___imgLst_15; }
	inline void set_imgLst_15(List_1_t272385497 * value)
	{
		___imgLst_15 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_15, value);
	}

	inline static int32_t get_offset_of_favoritouLst_16() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___favoritouLst_16)); }
	inline List_1_t1844984270 * get_favoritouLst_16() const { return ___favoritouLst_16; }
	inline List_1_t1844984270 ** get_address_of_favoritouLst_16() { return &___favoritouLst_16; }
	inline void set_favoritouLst_16(List_1_t1844984270 * value)
	{
		___favoritouLst_16 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouLst_16, value);
	}

	inline static int32_t get_offset_of_itensList_21() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___itensList_21)); }
	inline List_1_t747900261 * get_itensList_21() const { return ___itensList_21; }
	inline List_1_t747900261 ** get_address_of_itensList_21() { return &___itensList_21; }
	inline void set_itensList_21(List_1_t747900261 * value)
	{
		___itensList_21 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_21, value);
	}

	inline static int32_t get_offset_of_currentItem_26() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___currentItem_26)); }
	inline GameObject_t3674682005 * get_currentItem_26() const { return ___currentItem_26; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_26() { return &___currentItem_26; }
	inline void set_currentItem_26(GameObject_t3674682005 * value)
	{
		___currentItem_26 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_26, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_29() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___currentJsonTxt_29)); }
	inline String_t* get_currentJsonTxt_29() const { return ___currentJsonTxt_29; }
	inline String_t** get_address_of_currentJsonTxt_29() { return &___currentJsonTxt_29; }
	inline void set_currentJsonTxt_29(String_t* value)
	{
		___currentJsonTxt_29 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_29, value);
	}

	inline static int32_t get_offset_of_loadedItensCount_30() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___loadedItensCount_30)); }
	inline int32_t get_loadedItensCount_30() const { return ___loadedItensCount_30; }
	inline int32_t* get_address_of_loadedItensCount_30() { return &___loadedItensCount_30; }
	inline void set_loadedItensCount_30(int32_t value)
	{
		___loadedItensCount_30 = value;
	}

	inline static int32_t get_offset_of_loadedImgsCount_33() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_Exclusivas_t295598117_StaticFields, ___loadedImgsCount_33)); }
	inline int32_t get_loadedImgsCount_33() const { return ___loadedImgsCount_33; }
	inline int32_t* get_address_of_loadedImgsCount_33() { return &___loadedImgsCount_33; }
	inline void set_loadedImgsCount_33(int32_t value)
	{
		___loadedImgsCount_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
