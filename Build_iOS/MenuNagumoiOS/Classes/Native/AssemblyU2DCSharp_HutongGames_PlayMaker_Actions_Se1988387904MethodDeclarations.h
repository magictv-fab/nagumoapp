﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightCookie
struct SetLightCookie_t1988387904;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::.ctor()
extern "C"  void SetLightCookie__ctor_m1478192166 (SetLightCookie_t1988387904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::Reset()
extern "C"  void SetLightCookie_Reset_m3419592403 (SetLightCookie_t1988387904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::OnEnter()
extern "C"  void SetLightCookie_OnEnter_m2595192445 (SetLightCookie_t1988387904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::DoSetLightCookie()
extern "C"  void SetLightCookie_DoSetLightCookie_m3086426849 (SetLightCookie_t1988387904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
