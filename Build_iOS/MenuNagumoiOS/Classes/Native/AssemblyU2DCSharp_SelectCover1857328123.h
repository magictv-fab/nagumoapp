﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectCover
struct  SelectCover_t1857328123  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SelectCover_t1857328123_StaticFields
{
public:
	// System.String SelectCover::clickedName
	String_t* ___clickedName_2;
	// System.String SelectCover::clickedEdition
	String_t* ___clickedEdition_3;

public:
	inline static int32_t get_offset_of_clickedName_2() { return static_cast<int32_t>(offsetof(SelectCover_t1857328123_StaticFields, ___clickedName_2)); }
	inline String_t* get_clickedName_2() const { return ___clickedName_2; }
	inline String_t** get_address_of_clickedName_2() { return &___clickedName_2; }
	inline void set_clickedName_2(String_t* value)
	{
		___clickedName_2 = value;
		Il2CppCodeGenWriteBarrier(&___clickedName_2, value);
	}

	inline static int32_t get_offset_of_clickedEdition_3() { return static_cast<int32_t>(offsetof(SelectCover_t1857328123_StaticFields, ___clickedEdition_3)); }
	inline String_t* get_clickedEdition_3() const { return ___clickedEdition_3; }
	inline String_t** get_address_of_clickedEdition_3() { return &___clickedEdition_3; }
	inline void set_clickedEdition_3(String_t* value)
	{
		___clickedEdition_3 = value;
		Il2CppCodeGenWriteBarrier(&___clickedEdition_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
