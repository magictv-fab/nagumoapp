﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatAddMutiple
struct  FloatAddMutiple_t949141333  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.FloatAddMutiple::floatVariables
	FsmFloatU5BU5D_t2945380875* ___floatVariables_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatAddMutiple::addTo
	FsmFloat_t2134102846 * ___addTo_10;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatAddMutiple::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_floatVariables_9() { return static_cast<int32_t>(offsetof(FloatAddMutiple_t949141333, ___floatVariables_9)); }
	inline FsmFloatU5BU5D_t2945380875* get_floatVariables_9() const { return ___floatVariables_9; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_floatVariables_9() { return &___floatVariables_9; }
	inline void set_floatVariables_9(FsmFloatU5BU5D_t2945380875* value)
	{
		___floatVariables_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariables_9, value);
	}

	inline static int32_t get_offset_of_addTo_10() { return static_cast<int32_t>(offsetof(FloatAddMutiple_t949141333, ___addTo_10)); }
	inline FsmFloat_t2134102846 * get_addTo_10() const { return ___addTo_10; }
	inline FsmFloat_t2134102846 ** get_address_of_addTo_10() { return &___addTo_10; }
	inline void set_addTo_10(FsmFloat_t2134102846 * value)
	{
		___addTo_10 = value;
		Il2CppCodeGenWriteBarrier(&___addTo_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(FloatAddMutiple_t949141333, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
