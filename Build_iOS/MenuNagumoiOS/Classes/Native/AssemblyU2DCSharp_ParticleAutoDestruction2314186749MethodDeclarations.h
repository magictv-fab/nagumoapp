﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ParticleAutoDestruction
struct ParticleAutoDestruction_t2314186749;

#include "codegen/il2cpp-codegen.h"

// System.Void ParticleAutoDestruction::.ctor()
extern "C"  void ParticleAutoDestruction__ctor_m2313706510 (ParticleAutoDestruction_t2314186749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleAutoDestruction::Start()
extern "C"  void ParticleAutoDestruction_Start_m1260844302 (ParticleAutoDestruction_t2314186749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleAutoDestruction::Update()
extern "C"  void ParticleAutoDestruction_Update_m437319871 (ParticleAutoDestruction_t2314186749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
