﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "QRCode_ZXing_OneD_UPCEANReader3527170699.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEReader
struct  UPCEReader_t2407515358  : public UPCEANReader_t3527170699
{
public:
	// System.Int32[] ZXing.OneD.UPCEReader::decodeMiddleCounters
	Int32U5BU5D_t3230847821* ___decodeMiddleCounters_13;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_13() { return static_cast<int32_t>(offsetof(UPCEReader_t2407515358, ___decodeMiddleCounters_13)); }
	inline Int32U5BU5D_t3230847821* get_decodeMiddleCounters_13() const { return ___decodeMiddleCounters_13; }
	inline Int32U5BU5D_t3230847821** get_address_of_decodeMiddleCounters_13() { return &___decodeMiddleCounters_13; }
	inline void set_decodeMiddleCounters_13(Int32U5BU5D_t3230847821* value)
	{
		___decodeMiddleCounters_13 = value;
		Il2CppCodeGenWriteBarrier(&___decodeMiddleCounters_13, value);
	}
};

struct UPCEReader_t2407515358_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.UPCEReader::MIDDLE_END_PATTERN
	Int32U5BU5D_t3230847821* ___MIDDLE_END_PATTERN_11;
	// System.Int32[][] ZXing.OneD.UPCEReader::NUMSYS_AND_CHECK_DIGIT_PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___NUMSYS_AND_CHECK_DIGIT_PATTERNS_12;

public:
	inline static int32_t get_offset_of_MIDDLE_END_PATTERN_11() { return static_cast<int32_t>(offsetof(UPCEReader_t2407515358_StaticFields, ___MIDDLE_END_PATTERN_11)); }
	inline Int32U5BU5D_t3230847821* get_MIDDLE_END_PATTERN_11() const { return ___MIDDLE_END_PATTERN_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_MIDDLE_END_PATTERN_11() { return &___MIDDLE_END_PATTERN_11; }
	inline void set_MIDDLE_END_PATTERN_11(Int32U5BU5D_t3230847821* value)
	{
		___MIDDLE_END_PATTERN_11 = value;
		Il2CppCodeGenWriteBarrier(&___MIDDLE_END_PATTERN_11, value);
	}

	inline static int32_t get_offset_of_NUMSYS_AND_CHECK_DIGIT_PATTERNS_12() { return static_cast<int32_t>(offsetof(UPCEReader_t2407515358_StaticFields, ___NUMSYS_AND_CHECK_DIGIT_PATTERNS_12)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_NUMSYS_AND_CHECK_DIGIT_PATTERNS_12() const { return ___NUMSYS_AND_CHECK_DIGIT_PATTERNS_12; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_NUMSYS_AND_CHECK_DIGIT_PATTERNS_12() { return &___NUMSYS_AND_CHECK_DIGIT_PATTERNS_12; }
	inline void set_NUMSYS_AND_CHECK_DIGIT_PATTERNS_12(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___NUMSYS_AND_CHECK_DIGIT_PATTERNS_12 = value;
		Il2CppCodeGenWriteBarrier(&___NUMSYS_AND_CHECK_DIGIT_PATTERNS_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
