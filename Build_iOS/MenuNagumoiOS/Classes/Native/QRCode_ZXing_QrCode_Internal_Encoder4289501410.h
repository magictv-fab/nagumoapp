﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Encoder
struct  Encoder_t4289501410  : public Il2CppObject
{
public:

public:
};

struct Encoder_t4289501410_StaticFields
{
public:
	// System.Int32[] ZXing.QrCode.Internal.Encoder::ALPHANUMERIC_TABLE
	Int32U5BU5D_t3230847821* ___ALPHANUMERIC_TABLE_0;
	// System.String ZXing.QrCode.Internal.Encoder::DEFAULT_BYTE_MODE_ENCODING
	String_t* ___DEFAULT_BYTE_MODE_ENCODING_1;

public:
	inline static int32_t get_offset_of_ALPHANUMERIC_TABLE_0() { return static_cast<int32_t>(offsetof(Encoder_t4289501410_StaticFields, ___ALPHANUMERIC_TABLE_0)); }
	inline Int32U5BU5D_t3230847821* get_ALPHANUMERIC_TABLE_0() const { return ___ALPHANUMERIC_TABLE_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_ALPHANUMERIC_TABLE_0() { return &___ALPHANUMERIC_TABLE_0; }
	inline void set_ALPHANUMERIC_TABLE_0(Int32U5BU5D_t3230847821* value)
	{
		___ALPHANUMERIC_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHANUMERIC_TABLE_0, value);
	}

	inline static int32_t get_offset_of_DEFAULT_BYTE_MODE_ENCODING_1() { return static_cast<int32_t>(offsetof(Encoder_t4289501410_StaticFields, ___DEFAULT_BYTE_MODE_ENCODING_1)); }
	inline String_t* get_DEFAULT_BYTE_MODE_ENCODING_1() const { return ___DEFAULT_BYTE_MODE_ENCODING_1; }
	inline String_t** get_address_of_DEFAULT_BYTE_MODE_ENCODING_1() { return &___DEFAULT_BYTE_MODE_ENCODING_1; }
	inline void set_DEFAULT_BYTE_MODE_ENCODING_1(String_t* value)
	{
		___DEFAULT_BYTE_MODE_ENCODING_1 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_BYTE_MODE_ENCODING_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
