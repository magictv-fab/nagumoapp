﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCEANExtensionSupport
struct UPCEANExtensionSupport_t269614168;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// ZXing.Result ZXing.OneD.UPCEANExtensionSupport::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32)
extern "C"  Result_t2610723219 * UPCEANExtensionSupport_decodeRow_m2077683412 (UPCEANExtensionSupport_t269614168 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, int32_t ___rowOffset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANExtensionSupport::.ctor()
extern "C"  void UPCEANExtensionSupport__ctor_m405126859 (UPCEANExtensionSupport_t269614168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANExtensionSupport::.cctor()
extern "C"  void UPCEANExtensionSupport__cctor_m3486901826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
