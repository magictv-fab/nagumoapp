﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetLocationInfo
struct GetLocationInfo_t1970767239;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::.ctor()
extern "C"  void GetLocationInfo__ctor_m3145660815 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::Reset()
extern "C"  void GetLocationInfo_Reset_m792093756 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::OnEnter()
extern "C"  void GetLocationInfo_OnEnter_m3009762726 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::DoGetLocationInfo()
extern "C"  void GetLocationInfo_DoGetLocationInfo_m3720127899 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
