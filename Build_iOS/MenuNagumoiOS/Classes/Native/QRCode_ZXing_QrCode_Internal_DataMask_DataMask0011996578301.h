﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_QrCode_Internal_DataMask1708385874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DataMask/DataMask001
struct  DataMask001_t1996578301  : public DataMask_t1708385874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
