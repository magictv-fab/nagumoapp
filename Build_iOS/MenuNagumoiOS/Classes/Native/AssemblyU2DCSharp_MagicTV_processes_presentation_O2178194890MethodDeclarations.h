﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.processes.presentation.OpenProcessComponent
struct OpenProcessComponent_t2178194890;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.processes.presentation.OpenProcessComponent::.ctor()
extern "C"  void OpenProcessComponent__ctor_m1195176879 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::prepare()
extern "C"  void OpenProcessComponent_prepare_m2735693108 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::loadAllDataSet()
extern "C"  void OpenProcessComponent_loadAllDataSet_m158291026 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::createDataList()
extern "C"  void OpenProcessComponent_createDataList_m2705283609 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::startLoadDatasetFolder()
extern "C"  void OpenProcessComponent_startLoadDatasetFolder_m2529515155 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::Update()
extern "C"  void OpenProcessComponent_Update_m122639678 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::loadNextDataset()
extern "C"  void OpenProcessComponent_loadNextDataset_m1476888428 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::loadLocalDataset(System.String,System.Boolean)
extern "C"  void OpenProcessComponent_loadLocalDataset_m1536468899 (OpenProcessComponent_t2178194890 * __this, String_t* ___xmlFilePath0, bool ___persistTrack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::Play()
extern "C"  void OpenProcessComponent_Play_m3652097513 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::checkTrackingFolders()
extern "C"  void OpenProcessComponent_checkTrackingFolders_m1272197883 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::loadTracks(System.Boolean)
extern "C"  void OpenProcessComponent_loadTracks_m1836823130 (OpenProcessComponent_t2178194890 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::step2()
extern "C"  void OpenProcessComponent_step2_m2776199763 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::organizeTracks()
extern "C"  void OpenProcessComponent_organizeTracks_m2691415008 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::Stop()
extern "C"  void OpenProcessComponent_Stop_m3745781559 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.presentation.OpenProcessComponent::Pause()
extern "C"  void OpenProcessComponent_Pause_m1249302851 (OpenProcessComponent_t2178194890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
