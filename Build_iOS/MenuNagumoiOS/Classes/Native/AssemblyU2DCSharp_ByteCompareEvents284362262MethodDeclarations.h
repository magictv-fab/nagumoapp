﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ByteCompareEvents
struct ByteCompareEvents_t284362262;

#include "codegen/il2cpp-codegen.h"

// System.Void ByteCompareEvents::.ctor()
extern "C"  void ByteCompareEvents__ctor_m2671190037 (ByteCompareEvents_t284362262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteCompareEvents::Start()
extern "C"  void ByteCompareEvents_Start_m1618327829 (ByteCompareEvents_t284362262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteCompareEvents::startByteComparison()
extern "C"  void ByteCompareEvents_startByteComparison_m4187648326 (ByteCompareEvents_t284362262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteCompareEvents::TrackCameraMovements()
extern "C"  void ByteCompareEvents_TrackCameraMovements_m41988291 (ByteCompareEvents_t284362262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
