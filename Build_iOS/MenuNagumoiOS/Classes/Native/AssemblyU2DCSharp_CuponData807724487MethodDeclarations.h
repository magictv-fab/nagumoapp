﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CuponData
struct CuponData_t807724487;

#include "codegen/il2cpp-codegen.h"

// System.Void CuponData::.ctor()
extern "C"  void CuponData__ctor_m3435052932 (CuponData_t807724487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
