﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t1845338141;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb1091759131.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t1845338141  : public VuforiaAbstractBehaviour_t1091759131
{
public:

public:
};

struct VuforiaBehaviour_t1845338141_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t1845338141 * ___mVuforiaBehaviour_48;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_48() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t1845338141_StaticFields, ___mVuforiaBehaviour_48)); }
	inline VuforiaBehaviour_t1845338141 * get_mVuforiaBehaviour_48() const { return ___mVuforiaBehaviour_48; }
	inline VuforiaBehaviour_t1845338141 ** get_address_of_mVuforiaBehaviour_48() { return &___mVuforiaBehaviour_48; }
	inline void set_mVuforiaBehaviour_48(VuforiaBehaviour_t1845338141 * value)
	{
		___mVuforiaBehaviour_48 = value;
		Il2CppCodeGenWriteBarrier(&___mVuforiaBehaviour_48, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
