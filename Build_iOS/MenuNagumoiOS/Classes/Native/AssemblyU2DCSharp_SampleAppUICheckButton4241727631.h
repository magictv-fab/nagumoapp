﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SampleAppUIRect
struct SampleAppUIRect_t2784570703;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;

#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUICheckButton
struct  SampleAppUICheckButton_t4241727631  : public ISampleAppUIElement_t2180050874
{
public:
	// SampleAppUIRect SampleAppUICheckButton::mRect
	SampleAppUIRect_t2784570703 * ___mRect_0;
	// System.Boolean SampleAppUICheckButton::mTappedOn
	bool ___mTappedOn_1;
	// System.Boolean SampleAppUICheckButton::mSelected
	bool ___mSelected_2;
	// UnityEngine.GUIStyle SampleAppUICheckButton::mStyle
	GUIStyle_t2990928826 * ___mStyle_3;
	// System.Single SampleAppUICheckButton::mHeight
	float ___mHeight_4;
	// System.Single SampleAppUICheckButton::mWidth
	float ___mWidth_5;
	// System.String SampleAppUICheckButton::mTitle
	String_t* ___mTitle_6;
	// System.Action`1<System.Boolean> SampleAppUICheckButton::TappedOn
	Action_1_t872614854 * ___TappedOn_7;

public:
	inline static int32_t get_offset_of_mRect_0() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mRect_0)); }
	inline SampleAppUIRect_t2784570703 * get_mRect_0() const { return ___mRect_0; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_mRect_0() { return &___mRect_0; }
	inline void set_mRect_0(SampleAppUIRect_t2784570703 * value)
	{
		___mRect_0 = value;
		Il2CppCodeGenWriteBarrier(&___mRect_0, value);
	}

	inline static int32_t get_offset_of_mTappedOn_1() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mTappedOn_1)); }
	inline bool get_mTappedOn_1() const { return ___mTappedOn_1; }
	inline bool* get_address_of_mTappedOn_1() { return &___mTappedOn_1; }
	inline void set_mTappedOn_1(bool value)
	{
		___mTappedOn_1 = value;
	}

	inline static int32_t get_offset_of_mSelected_2() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mSelected_2)); }
	inline bool get_mSelected_2() const { return ___mSelected_2; }
	inline bool* get_address_of_mSelected_2() { return &___mSelected_2; }
	inline void set_mSelected_2(bool value)
	{
		___mSelected_2 = value;
	}

	inline static int32_t get_offset_of_mStyle_3() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mStyle_3)); }
	inline GUIStyle_t2990928826 * get_mStyle_3() const { return ___mStyle_3; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyle_3() { return &___mStyle_3; }
	inline void set_mStyle_3(GUIStyle_t2990928826 * value)
	{
		___mStyle_3 = value;
		Il2CppCodeGenWriteBarrier(&___mStyle_3, value);
	}

	inline static int32_t get_offset_of_mHeight_4() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mHeight_4)); }
	inline float get_mHeight_4() const { return ___mHeight_4; }
	inline float* get_address_of_mHeight_4() { return &___mHeight_4; }
	inline void set_mHeight_4(float value)
	{
		___mHeight_4 = value;
	}

	inline static int32_t get_offset_of_mWidth_5() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mWidth_5)); }
	inline float get_mWidth_5() const { return ___mWidth_5; }
	inline float* get_address_of_mWidth_5() { return &___mWidth_5; }
	inline void set_mWidth_5(float value)
	{
		___mWidth_5 = value;
	}

	inline static int32_t get_offset_of_mTitle_6() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___mTitle_6)); }
	inline String_t* get_mTitle_6() const { return ___mTitle_6; }
	inline String_t** get_address_of_mTitle_6() { return &___mTitle_6; }
	inline void set_mTitle_6(String_t* value)
	{
		___mTitle_6 = value;
		Il2CppCodeGenWriteBarrier(&___mTitle_6, value);
	}

	inline static int32_t get_offset_of_TappedOn_7() { return static_cast<int32_t>(offsetof(SampleAppUICheckButton_t4241727631, ___TappedOn_7)); }
	inline Action_1_t872614854 * get_TappedOn_7() const { return ___TappedOn_7; }
	inline Action_1_t872614854 ** get_address_of_TappedOn_7() { return &___TappedOn_7; }
	inline void set_TappedOn_7(Action_1_t872614854 * value)
	{
		___TappedOn_7 = value;
		Il2CppCodeGenWriteBarrier(&___TappedOn_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
