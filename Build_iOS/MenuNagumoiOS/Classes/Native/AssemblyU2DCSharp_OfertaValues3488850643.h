﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Button
struct Button_t3896396478;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertaValues
struct  OfertaValues_t3488850643  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text OfertaValues::title
	Text_t9039225 * ___title_2;
	// UnityEngine.UI.Text OfertaValues::info
	Text_t9039225 * ___info_3;
	// UnityEngine.UI.Text OfertaValues::value
	Text_t9039225 * ___value_4;
	// UnityEngine.UI.Text OfertaValues::validade
	Text_t9039225 * ___validade_5;
	// UnityEngine.UI.Text OfertaValues::unidades
	Text_t9039225 * ___unidades_6;
	// UnityEngine.UI.Text OfertaValues::aquisicao
	Text_t9039225 * ___aquisicao_7;
	// UnityEngine.UI.Image OfertaValues::img
	Image_t538875265 * ___img_8;
	// UnityEngine.UI.Image OfertaValues::favoritoImg
	Image_t538875265 * ___favoritoImg_9;
	// UnityEngine.UI.Button OfertaValues::btn
	Button_t3896396478 * ___btn_10;
	// System.String OfertaValues::id
	String_t* ___id_11;
	// UnityEngine.Texture2D OfertaValues::saveImageSource
	Texture2D_t3884108195 * ___saveImageSource_12;
	// System.Boolean OfertaValues::favoritou
	bool ___favoritou_13;
	// UnityEngine.Sprite OfertaValues::favoritoSprite
	Sprite_t3199167241 * ___favoritoSprite_14;
	// UnityEngine.Sprite OfertaValues::desfavoritouSprite
	Sprite_t3199167241 * ___desfavoritouSprite_15;
	// UnityEngine.GameObject OfertaValues::favoritouObj
	GameObject_t3674682005 * ___favoritouObj_16;
	// UnityEngine.GameObject OfertaValues::btnFavoritado
	GameObject_t3674682005 * ___btnFavoritado_17;
	// UnityEngine.GameObject OfertaValues::btnFavoritar
	GameObject_t3674682005 * ___btnFavoritar_18;
	// UnityEngine.GameObject OfertaValues::popupSemConexao
	GameObject_t3674682005 * ___popupSemConexao_19;
	// System.Boolean OfertaValues::showPopup
	bool ___showPopup_20;
	// System.String OfertaValues::link
	String_t* ___link_21;
	// System.String OfertaValues::imgLink
	String_t* ___imgLink_22;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___title_2)); }
	inline Text_t9039225 * get_title_2() const { return ___title_2; }
	inline Text_t9039225 ** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(Text_t9039225 * value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_info_3() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___info_3)); }
	inline Text_t9039225 * get_info_3() const { return ___info_3; }
	inline Text_t9039225 ** get_address_of_info_3() { return &___info_3; }
	inline void set_info_3(Text_t9039225 * value)
	{
		___info_3 = value;
		Il2CppCodeGenWriteBarrier(&___info_3, value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___value_4)); }
	inline Text_t9039225 * get_value_4() const { return ___value_4; }
	inline Text_t9039225 ** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(Text_t9039225 * value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier(&___value_4, value);
	}

	inline static int32_t get_offset_of_validade_5() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___validade_5)); }
	inline Text_t9039225 * get_validade_5() const { return ___validade_5; }
	inline Text_t9039225 ** get_address_of_validade_5() { return &___validade_5; }
	inline void set_validade_5(Text_t9039225 * value)
	{
		___validade_5 = value;
		Il2CppCodeGenWriteBarrier(&___validade_5, value);
	}

	inline static int32_t get_offset_of_unidades_6() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___unidades_6)); }
	inline Text_t9039225 * get_unidades_6() const { return ___unidades_6; }
	inline Text_t9039225 ** get_address_of_unidades_6() { return &___unidades_6; }
	inline void set_unidades_6(Text_t9039225 * value)
	{
		___unidades_6 = value;
		Il2CppCodeGenWriteBarrier(&___unidades_6, value);
	}

	inline static int32_t get_offset_of_aquisicao_7() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___aquisicao_7)); }
	inline Text_t9039225 * get_aquisicao_7() const { return ___aquisicao_7; }
	inline Text_t9039225 ** get_address_of_aquisicao_7() { return &___aquisicao_7; }
	inline void set_aquisicao_7(Text_t9039225 * value)
	{
		___aquisicao_7 = value;
		Il2CppCodeGenWriteBarrier(&___aquisicao_7, value);
	}

	inline static int32_t get_offset_of_img_8() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___img_8)); }
	inline Image_t538875265 * get_img_8() const { return ___img_8; }
	inline Image_t538875265 ** get_address_of_img_8() { return &___img_8; }
	inline void set_img_8(Image_t538875265 * value)
	{
		___img_8 = value;
		Il2CppCodeGenWriteBarrier(&___img_8, value);
	}

	inline static int32_t get_offset_of_favoritoImg_9() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___favoritoImg_9)); }
	inline Image_t538875265 * get_favoritoImg_9() const { return ___favoritoImg_9; }
	inline Image_t538875265 ** get_address_of_favoritoImg_9() { return &___favoritoImg_9; }
	inline void set_favoritoImg_9(Image_t538875265 * value)
	{
		___favoritoImg_9 = value;
		Il2CppCodeGenWriteBarrier(&___favoritoImg_9, value);
	}

	inline static int32_t get_offset_of_btn_10() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___btn_10)); }
	inline Button_t3896396478 * get_btn_10() const { return ___btn_10; }
	inline Button_t3896396478 ** get_address_of_btn_10() { return &___btn_10; }
	inline void set_btn_10(Button_t3896396478 * value)
	{
		___btn_10 = value;
		Il2CppCodeGenWriteBarrier(&___btn_10, value);
	}

	inline static int32_t get_offset_of_id_11() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___id_11)); }
	inline String_t* get_id_11() const { return ___id_11; }
	inline String_t** get_address_of_id_11() { return &___id_11; }
	inline void set_id_11(String_t* value)
	{
		___id_11 = value;
		Il2CppCodeGenWriteBarrier(&___id_11, value);
	}

	inline static int32_t get_offset_of_saveImageSource_12() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___saveImageSource_12)); }
	inline Texture2D_t3884108195 * get_saveImageSource_12() const { return ___saveImageSource_12; }
	inline Texture2D_t3884108195 ** get_address_of_saveImageSource_12() { return &___saveImageSource_12; }
	inline void set_saveImageSource_12(Texture2D_t3884108195 * value)
	{
		___saveImageSource_12 = value;
		Il2CppCodeGenWriteBarrier(&___saveImageSource_12, value);
	}

	inline static int32_t get_offset_of_favoritou_13() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___favoritou_13)); }
	inline bool get_favoritou_13() const { return ___favoritou_13; }
	inline bool* get_address_of_favoritou_13() { return &___favoritou_13; }
	inline void set_favoritou_13(bool value)
	{
		___favoritou_13 = value;
	}

	inline static int32_t get_offset_of_favoritoSprite_14() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___favoritoSprite_14)); }
	inline Sprite_t3199167241 * get_favoritoSprite_14() const { return ___favoritoSprite_14; }
	inline Sprite_t3199167241 ** get_address_of_favoritoSprite_14() { return &___favoritoSprite_14; }
	inline void set_favoritoSprite_14(Sprite_t3199167241 * value)
	{
		___favoritoSprite_14 = value;
		Il2CppCodeGenWriteBarrier(&___favoritoSprite_14, value);
	}

	inline static int32_t get_offset_of_desfavoritouSprite_15() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___desfavoritouSprite_15)); }
	inline Sprite_t3199167241 * get_desfavoritouSprite_15() const { return ___desfavoritouSprite_15; }
	inline Sprite_t3199167241 ** get_address_of_desfavoritouSprite_15() { return &___desfavoritouSprite_15; }
	inline void set_desfavoritouSprite_15(Sprite_t3199167241 * value)
	{
		___desfavoritouSprite_15 = value;
		Il2CppCodeGenWriteBarrier(&___desfavoritouSprite_15, value);
	}

	inline static int32_t get_offset_of_favoritouObj_16() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___favoritouObj_16)); }
	inline GameObject_t3674682005 * get_favoritouObj_16() const { return ___favoritouObj_16; }
	inline GameObject_t3674682005 ** get_address_of_favoritouObj_16() { return &___favoritouObj_16; }
	inline void set_favoritouObj_16(GameObject_t3674682005 * value)
	{
		___favoritouObj_16 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouObj_16, value);
	}

	inline static int32_t get_offset_of_btnFavoritado_17() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___btnFavoritado_17)); }
	inline GameObject_t3674682005 * get_btnFavoritado_17() const { return ___btnFavoritado_17; }
	inline GameObject_t3674682005 ** get_address_of_btnFavoritado_17() { return &___btnFavoritado_17; }
	inline void set_btnFavoritado_17(GameObject_t3674682005 * value)
	{
		___btnFavoritado_17 = value;
		Il2CppCodeGenWriteBarrier(&___btnFavoritado_17, value);
	}

	inline static int32_t get_offset_of_btnFavoritar_18() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___btnFavoritar_18)); }
	inline GameObject_t3674682005 * get_btnFavoritar_18() const { return ___btnFavoritar_18; }
	inline GameObject_t3674682005 ** get_address_of_btnFavoritar_18() { return &___btnFavoritar_18; }
	inline void set_btnFavoritar_18(GameObject_t3674682005 * value)
	{
		___btnFavoritar_18 = value;
		Il2CppCodeGenWriteBarrier(&___btnFavoritar_18, value);
	}

	inline static int32_t get_offset_of_popupSemConexao_19() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___popupSemConexao_19)); }
	inline GameObject_t3674682005 * get_popupSemConexao_19() const { return ___popupSemConexao_19; }
	inline GameObject_t3674682005 ** get_address_of_popupSemConexao_19() { return &___popupSemConexao_19; }
	inline void set_popupSemConexao_19(GameObject_t3674682005 * value)
	{
		___popupSemConexao_19 = value;
		Il2CppCodeGenWriteBarrier(&___popupSemConexao_19, value);
	}

	inline static int32_t get_offset_of_showPopup_20() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___showPopup_20)); }
	inline bool get_showPopup_20() const { return ___showPopup_20; }
	inline bool* get_address_of_showPopup_20() { return &___showPopup_20; }
	inline void set_showPopup_20(bool value)
	{
		___showPopup_20 = value;
	}

	inline static int32_t get_offset_of_link_21() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___link_21)); }
	inline String_t* get_link_21() const { return ___link_21; }
	inline String_t** get_address_of_link_21() { return &___link_21; }
	inline void set_link_21(String_t* value)
	{
		___link_21 = value;
		Il2CppCodeGenWriteBarrier(&___link_21, value);
	}

	inline static int32_t get_offset_of_imgLink_22() { return static_cast<int32_t>(offsetof(OfertaValues_t3488850643, ___imgLink_22)); }
	inline String_t* get_imgLink_22() const { return ___imgLink_22; }
	inline String_t** get_address_of_imgLink_22() { return &___imgLink_22; }
	inline void set_imgLink_22(String_t* value)
	{
		___imgLink_22 = value;
		Il2CppCodeGenWriteBarrier(&___imgLink_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
