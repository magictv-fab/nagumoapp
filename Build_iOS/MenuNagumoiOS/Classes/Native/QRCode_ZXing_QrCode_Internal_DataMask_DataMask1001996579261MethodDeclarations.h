﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask100
struct DataMask100_t1996579261;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask100::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask100_isMasked_m1935952183 (DataMask100_t1996579261 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask100::.ctor()
extern "C"  void DataMask100__ctor_m3215308830 (DataMask100_t1996579261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
