﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DropDownRemoveLastItem
struct DropDownRemoveLastItem_t1997694494;

#include "codegen/il2cpp-codegen.h"

// System.Void DropDownRemoveLastItem::.ctor()
extern "C"  void DropDownRemoveLastItem__ctor_m3755462717 (DropDownRemoveLastItem_t1997694494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropDownRemoveLastItem::Start()
extern "C"  void DropDownRemoveLastItem_Start_m2702600509 (DropDownRemoveLastItem_t1997694494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropDownRemoveLastItem::Update()
extern "C"  void DropDownRemoveLastItem_Update_m2182089328 (DropDownRemoveLastItem_t1997694494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
