﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSNotification
struct OSNotification_t892481775;

#include "codegen/il2cpp-codegen.h"

// System.Void OSNotification::.ctor()
extern "C"  void OSNotification__ctor_m3203020940 (OSNotification_t892481775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
