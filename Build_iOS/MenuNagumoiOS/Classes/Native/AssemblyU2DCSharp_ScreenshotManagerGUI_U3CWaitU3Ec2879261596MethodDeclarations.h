﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManagerGUI/<Wait>c__Iterator2E
struct U3CWaitU3Ec__Iterator2E_t2879261596;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenshotManagerGUI/<Wait>c__Iterator2E::.ctor()
extern "C"  void U3CWaitU3Ec__Iterator2E__ctor_m153199231 (U3CWaitU3Ec__Iterator2E_t2879261596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManagerGUI/<Wait>c__Iterator2E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitU3Ec__Iterator2E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3527590845 (U3CWaitU3Ec__Iterator2E_t2879261596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManagerGUI/<Wait>c__Iterator2E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitU3Ec__Iterator2E_System_Collections_IEnumerator_get_Current_m3278233937 (U3CWaitU3Ec__Iterator2E_t2879261596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenshotManagerGUI/<Wait>c__Iterator2E::MoveNext()
extern "C"  bool U3CWaitU3Ec__Iterator2E_MoveNext_m1367952125 (U3CWaitU3Ec__Iterator2E_t2879261596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI/<Wait>c__Iterator2E::Dispose()
extern "C"  void U3CWaitU3Ec__Iterator2E_Dispose_m1093855996 (U3CWaitU3Ec__Iterator2E_t2879261596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI/<Wait>c__Iterator2E::Reset()
extern "C"  void U3CWaitU3Ec__Iterator2E_Reset_m2094599468 (U3CWaitU3Ec__Iterator2E_t2879261596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
