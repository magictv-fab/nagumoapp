﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Dropdown
struct Dropdown_t4201779933;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImagemScroll
struct  ImagemScroll_t3817704447  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Dropdown ImagemScroll::dropdown
	Dropdown_t4201779933 * ___dropdown_2;
	// UnityEngine.UI.ScrollRect ImagemScroll::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ImagemScroll::infoRegionLst
	List_1_t747900261 * ___infoRegionLst_4;

public:
	inline static int32_t get_offset_of_dropdown_2() { return static_cast<int32_t>(offsetof(ImagemScroll_t3817704447, ___dropdown_2)); }
	inline Dropdown_t4201779933 * get_dropdown_2() const { return ___dropdown_2; }
	inline Dropdown_t4201779933 ** get_address_of_dropdown_2() { return &___dropdown_2; }
	inline void set_dropdown_2(Dropdown_t4201779933 * value)
	{
		___dropdown_2 = value;
		Il2CppCodeGenWriteBarrier(&___dropdown_2, value);
	}

	inline static int32_t get_offset_of_scrollRect_3() { return static_cast<int32_t>(offsetof(ImagemScroll_t3817704447, ___scrollRect_3)); }
	inline ScrollRect_t3606982749 * get_scrollRect_3() const { return ___scrollRect_3; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_3() { return &___scrollRect_3; }
	inline void set_scrollRect_3(ScrollRect_t3606982749 * value)
	{
		___scrollRect_3 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_3, value);
	}

	inline static int32_t get_offset_of_infoRegionLst_4() { return static_cast<int32_t>(offsetof(ImagemScroll_t3817704447, ___infoRegionLst_4)); }
	inline List_1_t747900261 * get_infoRegionLst_4() const { return ___infoRegionLst_4; }
	inline List_1_t747900261 ** get_address_of_infoRegionLst_4() { return &___infoRegionLst_4; }
	inline void set_infoRegionLst_4(List_1_t747900261 * value)
	{
		___infoRegionLst_4 = value;
		Il2CppCodeGenWriteBarrier(&___infoRegionLst_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
