﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SmoothLookAt
struct SmoothLookAt_t4236557480;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::.ctor()
extern "C"  void SmoothLookAt__ctor_m985640638 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::Reset()
extern "C"  void SmoothLookAt_Reset_m2927040875 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::OnEnter()
extern "C"  void SmoothLookAt_OnEnter_m1699576597 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::OnLateUpdate()
extern "C"  void SmoothLookAt_OnLateUpdate_m393560212 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::DoSmoothLookAt()
extern "C"  void SmoothLookAt_DoSmoothLookAt_m3481202801 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
