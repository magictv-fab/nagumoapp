﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.IMB.IMBReader
struct IMBReader_t1813842883;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t728975084;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Char[][]
struct CharU5BU5DU5BU5D_t2611876246;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.IMB.IMBReader::.cctor()
extern "C"  void IMBReader__cctor_m1329610921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.IMB.IMBReader::doDecode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * IMBReader_doDecode_m3349631108 (IMBReader_t1813842883 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.IMB.IMBReader::reset()
extern "C"  void IMBReader_reset_m2967588433 (IMBReader_t1813842883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ZXing.IMB.IMBReader::binaryStringToDec(System.String)
extern "C"  uint16_t IMBReader_binaryStringToDec_m459322678 (IMBReader_t1813842883 * __this, String_t* ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.IMB.IMBReader::invertedBinaryString(System.String)
extern "C"  String_t* IMBReader_invertedBinaryString_m3533515550 (IMBReader_t1813842883 * __this, String_t* ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.IMB.IMBReader::getCodeWords(System.Int32[]&,System.String,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>,System.Int32[][],System.Char[][])
extern "C"  bool IMBReader_getCodeWords_m3617582911 (IMBReader_t1813842883 * __this, Int32U5BU5D_t3230847821** ___codeWord0, String_t* ___imb1, Il2CppObject* ___table1Check2, Il2CppObject* ___table2Check3, Int32U5BU5DU5BU5D_t1820556512* ___barPos4, CharU5BU5DU5BU5D_t2611876246* ___barType5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.IMB.IMBReader::getTrackingNumber(System.String)
extern "C"  String_t* IMBReader_getTrackingNumber_m795034663 (IMBReader_t1813842883 * __this, String_t* ___imb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.IMB.IMBReader::fillLists(ZXing.Common.BitArray,ZXing.Common.BitArray,ZXing.Common.BitArray,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&,System.Int32,System.Int32)
extern "C"  void IMBReader_fillLists_m2681090026 (IMBReader_t1813842883 * __this, BitArray_t4163851164 * ___row0, BitArray_t4163851164 * ___topRow1, BitArray_t4163851164 * ___botRow2, List_1_t2522024052 ** ___listRow3, List_1_t2522024052 ** ___listTop4, List_1_t2522024052 ** ___listBot5, int32_t ___start6, int32_t ___stop7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.IMB.IMBReader::isIMB(ZXing.Common.BitArray,System.Int32&,System.Int32&,System.Int32&)
extern "C"  int32_t IMBReader_isIMB_m2024940456 (IMBReader_t1813842883 * __this, BitArray_t4163851164 * ___row0, int32_t* ___pixelStartOffset1, int32_t* ___pixelStopOffset2, int32_t* ___pixelBarLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.IMB.IMBReader::getNumberBars(ZXing.Common.BitArray,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t IMBReader_getNumberBars_m317809369 (IMBReader_t1813842883 * __this, BitArray_t4163851164 * ___row0, int32_t ___start1, int32_t ___stop2, int32_t ___barWidth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.IMB.IMBReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * IMBReader_decodeRow_m1475236465 (IMBReader_t1813842883 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.IMB.IMBReader::.ctor()
extern "C"  void IMBReader__ctor_m2690841476 (IMBReader_t1813842883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
