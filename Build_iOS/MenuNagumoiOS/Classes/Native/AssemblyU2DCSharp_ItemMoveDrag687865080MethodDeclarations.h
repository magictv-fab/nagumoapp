﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemMoveDrag
struct ItemMoveDrag_t687865080;

#include "codegen/il2cpp-codegen.h"

// System.Void ItemMoveDrag::.ctor()
extern "C"  void ItemMoveDrag__ctor_m2690211 (ItemMoveDrag_t687865080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemMoveDrag::Start()
extern "C"  void ItemMoveDrag_Start_m3244795299 (ItemMoveDrag_t687865080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemMoveDrag::LateUpdate()
extern "C"  void ItemMoveDrag_LateUpdate_m3130633104 (ItemMoveDrag_t687865080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemMoveDrag::OnSelect()
extern "C"  void ItemMoveDrag_OnSelect_m160655676 (ItemMoveDrag_t687865080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemMoveDrag::SelectAnim()
extern "C"  void ItemMoveDrag_SelectAnim_m3902703150 (ItemMoveDrag_t687865080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemMoveDrag::DestroyNow()
extern "C"  void ItemMoveDrag_DestroyNow_m1052522941 (ItemMoveDrag_t687865080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
