﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasCRMDataGroup
struct OfertasCRMDataGroup_t273423897;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasCRMDataGroup::.ctor()
extern "C"  void OfertasCRMDataGroup__ctor_m2646589810 (OfertasCRMDataGroup_t273423897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
