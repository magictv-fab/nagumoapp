﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Cloth
struct Cloth_t4194460560;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Cloth::set_enabled(System.Boolean)
extern "C"  void Cloth_set_enabled_m134879832 (Cloth_t4194460560 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
