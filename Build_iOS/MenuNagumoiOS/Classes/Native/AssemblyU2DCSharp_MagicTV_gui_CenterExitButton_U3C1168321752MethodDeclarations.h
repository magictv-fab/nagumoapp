﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.gui.CenterExitButton/<Delay>c__Iterator39
struct U3CDelayU3Ec__Iterator39_t1168321752;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.gui.CenterExitButton/<Delay>c__Iterator39::.ctor()
extern "C"  void U3CDelayU3Ec__Iterator39__ctor_m887870355 (U3CDelayU3Ec__Iterator39_t1168321752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.gui.CenterExitButton/<Delay>c__Iterator39::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayU3Ec__Iterator39_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1925435615 (U3CDelayU3Ec__Iterator39_t1168321752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.gui.CenterExitButton/<Delay>c__Iterator39::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayU3Ec__Iterator39_System_Collections_IEnumerator_get_Current_m1016689267 (U3CDelayU3Ec__Iterator39_t1168321752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.gui.CenterExitButton/<Delay>c__Iterator39::MoveNext()
extern "C"  bool U3CDelayU3Ec__Iterator39_MoveNext_m1441669761 (U3CDelayU3Ec__Iterator39_t1168321752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.CenterExitButton/<Delay>c__Iterator39::Dispose()
extern "C"  void U3CDelayU3Ec__Iterator39_Dispose_m2738169616 (U3CDelayU3Ec__Iterator39_t1168321752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.CenterExitButton/<Delay>c__Iterator39::Reset()
extern "C"  void U3CDelayU3Ec__Iterator39_Reset_m2829270592 (U3CDelayU3Ec__Iterator39_t1168321752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
