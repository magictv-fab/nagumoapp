﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipExtraData
struct ZipExtraData_t591052775;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Zip.ITaggedData
struct ITaggedData_t3070686385;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::.ctor()
extern "C"  void ZipExtraData__ctor_m103570340 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::.ctor(System.Byte[])
extern "C"  void ZipExtraData__ctor_m1171173029 (ZipExtraData_t591052775 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipExtraData::GetEntryData()
extern "C"  ByteU5BU5D_t4260760469* ZipExtraData_GetEntryData_m165895696 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Clear()
extern "C"  void ZipExtraData_Clear_m1804670927 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_Length()
extern "C"  int32_t ZipExtraData_get_Length_m1929474461 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipExtraData::GetStreamForTag(System.Int32)
extern "C"  Stream_t1561764144 * ZipExtraData_GetStreamForTag_m180546278 (ZipExtraData_t591052775 * __this, int32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ITaggedData ICSharpCode.SharpZipLib.Zip.ZipExtraData::GetData(System.Int16)
extern "C"  Il2CppObject * ZipExtraData_GetData_m633935387 (ZipExtraData_t591052775 * __this, int16_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ITaggedData ICSharpCode.SharpZipLib.Zip.ZipExtraData::Create(System.Int16,System.Byte[],System.Int32,System.Int32)
extern "C"  Il2CppObject * ZipExtraData_Create_m76732252 (Il2CppObject * __this /* static, unused */, int16_t ___tag0, ByteU5BU5D_t4260760469* ___data1, int32_t ___offset2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_ValueLength()
extern "C"  int32_t ZipExtraData_get_ValueLength_m2475774338 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_CurrentReadIndex()
extern "C"  int32_t ZipExtraData_get_CurrentReadIndex_m2742478682 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_UnreadCount()
extern "C"  int32_t ZipExtraData_get_UnreadCount_m2848390699 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Find(System.Int32)
extern "C"  bool ZipExtraData_Find_m3747158302 (ZipExtraData_t591052775 * __this, int32_t ___headerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddEntry(ICSharpCode.SharpZipLib.Zip.ITaggedData)
extern "C"  void ZipExtraData_AddEntry_m335224864 (ZipExtraData_t591052775 * __this, Il2CppObject * ___taggedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddEntry(System.Int32,System.Byte[])
extern "C"  void ZipExtraData_AddEntry_m1984340741 (ZipExtraData_t591052775 * __this, int32_t ___headerID0, ByteU5BU5D_t4260760469* ___fieldData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::StartNewEntry()
extern "C"  void ZipExtraData_StartNewEntry_m1141246966 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddNewEntry(System.Int32)
extern "C"  void ZipExtraData_AddNewEntry_m1336231814 (ZipExtraData_t591052775 * __this, int32_t ___headerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddData(System.Byte)
extern "C"  void ZipExtraData_AddData_m292904862 (ZipExtraData_t591052775 * __this, uint8_t ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddData(System.Byte[])
extern "C"  void ZipExtraData_AddData_m2308749116 (ZipExtraData_t591052775 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeShort(System.Int32)
extern "C"  void ZipExtraData_AddLeShort_m498761299 (ZipExtraData_t591052775 * __this, int32_t ___toAdd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeInt(System.Int32)
extern "C"  void ZipExtraData_AddLeInt_m2855061958 (ZipExtraData_t591052775 * __this, int32_t ___toAdd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeLong(System.Int64)
extern "C"  void ZipExtraData_AddLeLong_m3118658346 (ZipExtraData_t591052775 * __this, int64_t ___toAdd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Delete(System.Int32)
extern "C"  bool ZipExtraData_Delete_m537301200 (ZipExtraData_t591052775 * __this, int32_t ___headerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadLong()
extern "C"  int64_t ZipExtraData_ReadLong_m1475300961 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadInt()
extern "C"  int32_t ZipExtraData_ReadInt_m2304737261 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShort()
extern "C"  int32_t ZipExtraData_ReadShort_m3055337978 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadByte()
extern "C"  int32_t ZipExtraData_ReadByte_m2537191660 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Skip(System.Int32)
extern "C"  void ZipExtraData_Skip_m3496422832 (ZipExtraData_t591052775 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadCheck(System.Int32)
extern "C"  void ZipExtraData_ReadCheck_m2112830501 (ZipExtraData_t591052775 * __this, int32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShortInternal()
extern "C"  int32_t ZipExtraData_ReadShortInternal_m1781244055 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::SetShort(System.Int32&,System.Int32)
extern "C"  void ZipExtraData_SetShort_m3445728246 (ZipExtraData_t591052775 * __this, int32_t* ___index0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Dispose()
extern "C"  void ZipExtraData_Dispose_m645132001 (ZipExtraData_t591052775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
