﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>
struct Dictionary_2_t3719137485;
// System.Collections.Generic.IEqualityComparer`1<ZXing.EncodeHintType>
struct IEqualityComparer_1_t4035597361;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<ZXing.EncodeHintType>
struct ICollection_1_t4139152944;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2982628982;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>
struct IEnumerator_1_t1234815944;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>
struct KeyCollection_t1050929640;
// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.EncodeHintType,System.Object>
struct ValueCollection_t2419743198;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23617918191.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En741493581.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3066107845_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3066107845(__this, method) ((  void (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2__ctor_m3066107845_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m795285573_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m795285573(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m795285573_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m714218783_gshared (Dictionary_2_t3719137485 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m714218783(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3719137485 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m714218783_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3309919503_gshared (Dictionary_2_t3719137485 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3309919503(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3719137485 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3309919503_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m157606602_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m157606602(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m157606602_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1852093836_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1852093836(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1852093836_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4113270650_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4113270650(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4113270650_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m401174031_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m401174031(__this, method) ((  bool (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m401174031_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3653538570_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3653538570(__this, method) ((  bool (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3653538570_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m156866672_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m156866672(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m156866672_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3987078943_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3987078943(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3987078943_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m4099344338_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m4099344338(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m4099344338_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3707782816_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3707782816(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3707782816_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m60856157_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m60856157(__this, ___key0, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m60856157_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2288512180_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2288512180(__this, method) ((  bool (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2288512180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4017140006_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4017140006(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4017140006_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m153148408_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m153148408(__this, method) ((  bool (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m153148408_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3931058547_gshared (Dictionary_2_t3719137485 * __this, KeyValuePair_2_t3617918191  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3931058547(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3719137485 *, KeyValuePair_2_t3617918191 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3931058547_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1355222131_gshared (Dictionary_2_t3719137485 * __this, KeyValuePair_2_t3617918191  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1355222131(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3719137485 *, KeyValuePair_2_t3617918191 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1355222131_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1642842775_gshared (Dictionary_2_t3719137485 * __this, KeyValuePair_2U5BU5D_t2982628982* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1642842775(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3719137485 *, KeyValuePair_2U5BU5D_t2982628982*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1642842775_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1803793496_gshared (Dictionary_2_t3719137485 * __this, KeyValuePair_2_t3617918191  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1803793496(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3719137485 *, KeyValuePair_2_t3617918191 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1803793496_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3321367606_gshared (Dictionary_2_t3719137485 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3321367606(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3321367606_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4015899589_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4015899589(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4015899589_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3535200188_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3535200188(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3535200188_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2577762953_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2577762953(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2577762953_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m489424366_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m489424366(__this, method) ((  int32_t (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_get_Count_m489424366_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m4061444825_gshared (Dictionary_2_t3719137485 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m4061444825(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m4061444825_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m75358734_gshared (Dictionary_2_t3719137485 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m75358734(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3719137485 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m75358734_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2162070598_gshared (Dictionary_2_t3719137485 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2162070598(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3719137485 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2162070598_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2007262257_gshared (Dictionary_2_t3719137485 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2007262257(__this, ___size0, method) ((  void (*) (Dictionary_2_t3719137485 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2007262257_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4238841453_gshared (Dictionary_2_t3719137485 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4238841453(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4238841453_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3617918191  Dictionary_2_make_pair_m3744652545_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3744652545(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3617918191  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3744652545_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1108652445_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1108652445(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1108652445_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2863878585_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2863878585(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2863878585_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1110772930_gshared (Dictionary_2_t3719137485 * __this, KeyValuePair_2U5BU5D_t2982628982* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1110772930(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3719137485 *, KeyValuePair_2U5BU5D_t2982628982*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1110772930_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2791331114_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2791331114(__this, method) ((  void (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_Resize_m2791331114_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1383185985_gshared (Dictionary_2_t3719137485 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1383185985(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3719137485 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1383185985_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1096542457_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1096542457(__this, method) ((  void (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_Clear_m1096542457_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1211804515_gshared (Dictionary_2_t3719137485 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1211804515(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3719137485 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1211804515_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m4273706467_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m4273706467(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m4273706467_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2027955564_gshared (Dictionary_2_t3719137485 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2027955564(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3719137485 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2027955564_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m722933880_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m722933880(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m722933880_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2968173485_gshared (Dictionary_2_t3719137485 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2968173485(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3719137485 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2968173485_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2321739964_gshared (Dictionary_2_t3719137485 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2321739964(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3719137485 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2321739964_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::get_Keys()
extern "C"  KeyCollection_t1050929640 * Dictionary_2_get_Keys_m380521567_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m380521567(__this, method) ((  KeyCollection_t1050929640 * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_get_Keys_m380521567_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::get_Values()
extern "C"  ValueCollection_t2419743198 * Dictionary_2_get_Values_m349874619_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m349874619(__this, method) ((  ValueCollection_t2419743198 * (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_get_Values_m349874619_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m558511352_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m558511352(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m558511352_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1176192724_gshared (Dictionary_2_t3719137485 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1176192724(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3719137485 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1176192724_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m169345424_gshared (Dictionary_2_t3719137485 * __this, KeyValuePair_2_t3617918191  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m169345424(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3719137485 *, KeyValuePair_2_t3617918191 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m169345424_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t741493581  Dictionary_2_GetEnumerator_m223534297_gshared (Dictionary_2_t3719137485 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m223534297(__this, method) ((  Enumerator_t741493581  (*) (Dictionary_2_t3719137485 *, const MethodInfo*))Dictionary_2_GetEnumerator_m223534297_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m1553642062_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m1553642062(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m1553642062_gshared)(__this /* static, unused */, ___key0, ___value1, method)
