﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// GoogleUniversalAnalytics/Delegate_escapeString
struct Delegate_escapeString_t2694597681;
// GoogleUniversalAnalytics
struct GoogleUniversalAnalytics_t2200773556;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// System.IO.StreamReader
struct StreamReader_t2549717843;
// System.IO.StreamWriter
struct StreamWriter_t2705123075;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1615819279;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_HitType1313315314.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_NetAcce4130471992.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleUniversalAnalytics
struct  GoogleUniversalAnalytics_t2200773556  : public Il2CppObject
{
public:
	// System.String GoogleUniversalAnalytics::defaultHitData
	String_t* ___defaultHitData_4;
	// System.Text.StringBuilder GoogleUniversalAnalytics::sb
	StringBuilder_t243639308 * ___sb_5;
	// GoogleUniversalAnalytics/HitType GoogleUniversalAnalytics::hitType
	int32_t ___hitType_6;
	// GoogleUniversalAnalytics/Delegate_escapeString GoogleUniversalAnalytics::escapeString
	Delegate_escapeString_t2694597681 * ___escapeString_7;
	// System.Boolean GoogleUniversalAnalytics::useHTTPS
	bool ___useHTTPS_9;
	// UnityEngine.MonoBehaviour GoogleUniversalAnalytics::helperBehaviour
	MonoBehaviour_t667441552 * ___helperBehaviour_10;
	// System.String GoogleUniversalAnalytics::trackingID
	String_t* ___trackingID_11;
	// System.String GoogleUniversalAnalytics::clientID
	String_t* ___clientID_12;
	// System.String GoogleUniversalAnalytics::userID
	String_t* ___userID_13;
	// System.String GoogleUniversalAnalytics::userIP
	String_t* ___userIP_14;
	// System.String GoogleUniversalAnalytics::userAgent
	String_t* ___userAgent_15;
	// System.String GoogleUniversalAnalytics::appName
	String_t* ___appName_16;
	// System.String GoogleUniversalAnalytics::appVersion
	String_t* ___appVersion_17;
	// System.String GoogleUniversalAnalytics::appID
	String_t* ___appID_18;
	// System.Boolean GoogleUniversalAnalytics::analyticsDisabled_
	bool ___analyticsDisabled__19;
	// System.Boolean GoogleUniversalAnalytics::useOfflineCache
	bool ___useOfflineCache_20;
	// System.String GoogleUniversalAnalytics::offlineCacheFilePath
	String_t* ___offlineCacheFilePath_21;
	// System.IO.StreamReader GoogleUniversalAnalytics::offlineCacheReader
	StreamReader_t2549717843 * ___offlineCacheReader_22;
	// System.IO.StreamWriter GoogleUniversalAnalytics::offlineCacheWriter
	StreamWriter_t2705123075 * ___offlineCacheWriter_23;
	// System.Int32 GoogleUniversalAnalytics::offlineQueueLength
	int32_t ___offlineQueueLength_24;
	// System.Int32 GoogleUniversalAnalytics::offlineQueueSentHits
	int32_t ___offlineQueueSentHits_25;
	// GoogleUniversalAnalytics/NetAccessStatus GoogleUniversalAnalytics::netAccessStatus
	int32_t ___netAccessStatus_26;
	// UnityEngine.WaitForSeconds GoogleUniversalAnalytics::defaultReachabilityCheckPeriod
	WaitForSeconds_t1615819279 * ___defaultReachabilityCheckPeriod_27;
	// UnityEngine.WaitForSeconds GoogleUniversalAnalytics::hitBeingBuiltRetryTime
	WaitForSeconds_t1615819279 * ___hitBeingBuiltRetryTime_28;
	// UnityEngine.WaitForSeconds GoogleUniversalAnalytics::netVerificationErrorRetryTime
	WaitForSeconds_t1615819279 * ___netVerificationErrorRetryTime_29;
	// UnityEngine.WaitForSeconds GoogleUniversalAnalytics::netVerificationMismatchRetryTime
	WaitForSeconds_t1615819279 * ___netVerificationMismatchRetryTime_30;
	// UnityEngine.WaitForSeconds GoogleUniversalAnalytics::cachedHitSendThrottleTime
	WaitForSeconds_t1615819279 * ___cachedHitSendThrottleTime_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleUniversalAnalytics::customHeaders
	Dictionary_2_t827649927 * ___customHeaders_32;
	// System.Nullable`1<System.DateTime> GoogleUniversalAnalytics::epoch
	Nullable_1_t72820554  ___epoch_33;

public:
	inline static int32_t get_offset_of_defaultHitData_4() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___defaultHitData_4)); }
	inline String_t* get_defaultHitData_4() const { return ___defaultHitData_4; }
	inline String_t** get_address_of_defaultHitData_4() { return &___defaultHitData_4; }
	inline void set_defaultHitData_4(String_t* value)
	{
		___defaultHitData_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultHitData_4, value);
	}

	inline static int32_t get_offset_of_sb_5() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___sb_5)); }
	inline StringBuilder_t243639308 * get_sb_5() const { return ___sb_5; }
	inline StringBuilder_t243639308 ** get_address_of_sb_5() { return &___sb_5; }
	inline void set_sb_5(StringBuilder_t243639308 * value)
	{
		___sb_5 = value;
		Il2CppCodeGenWriteBarrier(&___sb_5, value);
	}

	inline static int32_t get_offset_of_hitType_6() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___hitType_6)); }
	inline int32_t get_hitType_6() const { return ___hitType_6; }
	inline int32_t* get_address_of_hitType_6() { return &___hitType_6; }
	inline void set_hitType_6(int32_t value)
	{
		___hitType_6 = value;
	}

	inline static int32_t get_offset_of_escapeString_7() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___escapeString_7)); }
	inline Delegate_escapeString_t2694597681 * get_escapeString_7() const { return ___escapeString_7; }
	inline Delegate_escapeString_t2694597681 ** get_address_of_escapeString_7() { return &___escapeString_7; }
	inline void set_escapeString_7(Delegate_escapeString_t2694597681 * value)
	{
		___escapeString_7 = value;
		Il2CppCodeGenWriteBarrier(&___escapeString_7, value);
	}

	inline static int32_t get_offset_of_useHTTPS_9() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___useHTTPS_9)); }
	inline bool get_useHTTPS_9() const { return ___useHTTPS_9; }
	inline bool* get_address_of_useHTTPS_9() { return &___useHTTPS_9; }
	inline void set_useHTTPS_9(bool value)
	{
		___useHTTPS_9 = value;
	}

	inline static int32_t get_offset_of_helperBehaviour_10() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___helperBehaviour_10)); }
	inline MonoBehaviour_t667441552 * get_helperBehaviour_10() const { return ___helperBehaviour_10; }
	inline MonoBehaviour_t667441552 ** get_address_of_helperBehaviour_10() { return &___helperBehaviour_10; }
	inline void set_helperBehaviour_10(MonoBehaviour_t667441552 * value)
	{
		___helperBehaviour_10 = value;
		Il2CppCodeGenWriteBarrier(&___helperBehaviour_10, value);
	}

	inline static int32_t get_offset_of_trackingID_11() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___trackingID_11)); }
	inline String_t* get_trackingID_11() const { return ___trackingID_11; }
	inline String_t** get_address_of_trackingID_11() { return &___trackingID_11; }
	inline void set_trackingID_11(String_t* value)
	{
		___trackingID_11 = value;
		Il2CppCodeGenWriteBarrier(&___trackingID_11, value);
	}

	inline static int32_t get_offset_of_clientID_12() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___clientID_12)); }
	inline String_t* get_clientID_12() const { return ___clientID_12; }
	inline String_t** get_address_of_clientID_12() { return &___clientID_12; }
	inline void set_clientID_12(String_t* value)
	{
		___clientID_12 = value;
		Il2CppCodeGenWriteBarrier(&___clientID_12, value);
	}

	inline static int32_t get_offset_of_userID_13() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___userID_13)); }
	inline String_t* get_userID_13() const { return ___userID_13; }
	inline String_t** get_address_of_userID_13() { return &___userID_13; }
	inline void set_userID_13(String_t* value)
	{
		___userID_13 = value;
		Il2CppCodeGenWriteBarrier(&___userID_13, value);
	}

	inline static int32_t get_offset_of_userIP_14() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___userIP_14)); }
	inline String_t* get_userIP_14() const { return ___userIP_14; }
	inline String_t** get_address_of_userIP_14() { return &___userIP_14; }
	inline void set_userIP_14(String_t* value)
	{
		___userIP_14 = value;
		Il2CppCodeGenWriteBarrier(&___userIP_14, value);
	}

	inline static int32_t get_offset_of_userAgent_15() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___userAgent_15)); }
	inline String_t* get_userAgent_15() const { return ___userAgent_15; }
	inline String_t** get_address_of_userAgent_15() { return &___userAgent_15; }
	inline void set_userAgent_15(String_t* value)
	{
		___userAgent_15 = value;
		Il2CppCodeGenWriteBarrier(&___userAgent_15, value);
	}

	inline static int32_t get_offset_of_appName_16() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___appName_16)); }
	inline String_t* get_appName_16() const { return ___appName_16; }
	inline String_t** get_address_of_appName_16() { return &___appName_16; }
	inline void set_appName_16(String_t* value)
	{
		___appName_16 = value;
		Il2CppCodeGenWriteBarrier(&___appName_16, value);
	}

	inline static int32_t get_offset_of_appVersion_17() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___appVersion_17)); }
	inline String_t* get_appVersion_17() const { return ___appVersion_17; }
	inline String_t** get_address_of_appVersion_17() { return &___appVersion_17; }
	inline void set_appVersion_17(String_t* value)
	{
		___appVersion_17 = value;
		Il2CppCodeGenWriteBarrier(&___appVersion_17, value);
	}

	inline static int32_t get_offset_of_appID_18() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___appID_18)); }
	inline String_t* get_appID_18() const { return ___appID_18; }
	inline String_t** get_address_of_appID_18() { return &___appID_18; }
	inline void set_appID_18(String_t* value)
	{
		___appID_18 = value;
		Il2CppCodeGenWriteBarrier(&___appID_18, value);
	}

	inline static int32_t get_offset_of_analyticsDisabled__19() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___analyticsDisabled__19)); }
	inline bool get_analyticsDisabled__19() const { return ___analyticsDisabled__19; }
	inline bool* get_address_of_analyticsDisabled__19() { return &___analyticsDisabled__19; }
	inline void set_analyticsDisabled__19(bool value)
	{
		___analyticsDisabled__19 = value;
	}

	inline static int32_t get_offset_of_useOfflineCache_20() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___useOfflineCache_20)); }
	inline bool get_useOfflineCache_20() const { return ___useOfflineCache_20; }
	inline bool* get_address_of_useOfflineCache_20() { return &___useOfflineCache_20; }
	inline void set_useOfflineCache_20(bool value)
	{
		___useOfflineCache_20 = value;
	}

	inline static int32_t get_offset_of_offlineCacheFilePath_21() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___offlineCacheFilePath_21)); }
	inline String_t* get_offlineCacheFilePath_21() const { return ___offlineCacheFilePath_21; }
	inline String_t** get_address_of_offlineCacheFilePath_21() { return &___offlineCacheFilePath_21; }
	inline void set_offlineCacheFilePath_21(String_t* value)
	{
		___offlineCacheFilePath_21 = value;
		Il2CppCodeGenWriteBarrier(&___offlineCacheFilePath_21, value);
	}

	inline static int32_t get_offset_of_offlineCacheReader_22() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___offlineCacheReader_22)); }
	inline StreamReader_t2549717843 * get_offlineCacheReader_22() const { return ___offlineCacheReader_22; }
	inline StreamReader_t2549717843 ** get_address_of_offlineCacheReader_22() { return &___offlineCacheReader_22; }
	inline void set_offlineCacheReader_22(StreamReader_t2549717843 * value)
	{
		___offlineCacheReader_22 = value;
		Il2CppCodeGenWriteBarrier(&___offlineCacheReader_22, value);
	}

	inline static int32_t get_offset_of_offlineCacheWriter_23() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___offlineCacheWriter_23)); }
	inline StreamWriter_t2705123075 * get_offlineCacheWriter_23() const { return ___offlineCacheWriter_23; }
	inline StreamWriter_t2705123075 ** get_address_of_offlineCacheWriter_23() { return &___offlineCacheWriter_23; }
	inline void set_offlineCacheWriter_23(StreamWriter_t2705123075 * value)
	{
		___offlineCacheWriter_23 = value;
		Il2CppCodeGenWriteBarrier(&___offlineCacheWriter_23, value);
	}

	inline static int32_t get_offset_of_offlineQueueLength_24() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___offlineQueueLength_24)); }
	inline int32_t get_offlineQueueLength_24() const { return ___offlineQueueLength_24; }
	inline int32_t* get_address_of_offlineQueueLength_24() { return &___offlineQueueLength_24; }
	inline void set_offlineQueueLength_24(int32_t value)
	{
		___offlineQueueLength_24 = value;
	}

	inline static int32_t get_offset_of_offlineQueueSentHits_25() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___offlineQueueSentHits_25)); }
	inline int32_t get_offlineQueueSentHits_25() const { return ___offlineQueueSentHits_25; }
	inline int32_t* get_address_of_offlineQueueSentHits_25() { return &___offlineQueueSentHits_25; }
	inline void set_offlineQueueSentHits_25(int32_t value)
	{
		___offlineQueueSentHits_25 = value;
	}

	inline static int32_t get_offset_of_netAccessStatus_26() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___netAccessStatus_26)); }
	inline int32_t get_netAccessStatus_26() const { return ___netAccessStatus_26; }
	inline int32_t* get_address_of_netAccessStatus_26() { return &___netAccessStatus_26; }
	inline void set_netAccessStatus_26(int32_t value)
	{
		___netAccessStatus_26 = value;
	}

	inline static int32_t get_offset_of_defaultReachabilityCheckPeriod_27() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___defaultReachabilityCheckPeriod_27)); }
	inline WaitForSeconds_t1615819279 * get_defaultReachabilityCheckPeriod_27() const { return ___defaultReachabilityCheckPeriod_27; }
	inline WaitForSeconds_t1615819279 ** get_address_of_defaultReachabilityCheckPeriod_27() { return &___defaultReachabilityCheckPeriod_27; }
	inline void set_defaultReachabilityCheckPeriod_27(WaitForSeconds_t1615819279 * value)
	{
		___defaultReachabilityCheckPeriod_27 = value;
		Il2CppCodeGenWriteBarrier(&___defaultReachabilityCheckPeriod_27, value);
	}

	inline static int32_t get_offset_of_hitBeingBuiltRetryTime_28() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___hitBeingBuiltRetryTime_28)); }
	inline WaitForSeconds_t1615819279 * get_hitBeingBuiltRetryTime_28() const { return ___hitBeingBuiltRetryTime_28; }
	inline WaitForSeconds_t1615819279 ** get_address_of_hitBeingBuiltRetryTime_28() { return &___hitBeingBuiltRetryTime_28; }
	inline void set_hitBeingBuiltRetryTime_28(WaitForSeconds_t1615819279 * value)
	{
		___hitBeingBuiltRetryTime_28 = value;
		Il2CppCodeGenWriteBarrier(&___hitBeingBuiltRetryTime_28, value);
	}

	inline static int32_t get_offset_of_netVerificationErrorRetryTime_29() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___netVerificationErrorRetryTime_29)); }
	inline WaitForSeconds_t1615819279 * get_netVerificationErrorRetryTime_29() const { return ___netVerificationErrorRetryTime_29; }
	inline WaitForSeconds_t1615819279 ** get_address_of_netVerificationErrorRetryTime_29() { return &___netVerificationErrorRetryTime_29; }
	inline void set_netVerificationErrorRetryTime_29(WaitForSeconds_t1615819279 * value)
	{
		___netVerificationErrorRetryTime_29 = value;
		Il2CppCodeGenWriteBarrier(&___netVerificationErrorRetryTime_29, value);
	}

	inline static int32_t get_offset_of_netVerificationMismatchRetryTime_30() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___netVerificationMismatchRetryTime_30)); }
	inline WaitForSeconds_t1615819279 * get_netVerificationMismatchRetryTime_30() const { return ___netVerificationMismatchRetryTime_30; }
	inline WaitForSeconds_t1615819279 ** get_address_of_netVerificationMismatchRetryTime_30() { return &___netVerificationMismatchRetryTime_30; }
	inline void set_netVerificationMismatchRetryTime_30(WaitForSeconds_t1615819279 * value)
	{
		___netVerificationMismatchRetryTime_30 = value;
		Il2CppCodeGenWriteBarrier(&___netVerificationMismatchRetryTime_30, value);
	}

	inline static int32_t get_offset_of_cachedHitSendThrottleTime_31() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___cachedHitSendThrottleTime_31)); }
	inline WaitForSeconds_t1615819279 * get_cachedHitSendThrottleTime_31() const { return ___cachedHitSendThrottleTime_31; }
	inline WaitForSeconds_t1615819279 ** get_address_of_cachedHitSendThrottleTime_31() { return &___cachedHitSendThrottleTime_31; }
	inline void set_cachedHitSendThrottleTime_31(WaitForSeconds_t1615819279 * value)
	{
		___cachedHitSendThrottleTime_31 = value;
		Il2CppCodeGenWriteBarrier(&___cachedHitSendThrottleTime_31, value);
	}

	inline static int32_t get_offset_of_customHeaders_32() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___customHeaders_32)); }
	inline Dictionary_2_t827649927 * get_customHeaders_32() const { return ___customHeaders_32; }
	inline Dictionary_2_t827649927 ** get_address_of_customHeaders_32() { return &___customHeaders_32; }
	inline void set_customHeaders_32(Dictionary_2_t827649927 * value)
	{
		___customHeaders_32 = value;
		Il2CppCodeGenWriteBarrier(&___customHeaders_32, value);
	}

	inline static int32_t get_offset_of_epoch_33() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556, ___epoch_33)); }
	inline Nullable_1_t72820554  get_epoch_33() const { return ___epoch_33; }
	inline Nullable_1_t72820554 * get_address_of_epoch_33() { return &___epoch_33; }
	inline void set_epoch_33(Nullable_1_t72820554  value)
	{
		___epoch_33 = value;
	}
};

struct GoogleUniversalAnalytics_t2200773556_StaticFields
{
public:
	// System.String[] GoogleUniversalAnalytics::hitTypeNames
	StringU5BU5D_t4054002952* ___hitTypeNames_0;
	// System.String GoogleUniversalAnalytics::httpCollectUrl
	String_t* ___httpCollectUrl_1;
	// System.String GoogleUniversalAnalytics::httpsCollectUrl
	String_t* ___httpsCollectUrl_2;
	// System.String GoogleUniversalAnalytics::guaVersionData
	String_t* ___guaVersionData_3;
	// GoogleUniversalAnalytics GoogleUniversalAnalytics::instance
	GoogleUniversalAnalytics_t2200773556 * ___instance_8;
	// System.String GoogleUniversalAnalytics::offlineQueueLengthPrefKey
	String_t* ___offlineQueueLengthPrefKey_34;
	// System.String GoogleUniversalAnalytics::offlineQueueSentHitsPrefKey
	String_t* ___offlineQueueSentHitsPrefKey_35;
	// System.Char[] GoogleUniversalAnalytics::tabRowSplitter
	CharU5BU5D_t3324145743* ___tabRowSplitter_36;

public:
	inline static int32_t get_offset_of_hitTypeNames_0() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___hitTypeNames_0)); }
	inline StringU5BU5D_t4054002952* get_hitTypeNames_0() const { return ___hitTypeNames_0; }
	inline StringU5BU5D_t4054002952** get_address_of_hitTypeNames_0() { return &___hitTypeNames_0; }
	inline void set_hitTypeNames_0(StringU5BU5D_t4054002952* value)
	{
		___hitTypeNames_0 = value;
		Il2CppCodeGenWriteBarrier(&___hitTypeNames_0, value);
	}

	inline static int32_t get_offset_of_httpCollectUrl_1() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___httpCollectUrl_1)); }
	inline String_t* get_httpCollectUrl_1() const { return ___httpCollectUrl_1; }
	inline String_t** get_address_of_httpCollectUrl_1() { return &___httpCollectUrl_1; }
	inline void set_httpCollectUrl_1(String_t* value)
	{
		___httpCollectUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___httpCollectUrl_1, value);
	}

	inline static int32_t get_offset_of_httpsCollectUrl_2() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___httpsCollectUrl_2)); }
	inline String_t* get_httpsCollectUrl_2() const { return ___httpsCollectUrl_2; }
	inline String_t** get_address_of_httpsCollectUrl_2() { return &___httpsCollectUrl_2; }
	inline void set_httpsCollectUrl_2(String_t* value)
	{
		___httpsCollectUrl_2 = value;
		Il2CppCodeGenWriteBarrier(&___httpsCollectUrl_2, value);
	}

	inline static int32_t get_offset_of_guaVersionData_3() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___guaVersionData_3)); }
	inline String_t* get_guaVersionData_3() const { return ___guaVersionData_3; }
	inline String_t** get_address_of_guaVersionData_3() { return &___guaVersionData_3; }
	inline void set_guaVersionData_3(String_t* value)
	{
		___guaVersionData_3 = value;
		Il2CppCodeGenWriteBarrier(&___guaVersionData_3, value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___instance_8)); }
	inline GoogleUniversalAnalytics_t2200773556 * get_instance_8() const { return ___instance_8; }
	inline GoogleUniversalAnalytics_t2200773556 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(GoogleUniversalAnalytics_t2200773556 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier(&___instance_8, value);
	}

	inline static int32_t get_offset_of_offlineQueueLengthPrefKey_34() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___offlineQueueLengthPrefKey_34)); }
	inline String_t* get_offlineQueueLengthPrefKey_34() const { return ___offlineQueueLengthPrefKey_34; }
	inline String_t** get_address_of_offlineQueueLengthPrefKey_34() { return &___offlineQueueLengthPrefKey_34; }
	inline void set_offlineQueueLengthPrefKey_34(String_t* value)
	{
		___offlineQueueLengthPrefKey_34 = value;
		Il2CppCodeGenWriteBarrier(&___offlineQueueLengthPrefKey_34, value);
	}

	inline static int32_t get_offset_of_offlineQueueSentHitsPrefKey_35() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___offlineQueueSentHitsPrefKey_35)); }
	inline String_t* get_offlineQueueSentHitsPrefKey_35() const { return ___offlineQueueSentHitsPrefKey_35; }
	inline String_t** get_address_of_offlineQueueSentHitsPrefKey_35() { return &___offlineQueueSentHitsPrefKey_35; }
	inline void set_offlineQueueSentHitsPrefKey_35(String_t* value)
	{
		___offlineQueueSentHitsPrefKey_35 = value;
		Il2CppCodeGenWriteBarrier(&___offlineQueueSentHitsPrefKey_35, value);
	}

	inline static int32_t get_offset_of_tabRowSplitter_36() { return static_cast<int32_t>(offsetof(GoogleUniversalAnalytics_t2200773556_StaticFields, ___tabRowSplitter_36)); }
	inline CharU5BU5D_t3324145743* get_tabRowSplitter_36() const { return ___tabRowSplitter_36; }
	inline CharU5BU5D_t3324145743** get_address_of_tabRowSplitter_36() { return &___tabRowSplitter_36; }
	inline void set_tabRowSplitter_36(CharU5BU5D_t3324145743* value)
	{
		___tabRowSplitter_36 = value;
		Il2CppCodeGenWriteBarrier(&___tabRowSplitter_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
