﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendEvent
struct  SendEvent_t113322816  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.SendEvent::eventTarget
	FsmEventTarget_t1823904941 * ___eventTarget_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SendEvent::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendEvent::delay
	FsmFloat_t2134102846 * ___delay_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SendEvent::everyFrame
	bool ___everyFrame_12;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendEvent::delayedEvent
	DelayedEvent_t1938906778 * ___delayedEvent_13;

public:
	inline static int32_t get_offset_of_eventTarget_9() { return static_cast<int32_t>(offsetof(SendEvent_t113322816, ___eventTarget_9)); }
	inline FsmEventTarget_t1823904941 * get_eventTarget_9() const { return ___eventTarget_9; }
	inline FsmEventTarget_t1823904941 ** get_address_of_eventTarget_9() { return &___eventTarget_9; }
	inline void set_eventTarget_9(FsmEventTarget_t1823904941 * value)
	{
		___eventTarget_9 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_9, value);
	}

	inline static int32_t get_offset_of_sendEvent_10() { return static_cast<int32_t>(offsetof(SendEvent_t113322816, ___sendEvent_10)); }
	inline FsmEvent_t2133468028 * get_sendEvent_10() const { return ___sendEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_10() { return &___sendEvent_10; }
	inline void set_sendEvent_10(FsmEvent_t2133468028 * value)
	{
		___sendEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_10, value);
	}

	inline static int32_t get_offset_of_delay_11() { return static_cast<int32_t>(offsetof(SendEvent_t113322816, ___delay_11)); }
	inline FsmFloat_t2134102846 * get_delay_11() const { return ___delay_11; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_11() { return &___delay_11; }
	inline void set_delay_11(FsmFloat_t2134102846 * value)
	{
		___delay_11 = value;
		Il2CppCodeGenWriteBarrier(&___delay_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SendEvent_t113322816, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of_delayedEvent_13() { return static_cast<int32_t>(offsetof(SendEvent_t113322816, ___delayedEvent_13)); }
	inline DelayedEvent_t1938906778 * get_delayedEvent_13() const { return ___delayedEvent_13; }
	inline DelayedEvent_t1938906778 ** get_address_of_delayedEvent_13() { return &___delayedEvent_13; }
	inline void set_delayedEvent_13(DelayedEvent_t1938906778 * value)
	{
		___delayedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___delayedEvent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
