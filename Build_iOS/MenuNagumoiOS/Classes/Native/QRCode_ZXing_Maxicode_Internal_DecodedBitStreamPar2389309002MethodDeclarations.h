﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// ZXing.Common.DecoderResult ZXing.Maxicode.Internal.DecodedBitStreamParser::decode(System.Byte[],System.Int32)
extern "C"  DecoderResult_t3752650303 * DecodedBitStreamParser_decode_m1645679087 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getBit(System.Int32,System.Byte[])
extern "C"  int32_t DecodedBitStreamParser_getBit_m3200546071 (Il2CppObject * __this /* static, unused */, int32_t ___bit0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getInt(System.Byte[],System.Byte[])
extern "C"  int32_t DecodedBitStreamParser_getInt_m3461813575 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, ByteU5BU5D_t4260760469* ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getCountry(System.Byte[])
extern "C"  int32_t DecodedBitStreamParser_getCountry_m1758421821 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getServiceClass(System.Byte[])
extern "C"  int32_t DecodedBitStreamParser_getServiceClass_m909408250 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode2Length(System.Byte[])
extern "C"  int32_t DecodedBitStreamParser_getPostCode2Length_m2067093704 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode2(System.Byte[])
extern "C"  int32_t DecodedBitStreamParser_getPostCode2_m35307790 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode3(System.Byte[])
extern "C"  String_t* DecodedBitStreamParser_getPostCode3_m2341616888 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Maxicode.Internal.DecodedBitStreamParser::getMessage(System.Byte[],System.Int32,System.Int32)
extern "C"  String_t* DecodedBitStreamParser_getMessage_m950087191 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___start1, int32_t ___len2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Maxicode.Internal.DecodedBitStreamParser::.cctor()
extern "C"  void DecodedBitStreamParser__cctor_m1671015463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
