﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"

// System.Int32 HutongGames.PlayMaker.FsmInt::get_Value()
extern "C"  int32_t FsmInt_get_Value_m27059446 (FsmInt_t1596138449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::set_Value(System.Int32)
extern "C"  void FsmInt_set_Value_m2087583461 (FsmInt_t1596138449 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::.ctor()
extern "C"  void FsmInt__ctor_m2389102498 (FsmInt_t1596138449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::.ctor(System.String)
extern "C"  void FsmInt__ctor_m2719794848 (FsmInt_t1596138449 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::.ctor(HutongGames.PlayMaker.FsmInt)
extern "C"  void FsmInt__ctor_m3297861991 (FsmInt_t1596138449 * __this, FsmInt_t1596138449 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmInt::ToString()
extern "C"  String_t* FsmInt_ToString_m2813805553 (FsmInt_t1596138449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmInt::op_Implicit(System.Int32)
extern "C"  FsmInt_t1596138449 * FsmInt_op_Implicit_m1006909518 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
