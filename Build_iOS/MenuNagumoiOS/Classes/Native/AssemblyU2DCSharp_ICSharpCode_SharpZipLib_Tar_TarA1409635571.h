﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Tar.TarInputStream
struct TarInputStream_t1386636699;
// ICSharpCode.SharpZipLib.Tar.TarOutputStream
struct TarOutputStream_t1551862984;
// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler
struct ProgressMessageHandler_t76602726;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarArchive
struct  TarArchive_t1409635571  : public Il2CppObject
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::keepOldFiles
	bool ___keepOldFiles_0;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::asciiTranslate
	bool ___asciiTranslate_1;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarArchive::userId
	int32_t ___userId_2;
	// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::userName
	String_t* ___userName_3;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarArchive::groupId
	int32_t ___groupId_4;
	// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::groupName
	String_t* ___groupName_5;
	// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::rootPath
	String_t* ___rootPath_6;
	// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::pathPrefix
	String_t* ___pathPrefix_7;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::applyUserInfoOverrides
	bool ___applyUserInfoOverrides_8;
	// ICSharpCode.SharpZipLib.Tar.TarInputStream ICSharpCode.SharpZipLib.Tar.TarArchive::tarIn
	TarInputStream_t1386636699 * ___tarIn_9;
	// ICSharpCode.SharpZipLib.Tar.TarOutputStream ICSharpCode.SharpZipLib.Tar.TarArchive::tarOut
	TarOutputStream_t1551862984 * ___tarOut_10;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::isDisposed
	bool ___isDisposed_11;
	// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler ICSharpCode.SharpZipLib.Tar.TarArchive::ProgressMessageEvent
	ProgressMessageHandler_t76602726 * ___ProgressMessageEvent_12;

public:
	inline static int32_t get_offset_of_keepOldFiles_0() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___keepOldFiles_0)); }
	inline bool get_keepOldFiles_0() const { return ___keepOldFiles_0; }
	inline bool* get_address_of_keepOldFiles_0() { return &___keepOldFiles_0; }
	inline void set_keepOldFiles_0(bool value)
	{
		___keepOldFiles_0 = value;
	}

	inline static int32_t get_offset_of_asciiTranslate_1() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___asciiTranslate_1)); }
	inline bool get_asciiTranslate_1() const { return ___asciiTranslate_1; }
	inline bool* get_address_of_asciiTranslate_1() { return &___asciiTranslate_1; }
	inline void set_asciiTranslate_1(bool value)
	{
		___asciiTranslate_1 = value;
	}

	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___userId_2)); }
	inline int32_t get_userId_2() const { return ___userId_2; }
	inline int32_t* get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(int32_t value)
	{
		___userId_2 = value;
	}

	inline static int32_t get_offset_of_userName_3() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___userName_3)); }
	inline String_t* get_userName_3() const { return ___userName_3; }
	inline String_t** get_address_of_userName_3() { return &___userName_3; }
	inline void set_userName_3(String_t* value)
	{
		___userName_3 = value;
		Il2CppCodeGenWriteBarrier(&___userName_3, value);
	}

	inline static int32_t get_offset_of_groupId_4() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___groupId_4)); }
	inline int32_t get_groupId_4() const { return ___groupId_4; }
	inline int32_t* get_address_of_groupId_4() { return &___groupId_4; }
	inline void set_groupId_4(int32_t value)
	{
		___groupId_4 = value;
	}

	inline static int32_t get_offset_of_groupName_5() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___groupName_5)); }
	inline String_t* get_groupName_5() const { return ___groupName_5; }
	inline String_t** get_address_of_groupName_5() { return &___groupName_5; }
	inline void set_groupName_5(String_t* value)
	{
		___groupName_5 = value;
		Il2CppCodeGenWriteBarrier(&___groupName_5, value);
	}

	inline static int32_t get_offset_of_rootPath_6() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___rootPath_6)); }
	inline String_t* get_rootPath_6() const { return ___rootPath_6; }
	inline String_t** get_address_of_rootPath_6() { return &___rootPath_6; }
	inline void set_rootPath_6(String_t* value)
	{
		___rootPath_6 = value;
		Il2CppCodeGenWriteBarrier(&___rootPath_6, value);
	}

	inline static int32_t get_offset_of_pathPrefix_7() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___pathPrefix_7)); }
	inline String_t* get_pathPrefix_7() const { return ___pathPrefix_7; }
	inline String_t** get_address_of_pathPrefix_7() { return &___pathPrefix_7; }
	inline void set_pathPrefix_7(String_t* value)
	{
		___pathPrefix_7 = value;
		Il2CppCodeGenWriteBarrier(&___pathPrefix_7, value);
	}

	inline static int32_t get_offset_of_applyUserInfoOverrides_8() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___applyUserInfoOverrides_8)); }
	inline bool get_applyUserInfoOverrides_8() const { return ___applyUserInfoOverrides_8; }
	inline bool* get_address_of_applyUserInfoOverrides_8() { return &___applyUserInfoOverrides_8; }
	inline void set_applyUserInfoOverrides_8(bool value)
	{
		___applyUserInfoOverrides_8 = value;
	}

	inline static int32_t get_offset_of_tarIn_9() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___tarIn_9)); }
	inline TarInputStream_t1386636699 * get_tarIn_9() const { return ___tarIn_9; }
	inline TarInputStream_t1386636699 ** get_address_of_tarIn_9() { return &___tarIn_9; }
	inline void set_tarIn_9(TarInputStream_t1386636699 * value)
	{
		___tarIn_9 = value;
		Il2CppCodeGenWriteBarrier(&___tarIn_9, value);
	}

	inline static int32_t get_offset_of_tarOut_10() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___tarOut_10)); }
	inline TarOutputStream_t1551862984 * get_tarOut_10() const { return ___tarOut_10; }
	inline TarOutputStream_t1551862984 ** get_address_of_tarOut_10() { return &___tarOut_10; }
	inline void set_tarOut_10(TarOutputStream_t1551862984 * value)
	{
		___tarOut_10 = value;
		Il2CppCodeGenWriteBarrier(&___tarOut_10, value);
	}

	inline static int32_t get_offset_of_isDisposed_11() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___isDisposed_11)); }
	inline bool get_isDisposed_11() const { return ___isDisposed_11; }
	inline bool* get_address_of_isDisposed_11() { return &___isDisposed_11; }
	inline void set_isDisposed_11(bool value)
	{
		___isDisposed_11 = value;
	}

	inline static int32_t get_offset_of_ProgressMessageEvent_12() { return static_cast<int32_t>(offsetof(TarArchive_t1409635571, ___ProgressMessageEvent_12)); }
	inline ProgressMessageHandler_t76602726 * get_ProgressMessageEvent_12() const { return ___ProgressMessageEvent_12; }
	inline ProgressMessageHandler_t76602726 ** get_address_of_ProgressMessageEvent_12() { return &___ProgressMessageEvent_12; }
	inline void set_ProgressMessageEvent_12(ProgressMessageHandler_t76602726 * value)
	{
		___ProgressMessageEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressMessageEvent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
