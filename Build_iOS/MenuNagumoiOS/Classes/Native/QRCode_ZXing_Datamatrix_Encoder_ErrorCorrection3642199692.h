﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Encoder.ErrorCorrection
struct  ErrorCorrection_t3642199692  : public Il2CppObject
{
public:

public:
};

struct ErrorCorrection_t3642199692_StaticFields
{
public:
	// System.Int32[] ZXing.Datamatrix.Encoder.ErrorCorrection::FACTOR_SETS
	Int32U5BU5D_t3230847821* ___FACTOR_SETS_0;
	// System.Int32[][] ZXing.Datamatrix.Encoder.ErrorCorrection::FACTORS
	Int32U5BU5DU5BU5D_t1820556512* ___FACTORS_1;
	// System.Int32[] ZXing.Datamatrix.Encoder.ErrorCorrection::LOG
	Int32U5BU5D_t3230847821* ___LOG_2;
	// System.Int32[] ZXing.Datamatrix.Encoder.ErrorCorrection::ALOG
	Int32U5BU5D_t3230847821* ___ALOG_3;

public:
	inline static int32_t get_offset_of_FACTOR_SETS_0() { return static_cast<int32_t>(offsetof(ErrorCorrection_t3642199692_StaticFields, ___FACTOR_SETS_0)); }
	inline Int32U5BU5D_t3230847821* get_FACTOR_SETS_0() const { return ___FACTOR_SETS_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_FACTOR_SETS_0() { return &___FACTOR_SETS_0; }
	inline void set_FACTOR_SETS_0(Int32U5BU5D_t3230847821* value)
	{
		___FACTOR_SETS_0 = value;
		Il2CppCodeGenWriteBarrier(&___FACTOR_SETS_0, value);
	}

	inline static int32_t get_offset_of_FACTORS_1() { return static_cast<int32_t>(offsetof(ErrorCorrection_t3642199692_StaticFields, ___FACTORS_1)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_FACTORS_1() const { return ___FACTORS_1; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_FACTORS_1() { return &___FACTORS_1; }
	inline void set_FACTORS_1(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___FACTORS_1 = value;
		Il2CppCodeGenWriteBarrier(&___FACTORS_1, value);
	}

	inline static int32_t get_offset_of_LOG_2() { return static_cast<int32_t>(offsetof(ErrorCorrection_t3642199692_StaticFields, ___LOG_2)); }
	inline Int32U5BU5D_t3230847821* get_LOG_2() const { return ___LOG_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_LOG_2() { return &___LOG_2; }
	inline void set_LOG_2(Int32U5BU5D_t3230847821* value)
	{
		___LOG_2 = value;
		Il2CppCodeGenWriteBarrier(&___LOG_2, value);
	}

	inline static int32_t get_offset_of_ALOG_3() { return static_cast<int32_t>(offsetof(ErrorCorrection_t3642199692_StaticFields, ___ALOG_3)); }
	inline Int32U5BU5D_t3230847821* get_ALOG_3() const { return ___ALOG_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_ALOG_3() { return &___ALOG_3; }
	inline void set_ALOG_3(Int32U5BU5D_t3230847821* value)
	{
		___ALOG_3 = value;
		Il2CppCodeGenWriteBarrier(&___ALOG_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
