﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.components.GeneralComponentsObjectAbstract
struct GeneralComponentsObjectAbstract_t1944110717;
// OnEvent
struct OnEvent_t314892251;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnEvent314892251.h"

// System.Void ARM.abstracts.components.GeneralComponentsObjectAbstract::.ctor()
extern "C"  void GeneralComponentsObjectAbstract__ctor_m1768245797 (GeneralComponentsObjectAbstract_t1944110717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.components.GeneralComponentsObjectAbstract::AddCompoentIsReadyEventHandler(OnEvent)
extern "C"  void GeneralComponentsObjectAbstract_AddCompoentIsReadyEventHandler_m680942395 (GeneralComponentsObjectAbstract_t1944110717 * __this, OnEvent_t314892251 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.components.GeneralComponentsObjectAbstract::RemoveCompoentIsReadyEventHandler(OnEvent)
extern "C"  void GeneralComponentsObjectAbstract_RemoveCompoentIsReadyEventHandler_m2096194010 (GeneralComponentsObjectAbstract_t1944110717 * __this, OnEvent_t314892251 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.components.GeneralComponentsObjectAbstract::RaiseComponentIsReady()
extern "C"  void GeneralComponentsObjectAbstract_RaiseComponentIsReady_m2695675947 (GeneralComponentsObjectAbstract_t1944110717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.abstracts.components.GeneralComponentsObjectAbstract::isComponentIsReady()
extern "C"  bool GeneralComponentsObjectAbstract_isComponentIsReady_m1289358393 (GeneralComponentsObjectAbstract_t1944110717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
