﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// OfertasValuesCRM
struct OfertasValuesCRM_t2856949562;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasValuesCRM/<IFavoritar>c__Iterator15
struct  U3CIFavoritarU3Ec__Iterator15_t3323870959  : public Il2CppObject
{
public:
	// System.String OfertasValuesCRM/<IFavoritar>c__Iterator15::<acao>__0
	String_t* ___U3CacaoU3E__0_0;
	// System.String OfertasValuesCRM/<IFavoritar>c__Iterator15::id
	String_t* ___id_1;
	// System.String OfertasValuesCRM/<IFavoritar>c__Iterator15::<json>__1
	String_t* ___U3CjsonU3E__1_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OfertasValuesCRM/<IFavoritar>c__Iterator15::<headers>__2
	Dictionary_2_t827649927 * ___U3CheadersU3E__2_3;
	// System.Byte[] OfertasValuesCRM/<IFavoritar>c__Iterator15::<pData>__3
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__3_4;
	// UnityEngine.WWW OfertasValuesCRM/<IFavoritar>c__Iterator15::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_5;
	// System.Int32 OfertasValuesCRM/<IFavoritar>c__Iterator15::$PC
	int32_t ___U24PC_6;
	// System.Object OfertasValuesCRM/<IFavoritar>c__Iterator15::$current
	Il2CppObject * ___U24current_7;
	// System.String OfertasValuesCRM/<IFavoritar>c__Iterator15::<$>id
	String_t* ___U3CU24U3Eid_8;
	// OfertasValuesCRM OfertasValuesCRM/<IFavoritar>c__Iterator15::<>f__this
	OfertasValuesCRM_t2856949562 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CacaoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CacaoU3E__0_0)); }
	inline String_t* get_U3CacaoU3E__0_0() const { return ___U3CacaoU3E__0_0; }
	inline String_t** get_address_of_U3CacaoU3E__0_0() { return &___U3CacaoU3E__0_0; }
	inline void set_U3CacaoU3E__0_0(String_t* value)
	{
		___U3CacaoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CacaoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_2() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CjsonU3E__1_2)); }
	inline String_t* get_U3CjsonU3E__1_2() const { return ___U3CjsonU3E__1_2; }
	inline String_t** get_address_of_U3CjsonU3E__1_2() { return &___U3CjsonU3E__1_2; }
	inline void set_U3CjsonU3E__1_2(String_t* value)
	{
		___U3CjsonU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__2_3() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CheadersU3E__2_3)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__2_3() const { return ___U3CheadersU3E__2_3; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__2_3() { return &___U3CheadersU3E__2_3; }
	inline void set_U3CheadersU3E__2_3(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__3_4() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CpDataU3E__3_4)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__3_4() const { return ___U3CpDataU3E__3_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__3_4() { return &___U3CpDataU3E__3_4; }
	inline void set_U3CpDataU3E__3_4(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_5() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CwwwU3E__4_5)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_5() const { return ___U3CwwwU3E__4_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_5() { return &___U3CwwwU3E__4_5; }
	inline void set_U3CwwwU3E__4_5(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eid_8() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CU24U3Eid_8)); }
	inline String_t* get_U3CU24U3Eid_8() const { return ___U3CU24U3Eid_8; }
	inline String_t** get_address_of_U3CU24U3Eid_8() { return &___U3CU24U3Eid_8; }
	inline void set_U3CU24U3Eid_8(String_t* value)
	{
		___U3CU24U3Eid_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eid_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator15_t3323870959, ___U3CU3Ef__this_9)); }
	inline OfertasValuesCRM_t2856949562 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline OfertasValuesCRM_t2856949562 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(OfertasValuesCRM_t2856949562 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
