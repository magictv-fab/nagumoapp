﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMovieTexture
struct PlayMovieTexture_t187608383;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMovieTexture::.ctor()
extern "C"  void PlayMovieTexture__ctor_m1349251132 (PlayMovieTexture_t187608383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMovieTexture::Start()
extern "C"  void PlayMovieTexture_Start_m296388924 (PlayMovieTexture_t187608383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMovieTexture::PlayMovieOneShot()
extern "C"  void PlayMovieTexture_PlayMovieOneShot_m4164653196 (PlayMovieTexture_t187608383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMovieTexture::doPlayAudio()
extern "C"  void PlayMovieTexture_doPlayAudio_m4181880241 (PlayMovieTexture_t187608383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMovieTexture::Update()
extern "C"  void PlayMovieTexture_Update_m603974225 (PlayMovieTexture_t187608383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
