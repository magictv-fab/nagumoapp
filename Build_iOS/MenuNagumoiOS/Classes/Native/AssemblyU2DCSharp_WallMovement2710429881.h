﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WallMovement
struct  WallMovement_t2710429881  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject WallMovement::WallUpL
	GameObject_t3674682005 * ___WallUpL_2;
	// UnityEngine.GameObject WallMovement::WallUpR
	GameObject_t3674682005 * ___WallUpR_3;
	// UnityEngine.GameObject WallMovement::WallDownL
	GameObject_t3674682005 * ___WallDownL_4;
	// UnityEngine.GameObject WallMovement::WallDownR
	GameObject_t3674682005 * ___WallDownR_5;

public:
	inline static int32_t get_offset_of_WallUpL_2() { return static_cast<int32_t>(offsetof(WallMovement_t2710429881, ___WallUpL_2)); }
	inline GameObject_t3674682005 * get_WallUpL_2() const { return ___WallUpL_2; }
	inline GameObject_t3674682005 ** get_address_of_WallUpL_2() { return &___WallUpL_2; }
	inline void set_WallUpL_2(GameObject_t3674682005 * value)
	{
		___WallUpL_2 = value;
		Il2CppCodeGenWriteBarrier(&___WallUpL_2, value);
	}

	inline static int32_t get_offset_of_WallUpR_3() { return static_cast<int32_t>(offsetof(WallMovement_t2710429881, ___WallUpR_3)); }
	inline GameObject_t3674682005 * get_WallUpR_3() const { return ___WallUpR_3; }
	inline GameObject_t3674682005 ** get_address_of_WallUpR_3() { return &___WallUpR_3; }
	inline void set_WallUpR_3(GameObject_t3674682005 * value)
	{
		___WallUpR_3 = value;
		Il2CppCodeGenWriteBarrier(&___WallUpR_3, value);
	}

	inline static int32_t get_offset_of_WallDownL_4() { return static_cast<int32_t>(offsetof(WallMovement_t2710429881, ___WallDownL_4)); }
	inline GameObject_t3674682005 * get_WallDownL_4() const { return ___WallDownL_4; }
	inline GameObject_t3674682005 ** get_address_of_WallDownL_4() { return &___WallDownL_4; }
	inline void set_WallDownL_4(GameObject_t3674682005 * value)
	{
		___WallDownL_4 = value;
		Il2CppCodeGenWriteBarrier(&___WallDownL_4, value);
	}

	inline static int32_t get_offset_of_WallDownR_5() { return static_cast<int32_t>(offsetof(WallMovement_t2710429881, ___WallDownR_5)); }
	inline GameObject_t3674682005 * get_WallDownR_5() const { return ___WallDownR_5; }
	inline GameObject_t3674682005 ** get_address_of_WallDownR_5() { return &___WallDownR_5; }
	inline void set_WallDownR_5(GameObject_t3674682005 * value)
	{
		___WallDownR_5 = value;
		Il2CppCodeGenWriteBarrier(&___WallDownR_5, value);
	}
};

struct WallMovement_t2710429881_StaticFields
{
public:
	// System.Single WallMovement::diffY
	float ___diffY_6;

public:
	inline static int32_t get_offset_of_diffY_6() { return static_cast<int32_t>(offsetof(WallMovement_t2710429881_StaticFields, ___diffY_6)); }
	inline float get_diffY_6() const { return ___diffY_6; }
	inline float* get_address_of_diffY_6() { return &___diffY_6; }
	inline void set_diffY_6(float value)
	{
		___diffY_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
