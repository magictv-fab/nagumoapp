﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.DebugToggleOnOff
struct DebugToggleOnOff_t1258336469;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.events.DebugToggleOnOff::.ctor()
extern "C"  void DebugToggleOnOff__ctor_m2531709567 (DebugToggleOnOff_t1258336469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.DebugToggleOnOff::Update()
extern "C"  void DebugToggleOnOff_Update_m2900447342 (DebugToggleOnOff_t1258336469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
