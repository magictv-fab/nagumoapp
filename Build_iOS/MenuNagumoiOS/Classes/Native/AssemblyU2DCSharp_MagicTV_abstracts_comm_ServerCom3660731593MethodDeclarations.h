﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool
struct OnBool_t3660731593;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool::.ctor(System.Object,System.IntPtr)
extern "C"  void OnBool__ctor_m1326895136 (OnBool_t3660731593 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool::Invoke(System.Boolean)
extern "C"  void OnBool_Invoke_m2080095345 (OnBool_t3660731593 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnBool_BeginInvoke_m2661145110 (OnBool_t3660731593 * __this, bool ___success0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool::EndInvoke(System.IAsyncResult)
extern "C"  void OnBool_EndInvoke_m3130479152 (OnBool_t3660731593 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
