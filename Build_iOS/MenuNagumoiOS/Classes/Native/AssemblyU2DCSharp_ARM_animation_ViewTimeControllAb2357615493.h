﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler
struct OnReadyEventHandlerandler_t2819726214;
// ARM.animation.ViewTimeControllAbstract/OnFinishedEvent
struct OnFinishedEvent_t3844636377;

#include "AssemblyU2DCSharp_ARM_abstracts_AViewAbstract2756665534.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.animation.ViewTimeControllAbstract
struct  ViewTimeControllAbstract_t2357615493  : public AViewAbstract_t2756665534
{
public:
	// ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler ARM.animation.ViewTimeControllAbstract::_onReady
	OnReadyEventHandlerandler_t2819726214 * ____onReady_2;
	// System.Boolean ARM.animation.ViewTimeControllAbstract::_isReady
	bool ____isReady_3;
	// ARM.animation.ViewTimeControllAbstract/OnFinishedEvent ARM.animation.ViewTimeControllAbstract::onFinished
	OnFinishedEvent_t3844636377 * ___onFinished_4;
	// System.Boolean ARM.animation.ViewTimeControllAbstract::_isPlaying
	bool ____isPlaying_5;

public:
	inline static int32_t get_offset_of__onReady_2() { return static_cast<int32_t>(offsetof(ViewTimeControllAbstract_t2357615493, ____onReady_2)); }
	inline OnReadyEventHandlerandler_t2819726214 * get__onReady_2() const { return ____onReady_2; }
	inline OnReadyEventHandlerandler_t2819726214 ** get_address_of__onReady_2() { return &____onReady_2; }
	inline void set__onReady_2(OnReadyEventHandlerandler_t2819726214 * value)
	{
		____onReady_2 = value;
		Il2CppCodeGenWriteBarrier(&____onReady_2, value);
	}

	inline static int32_t get_offset_of__isReady_3() { return static_cast<int32_t>(offsetof(ViewTimeControllAbstract_t2357615493, ____isReady_3)); }
	inline bool get__isReady_3() const { return ____isReady_3; }
	inline bool* get_address_of__isReady_3() { return &____isReady_3; }
	inline void set__isReady_3(bool value)
	{
		____isReady_3 = value;
	}

	inline static int32_t get_offset_of_onFinished_4() { return static_cast<int32_t>(offsetof(ViewTimeControllAbstract_t2357615493, ___onFinished_4)); }
	inline OnFinishedEvent_t3844636377 * get_onFinished_4() const { return ___onFinished_4; }
	inline OnFinishedEvent_t3844636377 ** get_address_of_onFinished_4() { return &___onFinished_4; }
	inline void set_onFinished_4(OnFinishedEvent_t3844636377 * value)
	{
		___onFinished_4 = value;
		Il2CppCodeGenWriteBarrier(&___onFinished_4, value);
	}

	inline static int32_t get_offset_of__isPlaying_5() { return static_cast<int32_t>(offsetof(ViewTimeControllAbstract_t2357615493, ____isPlaying_5)); }
	inline bool get__isPlaying_5() const { return ____isPlaying_5; }
	inline bool* get_address_of__isPlaying_5() { return &____isPlaying_5; }
	inline void set__isPlaying_5(bool value)
	{
		____isPlaying_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
