﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m172153843(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m715250781(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, DataEventHandlerFunction_t438967822 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2704628749(__this, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1469075204(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, DataEventHandlerFunction_t438967822 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m522709050(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, DataEventHandlerFunction_t438967822 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3637895370(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3067502768(__this, ___index0, method) ((  DataEventHandlerFunction_t438967822 * (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m546627291(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, DataEventHandlerFunction_t438967822 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3718184981(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1085945634(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m54508337(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2391401772(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1996045358 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2805747248(__this, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3320862996(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1857791364(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1996045358 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m106651639(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1330160593(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4165628423(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1547793864(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3223827066(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m824534915(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m757701398(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2492001665(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m26962446(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::Contains(T)
#define ReadOnlyCollection_1_Contains_m3431970687(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1996045358 *, DataEventHandlerFunction_t438967822 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m2076587661(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1996045358 *, DataEventHandlerFunctionU5BU5D_t3937964539*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2149801430(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m336328729(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1996045358 *, DataEventHandlerFunction_t438967822 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1653563522(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1996045358 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnionAssets.FLE.DataEventHandlerFunction>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m612080240(__this, ___index0, method) ((  DataEventHandlerFunction_t438967822 * (*) (ReadOnlyCollection_1_t1996045358 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
