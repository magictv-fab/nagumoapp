﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.BitSource
struct  BitSource_t1243445190  : public Il2CppObject
{
public:
	// System.Byte[] ZXing.Common.BitSource::bytes
	ByteU5BU5D_t4260760469* ___bytes_0;
	// System.Int32 ZXing.Common.BitSource::byteOffset
	int32_t ___byteOffset_1;
	// System.Int32 ZXing.Common.BitSource::bitOffset
	int32_t ___bitOffset_2;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(BitSource_t1243445190, ___bytes_0)); }
	inline ByteU5BU5D_t4260760469* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_t4260760469** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_t4260760469* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier(&___bytes_0, value);
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(BitSource_t1243445190, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_bitOffset_2() { return static_cast<int32_t>(offsetof(BitSource_t1243445190, ___bitOffset_2)); }
	inline int32_t get_bitOffset_2() const { return ___bitOffset_2; }
	inline int32_t* get_address_of_bitOffset_2() { return &___bitOffset_2; }
	inline void set_bitOffset_2(int32_t value)
	{
		___bitOffset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
