﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t2903303273;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"
#include "mscorlib_System_String7231557.h"

// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
extern "C"  void PlayModeUnityPlayer_LoadNativeLibraries_m1623466838 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
extern "C"  void PlayModeUnityPlayer_InitializePlatform_m3034348139 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
extern "C"  void PlayModeUnityPlayer_InitializeSurface_m658246103 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
extern "C"  int32_t PlayModeUnityPlayer_Start_m3846841497 (PlayModeUnityPlayer_t2903303273 * __this, String_t* ___licenseKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Update()
extern "C"  void PlayModeUnityPlayer_Update_m1208855569 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
extern "C"  void PlayModeUnityPlayer_Dispose_m1019950009 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
extern "C"  void PlayModeUnityPlayer_OnPause_m1315150577 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
extern "C"  void PlayModeUnityPlayer_OnResume_m505548724 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
extern "C"  void PlayModeUnityPlayer_OnDestroy_m2298031477 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C"  void PlayModeUnityPlayer__ctor_m4139710076 (PlayModeUnityPlayer_t2903303273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
