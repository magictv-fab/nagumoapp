﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CopyPositionAngle
struct  CopyPositionAngle_t1779009429  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject CopyPositionAngle::gameObjectToCopy
	GameObject_t3674682005 * ___gameObjectToCopy_2;
	// System.Boolean CopyPositionAngle::globalPosition
	bool ___globalPosition_3;
	// System.Boolean CopyPositionAngle::copyPosition
	bool ___copyPosition_4;
	// System.Boolean CopyPositionAngle::copyAngle
	bool ___copyAngle_5;
	// System.Boolean CopyPositionAngle::_changeOnLateUpdate
	bool ____changeOnLateUpdate_6;

public:
	inline static int32_t get_offset_of_gameObjectToCopy_2() { return static_cast<int32_t>(offsetof(CopyPositionAngle_t1779009429, ___gameObjectToCopy_2)); }
	inline GameObject_t3674682005 * get_gameObjectToCopy_2() const { return ___gameObjectToCopy_2; }
	inline GameObject_t3674682005 ** get_address_of_gameObjectToCopy_2() { return &___gameObjectToCopy_2; }
	inline void set_gameObjectToCopy_2(GameObject_t3674682005 * value)
	{
		___gameObjectToCopy_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToCopy_2, value);
	}

	inline static int32_t get_offset_of_globalPosition_3() { return static_cast<int32_t>(offsetof(CopyPositionAngle_t1779009429, ___globalPosition_3)); }
	inline bool get_globalPosition_3() const { return ___globalPosition_3; }
	inline bool* get_address_of_globalPosition_3() { return &___globalPosition_3; }
	inline void set_globalPosition_3(bool value)
	{
		___globalPosition_3 = value;
	}

	inline static int32_t get_offset_of_copyPosition_4() { return static_cast<int32_t>(offsetof(CopyPositionAngle_t1779009429, ___copyPosition_4)); }
	inline bool get_copyPosition_4() const { return ___copyPosition_4; }
	inline bool* get_address_of_copyPosition_4() { return &___copyPosition_4; }
	inline void set_copyPosition_4(bool value)
	{
		___copyPosition_4 = value;
	}

	inline static int32_t get_offset_of_copyAngle_5() { return static_cast<int32_t>(offsetof(CopyPositionAngle_t1779009429, ___copyAngle_5)); }
	inline bool get_copyAngle_5() const { return ___copyAngle_5; }
	inline bool* get_address_of_copyAngle_5() { return &___copyAngle_5; }
	inline void set_copyAngle_5(bool value)
	{
		___copyAngle_5 = value;
	}

	inline static int32_t get_offset_of__changeOnLateUpdate_6() { return static_cast<int32_t>(offsetof(CopyPositionAngle_t1779009429, ____changeOnLateUpdate_6)); }
	inline bool get__changeOnLateUpdate_6() const { return ____changeOnLateUpdate_6; }
	inline bool* get_address_of__changeOnLateUpdate_6() { return &____changeOnLateUpdate_6; }
	inline void set__changeOnLateUpdate_6(bool value)
	{
		____changeOnLateUpdate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
