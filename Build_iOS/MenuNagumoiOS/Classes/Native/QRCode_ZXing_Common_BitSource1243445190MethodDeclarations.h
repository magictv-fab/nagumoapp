﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.BitSource
struct BitSource_t1243445190;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.Common.BitSource::.ctor(System.Byte[])
extern "C"  void BitSource__ctor_m3123517 (BitSource_t1243445190 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitSource::get_BitOffset()
extern "C"  int32_t BitSource_get_BitOffset_m3500267443 (BitSource_t1243445190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitSource::get_ByteOffset()
extern "C"  int32_t BitSource_get_ByteOffset_m3843776906 (BitSource_t1243445190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitSource::readBits(System.Int32)
extern "C"  int32_t BitSource_readBits_m1087985907 (BitSource_t1243445190 * __this, int32_t ___numBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitSource::available()
extern "C"  int32_t BitSource_available_m1317786949 (BitSource_t1243445190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
