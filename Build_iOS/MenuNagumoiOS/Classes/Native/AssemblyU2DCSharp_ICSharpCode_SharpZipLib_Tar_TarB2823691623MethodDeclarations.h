﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarBuffer
struct TarBuffer_t2823691623;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::.ctor()
extern "C"  void TarBuffer__ctor_m3896844384 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::get_RecordSize()
extern "C"  int32_t TarBuffer_get_RecordSize_m3943885073 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::GetRecordSize()
extern "C"  int32_t TarBuffer_GetRecordSize_m1130926580 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::get_BlockFactor()
extern "C"  int32_t TarBuffer_get_BlockFactor_m3959774559 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::GetBlockFactor()
extern "C"  int32_t TarBuffer_GetBlockFactor_m2657407196 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarBuffer::CreateInputTarBuffer(System.IO.Stream)
extern "C"  TarBuffer_t2823691623 * TarBuffer_CreateInputTarBuffer_m3528233642 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___inputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarBuffer::CreateInputTarBuffer(System.IO.Stream,System.Int32)
extern "C"  TarBuffer_t2823691623 * TarBuffer_CreateInputTarBuffer_m2996547181 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___inputStream0, int32_t ___blockFactor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarBuffer::CreateOutputTarBuffer(System.IO.Stream)
extern "C"  TarBuffer_t2823691623 * TarBuffer_CreateOutputTarBuffer_m3580940677 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___outputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarBuffer::CreateOutputTarBuffer(System.IO.Stream,System.Int32)
extern "C"  TarBuffer_t2823691623 * TarBuffer_CreateOutputTarBuffer_m3649547122 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___outputStream0, int32_t ___blockFactor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::Initialize(System.Int32)
extern "C"  void TarBuffer_Initialize_m3996940677 (TarBuffer_t2823691623 * __this, int32_t ___archiveBlockFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarBuffer::IsEOFBlock(System.Byte[])
extern "C"  bool TarBuffer_IsEOFBlock_m2509331646 (TarBuffer_t2823691623 * __this, ByteU5BU5D_t4260760469* ___block0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarBuffer::IsEndOfArchiveBlock(System.Byte[])
extern "C"  bool TarBuffer_IsEndOfArchiveBlock_m520722212 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___block0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::SkipBlock()
extern "C"  void TarBuffer_SkipBlock_m3274648012 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarBuffer::ReadBlock()
extern "C"  ByteU5BU5D_t4260760469* TarBuffer_ReadBlock_m566037055 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarBuffer::ReadRecord()
extern "C"  bool TarBuffer_ReadRecord_m1822526263 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::get_CurrentBlock()
extern "C"  int32_t TarBuffer_get_CurrentBlock_m4042228659 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarBuffer::get_IsStreamOwner()
extern "C"  bool TarBuffer_get_IsStreamOwner_m2015085074 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::set_IsStreamOwner(System.Boolean)
extern "C"  void TarBuffer_set_IsStreamOwner_m3850298721 (TarBuffer_t2823691623 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::GetCurrentBlockNum()
extern "C"  int32_t TarBuffer_GetCurrentBlockNum_m3105492978 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::get_CurrentRecord()
extern "C"  int32_t TarBuffer_get_CurrentRecord_m620438605 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::GetCurrentRecordNum()
extern "C"  int32_t TarBuffer_GetCurrentRecordNum_m2340370110 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::WriteBlock(System.Byte[])
extern "C"  void TarBuffer_WriteBlock_m186971639 (TarBuffer_t2823691623 * __this, ByteU5BU5D_t4260760469* ___block0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::WriteBlock(System.Byte[],System.Int32)
extern "C"  void TarBuffer_WriteBlock_m1299304512 (TarBuffer_t2823691623 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::WriteRecord()
extern "C"  void TarBuffer_WriteRecord_m485653710 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::WriteFinalRecord()
extern "C"  void TarBuffer_WriteFinalRecord_m1508510316 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarBuffer::Close()
extern "C"  void TarBuffer_Close_m1312736630 (TarBuffer_t2823691623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
