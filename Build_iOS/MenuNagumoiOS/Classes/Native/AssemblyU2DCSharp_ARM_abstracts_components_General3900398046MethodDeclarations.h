﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.components.GeneralComponentsAbstract
struct GeneralComponentsAbstract_t3900398046;
// OnEvent
struct OnEvent_t314892251;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnEvent314892251.h"

// System.Void ARM.abstracts.components.GeneralComponentsAbstract::.ctor()
extern "C"  void GeneralComponentsAbstract__ctor_m376464868 (GeneralComponentsAbstract_t3900398046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.components.GeneralComponentsAbstract::AddCompoentIsReadyEventHandler(OnEvent)
extern "C"  void GeneralComponentsAbstract_AddCompoentIsReadyEventHandler_m2389359354 (GeneralComponentsAbstract_t3900398046 * __this, OnEvent_t314892251 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.components.GeneralComponentsAbstract::RemoveCompoentIsReadyEventHandler(OnEvent)
extern "C"  void GeneralComponentsAbstract_RemoveCompoentIsReadyEventHandler_m2183361979 (GeneralComponentsAbstract_t3900398046 * __this, OnEvent_t314892251 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.components.GeneralComponentsAbstract::RaiseComponentIsReady()
extern "C"  void GeneralComponentsAbstract_RaiseComponentIsReady_m4041113066 (GeneralComponentsAbstract_t3900398046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.abstracts.components.GeneralComponentsAbstract::isComponentIsReady()
extern "C"  bool GeneralComponentsAbstract_isComponentIsReady_m3577091546 (GeneralComponentsAbstract_t3900398046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
