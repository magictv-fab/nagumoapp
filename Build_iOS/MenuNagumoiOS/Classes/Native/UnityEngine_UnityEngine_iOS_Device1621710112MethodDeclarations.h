﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration572715224.h"
#include "mscorlib_System_String7231557.h"

// UnityEngine.iOS.DeviceGeneration UnityEngine.iOS.Device::get_generation()
extern "C"  int32_t Device_get_generation_m1444092920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.Device::SetNoBackupFlag(System.String)
extern "C"  void Device_SetNoBackupFlag_m1297115677 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
