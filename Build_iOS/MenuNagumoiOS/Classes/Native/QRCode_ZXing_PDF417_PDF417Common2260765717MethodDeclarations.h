﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.PDF417.PDF417Common::getBitCountSum(System.Int32[])
extern "C"  int32_t PDF417Common_getBitCountSum_m3046463676 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.PDF417Common::getCodeword(System.Int64)
extern "C"  int32_t PDF417Common_getCodeword_m2418077767 (Il2CppObject * __this /* static, unused */, int64_t ___symbol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417Common::.cctor()
extern "C"  void PDF417Common__cctor_m1395230897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
