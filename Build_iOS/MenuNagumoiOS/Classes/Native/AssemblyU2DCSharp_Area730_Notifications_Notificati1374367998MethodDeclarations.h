﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.NotificationBuilder
struct NotificationBuilder_t1374367998;
// System.String
struct String_t;
// System.Int64[]
struct Int64U5BU5D_t2174042770;
// Area730.Notifications.Notification
struct Notification_t2273303539;
// Area730.Notifications.NotificationInstance
struct NotificationInstance_t3873938600;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notificati3873938600.h"

// System.Void Area730.Notifications.NotificationBuilder::.ctor(System.Int32,System.String,System.String)
extern "C"  void NotificationBuilder__ctor_m3915412305 (NotificationBuilder_t1374367998 * __this, int32_t ___id0, String_t* ___title1, String_t* ___body2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setTitle(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setTitle_m3882293760 (NotificationBuilder_t1374367998 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setBody(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setBody_m2050362152 (NotificationBuilder_t1374367998 * __this, String_t* ___body0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setColor(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setColor_m2575103381 (NotificationBuilder_t1374367998 * __this, String_t* ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setTicker(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setTicker_m2147296896 (NotificationBuilder_t1374367998 * __this, String_t* ___ticker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setRepeating(System.Boolean)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setRepeating_m1754103240 (NotificationBuilder_t1374367998 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setInterval(System.Int64)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setInterval_m553424463 (NotificationBuilder_t1374367998 * __this, int64_t ___interval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setInterval(System.TimeSpan)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setInterval_m3911268095 (NotificationBuilder_t1374367998 * __this, TimeSpan_t413522987  ___intervalTimeSpan0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setGroup(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setGroup_m261133049 (NotificationBuilder_t1374367998 * __this, String_t* ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setSortKey(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setSortKey_m354411415 (NotificationBuilder_t1374367998 * __this, String_t* ___sortKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setAlertOnlyOnce(System.Boolean)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setAlertOnlyOnce_m1146161834 (NotificationBuilder_t1374367998 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setNumber(System.Int32)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setNumber_m565173266 (NotificationBuilder_t1374367998 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setSmallIcon(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setSmallIcon_m1367381432 (NotificationBuilder_t1374367998 * __this, String_t* ___iconResourceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setLargeIcon(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setLargeIcon_m1301953284 (NotificationBuilder_t1374367998 * __this, String_t* ___iconResourceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setDefaults(System.Int32)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setDefaults_m2271370651 (NotificationBuilder_t1374367998 * __this, int32_t ___defaultFlags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setAutoCancel(System.Boolean)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setAutoCancel_m3187915320 (NotificationBuilder_t1374367998 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setId(System.Int32)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setId_m634317284 (NotificationBuilder_t1374367998 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setVibrate(System.Int64[])
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setVibrate_m1169596169 (NotificationBuilder_t1374367998 * __this, Int64U5BU5D_t2174042770* ___pattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setDelay(System.Int64)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setDelay_m3341884927 (NotificationBuilder_t1374367998 * __this, int64_t ___delayMilliseconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setDelay(System.TimeSpan)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setDelay_m1679511887 (NotificationBuilder_t1374367998 * __this, TimeSpan_t413522987  ___delayTimeSpan0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::setSound(System.String)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_setSound_m1444367977 (NotificationBuilder_t1374367998 * __this, String_t* ___soundResourceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Area730.Notifications.NotificationBuilder::ConvertToMillis(System.DateTime)
extern "C"  int64_t NotificationBuilder_ConvertToMillis_m428648111 (NotificationBuilder_t1374367998 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.Notification Area730.Notifications.NotificationBuilder::build()
extern "C"  Notification_t2273303539 * NotificationBuilder_build_m2279400633 (NotificationBuilder_t1374367998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.NotificationBuilder::FromInstance(Area730.Notifications.NotificationInstance)
extern "C"  NotificationBuilder_t1374367998 * NotificationBuilder_FromInstance_m2825241758 (Il2CppObject * __this /* static, unused */, NotificationInstance_t3873938600 * ___notif0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
