﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebViewLogger
struct UniWebViewLogger_t946769945;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UniWebViewLogger_Level2924530158.h"
#include "mscorlib_System_String7231557.h"

// System.Void UniWebViewLogger::.ctor(UniWebViewLogger/Level)
extern "C"  void UniWebViewLogger__ctor_m4155049396 (UniWebViewLogger_t946769945 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniWebViewLogger/Level UniWebViewLogger::get_LogLevel()
extern "C"  int32_t UniWebViewLogger_get_LogLevel_m1529717850 (UniWebViewLogger_t946769945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewLogger::set_LogLevel(UniWebViewLogger/Level)
extern "C"  void UniWebViewLogger_set_LogLevel_m1897221969 (UniWebViewLogger_t946769945 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniWebViewLogger UniWebViewLogger::get_Instance()
extern "C"  UniWebViewLogger_t946769945 * UniWebViewLogger_get_Instance_m968802308 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewLogger::Verbose(System.String)
extern "C"  void UniWebViewLogger_Verbose_m2086395200 (UniWebViewLogger_t946769945 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewLogger::Debug(System.String)
extern "C"  void UniWebViewLogger_Debug_m2819606671 (UniWebViewLogger_t946769945 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewLogger::Info(System.String)
extern "C"  void UniWebViewLogger_Info_m2269464946 (UniWebViewLogger_t946769945 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewLogger::Critical(System.String)
extern "C"  void UniWebViewLogger_Critical_m2054201057 (UniWebViewLogger_t946769945 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewLogger::Log(UniWebViewLogger/Level,System.String)
extern "C"  void UniWebViewLogger_Log_m2868225298 (UniWebViewLogger_t946769945 * __this, int32_t ___level0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
