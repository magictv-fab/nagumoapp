﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRoot
struct GetRoot_t1738725734;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRoot::.ctor()
extern "C"  void GetRoot__ctor_m3740661904 (GetRoot_t1738725734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRoot::Reset()
extern "C"  void GetRoot_Reset_m1387094845 (GetRoot_t1738725734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRoot::OnEnter()
extern "C"  void GetRoot_OnEnter_m3575158887 (GetRoot_t1738725734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRoot::DoGetRoot()
extern "C"  void GetRoot_DoGetRoot_m3774034491 (GetRoot_t1738725734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
