﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.InAppActorAbstract
struct  InAppActorAbstract_t1901198337  : public InAppAbstract_t382673128
{
public:
	// System.Boolean InApp.InAppActorAbstract::_autoPlayOnRun
	bool ____autoPlayOnRun_16;
	// System.Boolean InApp.InAppActorAbstract::_autoHideOnStop
	bool ____autoHideOnStop_17;
	// System.Boolean InApp.InAppActorAbstract::_resetOnStop
	bool ____resetOnStop_18;
	// System.Boolean InApp.InAppActorAbstract::_loop
	bool ____loop_19;

public:
	inline static int32_t get_offset_of__autoPlayOnRun_16() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337, ____autoPlayOnRun_16)); }
	inline bool get__autoPlayOnRun_16() const { return ____autoPlayOnRun_16; }
	inline bool* get_address_of__autoPlayOnRun_16() { return &____autoPlayOnRun_16; }
	inline void set__autoPlayOnRun_16(bool value)
	{
		____autoPlayOnRun_16 = value;
	}

	inline static int32_t get_offset_of__autoHideOnStop_17() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337, ____autoHideOnStop_17)); }
	inline bool get__autoHideOnStop_17() const { return ____autoHideOnStop_17; }
	inline bool* get_address_of__autoHideOnStop_17() { return &____autoHideOnStop_17; }
	inline void set__autoHideOnStop_17(bool value)
	{
		____autoHideOnStop_17 = value;
	}

	inline static int32_t get_offset_of__resetOnStop_18() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337, ____resetOnStop_18)); }
	inline bool get__resetOnStop_18() const { return ____resetOnStop_18; }
	inline bool* get_address_of__resetOnStop_18() { return &____resetOnStop_18; }
	inline void set__resetOnStop_18(bool value)
	{
		____resetOnStop_18 = value;
	}

	inline static int32_t get_offset_of__loop_19() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337, ____loop_19)); }
	inline bool get__loop_19() const { return ____loop_19; }
	inline bool* get_address_of__loop_19() { return &____loop_19; }
	inline void set__loop_19(bool value)
	{
		____loop_19 = value;
	}
};

struct InAppActorAbstract_t1901198337_StaticFields
{
public:
	// System.Boolean InApp.InAppActorAbstract::centerExit
	bool ___centerExit_14;
	// UnityEngine.ScreenOrientation InApp.InAppActorAbstract::defaultOrientation
	int32_t ___defaultOrientation_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> InApp.InAppActorAbstract::<>f__switch$map2
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map2_20;

public:
	inline static int32_t get_offset_of_centerExit_14() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337_StaticFields, ___centerExit_14)); }
	inline bool get_centerExit_14() const { return ___centerExit_14; }
	inline bool* get_address_of_centerExit_14() { return &___centerExit_14; }
	inline void set_centerExit_14(bool value)
	{
		___centerExit_14 = value;
	}

	inline static int32_t get_offset_of_defaultOrientation_15() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337_StaticFields, ___defaultOrientation_15)); }
	inline int32_t get_defaultOrientation_15() const { return ___defaultOrientation_15; }
	inline int32_t* get_address_of_defaultOrientation_15() { return &___defaultOrientation_15; }
	inline void set_defaultOrientation_15(int32_t value)
	{
		___defaultOrientation_15 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_20() { return static_cast<int32_t>(offsetof(InAppActorAbstract_t1901198337_StaticFields, ___U3CU3Ef__switchU24map2_20)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map2_20() const { return ___U3CU3Ef__switchU24map2_20; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map2_20() { return &___U3CU3Ef__switchU24map2_20; }
	inline void set_U3CU3Ef__switchU24map2_20(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map2_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
