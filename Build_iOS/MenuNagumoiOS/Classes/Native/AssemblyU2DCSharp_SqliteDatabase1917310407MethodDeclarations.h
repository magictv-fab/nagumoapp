﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SqliteDatabase
struct SqliteDatabase_t1917310407;
// System.String
struct String_t;
// ReturnDataVO
struct ReturnDataVO_t544837971;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void SqliteDatabase::.ctor(System.String)
extern "C"  void SqliteDatabase__ctor_m2625150158 (SqliteDatabase_t1917310407 * __this, String_t* ___dbName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_open(System.String,System.IntPtr&)
extern "C"  int32_t SqliteDatabase_sqlite3_open_m1552595100 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_close(System.IntPtr)
extern "C"  int32_t SqliteDatabase_sqlite3_close_m3300407888 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_prepare_v2(System.IntPtr,System.String,System.Int32,System.IntPtr&,System.IntPtr)
extern "C"  int32_t SqliteDatabase_sqlite3_prepare_v2_m2953207643 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, String_t* ___zSql1, int32_t ___nByte2, IntPtr_t* ___ppStmpt3, IntPtr_t ___pzTail4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_step(System.IntPtr)
extern "C"  int32_t SqliteDatabase_sqlite3_step_m334738354 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_finalize(System.IntPtr)
extern "C"  int32_t SqliteDatabase_sqlite3_finalize_m4176138912 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SqliteDatabase::sqlite3_errmsg(System.IntPtr)
extern "C"  IntPtr_t SqliteDatabase_sqlite3_errmsg_m432026555 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_column_count(System.IntPtr)
extern "C"  int32_t SqliteDatabase_sqlite3_column_count_m3377650008 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SqliteDatabase::sqlite3_column_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SqliteDatabase_sqlite3_column_name_m1679144924 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_column_type(System.IntPtr,System.Int32)
extern "C"  int32_t SqliteDatabase_sqlite3_column_type_m2722049618 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_column_int(System.IntPtr,System.Int32)
extern "C"  int32_t SqliteDatabase_sqlite3_column_int_m1208173663 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SqliteDatabase::sqlite3_column_text(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SqliteDatabase_sqlite3_column_text_m1542724478 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SqliteDatabase::sqlite3_column_double(System.IntPtr,System.Int32)
extern "C"  double SqliteDatabase_sqlite3_column_double_m1187847092 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SqliteDatabase::sqlite3_column_blob(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SqliteDatabase_sqlite3_column_blob_m1832713710 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SqliteDatabase::sqlite3_column_bytes(System.IntPtr,System.Int32)
extern "C"  int32_t SqliteDatabase_sqlite3_column_bytes_m2398502875 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmHandle0, int32_t ___iCol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SqliteDatabase::get_IsConnectionOpen()
extern "C"  bool SqliteDatabase_get_IsConnectionOpen_m1219164895 (SqliteDatabase_t1917310407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::set_IsConnectionOpen(System.Boolean)
extern "C"  void SqliteDatabase_set_IsConnectionOpen_m888186326 (SqliteDatabase_t1917310407 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SqliteDatabase::get_isReady()
extern "C"  bool SqliteDatabase_get_isReady_m368522158 (SqliteDatabase_t1917310407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::MigrateDataBaseToPersistentData(System.String)
extern "C"  void SqliteDatabase_MigrateDataBaseToPersistentData_m1311872174 (SqliteDatabase_t1917310407 * __this, String_t* ___dbName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::Open()
extern "C"  void SqliteDatabase_Open_m343620570 (SqliteDatabase_t1917310407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::Open(System.String)
extern "C"  void SqliteDatabase_Open_m2518414696 (SqliteDatabase_t1917310407 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::Close()
extern "C"  void SqliteDatabase_Close_m4192086986 (SqliteDatabase_t1917310407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::ExecuteNonQuery(System.String)
extern "C"  void SqliteDatabase_ExecuteNonQuery_m251429664 (SqliteDatabase_t1917310407 * __this, String_t* ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO SqliteDatabase::ExecuteQuery(System.String)
extern "C"  ReturnDataVO_t544837971 * SqliteDatabase_ExecuteQuery_m3179712437 (SqliteDatabase_t1917310407 * __this, String_t* ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::ExecuteScript(System.String)
extern "C"  void SqliteDatabase_ExecuteScript_m257088720 (SqliteDatabase_t1917310407 * __this, String_t* ___script0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SqliteDatabase::Prepare(System.String)
extern "C"  IntPtr_t SqliteDatabase_Prepare_m2041930878 (SqliteDatabase_t1917310407 * __this, String_t* ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SqliteDatabase::Finalize(System.IntPtr)
extern "C"  void SqliteDatabase_Finalize_m2688471526 (SqliteDatabase_t1917310407 * __this, IntPtr_t ___stmHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
