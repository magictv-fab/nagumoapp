﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StaticPinchCameraZoomFilter2892807452.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_ZeroFilter1729114916.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4059889395.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_h673389937.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_plugins_Bu1899048477.h"
#include "AssemblyU2DCSharp_ARM_utils_ARMChecksum599606869.h"
#include "AssemblyU2DCSharp_ARM_utils_ClonePosition1197760764.h"
#include "AssemblyU2DCSharp_ARM_utils_DevicePosition1402958259.h"
#include "AssemblyU2DCSharp_DeviceRotation2938415124.h"
#include "AssemblyU2DCSharp_ARM_utils_Math615367132.h"
#include "AssemblyU2DCSharp_MathScreen3403688372.h"
#include "AssemblyU2DCSharp_ARM_utils_PoolFloat382107510.h"
#include "AssemblyU2DCSharp_ARM_utils_PoolVector3628120362.h"
#include "AssemblyU2DCSharp_SmoothFollow651130655.h"
#include "AssemblyU2DCSharp_ARM_utils_VectorCompare4238853496.h"
#include "AssemblyU2DCSharp_ARM_utils_audio_SavWav443138056.h"
#include "AssemblyU2DCSharp_ByteCompare2690998941.h"
#include "AssemblyU2DCSharp_ByteCompareEvents284362262.h"
#include "AssemblyU2DCSharp_ARM_utils_bytes_ByteListAbstract980263662.h"
#include "AssemblyU2DCSharp_ARM_utils_bytes_ByteListAbstract1972392543.h"
#include "AssemblyU2DCSharp_ARM_utils_Cron615085410.h"
#include "AssemblyU2DCSharp_ARM_utils_CronWatcher1902937828.h"
#include "AssemblyU2DCSharp_ARM_utils_CronWatcher_OnCronEvent343176941.h"
#include "AssemblyU2DCSharp_ARM_utils_cron_EasyTimer3067488385.h"
#include "AssemblyU2DCSharp_ARM_utils_cron_EasyTimerDisposab1727843201.h"
#include "AssemblyU2DCSharp_ARM_utils_cron_EasyTimerDisposabl619398154.h"
#include "AssemblyU2DCSharp_ARM_utils_cron_EasyTimerDisposab1854685057.h"
#include "AssemblyU2DCSharp_ARM_utils_cron_IntervalItem889137048.h"
#include "AssemblyU2DCSharp_ARM_utils_cron_TimeoutItem109981810.h"
#include "AssemblyU2DCSharp_ARM_utils_download_DownloaderAbs3533812091.h"
#include "AssemblyU2DCSharp_ARM_utils_download_DownloaderAbs1994402001.h"
#include "AssemblyU2DCSharp_ARM_utils_download_GameObjectDow2978402602.h"
#include "AssemblyU2DCSharp_ARM_utils_download_GameObjectDown862749232.h"
#include "AssemblyU2DCSharp_ARM_utils_download_GenericDownlo3298305714.h"
#include "AssemblyU2DCSharp_ARM_utils_download_SingleGenericD168833466.h"
#include "AssemblyU2DCSharp_ARM_utils_filters_FilterFloatMin1913916793.h"
#include "AssemblyU2DCSharp_ARM_utils_PoolQuaternion2988316782.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_animation_AutoPla356039163.h"
#include "AssemblyU2DCSharp_BussolaSimuladaDebug1344610386.h"
#include "AssemblyU2DCSharp_CloneRigColliders491736492.h"
#include "AssemblyU2DCSharp_DataRow3107836336.h"
#include "AssemblyU2DCSharp_DataTable1629861412.h"
#include "AssemblyU2DCSharp_DeviceFileInfoDAO152212114.h"
#include "AssemblyU2DCSharp_DeviceFileInfoDAO_OnEvent2458123614.h"
#include "AssemblyU2DCSharp_ReturnDataVO544837971.h"
#include "AssemblyU2DCSharp_SqliteException1149867395.h"
#include "AssemblyU2DCSharp_SqliteDatabase1917310407.h"
#include "AssemblyU2DCSharp_DebugMedianVector33206408051.h"
#include "AssemblyU2DCSharp_DebugTracks138549403.h"
#include "AssemblyU2DCSharp_DebugsConfigGui3114428889.h"
#include "AssemblyU2DCSharp_EnableClothController4016542003.h"
#include "AssemblyU2DCSharp_GUICameraControll1046335215.h"
#include "AssemblyU2DCSharp_MagicApplicationSettings2410043078.h"
#include "AssemblyU2DCSharp_MagicApplicationSettings_U3CSetA3737625415.h"
#include "AssemblyU2DCSharp_MagicApplicationSettings_U3CRese4115973580.h"
#include "AssemblyU2DCSharp_MagicApplicationSettings_U3CRese4115973582.h"
#include "AssemblyU2DCSharp_MagicApplicationSettings_U3CRese4115973579.h"
#include "AssemblyU2DCSharp_MagicApplicationSettings_U3CRese4115973581.h"
#include "AssemblyU2DCSharp_PlayMovieTexture187608383.h"
#include "AssemblyU2DCSharp_PlaySoundController2294540311.h"
#include "AssemblyU2DCSharp_TesteEasyTimer1834723248.h"
#include "AssemblyU2DCSharp_TrackableEventHandler2616932597.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_GUILoaderAbstr2603370330.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppManagerAb3506247605.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerCom3244961639.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerComm239303760.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerCom3660731593.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerCom1521989041.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_BenchmarkAbstr3666376745.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_device_Benchmar940577717.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceCa121423689.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceF3788299492.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceIn471529032.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_Genera2842388060.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc4059313920.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc3624955211.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_MenuSc4225613753.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_MenuScr284867202.h"
#include "AssemblyU2DCSharp_Benchmark2672107741.h"
#include "AssemblyU2DCSharp_CameraToggle3549499129.h"
#include "AssemblyU2DCSharp_ExchangeQuality76539196.h"
#include "AssemblyU2DCSharp_MagicTV_download_BulkBundleDownl2985631149.h"
#include "AssemblyU2DCSharp_MagicTV_download_BulkBundleDownl3421606269.h"
#include "AssemblyU2DCSharp_MagicTV_download_BundleDownloadIt715938369.h"
#include "AssemblyU2DCSharp_MagicTV_download_DeviceCacheFile2756182409.h"
#include "AssemblyU2DCSharp_DownloadImagesQueue2595091377.h"
#include "AssemblyU2DCSharp_DownloadImagesQueue_U3CFinishDown803558192.h"
#include "AssemblyU2DCSharp_FakeSystemAccessControll698691415.h"
#include "AssemblyU2DCSharp_FakeUpdateBundlesProcess1533219132.h"
#include "AssemblyU2DCSharp_FakeUpdateFilesProcess1656501174.h"
#include "AssemblyU2DCSharp_FakeUpdateProcessController4246497901.h"
#include "AssemblyU2DCSharp_FakeUpdateTrackingProcess1137889082.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AppRoot1948717617.h"
#include "AssemblyU2DCSharp_DeviceFileInfo2242857184.h"
#include "AssemblyU2DCSharp_DeviceFileInfo_U3CpopulateIndexe1432591743.h"
#include "AssemblyU2DCSharp_DeviceFileInfo_U3CpopulateIndexe1185851167.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (StaticPinchCameraZoomFilter_t2892807452), -1, sizeof(StaticPinchCameraZoomFilter_t2892807452_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3800[6] = 
{
	StaticPinchCameraZoomFilter_t2892807452_StaticFields::get_offset_of_Instance_2(),
	StaticPinchCameraZoomFilter_t2892807452::get_offset_of_CameraToPinchControl_3(),
	StaticPinchCameraZoomFilter_t2892807452::get_offset_of_ZoomTotal_4(),
	StaticPinchCameraZoomFilter_t2892807452::get_offset_of_ZoomPinch_5(),
	StaticPinchCameraZoomFilter_t2892807452::get_offset_of_ZoomBonus_6(),
	StaticPinchCameraZoomFilter_t2892807452::get_offset_of_InitalCamerasDistance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (ZeroFilter_t1729114916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3801[4] = 
{
	ZeroFilter_t1729114916::get_offset_of_keepHistory_15(),
	ZeroFilter_t1729114916::get_offset_of_arCamera_16(),
	ZeroFilter_t1729114916::get_offset_of__lastHistoryRotation_17(),
	ZeroFilter_t1729114916::get_offset_of__lastHistoryPosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (TransformPositionAngleFilterAbstract_t4158746314), -1, sizeof(TransformPositionAngleFilterAbstract_t4158746314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3802[13] = 
{
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_Alias_2(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_filters_3(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of__hasFilter_4(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_toggle_5(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of__hasToggle_6(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of__active_7(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of__started_8(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_byPass_9(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_activeByDefault_10(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of__initCalled_11(),
	TransformPositionAngleFilterAbstract_t4158746314_StaticFields::get_offset_of__countFilters_12(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_DebugFiltering_13(),
	TransformPositionAngleFilterAbstract_t4158746314::get_offset_of_filterThisFirstChilds_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (HistoryAngleFilterAbstract_t4059889395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (HistoryPositionFilterAbstract_t673389937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (BussolaPluginRotationCalibrate_t1899048477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3806[10] = 
{
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of_bussolaFilter_2(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of_objectReferenceToToggleOn_3(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of__bussolaFilterActive_4(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of_rotationReference_5(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of_timeToAutoOff_6(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of__isActive_7(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of_byPass_8(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of__hasObjects_9(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of__timeout_10(),
	BussolaPluginRotationCalibrate_t1899048477::get_offset_of_DebugCalibrando_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (ARMChecksum_t599606869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (ClonePosition_t1197760764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3808[1] = 
{
	ClonePosition_t1197760764::get_offset_of_someOne_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (DevicePosition_t1402958259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (DeviceRotation_t2938415124), -1, sizeof(DeviceRotation_t2938415124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3810[1] = 
{
	DeviceRotation_t2938415124_StaticFields::get_offset_of_gyroInitialized_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (Math_t615367132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (MathScreen_t3403688372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3812[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (PoolFloat_t382107510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3813[4] = 
{
	PoolFloat_t382107510::get_offset_of_itens_0(),
	PoolFloat_t382107510::get_offset_of__currentIndex_1(),
	PoolFloat_t382107510::get_offset_of__currentSize_2(),
	PoolFloat_t382107510::get_offset_of__maxSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (PoolVector3_t628120362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3814[7] = 
{
	PoolVector3_t628120362::get_offset_of__x_0(),
	PoolVector3_t628120362::get_offset_of__y_1(),
	PoolVector3_t628120362::get_offset_of__z_2(),
	PoolVector3_t628120362::get_offset_of__currentIndex_3(),
	PoolVector3_t628120362::get_offset_of__maxSize_4(),
	PoolVector3_t628120362::get_offset_of__currentSize_5(),
	PoolVector3_t628120362::get_offset_of_itens_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (SmoothFollow_t651130655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3815[1] = 
{
	SmoothFollow_t651130655::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (VectorCompare_t4238853496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (SavWav_t443138056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3817[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (ByteCompare_t2690998941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (ByteCompareEvents_t284362262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[3] = 
{
	ByteCompareEvents_t284362262::get_offset_of_ByteListSource_3(),
	ByteCompareEvents_t284362262::get_offset_of_CaptureRate_4(),
	ByteCompareEvents_t284362262::get_offset_of__historyBytes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (ByteListAbstract_t980263662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3820[3] = 
{
	ByteListAbstract_t980263662::get_offset_of__maxByte_2(),
	ByteListAbstract_t980263662::get_offset_of_IsReady_3(),
	ByteListAbstract_t980263662::get_offset_of_onReadyEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (OnReadyEventHandler_t1972392543), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (Cron_t615085410), -1, sizeof(Cron_t615085410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3822[2] = 
{
	0,
	Cron_t615085410_StaticFields::get_offset_of__CronWatcherReference_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (CronWatcher_t1902937828), -1, sizeof(CronWatcher_t1902937828_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3823[2] = 
{
	CronWatcher_t1902937828_StaticFields::get_offset_of_onUpdate_2(),
	CronWatcher_t1902937828_StaticFields::get_offset_of_onLateUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (OnCronEventHandler_t343176941), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (EasyTimer_t3067488385), -1, sizeof(EasyTimer_t3067488385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3825[2] = 
{
	EasyTimer_t3067488385_StaticFields::get_offset_of__id_2(),
	EasyTimer_t3067488385_StaticFields::get_offset_of_timers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (EasyTimerDisposable_t1727843201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (U3CSetIntervalU3Ec__AnonStorey92_t619398154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3827[1] = 
{
	U3CSetIntervalU3Ec__AnonStorey92_t619398154::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (U3CSetTimeoutU3Ec__AnonStorey93_t1854685057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[1] = 
{
	U3CSetTimeoutU3Ec__AnonStorey93_t1854685057::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (IntervalItem_t889137048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[2] = 
{
	IntervalItem_t889137048::get_offset_of_id_6(),
	IntervalItem_t889137048::get_offset_of_delay_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (TimeoutItem_t109981810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[2] = 
{
	TimeoutItem_t109981810::get_offset_of_id_6(),
	TimeoutItem_t109981810::get_offset_of_delay_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (DownloaderAbstract_t3533812091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3832[1] = 
{
	DownloaderAbstract_t3533812091::get_offset_of_onDownloadComplete_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (OnDownloadCompleteEventHandler_t1994402001), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (GameObjectDownloader_t2978402602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3834[5] = 
{
	GameObjectDownloader_t2978402602::get_offset_of__totalAttempts_15(),
	GameObjectDownloader_t2978402602::get_offset_of__attemptCount_16(),
	GameObjectDownloader_t2978402602::get_offset_of__loader_17(),
	GameObjectDownloader_t2978402602::get_offset_of_requestUrl_18(),
	GameObjectDownloader_t2978402602::get_offset_of__isDisposed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (U3CdoLoadU3Ec__Iterator35_t862749232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3835[4] = 
{
	U3CdoLoadU3Ec__Iterator35_t862749232::get_offset_of_U3CU24s_101U3E__0_0(),
	U3CdoLoadU3Ec__Iterator35_t862749232::get_offset_of_U24PC_1(),
	U3CdoLoadU3Ec__Iterator35_t862749232::get_offset_of_U24current_2(),
	U3CdoLoadU3Ec__Iterator35_t862749232::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (GenericDownloadManager_t3298305714), -1, sizeof(GenericDownloadManager_t3298305714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3836[3] = 
{
	GenericDownloadManager_t3298305714_StaticFields::get_offset_of_realInstanceId_15(),
	GenericDownloadManager_t3298305714::get_offset_of__downloaderInstanceGO_16(),
	GenericDownloadManager_t3298305714::get_offset_of__downloader_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (SingleGenericDownloader_t168833466), -1, sizeof(SingleGenericDownloader_t168833466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3837[5] = 
{
	0,
	SingleGenericDownloader_t168833466_StaticFields::get_offset_of__selfGameObject_19(),
	SingleGenericDownloader_t168833466_StaticFields::get_offset_of__selfInstance_20(),
	SingleGenericDownloader_t168833466::get_offset_of_onBusy_21(),
	SingleGenericDownloader_t168833466::get_offset_of__isDownloading_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (FilterFloatMinMaxSize_t1913916793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (PoolQuaternion_t2988316782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3839[8] = 
{
	PoolQuaternion_t2988316782::get_offset_of__x_0(),
	PoolQuaternion_t2988316782::get_offset_of__y_1(),
	PoolQuaternion_t2988316782::get_offset_of__z_2(),
	PoolQuaternion_t2988316782::get_offset_of__w_3(),
	PoolQuaternion_t2988316782::get_offset_of__currentIndex_4(),
	PoolQuaternion_t2988316782::get_offset_of__maxSize_5(),
	PoolQuaternion_t2988316782::get_offset_of__currentSize_6(),
	PoolQuaternion_t2988316782::get_offset_of_itens_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (AutoPlayController_t356039163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[1] = 
{
	AutoPlayController_t356039163::get_offset_of__amIVisible_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (BussolaSimuladaDebug_t1344610386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[6] = 
{
	BussolaSimuladaDebug_t1344610386::get_offset_of_bussola_2(),
	BussolaSimuladaDebug_t1344610386::get_offset_of_byPass_3(),
	BussolaSimuladaDebug_t1344610386::get_offset_of_sequenciaDeComportamento_4(),
	BussolaSimuladaDebug_t1344610386::get_offset_of_currentIndex_5(),
	BussolaSimuladaDebug_t1344610386::get_offset_of_currentVector_6(),
	BussolaSimuladaDebug_t1344610386::get_offset_of__timePassed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (CloneRigColliders_t491736492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3842[1] = 
{
	CloneRigColliders_t491736492::get_offset_of_ColliderRigToClone_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (DataRow_t3107836336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (DataTable_t1629861412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[3] = 
{
	DataTable_t1629861412::get_offset_of_last_insert_id_0(),
	DataTable_t1629861412::get_offset_of_U3CColumnsU3Ek__BackingField_1(),
	DataTable_t1629861412::get_offset_of_U3CRowsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (DeviceFileInfoDAO_t152212114), -1, sizeof(DeviceFileInfoDAO_t152212114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3845[13] = 
{
	DeviceFileInfoDAO_t152212114::get_offset_of_SQLiteDB_4(),
	DeviceFileInfoDAO_t152212114::get_offset_of__onClick_5(),
	DeviceFileInfoDAO_t152212114::get_offset_of_ativo_6(),
	DeviceFileInfoDAO_t152212114::get_offset_of_showDebug_7(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_10(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_11(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_12(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_13(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_14(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_15(),
	DeviceFileInfoDAO_t152212114_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (OnEvent_t2458123614), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (ReturnDataVO_t544837971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3847[4] = 
{
	ReturnDataVO_t544837971::get_offset_of_success_0(),
	ReturnDataVO_t544837971::get_offset_of_result_1(),
	ReturnDataVO_t544837971::get_offset_of_message_2(),
	ReturnDataVO_t544837971::get_offset_of_hasResult_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (SqliteException_t1149867395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (SqliteDatabase_t1917310407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3849[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SqliteDatabase_t1917310407::get_offset_of_showDebug_8(),
	SqliteDatabase_t1917310407::get_offset_of_CanExQuery_9(),
	SqliteDatabase_t1917310407::get_offset_of__connection_10(),
	SqliteDatabase_t1917310407::get_offset_of_pathDB_11(),
	SqliteDatabase_t1917310407::get_offset_of_U3CIsConnectionOpenU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (DebugMedianVector3_t3206408051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3850[8] = 
{
	DebugMedianVector3_t3206408051::get_offset_of_vec1_2(),
	DebugMedianVector3_t3206408051::get_offset_of_vec2_3(),
	DebugMedianVector3_t3206408051::get_offset_of_vec3_4(),
	DebugMedianVector3_t3206408051::get_offset_of_vec4_5(),
	DebugMedianVector3_t3206408051::get_offset_of_median_6(),
	DebugMedianVector3_t3206408051::get_offset_of_mean_7(),
	DebugMedianVector3_t3206408051::get_offset_of_pool_8(),
	DebugMedianVector3_t3206408051::get_offset_of__loaded_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (DebugTracks_t138549403), -1, sizeof(DebugTracks_t138549403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3851[6] = 
{
	DebugTracks_t138549403_StaticFields::get_offset_of_Instance_2(),
	DebugTracks_t138549403::get_offset_of_active_3(),
	DebugTracks_t138549403::get_offset_of_showMenu_4(),
	DebugTracks_t138549403::get_offset_of_fileInfo_5(),
	DebugTracks_t138549403::get_offset_of_currentPage_6(),
	DebugTracks_t138549403::get_offset_of_totalButtonsPerPage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (DebugsConfigGui_t3114428889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3852[15] = 
{
	DebugsConfigGui_t3114428889::get_offset_of__active_2(),
	DebugsConfigGui_t3114428889::get_offset_of_currentWindow_3(),
	DebugsConfigGui_t3114428889::get_offset_of_historyWindows_4(),
	DebugsConfigGui_t3114428889::get_offset_of__hasConfigurablesDebug_5(),
	DebugsConfigGui_t3114428889::get_offset_of__hasOnOffsDebug_6(),
	DebugsConfigGui_t3114428889::get_offset_of_configurables_7(),
	DebugsConfigGui_t3114428889::get_offset_of_onOffs_8(),
	DebugsConfigGui_t3114428889::get_offset_of__configurableComponents_9(),
	DebugsConfigGui_t3114428889::get_offset_of__onOffs_10(),
	DebugsConfigGui_t3114428889::get_offset_of_gameObjectsToFind_11(),
	DebugsConfigGui_t3114428889::get_offset_of_DebugOnOffComponents_12(),
	DebugsConfigGui_t3114428889::get_offset_of_DebugConfigurableComponents_13(),
	DebugsConfigGui_t3114428889::get_offset_of__buttonsOnsNames_14(),
	DebugsConfigGui_t3114428889::get_offset_of__buttonsConfigNames_15(),
	DebugsConfigGui_t3114428889::get_offset_of__isRootWindow_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (EnableClothController_t4016542003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[1] = 
{
	EnableClothController_t4016542003::get_offset_of_cloth_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (GUICameraControll_t1046335215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[7] = 
{
	GUICameraControll_t1046335215::get_offset_of__pinch_2(),
	GUICameraControll_t1046335215::get_offset_of__zoomFactor_3(),
	GUICameraControll_t1046335215::get_offset_of__rotateFactor_4(),
	GUICameraControll_t1046335215::get_offset_of__verticalFactor_5(),
	GUICameraControll_t1046335215::get_offset_of__sideFactor_6(),
	GUICameraControll_t1046335215::get_offset_of__originalPos_7(),
	GUICameraControll_t1046335215::get_offset_of__originaRot_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (MagicApplicationSettings_t2410043078), -1, sizeof(MagicApplicationSettings_t2410043078_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3855[15] = 
{
	MagicApplicationSettings_t2410043078_StaticFields::get_offset_of_Instance_2(),
	MagicApplicationSettings_t2410043078_StaticFields::get_offset_of_SOFTWARE_VERSION_NAME_3(),
	MagicApplicationSettings_t2410043078_StaticFields::get_offset_of_AppVersionNumber_4(),
	MagicApplicationSettings_t2410043078_StaticFields::get_offset_of_safeFolders_5(),
	MagicApplicationSettings_t2410043078::get_offset_of_GoogleAnalitcsID_6(),
	MagicApplicationSettings_t2410043078::get_offset_of_urlToGetInfo_7(),
	MagicApplicationSettings_t2410043078::get_offset_of_urlToRegisterDevice_8(),
	MagicApplicationSettings_t2410043078::get_offset_of_urlToGetPing_9(),
	MagicApplicationSettings_t2410043078::get_offset_of_app_10(),
	MagicApplicationSettings_t2410043078::get_offset_of_timeoutToleranceMiliseconds_11(),
	MagicApplicationSettings_t2410043078::get_offset_of_AppReady_12(),
	MagicApplicationSettings_t2410043078::get_offset_of_LoadMainScene_13(),
	MagicApplicationSettings_t2410043078::get_offset_of_onAppReady_14(),
	MagicApplicationSettings_t2410043078_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_15(),
	MagicApplicationSettings_t2410043078_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (U3CSetAppReadyU3Ec__Iterator36_t3737625415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[3] = 
{
	U3CSetAppReadyU3Ec__Iterator36_t3737625415::get_offset_of_U24PC_0(),
	U3CSetAppReadyU3Ec__Iterator36_t3737625415::get_offset_of_U24current_1(),
	U3CSetAppReadyU3Ec__Iterator36_t3737625415::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (U3CResetHardU3Ec__AnonStorey95_t4115973580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3857[1] = 
{
	U3CResetHardU3Ec__AnonStorey95_t4115973580::get_offset_of_folderOfThisFile_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (U3CResetHardU3Ec__AnonStorey97_t4115973582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3858[1] = 
{
	U3CResetHardU3Ec__AnonStorey97_t4115973582::get_offset_of_pathTemp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (U3CResetHardU3Ec__AnonStorey94_t4115973579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3859[1] = 
{
	U3CResetHardU3Ec__AnonStorey94_t4115973579::get_offset_of_f_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (U3CResetHardU3Ec__AnonStorey96_t4115973581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3860[1] = 
{
	U3CResetHardU3Ec__AnonStorey96_t4115973581::get_offset_of_f_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (PlayMovieTexture_t187608383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3861[6] = 
{
	PlayMovieTexture_t187608383::get_offset_of_playTarget_2(),
	PlayMovieTexture_t187608383::get_offset_of_playNow_3(),
	PlayMovieTexture_t187608383::get_offset_of_playAudioNow_4(),
	PlayMovieTexture_t187608383::get_offset_of_isLoop_5(),
	PlayMovieTexture_t187608383::get_offset_of_delayAudio_6(),
	PlayMovieTexture_t187608383::get_offset_of_audioSource_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (PlaySoundController_t2294540311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[1] = 
{
	PlaySoundController_t2294540311::get_offset_of_audioSource_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (TesteEasyTimer_t1834723248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (TrackableEventHandler_t2616932597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[4] = 
{
	TrackableEventHandler_t2616932597::get_offset_of_mTrackableBehaviour_2(),
	TrackableEventHandler_t2616932597::get_offset_of_mHasBeenFound_3(),
	TrackableEventHandler_t2616932597::get_offset_of_mLostTracking_4(),
	TrackableEventHandler_t2616932597::get_offset_of_mSecondsSinceLost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (GUILoaderAbstract_t2603370330), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (InAppAbstract_t382673128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3866[10] = 
{
	InAppAbstract_t382673128::get_offset_of__delayToRun_4(),
	InAppAbstract_t382673128::get_offset_of__waitingToRun_5(),
	InAppAbstract_t382673128::get_offset_of__delayFinished_6(),
	InAppAbstract_t382673128::get_offset_of__timerRunningId_7(),
	InAppAbstract_t382673128::get_offset_of__delayResetOnStop_8(),
	InAppAbstract_t382673128::get_offset_of__delayPaused_9(),
	InAppAbstract_t382673128::get_offset_of_DebugMetas_10(),
	InAppAbstract_t382673128::get_offset_of_onQuit_11(),
	InAppAbstract_t382673128::get_offset_of__vo_12(),
	InAppAbstract_t382673128::get_offset_of__analytics_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (InAppManagerAbstract_t3506247605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (ServerCommunicationAbstract_t3244961639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3868[3] = 
{
	ServerCommunicationAbstract_t3244961639::get_offset_of_onUpdateRequestComplete_4(),
	ServerCommunicationAbstract_t3244961639::get_offset_of_onTokenRequestComplete_5(),
	ServerCommunicationAbstract_t3244961639::get_offset_of_onUpdateRequestError_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (OnUpdateRequestCompleteEventHandler_t239303760), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (OnBool_t3660731593), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (OnUpdateRequestErrorEventHandler_t1521989041), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (BenchmarkAbstract_t3666376745), -1, sizeof(BenchmarkAbstract_t3666376745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3872[1] = 
{
	BenchmarkAbstract_t3666376745_StaticFields::get_offset_of_Instance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (BenchmarkInfo_t940577717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3873[1] = 
{
	BenchmarkInfo_t940577717::get_offset_of__quality_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (DeviceCacheFileManagerAbstract_t121423689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (DeviceFileInfoAbstract_t3788299492), -1, sizeof(DeviceFileInfoAbstract_t3788299492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3875[2] = 
{
	DeviceFileInfoAbstract_t3788299492::get_offset_of_tracks_4(),
	DeviceFileInfoAbstract_t3788299492_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (DeviceInfoAbstract_t471529032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (GeneralScreenAbstract_t2842388060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3877[1] = 
{
	GeneralScreenAbstract_t2842388060::get_offset_of__isVisible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (LoadScreenAbstract_t4059313920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[3] = 
{
	LoadScreenAbstract_t4059313920::get_offset_of_onLoadCanceled_5(),
	LoadScreenAbstract_t4059313920::get_offset_of_onLoadConfirmed_6(),
	LoadScreenAbstract_t4059313920::get_offset_of_onLoadConfirm_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (OnLoadAnswerEventHandler_t3624955211), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (MenuScreenAbstract_t4225613753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3880[3] = 
{
	MenuScreenAbstract_t4225613753::get_offset_of_onPrintScreenCalled_5(),
	MenuScreenAbstract_t4225613753::get_offset_of_onClearCacheCalled_6(),
	MenuScreenAbstract_t4225613753::get_offset_of_onResetCalled_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (OnVoidEventHandler_t284867202), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (Benchmark_t2672107741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3882[9] = 
{
	Benchmark_t2672107741::get_offset_of__userDeviceQuality_9(),
	Benchmark_t2672107741::get_offset_of__deviceModel_10(),
	Benchmark_t2672107741::get_offset_of__graphicsMemorySize_11(),
	Benchmark_t2672107741::get_offset_of__processorCount_12(),
	Benchmark_t2672107741::get_offset_of__supportShadows_13(),
	Benchmark_t2672107741::get_offset_of__systemMemorySize_14(),
	Benchmark_t2672107741::get_offset_of__benchmarkInfo_15(),
	Benchmark_t2672107741::get_offset_of__lowPerformanceDevice_16(),
	Benchmark_t2672107741::get_offset_of__midPerformanceDevice_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (CameraToggle_t3549499129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (ExchangeQuality_t76539196), -1, sizeof(ExchangeQuality_t76539196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3884[2] = 
{
	ExchangeQuality_t76539196_StaticFields::get_offset_of__instance_8(),
	ExchangeQuality_t76539196::get_offset_of__quality_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (BulkBundleDownloader_t2985631149), -1, sizeof(BulkBundleDownloader_t2985631149_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3885[14] = 
{
	0,
	0,
	BulkBundleDownloader_t2985631149::get_offset_of_onFileDownloadComplete_16(),
	BulkBundleDownloader_t2985631149::get_offset_of__bundles_17(),
	BulkBundleDownloader_t2985631149::get_offset_of__currentBundle_18(),
	BulkBundleDownloader_t2985631149::get_offset_of__bulkTotalProgress_19(),
	BulkBundleDownloader_t2985631149::get_offset_of__itensToDownload_20(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of__cancelBulk_21(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of_alert_i_22(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of_alert_a_23(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of__currentBulkBundleDownloaderGO_24(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of__instanceBulkBundleDownloader_25(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of__currentGerericDonwloadManagerGO_26(),
	BulkBundleDownloader_t2985631149_StaticFields::get_offset_of__instanceGenericDownload_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (OnFileDonwloadCompleteEventHandler_t3421606269), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (BundleDownloadItem_t715938369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3887[3] = 
{
	BundleDownloadItem_t715938369::get_offset_of_fileDestination_0(),
	BundleDownloadItem_t715938369::get_offset_of_UrlInfoVO_1(),
	BundleDownloadItem_t715938369::get_offset_of_fileWeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (DeviceCacheFileManager_t2756182409), -1, sizeof(DeviceCacheFileManager_t2756182409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3888[7] = 
{
	DeviceCacheFileManager_t2756182409::get_offset_of_internetInfo_14(),
	DeviceCacheFileManager_t2756182409::get_offset_of__revisionToCache_15(),
	DeviceCacheFileManager_t2756182409::get_offset_of__deviceFileInfo_16(),
	DeviceCacheFileManager_t2756182409::get_offset_of__lastMessageVO_17(),
	DeviceCacheFileManager_t2756182409_StaticFields::get_offset_of_Instance_18(),
	DeviceCacheFileManager_t2756182409::get_offset_of_DebugUrlCachedToDelete_19(),
	DeviceCacheFileManager_t2756182409_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (DownloadImagesQueue_t2595091377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3889[5] = 
{
	DownloadImagesQueue_t2595091377::get_offset_of__images_14(),
	DownloadImagesQueue_t2595091377::get_offset_of_totalBytes_15(),
	DownloadImagesQueue_t2595091377::get_offset_of_totalBytesLoaded_16(),
	DownloadImagesQueue_t2595091377::get_offset_of__paths_17(),
	DownloadImagesQueue_t2595091377::get_offset_of_currentDownloadIndex_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (U3CFinishDownloadU3Ec__Iterator37_t803558192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3890[6] = 
{
	U3CFinishDownloadU3Ec__Iterator37_t803558192::get_offset_of_url_0(),
	U3CFinishDownloadU3Ec__Iterator37_t803558192::get_offset_of_U3Chs_getU3E__0_1(),
	U3CFinishDownloadU3Ec__Iterator37_t803558192::get_offset_of_U24PC_2(),
	U3CFinishDownloadU3Ec__Iterator37_t803558192::get_offset_of_U24current_3(),
	U3CFinishDownloadU3Ec__Iterator37_t803558192::get_offset_of_U3CU24U3Eurl_4(),
	U3CFinishDownloadU3Ec__Iterator37_t803558192::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (FakeSystemAccessControll_t698691415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (FakeUpdateBundlesProcess_t1533219132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (FakeUpdateFilesProcess_t1656501174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (FakeUpdateProcessController_t4246497901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (FakeUpdateTrackingProcess_t1137889082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (AppRoot_t1948717617), -1, sizeof(AppRoot_t1948717617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3896[15] = 
{
	0,
	0,
	0,
	0,
	0,
	AppRoot_t1948717617::get_offset_of_info_5(),
	AppRoot_t1948717617_StaticFields::get_offset_of__stateMachine_6(),
	AppRoot_t1948717617_StaticFields::get_offset_of__device_id_7(),
	AppRoot_t1948717617_StaticFields::get_offset_of_connection_8(),
	AppRoot_t1948717617_StaticFields::get_offset_of_canTrack_9(),
	AppRoot_t1948717617_StaticFields::get_offset_of_revision_10(),
	AppRoot_t1948717617_StaticFields::get_offset_of_deviceInfo_11(),
	AppRoot_t1948717617_StaticFields::get_offset_of_user_agent_12(),
	AppRoot_t1948717617_StaticFields::get_offset_of_cacheFileManager_13(),
	AppRoot_t1948717617_StaticFields::get_offset_of_deviceFileInfo_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (DeviceFileInfo_t2242857184), -1, sizeof(DeviceFileInfo_t2242857184_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3897[20] = 
{
	DeviceFileInfo_t2242857184::get_offset_of__ResultRevisionVO_6(),
	DeviceFileInfo_t2242857184::get_offset_of__DeviceFileInfoDAO_7(),
	DeviceFileInfo_t2242857184::get_offset_of__indexedBundles_8(),
	DeviceFileInfo_t2242857184::get_offset_of__indexedBundlesArray_9(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_instance_10(),
	DeviceFileInfo_t2242857184::get_offset_of_hashVarName_11(),
	DeviceFileInfo_t2242857184::get_offset_of__debugClicked_12(),
	DeviceFileInfo_t2242857184::get_offset_of_KEY_TOTAL_IMAGE_13(),
	DeviceFileInfo_t2242857184::get_offset_of_KEY_LOCAL_IMAGE_URL__14(),
	DeviceFileInfo_t2242857184::get_offset_of_TAG_LAST_INDEX_IMAGE_SORTED_15(),
	DeviceFileInfo_t2242857184::get_offset_of_initialImages_16(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_17(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_18(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_19(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_20(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_21(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_22(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_23(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_24(),
	DeviceFileInfo_t2242857184_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3898[2] = 
{
	U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743::get_offset_of_revision_0(),
	U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3899[2] = 
{
	U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167::get_offset_of_t_0(),
	U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167::get_offset_of_U3CU3Ef__refU24152_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
