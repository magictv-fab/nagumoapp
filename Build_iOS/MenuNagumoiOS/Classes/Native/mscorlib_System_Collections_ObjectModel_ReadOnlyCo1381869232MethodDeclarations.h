﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m2195406817(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m392862027(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, FinderPattern_t4119758992 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1300511967(__this, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1245461106(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, FinderPattern_t4119758992 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3264499272(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, FinderPattern_t4119758992 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3414281272(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2427844412(__this, ___index0, method) ((  FinderPattern_t4119758992 * (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m401843913(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, FinderPattern_t4119758992 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m786714183(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1517770768(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2268709643(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1850816454(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1381869232 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3385370398(__this, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3102105030(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2667846430(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1381869232 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1764549193(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m56083519(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3323944793(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4289584086(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3088644418(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m56711605(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m594385572(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1430495817(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4128612320(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1530250125(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1381869232 *, FinderPattern_t4119758992 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m3241762171(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1381869232 *, FinderPatternU5BU5D_t1610216241*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m3754915184(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2680488511(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1381869232 *, FinderPattern_t4119758992 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1308072732(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1381869232 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.QrCode.Internal.FinderPattern>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1377404668(__this, ___index0, method) ((  FinderPattern_t4119758992 * (*) (ReadOnlyCollection_1_t1381869232 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
