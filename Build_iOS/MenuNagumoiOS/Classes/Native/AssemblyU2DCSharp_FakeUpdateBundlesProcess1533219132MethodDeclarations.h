﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeUpdateBundlesProcess
struct FakeUpdateBundlesProcess_t1533219132;

#include "codegen/il2cpp-codegen.h"

// System.Void FakeUpdateBundlesProcess::.ctor()
extern "C"  void FakeUpdateBundlesProcess__ctor_m1727537375 (FakeUpdateBundlesProcess_t1533219132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateBundlesProcess::prepare()
extern "C"  void FakeUpdateBundlesProcess_prepare_m3233021540 (FakeUpdateBundlesProcess_t1533219132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateBundlesProcess::Play()
extern "C"  void FakeUpdateBundlesProcess_Play_m205587129 (FakeUpdateBundlesProcess_t1533219132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateBundlesProcess::Stop()
extern "C"  void FakeUpdateBundlesProcess_Stop_m299271175 (FakeUpdateBundlesProcess_t1533219132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateBundlesProcess::Pause()
extern "C"  void FakeUpdateBundlesProcess_Pause_m1781663347 (FakeUpdateBundlesProcess_t1533219132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
