﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertaValues/<ExcluirFavorito>c__Iterator22
struct U3CExcluirFavoritoU3Ec__Iterator22_t3856895931;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertaValues/<ExcluirFavorito>c__Iterator22::.ctor()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator22__ctor_m3484995088 (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertaValues/<ExcluirFavorito>c__Iterator22::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m551487170 (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertaValues/<ExcluirFavorito>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m1143656022 (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertaValues/<ExcluirFavorito>c__Iterator22::MoveNext()
extern "C"  bool U3CExcluirFavoritoU3Ec__Iterator22_MoveNext_m2622555108 (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues/<ExcluirFavorito>c__Iterator22::Dispose()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator22_Dispose_m3199039053 (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues/<ExcluirFavorito>c__Iterator22::Reset()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator22_Reset_m1131428029 (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
