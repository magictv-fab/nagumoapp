﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CagadorDeCubo
struct CagadorDeCubo_t3423560111;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void CagadorDeCubo::.ctor()
extern "C"  void CagadorDeCubo__ctor_m228357276 (CagadorDeCubo_t3423560111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CagadorDeCubo::Start()
extern "C"  void CagadorDeCubo_Start_m3470462364 (CagadorDeCubo_t3423560111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CagadorDeCubo::CagaCubo(UnityEngine.Vector3)
extern "C"  void CagadorDeCubo_CagaCubo_m570155898 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CagadorDeCubo::Update()
extern "C"  void CagadorDeCubo_Update_m216003057 (CagadorDeCubo_t3423560111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
