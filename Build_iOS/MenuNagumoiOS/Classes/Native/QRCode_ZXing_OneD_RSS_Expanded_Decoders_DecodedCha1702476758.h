﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_DecodedObje746289471.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedChar
struct  DecodedChar_t1702476758  : public DecodedObject_t746289471
{
public:
	// System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::value
	Il2CppChar ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DecodedChar_t1702476758, ___value_1)); }
	inline Il2CppChar get_value_1() const { return ___value_1; }
	inline Il2CppChar* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Il2CppChar value)
	{
		___value_1 = value;
	}
};

struct DecodedChar_t1702476758_StaticFields
{
public:
	// System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::FNC1
	Il2CppChar ___FNC1_2;

public:
	inline static int32_t get_offset_of_FNC1_2() { return static_cast<int32_t>(offsetof(DecodedChar_t1702476758_StaticFields, ___FNC1_2)); }
	inline Il2CppChar get_FNC1_2() const { return ___FNC1_2; }
	inline Il2CppChar* get_address_of_FNC1_2() { return &___FNC1_2; }
	inline void set_FNC1_2(Il2CppChar value)
	{
		___FNC1_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
