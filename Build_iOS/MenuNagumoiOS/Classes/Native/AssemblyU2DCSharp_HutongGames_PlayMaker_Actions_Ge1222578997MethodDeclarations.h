﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmState
struct GetFsmState_t1222578997;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmState::.ctor()
extern "C"  void GetFsmState__ctor_m264348577 (GetFsmState_t1222578997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::Reset()
extern "C"  void GetFsmState_Reset_m2205748814 (GetFsmState_t1222578997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::OnEnter()
extern "C"  void GetFsmState_OnEnter_m27640632 (GetFsmState_t1222578997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::OnUpdate()
extern "C"  void GetFsmState_OnUpdate_m4285386251 (GetFsmState_t1222578997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::DoGetFsmState()
extern "C"  void GetFsmState_DoGetFsmState_m2029111003 (GetFsmState_t1222578997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
