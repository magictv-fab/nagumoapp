﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.Cron
struct Cron_t615085410;
// ARM.utils.CronWatcher
struct CronWatcher_t1902937828;
// ARM.utils.CronWatcher/OnCronEventHandler
struct OnCronEventHandler_t343176941;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_utils_CronWatcher_OnCronEvent343176941.h"

// System.Void ARM.utils.Cron::.ctor()
extern "C"  void Cron__ctor_m1615990910 (Cron_t615085410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.CronWatcher ARM.utils.Cron::get__CronWatcher()
extern "C"  CronWatcher_t1902937828 * Cron_get__CronWatcher_m373907858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.Cron::addUpdateListener(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void Cron_addUpdateListener_m3944050445 (Il2CppObject * __this /* static, unused */, OnCronEventHandler_t343176941 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.Cron::addLateUpdateListener(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void Cron_addLateUpdateListener_m3312033235 (Il2CppObject * __this /* static, unused */, OnCronEventHandler_t343176941 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
