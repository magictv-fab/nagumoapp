﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager/<ExcluirFavorito>c__Iterator50
struct U3CExcluirFavoritoU3Ec__Iterator50_t3440772553;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FavoritosManager/<ExcluirFavorito>c__Iterator50::.ctor()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator50__ctor_m2981860290 (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ExcluirFavorito>c__Iterator50::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator50_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m299761360 (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ExcluirFavorito>c__Iterator50::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator50_System_Collections_IEnumerator_get_Current_m1476895332 (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FavoritosManager/<ExcluirFavorito>c__Iterator50::MoveNext()
extern "C"  bool U3CExcluirFavoritoU3Ec__Iterator50_MoveNext_m4092379890 (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ExcluirFavorito>c__Iterator50::Dispose()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator50_Dispose_m722835327 (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ExcluirFavorito>c__Iterator50::Reset()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator50_Reset_m628293231 (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
