﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetTextureScale
struct  SetTextureScale_t1425090431  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetTextureScale::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetTextureScale::materialIndex
	FsmInt_t1596138449 * ___materialIndex_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetTextureScale::namedTexture
	FsmString_t952858651 * ___namedTexture_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetTextureScale::scaleX
	FsmFloat_t2134102846 * ___scaleX_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetTextureScale::scaleY
	FsmFloat_t2134102846 * ___scaleY_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetTextureScale::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetTextureScale_t1425090431, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_materialIndex_12() { return static_cast<int32_t>(offsetof(SetTextureScale_t1425090431, ___materialIndex_12)); }
	inline FsmInt_t1596138449 * get_materialIndex_12() const { return ___materialIndex_12; }
	inline FsmInt_t1596138449 ** get_address_of_materialIndex_12() { return &___materialIndex_12; }
	inline void set_materialIndex_12(FsmInt_t1596138449 * value)
	{
		___materialIndex_12 = value;
		Il2CppCodeGenWriteBarrier(&___materialIndex_12, value);
	}

	inline static int32_t get_offset_of_namedTexture_13() { return static_cast<int32_t>(offsetof(SetTextureScale_t1425090431, ___namedTexture_13)); }
	inline FsmString_t952858651 * get_namedTexture_13() const { return ___namedTexture_13; }
	inline FsmString_t952858651 ** get_address_of_namedTexture_13() { return &___namedTexture_13; }
	inline void set_namedTexture_13(FsmString_t952858651 * value)
	{
		___namedTexture_13 = value;
		Il2CppCodeGenWriteBarrier(&___namedTexture_13, value);
	}

	inline static int32_t get_offset_of_scaleX_14() { return static_cast<int32_t>(offsetof(SetTextureScale_t1425090431, ___scaleX_14)); }
	inline FsmFloat_t2134102846 * get_scaleX_14() const { return ___scaleX_14; }
	inline FsmFloat_t2134102846 ** get_address_of_scaleX_14() { return &___scaleX_14; }
	inline void set_scaleX_14(FsmFloat_t2134102846 * value)
	{
		___scaleX_14 = value;
		Il2CppCodeGenWriteBarrier(&___scaleX_14, value);
	}

	inline static int32_t get_offset_of_scaleY_15() { return static_cast<int32_t>(offsetof(SetTextureScale_t1425090431, ___scaleY_15)); }
	inline FsmFloat_t2134102846 * get_scaleY_15() const { return ___scaleY_15; }
	inline FsmFloat_t2134102846 ** get_address_of_scaleY_15() { return &___scaleY_15; }
	inline void set_scaleY_15(FsmFloat_t2134102846 * value)
	{
		___scaleY_15 = value;
		Il2CppCodeGenWriteBarrier(&___scaleY_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetTextureScale_t1425090431, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
