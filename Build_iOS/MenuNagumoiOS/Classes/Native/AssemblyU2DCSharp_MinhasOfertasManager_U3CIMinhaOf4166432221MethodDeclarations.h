﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D
struct U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D::.ctor()
extern "C"  void U3CIMinhaOfertaRemoveU3Ec__Iterator1D__ctor_m2991679326 (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIMinhaOfertaRemoveU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3751705470 (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIMinhaOfertaRemoveU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m3431152402 (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D::MoveNext()
extern "C"  bool U3CIMinhaOfertaRemoveU3Ec__Iterator1D_MoveNext_m3699776190 (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D::Dispose()
extern "C"  void U3CIMinhaOfertaRemoveU3Ec__Iterator1D_Dispose_m1568994331 (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<IMinhaOfertaRemove>c__Iterator1D::Reset()
extern "C"  void U3CIMinhaOfertaRemoveU3Ec__Iterator1D_Reset_m638112267 (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
