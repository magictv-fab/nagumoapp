﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal
struct  GUILayoutBeginHorizontal_t2139859280  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::image
	FsmTexture_t3073272573 * ___image_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::text
	FsmString_t952858651 * ___text_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::tooltip
	FsmString_t952858651 * ___tooltip_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::style
	FsmString_t952858651 * ___style_14;

public:
	inline static int32_t get_offset_of_image_11() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2139859280, ___image_11)); }
	inline FsmTexture_t3073272573 * get_image_11() const { return ___image_11; }
	inline FsmTexture_t3073272573 ** get_address_of_image_11() { return &___image_11; }
	inline void set_image_11(FsmTexture_t3073272573 * value)
	{
		___image_11 = value;
		Il2CppCodeGenWriteBarrier(&___image_11, value);
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2139859280, ___text_12)); }
	inline FsmString_t952858651 * get_text_12() const { return ___text_12; }
	inline FsmString_t952858651 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(FsmString_t952858651 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier(&___text_12, value);
	}

	inline static int32_t get_offset_of_tooltip_13() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2139859280, ___tooltip_13)); }
	inline FsmString_t952858651 * get_tooltip_13() const { return ___tooltip_13; }
	inline FsmString_t952858651 ** get_address_of_tooltip_13() { return &___tooltip_13; }
	inline void set_tooltip_13(FsmString_t952858651 * value)
	{
		___tooltip_13 = value;
		Il2CppCodeGenWriteBarrier(&___tooltip_13, value);
	}

	inline static int32_t get_offset_of_style_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2139859280, ___style_14)); }
	inline FsmString_t952858651 * get_style_14() const { return ___style_14; }
	inline FsmString_t952858651 ** get_address_of_style_14() { return &___style_14; }
	inline void set_style_14(FsmString_t952858651 * value)
	{
		___style_14 = value;
		Il2CppCodeGenWriteBarrier(&___style_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
