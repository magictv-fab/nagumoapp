﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.AztecReader
struct AztecReader_t3105399986;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"

// ZXing.Result ZXing.Aztec.AztecReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * AztecReader_decode_m81072089 (AztecReader_t3105399986 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.AztecReader::reset()
extern "C"  void AztecReader_reset_m675447771 (AztecReader_t3105399986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.AztecReader::.ctor()
extern "C"  void AztecReader__ctor_m398700814 (AztecReader_t3105399986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
