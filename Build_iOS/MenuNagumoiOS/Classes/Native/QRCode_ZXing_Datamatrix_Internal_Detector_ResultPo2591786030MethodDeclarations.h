﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator
struct ResultPointsAndTransitionsComparator_t2591786030;
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct ResultPointsAndTransitions_t1206419768;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Internal_Detector_ResultPo1206419768.h"

// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator::Compare(ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions,ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions)
extern "C"  int32_t ResultPointsAndTransitionsComparator_Compare_m3800067456 (ResultPointsAndTransitionsComparator_t2591786030 * __this, ResultPointsAndTransitions_t1206419768 * ___o10, ResultPointsAndTransitions_t1206419768 * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitionsComparator::.ctor()
extern "C"  void ResultPointsAndTransitionsComparator__ctor_m518112861 (ResultPointsAndTransitionsComparator_t2591786030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
