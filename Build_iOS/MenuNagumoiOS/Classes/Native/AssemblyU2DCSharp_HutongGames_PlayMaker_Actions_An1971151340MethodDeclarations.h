﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorStopPlayback
struct AnimatorStopPlayback_t1971151340;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::.ctor()
extern "C"  void AnimatorStopPlayback__ctor_m2330795514 (AnimatorStopPlayback_t1971151340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::Reset()
extern "C"  void AnimatorStopPlayback_Reset_m4272195751 (AnimatorStopPlayback_t1971151340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::OnEnter()
extern "C"  void AnimatorStopPlayback_OnEnter_m1608256337 (AnimatorStopPlayback_t1971151340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
