﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SampleAppUIRect
struct SampleAppUIRect_t2784570703;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUIConstants
struct  SampleAppUIConstants_t672344260  : public Il2CppObject
{
public:

public:
};

struct SampleAppUIConstants_t672344260_StaticFields
{
public:
	// SampleAppUIRect SampleAppUIConstants::RectLabelOne
	SampleAppUIRect_t2784570703 * ___RectLabelOne_0;
	// SampleAppUIRect SampleAppUIConstants::RectLabelAbout
	SampleAppUIRect_t2784570703 * ___RectLabelAbout_1;
	// SampleAppUIRect SampleAppUIConstants::RectOptionOne
	SampleAppUIRect_t2784570703 * ___RectOptionOne_2;
	// SampleAppUIRect SampleAppUIConstants::RectOptionTwo
	SampleAppUIRect_t2784570703 * ___RectOptionTwo_3;
	// SampleAppUIRect SampleAppUIConstants::RectOptionThree
	SampleAppUIRect_t2784570703 * ___RectOptionThree_4;
	// SampleAppUIRect SampleAppUIConstants::RectLabelTwo
	SampleAppUIRect_t2784570703 * ___RectLabelTwo_5;
	// SampleAppUIRect SampleAppUIConstants::RectOptionFour
	SampleAppUIRect_t2784570703 * ___RectOptionFour_6;
	// SampleAppUIRect SampleAppUIConstants::RectOptionFive
	SampleAppUIRect_t2784570703 * ___RectOptionFive_7;
	// SampleAppUIRect SampleAppUIConstants::RectLabelThree
	SampleAppUIRect_t2784570703 * ___RectLabelThree_8;
	// SampleAppUIRect SampleAppUIConstants::RectOptionSix
	SampleAppUIRect_t2784570703 * ___RectOptionSix_9;
	// SampleAppUIRect SampleAppUIConstants::RectOptionSeven
	SampleAppUIRect_t2784570703 * ___RectOptionSeven_10;
	// SampleAppUIRect SampleAppUIConstants::RectOptionEight
	SampleAppUIRect_t2784570703 * ___RectOptionEight_11;
	// SampleAppUIRect SampleAppUIConstants::RectReset
	SampleAppUIRect_t2784570703 * ___RectReset_12;
	// UnityEngine.Rect SampleAppUIConstants::CloseButtonRect
	Rect_t4241904616  ___CloseButtonRect_13;
	// UnityEngine.Rect SampleAppUIConstants::BoxRect
	Rect_t4241904616  ___BoxRect_14;
	// SampleAppUIRect SampleAppUIConstants::RectLabelFour
	SampleAppUIRect_t2784570703 * ___RectLabelFour_15;
	// SampleAppUIRect SampleAppUIConstants::RectOptionTen
	SampleAppUIRect_t2784570703 * ___RectOptionTen_16;
	// SampleAppUIRect SampleAppUIConstants::RectOptionEleven
	SampleAppUIRect_t2784570703 * ___RectOptionEleven_17;
	// SampleAppUIRect SampleAppUIConstants::RectLabelFive
	SampleAppUIRect_t2784570703 * ___RectLabelFive_18;
	// SampleAppUIRect SampleAppUIConstants::RectOptionTwelve
	SampleAppUIRect_t2784570703 * ___RectOptionTwelve_19;
	// SampleAppUIRect SampleAppUIConstants::RectOptionThirteen
	SampleAppUIRect_t2784570703 * ___RectOptionThirteen_20;
	// SampleAppUIRect SampleAppUIConstants::RectOptionFourteen
	SampleAppUIRect_t2784570703 * ___RectOptionFourteen_21;
	// SampleAppUIRect SampleAppUIConstants::RectOptionFifteen
	SampleAppUIRect_t2784570703 * ___RectOptionFifteen_22;
	// SampleAppUIRect SampleAppUIConstants::RectOptionSixteen
	SampleAppUIRect_t2784570703 * ___RectOptionSixteen_23;
	// SampleAppUIRect SampleAppUIConstants::RectLabelSix
	SampleAppUIRect_t2784570703 * ___RectLabelSix_24;
	// SampleAppUIRect SampleAppUIConstants::RectOptionsSvnteen
	SampleAppUIRect_t2784570703 * ___RectOptionsSvnteen_25;
	// SampleAppUIRect SampleAppUIConstants::RectOptionsEighteen
	SampleAppUIRect_t2784570703 * ___RectOptionsEighteen_26;
	// System.String SampleAppUIConstants::MainBackground
	String_t* ___MainBackground_27;
	// System.String SampleAppUIConstants::ImageTargetLabelStyle
	String_t* ___ImageTargetLabelStyle_28;
	// System.String SampleAppUIConstants::CylinderTargetLabelStyle
	String_t* ___CylinderTargetLabelStyle_29;
	// System.String SampleAppUIConstants::MultiTargetLabelStyle
	String_t* ___MultiTargetLabelStyle_30;
	// System.String SampleAppUIConstants::FrameMarkerLabelStyle
	String_t* ___FrameMarkerLabelStyle_31;
	// System.String SampleAppUIConstants::TextRecognitionLabelStyle
	String_t* ___TextRecognitionLabelStyle_32;
	// System.String SampleAppUIConstants::VideoPlaybackLabelStyle
	String_t* ___VideoPlaybackLabelStyle_33;
	// System.String SampleAppUIConstants::VirtualButtonsLabelStyle
	String_t* ___VirtualButtonsLabelStyle_34;
	// System.String SampleAppUIConstants::OcclusionManagementStyle
	String_t* ___OcclusionManagementStyle_35;
	// System.String SampleAppUIConstants::BackgroundTextureStyle
	String_t* ___BackgroundTextureStyle_36;
	// System.String SampleAppUIConstants::UDTTextureStyle
	String_t* ___UDTTextureStyle_37;
	// System.String SampleAppUIConstants::Books
	String_t* ___Books_38;
	// System.String SampleAppUIConstants::CloudRecognition
	String_t* ___CloudRecognition_39;
	// System.String SampleAppUIConstants::AboutLableStyle
	String_t* ___AboutLableStyle_40;
	// System.String SampleAppUIConstants::ButtonsLabelStyle
	String_t* ___ButtonsLabelStyle_41;
	// System.String SampleAppUIConstants::ExtendedTrackingStyleOff
	String_t* ___ExtendedTrackingStyleOff_42;
	// System.String SampleAppUIConstants::ExtendedTrackingStyleOn
	String_t* ___ExtendedTrackingStyleOn_43;
	// System.String SampleAppUIConstants::CameraFlashStyleOff
	String_t* ___CameraFlashStyleOff_44;
	// System.String SampleAppUIConstants::CameraFlashStyleOn
	String_t* ___CameraFlashStyleOn_45;
	// System.String SampleAppUIConstants::AutoFocusStyleOn
	String_t* ___AutoFocusStyleOn_46;
	// System.String SampleAppUIConstants::AutoFocusStyleOff
	String_t* ___AutoFocusStyleOff_47;
	// System.String SampleAppUIConstants::PlayFullscreenModeOn
	String_t* ___PlayFullscreenModeOn_48;
	// System.String SampleAppUIConstants::PlayFullscreenModeOff
	String_t* ___PlayFullscreenModeOff_49;
	// System.String SampleAppUIConstants::CameraLabelStyle
	String_t* ___CameraLabelStyle_50;
	// System.String SampleAppUIConstants::CameraFacingFrontStyleOn
	String_t* ___CameraFacingFrontStyleOn_51;
	// System.String SampleAppUIConstants::CameraFacingFrontStyleOff
	String_t* ___CameraFacingFrontStyleOff_52;
	// System.String SampleAppUIConstants::CameraFacingRearStyleOn
	String_t* ___CameraFacingRearStyleOn_53;
	// System.String SampleAppUIConstants::CameraFacingRearStyleOff
	String_t* ___CameraFacingRearStyleOff_54;
	// System.String SampleAppUIConstants::StonesAndChipsStyleOn
	String_t* ___StonesAndChipsStyleOn_55;
	// System.String SampleAppUIConstants::StonesAndChipsStyleOff
	String_t* ___StonesAndChipsStyleOff_56;
	// System.String SampleAppUIConstants::TarmacOn
	String_t* ___TarmacOn_57;
	// System.String SampleAppUIConstants::TarmacOff
	String_t* ___TarmacOff_58;
	// System.String SampleAppUIConstants::DatasetLabelStyle
	String_t* ___DatasetLabelStyle_59;
	// System.String SampleAppUIConstants::closeButtonStyleOff
	String_t* ___closeButtonStyleOff_60;
	// System.String SampleAppUIConstants::closeButtonStyleOn
	String_t* ___closeButtonStyleOn_61;
	// System.String SampleAppUIConstants::CharacterModeStyleOn
	String_t* ___CharacterModeStyleOn_62;
	// System.String SampleAppUIConstants::CharacterModeStyleOff
	String_t* ___CharacterModeStyleOff_63;
	// System.String SampleAppUIConstants::TrackerOff
	String_t* ___TrackerOff_64;
	// System.String SampleAppUIConstants::TrackerOn
	String_t* ___TrackerOn_65;
	// System.String SampleAppUIConstants::MeshUpdatesOff
	String_t* ___MeshUpdatesOff_66;
	// System.String SampleAppUIConstants::MeshUpdatesOn
	String_t* ___MeshUpdatesOn_67;
	// System.String SampleAppUIConstants::SmartTerrainLabel
	String_t* ___SmartTerrainLabel_68;
	// System.String SampleAppUIConstants::Reset
	String_t* ___Reset_69;
	// System.String SampleAppUIConstants::YellowButtonStyleOn
	String_t* ___YellowButtonStyleOn_70;
	// System.String SampleAppUIConstants::YellowButtonStyleOff
	String_t* ___YellowButtonStyleOff_71;
	// System.String SampleAppUIConstants::RedButtonStyleOn
	String_t* ___RedButtonStyleOn_72;
	// System.String SampleAppUIConstants::RedButtonStyleOff
	String_t* ___RedButtonStyleOff_73;
	// System.String SampleAppUIConstants::GreenButtonStyleOn
	String_t* ___GreenButtonStyleOn_74;
	// System.String SampleAppUIConstants::GreenButtonStyleOff
	String_t* ___GreenButtonStyleOff_75;
	// System.String SampleAppUIConstants::BlueButtonStyleOn
	String_t* ___BlueButtonStyleOn_76;
	// System.String SampleAppUIConstants::BlueButtonStyleOff
	String_t* ___BlueButtonStyleOff_77;
	// System.String SampleAppUIConstants::AboutTitleForImageTgt
	String_t* ___AboutTitleForImageTgt_78;
	// System.String SampleAppUIConstants::AboutTitleFoMultiTgt
	String_t* ___AboutTitleFoMultiTgt_79;
	// System.String SampleAppUIConstants::AboutTitleForCylinderTgt
	String_t* ___AboutTitleForCylinderTgt_80;
	// System.String SampleAppUIConstants::AboutTitleForFrameMarkers
	String_t* ___AboutTitleForFrameMarkers_81;
	// System.String SampleAppUIConstants::AboutTitleForUDT
	String_t* ___AboutTitleForUDT_82;
	// System.String SampleAppUIConstants::AboutTitleForTextReco
	String_t* ___AboutTitleForTextReco_83;
	// System.String SampleAppUIConstants::AboutTitleForCloudReco
	String_t* ___AboutTitleForCloudReco_84;
	// System.String SampleAppUIConstants::AboutTitleForBooks
	String_t* ___AboutTitleForBooks_85;
	// System.String SampleAppUIConstants::AboutTitleForVirtualBtns
	String_t* ___AboutTitleForVirtualBtns_86;
	// System.String SampleAppUIConstants::AboutTitleForVideoBg
	String_t* ___AboutTitleForVideoBg_87;
	// System.String SampleAppUIConstants::AboutTitleForVideoPb
	String_t* ___AboutTitleForVideoPb_88;
	// System.String SampleAppUIConstants::AboutTitleForOcclusionMgt
	String_t* ___AboutTitleForOcclusionMgt_89;

public:
	inline static int32_t get_offset_of_RectLabelOne_0() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelOne_0)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelOne_0() const { return ___RectLabelOne_0; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelOne_0() { return &___RectLabelOne_0; }
	inline void set_RectLabelOne_0(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelOne_0 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelOne_0, value);
	}

	inline static int32_t get_offset_of_RectLabelAbout_1() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelAbout_1)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelAbout_1() const { return ___RectLabelAbout_1; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelAbout_1() { return &___RectLabelAbout_1; }
	inline void set_RectLabelAbout_1(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelAbout_1 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelAbout_1, value);
	}

	inline static int32_t get_offset_of_RectOptionOne_2() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionOne_2)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionOne_2() const { return ___RectOptionOne_2; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionOne_2() { return &___RectOptionOne_2; }
	inline void set_RectOptionOne_2(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionOne_2 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionOne_2, value);
	}

	inline static int32_t get_offset_of_RectOptionTwo_3() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionTwo_3)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionTwo_3() const { return ___RectOptionTwo_3; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionTwo_3() { return &___RectOptionTwo_3; }
	inline void set_RectOptionTwo_3(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionTwo_3 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionTwo_3, value);
	}

	inline static int32_t get_offset_of_RectOptionThree_4() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionThree_4)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionThree_4() const { return ___RectOptionThree_4; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionThree_4() { return &___RectOptionThree_4; }
	inline void set_RectOptionThree_4(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionThree_4 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionThree_4, value);
	}

	inline static int32_t get_offset_of_RectLabelTwo_5() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelTwo_5)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelTwo_5() const { return ___RectLabelTwo_5; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelTwo_5() { return &___RectLabelTwo_5; }
	inline void set_RectLabelTwo_5(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelTwo_5 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelTwo_5, value);
	}

	inline static int32_t get_offset_of_RectOptionFour_6() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionFour_6)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionFour_6() const { return ___RectOptionFour_6; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionFour_6() { return &___RectOptionFour_6; }
	inline void set_RectOptionFour_6(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionFour_6 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionFour_6, value);
	}

	inline static int32_t get_offset_of_RectOptionFive_7() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionFive_7)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionFive_7() const { return ___RectOptionFive_7; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionFive_7() { return &___RectOptionFive_7; }
	inline void set_RectOptionFive_7(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionFive_7 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionFive_7, value);
	}

	inline static int32_t get_offset_of_RectLabelThree_8() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelThree_8)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelThree_8() const { return ___RectLabelThree_8; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelThree_8() { return &___RectLabelThree_8; }
	inline void set_RectLabelThree_8(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelThree_8 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelThree_8, value);
	}

	inline static int32_t get_offset_of_RectOptionSix_9() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionSix_9)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionSix_9() const { return ___RectOptionSix_9; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionSix_9() { return &___RectOptionSix_9; }
	inline void set_RectOptionSix_9(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionSix_9 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionSix_9, value);
	}

	inline static int32_t get_offset_of_RectOptionSeven_10() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionSeven_10)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionSeven_10() const { return ___RectOptionSeven_10; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionSeven_10() { return &___RectOptionSeven_10; }
	inline void set_RectOptionSeven_10(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionSeven_10 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionSeven_10, value);
	}

	inline static int32_t get_offset_of_RectOptionEight_11() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionEight_11)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionEight_11() const { return ___RectOptionEight_11; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionEight_11() { return &___RectOptionEight_11; }
	inline void set_RectOptionEight_11(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionEight_11 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionEight_11, value);
	}

	inline static int32_t get_offset_of_RectReset_12() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectReset_12)); }
	inline SampleAppUIRect_t2784570703 * get_RectReset_12() const { return ___RectReset_12; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectReset_12() { return &___RectReset_12; }
	inline void set_RectReset_12(SampleAppUIRect_t2784570703 * value)
	{
		___RectReset_12 = value;
		Il2CppCodeGenWriteBarrier(&___RectReset_12, value);
	}

	inline static int32_t get_offset_of_CloseButtonRect_13() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CloseButtonRect_13)); }
	inline Rect_t4241904616  get_CloseButtonRect_13() const { return ___CloseButtonRect_13; }
	inline Rect_t4241904616 * get_address_of_CloseButtonRect_13() { return &___CloseButtonRect_13; }
	inline void set_CloseButtonRect_13(Rect_t4241904616  value)
	{
		___CloseButtonRect_13 = value;
	}

	inline static int32_t get_offset_of_BoxRect_14() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___BoxRect_14)); }
	inline Rect_t4241904616  get_BoxRect_14() const { return ___BoxRect_14; }
	inline Rect_t4241904616 * get_address_of_BoxRect_14() { return &___BoxRect_14; }
	inline void set_BoxRect_14(Rect_t4241904616  value)
	{
		___BoxRect_14 = value;
	}

	inline static int32_t get_offset_of_RectLabelFour_15() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelFour_15)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelFour_15() const { return ___RectLabelFour_15; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelFour_15() { return &___RectLabelFour_15; }
	inline void set_RectLabelFour_15(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelFour_15 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelFour_15, value);
	}

	inline static int32_t get_offset_of_RectOptionTen_16() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionTen_16)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionTen_16() const { return ___RectOptionTen_16; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionTen_16() { return &___RectOptionTen_16; }
	inline void set_RectOptionTen_16(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionTen_16 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionTen_16, value);
	}

	inline static int32_t get_offset_of_RectOptionEleven_17() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionEleven_17)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionEleven_17() const { return ___RectOptionEleven_17; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionEleven_17() { return &___RectOptionEleven_17; }
	inline void set_RectOptionEleven_17(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionEleven_17 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionEleven_17, value);
	}

	inline static int32_t get_offset_of_RectLabelFive_18() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelFive_18)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelFive_18() const { return ___RectLabelFive_18; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelFive_18() { return &___RectLabelFive_18; }
	inline void set_RectLabelFive_18(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelFive_18 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelFive_18, value);
	}

	inline static int32_t get_offset_of_RectOptionTwelve_19() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionTwelve_19)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionTwelve_19() const { return ___RectOptionTwelve_19; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionTwelve_19() { return &___RectOptionTwelve_19; }
	inline void set_RectOptionTwelve_19(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionTwelve_19 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionTwelve_19, value);
	}

	inline static int32_t get_offset_of_RectOptionThirteen_20() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionThirteen_20)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionThirteen_20() const { return ___RectOptionThirteen_20; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionThirteen_20() { return &___RectOptionThirteen_20; }
	inline void set_RectOptionThirteen_20(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionThirteen_20 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionThirteen_20, value);
	}

	inline static int32_t get_offset_of_RectOptionFourteen_21() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionFourteen_21)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionFourteen_21() const { return ___RectOptionFourteen_21; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionFourteen_21() { return &___RectOptionFourteen_21; }
	inline void set_RectOptionFourteen_21(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionFourteen_21 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionFourteen_21, value);
	}

	inline static int32_t get_offset_of_RectOptionFifteen_22() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionFifteen_22)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionFifteen_22() const { return ___RectOptionFifteen_22; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionFifteen_22() { return &___RectOptionFifteen_22; }
	inline void set_RectOptionFifteen_22(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionFifteen_22 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionFifteen_22, value);
	}

	inline static int32_t get_offset_of_RectOptionSixteen_23() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionSixteen_23)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionSixteen_23() const { return ___RectOptionSixteen_23; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionSixteen_23() { return &___RectOptionSixteen_23; }
	inline void set_RectOptionSixteen_23(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionSixteen_23 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionSixteen_23, value);
	}

	inline static int32_t get_offset_of_RectLabelSix_24() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectLabelSix_24)); }
	inline SampleAppUIRect_t2784570703 * get_RectLabelSix_24() const { return ___RectLabelSix_24; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectLabelSix_24() { return &___RectLabelSix_24; }
	inline void set_RectLabelSix_24(SampleAppUIRect_t2784570703 * value)
	{
		___RectLabelSix_24 = value;
		Il2CppCodeGenWriteBarrier(&___RectLabelSix_24, value);
	}

	inline static int32_t get_offset_of_RectOptionsSvnteen_25() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionsSvnteen_25)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionsSvnteen_25() const { return ___RectOptionsSvnteen_25; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionsSvnteen_25() { return &___RectOptionsSvnteen_25; }
	inline void set_RectOptionsSvnteen_25(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionsSvnteen_25 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionsSvnteen_25, value);
	}

	inline static int32_t get_offset_of_RectOptionsEighteen_26() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RectOptionsEighteen_26)); }
	inline SampleAppUIRect_t2784570703 * get_RectOptionsEighteen_26() const { return ___RectOptionsEighteen_26; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_RectOptionsEighteen_26() { return &___RectOptionsEighteen_26; }
	inline void set_RectOptionsEighteen_26(SampleAppUIRect_t2784570703 * value)
	{
		___RectOptionsEighteen_26 = value;
		Il2CppCodeGenWriteBarrier(&___RectOptionsEighteen_26, value);
	}

	inline static int32_t get_offset_of_MainBackground_27() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___MainBackground_27)); }
	inline String_t* get_MainBackground_27() const { return ___MainBackground_27; }
	inline String_t** get_address_of_MainBackground_27() { return &___MainBackground_27; }
	inline void set_MainBackground_27(String_t* value)
	{
		___MainBackground_27 = value;
		Il2CppCodeGenWriteBarrier(&___MainBackground_27, value);
	}

	inline static int32_t get_offset_of_ImageTargetLabelStyle_28() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___ImageTargetLabelStyle_28)); }
	inline String_t* get_ImageTargetLabelStyle_28() const { return ___ImageTargetLabelStyle_28; }
	inline String_t** get_address_of_ImageTargetLabelStyle_28() { return &___ImageTargetLabelStyle_28; }
	inline void set_ImageTargetLabelStyle_28(String_t* value)
	{
		___ImageTargetLabelStyle_28 = value;
		Il2CppCodeGenWriteBarrier(&___ImageTargetLabelStyle_28, value);
	}

	inline static int32_t get_offset_of_CylinderTargetLabelStyle_29() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CylinderTargetLabelStyle_29)); }
	inline String_t* get_CylinderTargetLabelStyle_29() const { return ___CylinderTargetLabelStyle_29; }
	inline String_t** get_address_of_CylinderTargetLabelStyle_29() { return &___CylinderTargetLabelStyle_29; }
	inline void set_CylinderTargetLabelStyle_29(String_t* value)
	{
		___CylinderTargetLabelStyle_29 = value;
		Il2CppCodeGenWriteBarrier(&___CylinderTargetLabelStyle_29, value);
	}

	inline static int32_t get_offset_of_MultiTargetLabelStyle_30() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___MultiTargetLabelStyle_30)); }
	inline String_t* get_MultiTargetLabelStyle_30() const { return ___MultiTargetLabelStyle_30; }
	inline String_t** get_address_of_MultiTargetLabelStyle_30() { return &___MultiTargetLabelStyle_30; }
	inline void set_MultiTargetLabelStyle_30(String_t* value)
	{
		___MultiTargetLabelStyle_30 = value;
		Il2CppCodeGenWriteBarrier(&___MultiTargetLabelStyle_30, value);
	}

	inline static int32_t get_offset_of_FrameMarkerLabelStyle_31() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___FrameMarkerLabelStyle_31)); }
	inline String_t* get_FrameMarkerLabelStyle_31() const { return ___FrameMarkerLabelStyle_31; }
	inline String_t** get_address_of_FrameMarkerLabelStyle_31() { return &___FrameMarkerLabelStyle_31; }
	inline void set_FrameMarkerLabelStyle_31(String_t* value)
	{
		___FrameMarkerLabelStyle_31 = value;
		Il2CppCodeGenWriteBarrier(&___FrameMarkerLabelStyle_31, value);
	}

	inline static int32_t get_offset_of_TextRecognitionLabelStyle_32() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___TextRecognitionLabelStyle_32)); }
	inline String_t* get_TextRecognitionLabelStyle_32() const { return ___TextRecognitionLabelStyle_32; }
	inline String_t** get_address_of_TextRecognitionLabelStyle_32() { return &___TextRecognitionLabelStyle_32; }
	inline void set_TextRecognitionLabelStyle_32(String_t* value)
	{
		___TextRecognitionLabelStyle_32 = value;
		Il2CppCodeGenWriteBarrier(&___TextRecognitionLabelStyle_32, value);
	}

	inline static int32_t get_offset_of_VideoPlaybackLabelStyle_33() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___VideoPlaybackLabelStyle_33)); }
	inline String_t* get_VideoPlaybackLabelStyle_33() const { return ___VideoPlaybackLabelStyle_33; }
	inline String_t** get_address_of_VideoPlaybackLabelStyle_33() { return &___VideoPlaybackLabelStyle_33; }
	inline void set_VideoPlaybackLabelStyle_33(String_t* value)
	{
		___VideoPlaybackLabelStyle_33 = value;
		Il2CppCodeGenWriteBarrier(&___VideoPlaybackLabelStyle_33, value);
	}

	inline static int32_t get_offset_of_VirtualButtonsLabelStyle_34() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___VirtualButtonsLabelStyle_34)); }
	inline String_t* get_VirtualButtonsLabelStyle_34() const { return ___VirtualButtonsLabelStyle_34; }
	inline String_t** get_address_of_VirtualButtonsLabelStyle_34() { return &___VirtualButtonsLabelStyle_34; }
	inline void set_VirtualButtonsLabelStyle_34(String_t* value)
	{
		___VirtualButtonsLabelStyle_34 = value;
		Il2CppCodeGenWriteBarrier(&___VirtualButtonsLabelStyle_34, value);
	}

	inline static int32_t get_offset_of_OcclusionManagementStyle_35() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___OcclusionManagementStyle_35)); }
	inline String_t* get_OcclusionManagementStyle_35() const { return ___OcclusionManagementStyle_35; }
	inline String_t** get_address_of_OcclusionManagementStyle_35() { return &___OcclusionManagementStyle_35; }
	inline void set_OcclusionManagementStyle_35(String_t* value)
	{
		___OcclusionManagementStyle_35 = value;
		Il2CppCodeGenWriteBarrier(&___OcclusionManagementStyle_35, value);
	}

	inline static int32_t get_offset_of_BackgroundTextureStyle_36() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___BackgroundTextureStyle_36)); }
	inline String_t* get_BackgroundTextureStyle_36() const { return ___BackgroundTextureStyle_36; }
	inline String_t** get_address_of_BackgroundTextureStyle_36() { return &___BackgroundTextureStyle_36; }
	inline void set_BackgroundTextureStyle_36(String_t* value)
	{
		___BackgroundTextureStyle_36 = value;
		Il2CppCodeGenWriteBarrier(&___BackgroundTextureStyle_36, value);
	}

	inline static int32_t get_offset_of_UDTTextureStyle_37() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___UDTTextureStyle_37)); }
	inline String_t* get_UDTTextureStyle_37() const { return ___UDTTextureStyle_37; }
	inline String_t** get_address_of_UDTTextureStyle_37() { return &___UDTTextureStyle_37; }
	inline void set_UDTTextureStyle_37(String_t* value)
	{
		___UDTTextureStyle_37 = value;
		Il2CppCodeGenWriteBarrier(&___UDTTextureStyle_37, value);
	}

	inline static int32_t get_offset_of_Books_38() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___Books_38)); }
	inline String_t* get_Books_38() const { return ___Books_38; }
	inline String_t** get_address_of_Books_38() { return &___Books_38; }
	inline void set_Books_38(String_t* value)
	{
		___Books_38 = value;
		Il2CppCodeGenWriteBarrier(&___Books_38, value);
	}

	inline static int32_t get_offset_of_CloudRecognition_39() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CloudRecognition_39)); }
	inline String_t* get_CloudRecognition_39() const { return ___CloudRecognition_39; }
	inline String_t** get_address_of_CloudRecognition_39() { return &___CloudRecognition_39; }
	inline void set_CloudRecognition_39(String_t* value)
	{
		___CloudRecognition_39 = value;
		Il2CppCodeGenWriteBarrier(&___CloudRecognition_39, value);
	}

	inline static int32_t get_offset_of_AboutLableStyle_40() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutLableStyle_40)); }
	inline String_t* get_AboutLableStyle_40() const { return ___AboutLableStyle_40; }
	inline String_t** get_address_of_AboutLableStyle_40() { return &___AboutLableStyle_40; }
	inline void set_AboutLableStyle_40(String_t* value)
	{
		___AboutLableStyle_40 = value;
		Il2CppCodeGenWriteBarrier(&___AboutLableStyle_40, value);
	}

	inline static int32_t get_offset_of_ButtonsLabelStyle_41() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___ButtonsLabelStyle_41)); }
	inline String_t* get_ButtonsLabelStyle_41() const { return ___ButtonsLabelStyle_41; }
	inline String_t** get_address_of_ButtonsLabelStyle_41() { return &___ButtonsLabelStyle_41; }
	inline void set_ButtonsLabelStyle_41(String_t* value)
	{
		___ButtonsLabelStyle_41 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonsLabelStyle_41, value);
	}

	inline static int32_t get_offset_of_ExtendedTrackingStyleOff_42() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___ExtendedTrackingStyleOff_42)); }
	inline String_t* get_ExtendedTrackingStyleOff_42() const { return ___ExtendedTrackingStyleOff_42; }
	inline String_t** get_address_of_ExtendedTrackingStyleOff_42() { return &___ExtendedTrackingStyleOff_42; }
	inline void set_ExtendedTrackingStyleOff_42(String_t* value)
	{
		___ExtendedTrackingStyleOff_42 = value;
		Il2CppCodeGenWriteBarrier(&___ExtendedTrackingStyleOff_42, value);
	}

	inline static int32_t get_offset_of_ExtendedTrackingStyleOn_43() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___ExtendedTrackingStyleOn_43)); }
	inline String_t* get_ExtendedTrackingStyleOn_43() const { return ___ExtendedTrackingStyleOn_43; }
	inline String_t** get_address_of_ExtendedTrackingStyleOn_43() { return &___ExtendedTrackingStyleOn_43; }
	inline void set_ExtendedTrackingStyleOn_43(String_t* value)
	{
		___ExtendedTrackingStyleOn_43 = value;
		Il2CppCodeGenWriteBarrier(&___ExtendedTrackingStyleOn_43, value);
	}

	inline static int32_t get_offset_of_CameraFlashStyleOff_44() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraFlashStyleOff_44)); }
	inline String_t* get_CameraFlashStyleOff_44() const { return ___CameraFlashStyleOff_44; }
	inline String_t** get_address_of_CameraFlashStyleOff_44() { return &___CameraFlashStyleOff_44; }
	inline void set_CameraFlashStyleOff_44(String_t* value)
	{
		___CameraFlashStyleOff_44 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFlashStyleOff_44, value);
	}

	inline static int32_t get_offset_of_CameraFlashStyleOn_45() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraFlashStyleOn_45)); }
	inline String_t* get_CameraFlashStyleOn_45() const { return ___CameraFlashStyleOn_45; }
	inline String_t** get_address_of_CameraFlashStyleOn_45() { return &___CameraFlashStyleOn_45; }
	inline void set_CameraFlashStyleOn_45(String_t* value)
	{
		___CameraFlashStyleOn_45 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFlashStyleOn_45, value);
	}

	inline static int32_t get_offset_of_AutoFocusStyleOn_46() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AutoFocusStyleOn_46)); }
	inline String_t* get_AutoFocusStyleOn_46() const { return ___AutoFocusStyleOn_46; }
	inline String_t** get_address_of_AutoFocusStyleOn_46() { return &___AutoFocusStyleOn_46; }
	inline void set_AutoFocusStyleOn_46(String_t* value)
	{
		___AutoFocusStyleOn_46 = value;
		Il2CppCodeGenWriteBarrier(&___AutoFocusStyleOn_46, value);
	}

	inline static int32_t get_offset_of_AutoFocusStyleOff_47() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AutoFocusStyleOff_47)); }
	inline String_t* get_AutoFocusStyleOff_47() const { return ___AutoFocusStyleOff_47; }
	inline String_t** get_address_of_AutoFocusStyleOff_47() { return &___AutoFocusStyleOff_47; }
	inline void set_AutoFocusStyleOff_47(String_t* value)
	{
		___AutoFocusStyleOff_47 = value;
		Il2CppCodeGenWriteBarrier(&___AutoFocusStyleOff_47, value);
	}

	inline static int32_t get_offset_of_PlayFullscreenModeOn_48() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___PlayFullscreenModeOn_48)); }
	inline String_t* get_PlayFullscreenModeOn_48() const { return ___PlayFullscreenModeOn_48; }
	inline String_t** get_address_of_PlayFullscreenModeOn_48() { return &___PlayFullscreenModeOn_48; }
	inline void set_PlayFullscreenModeOn_48(String_t* value)
	{
		___PlayFullscreenModeOn_48 = value;
		Il2CppCodeGenWriteBarrier(&___PlayFullscreenModeOn_48, value);
	}

	inline static int32_t get_offset_of_PlayFullscreenModeOff_49() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___PlayFullscreenModeOff_49)); }
	inline String_t* get_PlayFullscreenModeOff_49() const { return ___PlayFullscreenModeOff_49; }
	inline String_t** get_address_of_PlayFullscreenModeOff_49() { return &___PlayFullscreenModeOff_49; }
	inline void set_PlayFullscreenModeOff_49(String_t* value)
	{
		___PlayFullscreenModeOff_49 = value;
		Il2CppCodeGenWriteBarrier(&___PlayFullscreenModeOff_49, value);
	}

	inline static int32_t get_offset_of_CameraLabelStyle_50() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraLabelStyle_50)); }
	inline String_t* get_CameraLabelStyle_50() const { return ___CameraLabelStyle_50; }
	inline String_t** get_address_of_CameraLabelStyle_50() { return &___CameraLabelStyle_50; }
	inline void set_CameraLabelStyle_50(String_t* value)
	{
		___CameraLabelStyle_50 = value;
		Il2CppCodeGenWriteBarrier(&___CameraLabelStyle_50, value);
	}

	inline static int32_t get_offset_of_CameraFacingFrontStyleOn_51() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraFacingFrontStyleOn_51)); }
	inline String_t* get_CameraFacingFrontStyleOn_51() const { return ___CameraFacingFrontStyleOn_51; }
	inline String_t** get_address_of_CameraFacingFrontStyleOn_51() { return &___CameraFacingFrontStyleOn_51; }
	inline void set_CameraFacingFrontStyleOn_51(String_t* value)
	{
		___CameraFacingFrontStyleOn_51 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFacingFrontStyleOn_51, value);
	}

	inline static int32_t get_offset_of_CameraFacingFrontStyleOff_52() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraFacingFrontStyleOff_52)); }
	inline String_t* get_CameraFacingFrontStyleOff_52() const { return ___CameraFacingFrontStyleOff_52; }
	inline String_t** get_address_of_CameraFacingFrontStyleOff_52() { return &___CameraFacingFrontStyleOff_52; }
	inline void set_CameraFacingFrontStyleOff_52(String_t* value)
	{
		___CameraFacingFrontStyleOff_52 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFacingFrontStyleOff_52, value);
	}

	inline static int32_t get_offset_of_CameraFacingRearStyleOn_53() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraFacingRearStyleOn_53)); }
	inline String_t* get_CameraFacingRearStyleOn_53() const { return ___CameraFacingRearStyleOn_53; }
	inline String_t** get_address_of_CameraFacingRearStyleOn_53() { return &___CameraFacingRearStyleOn_53; }
	inline void set_CameraFacingRearStyleOn_53(String_t* value)
	{
		___CameraFacingRearStyleOn_53 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFacingRearStyleOn_53, value);
	}

	inline static int32_t get_offset_of_CameraFacingRearStyleOff_54() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CameraFacingRearStyleOff_54)); }
	inline String_t* get_CameraFacingRearStyleOff_54() const { return ___CameraFacingRearStyleOff_54; }
	inline String_t** get_address_of_CameraFacingRearStyleOff_54() { return &___CameraFacingRearStyleOff_54; }
	inline void set_CameraFacingRearStyleOff_54(String_t* value)
	{
		___CameraFacingRearStyleOff_54 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFacingRearStyleOff_54, value);
	}

	inline static int32_t get_offset_of_StonesAndChipsStyleOn_55() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___StonesAndChipsStyleOn_55)); }
	inline String_t* get_StonesAndChipsStyleOn_55() const { return ___StonesAndChipsStyleOn_55; }
	inline String_t** get_address_of_StonesAndChipsStyleOn_55() { return &___StonesAndChipsStyleOn_55; }
	inline void set_StonesAndChipsStyleOn_55(String_t* value)
	{
		___StonesAndChipsStyleOn_55 = value;
		Il2CppCodeGenWriteBarrier(&___StonesAndChipsStyleOn_55, value);
	}

	inline static int32_t get_offset_of_StonesAndChipsStyleOff_56() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___StonesAndChipsStyleOff_56)); }
	inline String_t* get_StonesAndChipsStyleOff_56() const { return ___StonesAndChipsStyleOff_56; }
	inline String_t** get_address_of_StonesAndChipsStyleOff_56() { return &___StonesAndChipsStyleOff_56; }
	inline void set_StonesAndChipsStyleOff_56(String_t* value)
	{
		___StonesAndChipsStyleOff_56 = value;
		Il2CppCodeGenWriteBarrier(&___StonesAndChipsStyleOff_56, value);
	}

	inline static int32_t get_offset_of_TarmacOn_57() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___TarmacOn_57)); }
	inline String_t* get_TarmacOn_57() const { return ___TarmacOn_57; }
	inline String_t** get_address_of_TarmacOn_57() { return &___TarmacOn_57; }
	inline void set_TarmacOn_57(String_t* value)
	{
		___TarmacOn_57 = value;
		Il2CppCodeGenWriteBarrier(&___TarmacOn_57, value);
	}

	inline static int32_t get_offset_of_TarmacOff_58() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___TarmacOff_58)); }
	inline String_t* get_TarmacOff_58() const { return ___TarmacOff_58; }
	inline String_t** get_address_of_TarmacOff_58() { return &___TarmacOff_58; }
	inline void set_TarmacOff_58(String_t* value)
	{
		___TarmacOff_58 = value;
		Il2CppCodeGenWriteBarrier(&___TarmacOff_58, value);
	}

	inline static int32_t get_offset_of_DatasetLabelStyle_59() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___DatasetLabelStyle_59)); }
	inline String_t* get_DatasetLabelStyle_59() const { return ___DatasetLabelStyle_59; }
	inline String_t** get_address_of_DatasetLabelStyle_59() { return &___DatasetLabelStyle_59; }
	inline void set_DatasetLabelStyle_59(String_t* value)
	{
		___DatasetLabelStyle_59 = value;
		Il2CppCodeGenWriteBarrier(&___DatasetLabelStyle_59, value);
	}

	inline static int32_t get_offset_of_closeButtonStyleOff_60() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___closeButtonStyleOff_60)); }
	inline String_t* get_closeButtonStyleOff_60() const { return ___closeButtonStyleOff_60; }
	inline String_t** get_address_of_closeButtonStyleOff_60() { return &___closeButtonStyleOff_60; }
	inline void set_closeButtonStyleOff_60(String_t* value)
	{
		___closeButtonStyleOff_60 = value;
		Il2CppCodeGenWriteBarrier(&___closeButtonStyleOff_60, value);
	}

	inline static int32_t get_offset_of_closeButtonStyleOn_61() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___closeButtonStyleOn_61)); }
	inline String_t* get_closeButtonStyleOn_61() const { return ___closeButtonStyleOn_61; }
	inline String_t** get_address_of_closeButtonStyleOn_61() { return &___closeButtonStyleOn_61; }
	inline void set_closeButtonStyleOn_61(String_t* value)
	{
		___closeButtonStyleOn_61 = value;
		Il2CppCodeGenWriteBarrier(&___closeButtonStyleOn_61, value);
	}

	inline static int32_t get_offset_of_CharacterModeStyleOn_62() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CharacterModeStyleOn_62)); }
	inline String_t* get_CharacterModeStyleOn_62() const { return ___CharacterModeStyleOn_62; }
	inline String_t** get_address_of_CharacterModeStyleOn_62() { return &___CharacterModeStyleOn_62; }
	inline void set_CharacterModeStyleOn_62(String_t* value)
	{
		___CharacterModeStyleOn_62 = value;
		Il2CppCodeGenWriteBarrier(&___CharacterModeStyleOn_62, value);
	}

	inline static int32_t get_offset_of_CharacterModeStyleOff_63() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___CharacterModeStyleOff_63)); }
	inline String_t* get_CharacterModeStyleOff_63() const { return ___CharacterModeStyleOff_63; }
	inline String_t** get_address_of_CharacterModeStyleOff_63() { return &___CharacterModeStyleOff_63; }
	inline void set_CharacterModeStyleOff_63(String_t* value)
	{
		___CharacterModeStyleOff_63 = value;
		Il2CppCodeGenWriteBarrier(&___CharacterModeStyleOff_63, value);
	}

	inline static int32_t get_offset_of_TrackerOff_64() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___TrackerOff_64)); }
	inline String_t* get_TrackerOff_64() const { return ___TrackerOff_64; }
	inline String_t** get_address_of_TrackerOff_64() { return &___TrackerOff_64; }
	inline void set_TrackerOff_64(String_t* value)
	{
		___TrackerOff_64 = value;
		Il2CppCodeGenWriteBarrier(&___TrackerOff_64, value);
	}

	inline static int32_t get_offset_of_TrackerOn_65() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___TrackerOn_65)); }
	inline String_t* get_TrackerOn_65() const { return ___TrackerOn_65; }
	inline String_t** get_address_of_TrackerOn_65() { return &___TrackerOn_65; }
	inline void set_TrackerOn_65(String_t* value)
	{
		___TrackerOn_65 = value;
		Il2CppCodeGenWriteBarrier(&___TrackerOn_65, value);
	}

	inline static int32_t get_offset_of_MeshUpdatesOff_66() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___MeshUpdatesOff_66)); }
	inline String_t* get_MeshUpdatesOff_66() const { return ___MeshUpdatesOff_66; }
	inline String_t** get_address_of_MeshUpdatesOff_66() { return &___MeshUpdatesOff_66; }
	inline void set_MeshUpdatesOff_66(String_t* value)
	{
		___MeshUpdatesOff_66 = value;
		Il2CppCodeGenWriteBarrier(&___MeshUpdatesOff_66, value);
	}

	inline static int32_t get_offset_of_MeshUpdatesOn_67() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___MeshUpdatesOn_67)); }
	inline String_t* get_MeshUpdatesOn_67() const { return ___MeshUpdatesOn_67; }
	inline String_t** get_address_of_MeshUpdatesOn_67() { return &___MeshUpdatesOn_67; }
	inline void set_MeshUpdatesOn_67(String_t* value)
	{
		___MeshUpdatesOn_67 = value;
		Il2CppCodeGenWriteBarrier(&___MeshUpdatesOn_67, value);
	}

	inline static int32_t get_offset_of_SmartTerrainLabel_68() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___SmartTerrainLabel_68)); }
	inline String_t* get_SmartTerrainLabel_68() const { return ___SmartTerrainLabel_68; }
	inline String_t** get_address_of_SmartTerrainLabel_68() { return &___SmartTerrainLabel_68; }
	inline void set_SmartTerrainLabel_68(String_t* value)
	{
		___SmartTerrainLabel_68 = value;
		Il2CppCodeGenWriteBarrier(&___SmartTerrainLabel_68, value);
	}

	inline static int32_t get_offset_of_Reset_69() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___Reset_69)); }
	inline String_t* get_Reset_69() const { return ___Reset_69; }
	inline String_t** get_address_of_Reset_69() { return &___Reset_69; }
	inline void set_Reset_69(String_t* value)
	{
		___Reset_69 = value;
		Il2CppCodeGenWriteBarrier(&___Reset_69, value);
	}

	inline static int32_t get_offset_of_YellowButtonStyleOn_70() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___YellowButtonStyleOn_70)); }
	inline String_t* get_YellowButtonStyleOn_70() const { return ___YellowButtonStyleOn_70; }
	inline String_t** get_address_of_YellowButtonStyleOn_70() { return &___YellowButtonStyleOn_70; }
	inline void set_YellowButtonStyleOn_70(String_t* value)
	{
		___YellowButtonStyleOn_70 = value;
		Il2CppCodeGenWriteBarrier(&___YellowButtonStyleOn_70, value);
	}

	inline static int32_t get_offset_of_YellowButtonStyleOff_71() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___YellowButtonStyleOff_71)); }
	inline String_t* get_YellowButtonStyleOff_71() const { return ___YellowButtonStyleOff_71; }
	inline String_t** get_address_of_YellowButtonStyleOff_71() { return &___YellowButtonStyleOff_71; }
	inline void set_YellowButtonStyleOff_71(String_t* value)
	{
		___YellowButtonStyleOff_71 = value;
		Il2CppCodeGenWriteBarrier(&___YellowButtonStyleOff_71, value);
	}

	inline static int32_t get_offset_of_RedButtonStyleOn_72() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RedButtonStyleOn_72)); }
	inline String_t* get_RedButtonStyleOn_72() const { return ___RedButtonStyleOn_72; }
	inline String_t** get_address_of_RedButtonStyleOn_72() { return &___RedButtonStyleOn_72; }
	inline void set_RedButtonStyleOn_72(String_t* value)
	{
		___RedButtonStyleOn_72 = value;
		Il2CppCodeGenWriteBarrier(&___RedButtonStyleOn_72, value);
	}

	inline static int32_t get_offset_of_RedButtonStyleOff_73() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___RedButtonStyleOff_73)); }
	inline String_t* get_RedButtonStyleOff_73() const { return ___RedButtonStyleOff_73; }
	inline String_t** get_address_of_RedButtonStyleOff_73() { return &___RedButtonStyleOff_73; }
	inline void set_RedButtonStyleOff_73(String_t* value)
	{
		___RedButtonStyleOff_73 = value;
		Il2CppCodeGenWriteBarrier(&___RedButtonStyleOff_73, value);
	}

	inline static int32_t get_offset_of_GreenButtonStyleOn_74() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___GreenButtonStyleOn_74)); }
	inline String_t* get_GreenButtonStyleOn_74() const { return ___GreenButtonStyleOn_74; }
	inline String_t** get_address_of_GreenButtonStyleOn_74() { return &___GreenButtonStyleOn_74; }
	inline void set_GreenButtonStyleOn_74(String_t* value)
	{
		___GreenButtonStyleOn_74 = value;
		Il2CppCodeGenWriteBarrier(&___GreenButtonStyleOn_74, value);
	}

	inline static int32_t get_offset_of_GreenButtonStyleOff_75() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___GreenButtonStyleOff_75)); }
	inline String_t* get_GreenButtonStyleOff_75() const { return ___GreenButtonStyleOff_75; }
	inline String_t** get_address_of_GreenButtonStyleOff_75() { return &___GreenButtonStyleOff_75; }
	inline void set_GreenButtonStyleOff_75(String_t* value)
	{
		___GreenButtonStyleOff_75 = value;
		Il2CppCodeGenWriteBarrier(&___GreenButtonStyleOff_75, value);
	}

	inline static int32_t get_offset_of_BlueButtonStyleOn_76() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___BlueButtonStyleOn_76)); }
	inline String_t* get_BlueButtonStyleOn_76() const { return ___BlueButtonStyleOn_76; }
	inline String_t** get_address_of_BlueButtonStyleOn_76() { return &___BlueButtonStyleOn_76; }
	inline void set_BlueButtonStyleOn_76(String_t* value)
	{
		___BlueButtonStyleOn_76 = value;
		Il2CppCodeGenWriteBarrier(&___BlueButtonStyleOn_76, value);
	}

	inline static int32_t get_offset_of_BlueButtonStyleOff_77() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___BlueButtonStyleOff_77)); }
	inline String_t* get_BlueButtonStyleOff_77() const { return ___BlueButtonStyleOff_77; }
	inline String_t** get_address_of_BlueButtonStyleOff_77() { return &___BlueButtonStyleOff_77; }
	inline void set_BlueButtonStyleOff_77(String_t* value)
	{
		___BlueButtonStyleOff_77 = value;
		Il2CppCodeGenWriteBarrier(&___BlueButtonStyleOff_77, value);
	}

	inline static int32_t get_offset_of_AboutTitleForImageTgt_78() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForImageTgt_78)); }
	inline String_t* get_AboutTitleForImageTgt_78() const { return ___AboutTitleForImageTgt_78; }
	inline String_t** get_address_of_AboutTitleForImageTgt_78() { return &___AboutTitleForImageTgt_78; }
	inline void set_AboutTitleForImageTgt_78(String_t* value)
	{
		___AboutTitleForImageTgt_78 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForImageTgt_78, value);
	}

	inline static int32_t get_offset_of_AboutTitleFoMultiTgt_79() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleFoMultiTgt_79)); }
	inline String_t* get_AboutTitleFoMultiTgt_79() const { return ___AboutTitleFoMultiTgt_79; }
	inline String_t** get_address_of_AboutTitleFoMultiTgt_79() { return &___AboutTitleFoMultiTgt_79; }
	inline void set_AboutTitleFoMultiTgt_79(String_t* value)
	{
		___AboutTitleFoMultiTgt_79 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleFoMultiTgt_79, value);
	}

	inline static int32_t get_offset_of_AboutTitleForCylinderTgt_80() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForCylinderTgt_80)); }
	inline String_t* get_AboutTitleForCylinderTgt_80() const { return ___AboutTitleForCylinderTgt_80; }
	inline String_t** get_address_of_AboutTitleForCylinderTgt_80() { return &___AboutTitleForCylinderTgt_80; }
	inline void set_AboutTitleForCylinderTgt_80(String_t* value)
	{
		___AboutTitleForCylinderTgt_80 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForCylinderTgt_80, value);
	}

	inline static int32_t get_offset_of_AboutTitleForFrameMarkers_81() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForFrameMarkers_81)); }
	inline String_t* get_AboutTitleForFrameMarkers_81() const { return ___AboutTitleForFrameMarkers_81; }
	inline String_t** get_address_of_AboutTitleForFrameMarkers_81() { return &___AboutTitleForFrameMarkers_81; }
	inline void set_AboutTitleForFrameMarkers_81(String_t* value)
	{
		___AboutTitleForFrameMarkers_81 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForFrameMarkers_81, value);
	}

	inline static int32_t get_offset_of_AboutTitleForUDT_82() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForUDT_82)); }
	inline String_t* get_AboutTitleForUDT_82() const { return ___AboutTitleForUDT_82; }
	inline String_t** get_address_of_AboutTitleForUDT_82() { return &___AboutTitleForUDT_82; }
	inline void set_AboutTitleForUDT_82(String_t* value)
	{
		___AboutTitleForUDT_82 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForUDT_82, value);
	}

	inline static int32_t get_offset_of_AboutTitleForTextReco_83() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForTextReco_83)); }
	inline String_t* get_AboutTitleForTextReco_83() const { return ___AboutTitleForTextReco_83; }
	inline String_t** get_address_of_AboutTitleForTextReco_83() { return &___AboutTitleForTextReco_83; }
	inline void set_AboutTitleForTextReco_83(String_t* value)
	{
		___AboutTitleForTextReco_83 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForTextReco_83, value);
	}

	inline static int32_t get_offset_of_AboutTitleForCloudReco_84() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForCloudReco_84)); }
	inline String_t* get_AboutTitleForCloudReco_84() const { return ___AboutTitleForCloudReco_84; }
	inline String_t** get_address_of_AboutTitleForCloudReco_84() { return &___AboutTitleForCloudReco_84; }
	inline void set_AboutTitleForCloudReco_84(String_t* value)
	{
		___AboutTitleForCloudReco_84 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForCloudReco_84, value);
	}

	inline static int32_t get_offset_of_AboutTitleForBooks_85() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForBooks_85)); }
	inline String_t* get_AboutTitleForBooks_85() const { return ___AboutTitleForBooks_85; }
	inline String_t** get_address_of_AboutTitleForBooks_85() { return &___AboutTitleForBooks_85; }
	inline void set_AboutTitleForBooks_85(String_t* value)
	{
		___AboutTitleForBooks_85 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForBooks_85, value);
	}

	inline static int32_t get_offset_of_AboutTitleForVirtualBtns_86() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForVirtualBtns_86)); }
	inline String_t* get_AboutTitleForVirtualBtns_86() const { return ___AboutTitleForVirtualBtns_86; }
	inline String_t** get_address_of_AboutTitleForVirtualBtns_86() { return &___AboutTitleForVirtualBtns_86; }
	inline void set_AboutTitleForVirtualBtns_86(String_t* value)
	{
		___AboutTitleForVirtualBtns_86 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForVirtualBtns_86, value);
	}

	inline static int32_t get_offset_of_AboutTitleForVideoBg_87() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForVideoBg_87)); }
	inline String_t* get_AboutTitleForVideoBg_87() const { return ___AboutTitleForVideoBg_87; }
	inline String_t** get_address_of_AboutTitleForVideoBg_87() { return &___AboutTitleForVideoBg_87; }
	inline void set_AboutTitleForVideoBg_87(String_t* value)
	{
		___AboutTitleForVideoBg_87 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForVideoBg_87, value);
	}

	inline static int32_t get_offset_of_AboutTitleForVideoPb_88() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForVideoPb_88)); }
	inline String_t* get_AboutTitleForVideoPb_88() const { return ___AboutTitleForVideoPb_88; }
	inline String_t** get_address_of_AboutTitleForVideoPb_88() { return &___AboutTitleForVideoPb_88; }
	inline void set_AboutTitleForVideoPb_88(String_t* value)
	{
		___AboutTitleForVideoPb_88 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForVideoPb_88, value);
	}

	inline static int32_t get_offset_of_AboutTitleForOcclusionMgt_89() { return static_cast<int32_t>(offsetof(SampleAppUIConstants_t672344260_StaticFields, ___AboutTitleForOcclusionMgt_89)); }
	inline String_t* get_AboutTitleForOcclusionMgt_89() const { return ___AboutTitleForOcclusionMgt_89; }
	inline String_t** get_address_of_AboutTitleForOcclusionMgt_89() { return &___AboutTitleForOcclusionMgt_89; }
	inline void set_AboutTitleForOcclusionMgt_89(String_t* value)
	{
		___AboutTitleForOcclusionMgt_89 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTitleForOcclusionMgt_89, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
