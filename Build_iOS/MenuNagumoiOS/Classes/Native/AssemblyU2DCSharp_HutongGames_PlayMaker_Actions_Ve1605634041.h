﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Interpolate
struct  Vector3Interpolate_t1605634041  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.InterpolationType HutongGames.PlayMaker.Actions.Vector3Interpolate::mode
	int32_t ___mode_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Interpolate::fromVector
	FsmVector3_t533912882 * ___fromVector_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Interpolate::toVector
	FsmVector3_t533912882 * ___toVector_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Interpolate::time
	FsmFloat_t2134102846 * ___time_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Interpolate::storeResult
	FsmVector3_t533912882 * ___storeResult_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Vector3Interpolate::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Interpolate::realTime
	bool ___realTime_15;
	// System.Single HutongGames.PlayMaker.Actions.Vector3Interpolate::startTime
	float ___startTime_16;
	// System.Single HutongGames.PlayMaker.Actions.Vector3Interpolate::currentTime
	float ___currentTime_17;

public:
	inline static int32_t get_offset_of_mode_9() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___mode_9)); }
	inline int32_t get_mode_9() const { return ___mode_9; }
	inline int32_t* get_address_of_mode_9() { return &___mode_9; }
	inline void set_mode_9(int32_t value)
	{
		___mode_9 = value;
	}

	inline static int32_t get_offset_of_fromVector_10() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___fromVector_10)); }
	inline FsmVector3_t533912882 * get_fromVector_10() const { return ___fromVector_10; }
	inline FsmVector3_t533912882 ** get_address_of_fromVector_10() { return &___fromVector_10; }
	inline void set_fromVector_10(FsmVector3_t533912882 * value)
	{
		___fromVector_10 = value;
		Il2CppCodeGenWriteBarrier(&___fromVector_10, value);
	}

	inline static int32_t get_offset_of_toVector_11() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___toVector_11)); }
	inline FsmVector3_t533912882 * get_toVector_11() const { return ___toVector_11; }
	inline FsmVector3_t533912882 ** get_address_of_toVector_11() { return &___toVector_11; }
	inline void set_toVector_11(FsmVector3_t533912882 * value)
	{
		___toVector_11 = value;
		Il2CppCodeGenWriteBarrier(&___toVector_11, value);
	}

	inline static int32_t get_offset_of_time_12() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___time_12)); }
	inline FsmFloat_t2134102846 * get_time_12() const { return ___time_12; }
	inline FsmFloat_t2134102846 ** get_address_of_time_12() { return &___time_12; }
	inline void set_time_12(FsmFloat_t2134102846 * value)
	{
		___time_12 = value;
		Il2CppCodeGenWriteBarrier(&___time_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___storeResult_13)); }
	inline FsmVector3_t533912882 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmVector3_t533912882 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmVector3_t533912882 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_finishEvent_14() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___finishEvent_14)); }
	inline FsmEvent_t2133468028 * get_finishEvent_14() const { return ___finishEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_14() { return &___finishEvent_14; }
	inline void set_finishEvent_14(FsmEvent_t2133468028 * value)
	{
		___finishEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_14, value);
	}

	inline static int32_t get_offset_of_realTime_15() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___realTime_15)); }
	inline bool get_realTime_15() const { return ___realTime_15; }
	inline bool* get_address_of_realTime_15() { return &___realTime_15; }
	inline void set_realTime_15(bool value)
	{
		___realTime_15 = value;
	}

	inline static int32_t get_offset_of_startTime_16() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___startTime_16)); }
	inline float get_startTime_16() const { return ___startTime_16; }
	inline float* get_address_of_startTime_16() { return &___startTime_16; }
	inline void set_startTime_16(float value)
	{
		___startTime_16 = value;
	}

	inline static int32_t get_offset_of_currentTime_17() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1605634041, ___currentTime_17)); }
	inline float get_currentTime_17() const { return ___currentTime_17; }
	inline float* get_address_of_currentTime_17() { return &___currentTime_17; }
	inline void set_currentTime_17(float value)
	{
		___currentTime_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
