﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2968701126.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3570646406_gshared (InternalEnumerator_1_t2968701126 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3570646406(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2968701126 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3570646406_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1117097562_gshared (InternalEnumerator_1_t2968701126 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1117097562(__this, method) ((  void (*) (InternalEnumerator_1_t2968701126 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1117097562_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1958444998_gshared (InternalEnumerator_1_t2968701126 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1958444998(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2968701126 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1958444998_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1374770909_gshared (InternalEnumerator_1_t2968701126 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1374770909(__this, method) ((  void (*) (InternalEnumerator_1_t2968701126 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1374770909_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3791934150_gshared (InternalEnumerator_1_t2968701126 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3791934150(__this, method) ((  bool (*) (InternalEnumerator_1_t2968701126 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3791934150_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t4186358450  InternalEnumerator_1_get_Current_m794903629_gshared (InternalEnumerator_1_t2968701126 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m794903629(__this, method) ((  KeyValuePair_2_t4186358450  (*) (InternalEnumerator_1_t2968701126 *, const MethodInfo*))InternalEnumerator_1_get_Current_m794903629_gshared)(__this, method)
