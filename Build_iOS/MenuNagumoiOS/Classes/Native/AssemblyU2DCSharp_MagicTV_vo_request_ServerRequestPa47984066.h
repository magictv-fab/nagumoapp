﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.request.ServerRequestParamsUpdateVO
struct  ServerRequestParamsUpdateVO_t47984066  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::_revision
	String_t* ____revision_0;
	// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::device_id
	String_t* ___device_id_1;
	// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::app
	String_t* ___app_2;
	// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::api
	String_t* ___api_3;
	// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::user_agent
	String_t* ___user_agent_4;
	// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::connection
	String_t* ___connection_5;

public:
	inline static int32_t get_offset_of__revision_0() { return static_cast<int32_t>(offsetof(ServerRequestParamsUpdateVO_t47984066, ____revision_0)); }
	inline String_t* get__revision_0() const { return ____revision_0; }
	inline String_t** get_address_of__revision_0() { return &____revision_0; }
	inline void set__revision_0(String_t* value)
	{
		____revision_0 = value;
		Il2CppCodeGenWriteBarrier(&____revision_0, value);
	}

	inline static int32_t get_offset_of_device_id_1() { return static_cast<int32_t>(offsetof(ServerRequestParamsUpdateVO_t47984066, ___device_id_1)); }
	inline String_t* get_device_id_1() const { return ___device_id_1; }
	inline String_t** get_address_of_device_id_1() { return &___device_id_1; }
	inline void set_device_id_1(String_t* value)
	{
		___device_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___device_id_1, value);
	}

	inline static int32_t get_offset_of_app_2() { return static_cast<int32_t>(offsetof(ServerRequestParamsUpdateVO_t47984066, ___app_2)); }
	inline String_t* get_app_2() const { return ___app_2; }
	inline String_t** get_address_of_app_2() { return &___app_2; }
	inline void set_app_2(String_t* value)
	{
		___app_2 = value;
		Il2CppCodeGenWriteBarrier(&___app_2, value);
	}

	inline static int32_t get_offset_of_api_3() { return static_cast<int32_t>(offsetof(ServerRequestParamsUpdateVO_t47984066, ___api_3)); }
	inline String_t* get_api_3() const { return ___api_3; }
	inline String_t** get_address_of_api_3() { return &___api_3; }
	inline void set_api_3(String_t* value)
	{
		___api_3 = value;
		Il2CppCodeGenWriteBarrier(&___api_3, value);
	}

	inline static int32_t get_offset_of_user_agent_4() { return static_cast<int32_t>(offsetof(ServerRequestParamsUpdateVO_t47984066, ___user_agent_4)); }
	inline String_t* get_user_agent_4() const { return ___user_agent_4; }
	inline String_t** get_address_of_user_agent_4() { return &___user_agent_4; }
	inline void set_user_agent_4(String_t* value)
	{
		___user_agent_4 = value;
		Il2CppCodeGenWriteBarrier(&___user_agent_4, value);
	}

	inline static int32_t get_offset_of_connection_5() { return static_cast<int32_t>(offsetof(ServerRequestParamsUpdateVO_t47984066, ___connection_5)); }
	inline String_t* get_connection_5() const { return ___connection_5; }
	inline String_t** get_address_of_connection_5() { return &___connection_5; }
	inline void set_connection_5(String_t* value)
	{
		___connection_5 = value;
		Il2CppCodeGenWriteBarrier(&___connection_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
