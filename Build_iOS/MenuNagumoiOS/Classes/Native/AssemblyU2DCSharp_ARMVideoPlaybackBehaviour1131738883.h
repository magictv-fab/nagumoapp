﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// VideoPlayerHelper
struct VideoPlayerHelper_t638710250;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Texture
struct Texture_t2526458961;
// ARMVideoPlaybackBehaviour/OnVoidEvent
struct OnVoidEvent_t2058934555;

#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2357615493.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaState2586048626.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaType3131497273.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMVideoPlaybackBehaviour
struct  ARMVideoPlaybackBehaviour_t1131738883  : public ViewTimeControllAbstract_t2357615493
{
public:
	// UnityEngine.GameObject ARMVideoPlaybackBehaviour::buttonGame
	GameObject_t3674682005 * ___buttonGame_6;
	// System.String ARMVideoPlaybackBehaviour::m_path
	String_t* ___m_path_7;
	// System.Boolean ARMVideoPlaybackBehaviour::loop
	bool ___loop_8;
	// VideoPlayerHelper/MediaState ARMVideoPlaybackBehaviour::ARMCurrentState
	int32_t ___ARMCurrentState_9;
	// VideoPlayerHelper ARMVideoPlaybackBehaviour::mVideoPlayer
	VideoPlayerHelper_t638710250 * ___mVideoPlayer_10;
	// System.Boolean ARMVideoPlaybackBehaviour::mIsInternalComponentInited
	bool ___mIsInternalComponentInited_11;
	// System.Boolean ARMVideoPlaybackBehaviour::mIsPrepared
	bool ___mIsPrepared_12;
	// UnityEngine.Texture2D ARMVideoPlaybackBehaviour::mVideoTexture
	Texture2D_t3884108195 * ___mVideoTexture_13;
	// System.Boolean ARMVideoPlaybackBehaviour::firstPlay
	bool ___firstPlay_14;
	// UnityEngine.Texture ARMVideoPlaybackBehaviour::mKeyframeTexture
	Texture_t2526458961 * ___mKeyframeTexture_15;
	// VideoPlayerHelper/MediaType ARMVideoPlaybackBehaviour::mMediaType
	int32_t ___mMediaType_16;
	// VideoPlayerHelper/MediaState ARMVideoPlaybackBehaviour::mCurrentState
	int32_t ___mCurrentState_17;
	// System.Single ARMVideoPlaybackBehaviour::mSeekPosition
	float ___mSeekPosition_18;
	// System.Single ARMVideoPlaybackBehaviour::mPlayPosition
	float ___mPlayPosition_19;
	// System.Boolean ARMVideoPlaybackBehaviour::isPlayableOnTexture
	bool ___isPlayableOnTexture_20;
	// System.Int32 ARMVideoPlaybackBehaviour::debug_status
	int32_t ___debug_status_21;
	// System.Boolean ARMVideoPlaybackBehaviour::initCalled
	bool ___initCalled_22;
	// ARMVideoPlaybackBehaviour/OnVoidEvent ARMVideoPlaybackBehaviour::onYouCanShowMe
	OnVoidEvent_t2058934555 * ___onYouCanShowMe_23;
	// System.Boolean ARMVideoPlaybackBehaviour::_onFinishedEventTrigger
	bool ____onFinishedEventTrigger_24;

public:
	inline static int32_t get_offset_of_buttonGame_6() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___buttonGame_6)); }
	inline GameObject_t3674682005 * get_buttonGame_6() const { return ___buttonGame_6; }
	inline GameObject_t3674682005 ** get_address_of_buttonGame_6() { return &___buttonGame_6; }
	inline void set_buttonGame_6(GameObject_t3674682005 * value)
	{
		___buttonGame_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonGame_6, value);
	}

	inline static int32_t get_offset_of_m_path_7() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___m_path_7)); }
	inline String_t* get_m_path_7() const { return ___m_path_7; }
	inline String_t** get_address_of_m_path_7() { return &___m_path_7; }
	inline void set_m_path_7(String_t* value)
	{
		___m_path_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_path_7, value);
	}

	inline static int32_t get_offset_of_loop_8() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___loop_8)); }
	inline bool get_loop_8() const { return ___loop_8; }
	inline bool* get_address_of_loop_8() { return &___loop_8; }
	inline void set_loop_8(bool value)
	{
		___loop_8 = value;
	}

	inline static int32_t get_offset_of_ARMCurrentState_9() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___ARMCurrentState_9)); }
	inline int32_t get_ARMCurrentState_9() const { return ___ARMCurrentState_9; }
	inline int32_t* get_address_of_ARMCurrentState_9() { return &___ARMCurrentState_9; }
	inline void set_ARMCurrentState_9(int32_t value)
	{
		___ARMCurrentState_9 = value;
	}

	inline static int32_t get_offset_of_mVideoPlayer_10() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mVideoPlayer_10)); }
	inline VideoPlayerHelper_t638710250 * get_mVideoPlayer_10() const { return ___mVideoPlayer_10; }
	inline VideoPlayerHelper_t638710250 ** get_address_of_mVideoPlayer_10() { return &___mVideoPlayer_10; }
	inline void set_mVideoPlayer_10(VideoPlayerHelper_t638710250 * value)
	{
		___mVideoPlayer_10 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoPlayer_10, value);
	}

	inline static int32_t get_offset_of_mIsInternalComponentInited_11() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mIsInternalComponentInited_11)); }
	inline bool get_mIsInternalComponentInited_11() const { return ___mIsInternalComponentInited_11; }
	inline bool* get_address_of_mIsInternalComponentInited_11() { return &___mIsInternalComponentInited_11; }
	inline void set_mIsInternalComponentInited_11(bool value)
	{
		___mIsInternalComponentInited_11 = value;
	}

	inline static int32_t get_offset_of_mIsPrepared_12() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mIsPrepared_12)); }
	inline bool get_mIsPrepared_12() const { return ___mIsPrepared_12; }
	inline bool* get_address_of_mIsPrepared_12() { return &___mIsPrepared_12; }
	inline void set_mIsPrepared_12(bool value)
	{
		___mIsPrepared_12 = value;
	}

	inline static int32_t get_offset_of_mVideoTexture_13() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mVideoTexture_13)); }
	inline Texture2D_t3884108195 * get_mVideoTexture_13() const { return ___mVideoTexture_13; }
	inline Texture2D_t3884108195 ** get_address_of_mVideoTexture_13() { return &___mVideoTexture_13; }
	inline void set_mVideoTexture_13(Texture2D_t3884108195 * value)
	{
		___mVideoTexture_13 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoTexture_13, value);
	}

	inline static int32_t get_offset_of_firstPlay_14() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___firstPlay_14)); }
	inline bool get_firstPlay_14() const { return ___firstPlay_14; }
	inline bool* get_address_of_firstPlay_14() { return &___firstPlay_14; }
	inline void set_firstPlay_14(bool value)
	{
		___firstPlay_14 = value;
	}

	inline static int32_t get_offset_of_mKeyframeTexture_15() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mKeyframeTexture_15)); }
	inline Texture_t2526458961 * get_mKeyframeTexture_15() const { return ___mKeyframeTexture_15; }
	inline Texture_t2526458961 ** get_address_of_mKeyframeTexture_15() { return &___mKeyframeTexture_15; }
	inline void set_mKeyframeTexture_15(Texture_t2526458961 * value)
	{
		___mKeyframeTexture_15 = value;
		Il2CppCodeGenWriteBarrier(&___mKeyframeTexture_15, value);
	}

	inline static int32_t get_offset_of_mMediaType_16() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mMediaType_16)); }
	inline int32_t get_mMediaType_16() const { return ___mMediaType_16; }
	inline int32_t* get_address_of_mMediaType_16() { return &___mMediaType_16; }
	inline void set_mMediaType_16(int32_t value)
	{
		___mMediaType_16 = value;
	}

	inline static int32_t get_offset_of_mCurrentState_17() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mCurrentState_17)); }
	inline int32_t get_mCurrentState_17() const { return ___mCurrentState_17; }
	inline int32_t* get_address_of_mCurrentState_17() { return &___mCurrentState_17; }
	inline void set_mCurrentState_17(int32_t value)
	{
		___mCurrentState_17 = value;
	}

	inline static int32_t get_offset_of_mSeekPosition_18() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mSeekPosition_18)); }
	inline float get_mSeekPosition_18() const { return ___mSeekPosition_18; }
	inline float* get_address_of_mSeekPosition_18() { return &___mSeekPosition_18; }
	inline void set_mSeekPosition_18(float value)
	{
		___mSeekPosition_18 = value;
	}

	inline static int32_t get_offset_of_mPlayPosition_19() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___mPlayPosition_19)); }
	inline float get_mPlayPosition_19() const { return ___mPlayPosition_19; }
	inline float* get_address_of_mPlayPosition_19() { return &___mPlayPosition_19; }
	inline void set_mPlayPosition_19(float value)
	{
		___mPlayPosition_19 = value;
	}

	inline static int32_t get_offset_of_isPlayableOnTexture_20() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___isPlayableOnTexture_20)); }
	inline bool get_isPlayableOnTexture_20() const { return ___isPlayableOnTexture_20; }
	inline bool* get_address_of_isPlayableOnTexture_20() { return &___isPlayableOnTexture_20; }
	inline void set_isPlayableOnTexture_20(bool value)
	{
		___isPlayableOnTexture_20 = value;
	}

	inline static int32_t get_offset_of_debug_status_21() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___debug_status_21)); }
	inline int32_t get_debug_status_21() const { return ___debug_status_21; }
	inline int32_t* get_address_of_debug_status_21() { return &___debug_status_21; }
	inline void set_debug_status_21(int32_t value)
	{
		___debug_status_21 = value;
	}

	inline static int32_t get_offset_of_initCalled_22() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___initCalled_22)); }
	inline bool get_initCalled_22() const { return ___initCalled_22; }
	inline bool* get_address_of_initCalled_22() { return &___initCalled_22; }
	inline void set_initCalled_22(bool value)
	{
		___initCalled_22 = value;
	}

	inline static int32_t get_offset_of_onYouCanShowMe_23() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ___onYouCanShowMe_23)); }
	inline OnVoidEvent_t2058934555 * get_onYouCanShowMe_23() const { return ___onYouCanShowMe_23; }
	inline OnVoidEvent_t2058934555 ** get_address_of_onYouCanShowMe_23() { return &___onYouCanShowMe_23; }
	inline void set_onYouCanShowMe_23(OnVoidEvent_t2058934555 * value)
	{
		___onYouCanShowMe_23 = value;
		Il2CppCodeGenWriteBarrier(&___onYouCanShowMe_23, value);
	}

	inline static int32_t get_offset_of__onFinishedEventTrigger_24() { return static_cast<int32_t>(offsetof(ARMVideoPlaybackBehaviour_t1131738883, ____onFinishedEventTrigger_24)); }
	inline bool get__onFinishedEventTrigger_24() const { return ____onFinishedEventTrigger_24; }
	inline bool* get_address_of__onFinishedEventTrigger_24() { return &____onFinishedEventTrigger_24; }
	inline void set__onFinishedEventTrigger_24(bool value)
	{
		____onFinishedEventTrigger_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
