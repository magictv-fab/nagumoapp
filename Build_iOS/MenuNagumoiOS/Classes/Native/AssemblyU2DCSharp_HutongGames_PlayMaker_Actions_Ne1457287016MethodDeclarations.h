﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NextFrameEvent
struct NextFrameEvent_t1457287016;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NextFrameEvent::.ctor()
extern "C"  void NextFrameEvent__ctor_m2920902142 (NextFrameEvent_t1457287016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NextFrameEvent::Reset()
extern "C"  void NextFrameEvent_Reset_m567335083 (NextFrameEvent_t1457287016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NextFrameEvent::OnEnter()
extern "C"  void NextFrameEvent_OnEnter_m1765042773 (NextFrameEvent_t1457287016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NextFrameEvent::OnUpdate()
extern "C"  void NextFrameEvent_OnUpdate_m2310277774 (NextFrameEvent_t1457287016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
