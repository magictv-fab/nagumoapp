﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>
struct ReadOnlyCollection_1_t2710916131;
// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_t3848485798;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Int64[]
struct Int64U5BU5D_t2174042770;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t3065703644;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3280093122_gshared (ReadOnlyCollection_1_t2710916131 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3280093122(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3280093122_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2866593964_gshared (ReadOnlyCollection_1_t2710916131 * __this, int64_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2866593964(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int64_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2866593964_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m676790686_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m676790686(__this, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m676790686_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4178664595_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, int64_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4178664595(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, int64_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4178664595_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1958995083_gshared (ReadOnlyCollection_1_t2710916131 * __this, int64_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1958995083(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, int64_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1958995083_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2052517465_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2052517465(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2052517465_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int64_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1227766207_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1227766207(__this, ___index0, method) ((  int64_t (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1227766207_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1711850666_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1711850666(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, int64_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1711850666_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2584655524_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2584655524(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2584655524_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m991468913_gshared (ReadOnlyCollection_1_t2710916131 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m991468913(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m991468913_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1341057600_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1341057600(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1341057600_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1199099261_gshared (ReadOnlyCollection_1_t2710916131 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1199099261(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2710916131 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1199099261_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1108324479_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1108324479(__this, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1108324479_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4071171683_gshared (ReadOnlyCollection_1_t2710916131 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m4071171683(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m4071171683_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3396074837_gshared (ReadOnlyCollection_1_t2710916131 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3396074837(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2710916131 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3396074837_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2499544136_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2499544136(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2499544136_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3057536992_gshared (ReadOnlyCollection_1_t2710916131 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3057536992(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3057536992_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1879721944_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1879721944(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1879721944_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2984079897_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2984079897(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2984079897_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m289757707_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m289757707(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m289757707_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3600423954_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3600423954(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3600423954_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2232719527_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2232719527(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2232719527_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m349893842_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m349893842(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m349893842_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1789148703_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1789148703(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1789148703_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2452822224_gshared (ReadOnlyCollection_1_t2710916131 * __this, int64_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2452822224(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2710916131 *, int64_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m2452822224_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1227642844_gshared (ReadOnlyCollection_1_t2710916131 * __this, Int64U5BU5D_t2174042770* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1227642844(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2710916131 *, Int64U5BU5D_t2174042770*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1227642844_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3889805735_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3889805735(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3889805735_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3845668264_gshared (ReadOnlyCollection_1_t2710916131 * __this, int64_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3845668264(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2710916131 *, int64_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3845668264_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3068906707_gshared (ReadOnlyCollection_1_t2710916131 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3068906707(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2710916131 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3068906707_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>::get_Item(System.Int32)
extern "C"  int64_t ReadOnlyCollection_1_get_Item_m2220773119_gshared (ReadOnlyCollection_1_t2710916131 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2220773119(__this, ___index0, method) ((  int64_t (*) (ReadOnlyCollection_1_t2710916131 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2220773119_gshared)(__this, ___index0, method)
