﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManagerGUI/<SaveExisting>c__Iterator2D
struct  U3CSaveExistingU3Ec__Iterator2D_t2015724046  : public Il2CppObject
{
public:
	// System.Boolean ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::<photoSaved>__0
	bool ___U3CphotoSavedU3E__0_0;
	// System.String ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::fileName
	String_t* ___fileName_1;
	// System.String ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::<path>__1
	String_t* ___U3CpathU3E__1_2;
	// System.Byte[] ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::bytes
	ByteU5BU5D_t4260760469* ___bytes_3;
	// System.Boolean ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::callback
	bool ___callback_4;
	// System.Int32 ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::$PC
	int32_t ___U24PC_5;
	// System.Object ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::$current
	Il2CppObject * ___U24current_6;
	// System.String ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::<$>fileName
	String_t* ___U3CU24U3EfileName_7;
	// System.Byte[] ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::<$>bytes
	ByteU5BU5D_t4260760469* ___U3CU24U3Ebytes_8;
	// System.Boolean ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::<$>callback
	bool ___U3CU24U3Ecallback_9;

public:
	inline static int32_t get_offset_of_U3CphotoSavedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U3CphotoSavedU3E__0_0)); }
	inline bool get_U3CphotoSavedU3E__0_0() const { return ___U3CphotoSavedU3E__0_0; }
	inline bool* get_address_of_U3CphotoSavedU3E__0_0() { return &___U3CphotoSavedU3E__0_0; }
	inline void set_U3CphotoSavedU3E__0_0(bool value)
	{
		___U3CphotoSavedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_fileName_1() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___fileName_1)); }
	inline String_t* get_fileName_1() const { return ___fileName_1; }
	inline String_t** get_address_of_fileName_1() { return &___fileName_1; }
	inline void set_fileName_1(String_t* value)
	{
		___fileName_1 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_1, value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U3CpathU3E__1_2)); }
	inline String_t* get_U3CpathU3E__1_2() const { return ___U3CpathU3E__1_2; }
	inline String_t** get_address_of_U3CpathU3E__1_2() { return &___U3CpathU3E__1_2; }
	inline void set_U3CpathU3E__1_2(String_t* value)
	{
		___U3CpathU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__1_2, value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___bytes_3)); }
	inline ByteU5BU5D_t4260760469* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_t4260760469* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier(&___bytes_3, value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___callback_4)); }
	inline bool get_callback_4() const { return ___callback_4; }
	inline bool* get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(bool value)
	{
		___callback_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EfileName_7() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U3CU24U3EfileName_7)); }
	inline String_t* get_U3CU24U3EfileName_7() const { return ___U3CU24U3EfileName_7; }
	inline String_t** get_address_of_U3CU24U3EfileName_7() { return &___U3CU24U3EfileName_7; }
	inline void set_U3CU24U3EfileName_7(String_t* value)
	{
		___U3CU24U3EfileName_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EfileName_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ebytes_8() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U3CU24U3Ebytes_8)); }
	inline ByteU5BU5D_t4260760469* get_U3CU24U3Ebytes_8() const { return ___U3CU24U3Ebytes_8; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CU24U3Ebytes_8() { return &___U3CU24U3Ebytes_8; }
	inline void set_U3CU24U3Ebytes_8(ByteU5BU5D_t4260760469* value)
	{
		___U3CU24U3Ebytes_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ebytes_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecallback_9() { return static_cast<int32_t>(offsetof(U3CSaveExistingU3Ec__Iterator2D_t2015724046, ___U3CU24U3Ecallback_9)); }
	inline bool get_U3CU24U3Ecallback_9() const { return ___U3CU24U3Ecallback_9; }
	inline bool* get_address_of_U3CU24U3Ecallback_9() { return &___U3CU24U3Ecallback_9; }
	inline void set_U3CU24U3Ecallback_9(bool value)
	{
		___U3CU24U3Ecallback_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
