﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ForwardAllEvents
struct  ForwardAllEvents_t4134244477  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.ForwardAllEvents::forwardTo
	FsmEventTarget_t1823904941 * ___forwardTo_9;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.ForwardAllEvents::exceptThese
	FsmEventU5BU5D_t2862142229* ___exceptThese_10;
	// System.Boolean HutongGames.PlayMaker.Actions.ForwardAllEvents::eatEvents
	bool ___eatEvents_11;

public:
	inline static int32_t get_offset_of_forwardTo_9() { return static_cast<int32_t>(offsetof(ForwardAllEvents_t4134244477, ___forwardTo_9)); }
	inline FsmEventTarget_t1823904941 * get_forwardTo_9() const { return ___forwardTo_9; }
	inline FsmEventTarget_t1823904941 ** get_address_of_forwardTo_9() { return &___forwardTo_9; }
	inline void set_forwardTo_9(FsmEventTarget_t1823904941 * value)
	{
		___forwardTo_9 = value;
		Il2CppCodeGenWriteBarrier(&___forwardTo_9, value);
	}

	inline static int32_t get_offset_of_exceptThese_10() { return static_cast<int32_t>(offsetof(ForwardAllEvents_t4134244477, ___exceptThese_10)); }
	inline FsmEventU5BU5D_t2862142229* get_exceptThese_10() const { return ___exceptThese_10; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_exceptThese_10() { return &___exceptThese_10; }
	inline void set_exceptThese_10(FsmEventU5BU5D_t2862142229* value)
	{
		___exceptThese_10 = value;
		Il2CppCodeGenWriteBarrier(&___exceptThese_10, value);
	}

	inline static int32_t get_offset_of_eatEvents_11() { return static_cast<int32_t>(offsetof(ForwardAllEvents_t4134244477, ___eatEvents_11)); }
	inline bool get_eatEvents_11() const { return ___eatEvents_11; }
	inline bool* get_address_of_eatEvents_11() { return &___eatEvents_11; }
	inline void set_eatEvents_11(bool value)
	{
		___eatEvents_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
