﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InfiniteScroll
struct InfiniteScroll_t1391012385;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void InfiniteScroll::.ctor()
extern "C"  void InfiniteScroll__ctor_m3545462682 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform InfiniteScroll::get_t()
extern "C"  RectTransform_t972643934 * InfiniteScroll_get_t_m3902555457 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::Awake()
extern "C"  void InfiniteScroll_Awake_m3783067901 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::Init()
extern "C"  void InfiniteScroll_Init_m1312832378 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::Update()
extern "C"  void InfiniteScroll_Update_m4262022835 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform InfiniteScroll::NewItemAtStart()
extern "C"  RectTransform_t972643934 * InfiniteScroll_NewItemAtStart_m4289474600 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform InfiniteScroll::NewItemAtEnd()
extern "C"  RectTransform_t972643934 * InfiniteScroll_NewItemAtEnd_m3799173025 (InfiniteScroll_t1391012385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform InfiniteScroll::InstantiateNextItem(System.Int32)
extern "C"  RectTransform_t972643934 * InfiniteScroll_InstantiateNextItem_m2482361175 (InfiniteScroll_t1391012385 * __this, int32_t ___itemType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void InfiniteScroll_OnBeginDrag_m95304680 (InfiniteScroll_t1391012385 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void InfiniteScroll_OnDrag_m3947990785 (InfiniteScroll_t1391012385 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::Subtract(System.Int32&)
extern "C"  void InfiniteScroll_Subtract_m1320566109 (InfiniteScroll_t1391012385 * __this, int32_t* ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InfiniteScroll::Add(System.Int32&)
extern "C"  void InfiniteScroll_Add_m3574393442 (InfiniteScroll_t1391012385 * __this, int32_t* ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
