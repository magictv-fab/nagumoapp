﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Light
struct Light_t4202674828;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Flare
struct Flare_t4197217604;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LightType1292142182.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Flare4197217604.h"

// System.Void UnityEngine.Light::set_type(UnityEngine.LightType)
extern "C"  void Light_set_type_m1196490817 (Light_t4202674828 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Light::get_color()
extern "C"  Color_t4194546905  Light_get_color_m2336101442 (Light_t4202674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C"  void Light_set_color_m763171967 (Light_t4202674828 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void Light_INTERNAL_get_color_m4212442015 (Light_t4202674828 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void Light_INTERNAL_set_color_m3939727787 (Light_t4202674828 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m2689709876 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowStrength(System.Single)
extern "C"  void Light_set_shadowStrength_m561788748 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_range(System.Single)
extern "C"  void Light_set_range_m1834313578 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_spotAngle(System.Single)
extern "C"  void Light_set_spotAngle_m3212810934 (Light_t4202674828 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
extern "C"  void Light_set_cookie_m2487166540 (Light_t4202674828 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_flare(UnityEngine.Flare)
extern "C"  void Light_set_flare_m3803200297 (Light_t4202674828 * __this, Flare_t4197217604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
