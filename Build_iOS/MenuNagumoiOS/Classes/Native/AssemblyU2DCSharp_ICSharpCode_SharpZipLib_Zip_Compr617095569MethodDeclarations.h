﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t617095569;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2348681196.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern "C"  void OutputWindow__ctor_m427954569 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern "C"  void OutputWindow_Write_m702222103 (OutputWindow_t617095569 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern "C"  void OutputWindow_SlowRepeat_m3900123464 (OutputWindow_t617095569 * __this, int32_t ___repStart0, int32_t ___length1, int32_t ___distance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern "C"  void OutputWindow_Repeat_m2424535280 (OutputWindow_t617095569 * __this, int32_t ___length0, int32_t ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern "C"  int32_t OutputWindow_CopyStored_m2655612723 (OutputWindow_t617095569 * __this, StreamManipulator_t2348681196 * ___input0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyDict(System.Byte[],System.Int32,System.Int32)
extern "C"  void OutputWindow_CopyDict_m2073279875 (OutputWindow_t617095569 * __this, ByteU5BU5D_t4260760469* ___dictionary0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern "C"  int32_t OutputWindow_GetFreeSpace_m448878573 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern "C"  int32_t OutputWindow_GetAvailable_m1658313980 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t OutputWindow_CopyOutput_m2310282250 (OutputWindow_t617095569 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___len2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern "C"  void OutputWindow_Reset_m2369354806 (OutputWindow_t617095569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
