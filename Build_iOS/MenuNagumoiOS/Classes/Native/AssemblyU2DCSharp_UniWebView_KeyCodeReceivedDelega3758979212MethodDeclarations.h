﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/KeyCodeReceivedDelegate
struct KeyCodeReceivedDelegate_t3758979212;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/KeyCodeReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void KeyCodeReceivedDelegate__ctor_m3221546291 (KeyCodeReceivedDelegate_t3758979212 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/KeyCodeReceivedDelegate::Invoke(UniWebView,System.Int32)
extern "C"  void KeyCodeReceivedDelegate_Invoke_m594094355 (KeyCodeReceivedDelegate_t3758979212 * __this, UniWebView_t424341801 * ___webView0, int32_t ___keyCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/KeyCodeReceivedDelegate::BeginInvoke(UniWebView,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * KeyCodeReceivedDelegate_BeginInvoke_m2932605980 (KeyCodeReceivedDelegate_t3758979212 * __this, UniWebView_t424341801 * ___webView0, int32_t ___keyCode1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/KeyCodeReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void KeyCodeReceivedDelegate_EndInvoke_m304670915 (KeyCodeReceivedDelegate_t3758979212 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
