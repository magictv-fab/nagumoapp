﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeBackgroundImage
struct ChangeBackgroundImage_t1317572317;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"

// System.Void ChangeBackgroundImage::.ctor()
extern "C"  void ChangeBackgroundImage__ctor_m1845809966 (ChangeBackgroundImage_t1317572317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage::Start()
extern "C"  void ChangeBackgroundImage_Start_m792947758 (ChangeBackgroundImage_t1317572317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage::StartLoad()
extern "C"  void ChangeBackgroundImage_StartLoad_m195829044 (ChangeBackgroundImage_t1317572317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage::OnDisable()
extern "C"  void ChangeBackgroundImage_OnDisable_m2045960341 (ChangeBackgroundImage_t1317572317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage::ChangeImage(System.String)
extern "C"  void ChangeBackgroundImage_ChangeImage_m2777408395 (ChangeBackgroundImage_t1317572317 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage::BtnClose()
extern "C"  void ChangeBackgroundImage_BtnClose_m1689830354 (ChangeBackgroundImage_t1317572317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage::ClickBackgroudCover()
extern "C"  void ChangeBackgroundImage_ClickBackgroudCover_m1619738911 (ChangeBackgroundImage_t1317572317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChangeBackgroundImage::ColorTransition(UnityEngine.Sprite)
extern "C"  Il2CppObject * ChangeBackgroundImage_ColorTransition_m2994857456 (ChangeBackgroundImage_t1317572317 * __this, Sprite_t3199167241 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
