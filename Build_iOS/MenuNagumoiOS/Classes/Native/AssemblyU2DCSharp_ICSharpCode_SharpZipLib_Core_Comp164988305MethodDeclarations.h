﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.CompletedFileHandler
struct CompletedFileHandler_t164988305;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.ScanEventArgs
struct ScanEventArgs_t1070518092;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca1070518092.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Core.CompletedFileHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CompletedFileHandler__ctor_m3457218328 (CompletedFileHandler_t164988305 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.CompletedFileHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanEventArgs)
extern "C"  void CompletedFileHandler_Invoke_m1301872552 (CompletedFileHandler_t164988305 * __this, Il2CppObject * ___sender0, ScanEventArgs_t1070518092 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Core.CompletedFileHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CompletedFileHandler_BeginInvoke_m2250752559 (CompletedFileHandler_t164988305 * __this, Il2CppObject * ___sender0, ScanEventArgs_t1070518092 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.CompletedFileHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CompletedFileHandler_EndInvoke_m283882792 (CompletedFileHandler_t164988305 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
