﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m3388149675(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1797354517(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, FsmGameObject_t1697147867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1890106197(__this, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m790601916(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, FsmGameObject_t1697147867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2928871230(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, FsmGameObject_t1697147867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2959422082(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1972985222(__this, ___index0, method) ((  FsmGameObject_t1697147867 * (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1368826515(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, FsmGameObject_t1697147867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m331854993(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3477298906(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1738328149(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3967502012(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3254225403 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3029489128(__this, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m211630224(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3405889300(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3254225403 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m548816191(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3820669321(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4061987663(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3953956044(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1408967352(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3034485759(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4154126106(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2168538687(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4040301910(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::Contains(T)
#define ReadOnlyCollection_1_Contains_m2200513155(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3254225403 *, FsmGameObject_t1697147867 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m3315299909(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3254225403 *, FsmGameObjectU5BU5D_t1706220122*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2167042406(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m485352585(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3254225403 *, FsmGameObject_t1697147867 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1978335762(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3254225403 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.FsmGameObject>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m4041165382(__this, ___index0, method) ((  FsmGameObject_t1697147867 * (*) (ReadOnlyCollection_1_t3254225403 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
