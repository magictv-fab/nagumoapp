﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateAdd
struct iTweenRotateAdd_t368396562;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::.ctor()
extern "C"  void iTweenRotateAdd__ctor_m3970761316 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::Reset()
extern "C"  void iTweenRotateAdd_Reset_m1617194257 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::OnEnter()
extern "C"  void iTweenRotateAdd_OnEnter_m1362394427 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::OnExit()
extern "C"  void iTweenRotateAdd_OnExit_m52867837 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::DoiTween()
extern "C"  void iTweenRotateAdd_DoiTween_m735686509 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
