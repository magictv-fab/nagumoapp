﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMouseX
struct  GetMouseX_t738295447  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetMouseX::storeResult
	FsmFloat_t2134102846 * ___storeResult_9;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMouseX::normalize
	bool ___normalize_10;

public:
	inline static int32_t get_offset_of_storeResult_9() { return static_cast<int32_t>(offsetof(GetMouseX_t738295447, ___storeResult_9)); }
	inline FsmFloat_t2134102846 * get_storeResult_9() const { return ___storeResult_9; }
	inline FsmFloat_t2134102846 ** get_address_of_storeResult_9() { return &___storeResult_9; }
	inline void set_storeResult_9(FsmFloat_t2134102846 * value)
	{
		___storeResult_9 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_9, value);
	}

	inline static int32_t get_offset_of_normalize_10() { return static_cast<int32_t>(offsetof(GetMouseX_t738295447, ___normalize_10)); }
	inline bool get_normalize_10() const { return ___normalize_10; }
	inline bool* get_address_of_normalize_10() { return &___normalize_10; }
	inline void set_normalize_10(bool value)
	{
		___normalize_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
