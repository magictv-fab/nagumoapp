﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ARM.utils.download.SingleGenericDownloader
struct SingleGenericDownloader_t168833466;
// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler
struct OnCompleteEventHandler_t2318258528;

#include "AssemblyU2DCSharp_ARM_utils_download_GenericDownlo3298305714.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.download.SingleGenericDownloader
struct  SingleGenericDownloader_t168833466  : public GenericDownloadManager_t3298305714
{
public:
	// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler ARM.utils.download.SingleGenericDownloader::onBusy
	OnCompleteEventHandler_t2318258528 * ___onBusy_21;
	// System.Boolean ARM.utils.download.SingleGenericDownloader::_isDownloading
	bool ____isDownloading_22;

public:
	inline static int32_t get_offset_of_onBusy_21() { return static_cast<int32_t>(offsetof(SingleGenericDownloader_t168833466, ___onBusy_21)); }
	inline OnCompleteEventHandler_t2318258528 * get_onBusy_21() const { return ___onBusy_21; }
	inline OnCompleteEventHandler_t2318258528 ** get_address_of_onBusy_21() { return &___onBusy_21; }
	inline void set_onBusy_21(OnCompleteEventHandler_t2318258528 * value)
	{
		___onBusy_21 = value;
		Il2CppCodeGenWriteBarrier(&___onBusy_21, value);
	}

	inline static int32_t get_offset_of__isDownloading_22() { return static_cast<int32_t>(offsetof(SingleGenericDownloader_t168833466, ____isDownloading_22)); }
	inline bool get__isDownloading_22() const { return ____isDownloading_22; }
	inline bool* get_address_of__isDownloading_22() { return &____isDownloading_22; }
	inline void set__isDownloading_22(bool value)
	{
		____isDownloading_22 = value;
	}
};

struct SingleGenericDownloader_t168833466_StaticFields
{
public:
	// UnityEngine.GameObject ARM.utils.download.SingleGenericDownloader::_selfGameObject
	GameObject_t3674682005 * ____selfGameObject_19;
	// ARM.utils.download.SingleGenericDownloader ARM.utils.download.SingleGenericDownloader::_selfInstance
	SingleGenericDownloader_t168833466 * ____selfInstance_20;

public:
	inline static int32_t get_offset_of__selfGameObject_19() { return static_cast<int32_t>(offsetof(SingleGenericDownloader_t168833466_StaticFields, ____selfGameObject_19)); }
	inline GameObject_t3674682005 * get__selfGameObject_19() const { return ____selfGameObject_19; }
	inline GameObject_t3674682005 ** get_address_of__selfGameObject_19() { return &____selfGameObject_19; }
	inline void set__selfGameObject_19(GameObject_t3674682005 * value)
	{
		____selfGameObject_19 = value;
		Il2CppCodeGenWriteBarrier(&____selfGameObject_19, value);
	}

	inline static int32_t get_offset_of__selfInstance_20() { return static_cast<int32_t>(offsetof(SingleGenericDownloader_t168833466_StaticFields, ____selfInstance_20)); }
	inline SingleGenericDownloader_t168833466 * get__selfInstance_20() const { return ____selfInstance_20; }
	inline SingleGenericDownloader_t168833466 ** get_address_of__selfInstance_20() { return &____selfInstance_20; }
	inline void set__selfInstance_20(SingleGenericDownloader_t168833466 * value)
	{
		____selfInstance_20 = value;
		Il2CppCodeGenWriteBarrier(&____selfInstance_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
