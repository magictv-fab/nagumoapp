﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UniWebViewNativeListener
struct UniWebViewNativeListener_t795571060;
// System.Collections.Generic.Dictionary`2<System.String,System.Action>
struct Dictionary_2_t296684972;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>
struct Dictionary_2_t2212926459;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UniWebView/PageStartedDelegate
struct PageStartedDelegate_t2373082257;
// UniWebView/PageFinishedDelegate
struct PageFinishedDelegate_t1325724172;
// UniWebView/PageErrorReceivedDelegate
struct PageErrorReceivedDelegate_t4149620697;
// UniWebView/MessageReceivedDelegate
struct MessageReceivedDelegate_t2778163623;
// UniWebView/ShouldCloseDelegate
struct ShouldCloseDelegate_t2209923748;
// UniWebView/KeyCodeReceivedDelegate
struct KeyCodeReceivedDelegate_t3758979212;
// UniWebView/OreintationChangedDelegate
struct OreintationChangedDelegate_t3987443383;
// UniWebView/OnWebContentProcessTerminatedDelegate
struct OnWebContentProcessTerminatedDelegate_t1791934381;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UniWebViewToolbarPosition920849755.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView
struct  UniWebView_t424341801  : public MonoBehaviour_t667441552
{
public:
	// System.String UniWebView::id
	String_t* ___id_2;
	// UniWebViewNativeListener UniWebView::listener
	UniWebViewNativeListener_t795571060 * ___listener_3;
	// System.Boolean UniWebView::isPortrait
	bool ___isPortrait_4;
	// System.String UniWebView::urlOnStart
	String_t* ___urlOnStart_5;
	// System.Boolean UniWebView::showOnStart
	bool ___showOnStart_6;
	// System.Boolean UniWebView::isBackButton
	bool ___isBackButton_7;
	// System.Boolean UniWebView::isZoom
	bool ___isZoom_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action> UniWebView::actions
	Dictionary_2_t296684972 * ___actions_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<UniWebViewNativeResultPayload>> UniWebView::payloadActions
	Dictionary_2_t2212926459 * ___payloadActions_10;
	// System.Boolean UniWebView::fullScreen
	bool ___fullScreen_11;
	// UnityEngine.Rect UniWebView::frame
	Rect_t4241904616  ___frame_12;
	// UnityEngine.RectTransform UniWebView::referenceRectTransform
	RectTransform_t972643934 * ___referenceRectTransform_13;
	// System.Boolean UniWebView::useToolbar
	bool ___useToolbar_14;
	// UniWebViewToolbarPosition UniWebView::toolbarPosition
	int32_t ___toolbarPosition_15;
	// System.Boolean UniWebView::started
	bool ___started_16;
	// UnityEngine.Color UniWebView::backgroundColor
	Color_t4194546905  ___backgroundColor_17;
	// UniWebView/PageStartedDelegate UniWebView::OnPageStarted
	PageStartedDelegate_t2373082257 * ___OnPageStarted_18;
	// UniWebView/PageFinishedDelegate UniWebView::OnPageFinished
	PageFinishedDelegate_t1325724172 * ___OnPageFinished_19;
	// UniWebView/PageErrorReceivedDelegate UniWebView::OnPageErrorReceived
	PageErrorReceivedDelegate_t4149620697 * ___OnPageErrorReceived_20;
	// UniWebView/MessageReceivedDelegate UniWebView::OnMessageReceived
	MessageReceivedDelegate_t2778163623 * ___OnMessageReceived_21;
	// UniWebView/ShouldCloseDelegate UniWebView::OnShouldClose
	ShouldCloseDelegate_t2209923748 * ___OnShouldClose_22;
	// UniWebView/KeyCodeReceivedDelegate UniWebView::OnKeyCodeReceived
	KeyCodeReceivedDelegate_t3758979212 * ___OnKeyCodeReceived_23;
	// UniWebView/OreintationChangedDelegate UniWebView::OnOreintationChanged
	OreintationChangedDelegate_t3987443383 * ___OnOreintationChanged_24;
	// UniWebView/OnWebContentProcessTerminatedDelegate UniWebView::OnWebContentProcessTerminated
	OnWebContentProcessTerminatedDelegate_t1791934381 * ___OnWebContentProcessTerminated_25;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier(&___id_2, value);
	}

	inline static int32_t get_offset_of_listener_3() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___listener_3)); }
	inline UniWebViewNativeListener_t795571060 * get_listener_3() const { return ___listener_3; }
	inline UniWebViewNativeListener_t795571060 ** get_address_of_listener_3() { return &___listener_3; }
	inline void set_listener_3(UniWebViewNativeListener_t795571060 * value)
	{
		___listener_3 = value;
		Il2CppCodeGenWriteBarrier(&___listener_3, value);
	}

	inline static int32_t get_offset_of_isPortrait_4() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___isPortrait_4)); }
	inline bool get_isPortrait_4() const { return ___isPortrait_4; }
	inline bool* get_address_of_isPortrait_4() { return &___isPortrait_4; }
	inline void set_isPortrait_4(bool value)
	{
		___isPortrait_4 = value;
	}

	inline static int32_t get_offset_of_urlOnStart_5() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___urlOnStart_5)); }
	inline String_t* get_urlOnStart_5() const { return ___urlOnStart_5; }
	inline String_t** get_address_of_urlOnStart_5() { return &___urlOnStart_5; }
	inline void set_urlOnStart_5(String_t* value)
	{
		___urlOnStart_5 = value;
		Il2CppCodeGenWriteBarrier(&___urlOnStart_5, value);
	}

	inline static int32_t get_offset_of_showOnStart_6() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___showOnStart_6)); }
	inline bool get_showOnStart_6() const { return ___showOnStart_6; }
	inline bool* get_address_of_showOnStart_6() { return &___showOnStart_6; }
	inline void set_showOnStart_6(bool value)
	{
		___showOnStart_6 = value;
	}

	inline static int32_t get_offset_of_isBackButton_7() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___isBackButton_7)); }
	inline bool get_isBackButton_7() const { return ___isBackButton_7; }
	inline bool* get_address_of_isBackButton_7() { return &___isBackButton_7; }
	inline void set_isBackButton_7(bool value)
	{
		___isBackButton_7 = value;
	}

	inline static int32_t get_offset_of_isZoom_8() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___isZoom_8)); }
	inline bool get_isZoom_8() const { return ___isZoom_8; }
	inline bool* get_address_of_isZoom_8() { return &___isZoom_8; }
	inline void set_isZoom_8(bool value)
	{
		___isZoom_8 = value;
	}

	inline static int32_t get_offset_of_actions_9() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___actions_9)); }
	inline Dictionary_2_t296684972 * get_actions_9() const { return ___actions_9; }
	inline Dictionary_2_t296684972 ** get_address_of_actions_9() { return &___actions_9; }
	inline void set_actions_9(Dictionary_2_t296684972 * value)
	{
		___actions_9 = value;
		Il2CppCodeGenWriteBarrier(&___actions_9, value);
	}

	inline static int32_t get_offset_of_payloadActions_10() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___payloadActions_10)); }
	inline Dictionary_2_t2212926459 * get_payloadActions_10() const { return ___payloadActions_10; }
	inline Dictionary_2_t2212926459 ** get_address_of_payloadActions_10() { return &___payloadActions_10; }
	inline void set_payloadActions_10(Dictionary_2_t2212926459 * value)
	{
		___payloadActions_10 = value;
		Il2CppCodeGenWriteBarrier(&___payloadActions_10, value);
	}

	inline static int32_t get_offset_of_fullScreen_11() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___fullScreen_11)); }
	inline bool get_fullScreen_11() const { return ___fullScreen_11; }
	inline bool* get_address_of_fullScreen_11() { return &___fullScreen_11; }
	inline void set_fullScreen_11(bool value)
	{
		___fullScreen_11 = value;
	}

	inline static int32_t get_offset_of_frame_12() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___frame_12)); }
	inline Rect_t4241904616  get_frame_12() const { return ___frame_12; }
	inline Rect_t4241904616 * get_address_of_frame_12() { return &___frame_12; }
	inline void set_frame_12(Rect_t4241904616  value)
	{
		___frame_12 = value;
	}

	inline static int32_t get_offset_of_referenceRectTransform_13() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___referenceRectTransform_13)); }
	inline RectTransform_t972643934 * get_referenceRectTransform_13() const { return ___referenceRectTransform_13; }
	inline RectTransform_t972643934 ** get_address_of_referenceRectTransform_13() { return &___referenceRectTransform_13; }
	inline void set_referenceRectTransform_13(RectTransform_t972643934 * value)
	{
		___referenceRectTransform_13 = value;
		Il2CppCodeGenWriteBarrier(&___referenceRectTransform_13, value);
	}

	inline static int32_t get_offset_of_useToolbar_14() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___useToolbar_14)); }
	inline bool get_useToolbar_14() const { return ___useToolbar_14; }
	inline bool* get_address_of_useToolbar_14() { return &___useToolbar_14; }
	inline void set_useToolbar_14(bool value)
	{
		___useToolbar_14 = value;
	}

	inline static int32_t get_offset_of_toolbarPosition_15() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___toolbarPosition_15)); }
	inline int32_t get_toolbarPosition_15() const { return ___toolbarPosition_15; }
	inline int32_t* get_address_of_toolbarPosition_15() { return &___toolbarPosition_15; }
	inline void set_toolbarPosition_15(int32_t value)
	{
		___toolbarPosition_15 = value;
	}

	inline static int32_t get_offset_of_started_16() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___started_16)); }
	inline bool get_started_16() const { return ___started_16; }
	inline bool* get_address_of_started_16() { return &___started_16; }
	inline void set_started_16(bool value)
	{
		___started_16 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_17() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___backgroundColor_17)); }
	inline Color_t4194546905  get_backgroundColor_17() const { return ___backgroundColor_17; }
	inline Color_t4194546905 * get_address_of_backgroundColor_17() { return &___backgroundColor_17; }
	inline void set_backgroundColor_17(Color_t4194546905  value)
	{
		___backgroundColor_17 = value;
	}

	inline static int32_t get_offset_of_OnPageStarted_18() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnPageStarted_18)); }
	inline PageStartedDelegate_t2373082257 * get_OnPageStarted_18() const { return ___OnPageStarted_18; }
	inline PageStartedDelegate_t2373082257 ** get_address_of_OnPageStarted_18() { return &___OnPageStarted_18; }
	inline void set_OnPageStarted_18(PageStartedDelegate_t2373082257 * value)
	{
		___OnPageStarted_18 = value;
		Il2CppCodeGenWriteBarrier(&___OnPageStarted_18, value);
	}

	inline static int32_t get_offset_of_OnPageFinished_19() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnPageFinished_19)); }
	inline PageFinishedDelegate_t1325724172 * get_OnPageFinished_19() const { return ___OnPageFinished_19; }
	inline PageFinishedDelegate_t1325724172 ** get_address_of_OnPageFinished_19() { return &___OnPageFinished_19; }
	inline void set_OnPageFinished_19(PageFinishedDelegate_t1325724172 * value)
	{
		___OnPageFinished_19 = value;
		Il2CppCodeGenWriteBarrier(&___OnPageFinished_19, value);
	}

	inline static int32_t get_offset_of_OnPageErrorReceived_20() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnPageErrorReceived_20)); }
	inline PageErrorReceivedDelegate_t4149620697 * get_OnPageErrorReceived_20() const { return ___OnPageErrorReceived_20; }
	inline PageErrorReceivedDelegate_t4149620697 ** get_address_of_OnPageErrorReceived_20() { return &___OnPageErrorReceived_20; }
	inline void set_OnPageErrorReceived_20(PageErrorReceivedDelegate_t4149620697 * value)
	{
		___OnPageErrorReceived_20 = value;
		Il2CppCodeGenWriteBarrier(&___OnPageErrorReceived_20, value);
	}

	inline static int32_t get_offset_of_OnMessageReceived_21() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnMessageReceived_21)); }
	inline MessageReceivedDelegate_t2778163623 * get_OnMessageReceived_21() const { return ___OnMessageReceived_21; }
	inline MessageReceivedDelegate_t2778163623 ** get_address_of_OnMessageReceived_21() { return &___OnMessageReceived_21; }
	inline void set_OnMessageReceived_21(MessageReceivedDelegate_t2778163623 * value)
	{
		___OnMessageReceived_21 = value;
		Il2CppCodeGenWriteBarrier(&___OnMessageReceived_21, value);
	}

	inline static int32_t get_offset_of_OnShouldClose_22() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnShouldClose_22)); }
	inline ShouldCloseDelegate_t2209923748 * get_OnShouldClose_22() const { return ___OnShouldClose_22; }
	inline ShouldCloseDelegate_t2209923748 ** get_address_of_OnShouldClose_22() { return &___OnShouldClose_22; }
	inline void set_OnShouldClose_22(ShouldCloseDelegate_t2209923748 * value)
	{
		___OnShouldClose_22 = value;
		Il2CppCodeGenWriteBarrier(&___OnShouldClose_22, value);
	}

	inline static int32_t get_offset_of_OnKeyCodeReceived_23() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnKeyCodeReceived_23)); }
	inline KeyCodeReceivedDelegate_t3758979212 * get_OnKeyCodeReceived_23() const { return ___OnKeyCodeReceived_23; }
	inline KeyCodeReceivedDelegate_t3758979212 ** get_address_of_OnKeyCodeReceived_23() { return &___OnKeyCodeReceived_23; }
	inline void set_OnKeyCodeReceived_23(KeyCodeReceivedDelegate_t3758979212 * value)
	{
		___OnKeyCodeReceived_23 = value;
		Il2CppCodeGenWriteBarrier(&___OnKeyCodeReceived_23, value);
	}

	inline static int32_t get_offset_of_OnOreintationChanged_24() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnOreintationChanged_24)); }
	inline OreintationChangedDelegate_t3987443383 * get_OnOreintationChanged_24() const { return ___OnOreintationChanged_24; }
	inline OreintationChangedDelegate_t3987443383 ** get_address_of_OnOreintationChanged_24() { return &___OnOreintationChanged_24; }
	inline void set_OnOreintationChanged_24(OreintationChangedDelegate_t3987443383 * value)
	{
		___OnOreintationChanged_24 = value;
		Il2CppCodeGenWriteBarrier(&___OnOreintationChanged_24, value);
	}

	inline static int32_t get_offset_of_OnWebContentProcessTerminated_25() { return static_cast<int32_t>(offsetof(UniWebView_t424341801, ___OnWebContentProcessTerminated_25)); }
	inline OnWebContentProcessTerminatedDelegate_t1791934381 * get_OnWebContentProcessTerminated_25() const { return ___OnWebContentProcessTerminated_25; }
	inline OnWebContentProcessTerminatedDelegate_t1791934381 ** get_address_of_OnWebContentProcessTerminated_25() { return &___OnWebContentProcessTerminated_25; }
	inline void set_OnWebContentProcessTerminated_25(OnWebContentProcessTerminatedDelegate_t1791934381 * value)
	{
		___OnWebContentProcessTerminated_25 = value;
		Il2CppCodeGenWriteBarrier(&___OnWebContentProcessTerminated_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
