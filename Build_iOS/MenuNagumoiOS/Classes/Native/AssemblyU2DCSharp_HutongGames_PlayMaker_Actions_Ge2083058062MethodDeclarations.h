﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmVariable
struct GetFsmVariable_t2083058062;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::.ctor()
extern "C"  void GetFsmVariable__ctor_m1285944984 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::Reset()
extern "C"  void GetFsmVariable_Reset_m3227345221 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::OnEnter()
extern "C"  void GetFsmVariable_OnEnter_m2529244271 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::OnUpdate()
extern "C"  void GetFsmVariable_OnUpdate_m230720436 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::InitFsmVar()
extern "C"  void GetFsmVariable_InitFsmVar_m1467331747 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::DoGetFsmVariable()
extern "C"  void GetFsmVariable_DoGetFsmVariable_m3357830909 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
