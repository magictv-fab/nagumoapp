﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Timers.ElapsedEventArgs
struct ElapsedEventArgs_t2035959611;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void System.Timers.ElapsedEventArgs::.ctor(System.DateTime)
extern "C"  void ElapsedEventArgs__ctor_m1677043672 (ElapsedEventArgs_t2035959611 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
