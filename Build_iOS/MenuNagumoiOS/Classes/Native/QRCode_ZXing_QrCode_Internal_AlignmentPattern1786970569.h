﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_ResultPoint1538592853.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.AlignmentPattern
struct  AlignmentPattern_t1786970569  : public ResultPoint_t1538592853
{
public:
	// System.Single ZXing.QrCode.Internal.AlignmentPattern::estimatedModuleSize
	float ___estimatedModuleSize_5;

public:
	inline static int32_t get_offset_of_estimatedModuleSize_5() { return static_cast<int32_t>(offsetof(AlignmentPattern_t1786970569, ___estimatedModuleSize_5)); }
	inline float get_estimatedModuleSize_5() const { return ___estimatedModuleSize_5; }
	inline float* get_address_of_estimatedModuleSize_5() { return &___estimatedModuleSize_5; }
	inline void set_estimatedModuleSize_5(float value)
	{
		___estimatedModuleSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
