﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager/<ILoadImgs>c__Iterator54
struct U3CILoadImgsU3Ec__Iterator54_t655541284;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FavoritosManager/<ILoadImgs>c__Iterator54::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator54__ctor_m93002183 (U3CILoadImgsU3Ec__Iterator54_t655541284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ILoadImgs>c__Iterator54::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator54_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1999429163 (U3CILoadImgsU3Ec__Iterator54_t655541284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ILoadImgs>c__Iterator54::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator54_System_Collections_IEnumerator_get_Current_m729716671 (U3CILoadImgsU3Ec__Iterator54_t655541284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FavoritosManager/<ILoadImgs>c__Iterator54::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator54_MoveNext_m4013509581 (U3CILoadImgsU3Ec__Iterator54_t655541284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ILoadImgs>c__Iterator54::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator54_Dispose_m3374035012 (U3CILoadImgsU3Ec__Iterator54_t655541284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ILoadImgs>c__Iterator54::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator54_Reset_m2034402420 (U3CILoadImgsU3Ec__Iterator54_t655541284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
