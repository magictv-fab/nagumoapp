﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReturnConfigTest
struct  ReturnConfigTest_t510487652  : public MonoBehaviour_t667441552
{
public:
	// System.Single ReturnConfigTest::f
	float ___f_2;
	// UnityEngine.Vector3 ReturnConfigTest::v
	Vector3_t4282066566  ___v_3;

public:
	inline static int32_t get_offset_of_f_2() { return static_cast<int32_t>(offsetof(ReturnConfigTest_t510487652, ___f_2)); }
	inline float get_f_2() const { return ___f_2; }
	inline float* get_address_of_f_2() { return &___f_2; }
	inline void set_f_2(float value)
	{
		___f_2 = value;
	}

	inline static int32_t get_offset_of_v_3() { return static_cast<int32_t>(offsetof(ReturnConfigTest_t510487652, ___v_3)); }
	inline Vector3_t4282066566  get_v_3() const { return ___v_3; }
	inline Vector3_t4282066566 * get_address_of_v_3() { return &___v_3; }
	inline void set_v_3(Vector3_t4282066566  value)
	{
		___v_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
