﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler
struct OnReadyEventHandler_t1972392543;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.bytes.ByteListAbstract
struct  ByteListAbstract_t980263662  : public MonoBehaviour_t667441552
{
public:
	// System.UInt32 ARM.utils.bytes.ByteListAbstract::_maxByte
	uint32_t ____maxByte_2;
	// System.Boolean ARM.utils.bytes.ByteListAbstract::IsReady
	bool ___IsReady_3;
	// ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler ARM.utils.bytes.ByteListAbstract::onReadyEvent
	OnReadyEventHandler_t1972392543 * ___onReadyEvent_4;

public:
	inline static int32_t get_offset_of__maxByte_2() { return static_cast<int32_t>(offsetof(ByteListAbstract_t980263662, ____maxByte_2)); }
	inline uint32_t get__maxByte_2() const { return ____maxByte_2; }
	inline uint32_t* get_address_of__maxByte_2() { return &____maxByte_2; }
	inline void set__maxByte_2(uint32_t value)
	{
		____maxByte_2 = value;
	}

	inline static int32_t get_offset_of_IsReady_3() { return static_cast<int32_t>(offsetof(ByteListAbstract_t980263662, ___IsReady_3)); }
	inline bool get_IsReady_3() const { return ___IsReady_3; }
	inline bool* get_address_of_IsReady_3() { return &___IsReady_3; }
	inline void set_IsReady_3(bool value)
	{
		___IsReady_3 = value;
	}

	inline static int32_t get_offset_of_onReadyEvent_4() { return static_cast<int32_t>(offsetof(ByteListAbstract_t980263662, ___onReadyEvent_4)); }
	inline OnReadyEventHandler_t1972392543 * get_onReadyEvent_4() const { return ___onReadyEvent_4; }
	inline OnReadyEventHandler_t1972392543 ** get_address_of_onReadyEvent_4() { return &___onReadyEvent_4; }
	inline void set_onReadyEvent_4(OnReadyEventHandler_t1972392543 * value)
	{
		___onReadyEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___onReadyEvent_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
