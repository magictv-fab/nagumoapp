﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.utils.UrlHelper
struct UrlHelper_t1208334183;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"

// System.Void MagicTV.utils.UrlHelper::.ctor()
extern "C"  void UrlHelper__ctor_m2848392906 (UrlHelper_t1208334183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.UrlHelper::DeleteZippedFile(MagicTV.vo.UrlInfoVO)
extern "C"  void UrlHelper_DeleteZippedFile_m1151169679 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.UrlHelper::GetFolderToTrack(MagicTV.vo.UrlInfoVO)
extern "C"  String_t* UrlHelper_GetFolderToTrack_m3760068963 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.UrlHelper::UnzipFolder(MagicTV.vo.UrlInfoVO)
extern "C"  void UrlHelper_UnzipFolder_m1145827646 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.UrlHelper::ConvertMp3ToWav(MagicTV.vo.UrlInfoVO,UnityEngine.WWW)
extern "C"  void UrlHelper_ConvertMp3ToWav_m2519162920 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___url0, WWW_t3134621005 * ___www1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
