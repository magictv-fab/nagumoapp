﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectTagSwitch
struct  GameObjectTagSwitch_t3433592875  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectTagSwitch::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.GameObjectTagSwitch::compareTo
	FsmStringU5BU5D_t2523845914* ___compareTo_10;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.GameObjectTagSwitch::sendEvent
	FsmEventU5BU5D_t2862142229* ___sendEvent_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectTagSwitch::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3433592875, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_compareTo_10() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3433592875, ___compareTo_10)); }
	inline FsmStringU5BU5D_t2523845914* get_compareTo_10() const { return ___compareTo_10; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_compareTo_10() { return &___compareTo_10; }
	inline void set_compareTo_10(FsmStringU5BU5D_t2523845914* value)
	{
		___compareTo_10 = value;
		Il2CppCodeGenWriteBarrier(&___compareTo_10, value);
	}

	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3433592875, ___sendEvent_11)); }
	inline FsmEventU5BU5D_t2862142229* get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmEventU5BU5D_t2862142229* value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3433592875, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
