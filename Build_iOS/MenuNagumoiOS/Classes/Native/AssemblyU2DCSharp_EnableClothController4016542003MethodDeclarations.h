﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnableClothController
struct EnableClothController_t4016542003;

#include "codegen/il2cpp-codegen.h"

// System.Void EnableClothController::.ctor()
extern "C"  void EnableClothController__ctor_m4220785048 (EnableClothController_t4016542003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnableClothController::EnableCloth()
extern "C"  void EnableClothController_EnableCloth_m1643586445 (EnableClothController_t4016542003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
