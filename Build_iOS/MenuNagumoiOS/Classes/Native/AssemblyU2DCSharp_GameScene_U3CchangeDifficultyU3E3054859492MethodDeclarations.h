﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameScene/<changeDifficulty>c__Iterator43
struct U3CchangeDifficultyU3Ec__Iterator43_t3054859492;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameScene/<changeDifficulty>c__Iterator43::.ctor()
extern "C"  void U3CchangeDifficultyU3Ec__Iterator43__ctor_m3986899207 (U3CchangeDifficultyU3Ec__Iterator43_t3054859492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameScene/<changeDifficulty>c__Iterator43::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CchangeDifficultyU3Ec__Iterator43_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m624869099 (U3CchangeDifficultyU3Ec__Iterator43_t3054859492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameScene/<changeDifficulty>c__Iterator43::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CchangeDifficultyU3Ec__Iterator43_System_Collections_IEnumerator_get_Current_m3706321023 (U3CchangeDifficultyU3Ec__Iterator43_t3054859492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameScene/<changeDifficulty>c__Iterator43::MoveNext()
extern "C"  bool U3CchangeDifficultyU3Ec__Iterator43_MoveNext_m33086605 (U3CchangeDifficultyU3Ec__Iterator43_t3054859492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene/<changeDifficulty>c__Iterator43::Dispose()
extern "C"  void U3CchangeDifficultyU3Ec__Iterator43_Dispose_m197592964 (U3CchangeDifficultyU3Ec__Iterator43_t3054859492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene/<changeDifficulty>c__Iterator43::Reset()
extern "C"  void U3CchangeDifficultyU3Ec__Iterator43_Reset_m1633332148 (U3CchangeDifficultyU3Ec__Iterator43_t3054859492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
