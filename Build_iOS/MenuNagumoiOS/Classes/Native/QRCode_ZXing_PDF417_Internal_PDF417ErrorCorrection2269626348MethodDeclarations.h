﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Int32 ZXing.PDF417.Internal.PDF417ErrorCorrection::getErrorCorrectionCodewordCount(System.Int32)
extern "C"  int32_t PDF417ErrorCorrection_getErrorCorrectionCodewordCount_m1131424839 (Il2CppObject * __this /* static, unused */, int32_t ___errorCorrectionLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.PDF417ErrorCorrection::generateErrorCorrection(System.String,System.Int32)
extern "C"  String_t* PDF417ErrorCorrection_generateErrorCorrection_m4181676489 (Il2CppObject * __this /* static, unused */, String_t* ___dataCodewords0, int32_t ___errorCorrectionLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417ErrorCorrection::.cctor()
extern "C"  void PDF417ErrorCorrection__cctor_m3595945643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
