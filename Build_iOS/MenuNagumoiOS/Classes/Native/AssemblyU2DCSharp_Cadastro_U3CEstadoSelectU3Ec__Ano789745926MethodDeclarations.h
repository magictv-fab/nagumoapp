﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cadastro/<EstadoSelect>c__AnonStoreyA6
struct U3CEstadoSelectU3Ec__AnonStoreyA6_t789745926;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t185687546;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData185687546.h"

// System.Void Cadastro/<EstadoSelect>c__AnonStoreyA6::.ctor()
extern "C"  void U3CEstadoSelectU3Ec__AnonStoreyA6__ctor_m101333589 (U3CEstadoSelectU3Ec__AnonStoreyA6_t789745926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro/<EstadoSelect>c__AnonStoreyA6::<>m__83(UnityEngine.UI.Dropdown/OptionData)
extern "C"  bool U3CEstadoSelectU3Ec__AnonStoreyA6_U3CU3Em__83_m1358703147 (U3CEstadoSelectU3Ec__AnonStoreyA6_t789745926 * __this, OptionData_t185687546 * ___estado0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
