﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCEANReader
struct UPCEANReader_t3527170699;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.Result
struct Result_t2610723219;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.String
struct String_t;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.OneD.UPCEANReader::.cctor()
extern "C"  void UPCEANReader__cctor_m2881382069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANReader::.ctor()
extern "C"  void UPCEANReader__ctor_m801235960 (UPCEANReader_t3527170699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.UPCEANReader::findStartGuardPattern(ZXing.Common.BitArray)
extern "C"  Int32U5BU5D_t3230847821* UPCEANReader_findStartGuardPattern_m76587095 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.UPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * UPCEANReader_decodeRow_m1266989181 (UPCEANReader_t3527170699 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.UPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * UPCEANReader_decodeRow_m3576829448 (UPCEANReader_t3527170699 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Int32U5BU5D_t3230847821* ___startGuardRange2, Il2CppObject* ___hints3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.UPCEANReader::checkChecksum(System.String)
extern "C"  bool UPCEANReader_checkChecksum_m2055530061 (UPCEANReader_t3527170699 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.UPCEANReader::checkStandardUPCEANChecksum(System.String)
extern "C"  bool UPCEANReader_checkStandardUPCEANChecksum_m1682975206 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.UPCEANReader::decodeEnd(ZXing.Common.BitArray,System.Int32)
extern "C"  Int32U5BU5D_t3230847821* UPCEANReader_decodeEnd_m1158375673 (UPCEANReader_t3527170699 * __this, BitArray_t4163851164 * ___row0, int32_t ___endStart1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.UPCEANReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* UPCEANReader_findGuardPattern_m335836198 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___rowOffset1, bool ___whiteFirst2, Int32U5BU5D_t3230847821* ___pattern3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.UPCEANReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[],System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* UPCEANReader_findGuardPattern_m712770895 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___rowOffset1, bool ___whiteFirst2, Int32U5BU5D_t3230847821* ___pattern3, Int32U5BU5D_t3230847821* ___counters4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.UPCEANReader::decodeDigit(ZXing.Common.BitArray,System.Int32[],System.Int32,System.Int32[][],System.Int32&)
extern "C"  bool UPCEANReader_decodeDigit_m2100341410 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___counters1, int32_t ___rowOffset2, Int32U5BU5DU5BU5D_t1820556512* ___patterns3, int32_t* ___digit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
