﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Explosion
struct Explosion_t444282467;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.Explosion::.ctor()
extern "C"  void Explosion__ctor_m2742373939 (Explosion_t444282467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::Reset()
extern "C"  void Explosion_Reset_m388806880 (Explosion_t444282467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::Awake()
extern "C"  void Explosion_Awake_m2979979158 (Explosion_t444282467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnEnter()
extern "C"  void Explosion_OnEnter_m1998131530 (Explosion_t444282467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnFixedUpdate()
extern "C"  void Explosion_OnFixedUpdate_m861276623 (Explosion_t444282467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::DoExplosion()
extern "C"  void Explosion_DoExplosion_m4098056091 (Explosion_t444282467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.Explosion::ShouldApplyForce(UnityEngine.GameObject)
extern "C"  bool Explosion_ShouldApplyForce_m776029029 (Explosion_t444282467 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
