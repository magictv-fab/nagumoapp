﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateData/<ILogin>c__Iterator83
struct U3CILoginU3Ec__Iterator83_t2470511182;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateData/<ILogin>c__Iterator83::.ctor()
extern "C"  void U3CILoginU3Ec__Iterator83__ctor_m3261574157 (U3CILoginU3Ec__Iterator83_t2470511182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UpdateData/<ILogin>c__Iterator83::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoginU3Ec__Iterator83_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2806733807 (U3CILoginU3Ec__Iterator83_t2470511182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UpdateData/<ILogin>c__Iterator83::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoginU3Ec__Iterator83_System_Collections_IEnumerator_get_Current_m221846915 (U3CILoginU3Ec__Iterator83_t2470511182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UpdateData/<ILogin>c__Iterator83::MoveNext()
extern "C"  bool U3CILoginU3Ec__Iterator83_MoveNext_m3748471471 (U3CILoginU3Ec__Iterator83_t2470511182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateData/<ILogin>c__Iterator83::Dispose()
extern "C"  void U3CILoginU3Ec__Iterator83_Dispose_m3239889162 (U3CILoginU3Ec__Iterator83_t2470511182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateData/<ILogin>c__Iterator83::Reset()
extern "C"  void U3CILoginU3Ec__Iterator83_Reset_m908007098 (U3CILoginU3Ec__Iterator83_t2470511182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
