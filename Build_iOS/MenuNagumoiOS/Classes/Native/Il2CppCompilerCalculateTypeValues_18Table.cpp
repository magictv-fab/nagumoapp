﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_NetworkInterf3597375525.h"
#include "System_System_Net_NetworkInformation_UnixNetworkIn4099147213.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkIn908270265.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1851933528.h"
#include "System_System_Net_NetworkInformation_Win32NetworkI1525506662.h"
#include "System_System_Net_NetworkInformation_NetworkInterf1412148735.h"
#include "System_System_Net_NetworkInformation_OperationalSt3159343454.h"
#include "System_System_Net_NetworkInformation_PhysicalAddre2881305111.h"
#include "System_System_Net_NetworkInformation_AlignmentUnion262511654.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP3597816152.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR1777568538.h"
#include "System_System_Net_ProtocolViolationException574056316.h"
#include "System_System_Net_Security_AuthenticatedStream3871465409.h"
#include "System_System_Net_Security_AuthenticationLevel4161053470.h"
#include "System_System_Net_SecurityProtocolType2898870124.h"
#include "System_System_Net_Security_SslStream490807902.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe3681700014.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "System_System_Net_ServicePoint4193060341.h"
#include "System_System_Net_ServicePointManager165502476.h"
#include "System_System_Net_ServicePointManager_SPKey1069823253.h"
#include "System_System_Net_ServicePointManager_ChainValidat1508645531.h"
#include "System_System_Net_SocketAddress4232434619.h"
#include "System_System_Net_Sockets_AddressFamily3770679850.h"
#include "System_System_Net_Sockets_LingerOption3290254502.h"
#include "System_System_Net_Sockets_MulticastOption979356607.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "System_System_Net_Sockets_ProtocolType3327388960.h"
#include "System_System_Net_Sockets_SelectMode3812195181.h"
#include "System_System_Net_Sockets_Socket2157335841.h"
#include "System_System_Net_Sockets_Socket_SocketOperation4113109398.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult753587752.h"
#include "System_System_Net_Sockets_Socket_Worker3140563676.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall742231849.h"
#include "System_System_Net_Sockets_SocketError4204345479.h"
#include "System_System_Net_Sockets_SocketException3119490894.h"
#include "System_System_Net_Sockets_SocketFlags4205073670.h"
#include "System_System_Net_Sockets_SocketOptionLevel2476940110.h"
#include "System_System_Net_Sockets_SocketOptionName1841276065.h"
#include "System_System_Net_Sockets_SocketShutdown1229168279.h"
#include "System_System_Net_Sockets_SocketType1204660219.h"
#include "System_System_Net_WebAsyncResult3680222175.h"
#include "System_System_Net_ReadState702064469.h"
#include "System_System_Net_WebConnection1384678412.h"
#include "System_System_Net_WebConnection_AbortHelper3193016561.h"
#include "System_System_Net_WebConnectionData2478425814.h"
#include "System_System_Net_WebConnectionGroup1104492135.h"
#include "System_System_Net_WebConnectionStream1804418604.h"
#include "System_System_Net_WebException2331648373.h"
#include "System_System_Net_WebExceptionStatus2952522055.h"
#include "System_System_Net_WebHeaderCollection1958609721.h"
#include "System_System_Net_WebProxy2877839764.h"
#include "System_System_Net_WebRequest51806901.h"
#include "System_System_Net_WebResponse3238378095.h"
#include "System_System_Security_Authentication_SslProtocols1694761299.h"
#include "System_System_Security_Cryptography_AsnDecodeStatu1762590658.h"
#include "System_System_Security_Cryptography_AsnEncodedData2861953768.h"
#include "System_System_Security_Cryptography_OidCollection1976115950.h"
#include "System_System_Security_Cryptography_Oid2466812144.h"
#include "System_System_Security_Cryptography_OidEnumerator2796768276.h"
#include "System_System_Security_Cryptography_X509Certificat1258989979.h"
#include "System_Mono_Security_X509_OSX509Certificates1898181022.h"
#include "System_Mono_Security_X509_OSX509Certificates_SecTr1670280072.h"
#include "System_System_Security_Cryptography_X509Certificat1182884468.h"
#include "System_System_Security_Cryptography_X509Certificat3209511796.h"
#include "System_System_Security_Cryptography_X509Certificat1427767882.h"
#include "System_System_Security_Cryptography_X509Certificate815951664.h"
#include "System_System_Security_Cryptography_X509Certificat2073611205.h"
#include "System_System_Security_Cryptography_X509Certificate134105807.h"
#include "System_System_Security_Cryptography_X509Certificat1692475439.h"
#include "System_System_Security_Cryptography_X509Certificate160474609.h"
#include "System_System_Security_Cryptography_X509Certificat2513127765.h"
#include "System_System_Security_Cryptography_X509Certificat3220522733.h"
#include "System_System_Security_Cryptography_X509Certificat3573322844.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Security_Cryptography_X509Certificat1625801647.h"
#include "System_System_Security_Cryptography_X509Certificat1801824625.h"
#include "System_System_Security_Cryptography_X509Certificat2446453973.h"
#include "System_System_Security_Cryptography_X509Certificate676713451.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "System_System_Security_Cryptography_X509Certificat2303373610.h"
#include "System_System_Security_Cryptography_X509Certificat3535934943.h"
#include "System_System_Security_Cryptography_X509Certificat1445540821.h"
#include "System_System_Security_Cryptography_X509Certificat3645370647.h"
#include "System_System_Security_Cryptography_X509Certificat2266193147.h"
#include "System_System_Security_Cryptography_X509Certificat2945450121.h"
#include "System_System_Security_Cryptography_X509Certificat3675963317.h"
#include "System_System_Security_Cryptography_X509Certificat1237379453.h"
#include "System_System_Security_Cryptography_X509Certificat1170263131.h"
#include "System_System_Security_Cryptography_X509Certificate326021344.h"
#include "System_System_Security_Cryptography_X509Certificate326232855.h"
#include "System_System_Security_Cryptography_X509Certificat1127032377.h"
#include "System_System_Security_Cryptography_X509Certificat1223239515.h"
#include "System_System_Security_Cryptography_X509Certificat1277662109.h"
#include "System_System_Security_Cryptography_X509Certificat2783749380.h"
#include "System_System_Text_RegularExpressions_OpCode2459786422.h"
#include "System_System_Text_RegularExpressions_OpFlags3204502900.h"
#include "System_System_Text_RegularExpressions_Position2659647281.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (NetworkInterface_t3597375525), -1, sizeof(NetworkInterface_t3597375525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	NetworkInterface_t3597375525_StaticFields::get_offset_of_windowsVer51_0(),
	NetworkInterface_t3597375525_StaticFields::get_offset_of_runningOnUnix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (UnixNetworkInterface_t4099147213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[5] = 
{
	UnixNetworkInterface_t4099147213::get_offset_of_name_2(),
	UnixNetworkInterface_t4099147213::get_offset_of_index_3(),
	UnixNetworkInterface_t4099147213::get_offset_of_addresses_4(),
	UnixNetworkInterface_t4099147213::get_offset_of_macAddress_5(),
	UnixNetworkInterface_t4099147213::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (LinuxNetworkInterface_t908270265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[3] = 
{
	LinuxNetworkInterface_t908270265::get_offset_of_iface_path_7(),
	LinuxNetworkInterface_t908270265::get_offset_of_iface_operstate_path_8(),
	LinuxNetworkInterface_t908270265::get_offset_of_iface_flags_path_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (MacOsNetworkInterface_t1851933528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (Win32NetworkInterface2_t1525506662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[5] = 
{
	Win32NetworkInterface2_t1525506662::get_offset_of_addr_2(),
	Win32NetworkInterface2_t1525506662::get_offset_of_mib4_3(),
	Win32NetworkInterface2_t1525506662::get_offset_of_mib6_4(),
	Win32NetworkInterface2_t1525506662::get_offset_of_ip4stats_5(),
	Win32NetworkInterface2_t1525506662::get_offset_of_ip_if_props_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (NetworkInterfaceType_t1412148735)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[26] = 
{
	NetworkInterfaceType_t1412148735::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (OperationalStatus_t3159343454)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[8] = 
{
	OperationalStatus_t3159343454::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (PhysicalAddress_t2881305111), -1, sizeof(PhysicalAddress_t2881305111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	PhysicalAddress_t2881305111_StaticFields::get_offset_of_None_0(),
	PhysicalAddress_t2881305111::get_offset_of_bytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (AlignmentUnion_t262511654)+ sizeof (Il2CppObject), sizeof(AlignmentUnion_t262511654_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1808[3] = 
{
	AlignmentUnion_t262511654::get_offset_of_Alignment_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AlignmentUnion_t262511654::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AlignmentUnion_t262511654::get_offset_of_IfIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (Win32_IP_ADAPTER_ADDRESSES_t3597816152), sizeof(Win32_IP_ADAPTER_ADDRESSES_t3597816152_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[18] = 
{
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_Alignment_0(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_Next_1(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_AdapterName_2(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_FirstUnicastAddress_3(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_FirstAnycastAddress_4(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_FirstMulticastAddress_5(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_FirstDnsServerAddress_6(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_DnsSuffix_7(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_Description_8(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_FriendlyName_9(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_PhysicalAddress_10(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_PhysicalAddressLength_11(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_Flags_12(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_Mtu_13(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_IfType_14(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_OperStatus_15(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_Ipv6IfIndex_16(),
	Win32_IP_ADAPTER_ADDRESSES_t3597816152::get_offset_of_ZoneIndices_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Win32_MIB_IFROW_t1777568538)+ sizeof (Il2CppObject), sizeof(Win32_MIB_IFROW_t1777568538_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[24] = 
{
	Win32_MIB_IFROW_t1777568538::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_Index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_Mtu_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_Speed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_PhysAddrLen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_PhysAddr_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_AdminStatus_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OperStatus_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_LastChange_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_InOctets_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_InUcastPkts_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_InNUcastPkts_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_InDiscards_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_InErrors_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_InUnknownProtos_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OutOctets_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OutUcastPkts_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OutNUcastPkts_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OutDiscards_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OutErrors_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_OutQLen_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_DescrLen_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t1777568538::get_offset_of_Descr_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (ProtocolViolationException_t574056316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (AuthenticatedStream_t3871465409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[2] = 
{
	AuthenticatedStream_t3871465409::get_offset_of_innerStream_2(),
	AuthenticatedStream_t3871465409::get_offset_of_leaveStreamOpen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (AuthenticationLevel_t4161053470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1813[4] = 
{
	AuthenticationLevel_t4161053470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (SecurityProtocolType_t2898870124)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[3] = 
{
	SecurityProtocolType_t2898870124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (SslStream_t490807902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[3] = 
{
	SslStream_t490807902::get_offset_of_ssl_stream_4(),
	SslStream_t490807902::get_offset_of_validation_callback_5(),
	SslStream_t490807902::get_offset_of_selection_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (SslPolicyErrors_t3099591579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[5] = 
{
	SslPolicyErrors_t3099591579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (ServicePoint_t4193060341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[17] = 
{
	ServicePoint_t4193060341::get_offset_of_uri_0(),
	ServicePoint_t4193060341::get_offset_of_connectionLimit_1(),
	ServicePoint_t4193060341::get_offset_of_maxIdleTime_2(),
	ServicePoint_t4193060341::get_offset_of_currentConnections_3(),
	ServicePoint_t4193060341::get_offset_of_idleSince_4(),
	ServicePoint_t4193060341::get_offset_of_protocolVersion_5(),
	ServicePoint_t4193060341::get_offset_of_certificate_6(),
	ServicePoint_t4193060341::get_offset_of_clientCertificate_7(),
	ServicePoint_t4193060341::get_offset_of_host_8(),
	ServicePoint_t4193060341::get_offset_of_usesProxy_9(),
	ServicePoint_t4193060341::get_offset_of_groups_10(),
	ServicePoint_t4193060341::get_offset_of_sendContinue_11(),
	ServicePoint_t4193060341::get_offset_of_useConnect_12(),
	ServicePoint_t4193060341::get_offset_of_locker_13(),
	ServicePoint_t4193060341::get_offset_of_hostE_14(),
	ServicePoint_t4193060341::get_offset_of_useNagle_15(),
	ServicePoint_t4193060341::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (ServicePointManager_t165502476), -1, sizeof(ServicePointManager_t165502476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1819[11] = 
{
	ServicePointManager_t165502476_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t165502476_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t165502476_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_server_cert_cb_9(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_manager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (SPKey_t1069823253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	SPKey_t1069823253::get_offset_of_uri_0(),
	SPKey_t1069823253::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (ChainValidationHelper_t1508645531), -1, sizeof(ChainValidationHelper_t1508645531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1821[4] = 
{
	ChainValidationHelper_t1508645531::get_offset_of_sender_0(),
	ChainValidationHelper_t1508645531::get_offset_of_host_1(),
	ChainValidationHelper_t1508645531_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t1508645531_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (SocketAddress_t4232434619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[1] = 
{
	SocketAddress_t4232434619::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (AddressFamily_t3770679850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1823[32] = 
{
	AddressFamily_t3770679850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (LingerOption_t3290254502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[2] = 
{
	LingerOption_t3290254502::get_offset_of_enabled_0(),
	LingerOption_t3290254502::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (MulticastOption_t979356607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (NetworkStream_t3953762560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[6] = 
{
	NetworkStream_t3953762560::get_offset_of_access_2(),
	NetworkStream_t3953762560::get_offset_of_socket_3(),
	NetworkStream_t3953762560::get_offset_of_owns_socket_4(),
	NetworkStream_t3953762560::get_offset_of_readable_5(),
	NetworkStream_t3953762560::get_offset_of_writeable_6(),
	NetworkStream_t3953762560::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ProtocolType_t3327388960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[26] = 
{
	ProtocolType_t3327388960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (SelectMode_t3812195181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1828[4] = 
{
	SelectMode_t3812195181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Socket_t2157335841), -1, sizeof(Socket_t2157335841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1829[22] = 
{
	Socket_t2157335841::get_offset_of_readQ_0(),
	Socket_t2157335841::get_offset_of_writeQ_1(),
	Socket_t2157335841::get_offset_of_islistening_2(),
	Socket_t2157335841::get_offset_of_MinListenPort_3(),
	Socket_t2157335841::get_offset_of_MaxListenPort_4(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t2157335841::get_offset_of_linger_timeout_7(),
	Socket_t2157335841::get_offset_of_socket_8(),
	Socket_t2157335841::get_offset_of_address_family_9(),
	Socket_t2157335841::get_offset_of_socket_type_10(),
	Socket_t2157335841::get_offset_of_protocol_type_11(),
	Socket_t2157335841::get_offset_of_blocking_12(),
	Socket_t2157335841::get_offset_of_blocking_thread_13(),
	Socket_t2157335841::get_offset_of_isbound_14(),
	Socket_t2157335841_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t2157335841::get_offset_of_max_bind_count_16(),
	Socket_t2157335841::get_offset_of_connected_17(),
	Socket_t2157335841::get_offset_of_closed_18(),
	Socket_t2157335841::get_offset_of_disposed_19(),
	Socket_t2157335841::get_offset_of_seed_endpoint_20(),
	Socket_t2157335841_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (SocketOperation_t4113109398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1830[15] = 
{
	SocketOperation_t4113109398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (SocketAsyncResult_t753587752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[25] = 
{
	SocketAsyncResult_t753587752::get_offset_of_Sock_0(),
	SocketAsyncResult_t753587752::get_offset_of_handle_1(),
	SocketAsyncResult_t753587752::get_offset_of_state_2(),
	SocketAsyncResult_t753587752::get_offset_of_callback_3(),
	SocketAsyncResult_t753587752::get_offset_of_waithandle_4(),
	SocketAsyncResult_t753587752::get_offset_of_delayedException_5(),
	SocketAsyncResult_t753587752::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t753587752::get_offset_of_Buffer_7(),
	SocketAsyncResult_t753587752::get_offset_of_Offset_8(),
	SocketAsyncResult_t753587752::get_offset_of_Size_9(),
	SocketAsyncResult_t753587752::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t753587752::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t753587752::get_offset_of_Addresses_12(),
	SocketAsyncResult_t753587752::get_offset_of_Port_13(),
	SocketAsyncResult_t753587752::get_offset_of_Buffers_14(),
	SocketAsyncResult_t753587752::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t753587752::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t753587752::get_offset_of_total_17(),
	SocketAsyncResult_t753587752::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t753587752::get_offset_of_completed_19(),
	SocketAsyncResult_t753587752::get_offset_of_blocking_20(),
	SocketAsyncResult_t753587752::get_offset_of_error_21(),
	SocketAsyncResult_t753587752::get_offset_of_operation_22(),
	SocketAsyncResult_t753587752::get_offset_of_ares_23(),
	SocketAsyncResult_t753587752::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (Worker_t3140563676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[3] = 
{
	Worker_t3140563676::get_offset_of_result_0(),
	Worker_t3140563676::get_offset_of_requireSocketSecurity_1(),
	Worker_t3140563676::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (SocketAsyncCall_t742231849), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (SocketError_t4204345479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1834[48] = 
{
	SocketError_t4204345479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (SocketException_t3119490894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (SocketFlags_t4205073670)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[11] = 
{
	SocketFlags_t4205073670::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (SocketOptionLevel_t2476940110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1837[6] = 
{
	SocketOptionLevel_t2476940110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (SocketOptionName_t1841276065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1838[44] = 
{
	SocketOptionName_t1841276065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (SocketShutdown_t1229168279)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1839[4] = 
{
	SocketShutdown_t1229168279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (SocketType_t1204660219)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1840[7] = 
{
	SocketType_t1204660219::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (WebAsyncResult_t3680222175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[17] = 
{
	WebAsyncResult_t3680222175::get_offset_of_handle_0(),
	WebAsyncResult_t3680222175::get_offset_of_synch_1(),
	WebAsyncResult_t3680222175::get_offset_of_isCompleted_2(),
	WebAsyncResult_t3680222175::get_offset_of_cb_3(),
	WebAsyncResult_t3680222175::get_offset_of_state_4(),
	WebAsyncResult_t3680222175::get_offset_of_nbytes_5(),
	WebAsyncResult_t3680222175::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t3680222175::get_offset_of_callbackDone_7(),
	WebAsyncResult_t3680222175::get_offset_of_exc_8(),
	WebAsyncResult_t3680222175::get_offset_of_response_9(),
	WebAsyncResult_t3680222175::get_offset_of_writeStream_10(),
	WebAsyncResult_t3680222175::get_offset_of_buffer_11(),
	WebAsyncResult_t3680222175::get_offset_of_offset_12(),
	WebAsyncResult_t3680222175::get_offset_of_size_13(),
	WebAsyncResult_t3680222175::get_offset_of_locker_14(),
	WebAsyncResult_t3680222175::get_offset_of_EndCalled_15(),
	WebAsyncResult_t3680222175::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (ReadState_t702064469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[5] = 
{
	ReadState_t702064469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (WebConnection_t1384678412), -1, sizeof(WebConnection_t1384678412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1843[32] = 
{
	WebConnection_t1384678412::get_offset_of_sPoint_0(),
	WebConnection_t1384678412::get_offset_of_nstream_1(),
	WebConnection_t1384678412::get_offset_of_socket_2(),
	WebConnection_t1384678412::get_offset_of_socketLock_3(),
	WebConnection_t1384678412::get_offset_of_status_4(),
	WebConnection_t1384678412::get_offset_of_initConn_5(),
	WebConnection_t1384678412::get_offset_of_keepAlive_6(),
	WebConnection_t1384678412::get_offset_of_buffer_7(),
	WebConnection_t1384678412_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t1384678412::get_offset_of_abortHandler_9(),
	WebConnection_t1384678412::get_offset_of_abortHelper_10(),
	WebConnection_t1384678412::get_offset_of_readState_11(),
	WebConnection_t1384678412::get_offset_of_Data_12(),
	WebConnection_t1384678412::get_offset_of_chunkedRead_13(),
	WebConnection_t1384678412::get_offset_of_chunkStream_14(),
	WebConnection_t1384678412::get_offset_of_queue_15(),
	WebConnection_t1384678412::get_offset_of_reused_16(),
	WebConnection_t1384678412::get_offset_of_position_17(),
	WebConnection_t1384678412::get_offset_of_busy_18(),
	WebConnection_t1384678412::get_offset_of_priority_request_19(),
	WebConnection_t1384678412::get_offset_of_ntlm_credentials_20(),
	WebConnection_t1384678412::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t1384678412::get_offset_of_unsafe_sharing_22(),
	WebConnection_t1384678412::get_offset_of_ssl_23(),
	WebConnection_t1384678412::get_offset_of_certsAvailable_24(),
	WebConnection_t1384678412::get_offset_of_connect_exception_25(),
	WebConnection_t1384678412_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t1384678412_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t1384678412_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t1384678412_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t1384678412_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t1384678412_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (AbortHelper_t3193016561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	AbortHelper_t3193016561::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (WebConnectionData_t2478425814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[7] = 
{
	WebConnectionData_t2478425814::get_offset_of_request_0(),
	WebConnectionData_t2478425814::get_offset_of_StatusCode_1(),
	WebConnectionData_t2478425814::get_offset_of_StatusDescription_2(),
	WebConnectionData_t2478425814::get_offset_of_Headers_3(),
	WebConnectionData_t2478425814::get_offset_of_Version_4(),
	WebConnectionData_t2478425814::get_offset_of_stream_5(),
	WebConnectionData_t2478425814::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (WebConnectionGroup_t1104492135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[5] = 
{
	WebConnectionGroup_t1104492135::get_offset_of_sPoint_0(),
	WebConnectionGroup_t1104492135::get_offset_of_name_1(),
	WebConnectionGroup_t1104492135::get_offset_of_connections_2(),
	WebConnectionGroup_t1104492135::get_offset_of_rnd_3(),
	WebConnectionGroup_t1104492135::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (WebConnectionStream_t1804418604), -1, sizeof(WebConnectionStream_t1804418604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1847[27] = 
{
	WebConnectionStream_t1804418604_StaticFields::get_offset_of_crlf_2(),
	WebConnectionStream_t1804418604::get_offset_of_isRead_3(),
	WebConnectionStream_t1804418604::get_offset_of_cnc_4(),
	WebConnectionStream_t1804418604::get_offset_of_request_5(),
	WebConnectionStream_t1804418604::get_offset_of_readBuffer_6(),
	WebConnectionStream_t1804418604::get_offset_of_readBufferOffset_7(),
	WebConnectionStream_t1804418604::get_offset_of_readBufferSize_8(),
	WebConnectionStream_t1804418604::get_offset_of_contentLength_9(),
	WebConnectionStream_t1804418604::get_offset_of_totalRead_10(),
	WebConnectionStream_t1804418604::get_offset_of_totalWritten_11(),
	WebConnectionStream_t1804418604::get_offset_of_nextReadCalled_12(),
	WebConnectionStream_t1804418604::get_offset_of_pendingReads_13(),
	WebConnectionStream_t1804418604::get_offset_of_pendingWrites_14(),
	WebConnectionStream_t1804418604::get_offset_of_pending_15(),
	WebConnectionStream_t1804418604::get_offset_of_allowBuffering_16(),
	WebConnectionStream_t1804418604::get_offset_of_sendChunked_17(),
	WebConnectionStream_t1804418604::get_offset_of_writeBuffer_18(),
	WebConnectionStream_t1804418604::get_offset_of_requestWritten_19(),
	WebConnectionStream_t1804418604::get_offset_of_headers_20(),
	WebConnectionStream_t1804418604::get_offset_of_disposed_21(),
	WebConnectionStream_t1804418604::get_offset_of_headersSent_22(),
	WebConnectionStream_t1804418604::get_offset_of_locker_23(),
	WebConnectionStream_t1804418604::get_offset_of_initRead_24(),
	WebConnectionStream_t1804418604::get_offset_of_read_eof_25(),
	WebConnectionStream_t1804418604::get_offset_of_complete_request_written_26(),
	WebConnectionStream_t1804418604::get_offset_of_read_timeout_27(),
	WebConnectionStream_t1804418604::get_offset_of_write_timeout_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (WebException_t2331648373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[2] = 
{
	WebException_t2331648373::get_offset_of_response_12(),
	WebException_t2331648373::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (WebExceptionStatus_t2952522055)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1849[22] = 
{
	WebExceptionStatus_t2952522055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (WebHeaderCollection_t1958609721), -1, sizeof(WebHeaderCollection_t1958609721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1850[5] = 
{
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t1958609721::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (WebProxy_t2877839764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[5] = 
{
	WebProxy_t2877839764::get_offset_of_address_0(),
	WebProxy_t2877839764::get_offset_of_bypassOnLocal_1(),
	WebProxy_t2877839764::get_offset_of_bypassList_2(),
	WebProxy_t2877839764::get_offset_of_credentials_3(),
	WebProxy_t2877839764::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (WebRequest_t51806901), -1, sizeof(WebRequest_t51806901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1852[5] = 
{
	WebRequest_t51806901_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t51806901_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t51806901_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t51806901::get_offset_of_authentication_level_4(),
	WebRequest_t51806901_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (WebResponse_t3238378095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (SslProtocols_t1694761299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1854[6] = 
{
	SslProtocols_t1694761299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (AsnDecodeStatus_t1762590658)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1855[7] = 
{
	AsnDecodeStatus_t1762590658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (AsnEncodedData_t2861953768), -1, sizeof(AsnEncodedData_t2861953768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1856[3] = 
{
	AsnEncodedData_t2861953768::get_offset_of__oid_0(),
	AsnEncodedData_t2861953768::get_offset_of__raw_1(),
	AsnEncodedData_t2861953768_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (OidCollection_t1976115950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[2] = 
{
	OidCollection_t1976115950::get_offset_of__list_0(),
	OidCollection_t1976115950::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (Oid_t2466812144), -1, sizeof(Oid_t2466812144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1858[3] = 
{
	Oid_t2466812144::get_offset_of__value_0(),
	Oid_t2466812144::get_offset_of__name_1(),
	Oid_t2466812144_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (OidEnumerator_t2796768276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[2] = 
{
	OidEnumerator_t2796768276::get_offset_of__collection_0(),
	OidEnumerator_t2796768276::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (OpenFlags_t1258989979)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1860[6] = 
{
	OpenFlags_t1258989979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (OSX509Certificates_t1898181022), -1, sizeof(OSX509Certificates_t1898181022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1861[1] = 
{
	OSX509Certificates_t1898181022_StaticFields::get_offset_of_sslsecpolicy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (SecTrustResult_t1670280072)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1862[9] = 
{
	SecTrustResult_t1670280072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (PublicKey_t1182884468), -1, sizeof(PublicKey_t1182884468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1863[5] = 
{
	PublicKey_t1182884468::get_offset_of__key_0(),
	PublicKey_t1182884468::get_offset_of__keyValue_1(),
	PublicKey_t1182884468::get_offset_of__params_2(),
	PublicKey_t1182884468::get_offset_of__oid_3(),
	PublicKey_t1182884468_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (StoreLocation_t3209511796)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1864[3] = 
{
	StoreLocation_t3209511796::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (StoreName_t1427767882)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[9] = 
{
	StoreName_t1427767882::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (X500DistinguishedName_t815951664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[1] = 
{
	X500DistinguishedName_t815951664::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (X500DistinguishedNameFlags_t2073611205)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[11] = 
{
	X500DistinguishedNameFlags_t2073611205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (X509BasicConstraintsExtension_t134105807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t134105807::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t134105807::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t134105807::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t134105807::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (X509Certificate2Collection_t1692475439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (X509Certificate2_t160474609), -1, sizeof(X509Certificate2_t160474609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1870[13] = 
{
	X509Certificate2_t160474609::get_offset_of__archived_5(),
	X509Certificate2_t160474609::get_offset_of__extensions_6(),
	X509Certificate2_t160474609::get_offset_of__name_7(),
	X509Certificate2_t160474609::get_offset_of__serial_8(),
	X509Certificate2_t160474609::get_offset_of__publicKey_9(),
	X509Certificate2_t160474609::get_offset_of_issuer_name_10(),
	X509Certificate2_t160474609::get_offset_of_subject_name_11(),
	X509Certificate2_t160474609::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t160474609::get_offset_of__cert_13(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (X509Certificate2Enumerator_t2513127765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[1] = 
{
	X509Certificate2Enumerator_t2513127765::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (X509CertificateCollection_t3220522733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (X509CertificateEnumerator_t3573322844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	X509CertificateEnumerator_t3573322844::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (X509Chain_t1111884825), -1, sizeof(X509Chain_t1111884825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1874[15] = 
{
	X509Chain_t1111884825::get_offset_of_location_0(),
	X509Chain_t1111884825::get_offset_of_elements_1(),
	X509Chain_t1111884825::get_offset_of_policy_2(),
	X509Chain_t1111884825::get_offset_of_status_3(),
	X509Chain_t1111884825_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t1111884825::get_offset_of_max_path_length_5(),
	X509Chain_t1111884825::get_offset_of_working_issuer_name_6(),
	X509Chain_t1111884825::get_offset_of_working_public_key_7(),
	X509Chain_t1111884825::get_offset_of_bce_restriction_8(),
	X509Chain_t1111884825::get_offset_of_roots_9(),
	X509Chain_t1111884825::get_offset_of_cas_10(),
	X509Chain_t1111884825::get_offset_of_collection_11(),
	X509Chain_t1111884825_StaticFields::get_offset_of_U3CU3Ef__switchU24map17_12(),
	X509Chain_t1111884825_StaticFields::get_offset_of_U3CU3Ef__switchU24map18_13(),
	X509Chain_t1111884825_StaticFields::get_offset_of_U3CU3Ef__switchU24map19_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (X509ChainElementCollection_t1625801647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[1] = 
{
	X509ChainElementCollection_t1625801647::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (X509ChainElement_t1801824625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[4] = 
{
	X509ChainElement_t1801824625::get_offset_of_certificate_0(),
	X509ChainElement_t1801824625::get_offset_of_status_1(),
	X509ChainElement_t1801824625::get_offset_of_info_2(),
	X509ChainElement_t1801824625::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (X509ChainElementEnumerator_t2446453973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	X509ChainElementEnumerator_t2446453973::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (X509ChainPolicy_t676713451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[8] = 
{
	X509ChainPolicy_t676713451::get_offset_of_apps_0(),
	X509ChainPolicy_t676713451::get_offset_of_cert_1(),
	X509ChainPolicy_t676713451::get_offset_of_store_2(),
	X509ChainPolicy_t676713451::get_offset_of_rflag_3(),
	X509ChainPolicy_t676713451::get_offset_of_mode_4(),
	X509ChainPolicy_t676713451::get_offset_of_timeout_5(),
	X509ChainPolicy_t676713451::get_offset_of_vflags_6(),
	X509ChainPolicy_t676713451::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (X509ChainStatus_t766901931)+ sizeof (Il2CppObject), sizeof(X509ChainStatus_t766901931_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1879[2] = 
{
	X509ChainStatus_t766901931::get_offset_of_status_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	X509ChainStatus_t766901931::get_offset_of_info_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (X509ChainStatusFlags_t2303373610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[24] = 
{
	X509ChainStatusFlags_t2303373610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (X509EnhancedKeyUsageExtension_t3535934943), -1, sizeof(X509EnhancedKeyUsageExtension_t3535934943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1881[3] = 
{
	X509EnhancedKeyUsageExtension_t3535934943::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t3535934943::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t3535934943_StaticFields::get_offset_of_U3CU3Ef__switchU24map1A_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (X509ExtensionCollection_t1445540821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	X509ExtensionCollection_t1445540821::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (X509Extension_t3645370647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[1] = 
{
	X509Extension_t3645370647::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (X509ExtensionEnumerator_t2266193147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[1] = 
{
	X509ExtensionEnumerator_t2266193147::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (X509FindType_t2945450121)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[16] = 
{
	X509FindType_t2945450121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (X509KeyUsageExtension_t3675963317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t3675963317::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t3675963317::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (X509KeyUsageFlags_t1237379453)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1887[11] = 
{
	X509KeyUsageFlags_t1237379453::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (X509NameType_t1170263131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1888[7] = 
{
	X509NameType_t1170263131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (X509RevocationFlag_t326021344)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1889[4] = 
{
	X509RevocationFlag_t326021344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (X509RevocationMode_t326232855)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1890[4] = 
{
	X509RevocationMode_t326232855::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (X509Store_t1127032377), -1, sizeof(X509Store_t1127032377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1891[6] = 
{
	X509Store_t1127032377::get_offset_of__name_0(),
	X509Store_t1127032377::get_offset_of__location_1(),
	X509Store_t1127032377::get_offset_of_list_2(),
	X509Store_t1127032377::get_offset_of__flags_3(),
	X509Store_t1127032377::get_offset_of_store_4(),
	X509Store_t1127032377_StaticFields::get_offset_of_U3CU3Ef__switchU24map1B_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (X509SubjectKeyIdentifierExtension_t1223239515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t1223239515::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_t1223239515::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_t1223239515::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t1277662109)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1893[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t1277662109::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (X509VerificationFlags_t2783749380)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1894[15] = 
{
	X509VerificationFlags_t2783749380::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (OpCode_t2459786422)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[26] = 
{
	OpCode_t2459786422::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (OpFlags_t3204502900)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[6] = 
{
	OpFlags_t3204502900::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (Position_t2659647281)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1897[11] = 
{
	Position_t2659647281::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
