﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.components.GroupInAppAbstractComponentsManager
struct GroupInAppAbstractComponentsManager_t739334794;
// MagicTV.abstracts.InAppAbstract[]
struct InAppAbstractU5BU5D_t412350073;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"

// System.Void ARM.components.GroupInAppAbstractComponentsManager::.ctor()
extern "C"  void GroupInAppAbstractComponentsManager__ctor_m129509161 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::SetComponents(MagicTV.abstracts.InAppAbstract[])
extern "C"  void GroupInAppAbstractComponentsManager_SetComponents_m2799442735 (GroupInAppAbstractComponentsManager_t739334794 * __this, InAppAbstractU5BU5D_t412350073* ___components0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::checkAllIsReady()
extern "C"  void GroupInAppAbstractComponentsManager_checkAllIsReady_m2302380487 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::prepare()
extern "C"  void GroupInAppAbstractComponentsManager_prepare_m831232558 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::doDispose()
extern "C"  void GroupInAppAbstractComponentsManager_doDispose_m3874810747 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::removeGameObject()
extern "C"  void GroupInAppAbstractComponentsManager_removeGameObject_m2813373968 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::Dispose()
extern "C"  void GroupInAppAbstractComponentsManager_Dispose_m4097502502 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::DoRun()
extern "C"  void GroupInAppAbstractComponentsManager_DoRun_m2787045959 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::DoStop()
extern "C"  void GroupInAppAbstractComponentsManager_DoStop_m526883432 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupInAppAbstractComponentsManager::SetBundleVO(MagicTV.vo.BundleVO)
extern "C"  void GroupInAppAbstractComponentsManager_SetBundleVO_m1905543421 (GroupInAppAbstractComponentsManager_t739334794 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.components.GroupInAppAbstractComponentsManager::GetInAppName()
extern "C"  String_t* GroupInAppAbstractComponentsManager_GetInAppName_m3426216143 (GroupInAppAbstractComponentsManager_t739334794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
