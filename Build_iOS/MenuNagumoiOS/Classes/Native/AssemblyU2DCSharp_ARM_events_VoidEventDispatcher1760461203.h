﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.events.VoidEventDispatcher/VoidEvent
struct VoidEvent_t1756309665;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.VoidEventDispatcher
struct  VoidEventDispatcher_t1760461203  : public Il2CppObject
{
public:
	// ARM.events.VoidEventDispatcher/VoidEvent ARM.events.VoidEventDispatcher::_voidEvent
	VoidEvent_t1756309665 * ____voidEvent_0;

public:
	inline static int32_t get_offset_of__voidEvent_0() { return static_cast<int32_t>(offsetof(VoidEventDispatcher_t1760461203, ____voidEvent_0)); }
	inline VoidEvent_t1756309665 * get__voidEvent_0() const { return ____voidEvent_0; }
	inline VoidEvent_t1756309665 ** get_address_of__voidEvent_0() { return &____voidEvent_0; }
	inline void set__voidEvent_0(VoidEvent_t1756309665 * value)
	{
		____voidEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&____voidEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
