﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3689713007.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkReachability>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m927720851_gshared (InternalEnumerator_1_t3689713007 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m927720851(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3689713007 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m927720851_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkReachability>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m701778221_gshared (InternalEnumerator_1_t3689713007 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m701778221(__this, method) ((  void (*) (InternalEnumerator_1_t3689713007 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m701778221_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.NetworkReachability>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791603171_gshared (InternalEnumerator_1_t3689713007 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791603171(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3689713007 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791603171_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkReachability>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2609691434_gshared (InternalEnumerator_1_t3689713007 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2609691434(__this, method) ((  void (*) (InternalEnumerator_1_t3689713007 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2609691434_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.NetworkReachability>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2757338973_gshared (InternalEnumerator_1_t3689713007 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2757338973(__this, method) ((  bool (*) (InternalEnumerator_1_t3689713007 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2757338973_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.NetworkReachability>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m53465212_gshared (InternalEnumerator_1_t3689713007 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m53465212(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3689713007 *, const MethodInfo*))InternalEnumerator_1_get_Current_m53465212_gshared)(__this, method)
