﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ZXing.Dimension
struct Dimension_t1395692488;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ZXing.Datamatrix.Encoder.SymbolInfo
struct SymbolInfo_t553159586;
// System.Text.Encoding
struct Encoding_t2012439129;

#include "mscorlib_System_Object4170816371.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolShapeHint2380532246.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Encoder.EncoderContext
struct  EncoderContext_t1774722223  : public Il2CppObject
{
public:
	// System.String ZXing.Datamatrix.Encoder.EncoderContext::msg
	String_t* ___msg_0;
	// ZXing.Datamatrix.Encoder.SymbolShapeHint ZXing.Datamatrix.Encoder.EncoderContext::shape
	int32_t ___shape_1;
	// ZXing.Dimension ZXing.Datamatrix.Encoder.EncoderContext::minSize
	Dimension_t1395692488 * ___minSize_2;
	// ZXing.Dimension ZXing.Datamatrix.Encoder.EncoderContext::maxSize
	Dimension_t1395692488 * ___maxSize_3;
	// System.Text.StringBuilder ZXing.Datamatrix.Encoder.EncoderContext::codewords
	StringBuilder_t243639308 * ___codewords_4;
	// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::pos
	int32_t ___pos_5;
	// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::newEncoding
	int32_t ___newEncoding_6;
	// ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.EncoderContext::symbolInfo
	SymbolInfo_t553159586 * ___symbolInfo_7;
	// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::skipAtEnd
	int32_t ___skipAtEnd_8;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier(&___msg_0, value);
	}

	inline static int32_t get_offset_of_shape_1() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___shape_1)); }
	inline int32_t get_shape_1() const { return ___shape_1; }
	inline int32_t* get_address_of_shape_1() { return &___shape_1; }
	inline void set_shape_1(int32_t value)
	{
		___shape_1 = value;
	}

	inline static int32_t get_offset_of_minSize_2() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___minSize_2)); }
	inline Dimension_t1395692488 * get_minSize_2() const { return ___minSize_2; }
	inline Dimension_t1395692488 ** get_address_of_minSize_2() { return &___minSize_2; }
	inline void set_minSize_2(Dimension_t1395692488 * value)
	{
		___minSize_2 = value;
		Il2CppCodeGenWriteBarrier(&___minSize_2, value);
	}

	inline static int32_t get_offset_of_maxSize_3() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___maxSize_3)); }
	inline Dimension_t1395692488 * get_maxSize_3() const { return ___maxSize_3; }
	inline Dimension_t1395692488 ** get_address_of_maxSize_3() { return &___maxSize_3; }
	inline void set_maxSize_3(Dimension_t1395692488 * value)
	{
		___maxSize_3 = value;
		Il2CppCodeGenWriteBarrier(&___maxSize_3, value);
	}

	inline static int32_t get_offset_of_codewords_4() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___codewords_4)); }
	inline StringBuilder_t243639308 * get_codewords_4() const { return ___codewords_4; }
	inline StringBuilder_t243639308 ** get_address_of_codewords_4() { return &___codewords_4; }
	inline void set_codewords_4(StringBuilder_t243639308 * value)
	{
		___codewords_4 = value;
		Il2CppCodeGenWriteBarrier(&___codewords_4, value);
	}

	inline static int32_t get_offset_of_pos_5() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___pos_5)); }
	inline int32_t get_pos_5() const { return ___pos_5; }
	inline int32_t* get_address_of_pos_5() { return &___pos_5; }
	inline void set_pos_5(int32_t value)
	{
		___pos_5 = value;
	}

	inline static int32_t get_offset_of_newEncoding_6() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___newEncoding_6)); }
	inline int32_t get_newEncoding_6() const { return ___newEncoding_6; }
	inline int32_t* get_address_of_newEncoding_6() { return &___newEncoding_6; }
	inline void set_newEncoding_6(int32_t value)
	{
		___newEncoding_6 = value;
	}

	inline static int32_t get_offset_of_symbolInfo_7() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___symbolInfo_7)); }
	inline SymbolInfo_t553159586 * get_symbolInfo_7() const { return ___symbolInfo_7; }
	inline SymbolInfo_t553159586 ** get_address_of_symbolInfo_7() { return &___symbolInfo_7; }
	inline void set_symbolInfo_7(SymbolInfo_t553159586 * value)
	{
		___symbolInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___symbolInfo_7, value);
	}

	inline static int32_t get_offset_of_skipAtEnd_8() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223, ___skipAtEnd_8)); }
	inline int32_t get_skipAtEnd_8() const { return ___skipAtEnd_8; }
	inline int32_t* get_address_of_skipAtEnd_8() { return &___skipAtEnd_8; }
	inline void set_skipAtEnd_8(int32_t value)
	{
		___skipAtEnd_8 = value;
	}
};

struct EncoderContext_t1774722223_StaticFields
{
public:
	// System.Text.Encoding ZXing.Datamatrix.Encoder.EncoderContext::encoding
	Encoding_t2012439129 * ___encoding_9;

public:
	inline static int32_t get_offset_of_encoding_9() { return static_cast<int32_t>(offsetof(EncoderContext_t1774722223_StaticFields, ___encoding_9)); }
	inline Encoding_t2012439129 * get_encoding_9() const { return ___encoding_9; }
	inline Encoding_t2012439129 ** get_address_of_encoding_9() { return &___encoding_9; }
	inline void set_encoding_9(Encoding_t2012439129 * value)
	{
		___encoding_9 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
