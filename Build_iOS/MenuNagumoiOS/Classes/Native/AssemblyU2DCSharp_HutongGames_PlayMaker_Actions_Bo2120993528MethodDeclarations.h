﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolChanged
struct BoolChanged_t2120993528;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolChanged::.ctor()
extern "C"  void BoolChanged__ctor_m3951294270 (BoolChanged_t2120993528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolChanged::Reset()
extern "C"  void BoolChanged_Reset_m1597727211 (BoolChanged_t2120993528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolChanged::OnEnter()
extern "C"  void BoolChanged_OnEnter_m4129399701 (BoolChanged_t2120993528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolChanged::OnUpdate()
extern "C"  void BoolChanged_OnUpdate_m2590898510 (BoolChanged_t2120993528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
