﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManager/<GrabScreenshot>c__Iterator30
struct U3CGrabScreenshotU3Ec__Iterator30_t651971222;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenshotManager/<GrabScreenshot>c__Iterator30::.ctor()
extern "C"  void U3CGrabScreenshotU3Ec__Iterator30__ctor_m395404693 (U3CGrabScreenshotU3Ec__Iterator30_t651971222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManager/<GrabScreenshot>c__Iterator30::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGrabScreenshotU3Ec__Iterator30_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m506604253 (U3CGrabScreenshotU3Ec__Iterator30_t651971222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManager/<GrabScreenshot>c__Iterator30::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGrabScreenshotU3Ec__Iterator30_System_Collections_IEnumerator_get_Current_m206014577 (U3CGrabScreenshotU3Ec__Iterator30_t651971222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenshotManager/<GrabScreenshot>c__Iterator30::MoveNext()
extern "C"  bool U3CGrabScreenshotU3Ec__Iterator30_MoveNext_m2706979903 (U3CGrabScreenshotU3Ec__Iterator30_t651971222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager/<GrabScreenshot>c__Iterator30::Dispose()
extern "C"  void U3CGrabScreenshotU3Ec__Iterator30_Dispose_m1925070994 (U3CGrabScreenshotU3Ec__Iterator30_t651971222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager/<GrabScreenshot>c__Iterator30::Reset()
extern "C"  void U3CGrabScreenshotU3Ec__Iterator30_Reset_m2336804930 (U3CGrabScreenshotU3Ec__Iterator30_t651971222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
