﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.AztecCode
struct  AztecCode_t1281541704  : public Il2CppObject
{
public:
	// System.Boolean ZXing.Aztec.Internal.AztecCode::<isCompact>k__BackingField
	bool ___U3CisCompactU3Ek__BackingField_0;
	// System.Int32 ZXing.Aztec.Internal.AztecCode::<Size>k__BackingField
	int32_t ___U3CSizeU3Ek__BackingField_1;
	// System.Int32 ZXing.Aztec.Internal.AztecCode::<Layers>k__BackingField
	int32_t ___U3CLayersU3Ek__BackingField_2;
	// System.Int32 ZXing.Aztec.Internal.AztecCode::<CodeWords>k__BackingField
	int32_t ___U3CCodeWordsU3Ek__BackingField_3;
	// ZXing.Common.BitMatrix ZXing.Aztec.Internal.AztecCode::<Matrix>k__BackingField
	BitMatrix_t1058711404 * ___U3CMatrixU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisCompactU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AztecCode_t1281541704, ___U3CisCompactU3Ek__BackingField_0)); }
	inline bool get_U3CisCompactU3Ek__BackingField_0() const { return ___U3CisCompactU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCompactU3Ek__BackingField_0() { return &___U3CisCompactU3Ek__BackingField_0; }
	inline void set_U3CisCompactU3Ek__BackingField_0(bool value)
	{
		___U3CisCompactU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AztecCode_t1281541704, ___U3CSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CSizeU3Ek__BackingField_1() const { return ___U3CSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CSizeU3Ek__BackingField_1() { return &___U3CSizeU3Ek__BackingField_1; }
	inline void set_U3CSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CSizeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLayersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AztecCode_t1281541704, ___U3CLayersU3Ek__BackingField_2)); }
	inline int32_t get_U3CLayersU3Ek__BackingField_2() const { return ___U3CLayersU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLayersU3Ek__BackingField_2() { return &___U3CLayersU3Ek__BackingField_2; }
	inline void set_U3CLayersU3Ek__BackingField_2(int32_t value)
	{
		___U3CLayersU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCodeWordsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AztecCode_t1281541704, ___U3CCodeWordsU3Ek__BackingField_3)); }
	inline int32_t get_U3CCodeWordsU3Ek__BackingField_3() const { return ___U3CCodeWordsU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCodeWordsU3Ek__BackingField_3() { return &___U3CCodeWordsU3Ek__BackingField_3; }
	inline void set_U3CCodeWordsU3Ek__BackingField_3(int32_t value)
	{
		___U3CCodeWordsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AztecCode_t1281541704, ___U3CMatrixU3Ek__BackingField_4)); }
	inline BitMatrix_t1058711404 * get_U3CMatrixU3Ek__BackingField_4() const { return ___U3CMatrixU3Ek__BackingField_4; }
	inline BitMatrix_t1058711404 ** get_address_of_U3CMatrixU3Ek__BackingField_4() { return &___U3CMatrixU3Ek__BackingField_4; }
	inline void set_U3CMatrixU3Ek__BackingField_4(BitMatrix_t1058711404 * value)
	{
		___U3CMatrixU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMatrixU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
