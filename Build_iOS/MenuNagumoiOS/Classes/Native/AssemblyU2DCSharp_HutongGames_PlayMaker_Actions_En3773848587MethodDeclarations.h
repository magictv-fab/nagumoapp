﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableFSM
struct EnableFSM_t3773848587;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnableFSM::.ctor()
extern "C"  void EnableFSM__ctor_m1043072395 (EnableFSM_t3773848587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::Reset()
extern "C"  void EnableFSM_Reset_m2984472632 (EnableFSM_t3773848587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::OnEnter()
extern "C"  void EnableFSM_OnEnter_m1056920226 (EnableFSM_t3773848587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::DoEnableFSM()
extern "C"  void EnableFSM_DoEnableFSM_m4022046875 (EnableFSM_t3773848587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::OnExit()
extern "C"  void EnableFSM_OnExit_m3783791798 (EnableFSM_t3773848587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
