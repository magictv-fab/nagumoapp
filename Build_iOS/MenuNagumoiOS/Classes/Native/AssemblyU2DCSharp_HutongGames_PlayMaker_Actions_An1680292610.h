﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget
struct  AnimatorInterruptMatchTarget_t1680292610  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::completeMatch
	FsmBool_t1075959796 * ___completeMatch_10;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimatorInterruptMatchTarget_t1680292610, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_completeMatch_10() { return static_cast<int32_t>(offsetof(AnimatorInterruptMatchTarget_t1680292610, ___completeMatch_10)); }
	inline FsmBool_t1075959796 * get_completeMatch_10() const { return ___completeMatch_10; }
	inline FsmBool_t1075959796 ** get_address_of_completeMatch_10() { return &___completeMatch_10; }
	inline void set_completeMatch_10(FsmBool_t1075959796 * value)
	{
		___completeMatch_10 = value;
		Il2CppCodeGenWriteBarrier(&___completeMatch_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
