﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.AREvents/OnARToggleOnOff
struct OnARToggleOnOff_t2139692132;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.AREvents/OnARToggleOnOff::.ctor(System.Object,System.IntPtr)
extern "C"  void OnARToggleOnOff__ctor_m3854380811 (OnARToggleOnOff_t2139692132 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents/OnARToggleOnOff::Invoke(System.Boolean)
extern "C"  void OnARToggleOnOff_Invoke_m1533076444 (OnARToggleOnOff_t2139692132 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.AREvents/OnARToggleOnOff::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnARToggleOnOff_BeginInvoke_m2125969673 (OnARToggleOnOff_t2139692132 * __this, bool ___b0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents/OnARToggleOnOff::EndInvoke(System.IAsyncResult)
extern "C"  void OnARToggleOnOff_EndInvoke_m3397377691 (OnARToggleOnOff_t2139692132 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
