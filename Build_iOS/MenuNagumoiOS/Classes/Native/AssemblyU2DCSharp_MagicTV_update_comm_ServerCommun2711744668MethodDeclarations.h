﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D
struct U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D::.ctor()
extern "C"  void U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D__ctor_m2426393167 (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m356263139 (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_System_Collections_IEnumerator_get_Current_m2980204151 (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D::MoveNext()
extern "C"  bool U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_MoveNext_m3972818757 (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D::Dispose()
extern "C"  void U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_Dispose_m3789842124 (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestDeviceRegister>c__Iterator3D::Reset()
extern "C"  void U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_Reset_m72826108 (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
