﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CRM_ScrollViewsManager
struct CRM_ScrollViewsManager_t2923429133;

#include "codegen/il2cpp-codegen.h"

// System.Void CRM_ScrollViewsManager::.ctor()
extern "C"  void CRM_ScrollViewsManager__ctor_m2216924718 (CRM_ScrollViewsManager_t2923429133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CRM_ScrollViewsManager::Start()
extern "C"  void CRM_ScrollViewsManager_Start_m1164062510 (CRM_ScrollViewsManager_t2923429133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CRM_ScrollViewsManager::removeMeFromScrollSnap(System.Int32)
extern "C"  void CRM_ScrollViewsManager_removeMeFromScrollSnap_m3078209380 (CRM_ScrollViewsManager_t2923429133 * __this, int32_t ___myIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CRM_ScrollViewsManager::downloadDone()
extern "C"  void CRM_ScrollViewsManager_downloadDone_m1361519072 (CRM_ScrollViewsManager_t2923429133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
