﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern "C"  void StreamManipulator__ctor_m392625246 (StreamManipulator_t2348681196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern "C"  int32_t StreamManipulator_PeekBits_m3968378858 (StreamManipulator_t2348681196 * __this, int32_t ___bitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern "C"  void StreamManipulator_DropBits_m353078988 (StreamManipulator_t2348681196 * __this, int32_t ___bitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::GetBits(System.Int32)
extern "C"  int32_t StreamManipulator_GetBits_m3375027863 (StreamManipulator_t2348681196 * __this, int32_t ___bitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern "C"  int32_t StreamManipulator_get_AvailableBits_m3311269200 (StreamManipulator_t2348681196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern "C"  int32_t StreamManipulator_get_AvailableBytes_m27851523 (StreamManipulator_t2348681196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern "C"  void StreamManipulator_SkipToByteBoundary_m1377096754 (StreamManipulator_t2348681196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern "C"  bool StreamManipulator_get_IsNeedingInput_m1869955299 (StreamManipulator_t2348681196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t StreamManipulator_CopyBytes_m1563193833 (StreamManipulator_t2348681196 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern "C"  void StreamManipulator_Reset_m2334025483 (StreamManipulator_t2348681196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void StreamManipulator_SetInput_m46349051 (StreamManipulator_t2348681196 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
