﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerRequestUpdateVO
struct ServerRequestUpdateVO_t3510446940;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.request.ServerRequestUpdateVO::.ctor()
extern "C"  void ServerRequestUpdateVO__ctor_m1448520962 (ServerRequestUpdateVO_t3510446940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.vo.request.ServerRequestUpdateVO::.cctor()
extern "C"  void ServerRequestUpdateVO__cctor_m1472380651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MagicTV.vo.request.ServerRequestUpdateVO::getId()
extern "C"  int32_t ServerRequestUpdateVO_getId_m1923500227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
