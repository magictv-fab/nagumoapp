﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3703556008(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4071902701 *, String_t*, List_1_t3352703625 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::get_Key()
#define KeyValuePair_2_get_Key_m3366635328(__this, method) ((  String_t* (*) (KeyValuePair_2_t4071902701 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3547031937(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4071902701 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::get_Value()
#define KeyValuePair_2_get_Value_m1031153664(__this, method) ((  List_1_t3352703625 * (*) (KeyValuePair_2_t4071902701 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3909358849(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4071902701 *, List_1_t3352703625 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::ToString()
#define KeyValuePair_2_ToString_m979712577(__this, method) ((  String_t* (*) (KeyValuePair_2_t4071902701 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
