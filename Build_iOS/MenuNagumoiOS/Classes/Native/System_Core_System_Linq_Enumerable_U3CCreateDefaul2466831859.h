﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>
struct  U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859  : public Il2CppObject
{
public:
	// System.Boolean System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::<empty>__0
	bool ___U3CemptyU3E__0_0;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::source
	Il2CppObject* ___source_1;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::<$s_54>__1
	Il2CppObject* ___U3CU24s_54U3E__1_2;
	// TSource System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::<item>__2
	Il2CppObject * ___U3CitemU3E__2_3;
	// TSource System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::defaultValue
	Il2CppObject * ___defaultValue_4;
	// System.Int32 System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::$PC
	int32_t ___U24PC_5;
	// TSource System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::$current
	Il2CppObject * ___U24current_6;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::<$>source
	Il2CppObject* ___U3CU24U3Esource_7;
	// TSource System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1::<$>defaultValue
	Il2CppObject * ___U3CU24U3EdefaultValue_8;

public:
	inline static int32_t get_offset_of_U3CemptyU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U3CemptyU3E__0_0)); }
	inline bool get_U3CemptyU3E__0_0() const { return ___U3CemptyU3E__0_0; }
	inline bool* get_address_of_U3CemptyU3E__0_0() { return &___U3CemptyU3E__0_0; }
	inline void set_U3CemptyU3E__0_0(bool value)
	{
		___U3CemptyU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_54U3E__1_2() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U3CU24s_54U3E__1_2)); }
	inline Il2CppObject* get_U3CU24s_54U3E__1_2() const { return ___U3CU24s_54U3E__1_2; }
	inline Il2CppObject** get_address_of_U3CU24s_54U3E__1_2() { return &___U3CU24s_54U3E__1_2; }
	inline void set_U3CU24s_54U3E__1_2(Il2CppObject* value)
	{
		___U3CU24s_54U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_54U3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__2_3() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U3CitemU3E__2_3)); }
	inline Il2CppObject * get_U3CitemU3E__2_3() const { return ___U3CitemU3E__2_3; }
	inline Il2CppObject ** get_address_of_U3CitemU3E__2_3() { return &___U3CitemU3E__2_3; }
	inline void set_U3CitemU3E__2_3(Il2CppObject * value)
	{
		___U3CitemU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__2_3, value);
	}

	inline static int32_t get_offset_of_defaultValue_4() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___defaultValue_4)); }
	inline Il2CppObject * get_defaultValue_4() const { return ___defaultValue_4; }
	inline Il2CppObject ** get_address_of_defaultValue_4() { return &___defaultValue_4; }
	inline void set_defaultValue_4(Il2CppObject * value)
	{
		___defaultValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultValue_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_7() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U3CU24U3Esource_7)); }
	inline Il2CppObject* get_U3CU24U3Esource_7() const { return ___U3CU24U3Esource_7; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_7() { return &___U3CU24U3Esource_7; }
	inline void set_U3CU24U3Esource_7(Il2CppObject* value)
	{
		___U3CU24U3Esource_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EdefaultValue_8() { return static_cast<int32_t>(offsetof(U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859, ___U3CU24U3EdefaultValue_8)); }
	inline Il2CppObject * get_U3CU24U3EdefaultValue_8() const { return ___U3CU24U3EdefaultValue_8; }
	inline Il2CppObject ** get_address_of_U3CU24U3EdefaultValue_8() { return &___U3CU24U3EdefaultValue_8; }
	inline void set_U3CU24U3EdefaultValue_8(Il2CppObject * value)
	{
		___U3CU24U3EdefaultValue_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EdefaultValue_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
