﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerGetNextHostData
struct  MasterServerGetNextHostData_t2113983876  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::loopEvent
	FsmEvent_t2133468028 * ___loopEvent_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::finishedEvent
	FsmEvent_t2133468028 * ___finishedEvent_10;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::index
	FsmInt_t1596138449 * ___index_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::useNat
	FsmBool_t1075959796 * ___useNat_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::gameType
	FsmString_t952858651 * ___gameType_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::gameName
	FsmString_t952858651 * ___gameName_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::connectedPlayers
	FsmInt_t1596138449 * ___connectedPlayers_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::playerLimit
	FsmInt_t1596138449 * ___playerLimit_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::ipAddress
	FsmString_t952858651 * ___ipAddress_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::port
	FsmInt_t1596138449 * ___port_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::passwordProtected
	FsmBool_t1075959796 * ___passwordProtected_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::comment
	FsmString_t952858651 * ___comment_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::guid
	FsmString_t952858651 * ___guid_21;
	// System.Int32 HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::nextItemIndex
	int32_t ___nextItemIndex_22;
	// System.Boolean HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::noMoreItems
	bool ___noMoreItems_23;

public:
	inline static int32_t get_offset_of_loopEvent_9() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___loopEvent_9)); }
	inline FsmEvent_t2133468028 * get_loopEvent_9() const { return ___loopEvent_9; }
	inline FsmEvent_t2133468028 ** get_address_of_loopEvent_9() { return &___loopEvent_9; }
	inline void set_loopEvent_9(FsmEvent_t2133468028 * value)
	{
		___loopEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_9, value);
	}

	inline static int32_t get_offset_of_finishedEvent_10() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___finishedEvent_10)); }
	inline FsmEvent_t2133468028 * get_finishedEvent_10() const { return ___finishedEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_finishedEvent_10() { return &___finishedEvent_10; }
	inline void set_finishedEvent_10(FsmEvent_t2133468028 * value)
	{
		___finishedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_10, value);
	}

	inline static int32_t get_offset_of_index_11() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___index_11)); }
	inline FsmInt_t1596138449 * get_index_11() const { return ___index_11; }
	inline FsmInt_t1596138449 ** get_address_of_index_11() { return &___index_11; }
	inline void set_index_11(FsmInt_t1596138449 * value)
	{
		___index_11 = value;
		Il2CppCodeGenWriteBarrier(&___index_11, value);
	}

	inline static int32_t get_offset_of_useNat_12() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___useNat_12)); }
	inline FsmBool_t1075959796 * get_useNat_12() const { return ___useNat_12; }
	inline FsmBool_t1075959796 ** get_address_of_useNat_12() { return &___useNat_12; }
	inline void set_useNat_12(FsmBool_t1075959796 * value)
	{
		___useNat_12 = value;
		Il2CppCodeGenWriteBarrier(&___useNat_12, value);
	}

	inline static int32_t get_offset_of_gameType_13() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___gameType_13)); }
	inline FsmString_t952858651 * get_gameType_13() const { return ___gameType_13; }
	inline FsmString_t952858651 ** get_address_of_gameType_13() { return &___gameType_13; }
	inline void set_gameType_13(FsmString_t952858651 * value)
	{
		___gameType_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameType_13, value);
	}

	inline static int32_t get_offset_of_gameName_14() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___gameName_14)); }
	inline FsmString_t952858651 * get_gameName_14() const { return ___gameName_14; }
	inline FsmString_t952858651 ** get_address_of_gameName_14() { return &___gameName_14; }
	inline void set_gameName_14(FsmString_t952858651 * value)
	{
		___gameName_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_14, value);
	}

	inline static int32_t get_offset_of_connectedPlayers_15() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___connectedPlayers_15)); }
	inline FsmInt_t1596138449 * get_connectedPlayers_15() const { return ___connectedPlayers_15; }
	inline FsmInt_t1596138449 ** get_address_of_connectedPlayers_15() { return &___connectedPlayers_15; }
	inline void set_connectedPlayers_15(FsmInt_t1596138449 * value)
	{
		___connectedPlayers_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedPlayers_15, value);
	}

	inline static int32_t get_offset_of_playerLimit_16() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___playerLimit_16)); }
	inline FsmInt_t1596138449 * get_playerLimit_16() const { return ___playerLimit_16; }
	inline FsmInt_t1596138449 ** get_address_of_playerLimit_16() { return &___playerLimit_16; }
	inline void set_playerLimit_16(FsmInt_t1596138449 * value)
	{
		___playerLimit_16 = value;
		Il2CppCodeGenWriteBarrier(&___playerLimit_16, value);
	}

	inline static int32_t get_offset_of_ipAddress_17() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___ipAddress_17)); }
	inline FsmString_t952858651 * get_ipAddress_17() const { return ___ipAddress_17; }
	inline FsmString_t952858651 ** get_address_of_ipAddress_17() { return &___ipAddress_17; }
	inline void set_ipAddress_17(FsmString_t952858651 * value)
	{
		___ipAddress_17 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_17, value);
	}

	inline static int32_t get_offset_of_port_18() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___port_18)); }
	inline FsmInt_t1596138449 * get_port_18() const { return ___port_18; }
	inline FsmInt_t1596138449 ** get_address_of_port_18() { return &___port_18; }
	inline void set_port_18(FsmInt_t1596138449 * value)
	{
		___port_18 = value;
		Il2CppCodeGenWriteBarrier(&___port_18, value);
	}

	inline static int32_t get_offset_of_passwordProtected_19() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___passwordProtected_19)); }
	inline FsmBool_t1075959796 * get_passwordProtected_19() const { return ___passwordProtected_19; }
	inline FsmBool_t1075959796 ** get_address_of_passwordProtected_19() { return &___passwordProtected_19; }
	inline void set_passwordProtected_19(FsmBool_t1075959796 * value)
	{
		___passwordProtected_19 = value;
		Il2CppCodeGenWriteBarrier(&___passwordProtected_19, value);
	}

	inline static int32_t get_offset_of_comment_20() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___comment_20)); }
	inline FsmString_t952858651 * get_comment_20() const { return ___comment_20; }
	inline FsmString_t952858651 ** get_address_of_comment_20() { return &___comment_20; }
	inline void set_comment_20(FsmString_t952858651 * value)
	{
		___comment_20 = value;
		Il2CppCodeGenWriteBarrier(&___comment_20, value);
	}

	inline static int32_t get_offset_of_guid_21() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___guid_21)); }
	inline FsmString_t952858651 * get_guid_21() const { return ___guid_21; }
	inline FsmString_t952858651 ** get_address_of_guid_21() { return &___guid_21; }
	inline void set_guid_21(FsmString_t952858651 * value)
	{
		___guid_21 = value;
		Il2CppCodeGenWriteBarrier(&___guid_21, value);
	}

	inline static int32_t get_offset_of_nextItemIndex_22() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___nextItemIndex_22)); }
	inline int32_t get_nextItemIndex_22() const { return ___nextItemIndex_22; }
	inline int32_t* get_address_of_nextItemIndex_22() { return &___nextItemIndex_22; }
	inline void set_nextItemIndex_22(int32_t value)
	{
		___nextItemIndex_22 = value;
	}

	inline static int32_t get_offset_of_noMoreItems_23() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___noMoreItems_23)); }
	inline bool get_noMoreItems_23() const { return ___noMoreItems_23; }
	inline bool* get_address_of_noMoreItems_23() { return &___noMoreItems_23; }
	inline void set_noMoreItems_23(bool value)
	{
		___noMoreItems_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
