﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase
struct PkzipClassicCryptoBase_t125954580;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::.ctor()
extern "C"  void PkzipClassicCryptoBase__ctor_m429331443 (PkzipClassicCryptoBase_t125954580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::TransformByte()
extern "C"  uint8_t PkzipClassicCryptoBase_TransformByte_m2045894321 (PkzipClassicCryptoBase_t125954580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::SetKeys(System.Byte[])
extern "C"  void PkzipClassicCryptoBase_SetKeys_m2249135650 (PkzipClassicCryptoBase_t125954580 * __this, ByteU5BU5D_t4260760469* ___keyData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::UpdateKeys(System.Byte)
extern "C"  void PkzipClassicCryptoBase_UpdateKeys_m3760909469 (PkzipClassicCryptoBase_t125954580 * __this, uint8_t ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::Reset()
extern "C"  void PkzipClassicCryptoBase_Reset_m2370731680 (PkzipClassicCryptoBase_t125954580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
