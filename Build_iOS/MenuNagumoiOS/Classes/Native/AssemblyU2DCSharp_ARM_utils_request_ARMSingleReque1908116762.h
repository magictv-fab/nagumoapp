﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// ARM.utils.request.ARMSingleRequest
struct ARMSingleRequest_t410726329;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C
struct  U3CdoLoadU3Ec__Iterator3C_t1908116762  : public Il2CppObject
{
public:
	// UnityEngine.WWW ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::<$s_233>__0
	WWW_t3134621005 * ___U3CU24s_233U3E__0_0;
	// System.Int32 ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::$PC
	int32_t ___U24PC_1;
	// System.Object ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::$current
	Il2CppObject * ___U24current_2;
	// ARM.utils.request.ARMSingleRequest ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::<>f__this
	ARMSingleRequest_t410726329 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CU24s_233U3E__0_0() { return static_cast<int32_t>(offsetof(U3CdoLoadU3Ec__Iterator3C_t1908116762, ___U3CU24s_233U3E__0_0)); }
	inline WWW_t3134621005 * get_U3CU24s_233U3E__0_0() const { return ___U3CU24s_233U3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CU24s_233U3E__0_0() { return &___U3CU24s_233U3E__0_0; }
	inline void set_U3CU24s_233U3E__0_0(WWW_t3134621005 * value)
	{
		___U3CU24s_233U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_233U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CdoLoadU3Ec__Iterator3C_t1908116762, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CdoLoadU3Ec__Iterator3C_t1908116762, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CdoLoadU3Ec__Iterator3C_t1908116762, ___U3CU3Ef__this_3)); }
	inline ARMSingleRequest_t410726329 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline ARMSingleRequest_t410726329 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(ARMSingleRequest_t410726329 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
