﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_TriggerType3348422332.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TriggerEvent
struct  TriggerEvent_t3929747946  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.TriggerType HutongGames.PlayMaker.Actions.TriggerEvent::trigger
	int32_t ___trigger_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.TriggerEvent::collideTag
	FsmString_t952858651 * ___collideTag_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TriggerEvent::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.TriggerEvent::storeCollider
	FsmGameObject_t1697147867 * ___storeCollider_12;

public:
	inline static int32_t get_offset_of_trigger_9() { return static_cast<int32_t>(offsetof(TriggerEvent_t3929747946, ___trigger_9)); }
	inline int32_t get_trigger_9() const { return ___trigger_9; }
	inline int32_t* get_address_of_trigger_9() { return &___trigger_9; }
	inline void set_trigger_9(int32_t value)
	{
		___trigger_9 = value;
	}

	inline static int32_t get_offset_of_collideTag_10() { return static_cast<int32_t>(offsetof(TriggerEvent_t3929747946, ___collideTag_10)); }
	inline FsmString_t952858651 * get_collideTag_10() const { return ___collideTag_10; }
	inline FsmString_t952858651 ** get_address_of_collideTag_10() { return &___collideTag_10; }
	inline void set_collideTag_10(FsmString_t952858651 * value)
	{
		___collideTag_10 = value;
		Il2CppCodeGenWriteBarrier(&___collideTag_10, value);
	}

	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(TriggerEvent_t3929747946, ___sendEvent_11)); }
	inline FsmEvent_t2133468028 * get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmEvent_t2133468028 * value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_storeCollider_12() { return static_cast<int32_t>(offsetof(TriggerEvent_t3929747946, ___storeCollider_12)); }
	inline FsmGameObject_t1697147867 * get_storeCollider_12() const { return ___storeCollider_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeCollider_12() { return &___storeCollider_12; }
	inline void set_storeCollider_12(FsmGameObject_t1697147867 * value)
	{
		___storeCollider_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeCollider_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
