﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.Common.Detector.MathUtils::round(System.Single)
extern "C"  int32_t MathUtils_round_m385319040 (Il2CppObject * __this /* static, unused */, float ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.Common.Detector.MathUtils::distance(System.Single,System.Single,System.Single,System.Single)
extern "C"  float MathUtils_distance_m2341647582 (Il2CppObject * __this /* static, unused */, float ___aX0, float ___aY1, float ___bX2, float ___bY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.Common.Detector.MathUtils::distance(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  float MathUtils_distance_m4033097098 (Il2CppObject * __this /* static, unused */, int32_t ___aX0, int32_t ___aY1, int32_t ___bX2, int32_t ___bY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
