﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeftMenuControlScript
struct  LeftMenuControlScript_t2683180034  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject LeftMenuControlScript::MenuGameObject
	GameObject_t3674682005 * ___MenuGameObject_2;
	// UnityEngine.GameObject LeftMenuControlScript::MenuParentGameObject
	GameObject_t3674682005 * ___MenuParentGameObject_3;
	// UnityEngine.GameObject LeftMenuControlScript::SetaParentGameObject
	GameObject_t3674682005 * ___SetaParentGameObject_4;

public:
	inline static int32_t get_offset_of_MenuGameObject_2() { return static_cast<int32_t>(offsetof(LeftMenuControlScript_t2683180034, ___MenuGameObject_2)); }
	inline GameObject_t3674682005 * get_MenuGameObject_2() const { return ___MenuGameObject_2; }
	inline GameObject_t3674682005 ** get_address_of_MenuGameObject_2() { return &___MenuGameObject_2; }
	inline void set_MenuGameObject_2(GameObject_t3674682005 * value)
	{
		___MenuGameObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___MenuGameObject_2, value);
	}

	inline static int32_t get_offset_of_MenuParentGameObject_3() { return static_cast<int32_t>(offsetof(LeftMenuControlScript_t2683180034, ___MenuParentGameObject_3)); }
	inline GameObject_t3674682005 * get_MenuParentGameObject_3() const { return ___MenuParentGameObject_3; }
	inline GameObject_t3674682005 ** get_address_of_MenuParentGameObject_3() { return &___MenuParentGameObject_3; }
	inline void set_MenuParentGameObject_3(GameObject_t3674682005 * value)
	{
		___MenuParentGameObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___MenuParentGameObject_3, value);
	}

	inline static int32_t get_offset_of_SetaParentGameObject_4() { return static_cast<int32_t>(offsetof(LeftMenuControlScript_t2683180034, ___SetaParentGameObject_4)); }
	inline GameObject_t3674682005 * get_SetaParentGameObject_4() const { return ___SetaParentGameObject_4; }
	inline GameObject_t3674682005 ** get_address_of_SetaParentGameObject_4() { return &___SetaParentGameObject_4; }
	inline void set_SetaParentGameObject_4(GameObject_t3674682005 * value)
	{
		___SetaParentGameObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___SetaParentGameObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
