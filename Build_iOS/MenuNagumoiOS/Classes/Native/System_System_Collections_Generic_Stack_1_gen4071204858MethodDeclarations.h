﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::.ctor()
#define Stack_1__ctor_m4279320172(__this, method) ((  void (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::.ctor(System.Int32)
#define Stack_1__ctor_m1060926172(__this, ___count0, method) ((  void (*) (Stack_1_t4071204858 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m1580889733(__this, method) ((  bool (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3168188387(__this, method) ((  Il2CppObject * (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m3705103769(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t4071204858 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3004248661(__this, method) ((  Il2CppObject* (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m4230986836(__this, method) ((  Il2CppObject * (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Clear()
#define Stack_1_Clear_m242430518(__this, method) ((  void (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Contains(T)
#define Stack_1_Contains_m3076389180(__this, ___t0, method) ((  bool (*) (Stack_1_t4071204858 *, RectTransform_t972643934 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::CopyTo(T[],System.Int32)
#define Stack_1_CopyTo_m2536794116(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t4071204858 *, RectTransformU5BU5D_t3587651179*, int32_t, const MethodInfo*))Stack_1_CopyTo_m2694759063_gshared)(__this, ___dest0, ___idx1, method)
// T System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Peek()
#define Stack_1_Peek_m3384373653(__this, method) ((  RectTransform_t972643934 * (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Pop()
#define Stack_1_Pop_m1910595065(__this, method) ((  RectTransform_t972643934 * (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Push(T)
#define Stack_1_Push_m1693807971(__this, ___t0, method) ((  void (*) (Stack_1_t4071204858 *, RectTransform_t972643934 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// T[] System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::ToArray()
#define Stack_1_ToArray_m2145420276(__this, method) ((  RectTransformU5BU5D_t3587651179* (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_ToArray_m3752136753_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::get_Count()
#define Stack_1_get_Count_m2723978431(__this, method) ((  int32_t (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::GetEnumerator()
#define Stack_1_GetEnumerator_m3147324549(__this, method) ((  Enumerator_t3628990884  (*) (Stack_1_t4071204858 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
