﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonSendMessage
struct  ButtonSendMessage_t958819501  : public MonoBehaviour_t667441552
{
public:
	// System.String ButtonSendMessage::methodName
	String_t* ___methodName_2;
	// UnityEngine.GameObject ButtonSendMessage::listenerObj
	GameObject_t3674682005 * ___listenerObj_3;

public:
	inline static int32_t get_offset_of_methodName_2() { return static_cast<int32_t>(offsetof(ButtonSendMessage_t958819501, ___methodName_2)); }
	inline String_t* get_methodName_2() const { return ___methodName_2; }
	inline String_t** get_address_of_methodName_2() { return &___methodName_2; }
	inline void set_methodName_2(String_t* value)
	{
		___methodName_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_2, value);
	}

	inline static int32_t get_offset_of_listenerObj_3() { return static_cast<int32_t>(offsetof(ButtonSendMessage_t958819501, ___listenerObj_3)); }
	inline GameObject_t3674682005 * get_listenerObj_3() const { return ___listenerObj_3; }
	inline GameObject_t3674682005 ** get_address_of_listenerObj_3() { return &___listenerObj_3; }
	inline void set_listenerObj_3(GameObject_t3674682005 * value)
	{
		___listenerObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___listenerObj_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
