﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QRCodeDecodeController
struct QRCodeDecodeController_t2145328120;
// QRCodeDecodeController/QRScanFinished
struct QRScanFinished_t1583106503;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_QRCodeDecodeController_QRScanFin1583106503.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void QRCodeDecodeController::.ctor()
extern "C"  void QRCodeDecodeController__ctor_m4027649955 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::add_onQRScanFinished(QRCodeDecodeController/QRScanFinished)
extern "C"  void QRCodeDecodeController_add_onQRScanFinished_m3775438429 (QRCodeDecodeController_t2145328120 * __this, QRScanFinished_t1583106503 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::remove_onQRScanFinished(QRCodeDecodeController/QRScanFinished)
extern "C"  void QRCodeDecodeController_remove_onQRScanFinished_m176831872 (QRCodeDecodeController_t2145328120 * __this, QRScanFinished_t1583106503 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::Start()
extern "C"  void QRCodeDecodeController_Start_m2974787747 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::Update()
extern "C"  void QRCodeDecodeController_Update_m2029959114 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::Reset()
extern "C"  void QRCodeDecodeController_Reset_m1674082896 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::StartWork()
extern "C"  void QRCodeDecodeController_StartWork_m242760340 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::StopWork()
extern "C"  void QRCodeDecodeController_StopWork_m4228656820 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String QRCodeDecodeController::DecodeByStaticPic(UnityEngine.Texture2D)
extern "C"  String_t* QRCodeDecodeController_DecodeByStaticPic_m1160591719 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeDecodeController::<Update>m__80()
extern "C"  void QRCodeDecodeController_U3CUpdateU3Em__80_m4076760461 (QRCodeDecodeController_t2145328120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
