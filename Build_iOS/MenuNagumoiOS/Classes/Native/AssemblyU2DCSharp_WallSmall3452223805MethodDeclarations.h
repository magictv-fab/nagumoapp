﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WallSmall
struct WallSmall_t3452223805;

#include "codegen/il2cpp-codegen.h"

// System.Void WallSmall::.ctor()
extern "C"  void WallSmall__ctor_m2141153486 (WallSmall_t3452223805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallSmall::Start()
extern "C"  void WallSmall_Start_m1088291278 (WallSmall_t3452223805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallSmall::Update()
extern "C"  void WallSmall_Update_m3678110719 (WallSmall_t3452223805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
