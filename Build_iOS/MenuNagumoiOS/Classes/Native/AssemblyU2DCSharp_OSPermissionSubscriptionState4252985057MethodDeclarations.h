﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSPermissionSubscriptionState
struct OSPermissionSubscriptionState_t4252985057;

#include "codegen/il2cpp-codegen.h"

// System.Void OSPermissionSubscriptionState::.ctor()
extern "C"  void OSPermissionSubscriptionState__ctor_m2398836650 (OSPermissionSubscriptionState_t4252985057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
