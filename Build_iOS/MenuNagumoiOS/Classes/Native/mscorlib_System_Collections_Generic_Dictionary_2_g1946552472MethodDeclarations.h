﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>
struct Dictionary_2_t1946552472;
// System.Collections.Generic.IEqualityComparer`1<MagicTV.globals.StateMachine>
struct IEqualityComparer_1_t1159030274;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<MagicTV.globals.StateMachine>
struct ICollection_1_t1262585857;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>[]
struct KeyValuePair_2U5BU5D_t1352210207;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>>
struct IEnumerator_1_t3757198227;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,System.Object>
struct KeyCollection_t3573311923;
// System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,System.Object>
struct ValueCollection_t647158185;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3263875864.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m313334048_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m313334048(__this, method) ((  void (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2__ctor_m313334048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1040965527_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1040965527(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1040965527_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m700325489_gshared (Dictionary_2_t1946552472 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m700325489(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1946552472 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m700325489_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3970493025_gshared (Dictionary_2_t1946552472 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3970493025(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1946552472 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3970493025_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3693763896_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3693763896(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3693763896_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1334530398_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1334530398(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1334530398_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m656045772_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m656045772(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m656045772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2581260285_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2581260285(__this, method) ((  bool (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2581260285_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2615485276_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2615485276(__this, method) ((  bool (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2615485276_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2223991518_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2223991518(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2223991518_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1844925453_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1844925453(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1844925453_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m163829284_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m163829284(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m163829284_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1715344910_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1715344910(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1715344910_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1306888267_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1306888267(__this, ___key0, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1306888267_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m742654982_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m742654982(__this, method) ((  bool (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m742654982_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2476792_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2476792(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2476792_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2443036874_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2443036874(__this, method) ((  bool (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2443036874_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2715571041_gshared (Dictionary_2_t1946552472 * __this, KeyValuePair_2_t1845333178  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2715571041(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1946552472 *, KeyValuePair_2_t1845333178 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2715571041_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3927723333_gshared (Dictionary_2_t1946552472 * __this, KeyValuePair_2_t1845333178  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3927723333(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1946552472 *, KeyValuePair_2_t1845333178 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3927723333_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1530498309_gshared (Dictionary_2_t1946552472 * __this, KeyValuePair_2U5BU5D_t1352210207* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1530498309(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1946552472 *, KeyValuePair_2U5BU5D_t1352210207*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1530498309_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1953956266_gshared (Dictionary_2_t1946552472 * __this, KeyValuePair_2_t1845333178  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1953956266(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1946552472 *, KeyValuePair_2_t1845333178 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1953956266_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1579485220_gshared (Dictionary_2_t1946552472 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1579485220(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1579485220_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1855337779_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1855337779(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1855337779_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3463505834_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3463505834(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3463505834_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2878055927_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2878055927(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2878055927_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m442735936_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m442735936(__this, method) ((  int32_t (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_get_Count_m442735936_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3945905223_gshared (Dictionary_2_t1946552472 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3945905223(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3945905223_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3903772384_gshared (Dictionary_2_t1946552472 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3903772384(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1946552472 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3903772384_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4186501272_gshared (Dictionary_2_t1946552472 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4186501272(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1946552472 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4186501272_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1421764127_gshared (Dictionary_2_t1946552472 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1421764127(__this, ___size0, method) ((  void (*) (Dictionary_2_t1946552472 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1421764127_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1235787739_gshared (Dictionary_2_t1946552472 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1235787739(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1235787739_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1845333178  Dictionary_2_make_pair_m2182700655_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2182700655(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1845333178  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2182700655_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2564908655_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2564908655(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2564908655_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3811970059_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3811970059(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3811970059_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3261686676_gshared (Dictionary_2_t1946552472 * __this, KeyValuePair_2U5BU5D_t1352210207* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3261686676(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1946552472 *, KeyValuePair_2U5BU5D_t1352210207*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3261686676_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1181217560_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1181217560(__this, method) ((  void (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_Resize_m1181217560_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3241435029_gshared (Dictionary_2_t1946552472 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3241435029(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1946552472 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3241435029_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2014434635_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2014434635(__this, method) ((  void (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_Clear_m2014434635_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2859218741_gshared (Dictionary_2_t1946552472 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2859218741(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1946552472 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2859218741_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2461984949_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2461984949(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2461984949_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m5128382_gshared (Dictionary_2_t1946552472 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m5128382(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1946552472 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m5128382_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1782533222_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1782533222(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1782533222_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3579942043_gshared (Dictionary_2_t1946552472 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3579942043(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1946552472 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3579942043_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4093414030_gshared (Dictionary_2_t1946552472 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4093414030(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1946552472 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m4093414030_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::get_Keys()
extern "C"  KeyCollection_t3573311923 * Dictionary_2_get_Keys_m1171995341_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1171995341(__this, method) ((  KeyCollection_t3573311923 * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_get_Keys_m1171995341_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::get_Values()
extern "C"  ValueCollection_t647158185 * Dictionary_2_get_Values_m731752105_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m731752105(__this, method) ((  ValueCollection_t647158185 * (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_get_Values_m731752105_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2014767562_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2014767562(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2014767562_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2124284198_gshared (Dictionary_2_t1946552472 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2124284198(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1946552472 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2124284198_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1406167678_gshared (Dictionary_2_t1946552472 * __this, KeyValuePair_2_t1845333178  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1406167678(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1946552472 *, KeyValuePair_2_t1845333178 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1406167678_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3263875864  Dictionary_2_GetEnumerator_m1753609259_gshared (Dictionary_2_t1946552472 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1753609259(__this, method) ((  Enumerator_t3263875864  (*) (Dictionary_2_t1946552472 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1753609259_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m41236256_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m41236256(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m41236256_gshared)(__this /* static, unused */, ___key0, ___value1, method)
