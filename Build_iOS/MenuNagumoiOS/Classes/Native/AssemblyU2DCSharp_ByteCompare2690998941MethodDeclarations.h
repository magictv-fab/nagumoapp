﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ByteCompare
struct ByteCompare_t2690998941;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ByteCompare::.ctor()
extern "C"  void ByteCompare__ctor_m1119795566 (ByteCompare_t2690998941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ByteCompare::Compare(System.Byte[],System.Byte[],System.UInt32)
extern "C"  float ByteCompare_Compare_m4289673123 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___list10, ByteU5BU5D_t4260760469* ___list21, uint32_t ___byteMaxValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
