﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.DeviceEvents/OnSomething
struct OnSomething_t1610687155;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.DeviceEvents/OnSomething::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSomething__ctor_m761114378 (OnSomething_t1610687155 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents/OnSomething::Invoke()
extern "C"  void OnSomething_Invoke_m159781860 (OnSomething_t1610687155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.DeviceEvents/OnSomething::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSomething_BeginInvoke_m449031975 (OnSomething_t1610687155 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents/OnSomething::EndInvoke(System.IAsyncResult)
extern "C"  void OnSomething_EndInvoke_m2217096730 (OnSomething_t1610687155 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
