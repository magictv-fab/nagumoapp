﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSSubscriptionStateChanges
struct OSSubscriptionStateChanges_t52612275;

#include "codegen/il2cpp-codegen.h"

// System.Void OSSubscriptionStateChanges::.ctor()
extern "C"  void OSSubscriptionStateChanges__ctor_m2687841608 (OSSubscriptionStateChanges_t52612275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
