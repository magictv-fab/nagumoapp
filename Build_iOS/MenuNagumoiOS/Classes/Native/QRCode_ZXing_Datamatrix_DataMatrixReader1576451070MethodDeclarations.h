﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.DataMatrixReader
struct DataMatrixReader_t1576451070;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Result ZXing.Datamatrix.DataMatrixReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * DataMatrixReader_decode_m3011362479 (DataMatrixReader_t1576451070 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.DataMatrixReader::reset()
extern "C"  void DataMatrixReader_reset_m567987761 (DataMatrixReader_t1576451070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixReader::extractPureBits(ZXing.Common.BitMatrix)
extern "C"  BitMatrix_t1058711404 * DataMatrixReader_extractPureBits_m1774895732 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.DataMatrixReader::moduleSize(System.Int32[],ZXing.Common.BitMatrix,System.Int32&)
extern "C"  bool DataMatrixReader_moduleSize_m1663149950 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___leftTopBlack0, BitMatrix_t1058711404 * ___image1, int32_t* ___modulesize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.DataMatrixReader::.ctor()
extern "C"  void DataMatrixReader__ctor_m291240804 (DataMatrixReader_t1576451070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.DataMatrixReader::.cctor()
extern "C"  void DataMatrixReader__cctor_m4251401417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
