﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamTextureToImage
struct  WebCamTextureToImage_t27496160  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.WebCamTexture WebCamTextureToImage::webCamTexture
	WebCamTexture_t1290350902 * ___webCamTexture_2;

public:
	inline static int32_t get_offset_of_webCamTexture_2() { return static_cast<int32_t>(offsetof(WebCamTextureToImage_t27496160, ___webCamTexture_2)); }
	inline WebCamTexture_t1290350902 * get_webCamTexture_2() const { return ___webCamTexture_2; }
	inline WebCamTexture_t1290350902 ** get_address_of_webCamTexture_2() { return &___webCamTexture_2; }
	inline void set_webCamTexture_2(WebCamTexture_t1290350902 * value)
	{
		___webCamTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___webCamTexture_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
