﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.PDF417Writer
struct PDF417Writer_t2836006557;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// ZXing.PDF417.Internal.PDF417
struct PDF417_t3545372256;
// System.SByte[][]
struct SByteU5BU5DU5BU5D_t694789605;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_PDF417_Internal_PDF4173545372256.h"

// ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * PDF417Writer_encode_m3575852786 (PDF417Writer_t2836006557 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::bitMatrixFromEncoder(ZXing.PDF417.Internal.PDF417,System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  BitMatrix_t1058711404 * PDF417Writer_bitMatrixFromEncoder_m1805925835 (Il2CppObject * __this /* static, unused */, PDF417_t3545372256 * ___encoder0, String_t* ___contents1, int32_t ___width2, int32_t ___height3, int32_t ___margin4, int32_t ___errorCorrectionLevel5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::bitMatrixFrombitArray(System.SByte[][],System.Int32)
extern "C"  BitMatrix_t1058711404 * PDF417Writer_bitMatrixFrombitArray_m969192665 (Il2CppObject * __this /* static, unused */, SByteU5BU5DU5BU5D_t694789605* ___input0, int32_t ___margin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte[][] ZXing.PDF417.PDF417Writer::rotateArray(System.SByte[][])
extern "C"  SByteU5BU5DU5BU5D_t694789605* PDF417Writer_rotateArray_m507205613 (Il2CppObject * __this /* static, unused */, SByteU5BU5DU5BU5D_t694789605* ___bitarray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417Writer::.ctor()
extern "C"  void PDF417Writer__ctor_m1819308276 (PDF417Writer_t2836006557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
