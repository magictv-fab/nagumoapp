﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManagerGUI/<SaveExisting>c__Iterator2D
struct U3CSaveExistingU3Ec__Iterator2D_t2015724046;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::.ctor()
extern "C"  void U3CSaveExistingU3Ec__Iterator2D__ctor_m998749261 (U3CSaveExistingU3Ec__Iterator2D_t2015724046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveExistingU3Ec__Iterator2D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m501182895 (U3CSaveExistingU3Ec__Iterator2D_t2015724046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveExistingU3Ec__Iterator2D_System_Collections_IEnumerator_get_Current_m4096832323 (U3CSaveExistingU3Ec__Iterator2D_t2015724046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::MoveNext()
extern "C"  bool U3CSaveExistingU3Ec__Iterator2D_MoveNext_m471681647 (U3CSaveExistingU3Ec__Iterator2D_t2015724046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::Dispose()
extern "C"  void U3CSaveExistingU3Ec__Iterator2D_Dispose_m1918615882 (U3CSaveExistingU3Ec__Iterator2D_t2015724046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI/<SaveExisting>c__Iterator2D::Reset()
extern "C"  void U3CSaveExistingU3Ec__Iterator2D_Reset_m2940149498 (U3CSaveExistingU3Ec__Iterator2D_t2015724046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
