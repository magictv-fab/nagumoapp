﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct DecodedInformation_t1859732120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::.ctor(System.Int32,System.String)
extern "C"  void DecodedInformation__ctor_m3250570646 (DecodedInformation_t1859732120 * __this, int32_t ___newPosition0, String_t* ___newString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::.ctor(System.Int32,System.String,System.Int32)
extern "C"  void DecodedInformation__ctor_m1071618817 (DecodedInformation_t1859732120 * __this, int32_t ___newPosition0, String_t* ___newString1, int32_t ___remainingValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::getNewString()
extern "C"  String_t* DecodedInformation_getNewString_m2767921747 (DecodedInformation_t1859732120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::isRemaining()
extern "C"  bool DecodedInformation_isRemaining_m3873075111 (DecodedInformation_t1859732120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::getRemainingValue()
extern "C"  int32_t DecodedInformation_getRemainingValue_m55505510 (DecodedInformation_t1859732120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
