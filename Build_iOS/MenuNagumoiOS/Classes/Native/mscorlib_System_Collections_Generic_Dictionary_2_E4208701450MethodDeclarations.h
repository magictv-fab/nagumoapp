﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4133749648(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4208701450 *, Dictionary_2_t2891378058 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2081679185(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2867884837(__this, method) ((  void (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m150120878(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m733127533(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1209359039(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::MoveNext()
#define Enumerator_MoveNext_m2782864776(__this, method) ((  bool (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::get_Current()
#define Enumerator_get_Current_m814533079(__this, method) ((  KeyValuePair_2_t2790158764  (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3508702366(__this, method) ((  String_t* (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1715802946(__this, method) ((  AssetBundle_t2070959688 * (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::Reset()
#define Enumerator_Reset_m3219408354(__this, method) ((  void (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::VerifyState()
#define Enumerator_VerifyState_m942679147(__this, method) ((  void (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2421432595(__this, method) ((  void (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AssetBundle>::Dispose()
#define Enumerator_Dispose_m3998404146(__this, method) ((  void (*) (Enumerator_t4208701450 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
