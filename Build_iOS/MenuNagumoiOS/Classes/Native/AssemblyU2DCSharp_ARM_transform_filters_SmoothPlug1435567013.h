﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.device.AccelerometerEventDispacher
struct AccelerometerEventDispacher_t1386858712;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.SmoothPlugin
struct  SmoothPlugin_t1435567013  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// ARM.device.AccelerometerEventDispacher ARM.transform.filters.SmoothPlugin::accelerationShakeEvent
	AccelerometerEventDispacher_t1386858712 * ___accelerationShakeEvent_15;
	// System.Single ARM.transform.filters.SmoothPlugin::initialToleranceTime
	float ___initialToleranceTime_16;
	// System.Single ARM.transform.filters.SmoothPlugin::_toleranceTimePassed
	float ____toleranceTimePassed_17;
	// System.Single ARM.transform.filters.SmoothPlugin::tweenSpeedAngle
	float ___tweenSpeedAngle_18;
	// System.Single ARM.transform.filters.SmoothPlugin::tweenSpeedPosition
	float ___tweenSpeedPosition_19;
	// System.Single ARM.transform.filters.SmoothPlugin::_resetTweenSpeedAngle
	float ____resetTweenSpeedAngle_20;
	// System.Single ARM.transform.filters.SmoothPlugin::_resetTweenSpeedPosition
	float ____resetTweenSpeedPosition_21;
	// System.Single ARM.transform.filters.SmoothPlugin::_resetInitialToleranceTime
	float ____resetInitialToleranceTime_22;
	// System.Boolean ARM.transform.filters.SmoothPlugin::_moveQuick
	bool ____moveQuick_23;

public:
	inline static int32_t get_offset_of_accelerationShakeEvent_15() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ___accelerationShakeEvent_15)); }
	inline AccelerometerEventDispacher_t1386858712 * get_accelerationShakeEvent_15() const { return ___accelerationShakeEvent_15; }
	inline AccelerometerEventDispacher_t1386858712 ** get_address_of_accelerationShakeEvent_15() { return &___accelerationShakeEvent_15; }
	inline void set_accelerationShakeEvent_15(AccelerometerEventDispacher_t1386858712 * value)
	{
		___accelerationShakeEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___accelerationShakeEvent_15, value);
	}

	inline static int32_t get_offset_of_initialToleranceTime_16() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ___initialToleranceTime_16)); }
	inline float get_initialToleranceTime_16() const { return ___initialToleranceTime_16; }
	inline float* get_address_of_initialToleranceTime_16() { return &___initialToleranceTime_16; }
	inline void set_initialToleranceTime_16(float value)
	{
		___initialToleranceTime_16 = value;
	}

	inline static int32_t get_offset_of__toleranceTimePassed_17() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ____toleranceTimePassed_17)); }
	inline float get__toleranceTimePassed_17() const { return ____toleranceTimePassed_17; }
	inline float* get_address_of__toleranceTimePassed_17() { return &____toleranceTimePassed_17; }
	inline void set__toleranceTimePassed_17(float value)
	{
		____toleranceTimePassed_17 = value;
	}

	inline static int32_t get_offset_of_tweenSpeedAngle_18() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ___tweenSpeedAngle_18)); }
	inline float get_tweenSpeedAngle_18() const { return ___tweenSpeedAngle_18; }
	inline float* get_address_of_tweenSpeedAngle_18() { return &___tweenSpeedAngle_18; }
	inline void set_tweenSpeedAngle_18(float value)
	{
		___tweenSpeedAngle_18 = value;
	}

	inline static int32_t get_offset_of_tweenSpeedPosition_19() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ___tweenSpeedPosition_19)); }
	inline float get_tweenSpeedPosition_19() const { return ___tweenSpeedPosition_19; }
	inline float* get_address_of_tweenSpeedPosition_19() { return &___tweenSpeedPosition_19; }
	inline void set_tweenSpeedPosition_19(float value)
	{
		___tweenSpeedPosition_19 = value;
	}

	inline static int32_t get_offset_of__resetTweenSpeedAngle_20() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ____resetTweenSpeedAngle_20)); }
	inline float get__resetTweenSpeedAngle_20() const { return ____resetTweenSpeedAngle_20; }
	inline float* get_address_of__resetTweenSpeedAngle_20() { return &____resetTweenSpeedAngle_20; }
	inline void set__resetTweenSpeedAngle_20(float value)
	{
		____resetTweenSpeedAngle_20 = value;
	}

	inline static int32_t get_offset_of__resetTweenSpeedPosition_21() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ____resetTweenSpeedPosition_21)); }
	inline float get__resetTweenSpeedPosition_21() const { return ____resetTweenSpeedPosition_21; }
	inline float* get_address_of__resetTweenSpeedPosition_21() { return &____resetTweenSpeedPosition_21; }
	inline void set__resetTweenSpeedPosition_21(float value)
	{
		____resetTweenSpeedPosition_21 = value;
	}

	inline static int32_t get_offset_of__resetInitialToleranceTime_22() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ____resetInitialToleranceTime_22)); }
	inline float get__resetInitialToleranceTime_22() const { return ____resetInitialToleranceTime_22; }
	inline float* get_address_of__resetInitialToleranceTime_22() { return &____resetInitialToleranceTime_22; }
	inline void set__resetInitialToleranceTime_22(float value)
	{
		____resetInitialToleranceTime_22 = value;
	}

	inline static int32_t get_offset_of__moveQuick_23() { return static_cast<int32_t>(offsetof(SmoothPlugin_t1435567013, ____moveQuick_23)); }
	inline bool get__moveQuick_23() const { return ____moveQuick_23; }
	inline bool* get_address_of__moveQuick_23() { return &____moveQuick_23; }
	inline void set__moveQuick_23(bool value)
	{
		____moveQuick_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
