﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputController
struct  InputController_t2664602342  : public Il2CppObject
{
public:

public:
};

struct InputController_t2664602342_StaticFields
{
public:
	// System.Boolean InputController::GUIInput
	bool ___GUIInput_0;
	// System.Action InputController::DoubleTapped
	Action_t3771233898 * ___DoubleTapped_1;
	// System.Action InputController::SingleTapped
	Action_t3771233898 * ___SingleTapped_2;
	// System.Action InputController::BackButtonTapped
	Action_t3771233898 * ___BackButtonTapped_3;
	// System.Single InputController::timeSinceBackEventDispatched
	float ___timeSinceBackEventDispatched_4;
	// System.Boolean InputController::backButtonEventDispached
	bool ___backButtonEventDispached_5;
	// System.Boolean InputController::tapEventDispatched
	bool ___tapEventDispatched_6;
	// System.Single InputController::mMillisecondsSinceLastTap
	float ___mMillisecondsSinceLastTap_7;
	// System.Int32 InputController::MAX_TAP_MILLISEC
	int32_t ___MAX_TAP_MILLISEC_8;
	// System.Single InputController::MAX_TAP_DISTANCE_SCREEN_SPACE
	float ___MAX_TAP_DISTANCE_SCREEN_SPACE_9;
	// System.Boolean InputController::mWaitingForSecondTap
	bool ___mWaitingForSecondTap_10;
	// UnityEngine.Vector3 InputController::mFirstTapPosition
	Vector3_t4282066566  ___mFirstTapPosition_11;
	// System.DateTime InputController::mFirstTapTime
	DateTime_t4283661327  ___mFirstTapTime_12;

public:
	inline static int32_t get_offset_of_GUIInput_0() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___GUIInput_0)); }
	inline bool get_GUIInput_0() const { return ___GUIInput_0; }
	inline bool* get_address_of_GUIInput_0() { return &___GUIInput_0; }
	inline void set_GUIInput_0(bool value)
	{
		___GUIInput_0 = value;
	}

	inline static int32_t get_offset_of_DoubleTapped_1() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___DoubleTapped_1)); }
	inline Action_t3771233898 * get_DoubleTapped_1() const { return ___DoubleTapped_1; }
	inline Action_t3771233898 ** get_address_of_DoubleTapped_1() { return &___DoubleTapped_1; }
	inline void set_DoubleTapped_1(Action_t3771233898 * value)
	{
		___DoubleTapped_1 = value;
		Il2CppCodeGenWriteBarrier(&___DoubleTapped_1, value);
	}

	inline static int32_t get_offset_of_SingleTapped_2() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___SingleTapped_2)); }
	inline Action_t3771233898 * get_SingleTapped_2() const { return ___SingleTapped_2; }
	inline Action_t3771233898 ** get_address_of_SingleTapped_2() { return &___SingleTapped_2; }
	inline void set_SingleTapped_2(Action_t3771233898 * value)
	{
		___SingleTapped_2 = value;
		Il2CppCodeGenWriteBarrier(&___SingleTapped_2, value);
	}

	inline static int32_t get_offset_of_BackButtonTapped_3() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___BackButtonTapped_3)); }
	inline Action_t3771233898 * get_BackButtonTapped_3() const { return ___BackButtonTapped_3; }
	inline Action_t3771233898 ** get_address_of_BackButtonTapped_3() { return &___BackButtonTapped_3; }
	inline void set_BackButtonTapped_3(Action_t3771233898 * value)
	{
		___BackButtonTapped_3 = value;
		Il2CppCodeGenWriteBarrier(&___BackButtonTapped_3, value);
	}

	inline static int32_t get_offset_of_timeSinceBackEventDispatched_4() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___timeSinceBackEventDispatched_4)); }
	inline float get_timeSinceBackEventDispatched_4() const { return ___timeSinceBackEventDispatched_4; }
	inline float* get_address_of_timeSinceBackEventDispatched_4() { return &___timeSinceBackEventDispatched_4; }
	inline void set_timeSinceBackEventDispatched_4(float value)
	{
		___timeSinceBackEventDispatched_4 = value;
	}

	inline static int32_t get_offset_of_backButtonEventDispached_5() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___backButtonEventDispached_5)); }
	inline bool get_backButtonEventDispached_5() const { return ___backButtonEventDispached_5; }
	inline bool* get_address_of_backButtonEventDispached_5() { return &___backButtonEventDispached_5; }
	inline void set_backButtonEventDispached_5(bool value)
	{
		___backButtonEventDispached_5 = value;
	}

	inline static int32_t get_offset_of_tapEventDispatched_6() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___tapEventDispatched_6)); }
	inline bool get_tapEventDispatched_6() const { return ___tapEventDispatched_6; }
	inline bool* get_address_of_tapEventDispatched_6() { return &___tapEventDispatched_6; }
	inline void set_tapEventDispatched_6(bool value)
	{
		___tapEventDispatched_6 = value;
	}

	inline static int32_t get_offset_of_mMillisecondsSinceLastTap_7() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___mMillisecondsSinceLastTap_7)); }
	inline float get_mMillisecondsSinceLastTap_7() const { return ___mMillisecondsSinceLastTap_7; }
	inline float* get_address_of_mMillisecondsSinceLastTap_7() { return &___mMillisecondsSinceLastTap_7; }
	inline void set_mMillisecondsSinceLastTap_7(float value)
	{
		___mMillisecondsSinceLastTap_7 = value;
	}

	inline static int32_t get_offset_of_MAX_TAP_MILLISEC_8() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___MAX_TAP_MILLISEC_8)); }
	inline int32_t get_MAX_TAP_MILLISEC_8() const { return ___MAX_TAP_MILLISEC_8; }
	inline int32_t* get_address_of_MAX_TAP_MILLISEC_8() { return &___MAX_TAP_MILLISEC_8; }
	inline void set_MAX_TAP_MILLISEC_8(int32_t value)
	{
		___MAX_TAP_MILLISEC_8 = value;
	}

	inline static int32_t get_offset_of_MAX_TAP_DISTANCE_SCREEN_SPACE_9() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___MAX_TAP_DISTANCE_SCREEN_SPACE_9)); }
	inline float get_MAX_TAP_DISTANCE_SCREEN_SPACE_9() const { return ___MAX_TAP_DISTANCE_SCREEN_SPACE_9; }
	inline float* get_address_of_MAX_TAP_DISTANCE_SCREEN_SPACE_9() { return &___MAX_TAP_DISTANCE_SCREEN_SPACE_9; }
	inline void set_MAX_TAP_DISTANCE_SCREEN_SPACE_9(float value)
	{
		___MAX_TAP_DISTANCE_SCREEN_SPACE_9 = value;
	}

	inline static int32_t get_offset_of_mWaitingForSecondTap_10() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___mWaitingForSecondTap_10)); }
	inline bool get_mWaitingForSecondTap_10() const { return ___mWaitingForSecondTap_10; }
	inline bool* get_address_of_mWaitingForSecondTap_10() { return &___mWaitingForSecondTap_10; }
	inline void set_mWaitingForSecondTap_10(bool value)
	{
		___mWaitingForSecondTap_10 = value;
	}

	inline static int32_t get_offset_of_mFirstTapPosition_11() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___mFirstTapPosition_11)); }
	inline Vector3_t4282066566  get_mFirstTapPosition_11() const { return ___mFirstTapPosition_11; }
	inline Vector3_t4282066566 * get_address_of_mFirstTapPosition_11() { return &___mFirstTapPosition_11; }
	inline void set_mFirstTapPosition_11(Vector3_t4282066566  value)
	{
		___mFirstTapPosition_11 = value;
	}

	inline static int32_t get_offset_of_mFirstTapTime_12() { return static_cast<int32_t>(offsetof(InputController_t2664602342_StaticFields, ___mFirstTapTime_12)); }
	inline DateTime_t4283661327  get_mFirstTapTime_12() const { return ___mFirstTapTime_12; }
	inline DateTime_t4283661327 * get_address_of_mFirstTapTime_12() { return &___mFirstTapTime_12; }
	inline void set_mFirstTapTime_12(DateTime_t4283661327  value)
	{
		___mFirstTapTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
