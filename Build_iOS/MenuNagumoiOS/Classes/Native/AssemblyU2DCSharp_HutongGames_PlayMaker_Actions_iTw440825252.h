﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenLookTo
struct  iTweenLookTo_t440825252  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenLookTo::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenLookTo::id
	FsmString_t952858651 * ___id_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenLookTo::transformTarget
	FsmGameObject_t1697147867 * ___transformTarget_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenLookTo::vectorTarget
	FsmVector3_t533912882 * ___vectorTarget_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenLookTo::time
	FsmFloat_t2134102846 * ___time_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenLookTo::delay
	FsmFloat_t2134102846 * ___delay_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenLookTo::speed
	FsmFloat_t2134102846 * ___speed_23;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenLookTo::easeType
	int32_t ___easeType_24;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenLookTo::loopType
	int32_t ___loopType_25;
	// HutongGames.PlayMaker.Actions.iTweenFsmAction/AxisRestriction HutongGames.PlayMaker.Actions.iTweenLookTo::axis
	int32_t ___axis_26;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_id_18() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___id_18)); }
	inline FsmString_t952858651 * get_id_18() const { return ___id_18; }
	inline FsmString_t952858651 ** get_address_of_id_18() { return &___id_18; }
	inline void set_id_18(FsmString_t952858651 * value)
	{
		___id_18 = value;
		Il2CppCodeGenWriteBarrier(&___id_18, value);
	}

	inline static int32_t get_offset_of_transformTarget_19() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___transformTarget_19)); }
	inline FsmGameObject_t1697147867 * get_transformTarget_19() const { return ___transformTarget_19; }
	inline FsmGameObject_t1697147867 ** get_address_of_transformTarget_19() { return &___transformTarget_19; }
	inline void set_transformTarget_19(FsmGameObject_t1697147867 * value)
	{
		___transformTarget_19 = value;
		Il2CppCodeGenWriteBarrier(&___transformTarget_19, value);
	}

	inline static int32_t get_offset_of_vectorTarget_20() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___vectorTarget_20)); }
	inline FsmVector3_t533912882 * get_vectorTarget_20() const { return ___vectorTarget_20; }
	inline FsmVector3_t533912882 ** get_address_of_vectorTarget_20() { return &___vectorTarget_20; }
	inline void set_vectorTarget_20(FsmVector3_t533912882 * value)
	{
		___vectorTarget_20 = value;
		Il2CppCodeGenWriteBarrier(&___vectorTarget_20, value);
	}

	inline static int32_t get_offset_of_time_21() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___time_21)); }
	inline FsmFloat_t2134102846 * get_time_21() const { return ___time_21; }
	inline FsmFloat_t2134102846 ** get_address_of_time_21() { return &___time_21; }
	inline void set_time_21(FsmFloat_t2134102846 * value)
	{
		___time_21 = value;
		Il2CppCodeGenWriteBarrier(&___time_21, value);
	}

	inline static int32_t get_offset_of_delay_22() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___delay_22)); }
	inline FsmFloat_t2134102846 * get_delay_22() const { return ___delay_22; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_22() { return &___delay_22; }
	inline void set_delay_22(FsmFloat_t2134102846 * value)
	{
		___delay_22 = value;
		Il2CppCodeGenWriteBarrier(&___delay_22, value);
	}

	inline static int32_t get_offset_of_speed_23() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___speed_23)); }
	inline FsmFloat_t2134102846 * get_speed_23() const { return ___speed_23; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_23() { return &___speed_23; }
	inline void set_speed_23(FsmFloat_t2134102846 * value)
	{
		___speed_23 = value;
		Il2CppCodeGenWriteBarrier(&___speed_23, value);
	}

	inline static int32_t get_offset_of_easeType_24() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___easeType_24)); }
	inline int32_t get_easeType_24() const { return ___easeType_24; }
	inline int32_t* get_address_of_easeType_24() { return &___easeType_24; }
	inline void set_easeType_24(int32_t value)
	{
		___easeType_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_axis_26() { return static_cast<int32_t>(offsetof(iTweenLookTo_t440825252, ___axis_26)); }
	inline int32_t get_axis_26() const { return ___axis_26; }
	inline int32_t* get_address_of_axis_26() { return &___axis_26; }
	inline void set_axis_26(int32_t value)
	{
		___axis_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
