﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int64>
struct List_1_t2522024147;
// System.Collections.Generic.IEnumerable`1<System.Int64>
struct IEnumerable_1_t159784256;
// System.Int64[]
struct Int64U5BU5D_t2174042770;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t3065703644;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t2048428582;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>
struct ReadOnlyCollection_1_t2710916131;
// System.Predicate`1<System.Int64>
struct Predicate_1_t764895478;
// System.Action`1<System.Int64>
struct Action_1_t1549654731;
// System.Collections.Generic.IComparer`1<System.Int64>
struct IComparer_1_t3728852637;
// System.Comparison`1<System.Int64>
struct Comparison_1_t4165167078;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696917.h"

// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor()
extern "C"  void List_1__ctor_m2722530713_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1__ctor_m2722530713(__this, method) ((  void (*) (List_1_t2522024147 *, const MethodInfo*))List_1__ctor_m2722530713_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4154939677_gshared (List_1_t2522024147 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4154939677(__this, ___collection0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4154939677_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m120513651_gshared (List_1_t2522024147 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m120513651(__this, ___capacity0, method) ((  void (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1__ctor_m120513651_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m3423663997_gshared (List_1_t2522024147 * __this, Int64U5BU5D_t2174042770* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m3423663997(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2522024147 *, Int64U5BU5D_t2174042770*, int32_t, const MethodInfo*))List_1__ctor_m3423663997_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.cctor()
extern "C"  void List_1__cctor_m491306443_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m491306443(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m491306443_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int64>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751001516_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751001516(__this, method) ((  Il2CppObject* (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751001516_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2167217506_gshared (List_1_t2522024147 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2167217506(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2522024147 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2167217506_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3786885873_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3786885873(__this, method) ((  Il2CppObject * (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3786885873_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1407510508_gshared (List_1_t2522024147 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1407510508(__this, ___item0, method) ((  int32_t (*) (List_1_t2522024147 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1407510508_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m530987988_gshared (List_1_t2522024147 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m530987988(__this, ___item0, method) ((  bool (*) (List_1_t2522024147 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m530987988_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m894912580_gshared (List_1_t2522024147 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m894912580(__this, ___item0, method) ((  int32_t (*) (List_1_t2522024147 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m894912580_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2059506103_gshared (List_1_t2522024147 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2059506103(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2522024147 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2059506103_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m536123921_gshared (List_1_t2522024147 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m536123921(__this, ___item0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m536123921_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3932713685_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3932713685(__this, method) ((  bool (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3932713685_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m293575432_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m293575432(__this, method) ((  bool (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m293575432_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m119941370_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m119941370(__this, method) ((  Il2CppObject * (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m119941370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4138490435_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4138490435(__this, method) ((  bool (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4138490435_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1972981846_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1972981846(__this, method) ((  bool (*) (List_1_t2522024147 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1972981846_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m365151233_gshared (List_1_t2522024147 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m365151233(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m365151233_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4114361294_gshared (List_1_t2522024147 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4114361294(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2522024147 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4114361294_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Add(T)
extern "C"  void List_1_Add_m558752285_gshared (List_1_t2522024147 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_Add_m558752285(__this, ___item0, method) ((  void (*) (List_1_t2522024147 *, int64_t, const MethodInfo*))List_1_Add_m558752285_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2593120216_gshared (List_1_t2522024147 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2593120216(__this, ___newCount0, method) ((  void (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2593120216_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2473356559_gshared (List_1_t2522024147 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2473356559(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2522024147 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2473356559_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3863664342_gshared (List_1_t2522024147 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3863664342(__this, ___collection0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3863664342_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3124636566_gshared (List_1_t2522024147 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3124636566(__this, ___enumerable0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3124636566_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2901368129_gshared (List_1_t2522024147 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2901368129(__this, ___collection0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2901368129_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int64>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2710916131 * List_1_AsReadOnly_m2857059748_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2857059748(__this, method) ((  ReadOnlyCollection_1_t2710916131 * (*) (List_1_t2522024147 *, const MethodInfo*))List_1_AsReadOnly_m2857059748_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Clear()
extern "C"  void List_1_Clear_m2148142669_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_Clear_m2148142669(__this, method) ((  void (*) (List_1_t2522024147 *, const MethodInfo*))List_1_Clear_m2148142669_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::Contains(T)
extern "C"  bool List_1_Contains_m348921023_gshared (List_1_t2522024147 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_Contains_m348921023(__this, ___item0, method) ((  bool (*) (List_1_t2522024147 *, int64_t, const MethodInfo*))List_1_Contains_m348921023_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3419167949_gshared (List_1_t2522024147 * __this, Int64U5BU5D_t2174042770* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3419167949(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2522024147 *, Int64U5BU5D_t2174042770*, int32_t, const MethodInfo*))List_1_CopyTo_m3419167949_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Int64>::Find(System.Predicate`1<T>)
extern "C"  int64_t List_1_Find_m3570887833_gshared (List_1_t2522024147 * __this, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_Find_m3570887833(__this, ___match0, method) ((  int64_t (*) (List_1_t2522024147 *, Predicate_1_t764895478 *, const MethodInfo*))List_1_Find_m3570887833_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m728553654_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m728553654(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t764895478 *, const MethodInfo*))List_1_CheckMatch_m728553654_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int64>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t2522024147 * List_1_FindAll_m4130952106_gshared (List_1_t2522024147 * __this, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m4130952106(__this, ___match0, method) ((  List_1_t2522024147 * (*) (List_1_t2522024147 *, Predicate_1_t764895478 *, const MethodInfo*))List_1_FindAll_m4130952106_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int64>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t2522024147 * List_1_FindAllStackBits_m4207285424_gshared (List_1_t2522024147 * __this, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m4207285424(__this, ___match0, method) ((  List_1_t2522024147 * (*) (List_1_t2522024147 *, Predicate_1_t764895478 *, const MethodInfo*))List_1_FindAllStackBits_m4207285424_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int64>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t2522024147 * List_1_FindAllList_m2068622892_gshared (List_1_t2522024147 * __this, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2068622892(__this, ___match0, method) ((  List_1_t2522024147 * (*) (List_1_t2522024147 *, Predicate_1_t764895478 *, const MethodInfo*))List_1_FindAllList_m2068622892_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1911567507_gshared (List_1_t2522024147 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t764895478 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1911567507(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2522024147 *, int32_t, int32_t, Predicate_1_t764895478 *, const MethodInfo*))List_1_GetIndex_m1911567507_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1861088426_gshared (List_1_t2522024147 * __this, Action_1_t1549654731 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1861088426(__this, ___action0, method) ((  void (*) (List_1_t2522024147 *, Action_1_t1549654731 *, const MethodInfo*))List_1_ForEach_m1861088426_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int64>::GetEnumerator()
extern "C"  Enumerator_t2541696917  List_1_GetEnumerator_m3800196732_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3800196732(__this, method) ((  Enumerator_t2541696917  (*) (List_1_t2522024147 *, const MethodInfo*))List_1_GetEnumerator_m3800196732_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int64>::GetRange(System.Int32,System.Int32)
extern "C"  List_1_t2522024147 * List_1_GetRange_m2700295104_gshared (List_1_t2522024147 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_GetRange_m2700295104(__this, ___index0, ___count1, method) ((  List_1_t2522024147 * (*) (List_1_t2522024147 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m2700295104_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4291468633_gshared (List_1_t2522024147 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4291468633(__this, ___item0, method) ((  int32_t (*) (List_1_t2522024147 *, int64_t, const MethodInfo*))List_1_IndexOf_m4291468633_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4275377700_gshared (List_1_t2522024147 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4275377700(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2522024147 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4275377700_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3165535133_gshared (List_1_t2522024147 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3165535133(__this, ___index0, method) ((  void (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3165535133_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1048920708_gshared (List_1_t2522024147 * __this, int32_t ___index0, int64_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1048920708(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2522024147 *, int32_t, int64_t, const MethodInfo*))List_1_Insert_m1048920708_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2656357369_gshared (List_1_t2522024147 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2656357369(__this, ___collection0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2656357369_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::Remove(T)
extern "C"  bool List_1_Remove_m2609840250_gshared (List_1_t2522024147 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2609840250(__this, ___item0, method) ((  bool (*) (List_1_t2522024147 *, int64_t, const MethodInfo*))List_1_Remove_m2609840250_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m530574428_gshared (List_1_t2522024147 * __this, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m530574428(__this, ___match0, method) ((  int32_t (*) (List_1_t2522024147 *, Predicate_1_t764895478 *, const MethodInfo*))List_1_RemoveAll_m530574428_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3217740874_gshared (List_1_t2522024147 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3217740874(__this, ___index0, method) ((  void (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3217740874_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3394821229_gshared (List_1_t2522024147 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3394821229(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2522024147 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3394821229_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Reverse()
extern "C"  void List_1_Reverse_m148522466_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_Reverse_m148522466(__this, method) ((  void (*) (List_1_t2522024147 *, const MethodInfo*))List_1_Reverse_m148522466_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Sort()
extern "C"  void List_1_Sort_m1084724480_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_Sort_m1084724480(__this, method) ((  void (*) (List_1_t2522024147 *, const MethodInfo*))List_1_Sort_m1084724480_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3737735396_gshared (List_1_t2522024147 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3737735396(__this, ___comparer0, method) ((  void (*) (List_1_t2522024147 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3737735396_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3240091987_gshared (List_1_t2522024147 * __this, Comparison_1_t4165167078 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3240091987(__this, ___comparison0, method) ((  void (*) (List_1_t2522024147 *, Comparison_1_t4165167078 *, const MethodInfo*))List_1_Sort_m3240091987_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Int64>::ToArray()
extern "C"  Int64U5BU5D_t2174042770* List_1_ToArray_m2180258017_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_ToArray_m2180258017(__this, method) ((  Int64U5BU5D_t2174042770* (*) (List_1_t2522024147 *, const MethodInfo*))List_1_ToArray_m2180258017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3490903193_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3490903193(__this, method) ((  void (*) (List_1_t2522024147 *, const MethodInfo*))List_1_TrimExcess_m3490903193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::TrueForAll(System.Predicate`1<T>)
extern "C"  bool List_1_TrueForAll_m259617647_gshared (List_1_t2522024147 * __this, Predicate_1_t764895478 * ___match0, const MethodInfo* method);
#define List_1_TrueForAll_m259617647(__this, ___match0, method) ((  bool (*) (List_1_t2522024147 *, Predicate_1_t764895478 *, const MethodInfo*))List_1_TrueForAll_m259617647_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2486845705_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2486845705(__this, method) ((  int32_t (*) (List_1_t2522024147 *, const MethodInfo*))List_1_get_Capacity_m2486845705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4098276330_gshared (List_1_t2522024147 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4098276330(__this, ___value0, method) ((  void (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4098276330_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::get_Count()
extern "C"  int32_t List_1_get_Count_m4003816258_gshared (List_1_t2522024147 * __this, const MethodInfo* method);
#define List_1_get_Count_m4003816258(__this, method) ((  int32_t (*) (List_1_t2522024147 *, const MethodInfo*))List_1_get_Count_m4003816258_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int64>::get_Item(System.Int32)
extern "C"  int64_t List_1_get_Item_m846582384_gshared (List_1_t2522024147 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m846582384(__this, ___index0, method) ((  int64_t (*) (List_1_t2522024147 *, int32_t, const MethodInfo*))List_1_get_Item_m846582384_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m505082459_gshared (List_1_t2522024147 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m505082459(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2522024147 *, int32_t, int64_t, const MethodInfo*))List_1_set_Item_m505082459_gshared)(__this, ___index0, ___value1, method)
