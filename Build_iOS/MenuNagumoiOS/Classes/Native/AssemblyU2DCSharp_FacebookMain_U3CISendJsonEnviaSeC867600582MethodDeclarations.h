﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F
struct U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::.ctor()
extern "C"  void U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F__ctor_m3453166741 (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749684071 (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_System_Collections_IEnumerator_get_Current_m749505275 (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::MoveNext()
extern "C"  bool U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_MoveNext_m3738345767 (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::Dispose()
extern "C"  void U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_Dispose_m2676768658 (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::Reset()
extern "C"  void U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_Reset_m1099599682 (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
