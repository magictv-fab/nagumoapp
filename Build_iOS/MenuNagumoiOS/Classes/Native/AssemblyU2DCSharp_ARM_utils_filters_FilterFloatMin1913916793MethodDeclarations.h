﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.filters.FilterFloatMinMaxSize
struct FilterFloatMinMaxSize_t1913916793;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.filters.FilterFloatMinMaxSize::.ctor()
extern "C"  void FilterFloatMinMaxSize__ctor_m2068119126 (FilterFloatMinMaxSize_t1913916793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.filters.FilterFloatMinMaxSize::filter(System.Single)
extern "C"  float FilterFloatMinMaxSize_filter_m456099057 (FilterFloatMinMaxSize_t1913916793 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
