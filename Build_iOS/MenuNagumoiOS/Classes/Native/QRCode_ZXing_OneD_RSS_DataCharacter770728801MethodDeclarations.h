﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.DataCharacter
struct DataCharacter_t770728801;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 ZXing.OneD.RSS.DataCharacter::get_Value()
extern "C"  int32_t DataCharacter_get_Value_m2301466454 (DataCharacter_t770728801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.DataCharacter::set_Value(System.Int32)
extern "C"  void DataCharacter_set_Value_m6665537 (DataCharacter_t770728801 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.DataCharacter::get_ChecksumPortion()
extern "C"  int32_t DataCharacter_get_ChecksumPortion_m1086039625 (DataCharacter_t770728801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.DataCharacter::set_ChecksumPortion(System.Int32)
extern "C"  void DataCharacter_set_ChecksumPortion_m1450063668 (DataCharacter_t770728801 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.DataCharacter::.ctor(System.Int32,System.Int32)
extern "C"  void DataCharacter__ctor_m624996072 (DataCharacter_t770728801 * __this, int32_t ___value0, int32_t ___checksumPortion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.DataCharacter::ToString()
extern "C"  String_t* DataCharacter_ToString_m666245013 (DataCharacter_t770728801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.DataCharacter::Equals(System.Object)
extern "C"  bool DataCharacter_Equals_m4011541243 (DataCharacter_t770728801 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.DataCharacter::GetHashCode()
extern "C"  int32_t DataCharacter_GetHashCode_m3048138783 (DataCharacter_t770728801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
