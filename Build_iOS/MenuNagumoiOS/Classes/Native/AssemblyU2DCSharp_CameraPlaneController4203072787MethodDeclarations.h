﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraPlaneController
struct CameraPlaneController_t4203072787;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraPlaneController::.ctor()
extern "C"  void CameraPlaneController__ctor_m1066086840 (CameraPlaneController_t4203072787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraPlaneController::Start()
extern "C"  void CameraPlaneController_Start_m13224632 (CameraPlaneController_t4203072787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraPlaneController::init()
extern "C"  void CameraPlaneController_init_m2010438012 (CameraPlaneController_t4203072787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraPlaneController::Update()
extern "C"  void CameraPlaneController_Update_m415815765 (CameraPlaneController_t4203072787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraPlaneController::assignAngleVectors()
extern "C"  void CameraPlaneController_assignAngleVectors_m2626529272 (CameraPlaneController_t4203072787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraPlaneController::correctPlaneScale(System.Single)
extern "C"  void CameraPlaneController_correctPlaneScale_m2106588829 (CameraPlaneController_t4203072787 * __this, float ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraPlaneController::updateRotationAndScale()
extern "C"  void CameraPlaneController_updateRotationAndScale_m1459157862 (CameraPlaneController_t4203072787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
