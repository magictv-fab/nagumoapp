﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipNameTransform
struct ZipNameTransform_t1757759150;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipNameTransform::.ctor()
extern "C"  void ZipNameTransform__ctor_m665237693 (ZipNameTransform_t1757759150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipNameTransform::.ctor(System.String)
extern "C"  void ZipNameTransform__ctor_m4129538405 (ZipNameTransform_t1757759150 * __this, String_t* ___trimPrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipNameTransform::.cctor()
extern "C"  void ZipNameTransform__cctor_m2960403088 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipNameTransform::TransformDirectory(System.String)
extern "C"  String_t* ZipNameTransform_TransformDirectory_m2930866423 (ZipNameTransform_t1757759150 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipNameTransform::TransformFile(System.String)
extern "C"  String_t* ZipNameTransform_TransformFile_m642726242 (ZipNameTransform_t1757759150 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipNameTransform::get_TrimPrefix()
extern "C"  String_t* ZipNameTransform_get_TrimPrefix_m1464042215 (ZipNameTransform_t1757759150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipNameTransform::set_TrimPrefix(System.String)
extern "C"  void ZipNameTransform_set_TrimPrefix_m873711498 (ZipNameTransform_t1757759150 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipNameTransform::MakeValidName(System.String,System.Char)
extern "C"  String_t* ZipNameTransform_MakeValidName_m511210248 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Il2CppChar ___replacement1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipNameTransform::IsValidName(System.String,System.Boolean)
extern "C"  bool ZipNameTransform_IsValidName_m2163635391 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___relaxed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipNameTransform::IsValidName(System.String)
extern "C"  bool ZipNameTransform_IsValidName_m3964125950 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
