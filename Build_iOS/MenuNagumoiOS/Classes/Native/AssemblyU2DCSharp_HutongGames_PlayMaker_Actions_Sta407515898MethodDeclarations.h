﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StartLocationServiceUpdates
struct StartLocationServiceUpdates_t407515898;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::.ctor()
extern "C"  void StartLocationServiceUpdates__ctor_m3095830908 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::Reset()
extern "C"  void StartLocationServiceUpdates_Reset_m742263849 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::OnEnter()
extern "C"  void StartLocationServiceUpdates_OnEnter_m2367862355 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::OnUpdate()
extern "C"  void StartLocationServiceUpdates_OnUpdate_m3817815632 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
