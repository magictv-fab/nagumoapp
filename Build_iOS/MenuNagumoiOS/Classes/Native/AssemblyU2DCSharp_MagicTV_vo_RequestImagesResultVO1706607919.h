﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.RequestImagesItemVO[]
struct RequestImagesItemVOU5BU5D_t245365736;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.RequestImagesResultVO
struct  RequestImagesResultVO_t1706607919  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.RequestImagesResultVO::hash
	String_t* ___hash_0;
	// MagicTV.vo.RequestImagesItemVO[] MagicTV.vo.RequestImagesResultVO::imagens
	RequestImagesItemVOU5BU5D_t245365736* ___imagens_1;

public:
	inline static int32_t get_offset_of_hash_0() { return static_cast<int32_t>(offsetof(RequestImagesResultVO_t1706607919, ___hash_0)); }
	inline String_t* get_hash_0() const { return ___hash_0; }
	inline String_t** get_address_of_hash_0() { return &___hash_0; }
	inline void set_hash_0(String_t* value)
	{
		___hash_0 = value;
		Il2CppCodeGenWriteBarrier(&___hash_0, value);
	}

	inline static int32_t get_offset_of_imagens_1() { return static_cast<int32_t>(offsetof(RequestImagesResultVO_t1706607919, ___imagens_1)); }
	inline RequestImagesItemVOU5BU5D_t245365736* get_imagens_1() const { return ___imagens_1; }
	inline RequestImagesItemVOU5BU5D_t245365736** get_address_of_imagens_1() { return &___imagens_1; }
	inline void set_imagens_1(RequestImagesItemVOU5BU5D_t245365736* value)
	{
		___imagens_1 = value;
		Il2CppCodeGenWriteBarrier(&___imagens_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
