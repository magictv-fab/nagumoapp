﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM
struct OfertasManagerCRM_t3100875987;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertasManagerCRM::.ctor()
extern "C"  void OfertasManagerCRM__ctor_m903335416 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::.cctor()
extern "C"  void OfertasManagerCRM__cctor_m1751497909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::Start()
extern "C"  void OfertasManagerCRM_Start_m4145440504 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::ShowNumberSelected(System.Int32)
extern "C"  void OfertasManagerCRM_ShowNumberSelected_m163063806 (OfertasManagerCRM_t3100875987 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::InstantiateNextItemDePor()
extern "C"  void OfertasManagerCRM_InstantiateNextItemDePor_m804875918 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::InstantiateNextItemPreco2()
extern "C"  void OfertasManagerCRM_InstantiateNextItemPreco2_m2157900649 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::SendStatus(System.Int32)
extern "C"  void OfertasManagerCRM_SendStatus_m2040614775 (OfertasManagerCRM_t3100875987 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::BtnAccept()
extern "C"  void OfertasManagerCRM_BtnAccept_m3459283482 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::BtnRecuse()
extern "C"  void OfertasManagerCRM_BtnRecuse_m517179465 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * OfertasManagerCRM_ISendStatus_m3782149526 (OfertasManagerCRM_t3100875987 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM::ILoadOfertas()
extern "C"  Il2CppObject * OfertasManagerCRM_ILoadOfertas_m15259575 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::Update()
extern "C"  void OfertasManagerCRM_Update_m3960456213 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::LoadMoreItens()
extern "C"  void OfertasManagerCRM_LoadMoreItens_m1405981786 (OfertasManagerCRM_t3100875987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM::GetDateString(System.String)
extern "C"  String_t* OfertasManagerCRM_GetDateString_m3653705268 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM::GetDateTimeString(System.String)
extern "C"  String_t* OfertasManagerCRM_GetDateTimeString_m1795869543 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM::ILoadImgs(System.Int32)
extern "C"  Il2CppObject * OfertasManagerCRM_ILoadImgs_m2358372750 (OfertasManagerCRM_t3100875987 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM::LoadLoadeds(System.Int32)
extern "C"  Il2CppObject * OfertasManagerCRM_LoadLoadeds_m3488267575 (OfertasManagerCRM_t3100875987 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM::PopUp(System.String)
extern "C"  void OfertasManagerCRM_PopUp_m3758719328 (OfertasManagerCRM_t3100875987 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
