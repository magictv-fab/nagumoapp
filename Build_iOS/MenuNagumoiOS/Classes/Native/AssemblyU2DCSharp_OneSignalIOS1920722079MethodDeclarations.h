﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignalIOS
struct OneSignalIOS_t1920722079;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t405523272;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2701878760;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// OSPermissionSubscriptionState
struct OSPermissionSubscriptionState_t4252985057;
// OSPermissionStateChanges
struct OSPermissionStateChanges_t1134260069;
// OSEmailSubscriptionStateChanges
struct OSEmailSubscriptionStateChanges_t721307367;
// OSSubscriptionStateChanges
struct OSSubscriptionStateChanges_t52612275;
// OSPermissionState
struct OSPermissionState_t1777959294;
// System.Object
struct Il2CppObject;
// OSSubscriptionState
struct OSSubscriptionState_t1688362992;
// OSEmailSubscriptionState
struct OSEmailSubscriptionState_t2956802108;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_OneSignal_OSInFocusDisplayOption2441910921.h"
#include "AssemblyU2DCSharp_OneSignal_LOG_LEVEL343241288.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void OneSignalIOS::.ctor(System.String,System.String,System.Boolean,System.Boolean,OneSignal/OSInFocusDisplayOption,OneSignal/LOG_LEVEL,OneSignal/LOG_LEVEL,System.Boolean)
extern "C"  void OneSignalIOS__ctor_m1418977338 (OneSignalIOS_t1920722079 * __this, String_t* ___gameObjectName0, String_t* ___appId1, bool ___autoPrompt2, bool ___inAppLaunchURLs3, int32_t ___displayOption4, int32_t ___logLevel5, int32_t ___visualLevel6, bool ___requiresUserPrivacyConsent7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_init(System.String,System.String,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void OneSignalIOS__init_m1801590491 (Il2CppObject * __this /* static, unused */, String_t* ___listenerName0, String_t* ___appId1, bool ___autoPrompt2, bool ___inAppLaunchURLs3, int32_t ___displayOption4, int32_t ___logLevel5, int32_t ___visualLogLevel6, bool ___requiresUserPrivacyConsent7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_registerForPushNotifications()
extern "C"  void OneSignalIOS__registerForPushNotifications_m80091809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_sendTag(System.String,System.String)
extern "C"  void OneSignalIOS__sendTag_m1123202211 (Il2CppObject * __this /* static, unused */, String_t* ___tagName0, String_t* ___tagValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_sendTags(System.String)
extern "C"  void OneSignalIOS__sendTags_m1267504776 (Il2CppObject * __this /* static, unused */, String_t* ___tags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_getTags()
extern "C"  void OneSignalIOS__getTags_m969267352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_deleteTag(System.String)
extern "C"  void OneSignalIOS__deleteTag_m2142904842 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_deleteTags(System.String)
extern "C"  void OneSignalIOS__deleteTags_m1082088837 (Il2CppObject * __this /* static, unused */, String_t* ___keys0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_idsAvailable()
extern "C"  void OneSignalIOS__idsAvailable_m1744702954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setSubscription(System.Boolean)
extern "C"  void OneSignalIOS__setSubscription_m2416346655 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_postNotification(System.String)
extern "C"  void OneSignalIOS__postNotification_m2475944798 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_syncHashedEmail(System.String)
extern "C"  void OneSignalIOS__syncHashedEmail_m828437381 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_promptLocation()
extern "C"  void OneSignalIOS__promptLocation_m2229568562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setInFocusDisplayType(System.Int32)
extern "C"  void OneSignalIOS__setInFocusDisplayType_m997028037 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_promptForPushNotificationsWithUserResponse()
extern "C"  void OneSignalIOS__promptForPushNotificationsWithUserResponse_m2772914004 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_addPermissionObserver()
extern "C"  void OneSignalIOS__addPermissionObserver_m592484495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_removePermissionObserver()
extern "C"  void OneSignalIOS__removePermissionObserver_m1511387746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_addSubscriptionObserver()
extern "C"  void OneSignalIOS__addSubscriptionObserver_m2507359261 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_removeSubscriptionObserver()
extern "C"  void OneSignalIOS__removeSubscriptionObserver_m810120496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_addEmailSubscriptionObserver()
extern "C"  void OneSignalIOS__addEmailSubscriptionObserver_m2550647527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_removeEmailSubscriptionObserver()
extern "C"  void OneSignalIOS__removeEmailSubscriptionObserver_m282348980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OneSignalIOS::_getPermissionSubscriptionState()
extern "C"  String_t* OneSignalIOS__getPermissionSubscriptionState_m1105864485 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setEmail(System.String,System.String)
extern "C"  void OneSignalIOS__setEmail_m4114459339 (Il2CppObject * __this /* static, unused */, String_t* ___email0, String_t* ___emailAuthCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setUnauthenticatedEmail(System.String)
extern "C"  void OneSignalIOS__setUnauthenticatedEmail_m2671511121 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_logoutEmail()
extern "C"  void OneSignalIOS__logoutEmail_m3095881595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setOneSignalLogLevel(System.Int32,System.Int32)
extern "C"  void OneSignalIOS__setOneSignalLogLevel_m1907862881 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, int32_t ___visualLogLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_userDidProvideConsent(System.Boolean)
extern "C"  void OneSignalIOS__userDidProvideConsent_m896620941 (Il2CppObject * __this /* static, unused */, bool ___consent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OneSignalIOS::_userProvidedConsent()
extern "C"  bool OneSignalIOS__userProvidedConsent_m1197607657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setRequiresUserPrivacyConsent(System.Boolean)
extern "C"  void OneSignalIOS__setRequiresUserPrivacyConsent_m471335917 (Il2CppObject * __this /* static, unused */, bool ___required0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::_setLocationShared(System.Boolean)
extern "C"  void OneSignalIOS__setLocationShared_m2584626428 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetLocationShared(System.Boolean)
extern "C"  void OneSignalIOS_SetLocationShared_m3190913517 (OneSignalIOS_t1920722079 * __this, bool ___shared0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::RegisterForPushNotifications()
extern "C"  void OneSignalIOS_RegisterForPushNotifications_m3520295760 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SendTag(System.String,System.String)
extern "C"  void OneSignalIOS_SendTag_m2684390930 (OneSignalIOS_t1920722079 * __this, String_t* ___tagName0, String_t* ___tagValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SendTags(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void OneSignalIOS_SendTags_m1331188389 (OneSignalIOS_t1920722079 * __this, Il2CppObject* ___tags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::GetTags()
extern "C"  void OneSignalIOS_GetTags_m2456129673 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::DeleteTag(System.String)
extern "C"  void OneSignalIOS_DeleteTag_m2502388217 (OneSignalIOS_t1920722079 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::DeleteTags(System.Collections.Generic.IList`1<System.String>)
extern "C"  void OneSignalIOS_DeleteTags_m894797285 (OneSignalIOS_t1920722079 * __this, Il2CppObject* ___keys0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::IdsAvailable()
extern "C"  void OneSignalIOS_IdsAvailable_m2094572697 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetSubscription(System.Boolean)
extern "C"  void OneSignalIOS_SetSubscription_m1688486736 (OneSignalIOS_t1920722079 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::PostNotification(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void OneSignalIOS_PostNotification_m3783225010 (OneSignalIOS_t1920722079 * __this, Dictionary_2_t696267445 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SyncHashedEmail(System.String)
extern "C"  void OneSignalIOS_SyncHashedEmail_m4268641332 (OneSignalIOS_t1920722079 * __this, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::PromptLocation()
extern "C"  void OneSignalIOS_PromptLocation_m3446942497 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetLogLevel(OneSignal/LOG_LEVEL,OneSignal/LOG_LEVEL)
extern "C"  void OneSignalIOS_SetLogLevel_m3755391154 (OneSignalIOS_t1920722079 * __this, int32_t ___logLevel0, int32_t ___visualLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetInFocusDisplaying(OneSignal/OSInFocusDisplayOption)
extern "C"  void OneSignalIOS_SetInFocusDisplaying_m2309690352 (OneSignalIOS_t1920722079 * __this, int32_t ___display0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::promptForPushNotificationsWithUserResponse()
extern "C"  void OneSignalIOS_promptForPushNotificationsWithUserResponse_m1319466403 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::addPermissionObserver()
extern "C"  void OneSignalIOS_addPermissionObserver_m812007136 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::removePermissionObserver()
extern "C"  void OneSignalIOS_removePermissionObserver_m75193969 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::addSubscriptionObserver()
extern "C"  void OneSignalIOS_addSubscriptionObserver_m3015219758 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::removeSubscriptionObserver()
extern "C"  void OneSignalIOS_removeSubscriptionObserver_m3607370111 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::addEmailSubscriptionObserver()
extern "C"  void OneSignalIOS_addEmailSubscriptionObserver_m2058000246 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::removeEmailSubscriptionObserver()
extern "C"  void OneSignalIOS_removeEmailSubscriptionObserver_m4025418437 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetEmail(System.String,System.String)
extern "C"  void OneSignalIOS_SetEmail_m971702076 (OneSignalIOS_t1920722079 * __this, String_t* ___email0, String_t* ___emailAuthCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetEmail(System.String)
extern "C"  void OneSignalIOS_SetEmail_m3714455296 (OneSignalIOS_t1920722079 * __this, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::LogoutEmail()
extern "C"  void OneSignalIOS_LogoutEmail_m3384262380 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::UserDidProvideConsent(System.Boolean)
extern "C"  void OneSignalIOS_UserDidProvideConsent_m2048830974 (OneSignalIOS_t1920722079 * __this, bool ___consent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OneSignalIOS::UserProvidedConsent()
extern "C"  bool OneSignalIOS_UserProvidedConsent_m3459756402 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignalIOS::SetRequiresUserPrivacyConsent(System.Boolean)
extern "C"  void OneSignalIOS_SetRequiresUserPrivacyConsent_m1248221022 (OneSignalIOS_t1920722079 * __this, bool ___required0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSPermissionSubscriptionState OneSignalIOS::getPermissionSubscriptionState()
extern "C"  OSPermissionSubscriptionState_t4252985057 * OneSignalIOS_getPermissionSubscriptionState_m2258322697 (OneSignalIOS_t1920722079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSPermissionStateChanges OneSignalIOS::parseOSPermissionStateChanges(System.String)
extern "C"  OSPermissionStateChanges_t1134260069 * OneSignalIOS_parseOSPermissionStateChanges_m3319413768 (OneSignalIOS_t1920722079 * __this, String_t* ___jsonStat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSEmailSubscriptionStateChanges OneSignalIOS::parseOSEmailSubscriptionStateChanges(System.String)
extern "C"  OSEmailSubscriptionStateChanges_t721307367 * OneSignalIOS_parseOSEmailSubscriptionStateChanges_m3124457050 (OneSignalIOS_t1920722079 * __this, String_t* ___jsonState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSSubscriptionStateChanges OneSignalIOS::parseOSSubscriptionStateChanges(System.String)
extern "C"  OSSubscriptionStateChanges_t52612275 * OneSignalIOS_parseOSSubscriptionStateChanges_m161334828 (OneSignalIOS_t1920722079 * __this, String_t* ___jsonStat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSPermissionState OneSignalIOS::parseOSPermissionState(System.Object)
extern "C"  OSPermissionState_t1777959294 * OneSignalIOS_parseOSPermissionState_m381824844 (OneSignalIOS_t1920722079 * __this, Il2CppObject * ___stateDict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSSubscriptionState OneSignalIOS::parseOSSubscriptionState(System.Object)
extern "C"  OSSubscriptionState_t1688362992 * OneSignalIOS_parseOSSubscriptionState_m397630604 (OneSignalIOS_t1920722079 * __this, Il2CppObject * ___stateDict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSEmailSubscriptionState OneSignalIOS::parseOSEmailSubscriptionState(System.Object)
extern "C"  OSEmailSubscriptionState_t2956802108 * OneSignalIOS_parseOSEmailSubscriptionState_m3590255020 (OneSignalIOS_t1920722079 * __this, Il2CppObject * ___stateDict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
