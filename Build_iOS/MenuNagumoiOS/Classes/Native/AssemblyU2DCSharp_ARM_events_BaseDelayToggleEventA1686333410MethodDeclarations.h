﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.BaseDelayToggleEventAbstract
struct BaseDelayToggleEventAbstract_t1686333410;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.events.BaseDelayToggleEventAbstract::.ctor()
extern "C"  void BaseDelayToggleEventAbstract__ctor_m2681708050 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.events.BaseDelayToggleEventAbstract::getUniqueName()
extern "C"  String_t* BaseDelayToggleEventAbstract_getUniqueName_m705089253 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::turnOn()
extern "C"  void BaseDelayToggleEventAbstract_turnOn_m2002249934 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::turnOff()
extern "C"  void BaseDelayToggleEventAbstract_turnOff_m1940027074 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::ResetOnOff()
extern "C"  void BaseDelayToggleEventAbstract_ResetOnOff_m1138631411 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.events.BaseDelayToggleEventAbstract::isActive()
extern "C"  bool BaseDelayToggleEventAbstract_isActive_m1557340654 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARM.events.BaseDelayToggleEventAbstract::getGameObjectReference()
extern "C"  GameObject_t3674682005 * BaseDelayToggleEventAbstract_getGameObjectReference_m3178783425 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::_turnToOn()
extern "C"  void BaseDelayToggleEventAbstract__turnToOn_m2413902726 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::_turnToOff()
extern "C"  void BaseDelayToggleEventAbstract__turnToOff_m1816361738 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::configByFloatList(System.Collections.Generic.List`1<System.Single>)
extern "C"  void BaseDelayToggleEventAbstract_configByFloatList_m3320486684 (BaseDelayToggleEventAbstract_t1686333410 * __this, List_1_t1365137228 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::Update()
extern "C"  void BaseDelayToggleEventAbstract_Update_m3255433019 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::configByString(System.String)
extern "C"  void BaseDelayToggleEventAbstract_configByString_m1118964422 (BaseDelayToggleEventAbstract_t1686333410 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.BaseDelayToggleEventAbstract::Reset()
extern "C"  void BaseDelayToggleEventAbstract_Reset_m328140991 (BaseDelayToggleEventAbstract_t1686333410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
