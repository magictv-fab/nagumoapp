﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SqliteDatabase
struct SqliteDatabase_t1917310407;
// DeviceFileInfoDAO/OnEvent
struct OnEvent_t2458123614;
// System.Func`2<MagicTV.vo.BundleVO,System.String>
struct Func_2_t1370182125;
// System.Func`2<MagicTV.vo.MetadataVO,System.String>
struct Func_2_t3445325228;
// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<System.String>>
struct Func_2_t376127786;
// System.Func`2<MagicTV.vo.FileVO,System.String>
struct Func_2_t4075771083;
// System.Func`2<MagicTV.vo.FileVO,System.Collections.Generic.IEnumerable`1<System.String>>
struct Func_2_t3081716744;
// System.Func`2<MagicTV.vo.UrlInfoVO,System.String>
struct Func_2_t888805570;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceFileInfoDAO
struct  DeviceFileInfoDAO_t152212114  : public GeneralComponentsAbstract_t3900398046
{
public:
	// SqliteDatabase DeviceFileInfoDAO::SQLiteDB
	SqliteDatabase_t1917310407 * ___SQLiteDB_4;
	// DeviceFileInfoDAO/OnEvent DeviceFileInfoDAO::_onClick
	OnEvent_t2458123614 * ____onClick_5;
	// System.Int32 DeviceFileInfoDAO::ativo
	int32_t ___ativo_6;
	// System.Boolean DeviceFileInfoDAO::showDebug
	bool ___showDebug_7;

public:
	inline static int32_t get_offset_of_SQLiteDB_4() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114, ___SQLiteDB_4)); }
	inline SqliteDatabase_t1917310407 * get_SQLiteDB_4() const { return ___SQLiteDB_4; }
	inline SqliteDatabase_t1917310407 ** get_address_of_SQLiteDB_4() { return &___SQLiteDB_4; }
	inline void set_SQLiteDB_4(SqliteDatabase_t1917310407 * value)
	{
		___SQLiteDB_4 = value;
		Il2CppCodeGenWriteBarrier(&___SQLiteDB_4, value);
	}

	inline static int32_t get_offset_of__onClick_5() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114, ____onClick_5)); }
	inline OnEvent_t2458123614 * get__onClick_5() const { return ____onClick_5; }
	inline OnEvent_t2458123614 ** get_address_of__onClick_5() { return &____onClick_5; }
	inline void set__onClick_5(OnEvent_t2458123614 * value)
	{
		____onClick_5 = value;
		Il2CppCodeGenWriteBarrier(&____onClick_5, value);
	}

	inline static int32_t get_offset_of_ativo_6() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114, ___ativo_6)); }
	inline int32_t get_ativo_6() const { return ___ativo_6; }
	inline int32_t* get_address_of_ativo_6() { return &___ativo_6; }
	inline void set_ativo_6(int32_t value)
	{
		___ativo_6 = value;
	}

	inline static int32_t get_offset_of_showDebug_7() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114, ___showDebug_7)); }
	inline bool get_showDebug_7() const { return ___showDebug_7; }
	inline bool* get_address_of_showDebug_7() { return &___showDebug_7; }
	inline void set_showDebug_7(bool value)
	{
		___showDebug_7 = value;
	}
};

struct DeviceFileInfoDAO_t152212114_StaticFields
{
public:
	// System.Func`2<MagicTV.vo.BundleVO,System.String> DeviceFileInfoDAO::<>f__am$cache4
	Func_2_t1370182125 * ___U3CU3Ef__amU24cache4_8;
	// System.Func`2<MagicTV.vo.MetadataVO,System.String> DeviceFileInfoDAO::<>f__am$cache5
	Func_2_t3445325228 * ___U3CU3Ef__amU24cache5_9;
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<System.String>> DeviceFileInfoDAO::<>f__am$cache6
	Func_2_t376127786 * ___U3CU3Ef__amU24cache6_10;
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<System.String>> DeviceFileInfoDAO::<>f__am$cache7
	Func_2_t376127786 * ___U3CU3Ef__amU24cache7_11;
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<System.String>> DeviceFileInfoDAO::<>f__am$cache8
	Func_2_t376127786 * ___U3CU3Ef__amU24cache8_12;
	// System.Func`2<MagicTV.vo.FileVO,System.String> DeviceFileInfoDAO::<>f__am$cache9
	Func_2_t4075771083 * ___U3CU3Ef__amU24cache9_13;
	// System.Func`2<MagicTV.vo.FileVO,System.Collections.Generic.IEnumerable`1<System.String>> DeviceFileInfoDAO::<>f__am$cacheA
	Func_2_t3081716744 * ___U3CU3Ef__amU24cacheA_14;
	// System.Func`2<MagicTV.vo.MetadataVO,System.String> DeviceFileInfoDAO::<>f__am$cacheB
	Func_2_t3445325228 * ___U3CU3Ef__amU24cacheB_15;
	// System.Func`2<MagicTV.vo.UrlInfoVO,System.String> DeviceFileInfoDAO::<>f__am$cacheC
	Func_2_t888805570 * ___U3CU3Ef__amU24cacheC_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline Func_2_t1370182125 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline Func_2_t1370182125 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(Func_2_t1370182125 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_9() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cache5_9)); }
	inline Func_2_t3445325228 * get_U3CU3Ef__amU24cache5_9() const { return ___U3CU3Ef__amU24cache5_9; }
	inline Func_2_t3445325228 ** get_address_of_U3CU3Ef__amU24cache5_9() { return &___U3CU3Ef__amU24cache5_9; }
	inline void set_U3CU3Ef__amU24cache5_9(Func_2_t3445325228 * value)
	{
		___U3CU3Ef__amU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_10() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cache6_10)); }
	inline Func_2_t376127786 * get_U3CU3Ef__amU24cache6_10() const { return ___U3CU3Ef__amU24cache6_10; }
	inline Func_2_t376127786 ** get_address_of_U3CU3Ef__amU24cache6_10() { return &___U3CU3Ef__amU24cache6_10; }
	inline void set_U3CU3Ef__amU24cache6_10(Func_2_t376127786 * value)
	{
		___U3CU3Ef__amU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_11() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cache7_11)); }
	inline Func_2_t376127786 * get_U3CU3Ef__amU24cache7_11() const { return ___U3CU3Ef__amU24cache7_11; }
	inline Func_2_t376127786 ** get_address_of_U3CU3Ef__amU24cache7_11() { return &___U3CU3Ef__amU24cache7_11; }
	inline void set_U3CU3Ef__amU24cache7_11(Func_2_t376127786 * value)
	{
		___U3CU3Ef__amU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_12() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cache8_12)); }
	inline Func_2_t376127786 * get_U3CU3Ef__amU24cache8_12() const { return ___U3CU3Ef__amU24cache8_12; }
	inline Func_2_t376127786 ** get_address_of_U3CU3Ef__amU24cache8_12() { return &___U3CU3Ef__amU24cache8_12; }
	inline void set_U3CU3Ef__amU24cache8_12(Func_2_t376127786 * value)
	{
		___U3CU3Ef__amU24cache8_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_13() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cache9_13)); }
	inline Func_2_t4075771083 * get_U3CU3Ef__amU24cache9_13() const { return ___U3CU3Ef__amU24cache9_13; }
	inline Func_2_t4075771083 ** get_address_of_U3CU3Ef__amU24cache9_13() { return &___U3CU3Ef__amU24cache9_13; }
	inline void set_U3CU3Ef__amU24cache9_13(Func_2_t4075771083 * value)
	{
		___U3CU3Ef__amU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_14() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cacheA_14)); }
	inline Func_2_t3081716744 * get_U3CU3Ef__amU24cacheA_14() const { return ___U3CU3Ef__amU24cacheA_14; }
	inline Func_2_t3081716744 ** get_address_of_U3CU3Ef__amU24cacheA_14() { return &___U3CU3Ef__amU24cacheA_14; }
	inline void set_U3CU3Ef__amU24cacheA_14(Func_2_t3081716744 * value)
	{
		___U3CU3Ef__amU24cacheA_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Func_2_t3445325228 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Func_2_t3445325228 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Func_2_t3445325228 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_16() { return static_cast<int32_t>(offsetof(DeviceFileInfoDAO_t152212114_StaticFields, ___U3CU3Ef__amU24cacheC_16)); }
	inline Func_2_t888805570 * get_U3CU3Ef__amU24cacheC_16() const { return ___U3CU3Ef__amU24cacheC_16; }
	inline Func_2_t888805570 ** get_address_of_U3CU3Ef__amU24cacheC_16() { return &___U3CU3Ef__amU24cacheC_16; }
	inline void set_U3CU3Ef__amU24cacheC_16(Func_2_t888805570 * value)
	{
		___U3CU3Ef__amU24cacheC_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
