﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2081037391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertFloatToInt
struct  ConvertFloatToInt_t1062086873  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertFloatToInt::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertFloatToInt::intVariable
	FsmInt_t1596138449 * ___intVariable_10;
	// HutongGames.PlayMaker.Actions.ConvertFloatToInt/FloatRounding HutongGames.PlayMaker.Actions.ConvertFloatToInt::rounding
	int32_t ___rounding_11;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertFloatToInt::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_floatVariable_9() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1062086873, ___floatVariable_9)); }
	inline FsmFloat_t2134102846 * get_floatVariable_9() const { return ___floatVariable_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_9() { return &___floatVariable_9; }
	inline void set_floatVariable_9(FsmFloat_t2134102846 * value)
	{
		___floatVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_9, value);
	}

	inline static int32_t get_offset_of_intVariable_10() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1062086873, ___intVariable_10)); }
	inline FsmInt_t1596138449 * get_intVariable_10() const { return ___intVariable_10; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_10() { return &___intVariable_10; }
	inline void set_intVariable_10(FsmInt_t1596138449 * value)
	{
		___intVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_10, value);
	}

	inline static int32_t get_offset_of_rounding_11() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1062086873, ___rounding_11)); }
	inline int32_t get_rounding_11() const { return ___rounding_11; }
	inline int32_t* get_address_of_rounding_11() { return &___rounding_11; }
	inline void set_rounding_11(int32_t value)
	{
		___rounding_11 = value;
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1062086873, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
