﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Maxicode.Internal.Decoder
struct Decoder_t3124105626;
// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Void ZXing.Maxicode.Internal.Decoder::.ctor()
extern "C"  void Decoder__ctor_m2422262918 (Decoder_t3124105626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.Maxicode.Internal.Decoder::decode(ZXing.Common.BitMatrix,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  DecoderResult_t3752650303 * Decoder_decode_m3653216683 (Decoder_t3124105626 * __this, BitMatrix_t1058711404 * ___bits0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Maxicode.Internal.Decoder::correctErrors(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool Decoder_correctErrors_m1070096316 (Decoder_t3124105626 * __this, ByteU5BU5D_t4260760469* ___codewordBytes0, int32_t ___start1, int32_t ___dataCodewords2, int32_t ___ecCodewords3, int32_t ___mode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
