﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebCamBackground
struct WebCamBackground_t1986997129;

#include "codegen/il2cpp-codegen.h"

// System.Void WebCamBackground::.ctor()
extern "C"  void WebCamBackground__ctor_m1343481138 (WebCamBackground_t1986997129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamBackground::Start()
extern "C"  void WebCamBackground_Start_m290618930 (WebCamBackground_t1986997129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamBackground::OnApplicationQuit()
extern "C"  void WebCamBackground_OnApplicationQuit_m2198083504 (WebCamBackground_t1986997129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamBackground::OnDestroy()
extern "C"  void WebCamBackground_OnDestroy_m1009504555 (WebCamBackground_t1986997129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamBackground::OnDisable()
extern "C"  void WebCamBackground_OnDisable_m3126939801 (WebCamBackground_t1986997129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
