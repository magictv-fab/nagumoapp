﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetColorRGBA
struct GetColorRGBA_t1254146697;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::.ctor()
extern "C"  void GetColorRGBA__ctor_m1405466109 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::Reset()
extern "C"  void GetColorRGBA_Reset_m3346866346 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::OnEnter()
extern "C"  void GetColorRGBA_OnEnter_m1424928404 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::OnUpdate()
extern "C"  void GetColorRGBA_OnUpdate_m356666927 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::DoGetColorRGBA()
extern "C"  void GetColorRGBA_DoGetColorRGBA_m1286975923 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
