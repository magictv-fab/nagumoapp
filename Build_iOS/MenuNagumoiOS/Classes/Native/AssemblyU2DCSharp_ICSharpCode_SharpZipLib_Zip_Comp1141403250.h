﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int16[]
struct Int16U5BU5D_t801762735;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t1141403250;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct  InflaterHuffmanTree_t1141403250  : public Il2CppObject
{
public:
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::tree
	Int16U5BU5D_t801762735* ___tree_1;

public:
	inline static int32_t get_offset_of_tree_1() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t1141403250, ___tree_1)); }
	inline Int16U5BU5D_t801762735* get_tree_1() const { return ___tree_1; }
	inline Int16U5BU5D_t801762735** get_address_of_tree_1() { return &___tree_1; }
	inline void set_tree_1(Int16U5BU5D_t801762735* value)
	{
		___tree_1 = value;
		Il2CppCodeGenWriteBarrier(&___tree_1, value);
	}
};

struct InflaterHuffmanTree_t1141403250_StaticFields
{
public:
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defLitLenTree
	InflaterHuffmanTree_t1141403250 * ___defLitLenTree_2;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defDistTree
	InflaterHuffmanTree_t1141403250 * ___defDistTree_3;

public:
	inline static int32_t get_offset_of_defLitLenTree_2() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t1141403250_StaticFields, ___defLitLenTree_2)); }
	inline InflaterHuffmanTree_t1141403250 * get_defLitLenTree_2() const { return ___defLitLenTree_2; }
	inline InflaterHuffmanTree_t1141403250 ** get_address_of_defLitLenTree_2() { return &___defLitLenTree_2; }
	inline void set_defLitLenTree_2(InflaterHuffmanTree_t1141403250 * value)
	{
		___defLitLenTree_2 = value;
		Il2CppCodeGenWriteBarrier(&___defLitLenTree_2, value);
	}

	inline static int32_t get_offset_of_defDistTree_3() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t1141403250_StaticFields, ___defDistTree_3)); }
	inline InflaterHuffmanTree_t1141403250 * get_defDistTree_3() const { return ___defDistTree_3; }
	inline InflaterHuffmanTree_t1141403250 ** get_address_of_defDistTree_3() { return &___defDistTree_3; }
	inline void set_defDistTree_3(InflaterHuffmanTree_t1141403250 * value)
	{
		___defDistTree_3 = value;
		Il2CppCodeGenWriteBarrier(&___defDistTree_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
