﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime
struct GetAnimatorPlayBackTime_t2941148851;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::.ctor()
extern "C"  void GetAnimatorPlayBackTime__ctor_m876104163 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::Reset()
extern "C"  void GetAnimatorPlayBackTime_Reset_m2817504400 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnEnter()
extern "C"  void GetAnimatorPlayBackTime_OnEnter_m3809206522 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnUpdate()
extern "C"  void GetAnimatorPlayBackTime_OnUpdate_m1254844553 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::GetPlayBackTime()
extern "C"  void GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
