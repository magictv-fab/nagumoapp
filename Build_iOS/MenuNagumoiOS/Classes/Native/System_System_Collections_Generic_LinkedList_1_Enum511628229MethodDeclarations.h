﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t3323440965;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enum511628229.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Enumerator__ctor_m21100526_gshared (Enumerator_t511628229 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Enumerator__ctor_m21100526(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t511628229 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Enumerator__ctor_m21100526_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m3775551166_gshared (Enumerator_t511628229 * __this, LinkedList_1_t3323440965 * ___parent0, const MethodInfo* method);
#define Enumerator__ctor_m3775551166(__this, ___parent0, method) ((  void (*) (Enumerator_t511628229 *, LinkedList_1_t3323440965 *, const MethodInfo*))Enumerator__ctor_m3775551166_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3113396643_gshared (Enumerator_t511628229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3113396643(__this, method) ((  Il2CppObject * (*) (Enumerator_t511628229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3113396643_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3278322477_gshared (Enumerator_t511628229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3278322477(__this, method) ((  void (*) (Enumerator_t511628229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3278322477_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m4245765310_gshared (Enumerator_t511628229 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m4245765310(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t511628229 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m4245765310_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C"  void Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m98136233_gshared (Enumerator_t511628229 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m98136233(__this, ___sender0, method) ((  void (*) (Enumerator_t511628229 *, Il2CppObject *, const MethodInfo*))Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m98136233_gshared)(__this, ___sender0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1721921340_gshared (Enumerator_t511628229 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1721921340(__this, method) ((  int32_t (*) (Enumerator_t511628229 *, const MethodInfo*))Enumerator_get_Current_m1721921340_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m211074417_gshared (Enumerator_t511628229 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m211074417(__this, method) ((  bool (*) (Enumerator_t511628229 *, const MethodInfo*))Enumerator_MoveNext_m211074417_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m4050946346_gshared (Enumerator_t511628229 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4050946346(__this, method) ((  void (*) (Enumerator_t511628229 *, const MethodInfo*))Enumerator_Dispose_m4050946346_gshared)(__this, method)
