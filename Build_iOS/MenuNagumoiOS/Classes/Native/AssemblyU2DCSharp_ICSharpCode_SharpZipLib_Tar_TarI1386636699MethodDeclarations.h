﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarInputStream
struct TarInputStream_t1386636699;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Tar.TarInputStream/IEntryFactory
struct IEntryFactory_t441050801;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::.ctor(System.IO.Stream)
extern "C"  void TarInputStream__ctor_m1575940211 (TarInputStream_t1386636699 * __this, Stream_t1561764144 * ___inputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void TarInputStream__ctor_m2275131204 (TarInputStream_t1386636699 * __this, Stream_t1561764144 * ___inputStream0, int32_t ___blockFactor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::get_IsStreamOwner()
extern "C"  bool TarInputStream_get_IsStreamOwner_m2542325414 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::set_IsStreamOwner(System.Boolean)
extern "C"  void TarInputStream_set_IsStreamOwner_m4078207965 (TarInputStream_t1386636699 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::get_CanRead()
extern "C"  bool TarInputStream_get_CanRead_m185179747 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::get_CanSeek()
extern "C"  bool TarInputStream_get_CanSeek_m213934789 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::get_CanWrite()
extern "C"  bool TarInputStream_get_CanWrite_m1968258388 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::get_Length()
extern "C"  int64_t TarInputStream_get_Length_m4016174950 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::get_Position()
extern "C"  int64_t TarInputStream_get_Position_m2884996393 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::set_Position(System.Int64)
extern "C"  void TarInputStream_set_Position_m3776262048 (TarInputStream_t1386636699 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::Flush()
extern "C"  void TarInputStream_Flush_m2515980542 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t TarInputStream_Seek_m273409214 (TarInputStream_t1386636699 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::SetLength(System.Int64)
extern "C"  void TarInputStream_SetLength_m3807549492 (TarInputStream_t1386636699 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void TarInputStream_Write_m4234624688 (TarInputStream_t1386636699 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::WriteByte(System.Byte)
extern "C"  void TarInputStream_WriteByte_m2537792138 (TarInputStream_t1386636699 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarInputStream::ReadByte()
extern "C"  int32_t TarInputStream_ReadByte_m1161501748 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarInputStream_Read_m3471280093 (TarInputStream_t1386636699 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::Close()
extern "C"  void TarInputStream_Close_m4142892786 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::SetEntryFactory(ICSharpCode.SharpZipLib.Tar.TarInputStream/IEntryFactory)
extern "C"  void TarInputStream_SetEntryFactory_m80197987 (TarInputStream_t1386636699 * __this, Il2CppObject * ___factory0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarInputStream::get_RecordSize()
extern "C"  int32_t TarInputStream_get_RecordSize_m3974747633 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarInputStream::GetRecordSize()
extern "C"  int32_t TarInputStream_GetRecordSize_m2794490132 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::get_Available()
extern "C"  int64_t TarInputStream_get_Available_m2767721515 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::Skip(System.Int64)
extern "C"  void TarInputStream_Skip_m2305433593 (TarInputStream_t1386636699 * __this, int64_t ___skipCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::get_IsMarkSupported()
extern "C"  bool TarInputStream_get_IsMarkSupported_m920717300 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::Mark(System.Int32)
extern "C"  void TarInputStream_Mark_m4097037638 (TarInputStream_t1386636699 * __this, int32_t ___markLimit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::Reset()
extern "C"  void TarInputStream_Reset_m78466185 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarInputStream::GetNextEntry()
extern "C"  TarEntry_t762185187 * TarInputStream_GetNextEntry_m2273910759 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::CopyEntryContents(System.IO.Stream)
extern "C"  void TarInputStream_CopyEntryContents_m1963419784 (TarInputStream_t1386636699 * __this, Stream_t1561764144 * ___outputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream::SkipToNextEntry()
extern "C"  void TarInputStream_SkipToNextEntry_m4266638463 (TarInputStream_t1386636699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
