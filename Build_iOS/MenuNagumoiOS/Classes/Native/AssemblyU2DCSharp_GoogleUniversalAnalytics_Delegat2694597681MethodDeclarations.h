﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleUniversalAnalytics/Delegate_escapeString
struct Delegate_escapeString_t2694597681;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GoogleUniversalAnalytics/Delegate_escapeString::.ctor(System.Object,System.IntPtr)
extern "C"  void Delegate_escapeString__ctor_m3706941016 (Delegate_escapeString_t2694597681 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleUniversalAnalytics/Delegate_escapeString::Invoke(System.String)
extern "C"  String_t* Delegate_escapeString_Invoke_m218444621 (Delegate_escapeString_t2694597681 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GoogleUniversalAnalytics/Delegate_escapeString::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Delegate_escapeString_BeginInvoke_m3769694997 (Delegate_escapeString_t2694597681 * __this, String_t* ___s0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleUniversalAnalytics/Delegate_escapeString::EndInvoke(System.IAsyncResult)
extern "C"  String_t* Delegate_escapeString_EndInvoke_m3627742539 (Delegate_escapeString_t2694597681 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
