﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.PositionMedianFilter
struct PositionMedianFilter_t3420889871;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.transform.filters.PositionMedianFilter::.ctor()
extern "C"  void PositionMedianFilter__ctor_m3481342089 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.PositionMedianFilter::get__lastMeanValue()
extern "C"  Vector3_t4282066566  PositionMedianFilter_get__lastMeanValue_m269722677 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::set__lastMeanValue(UnityEngine.Vector3)
extern "C"  void PositionMedianFilter_set__lastMeanValue_m409411658 (PositionMedianFilter_t3420889871 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::Start()
extern "C"  void PositionMedianFilter_Start_m2428479881 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::reset()
extern "C"  void PositionMedianFilter_reset_m3758089046 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::Update()
extern "C"  void PositionMedianFilter_Update_m2274284452 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.PositionMedianFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  PositionMedianFilter__doFilter_m4056590141 (PositionMedianFilter_t3420889871 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.PositionMedianFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  PositionMedianFilter__doFilter_m2748095941 (PositionMedianFilter_t3420889871 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.PositionMedianFilter::get_lastMeanValue()
extern "C"  Vector3_t4282066566  PositionMedianFilter_get_lastMeanValue_m1742602842 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::backToWork()
extern "C"  void PositionMedianFilter_backToWork_m2702345006 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::save(UnityEngine.Vector3)
extern "C"  void PositionMedianFilter_save_m674190113 (PositionMedianFilter_t3420889871 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.PositionMedianFilter::backToSave()
extern "C"  void PositionMedianFilter_backToSave_m2575012506 (PositionMedianFilter_t3420889871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
