﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3689162173;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolNoneTrue
struct  BoolNoneTrue_t654279704  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.Actions.BoolNoneTrue::boolVariables
	FsmBoolU5BU5D_t3689162173* ___boolVariables_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolNoneTrue::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolNoneTrue::storeResult
	FsmBool_t1075959796 * ___storeResult_11;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolNoneTrue::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_boolVariables_9() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t654279704, ___boolVariables_9)); }
	inline FsmBoolU5BU5D_t3689162173* get_boolVariables_9() const { return ___boolVariables_9; }
	inline FsmBoolU5BU5D_t3689162173** get_address_of_boolVariables_9() { return &___boolVariables_9; }
	inline void set_boolVariables_9(FsmBoolU5BU5D_t3689162173* value)
	{
		___boolVariables_9 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariables_9, value);
	}

	inline static int32_t get_offset_of_sendEvent_10() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t654279704, ___sendEvent_10)); }
	inline FsmEvent_t2133468028 * get_sendEvent_10() const { return ___sendEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_10() { return &___sendEvent_10; }
	inline void set_sendEvent_10(FsmEvent_t2133468028 * value)
	{
		___sendEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t654279704, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t654279704, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
