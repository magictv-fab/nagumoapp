﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject
struct  GUILayoutBeginAreaFollowObject_t439899113  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::offsetLeft
	FsmFloat_t2134102846 * ___offsetLeft_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::offsetTop
	FsmFloat_t2134102846 * ___offsetTop_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::width
	FsmFloat_t2134102846 * ___width_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::height
	FsmFloat_t2134102846 * ___height_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::normalized
	FsmBool_t1075959796 * ___normalized_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::style
	FsmString_t952858651 * ___style_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_offsetLeft_10() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___offsetLeft_10)); }
	inline FsmFloat_t2134102846 * get_offsetLeft_10() const { return ___offsetLeft_10; }
	inline FsmFloat_t2134102846 ** get_address_of_offsetLeft_10() { return &___offsetLeft_10; }
	inline void set_offsetLeft_10(FsmFloat_t2134102846 * value)
	{
		___offsetLeft_10 = value;
		Il2CppCodeGenWriteBarrier(&___offsetLeft_10, value);
	}

	inline static int32_t get_offset_of_offsetTop_11() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___offsetTop_11)); }
	inline FsmFloat_t2134102846 * get_offsetTop_11() const { return ___offsetTop_11; }
	inline FsmFloat_t2134102846 ** get_address_of_offsetTop_11() { return &___offsetTop_11; }
	inline void set_offsetTop_11(FsmFloat_t2134102846 * value)
	{
		___offsetTop_11 = value;
		Il2CppCodeGenWriteBarrier(&___offsetTop_11, value);
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___width_12)); }
	inline FsmFloat_t2134102846 * get_width_12() const { return ___width_12; }
	inline FsmFloat_t2134102846 ** get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(FsmFloat_t2134102846 * value)
	{
		___width_12 = value;
		Il2CppCodeGenWriteBarrier(&___width_12, value);
	}

	inline static int32_t get_offset_of_height_13() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___height_13)); }
	inline FsmFloat_t2134102846 * get_height_13() const { return ___height_13; }
	inline FsmFloat_t2134102846 ** get_address_of_height_13() { return &___height_13; }
	inline void set_height_13(FsmFloat_t2134102846 * value)
	{
		___height_13 = value;
		Il2CppCodeGenWriteBarrier(&___height_13, value);
	}

	inline static int32_t get_offset_of_normalized_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___normalized_14)); }
	inline FsmBool_t1075959796 * get_normalized_14() const { return ___normalized_14; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_14() { return &___normalized_14; }
	inline void set_normalized_14(FsmBool_t1075959796 * value)
	{
		___normalized_14 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t439899113, ___style_15)); }
	inline FsmString_t952858651 * get_style_15() const { return ___style_15; }
	inline FsmString_t952858651 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(FsmString_t952858651 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
