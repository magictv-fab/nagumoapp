﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t1829109954;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine
struct DeflaterEngine_t3684739431;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct  Deflater_t2454063301  : public Il2CppObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::level
	int32_t ___level_15;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::noZlibHeaderOrFooter
	bool ___noZlibHeaderOrFooter_16;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::state
	int32_t ___state_17;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::totalOut
	int64_t ___totalOut_18;
	// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending ICSharpCode.SharpZipLib.Zip.Compression.Deflater::pending
	DeflaterPending_t1829109954 * ___pending_19;
	// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine ICSharpCode.SharpZipLib.Zip.Compression.Deflater::engine
	DeflaterEngine_t3684739431 * ___engine_20;

public:
	inline static int32_t get_offset_of_level_15() { return static_cast<int32_t>(offsetof(Deflater_t2454063301, ___level_15)); }
	inline int32_t get_level_15() const { return ___level_15; }
	inline int32_t* get_address_of_level_15() { return &___level_15; }
	inline void set_level_15(int32_t value)
	{
		___level_15 = value;
	}

	inline static int32_t get_offset_of_noZlibHeaderOrFooter_16() { return static_cast<int32_t>(offsetof(Deflater_t2454063301, ___noZlibHeaderOrFooter_16)); }
	inline bool get_noZlibHeaderOrFooter_16() const { return ___noZlibHeaderOrFooter_16; }
	inline bool* get_address_of_noZlibHeaderOrFooter_16() { return &___noZlibHeaderOrFooter_16; }
	inline void set_noZlibHeaderOrFooter_16(bool value)
	{
		___noZlibHeaderOrFooter_16 = value;
	}

	inline static int32_t get_offset_of_state_17() { return static_cast<int32_t>(offsetof(Deflater_t2454063301, ___state_17)); }
	inline int32_t get_state_17() const { return ___state_17; }
	inline int32_t* get_address_of_state_17() { return &___state_17; }
	inline void set_state_17(int32_t value)
	{
		___state_17 = value;
	}

	inline static int32_t get_offset_of_totalOut_18() { return static_cast<int32_t>(offsetof(Deflater_t2454063301, ___totalOut_18)); }
	inline int64_t get_totalOut_18() const { return ___totalOut_18; }
	inline int64_t* get_address_of_totalOut_18() { return &___totalOut_18; }
	inline void set_totalOut_18(int64_t value)
	{
		___totalOut_18 = value;
	}

	inline static int32_t get_offset_of_pending_19() { return static_cast<int32_t>(offsetof(Deflater_t2454063301, ___pending_19)); }
	inline DeflaterPending_t1829109954 * get_pending_19() const { return ___pending_19; }
	inline DeflaterPending_t1829109954 ** get_address_of_pending_19() { return &___pending_19; }
	inline void set_pending_19(DeflaterPending_t1829109954 * value)
	{
		___pending_19 = value;
		Il2CppCodeGenWriteBarrier(&___pending_19, value);
	}

	inline static int32_t get_offset_of_engine_20() { return static_cast<int32_t>(offsetof(Deflater_t2454063301, ___engine_20)); }
	inline DeflaterEngine_t3684739431 * get_engine_20() const { return ___engine_20; }
	inline DeflaterEngine_t3684739431 ** get_address_of_engine_20() { return &___engine_20; }
	inline void set_engine_20(DeflaterEngine_t3684739431 * value)
	{
		___engine_20 = value;
		Il2CppCodeGenWriteBarrier(&___engine_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
