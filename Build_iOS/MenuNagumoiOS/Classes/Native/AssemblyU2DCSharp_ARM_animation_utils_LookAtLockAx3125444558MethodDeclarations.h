﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.utils.LookAtLockAxis
struct LookAtLockAxis_t3125444558;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.animation.utils.LookAtLockAxis::.ctor()
extern "C"  void LookAtLockAxis__ctor_m1963891876 (LookAtLockAxis_t3125444558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.utils.LookAtLockAxis::Start()
extern "C"  void LookAtLockAxis_Start_m911029668 (LookAtLockAxis_t3125444558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.utils.LookAtLockAxis::Update()
extern "C"  void LookAtLockAxis_Update_m2477968105 (LookAtLockAxis_t3125444558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.animation.utils.LookAtLockAxis::normalizeVector3(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  LookAtLockAxis_normalizeVector3_m767402620 (LookAtLockAxis_t3125444558 * __this, Vector3_t4282066566  ___newRotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.animation.utils.LookAtLockAxis::limitAngle(System.Single)
extern "C"  float LookAtLockAxis_limitAngle_m1946764999 (LookAtLockAxis_t3125444558 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
