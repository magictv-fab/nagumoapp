﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract[]
struct TransformPositionAngleFilterAbstractU5BU5D_t4281965263;
// ARM.abstracts.ToggleOnOffAbstract
struct ToggleOnOffAbstract_t832893812;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract
struct  TransformPositionAngleFilterAbstract_t4158746314  : public MonoBehaviour_t667441552
{
public:
	// System.String ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::Alias
	String_t* ___Alias_2;
	// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract[] ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::filters
	TransformPositionAngleFilterAbstractU5BU5D_t4281965263* ___filters_3;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_hasFilter
	bool ____hasFilter_4;
	// ARM.abstracts.ToggleOnOffAbstract ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::toggle
	ToggleOnOffAbstract_t832893812 * ___toggle_5;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_hasToggle
	bool ____hasToggle_6;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_active
	bool ____active_7;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_started
	bool ____started_8;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::byPass
	bool ___byPass_9;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::activeByDefault
	bool ___activeByDefault_10;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_initCalled
	bool ____initCalled_11;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::DebugFiltering
	bool ___DebugFiltering_13;
	// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::filterThisFirstChilds
	bool ___filterThisFirstChilds_14;

public:
	inline static int32_t get_offset_of_Alias_2() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___Alias_2)); }
	inline String_t* get_Alias_2() const { return ___Alias_2; }
	inline String_t** get_address_of_Alias_2() { return &___Alias_2; }
	inline void set_Alias_2(String_t* value)
	{
		___Alias_2 = value;
		Il2CppCodeGenWriteBarrier(&___Alias_2, value);
	}

	inline static int32_t get_offset_of_filters_3() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___filters_3)); }
	inline TransformPositionAngleFilterAbstractU5BU5D_t4281965263* get_filters_3() const { return ___filters_3; }
	inline TransformPositionAngleFilterAbstractU5BU5D_t4281965263** get_address_of_filters_3() { return &___filters_3; }
	inline void set_filters_3(TransformPositionAngleFilterAbstractU5BU5D_t4281965263* value)
	{
		___filters_3 = value;
		Il2CppCodeGenWriteBarrier(&___filters_3, value);
	}

	inline static int32_t get_offset_of__hasFilter_4() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ____hasFilter_4)); }
	inline bool get__hasFilter_4() const { return ____hasFilter_4; }
	inline bool* get_address_of__hasFilter_4() { return &____hasFilter_4; }
	inline void set__hasFilter_4(bool value)
	{
		____hasFilter_4 = value;
	}

	inline static int32_t get_offset_of_toggle_5() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___toggle_5)); }
	inline ToggleOnOffAbstract_t832893812 * get_toggle_5() const { return ___toggle_5; }
	inline ToggleOnOffAbstract_t832893812 ** get_address_of_toggle_5() { return &___toggle_5; }
	inline void set_toggle_5(ToggleOnOffAbstract_t832893812 * value)
	{
		___toggle_5 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_5, value);
	}

	inline static int32_t get_offset_of__hasToggle_6() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ____hasToggle_6)); }
	inline bool get__hasToggle_6() const { return ____hasToggle_6; }
	inline bool* get_address_of__hasToggle_6() { return &____hasToggle_6; }
	inline void set__hasToggle_6(bool value)
	{
		____hasToggle_6 = value;
	}

	inline static int32_t get_offset_of__active_7() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ____active_7)); }
	inline bool get__active_7() const { return ____active_7; }
	inline bool* get_address_of__active_7() { return &____active_7; }
	inline void set__active_7(bool value)
	{
		____active_7 = value;
	}

	inline static int32_t get_offset_of__started_8() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ____started_8)); }
	inline bool get__started_8() const { return ____started_8; }
	inline bool* get_address_of__started_8() { return &____started_8; }
	inline void set__started_8(bool value)
	{
		____started_8 = value;
	}

	inline static int32_t get_offset_of_byPass_9() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___byPass_9)); }
	inline bool get_byPass_9() const { return ___byPass_9; }
	inline bool* get_address_of_byPass_9() { return &___byPass_9; }
	inline void set_byPass_9(bool value)
	{
		___byPass_9 = value;
	}

	inline static int32_t get_offset_of_activeByDefault_10() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___activeByDefault_10)); }
	inline bool get_activeByDefault_10() const { return ___activeByDefault_10; }
	inline bool* get_address_of_activeByDefault_10() { return &___activeByDefault_10; }
	inline void set_activeByDefault_10(bool value)
	{
		___activeByDefault_10 = value;
	}

	inline static int32_t get_offset_of__initCalled_11() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ____initCalled_11)); }
	inline bool get__initCalled_11() const { return ____initCalled_11; }
	inline bool* get_address_of__initCalled_11() { return &____initCalled_11; }
	inline void set__initCalled_11(bool value)
	{
		____initCalled_11 = value;
	}

	inline static int32_t get_offset_of_DebugFiltering_13() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___DebugFiltering_13)); }
	inline bool get_DebugFiltering_13() const { return ___DebugFiltering_13; }
	inline bool* get_address_of_DebugFiltering_13() { return &___DebugFiltering_13; }
	inline void set_DebugFiltering_13(bool value)
	{
		___DebugFiltering_13 = value;
	}

	inline static int32_t get_offset_of_filterThisFirstChilds_14() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314, ___filterThisFirstChilds_14)); }
	inline bool get_filterThisFirstChilds_14() const { return ___filterThisFirstChilds_14; }
	inline bool* get_address_of_filterThisFirstChilds_14() { return &___filterThisFirstChilds_14; }
	inline void set_filterThisFirstChilds_14(bool value)
	{
		___filterThisFirstChilds_14 = value;
	}
};

struct TransformPositionAngleFilterAbstract_t4158746314_StaticFields
{
public:
	// System.Int32 ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_countFilters
	int32_t ____countFilters_12;

public:
	inline static int32_t get_offset_of__countFilters_12() { return static_cast<int32_t>(offsetof(TransformPositionAngleFilterAbstract_t4158746314_StaticFields, ____countFilters_12)); }
	inline int32_t get__countFilters_12() const { return ____countFilters_12; }
	inline int32_t* get_address_of__countFilters_12() { return &____countFilters_12; }
	inline void set__countFilters_12(int32_t value)
	{
		____countFilters_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
