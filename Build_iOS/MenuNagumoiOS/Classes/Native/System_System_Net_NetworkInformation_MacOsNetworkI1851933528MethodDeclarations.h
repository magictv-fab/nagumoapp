﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.MacOsNetworkInterface
struct MacOsNetworkInterface_t1851933528;
// System.String
struct String_t;
// System.Net.NetworkInformation.NetworkInterface[]
struct NetworkInterfaceU5BU5D_t611619240;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void System.Net.NetworkInformation.MacOsNetworkInterface::.ctor(System.String)
extern "C"  void MacOsNetworkInterface__ctor_m3852067350 (MacOsNetworkInterface_t1851933528 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.MacOsNetworkInterface::getifaddrs(System.IntPtr&)
extern "C"  int32_t MacOsNetworkInterface_getifaddrs_m1559031119 (Il2CppObject * __this /* static, unused */, IntPtr_t* ___ifap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.MacOsNetworkInterface::freeifaddrs(System.IntPtr)
extern "C"  void MacOsNetworkInterface_freeifaddrs_m4102543569 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ifap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.NetworkInterface[] System.Net.NetworkInformation.MacOsNetworkInterface::ImplGetAllNetworkInterfaces()
extern "C"  NetworkInterfaceU5BU5D_t611619240* MacOsNetworkInterface_ImplGetAllNetworkInterfaces_m2405010745 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
