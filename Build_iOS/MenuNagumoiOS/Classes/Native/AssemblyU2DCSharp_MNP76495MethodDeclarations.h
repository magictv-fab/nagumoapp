﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNP
struct MNP_t76495;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNP::.ctor()
extern "C"  void MNP__ctor_m20017532 (MNP_t76495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNP::ShowPreloader(System.String,System.String)
extern "C"  void MNP_ShowPreloader_m3247316619 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNP::HidePreloader()
extern "C"  void MNP_HidePreloader_m2174702062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
