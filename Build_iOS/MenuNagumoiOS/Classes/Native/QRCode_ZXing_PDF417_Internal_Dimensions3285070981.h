﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.Dimensions
struct  Dimensions_t3285070981  : public Il2CppObject
{
public:
	// System.Int32 ZXing.PDF417.Internal.Dimensions::minCols
	int32_t ___minCols_0;
	// System.Int32 ZXing.PDF417.Internal.Dimensions::maxCols
	int32_t ___maxCols_1;
	// System.Int32 ZXing.PDF417.Internal.Dimensions::minRows
	int32_t ___minRows_2;
	// System.Int32 ZXing.PDF417.Internal.Dimensions::maxRows
	int32_t ___maxRows_3;

public:
	inline static int32_t get_offset_of_minCols_0() { return static_cast<int32_t>(offsetof(Dimensions_t3285070981, ___minCols_0)); }
	inline int32_t get_minCols_0() const { return ___minCols_0; }
	inline int32_t* get_address_of_minCols_0() { return &___minCols_0; }
	inline void set_minCols_0(int32_t value)
	{
		___minCols_0 = value;
	}

	inline static int32_t get_offset_of_maxCols_1() { return static_cast<int32_t>(offsetof(Dimensions_t3285070981, ___maxCols_1)); }
	inline int32_t get_maxCols_1() const { return ___maxCols_1; }
	inline int32_t* get_address_of_maxCols_1() { return &___maxCols_1; }
	inline void set_maxCols_1(int32_t value)
	{
		___maxCols_1 = value;
	}

	inline static int32_t get_offset_of_minRows_2() { return static_cast<int32_t>(offsetof(Dimensions_t3285070981, ___minRows_2)); }
	inline int32_t get_minRows_2() const { return ___minRows_2; }
	inline int32_t* get_address_of_minRows_2() { return &___minRows_2; }
	inline void set_minRows_2(int32_t value)
	{
		___minRows_2 = value;
	}

	inline static int32_t get_offset_of_maxRows_3() { return static_cast<int32_t>(offsetof(Dimensions_t3285070981, ___maxRows_3)); }
	inline int32_t get_maxRows_3() const { return ___maxRows_3; }
	inline int32_t* get_address_of_maxRows_3() { return &___maxRows_3; }
	inline void set_maxRows_3(int32_t value)
	{
		___maxRows_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
