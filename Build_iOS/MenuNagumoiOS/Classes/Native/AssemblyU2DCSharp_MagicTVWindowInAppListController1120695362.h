﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowInAppListControllerScript
struct  MagicTVWindowInAppListControllerScript_t1120695362  : public GenericWindowControllerScript_t248075822
{
public:
	// System.Action MagicTVWindowInAppListControllerScript::onClickStart
	Action_t3771233898 * ___onClickStart_4;

public:
	inline static int32_t get_offset_of_onClickStart_4() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppListControllerScript_t1120695362, ___onClickStart_4)); }
	inline Action_t3771233898 * get_onClickStart_4() const { return ___onClickStart_4; }
	inline Action_t3771233898 ** get_address_of_onClickStart_4() { return &___onClickStart_4; }
	inline void set_onClickStart_4(Action_t3771233898 * value)
	{
		___onClickStart_4 = value;
		Il2CppCodeGenWriteBarrier(&___onClickStart_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
