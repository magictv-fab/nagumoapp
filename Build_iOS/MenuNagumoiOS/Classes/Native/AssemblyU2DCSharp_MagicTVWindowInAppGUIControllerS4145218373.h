﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LeftMenuControlScript
struct LeftMenuControlScript_t2683180034;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Action
struct Action_t3771233898;
// MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate
struct ScreenShotCompleteDelegate_t509502638;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowInAppGUIControllerScript
struct  MagicTVWindowInAppGUIControllerScript_t4145218373  : public GenericWindowControllerScript_t248075822
{
public:
	// LeftMenuControlScript MagicTVWindowInAppGUIControllerScript::MenuContainer
	LeftMenuControlScript_t2683180034 * ___MenuContainer_4;
	// UnityEngine.GameObject MagicTVWindowInAppGUIControllerScript::CameraButton
	GameObject_t3674682005 * ___CameraButton_5;
	// UnityEngine.GameObject MagicTVWindowInAppGUIControllerScript::InfoButton
	GameObject_t3674682005 * ___InfoButton_6;
	// UnityEngine.GameObject MagicTVWindowInAppGUIControllerScript::UpdateButton
	GameObject_t3674682005 * ___UpdateButton_7;
	// System.Action MagicTVWindowInAppGUIControllerScript::onUpdateClick
	Action_t3771233898 * ___onUpdateClick_8;
	// UnityEngine.GameObject MagicTVWindowInAppGUIControllerScript::canvasObject
	GameObject_t3674682005 * ___canvasObject_9;
	// System.Action MagicTVWindowInAppGUIControllerScript::onClickCamera
	Action_t3771233898 * ___onClickCamera_10;
	// UnityEngine.GameObject MagicTVWindowInAppGUIControllerScript::ExitButton
	GameObject_t3674682005 * ___ExitButton_11;
	// System.Boolean MagicTVWindowInAppGUIControllerScript::isCloseBtnOn
	bool ___isCloseBtnOn_12;
	// System.Boolean MagicTVWindowInAppGUIControllerScript::_upButtonPressed
	bool ____upButtonPressed_13;
	// System.Int32 MagicTVWindowInAppGUIControllerScript::_timeout
	int32_t ____timeout_14;
	// MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate MagicTVWindowInAppGUIControllerScript::onScreenShotComplete
	ScreenShotCompleteDelegate_t509502638 * ___onScreenShotComplete_15;
	// System.Action MagicTVWindowInAppGUIControllerScript::onClickReset
	Action_t3771233898 * ___onClickReset_16;
	// System.Action MagicTVWindowInAppGUIControllerScript::onClickClearCache
	Action_t3771233898 * ___onClickClearCache_17;
	// System.Action MagicTVWindowInAppGUIControllerScript::onClickBack
	Action_t3771233898 * ___onClickBack_18;

public:
	inline static int32_t get_offset_of_MenuContainer_4() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___MenuContainer_4)); }
	inline LeftMenuControlScript_t2683180034 * get_MenuContainer_4() const { return ___MenuContainer_4; }
	inline LeftMenuControlScript_t2683180034 ** get_address_of_MenuContainer_4() { return &___MenuContainer_4; }
	inline void set_MenuContainer_4(LeftMenuControlScript_t2683180034 * value)
	{
		___MenuContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___MenuContainer_4, value);
	}

	inline static int32_t get_offset_of_CameraButton_5() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___CameraButton_5)); }
	inline GameObject_t3674682005 * get_CameraButton_5() const { return ___CameraButton_5; }
	inline GameObject_t3674682005 ** get_address_of_CameraButton_5() { return &___CameraButton_5; }
	inline void set_CameraButton_5(GameObject_t3674682005 * value)
	{
		___CameraButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___CameraButton_5, value);
	}

	inline static int32_t get_offset_of_InfoButton_6() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___InfoButton_6)); }
	inline GameObject_t3674682005 * get_InfoButton_6() const { return ___InfoButton_6; }
	inline GameObject_t3674682005 ** get_address_of_InfoButton_6() { return &___InfoButton_6; }
	inline void set_InfoButton_6(GameObject_t3674682005 * value)
	{
		___InfoButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___InfoButton_6, value);
	}

	inline static int32_t get_offset_of_UpdateButton_7() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___UpdateButton_7)); }
	inline GameObject_t3674682005 * get_UpdateButton_7() const { return ___UpdateButton_7; }
	inline GameObject_t3674682005 ** get_address_of_UpdateButton_7() { return &___UpdateButton_7; }
	inline void set_UpdateButton_7(GameObject_t3674682005 * value)
	{
		___UpdateButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___UpdateButton_7, value);
	}

	inline static int32_t get_offset_of_onUpdateClick_8() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___onUpdateClick_8)); }
	inline Action_t3771233898 * get_onUpdateClick_8() const { return ___onUpdateClick_8; }
	inline Action_t3771233898 ** get_address_of_onUpdateClick_8() { return &___onUpdateClick_8; }
	inline void set_onUpdateClick_8(Action_t3771233898 * value)
	{
		___onUpdateClick_8 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateClick_8, value);
	}

	inline static int32_t get_offset_of_canvasObject_9() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___canvasObject_9)); }
	inline GameObject_t3674682005 * get_canvasObject_9() const { return ___canvasObject_9; }
	inline GameObject_t3674682005 ** get_address_of_canvasObject_9() { return &___canvasObject_9; }
	inline void set_canvasObject_9(GameObject_t3674682005 * value)
	{
		___canvasObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___canvasObject_9, value);
	}

	inline static int32_t get_offset_of_onClickCamera_10() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___onClickCamera_10)); }
	inline Action_t3771233898 * get_onClickCamera_10() const { return ___onClickCamera_10; }
	inline Action_t3771233898 ** get_address_of_onClickCamera_10() { return &___onClickCamera_10; }
	inline void set_onClickCamera_10(Action_t3771233898 * value)
	{
		___onClickCamera_10 = value;
		Il2CppCodeGenWriteBarrier(&___onClickCamera_10, value);
	}

	inline static int32_t get_offset_of_ExitButton_11() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___ExitButton_11)); }
	inline GameObject_t3674682005 * get_ExitButton_11() const { return ___ExitButton_11; }
	inline GameObject_t3674682005 ** get_address_of_ExitButton_11() { return &___ExitButton_11; }
	inline void set_ExitButton_11(GameObject_t3674682005 * value)
	{
		___ExitButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___ExitButton_11, value);
	}

	inline static int32_t get_offset_of_isCloseBtnOn_12() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___isCloseBtnOn_12)); }
	inline bool get_isCloseBtnOn_12() const { return ___isCloseBtnOn_12; }
	inline bool* get_address_of_isCloseBtnOn_12() { return &___isCloseBtnOn_12; }
	inline void set_isCloseBtnOn_12(bool value)
	{
		___isCloseBtnOn_12 = value;
	}

	inline static int32_t get_offset_of__upButtonPressed_13() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ____upButtonPressed_13)); }
	inline bool get__upButtonPressed_13() const { return ____upButtonPressed_13; }
	inline bool* get_address_of__upButtonPressed_13() { return &____upButtonPressed_13; }
	inline void set__upButtonPressed_13(bool value)
	{
		____upButtonPressed_13 = value;
	}

	inline static int32_t get_offset_of__timeout_14() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ____timeout_14)); }
	inline int32_t get__timeout_14() const { return ____timeout_14; }
	inline int32_t* get_address_of__timeout_14() { return &____timeout_14; }
	inline void set__timeout_14(int32_t value)
	{
		____timeout_14 = value;
	}

	inline static int32_t get_offset_of_onScreenShotComplete_15() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___onScreenShotComplete_15)); }
	inline ScreenShotCompleteDelegate_t509502638 * get_onScreenShotComplete_15() const { return ___onScreenShotComplete_15; }
	inline ScreenShotCompleteDelegate_t509502638 ** get_address_of_onScreenShotComplete_15() { return &___onScreenShotComplete_15; }
	inline void set_onScreenShotComplete_15(ScreenShotCompleteDelegate_t509502638 * value)
	{
		___onScreenShotComplete_15 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenShotComplete_15, value);
	}

	inline static int32_t get_offset_of_onClickReset_16() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___onClickReset_16)); }
	inline Action_t3771233898 * get_onClickReset_16() const { return ___onClickReset_16; }
	inline Action_t3771233898 ** get_address_of_onClickReset_16() { return &___onClickReset_16; }
	inline void set_onClickReset_16(Action_t3771233898 * value)
	{
		___onClickReset_16 = value;
		Il2CppCodeGenWriteBarrier(&___onClickReset_16, value);
	}

	inline static int32_t get_offset_of_onClickClearCache_17() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___onClickClearCache_17)); }
	inline Action_t3771233898 * get_onClickClearCache_17() const { return ___onClickClearCache_17; }
	inline Action_t3771233898 ** get_address_of_onClickClearCache_17() { return &___onClickClearCache_17; }
	inline void set_onClickClearCache_17(Action_t3771233898 * value)
	{
		___onClickClearCache_17 = value;
		Il2CppCodeGenWriteBarrier(&___onClickClearCache_17, value);
	}

	inline static int32_t get_offset_of_onClickBack_18() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppGUIControllerScript_t4145218373, ___onClickBack_18)); }
	inline Action_t3771233898 * get_onClickBack_18() const { return ___onClickBack_18; }
	inline Action_t3771233898 ** get_address_of_onClickBack_18() { return &___onClickBack_18; }
	inline void set_onClickBack_18(Action_t3771233898 * value)
	{
		___onClickBack_18 = value;
		Il2CppCodeGenWriteBarrier(&___onClickBack_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
