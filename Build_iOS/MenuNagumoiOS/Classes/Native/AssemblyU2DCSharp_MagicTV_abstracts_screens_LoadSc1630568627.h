﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc4059313920.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.screens.LoadScreenFake
struct  LoadScreenFake_t1630568627  : public LoadScreenAbstract_t4059313920
{
public:
	// System.Boolean MagicTV.abstracts.screens.LoadScreenFake::_active
	bool ____active_8;
	// MagicTV.globals.Perspective MagicTV.abstracts.screens.LoadScreenFake::_currentPerspective
	int32_t ____currentPerspective_9;
	// System.String MagicTV.abstracts.screens.LoadScreenFake::_message
	String_t* ____message_10;
	// System.Int32 MagicTV.abstracts.screens.LoadScreenFake::totalInt
	int32_t ___totalInt_11;
	// System.Single MagicTV.abstracts.screens.LoadScreenFake::_showedTotal
	float ____showedTotal_12;

public:
	inline static int32_t get_offset_of__active_8() { return static_cast<int32_t>(offsetof(LoadScreenFake_t1630568627, ____active_8)); }
	inline bool get__active_8() const { return ____active_8; }
	inline bool* get_address_of__active_8() { return &____active_8; }
	inline void set__active_8(bool value)
	{
		____active_8 = value;
	}

	inline static int32_t get_offset_of__currentPerspective_9() { return static_cast<int32_t>(offsetof(LoadScreenFake_t1630568627, ____currentPerspective_9)); }
	inline int32_t get__currentPerspective_9() const { return ____currentPerspective_9; }
	inline int32_t* get_address_of__currentPerspective_9() { return &____currentPerspective_9; }
	inline void set__currentPerspective_9(int32_t value)
	{
		____currentPerspective_9 = value;
	}

	inline static int32_t get_offset_of__message_10() { return static_cast<int32_t>(offsetof(LoadScreenFake_t1630568627, ____message_10)); }
	inline String_t* get__message_10() const { return ____message_10; }
	inline String_t** get_address_of__message_10() { return &____message_10; }
	inline void set__message_10(String_t* value)
	{
		____message_10 = value;
		Il2CppCodeGenWriteBarrier(&____message_10, value);
	}

	inline static int32_t get_offset_of_totalInt_11() { return static_cast<int32_t>(offsetof(LoadScreenFake_t1630568627, ___totalInt_11)); }
	inline int32_t get_totalInt_11() const { return ___totalInt_11; }
	inline int32_t* get_address_of_totalInt_11() { return &___totalInt_11; }
	inline void set_totalInt_11(int32_t value)
	{
		___totalInt_11 = value;
	}

	inline static int32_t get_offset_of__showedTotal_12() { return static_cast<int32_t>(offsetof(LoadScreenFake_t1630568627, ____showedTotal_12)); }
	inline float get__showedTotal_12() const { return ____showedTotal_12; }
	inline float* get_address_of__showedTotal_12() { return &____showedTotal_12; }
	inline void set__showedTotal_12(float value)
	{
		____showedTotal_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
