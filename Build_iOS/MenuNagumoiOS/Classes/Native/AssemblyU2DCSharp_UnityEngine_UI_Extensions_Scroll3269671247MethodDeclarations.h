﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollRectEx/<OnDrag>c__AnonStorey8C
struct U3COnDragU3Ec__AnonStorey8C_t3269671247;
// UnityEngine.EventSystems.IDragHandler
struct IDragHandler_t880586197;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnDrag>c__AnonStorey8C::.ctor()
extern "C"  void U3COnDragU3Ec__AnonStorey8C__ctor_m1329269804 (U3COnDragU3Ec__AnonStorey8C_t3269671247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnDrag>c__AnonStorey8C::<>m__1(UnityEngine.EventSystems.IDragHandler)
extern "C"  void U3COnDragU3Ec__AnonStorey8C_U3CU3Em__1_m1177652186 (U3COnDragU3Ec__AnonStorey8C_t3269671247 * __this, Il2CppObject * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
