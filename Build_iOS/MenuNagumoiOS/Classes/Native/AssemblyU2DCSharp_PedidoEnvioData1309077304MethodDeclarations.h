﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoEnvioData
struct PedidoEnvioData_t1309077304;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoEnvioData::.ctor()
extern "C"  void PedidoEnvioData__ctor_m3951982387 (PedidoEnvioData_t1309077304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
