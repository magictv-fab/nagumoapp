﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;
// HutongGames.PlayMaker.FsmDebugUtility
struct FsmDebugUtility_t965175459;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"

// System.Void HutongGames.PlayMaker.FsmDebugUtility::Log(HutongGames.PlayMaker.Fsm,System.String,System.Boolean)
extern "C"  void FsmDebugUtility_Log_m3574791361 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, String_t* ___text1, bool ___frameCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmDebugUtility::Log(System.String,System.Boolean)
extern "C"  void FsmDebugUtility_Log_m257711993 (Il2CppObject * __this /* static, unused */, String_t* ___text0, bool ___frameCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmDebugUtility::Log(UnityEngine.Object,System.String)
extern "C"  void FsmDebugUtility_Log_m825906884 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmDebugUtility::.ctor()
extern "C"  void FsmDebugUtility__ctor_m1255986844 (FsmDebugUtility_t965175459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
