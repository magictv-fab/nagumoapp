﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveTo
struct iTweenMoveTo_t469657174;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::.ctor()
extern "C"  void iTweenMoveTo__ctor_m4177463504 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnDrawGizmos()
extern "C"  void iTweenMoveTo_OnDrawGizmos_m310804048 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::Reset()
extern "C"  void iTweenMoveTo_Reset_m1823896445 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnEnter()
extern "C"  void iTweenMoveTo_OnEnter_m2434701479 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnExit()
extern "C"  void iTweenMoveTo_OnExit_m2165668369 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::DoiTween()
extern "C"  void iTweenMoveTo_DoiTween_m3912434049 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
