﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_996820380.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2600404039_gshared (KeyValuePair_2_t996820380 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2600404039(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t996820380 *, Il2CppChar, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m2600404039_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Key()
extern "C"  Il2CppChar KeyValuePair_2_get_Key_m1713880001_gshared (KeyValuePair_2_t996820380 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1713880001(__this, method) ((  Il2CppChar (*) (KeyValuePair_2_t996820380 *, const MethodInfo*))KeyValuePair_2_get_Key_m1713880001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2062714242_gshared (KeyValuePair_2_t996820380 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2062714242(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t996820380 *, Il2CppChar, const MethodInfo*))KeyValuePair_2_set_Key_m2062714242_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m694146753_gshared (KeyValuePair_2_t996820380 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m694146753(__this, method) ((  int32_t (*) (KeyValuePair_2_t996820380 *, const MethodInfo*))KeyValuePair_2_get_Value_m694146753_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4289415298_gshared (KeyValuePair_2_t996820380 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4289415298(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t996820380 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m4289415298_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1634587040_gshared (KeyValuePair_2_t996820380 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1634587040(__this, method) ((  String_t* (*) (KeyValuePair_2_t996820380 *, const MethodInfo*))KeyValuePair_2_ToString_m1634587040_gshared)(__this, method)
