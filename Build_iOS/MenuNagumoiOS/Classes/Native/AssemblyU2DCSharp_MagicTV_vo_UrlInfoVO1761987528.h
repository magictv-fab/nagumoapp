﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.UrlInfoVO
struct  UrlInfoVO_t1761987528  : public VOWithId_t2461972856
{
public:
	// System.String MagicTV.vo.UrlInfoVO::bundle_file_id
	String_t* ___bundle_file_id_1;
	// System.String MagicTV.vo.UrlInfoVO::type
	String_t* ___type_2;
	// System.String MagicTV.vo.UrlInfoVO::md5
	String_t* ___md5_3;
	// System.String MagicTV.vo.UrlInfoVO::url
	String_t* ___url_4;
	// System.Int32 MagicTV.vo.UrlInfoVO::size
	int32_t ___size_5;
	// System.String MagicTV.vo.UrlInfoVO::local_url
	String_t* ___local_url_6;
	// System.Int32 MagicTV.vo.UrlInfoVO::status
	int32_t ___status_7;

public:
	inline static int32_t get_offset_of_bundle_file_id_1() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___bundle_file_id_1)); }
	inline String_t* get_bundle_file_id_1() const { return ___bundle_file_id_1; }
	inline String_t** get_address_of_bundle_file_id_1() { return &___bundle_file_id_1; }
	inline void set_bundle_file_id_1(String_t* value)
	{
		___bundle_file_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___bundle_file_id_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_md5_3() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___md5_3)); }
	inline String_t* get_md5_3() const { return ___md5_3; }
	inline String_t** get_address_of_md5_3() { return &___md5_3; }
	inline void set_md5_3(String_t* value)
	{
		___md5_3 = value;
		Il2CppCodeGenWriteBarrier(&___md5_3, value);
	}

	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier(&___url_4, value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_local_url_6() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___local_url_6)); }
	inline String_t* get_local_url_6() const { return ___local_url_6; }
	inline String_t** get_address_of_local_url_6() { return &___local_url_6; }
	inline void set_local_url_6(String_t* value)
	{
		___local_url_6 = value;
		Il2CppCodeGenWriteBarrier(&___local_url_6, value);
	}

	inline static int32_t get_offset_of_status_7() { return static_cast<int32_t>(offsetof(UrlInfoVO_t1761987528, ___status_7)); }
	inline int32_t get_status_7() const { return ___status_7; }
	inline int32_t* get_address_of_status_7() { return &___status_7; }
	inline void set_status_7(int32_t value)
	{
		___status_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
