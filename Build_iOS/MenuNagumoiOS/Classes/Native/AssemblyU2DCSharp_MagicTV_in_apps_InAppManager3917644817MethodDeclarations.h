﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppManager
struct InAppManager_t3917644817;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String
struct String_t;
// MagicTV.abstracts.InAppAbstract[]
struct InAppAbstractU5BU5D_t412350073;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.in_apps.InAppManager::.ctor()
extern "C"  void InAppManager__ctor_m2295909449 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::.cctor()
extern "C"  void InAppManager__cctor_m1971619972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Update()
extern "C"  void InAppManager_Update_m4180578276 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::prepare()
extern "C"  void InAppManager_prepare_m3977738062 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::KillCurrentPresentation()
extern "C"  void InAppManager_KillCurrentPresentation_m587809180 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::showLoader()
extern "C"  void InAppManager_showLoader_m1427357099 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::hideLoader()
extern "C"  void InAppManager_hideLoader_m2841553648 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.InAppAbstract MagicTV.in_apps.InAppManager::getInAppByBundle(MagicTV.vo.BundleVO)
extern "C"  InAppAbstract_t382673128 * InAppManager_getInAppByBundle_m2590456150 (InAppManager_t3917644817 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.InAppAbstract MagicTV.in_apps.InAppManager::getPresentationPrefab(System.String)
extern "C"  InAppAbstract_t382673128 * InAppManager_getPresentationPrefab_m2121692952 (InAppManager_t3917644817 * __this, String_t* ___prefabName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.InAppAbstract[] MagicTV.in_apps.InAppManager::getInAppByString(System.String)
extern "C"  InAppAbstractU5BU5D_t412350073* InAppManager_getInAppByString_m471768458 (InAppManager_t3917644817 * __this, String_t* ___appContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Play(System.String)
extern "C"  void InAppManager_Play_m169045331 (InAppManager_t3917644817 * __this, String_t* ___appName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Reset()
extern "C"  void InAppManager_Reset_m4237309686 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::disposeCurrent(System.String)
extern "C"  void InAppManager_disposeCurrent_m2174071309 (InAppManager_t3917644817 * __this, String_t* ___appName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::doDisposeCurrent(System.String)
extern "C"  void InAppManager_doDisposeCurrent_m1206049730 (InAppManager_t3917644817 * __this, String_t* ___appName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::doPlay()
extern "C"  void InAppManager_doPlay_m3102454746 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::prepareCurrent()
extern "C"  void InAppManager_prepareCurrent_m2657301805 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Stop(System.String)
extern "C"  void InAppManager_Stop_m315292357 (InAppManager_t3917644817 * __this, String_t* ___appName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Stop()
extern "C"  void InAppManager_Stop_m3504194397 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Pause()
extern "C"  void InAppManager_Pause_m2350035421 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppManager::Play()
extern "C"  void InAppManager_Play_m3410510351 (InAppManager_t3917644817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
