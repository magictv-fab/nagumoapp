﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ScanEventArgs
struct ScanEventArgs_t1070518092;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.ScanEventArgs::.ctor(System.String)
extern "C"  void ScanEventArgs__ctor_m2174238403 (ScanEventArgs_t1070518092 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Core.ScanEventArgs::get_Name()
extern "C"  String_t* ScanEventArgs_get_Name_m2068354940 (ScanEventArgs_t1070518092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.ScanEventArgs::get_ContinueRunning()
extern "C"  bool ScanEventArgs_get_ContinueRunning_m824553208 (ScanEventArgs_t1070518092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ScanEventArgs::set_ContinueRunning(System.Boolean)
extern "C"  void ScanEventArgs_set_ContinueRunning_m4088650927 (ScanEventArgs_t1070518092 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
