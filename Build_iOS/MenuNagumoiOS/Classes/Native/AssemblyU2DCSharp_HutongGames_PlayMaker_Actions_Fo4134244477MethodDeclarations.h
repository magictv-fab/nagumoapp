﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ForwardAllEvents
struct ForwardAllEvents_t4134244477;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"

// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::.ctor()
extern "C"  void ForwardAllEvents__ctor_m3245051529 (ForwardAllEvents_t4134244477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::Reset()
extern "C"  void ForwardAllEvents_Reset_m891484470 (ForwardAllEvents_t4134244477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ForwardAllEvents::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardAllEvents_Event_m4176117383 (ForwardAllEvents_t4134244477 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
