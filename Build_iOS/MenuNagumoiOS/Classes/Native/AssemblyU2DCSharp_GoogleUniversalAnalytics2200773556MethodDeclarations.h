﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleUniversalAnalytics
struct GoogleUniversalAnalytics_t2200773556;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_HitType1313315314.h"

// System.Void GoogleUniversalAnalytics::.ctor()
extern "C"  void GoogleUniversalAnalytics__ctor_m248468839 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::.cctor()
extern "C"  void GoogleUniversalAnalytics__cctor_m2925470502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GoogleUniversalAnalytics GoogleUniversalAnalytics::get_Instance()
extern "C"  GoogleUniversalAnalytics_t2200773556 * GoogleUniversalAnalytics_get_Instance_m2517431140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::get_analyticsDisabled()
extern "C"  bool GoogleUniversalAnalytics_get_analyticsDisabled_m1442440106 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::set_analyticsDisabled(System.Boolean)
extern "C"  void GoogleUniversalAnalytics_set_analyticsDisabled_m1198180065 (GoogleUniversalAnalytics_t2200773556 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setOfflineQueueNetActivityTimes(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void GoogleUniversalAnalytics_setOfflineQueueNetActivityTimes_m1663040656 (GoogleUniversalAnalytics_t2200773556 * __this, float ___defaultReachabilityCheckPeriodSeconds0, float ___hitBeingBuiltRetryTimeSeconds1, float ___netVerificationErrorRetryTimeSeconds2, float ___netVerificationMismatchRetryTimeSeconds3, float ___cachedHitSendThrottleTimeSeconds4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GoogleUniversalAnalytics::get_remainingEntriesInOfflineCache()
extern "C"  int32_t GoogleUniversalAnalytics_get_remainingEntriesInOfflineCache_m481512754 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GoogleUniversalAnalytics::returnStringAsIs(System.String)
extern "C"  String_t* GoogleUniversalAnalytics_returnStringAsIs_m2393651429 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::get_isHitBeingBuilt()
extern "C"  bool GoogleUniversalAnalytics_get_isHitBeingBuilt_m2320001968 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::get_internetReachable()
extern "C"  bool GoogleUniversalAnalytics_get_internetReachable_m961276948 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GoogleUniversalAnalytics::netActivity()
extern "C"  Il2CppObject * GoogleUniversalAnalytics_netActivity_m2784831785 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::initialize(UnityEngine.MonoBehaviour,System.String,System.String,System.String,System.String,System.Boolean,System.String)
extern "C"  void GoogleUniversalAnalytics_initialize_m2429868743 (GoogleUniversalAnalytics_t2200773556 * __this, MonoBehaviour_t667441552 * ___analyticsHelperBehaviour0, String_t* ___trackingID1, String_t* ___anonymousClientID2, String_t* ___appName3, String_t* ___appVersion4, bool ___useHTTPS5, String_t* ___offlineCacheFilePath6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setStringEscaping(System.Boolean)
extern "C"  void GoogleUniversalAnalytics_setStringEscaping_m2305643661 (GoogleUniversalAnalytics_t2200773556 * __this, bool ___useStringEscaping0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::beginHit(GoogleUniversalAnalytics/HitType)
extern "C"  bool GoogleUniversalAnalytics_beginHit_m1720150409 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___hitType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addAnonymizeIP()
extern "C"  void GoogleUniversalAnalytics_addAnonymizeIP_m44688535 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addQueueTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addQueueTime_m207523275 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___ms0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setUserID(System.String)
extern "C"  void GoogleUniversalAnalytics_setUserID_m944793685 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___userID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addSessionControl(System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addSessionControl_m1249561156 (GoogleUniversalAnalytics_t2200773556 * __this, bool ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setIPOverride(System.String)
extern "C"  void GoogleUniversalAnalytics_setIPOverride_m149712872 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___ip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setUserAgentOverride(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_setUserAgentOverride_m2137902652 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___userAgent0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDocumentReferrer(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addDocumentReferrer_m3255036475 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___url0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCampaignName(System.String)
extern "C"  void GoogleUniversalAnalytics_addCampaignName_m3625364225 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCampaignSource(System.String)
extern "C"  void GoogleUniversalAnalytics_addCampaignSource_m1035584689 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCampaignMedium(System.String)
extern "C"  void GoogleUniversalAnalytics_addCampaignMedium_m216055319 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCampaignKeyword(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addCampaignKeyword_m1153853040 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCampaignContent(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addCampaignContent_m1387176192 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCampaignID(System.String)
extern "C"  void GoogleUniversalAnalytics_addCampaignID_m78954833 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addGoogleAdWordsID(System.String)
extern "C"  void GoogleUniversalAnalytics_addGoogleAdWordsID_m1264193182 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addGoogleDisplayAdsID(System.String)
extern "C"  void GoogleUniversalAnalytics_addGoogleDisplayAdsID_m943708314 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addScreenResolution(System.Int32,System.Int32)
extern "C"  void GoogleUniversalAnalytics_addScreenResolution_m2209425032 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addViewportSize(System.Int32,System.Int32)
extern "C"  void GoogleUniversalAnalytics_addViewportSize_m1483460377 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDocumentEncoding(System.String)
extern "C"  void GoogleUniversalAnalytics_addDocumentEncoding_m3271411022 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addScreenColors(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addScreenColors_m1076586739 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___depthBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addUserLanguage(System.String)
extern "C"  void GoogleUniversalAnalytics_addUserLanguage_m17729913 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addJavaEnabled(System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addJavaEnabled_m2530021874 (GoogleUniversalAnalytics_t2200773556 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addFlashVersion(System.Int32,System.Int32,System.Int32)
extern "C"  void GoogleUniversalAnalytics_addFlashVersion_m2144466527 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___major0, int32_t ___minor1, int32_t ___revision2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addNonInteractionHit()
extern "C"  void GoogleUniversalAnalytics_addNonInteractionHit_m1904179530 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDocumentLocationURL(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addDocumentLocationURL_m3223011606 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___url0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDocumentHostName(System.String)
extern "C"  void GoogleUniversalAnalytics_addDocumentHostName_m2463089230 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDocumentPath(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addDocumentPath_m495702977 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDocumentTitle(System.String)
extern "C"  void GoogleUniversalAnalytics_addDocumentTitle_m1136165609 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addScreenName(System.String)
extern "C"  void GoogleUniversalAnalytics_addScreenName_m4263999717 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addLinkID(System.String)
extern "C"  void GoogleUniversalAnalytics_addLinkID_m720656679 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setApplicationName(System.String)
extern "C"  void GoogleUniversalAnalytics_setApplicationName_m150301676 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setApplicationID(System.String)
extern "C"  void GoogleUniversalAnalytics_setApplicationID_m3985948924 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::setApplicationVersion(System.String)
extern "C"  void GoogleUniversalAnalytics_setApplicationVersion_m3675864531 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addApplicationInstallerID(System.String)
extern "C"  void GoogleUniversalAnalytics_addApplicationInstallerID_m1126557385 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addEventCategory(System.String)
extern "C"  void GoogleUniversalAnalytics_addEventCategory_m3552392654 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addEventAction(System.String)
extern "C"  void GoogleUniversalAnalytics_addEventAction_m2960825430 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addEventLabel(System.String)
extern "C"  void GoogleUniversalAnalytics_addEventLabel_m355259170 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addEventValue(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addEventValue_m2296639726 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addTransactionID(System.String)
extern "C"  void GoogleUniversalAnalytics_addTransactionID_m1863186669 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addTransactionAffiliation(System.String)
extern "C"  void GoogleUniversalAnalytics_addTransactionAffiliation_m3103848170 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addTransactionRevenue(System.Double)
extern "C"  void GoogleUniversalAnalytics_addTransactionRevenue_m3271465214 (GoogleUniversalAnalytics_t2200773556 * __this, double ___currency0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addTransactionShipping(System.Double)
extern "C"  void GoogleUniversalAnalytics_addTransactionShipping_m863243994 (GoogleUniversalAnalytics_t2200773556 * __this, double ___currency0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addTransactionTax(System.Double)
extern "C"  void GoogleUniversalAnalytics_addTransactionTax_m388231695 (GoogleUniversalAnalytics_t2200773556 * __this, double ___currency0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addItemName(System.String)
extern "C"  void GoogleUniversalAnalytics_addItemName_m3017599390 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addItemPrice(System.Double)
extern "C"  void GoogleUniversalAnalytics_addItemPrice_m3704989840 (GoogleUniversalAnalytics_t2200773556 * __this, double ___currency0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addItemQuantity(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addItemQuantity_m3326133557 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addItemCode(System.String)
extern "C"  void GoogleUniversalAnalytics_addItemCode_m3347858940 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addItemCategory(System.String)
extern "C"  void GoogleUniversalAnalytics_addItemCategory_m3741441739 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCurrencyCode(System.String)
extern "C"  void GoogleUniversalAnalytics_addCurrencyCode_m682135230 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addSocialNetwork(System.String)
extern "C"  void GoogleUniversalAnalytics_addSocialNetwork_m1634221669 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addSocialAction(System.String)
extern "C"  void GoogleUniversalAnalytics_addSocialAction_m1968373177 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addSocialActionTarget(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addSocialActionTarget_m973230229 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, bool ___allowNonEscaped1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addUserTimingCategory(System.String)
extern "C"  void GoogleUniversalAnalytics_addUserTimingCategory_m2027319145 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addUserTimingVariableName(System.String)
extern "C"  void GoogleUniversalAnalytics_addUserTimingVariableName_m3740737312 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addUserTimingTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addUserTimingTime_m2273962137 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addUserTimingLabel(System.String)
extern "C"  void GoogleUniversalAnalytics_addUserTimingLabel_m2581624679 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addPageLoadTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addPageLoadTime_m4074425849 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addDNSTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addDNSTime_m775053987 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addPageDownloadTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addPageDownloadTime_m231670555 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addRedirectResponseTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addRedirectResponseTime_m2833266465 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addTCPConnectTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addTCPConnectTime_m1154121069 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addServerResponseTime(System.Int32)
extern "C"  void GoogleUniversalAnalytics_addServerResponseTime_m145289480 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addExceptionDescription(System.String)
extern "C"  void GoogleUniversalAnalytics_addExceptionDescription_m2146488655 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addExceptionIsFatal(System.Boolean)
extern "C"  void GoogleUniversalAnalytics_addExceptionIsFatal_m2801396168 (GoogleUniversalAnalytics_t2200773556 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCustomDimension(System.Int32,System.String)
extern "C"  void GoogleUniversalAnalytics_addCustomDimension_m2333415294 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___index0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCustomMetric(System.Int32,System.Int64)
extern "C"  void GoogleUniversalAnalytics_addCustomMetric_m3216124832 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addExperimentID(System.String)
extern "C"  void GoogleUniversalAnalytics_addExperimentID_m576625476 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addExperimentVariant(System.String)
extern "C"  void GoogleUniversalAnalytics_addExperimentVariant_m1349201214 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendPageViewHit(System.String,System.String,System.String)
extern "C"  void GoogleUniversalAnalytics_sendPageViewHit_m4111537822 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___documentHostName0, String_t* ___documentPath1, String_t* ___documentTitle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendEventHit(System.String,System.String,System.String,System.Int32)
extern "C"  void GoogleUniversalAnalytics_sendEventHit_m2330882555 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___eventCategory0, String_t* ___eventAction1, String_t* ___eventLabel2, int32_t ___eventValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendTransactionHit(System.String,System.String,System.Double,System.Double,System.Double,System.String)
extern "C"  void GoogleUniversalAnalytics_sendTransactionHit_m1666422548 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___transactionID0, String_t* ___affiliation1, double ___revenue2, double ___shipping3, double ___tax4, String_t* ___currencyCode5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendItemHit(System.String,System.String,System.Double,System.Int32,System.String,System.String,System.String)
extern "C"  void GoogleUniversalAnalytics_sendItemHit_m4085903694 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___transactionID0, String_t* ___itemName1, double ___price2, int32_t ___quantity3, String_t* ___itemCode4, String_t* ___itemCategory5, String_t* ___currencyCode6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendSocialHit(System.String,System.String,System.String)
extern "C"  void GoogleUniversalAnalytics_sendSocialHit_m3085674871 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___network0, String_t* ___action1, String_t* ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendExceptionHit(System.String,System.Boolean)
extern "C"  void GoogleUniversalAnalytics_sendExceptionHit_m3322761988 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___description0, bool ___isFatal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendUserTimingHit(System.String,System.String,System.Int32,System.String)
extern "C"  void GoogleUniversalAnalytics_sendUserTimingHit_m1783420912 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___category0, String_t* ___variable1, int32_t ___time2, String_t* ___label3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendBrowserTimingHit(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GoogleUniversalAnalytics_sendBrowserTimingHit_m1048326775 (GoogleUniversalAnalytics_t2200773556 * __this, int32_t ___dnsTime0, int32_t ___pageDownloadTime1, int32_t ___redirectTime2, int32_t ___tcpConnectTime3, int32_t ___serverResponseTime4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::sendAppScreenHit(System.String)
extern "C"  void GoogleUniversalAnalytics_sendAppScreenHit_m3182252407 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___screenName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GoogleUniversalAnalytics::getPOSIXTimeMilliseconds()
extern "C"  int64_t GoogleUniversalAnalytics_getPOSIXTimeMilliseconds_m3742269016 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::stopOfflineCacheReader()
extern "C"  void GoogleUniversalAnalytics_stopOfflineCacheReader_m230964097 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::stopOfflineCacheWriter()
extern "C"  void GoogleUniversalAnalytics_stopOfflineCacheWriter_m3546503985 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::closeOfflineCacheFile()
extern "C"  void GoogleUniversalAnalytics_closeOfflineCacheFile_m4216946040 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::increasePlayerPrefOfflineQueueLength()
extern "C"  void GoogleUniversalAnalytics_increasePlayerPrefOfflineQueueLength_m3244503287 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::increasePlayerPrefOfflineQueueSentHits()
extern "C"  void GoogleUniversalAnalytics_increasePlayerPrefOfflineQueueSentHits_m136998025 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::clearOfflineQueue()
extern "C"  void GoogleUniversalAnalytics_clearOfflineQueue_m3264319936 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::saveHitToOfflineQueue(System.String)
extern "C"  bool GoogleUniversalAnalytics_saveHitToOfflineQueue_m306731218 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___hitData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::pendingQueuedOfflineHits()
extern "C"  bool GoogleUniversalAnalytics_pendingQueuedOfflineHits_m3077773002 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::sendOnePendingOfflineHit()
extern "C"  bool GoogleUniversalAnalytics_sendOnePendingOfflineHit_m4118718682 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics::addCommonOptionalHitParams()
extern "C"  void GoogleUniversalAnalytics_addCommonOptionalHitParams_m4152390026 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics::sendHit()
extern "C"  bool GoogleUniversalAnalytics_sendHit_m498265756 (GoogleUniversalAnalytics_t2200773556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW GoogleUniversalAnalytics::beginWWWRequest(System.String)
extern "C"  WWW_t3134621005 * GoogleUniversalAnalytics_beginWWWRequest_m2715151599 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___postDataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GoogleUniversalAnalytics::doWWWRequestAndCheckResult(System.String)
extern "C"  Il2CppObject * GoogleUniversalAnalytics_doWWWRequestAndCheckResult_m2412021036 (GoogleUniversalAnalytics_t2200773556 * __this, String_t* ___postDataString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW GoogleUniversalAnalytics::finalizeAndSendHit(System.Boolean)
extern "C"  WWW_t3134621005 * GoogleUniversalAnalytics_finalizeAndSendHit_m1632948985 (GoogleUniversalAnalytics_t2200773556 * __this, bool ___needReturnWWW0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
