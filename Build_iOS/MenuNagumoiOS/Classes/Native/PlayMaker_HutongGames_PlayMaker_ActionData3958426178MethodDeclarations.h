﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionData
struct ActionData_t3958426178;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString>
struct List_1_t2321044203;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject>
struct List_1_t2189661721;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>
struct List_1_t3065333419;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault>
struct List_1_t1620082664;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve>
struct List_1_t4054181541;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>
struct List_1_t353063272;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>
struct List_1_t4154693685;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar>
struct List_1_t2964336089;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty>
struct List_1_t1000377263;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget>
struct List_1_t3192090493;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption>
struct List_1_t2333180753;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t2476090292;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// System.Object
struct Il2CppObject;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Array
struct Il2CppArray;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t2786508133;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t3279845016;
// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t3927159007;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t964995201;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t3757199647;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Int32 HutongGames.PlayMaker.ActionData::get_ActionCount()
extern "C"  int32_t ActionData_get_ActionCount_m1125497069 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.ActionData::Copy()
extern "C"  ActionData_t3958426178 * ActionData_Copy_m1426077309 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString> HutongGames.PlayMaker.ActionData::CopyFsmStringParams()
extern "C"  List_1_t2321044203 * ActionData_CopyFsmStringParams_m560035175 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject> HutongGames.PlayMaker.ActionData::CopyFsmObjectParams()
extern "C"  List_1_t2189661721 * ActionData_CopyFsmObjectParams_m2512514855 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject> HutongGames.PlayMaker.ActionData::CopyFsmGameObjectParams()
extern "C"  List_1_t3065333419 * ActionData_CopyFsmGameObjectParams_m4253082727 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault> HutongGames.PlayMaker.ActionData::CopyFsmOwnerDefaultParams()
extern "C"  List_1_t1620082664 * ActionData_CopyFsmOwnerDefaultParams_m3703922503 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve> HutongGames.PlayMaker.ActionData::CopyAnimationCurveParams()
extern "C"  List_1_t4054181541 * ActionData_CopyAnimationCurveParams_m2581662909 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall> HutongGames.PlayMaker.ActionData::CopyFunctionCallParams()
extern "C"  List_1_t353063272 * ActionData_CopyFunctionCallParams_m3770948633 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl> HutongGames.PlayMaker.ActionData::CopyFsmTemplateControlParams()
extern "C"  List_1_t4154693685 * ActionData_CopyFsmTemplateControlParams_m2258795571 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar> HutongGames.PlayMaker.ActionData::CopyFsmVarParams()
extern "C"  List_1_t2964336089 * ActionData_CopyFsmVarParams_m2646929019 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty> HutongGames.PlayMaker.ActionData::CopyFsmPropertyParams()
extern "C"  List_1_t1000377263 * ActionData_CopyFsmPropertyParams_m2190701159 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget> HutongGames.PlayMaker.ActionData::CopyFsmEventTargetParams()
extern "C"  List_1_t3192090493 * ActionData_CopyFsmEventTargetParams_m2654400195 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption> HutongGames.PlayMaker.ActionData::CopyLayoutOptionParams()
extern "C"  List_1_t2333180753 * ActionData_CopyLayoutOptionParams_m1847597931 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::ClearActionData()
extern "C"  void ActionData_ClearActionData_m873293020 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ActionData::GetActionType(System.String)
extern "C"  Type_t * ActionData_GetActionType_m1601647431 (Il2CppObject * __this /* static, unused */, String_t* ___actionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] HutongGames.PlayMaker.ActionData::GetFields(System.Type)
extern "C"  FieldInfoU5BU5D_t2567562023* ActionData_GetFields_m3305443080 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionData::GetActionTypeHashCode(System.Type)
extern "C"  int32_t ActionData_GetActionTypeHashCode_m1842636123 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.ActionData::LoadActions(HutongGames.PlayMaker.FsmState)
extern "C"  FsmStateActionU5BU5D_t2476090292* ActionData_LoadActions_m1529704733 (ActionData_t3958426178 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::CreateAction(HutongGames.PlayMaker.FsmState,System.Int32)
extern "C"  FsmStateAction_t2366529033 * ActionData_CreateAction_m1781451649 (ActionData_t3958426178 * __this, FsmState_t2146334067 * ___state0, int32_t ___actionIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LoadActionField(System.Object,System.Reflection.FieldInfo,System.Int32)
extern "C"  void ActionData_LoadActionField_m2961223912 (ActionData_t3958426178 * __this, Il2CppObject * ___obj0, FieldInfo_t * ___field1, int32_t ___paramIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LoadArrayElement(System.Array,System.Type,System.Int32,System.Int32)
extern "C"  void ActionData_LoadArrayElement_m393777973 (ActionData_t3958426178 * __this, Il2CppArray * ___field0, Type_t * ___fieldType1, int32_t ___elementIndex2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LogError(System.String)
extern "C"  void ActionData_LogError_m4034432363 (Il2CppObject * __this /* static, unused */, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LogInfo(System.String)
extern "C"  void ActionData_LogInfo_m3218038305 (Il2CppObject * __this /* static, unused */, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.ActionData::GetFsmGameObject(System.Int32)
extern "C"  FsmGameObject_t1697147867 * ActionData_GetFsmGameObject_m3859424493 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTemplateControl HutongGames.PlayMaker.ActionData::GetFsmTemplateControl(System.Int32)
extern "C"  FsmTemplateControl_t2786508133 * ActionData_GetFsmTemplateControl_m993137447 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.ActionData::GetFsmVar(System.Int32)
extern "C"  FsmVar_t1596150537 * ActionData_GetFsmVar_m2740781295 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FunctionCall HutongGames.PlayMaker.ActionData::GetFunctionCall(System.Int32)
extern "C"  FunctionCall_t3279845016 * ActionData_GetFunctionCall_m1808520077 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmProperty HutongGames.PlayMaker.ActionData::GetFsmProperty(System.Int32)
extern "C"  FsmProperty_t3927159007 * ActionData_GetFsmProperty_m3229730093 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.ActionData::GetFsmEventTarget(System.Int32)
extern "C"  FsmEventTarget_t1823904941 * ActionData_GetFsmEventTarget_m4110420279 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.LayoutOption HutongGames.PlayMaker.ActionData::GetLayoutOption(System.Int32)
extern "C"  LayoutOption_t964995201 * ActionData_GetLayoutOption_m1453220767 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.ActionData::GetFsmOwnerDefault(System.Int32)
extern "C"  FsmOwnerDefault_t251897112 * ActionData_GetFsmOwnerDefault_m981991757 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.ActionData::GetFsmString(System.Int32)
extern "C"  FsmString_t952858651 * ActionData_GetFsmString_m3336027245 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.ActionData::GetFsmObject(System.Int32)
extern "C"  FsmObject_t821476169 * ActionData_GetFsmObject_m3961684653 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.ActionData::GetFsmMaterial(System.Int32)
extern "C"  FsmMaterial_t924399665 * ActionData_GetFsmMaterial_m2828004205 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.ActionData::GetFsmTexture(System.Int32)
extern "C"  FsmTexture_t3073272573 * ActionData_GetFsmTexture_m3645307223 (ActionData_t3958426178 * __this, int32_t ___paramIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionData::TryFixActionName(System.String)
extern "C"  String_t* ActionData_TryFixActionName_m752710545 (Il2CppObject * __this /* static, unused */, String_t* ___actionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::TryRecoverAction(System.Type,HutongGames.PlayMaker.FsmStateAction,System.Int32)
extern "C"  FsmStateAction_t2366529033 * ActionData_TryRecoverAction_m3744628477 (ActionData_t3958426178 * __this, Type_t * ___actionType0, FsmStateAction_t2366529033 * ___action1, int32_t ___actionIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo HutongGames.PlayMaker.ActionData::FindField(System.Type,System.Int32)
extern "C"  FieldInfo_t * ActionData_FindField_m3677596223 (ActionData_t3958426178 * __this, Type_t * ___actionType0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo HutongGames.PlayMaker.ActionData::FindField(System.Type,System.String)
extern "C"  FieldInfo_t * ActionData_FindField_m2793031636 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::TryConvertParameter(HutongGames.PlayMaker.FsmStateAction,System.Reflection.FieldInfo,System.Int32)
extern "C"  bool ActionData_TryConvertParameter_m3364364282 (ActionData_t3958426178 * __this, FsmStateAction_t2366529033 * ___action0, FieldInfo_t * ___field1, int32_t ___paramIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::TryConvertArrayElement(System.Array,HutongGames.PlayMaker.ParamDataType,HutongGames.PlayMaker.ParamDataType,System.Int32,System.Int32)
extern "C"  bool ActionData_TryConvertArrayElement_m260774682 (ActionData_t3958426178 * __this, Il2CppArray * ___field0, int32_t ___originalParamType1, int32_t ___currentParamType2, int32_t ___elementIndex3, int32_t ___paramIndex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ActionData::ConvertType(HutongGames.PlayMaker.ParamDataType,HutongGames.PlayMaker.ParamDataType,System.Int32)
extern "C"  Il2CppObject * ActionData_ConvertType_m1109321514 (ActionData_t3958426178 * __this, int32_t ___originalParamType0, int32_t ___currentParamType1, int32_t ___paramIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveActions(HutongGames.PlayMaker.FsmStateAction[])
extern "C"  void ActionData_SaveActions_m2286373338 (ActionData_t3958426178 * __this, FsmStateActionU5BU5D_t2476090292* ___actions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveAction(HutongGames.PlayMaker.FsmStateAction)
extern "C"  void ActionData_SaveAction_m216384307 (ActionData_t3958426178 * __this, FsmStateAction_t2366529033 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveActionField(System.Type,System.Object)
extern "C"  void ActionData_SaveActionField_m3027907441 (ActionData_t3958426178 * __this, Type_t * ___fieldType0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::AddByteData(System.Collections.Generic.ICollection`1<System.Byte>)
extern "C"  void ActionData_AddByteData_m4153890598 (ActionData_t3958426178 * __this, Il2CppObject* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ParamDataType HutongGames.PlayMaker.ActionData::GetParamDataType(System.Type)
extern "C"  int32_t ActionData_GetParamDataType_m11844701 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::.ctor()
extern "C"  void ActionData__ctor_m256498513 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::.cctor()
extern "C"  void ActionData__cctor_m3174390396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
