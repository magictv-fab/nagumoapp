﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonPropertyNotSerializable
struct JsonPropertyNotSerializable_t1551128245;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonPropertyNotSerializable::.ctor()
extern "C"  void JsonPropertyNotSerializable__ctor_m1257377878 (JsonPropertyNotSerializable_t1551128245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
