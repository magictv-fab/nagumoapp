﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.HelpUrlAttribute
struct HelpUrlAttribute_t2247792144;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.HelpUrlAttribute::get_Url()
extern "C"  String_t* HelpUrlAttribute_get_Url_m328506404 (HelpUrlAttribute_t2247792144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.HelpUrlAttribute::.ctor(System.String)
extern "C"  void HelpUrlAttribute__ctor_m4100057119 (HelpUrlAttribute_t2247792144 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
