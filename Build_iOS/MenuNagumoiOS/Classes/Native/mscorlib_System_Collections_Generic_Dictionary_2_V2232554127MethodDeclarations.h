﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2632136600MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1098777025(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2232554127 *, Dictionary_2_t3531948414 *, const MethodInfo*))ValueCollection__ctor_m1605897482_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3368582737(__this, ___item0, method) ((  void (*) (ValueCollection_t2232554127 *, Action_t3771233898 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1860920808_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3449043866(__this, method) ((  void (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2945484465_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3836249333(__this, ___item0, method) ((  bool (*) (ValueCollection_t2232554127 *, Action_t3771233898 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4108960702_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1381521114(__this, ___item0, method) ((  bool (*) (ValueCollection_t2232554127 *, Action_t3771233898 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3392975843_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1898495720(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4124050175_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2005967902(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2232554127 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1870085749_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1949718553(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3766397488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2111425192(__this, method) ((  bool (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2384136561_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1811729544(__this, method) ((  bool (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2169974993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2671796(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2970768317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2360450504(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2232554127 *, ActionU5BU5D_t1643143343*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3141820113_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::GetEnumerator()
#define ValueCollection_GetEnumerator_m16668523(__this, method) ((  Enumerator_t1463781822  (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_GetEnumerator_m28159028_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Action>::get_Count()
#define ValueCollection_get_Count_m1449208654(__this, method) ((  int32_t (*) (ValueCollection_t2232554127 *, const MethodInfo*))ValueCollection_get_Count_m3414445207_gshared)(__this, method)
