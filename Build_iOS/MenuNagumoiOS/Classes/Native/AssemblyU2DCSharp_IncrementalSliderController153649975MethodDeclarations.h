﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IncrementalSliderController
struct IncrementalSliderController_t153649975;

#include "codegen/il2cpp-codegen.h"

// System.Void IncrementalSliderController::.ctor()
extern "C"  void IncrementalSliderController__ctor_m1434113556 (IncrementalSliderController_t153649975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementalSliderController::ParseMultiplyValue(System.Single)
extern "C"  void IncrementalSliderController_ParseMultiplyValue_m1042834497 (IncrementalSliderController_t153649975 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementalSliderController::ParseValue(System.Single)
extern "C"  void IncrementalSliderController_ParseValue_m43812861 (IncrementalSliderController_t153649975 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementalSliderController::LateUpdate()
extern "C"  void IncrementalSliderController_LateUpdate_m3469811903 (IncrementalSliderController_t153649975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementalSliderController::OnEndDrag()
extern "C"  void IncrementalSliderController_OnEndDrag_m818793602 (IncrementalSliderController_t153649975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
