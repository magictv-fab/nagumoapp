﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollSnapBase
struct ScrollSnapBase_t1686927404;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeStartEvent
struct SelectionChangeStartEvent_t2472858916;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent
struct SelectionPageChangedEvent_t2882117233;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent
struct SelectionChangeEndEvent_t16447307;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll2472858916.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll2882117233.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_ScrollSn16447307.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::.ctor()
extern "C"  void ScrollSnapBase__ctor_m1466054396 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::get_CurrentPage()
extern "C"  int32_t ScrollSnapBase_get_CurrentPage_m2995762827 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::set_CurrentPage(System.Int32)
extern "C"  void ScrollSnapBase_set_CurrentPage_m929507126 (ScrollSnapBase_t1686927404 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeStartEvent UnityEngine.UI.Extensions.ScrollSnapBase::get_OnSelectionChangeStartEvent()
extern "C"  SelectionChangeStartEvent_t2472858916 * ScrollSnapBase_get_OnSelectionChangeStartEvent_m1924975789 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::set_OnSelectionChangeStartEvent(UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeStartEvent)
extern "C"  void ScrollSnapBase_set_OnSelectionChangeStartEvent_m3929393998 (ScrollSnapBase_t1686927404 * __this, SelectionChangeStartEvent_t2472858916 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent UnityEngine.UI.Extensions.ScrollSnapBase::get_OnSelectionPageChangedEvent()
extern "C"  SelectionPageChangedEvent_t2882117233 * ScrollSnapBase_get_OnSelectionPageChangedEvent_m3873950855 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::set_OnSelectionPageChangedEvent(UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent)
extern "C"  void ScrollSnapBase_set_OnSelectionPageChangedEvent_m2751635054 (ScrollSnapBase_t1686927404 * __this, SelectionPageChangedEvent_t2882117233 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent UnityEngine.UI.Extensions.ScrollSnapBase::get_OnSelectionChangeEndEvent()
extern "C"  SelectionChangeEndEvent_t16447307 * ScrollSnapBase_get_OnSelectionChangeEndEvent_m3948364603 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::set_OnSelectionChangeEndEvent(UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent)
extern "C"  void ScrollSnapBase_set_OnSelectionChangeEndEvent_m1307486190 (ScrollSnapBase_t1686927404 * __this, SelectionChangeEndEvent_t16447307 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::Awake()
extern "C"  void ScrollSnapBase_Awake_m1703659615 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::InitialiseChildObjectsFromScene()
extern "C"  void ScrollSnapBase_InitialiseChildObjectsFromScene_m3888435501 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::InitialiseChildObjectsFromArray()
extern "C"  void ScrollSnapBase_InitialiseChildObjectsFromArray_m1239346106 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::UpdateVisible()
extern "C"  void ScrollSnapBase_UpdateVisible_m2479867971 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::NextScreen()
extern "C"  void ScrollSnapBase_NextScreen_m3207239431 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::PreviousScreen()
extern "C"  void ScrollSnapBase_PreviousScreen_m1642260363 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::GoToScreen(System.Int32)
extern "C"  void ScrollSnapBase_GoToScreen_m222102632 (ScrollSnapBase_t1686927404 * __this, int32_t ___screenIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.Extensions.ScrollSnapBase::GetPageforPosition(UnityEngine.Vector3)
extern "C"  int32_t ScrollSnapBase_GetPageforPosition_m1182614838 (ScrollSnapBase_t1686927404 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.ScrollSnapBase::IsRectSettledOnaPage(UnityEngine.Vector3)
extern "C"  bool ScrollSnapBase_IsRectSettledOnaPage_m662757239 (ScrollSnapBase_t1686927404 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::GetPositionforPage(System.Int32,UnityEngine.Vector3&)
extern "C"  void ScrollSnapBase_GetPositionforPage_m1571707975 (ScrollSnapBase_t1686927404 * __this, int32_t ___page0, Vector3_t4282066566 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::ScrollToClosestElement()
extern "C"  void ScrollSnapBase_ScrollToClosestElement_m4192206579 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::ChangeBulletsInfo(System.Int32)
extern "C"  void ScrollSnapBase_ChangeBulletsInfo_m1765316826 (ScrollSnapBase_t1686927404 * __this, int32_t ___targetScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::OnValidate()
extern "C"  void ScrollSnapBase_OnValidate_m3663626333 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::StartScreenChange()
extern "C"  void ScrollSnapBase_StartScreenChange_m2025839576 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::ScreenChange()
extern "C"  void ScrollSnapBase_ScreenChange_m1432342532 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::EndScreenChange()
extern "C"  void ScrollSnapBase_EndScreenChange_m120089297 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnapBase_OnBeginDrag_m708285510 (ScrollSnapBase_t1686927404 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnapBase_OnDrag_m311292899 (ScrollSnapBase_t1686927404 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::<Awake>m__8A()
extern "C"  void ScrollSnapBase_U3CAwakeU3Em__8A_m2953336489 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnapBase::<Awake>m__8B()
extern "C"  void ScrollSnapBase_U3CAwakeU3Em__8B_m2953337450 (ScrollSnapBase_t1686927404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
