﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertBoolToFloat
struct  ConvertBoolToFloat_t316188428  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ConvertBoolToFloat::boolVariable
	FsmBool_t1075959796 * ___boolVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertBoolToFloat::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertBoolToFloat::falseValue
	FsmFloat_t2134102846 * ___falseValue_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertBoolToFloat::trueValue
	FsmFloat_t2134102846 * ___trueValue_12;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertBoolToFloat::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_boolVariable_9() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t316188428, ___boolVariable_9)); }
	inline FsmBool_t1075959796 * get_boolVariable_9() const { return ___boolVariable_9; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_9() { return &___boolVariable_9; }
	inline void set_boolVariable_9(FsmBool_t1075959796 * value)
	{
		___boolVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_9, value);
	}

	inline static int32_t get_offset_of_floatVariable_10() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t316188428, ___floatVariable_10)); }
	inline FsmFloat_t2134102846 * get_floatVariable_10() const { return ___floatVariable_10; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_10() { return &___floatVariable_10; }
	inline void set_floatVariable_10(FsmFloat_t2134102846 * value)
	{
		___floatVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_10, value);
	}

	inline static int32_t get_offset_of_falseValue_11() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t316188428, ___falseValue_11)); }
	inline FsmFloat_t2134102846 * get_falseValue_11() const { return ___falseValue_11; }
	inline FsmFloat_t2134102846 ** get_address_of_falseValue_11() { return &___falseValue_11; }
	inline void set_falseValue_11(FsmFloat_t2134102846 * value)
	{
		___falseValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___falseValue_11, value);
	}

	inline static int32_t get_offset_of_trueValue_12() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t316188428, ___trueValue_12)); }
	inline FsmFloat_t2134102846 * get_trueValue_12() const { return ___trueValue_12; }
	inline FsmFloat_t2134102846 ** get_address_of_trueValue_12() { return &___trueValue_12; }
	inline void set_trueValue_12(FsmFloat_t2134102846 * value)
	{
		___trueValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___trueValue_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t316188428, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
