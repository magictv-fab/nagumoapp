﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRaycastHitInfo
struct  GetRaycastHitInfo_t1078696316  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRaycastHitInfo::gameObjectHit
	FsmGameObject_t1697147867 * ___gameObjectHit_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetRaycastHitInfo::point
	FsmVector3_t533912882 * ___point_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetRaycastHitInfo::normal
	FsmVector3_t533912882 * ___normal_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRaycastHitInfo::distance
	FsmFloat_t2134102846 * ___distance_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRaycastHitInfo::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_gameObjectHit_9() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1078696316, ___gameObjectHit_9)); }
	inline FsmGameObject_t1697147867 * get_gameObjectHit_9() const { return ___gameObjectHit_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObjectHit_9() { return &___gameObjectHit_9; }
	inline void set_gameObjectHit_9(FsmGameObject_t1697147867 * value)
	{
		___gameObjectHit_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectHit_9, value);
	}

	inline static int32_t get_offset_of_point_10() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1078696316, ___point_10)); }
	inline FsmVector3_t533912882 * get_point_10() const { return ___point_10; }
	inline FsmVector3_t533912882 ** get_address_of_point_10() { return &___point_10; }
	inline void set_point_10(FsmVector3_t533912882 * value)
	{
		___point_10 = value;
		Il2CppCodeGenWriteBarrier(&___point_10, value);
	}

	inline static int32_t get_offset_of_normal_11() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1078696316, ___normal_11)); }
	inline FsmVector3_t533912882 * get_normal_11() const { return ___normal_11; }
	inline FsmVector3_t533912882 ** get_address_of_normal_11() { return &___normal_11; }
	inline void set_normal_11(FsmVector3_t533912882 * value)
	{
		___normal_11 = value;
		Il2CppCodeGenWriteBarrier(&___normal_11, value);
	}

	inline static int32_t get_offset_of_distance_12() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1078696316, ___distance_12)); }
	inline FsmFloat_t2134102846 * get_distance_12() const { return ___distance_12; }
	inline FsmFloat_t2134102846 ** get_address_of_distance_12() { return &___distance_12; }
	inline void set_distance_12(FsmFloat_t2134102846 * value)
	{
		___distance_12 = value;
		Il2CppCodeGenWriteBarrier(&___distance_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1078696316, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
