﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Collections.Generic.IEqualityComparer`1<MagicTV.globals.Perspective>
struct IEqualityComparer_1_t1435588206;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<MagicTV.globals.Perspective>
struct ICollection_1_t1539143789;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>[]
struct KeyValuePair_2U5BU5D_t2038243499;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>
struct IEnumerator_1_t26562551;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>
struct KeyCollection_t4137643543;
// System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>
struct ValueCollection_t1211489805;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3828207484.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m429752480_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m429752480(__this, method) ((  void (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2__ctor_m429752480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3888088855_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3888088855(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3888088855_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1104102897_gshared (Dictionary_2_t2510884092 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1104102897(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2510884092 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1104102897_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1937707489_gshared (Dictionary_2_t2510884092 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1937707489(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2510884092 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1937707489_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2429041518_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2429041518(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2429041518_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3473670502_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3473670502(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3473670502_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3375318228_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3375318228(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3375318228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1882524737_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1882524737(__this, method) ((  bool (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1882524737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3008587416_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3008587416(__this, method) ((  bool (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3008587416_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3243576616_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3243576616(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3243576616_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m187178125_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m187178125(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m187178125_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3663183268_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3663183268(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3663183268_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m4271175506_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m4271175506(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m4271175506_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2729922763_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2729922763(__this, ___key0, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2729922763_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2418428226_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2418428226(__this, method) ((  bool (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2418428226_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1152683374_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1152683374(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1152683374_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3788229382_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3788229382(__this, method) ((  bool (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3788229382_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4073708513_gshared (Dictionary_2_t2510884092 * __this, KeyValuePair_2_t2409664798  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4073708513(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2510884092 *, KeyValuePair_2_t2409664798 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4073708513_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3720243073_gshared (Dictionary_2_t2510884092 * __this, KeyValuePair_2_t2409664798  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3720243073(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2510884092 *, KeyValuePair_2_t2409664798 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3720243073_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3465278853_gshared (Dictionary_2_t2510884092 * __this, KeyValuePair_2U5BU5D_t2038243499* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3465278853(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2510884092 *, KeyValuePair_2U5BU5D_t2038243499*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3465278853_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m554859238_gshared (Dictionary_2_t2510884092 * __this, KeyValuePair_2_t2409664798  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m554859238(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2510884092 *, KeyValuePair_2_t2409664798 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m554859238_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2685276324_gshared (Dictionary_2_t2510884092 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2685276324(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2685276324_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m973889823_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m973889823(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m973889823_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2687040668_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2687040668(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2687040668_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m354097975_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m354097975(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m354097975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2636101896_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2636101896(__this, method) ((  int32_t (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_get_Count_m2636101896_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1786267363_gshared (Dictionary_2_t2510884092 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1786267363(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1786267363_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1091837536_gshared (Dictionary_2_t2510884092 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1091837536(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2510884092 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1091837536_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1769822232_gshared (Dictionary_2_t2510884092 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1769822232(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2510884092 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1769822232_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m112848543_gshared (Dictionary_2_t2510884092 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m112848543(__this, ___size0, method) ((  void (*) (Dictionary_2_t2510884092 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m112848543_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3867887707_gshared (Dictionary_2_t2510884092 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3867887707(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3867887707_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2409664798  Dictionary_2_make_pair_m360721575_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m360721575(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2409664798  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m360721575_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2646156655_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2646156655(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2646156655_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2792075503_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2792075503(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2792075503_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2152868628_gshared (Dictionary_2_t2510884092 * __this, KeyValuePair_2U5BU5D_t2038243499* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2152868628(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2510884092 *, KeyValuePair_2U5BU5D_t2038243499*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2152868628_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m495221656_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m495221656(__this, method) ((  void (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_Resize_m495221656_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m964716565_gshared (Dictionary_2_t2510884092 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m964716565(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2510884092 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m964716565_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2130853067_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2130853067(__this, method) ((  void (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_Clear_m2130853067_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2132183921_gshared (Dictionary_2_t2510884092 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2132183921(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2510884092 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2132183921_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2775367409_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2775367409(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2775367409_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3762612286_gshared (Dictionary_2_t2510884092 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3762612286(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2510884092 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3762612286_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1852462310_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1852462310(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1852462310_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3521809119_gshared (Dictionary_2_t2510884092 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3521809119(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2510884092 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3521809119_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3005014730_gshared (Dictionary_2_t2510884092 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3005014730(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2510884092 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3005014730_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::get_Keys()
extern "C"  KeyCollection_t4137643543 * Dictionary_2_get_Keys_m268167541_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m268167541(__this, method) ((  KeyCollection_t4137643543 * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_get_Keys_m268167541_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::get_Values()
extern "C"  ValueCollection_t1211489805 * Dictionary_2_get_Values_m2293301557_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2293301557(__this, method) ((  ValueCollection_t1211489805 * (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_get_Values_m2293301557_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2096015562_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2096015562(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2096015562_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1104389642_gshared (Dictionary_2_t2510884092 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1104389642(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t2510884092 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1104389642_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3028631746_gshared (Dictionary_2_t2510884092 * __this, KeyValuePair_2_t2409664798  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3028631746(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2510884092 *, KeyValuePair_2_t2409664798 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3028631746_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3828207484  Dictionary_2_GetEnumerator_m1196320229_gshared (Dictionary_2_t2510884092 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1196320229(__this, method) ((  Enumerator_t3828207484  (*) (Dictionary_2_t2510884092 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1196320229_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m1152735794_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m1152735794(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m1152735794_gshared)(__this /* static, unused */, ___key0, ___value1, method)
