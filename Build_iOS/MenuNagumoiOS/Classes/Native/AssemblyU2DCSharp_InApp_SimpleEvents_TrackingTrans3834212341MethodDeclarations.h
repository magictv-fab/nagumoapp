﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.TrackingTransformConfig
struct TrackingTransformConfig_t3834212341;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void InApp.SimpleEvents.TrackingTransformConfig::.ctor()
extern "C"  void TrackingTransformConfig__ctor_m826749109 (TrackingTransformConfig_t3834212341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.SimpleEvents.TrackingTransformConfig InApp.SimpleEvents.TrackingTransformConfig::GetInstance()
extern "C"  TrackingTransformConfig_t3834212341 * TrackingTransformConfig_GetInstance_m377926119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.TrackingTransformConfig::Start()
extern "C"  void TrackingTransformConfig_Start_m4068854197 (TrackingTransformConfig_t3834212341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.TrackingTransformConfig::configByString(System.String)
extern "C"  void TrackingTransformConfig_configByString_m232567081 (TrackingTransformConfig_t3834212341 * __this, String_t* ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::cloneVector3(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TrackingTransformConfig_cloneVector3_m217554717 (TrackingTransformConfig_t3834212341 * __this, Vector3_t4282066566  ___vec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.TrackingTransformConfig::Reset()
extern "C"  void TrackingTransformConfig_Reset_m2768149346 (TrackingTransformConfig_t3834212341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
