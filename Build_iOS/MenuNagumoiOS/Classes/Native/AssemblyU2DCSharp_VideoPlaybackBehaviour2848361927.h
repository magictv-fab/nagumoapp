﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// VideoPlayerHelper
struct VideoPlayerHelper_t638710250;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaType3131497273.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaState2586048626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoPlaybackBehaviour
struct  VideoPlaybackBehaviour_t2848361927  : public MonoBehaviour_t667441552
{
public:
	// System.String VideoPlaybackBehaviour::m_path
	String_t* ___m_path_2;
	// UnityEngine.Texture VideoPlaybackBehaviour::m_playTexture
	Texture_t2526458961 * ___m_playTexture_3;
	// UnityEngine.Texture VideoPlaybackBehaviour::m_busyTexture
	Texture_t2526458961 * ___m_busyTexture_4;
	// UnityEngine.Texture VideoPlaybackBehaviour::m_errorTexture
	Texture_t2526458961 * ___m_errorTexture_5;
	// System.Boolean VideoPlaybackBehaviour::m_autoPlay
	bool ___m_autoPlay_6;
	// VideoPlayerHelper VideoPlaybackBehaviour::mVideoPlayer
	VideoPlayerHelper_t638710250 * ___mVideoPlayer_7;
	// System.Boolean VideoPlaybackBehaviour::mIsInited
	bool ___mIsInited_8;
	// System.Boolean VideoPlaybackBehaviour::mIsPrepared
	bool ___mIsPrepared_9;
	// System.Boolean VideoPlaybackBehaviour::mAppPaused
	bool ___mAppPaused_10;
	// UnityEngine.Texture2D VideoPlaybackBehaviour::mVideoTexture
	Texture2D_t3884108195 * ___mVideoTexture_11;
	// UnityEngine.Texture VideoPlaybackBehaviour::mKeyframeTexture
	Texture_t2526458961 * ___mKeyframeTexture_12;
	// VideoPlayerHelper/MediaType VideoPlaybackBehaviour::mMediaType
	int32_t ___mMediaType_13;
	// VideoPlayerHelper/MediaState VideoPlaybackBehaviour::mCurrentState
	int32_t ___mCurrentState_14;
	// System.Single VideoPlaybackBehaviour::mSeekPosition
	float ___mSeekPosition_15;
	// System.Boolean VideoPlaybackBehaviour::isPlayableOnTexture
	bool ___isPlayableOnTexture_16;
	// UnityEngine.GameObject VideoPlaybackBehaviour::mIconPlane
	GameObject_t3674682005 * ___mIconPlane_17;
	// System.Boolean VideoPlaybackBehaviour::mIconPlaneActive
	bool ___mIconPlaneActive_18;

public:
	inline static int32_t get_offset_of_m_path_2() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___m_path_2)); }
	inline String_t* get_m_path_2() const { return ___m_path_2; }
	inline String_t** get_address_of_m_path_2() { return &___m_path_2; }
	inline void set_m_path_2(String_t* value)
	{
		___m_path_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_path_2, value);
	}

	inline static int32_t get_offset_of_m_playTexture_3() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___m_playTexture_3)); }
	inline Texture_t2526458961 * get_m_playTexture_3() const { return ___m_playTexture_3; }
	inline Texture_t2526458961 ** get_address_of_m_playTexture_3() { return &___m_playTexture_3; }
	inline void set_m_playTexture_3(Texture_t2526458961 * value)
	{
		___m_playTexture_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_playTexture_3, value);
	}

	inline static int32_t get_offset_of_m_busyTexture_4() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___m_busyTexture_4)); }
	inline Texture_t2526458961 * get_m_busyTexture_4() const { return ___m_busyTexture_4; }
	inline Texture_t2526458961 ** get_address_of_m_busyTexture_4() { return &___m_busyTexture_4; }
	inline void set_m_busyTexture_4(Texture_t2526458961 * value)
	{
		___m_busyTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_busyTexture_4, value);
	}

	inline static int32_t get_offset_of_m_errorTexture_5() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___m_errorTexture_5)); }
	inline Texture_t2526458961 * get_m_errorTexture_5() const { return ___m_errorTexture_5; }
	inline Texture_t2526458961 ** get_address_of_m_errorTexture_5() { return &___m_errorTexture_5; }
	inline void set_m_errorTexture_5(Texture_t2526458961 * value)
	{
		___m_errorTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_errorTexture_5, value);
	}

	inline static int32_t get_offset_of_m_autoPlay_6() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___m_autoPlay_6)); }
	inline bool get_m_autoPlay_6() const { return ___m_autoPlay_6; }
	inline bool* get_address_of_m_autoPlay_6() { return &___m_autoPlay_6; }
	inline void set_m_autoPlay_6(bool value)
	{
		___m_autoPlay_6 = value;
	}

	inline static int32_t get_offset_of_mVideoPlayer_7() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mVideoPlayer_7)); }
	inline VideoPlayerHelper_t638710250 * get_mVideoPlayer_7() const { return ___mVideoPlayer_7; }
	inline VideoPlayerHelper_t638710250 ** get_address_of_mVideoPlayer_7() { return &___mVideoPlayer_7; }
	inline void set_mVideoPlayer_7(VideoPlayerHelper_t638710250 * value)
	{
		___mVideoPlayer_7 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoPlayer_7, value);
	}

	inline static int32_t get_offset_of_mIsInited_8() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mIsInited_8)); }
	inline bool get_mIsInited_8() const { return ___mIsInited_8; }
	inline bool* get_address_of_mIsInited_8() { return &___mIsInited_8; }
	inline void set_mIsInited_8(bool value)
	{
		___mIsInited_8 = value;
	}

	inline static int32_t get_offset_of_mIsPrepared_9() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mIsPrepared_9)); }
	inline bool get_mIsPrepared_9() const { return ___mIsPrepared_9; }
	inline bool* get_address_of_mIsPrepared_9() { return &___mIsPrepared_9; }
	inline void set_mIsPrepared_9(bool value)
	{
		___mIsPrepared_9 = value;
	}

	inline static int32_t get_offset_of_mAppPaused_10() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mAppPaused_10)); }
	inline bool get_mAppPaused_10() const { return ___mAppPaused_10; }
	inline bool* get_address_of_mAppPaused_10() { return &___mAppPaused_10; }
	inline void set_mAppPaused_10(bool value)
	{
		___mAppPaused_10 = value;
	}

	inline static int32_t get_offset_of_mVideoTexture_11() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mVideoTexture_11)); }
	inline Texture2D_t3884108195 * get_mVideoTexture_11() const { return ___mVideoTexture_11; }
	inline Texture2D_t3884108195 ** get_address_of_mVideoTexture_11() { return &___mVideoTexture_11; }
	inline void set_mVideoTexture_11(Texture2D_t3884108195 * value)
	{
		___mVideoTexture_11 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoTexture_11, value);
	}

	inline static int32_t get_offset_of_mKeyframeTexture_12() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mKeyframeTexture_12)); }
	inline Texture_t2526458961 * get_mKeyframeTexture_12() const { return ___mKeyframeTexture_12; }
	inline Texture_t2526458961 ** get_address_of_mKeyframeTexture_12() { return &___mKeyframeTexture_12; }
	inline void set_mKeyframeTexture_12(Texture_t2526458961 * value)
	{
		___mKeyframeTexture_12 = value;
		Il2CppCodeGenWriteBarrier(&___mKeyframeTexture_12, value);
	}

	inline static int32_t get_offset_of_mMediaType_13() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mMediaType_13)); }
	inline int32_t get_mMediaType_13() const { return ___mMediaType_13; }
	inline int32_t* get_address_of_mMediaType_13() { return &___mMediaType_13; }
	inline void set_mMediaType_13(int32_t value)
	{
		___mMediaType_13 = value;
	}

	inline static int32_t get_offset_of_mCurrentState_14() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mCurrentState_14)); }
	inline int32_t get_mCurrentState_14() const { return ___mCurrentState_14; }
	inline int32_t* get_address_of_mCurrentState_14() { return &___mCurrentState_14; }
	inline void set_mCurrentState_14(int32_t value)
	{
		___mCurrentState_14 = value;
	}

	inline static int32_t get_offset_of_mSeekPosition_15() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mSeekPosition_15)); }
	inline float get_mSeekPosition_15() const { return ___mSeekPosition_15; }
	inline float* get_address_of_mSeekPosition_15() { return &___mSeekPosition_15; }
	inline void set_mSeekPosition_15(float value)
	{
		___mSeekPosition_15 = value;
	}

	inline static int32_t get_offset_of_isPlayableOnTexture_16() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___isPlayableOnTexture_16)); }
	inline bool get_isPlayableOnTexture_16() const { return ___isPlayableOnTexture_16; }
	inline bool* get_address_of_isPlayableOnTexture_16() { return &___isPlayableOnTexture_16; }
	inline void set_isPlayableOnTexture_16(bool value)
	{
		___isPlayableOnTexture_16 = value;
	}

	inline static int32_t get_offset_of_mIconPlane_17() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mIconPlane_17)); }
	inline GameObject_t3674682005 * get_mIconPlane_17() const { return ___mIconPlane_17; }
	inline GameObject_t3674682005 ** get_address_of_mIconPlane_17() { return &___mIconPlane_17; }
	inline void set_mIconPlane_17(GameObject_t3674682005 * value)
	{
		___mIconPlane_17 = value;
		Il2CppCodeGenWriteBarrier(&___mIconPlane_17, value);
	}

	inline static int32_t get_offset_of_mIconPlaneActive_18() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t2848361927, ___mIconPlaneActive_18)); }
	inline bool get_mIconPlaneActive_18() const { return ___mIconPlaneActive_18; }
	inline bool* get_address_of_mIconPlaneActive_18() { return &___mIconPlaneActive_18; }
	inline void set_mIconPlaneActive_18(bool value)
	{
		___mIconPlaneActive_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
