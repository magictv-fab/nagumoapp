﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InApp.SimpleEvents.CameraDistanceConfig
struct CameraDistanceConfig_t2003187004;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.CameraDistanceConfig
struct  CameraDistanceConfig_t2003187004  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// UnityEngine.Camera InApp.SimpleEvents.CameraDistanceConfig::mainCamera
	Camera_t2727095145 * ___mainCamera_3;
	// System.Single InApp.SimpleEvents.CameraDistanceConfig::cameraNear
	float ___cameraNear_4;
	// System.Single InApp.SimpleEvents.CameraDistanceConfig::cameraFar
	float ___cameraFar_5;

public:
	inline static int32_t get_offset_of_mainCamera_3() { return static_cast<int32_t>(offsetof(CameraDistanceConfig_t2003187004, ___mainCamera_3)); }
	inline Camera_t2727095145 * get_mainCamera_3() const { return ___mainCamera_3; }
	inline Camera_t2727095145 ** get_address_of_mainCamera_3() { return &___mainCamera_3; }
	inline void set_mainCamera_3(Camera_t2727095145 * value)
	{
		___mainCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_3, value);
	}

	inline static int32_t get_offset_of_cameraNear_4() { return static_cast<int32_t>(offsetof(CameraDistanceConfig_t2003187004, ___cameraNear_4)); }
	inline float get_cameraNear_4() const { return ___cameraNear_4; }
	inline float* get_address_of_cameraNear_4() { return &___cameraNear_4; }
	inline void set_cameraNear_4(float value)
	{
		___cameraNear_4 = value;
	}

	inline static int32_t get_offset_of_cameraFar_5() { return static_cast<int32_t>(offsetof(CameraDistanceConfig_t2003187004, ___cameraFar_5)); }
	inline float get_cameraFar_5() const { return ___cameraFar_5; }
	inline float* get_address_of_cameraFar_5() { return &___cameraFar_5; }
	inline void set_cameraFar_5(float value)
	{
		___cameraFar_5 = value;
	}
};

struct CameraDistanceConfig_t2003187004_StaticFields
{
public:
	// InApp.SimpleEvents.CameraDistanceConfig InApp.SimpleEvents.CameraDistanceConfig::_instance
	CameraDistanceConfig_t2003187004 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(CameraDistanceConfig_t2003187004_StaticFields, ____instance_2)); }
	inline CameraDistanceConfig_t2003187004 * get__instance_2() const { return ____instance_2; }
	inline CameraDistanceConfig_t2003187004 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(CameraDistanceConfig_t2003187004 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
