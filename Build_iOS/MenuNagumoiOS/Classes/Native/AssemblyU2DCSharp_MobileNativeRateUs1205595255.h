﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<MNDialogResult>
struct Action_1_t3521133710;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcherB1248907224.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileNativeRateUs
struct  MobileNativeRateUs_t1205595255  : public EventDispatcherBase_t1248907224
{
public:
	// System.String MobileNativeRateUs::title
	String_t* ___title_2;
	// System.String MobileNativeRateUs::message
	String_t* ___message_3;
	// System.String MobileNativeRateUs::yes
	String_t* ___yes_4;
	// System.String MobileNativeRateUs::later
	String_t* ___later_5;
	// System.String MobileNativeRateUs::no
	String_t* ___no_6;
	// System.String MobileNativeRateUs::url
	String_t* ___url_7;
	// System.String MobileNativeRateUs::appleId
	String_t* ___appleId_8;
	// System.Action`1<MNDialogResult> MobileNativeRateUs::OnComplete
	Action_1_t3521133710 * ___OnComplete_9;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___message_3)); }
	inline String_t* get_message_3() const { return ___message_3; }
	inline String_t** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(String_t* value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier(&___message_3, value);
	}

	inline static int32_t get_offset_of_yes_4() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___yes_4)); }
	inline String_t* get_yes_4() const { return ___yes_4; }
	inline String_t** get_address_of_yes_4() { return &___yes_4; }
	inline void set_yes_4(String_t* value)
	{
		___yes_4 = value;
		Il2CppCodeGenWriteBarrier(&___yes_4, value);
	}

	inline static int32_t get_offset_of_later_5() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___later_5)); }
	inline String_t* get_later_5() const { return ___later_5; }
	inline String_t** get_address_of_later_5() { return &___later_5; }
	inline void set_later_5(String_t* value)
	{
		___later_5 = value;
		Il2CppCodeGenWriteBarrier(&___later_5, value);
	}

	inline static int32_t get_offset_of_no_6() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___no_6)); }
	inline String_t* get_no_6() const { return ___no_6; }
	inline String_t** get_address_of_no_6() { return &___no_6; }
	inline void set_no_6(String_t* value)
	{
		___no_6 = value;
		Il2CppCodeGenWriteBarrier(&___no_6, value);
	}

	inline static int32_t get_offset_of_url_7() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___url_7)); }
	inline String_t* get_url_7() const { return ___url_7; }
	inline String_t** get_address_of_url_7() { return &___url_7; }
	inline void set_url_7(String_t* value)
	{
		___url_7 = value;
		Il2CppCodeGenWriteBarrier(&___url_7, value);
	}

	inline static int32_t get_offset_of_appleId_8() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___appleId_8)); }
	inline String_t* get_appleId_8() const { return ___appleId_8; }
	inline String_t** get_address_of_appleId_8() { return &___appleId_8; }
	inline void set_appleId_8(String_t* value)
	{
		___appleId_8 = value;
		Il2CppCodeGenWriteBarrier(&___appleId_8, value);
	}

	inline static int32_t get_offset_of_OnComplete_9() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255, ___OnComplete_9)); }
	inline Action_1_t3521133710 * get_OnComplete_9() const { return ___OnComplete_9; }
	inline Action_1_t3521133710 ** get_address_of_OnComplete_9() { return &___OnComplete_9; }
	inline void set_OnComplete_9(Action_1_t3521133710 * value)
	{
		___OnComplete_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnComplete_9, value);
	}
};

struct MobileNativeRateUs_t1205595255_StaticFields
{
public:
	// System.Action`1<MNDialogResult> MobileNativeRateUs::<>f__am$cache8
	Action_1_t3521133710 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(MobileNativeRateUs_t1205595255_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_1_t3521133710 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_1_t3521133710 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_1_t3521133710 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
