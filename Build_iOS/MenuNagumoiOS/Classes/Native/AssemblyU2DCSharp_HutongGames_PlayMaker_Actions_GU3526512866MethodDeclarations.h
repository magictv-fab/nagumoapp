﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginVertical
struct GUILayoutBeginVertical_t3526512866;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::.ctor()
extern "C"  void GUILayoutBeginVertical__ctor_m3448831428 (GUILayoutBeginVertical_t3526512866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::Reset()
extern "C"  void GUILayoutBeginVertical_Reset_m1095264369 (GUILayoutBeginVertical_t3526512866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::OnGUI()
extern "C"  void GUILayoutBeginVertical_OnGUI_m2944230078 (GUILayoutBeginVertical_t3526512866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
