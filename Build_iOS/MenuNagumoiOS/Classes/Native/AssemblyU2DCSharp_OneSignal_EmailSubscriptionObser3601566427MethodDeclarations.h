﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/EmailSubscriptionObservable
struct EmailSubscriptionObservable_t3601566427;
// System.Object
struct Il2CppObject;
// OSEmailSubscriptionStateChanges
struct OSEmailSubscriptionStateChanges_t721307367;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_OSEmailSubscriptionStateChanges721307367.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/EmailSubscriptionObservable::.ctor(System.Object,System.IntPtr)
extern "C"  void EmailSubscriptionObservable__ctor_m2419450162 (EmailSubscriptionObservable_t3601566427 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/EmailSubscriptionObservable::Invoke(OSEmailSubscriptionStateChanges)
extern "C"  void EmailSubscriptionObservable_Invoke_m3308892639 (EmailSubscriptionObservable_t3601566427 * __this, OSEmailSubscriptionStateChanges_t721307367 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/EmailSubscriptionObservable::BeginInvoke(OSEmailSubscriptionStateChanges,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EmailSubscriptionObservable_BeginInvoke_m125418170 (EmailSubscriptionObservable_t3601566427 * __this, OSEmailSubscriptionStateChanges_t721307367 * ___stateChanges0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/EmailSubscriptionObservable::EndInvoke(System.IAsyncResult)
extern "C"  void EmailSubscriptionObservable_EndInvoke_m1207811138 (EmailSubscriptionObservable_t3601566427 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
