﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.Detector/Point
struct Point_t330760921;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.Aztec.Internal.Detector/Point::get_X()
extern "C"  int32_t Point_get_X_m2181929357 (Point_t330760921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Detector/Point::set_X(System.Int32)
extern "C"  void Point_set_X_m2802039580 (Point_t330760921 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Detector/Point::get_Y()
extern "C"  int32_t Point_get_Y_m2181930318 (Point_t330760921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Detector/Point::set_Y(System.Int32)
extern "C"  void Point_set_Y_m14624093 (Point_t330760921 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.Aztec.Internal.Detector/Point::toResultPoint()
extern "C"  ResultPoint_t1538592853 * Point_toResultPoint_m2125562226 (Point_t330760921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Detector/Point::.ctor(System.Int32,System.Int32)
extern "C"  void Point__ctor_m3874459476 (Point_t330760921 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Aztec.Internal.Detector/Point::ToString()
extern "C"  String_t* Point_ToString_m2186693979 (Point_t330760921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
