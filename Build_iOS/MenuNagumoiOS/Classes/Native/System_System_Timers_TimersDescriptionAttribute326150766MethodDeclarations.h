﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Timers.TimersDescriptionAttribute
struct TimersDescriptionAttribute_t326150766;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void System.Timers.TimersDescriptionAttribute::.ctor(System.String)
extern "C"  void TimersDescriptionAttribute__ctor_m3539056341 (TimersDescriptionAttribute_t326150766 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Timers.TimersDescriptionAttribute::get_Description()
extern "C"  String_t* TimersDescriptionAttribute_get_Description_m2591192443 (TimersDescriptionAttribute_t326150766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
