﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Color32LuminanceSource
struct Color32LuminanceSource_t1298990919;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.Color32LuminanceSource::.ctor(System.Int32,System.Int32)
extern "C"  void Color32LuminanceSource__ctor_m526375110 (Color32LuminanceSource_t1298990919 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Color32LuminanceSource::.ctor(UnityEngine.Color32[],System.Int32,System.Int32)
extern "C"  void Color32LuminanceSource__ctor_m3100710021 (Color32LuminanceSource_t1298990919 * __this, Color32U5BU5D_t2960766953* ___color32s0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Color32LuminanceSource::SetPixels(UnityEngine.Color32[])
extern "C"  void Color32LuminanceSource_SetPixels_m701343992 (Color32LuminanceSource_t1298990919 * __this, Color32U5BU5D_t2960766953* ___color32s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.Color32LuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern "C"  LuminanceSource_t1231523093 * Color32LuminanceSource_CreateLuminanceSource_m1196122846 (Color32LuminanceSource_t1298990919 * __this, ByteU5BU5D_t4260760469* ___newLuminances0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
