﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableFSM
struct  EnableFSM_t3773848587  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableFSM::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableFSM::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableFSM::enable
	FsmBool_t1075959796 * ___enable_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableFSM::resetOnExit
	FsmBool_t1075959796 * ___resetOnExit_12;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.EnableFSM::fsmComponent
	PlayMakerFSM_t3799847376 * ___fsmComponent_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(EnableFSM_t3773848587, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(EnableFSM_t3773848587, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_enable_11() { return static_cast<int32_t>(offsetof(EnableFSM_t3773848587, ___enable_11)); }
	inline FsmBool_t1075959796 * get_enable_11() const { return ___enable_11; }
	inline FsmBool_t1075959796 ** get_address_of_enable_11() { return &___enable_11; }
	inline void set_enable_11(FsmBool_t1075959796 * value)
	{
		___enable_11 = value;
		Il2CppCodeGenWriteBarrier(&___enable_11, value);
	}

	inline static int32_t get_offset_of_resetOnExit_12() { return static_cast<int32_t>(offsetof(EnableFSM_t3773848587, ___resetOnExit_12)); }
	inline FsmBool_t1075959796 * get_resetOnExit_12() const { return ___resetOnExit_12; }
	inline FsmBool_t1075959796 ** get_address_of_resetOnExit_12() { return &___resetOnExit_12; }
	inline void set_resetOnExit_12(FsmBool_t1075959796 * value)
	{
		___resetOnExit_12 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_12, value);
	}

	inline static int32_t get_offset_of_fsmComponent_13() { return static_cast<int32_t>(offsetof(EnableFSM_t3773848587, ___fsmComponent_13)); }
	inline PlayMakerFSM_t3799847376 * get_fsmComponent_13() const { return ___fsmComponent_13; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsmComponent_13() { return &___fsmComponent_13; }
	inline void set_fsmComponent_13(PlayMakerFSM_t3799847376 * value)
	{
		___fsmComponent_13 = value;
		Il2CppCodeGenWriteBarrier(&___fsmComponent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
