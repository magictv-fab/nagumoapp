﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask
struct DataMask_t1708385874;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Void ZXing.QrCode.Internal.DataMask::.ctor()
extern "C"  void DataMask__ctor_m2261077482 (DataMask_t1708385874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask::unmaskBitMatrix(ZXing.Common.BitMatrix,System.Int32)
extern "C"  void DataMask_unmaskBitMatrix_m628786587 (DataMask_t1708385874 * __this, BitMatrix_t1058711404 * ___bits0, int32_t ___dimension1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.DataMask ZXing.QrCode.Internal.DataMask::forReference(System.Int32)
extern "C"  DataMask_t1708385874 * DataMask_forReference_m482955577 (Il2CppObject * __this /* static, unused */, int32_t ___reference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask::.cctor()
extern "C"  void DataMask__cctor_m891828995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
