﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoAcougue/<ISendStatus>c__Iterator6E
struct  U3CISendStatusU3Ec__Iterator6E_t3429446286  : public Il2CppObject
{
public:
	// System.String PedidoAcougue/<ISendStatus>c__Iterator6E::id
	String_t* ___id_0;
	// System.Int32 PedidoAcougue/<ISendStatus>c__Iterator6E::status
	int32_t ___status_1;
	// System.String PedidoAcougue/<ISendStatus>c__Iterator6E::<json>__0
	String_t* ___U3CjsonU3E__0_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PedidoAcougue/<ISendStatus>c__Iterator6E::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_3;
	// System.Byte[] PedidoAcougue/<ISendStatus>c__Iterator6E::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_4;
	// UnityEngine.WWW PedidoAcougue/<ISendStatus>c__Iterator6E::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_5;
	// System.Int32 PedidoAcougue/<ISendStatus>c__Iterator6E::$PC
	int32_t ___U24PC_6;
	// System.Object PedidoAcougue/<ISendStatus>c__Iterator6E::$current
	Il2CppObject * ___U24current_7;
	// System.String PedidoAcougue/<ISendStatus>c__Iterator6E::<$>id
	String_t* ___U3CU24U3Eid_8;
	// System.Int32 PedidoAcougue/<ISendStatus>c__Iterator6E::<$>status
	int32_t ___U3CU24U3Estatus_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___status_1)); }
	inline int32_t get_status_1() const { return ___status_1; }
	inline int32_t* get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(int32_t value)
	{
		___status_1 = value;
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U3CjsonU3E__0_2)); }
	inline String_t* get_U3CjsonU3E__0_2() const { return ___U3CjsonU3E__0_2; }
	inline String_t** get_address_of_U3CjsonU3E__0_2() { return &___U3CjsonU3E__0_2; }
	inline void set_U3CjsonU3E__0_2(String_t* value)
	{
		___U3CjsonU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_3() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U3CheadersU3E__1_3)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_3() const { return ___U3CheadersU3E__1_3; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_3() { return &___U3CheadersU3E__1_3; }
	inline void set_U3CheadersU3E__1_3(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_4() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U3CpDataU3E__2_4)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_4() const { return ___U3CpDataU3E__2_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_4() { return &___U3CpDataU3E__2_4; }
	inline void set_U3CpDataU3E__2_4(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_5() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U3CwwwU3E__3_5)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_5() const { return ___U3CwwwU3E__3_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_5() { return &___U3CwwwU3E__3_5; }
	inline void set_U3CwwwU3E__3_5(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eid_8() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U3CU24U3Eid_8)); }
	inline String_t* get_U3CU24U3Eid_8() const { return ___U3CU24U3Eid_8; }
	inline String_t** get_address_of_U3CU24U3Eid_8() { return &___U3CU24U3Eid_8; }
	inline void set_U3CU24U3Eid_8(String_t* value)
	{
		___U3CU24U3Eid_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eid_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Estatus_9() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__Iterator6E_t3429446286, ___U3CU24U3Estatus_9)); }
	inline int32_t get_U3CU24U3Estatus_9() const { return ___U3CU24U3Estatus_9; }
	inline int32_t* get_address_of_U3CU24U3Estatus_9() { return &___U3CU24U3Estatus_9; }
	inline void set_U3CU24U3Estatus_9(int32_t value)
	{
		___U3CU24U3Estatus_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
