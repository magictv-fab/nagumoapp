﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputController
struct InputController_t2664602342;

#include "codegen/il2cpp-codegen.h"

// System.Void InputController::.ctor()
extern "C"  void InputController__ctor_m1738411845 (InputController_t2664602342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::.cctor()
extern "C"  void InputController__cctor_m1869063432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::UpdateInput()
extern "C"  void InputController_UpdateInput_m3495889700 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
