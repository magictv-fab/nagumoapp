﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1702563344.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetBackgroundColor
struct  SetBackgroundColor_t3440566907  : public ComponentAction_1_t1702563344
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetBackgroundColor::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetBackgroundColor::backgroundColor
	FsmColor_t2131419205 * ___backgroundColor_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetBackgroundColor::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t3440566907, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_backgroundColor_12() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t3440566907, ___backgroundColor_12)); }
	inline FsmColor_t2131419205 * get_backgroundColor_12() const { return ___backgroundColor_12; }
	inline FsmColor_t2131419205 ** get_address_of_backgroundColor_12() { return &___backgroundColor_12; }
	inline void set_backgroundColor_12(FsmColor_t2131419205 * value)
	{
		___backgroundColor_12 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundColor_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t3440566907, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
