﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// updateCupomTabloid
struct updateCupomTabloid_t933712416;

#include "codegen/il2cpp-codegen.h"

// System.Void updateCupomTabloid::.ctor()
extern "C"  void updateCupomTabloid__ctor_m1539904123 (updateCupomTabloid_t933712416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void updateCupomTabloid::Awake()
extern "C"  void updateCupomTabloid_Awake_m1777509342 (updateCupomTabloid_t933712416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void updateCupomTabloid::Start()
extern "C"  void updateCupomTabloid_Start_m487041915 (updateCupomTabloid_t933712416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void updateCupomTabloid::Update()
extern "C"  void updateCupomTabloid_Update_m2219249650 (updateCupomTabloid_t933712416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
