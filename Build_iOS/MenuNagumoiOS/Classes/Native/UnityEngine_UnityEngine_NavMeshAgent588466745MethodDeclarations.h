﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.NavMeshAgent::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_velocity_m2183624243 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_velocity_m2849407365 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
