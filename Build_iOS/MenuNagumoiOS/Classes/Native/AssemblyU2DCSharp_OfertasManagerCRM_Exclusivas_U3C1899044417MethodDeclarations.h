﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9
struct U3CILoadOfertasU3Ec__Iterator9_t1899044417;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9::.ctor()
extern "C"  void U3CILoadOfertasU3Ec__Iterator9__ctor_m3297712714 (U3CILoadOfertasU3Ec__Iterator9_t1899044417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3945456200 (U3CILoadOfertasU3Ec__Iterator9_t1899044417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m62551516 (U3CILoadOfertasU3Ec__Iterator9_t1899044417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9::MoveNext()
extern "C"  bool U3CILoadOfertasU3Ec__Iterator9_MoveNext_m647611242 (U3CILoadOfertasU3Ec__Iterator9_t1899044417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9::Dispose()
extern "C"  void U3CILoadOfertasU3Ec__Iterator9_Dispose_m3609304071 (U3CILoadOfertasU3Ec__Iterator9_t1899044417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<ILoadOfertas>c__Iterator9::Reset()
extern "C"  void U3CILoadOfertasU3Ec__Iterator9_Reset_m944145655 (U3CILoadOfertasU3Ec__Iterator9_t1899044417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
