﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>
struct ShimEnumerator_t3434915512;
// System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>
struct Dictionary_2_t3719137485;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3225053257_gshared (ShimEnumerator_t3434915512 * __this, Dictionary_2_t3719137485 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3225053257(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3434915512 *, Dictionary_2_t3719137485 *, const MethodInfo*))ShimEnumerator__ctor_m3225053257_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2965846396_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2965846396(__this, method) ((  bool (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))ShimEnumerator_MoveNext_m2965846396_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2814820398_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2814820398(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))ShimEnumerator_get_Entry_m2814820398_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m925275117_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m925275117(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))ShimEnumerator_get_Key_m925275117_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1179593535_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1179593535(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))ShimEnumerator_get_Value_m1179593535_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1142711815_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1142711815(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))ShimEnumerator_get_Current_m1142711815_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3603604763_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3603604763(__this, method) ((  void (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))ShimEnumerator_Reset_m3603604763_gshared)(__this, method)
