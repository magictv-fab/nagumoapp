﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// AMR.device.DeviceAppStateMachineEvents
struct DeviceAppStateMachineEvents_t922942516;
// AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler
struct OnPauseEventHandler_t3609693858;
// AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler
struct OnAwakeEventHandler_t1806304177;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AMR.device.DeviceAppStateMachineEvents
struct  DeviceAppStateMachineEvents_t922942516  : public MonoBehaviour_t667441552
{
public:
	// AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler AMR.device.DeviceAppStateMachineEvents::onPause
	OnPauseEventHandler_t3609693858 * ___onPause_5;
	// AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler AMR.device.DeviceAppStateMachineEvents::onResume
	OnPauseEventHandler_t3609693858 * ___onResume_6;
	// AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler AMR.device.DeviceAppStateMachineEvents::onAwake
	OnAwakeEventHandler_t1806304177 * ___onAwake_7;
	// System.Int32 AMR.device.DeviceAppStateMachineEvents::_IOSPauseToleranceInMilliseconds
	int32_t ____IOSPauseToleranceInMilliseconds_8;
	// System.Boolean AMR.device.DeviceAppStateMachineEvents::_currentFocus
	bool ____currentFocus_9;
	// System.DateTime AMR.device.DeviceAppStateMachineEvents::_pauseTime
	DateTime_t4283661327  ____pauseTime_10;

public:
	inline static int32_t get_offset_of_onPause_5() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516, ___onPause_5)); }
	inline OnPauseEventHandler_t3609693858 * get_onPause_5() const { return ___onPause_5; }
	inline OnPauseEventHandler_t3609693858 ** get_address_of_onPause_5() { return &___onPause_5; }
	inline void set_onPause_5(OnPauseEventHandler_t3609693858 * value)
	{
		___onPause_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPause_5, value);
	}

	inline static int32_t get_offset_of_onResume_6() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516, ___onResume_6)); }
	inline OnPauseEventHandler_t3609693858 * get_onResume_6() const { return ___onResume_6; }
	inline OnPauseEventHandler_t3609693858 ** get_address_of_onResume_6() { return &___onResume_6; }
	inline void set_onResume_6(OnPauseEventHandler_t3609693858 * value)
	{
		___onResume_6 = value;
		Il2CppCodeGenWriteBarrier(&___onResume_6, value);
	}

	inline static int32_t get_offset_of_onAwake_7() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516, ___onAwake_7)); }
	inline OnAwakeEventHandler_t1806304177 * get_onAwake_7() const { return ___onAwake_7; }
	inline OnAwakeEventHandler_t1806304177 ** get_address_of_onAwake_7() { return &___onAwake_7; }
	inline void set_onAwake_7(OnAwakeEventHandler_t1806304177 * value)
	{
		___onAwake_7 = value;
		Il2CppCodeGenWriteBarrier(&___onAwake_7, value);
	}

	inline static int32_t get_offset_of__IOSPauseToleranceInMilliseconds_8() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516, ____IOSPauseToleranceInMilliseconds_8)); }
	inline int32_t get__IOSPauseToleranceInMilliseconds_8() const { return ____IOSPauseToleranceInMilliseconds_8; }
	inline int32_t* get_address_of__IOSPauseToleranceInMilliseconds_8() { return &____IOSPauseToleranceInMilliseconds_8; }
	inline void set__IOSPauseToleranceInMilliseconds_8(int32_t value)
	{
		____IOSPauseToleranceInMilliseconds_8 = value;
	}

	inline static int32_t get_offset_of__currentFocus_9() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516, ____currentFocus_9)); }
	inline bool get__currentFocus_9() const { return ____currentFocus_9; }
	inline bool* get_address_of__currentFocus_9() { return &____currentFocus_9; }
	inline void set__currentFocus_9(bool value)
	{
		____currentFocus_9 = value;
	}

	inline static int32_t get_offset_of__pauseTime_10() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516, ____pauseTime_10)); }
	inline DateTime_t4283661327  get__pauseTime_10() const { return ____pauseTime_10; }
	inline DateTime_t4283661327 * get_address_of__pauseTime_10() { return &____pauseTime_10; }
	inline void set__pauseTime_10(DateTime_t4283661327  value)
	{
		____pauseTime_10 = value;
	}
};

struct DeviceAppStateMachineEvents_t922942516_StaticFields
{
public:
	// UnityEngine.GameObject AMR.device.DeviceAppStateMachineEvents::_selfGameObject
	GameObject_t3674682005 * ____selfGameObject_3;
	// AMR.device.DeviceAppStateMachineEvents AMR.device.DeviceAppStateMachineEvents::_selfInstance
	DeviceAppStateMachineEvents_t922942516 * ____selfInstance_4;

public:
	inline static int32_t get_offset_of__selfGameObject_3() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516_StaticFields, ____selfGameObject_3)); }
	inline GameObject_t3674682005 * get__selfGameObject_3() const { return ____selfGameObject_3; }
	inline GameObject_t3674682005 ** get_address_of__selfGameObject_3() { return &____selfGameObject_3; }
	inline void set__selfGameObject_3(GameObject_t3674682005 * value)
	{
		____selfGameObject_3 = value;
		Il2CppCodeGenWriteBarrier(&____selfGameObject_3, value);
	}

	inline static int32_t get_offset_of__selfInstance_4() { return static_cast<int32_t>(offsetof(DeviceAppStateMachineEvents_t922942516_StaticFields, ____selfInstance_4)); }
	inline DeviceAppStateMachineEvents_t922942516 * get__selfInstance_4() const { return ____selfInstance_4; }
	inline DeviceAppStateMachineEvents_t922942516 ** get_address_of__selfInstance_4() { return &____selfInstance_4; }
	inline void set__selfInstance_4(DeviceAppStateMachineEvents_t922942516 * value)
	{
		____selfInstance_4 = value;
		Il2CppCodeGenWriteBarrier(&____selfInstance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
