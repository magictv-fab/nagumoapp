﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos
struct  NetworkGetNetworkDisconnectionInfos_t2164373997  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::disconnectionLabel
	FsmString_t952858651 * ___disconnectionLabel_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::lostConnectionEvent
	FsmEvent_t2133468028 * ___lostConnectionEvent_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::disConnectedEvent
	FsmEvent_t2133468028 * ___disConnectedEvent_11;

public:
	inline static int32_t get_offset_of_disconnectionLabel_9() { return static_cast<int32_t>(offsetof(NetworkGetNetworkDisconnectionInfos_t2164373997, ___disconnectionLabel_9)); }
	inline FsmString_t952858651 * get_disconnectionLabel_9() const { return ___disconnectionLabel_9; }
	inline FsmString_t952858651 ** get_address_of_disconnectionLabel_9() { return &___disconnectionLabel_9; }
	inline void set_disconnectionLabel_9(FsmString_t952858651 * value)
	{
		___disconnectionLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___disconnectionLabel_9, value);
	}

	inline static int32_t get_offset_of_lostConnectionEvent_10() { return static_cast<int32_t>(offsetof(NetworkGetNetworkDisconnectionInfos_t2164373997, ___lostConnectionEvent_10)); }
	inline FsmEvent_t2133468028 * get_lostConnectionEvent_10() const { return ___lostConnectionEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_lostConnectionEvent_10() { return &___lostConnectionEvent_10; }
	inline void set_lostConnectionEvent_10(FsmEvent_t2133468028 * value)
	{
		___lostConnectionEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___lostConnectionEvent_10, value);
	}

	inline static int32_t get_offset_of_disConnectedEvent_11() { return static_cast<int32_t>(offsetof(NetworkGetNetworkDisconnectionInfos_t2164373997, ___disConnectedEvent_11)); }
	inline FsmEvent_t2133468028 * get_disConnectedEvent_11() const { return ___disConnectedEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_disConnectedEvent_11() { return &___disConnectedEvent_11; }
	inline void set_disConnectedEvent_11(FsmEvent_t2133468028 * value)
	{
		___disConnectedEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___disConnectedEvent_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
