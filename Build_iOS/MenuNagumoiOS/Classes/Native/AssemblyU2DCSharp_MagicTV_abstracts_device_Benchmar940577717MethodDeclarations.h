﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.device.BenchmarkInfo
struct BenchmarkInfo_t940577717;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"

// System.Void MagicTV.abstracts.device.BenchmarkInfo::.ctor(System.Int32)
extern "C"  void BenchmarkInfo__ctor_m2215991549 (BenchmarkInfo_t940577717 * __this, int32_t ___quality0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MagicTV.abstracts.device.BenchmarkInfo::get_quality()
extern "C"  int32_t BenchmarkInfo_get_quality_m2542476914 (BenchmarkInfo_t940577717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.abstracts.device.BenchmarkInfo::FilterFilesByQuality(MagicTV.vo.FileVO)
extern "C"  UrlInfoVO_t1761987528 * BenchmarkInfo_FilterFilesByQuality_m1545191049 (BenchmarkInfo_t940577717 * __this, FileVO_t1935348659 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.abstracts.device.BenchmarkInfo::filterZip(MagicTV.vo.UrlInfoVO)
extern "C"  UrlInfoVO_t1761987528 * BenchmarkInfo_filterZip_m3066730454 (BenchmarkInfo_t940577717 * __this, UrlInfoVO_t1761987528 * ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
