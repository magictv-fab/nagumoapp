﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
struct GetAnimatorIsParameterControlledByCurve_t1394563734;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::.ctor()
extern "C"  void GetAnimatorIsParameterControlledByCurve__ctor_m1539452256 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::Reset()
extern "C"  void GetAnimatorIsParameterControlledByCurve_Reset_m3480852493 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::OnEnter()
extern "C"  void GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::DoCheckIsParameterControlledByCurve()
extern "C"  void GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
