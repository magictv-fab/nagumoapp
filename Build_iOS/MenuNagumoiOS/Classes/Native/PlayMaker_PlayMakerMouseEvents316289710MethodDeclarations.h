﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerMouseEvents
struct PlayMakerMouseEvents_t316289710;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerMouseEvents::OnMouseEnter()
extern "C"  void PlayMakerMouseEvents_OnMouseEnter_m479540455 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseDown()
extern "C"  void PlayMakerMouseEvents_OnMouseDown_m403501141 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseUp()
extern "C"  void PlayMakerMouseEvents_OnMouseUp_m121595022 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseExit()
extern "C"  void PlayMakerMouseEvents_OnMouseExit_m440030673 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseDrag()
extern "C"  void PlayMakerMouseEvents_OnMouseDrag_m405609575 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseOver()
extern "C"  void PlayMakerMouseEvents_OnMouseOver_m724354055 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::.ctor()
extern "C"  void PlayMakerMouseEvents__ctor_m2803383919 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
