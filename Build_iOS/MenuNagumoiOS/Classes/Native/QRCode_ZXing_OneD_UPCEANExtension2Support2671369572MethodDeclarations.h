﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCEANExtension2Support
struct UPCEANExtension2Support_t2671369572;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>
struct IDictionary_2_t2712902339;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_String7231557.h"

// ZXing.Result ZXing.OneD.UPCEANExtension2Support::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[])
extern "C"  Result_t2610723219 * UPCEANExtension2Support_decodeRow_m1822532346 (UPCEANExtension2Support_t2671369572 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Int32U5BU5D_t3230847821* ___extensionStartRange2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.UPCEANExtension2Support::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern "C"  int32_t UPCEANExtension2Support_decodeMiddle_m3533888146 (UPCEANExtension2Support_t2671369572 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___startRange1, StringBuilder_t243639308 * ___resultString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.OneD.UPCEANExtension2Support::parseExtensionString(System.String)
extern "C"  Il2CppObject* UPCEANExtension2Support_parseExtensionString_m2388474869 (Il2CppObject * __this /* static, unused */, String_t* ___raw0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANExtension2Support::.ctor()
extern "C"  void UPCEANExtension2Support__ctor_m654707631 (UPCEANExtension2Support_t2671369572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
