﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInAppListControllerScript
struct MagicTVWindowInAppListControllerScript_t1120695362;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowInAppListControllerScript::.ctor()
extern "C"  void MagicTVWindowInAppListControllerScript__ctor_m1928739481 (MagicTVWindowInAppListControllerScript_t1120695362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppListControllerScript::StarClick()
extern "C"  void MagicTVWindowInAppListControllerScript_StarClick_m4155246989 (MagicTVWindowInAppListControllerScript_t1120695362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
