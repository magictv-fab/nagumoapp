﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveUpdate
struct iTweenMoveUpdate_t733362340;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::.ctor()
extern "C"  void iTweenMoveUpdate__ctor_m1221468738 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::Reset()
extern "C"  void iTweenMoveUpdate_Reset_m3162868975 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnEnter()
extern "C"  void iTweenMoveUpdate_OnEnter_m697114009 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnUpdate()
extern "C"  void iTweenMoveUpdate_OnUpdate_m3564224458 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnExit()
extern "C"  void iTweenMoveUpdate_OnExit_m724143839 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::DoiTween()
extern "C"  void iTweenMoveUpdate_DoiTween_m1586830031 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
