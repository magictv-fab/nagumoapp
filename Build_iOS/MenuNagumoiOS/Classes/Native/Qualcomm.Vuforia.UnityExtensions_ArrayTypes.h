﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t2475465524;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t1613618350;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t796991165;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t465046465;
// Vuforia.VirtualButton
struct VirtualButton_t704206407;
// Vuforia.Image
struct Image_t2247677317;
// Vuforia.Trackable
struct Trackable_t3781061455;
// Vuforia.DataSetImpl
struct DataSetImpl_t1837478850;
// Vuforia.DataSet
struct DataSet_t2095838082;
// Vuforia.Marker
struct Marker_t616694972;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;
// Vuforia.IEditorTrackableBehaviour
struct IEditorTrackableBehaviour_t1840126232;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t1276145027;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t1860057025;
// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t2826579942;
// Vuforia.WordResult
struct WordResult_t1079862857;
// Vuforia.Word
struct Word_t2165514892;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t545947899;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t678501447;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t2222092879;
// Vuforia.Surface
struct Surface_t3094389719;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t142368528;
// Vuforia.Prop
struct Prop_t2165309157;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t1293468098;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t865486027;
// Vuforia.IEditorMarkerBehaviour
struct IEditorMarkerBehaviour_t1845962767;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_t3541475369;
// Vuforia.DataSetTrackableBehaviour
struct DataSetTrackableBehaviour_t3340678586;
// Vuforia.IEditorDataSetTrackableBehaviour
struct IEditorDataSetTrackableBehaviour_t189565564;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t1191238304;
// Vuforia.IEditorVirtualButtonBehaviour
struct IEditorVirtualButtonBehaviour_t4082570784;
// Vuforia.ImageTarget
struct ImageTarget_t3520455670;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t3566668545;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t4010260306;
// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t2608219151;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t155716687;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t2660219000;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t2952053798;
// Vuforia.ObjectTracker
struct ObjectTracker_t455954211;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t2347335889;
// Vuforia.IEditorImageTargetBehaviour
struct IEditorImageTargetBehaviour_t4106486993;

#include "mscorlib_System_Array1146569071.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E3010462567.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBack2475465524.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3609410114.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcess465046465.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBut704206407.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXE354375056.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image2247677317.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetIm1837478850.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1548845516.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1826866697.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa3919795561.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1737958143.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan526813605.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru1860057025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2826579942.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Rectangle2265684451.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResul1079862857.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstra545947899.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbs142368528.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstr1293468098.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbst865486027.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTr3340678586.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1191238304.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan459380335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro1961690790.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Backgroun2608219151.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac455954211.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2347335889.h"

#pragma once
// Vuforia.Eyewear/EyewearCalibrationReading[]
struct EyewearCalibrationReadingU5BU5D_t4186969886  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EyewearCalibrationReading_t3010462567  m_Items[1];

public:
	inline EyewearCalibrationReading_t3010462567  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EyewearCalibrationReading_t3010462567 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EyewearCalibrationReading_t3010462567  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct VideoBackgroundAbstractBehaviourU5BU5D_t1950854525  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VideoBackgroundAbstractBehaviour_t2475465524 * m_Items[1];

public:
	inline VideoBackgroundAbstractBehaviour_t2475465524 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VideoBackgroundAbstractBehaviour_t2475465524 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VideoBackgroundAbstractBehaviour_t2475465524 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t3603817691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t159049136  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t1920244343  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TargetSearchResult_t3609410114  m_Items[1];

public:
	inline TargetSearchResult_t3609410114  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TargetSearchResult_t3609410114 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TargetSearchResult_t3609410114  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t3294375964  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HideExcessAreaAbstractBehaviour_t465046465 * m_Items[1];

public:
	inline HideExcessAreaAbstractBehaviour_t465046465 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HideExcessAreaAbstractBehaviour_t465046465 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HideExcessAreaAbstractBehaviour_t465046465 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3057368254  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VirtualButton_t704206407 * m_Items[1];

public:
	inline VirtualButton_t704206407 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualButton_t704206407 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualButton_t704206407 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t882070065  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.Image[]
struct ImageU5BU5D_t585028296  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Image_t2247677317 * m_Items[1];

public:
	inline Image_t2247677317 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Image_t2247677317 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Image_t2247677317 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.Trackable[]
struct TrackableU5BU5D_t4208183958  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t278235895  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataSetImpl_t1837478850 * m_Items[1];

public:
	inline DataSetImpl_t1837478850 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataSetImpl_t1837478850 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataSetImpl_t1837478850 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.DataSet[]
struct DataSetU5BU5D_t853906487  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataSet_t2095838082 * m_Items[1];

public:
	inline DataSet_t2095838082 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataSet_t2095838082 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataSet_t2095838082 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.Marker[]
struct MarkerU5BU5D_t1754334421  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t1273933373  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrackableResultData_t395876724  m_Items[1];

public:
	inline TrackableResultData_t395876724  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TrackableResultData_t395876724 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TrackableResultData_t395876724  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VuforiaManagerImpl/WordData[]
struct WordDataU5BU5D_t2668962181  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WordData_t1548845516  m_Items[1];

public:
	inline WordData_t1548845516  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WordData_t1548845516 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WordData_t1548845516  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VuforiaManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t2868255668  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WordResultData_t1826866697  m_Items[1];

public:
	inline WordResultData_t1826866697  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WordResultData_t1826866697 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WordResultData_t1826866697  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t525370068  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SmartTerrainRevisionData_t3919795561  m_Items[1];

public:
	inline SmartTerrainRevisionData_t3919795561  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SmartTerrainRevisionData_t3919795561 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SmartTerrainRevisionData_t3919795561  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VuforiaManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t1400262182  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SurfaceData_t1737958143  m_Items[1];

public:
	inline SurfaceData_t1737958143  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SurfaceData_t1737958143 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SurfaceData_t1737958143  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.VuforiaManagerImpl/PropData[]
struct PropDataU5BU5D_t921971240  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropData_t526813605  m_Items[1];

public:
	inline PropData_t526813605  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropData_t526813605 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropData_t526813605  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t1519469759  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TrackableBehaviour_t4179556250 * m_Items[1];

public:
	inline TrackableBehaviour_t4179556250 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TrackableBehaviour_t4179556250 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TrackableBehaviour_t4179556250 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IEditorTrackableBehaviour[]
struct IEditorTrackableBehaviourU5BU5D_t557885321  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3175650578  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t98292764  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReconstructionAbstractBehaviour_t1860057025 * m_Items[1];

public:
	inline ReconstructionAbstractBehaviour_t1860057025 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReconstructionAbstractBehaviour_t1860057025 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReconstructionAbstractBehaviour_t1860057025 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.SmartTerrainTrackableBehaviour[]
struct SmartTerrainTrackableBehaviourU5BU5D_t1634895299  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SmartTerrainTrackableBehaviour_t2826579942 * m_Items[1];

public:
	inline SmartTerrainTrackableBehaviour_t2826579942 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SmartTerrainTrackableBehaviour_t2826579942 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SmartTerrainTrackableBehaviour_t2826579942 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.RectangleData[]
struct RectangleDataU5BU5D_t4194061106  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RectangleData_t2265684451  m_Items[1];

public:
	inline RectangleData_t2265684451  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RectangleData_t2265684451 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RectangleData_t2265684451  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.WordResult[]
struct WordResultU5BU5D_t166376052  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WordResult_t1079862857 * m_Items[1];

public:
	inline WordResult_t1079862857 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WordResult_t1079862857 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WordResult_t1079862857 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.Word[]
struct WordU5BU5D_t2482185669  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t3845729466  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WordAbstractBehaviour_t545947899 * m_Items[1];

public:
	inline WordAbstractBehaviour_t545947899 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WordAbstractBehaviour_t545947899 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WordAbstractBehaviour_t545947899 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t2382241470  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t4284729238  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.Surface[]
struct SurfaceU5BU5D_t2483042030  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.SurfaceAbstractBehaviour[]
struct SurfaceAbstractBehaviourU5BU5D_t2351788721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SurfaceAbstractBehaviour_t142368528 * m_Items[1];

public:
	inline SurfaceAbstractBehaviour_t142368528 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SurfaceAbstractBehaviour_t142368528 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SurfaceAbstractBehaviour_t142368528 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.Prop[]
struct PropU5BU5D_t2141059048  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.PropAbstractBehaviour[]
struct PropAbstractBehaviourU5BU5D_t238978295  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropAbstractBehaviour_t1293468098 * m_Items[1];

public:
	inline PropAbstractBehaviour_t1293468098 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropAbstractBehaviour_t1293468098 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropAbstractBehaviour_t1293468098 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.MarkerAbstractBehaviour[]
struct MarkerAbstractBehaviourU5BU5D_t2605928362  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MarkerAbstractBehaviour_t865486027 * m_Items[1];

public:
	inline MarkerAbstractBehaviour_t865486027 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MarkerAbstractBehaviour_t865486027 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MarkerAbstractBehaviour_t865486027 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IEditorMarkerBehaviour[]
struct IEditorMarkerBehaviourU5BU5D_t1780400854  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WorldCenterTrackableBehaviour[]
struct WorldCenterTrackableBehaviourU5BU5D_t3384734996  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.DataSetTrackableBehaviour[]
struct DataSetTrackableBehaviourU5BU5D_t2515085855  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataSetTrackableBehaviour_t3340678586 * m_Items[1];

public:
	inline DataSetTrackableBehaviour_t3340678586 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataSetTrackableBehaviour_t3340678586 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataSetTrackableBehaviour_t3340678586 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct IEditorDataSetTrackableBehaviourU5BU5D_t3879006229  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t1213482721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VirtualButtonAbstractBehaviour_t1191238304 * m_Items[1];

public:
	inline VirtualButtonAbstractBehaviour_t1191238304 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualButtonAbstractBehaviour_t1191238304 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualButtonAbstractBehaviour_t1191238304 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IEditorVirtualButtonBehaviour[]
struct IEditorVirtualButtonBehaviourU5BU5D_t1656761697  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.VuforiaManagerImpl/VirtualButtonData[]
struct VirtualButtonDataU5BU5D_t974384886  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VirtualButtonData_t459380335  m_Items[1];

public:
	inline VirtualButtonData_t459380335  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualButtonData_t459380335 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualButtonData_t459380335  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.ImageTarget[]
struct ImageTargetU5BU5D_t1652010867  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t2870328323  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProfileData_t1961690790  m_Items[1];

public:
	inline ProfileData_t1961690790  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProfileData_t1961690790 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProfileData_t1961690790  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t1294404572  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t1538242343  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct BackgroundPlaneAbstractBehaviourU5BU5D_t4024678102  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BackgroundPlaneAbstractBehaviour_t2608219151 * m_Items[1];

public:
	inline BackgroundPlaneAbstractBehaviour_t2608219151 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BackgroundPlaneAbstractBehaviour_t2608219151 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BackgroundPlaneAbstractBehaviour_t2608219151 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t831686038  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t3705585321  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t530569347  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ObjectTracker[]
struct ObjectTrackerU5BU5D_t134535410  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectTracker_t455954211 * m_Items[1];

public:
	inline ObjectTracker_t455954211 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjectTracker_t455954211 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjectTracker_t455954211 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ImageTargetAbstractBehaviour[]
struct ImageTargetAbstractBehaviourU5BU5D_t476017868  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ImageTargetAbstractBehaviour_t2347335889 * m_Items[1];

public:
	inline ImageTargetAbstractBehaviour_t2347335889 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImageTargetAbstractBehaviour_t2347335889 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImageTargetAbstractBehaviour_t2347335889 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.IEditorImageTargetBehaviour[]
struct IEditorImageTargetBehaviourU5BU5D_t4275462348  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
