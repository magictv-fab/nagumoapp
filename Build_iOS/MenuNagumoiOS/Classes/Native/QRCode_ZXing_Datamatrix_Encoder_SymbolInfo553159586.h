﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Datamatrix.Encoder.SymbolInfo[]
struct SymbolInfoU5BU5D_t2435839895;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Encoder.SymbolInfo
struct  SymbolInfo_t553159586  : public Il2CppObject
{
public:
	// System.Boolean ZXing.Datamatrix.Encoder.SymbolInfo::rectangular
	bool ___rectangular_2;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::dataCapacity
	int32_t ___dataCapacity_3;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::errorCodewords
	int32_t ___errorCodewords_4;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::matrixWidth
	int32_t ___matrixWidth_5;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::matrixHeight
	int32_t ___matrixHeight_6;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::dataRegions
	int32_t ___dataRegions_7;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::rsBlockData
	int32_t ___rsBlockData_8;
	// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::rsBlockError
	int32_t ___rsBlockError_9;

public:
	inline static int32_t get_offset_of_rectangular_2() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___rectangular_2)); }
	inline bool get_rectangular_2() const { return ___rectangular_2; }
	inline bool* get_address_of_rectangular_2() { return &___rectangular_2; }
	inline void set_rectangular_2(bool value)
	{
		___rectangular_2 = value;
	}

	inline static int32_t get_offset_of_dataCapacity_3() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___dataCapacity_3)); }
	inline int32_t get_dataCapacity_3() const { return ___dataCapacity_3; }
	inline int32_t* get_address_of_dataCapacity_3() { return &___dataCapacity_3; }
	inline void set_dataCapacity_3(int32_t value)
	{
		___dataCapacity_3 = value;
	}

	inline static int32_t get_offset_of_errorCodewords_4() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___errorCodewords_4)); }
	inline int32_t get_errorCodewords_4() const { return ___errorCodewords_4; }
	inline int32_t* get_address_of_errorCodewords_4() { return &___errorCodewords_4; }
	inline void set_errorCodewords_4(int32_t value)
	{
		___errorCodewords_4 = value;
	}

	inline static int32_t get_offset_of_matrixWidth_5() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___matrixWidth_5)); }
	inline int32_t get_matrixWidth_5() const { return ___matrixWidth_5; }
	inline int32_t* get_address_of_matrixWidth_5() { return &___matrixWidth_5; }
	inline void set_matrixWidth_5(int32_t value)
	{
		___matrixWidth_5 = value;
	}

	inline static int32_t get_offset_of_matrixHeight_6() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___matrixHeight_6)); }
	inline int32_t get_matrixHeight_6() const { return ___matrixHeight_6; }
	inline int32_t* get_address_of_matrixHeight_6() { return &___matrixHeight_6; }
	inline void set_matrixHeight_6(int32_t value)
	{
		___matrixHeight_6 = value;
	}

	inline static int32_t get_offset_of_dataRegions_7() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___dataRegions_7)); }
	inline int32_t get_dataRegions_7() const { return ___dataRegions_7; }
	inline int32_t* get_address_of_dataRegions_7() { return &___dataRegions_7; }
	inline void set_dataRegions_7(int32_t value)
	{
		___dataRegions_7 = value;
	}

	inline static int32_t get_offset_of_rsBlockData_8() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___rsBlockData_8)); }
	inline int32_t get_rsBlockData_8() const { return ___rsBlockData_8; }
	inline int32_t* get_address_of_rsBlockData_8() { return &___rsBlockData_8; }
	inline void set_rsBlockData_8(int32_t value)
	{
		___rsBlockData_8 = value;
	}

	inline static int32_t get_offset_of_rsBlockError_9() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586, ___rsBlockError_9)); }
	inline int32_t get_rsBlockError_9() const { return ___rsBlockError_9; }
	inline int32_t* get_address_of_rsBlockError_9() { return &___rsBlockError_9; }
	inline void set_rsBlockError_9(int32_t value)
	{
		___rsBlockError_9 = value;
	}
};

struct SymbolInfo_t553159586_StaticFields
{
public:
	// ZXing.Datamatrix.Encoder.SymbolInfo[] ZXing.Datamatrix.Encoder.SymbolInfo::PROD_SYMBOLS
	SymbolInfoU5BU5D_t2435839895* ___PROD_SYMBOLS_0;
	// ZXing.Datamatrix.Encoder.SymbolInfo[] ZXing.Datamatrix.Encoder.SymbolInfo::symbols
	SymbolInfoU5BU5D_t2435839895* ___symbols_1;

public:
	inline static int32_t get_offset_of_PROD_SYMBOLS_0() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586_StaticFields, ___PROD_SYMBOLS_0)); }
	inline SymbolInfoU5BU5D_t2435839895* get_PROD_SYMBOLS_0() const { return ___PROD_SYMBOLS_0; }
	inline SymbolInfoU5BU5D_t2435839895** get_address_of_PROD_SYMBOLS_0() { return &___PROD_SYMBOLS_0; }
	inline void set_PROD_SYMBOLS_0(SymbolInfoU5BU5D_t2435839895* value)
	{
		___PROD_SYMBOLS_0 = value;
		Il2CppCodeGenWriteBarrier(&___PROD_SYMBOLS_0, value);
	}

	inline static int32_t get_offset_of_symbols_1() { return static_cast<int32_t>(offsetof(SymbolInfo_t553159586_StaticFields, ___symbols_1)); }
	inline SymbolInfoU5BU5D_t2435839895* get_symbols_1() const { return ___symbols_1; }
	inline SymbolInfoU5BU5D_t2435839895** get_address_of_symbols_1() { return &___symbols_1; }
	inline void set_symbols_1(SymbolInfoU5BU5D_t2435839895* value)
	{
		___symbols_1 = value;
		Il2CppCodeGenWriteBarrier(&___symbols_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
