﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoAcougue/<ILoadImgs>c__Iterator70
struct U3CILoadImgsU3Ec__Iterator70_t2033076116;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoAcougue/<ILoadImgs>c__Iterator70::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator70__ctor_m2708996999 (U3CILoadImgsU3Ec__Iterator70_t2033076116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<ILoadImgs>c__Iterator70::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator70_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m920236277 (U3CILoadImgsU3Ec__Iterator70_t2033076116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<ILoadImgs>c__Iterator70::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator70_System_Collections_IEnumerator_get_Current_m1563311753 (U3CILoadImgsU3Ec__Iterator70_t2033076116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PedidoAcougue/<ILoadImgs>c__Iterator70::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator70_MoveNext_m2331937269 (U3CILoadImgsU3Ec__Iterator70_t2033076116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<ILoadImgs>c__Iterator70::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator70_Dispose_m494217732 (U3CILoadImgsU3Ec__Iterator70_t2033076116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<ILoadImgs>c__Iterator70::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator70_Reset_m355429940 (U3CILoadImgsU3Ec__Iterator70_t2033076116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
