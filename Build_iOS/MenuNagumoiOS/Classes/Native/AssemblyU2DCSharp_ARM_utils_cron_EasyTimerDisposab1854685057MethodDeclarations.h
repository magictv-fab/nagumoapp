﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.cron.EasyTimerDisposable/<SetTimeout>c__AnonStorey93
struct U3CSetTimeoutU3Ec__AnonStorey93_t1854685057;
// System.Object
struct Il2CppObject;
// System.Timers.ElapsedEventArgs
struct ElapsedEventArgs_t2035959611;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_System_Timers_ElapsedEventArgs2035959611.h"

// System.Void ARM.utils.cron.EasyTimerDisposable/<SetTimeout>c__AnonStorey93::.ctor()
extern "C"  void U3CSetTimeoutU3Ec__AnonStorey93__ctor_m548462138 (U3CSetTimeoutU3Ec__AnonStorey93_t1854685057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.EasyTimerDisposable/<SetTimeout>c__AnonStorey93::<>m__17(System.Object,System.Timers.ElapsedEventArgs)
extern "C"  void U3CSetTimeoutU3Ec__AnonStorey93_U3CU3Em__17_m700502751 (U3CSetTimeoutU3Ec__AnonStorey93_t1854685057 * __this, Il2CppObject * ___source0, ElapsedEventArgs_t2035959611 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
