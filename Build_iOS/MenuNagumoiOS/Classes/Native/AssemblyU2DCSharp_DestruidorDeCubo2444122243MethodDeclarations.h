﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DestruidorDeCubo
struct DestruidorDeCubo_t2444122243;

#include "codegen/il2cpp-codegen.h"

// System.Void DestruidorDeCubo::.ctor()
extern "C"  void DestruidorDeCubo__ctor_m2598285688 (DestruidorDeCubo_t2444122243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestruidorDeCubo::Start()
extern "C"  void DestruidorDeCubo_Start_m1545423480 (DestruidorDeCubo_t2444122243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestruidorDeCubo::Update()
extern "C"  void DestruidorDeCubo_Update_m669339797 (DestruidorDeCubo_t2444122243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
