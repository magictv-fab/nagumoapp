﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.MarkerTrackerImpl
struct MarkerTrackerImpl_t1934199560;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t865486027;
// System.String
struct String_t;
// Vuforia.Marker
struct Marker_t616694972;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker>
struct IEnumerable_1_t3917607929;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean Vuforia.MarkerTrackerImpl::Start()
extern "C"  bool MarkerTrackerImpl_Start_m1994186217 (MarkerTrackerImpl_t1934199560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerTrackerImpl::Stop()
extern "C"  void MarkerTrackerImpl_Stop_m1316565865 (MarkerTrackerImpl_t1934199560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTrackerImpl::CreateMarker(System.Int32,System.String,System.Single)
extern "C"  MarkerAbstractBehaviour_t865486027 * MarkerTrackerImpl_CreateMarker_m3744732785 (MarkerTrackerImpl_t1934199560 * __this, int32_t ___markerID0, String_t* ___trackableName1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerTrackerImpl::DestroyMarker(Vuforia.Marker,System.Boolean)
extern "C"  bool MarkerTrackerImpl_DestroyMarker_m2803372392 (MarkerTrackerImpl_t1934199560 * __this, Il2CppObject * ___marker0, bool ___destroyGameObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTrackerImpl::GetMarkers()
extern "C"  Il2CppObject* MarkerTrackerImpl_GetMarkers_m4228608676 (MarkerTrackerImpl_t1934199560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Marker Vuforia.MarkerTrackerImpl::GetMarkerByMarkerID(System.Int32)
extern "C"  Il2CppObject * MarkerTrackerImpl_GetMarkerByMarkerID_m2499290583 (MarkerTrackerImpl_t1934199560 * __this, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Marker Vuforia.MarkerTrackerImpl::InternalCreateMarker(System.Int32,System.String,System.Single)
extern "C"  Il2CppObject * MarkerTrackerImpl_InternalCreateMarker_m2693598749 (MarkerTrackerImpl_t1934199560 * __this, int32_t ___markerID0, String_t* ___name1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerTrackerImpl::DestroyAllMarkers(System.Boolean)
extern "C"  void MarkerTrackerImpl_DestroyAllMarkers_m2280454212 (MarkerTrackerImpl_t1934199560 * __this, bool ___destroyGameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerTrackerImpl::RegisterMarker(System.Int32,System.String,System.Single)
extern "C"  int32_t MarkerTrackerImpl_RegisterMarker_m29500304 (MarkerTrackerImpl_t1934199560 * __this, int32_t ___markerID0, String_t* ___trackableName1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerTrackerImpl::.ctor()
extern "C"  void MarkerTrackerImpl__ctor_m3198901693 (MarkerTrackerImpl_t1934199560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
