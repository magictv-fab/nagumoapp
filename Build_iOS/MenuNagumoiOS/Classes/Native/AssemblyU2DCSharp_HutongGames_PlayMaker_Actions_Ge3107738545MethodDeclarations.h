﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight
struct GetAnimatorGravityWeight_t3107738545;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::.ctor()
extern "C"  void GetAnimatorGravityWeight__ctor_m2113375701 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::Reset()
extern "C"  void GetAnimatorGravityWeight_Reset_m4054775938 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnEnter()
extern "C"  void GetAnimatorGravityWeight_OnEnter_m3121213548 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorGravityWeight_OnAnimatorMoveEvent_m2632402102 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnUpdate()
extern "C"  void GetAnimatorGravityWeight_OnUpdate_m1401898839 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::DoGetGravityWeight()
extern "C"  void GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnExit()
extern "C"  void GetAnimatorGravityWeight_OnExit_m2603455916 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
