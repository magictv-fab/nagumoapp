﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState
struct CurrentParsingState_t868552452;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder
struct  GeneralAppIdDecoder_t1619283386  : public Il2CppObject
{
public:
	// ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::information
	BitArray_t4163851164 * ___information_0;
	// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::current
	CurrentParsingState_t868552452 * ___current_1;
	// System.Text.StringBuilder ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::buffer
	StringBuilder_t243639308 * ___buffer_2;

public:
	inline static int32_t get_offset_of_information_0() { return static_cast<int32_t>(offsetof(GeneralAppIdDecoder_t1619283386, ___information_0)); }
	inline BitArray_t4163851164 * get_information_0() const { return ___information_0; }
	inline BitArray_t4163851164 ** get_address_of_information_0() { return &___information_0; }
	inline void set_information_0(BitArray_t4163851164 * value)
	{
		___information_0 = value;
		Il2CppCodeGenWriteBarrier(&___information_0, value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(GeneralAppIdDecoder_t1619283386, ___current_1)); }
	inline CurrentParsingState_t868552452 * get_current_1() const { return ___current_1; }
	inline CurrentParsingState_t868552452 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(CurrentParsingState_t868552452 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier(&___current_1, value);
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(GeneralAppIdDecoder_t1619283386, ___buffer_2)); }
	inline StringBuilder_t243639308 * get_buffer_2() const { return ___buffer_2; }
	inline StringBuilder_t243639308 ** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(StringBuilder_t243639308 * value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
