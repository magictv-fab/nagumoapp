﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_ZoneIdentityP4039398921.h"
#include "mscorlib_System_Security_Policy_AllMembershipCondit929139212.h"
#include "mscorlib_System_Security_Policy_ApplicationTrust3515839536.h"
#include "mscorlib_System_Security_Policy_CodeConnectAccess2328951823.h"
#include "mscorlib_System_Security_Policy_CodeGroup4075050400.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies2646374783.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies_Ke1696001620.h"
#include "mscorlib_System_Security_Policy_Evidence4141397151.h"
#include "mscorlib_System_Security_Policy_Evidence_EvidenceE3077794246.h"
#include "mscorlib_System_Security_Policy_FileCodeGroup3153172996.h"
#include "mscorlib_System_Security_Policy_FirstMatchCodeGrou3773206635.h"
#include "mscorlib_System_Security_Policy_GacInstalled3491099257.h"
#include "mscorlib_System_Security_Policy_Hash333288918.h"
#include "mscorlib_System_Security_Policy_MembershipConditio1160002817.h"
#include "mscorlib_System_Security_Policy_NetCodeGroup1277793085.h"
#include "mscorlib_System_Security_Policy_PermissionRequestE3069524805.h"
#include "mscorlib_System_Security_Policy_PolicyException3879805451.h"
#include "mscorlib_System_Security_Policy_PolicyLevel1214940608.h"
#include "mscorlib_System_Security_Policy_PolicyStatement313031467.h"
#include "mscorlib_System_Security_Policy_PolicyStatementAtt1658364679.h"
#include "mscorlib_System_Security_Policy_Publisher2376725258.h"
#include "mscorlib_System_Security_Policy_Site333624335.h"
#include "mscorlib_System_Security_Policy_StrongName2878058698.h"
#include "mscorlib_System_Security_Policy_StrongNameMembersh3282897489.h"
#include "mscorlib_System_Security_Policy_UnionCodeGroup1990104843.h"
#include "mscorlib_System_Security_Policy_Url3877763261.h"
#include "mscorlib_System_Security_Policy_Zone333838452.h"
#include "mscorlib_System_Security_Policy_ZoneMembershipCond1828699751.h"
#include "mscorlib_System_Security_Principal_PrincipalPolicy1160124438.h"
#include "mscorlib_System_Security_Principal_WindowsAccountT4089799192.h"
#include "mscorlib_System_Security_Principal_WindowsIdentity4160730487.h"
#include "mscorlib_System_Text_ASCIIEncoding731765126.h"
#include "mscorlib_System_Text_Decoder2225063046.h"
#include "mscorlib_System_Text_DecoderExceptionFallback451785075.h"
#include "mscorlib_System_Text_DecoderExceptionFallbackBuffe3161404051.h"
#include "mscorlib_System_Text_DecoderFallback649129352.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer4139272936.h"
#include "mscorlib_System_Text_DecoderFallbackException2055765167.h"
#include "mscorlib_System_Text_DecoderReplacementFallback213425334.h"
#include "mscorlib_System_Text_DecoderReplacementFallbackBuf3747569558.h"
#include "mscorlib_System_Text_EncoderExceptionFallback2997724059.h"
#include "mscorlib_System_Text_EncoderExceptionFallbackBuffer820700859.h"
#include "mscorlib_System_Text_EncoderFallback1901620832.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer671205824.h"
#include "mscorlib_System_Text_EncoderFallbackException306736855.h"
#include "mscorlib_System_Text_EncoderReplacementFallback3024397534.h"
#include "mscorlib_System_Text_EncoderReplacementFallbackBuff599697854.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_Text_Encoding_ForwardingDecoder120440429.h"
#include "mscorlib_System_Text_EncodingInfo1898473639.h"
#include "mscorlib_System_Text_Latin1Encoding2319396262.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Text_UnicodeEncoding1820170642.h"
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecode2551239720.h"
#include "mscorlib_System_Text_UTF7Encoding330356969.h"
#include "mscorlib_System_Text_UTF7Encoding_UTF7Decoder1979535786.h"
#include "mscorlib_System_Text_UTF8Encoding2817869802.h"
#include "mscorlib_System_Text_UTF8Encoding_UTF8Decoder3933697034.h"
#include "mscorlib_System_Text_UTF32Encoding1136945019.h"
#include "mscorlib_System_Text_UTF32Encoding_UTF32Decoder2029919638.h"
#include "mscorlib_System_Threading_AutoResetEvent874642578.h"
#include "mscorlib_System_Threading_CompressedStack1790381301.h"
#include "mscorlib_System_Threading_EventResetMode833452304.h"
#include "mscorlib_System_Threading_EventWaitHandle77918117.h"
#include "mscorlib_System_Threading_ExecutionContext1900239151.h"
#include "mscorlib_System_Threading_Interlocked373807572.h"
#include "mscorlib_System_Threading_ManualResetEvent924017833.h"
#include "mscorlib_System_Threading_Monitor2734674376.h"
#include "mscorlib_System_Threading_Mutex1887071405.h"
#include "mscorlib_System_Threading_NativeEventCalls1668807818.h"
#include "mscorlib_System_Threading_RegisteredWaitHandle2539114327.h"
#include "mscorlib_System_Threading_SynchronizationLockExcep2921991964.h"
#include "mscorlib_System_Threading_Thread1973216770.h"
#include "mscorlib_System_Threading_ThreadAbortException2237739745.h"
#include "mscorlib_System_Threading_ThreadInterruptedExcepti3649634511.h"
#include "mscorlib_System_Threading_ThreadPool1279216318.h"
#include "mscorlib_System_Threading_ThreadPriority572011718.h"
#include "mscorlib_System_Threading_ThreadState124146581.h"
#include "mscorlib_System_Threading_ThreadStateException2582941344.h"
#include "mscorlib_System_Threading_Timer1893171827.h"
#include "mscorlib_System_Threading_Timer_TimerComparer2789813299.h"
#include "mscorlib_System_Threading_Timer_Scheduler119615196.h"
#include "mscorlib_System_Threading_WaitHandle1661568373.h"
#include "mscorlib_System_Collections_Generic_Link2063667470.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize900 = { sizeof (ZoneIdentityPermission_t4039398921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable900[1] = 
{
	ZoneIdentityPermission_t4039398921::get_offset_of_zone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize901 = { sizeof (AllMembershipCondition_t929139212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable901[1] = 
{
	AllMembershipCondition_t929139212::get_offset_of_version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize902 = { sizeof (ApplicationTrust_t3515839536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable902[6] = 
{
	ApplicationTrust_t3515839536::get_offset_of__appid_0(),
	ApplicationTrust_t3515839536::get_offset_of__defaultPolicy_1(),
	ApplicationTrust_t3515839536::get_offset_of__xtranfo_2(),
	ApplicationTrust_t3515839536::get_offset_of__trustrun_3(),
	ApplicationTrust_t3515839536::get_offset_of__persist_4(),
	ApplicationTrust_t3515839536::get_offset_of_fullTrustAssemblies_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize903 = { sizeof (CodeConnectAccess_t2328951823), -1, sizeof(CodeConnectAccess_t2328951823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable903[6] = 
{
	CodeConnectAccess_t2328951823_StaticFields::get_offset_of_AnyScheme_0(),
	CodeConnectAccess_t2328951823_StaticFields::get_offset_of_DefaultPort_1(),
	CodeConnectAccess_t2328951823_StaticFields::get_offset_of_OriginPort_2(),
	CodeConnectAccess_t2328951823_StaticFields::get_offset_of_OriginScheme_3(),
	CodeConnectAccess_t2328951823::get_offset_of__scheme_4(),
	CodeConnectAccess_t2328951823::get_offset_of__port_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize904 = { sizeof (CodeGroup_t4075050400), -1, sizeof(CodeGroup_t4075050400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable904[6] = 
{
	CodeGroup_t4075050400::get_offset_of_m_policy_0(),
	CodeGroup_t4075050400::get_offset_of_m_membershipCondition_1(),
	CodeGroup_t4075050400::get_offset_of_m_description_2(),
	CodeGroup_t4075050400::get_offset_of_m_name_3(),
	CodeGroup_t4075050400::get_offset_of_m_children_4(),
	CodeGroup_t4075050400_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize905 = { sizeof (DefaultPolicies_t2646374783), -1, sizeof(DefaultPolicies_t2646374783_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable905[12] = 
{
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__fxVersion_0(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__ecmaKey_1(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__ecma_2(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__msFinalKey_3(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__msFinal_4(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__fullTrust_5(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__localIntranet_6(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__internet_7(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__skipVerification_8(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__execution_9(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__nothing_10(),
	DefaultPolicies_t2646374783_StaticFields::get_offset_of__everything_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize906 = { sizeof (Key_t1696001620)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable906[3] = 
{
	Key_t1696001620::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize907 = { sizeof (Evidence_t4141397151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable907[4] = 
{
	Evidence_t4141397151::get_offset_of__locked_0(),
	Evidence_t4141397151::get_offset_of_hostEvidenceList_1(),
	Evidence_t4141397151::get_offset_of_assemblyEvidenceList_2(),
	Evidence_t4141397151::get_offset_of__hashCode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize908 = { sizeof (EvidenceEnumerator_t3077794246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable908[3] = 
{
	EvidenceEnumerator_t3077794246::get_offset_of_currentEnum_0(),
	EvidenceEnumerator_t3077794246::get_offset_of_hostEnum_1(),
	EvidenceEnumerator_t3077794246::get_offset_of_assemblyEnum_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize909 = { sizeof (FileCodeGroup_t3153172996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable909[1] = 
{
	FileCodeGroup_t3153172996::get_offset_of_m_access_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize910 = { sizeof (FirstMatchCodeGroup_t3773206635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize911 = { sizeof (GacInstalled_t3491099257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize912 = { sizeof (Hash_t333288918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable912[2] = 
{
	Hash_t333288918::get_offset_of_assembly_0(),
	Hash_t333288918::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize914 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize917 = { sizeof (MembershipConditionHelper_t1160002817), -1, sizeof(MembershipConditionHelper_t1160002817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable917[1] = 
{
	MembershipConditionHelper_t1160002817_StaticFields::get_offset_of_XmlTag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize918 = { sizeof (NetCodeGroup_t1277793085), -1, sizeof(NetCodeGroup_t1277793085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable918[4] = 
{
	NetCodeGroup_t1277793085_StaticFields::get_offset_of_AbsentOriginScheme_6(),
	NetCodeGroup_t1277793085_StaticFields::get_offset_of_AnyOtherOriginScheme_7(),
	NetCodeGroup_t1277793085::get_offset_of__rules_8(),
	NetCodeGroup_t1277793085::get_offset_of__hashcode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize919 = { sizeof (PermissionRequestEvidence_t3069524805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable919[3] = 
{
	PermissionRequestEvidence_t3069524805::get_offset_of_requested_0(),
	PermissionRequestEvidence_t3069524805::get_offset_of_optional_1(),
	PermissionRequestEvidence_t3069524805::get_offset_of_denied_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize920 = { sizeof (PolicyException_t3879805451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize921 = { sizeof (PolicyLevel_t1214940608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable921[8] = 
{
	PolicyLevel_t1214940608::get_offset_of_label_0(),
	PolicyLevel_t1214940608::get_offset_of_root_code_group_1(),
	PolicyLevel_t1214940608::get_offset_of_full_trust_assemblies_2(),
	PolicyLevel_t1214940608::get_offset_of_named_permission_sets_3(),
	PolicyLevel_t1214940608::get_offset_of__location_4(),
	PolicyLevel_t1214940608::get_offset_of__type_5(),
	PolicyLevel_t1214940608::get_offset_of_fullNames_6(),
	PolicyLevel_t1214940608::get_offset_of_xml_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize922 = { sizeof (PolicyStatement_t313031467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable922[2] = 
{
	PolicyStatement_t313031467::get_offset_of_perms_0(),
	PolicyStatement_t313031467::get_offset_of_attrs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize923 = { sizeof (PolicyStatementAttribute_t1658364679)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable923[5] = 
{
	PolicyStatementAttribute_t1658364679::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize924 = { sizeof (Publisher_t2376725258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable924[1] = 
{
	Publisher_t2376725258::get_offset_of_m_cert_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize925 = { sizeof (Site_t333624335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable925[1] = 
{
	Site_t333624335::get_offset_of_origin_site_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize926 = { sizeof (StrongName_t2878058698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable926[3] = 
{
	StrongName_t2878058698::get_offset_of_publickey_0(),
	StrongName_t2878058698::get_offset_of_name_1(),
	StrongName_t2878058698::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize927 = { sizeof (StrongNameMembershipCondition_t3282897489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable927[4] = 
{
	StrongNameMembershipCondition_t3282897489::get_offset_of_version_0(),
	StrongNameMembershipCondition_t3282897489::get_offset_of_blob_1(),
	StrongNameMembershipCondition_t3282897489::get_offset_of_name_2(),
	StrongNameMembershipCondition_t3282897489::get_offset_of_assemblyVersion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize928 = { sizeof (UnionCodeGroup_t1990104843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize929 = { sizeof (Url_t3877763261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable929[1] = 
{
	Url_t3877763261::get_offset_of_origin_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize930 = { sizeof (Zone_t333838452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable930[1] = 
{
	Zone_t333838452::get_offset_of_zone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize931 = { sizeof (ZoneMembershipCondition_t1828699751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable931[2] = 
{
	ZoneMembershipCondition_t1828699751::get_offset_of_version_0(),
	ZoneMembershipCondition_t1828699751::get_offset_of_zone_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize932 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize933 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize934 = { sizeof (PrincipalPolicy_t1160124438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable934[4] = 
{
	PrincipalPolicy_t1160124438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize935 = { sizeof (WindowsAccountType_t4089799192)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable935[5] = 
{
	WindowsAccountType_t4089799192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize936 = { sizeof (WindowsIdentity_t4160730487), -1, sizeof(WindowsIdentity_t4160730487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable936[7] = 
{
	WindowsIdentity_t4160730487::get_offset_of__token_0(),
	WindowsIdentity_t4160730487::get_offset_of__type_1(),
	WindowsIdentity_t4160730487::get_offset_of__account_2(),
	WindowsIdentity_t4160730487::get_offset_of__authenticated_3(),
	WindowsIdentity_t4160730487::get_offset_of__name_4(),
	WindowsIdentity_t4160730487::get_offset_of__info_5(),
	WindowsIdentity_t4160730487_StaticFields::get_offset_of_invalidWindows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize937 = { sizeof (ASCIIEncoding_t731765126), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize938 = { sizeof (Decoder_t2225063046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable938[2] = 
{
	Decoder_t2225063046::get_offset_of_fallback_0(),
	Decoder_t2225063046::get_offset_of_fallback_buffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize939 = { sizeof (DecoderExceptionFallback_t451785075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize940 = { sizeof (DecoderExceptionFallbackBuffer_t3161404051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize941 = { sizeof (DecoderFallback_t649129352), -1, sizeof(DecoderFallback_t649129352_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable941[3] = 
{
	DecoderFallback_t649129352_StaticFields::get_offset_of_exception_fallback_0(),
	DecoderFallback_t649129352_StaticFields::get_offset_of_replacement_fallback_1(),
	DecoderFallback_t649129352_StaticFields::get_offset_of_standard_safe_fallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize942 = { sizeof (DecoderFallbackBuffer_t4139272936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize943 = { sizeof (DecoderFallbackException_t2055765167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable943[2] = 
{
	DecoderFallbackException_t2055765167::get_offset_of_bytes_unknown_13(),
	DecoderFallbackException_t2055765167::get_offset_of_index_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize944 = { sizeof (DecoderReplacementFallback_t213425334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable944[1] = 
{
	DecoderReplacementFallback_t213425334::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize945 = { sizeof (DecoderReplacementFallbackBuffer_t3747569558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable945[3] = 
{
	DecoderReplacementFallbackBuffer_t3747569558::get_offset_of_fallback_assigned_0(),
	DecoderReplacementFallbackBuffer_t3747569558::get_offset_of_current_1(),
	DecoderReplacementFallbackBuffer_t3747569558::get_offset_of_replacement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize946 = { sizeof (EncoderExceptionFallback_t2997724059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize947 = { sizeof (EncoderExceptionFallbackBuffer_t820700859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize948 = { sizeof (EncoderFallback_t1901620832), -1, sizeof(EncoderFallback_t1901620832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable948[3] = 
{
	EncoderFallback_t1901620832_StaticFields::get_offset_of_exception_fallback_0(),
	EncoderFallback_t1901620832_StaticFields::get_offset_of_replacement_fallback_1(),
	EncoderFallback_t1901620832_StaticFields::get_offset_of_standard_safe_fallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize949 = { sizeof (EncoderFallbackBuffer_t671205824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize950 = { sizeof (EncoderFallbackException_t306736855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable950[4] = 
{
	EncoderFallbackException_t306736855::get_offset_of_char_unknown_13(),
	EncoderFallbackException_t306736855::get_offset_of_char_unknown_high_14(),
	EncoderFallbackException_t306736855::get_offset_of_char_unknown_low_15(),
	EncoderFallbackException_t306736855::get_offset_of_index_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize951 = { sizeof (EncoderReplacementFallback_t3024397534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable951[1] = 
{
	EncoderReplacementFallback_t3024397534::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize952 = { sizeof (EncoderReplacementFallbackBuffer_t599697854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable952[3] = 
{
	EncoderReplacementFallbackBuffer_t599697854::get_offset_of_replacement_0(),
	EncoderReplacementFallbackBuffer_t599697854::get_offset_of_current_1(),
	EncoderReplacementFallbackBuffer_t599697854::get_offset_of_fallback_assigned_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize953 = { sizeof (Encoding_t2012439129), -1, sizeof(Encoding_t2012439129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable953[29] = 
{
	Encoding_t2012439129::get_offset_of_codePage_0(),
	Encoding_t2012439129::get_offset_of_windows_code_page_1(),
	Encoding_t2012439129::get_offset_of_is_readonly_2(),
	Encoding_t2012439129::get_offset_of_decoder_fallback_3(),
	Encoding_t2012439129::get_offset_of_encoder_fallback_4(),
	Encoding_t2012439129_StaticFields::get_offset_of_i18nAssembly_5(),
	Encoding_t2012439129_StaticFields::get_offset_of_i18nDisabled_6(),
	Encoding_t2012439129_StaticFields::get_offset_of_encoding_infos_7(),
	Encoding_t2012439129_StaticFields::get_offset_of_encodings_8(),
	Encoding_t2012439129::get_offset_of_body_name_9(),
	Encoding_t2012439129::get_offset_of_encoding_name_10(),
	Encoding_t2012439129::get_offset_of_header_name_11(),
	Encoding_t2012439129::get_offset_of_is_mail_news_display_12(),
	Encoding_t2012439129::get_offset_of_is_mail_news_save_13(),
	Encoding_t2012439129::get_offset_of_is_browser_save_14(),
	Encoding_t2012439129::get_offset_of_is_browser_display_15(),
	Encoding_t2012439129::get_offset_of_web_name_16(),
	Encoding_t2012439129_StaticFields::get_offset_of_asciiEncoding_17(),
	Encoding_t2012439129_StaticFields::get_offset_of_bigEndianEncoding_18(),
	Encoding_t2012439129_StaticFields::get_offset_of_defaultEncoding_19(),
	Encoding_t2012439129_StaticFields::get_offset_of_utf7Encoding_20(),
	Encoding_t2012439129_StaticFields::get_offset_of_utf8EncodingWithMarkers_21(),
	Encoding_t2012439129_StaticFields::get_offset_of_utf8EncodingWithoutMarkers_22(),
	Encoding_t2012439129_StaticFields::get_offset_of_unicodeEncoding_23(),
	Encoding_t2012439129_StaticFields::get_offset_of_isoLatin1Encoding_24(),
	Encoding_t2012439129_StaticFields::get_offset_of_utf8EncodingUnsafe_25(),
	Encoding_t2012439129_StaticFields::get_offset_of_utf32Encoding_26(),
	Encoding_t2012439129_StaticFields::get_offset_of_bigEndianUTF32Encoding_27(),
	Encoding_t2012439129_StaticFields::get_offset_of_lockobj_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize954 = { sizeof (ForwardingDecoder_t120440429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable954[1] = 
{
	ForwardingDecoder_t120440429::get_offset_of_encoding_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize955 = { sizeof (EncodingInfo_t1898473639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable955[2] = 
{
	EncodingInfo_t1898473639::get_offset_of_codepage_0(),
	EncodingInfo_t1898473639::get_offset_of_encoding_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize956 = { sizeof (Latin1Encoding_t2319396262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize957 = { sizeof (StringBuilder_t243639308), sizeof(char*), 0, 0 };
extern const int32_t g_FieldOffsetTable957[5] = 
{
	0,
	StringBuilder_t243639308::get_offset_of__length_1(),
	StringBuilder_t243639308::get_offset_of__str_2(),
	StringBuilder_t243639308::get_offset_of__cached_str_3(),
	StringBuilder_t243639308::get_offset_of__maxCapacity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize958 = { sizeof (UnicodeEncoding_t1820170642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable958[2] = 
{
	UnicodeEncoding_t1820170642::get_offset_of_bigEndian_29(),
	UnicodeEncoding_t1820170642::get_offset_of_byteOrderMark_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize959 = { sizeof (UnicodeDecoder_t2551239720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable959[2] = 
{
	UnicodeDecoder_t2551239720::get_offset_of_bigEndian_2(),
	UnicodeDecoder_t2551239720::get_offset_of_leftOverByte_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize960 = { sizeof (UTF7Encoding_t330356969), -1, sizeof(UTF7Encoding_t330356969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable960[3] = 
{
	UTF7Encoding_t330356969::get_offset_of_allowOptionals_29(),
	UTF7Encoding_t330356969_StaticFields::get_offset_of_encodingRules_30(),
	UTF7Encoding_t330356969_StaticFields::get_offset_of_base64Values_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize961 = { sizeof (UTF7Decoder_t1979535786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable961[1] = 
{
	UTF7Decoder_t1979535786::get_offset_of_leftOver_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize962 = { sizeof (UTF8Encoding_t2817869802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable962[1] = 
{
	UTF8Encoding_t2817869802::get_offset_of_emitIdentifier_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize963 = { sizeof (UTF8Decoder_t3933697034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable963[2] = 
{
	UTF8Decoder_t3933697034::get_offset_of_leftOverBits_2(),
	UTF8Decoder_t3933697034::get_offset_of_leftOverCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize964 = { sizeof (UTF32Encoding_t1136945019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable964[2] = 
{
	UTF32Encoding_t1136945019::get_offset_of_bigEndian_29(),
	UTF32Encoding_t1136945019::get_offset_of_byteOrderMark_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize965 = { sizeof (UTF32Decoder_t2029919638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable965[3] = 
{
	UTF32Decoder_t2029919638::get_offset_of_bigEndian_2(),
	UTF32Decoder_t2029919638::get_offset_of_leftOverByte_3(),
	UTF32Decoder_t2029919638::get_offset_of_leftOverLength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize966 = { sizeof (AutoResetEvent_t874642578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize967 = { sizeof (CompressedStack_t1790381301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable967[1] = 
{
	CompressedStack_t1790381301::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize968 = { sizeof (EventResetMode_t833452304)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable968[3] = 
{
	EventResetMode_t833452304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize969 = { sizeof (EventWaitHandle_t77918117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize970 = { sizeof (ExecutionContext_t1900239151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable970[3] = 
{
	ExecutionContext_t1900239151::get_offset_of__sc_0(),
	ExecutionContext_t1900239151::get_offset_of__suppressFlow_1(),
	ExecutionContext_t1900239151::get_offset_of__capture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize971 = { sizeof (Interlocked_t373807572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize972 = { sizeof (ManualResetEvent_t924017833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize973 = { sizeof (Monitor_t2734674376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize974 = { sizeof (Mutex_t1887071405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize975 = { sizeof (NativeEventCalls_t1668807818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize976 = { sizeof (RegisteredWaitHandle_t2539114327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable976[9] = 
{
	RegisteredWaitHandle_t2539114327::get_offset_of__waitObject_1(),
	RegisteredWaitHandle_t2539114327::get_offset_of__callback_2(),
	RegisteredWaitHandle_t2539114327::get_offset_of__timeout_3(),
	RegisteredWaitHandle_t2539114327::get_offset_of__state_4(),
	RegisteredWaitHandle_t2539114327::get_offset_of__executeOnlyOnce_5(),
	RegisteredWaitHandle_t2539114327::get_offset_of__finalEvent_6(),
	RegisteredWaitHandle_t2539114327::get_offset_of__cancelEvent_7(),
	RegisteredWaitHandle_t2539114327::get_offset_of__callsInProcess_8(),
	RegisteredWaitHandle_t2539114327::get_offset_of__unregistered_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize977 = { sizeof (SynchronizationLockException_t2921991964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize978 = { sizeof (Thread_t1973216770), -1, sizeof(Thread_t1973216770_StaticFields), sizeof(Thread_t1973216770_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable978[52] = 
{
	Thread_t1973216770::get_offset_of_lock_thread_id_0(),
	Thread_t1973216770::get_offset_of_system_thread_handle_1(),
	Thread_t1973216770::get_offset_of_cached_culture_info_2(),
	Thread_t1973216770::get_offset_of_unused0_3(),
	Thread_t1973216770::get_offset_of_threadpool_thread_4(),
	Thread_t1973216770::get_offset_of_name_5(),
	Thread_t1973216770::get_offset_of_name_len_6(),
	Thread_t1973216770::get_offset_of_state_7(),
	Thread_t1973216770::get_offset_of_abort_exc_8(),
	Thread_t1973216770::get_offset_of_abort_state_handle_9(),
	Thread_t1973216770::get_offset_of_thread_id_10(),
	Thread_t1973216770::get_offset_of_start_notify_11(),
	Thread_t1973216770::get_offset_of_stack_ptr_12(),
	Thread_t1973216770::get_offset_of_static_data_13(),
	Thread_t1973216770::get_offset_of_jit_data_14(),
	Thread_t1973216770::get_offset_of_lock_data_15(),
	Thread_t1973216770::get_offset_of_current_appcontext_16(),
	Thread_t1973216770::get_offset_of_stack_size_17(),
	Thread_t1973216770::get_offset_of_start_obj_18(),
	Thread_t1973216770::get_offset_of_appdomain_refs_19(),
	Thread_t1973216770::get_offset_of_interruption_requested_20(),
	Thread_t1973216770::get_offset_of_suspend_event_21(),
	Thread_t1973216770::get_offset_of_suspended_event_22(),
	Thread_t1973216770::get_offset_of_resume_event_23(),
	Thread_t1973216770::get_offset_of_synch_cs_24(),
	Thread_t1973216770::get_offset_of_serialized_culture_info_25(),
	Thread_t1973216770::get_offset_of_serialized_culture_info_len_26(),
	Thread_t1973216770::get_offset_of_serialized_ui_culture_info_27(),
	Thread_t1973216770::get_offset_of_serialized_ui_culture_info_len_28(),
	Thread_t1973216770::get_offset_of_thread_dump_requested_29(),
	Thread_t1973216770::get_offset_of_end_stack_30(),
	Thread_t1973216770::get_offset_of_thread_interrupt_requested_31(),
	Thread_t1973216770::get_offset_of_apartment_state_32(),
	Thread_t1973216770::get_offset_of_critical_region_level_33(),
	Thread_t1973216770::get_offset_of_small_id_34(),
	Thread_t1973216770::get_offset_of_manage_callback_35(),
	Thread_t1973216770::get_offset_of_pending_exception_36(),
	Thread_t1973216770::get_offset_of_ec_to_set_37(),
	Thread_t1973216770::get_offset_of_interrupt_on_stop_38(),
	Thread_t1973216770::get_offset_of_unused3_39(),
	Thread_t1973216770::get_offset_of_unused4_40(),
	Thread_t1973216770::get_offset_of_unused5_41(),
	Thread_t1973216770::get_offset_of_unused6_42(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	Thread_t1973216770::get_offset_of_threadstart_45(),
	Thread_t1973216770::get_offset_of_managed_id_46(),
	Thread_t1973216770::get_offset_of__principal_47(),
	Thread_t1973216770_StaticFields::get_offset_of_datastorehash_48(),
	Thread_t1973216770_StaticFields::get_offset_of_datastore_lock_49(),
	Thread_t1973216770::get_offset_of_in_currentculture_50(),
	Thread_t1973216770_StaticFields::get_offset_of_culture_lock_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize979 = { sizeof (ThreadAbortException_t2237739745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize980 = { sizeof (ThreadInterruptedException_t3649634511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize981 = { sizeof (ThreadPool_t1279216318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize982 = { sizeof (ThreadPriority_t572011718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable982[6] = 
{
	ThreadPriority_t572011718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize983 = { sizeof (ThreadState_t124146581)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable983[11] = 
{
	ThreadState_t124146581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize984 = { sizeof (ThreadStateException_t2582941344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize985 = { sizeof (Timer_t1893171827), -1, sizeof(Timer_t1893171827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable985[7] = 
{
	Timer_t1893171827_StaticFields::get_offset_of_scheduler_1(),
	Timer_t1893171827::get_offset_of_callback_2(),
	Timer_t1893171827::get_offset_of_state_3(),
	Timer_t1893171827::get_offset_of_due_time_ms_4(),
	Timer_t1893171827::get_offset_of_period_ms_5(),
	Timer_t1893171827::get_offset_of_next_run_6(),
	Timer_t1893171827::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize986 = { sizeof (TimerComparer_t2789813299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize987 = { sizeof (Scheduler_t119615196), -1, sizeof(Scheduler_t119615196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable987[2] = 
{
	Scheduler_t119615196_StaticFields::get_offset_of_instance_0(),
	Scheduler_t119615196::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize988 = { sizeof (WaitHandle_t1661568373), -1, sizeof(WaitHandle_t1661568373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable988[4] = 
{
	0,
	WaitHandle_t1661568373::get_offset_of_safe_wait_handle_2(),
	WaitHandle_t1661568373_StaticFields::get_offset_of_InvalidHandle_3(),
	WaitHandle_t1661568373::get_offset_of_disposed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize989 = { sizeof (Link_t2063667470)+ sizeof (Il2CppObject), sizeof(Link_t2063667470_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable989[2] = 
{
	Link_t2063667470::get_offset_of_HashCode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t2063667470::get_offset_of_Next_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize990 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable990[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize991 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable991[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize992 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable992[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize993 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable993[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize994 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable994[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize995 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable995[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize996 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable996[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize997 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize998 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize999 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
