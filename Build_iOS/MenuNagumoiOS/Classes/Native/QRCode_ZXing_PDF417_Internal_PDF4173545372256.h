﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// ZXing.PDF417.Internal.BarcodeMatrix
struct BarcodeMatrix_t425107471;
// System.Text.Encoding
struct Encoding_t2012439129;

#include "mscorlib_System_Object4170816371.h"
#include "QRCode_ZXing_PDF417_Internal_Compaction2012764861.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417
struct  PDF417_t3545372256  : public Il2CppObject
{
public:
	// ZXing.PDF417.Internal.BarcodeMatrix ZXing.PDF417.Internal.PDF417::barcodeMatrix
	BarcodeMatrix_t425107471 * ___barcodeMatrix_1;
	// System.Boolean ZXing.PDF417.Internal.PDF417::compact
	bool ___compact_2;
	// ZXing.PDF417.Internal.Compaction ZXing.PDF417.Internal.PDF417::compaction
	int32_t ___compaction_3;
	// System.Text.Encoding ZXing.PDF417.Internal.PDF417::encoding
	Encoding_t2012439129 * ___encoding_4;
	// System.Boolean ZXing.PDF417.Internal.PDF417::disableEci
	bool ___disableEci_5;
	// System.Int32 ZXing.PDF417.Internal.PDF417::minCols
	int32_t ___minCols_6;
	// System.Int32 ZXing.PDF417.Internal.PDF417::maxCols
	int32_t ___maxCols_7;
	// System.Int32 ZXing.PDF417.Internal.PDF417::maxRows
	int32_t ___maxRows_8;
	// System.Int32 ZXing.PDF417.Internal.PDF417::minRows
	int32_t ___minRows_9;

public:
	inline static int32_t get_offset_of_barcodeMatrix_1() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___barcodeMatrix_1)); }
	inline BarcodeMatrix_t425107471 * get_barcodeMatrix_1() const { return ___barcodeMatrix_1; }
	inline BarcodeMatrix_t425107471 ** get_address_of_barcodeMatrix_1() { return &___barcodeMatrix_1; }
	inline void set_barcodeMatrix_1(BarcodeMatrix_t425107471 * value)
	{
		___barcodeMatrix_1 = value;
		Il2CppCodeGenWriteBarrier(&___barcodeMatrix_1, value);
	}

	inline static int32_t get_offset_of_compact_2() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___compact_2)); }
	inline bool get_compact_2() const { return ___compact_2; }
	inline bool* get_address_of_compact_2() { return &___compact_2; }
	inline void set_compact_2(bool value)
	{
		___compact_2 = value;
	}

	inline static int32_t get_offset_of_compaction_3() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___compaction_3)); }
	inline int32_t get_compaction_3() const { return ___compaction_3; }
	inline int32_t* get_address_of_compaction_3() { return &___compaction_3; }
	inline void set_compaction_3(int32_t value)
	{
		___compaction_3 = value;
	}

	inline static int32_t get_offset_of_encoding_4() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___encoding_4)); }
	inline Encoding_t2012439129 * get_encoding_4() const { return ___encoding_4; }
	inline Encoding_t2012439129 ** get_address_of_encoding_4() { return &___encoding_4; }
	inline void set_encoding_4(Encoding_t2012439129 * value)
	{
		___encoding_4 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_4, value);
	}

	inline static int32_t get_offset_of_disableEci_5() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___disableEci_5)); }
	inline bool get_disableEci_5() const { return ___disableEci_5; }
	inline bool* get_address_of_disableEci_5() { return &___disableEci_5; }
	inline void set_disableEci_5(bool value)
	{
		___disableEci_5 = value;
	}

	inline static int32_t get_offset_of_minCols_6() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___minCols_6)); }
	inline int32_t get_minCols_6() const { return ___minCols_6; }
	inline int32_t* get_address_of_minCols_6() { return &___minCols_6; }
	inline void set_minCols_6(int32_t value)
	{
		___minCols_6 = value;
	}

	inline static int32_t get_offset_of_maxCols_7() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___maxCols_7)); }
	inline int32_t get_maxCols_7() const { return ___maxCols_7; }
	inline int32_t* get_address_of_maxCols_7() { return &___maxCols_7; }
	inline void set_maxCols_7(int32_t value)
	{
		___maxCols_7 = value;
	}

	inline static int32_t get_offset_of_maxRows_8() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___maxRows_8)); }
	inline int32_t get_maxRows_8() const { return ___maxRows_8; }
	inline int32_t* get_address_of_maxRows_8() { return &___maxRows_8; }
	inline void set_maxRows_8(int32_t value)
	{
		___maxRows_8 = value;
	}

	inline static int32_t get_offset_of_minRows_9() { return static_cast<int32_t>(offsetof(PDF417_t3545372256, ___minRows_9)); }
	inline int32_t get_minRows_9() const { return ___minRows_9; }
	inline int32_t* get_address_of_minRows_9() { return &___minRows_9; }
	inline void set_minRows_9(int32_t value)
	{
		___minRows_9 = value;
	}
};

struct PDF417_t3545372256_StaticFields
{
public:
	// System.Int32[][] ZXing.PDF417.Internal.PDF417::CODEWORD_TABLE
	Int32U5BU5DU5BU5D_t1820556512* ___CODEWORD_TABLE_0;

public:
	inline static int32_t get_offset_of_CODEWORD_TABLE_0() { return static_cast<int32_t>(offsetof(PDF417_t3545372256_StaticFields, ___CODEWORD_TABLE_0)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_CODEWORD_TABLE_0() const { return ___CODEWORD_TABLE_0; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_CODEWORD_TABLE_0() { return &___CODEWORD_TABLE_0; }
	inline void set_CODEWORD_TABLE_0(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___CODEWORD_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CODEWORD_TABLE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
