﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SampleAppUIRect
struct SampleAppUIRect_t2784570703;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUILabel
struct  SampleAppUILabel_t416684265  : public ISampleAppUIElement_t2180050874
{
public:
	// SampleAppUIRect SampleAppUILabel::mRect
	SampleAppUIRect_t2784570703 * ___mRect_0;
	// UnityEngine.GUIStyle SampleAppUILabel::mStyle
	GUIStyle_t2990928826 * ___mStyle_1;
	// System.String SampleAppUILabel::mTitle
	String_t* ___mTitle_2;
	// System.Single SampleAppUILabel::mWidth
	float ___mWidth_3;
	// System.Single SampleAppUILabel::mHeight
	float ___mHeight_4;

public:
	inline static int32_t get_offset_of_mRect_0() { return static_cast<int32_t>(offsetof(SampleAppUILabel_t416684265, ___mRect_0)); }
	inline SampleAppUIRect_t2784570703 * get_mRect_0() const { return ___mRect_0; }
	inline SampleAppUIRect_t2784570703 ** get_address_of_mRect_0() { return &___mRect_0; }
	inline void set_mRect_0(SampleAppUIRect_t2784570703 * value)
	{
		___mRect_0 = value;
		Il2CppCodeGenWriteBarrier(&___mRect_0, value);
	}

	inline static int32_t get_offset_of_mStyle_1() { return static_cast<int32_t>(offsetof(SampleAppUILabel_t416684265, ___mStyle_1)); }
	inline GUIStyle_t2990928826 * get_mStyle_1() const { return ___mStyle_1; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyle_1() { return &___mStyle_1; }
	inline void set_mStyle_1(GUIStyle_t2990928826 * value)
	{
		___mStyle_1 = value;
		Il2CppCodeGenWriteBarrier(&___mStyle_1, value);
	}

	inline static int32_t get_offset_of_mTitle_2() { return static_cast<int32_t>(offsetof(SampleAppUILabel_t416684265, ___mTitle_2)); }
	inline String_t* get_mTitle_2() const { return ___mTitle_2; }
	inline String_t** get_address_of_mTitle_2() { return &___mTitle_2; }
	inline void set_mTitle_2(String_t* value)
	{
		___mTitle_2 = value;
		Il2CppCodeGenWriteBarrier(&___mTitle_2, value);
	}

	inline static int32_t get_offset_of_mWidth_3() { return static_cast<int32_t>(offsetof(SampleAppUILabel_t416684265, ___mWidth_3)); }
	inline float get_mWidth_3() const { return ___mWidth_3; }
	inline float* get_address_of_mWidth_3() { return &___mWidth_3; }
	inline void set_mWidth_3(float value)
	{
		___mWidth_3 = value;
	}

	inline static int32_t get_offset_of_mHeight_4() { return static_cast<int32_t>(offsetof(SampleAppUILabel_t416684265, ___mHeight_4)); }
	inline float get_mHeight_4() const { return ___mHeight_4; }
	inline float* get_address_of_mHeight_4() { return &___mHeight_4; }
	inline void set_mHeight_4(float value)
	{
		___mHeight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
