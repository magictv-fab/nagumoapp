﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t3371348110;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUISkin
struct  SetGUISkin_t884172798  : public FsmStateAction_t2366529033
{
public:
	// UnityEngine.GUISkin HutongGames.PlayMaker.Actions.SetGUISkin::skin
	GUISkin_t3371348110 * ___skin_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUISkin::applyGlobally
	FsmBool_t1075959796 * ___applyGlobally_10;

public:
	inline static int32_t get_offset_of_skin_9() { return static_cast<int32_t>(offsetof(SetGUISkin_t884172798, ___skin_9)); }
	inline GUISkin_t3371348110 * get_skin_9() const { return ___skin_9; }
	inline GUISkin_t3371348110 ** get_address_of_skin_9() { return &___skin_9; }
	inline void set_skin_9(GUISkin_t3371348110 * value)
	{
		___skin_9 = value;
		Il2CppCodeGenWriteBarrier(&___skin_9, value);
	}

	inline static int32_t get_offset_of_applyGlobally_10() { return static_cast<int32_t>(offsetof(SetGUISkin_t884172798, ___applyGlobally_10)); }
	inline FsmBool_t1075959796 * get_applyGlobally_10() const { return ___applyGlobally_10; }
	inline FsmBool_t1075959796 ** get_address_of_applyGlobally_10() { return &___applyGlobally_10; }
	inline void set_applyGlobally_10(FsmBool_t1075959796 * value)
	{
		___applyGlobally_10 = value;
		Il2CppCodeGenWriteBarrier(&___applyGlobally_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
