﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlaySound
struct  PlaySound_t2516791689  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlaySound::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.PlaySound::position
	FsmVector3_t533912882 * ___position_10;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.PlaySound::clip
	FsmObject_t821476169 * ___clip_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlaySound::volume
	FsmFloat_t2134102846 * ___volume_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_position_10() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___position_10)); }
	inline FsmVector3_t533912882 * get_position_10() const { return ___position_10; }
	inline FsmVector3_t533912882 ** get_address_of_position_10() { return &___position_10; }
	inline void set_position_10(FsmVector3_t533912882 * value)
	{
		___position_10 = value;
		Il2CppCodeGenWriteBarrier(&___position_10, value);
	}

	inline static int32_t get_offset_of_clip_11() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___clip_11)); }
	inline FsmObject_t821476169 * get_clip_11() const { return ___clip_11; }
	inline FsmObject_t821476169 ** get_address_of_clip_11() { return &___clip_11; }
	inline void set_clip_11(FsmObject_t821476169 * value)
	{
		___clip_11 = value;
		Il2CppCodeGenWriteBarrier(&___clip_11, value);
	}

	inline static int32_t get_offset_of_volume_12() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___volume_12)); }
	inline FsmFloat_t2134102846 * get_volume_12() const { return ___volume_12; }
	inline FsmFloat_t2134102846 ** get_address_of_volume_12() { return &___volume_12; }
	inline void set_volume_12(FsmFloat_t2134102846 * value)
	{
		___volume_12 = value;
		Il2CppCodeGenWriteBarrier(&___volume_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
