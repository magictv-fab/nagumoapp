﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
struct  GetAnimatorBoneGameObject_t14629568  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// UnityEngine.HumanBodyBones HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::bone
	int32_t ___bone_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::boneAsString
	FsmString_t952858651 * ___boneAsString_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::boneGameObject
	FsmGameObject_t1697147867 * ___boneGameObject_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_bone_10() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___bone_10)); }
	inline int32_t get_bone_10() const { return ___bone_10; }
	inline int32_t* get_address_of_bone_10() { return &___bone_10; }
	inline void set_bone_10(int32_t value)
	{
		___bone_10 = value;
	}

	inline static int32_t get_offset_of_boneAsString_11() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___boneAsString_11)); }
	inline FsmString_t952858651 * get_boneAsString_11() const { return ___boneAsString_11; }
	inline FsmString_t952858651 ** get_address_of_boneAsString_11() { return &___boneAsString_11; }
	inline void set_boneAsString_11(FsmString_t952858651 * value)
	{
		___boneAsString_11 = value;
		Il2CppCodeGenWriteBarrier(&___boneAsString_11, value);
	}

	inline static int32_t get_offset_of_boneGameObject_12() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___boneGameObject_12)); }
	inline FsmGameObject_t1697147867 * get_boneGameObject_12() const { return ___boneGameObject_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_boneGameObject_12() { return &___boneGameObject_12; }
	inline void set_boneGameObject_12(FsmGameObject_t1697147867 * value)
	{
		___boneGameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___boneGameObject_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
