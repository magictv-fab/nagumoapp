﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarHeader
struct TarHeader_t2980539316;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::.ctor()
extern "C"  void TarHeader__ctor_m765106355 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::.cctor()
extern "C"  void TarHeader__cctor_m1761364314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::get_Name()
extern "C"  String_t* TarHeader_get_Name_m1905494850 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_Name(System.String)
extern "C"  void TarHeader_set_Name_m3755855657 (TarHeader_t2980539316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::GetName()
extern "C"  String_t* TarHeader_GetName_m2150824117 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::get_Mode()
extern "C"  int32_t TarHeader_get_Mode_m1319427023 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_Mode(System.Int32)
extern "C"  void TarHeader_set_Mode_m2061424994 (TarHeader_t2980539316 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::get_UserId()
extern "C"  int32_t TarHeader_get_UserId_m1308321010 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_UserId(System.Int32)
extern "C"  void TarHeader_set_UserId_m1125326213 (TarHeader_t2980539316 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::get_GroupId()
extern "C"  int32_t TarHeader_get_GroupId_m101254320 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_GroupId(System.Int32)
extern "C"  void TarHeader_set_GroupId_m421338111 (TarHeader_t2980539316 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarHeader::get_Size()
extern "C"  int64_t TarHeader_get_Size_m1765240140 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_Size(System.Int64)
extern "C"  void TarHeader_set_Size_m2497187521 (TarHeader_t2980539316 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Tar.TarHeader::get_ModTime()
extern "C"  DateTime_t4283661327  TarHeader_get_ModTime_m3987574704 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_ModTime(System.DateTime)
extern "C"  void TarHeader_set_ModTime_m2871021685 (TarHeader_t2980539316 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::get_Checksum()
extern "C"  int32_t TarHeader_get_Checksum_m1377429743 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarHeader::get_IsChecksumValid()
extern "C"  bool TarHeader_get_IsChecksumValid_m3388540555 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ICSharpCode.SharpZipLib.Tar.TarHeader::get_TypeFlag()
extern "C"  uint8_t TarHeader_get_TypeFlag_m2150462324 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_TypeFlag(System.Byte)
extern "C"  void TarHeader_set_TypeFlag_m1405624439 (TarHeader_t2980539316 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::get_LinkName()
extern "C"  String_t* TarHeader_get_LinkName_m3178207452 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_LinkName(System.String)
extern "C"  void TarHeader_set_LinkName_m2541481807 (TarHeader_t2980539316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::get_Magic()
extern "C"  String_t* TarHeader_get_Magic_m2342896568 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_Magic(System.String)
extern "C"  void TarHeader_set_Magic_m2337131329 (TarHeader_t2980539316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::get_Version()
extern "C"  String_t* TarHeader_get_Version_m1721445795 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_Version(System.String)
extern "C"  void TarHeader_set_Version_m3897406838 (TarHeader_t2980539316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::get_UserName()
extern "C"  String_t* TarHeader_get_UserName_m3927045101 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_UserName(System.String)
extern "C"  void TarHeader_set_UserName_m1484773342 (TarHeader_t2980539316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::get_GroupName()
extern "C"  String_t* TarHeader_get_GroupName_m657745781 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_GroupName(System.String)
extern "C"  void TarHeader_set_GroupName_m1300533028 (TarHeader_t2980539316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::get_DevMajor()
extern "C"  int32_t TarHeader_get_DevMajor_m3526295312 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_DevMajor(System.Int32)
extern "C"  void TarHeader_set_DevMajor_m1049358883 (TarHeader_t2980539316 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::get_DevMinor()
extern "C"  int32_t TarHeader_get_DevMinor_m3759022604 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::set_DevMinor(System.Int32)
extern "C"  void TarHeader_set_DevMinor_m2780986143 (TarHeader_t2980539316 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ICSharpCode.SharpZipLib.Tar.TarHeader::Clone()
extern "C"  Il2CppObject * TarHeader_Clone_m1264639971 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::ParseBuffer(System.Byte[])
extern "C"  void TarHeader_ParseBuffer_m1881501797 (TarHeader_t2980539316 * __this, ByteU5BU5D_t4260760469* ___header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::WriteHeader(System.Byte[])
extern "C"  void TarHeader_WriteHeader_m4134540492 (TarHeader_t2980539316 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetHashCode()
extern "C"  int32_t TarHeader_GetHashCode_m4064526896 (TarHeader_t2980539316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarHeader::Equals(System.Object)
extern "C"  bool TarHeader_Equals_m2953504280 (TarHeader_t2980539316 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::SetValueDefaults(System.Int32,System.String,System.Int32,System.String)
extern "C"  void TarHeader_SetValueDefaults_m455398548 (Il2CppObject * __this /* static, unused */, int32_t ___userId0, String_t* ___userName1, int32_t ___groupId2, String_t* ___groupName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::RestoreSetValues()
extern "C"  void TarHeader_RestoreSetValues_m3472111079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarHeader::ParseBinaryOrOctal(System.Byte[],System.Int32,System.Int32)
extern "C"  int64_t TarHeader_ParseBinaryOrOctal_m622123763 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___header0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarHeader::ParseOctal(System.Byte[],System.Int32,System.Int32)
extern "C"  int64_t TarHeader_ParseOctal_m1303068175 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___header0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder ICSharpCode.SharpZipLib.Tar.TarHeader::ParseName(System.Byte[],System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * TarHeader_ParseName_m665042223 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___header0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetNameBytes(System.Text.StringBuilder,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetNameBytes_m3380171357 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___name0, int32_t ___nameOffset1, ByteU5BU5D_t4260760469* ___buffer2, int32_t ___bufferOffset3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetNameBytes(System.String,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetNameBytes_m1667031493 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___nameOffset1, ByteU5BU5D_t4260760469* ___buffer2, int32_t ___bufferOffset3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetNameBytes(System.Text.StringBuilder,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetNameBytes_m2487645152 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___name0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetNameBytes(System.String,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetNameBytes_m835058552 (Il2CppObject * __this /* static, unused */, String_t* ___name0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetAsciiBytes(System.String,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetAsciiBytes_m1916561895 (Il2CppObject * __this /* static, unused */, String_t* ___toAdd0, int32_t ___nameOffset1, ByteU5BU5D_t4260760469* ___buffer2, int32_t ___bufferOffset3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetOctalBytes(System.Int64,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetOctalBytes_m803496106 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetBinaryOrOctalBytes(System.Int64,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarHeader_GetBinaryOrOctalBytes_m241093262 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarHeader::GetCheckSumOctalBytes(System.Int64,System.Byte[],System.Int32,System.Int32)
extern "C"  void TarHeader_GetCheckSumOctalBytes_m383601247 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::ComputeCheckSum(System.Byte[])
extern "C"  int32_t TarHeader_ComputeCheckSum_m2983353232 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::MakeCheckSum(System.Byte[])
extern "C"  int32_t TarHeader_MakeCheckSum_m2910958453 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::GetCTime(System.DateTime)
extern "C"  int32_t TarHeader_GetCTime_m3952933147 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Tar.TarHeader::GetDateTimeFromCTime(System.Int64)
extern "C"  DateTime_t4283661327  TarHeader_GetDateTimeFromCTime_m338087487 (Il2CppObject * __this /* static, unused */, int64_t ___ticks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
