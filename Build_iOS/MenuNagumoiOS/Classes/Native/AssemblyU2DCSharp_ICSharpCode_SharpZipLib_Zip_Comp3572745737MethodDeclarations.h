﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer
struct PendingBuffer_t3572745737;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::.ctor()
extern "C"  void PendingBuffer__ctor_m2958045402 (PendingBuffer_t3572745737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
extern "C"  void PendingBuffer__ctor_m2074701611 (PendingBuffer_t3572745737 * __this, int32_t ___bufferSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
extern "C"  void PendingBuffer_Reset_m604478343 (PendingBuffer_t3572745737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteByte(System.Int32)
extern "C"  void PendingBuffer_WriteByte_m1376743696 (PendingBuffer_t3572745737 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
extern "C"  void PendingBuffer_WriteShort_m3309086168 (PendingBuffer_t3572745737 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteInt(System.Int32)
extern "C"  void PendingBuffer_WriteInt_m4037873291 (PendingBuffer_t3572745737 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  void PendingBuffer_WriteBlock_m1290106385 (PendingBuffer_t3572745737 * __this, ByteU5BU5D_t4260760469* ___block0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
extern "C"  int32_t PendingBuffer_get_BitCount_m1792095975 (PendingBuffer_t3572745737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
extern "C"  void PendingBuffer_AlignToByte_m3563657280 (PendingBuffer_t3572745737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
extern "C"  void PendingBuffer_WriteBits_m3522401033 (PendingBuffer_t3572745737 * __this, int32_t ___b0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
extern "C"  void PendingBuffer_WriteShortMSB_m2166788200 (PendingBuffer_t3572745737 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
extern "C"  bool PendingBuffer_get_IsFlushed_m2805362460 (PendingBuffer_t3572745737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t PendingBuffer_Flush_m571766559 (PendingBuffer_t3572745737 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::ToByteArray()
extern "C"  ByteU5BU5D_t4260760469* PendingBuffer_ToByteArray_m713429784 (PendingBuffer_t3572745737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
