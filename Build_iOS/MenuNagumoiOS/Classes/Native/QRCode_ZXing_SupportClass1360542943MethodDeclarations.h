﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String ZXing.SupportClass::ToBinaryString(System.Int32)
extern "C"  String_t* SupportClass_ToBinaryString_m3101702909 (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.SupportClass::bitCount(System.Int32)
extern "C"  int32_t SupportClass_bitCount_m3704119933 (Il2CppObject * __this /* static, unused */, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
