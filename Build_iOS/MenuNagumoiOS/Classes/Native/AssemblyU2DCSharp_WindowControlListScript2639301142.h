﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// WindowControlListScript
struct WindowControlListScript_t2639301142;
// MagicTVWindowInAppGUIControllerScript
struct MagicTVWindowInAppGUIControllerScript_t4145218373;
// MagicTVWindowSplashDownloadProgressControllerScript
struct MagicTVWindowSplashDownloadProgressControllerScript_t3918082434;
// MagicTVWindowInstructionsControllerScript
struct MagicTVWindowInstructionsControllerScript_t2425143435;
// MagicTVWindowAboutControllerScript
struct MagicTVWindowAboutControllerScript_t2586885333;
// MagicTVWindowInAppListControllerScriptWaiting
struct MagicTVWindowInAppListControllerScriptWaiting_t3050884011;
// MagicTVWindowErrorMessage
struct MagicTVWindowErrorMessage_t1185091614;
// MagicTVWindowInScreeShotControllerScript
struct MagicTVWindowInScreeShotControllerScript_t1504951103;
// MagicTVWindowInAppCloseControllerScript
struct MagicTVWindowInAppCloseControllerScript_t657370434;
// System.Action
struct Action_t3771233898;
// UnityEngine.Material
struct Material_t3870600107;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// MagicTVAbstractWindowPopUpControllerScript
struct MagicTVAbstractWindowPopUpControllerScript_t2050906514;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowControlListScript
struct  WindowControlListScript_t2639301142  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject WindowControlListScript::ButtonPrefab
	GameObject_t3674682005 * ___ButtonPrefab_2;
	// UnityEngine.GameObject WindowControlListScript::ButtonTable
	GameObject_t3674682005 * ___ButtonTable_3;
	// UnityEngine.GameObject WindowControlListScript::PopUpAlertPrefab
	GameObject_t3674682005 * ___PopUpAlertPrefab_4;
	// UnityEngine.GameObject WindowControlListScript::PopUpConfirmPrefab
	GameObject_t3674682005 * ___PopUpConfirmPrefab_5;
	// UnityEngine.GameObject WindowControlListScript::PopUpPromptPrefab
	GameObject_t3674682005 * ___PopUpPromptPrefab_6;
	// UnityEngine.AnimationClip WindowControlListScript::AminationForLabelChangeDefault
	AnimationClip_t2007702890 * ___AminationForLabelChangeDefault_7;
	// System.Boolean WindowControlListScript::AutoShowInAppGui
	bool ___AutoShowInAppGui_8;
	// UnityEngine.Canvas WindowControlListScript::CanvasRoot
	Canvas_t2727140764 * ___CanvasRoot_9;
	// UnityEngine.ScreenOrientation WindowControlListScript::freezeScreenRotation
	int32_t ___freezeScreenRotation_11;
	// MagicTVWindowInAppGUIControllerScript WindowControlListScript::WindowInAppGUI
	MagicTVWindowInAppGUIControllerScript_t4145218373 * ___WindowInAppGUI_12;
	// MagicTVWindowInAppGUIControllerScript WindowControlListScript::WindowGuiControlers
	MagicTVWindowInAppGUIControllerScript_t4145218373 * ___WindowGuiControlers_13;
	// MagicTVWindowSplashDownloadProgressControllerScript WindowControlListScript::WindowSplashDownloadProgress
	MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * ___WindowSplashDownloadProgress_14;
	// MagicTVWindowInstructionsControllerScript WindowControlListScript::WindowInstructions
	MagicTVWindowInstructionsControllerScript_t2425143435 * ___WindowInstructions_15;
	// MagicTVWindowAboutControllerScript WindowControlListScript::WindowAbout
	MagicTVWindowAboutControllerScript_t2586885333 * ___WindowAbout_16;
	// MagicTVWindowInAppListControllerScriptWaiting WindowControlListScript::WindowInAppListWaiting
	MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * ___WindowInAppListWaiting_17;
	// MagicTVWindowErrorMessage WindowControlListScript::WindowErrorMessage
	MagicTVWindowErrorMessage_t1185091614 * ___WindowErrorMessage_18;
	// MagicTVWindowSplashDownloadProgressControllerScript WindowControlListScript::WindowDownloadProgress
	MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * ___WindowDownloadProgress_19;
	// MagicTVWindowInScreeShotControllerScript WindowControlListScript::WindowScreenShot
	MagicTVWindowInScreeShotControllerScript_t1504951103 * ___WindowScreenShot_20;
	// MagicTVWindowInAppCloseControllerScript WindowControlListScript::WindowScreenClose
	MagicTVWindowInAppCloseControllerScript_t657370434 * ___WindowScreenClose_21;
	// System.Action WindowControlListScript::onClickYouTubeChannel
	Action_t3771233898 * ___onClickYouTubeChannel_22;
	// System.Action WindowControlListScript::onClickYouTubeVideo
	Action_t3771233898 * ___onClickYouTubeVideo_23;
	// System.Action WindowControlListScript::onClickTwitter
	Action_t3771233898 * ___onClickTwitter_24;
	// System.Action WindowControlListScript::onClickYouFacebook
	Action_t3771233898 * ___onClickYouFacebook_25;
	// System.Action WindowControlListScript::onClickEmail
	Action_t3771233898 * ___onClickEmail_26;
	// UnityEngine.Material WindowControlListScript::materialImagemInicial
	Material_t3870600107 * ___materialImagemInicial_27;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WindowControlListScript::popUpControllerList
	List_1_t747900261 * ___popUpControllerList_28;
	// MagicTVAbstractWindowPopUpControllerScript WindowControlListScript::_currentPopUp
	MagicTVAbstractWindowPopUpControllerScript_t2050906514 * ____currentPopUp_29;

public:
	inline static int32_t get_offset_of_ButtonPrefab_2() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___ButtonPrefab_2)); }
	inline GameObject_t3674682005 * get_ButtonPrefab_2() const { return ___ButtonPrefab_2; }
	inline GameObject_t3674682005 ** get_address_of_ButtonPrefab_2() { return &___ButtonPrefab_2; }
	inline void set_ButtonPrefab_2(GameObject_t3674682005 * value)
	{
		___ButtonPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonPrefab_2, value);
	}

	inline static int32_t get_offset_of_ButtonTable_3() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___ButtonTable_3)); }
	inline GameObject_t3674682005 * get_ButtonTable_3() const { return ___ButtonTable_3; }
	inline GameObject_t3674682005 ** get_address_of_ButtonTable_3() { return &___ButtonTable_3; }
	inline void set_ButtonTable_3(GameObject_t3674682005 * value)
	{
		___ButtonTable_3 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonTable_3, value);
	}

	inline static int32_t get_offset_of_PopUpAlertPrefab_4() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___PopUpAlertPrefab_4)); }
	inline GameObject_t3674682005 * get_PopUpAlertPrefab_4() const { return ___PopUpAlertPrefab_4; }
	inline GameObject_t3674682005 ** get_address_of_PopUpAlertPrefab_4() { return &___PopUpAlertPrefab_4; }
	inline void set_PopUpAlertPrefab_4(GameObject_t3674682005 * value)
	{
		___PopUpAlertPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___PopUpAlertPrefab_4, value);
	}

	inline static int32_t get_offset_of_PopUpConfirmPrefab_5() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___PopUpConfirmPrefab_5)); }
	inline GameObject_t3674682005 * get_PopUpConfirmPrefab_5() const { return ___PopUpConfirmPrefab_5; }
	inline GameObject_t3674682005 ** get_address_of_PopUpConfirmPrefab_5() { return &___PopUpConfirmPrefab_5; }
	inline void set_PopUpConfirmPrefab_5(GameObject_t3674682005 * value)
	{
		___PopUpConfirmPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___PopUpConfirmPrefab_5, value);
	}

	inline static int32_t get_offset_of_PopUpPromptPrefab_6() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___PopUpPromptPrefab_6)); }
	inline GameObject_t3674682005 * get_PopUpPromptPrefab_6() const { return ___PopUpPromptPrefab_6; }
	inline GameObject_t3674682005 ** get_address_of_PopUpPromptPrefab_6() { return &___PopUpPromptPrefab_6; }
	inline void set_PopUpPromptPrefab_6(GameObject_t3674682005 * value)
	{
		___PopUpPromptPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___PopUpPromptPrefab_6, value);
	}

	inline static int32_t get_offset_of_AminationForLabelChangeDefault_7() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___AminationForLabelChangeDefault_7)); }
	inline AnimationClip_t2007702890 * get_AminationForLabelChangeDefault_7() const { return ___AminationForLabelChangeDefault_7; }
	inline AnimationClip_t2007702890 ** get_address_of_AminationForLabelChangeDefault_7() { return &___AminationForLabelChangeDefault_7; }
	inline void set_AminationForLabelChangeDefault_7(AnimationClip_t2007702890 * value)
	{
		___AminationForLabelChangeDefault_7 = value;
		Il2CppCodeGenWriteBarrier(&___AminationForLabelChangeDefault_7, value);
	}

	inline static int32_t get_offset_of_AutoShowInAppGui_8() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___AutoShowInAppGui_8)); }
	inline bool get_AutoShowInAppGui_8() const { return ___AutoShowInAppGui_8; }
	inline bool* get_address_of_AutoShowInAppGui_8() { return &___AutoShowInAppGui_8; }
	inline void set_AutoShowInAppGui_8(bool value)
	{
		___AutoShowInAppGui_8 = value;
	}

	inline static int32_t get_offset_of_CanvasRoot_9() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___CanvasRoot_9)); }
	inline Canvas_t2727140764 * get_CanvasRoot_9() const { return ___CanvasRoot_9; }
	inline Canvas_t2727140764 ** get_address_of_CanvasRoot_9() { return &___CanvasRoot_9; }
	inline void set_CanvasRoot_9(Canvas_t2727140764 * value)
	{
		___CanvasRoot_9 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasRoot_9, value);
	}

	inline static int32_t get_offset_of_freezeScreenRotation_11() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___freezeScreenRotation_11)); }
	inline int32_t get_freezeScreenRotation_11() const { return ___freezeScreenRotation_11; }
	inline int32_t* get_address_of_freezeScreenRotation_11() { return &___freezeScreenRotation_11; }
	inline void set_freezeScreenRotation_11(int32_t value)
	{
		___freezeScreenRotation_11 = value;
	}

	inline static int32_t get_offset_of_WindowInAppGUI_12() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowInAppGUI_12)); }
	inline MagicTVWindowInAppGUIControllerScript_t4145218373 * get_WindowInAppGUI_12() const { return ___WindowInAppGUI_12; }
	inline MagicTVWindowInAppGUIControllerScript_t4145218373 ** get_address_of_WindowInAppGUI_12() { return &___WindowInAppGUI_12; }
	inline void set_WindowInAppGUI_12(MagicTVWindowInAppGUIControllerScript_t4145218373 * value)
	{
		___WindowInAppGUI_12 = value;
		Il2CppCodeGenWriteBarrier(&___WindowInAppGUI_12, value);
	}

	inline static int32_t get_offset_of_WindowGuiControlers_13() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowGuiControlers_13)); }
	inline MagicTVWindowInAppGUIControllerScript_t4145218373 * get_WindowGuiControlers_13() const { return ___WindowGuiControlers_13; }
	inline MagicTVWindowInAppGUIControllerScript_t4145218373 ** get_address_of_WindowGuiControlers_13() { return &___WindowGuiControlers_13; }
	inline void set_WindowGuiControlers_13(MagicTVWindowInAppGUIControllerScript_t4145218373 * value)
	{
		___WindowGuiControlers_13 = value;
		Il2CppCodeGenWriteBarrier(&___WindowGuiControlers_13, value);
	}

	inline static int32_t get_offset_of_WindowSplashDownloadProgress_14() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowSplashDownloadProgress_14)); }
	inline MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * get_WindowSplashDownloadProgress_14() const { return ___WindowSplashDownloadProgress_14; }
	inline MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 ** get_address_of_WindowSplashDownloadProgress_14() { return &___WindowSplashDownloadProgress_14; }
	inline void set_WindowSplashDownloadProgress_14(MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * value)
	{
		___WindowSplashDownloadProgress_14 = value;
		Il2CppCodeGenWriteBarrier(&___WindowSplashDownloadProgress_14, value);
	}

	inline static int32_t get_offset_of_WindowInstructions_15() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowInstructions_15)); }
	inline MagicTVWindowInstructionsControllerScript_t2425143435 * get_WindowInstructions_15() const { return ___WindowInstructions_15; }
	inline MagicTVWindowInstructionsControllerScript_t2425143435 ** get_address_of_WindowInstructions_15() { return &___WindowInstructions_15; }
	inline void set_WindowInstructions_15(MagicTVWindowInstructionsControllerScript_t2425143435 * value)
	{
		___WindowInstructions_15 = value;
		Il2CppCodeGenWriteBarrier(&___WindowInstructions_15, value);
	}

	inline static int32_t get_offset_of_WindowAbout_16() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowAbout_16)); }
	inline MagicTVWindowAboutControllerScript_t2586885333 * get_WindowAbout_16() const { return ___WindowAbout_16; }
	inline MagicTVWindowAboutControllerScript_t2586885333 ** get_address_of_WindowAbout_16() { return &___WindowAbout_16; }
	inline void set_WindowAbout_16(MagicTVWindowAboutControllerScript_t2586885333 * value)
	{
		___WindowAbout_16 = value;
		Il2CppCodeGenWriteBarrier(&___WindowAbout_16, value);
	}

	inline static int32_t get_offset_of_WindowInAppListWaiting_17() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowInAppListWaiting_17)); }
	inline MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * get_WindowInAppListWaiting_17() const { return ___WindowInAppListWaiting_17; }
	inline MagicTVWindowInAppListControllerScriptWaiting_t3050884011 ** get_address_of_WindowInAppListWaiting_17() { return &___WindowInAppListWaiting_17; }
	inline void set_WindowInAppListWaiting_17(MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * value)
	{
		___WindowInAppListWaiting_17 = value;
		Il2CppCodeGenWriteBarrier(&___WindowInAppListWaiting_17, value);
	}

	inline static int32_t get_offset_of_WindowErrorMessage_18() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowErrorMessage_18)); }
	inline MagicTVWindowErrorMessage_t1185091614 * get_WindowErrorMessage_18() const { return ___WindowErrorMessage_18; }
	inline MagicTVWindowErrorMessage_t1185091614 ** get_address_of_WindowErrorMessage_18() { return &___WindowErrorMessage_18; }
	inline void set_WindowErrorMessage_18(MagicTVWindowErrorMessage_t1185091614 * value)
	{
		___WindowErrorMessage_18 = value;
		Il2CppCodeGenWriteBarrier(&___WindowErrorMessage_18, value);
	}

	inline static int32_t get_offset_of_WindowDownloadProgress_19() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowDownloadProgress_19)); }
	inline MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * get_WindowDownloadProgress_19() const { return ___WindowDownloadProgress_19; }
	inline MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 ** get_address_of_WindowDownloadProgress_19() { return &___WindowDownloadProgress_19; }
	inline void set_WindowDownloadProgress_19(MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * value)
	{
		___WindowDownloadProgress_19 = value;
		Il2CppCodeGenWriteBarrier(&___WindowDownloadProgress_19, value);
	}

	inline static int32_t get_offset_of_WindowScreenShot_20() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowScreenShot_20)); }
	inline MagicTVWindowInScreeShotControllerScript_t1504951103 * get_WindowScreenShot_20() const { return ___WindowScreenShot_20; }
	inline MagicTVWindowInScreeShotControllerScript_t1504951103 ** get_address_of_WindowScreenShot_20() { return &___WindowScreenShot_20; }
	inline void set_WindowScreenShot_20(MagicTVWindowInScreeShotControllerScript_t1504951103 * value)
	{
		___WindowScreenShot_20 = value;
		Il2CppCodeGenWriteBarrier(&___WindowScreenShot_20, value);
	}

	inline static int32_t get_offset_of_WindowScreenClose_21() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___WindowScreenClose_21)); }
	inline MagicTVWindowInAppCloseControllerScript_t657370434 * get_WindowScreenClose_21() const { return ___WindowScreenClose_21; }
	inline MagicTVWindowInAppCloseControllerScript_t657370434 ** get_address_of_WindowScreenClose_21() { return &___WindowScreenClose_21; }
	inline void set_WindowScreenClose_21(MagicTVWindowInAppCloseControllerScript_t657370434 * value)
	{
		___WindowScreenClose_21 = value;
		Il2CppCodeGenWriteBarrier(&___WindowScreenClose_21, value);
	}

	inline static int32_t get_offset_of_onClickYouTubeChannel_22() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___onClickYouTubeChannel_22)); }
	inline Action_t3771233898 * get_onClickYouTubeChannel_22() const { return ___onClickYouTubeChannel_22; }
	inline Action_t3771233898 ** get_address_of_onClickYouTubeChannel_22() { return &___onClickYouTubeChannel_22; }
	inline void set_onClickYouTubeChannel_22(Action_t3771233898 * value)
	{
		___onClickYouTubeChannel_22 = value;
		Il2CppCodeGenWriteBarrier(&___onClickYouTubeChannel_22, value);
	}

	inline static int32_t get_offset_of_onClickYouTubeVideo_23() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___onClickYouTubeVideo_23)); }
	inline Action_t3771233898 * get_onClickYouTubeVideo_23() const { return ___onClickYouTubeVideo_23; }
	inline Action_t3771233898 ** get_address_of_onClickYouTubeVideo_23() { return &___onClickYouTubeVideo_23; }
	inline void set_onClickYouTubeVideo_23(Action_t3771233898 * value)
	{
		___onClickYouTubeVideo_23 = value;
		Il2CppCodeGenWriteBarrier(&___onClickYouTubeVideo_23, value);
	}

	inline static int32_t get_offset_of_onClickTwitter_24() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___onClickTwitter_24)); }
	inline Action_t3771233898 * get_onClickTwitter_24() const { return ___onClickTwitter_24; }
	inline Action_t3771233898 ** get_address_of_onClickTwitter_24() { return &___onClickTwitter_24; }
	inline void set_onClickTwitter_24(Action_t3771233898 * value)
	{
		___onClickTwitter_24 = value;
		Il2CppCodeGenWriteBarrier(&___onClickTwitter_24, value);
	}

	inline static int32_t get_offset_of_onClickYouFacebook_25() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___onClickYouFacebook_25)); }
	inline Action_t3771233898 * get_onClickYouFacebook_25() const { return ___onClickYouFacebook_25; }
	inline Action_t3771233898 ** get_address_of_onClickYouFacebook_25() { return &___onClickYouFacebook_25; }
	inline void set_onClickYouFacebook_25(Action_t3771233898 * value)
	{
		___onClickYouFacebook_25 = value;
		Il2CppCodeGenWriteBarrier(&___onClickYouFacebook_25, value);
	}

	inline static int32_t get_offset_of_onClickEmail_26() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___onClickEmail_26)); }
	inline Action_t3771233898 * get_onClickEmail_26() const { return ___onClickEmail_26; }
	inline Action_t3771233898 ** get_address_of_onClickEmail_26() { return &___onClickEmail_26; }
	inline void set_onClickEmail_26(Action_t3771233898 * value)
	{
		___onClickEmail_26 = value;
		Il2CppCodeGenWriteBarrier(&___onClickEmail_26, value);
	}

	inline static int32_t get_offset_of_materialImagemInicial_27() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___materialImagemInicial_27)); }
	inline Material_t3870600107 * get_materialImagemInicial_27() const { return ___materialImagemInicial_27; }
	inline Material_t3870600107 ** get_address_of_materialImagemInicial_27() { return &___materialImagemInicial_27; }
	inline void set_materialImagemInicial_27(Material_t3870600107 * value)
	{
		___materialImagemInicial_27 = value;
		Il2CppCodeGenWriteBarrier(&___materialImagemInicial_27, value);
	}

	inline static int32_t get_offset_of_popUpControllerList_28() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ___popUpControllerList_28)); }
	inline List_1_t747900261 * get_popUpControllerList_28() const { return ___popUpControllerList_28; }
	inline List_1_t747900261 ** get_address_of_popUpControllerList_28() { return &___popUpControllerList_28; }
	inline void set_popUpControllerList_28(List_1_t747900261 * value)
	{
		___popUpControllerList_28 = value;
		Il2CppCodeGenWriteBarrier(&___popUpControllerList_28, value);
	}

	inline static int32_t get_offset_of__currentPopUp_29() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142, ____currentPopUp_29)); }
	inline MagicTVAbstractWindowPopUpControllerScript_t2050906514 * get__currentPopUp_29() const { return ____currentPopUp_29; }
	inline MagicTVAbstractWindowPopUpControllerScript_t2050906514 ** get_address_of__currentPopUp_29() { return &____currentPopUp_29; }
	inline void set__currentPopUp_29(MagicTVAbstractWindowPopUpControllerScript_t2050906514 * value)
	{
		____currentPopUp_29 = value;
		Il2CppCodeGenWriteBarrier(&____currentPopUp_29, value);
	}
};

struct WindowControlListScript_t2639301142_StaticFields
{
public:
	// WindowControlListScript WindowControlListScript::Instance
	WindowControlListScript_t2639301142 * ___Instance_10;

public:
	inline static int32_t get_offset_of_Instance_10() { return static_cast<int32_t>(offsetof(WindowControlListScript_t2639301142_StaticFields, ___Instance_10)); }
	inline WindowControlListScript_t2639301142 * get_Instance_10() const { return ___Instance_10; }
	inline WindowControlListScript_t2639301142 ** get_address_of_Instance_10() { return &___Instance_10; }
	inline void set_Instance_10(WindowControlListScript_t2639301142 * value)
	{
		___Instance_10 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
