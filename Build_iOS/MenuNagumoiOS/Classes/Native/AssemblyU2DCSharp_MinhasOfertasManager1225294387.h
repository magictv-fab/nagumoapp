﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertasData
struct OfertasData_t2909397772;
// MinhasOfertasManager
struct MinhasOfertasManager_t1225294387;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MinhasOfertasManager
struct  MinhasOfertasManager_t1225294387  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MinhasOfertasManager::loadingObj
	GameObject_t3674682005 * ___loadingObj_13;
	// UnityEngine.GameObject MinhasOfertasManager::popup
	GameObject_t3674682005 * ___popup_14;
	// UnityEngine.GameObject MinhasOfertasManager::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_15;
	// UnityEngine.GameObject MinhasOfertasManager::containerItens
	GameObject_t3674682005 * ___containerItens_16;
	// UnityEngine.UI.ScrollRect MinhasOfertasManager::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_18;
	// System.Int32 MinhasOfertasManager::ofertaCounter
	int32_t ___ofertaCounter_19;
	// UnityEngine.GameObject MinhasOfertasManager::currentItem
	GameObject_t3674682005 * ___currentItem_20;

public:
	inline static int32_t get_offset_of_loadingObj_13() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___loadingObj_13)); }
	inline GameObject_t3674682005 * get_loadingObj_13() const { return ___loadingObj_13; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_13() { return &___loadingObj_13; }
	inline void set_loadingObj_13(GameObject_t3674682005 * value)
	{
		___loadingObj_13 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_13, value);
	}

	inline static int32_t get_offset_of_popup_14() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___popup_14)); }
	inline GameObject_t3674682005 * get_popup_14() const { return ___popup_14; }
	inline GameObject_t3674682005 ** get_address_of_popup_14() { return &___popup_14; }
	inline void set_popup_14(GameObject_t3674682005 * value)
	{
		___popup_14 = value;
		Il2CppCodeGenWriteBarrier(&___popup_14, value);
	}

	inline static int32_t get_offset_of_itemPrefab_15() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___itemPrefab_15)); }
	inline GameObject_t3674682005 * get_itemPrefab_15() const { return ___itemPrefab_15; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_15() { return &___itemPrefab_15; }
	inline void set_itemPrefab_15(GameObject_t3674682005 * value)
	{
		___itemPrefab_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_15, value);
	}

	inline static int32_t get_offset_of_containerItens_16() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___containerItens_16)); }
	inline GameObject_t3674682005 * get_containerItens_16() const { return ___containerItens_16; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_16() { return &___containerItens_16; }
	inline void set_containerItens_16(GameObject_t3674682005 * value)
	{
		___containerItens_16 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_16, value);
	}

	inline static int32_t get_offset_of_scrollRect_18() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___scrollRect_18)); }
	inline ScrollRect_t3606982749 * get_scrollRect_18() const { return ___scrollRect_18; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_18() { return &___scrollRect_18; }
	inline void set_scrollRect_18(ScrollRect_t3606982749 * value)
	{
		___scrollRect_18 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_18, value);
	}

	inline static int32_t get_offset_of_ofertaCounter_19() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___ofertaCounter_19)); }
	inline int32_t get_ofertaCounter_19() const { return ___ofertaCounter_19; }
	inline int32_t* get_address_of_ofertaCounter_19() { return &___ofertaCounter_19; }
	inline void set_ofertaCounter_19(int32_t value)
	{
		___ofertaCounter_19 = value;
	}

	inline static int32_t get_offset_of_currentItem_20() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387, ___currentItem_20)); }
	inline GameObject_t3674682005 * get_currentItem_20() const { return ___currentItem_20; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_20() { return &___currentItem_20; }
	inline void set_currentItem_20(GameObject_t3674682005 * value)
	{
		___currentItem_20 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_20, value);
	}
};

struct MinhasOfertasManager_t1225294387_StaticFields
{
public:
	// OfertasData MinhasOfertasManager::ofertasData
	OfertasData_t2909397772 * ___ofertasData_2;
	// MinhasOfertasManager MinhasOfertasManager::instance
	MinhasOfertasManager_t1225294387 * ___instance_3;
	// System.Collections.Generic.List`1<System.String> MinhasOfertasManager::titleLst
	List_1_t1375417109 * ___titleLst_4;
	// System.Collections.Generic.List`1<System.String> MinhasOfertasManager::valueLst
	List_1_t1375417109 * ___valueLst_5;
	// System.Collections.Generic.List`1<System.String> MinhasOfertasManager::infoLst
	List_1_t1375417109 * ___infoLst_6;
	// System.Collections.Generic.List`1<System.String> MinhasOfertasManager::idLst
	List_1_t1375417109 * ___idLst_7;
	// System.Collections.Generic.List`1<System.String> MinhasOfertasManager::validadeLst
	List_1_t1375417109 * ___validadeLst_8;
	// System.Collections.Generic.List`1<System.String> MinhasOfertasManager::aquisicaoLst
	List_1_t1375417109 * ___aquisicaoLst_9;
	// System.Collections.Generic.List`1<System.Int32> MinhasOfertasManager::unidadesLst
	List_1_t2522024052 * ___unidadesLst_10;
	// System.Collections.Generic.List`1<System.Single> MinhasOfertasManager::pesoLst
	List_1_t1365137228 * ___pesoLst_11;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> MinhasOfertasManager::imgLst
	List_1_t272385497 * ___imgLst_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MinhasOfertasManager::itensList
	List_1_t747900261 * ___itensList_17;
	// System.String MinhasOfertasManager::currentJsonTxt
	String_t* ___currentJsonTxt_21;
	// System.Int32 MinhasOfertasManager::loadedImgsCount
	int32_t ___loadedImgsCount_22;

public:
	inline static int32_t get_offset_of_ofertasData_2() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___ofertasData_2)); }
	inline OfertasData_t2909397772 * get_ofertasData_2() const { return ___ofertasData_2; }
	inline OfertasData_t2909397772 ** get_address_of_ofertasData_2() { return &___ofertasData_2; }
	inline void set_ofertasData_2(OfertasData_t2909397772 * value)
	{
		___ofertasData_2 = value;
		Il2CppCodeGenWriteBarrier(&___ofertasData_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___instance_3)); }
	inline MinhasOfertasManager_t1225294387 * get_instance_3() const { return ___instance_3; }
	inline MinhasOfertasManager_t1225294387 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(MinhasOfertasManager_t1225294387 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of_titleLst_4() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___titleLst_4)); }
	inline List_1_t1375417109 * get_titleLst_4() const { return ___titleLst_4; }
	inline List_1_t1375417109 ** get_address_of_titleLst_4() { return &___titleLst_4; }
	inline void set_titleLst_4(List_1_t1375417109 * value)
	{
		___titleLst_4 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_4, value);
	}

	inline static int32_t get_offset_of_valueLst_5() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___valueLst_5)); }
	inline List_1_t1375417109 * get_valueLst_5() const { return ___valueLst_5; }
	inline List_1_t1375417109 ** get_address_of_valueLst_5() { return &___valueLst_5; }
	inline void set_valueLst_5(List_1_t1375417109 * value)
	{
		___valueLst_5 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_5, value);
	}

	inline static int32_t get_offset_of_infoLst_6() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___infoLst_6)); }
	inline List_1_t1375417109 * get_infoLst_6() const { return ___infoLst_6; }
	inline List_1_t1375417109 ** get_address_of_infoLst_6() { return &___infoLst_6; }
	inline void set_infoLst_6(List_1_t1375417109 * value)
	{
		___infoLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_6, value);
	}

	inline static int32_t get_offset_of_idLst_7() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___idLst_7)); }
	inline List_1_t1375417109 * get_idLst_7() const { return ___idLst_7; }
	inline List_1_t1375417109 ** get_address_of_idLst_7() { return &___idLst_7; }
	inline void set_idLst_7(List_1_t1375417109 * value)
	{
		___idLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_7, value);
	}

	inline static int32_t get_offset_of_validadeLst_8() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___validadeLst_8)); }
	inline List_1_t1375417109 * get_validadeLst_8() const { return ___validadeLst_8; }
	inline List_1_t1375417109 ** get_address_of_validadeLst_8() { return &___validadeLst_8; }
	inline void set_validadeLst_8(List_1_t1375417109 * value)
	{
		___validadeLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___validadeLst_8, value);
	}

	inline static int32_t get_offset_of_aquisicaoLst_9() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___aquisicaoLst_9)); }
	inline List_1_t1375417109 * get_aquisicaoLst_9() const { return ___aquisicaoLst_9; }
	inline List_1_t1375417109 ** get_address_of_aquisicaoLst_9() { return &___aquisicaoLst_9; }
	inline void set_aquisicaoLst_9(List_1_t1375417109 * value)
	{
		___aquisicaoLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___aquisicaoLst_9, value);
	}

	inline static int32_t get_offset_of_unidadesLst_10() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___unidadesLst_10)); }
	inline List_1_t2522024052 * get_unidadesLst_10() const { return ___unidadesLst_10; }
	inline List_1_t2522024052 ** get_address_of_unidadesLst_10() { return &___unidadesLst_10; }
	inline void set_unidadesLst_10(List_1_t2522024052 * value)
	{
		___unidadesLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___unidadesLst_10, value);
	}

	inline static int32_t get_offset_of_pesoLst_11() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___pesoLst_11)); }
	inline List_1_t1365137228 * get_pesoLst_11() const { return ___pesoLst_11; }
	inline List_1_t1365137228 ** get_address_of_pesoLst_11() { return &___pesoLst_11; }
	inline void set_pesoLst_11(List_1_t1365137228 * value)
	{
		___pesoLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___pesoLst_11, value);
	}

	inline static int32_t get_offset_of_imgLst_12() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___imgLst_12)); }
	inline List_1_t272385497 * get_imgLst_12() const { return ___imgLst_12; }
	inline List_1_t272385497 ** get_address_of_imgLst_12() { return &___imgLst_12; }
	inline void set_imgLst_12(List_1_t272385497 * value)
	{
		___imgLst_12 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_12, value);
	}

	inline static int32_t get_offset_of_itensList_17() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___itensList_17)); }
	inline List_1_t747900261 * get_itensList_17() const { return ___itensList_17; }
	inline List_1_t747900261 ** get_address_of_itensList_17() { return &___itensList_17; }
	inline void set_itensList_17(List_1_t747900261 * value)
	{
		___itensList_17 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_17, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_21() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___currentJsonTxt_21)); }
	inline String_t* get_currentJsonTxt_21() const { return ___currentJsonTxt_21; }
	inline String_t** get_address_of_currentJsonTxt_21() { return &___currentJsonTxt_21; }
	inline void set_currentJsonTxt_21(String_t* value)
	{
		___currentJsonTxt_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_21, value);
	}

	inline static int32_t get_offset_of_loadedImgsCount_22() { return static_cast<int32_t>(offsetof(MinhasOfertasManager_t1225294387_StaticFields, ___loadedImgsCount_22)); }
	inline int32_t get_loadedImgsCount_22() const { return ___loadedImgsCount_22; }
	inline int32_t* get_address_of_loadedImgsCount_22() { return &___loadedImgsCount_22; }
	inline void set_loadedImgsCount_22(int32_t value)
	{
		___loadedImgsCount_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
