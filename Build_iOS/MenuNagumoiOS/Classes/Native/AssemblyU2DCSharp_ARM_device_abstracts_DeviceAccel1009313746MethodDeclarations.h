﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract
struct DeviceAccelerometerEventDispetcherAbstract_t1009313746;
// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler
struct OnVoidEventHandler_t1904900265;
// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler
struct OnMoveHandler_t662891892;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_device_abstracts_DeviceAccel1904900265.h"
#include "AssemblyU2DCSharp_ARM_device_abstracts_DeviceAccele662891892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::.ctor()
extern "C"  void DeviceAccelerometerEventDispetcherAbstract__ctor_m3836603202 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::add_onShakeInit(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_add_onShakeInit_m1364136816 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::remove_onShakeInit(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_remove_onShakeInit_m414821621 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::add_onShakeEnd(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_add_onShakeEnd_m2915277035 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::remove_onShakeEnd(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_remove_onShakeEnd_m529349318 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::add_onMoveInit(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_add_onMoveInit_m1390748535 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::remove_onMoveInit(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_remove_onMoveInit_m3299788114 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::add_onMoveEnd(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_add_onMoveEnd_m1253567492 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::remove_onMoveEnd(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_remove_onMoveEnd_m345318089 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::add_onMove(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_add_onMove_m3141173166 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnMoveHandler_t662891892 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::remove_onMove(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_remove_onMove_m912870259 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnMoveHandler_t662891892 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::get__lastVector()
extern "C"  Vector3_t4282066566  DeviceAccelerometerEventDispetcherAbstract_get__lastVector_m239270569 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::set__lastVector(UnityEngine.Vector3)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_set__lastVector_m267812926 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::addOnMoveListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_addOnMoveListener_m2693910521 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnMoveHandler_t662891892 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::addOnMoveInitListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_addOnMoveInitListener_m1642025356 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::addOnMoveEndListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_addOnMoveEndListener_m2259117175 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::addOnShakeInitListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_addOnShakeInitListener_m780235811 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::addOnShakeEndListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_addOnShakeEndListener_m1954222848 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::removeOnMoveListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_removeOnMoveListener_m1417686164 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnMoveHandler_t662891892 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::removeOnMoveInitListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_removeOnMoveInitListener_m2261775185 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::removeOnMoveEndListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_removeOnMoveEndListener_m3387487762 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::removeOnShakeInitListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_removeOnShakeInitListener_m2812611326 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::removeOnShakeEndListener(ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract_removeOnShakeEndListener_m2573972677 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, OnVoidEventHandler_t1904900265 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_onShakeInit()
extern "C"  void DeviceAccelerometerEventDispetcherAbstract__onShakeInit_m4285419258 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_onShakeEnd()
extern "C"  void DeviceAccelerometerEventDispetcherAbstract__onShakeEnd_m1658558739 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_onMoveInit()
extern "C"  void DeviceAccelerometerEventDispetcherAbstract__onMoveInit_m3515698847 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_onMoveEnd()
extern "C"  void DeviceAccelerometerEventDispetcherAbstract__onMoveEnd_m3434844366 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_onMove(UnityEngine.Vector3)
extern "C"  void DeviceAccelerometerEventDispetcherAbstract__onMove_m1763883786 (DeviceAccelerometerEventDispetcherAbstract_t1009313746 * __this, Vector3_t4282066566  ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
