﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.GarbageCollector
struct GarbageCollector_t1305589272;
// ARM.animation.ViewTimeControllAbstract
struct ViewTimeControllAbstract_t2357615493;
// MagicTV.in_apps.GarbageItemVO
struct GarbageItemVO_t1335202303;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2357615493.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_GarbageItemVO1335202303.h"

// System.Void MagicTV.in_apps.GarbageCollector::.ctor()
extern "C"  void GarbageCollector__ctor_m3140036130 (GarbageCollector_t1305589272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.GarbageCollector::AddToIncinerationList(ARM.animation.ViewTimeControllAbstract)
extern "C"  void GarbageCollector_AddToIncinerationList_m178307680 (GarbageCollector_t1305589272 * __this, ViewTimeControllAbstract_t2357615493 * ___presentation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.GarbageCollector::LateUpdate()
extern "C"  void GarbageCollector_LateUpdate_m1635297649 (GarbageCollector_t1305589272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.in_apps.GarbageItemVO MagicTV.in_apps.GarbageCollector::getGarbageItem(System.String)
extern "C"  GarbageItemVO_t1335202303 * GarbageCollector_getGarbageItem_m3029776923 (GarbageCollector_t1305589272 * __this, String_t* ___trackingName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.animation.ViewTimeControllAbstract MagicTV.in_apps.GarbageCollector::getPresentation(System.String)
extern "C"  ViewTimeControllAbstract_t2357615493 * GarbageCollector_getPresentation_m1928224080 (GarbageCollector_t1305589272 * __this, String_t* ___trackName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.GarbageCollector::clearAll()
extern "C"  void GarbageCollector_clearAll_m3923795286 (GarbageCollector_t1305589272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.GarbageCollector::doDestroy(MagicTV.in_apps.GarbageItemVO)
extern "C"  void GarbageCollector_doDestroy_m1652933763 (GarbageCollector_t1305589272 * __this, GarbageItemVO_t1335202303 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.GarbageCollector::RemoveFromList(ARM.animation.ViewTimeControllAbstract)
extern "C"  void GarbageCollector_RemoveFromList_m3975153519 (GarbageCollector_t1305589272 * __this, ViewTimeControllAbstract_t2357615493 * ___presentation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.GarbageCollector::<LateUpdate>m__4C(MagicTV.in_apps.GarbageItemVO)
extern "C"  void GarbageCollector_U3CLateUpdateU3Em__4C_m2404498985 (Il2CppObject * __this /* static, unused */, GarbageItemVO_t1335202303 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.in_apps.GarbageCollector::<LateUpdate>m__4D(MagicTV.in_apps.GarbageItemVO)
extern "C"  bool GarbageCollector_U3CLateUpdateU3Em__4D_m1484359612 (GarbageCollector_t1305589272 * __this, GarbageItemVO_t1335202303 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
