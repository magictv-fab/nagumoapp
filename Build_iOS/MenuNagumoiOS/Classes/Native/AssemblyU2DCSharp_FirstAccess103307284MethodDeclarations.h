﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FirstAccess
struct FirstAccess_t103307284;

#include "codegen/il2cpp-codegen.h"

// System.Void FirstAccess::.ctor()
extern "C"  void FirstAccess__ctor_m245561815 (FirstAccess_t103307284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstAccess::Awake()
extern "C"  void FirstAccess_Awake_m483167034 (FirstAccess_t103307284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstAccess::Update()
extern "C"  void FirstAccess_Update_m749343766 (FirstAccess_t103307284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstAccess::ButtonImprimir()
extern "C"  void FirstAccess_ButtonImprimir_m2112010770 (FirstAccess_t103307284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
