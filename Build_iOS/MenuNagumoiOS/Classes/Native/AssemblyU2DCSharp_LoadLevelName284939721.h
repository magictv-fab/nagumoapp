﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadLevelName
struct  LoadLevelName_t284939721  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject LoadLevelName::disableObj
	GameObject_t3674682005 * ___disableObj_4;
	// UnityEngine.GameObject LoadLevelName::enableAcougue
	GameObject_t3674682005 * ___enableAcougue_5;
	// System.Boolean LoadLevelName::back
	bool ___back_6;
	// System.String LoadLevelName::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_disableObj_4() { return static_cast<int32_t>(offsetof(LoadLevelName_t284939721, ___disableObj_4)); }
	inline GameObject_t3674682005 * get_disableObj_4() const { return ___disableObj_4; }
	inline GameObject_t3674682005 ** get_address_of_disableObj_4() { return &___disableObj_4; }
	inline void set_disableObj_4(GameObject_t3674682005 * value)
	{
		___disableObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___disableObj_4, value);
	}

	inline static int32_t get_offset_of_enableAcougue_5() { return static_cast<int32_t>(offsetof(LoadLevelName_t284939721, ___enableAcougue_5)); }
	inline GameObject_t3674682005 * get_enableAcougue_5() const { return ___enableAcougue_5; }
	inline GameObject_t3674682005 ** get_address_of_enableAcougue_5() { return &___enableAcougue_5; }
	inline void set_enableAcougue_5(GameObject_t3674682005 * value)
	{
		___enableAcougue_5 = value;
		Il2CppCodeGenWriteBarrier(&___enableAcougue_5, value);
	}

	inline static int32_t get_offset_of_back_6() { return static_cast<int32_t>(offsetof(LoadLevelName_t284939721, ___back_6)); }
	inline bool get_back_6() const { return ___back_6; }
	inline bool* get_address_of_back_6() { return &___back_6; }
	inline void set_back_6(bool value)
	{
		___back_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(LoadLevelName_t284939721, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier(&___name_7, value);
	}
};

struct LoadLevelName_t284939721_StaticFields
{
public:
	// System.String LoadLevelName::loadedScene
	String_t* ___loadedScene_2;
	// System.Boolean LoadLevelName::isAcougue
	bool ___isAcougue_3;

public:
	inline static int32_t get_offset_of_loadedScene_2() { return static_cast<int32_t>(offsetof(LoadLevelName_t284939721_StaticFields, ___loadedScene_2)); }
	inline String_t* get_loadedScene_2() const { return ___loadedScene_2; }
	inline String_t** get_address_of_loadedScene_2() { return &___loadedScene_2; }
	inline void set_loadedScene_2(String_t* value)
	{
		___loadedScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadedScene_2, value);
	}

	inline static int32_t get_offset_of_isAcougue_3() { return static_cast<int32_t>(offsetof(LoadLevelName_t284939721_StaticFields, ___isAcougue_3)); }
	inline bool get_isAcougue_3() const { return ___isAcougue_3; }
	inline bool* get_address_of_isAcougue_3() { return &___isAcougue_3; }
	inline void set_isAcougue_3(bool value)
	{
		___isAcougue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
