﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>
struct ValueCollection_t1211489805;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va442717500.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1189758011_gshared (ValueCollection_t1211489805 * __this, Dictionary_2_t2510884092 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1189758011(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1211489805 *, Dictionary_2_t2510884092 *, const MethodInfo*))ValueCollection__ctor_m1189758011_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2817328279_gshared (ValueCollection_t1211489805 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2817328279(__this, ___item0, method) ((  void (*) (ValueCollection_t1211489805 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2817328279_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2345850080_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2345850080(__this, method) ((  void (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2345850080_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m515246447_gshared (ValueCollection_t1211489805 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m515246447(__this, ___item0, method) ((  bool (*) (ValueCollection_t1211489805 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m515246447_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1494266324_gshared (ValueCollection_t1211489805 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1494266324(__this, ___item0, method) ((  bool (*) (ValueCollection_t1211489805 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1494266324_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4033103278_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4033103278(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4033103278_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1705827812_gshared (ValueCollection_t1211489805 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1705827812(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1211489805 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1705827812_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1102254687_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1102254687(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1102254687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3085389602_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3085389602(__this, method) ((  bool (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3085389602_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2325854210_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2325854210(__this, method) ((  bool (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2325854210_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m836801070_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m836801070(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m836801070_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m133534146_gshared (ValueCollection_t1211489805 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m133534146(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1211489805 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m133534146_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::GetEnumerator()
extern "C"  Enumerator_t442717500  ValueCollection_GetEnumerator_m4076504933_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m4076504933(__this, method) ((  Enumerator_t442717500  (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_GetEnumerator_m4076504933_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m371776456_gshared (ValueCollection_t1211489805 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m371776456(__this, method) ((  int32_t (*) (ValueCollection_t1211489805 *, const MethodInfo*))ValueCollection_get_Count_m371776456_gshared)(__this, method)
