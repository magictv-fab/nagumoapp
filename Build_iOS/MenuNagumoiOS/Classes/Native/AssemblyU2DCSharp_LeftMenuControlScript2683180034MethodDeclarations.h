﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeftMenuControlScript
struct LeftMenuControlScript_t2683180034;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientationT3688806624.h"

// System.Void LeftMenuControlScript::.ctor()
extern "C"  void LeftMenuControlScript__ctor_m1419921321 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LeftMenuControlScript::GetStateOpened()
extern "C"  bool LeftMenuControlScript_GetStateOpened_m3957752043 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::Hide()
extern "C"  void LeftMenuControlScript_Hide_m518119741 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::Show()
extern "C"  void LeftMenuControlScript_Show_m832461880 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::OnClick()
extern "C"  void LeftMenuControlScript_OnClick_m588994544 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::OnChangeScreenOrientationType(UnityEngine.Vector2,ARM.gui.utils.ScreenOrientationType)
extern "C"  void LeftMenuControlScript_OnChangeScreenOrientationType_m3569902996 (LeftMenuControlScript_t2683180034 * __this, Vector2_t4282066565  ___screenSize0, int32_t ___screenOrientationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::Start()
extern "C"  void LeftMenuControlScript_Start_m367059113 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::OnDestroy()
extern "C"  void LeftMenuControlScript_OnDestroy_m3041271842 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeftMenuControlScript::Update()
extern "C"  void LeftMenuControlScript_Update_m2794750084 (LeftMenuControlScript_t2683180034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
