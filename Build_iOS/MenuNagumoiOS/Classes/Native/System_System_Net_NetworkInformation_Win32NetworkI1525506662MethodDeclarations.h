﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32NetworkInterface2
struct Win32NetworkInterface2_t1525506662;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t3597816152;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Net.NetworkInformation.NetworkInterface[]
struct NetworkInterfaceU5BU5D_t611619240;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES[]
struct Win32_IP_ADAPTER_ADDRESSESU5BU5D_t3677701705;
// System.Net.NetworkInformation.PhysicalAddress
struct PhysicalAddress_t2881305111;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP3597816152.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR1777568538.h"

// System.Void System.Net.NetworkInformation.Win32NetworkInterface2::.ctor(System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES)
extern "C"  void Win32NetworkInterface2__ctor_m134395154 (Win32NetworkInterface2_t1525506662 * __this, Win32_IP_ADAPTER_ADDRESSES_t3597816152 * ___addr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32NetworkInterface2::GetAdaptersAddresses(System.UInt32,System.UInt32,System.IntPtr,System.Byte[],System.Int32&)
extern "C"  int32_t Win32NetworkInterface2_GetAdaptersAddresses_m3922364808 (Il2CppObject * __this /* static, unused */, uint32_t ___family0, uint32_t ___flags1, IntPtr_t ___reserved2, ByteU5BU5D_t4260760469* ___info3, int32_t* ___size4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32NetworkInterface2::GetIfEntry(System.Net.NetworkInformation.Win32_MIB_IFROW&)
extern "C"  int32_t Win32NetworkInterface2_GetIfEntry_m2863289629 (Il2CppObject * __this /* static, unused */, Win32_MIB_IFROW_t1777568538 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.NetworkInterface[] System.Net.NetworkInformation.Win32NetworkInterface2::ImplGetAllNetworkInterfaces()
extern "C"  NetworkInterfaceU5BU5D_t611619240* Win32NetworkInterface2_ImplGetAllNetworkInterfaces_m793525145 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES[] System.Net.NetworkInformation.Win32NetworkInterface2::GetAdaptersAddresses()
extern "C"  Win32_IP_ADAPTER_ADDRESSESU5BU5D_t3677701705* Win32NetworkInterface2_GetAdaptersAddresses_m1975031419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PhysicalAddress System.Net.NetworkInformation.Win32NetworkInterface2::GetPhysicalAddress()
extern "C"  PhysicalAddress_t2881305111 * Win32NetworkInterface2_GetPhysicalAddress_m3891855633 (Win32NetworkInterface2_t1525506662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
