﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonAnimationSoundScript
struct ButtonAnimationSoundScript_t4202863368;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonAnimationSoundScript::.ctor()
extern "C"  void ButtonAnimationSoundScript__ctor_m3105863315 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimationSoundScript::Start()
extern "C"  void ButtonAnimationSoundScript_Start_m2053001107 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimationSoundScript::OnClick()
extern "C"  void ButtonAnimationSoundScript_OnClick_m1576580186 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimationSoundScript::StopAnimation()
extern "C"  void ButtonAnimationSoundScript_StopAnimation_m3210584275 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimationSoundScript::StopSound()
extern "C"  void ButtonAnimationSoundScript_StopSound_m1955524254 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimationSoundScript::PlayAnimation()
extern "C"  void ButtonAnimationSoundScript_PlayAnimation_m3706817121 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimationSoundScript::PlaySound()
extern "C"  void ButtonAnimationSoundScript_PlaySound_m2663501612 (ButtonAnimationSoundScript_t4202863368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
