﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1702563344.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetCameraCullingMask
struct  SetCameraCullingMask_t2588626109  : public ComponentAction_1_t1702563344
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetCameraCullingMask::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.SetCameraCullingMask::cullingMask
	FsmIntU5BU5D_t1976821196* ___cullingMask_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetCameraCullingMask::invertMask
	FsmBool_t1075959796 * ___invertMask_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetCameraCullingMask::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2588626109, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_cullingMask_12() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2588626109, ___cullingMask_12)); }
	inline FsmIntU5BU5D_t1976821196* get_cullingMask_12() const { return ___cullingMask_12; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_cullingMask_12() { return &___cullingMask_12; }
	inline void set_cullingMask_12(FsmIntU5BU5D_t1976821196* value)
	{
		___cullingMask_12 = value;
		Il2CppCodeGenWriteBarrier(&___cullingMask_12, value);
	}

	inline static int32_t get_offset_of_invertMask_13() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2588626109, ___invertMask_13)); }
	inline FsmBool_t1075959796 * get_invertMask_13() const { return ___invertMask_13; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_13() { return &___invertMask_13; }
	inline void set_invertMask_13(FsmBool_t1075959796 * value)
	{
		___invertMask_13 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2588626109, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
