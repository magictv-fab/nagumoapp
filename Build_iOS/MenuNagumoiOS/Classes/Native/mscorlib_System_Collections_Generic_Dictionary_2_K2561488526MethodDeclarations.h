﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>
struct Dictionary_2_t1946552472;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2561488526.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m742386434_gshared (Enumerator_t2561488526 * __this, Dictionary_2_t1946552472 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m742386434(__this, ___host0, method) ((  void (*) (Enumerator_t2561488526 *, Dictionary_2_t1946552472 *, const MethodInfo*))Enumerator__ctor_m742386434_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3023391849_gshared (Enumerator_t2561488526 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3023391849(__this, method) ((  Il2CppObject * (*) (Enumerator_t2561488526 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3023391849_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3311569011_gshared (Enumerator_t2561488526 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3311569011(__this, method) ((  void (*) (Enumerator_t2561488526 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3311569011_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2587645476_gshared (Enumerator_t2561488526 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2587645476(__this, method) ((  void (*) (Enumerator_t2561488526 *, const MethodInfo*))Enumerator_Dispose_m2587645476_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4132170019_gshared (Enumerator_t2561488526 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4132170019(__this, method) ((  bool (*) (Enumerator_t2561488526 *, const MethodInfo*))Enumerator_MoveNext_m4132170019_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3924462357_gshared (Enumerator_t2561488526 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3924462357(__this, method) ((  int32_t (*) (Enumerator_t2561488526 *, const MethodInfo*))Enumerator_get_Current_m3924462357_gshared)(__this, method)
