﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TriggerEvent
struct TriggerEvent_t3929747946;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::.ctor()
extern "C"  void TriggerEvent__ctor_m3849061308 (TriggerEvent_t3929747946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::Reset()
extern "C"  void TriggerEvent_Reset_m1495494249 (TriggerEvent_t3929747946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::Awake()
extern "C"  void TriggerEvent_Awake_m4086666527 (TriggerEvent_t3929747946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::StoreCollisionInfo(UnityEngine.Collider)
extern "C"  void TriggerEvent_StoreCollisionInfo_m940229084 (TriggerEvent_t3929747946 * __this, Collider_t2939674232 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void TriggerEvent_DoTriggerEnter_m3030525160 (TriggerEvent_t3929747946 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::DoTriggerStay(UnityEngine.Collider)
extern "C"  void TriggerEvent_DoTriggerStay_m3889791669 (TriggerEvent_t3929747946 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::DoTriggerExit(UnityEngine.Collider)
extern "C"  void TriggerEvent_DoTriggerExit_m1271702458 (TriggerEvent_t3929747946 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.TriggerEvent::ErrorCheck()
extern "C"  String_t* TriggerEvent_ErrorCheck_m3847931339 (TriggerEvent_t3929747946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
