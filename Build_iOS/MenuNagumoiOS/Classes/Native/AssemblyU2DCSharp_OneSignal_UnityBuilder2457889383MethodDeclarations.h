﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/UnityBuilder
struct UnityBuilder_t2457889383;
// OneSignal/NotificationReceived
struct NotificationReceived_t1402323693;
// OneSignal/NotificationOpened
struct NotificationOpened_t3322442869;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OneSignal_NotificationReceived1402323693.h"
#include "AssemblyU2DCSharp_OneSignal_NotificationOpened3322442869.h"
#include "AssemblyU2DCSharp_OneSignal_OSInFocusDisplayOption2441910921.h"

// System.Void OneSignal/UnityBuilder::.ctor()
extern "C"  void UnityBuilder__ctor_m2397015572 (UnityBuilder_t2457889383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/UnityBuilder OneSignal/UnityBuilder::HandleNotificationReceived(OneSignal/NotificationReceived)
extern "C"  UnityBuilder_t2457889383 * UnityBuilder_HandleNotificationReceived_m3143679757 (UnityBuilder_t2457889383 * __this, NotificationReceived_t1402323693 * ___inNotificationReceivedDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/UnityBuilder OneSignal/UnityBuilder::HandleNotificationOpened(OneSignal/NotificationOpened)
extern "C"  UnityBuilder_t2457889383 * UnityBuilder_HandleNotificationOpened_m3764213069 (UnityBuilder_t2457889383 * __this, NotificationOpened_t3322442869 * ___inNotificationOpenedDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/UnityBuilder OneSignal/UnityBuilder::InFocusDisplaying(OneSignal/OSInFocusDisplayOption)
extern "C"  UnityBuilder_t2457889383 * UnityBuilder_InFocusDisplaying_m615841350 (UnityBuilder_t2457889383 * __this, int32_t ___display0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/UnityBuilder OneSignal/UnityBuilder::Settings(System.Collections.Generic.Dictionary`2<System.String,System.Boolean>)
extern "C"  UnityBuilder_t2457889383 * UnityBuilder_Settings_m1608022229 (UnityBuilder_t2457889383 * __this, Dictionary_2_t1297217088 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/UnityBuilder::EndInit()
extern "C"  void UnityBuilder_EndInit_m2394009789 (UnityBuilder_t2457889383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/UnityBuilder OneSignal/UnityBuilder::SetRequiresUserPrivacyConsent(System.Boolean)
extern "C"  UnityBuilder_t2457889383 * UnityBuilder_SetRequiresUserPrivacyConsent_m913813984 (UnityBuilder_t2457889383 * __this, bool ___required0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
