﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10
struct U3CISendStatusU3Ec__Iterator10_t559145477;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator10__ctor_m1608070198 (U3CISendStatusU3Ec__Iterator10_t559145477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4006963238 (U3CISendStatusU3Ec__Iterator10_t559145477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m641234874 (U3CISendStatusU3Ec__Iterator10_t559145477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator10_MoveNext_m1946286310 (U3CISendStatusU3Ec__Iterator10_t559145477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator10_Dispose_m3360484083 (U3CISendStatusU3Ec__Iterator10_t559145477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<ISendStatus>c__Iterator10::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator10_Reset_m3549470435 (U3CISendStatusU3Ec__Iterator10_t559145477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
