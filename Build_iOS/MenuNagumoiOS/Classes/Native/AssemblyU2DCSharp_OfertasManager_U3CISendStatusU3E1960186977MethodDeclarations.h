﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManager/<ISendStatus>c__Iterator69
struct U3CISendStatusU3Ec__Iterator69_t1960186977;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManager/<ISendStatus>c__Iterator69::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator69__ctor_m2885795370 (U3CISendStatusU3Ec__Iterator69_t1960186977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<ISendStatus>c__Iterator69::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator69_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2755047848 (U3CISendStatusU3Ec__Iterator69_t1960186977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<ISendStatus>c__Iterator69::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator69_System_Collections_IEnumerator_get_Current_m101778748 (U3CISendStatusU3Ec__Iterator69_t1960186977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManager/<ISendStatus>c__Iterator69::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator69_MoveNext_m2008885386 (U3CISendStatusU3Ec__Iterator69_t1960186977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<ISendStatus>c__Iterator69::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator69_Dispose_m2893727719 (U3CISendStatusU3Ec__Iterator69_t1960186977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<ISendStatus>c__Iterator69::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator69_Reset_m532228311 (U3CISendStatusU3Ec__Iterator69_t1960186977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
