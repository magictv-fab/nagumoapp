﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AudioMute
struct  AudioMute_t298551901  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AudioMute::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AudioMute::mute
	FsmBool_t1075959796 * ___mute_10;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AudioMute_t298551901, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_mute_10() { return static_cast<int32_t>(offsetof(AudioMute_t298551901, ___mute_10)); }
	inline FsmBool_t1075959796 * get_mute_10() const { return ___mute_10; }
	inline FsmBool_t1075959796 ** get_address_of_mute_10() { return &___mute_10; }
	inline void set_mute_10(FsmBool_t1075959796 * value)
	{
		___mute_10 = value;
		Il2CppCodeGenWriteBarrier(&___mute_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
