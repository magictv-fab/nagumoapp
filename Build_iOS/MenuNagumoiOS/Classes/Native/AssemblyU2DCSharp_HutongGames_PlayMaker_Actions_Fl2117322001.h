﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatCompare
struct  FloatCompare_t2117322001  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatCompare::float1
	FsmFloat_t2134102846 * ___float1_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatCompare::float2
	FsmFloat_t2134102846 * ___float2_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatCompare::tolerance
	FsmFloat_t2134102846 * ___tolerance_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatCompare::equal
	FsmEvent_t2133468028 * ___equal_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatCompare::lessThan
	FsmEvent_t2133468028 * ___lessThan_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatCompare::greaterThan
	FsmEvent_t2133468028 * ___greaterThan_14;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatCompare::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_float1_9() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___float1_9)); }
	inline FsmFloat_t2134102846 * get_float1_9() const { return ___float1_9; }
	inline FsmFloat_t2134102846 ** get_address_of_float1_9() { return &___float1_9; }
	inline void set_float1_9(FsmFloat_t2134102846 * value)
	{
		___float1_9 = value;
		Il2CppCodeGenWriteBarrier(&___float1_9, value);
	}

	inline static int32_t get_offset_of_float2_10() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___float2_10)); }
	inline FsmFloat_t2134102846 * get_float2_10() const { return ___float2_10; }
	inline FsmFloat_t2134102846 ** get_address_of_float2_10() { return &___float2_10; }
	inline void set_float2_10(FsmFloat_t2134102846 * value)
	{
		___float2_10 = value;
		Il2CppCodeGenWriteBarrier(&___float2_10, value);
	}

	inline static int32_t get_offset_of_tolerance_11() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___tolerance_11)); }
	inline FsmFloat_t2134102846 * get_tolerance_11() const { return ___tolerance_11; }
	inline FsmFloat_t2134102846 ** get_address_of_tolerance_11() { return &___tolerance_11; }
	inline void set_tolerance_11(FsmFloat_t2134102846 * value)
	{
		___tolerance_11 = value;
		Il2CppCodeGenWriteBarrier(&___tolerance_11, value);
	}

	inline static int32_t get_offset_of_equal_12() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___equal_12)); }
	inline FsmEvent_t2133468028 * get_equal_12() const { return ___equal_12; }
	inline FsmEvent_t2133468028 ** get_address_of_equal_12() { return &___equal_12; }
	inline void set_equal_12(FsmEvent_t2133468028 * value)
	{
		___equal_12 = value;
		Il2CppCodeGenWriteBarrier(&___equal_12, value);
	}

	inline static int32_t get_offset_of_lessThan_13() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___lessThan_13)); }
	inline FsmEvent_t2133468028 * get_lessThan_13() const { return ___lessThan_13; }
	inline FsmEvent_t2133468028 ** get_address_of_lessThan_13() { return &___lessThan_13; }
	inline void set_lessThan_13(FsmEvent_t2133468028 * value)
	{
		___lessThan_13 = value;
		Il2CppCodeGenWriteBarrier(&___lessThan_13, value);
	}

	inline static int32_t get_offset_of_greaterThan_14() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___greaterThan_14)); }
	inline FsmEvent_t2133468028 * get_greaterThan_14() const { return ___greaterThan_14; }
	inline FsmEvent_t2133468028 ** get_address_of_greaterThan_14() { return &___greaterThan_14; }
	inline void set_greaterThan_14(FsmEvent_t2133468028 * value)
	{
		___greaterThan_14 = value;
		Il2CppCodeGenWriteBarrier(&___greaterThan_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(FloatCompare_t2117322001, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
