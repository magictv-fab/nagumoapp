﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutFloatField
struct  GUILayoutFloatField_t2175025585  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutFloatField::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutFloatField::style
	FsmString_t952858651 * ___style_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutFloatField::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_13;

public:
	inline static int32_t get_offset_of_floatVariable_11() { return static_cast<int32_t>(offsetof(GUILayoutFloatField_t2175025585, ___floatVariable_11)); }
	inline FsmFloat_t2134102846 * get_floatVariable_11() const { return ___floatVariable_11; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_11() { return &___floatVariable_11; }
	inline void set_floatVariable_11(FsmFloat_t2134102846 * value)
	{
		___floatVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_11, value);
	}

	inline static int32_t get_offset_of_style_12() { return static_cast<int32_t>(offsetof(GUILayoutFloatField_t2175025585, ___style_12)); }
	inline FsmString_t952858651 * get_style_12() const { return ___style_12; }
	inline FsmString_t952858651 ** get_address_of_style_12() { return &___style_12; }
	inline void set_style_12(FsmString_t952858651 * value)
	{
		___style_12 = value;
		Il2CppCodeGenWriteBarrier(&___style_12, value);
	}

	inline static int32_t get_offset_of_changedEvent_13() { return static_cast<int32_t>(offsetof(GUILayoutFloatField_t2175025585, ___changedEvent_13)); }
	inline FsmEvent_t2133468028 * get_changedEvent_13() const { return ___changedEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_13() { return &___changedEvent_13; }
	inline void set_changedEvent_13(FsmEvent_t2133468028 * value)
	{
		___changedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
