﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CutToCamera
struct  CutToCamera_t3450962032  : public FsmStateAction_t2366529033
{
public:
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.CutToCamera::camera
	Camera_t2727095145 * ___camera_9;
	// System.Boolean HutongGames.PlayMaker.Actions.CutToCamera::makeMainCamera
	bool ___makeMainCamera_10;
	// System.Boolean HutongGames.PlayMaker.Actions.CutToCamera::cutBackOnExit
	bool ___cutBackOnExit_11;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.CutToCamera::oldCamera
	Camera_t2727095145 * ___oldCamera_12;

public:
	inline static int32_t get_offset_of_camera_9() { return static_cast<int32_t>(offsetof(CutToCamera_t3450962032, ___camera_9)); }
	inline Camera_t2727095145 * get_camera_9() const { return ___camera_9; }
	inline Camera_t2727095145 ** get_address_of_camera_9() { return &___camera_9; }
	inline void set_camera_9(Camera_t2727095145 * value)
	{
		___camera_9 = value;
		Il2CppCodeGenWriteBarrier(&___camera_9, value);
	}

	inline static int32_t get_offset_of_makeMainCamera_10() { return static_cast<int32_t>(offsetof(CutToCamera_t3450962032, ___makeMainCamera_10)); }
	inline bool get_makeMainCamera_10() const { return ___makeMainCamera_10; }
	inline bool* get_address_of_makeMainCamera_10() { return &___makeMainCamera_10; }
	inline void set_makeMainCamera_10(bool value)
	{
		___makeMainCamera_10 = value;
	}

	inline static int32_t get_offset_of_cutBackOnExit_11() { return static_cast<int32_t>(offsetof(CutToCamera_t3450962032, ___cutBackOnExit_11)); }
	inline bool get_cutBackOnExit_11() const { return ___cutBackOnExit_11; }
	inline bool* get_address_of_cutBackOnExit_11() { return &___cutBackOnExit_11; }
	inline void set_cutBackOnExit_11(bool value)
	{
		___cutBackOnExit_11 = value;
	}

	inline static int32_t get_offset_of_oldCamera_12() { return static_cast<int32_t>(offsetof(CutToCamera_t3450962032, ___oldCamera_12)); }
	inline Camera_t2727095145 * get_oldCamera_12() const { return ___oldCamera_12; }
	inline Camera_t2727095145 ** get_address_of_oldCamera_12() { return &___oldCamera_12; }
	inline void set_oldCamera_12(Camera_t2727095145 * value)
	{
		___oldCamera_12 = value;
		Il2CppCodeGenWriteBarrier(&___oldCamera_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
