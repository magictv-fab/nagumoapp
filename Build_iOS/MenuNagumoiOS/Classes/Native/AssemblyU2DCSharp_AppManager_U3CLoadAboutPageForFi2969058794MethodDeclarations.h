﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppManager/<LoadAboutPageForFirstTime>c__Iterator28
struct U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AppManager/<LoadAboutPageForFirstTime>c__Iterator28::.ctor()
extern "C"  void U3CLoadAboutPageForFirstTimeU3Ec__Iterator28__ctor_m504773825 (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AppManager/<LoadAboutPageForFirstTime>c__Iterator28::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749680049 (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AppManager/<LoadAboutPageForFirstTime>c__Iterator28::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m1201295173 (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AppManager/<LoadAboutPageForFirstTime>c__Iterator28::MoveNext()
extern "C"  bool U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_MoveNext_m448249747 (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager/<LoadAboutPageForFirstTime>c__Iterator28::Dispose()
extern "C"  void U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_Dispose_m3949591742 (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager/<LoadAboutPageForFirstTime>c__Iterator28::Reset()
extern "C"  void U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_Reset_m2446174062 (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
