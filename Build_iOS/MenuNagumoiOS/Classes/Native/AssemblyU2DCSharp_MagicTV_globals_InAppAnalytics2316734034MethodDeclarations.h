﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.globals.InAppAnalytics::.ctor(System.String,System.String)
extern "C"  void InAppAnalytics__ctor_m2010181514 (InAppAnalytics_t2316734034 * __this, String_t* ___appName0, String_t* ___trackingID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::.cctor()
extern "C"  void InAppAnalytics__cctor_m931169913 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.InAppAnalytics MagicTV.globals.InAppAnalytics::GetNewInstance(System.String,System.String)
extern "C"  InAppAnalytics_t2316734034 * InAppAnalytics_GetNewInstance_m633034713 (Il2CppObject * __this /* static, unused */, String_t* ___appName0, String_t* ___trackingID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.InAppAnalytics MagicTV.globals.InAppAnalytics::GetAnalytics(System.String,System.String)
extern "C"  InAppAnalytics_t2316734034 * InAppAnalytics_GetAnalytics_m25985160 (Il2CppObject * __this /* static, unused */, String_t* ___appName0, String_t* ___trackingID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.InAppAnalytics MagicTV.globals.InAppAnalytics::GetAnalytics(System.String)
extern "C"  InAppAnalytics_t2316734034 * InAppAnalytics_GetAnalytics_m2347404620 (Il2CppObject * __this /* static, unused */, String_t* ___appName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.InAppAnalytics::AnalyticsExists(System.String)
extern "C"  bool InAppAnalytics_AnalyticsExists_m349152418 (Il2CppObject * __this /* static, unused */, String_t* ___appName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::init()
extern "C"  void InAppAnalytics_init_m2607685632 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::stop()
extern "C"  void InAppAnalytics_stop_m2899693170 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::CustomEvent(System.String,System.String)
extern "C"  void InAppAnalytics_CustomEvent_m1860409187 (InAppAnalytics_t2316734034 * __this, String_t* ___category0, String_t* ___eventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::Open()
extern "C"  void InAppAnalytics_Open_m1865049818 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::SendEvent(System.String,System.String)
extern "C"  void InAppAnalytics_SendEvent_m2586664314 (InAppAnalytics_t2316734034 * __this, String_t* ___action0, String_t* ___label1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::Close()
extern "C"  void InAppAnalytics_Close_m4111753418 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowInAppGUIonClickCamera()
extern "C"  void InAppAnalytics_WindowInAppGUIonClickCamera_m3078799665 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowInstructionsOnShow()
extern "C"  void InAppAnalytics_WindowInstructionsOnShow_m1292845281 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowHelpOnShow()
extern "C"  void InAppAnalytics_WindowHelpOnShow_m777264509 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowHelpOnClickPrint()
extern "C"  void InAppAnalytics_WindowHelpOnClickPrint_m2573338437 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowHelpOnClickYouTubeVideo()
extern "C"  void InAppAnalytics_WindowHelpOnClickYouTubeVideo_m1634494274 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowAboutOnShow()
extern "C"  void InAppAnalytics_WindowAboutOnShow_m3444554347 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowAboutOnClickEmail()
extern "C"  void InAppAnalytics_WindowAboutOnClickEmail_m4217623266 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowAboutOnClickFacebook()
extern "C"  void InAppAnalytics_WindowAboutOnClickFacebook_m1976617698 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowAboutOnClickSite()
extern "C"  void InAppAnalytics_WindowAboutOnClickSite_m1503557795 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowAboutOnClickTwitter()
extern "C"  void InAppAnalytics_WindowAboutOnClickTwitter_m722311865 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.InAppAnalytics::WindowAboutOnClickYouTubeChannel()
extern "C"  void InAppAnalytics_WindowAboutOnClickYouTubeChannel_m2217143708 (InAppAnalytics_t2316734034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
