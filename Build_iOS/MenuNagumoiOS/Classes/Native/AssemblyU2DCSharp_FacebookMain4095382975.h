﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FacebookData
struct FacebookData_t4095115184;
// FacebookMain
struct FacebookMain_t4095382975;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t3255442115;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookMain
struct  FacebookMain_t4095382975  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D FacebookMain::sharerInitTexture
	Texture2D_t3884108195 * ___sharerInitTexture_4;
	// System.String FacebookMain::linkOferta
	String_t* ___linkOferta_5;
	// System.String FacebookMain::linkImg
	String_t* ___linkImg_6;
	// System.String FacebookMain::title
	String_t* ___title_7;
	// System.String FacebookMain::info
	String_t* ___info_8;

public:
	inline static int32_t get_offset_of_sharerInitTexture_4() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975, ___sharerInitTexture_4)); }
	inline Texture2D_t3884108195 * get_sharerInitTexture_4() const { return ___sharerInitTexture_4; }
	inline Texture2D_t3884108195 ** get_address_of_sharerInitTexture_4() { return &___sharerInitTexture_4; }
	inline void set_sharerInitTexture_4(Texture2D_t3884108195 * value)
	{
		___sharerInitTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___sharerInitTexture_4, value);
	}

	inline static int32_t get_offset_of_linkOferta_5() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975, ___linkOferta_5)); }
	inline String_t* get_linkOferta_5() const { return ___linkOferta_5; }
	inline String_t** get_address_of_linkOferta_5() { return &___linkOferta_5; }
	inline void set_linkOferta_5(String_t* value)
	{
		___linkOferta_5 = value;
		Il2CppCodeGenWriteBarrier(&___linkOferta_5, value);
	}

	inline static int32_t get_offset_of_linkImg_6() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975, ___linkImg_6)); }
	inline String_t* get_linkImg_6() const { return ___linkImg_6; }
	inline String_t** get_address_of_linkImg_6() { return &___linkImg_6; }
	inline void set_linkImg_6(String_t* value)
	{
		___linkImg_6 = value;
		Il2CppCodeGenWriteBarrier(&___linkImg_6, value);
	}

	inline static int32_t get_offset_of_title_7() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975, ___title_7)); }
	inline String_t* get_title_7() const { return ___title_7; }
	inline String_t** get_address_of_title_7() { return &___title_7; }
	inline void set_title_7(String_t* value)
	{
		___title_7 = value;
		Il2CppCodeGenWriteBarrier(&___title_7, value);
	}

	inline static int32_t get_offset_of_info_8() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975, ___info_8)); }
	inline String_t* get_info_8() const { return ___info_8; }
	inline String_t** get_address_of_info_8() { return &___info_8; }
	inline void set_info_8(String_t* value)
	{
		___info_8 = value;
		Il2CppCodeGenWriteBarrier(&___info_8, value);
	}
};

struct FacebookMain_t4095382975_StaticFields
{
public:
	// FacebookData FacebookMain::facebookData
	FacebookData_t4095115184 * ___facebookData_2;
	// FacebookMain FacebookMain::instance
	FacebookMain_t4095382975 * ___instance_3;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult> FacebookMain::<>f__am$cache7
	FacebookDelegate_1_t3255442115 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_facebookData_2() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975_StaticFields, ___facebookData_2)); }
	inline FacebookData_t4095115184 * get_facebookData_2() const { return ___facebookData_2; }
	inline FacebookData_t4095115184 ** get_address_of_facebookData_2() { return &___facebookData_2; }
	inline void set_facebookData_2(FacebookData_t4095115184 * value)
	{
		___facebookData_2 = value;
		Il2CppCodeGenWriteBarrier(&___facebookData_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975_StaticFields, ___instance_3)); }
	inline FacebookMain_t4095382975 * get_instance_3() const { return ___instance_3; }
	inline FacebookMain_t4095382975 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(FacebookMain_t4095382975 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(FacebookMain_t4095382975_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline FacebookDelegate_1_t3255442115 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline FacebookDelegate_1_t3255442115 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(FacebookDelegate_1_t3255442115 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
