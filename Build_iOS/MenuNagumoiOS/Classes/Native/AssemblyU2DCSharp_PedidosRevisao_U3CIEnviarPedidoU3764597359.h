﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PedidoEnvioData
struct PedidoEnvioData_t1309077304;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Exception
struct Exception_t3991598821;
// System.Object
struct Il2CppObject;
// PedidosRevisao
struct PedidosRevisao_t3346969709;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidosRevisao/<IEnviarPedido>c__Iterator72
struct  U3CIEnviarPedidoU3Ec__Iterator72_t764597359  : public Il2CppObject
{
public:
	// PedidoEnvioData PedidosRevisao/<IEnviarPedido>c__Iterator72::<pedidoEnvioData>__0
	PedidoEnvioData_t1309077304 * ___U3CpedidoEnvioDataU3E__0_0;
	// System.Boolean PedidosRevisao/<IEnviarPedido>c__Iterator72::aceitaPassarPeso
	bool ___aceitaPassarPeso_1;
	// System.String PedidosRevisao/<IEnviarPedido>c__Iterator72::<json>__1
	String_t* ___U3CjsonU3E__1_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PedidosRevisao/<IEnviarPedido>c__Iterator72::<headers>__2
	Dictionary_2_t827649927 * ___U3CheadersU3E__2_3;
	// System.Byte[] PedidosRevisao/<IEnviarPedido>c__Iterator72::<pData>__3
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__3_4;
	// UnityEngine.WWW PedidosRevisao/<IEnviarPedido>c__Iterator72::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_5;
	// System.String PedidosRevisao/<IEnviarPedido>c__Iterator72::<message>__5
	String_t* ___U3CmessageU3E__5_6;
	// System.Exception PedidosRevisao/<IEnviarPedido>c__Iterator72::<ex>__6
	Exception_t3991598821 * ___U3CexU3E__6_7;
	// System.Int32 PedidosRevisao/<IEnviarPedido>c__Iterator72::$PC
	int32_t ___U24PC_8;
	// System.Object PedidosRevisao/<IEnviarPedido>c__Iterator72::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean PedidosRevisao/<IEnviarPedido>c__Iterator72::<$>aceitaPassarPeso
	bool ___U3CU24U3EaceitaPassarPeso_10;
	// PedidosRevisao PedidosRevisao/<IEnviarPedido>c__Iterator72::<>f__this
	PedidosRevisao_t3346969709 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CpedidoEnvioDataU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CpedidoEnvioDataU3E__0_0)); }
	inline PedidoEnvioData_t1309077304 * get_U3CpedidoEnvioDataU3E__0_0() const { return ___U3CpedidoEnvioDataU3E__0_0; }
	inline PedidoEnvioData_t1309077304 ** get_address_of_U3CpedidoEnvioDataU3E__0_0() { return &___U3CpedidoEnvioDataU3E__0_0; }
	inline void set_U3CpedidoEnvioDataU3E__0_0(PedidoEnvioData_t1309077304 * value)
	{
		___U3CpedidoEnvioDataU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpedidoEnvioDataU3E__0_0, value);
	}

	inline static int32_t get_offset_of_aceitaPassarPeso_1() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___aceitaPassarPeso_1)); }
	inline bool get_aceitaPassarPeso_1() const { return ___aceitaPassarPeso_1; }
	inline bool* get_address_of_aceitaPassarPeso_1() { return &___aceitaPassarPeso_1; }
	inline void set_aceitaPassarPeso_1(bool value)
	{
		___aceitaPassarPeso_1 = value;
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_2() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CjsonU3E__1_2)); }
	inline String_t* get_U3CjsonU3E__1_2() const { return ___U3CjsonU3E__1_2; }
	inline String_t** get_address_of_U3CjsonU3E__1_2() { return &___U3CjsonU3E__1_2; }
	inline void set_U3CjsonU3E__1_2(String_t* value)
	{
		___U3CjsonU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__2_3() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CheadersU3E__2_3)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__2_3() const { return ___U3CheadersU3E__2_3; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__2_3() { return &___U3CheadersU3E__2_3; }
	inline void set_U3CheadersU3E__2_3(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__3_4() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CpDataU3E__3_4)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__3_4() const { return ___U3CpDataU3E__3_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__3_4() { return &___U3CpDataU3E__3_4; }
	inline void set_U3CpDataU3E__3_4(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_5() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CwwwU3E__4_5)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_5() const { return ___U3CwwwU3E__4_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_5() { return &___U3CwwwU3E__4_5; }
	inline void set_U3CwwwU3E__4_5(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__5_6() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CmessageU3E__5_6)); }
	inline String_t* get_U3CmessageU3E__5_6() const { return ___U3CmessageU3E__5_6; }
	inline String_t** get_address_of_U3CmessageU3E__5_6() { return &___U3CmessageU3E__5_6; }
	inline void set_U3CmessageU3E__5_6(String_t* value)
	{
		___U3CmessageU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__6_7() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CexU3E__6_7)); }
	inline Exception_t3991598821 * get_U3CexU3E__6_7() const { return ___U3CexU3E__6_7; }
	inline Exception_t3991598821 ** get_address_of_U3CexU3E__6_7() { return &___U3CexU3E__6_7; }
	inline void set_U3CexU3E__6_7(Exception_t3991598821 * value)
	{
		___U3CexU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EaceitaPassarPeso_10() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CU24U3EaceitaPassarPeso_10)); }
	inline bool get_U3CU24U3EaceitaPassarPeso_10() const { return ___U3CU24U3EaceitaPassarPeso_10; }
	inline bool* get_address_of_U3CU24U3EaceitaPassarPeso_10() { return &___U3CU24U3EaceitaPassarPeso_10; }
	inline void set_U3CU24U3EaceitaPassarPeso_10(bool value)
	{
		___U3CU24U3EaceitaPassarPeso_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CIEnviarPedidoU3Ec__Iterator72_t764597359, ___U3CU3Ef__this_11)); }
	inline PedidosRevisao_t3346969709 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline PedidosRevisao_t3346969709 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(PedidosRevisao_t3346969709 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
