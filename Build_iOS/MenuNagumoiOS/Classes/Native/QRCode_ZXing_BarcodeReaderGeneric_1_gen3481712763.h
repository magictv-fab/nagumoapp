﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>
struct Func_2_t2392815316;
// System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>
struct Func_4_t3774219921;
// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>
struct Func_5_t533482363;
// ZXing.Common.DecodingOptions
struct DecodingOptions_t3608870897;
// ZXing.Reader
struct Reader_t2610170425;
// System.Action`1<ZXing.ResultPoint>
struct Action_1_t1934408989;
// System.Action`1<ZXing.Result>
struct Action_1_t3006539355;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeReaderGeneric`1<System.Object>
struct  BarcodeReaderGeneric_1_t3481712763  : public Il2CppObject
{
public:
	// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric`1::createBinarizer
	Func_2_t2392815316 * ___createBinarizer_0;
	// System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1::createLuminanceSource
	Func_4_t3774219921 * ___createLuminanceSource_1;
	// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1::createRGBLuminanceSource
	Func_5_t533482363 * ___createRGBLuminanceSource_2;
	// ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric`1::options
	DecodingOptions_t3608870897 * ___options_5;
	// ZXing.Reader ZXing.BarcodeReaderGeneric`1::reader
	Il2CppObject * ___reader_6;
	// System.Boolean ZXing.BarcodeReaderGeneric`1::usePreviousState
	bool ___usePreviousState_7;
	// System.Action`1<ZXing.ResultPoint> ZXing.BarcodeReaderGeneric`1::explicitResultPointFound
	Action_1_t1934408989 * ___explicitResultPointFound_8;
	// System.Action`1<ZXing.Result> ZXing.BarcodeReaderGeneric`1::ResultFound
	Action_1_t3006539355 * ___ResultFound_9;
	// System.Boolean ZXing.BarcodeReaderGeneric`1::<AutoRotate>k__BackingField
	bool ___U3CAutoRotateU3Ek__BackingField_10;
	// System.Boolean ZXing.BarcodeReaderGeneric`1::<TryInverted>k__BackingField
	bool ___U3CTryInvertedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_createBinarizer_0() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___createBinarizer_0)); }
	inline Func_2_t2392815316 * get_createBinarizer_0() const { return ___createBinarizer_0; }
	inline Func_2_t2392815316 ** get_address_of_createBinarizer_0() { return &___createBinarizer_0; }
	inline void set_createBinarizer_0(Func_2_t2392815316 * value)
	{
		___createBinarizer_0 = value;
		Il2CppCodeGenWriteBarrier(&___createBinarizer_0, value);
	}

	inline static int32_t get_offset_of_createLuminanceSource_1() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___createLuminanceSource_1)); }
	inline Func_4_t3774219921 * get_createLuminanceSource_1() const { return ___createLuminanceSource_1; }
	inline Func_4_t3774219921 ** get_address_of_createLuminanceSource_1() { return &___createLuminanceSource_1; }
	inline void set_createLuminanceSource_1(Func_4_t3774219921 * value)
	{
		___createLuminanceSource_1 = value;
		Il2CppCodeGenWriteBarrier(&___createLuminanceSource_1, value);
	}

	inline static int32_t get_offset_of_createRGBLuminanceSource_2() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___createRGBLuminanceSource_2)); }
	inline Func_5_t533482363 * get_createRGBLuminanceSource_2() const { return ___createRGBLuminanceSource_2; }
	inline Func_5_t533482363 ** get_address_of_createRGBLuminanceSource_2() { return &___createRGBLuminanceSource_2; }
	inline void set_createRGBLuminanceSource_2(Func_5_t533482363 * value)
	{
		___createRGBLuminanceSource_2 = value;
		Il2CppCodeGenWriteBarrier(&___createRGBLuminanceSource_2, value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___options_5)); }
	inline DecodingOptions_t3608870897 * get_options_5() const { return ___options_5; }
	inline DecodingOptions_t3608870897 ** get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(DecodingOptions_t3608870897 * value)
	{
		___options_5 = value;
		Il2CppCodeGenWriteBarrier(&___options_5, value);
	}

	inline static int32_t get_offset_of_reader_6() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___reader_6)); }
	inline Il2CppObject * get_reader_6() const { return ___reader_6; }
	inline Il2CppObject ** get_address_of_reader_6() { return &___reader_6; }
	inline void set_reader_6(Il2CppObject * value)
	{
		___reader_6 = value;
		Il2CppCodeGenWriteBarrier(&___reader_6, value);
	}

	inline static int32_t get_offset_of_usePreviousState_7() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___usePreviousState_7)); }
	inline bool get_usePreviousState_7() const { return ___usePreviousState_7; }
	inline bool* get_address_of_usePreviousState_7() { return &___usePreviousState_7; }
	inline void set_usePreviousState_7(bool value)
	{
		___usePreviousState_7 = value;
	}

	inline static int32_t get_offset_of_explicitResultPointFound_8() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___explicitResultPointFound_8)); }
	inline Action_1_t1934408989 * get_explicitResultPointFound_8() const { return ___explicitResultPointFound_8; }
	inline Action_1_t1934408989 ** get_address_of_explicitResultPointFound_8() { return &___explicitResultPointFound_8; }
	inline void set_explicitResultPointFound_8(Action_1_t1934408989 * value)
	{
		___explicitResultPointFound_8 = value;
		Il2CppCodeGenWriteBarrier(&___explicitResultPointFound_8, value);
	}

	inline static int32_t get_offset_of_ResultFound_9() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___ResultFound_9)); }
	inline Action_1_t3006539355 * get_ResultFound_9() const { return ___ResultFound_9; }
	inline Action_1_t3006539355 ** get_address_of_ResultFound_9() { return &___ResultFound_9; }
	inline void set_ResultFound_9(Action_1_t3006539355 * value)
	{
		___ResultFound_9 = value;
		Il2CppCodeGenWriteBarrier(&___ResultFound_9, value);
	}

	inline static int32_t get_offset_of_U3CAutoRotateU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___U3CAutoRotateU3Ek__BackingField_10)); }
	inline bool get_U3CAutoRotateU3Ek__BackingField_10() const { return ___U3CAutoRotateU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CAutoRotateU3Ek__BackingField_10() { return &___U3CAutoRotateU3Ek__BackingField_10; }
	inline void set_U3CAutoRotateU3Ek__BackingField_10(bool value)
	{
		___U3CAutoRotateU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CTryInvertedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763, ___U3CTryInvertedU3Ek__BackingField_11)); }
	inline bool get_U3CTryInvertedU3Ek__BackingField_11() const { return ___U3CTryInvertedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CTryInvertedU3Ek__BackingField_11() { return &___U3CTryInvertedU3Ek__BackingField_11; }
	inline void set_U3CTryInvertedU3Ek__BackingField_11(bool value)
	{
		___U3CTryInvertedU3Ek__BackingField_11 = value;
	}
};

struct BarcodeReaderGeneric_1_t3481712763_StaticFields
{
public:
	// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric`1::defaultCreateBinarizer
	Func_2_t2392815316 * ___defaultCreateBinarizer_3;
	// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1::defaultCreateRGBLuminanceSource
	Func_5_t533482363 * ___defaultCreateRGBLuminanceSource_4;
	// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric`1::CS$<>9__CachedAnonymousMethodDelegate2
	Func_2_t2392815316 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12;
	// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1::CS$<>9__CachedAnonymousMethodDelegate3
	Func_5_t533482363 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13;

public:
	inline static int32_t get_offset_of_defaultCreateBinarizer_3() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763_StaticFields, ___defaultCreateBinarizer_3)); }
	inline Func_2_t2392815316 * get_defaultCreateBinarizer_3() const { return ___defaultCreateBinarizer_3; }
	inline Func_2_t2392815316 ** get_address_of_defaultCreateBinarizer_3() { return &___defaultCreateBinarizer_3; }
	inline void set_defaultCreateBinarizer_3(Func_2_t2392815316 * value)
	{
		___defaultCreateBinarizer_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCreateBinarizer_3, value);
	}

	inline static int32_t get_offset_of_defaultCreateRGBLuminanceSource_4() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763_StaticFields, ___defaultCreateRGBLuminanceSource_4)); }
	inline Func_5_t533482363 * get_defaultCreateRGBLuminanceSource_4() const { return ___defaultCreateRGBLuminanceSource_4; }
	inline Func_5_t533482363 ** get_address_of_defaultCreateRGBLuminanceSource_4() { return &___defaultCreateRGBLuminanceSource_4; }
	inline void set_defaultCreateRGBLuminanceSource_4(Func_5_t533482363 * value)
	{
		___defaultCreateRGBLuminanceSource_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCreateRGBLuminanceSource_4, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12)); }
	inline Func_2_t2392815316 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12; }
	inline Func_2_t2392815316 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12(Func_2_t2392815316 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_1_t3481712763_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13)); }
	inline Func_5_t533482363 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13; }
	inline Func_5_t533482363 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13(Func_5_t533482363 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
