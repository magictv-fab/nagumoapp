﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler
struct OnVoidEventHandler_t2512456021;
// System.String
struct String_t;
// MagicTV.globals.events.ScreenEvents/OnStringEventHandler
struct OnStringEventHandler_t2329146386;
// MagicTV.globals.events.ScreenEvents/OnIntBoolEventHandler
struct OnIntBoolEventHandler_t587466104;
// MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler
struct OnChangeToPerspectiveEventHandler_t1587175024;
// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler
struct OnChangeAnToPerspectiveEventHandler_t2702236419;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>
struct Dictionary_2_t1042304140;
// MagicTV.globals.events.ScreenEvents
struct ScreenEvents_t2706497391;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.ScreenEvents
struct  ScreenEvents_t2706497391  : public MonoBehaviour_t667441552
{
public:
	// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler MagicTV.globals.events.ScreenEvents::onAlertMessageClose
	OnVoidEventHandler_t2512456021 * ___onAlertMessageClose_2;
	// System.String MagicTV.globals.events.ScreenEvents::captionText
	String_t* ___captionText_3;
	// MagicTV.globals.events.ScreenEvents/OnStringEventHandler MagicTV.globals.events.ScreenEvents::OnSetCaption
	OnStringEventHandler_t2329146386 * ___OnSetCaption_4;
	// MagicTV.globals.events.ScreenEvents/OnIntBoolEventHandler MagicTV.globals.events.ScreenEvents::OnConfigCaption
	OnIntBoolEventHandler_t587466104 * ___OnConfigCaption_5;
	// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler MagicTV.globals.events.ScreenEvents::onShowHelpPage
	OnVoidEventHandler_t2512456021 * ___onShowHelpPage_6;
	// MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler MagicTV.globals.events.ScreenEvents::onChangeToPerspective
	OnChangeToPerspectiveEventHandler_t1587175024 * ___onChangeToPerspective_7;
	// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler MagicTV.globals.events.ScreenEvents::onChangeToPerspectiveAskDownload
	OnChangeAnToPerspectiveEventHandler_t2702236419 * ___onChangeToPerspectiveAskDownload_8;
	// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler MagicTV.globals.events.ScreenEvents::onChangeToPerspectiveBlank
	OnChangeAnToPerspectiveEventHandler_t2702236419 * ___onChangeToPerspectiveBlank_9;
	// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler MagicTV.globals.events.ScreenEvents::onChangeToPerspectiveProgressWithoutCancel
	OnChangeAnToPerspectiveEventHandler_t2702236419 * ___onChangeToPerspectiveProgressWithoutCancel_10;
	// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler MagicTV.globals.events.ScreenEvents::onChangeToPerspectiveProgressWithCancel
	OnChangeAnToPerspectiveEventHandler_t2702236419 * ___onChangeToPerspectiveProgressWithCancel_11;
	// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler MagicTV.globals.events.ScreenEvents::onChangeToPerspectiveProgressBarWithouCancel
	OnChangeAnToPerspectiveEventHandler_t2702236419 * ___onChangeToPerspectiveProgressBarWithouCancel_12;
	// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler> MagicTV.globals.events.ScreenEvents::eventsList
	Dictionary_2_t1042304140 * ___eventsList_13;
	// MagicTV.globals.events.ScreenEvents/OnStringEventHandler MagicTV.globals.events.ScreenEvents::onPrintScreenCompleted
	OnStringEventHandler_t2329146386 * ___onPrintScreenCompleted_14;
	// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler MagicTV.globals.events.ScreenEvents::onRaiseShowCloseButton
	OnVoidEventHandler_t2512456021 * ___onRaiseShowCloseButton_15;
	// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler MagicTV.globals.events.ScreenEvents::onRaiseHideCloseButton
	OnVoidEventHandler_t2512456021 * ___onRaiseHideCloseButton_16;
	// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler MagicTV.globals.events.ScreenEvents::onPrintScreenCalled
	OnVoidEventHandler_t2512456021 * ___onPrintScreenCalled_17;
	// MagicTV.globals.events.ScreenEvents/OnStringEventHandler MagicTV.globals.events.ScreenEvents::onSetPINEventHandler
	OnStringEventHandler_t2329146386 * ___onSetPINEventHandler_18;
	// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler MagicTV.globals.events.ScreenEvents::onGoBackEventHandler
	OnVoidEventHandler_t2512456021 * ___onGoBackEventHandler_19;

public:
	inline static int32_t get_offset_of_onAlertMessageClose_2() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onAlertMessageClose_2)); }
	inline OnVoidEventHandler_t2512456021 * get_onAlertMessageClose_2() const { return ___onAlertMessageClose_2; }
	inline OnVoidEventHandler_t2512456021 ** get_address_of_onAlertMessageClose_2() { return &___onAlertMessageClose_2; }
	inline void set_onAlertMessageClose_2(OnVoidEventHandler_t2512456021 * value)
	{
		___onAlertMessageClose_2 = value;
		Il2CppCodeGenWriteBarrier(&___onAlertMessageClose_2, value);
	}

	inline static int32_t get_offset_of_captionText_3() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___captionText_3)); }
	inline String_t* get_captionText_3() const { return ___captionText_3; }
	inline String_t** get_address_of_captionText_3() { return &___captionText_3; }
	inline void set_captionText_3(String_t* value)
	{
		___captionText_3 = value;
		Il2CppCodeGenWriteBarrier(&___captionText_3, value);
	}

	inline static int32_t get_offset_of_OnSetCaption_4() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___OnSetCaption_4)); }
	inline OnStringEventHandler_t2329146386 * get_OnSetCaption_4() const { return ___OnSetCaption_4; }
	inline OnStringEventHandler_t2329146386 ** get_address_of_OnSetCaption_4() { return &___OnSetCaption_4; }
	inline void set_OnSetCaption_4(OnStringEventHandler_t2329146386 * value)
	{
		___OnSetCaption_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnSetCaption_4, value);
	}

	inline static int32_t get_offset_of_OnConfigCaption_5() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___OnConfigCaption_5)); }
	inline OnIntBoolEventHandler_t587466104 * get_OnConfigCaption_5() const { return ___OnConfigCaption_5; }
	inline OnIntBoolEventHandler_t587466104 ** get_address_of_OnConfigCaption_5() { return &___OnConfigCaption_5; }
	inline void set_OnConfigCaption_5(OnIntBoolEventHandler_t587466104 * value)
	{
		___OnConfigCaption_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnConfigCaption_5, value);
	}

	inline static int32_t get_offset_of_onShowHelpPage_6() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onShowHelpPage_6)); }
	inline OnVoidEventHandler_t2512456021 * get_onShowHelpPage_6() const { return ___onShowHelpPage_6; }
	inline OnVoidEventHandler_t2512456021 ** get_address_of_onShowHelpPage_6() { return &___onShowHelpPage_6; }
	inline void set_onShowHelpPage_6(OnVoidEventHandler_t2512456021 * value)
	{
		___onShowHelpPage_6 = value;
		Il2CppCodeGenWriteBarrier(&___onShowHelpPage_6, value);
	}

	inline static int32_t get_offset_of_onChangeToPerspective_7() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onChangeToPerspective_7)); }
	inline OnChangeToPerspectiveEventHandler_t1587175024 * get_onChangeToPerspective_7() const { return ___onChangeToPerspective_7; }
	inline OnChangeToPerspectiveEventHandler_t1587175024 ** get_address_of_onChangeToPerspective_7() { return &___onChangeToPerspective_7; }
	inline void set_onChangeToPerspective_7(OnChangeToPerspectiveEventHandler_t1587175024 * value)
	{
		___onChangeToPerspective_7 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToPerspective_7, value);
	}

	inline static int32_t get_offset_of_onChangeToPerspectiveAskDownload_8() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onChangeToPerspectiveAskDownload_8)); }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 * get_onChangeToPerspectiveAskDownload_8() const { return ___onChangeToPerspectiveAskDownload_8; }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 ** get_address_of_onChangeToPerspectiveAskDownload_8() { return &___onChangeToPerspectiveAskDownload_8; }
	inline void set_onChangeToPerspectiveAskDownload_8(OnChangeAnToPerspectiveEventHandler_t2702236419 * value)
	{
		___onChangeToPerspectiveAskDownload_8 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToPerspectiveAskDownload_8, value);
	}

	inline static int32_t get_offset_of_onChangeToPerspectiveBlank_9() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onChangeToPerspectiveBlank_9)); }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 * get_onChangeToPerspectiveBlank_9() const { return ___onChangeToPerspectiveBlank_9; }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 ** get_address_of_onChangeToPerspectiveBlank_9() { return &___onChangeToPerspectiveBlank_9; }
	inline void set_onChangeToPerspectiveBlank_9(OnChangeAnToPerspectiveEventHandler_t2702236419 * value)
	{
		___onChangeToPerspectiveBlank_9 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToPerspectiveBlank_9, value);
	}

	inline static int32_t get_offset_of_onChangeToPerspectiveProgressWithoutCancel_10() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onChangeToPerspectiveProgressWithoutCancel_10)); }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 * get_onChangeToPerspectiveProgressWithoutCancel_10() const { return ___onChangeToPerspectiveProgressWithoutCancel_10; }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 ** get_address_of_onChangeToPerspectiveProgressWithoutCancel_10() { return &___onChangeToPerspectiveProgressWithoutCancel_10; }
	inline void set_onChangeToPerspectiveProgressWithoutCancel_10(OnChangeAnToPerspectiveEventHandler_t2702236419 * value)
	{
		___onChangeToPerspectiveProgressWithoutCancel_10 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToPerspectiveProgressWithoutCancel_10, value);
	}

	inline static int32_t get_offset_of_onChangeToPerspectiveProgressWithCancel_11() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onChangeToPerspectiveProgressWithCancel_11)); }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 * get_onChangeToPerspectiveProgressWithCancel_11() const { return ___onChangeToPerspectiveProgressWithCancel_11; }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 ** get_address_of_onChangeToPerspectiveProgressWithCancel_11() { return &___onChangeToPerspectiveProgressWithCancel_11; }
	inline void set_onChangeToPerspectiveProgressWithCancel_11(OnChangeAnToPerspectiveEventHandler_t2702236419 * value)
	{
		___onChangeToPerspectiveProgressWithCancel_11 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToPerspectiveProgressWithCancel_11, value);
	}

	inline static int32_t get_offset_of_onChangeToPerspectiveProgressBarWithouCancel_12() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onChangeToPerspectiveProgressBarWithouCancel_12)); }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 * get_onChangeToPerspectiveProgressBarWithouCancel_12() const { return ___onChangeToPerspectiveProgressBarWithouCancel_12; }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 ** get_address_of_onChangeToPerspectiveProgressBarWithouCancel_12() { return &___onChangeToPerspectiveProgressBarWithouCancel_12; }
	inline void set_onChangeToPerspectiveProgressBarWithouCancel_12(OnChangeAnToPerspectiveEventHandler_t2702236419 * value)
	{
		___onChangeToPerspectiveProgressBarWithouCancel_12 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToPerspectiveProgressBarWithouCancel_12, value);
	}

	inline static int32_t get_offset_of_eventsList_13() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___eventsList_13)); }
	inline Dictionary_2_t1042304140 * get_eventsList_13() const { return ___eventsList_13; }
	inline Dictionary_2_t1042304140 ** get_address_of_eventsList_13() { return &___eventsList_13; }
	inline void set_eventsList_13(Dictionary_2_t1042304140 * value)
	{
		___eventsList_13 = value;
		Il2CppCodeGenWriteBarrier(&___eventsList_13, value);
	}

	inline static int32_t get_offset_of_onPrintScreenCompleted_14() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onPrintScreenCompleted_14)); }
	inline OnStringEventHandler_t2329146386 * get_onPrintScreenCompleted_14() const { return ___onPrintScreenCompleted_14; }
	inline OnStringEventHandler_t2329146386 ** get_address_of_onPrintScreenCompleted_14() { return &___onPrintScreenCompleted_14; }
	inline void set_onPrintScreenCompleted_14(OnStringEventHandler_t2329146386 * value)
	{
		___onPrintScreenCompleted_14 = value;
		Il2CppCodeGenWriteBarrier(&___onPrintScreenCompleted_14, value);
	}

	inline static int32_t get_offset_of_onRaiseShowCloseButton_15() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onRaiseShowCloseButton_15)); }
	inline OnVoidEventHandler_t2512456021 * get_onRaiseShowCloseButton_15() const { return ___onRaiseShowCloseButton_15; }
	inline OnVoidEventHandler_t2512456021 ** get_address_of_onRaiseShowCloseButton_15() { return &___onRaiseShowCloseButton_15; }
	inline void set_onRaiseShowCloseButton_15(OnVoidEventHandler_t2512456021 * value)
	{
		___onRaiseShowCloseButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___onRaiseShowCloseButton_15, value);
	}

	inline static int32_t get_offset_of_onRaiseHideCloseButton_16() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onRaiseHideCloseButton_16)); }
	inline OnVoidEventHandler_t2512456021 * get_onRaiseHideCloseButton_16() const { return ___onRaiseHideCloseButton_16; }
	inline OnVoidEventHandler_t2512456021 ** get_address_of_onRaiseHideCloseButton_16() { return &___onRaiseHideCloseButton_16; }
	inline void set_onRaiseHideCloseButton_16(OnVoidEventHandler_t2512456021 * value)
	{
		___onRaiseHideCloseButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___onRaiseHideCloseButton_16, value);
	}

	inline static int32_t get_offset_of_onPrintScreenCalled_17() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onPrintScreenCalled_17)); }
	inline OnVoidEventHandler_t2512456021 * get_onPrintScreenCalled_17() const { return ___onPrintScreenCalled_17; }
	inline OnVoidEventHandler_t2512456021 ** get_address_of_onPrintScreenCalled_17() { return &___onPrintScreenCalled_17; }
	inline void set_onPrintScreenCalled_17(OnVoidEventHandler_t2512456021 * value)
	{
		___onPrintScreenCalled_17 = value;
		Il2CppCodeGenWriteBarrier(&___onPrintScreenCalled_17, value);
	}

	inline static int32_t get_offset_of_onSetPINEventHandler_18() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onSetPINEventHandler_18)); }
	inline OnStringEventHandler_t2329146386 * get_onSetPINEventHandler_18() const { return ___onSetPINEventHandler_18; }
	inline OnStringEventHandler_t2329146386 ** get_address_of_onSetPINEventHandler_18() { return &___onSetPINEventHandler_18; }
	inline void set_onSetPINEventHandler_18(OnStringEventHandler_t2329146386 * value)
	{
		___onSetPINEventHandler_18 = value;
		Il2CppCodeGenWriteBarrier(&___onSetPINEventHandler_18, value);
	}

	inline static int32_t get_offset_of_onGoBackEventHandler_19() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391, ___onGoBackEventHandler_19)); }
	inline OnVoidEventHandler_t2512456021 * get_onGoBackEventHandler_19() const { return ___onGoBackEventHandler_19; }
	inline OnVoidEventHandler_t2512456021 ** get_address_of_onGoBackEventHandler_19() { return &___onGoBackEventHandler_19; }
	inline void set_onGoBackEventHandler_19(OnVoidEventHandler_t2512456021 * value)
	{
		___onGoBackEventHandler_19 = value;
		Il2CppCodeGenWriteBarrier(&___onGoBackEventHandler_19, value);
	}
};

struct ScreenEvents_t2706497391_StaticFields
{
public:
	// MagicTV.globals.events.ScreenEvents MagicTV.globals.events.ScreenEvents::_instance
	ScreenEvents_t2706497391 * ____instance_20;

public:
	inline static int32_t get_offset_of__instance_20() { return static_cast<int32_t>(offsetof(ScreenEvents_t2706497391_StaticFields, ____instance_20)); }
	inline ScreenEvents_t2706497391 * get__instance_20() const { return ____instance_20; }
	inline ScreenEvents_t2706497391 ** get_address_of__instance_20() { return &____instance_20; }
	inline void set__instance_20(ScreenEvents_t2706497391 * value)
	{
		____instance_20 = value;
		Il2CppCodeGenWriteBarrier(&____instance_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
