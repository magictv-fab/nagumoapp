﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Facebook_Unity_Example_ConsoleBa1938891126.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode56248424.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_t1761432360  : public ConsoleBase_t1938891126
{
public:

public:
};

struct MenuBase_t1761432360_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_17;

public:
	inline static int32_t get_offset_of_shareDialogMode_17() { return static_cast<int32_t>(offsetof(MenuBase_t1761432360_StaticFields, ___shareDialogMode_17)); }
	inline int32_t get_shareDialogMode_17() const { return ___shareDialogMode_17; }
	inline int32_t* get_address_of_shareDialogMode_17() { return &___shareDialogMode_17; }
	inline void set_shareDialogMode_17(int32_t value)
	{
		___shareDialogMode_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
