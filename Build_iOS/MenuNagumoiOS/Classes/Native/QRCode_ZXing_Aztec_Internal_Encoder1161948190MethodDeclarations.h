﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.AztecCode
struct AztecCode_t1281541704;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// ZXing.Aztec.Internal.AztecCode ZXing.Aztec.Internal.Encoder::encode(System.Byte[],System.Int32,System.Int32)
extern "C"  AztecCode_t1281541704 * Encoder_encode_m682132791 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, int32_t ___minECCPercent1, int32_t ___userSpecifiedLayers2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Encoder::drawBullsEye(ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern "C"  void Encoder_drawBullsEye_m3437999283 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___matrix0, int32_t ___center1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::generateModeMessage(System.Boolean,System.Int32,System.Int32)
extern "C"  BitArray_t4163851164 * Encoder_generateModeMessage_m925625863 (Il2CppObject * __this /* static, unused */, bool ___compact0, int32_t ___layers1, int32_t ___messageSizeInWords2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Encoder::drawModeMessage(ZXing.Common.BitMatrix,System.Boolean,System.Int32,ZXing.Common.BitArray)
extern "C"  void Encoder_drawModeMessage_m3379096667 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___matrix0, bool ___compact1, int32_t ___matrixSize2, BitArray_t4163851164 * ___modeMessage3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::generateCheckWords(ZXing.Common.BitArray,System.Int32,System.Int32)
extern "C"  BitArray_t4163851164 * Encoder_generateCheckWords_m766568150 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___bitArray0, int32_t ___totalBits1, int32_t ___wordSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Aztec.Internal.Encoder::bitsToWords(ZXing.Common.BitArray,System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t3230847821* Encoder_bitsToWords_m3428276098 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___stuffedBits0, int32_t ___wordSize1, int32_t ___totalWords2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGF ZXing.Aztec.Internal.Encoder::getGF(System.Int32)
extern "C"  GenericGF_t2563420960 * Encoder_getGF_m2345537922 (Il2CppObject * __this /* static, unused */, int32_t ___wordSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::stuffBits(ZXing.Common.BitArray,System.Int32)
extern "C"  BitArray_t4163851164 * Encoder_stuffBits_m1683847653 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___bits0, int32_t ___wordSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Encoder::TotalBitsInLayer(System.Int32,System.Boolean)
extern "C"  int32_t Encoder_TotalBitsInLayer_m3235410409 (Il2CppObject * __this /* static, unused */, int32_t ___layers0, bool ___compact1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Encoder::.cctor()
extern "C"  void Encoder__cctor_m3334620092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
