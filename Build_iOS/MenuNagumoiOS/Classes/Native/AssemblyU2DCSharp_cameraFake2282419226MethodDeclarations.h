﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// cameraFake
struct cameraFake_t2282419226;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void cameraFake::.ctor()
extern "C"  void cameraFake__ctor_m1313382849 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake::Start()
extern "C"  void cameraFake_Start_m260520641 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake::BackButton()
extern "C"  void cameraFake_BackButton_m1074410140 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake::Close()
extern "C"  void cameraFake_Close_m3024242391 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake::OnDisable()
extern "C"  void cameraFake_OnDisable_m3753323944 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake::OnDestroy()
extern "C"  void cameraFake_OnDestroy_m1635888698 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator cameraFake::GoLevel()
extern "C"  Il2CppObject * cameraFake_GoLevel_m3155838611 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake::fakeScreenShot()
extern "C"  void cameraFake_fakeScreenShot_m3573120190 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator cameraFake::delay()
extern "C"  Il2CppObject * cameraFake_delay_m1162272890 (cameraFake_t2282419226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
