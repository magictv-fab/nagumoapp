﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetParent
struct GetParent_t811151086;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetParent::.ctor()
extern "C"  void GetParent__ctor_m2679030280 (GetParent_t811151086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetParent::Reset()
extern "C"  void GetParent_Reset_m325463221 (GetParent_t811151086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetParent::OnEnter()
extern "C"  void GetParent_OnEnter_m1254417375 (GetParent_t811151086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
