﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterExporter>c__AnonStoreyA2`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStoreyA2_1_t4240889260;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t1165300239;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1165300239.h"

// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStoreyA2`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStoreyA2_1__ctor_m1281961571_gshared (U3CRegisterExporterU3Ec__AnonStoreyA2_1_t4240889260 * __this, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStoreyA2_1__ctor_m1281961571(__this, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStoreyA2_1_t4240889260 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStoreyA2_1__ctor_m1281961571_gshared)(__this, method)
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStoreyA2`1<System.Object>::<>m__71(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStoreyA2_1_U3CU3Em__71_m3032010318_gshared (U3CRegisterExporterU3Ec__AnonStoreyA2_1_t4240889260 * __this, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStoreyA2_1_U3CU3Em__71_m3032010318(__this, ___obj0, ___writer1, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStoreyA2_1_t4240889260 *, Il2CppObject *, JsonWriter_t1165300239 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStoreyA2_1_U3CU3Em__71_m3032010318_gshared)(__this, ___obj0, ___writer1, method)
