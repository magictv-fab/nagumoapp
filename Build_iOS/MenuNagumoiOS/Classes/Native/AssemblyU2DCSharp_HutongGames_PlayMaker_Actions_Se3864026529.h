﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGameObject
struct  SetGameObject_t3864026529  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetGameObject::variable
	FsmGameObject_t1697147867 * ___variable_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetGameObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGameObject::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_variable_9() { return static_cast<int32_t>(offsetof(SetGameObject_t3864026529, ___variable_9)); }
	inline FsmGameObject_t1697147867 * get_variable_9() const { return ___variable_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_variable_9() { return &___variable_9; }
	inline void set_variable_9(FsmGameObject_t1697147867 * value)
	{
		___variable_9 = value;
		Il2CppCodeGenWriteBarrier(&___variable_9, value);
	}

	inline static int32_t get_offset_of_gameObject_10() { return static_cast<int32_t>(offsetof(SetGameObject_t3864026529, ___gameObject_10)); }
	inline FsmGameObject_t1697147867 * get_gameObject_10() const { return ___gameObject_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_10() { return &___gameObject_10; }
	inline void set_gameObject_10(FsmGameObject_t1697147867 * value)
	{
		___gameObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetGameObject_t3864026529, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
