﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<MagicTV.in_apps.GarbageItemVO>
struct List_1_t2703387855;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Action`1<MagicTV.in_apps.GarbageItemVO>
struct Action_1_t1731018439;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.GarbageCollector
struct  GarbageCollector_t1305589272  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<MagicTV.in_apps.GarbageItemVO> MagicTV.in_apps.GarbageCollector::_presentations
	List_1_t2703387855 * ____presentations_2;
	// System.Single MagicTV.in_apps.GarbageCollector::tolerance
	float ___tolerance_3;
	// System.Int32 MagicTV.in_apps.GarbageCollector::maxItens
	int32_t ___maxItens_4;
	// System.Single MagicTV.in_apps.GarbageCollector::maxMemoryUsage
	float ___maxMemoryUsage_5;
	// System.Int32 MagicTV.in_apps.GarbageCollector::debugTotalItens
	int32_t ___debugTotalItens_6;
	// System.String MagicTV.in_apps.GarbageCollector::debugFirstItemName
	String_t* ___debugFirstItemName_7;
	// System.Single MagicTV.in_apps.GarbageCollector::debugFirstItemTime
	float ___debugFirstItemTime_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MagicTV.in_apps.GarbageCollector::trashs
	List_1_t747900261 * ___trashs_9;

public:
	inline static int32_t get_offset_of__presentations_2() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ____presentations_2)); }
	inline List_1_t2703387855 * get__presentations_2() const { return ____presentations_2; }
	inline List_1_t2703387855 ** get_address_of__presentations_2() { return &____presentations_2; }
	inline void set__presentations_2(List_1_t2703387855 * value)
	{
		____presentations_2 = value;
		Il2CppCodeGenWriteBarrier(&____presentations_2, value);
	}

	inline static int32_t get_offset_of_tolerance_3() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___tolerance_3)); }
	inline float get_tolerance_3() const { return ___tolerance_3; }
	inline float* get_address_of_tolerance_3() { return &___tolerance_3; }
	inline void set_tolerance_3(float value)
	{
		___tolerance_3 = value;
	}

	inline static int32_t get_offset_of_maxItens_4() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___maxItens_4)); }
	inline int32_t get_maxItens_4() const { return ___maxItens_4; }
	inline int32_t* get_address_of_maxItens_4() { return &___maxItens_4; }
	inline void set_maxItens_4(int32_t value)
	{
		___maxItens_4 = value;
	}

	inline static int32_t get_offset_of_maxMemoryUsage_5() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___maxMemoryUsage_5)); }
	inline float get_maxMemoryUsage_5() const { return ___maxMemoryUsage_5; }
	inline float* get_address_of_maxMemoryUsage_5() { return &___maxMemoryUsage_5; }
	inline void set_maxMemoryUsage_5(float value)
	{
		___maxMemoryUsage_5 = value;
	}

	inline static int32_t get_offset_of_debugTotalItens_6() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___debugTotalItens_6)); }
	inline int32_t get_debugTotalItens_6() const { return ___debugTotalItens_6; }
	inline int32_t* get_address_of_debugTotalItens_6() { return &___debugTotalItens_6; }
	inline void set_debugTotalItens_6(int32_t value)
	{
		___debugTotalItens_6 = value;
	}

	inline static int32_t get_offset_of_debugFirstItemName_7() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___debugFirstItemName_7)); }
	inline String_t* get_debugFirstItemName_7() const { return ___debugFirstItemName_7; }
	inline String_t** get_address_of_debugFirstItemName_7() { return &___debugFirstItemName_7; }
	inline void set_debugFirstItemName_7(String_t* value)
	{
		___debugFirstItemName_7 = value;
		Il2CppCodeGenWriteBarrier(&___debugFirstItemName_7, value);
	}

	inline static int32_t get_offset_of_debugFirstItemTime_8() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___debugFirstItemTime_8)); }
	inline float get_debugFirstItemTime_8() const { return ___debugFirstItemTime_8; }
	inline float* get_address_of_debugFirstItemTime_8() { return &___debugFirstItemTime_8; }
	inline void set_debugFirstItemTime_8(float value)
	{
		___debugFirstItemTime_8 = value;
	}

	inline static int32_t get_offset_of_trashs_9() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272, ___trashs_9)); }
	inline List_1_t747900261 * get_trashs_9() const { return ___trashs_9; }
	inline List_1_t747900261 ** get_address_of_trashs_9() { return &___trashs_9; }
	inline void set_trashs_9(List_1_t747900261 * value)
	{
		___trashs_9 = value;
		Il2CppCodeGenWriteBarrier(&___trashs_9, value);
	}
};

struct GarbageCollector_t1305589272_StaticFields
{
public:
	// System.Action`1<MagicTV.in_apps.GarbageItemVO> MagicTV.in_apps.GarbageCollector::<>f__am$cache8
	Action_1_t1731018439 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(GarbageCollector_t1305589272_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_1_t1731018439 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_1_t1731018439 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_1_t1731018439 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
