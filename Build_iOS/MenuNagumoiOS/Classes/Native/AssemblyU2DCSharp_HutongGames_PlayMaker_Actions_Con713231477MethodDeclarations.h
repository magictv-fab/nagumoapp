﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerMove
struct ControllerMove_t713231477;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerMove::.ctor()
extern "C"  void ControllerMove__ctor_m122564497 (ControllerMove_t713231477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerMove::Reset()
extern "C"  void ControllerMove_Reset_m2063964734 (ControllerMove_t713231477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerMove::OnUpdate()
extern "C"  void ControllerMove_OnUpdate_m2348710939 (ControllerMove_t713231477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
