﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGlobals
struct PlayMakerGlobals_t3097244096;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>
struct List_1_t3501653580;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "mscorlib_System_Object4170816371.h"

// PlayMakerGlobals HutongGames.PlayMaker.FsmEvent::get_GlobalsComponent()
extern "C"  PlayMakerGlobals_t3097244096 * FsmEvent_get_GlobalsComponent_m1988274308 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.FsmEvent::get_globalEvents()
extern "C"  List_1_t1375417109 * FsmEvent_get_globalEvents_m258503559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.FsmEvent::get_EventList()
extern "C"  List_1_t3501653580 * FsmEvent_get_EventList_m3421034987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEvent::get_Name()
extern "C"  String_t* FsmEvent_get_Name_m1510599236 (FsmEvent_t2133468028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_Name(System.String)
extern "C"  void FsmEvent_set_Name_m3884605517 (FsmEvent_t2133468028 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsSystemEvent()
extern "C"  bool FsmEvent_get_IsSystemEvent_m1219736309 (FsmEvent_t2133468028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_IsSystemEvent(System.Boolean)
extern "C"  void FsmEvent_set_IsSystemEvent_m1307820720 (FsmEvent_t2133468028 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsMouseEvent()
extern "C"  bool FsmEvent_get_IsMouseEvent_m2547197069 (FsmEvent_t2133468028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsApplicationEvent()
extern "C"  bool FsmEvent_get_IsApplicationEvent_m3580942402 (FsmEvent_t2133468028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsGlobal()
extern "C"  bool FsmEvent_get_IsGlobal_m2966180667 (FsmEvent_t2133468028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_IsGlobal(System.Boolean)
extern "C"  void FsmEvent_set_IsGlobal_m470108142 (FsmEvent_t2133468028 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::IsNullOrEmpty(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmEvent_IsNullOrEmpty_m3021350928 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEvent::get_Path()
extern "C"  String_t* FsmEvent_get_Path_m1568068958 (FsmEvent_t2133468028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_Path(System.String)
extern "C"  void FsmEvent_set_Path_m1547292531 (FsmEvent_t2133468028 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::.ctor(System.String)
extern "C"  void FsmEvent__ctor_m3412042379 (FsmEvent_t2133468028 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::.ctor(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent__ctor_m1622103121 (FsmEvent_t2133468028 * __this, FsmEvent_t2133468028 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmEvent::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t FsmEvent_System_IComparable_CompareTo_m482553055 (FsmEvent_t2133468028 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::EventListContainsEvent(System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>,System.String)
extern "C"  bool FsmEvent_EventListContainsEvent_m3134334828 (Il2CppObject * __this /* static, unused */, List_1_t3501653580 * ___fsmEventList0, String_t* ___fsmEventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::RemoveEventFromEventList(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_RemoveEventFromEventList_m2202233727 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::FindEvent(System.String)
extern "C"  FsmEvent_t2133468028 * FsmEvent_FindEvent_m3809574375 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::IsEventGlobal(System.String)
extern "C"  bool FsmEvent_IsEventGlobal_m882668658 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::EventListContains(System.String)
extern "C"  bool FsmEvent_EventListContains_m1286850734 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::GetFsmEvent(System.String)
extern "C"  FsmEvent_t2133468028 * FsmEvent_GetFsmEvent_m2820038136 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::GetFsmEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  FsmEvent_t2133468028 * FsmEvent_GetFsmEvent_m151208068 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::AddFsmEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  FsmEvent_t2133468028 * FsmEvent_AddFsmEvent_m3625108175 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::AddSystemEvents()
extern "C"  void FsmEvent_AddSystemEvents_m128959262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::AddSystemEvent(System.String,System.String)
extern "C"  FsmEvent_t2133468028 * FsmEvent_AddSystemEvent_m177526764 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameInvisible()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_BecameInvisible_m133670501 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_BecameInvisible(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_BecameInvisible_m219159174 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameVisible()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_BecameVisible_m3823864746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_BecameVisible(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_BecameVisible_m1489671883 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionEnter()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_CollisionEnter_m1812349015 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionEnter(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionEnter_m127427530 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionExit()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_CollisionExit_m4223802465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionExit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionExit_m2032946498 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionStay()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_CollisionStay_m325715676 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionStay(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionStay_m3312905853 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ControllerColliderHit()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_ControllerColliderHit_m1867878900 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ControllerColliderHit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ControllerColliderHit_m4058327189 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_Finished()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_Finished_m374313731 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_Finished(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_Finished_m141766710 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_LevelLoaded()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_LevelLoaded_m1228564314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_LevelLoaded(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_LevelLoaded_m1809165371 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDown()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MouseDown_m2060976664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseDown(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseDown_m1860871225 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDrag()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MouseDrag_m2063085098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseDrag(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseDrag_m4057195723 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseEnter()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MouseEnter_m321674116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseEnter(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseEnter_m3261716151 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseExit()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MouseExit_m2097506196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseExit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseExit_m471484597 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseOver()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MouseOver_m2381829578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseOver(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseOver_m3904009323 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUp()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MouseUp_m3055160081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseUp(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseUp_m3049922866 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerEnter()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_TriggerEnter_m84292369 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerEnter(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerEnter_m1281113924 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerExit()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_TriggerExit_m704375399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerExit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerExit_m1931614856 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerStay()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_TriggerStay_m1101255906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerStay(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerStay_m3211574211 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationFocus()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_ApplicationFocus_m2966195833 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ApplicationFocus(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ApplicationFocus_m4194857004 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationPause()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_ApplicationPause_m2867040279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ApplicationPause(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ApplicationPause_m1112754506 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationQuit()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_ApplicationQuit_m1247604976 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ApplicationQuit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ApplicationQuit_m1962713041 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_PlayerConnected()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_PlayerConnected_m4086688793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_PlayerConnected(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_PlayerConnected_m3251924282 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ServerInitialized()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_ServerInitialized_m1749326914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ServerInitialized(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ServerInitialized_m2754626019 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ConnectedToServer()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_ConnectedToServer_m1901838072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ConnectedToServer(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ConnectedToServer_m3005104665 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_PlayerDisconnected()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_PlayerDisconnected_m3693946829 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_PlayerDisconnected(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_PlayerDisconnected_m4049744960 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_DisconnectedFromServer()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_DisconnectedFromServer_m2566089017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_DisconnectedFromServer(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_DisconnectedFromServer_m4026551340 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_FailedToConnect()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_FailedToConnect_m1479245475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_FailedToConnect(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_FailedToConnect_m3143266884 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_FailedToConnectToMasterServer()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_FailedToConnectToMasterServer_m3058113955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_FailedToConnectToMasterServer(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_FailedToConnectToMasterServer_m3236856068 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MasterServerEvent()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_MasterServerEvent_m3705414918 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MasterServerEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MasterServerEvent_m3810728871 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_NetworkInstantiate()
extern "C"  FsmEvent_t2133468028 * FsmEvent_get_NetworkInstantiate_m1398445037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_NetworkInstantiate(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_NetworkInstantiate_m761505888 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::AddGlobalEvents()
extern "C"  void FsmEvent_AddGlobalEvents_m1482530962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::SanityCheckEventList()
extern "C"  void FsmEvent_SanityCheckEventList_m744953771 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
