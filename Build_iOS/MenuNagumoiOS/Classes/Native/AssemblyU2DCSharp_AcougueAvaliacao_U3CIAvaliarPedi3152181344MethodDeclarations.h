﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AcougueAvaliacao/<IAvaliarPedido>c__Iterator44
struct U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::.ctor()
extern "C"  void U3CIAvaliarPedidoU3Ec__Iterator44__ctor_m2670616635 (U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIAvaliarPedidoU3Ec__Iterator44_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1606866881 (U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIAvaliarPedidoU3Ec__Iterator44_System_Collections_IEnumerator_get_Current_m855276885 (U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::MoveNext()
extern "C"  bool U3CIAvaliarPedidoU3Ec__Iterator44_MoveNext_m1130466497 (U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::Dispose()
extern "C"  void U3CIAvaliarPedidoU3Ec__Iterator44_Dispose_m2265393592 (U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::Reset()
extern "C"  void U3CIAvaliarPedidoU3Ec__Iterator44_Reset_m317049576 (U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
