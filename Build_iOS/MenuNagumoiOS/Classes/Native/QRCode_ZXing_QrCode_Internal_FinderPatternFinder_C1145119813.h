﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator
struct  CenterComparator_t1145119813  : public Il2CppObject
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::average
	float ___average_0;

public:
	inline static int32_t get_offset_of_average_0() { return static_cast<int32_t>(offsetof(CenterComparator_t1145119813, ___average_0)); }
	inline float get_average_0() const { return ___average_0; }
	inline float* get_address_of_average_0() { return &___average_0; }
	inline void set_average_0(float value)
	{
		___average_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
