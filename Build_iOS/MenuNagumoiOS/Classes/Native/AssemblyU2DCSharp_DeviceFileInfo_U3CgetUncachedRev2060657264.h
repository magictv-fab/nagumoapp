﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceFileInfo/<getUncachedRevision>c__AnonStorey9A
struct  U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264  : public Il2CppObject
{
public:
	// MagicTV.vo.BundleVO DeviceFileInfo/<getUncachedRevision>c__AnonStorey9A::bx
	BundleVO_t1984518073 * ___bx_0;

public:
	inline static int32_t get_offset_of_bx_0() { return static_cast<int32_t>(offsetof(U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264, ___bx_0)); }
	inline BundleVO_t1984518073 * get_bx_0() const { return ___bx_0; }
	inline BundleVO_t1984518073 ** get_address_of_bx_0() { return &___bx_0; }
	inline void set_bx_0(BundleVO_t1984518073 * value)
	{
		___bx_0 = value;
		Il2CppCodeGenWriteBarrier(&___bx_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
