﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertasCRMDataServer
struct OfertasCRMDataServer_t217840361;
// OfertasManagerCRM_PraVoce
struct OfertasManagerCRM_PraVoce_t4201212046;
// CRM_ScrollViewsManager
struct CRM_ScrollViewsManager_t2923429133;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<OfertasValuesCRM>
struct List_1_t4225135114;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t957326451;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// System.String
struct String_t;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM_PraVoce
struct  OfertasManagerCRM_PraVoce_t4201212046  : public MonoBehaviour_t667441552
{
public:
	// CRM_ScrollViewsManager OfertasManagerCRM_PraVoce::_CRM_ScrollViewManager
	CRM_ScrollViewsManager_t2923429133 * ____CRM_ScrollViewManager_5;
	// System.Collections.Generic.List`1<OfertasValuesCRM> OfertasManagerCRM_PraVoce::ofertaValuesLst
	List_1_t4225135114 * ___ofertaValuesLst_15;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::loadingObj
	GameObject_t3674682005 * ___loadingObj_19;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::popup
	GameObject_t3674682005 * ___popup_20;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_21;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::containerItens
	GameObject_t3674682005 * ___containerItens_22;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::favoritouPrimeiraVezObj
	GameObject_t3674682005 * ___favoritouPrimeiraVezObj_24;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::popupSemConexao
	GameObject_t3674682005 * ___popupSemConexao_25;
	// UnityEngine.UI.ScrollRect OfertasManagerCRM_PraVoce::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_26;
	// System.Int32 OfertasManagerCRM_PraVoce::ofertaCounter
	int32_t ___ofertaCounter_27;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::selectedItem
	GameObject_t3674682005 * ___selectedItem_29;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap OfertasManagerCRM_PraVoce::horizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnap_30;
	// UnityEngine.RectTransform OfertasManagerCRM_PraVoce::rectContainer
	RectTransform_t972643934 * ___rectContainer_33;
	// System.Boolean OfertasManagerCRM_PraVoce::onRectUpdate
	bool ___onRectUpdate_34;
	// System.Globalization.CultureInfo OfertasManagerCRM_PraVoce::myCulture
	CultureInfo_t1065375142 * ___myCulture_36;

public:
	inline static int32_t get_offset_of__CRM_ScrollViewManager_5() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ____CRM_ScrollViewManager_5)); }
	inline CRM_ScrollViewsManager_t2923429133 * get__CRM_ScrollViewManager_5() const { return ____CRM_ScrollViewManager_5; }
	inline CRM_ScrollViewsManager_t2923429133 ** get_address_of__CRM_ScrollViewManager_5() { return &____CRM_ScrollViewManager_5; }
	inline void set__CRM_ScrollViewManager_5(CRM_ScrollViewsManager_t2923429133 * value)
	{
		____CRM_ScrollViewManager_5 = value;
		Il2CppCodeGenWriteBarrier(&____CRM_ScrollViewManager_5, value);
	}

	inline static int32_t get_offset_of_ofertaValuesLst_15() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___ofertaValuesLst_15)); }
	inline List_1_t4225135114 * get_ofertaValuesLst_15() const { return ___ofertaValuesLst_15; }
	inline List_1_t4225135114 ** get_address_of_ofertaValuesLst_15() { return &___ofertaValuesLst_15; }
	inline void set_ofertaValuesLst_15(List_1_t4225135114 * value)
	{
		___ofertaValuesLst_15 = value;
		Il2CppCodeGenWriteBarrier(&___ofertaValuesLst_15, value);
	}

	inline static int32_t get_offset_of_loadingObj_19() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___loadingObj_19)); }
	inline GameObject_t3674682005 * get_loadingObj_19() const { return ___loadingObj_19; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_19() { return &___loadingObj_19; }
	inline void set_loadingObj_19(GameObject_t3674682005 * value)
	{
		___loadingObj_19 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_19, value);
	}

	inline static int32_t get_offset_of_popup_20() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___popup_20)); }
	inline GameObject_t3674682005 * get_popup_20() const { return ___popup_20; }
	inline GameObject_t3674682005 ** get_address_of_popup_20() { return &___popup_20; }
	inline void set_popup_20(GameObject_t3674682005 * value)
	{
		___popup_20 = value;
		Il2CppCodeGenWriteBarrier(&___popup_20, value);
	}

	inline static int32_t get_offset_of_itemPrefab_21() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___itemPrefab_21)); }
	inline GameObject_t3674682005 * get_itemPrefab_21() const { return ___itemPrefab_21; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_21() { return &___itemPrefab_21; }
	inline void set_itemPrefab_21(GameObject_t3674682005 * value)
	{
		___itemPrefab_21 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_21, value);
	}

	inline static int32_t get_offset_of_containerItens_22() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___containerItens_22)); }
	inline GameObject_t3674682005 * get_containerItens_22() const { return ___containerItens_22; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_22() { return &___containerItens_22; }
	inline void set_containerItens_22(GameObject_t3674682005 * value)
	{
		___containerItens_22 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_22, value);
	}

	inline static int32_t get_offset_of_favoritouPrimeiraVezObj_24() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___favoritouPrimeiraVezObj_24)); }
	inline GameObject_t3674682005 * get_favoritouPrimeiraVezObj_24() const { return ___favoritouPrimeiraVezObj_24; }
	inline GameObject_t3674682005 ** get_address_of_favoritouPrimeiraVezObj_24() { return &___favoritouPrimeiraVezObj_24; }
	inline void set_favoritouPrimeiraVezObj_24(GameObject_t3674682005 * value)
	{
		___favoritouPrimeiraVezObj_24 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouPrimeiraVezObj_24, value);
	}

	inline static int32_t get_offset_of_popupSemConexao_25() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___popupSemConexao_25)); }
	inline GameObject_t3674682005 * get_popupSemConexao_25() const { return ___popupSemConexao_25; }
	inline GameObject_t3674682005 ** get_address_of_popupSemConexao_25() { return &___popupSemConexao_25; }
	inline void set_popupSemConexao_25(GameObject_t3674682005 * value)
	{
		___popupSemConexao_25 = value;
		Il2CppCodeGenWriteBarrier(&___popupSemConexao_25, value);
	}

	inline static int32_t get_offset_of_scrollRect_26() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___scrollRect_26)); }
	inline ScrollRect_t3606982749 * get_scrollRect_26() const { return ___scrollRect_26; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_26() { return &___scrollRect_26; }
	inline void set_scrollRect_26(ScrollRect_t3606982749 * value)
	{
		___scrollRect_26 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_26, value);
	}

	inline static int32_t get_offset_of_ofertaCounter_27() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___ofertaCounter_27)); }
	inline int32_t get_ofertaCounter_27() const { return ___ofertaCounter_27; }
	inline int32_t* get_address_of_ofertaCounter_27() { return &___ofertaCounter_27; }
	inline void set_ofertaCounter_27(int32_t value)
	{
		___ofertaCounter_27 = value;
	}

	inline static int32_t get_offset_of_selectedItem_29() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___selectedItem_29)); }
	inline GameObject_t3674682005 * get_selectedItem_29() const { return ___selectedItem_29; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_29() { return &___selectedItem_29; }
	inline void set_selectedItem_29(GameObject_t3674682005 * value)
	{
		___selectedItem_29 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_29, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnap_30() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___horizontalScrollSnap_30)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnap_30() const { return ___horizontalScrollSnap_30; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnap_30() { return &___horizontalScrollSnap_30; }
	inline void set_horizontalScrollSnap_30(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnap_30 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnap_30, value);
	}

	inline static int32_t get_offset_of_rectContainer_33() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___rectContainer_33)); }
	inline RectTransform_t972643934 * get_rectContainer_33() const { return ___rectContainer_33; }
	inline RectTransform_t972643934 ** get_address_of_rectContainer_33() { return &___rectContainer_33; }
	inline void set_rectContainer_33(RectTransform_t972643934 * value)
	{
		___rectContainer_33 = value;
		Il2CppCodeGenWriteBarrier(&___rectContainer_33, value);
	}

	inline static int32_t get_offset_of_onRectUpdate_34() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___onRectUpdate_34)); }
	inline bool get_onRectUpdate_34() const { return ___onRectUpdate_34; }
	inline bool* get_address_of_onRectUpdate_34() { return &___onRectUpdate_34; }
	inline void set_onRectUpdate_34(bool value)
	{
		___onRectUpdate_34 = value;
	}

	inline static int32_t get_offset_of_myCulture_36() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046, ___myCulture_36)); }
	inline CultureInfo_t1065375142 * get_myCulture_36() const { return ___myCulture_36; }
	inline CultureInfo_t1065375142 ** get_address_of_myCulture_36() { return &___myCulture_36; }
	inline void set_myCulture_36(CultureInfo_t1065375142 * value)
	{
		___myCulture_36 = value;
		Il2CppCodeGenWriteBarrier(&___myCulture_36, value);
	}
};

struct OfertasManagerCRM_PraVoce_t4201212046_StaticFields
{
public:
	// OfertasCRMDataServer OfertasManagerCRM_PraVoce::ofertasDataServer
	OfertasCRMDataServer_t217840361 * ___ofertasDataServer_3;
	// OfertasManagerCRM_PraVoce OfertasManagerCRM_PraVoce::instance
	OfertasManagerCRM_PraVoce_t4201212046 * ___instance_4;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::titleLst
	List_1_t1375417109 * ___titleLst_6;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::valueLst
	List_1_t1375417109 * ___valueLst_7;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::infoLst
	List_1_t1375417109 * ___infoLst_8;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::idLst
	List_1_t1375417109 * ___idLst_9;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::idcrmLst
	List_1_t1375417109 * ___idcrmLst_10;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::validadeLst
	List_1_t1375417109 * ___validadeLst_11;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::linkLst
	List_1_t1375417109 * ___linkLst_12;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::valueDEPOR_DELst
	List_1_t1375417109 * ___valueDEPOR_DELst_13;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM_PraVoce::valueDEPORDE_PORLst
	List_1_t1375417109 * ___valueDEPORDE_PORLst_14;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> OfertasManagerCRM_PraVoce::textureLst
	List_1_t957326451 * ___textureLst_16;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> OfertasManagerCRM_PraVoce::imgLst
	List_1_t272385497 * ___imgLst_17;
	// System.Collections.Generic.List`1<System.Boolean> OfertasManagerCRM_PraVoce::favoritouLst
	List_1_t1844984270 * ___favoritouLst_18;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> OfertasManagerCRM_PraVoce::itensList
	List_1_t747900261 * ___itensList_23;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce::currentItem
	GameObject_t3674682005 * ___currentItem_28;
	// System.String OfertasManagerCRM_PraVoce::currentJsonTxt
	String_t* ___currentJsonTxt_31;
	// System.Int32 OfertasManagerCRM_PraVoce::loadedItensCount
	int32_t ___loadedItensCount_32;
	// System.Int32 OfertasManagerCRM_PraVoce::loadedImgsCount
	int32_t ___loadedImgsCount_35;

public:
	inline static int32_t get_offset_of_ofertasDataServer_3() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___ofertasDataServer_3)); }
	inline OfertasCRMDataServer_t217840361 * get_ofertasDataServer_3() const { return ___ofertasDataServer_3; }
	inline OfertasCRMDataServer_t217840361 ** get_address_of_ofertasDataServer_3() { return &___ofertasDataServer_3; }
	inline void set_ofertasDataServer_3(OfertasCRMDataServer_t217840361 * value)
	{
		___ofertasDataServer_3 = value;
		Il2CppCodeGenWriteBarrier(&___ofertasDataServer_3, value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___instance_4)); }
	inline OfertasManagerCRM_PraVoce_t4201212046 * get_instance_4() const { return ___instance_4; }
	inline OfertasManagerCRM_PraVoce_t4201212046 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(OfertasManagerCRM_PraVoce_t4201212046 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}

	inline static int32_t get_offset_of_titleLst_6() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___titleLst_6)); }
	inline List_1_t1375417109 * get_titleLst_6() const { return ___titleLst_6; }
	inline List_1_t1375417109 ** get_address_of_titleLst_6() { return &___titleLst_6; }
	inline void set_titleLst_6(List_1_t1375417109 * value)
	{
		___titleLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_6, value);
	}

	inline static int32_t get_offset_of_valueLst_7() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___valueLst_7)); }
	inline List_1_t1375417109 * get_valueLst_7() const { return ___valueLst_7; }
	inline List_1_t1375417109 ** get_address_of_valueLst_7() { return &___valueLst_7; }
	inline void set_valueLst_7(List_1_t1375417109 * value)
	{
		___valueLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_7, value);
	}

	inline static int32_t get_offset_of_infoLst_8() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___infoLst_8)); }
	inline List_1_t1375417109 * get_infoLst_8() const { return ___infoLst_8; }
	inline List_1_t1375417109 ** get_address_of_infoLst_8() { return &___infoLst_8; }
	inline void set_infoLst_8(List_1_t1375417109 * value)
	{
		___infoLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_8, value);
	}

	inline static int32_t get_offset_of_idLst_9() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___idLst_9)); }
	inline List_1_t1375417109 * get_idLst_9() const { return ___idLst_9; }
	inline List_1_t1375417109 ** get_address_of_idLst_9() { return &___idLst_9; }
	inline void set_idLst_9(List_1_t1375417109 * value)
	{
		___idLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_9, value);
	}

	inline static int32_t get_offset_of_idcrmLst_10() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___idcrmLst_10)); }
	inline List_1_t1375417109 * get_idcrmLst_10() const { return ___idcrmLst_10; }
	inline List_1_t1375417109 ** get_address_of_idcrmLst_10() { return &___idcrmLst_10; }
	inline void set_idcrmLst_10(List_1_t1375417109 * value)
	{
		___idcrmLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___idcrmLst_10, value);
	}

	inline static int32_t get_offset_of_validadeLst_11() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___validadeLst_11)); }
	inline List_1_t1375417109 * get_validadeLst_11() const { return ___validadeLst_11; }
	inline List_1_t1375417109 ** get_address_of_validadeLst_11() { return &___validadeLst_11; }
	inline void set_validadeLst_11(List_1_t1375417109 * value)
	{
		___validadeLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___validadeLst_11, value);
	}

	inline static int32_t get_offset_of_linkLst_12() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___linkLst_12)); }
	inline List_1_t1375417109 * get_linkLst_12() const { return ___linkLst_12; }
	inline List_1_t1375417109 ** get_address_of_linkLst_12() { return &___linkLst_12; }
	inline void set_linkLst_12(List_1_t1375417109 * value)
	{
		___linkLst_12 = value;
		Il2CppCodeGenWriteBarrier(&___linkLst_12, value);
	}

	inline static int32_t get_offset_of_valueDEPOR_DELst_13() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___valueDEPOR_DELst_13)); }
	inline List_1_t1375417109 * get_valueDEPOR_DELst_13() const { return ___valueDEPOR_DELst_13; }
	inline List_1_t1375417109 ** get_address_of_valueDEPOR_DELst_13() { return &___valueDEPOR_DELst_13; }
	inline void set_valueDEPOR_DELst_13(List_1_t1375417109 * value)
	{
		___valueDEPOR_DELst_13 = value;
		Il2CppCodeGenWriteBarrier(&___valueDEPOR_DELst_13, value);
	}

	inline static int32_t get_offset_of_valueDEPORDE_PORLst_14() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___valueDEPORDE_PORLst_14)); }
	inline List_1_t1375417109 * get_valueDEPORDE_PORLst_14() const { return ___valueDEPORDE_PORLst_14; }
	inline List_1_t1375417109 ** get_address_of_valueDEPORDE_PORLst_14() { return &___valueDEPORDE_PORLst_14; }
	inline void set_valueDEPORDE_PORLst_14(List_1_t1375417109 * value)
	{
		___valueDEPORDE_PORLst_14 = value;
		Il2CppCodeGenWriteBarrier(&___valueDEPORDE_PORLst_14, value);
	}

	inline static int32_t get_offset_of_textureLst_16() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___textureLst_16)); }
	inline List_1_t957326451 * get_textureLst_16() const { return ___textureLst_16; }
	inline List_1_t957326451 ** get_address_of_textureLst_16() { return &___textureLst_16; }
	inline void set_textureLst_16(List_1_t957326451 * value)
	{
		___textureLst_16 = value;
		Il2CppCodeGenWriteBarrier(&___textureLst_16, value);
	}

	inline static int32_t get_offset_of_imgLst_17() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___imgLst_17)); }
	inline List_1_t272385497 * get_imgLst_17() const { return ___imgLst_17; }
	inline List_1_t272385497 ** get_address_of_imgLst_17() { return &___imgLst_17; }
	inline void set_imgLst_17(List_1_t272385497 * value)
	{
		___imgLst_17 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_17, value);
	}

	inline static int32_t get_offset_of_favoritouLst_18() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___favoritouLst_18)); }
	inline List_1_t1844984270 * get_favoritouLst_18() const { return ___favoritouLst_18; }
	inline List_1_t1844984270 ** get_address_of_favoritouLst_18() { return &___favoritouLst_18; }
	inline void set_favoritouLst_18(List_1_t1844984270 * value)
	{
		___favoritouLst_18 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouLst_18, value);
	}

	inline static int32_t get_offset_of_itensList_23() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___itensList_23)); }
	inline List_1_t747900261 * get_itensList_23() const { return ___itensList_23; }
	inline List_1_t747900261 ** get_address_of_itensList_23() { return &___itensList_23; }
	inline void set_itensList_23(List_1_t747900261 * value)
	{
		___itensList_23 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_23, value);
	}

	inline static int32_t get_offset_of_currentItem_28() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___currentItem_28)); }
	inline GameObject_t3674682005 * get_currentItem_28() const { return ___currentItem_28; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_28() { return &___currentItem_28; }
	inline void set_currentItem_28(GameObject_t3674682005 * value)
	{
		___currentItem_28 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_28, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_31() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___currentJsonTxt_31)); }
	inline String_t* get_currentJsonTxt_31() const { return ___currentJsonTxt_31; }
	inline String_t** get_address_of_currentJsonTxt_31() { return &___currentJsonTxt_31; }
	inline void set_currentJsonTxt_31(String_t* value)
	{
		___currentJsonTxt_31 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_31, value);
	}

	inline static int32_t get_offset_of_loadedItensCount_32() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___loadedItensCount_32)); }
	inline int32_t get_loadedItensCount_32() const { return ___loadedItensCount_32; }
	inline int32_t* get_address_of_loadedItensCount_32() { return &___loadedItensCount_32; }
	inline void set_loadedItensCount_32(int32_t value)
	{
		___loadedItensCount_32 = value;
	}

	inline static int32_t get_offset_of_loadedImgsCount_35() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_PraVoce_t4201212046_StaticFields, ___loadedImgsCount_35)); }
	inline int32_t get_loadedImgsCount_35() const { return ___loadedImgsCount_35; }
	inline int32_t* get_address_of_loadedImgsCount_35() { return &___loadedImgsCount_35; }
	inline void set_loadedImgsCount_35(int32_t value)
	{
		___loadedImgsCount_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
