﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSystemDateTime
struct GetSystemDateTime_t1295868974;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::.ctor()
extern "C"  void GetSystemDateTime__ctor_m3383823560 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::Reset()
extern "C"  void GetSystemDateTime_Reset_m1030256501 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::OnEnter()
extern "C"  void GetSystemDateTime_OnEnter_m4250893983 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::OnUpdate()
extern "C"  void GetSystemDateTime_OnUpdate_m2062253956 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
