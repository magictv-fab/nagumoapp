﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.DetectionResult
struct DetectionResult_t1261579248;
// ZXing.PDF417.Internal.BarcodeMetadata
struct BarcodeMetadata_t443526333;
// ZXing.PDF417.Internal.DetectionResultColumn[]
struct DetectionResultColumnU5BU5D_t3380244739;
// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;
// ZXing.PDF417.Internal.DetectionResultColumn
struct DetectionResultColumn_t3195057574;
// ZXing.PDF417.Internal.Codeword
struct Codeword_t1124156527;
// ZXing.PDF417.Internal.Codeword[]
struct CodewordU5BU5D_t399100150;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_BarcodeMetadata443526333.h"
#include "QRCode_ZXing_PDF417_Internal_BoundingBox242606069.h"
#include "QRCode_ZXing_PDF417_Internal_DetectionResultColumn3195057574.h"
#include "QRCode_ZXing_PDF417_Internal_Codeword1124156527.h"

// ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResult::get_Metadata()
extern "C"  BarcodeMetadata_t443526333 * DetectionResult_get_Metadata_m212345797 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::set_Metadata(ZXing.PDF417.Internal.BarcodeMetadata)
extern "C"  void DetectionResult_set_Metadata_m3989463174 (DetectionResult_t1261579248 * __this, BarcodeMetadata_t443526333 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::get_DetectionResultColumns()
extern "C"  DetectionResultColumnU5BU5D_t3380244739* DetectionResult_get_DetectionResultColumns_m3861228636 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::set_DetectionResultColumns(ZXing.PDF417.Internal.DetectionResultColumn[])
extern "C"  void DetectionResult_set_DetectionResultColumns_m679106063 (DetectionResult_t1261579248 * __this, DetectionResultColumnU5BU5D_t3380244739* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResult::get_Box()
extern "C"  BoundingBox_t242606069 * DetectionResult_get_Box_m256557279 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::set_Box(ZXing.PDF417.Internal.BoundingBox)
extern "C"  void DetectionResult_set_Box_m3987650550 (DetectionResult_t1261579248 * __this, BoundingBox_t242606069 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::get_ColumnCount()
extern "C"  int32_t DetectionResult_get_ColumnCount_m525347930 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::set_ColumnCount(System.Int32)
extern "C"  void DetectionResult_set_ColumnCount_m984688745 (DetectionResult_t1261579248 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::get_RowCount()
extern "C"  int32_t DetectionResult_get_RowCount_m3257042134 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::get_ErrorCorrectionLevel()
extern "C"  int32_t DetectionResult_get_ErrorCorrectionLevel_m2526757599 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::.ctor(ZXing.PDF417.Internal.BarcodeMetadata,ZXing.PDF417.Internal.BoundingBox)
extern "C"  void DetectionResult__ctor_m1072963572 (DetectionResult_t1261579248 * __this, BarcodeMetadata_t443526333 * ___metadata0, BoundingBox_t242606069 * ___box1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::getDetectionResultColumns()
extern "C"  DetectionResultColumnU5BU5D_t3380244739* DetectionResult_getDetectionResultColumns_m3817771163 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::adjustIndicatorColumnRowNumbers(ZXing.PDF417.Internal.DetectionResultColumn)
extern "C"  void DetectionResult_adjustIndicatorColumnRowNumbers_m1869591437 (DetectionResult_t1261579248 * __this, DetectionResultColumn_t3195057574 * ___detectionResultColumn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbers()
extern "C"  int32_t DetectionResult_adjustRowNumbers_m1176157911 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersByRow()
extern "C"  int32_t DetectionResult_adjustRowNumbersByRow_m241919502 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromBothRI()
extern "C"  void DetectionResult_adjustRowNumbersFromBothRI_m2621920967 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromRRI()
extern "C"  int32_t DetectionResult_adjustRowNumbersFromRRI_m2479411370 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromLRI()
extern "C"  int32_t DetectionResult_adjustRowNumbersFromLRI_m2473870244 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumberIfValid(System.Int32,System.Int32,ZXing.PDF417.Internal.Codeword)
extern "C"  int32_t DetectionResult_adjustRowNumberIfValid_m1919924759 (Il2CppObject * __this /* static, unused */, int32_t ___rowIndicatorRowNumber0, int32_t ___invalidRowCounts1, Codeword_t1124156527 * ___codeword2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResult::adjustRowNumbers(System.Int32,System.Int32,ZXing.PDF417.Internal.Codeword[])
extern "C"  void DetectionResult_adjustRowNumbers_m181819767 (DetectionResult_t1261579248 * __this, int32_t ___barcodeColumn0, int32_t ___codewordsRow1, CodewordU5BU5D_t399100150* ___codewords2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.DetectionResult::adjustRowNumber(ZXing.PDF417.Internal.Codeword,ZXing.PDF417.Internal.Codeword)
extern "C"  bool DetectionResult_adjustRowNumber_m3097926882 (Il2CppObject * __this /* static, unused */, Codeword_t1124156527 * ___codeword0, Codeword_t1124156527 * ___otherCodeword1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.DetectionResult::ToString()
extern "C"  String_t* DetectionResult_ToString_m3355341551 (DetectionResult_t1261579248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
