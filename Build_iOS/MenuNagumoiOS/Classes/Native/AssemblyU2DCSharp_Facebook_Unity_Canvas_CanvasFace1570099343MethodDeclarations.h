﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFace2809338416MethodDeclarations.h"

// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IAppRequestResult>::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
#define CanvasUIMethodCall_1__ctor_m2394768521(__this, ___canvasImpl0, ___methodName1, ___callbackMethod2, method) ((  void (*) (CanvasUIMethodCall_1_t1570099343 *, CanvasFacebook_t3651918198 *, String_t*, String_t*, const MethodInfo*))CanvasUIMethodCall_1__ctor_m2024179164_gshared)(__this, ___canvasImpl0, ___methodName1, ___callbackMethod2, method)
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IAppRequestResult>::Call(Facebook.Unity.MethodArguments)
#define CanvasUIMethodCall_1_Call_m1452275018(__this, ___args0, method) ((  void (*) (CanvasUIMethodCall_1_t1570099343 *, MethodArguments_t3236074899 *, const MethodInfo*))CanvasUIMethodCall_1_Call_m4214808791_gshared)(__this, ___args0, method)
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<Facebook.Unity.IAppRequestResult>::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
#define CanvasUIMethodCall_1_UI_m2968241945(__this, ___method0, ___args1, ___callback2, method) ((  void (*) (CanvasUIMethodCall_1_t1570099343 *, String_t*, MethodArguments_t3236074899 *, FacebookDelegate_1_t1018651059 *, const MethodInfo*))CanvasUIMethodCall_1_UI_m1731377964_gshared)(__this, ___method0, ___args1, ___callback2, method)
