﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeliControl
struct HeliControl_t91627683;

#include "codegen/il2cpp-codegen.h"

// System.Void HeliControl::.ctor()
extern "C"  void HeliControl__ctor_m3411829288 (HeliControl_t91627683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeliControl::Start()
extern "C"  void HeliControl_Start_m2358967080 (HeliControl_t91627683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeliControl::Update()
extern "C"  void HeliControl_Update_m119387621 (HeliControl_t91627683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeliControl::OnMouseDown()
extern "C"  void HeliControl_OnMouseDown_m2915804494 (HeliControl_t91627683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeliControl::OnMouseUp()
extern "C"  void HeliControl_OnMouseUp_m1625883591 (HeliControl_t91627683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
