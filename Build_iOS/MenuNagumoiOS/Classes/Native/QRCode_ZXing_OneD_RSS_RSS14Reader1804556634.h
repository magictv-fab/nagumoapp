﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Pair>
struct List_1_t2413595184;

#include "QRCode_ZXing_OneD_RSS_AbstractRSSReader1625773429.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.RSS14Reader
struct  RSS14Reader_t1804556634  : public AbstractRSSReader_t1625773429
{
public:
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Pair> ZXing.OneD.RSS.RSS14Reader::possibleLeftPairs
	List_1_t2413595184 * ___possibleLeftPairs_17;
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Pair> ZXing.OneD.RSS.RSS14Reader::possibleRightPairs
	List_1_t2413595184 * ___possibleRightPairs_18;

public:
	inline static int32_t get_offset_of_possibleLeftPairs_17() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634, ___possibleLeftPairs_17)); }
	inline List_1_t2413595184 * get_possibleLeftPairs_17() const { return ___possibleLeftPairs_17; }
	inline List_1_t2413595184 ** get_address_of_possibleLeftPairs_17() { return &___possibleLeftPairs_17; }
	inline void set_possibleLeftPairs_17(List_1_t2413595184 * value)
	{
		___possibleLeftPairs_17 = value;
		Il2CppCodeGenWriteBarrier(&___possibleLeftPairs_17, value);
	}

	inline static int32_t get_offset_of_possibleRightPairs_18() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634, ___possibleRightPairs_18)); }
	inline List_1_t2413595184 * get_possibleRightPairs_18() const { return ___possibleRightPairs_18; }
	inline List_1_t2413595184 ** get_address_of_possibleRightPairs_18() { return &___possibleRightPairs_18; }
	inline void set_possibleRightPairs_18(List_1_t2413595184 * value)
	{
		___possibleRightPairs_18 = value;
		Il2CppCodeGenWriteBarrier(&___possibleRightPairs_18, value);
	}
};

struct RSS14Reader_t1804556634_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::OUTSIDE_EVEN_TOTAL_SUBSET
	Int32U5BU5D_t3230847821* ___OUTSIDE_EVEN_TOTAL_SUBSET_10;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::INSIDE_ODD_TOTAL_SUBSET
	Int32U5BU5D_t3230847821* ___INSIDE_ODD_TOTAL_SUBSET_11;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::OUTSIDE_GSUM
	Int32U5BU5D_t3230847821* ___OUTSIDE_GSUM_12;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::INSIDE_GSUM
	Int32U5BU5D_t3230847821* ___INSIDE_GSUM_13;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::OUTSIDE_ODD_WIDEST
	Int32U5BU5D_t3230847821* ___OUTSIDE_ODD_WIDEST_14;
	// System.Int32[] ZXing.OneD.RSS.RSS14Reader::INSIDE_ODD_WIDEST
	Int32U5BU5D_t3230847821* ___INSIDE_ODD_WIDEST_15;
	// System.Int32[][] ZXing.OneD.RSS.RSS14Reader::FINDER_PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___FINDER_PATTERNS_16;

public:
	inline static int32_t get_offset_of_OUTSIDE_EVEN_TOTAL_SUBSET_10() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___OUTSIDE_EVEN_TOTAL_SUBSET_10)); }
	inline Int32U5BU5D_t3230847821* get_OUTSIDE_EVEN_TOTAL_SUBSET_10() const { return ___OUTSIDE_EVEN_TOTAL_SUBSET_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_OUTSIDE_EVEN_TOTAL_SUBSET_10() { return &___OUTSIDE_EVEN_TOTAL_SUBSET_10; }
	inline void set_OUTSIDE_EVEN_TOTAL_SUBSET_10(Int32U5BU5D_t3230847821* value)
	{
		___OUTSIDE_EVEN_TOTAL_SUBSET_10 = value;
		Il2CppCodeGenWriteBarrier(&___OUTSIDE_EVEN_TOTAL_SUBSET_10, value);
	}

	inline static int32_t get_offset_of_INSIDE_ODD_TOTAL_SUBSET_11() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___INSIDE_ODD_TOTAL_SUBSET_11)); }
	inline Int32U5BU5D_t3230847821* get_INSIDE_ODD_TOTAL_SUBSET_11() const { return ___INSIDE_ODD_TOTAL_SUBSET_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_INSIDE_ODD_TOTAL_SUBSET_11() { return &___INSIDE_ODD_TOTAL_SUBSET_11; }
	inline void set_INSIDE_ODD_TOTAL_SUBSET_11(Int32U5BU5D_t3230847821* value)
	{
		___INSIDE_ODD_TOTAL_SUBSET_11 = value;
		Il2CppCodeGenWriteBarrier(&___INSIDE_ODD_TOTAL_SUBSET_11, value);
	}

	inline static int32_t get_offset_of_OUTSIDE_GSUM_12() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___OUTSIDE_GSUM_12)); }
	inline Int32U5BU5D_t3230847821* get_OUTSIDE_GSUM_12() const { return ___OUTSIDE_GSUM_12; }
	inline Int32U5BU5D_t3230847821** get_address_of_OUTSIDE_GSUM_12() { return &___OUTSIDE_GSUM_12; }
	inline void set_OUTSIDE_GSUM_12(Int32U5BU5D_t3230847821* value)
	{
		___OUTSIDE_GSUM_12 = value;
		Il2CppCodeGenWriteBarrier(&___OUTSIDE_GSUM_12, value);
	}

	inline static int32_t get_offset_of_INSIDE_GSUM_13() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___INSIDE_GSUM_13)); }
	inline Int32U5BU5D_t3230847821* get_INSIDE_GSUM_13() const { return ___INSIDE_GSUM_13; }
	inline Int32U5BU5D_t3230847821** get_address_of_INSIDE_GSUM_13() { return &___INSIDE_GSUM_13; }
	inline void set_INSIDE_GSUM_13(Int32U5BU5D_t3230847821* value)
	{
		___INSIDE_GSUM_13 = value;
		Il2CppCodeGenWriteBarrier(&___INSIDE_GSUM_13, value);
	}

	inline static int32_t get_offset_of_OUTSIDE_ODD_WIDEST_14() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___OUTSIDE_ODD_WIDEST_14)); }
	inline Int32U5BU5D_t3230847821* get_OUTSIDE_ODD_WIDEST_14() const { return ___OUTSIDE_ODD_WIDEST_14; }
	inline Int32U5BU5D_t3230847821** get_address_of_OUTSIDE_ODD_WIDEST_14() { return &___OUTSIDE_ODD_WIDEST_14; }
	inline void set_OUTSIDE_ODD_WIDEST_14(Int32U5BU5D_t3230847821* value)
	{
		___OUTSIDE_ODD_WIDEST_14 = value;
		Il2CppCodeGenWriteBarrier(&___OUTSIDE_ODD_WIDEST_14, value);
	}

	inline static int32_t get_offset_of_INSIDE_ODD_WIDEST_15() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___INSIDE_ODD_WIDEST_15)); }
	inline Int32U5BU5D_t3230847821* get_INSIDE_ODD_WIDEST_15() const { return ___INSIDE_ODD_WIDEST_15; }
	inline Int32U5BU5D_t3230847821** get_address_of_INSIDE_ODD_WIDEST_15() { return &___INSIDE_ODD_WIDEST_15; }
	inline void set_INSIDE_ODD_WIDEST_15(Int32U5BU5D_t3230847821* value)
	{
		___INSIDE_ODD_WIDEST_15 = value;
		Il2CppCodeGenWriteBarrier(&___INSIDE_ODD_WIDEST_15, value);
	}

	inline static int32_t get_offset_of_FINDER_PATTERNS_16() { return static_cast<int32_t>(offsetof(RSS14Reader_t1804556634_StaticFields, ___FINDER_PATTERNS_16)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_FINDER_PATTERNS_16() const { return ___FINDER_PATTERNS_16; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_FINDER_PATTERNS_16() { return &___FINDER_PATTERNS_16; }
	inline void set_FINDER_PATTERNS_16(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___FINDER_PATTERNS_16 = value;
		Il2CppCodeGenWriteBarrier(&___FINDER_PATTERNS_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
