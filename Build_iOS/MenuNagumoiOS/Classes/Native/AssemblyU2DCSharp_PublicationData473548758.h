﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublicationData
struct  PublicationData_t473548758  : public Il2CppObject
{
public:
	// System.Int32 PublicationData::id_publicacao
	int32_t ___id_publicacao_0;
	// System.Boolean PublicationData::favoritou
	bool ___favoritou_1;
	// System.String PublicationData::imagem
	String_t* ___imagem_2;
	// System.String PublicationData::titulo
	String_t* ___titulo_3;
	// System.String PublicationData::texto
	String_t* ___texto_4;
	// System.String PublicationData::datainicial
	String_t* ___datainicial_5;
	// System.String PublicationData::link
	String_t* ___link_6;

public:
	inline static int32_t get_offset_of_id_publicacao_0() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___id_publicacao_0)); }
	inline int32_t get_id_publicacao_0() const { return ___id_publicacao_0; }
	inline int32_t* get_address_of_id_publicacao_0() { return &___id_publicacao_0; }
	inline void set_id_publicacao_0(int32_t value)
	{
		___id_publicacao_0 = value;
	}

	inline static int32_t get_offset_of_favoritou_1() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___favoritou_1)); }
	inline bool get_favoritou_1() const { return ___favoritou_1; }
	inline bool* get_address_of_favoritou_1() { return &___favoritou_1; }
	inline void set_favoritou_1(bool value)
	{
		___favoritou_1 = value;
	}

	inline static int32_t get_offset_of_imagem_2() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___imagem_2)); }
	inline String_t* get_imagem_2() const { return ___imagem_2; }
	inline String_t** get_address_of_imagem_2() { return &___imagem_2; }
	inline void set_imagem_2(String_t* value)
	{
		___imagem_2 = value;
		Il2CppCodeGenWriteBarrier(&___imagem_2, value);
	}

	inline static int32_t get_offset_of_titulo_3() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___titulo_3)); }
	inline String_t* get_titulo_3() const { return ___titulo_3; }
	inline String_t** get_address_of_titulo_3() { return &___titulo_3; }
	inline void set_titulo_3(String_t* value)
	{
		___titulo_3 = value;
		Il2CppCodeGenWriteBarrier(&___titulo_3, value);
	}

	inline static int32_t get_offset_of_texto_4() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___texto_4)); }
	inline String_t* get_texto_4() const { return ___texto_4; }
	inline String_t** get_address_of_texto_4() { return &___texto_4; }
	inline void set_texto_4(String_t* value)
	{
		___texto_4 = value;
		Il2CppCodeGenWriteBarrier(&___texto_4, value);
	}

	inline static int32_t get_offset_of_datainicial_5() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___datainicial_5)); }
	inline String_t* get_datainicial_5() const { return ___datainicial_5; }
	inline String_t** get_address_of_datainicial_5() { return &___datainicial_5; }
	inline void set_datainicial_5(String_t* value)
	{
		___datainicial_5 = value;
		Il2CppCodeGenWriteBarrier(&___datainicial_5, value);
	}

	inline static int32_t get_offset_of_link_6() { return static_cast<int32_t>(offsetof(PublicationData_t473548758, ___link_6)); }
	inline String_t* get_link_6() const { return ___link_6; }
	inline String_t** get_address_of_link_6() { return &___link_6; }
	inline void set_link_6(String_t* value)
	{
		___link_6 = value;
		Il2CppCodeGenWriteBarrier(&___link_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
