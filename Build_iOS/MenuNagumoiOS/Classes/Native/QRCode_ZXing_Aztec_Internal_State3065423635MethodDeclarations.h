﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.State
struct State_t3065423635;
// ZXing.Aztec.Internal.Token
struct Token_t3066207355;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"
#include "QRCode_ZXing_Aztec_Internal_State3065423635.h"

// System.Void ZXing.Aztec.Internal.State::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32,System.Int32)
extern "C"  void State__ctor_m1328489154 (State_t3065423635 * __this, Token_t3066207355 * ___token0, int32_t ___mode1, int32_t ___binaryBytes2, int32_t ___bitCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.State::get_Mode()
extern "C"  int32_t State_get_Mode_m159976514 (State_t3065423635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.State::get_BinaryShiftByteCount()
extern "C"  int32_t State_get_BinaryShiftByteCount_m3493846725 (State_t3065423635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.State::get_BitCount()
extern "C"  int32_t State_get_BitCount_m3322698721 (State_t3065423635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::latchAndAppend(System.Int32,System.Int32)
extern "C"  State_t3065423635 * State_latchAndAppend_m3018849651 (State_t3065423635 * __this, int32_t ___mode0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::shiftAndAppend(System.Int32,System.Int32)
extern "C"  State_t3065423635 * State_shiftAndAppend_m1525538993 (State_t3065423635 * __this, int32_t ___mode0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::addBinaryShiftChar(System.Int32)
extern "C"  State_t3065423635 * State_addBinaryShiftChar_m641788621 (State_t3065423635 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::endBinaryShift(System.Int32)
extern "C"  State_t3065423635 * State_endBinaryShift_m2944678237 (State_t3065423635 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Aztec.Internal.State::isBetterThanOrEqualTo(ZXing.Aztec.Internal.State)
extern "C"  bool State_isBetterThanOrEqualTo_m342165880 (State_t3065423635 * __this, State_t3065423635 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Aztec.Internal.State::toBitArray(System.Byte[])
extern "C"  BitArray_t4163851164 * State_toBitArray_m208342924 (State_t3065423635 * __this, ByteU5BU5D_t4260760469* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Aztec.Internal.State::ToString()
extern "C"  String_t* State_ToString_m1855422679 (State_t3065423635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.State::.cctor()
extern "C"  void State__cctor_m1075511921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
