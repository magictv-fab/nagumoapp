﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenRotateFrom
struct  iTweenRotateFrom_t2181561423  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenRotateFrom::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenRotateFrom::id
	FsmString_t952858651 * ___id_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenRotateFrom::transformRotation
	FsmGameObject_t1697147867 * ___transformRotation_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenRotateFrom::vectorRotation
	FsmVector3_t533912882 * ___vectorRotation_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateFrom::time
	FsmFloat_t2134102846 * ___time_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateFrom::delay
	FsmFloat_t2134102846 * ___delay_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateFrom::speed
	FsmFloat_t2134102846 * ___speed_23;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenRotateFrom::easeType
	int32_t ___easeType_24;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenRotateFrom::loopType
	int32_t ___loopType_25;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenRotateFrom::space
	int32_t ___space_26;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_id_18() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___id_18)); }
	inline FsmString_t952858651 * get_id_18() const { return ___id_18; }
	inline FsmString_t952858651 ** get_address_of_id_18() { return &___id_18; }
	inline void set_id_18(FsmString_t952858651 * value)
	{
		___id_18 = value;
		Il2CppCodeGenWriteBarrier(&___id_18, value);
	}

	inline static int32_t get_offset_of_transformRotation_19() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___transformRotation_19)); }
	inline FsmGameObject_t1697147867 * get_transformRotation_19() const { return ___transformRotation_19; }
	inline FsmGameObject_t1697147867 ** get_address_of_transformRotation_19() { return &___transformRotation_19; }
	inline void set_transformRotation_19(FsmGameObject_t1697147867 * value)
	{
		___transformRotation_19 = value;
		Il2CppCodeGenWriteBarrier(&___transformRotation_19, value);
	}

	inline static int32_t get_offset_of_vectorRotation_20() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___vectorRotation_20)); }
	inline FsmVector3_t533912882 * get_vectorRotation_20() const { return ___vectorRotation_20; }
	inline FsmVector3_t533912882 ** get_address_of_vectorRotation_20() { return &___vectorRotation_20; }
	inline void set_vectorRotation_20(FsmVector3_t533912882 * value)
	{
		___vectorRotation_20 = value;
		Il2CppCodeGenWriteBarrier(&___vectorRotation_20, value);
	}

	inline static int32_t get_offset_of_time_21() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___time_21)); }
	inline FsmFloat_t2134102846 * get_time_21() const { return ___time_21; }
	inline FsmFloat_t2134102846 ** get_address_of_time_21() { return &___time_21; }
	inline void set_time_21(FsmFloat_t2134102846 * value)
	{
		___time_21 = value;
		Il2CppCodeGenWriteBarrier(&___time_21, value);
	}

	inline static int32_t get_offset_of_delay_22() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___delay_22)); }
	inline FsmFloat_t2134102846 * get_delay_22() const { return ___delay_22; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_22() { return &___delay_22; }
	inline void set_delay_22(FsmFloat_t2134102846 * value)
	{
		___delay_22 = value;
		Il2CppCodeGenWriteBarrier(&___delay_22, value);
	}

	inline static int32_t get_offset_of_speed_23() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___speed_23)); }
	inline FsmFloat_t2134102846 * get_speed_23() const { return ___speed_23; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_23() { return &___speed_23; }
	inline void set_speed_23(FsmFloat_t2134102846 * value)
	{
		___speed_23 = value;
		Il2CppCodeGenWriteBarrier(&___speed_23, value);
	}

	inline static int32_t get_offset_of_easeType_24() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___easeType_24)); }
	inline int32_t get_easeType_24() const { return ___easeType_24; }
	inline int32_t* get_address_of_easeType_24() { return &___easeType_24; }
	inline void set_easeType_24(int32_t value)
	{
		___easeType_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_space_26() { return static_cast<int32_t>(offsetof(iTweenRotateFrom_t2181561423, ___space_26)); }
	inline int32_t get_space_26() const { return ___space_26; }
	inline int32_t* get_address_of_space_26() { return &___space_26; }
	inline void set_space_26(int32_t value)
	{
		___space_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
