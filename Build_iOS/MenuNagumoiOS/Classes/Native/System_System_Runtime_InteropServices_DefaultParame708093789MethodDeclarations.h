﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.DefaultParameterValueAttribute
struct DefaultParameterValueAttribute_t708093789;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Runtime.InteropServices.DefaultParameterValueAttribute::.ctor(System.Object)
extern "C"  void DefaultParameterValueAttribute__ctor_m1133248517 (DefaultParameterValueAttribute_t708093789 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.DefaultParameterValueAttribute::get_Value()
extern "C"  Il2CppObject * DefaultParameterValueAttribute_get_Value_m3413348618 (DefaultParameterValueAttribute_t708093789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
