﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.BZip2.BZip2InputStream
struct BZip2InputStream_t1830158201;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::.ctor(System.IO.Stream)
extern "C"  void BZip2InputStream__ctor_m2595658959 (BZip2InputStream_t1830158201 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::get_IsStreamOwner()
extern "C"  bool BZip2InputStream_get_IsStreamOwner_m4036150018 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::set_IsStreamOwner(System.Boolean)
extern "C"  void BZip2InputStream_set_IsStreamOwner_m1298492729 (BZip2InputStream_t1830158201 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::get_CanRead()
extern "C"  bool BZip2InputStream_get_CanRead_m2655586239 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::get_CanSeek()
extern "C"  bool BZip2InputStream_get_CanSeek_m2684341281 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::get_CanWrite()
extern "C"  bool BZip2InputStream_get_CanWrite_m1241448312 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::get_Length()
extern "C"  int64_t BZip2InputStream_get_Length_m3813641482 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::get_Position()
extern "C"  int64_t BZip2InputStream_get_Position_m1523861965 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::set_Position(System.Int64)
extern "C"  void BZip2InputStream_set_Position_m3913799108 (BZip2InputStream_t1830158201 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Flush()
extern "C"  void BZip2InputStream_Flush_m4059739482 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t BZip2InputStream_Seek_m4285655194 (BZip2InputStream_t1830158201 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetLength(System.Int64)
extern "C"  void BZip2InputStream_SetLength_m532300944 (BZip2InputStream_t1830158201 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void BZip2InputStream_Write_m3964496596 (BZip2InputStream_t1830158201 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::WriteByte(System.Byte)
extern "C"  void BZip2InputStream_WriteByte_m1323760302 (BZip2InputStream_t1830158201 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t BZip2InputStream_Read_m164699705 (BZip2InputStream_t1830158201 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Close()
extern "C"  void BZip2InputStream_Close_m1391684430 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::ReadByte()
extern "C"  int32_t BZip2InputStream_ReadByte_m769266520 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::MakeMaps()
extern "C"  void BZip2InputStream_MakeMaps_m2534852337 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Initialize()
extern "C"  void BZip2InputStream_Initialize_m3123557212 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::InitBlock()
extern "C"  void BZip2InputStream_InitBlock_m442445555 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::EndBlock()
extern "C"  void BZip2InputStream_EndBlock_m2672298622 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::Complete()
extern "C"  void BZip2InputStream_Complete_m1925024133 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BsSetStream(System.IO.Stream)
extern "C"  void BZip2InputStream_BsSetStream_m247999774 (BZip2InputStream_t1830158201 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::FillBuffer()
extern "C"  void BZip2InputStream_FillBuffer_m966276783 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BsR(System.Int32)
extern "C"  int32_t BZip2InputStream_BsR_m1855357178 (BZip2InputStream_t1830158201 * __this, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BsGetUChar()
extern "C"  Il2CppChar BZip2InputStream_BsGetUChar_m1172916976 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BsGetIntVS(System.Int32)
extern "C"  int32_t BZip2InputStream_BsGetIntVS_m1826855826 (BZip2InputStream_t1830158201 * __this, int32_t ___numBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BsGetInt32()
extern "C"  int32_t BZip2InputStream_BsGetInt32_m3812891363 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::RecvDecodingTables()
extern "C"  void BZip2InputStream_RecvDecodingTables_m4027502658 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::GetAndMoveToFrontDecode()
extern "C"  void BZip2InputStream_GetAndMoveToFrontDecode_m3516541088 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupBlock()
extern "C"  void BZip2InputStream_SetupBlock_m1309763548 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupRandPartA()
extern "C"  void BZip2InputStream_SetupRandPartA_m152603352 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupNoRandPartA()
extern "C"  void BZip2InputStream_SetupNoRandPartA_m610944727 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupRandPartB()
extern "C"  void BZip2InputStream_SetupRandPartB_m152604313 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupRandPartC()
extern "C"  void BZip2InputStream_SetupRandPartC_m152605274 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupNoRandPartB()
extern "C"  void BZip2InputStream_SetupNoRandPartB_m610945688 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetupNoRandPartC()
extern "C"  void BZip2InputStream_SetupNoRandPartC_m610946649 (BZip2InputStream_t1830158201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::SetDecompressStructureSizes(System.Int32)
extern "C"  void BZip2InputStream_SetDecompressStructureSizes_m2177858059 (BZip2InputStream_t1830158201 * __this, int32_t ___newSize100k0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::CompressedStreamEOF()
extern "C"  void BZip2InputStream_CompressedStreamEOF_m3091435185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BlockOverrun()
extern "C"  void BZip2InputStream_BlockOverrun_m3499891318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::BadBlockHeader()
extern "C"  void BZip2InputStream_BadBlockHeader_m3915518273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::CrcError()
extern "C"  void BZip2InputStream_CrcError_m1225085472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::HbCreateDecodeTables(System.Int32[],System.Int32[],System.Int32[],System.Char[],System.Int32,System.Int32,System.Int32)
extern "C"  void BZip2InputStream_HbCreateDecodeTables_m2716904734 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___limit0, Int32U5BU5D_t3230847821* ___baseArray1, Int32U5BU5D_t3230847821* ___perm2, CharU5BU5D_t3324145743* ___length3, int32_t ___minLen4, int32_t ___maxLen5, int32_t ___alphaSize6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
