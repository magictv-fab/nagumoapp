﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ProcessFileHandler
struct ProcessFileHandler_t83050093;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.ScanEventArgs
struct ScanEventArgs_t1070518092;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca1070518092.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Core.ProcessFileHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ProcessFileHandler__ctor_m1723865076 (ProcessFileHandler_t83050093 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProcessFileHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanEventArgs)
extern "C"  void ProcessFileHandler_Invoke_m3676059524 (ProcessFileHandler_t83050093 * __this, Il2CppObject * ___sender0, ScanEventArgs_t1070518092 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Core.ProcessFileHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ProcessFileHandler_BeginInvoke_m3768859915 (ProcessFileHandler_t83050093 * __this, Il2CppObject * ___sender0, ScanEventArgs_t1070518092 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProcessFileHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ProcessFileHandler_EndInvoke_m3357910020 (ProcessFileHandler_t83050093 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
