﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSSubscriptionState
struct  OSSubscriptionState_t1688362992  : public Il2CppObject
{
public:
	// System.Boolean OSSubscriptionState::userSubscriptionSetting
	bool ___userSubscriptionSetting_0;
	// System.String OSSubscriptionState::userId
	String_t* ___userId_1;
	// System.String OSSubscriptionState::pushToken
	String_t* ___pushToken_2;
	// System.Boolean OSSubscriptionState::subscribed
	bool ___subscribed_3;

public:
	inline static int32_t get_offset_of_userSubscriptionSetting_0() { return static_cast<int32_t>(offsetof(OSSubscriptionState_t1688362992, ___userSubscriptionSetting_0)); }
	inline bool get_userSubscriptionSetting_0() const { return ___userSubscriptionSetting_0; }
	inline bool* get_address_of_userSubscriptionSetting_0() { return &___userSubscriptionSetting_0; }
	inline void set_userSubscriptionSetting_0(bool value)
	{
		___userSubscriptionSetting_0 = value;
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(OSSubscriptionState_t1688362992, ___userId_1)); }
	inline String_t* get_userId_1() const { return ___userId_1; }
	inline String_t** get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(String_t* value)
	{
		___userId_1 = value;
		Il2CppCodeGenWriteBarrier(&___userId_1, value);
	}

	inline static int32_t get_offset_of_pushToken_2() { return static_cast<int32_t>(offsetof(OSSubscriptionState_t1688362992, ___pushToken_2)); }
	inline String_t* get_pushToken_2() const { return ___pushToken_2; }
	inline String_t** get_address_of_pushToken_2() { return &___pushToken_2; }
	inline void set_pushToken_2(String_t* value)
	{
		___pushToken_2 = value;
		Il2CppCodeGenWriteBarrier(&___pushToken_2, value);
	}

	inline static int32_t get_offset_of_subscribed_3() { return static_cast<int32_t>(offsetof(OSSubscriptionState_t1688362992, ___subscribed_3)); }
	inline bool get_subscribed_3() const { return ___subscribed_3; }
	inline bool* get_address_of_subscribed_3() { return &___subscribed_3; }
	inline void set_subscribed_3(bool value)
	{
		___subscribed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
