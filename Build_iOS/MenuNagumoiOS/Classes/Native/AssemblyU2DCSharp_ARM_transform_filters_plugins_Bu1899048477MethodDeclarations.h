﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.plugins.BussolaPluginRotationCalibrate
struct BussolaPluginRotationCalibrate_t1899048477;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::.ctor()
extern "C"  void BussolaPluginRotationCalibrate__ctor_m1312849089 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::Start()
extern "C"  void BussolaPluginRotationCalibrate_Start_m259986881 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::reset()
extern "C"  void BussolaPluginRotationCalibrate_reset_m1589596046 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::_turnOff()
extern "C"  void BussolaPluginRotationCalibrate__turnOff_m3742569718 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::_turnOn()
extern "C"  void BussolaPluginRotationCalibrate__turnOn_m1090565146 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::Update()
extern "C"  void BussolaPluginRotationCalibrate_Update_m3770478188 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::checkToggle()
extern "C"  void BussolaPluginRotationCalibrate_checkToggle_m1487997691 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::calibrateBussola()
extern "C"  void BussolaPluginRotationCalibrate_calibrateBussola_m2834642253 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::turnOn()
extern "C"  void BussolaPluginRotationCalibrate_turnOn_m2517295103 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::turnOff()
extern "C"  void BussolaPluginRotationCalibrate_turnOff_m726558129 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::getUniqueName()
extern "C"  String_t* BussolaPluginRotationCalibrate_getUniqueName_m4221782062 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::ResetOnOff()
extern "C"  void BussolaPluginRotationCalibrate_ResetOnOff_m1425021348 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::getGameObjectReference()
extern "C"  GameObject_t3674682005 * BussolaPluginRotationCalibrate_getGameObjectReference_m1388134652 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::isActive()
extern "C"  bool BussolaPluginRotationCalibrate_isActive_m4126641863 (BussolaPluginRotationCalibrate_t1899048477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
