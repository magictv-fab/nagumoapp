﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1876580908(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3035933110 *, String_t*, InAppAnalytics_t2316734034 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>::get_Key()
#define KeyValuePair_2_get_Key_m348737596(__this, method) ((  String_t* (*) (KeyValuePair_2_t3035933110 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m534271101(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3035933110 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>::get_Value()
#define KeyValuePair_2_get_Value_m236751868(__this, method) ((  InAppAnalytics_t2316734034 * (*) (KeyValuePair_2_t3035933110 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m248193533(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3035933110 *, InAppAnalytics_t2316734034 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>::ToString()
#define KeyValuePair_2_ToString_m4056233925(__this, method) ((  String_t* (*) (KeyValuePair_2_t3035933110 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
