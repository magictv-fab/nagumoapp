﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.GZip.GZipInputStream
struct GZipInputStream_t2489725366;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ICSharpCode.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream)
extern "C"  void GZipInputStream__ctor_m2895924613 (GZipInputStream_t2489725366 * __this, Stream_t1561764144 * ___baseInputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void GZipInputStream__ctor_m2917511026 (GZipInputStream_t2489725366 * __this, Stream_t1561764144 * ___baseInputStream0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.GZip.GZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t GZipInputStream_Read_m2375960303 (GZipInputStream_t2489725366 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.GZip.GZipInputStream::ReadHeader()
extern "C"  bool GZipInputStream_ReadHeader_m3119436173 (GZipInputStream_t2489725366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.GZip.GZipInputStream::ReadFooter()
extern "C"  void GZipInputStream_ReadFooter_m4082086759 (GZipInputStream_t2489725366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
