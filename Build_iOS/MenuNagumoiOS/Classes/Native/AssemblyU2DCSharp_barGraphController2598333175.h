﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// barGraphController
struct  barGraphController_t2598333175  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text barGraphController::saldoAcumulado
	Text_t9039225 * ___saldoAcumulado_2;
	// UnityEngine.UI.Text barGraphController::valorRestante
	Text_t9039225 * ___valorRestante_3;
	// UnityEngine.UI.Image barGraphController::fillBar
	Image_t538875265 * ___fillBar_4;
	// System.Single barGraphController::total
	float ___total_5;

public:
	inline static int32_t get_offset_of_saldoAcumulado_2() { return static_cast<int32_t>(offsetof(barGraphController_t2598333175, ___saldoAcumulado_2)); }
	inline Text_t9039225 * get_saldoAcumulado_2() const { return ___saldoAcumulado_2; }
	inline Text_t9039225 ** get_address_of_saldoAcumulado_2() { return &___saldoAcumulado_2; }
	inline void set_saldoAcumulado_2(Text_t9039225 * value)
	{
		___saldoAcumulado_2 = value;
		Il2CppCodeGenWriteBarrier(&___saldoAcumulado_2, value);
	}

	inline static int32_t get_offset_of_valorRestante_3() { return static_cast<int32_t>(offsetof(barGraphController_t2598333175, ___valorRestante_3)); }
	inline Text_t9039225 * get_valorRestante_3() const { return ___valorRestante_3; }
	inline Text_t9039225 ** get_address_of_valorRestante_3() { return &___valorRestante_3; }
	inline void set_valorRestante_3(Text_t9039225 * value)
	{
		___valorRestante_3 = value;
		Il2CppCodeGenWriteBarrier(&___valorRestante_3, value);
	}

	inline static int32_t get_offset_of_fillBar_4() { return static_cast<int32_t>(offsetof(barGraphController_t2598333175, ___fillBar_4)); }
	inline Image_t538875265 * get_fillBar_4() const { return ___fillBar_4; }
	inline Image_t538875265 ** get_address_of_fillBar_4() { return &___fillBar_4; }
	inline void set_fillBar_4(Image_t538875265 * value)
	{
		___fillBar_4 = value;
		Il2CppCodeGenWriteBarrier(&___fillBar_4, value);
	}

	inline static int32_t get_offset_of_total_5() { return static_cast<int32_t>(offsetof(barGraphController_t2598333175, ___total_5)); }
	inline float get_total_5() const { return ___total_5; }
	inline float* get_address_of_total_5() { return &___total_5; }
	inline void set_total_5(float value)
	{
		___total_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
