﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorTarget
struct  SetAnimatorTarget_t2430133416  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// UnityEngine.AvatarTarget HutongGames.PlayMaker.Actions.SetAnimatorTarget::avatarTarget
	int32_t ___avatarTarget_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorTarget::targetNormalizedTime
	FsmFloat_t2134102846 * ___targetNormalizedTime_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorTarget::everyFrame
	bool ___everyFrame_12;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.SetAnimatorTarget::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorTarget::_animator
	Animator_t2776330603 * ____animator_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_avatarTarget_10() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___avatarTarget_10)); }
	inline int32_t get_avatarTarget_10() const { return ___avatarTarget_10; }
	inline int32_t* get_address_of_avatarTarget_10() { return &___avatarTarget_10; }
	inline void set_avatarTarget_10(int32_t value)
	{
		___avatarTarget_10 = value;
	}

	inline static int32_t get_offset_of_targetNormalizedTime_11() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___targetNormalizedTime_11)); }
	inline FsmFloat_t2134102846 * get_targetNormalizedTime_11() const { return ___targetNormalizedTime_11; }
	inline FsmFloat_t2134102846 ** get_address_of_targetNormalizedTime_11() { return &___targetNormalizedTime_11; }
	inline void set_targetNormalizedTime_11(FsmFloat_t2134102846 * value)
	{
		___targetNormalizedTime_11 = value;
		Il2CppCodeGenWriteBarrier(&___targetNormalizedTime_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of__animatorProxy_13() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ____animatorProxy_13)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_13() const { return ____animatorProxy_13; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_13() { return &____animatorProxy_13; }
	inline void set__animatorProxy_13(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_13 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
