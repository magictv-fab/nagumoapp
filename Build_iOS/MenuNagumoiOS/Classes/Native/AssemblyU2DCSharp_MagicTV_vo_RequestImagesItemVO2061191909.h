﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.RequestImagesItemVO
struct  RequestImagesItemVO_t2061191909  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.RequestImagesItemVO::url
	String_t* ___url_0;
	// System.String MagicTV.vo.RequestImagesItemVO::size
	String_t* ___size_1;
	// System.String MagicTV.vo.RequestImagesItemVO::tempo
	String_t* ___tempo_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(RequestImagesItemVO_t2061191909, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(RequestImagesItemVO_t2061191909, ___size_1)); }
	inline String_t* get_size_1() const { return ___size_1; }
	inline String_t** get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(String_t* value)
	{
		___size_1 = value;
		Il2CppCodeGenWriteBarrier(&___size_1, value);
	}

	inline static int32_t get_offset_of_tempo_2() { return static_cast<int32_t>(offsetof(RequestImagesItemVO_t2061191909, ___tempo_2)); }
	inline String_t* get_tempo_2() const { return ___tempo_2; }
	inline String_t** get_address_of_tempo_2() { return &___tempo_2; }
	inline void set_tempo_2(String_t* value)
	{
		___tempo_2 = value;
		Il2CppCodeGenWriteBarrier(&___tempo_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
