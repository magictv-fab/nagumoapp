﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.BarcodeFormat>
struct DefaultComparer_t2649582484;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.BarcodeFormat>::.ctor()
extern "C"  void DefaultComparer__ctor_m3012322923_gshared (DefaultComparer_t2649582484 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3012322923(__this, method) ((  void (*) (DefaultComparer_t2649582484 *, const MethodInfo*))DefaultComparer__ctor_m3012322923_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.BarcodeFormat>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2697922272_gshared (DefaultComparer_t2649582484 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2697922272(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2649582484 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2697922272_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.BarcodeFormat>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3784023484_gshared (DefaultComparer_t2649582484 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3784023484(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2649582484 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3784023484_gshared)(__this, ___x0, ___y1, method)
