﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.CameraDistanceConfig
struct CameraDistanceConfig_t2003187004;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.SimpleEvents.CameraDistanceConfig::.ctor()
extern "C"  void CameraDistanceConfig__ctor_m1680130590 (CameraDistanceConfig_t2003187004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.SimpleEvents.CameraDistanceConfig InApp.SimpleEvents.CameraDistanceConfig::GetInstance()
extern "C"  CameraDistanceConfig_t2003187004 * CameraDistanceConfig_GetInstance_m1693385869 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.CameraDistanceConfig::Start()
extern "C"  void CameraDistanceConfig_Start_m627268382 (CameraDistanceConfig_t2003187004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.CameraDistanceConfig::configByString(System.String)
extern "C"  void CameraDistanceConfig_configByString_m2468306386 (CameraDistanceConfig_t2003187004 * __this, String_t* ___metadado0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.CameraDistanceConfig::Reset()
extern "C"  void CameraDistanceConfig_Reset_m3621530827 (CameraDistanceConfig_t2003187004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
