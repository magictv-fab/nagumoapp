﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/OnSetEmailSuccess
struct OnSetEmailSuccess_t2952831753;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/OnSetEmailSuccess::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSetEmailSuccess__ctor_m2990880864 (OnSetEmailSuccess_t2952831753 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnSetEmailSuccess::Invoke()
extern "C"  void OnSetEmailSuccess_Invoke_m1711512762 (OnSetEmailSuccess_t2952831753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/OnSetEmailSuccess::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSetEmailSuccess_BeginInvoke_m551840657 (OnSetEmailSuccess_t2952831753 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnSetEmailSuccess::EndInvoke(System.IAsyncResult)
extern "C"  void OnSetEmailSuccess_EndInvoke_m331602544 (OnSetEmailSuccess_t2952831753 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
