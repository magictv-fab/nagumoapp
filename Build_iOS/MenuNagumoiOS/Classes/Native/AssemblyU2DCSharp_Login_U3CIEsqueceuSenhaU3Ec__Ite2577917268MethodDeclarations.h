﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login/<IEsqueceuSenha>c__Iterator65
struct U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Login/<IEsqueceuSenha>c__Iterator65::.ctor()
extern "C"  void U3CIEsqueceuSenhaU3Ec__Iterator65__ctor_m3023969431 (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<IEsqueceuSenha>c__Iterator65::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIEsqueceuSenhaU3Ec__Iterator65_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1644255643 (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<IEsqueceuSenha>c__Iterator65::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIEsqueceuSenhaU3Ec__Iterator65_System_Collections_IEnumerator_get_Current_m3155686191 (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Login/<IEsqueceuSenha>c__Iterator65::MoveNext()
extern "C"  bool U3CIEsqueceuSenhaU3Ec__Iterator65_MoveNext_m437772285 (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<IEsqueceuSenha>c__Iterator65::Dispose()
extern "C"  void U3CIEsqueceuSenhaU3Ec__Iterator65_Dispose_m2535014164 (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<IEsqueceuSenha>c__Iterator65::Reset()
extern "C"  void U3CIEsqueceuSenhaU3Ec__Iterator65_Reset_m670402372 (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
