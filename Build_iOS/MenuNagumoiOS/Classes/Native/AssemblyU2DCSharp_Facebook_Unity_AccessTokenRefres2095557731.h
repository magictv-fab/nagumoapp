﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.AccessToken
struct AccessToken_t3236047507;

#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase64408782.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessTokenRefreshResult
struct  AccessTokenRefreshResult_t2095557731  : public ResultBase_t64408782
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::<AccessToken>k__BackingField
	AccessToken_t3236047507 * ___U3CAccessTokenU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AccessTokenRefreshResult_t2095557731, ___U3CAccessTokenU3Ek__BackingField_5)); }
	inline AccessToken_t3236047507 * get_U3CAccessTokenU3Ek__BackingField_5() const { return ___U3CAccessTokenU3Ek__BackingField_5; }
	inline AccessToken_t3236047507 ** get_address_of_U3CAccessTokenU3Ek__BackingField_5() { return &___U3CAccessTokenU3Ek__BackingField_5; }
	inline void set_U3CAccessTokenU3Ek__BackingField_5(AccessToken_t3236047507 * value)
	{
		___U3CAccessTokenU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccessTokenU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
