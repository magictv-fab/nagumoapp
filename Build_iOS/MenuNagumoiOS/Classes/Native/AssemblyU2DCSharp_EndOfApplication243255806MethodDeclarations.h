﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EndOfApplication
struct EndOfApplication_t243255806;

#include "codegen/il2cpp-codegen.h"

// System.Void EndOfApplication::.ctor()
extern "C"  void EndOfApplication__ctor_m3278223965 (EndOfApplication_t243255806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EndOfApplication::Start()
extern "C"  void EndOfApplication_Start_m2225361757 (EndOfApplication_t243255806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
