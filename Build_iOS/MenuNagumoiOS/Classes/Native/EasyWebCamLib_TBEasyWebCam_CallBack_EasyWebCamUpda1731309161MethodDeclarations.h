﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate
struct EasyWebCamUpdateDelegate_t1731309161;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void EasyWebCamUpdateDelegate__ctor_m4246703454 (EasyWebCamUpdateDelegate_t1731309161 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::Invoke()
extern "C"  void EasyWebCamUpdateDelegate_Invoke_m2699506488 (EasyWebCamUpdateDelegate_t1731309161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EasyWebCamUpdateDelegate_BeginInvoke_m1144340427 (EasyWebCamUpdateDelegate_t1731309161 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void EasyWebCamUpdateDelegate_EndInvoke_m1184652910 (EasyWebCamUpdateDelegate_t1731309161 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
