﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_AI01weight1167885677.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder
struct  AI013x0x1xDecoder_t1405329809  : public AI01weightDecoder_t1167885677
{
public:
	// System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::dateCode
	String_t* ___dateCode_6;
	// System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::firstAIdigits
	String_t* ___firstAIdigits_7;

public:
	inline static int32_t get_offset_of_dateCode_6() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t1405329809, ___dateCode_6)); }
	inline String_t* get_dateCode_6() const { return ___dateCode_6; }
	inline String_t** get_address_of_dateCode_6() { return &___dateCode_6; }
	inline void set_dateCode_6(String_t* value)
	{
		___dateCode_6 = value;
		Il2CppCodeGenWriteBarrier(&___dateCode_6, value);
	}

	inline static int32_t get_offset_of_firstAIdigits_7() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t1405329809, ___firstAIdigits_7)); }
	inline String_t* get_firstAIdigits_7() const { return ___firstAIdigits_7; }
	inline String_t** get_address_of_firstAIdigits_7() { return &___firstAIdigits_7; }
	inline void set_firstAIdigits_7(String_t* value)
	{
		___firstAIdigits_7 = value;
		Il2CppCodeGenWriteBarrier(&___firstAIdigits_7, value);
	}
};

struct AI013x0x1xDecoder_t1405329809_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::WEIGHT_SIZE
	int32_t ___WEIGHT_SIZE_4;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::DATE_SIZE
	int32_t ___DATE_SIZE_5;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t1405329809_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}

	inline static int32_t get_offset_of_WEIGHT_SIZE_4() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t1405329809_StaticFields, ___WEIGHT_SIZE_4)); }
	inline int32_t get_WEIGHT_SIZE_4() const { return ___WEIGHT_SIZE_4; }
	inline int32_t* get_address_of_WEIGHT_SIZE_4() { return &___WEIGHT_SIZE_4; }
	inline void set_WEIGHT_SIZE_4(int32_t value)
	{
		___WEIGHT_SIZE_4 = value;
	}

	inline static int32_t get_offset_of_DATE_SIZE_5() { return static_cast<int32_t>(offsetof(AI013x0x1xDecoder_t1405329809_StaticFields, ___DATE_SIZE_5)); }
	inline int32_t get_DATE_SIZE_5() const { return ___DATE_SIZE_5; }
	inline int32_t* get_address_of_DATE_SIZE_5() { return &___DATE_SIZE_5; }
	inline void set_DATE_SIZE_5(int32_t value)
	{
		___DATE_SIZE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
