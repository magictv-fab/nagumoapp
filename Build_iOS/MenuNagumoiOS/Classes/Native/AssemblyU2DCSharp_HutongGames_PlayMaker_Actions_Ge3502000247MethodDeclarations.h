﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct GetAnimatorNextStateInfo_t3502000247;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::.ctor()
extern "C"  void GetAnimatorNextStateInfo__ctor_m1442289615 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::Reset()
extern "C"  void GetAnimatorNextStateInfo_Reset_m3383689852 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnEnter()
extern "C"  void GetAnimatorNextStateInfo_OnEnter_m2452579302 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorNextStateInfo_OnAnimatorMoveEvent_m3957936432 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnUpdate()
extern "C"  void GetAnimatorNextStateInfo_OnUpdate_m2149073693 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::GetLayerInfo()
extern "C"  void GetAnimatorNextStateInfo_GetLayerInfo_m3523314750 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnExit()
extern "C"  void GetAnimatorNextStateInfo_OnExit_m3274623730 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
