﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.in_apps.OnInAppCommonEventHandler
struct OnInAppCommonEventHandler_t1653412278;
// MagicTV.in_apps.OnInAppProgressEventHandler
struct OnInAppProgressEventHandler_t3139301368;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppProgressInfo
struct  InAppProgressInfo_t2775900725  : public Il2CppObject
{
public:
	// System.Boolean MagicTV.in_apps.InAppProgressInfo::_isComplete
	bool ____isComplete_0;
	// System.Single MagicTV.in_apps.InAppProgressInfo::_currentProgress
	float ____currentProgress_1;
	// System.Single MagicTV.in_apps.InAppProgressInfo::TotalBytesToDownload
	float ___TotalBytesToDownload_2;
	// MagicTV.in_apps.OnInAppCommonEventHandler MagicTV.in_apps.InAppProgressInfo::onComplete
	OnInAppCommonEventHandler_t1653412278 * ___onComplete_3;
	// MagicTV.in_apps.OnInAppCommonEventHandler MagicTV.in_apps.InAppProgressInfo::onError
	OnInAppCommonEventHandler_t1653412278 * ___onError_4;
	// MagicTV.in_apps.OnInAppProgressEventHandler MagicTV.in_apps.InAppProgressInfo::onProgress
	OnInAppProgressEventHandler_t3139301368 * ___onProgress_5;

public:
	inline static int32_t get_offset_of__isComplete_0() { return static_cast<int32_t>(offsetof(InAppProgressInfo_t2775900725, ____isComplete_0)); }
	inline bool get__isComplete_0() const { return ____isComplete_0; }
	inline bool* get_address_of__isComplete_0() { return &____isComplete_0; }
	inline void set__isComplete_0(bool value)
	{
		____isComplete_0 = value;
	}

	inline static int32_t get_offset_of__currentProgress_1() { return static_cast<int32_t>(offsetof(InAppProgressInfo_t2775900725, ____currentProgress_1)); }
	inline float get__currentProgress_1() const { return ____currentProgress_1; }
	inline float* get_address_of__currentProgress_1() { return &____currentProgress_1; }
	inline void set__currentProgress_1(float value)
	{
		____currentProgress_1 = value;
	}

	inline static int32_t get_offset_of_TotalBytesToDownload_2() { return static_cast<int32_t>(offsetof(InAppProgressInfo_t2775900725, ___TotalBytesToDownload_2)); }
	inline float get_TotalBytesToDownload_2() const { return ___TotalBytesToDownload_2; }
	inline float* get_address_of_TotalBytesToDownload_2() { return &___TotalBytesToDownload_2; }
	inline void set_TotalBytesToDownload_2(float value)
	{
		___TotalBytesToDownload_2 = value;
	}

	inline static int32_t get_offset_of_onComplete_3() { return static_cast<int32_t>(offsetof(InAppProgressInfo_t2775900725, ___onComplete_3)); }
	inline OnInAppCommonEventHandler_t1653412278 * get_onComplete_3() const { return ___onComplete_3; }
	inline OnInAppCommonEventHandler_t1653412278 ** get_address_of_onComplete_3() { return &___onComplete_3; }
	inline void set_onComplete_3(OnInAppCommonEventHandler_t1653412278 * value)
	{
		___onComplete_3 = value;
		Il2CppCodeGenWriteBarrier(&___onComplete_3, value);
	}

	inline static int32_t get_offset_of_onError_4() { return static_cast<int32_t>(offsetof(InAppProgressInfo_t2775900725, ___onError_4)); }
	inline OnInAppCommonEventHandler_t1653412278 * get_onError_4() const { return ___onError_4; }
	inline OnInAppCommonEventHandler_t1653412278 ** get_address_of_onError_4() { return &___onError_4; }
	inline void set_onError_4(OnInAppCommonEventHandler_t1653412278 * value)
	{
		___onError_4 = value;
		Il2CppCodeGenWriteBarrier(&___onError_4, value);
	}

	inline static int32_t get_offset_of_onProgress_5() { return static_cast<int32_t>(offsetof(InAppProgressInfo_t2775900725, ___onProgress_5)); }
	inline OnInAppProgressEventHandler_t3139301368 * get_onProgress_5() const { return ___onProgress_5; }
	inline OnInAppProgressEventHandler_t3139301368 ** get_address_of_onProgress_5() { return &___onProgress_5; }
	inline void set_onProgress_5(OnInAppProgressEventHandler_t3139301368 * value)
	{
		___onProgress_5 = value;
		Il2CppCodeGenWriteBarrier(&___onProgress_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
