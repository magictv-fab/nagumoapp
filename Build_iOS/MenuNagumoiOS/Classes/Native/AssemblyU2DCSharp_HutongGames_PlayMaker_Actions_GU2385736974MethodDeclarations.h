﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIHorizontalSlider
struct GUIHorizontalSlider_t2385736974;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIHorizontalSlider::.ctor()
extern "C"  void GUIHorizontalSlider__ctor_m3244547560 (GUIHorizontalSlider_t2385736974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIHorizontalSlider::Reset()
extern "C"  void GUIHorizontalSlider_Reset_m890980501 (GUIHorizontalSlider_t2385736974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIHorizontalSlider::OnGUI()
extern "C"  void GUIHorizontalSlider_OnGUI_m2739946210 (GUIHorizontalSlider_t2385736974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
