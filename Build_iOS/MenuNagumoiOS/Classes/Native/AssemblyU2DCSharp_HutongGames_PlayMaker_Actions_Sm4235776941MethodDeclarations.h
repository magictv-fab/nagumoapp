﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SmoothLookAtDirection
struct SmoothLookAtDirection_t4235776941;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::.ctor()
extern "C"  void SmoothLookAtDirection__ctor_m2568480809 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::Reset()
extern "C"  void SmoothLookAtDirection_Reset_m214913750 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnEnter()
extern "C"  void SmoothLookAtDirection_OnEnter_m2390558144 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnUpdate()
extern "C"  void SmoothLookAtDirection_OnUpdate_m226417795 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnLateUpdate()
extern "C"  void SmoothLookAtDirection_OnLateUpdate_m4097353929 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::DoSmoothLookAtDirection()
extern "C"  void SmoothLookAtDirection_DoSmoothLookAtDirection_m1773377499 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
