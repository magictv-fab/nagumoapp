﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen2340485113MethodDeclarations.h"

// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3374633554(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2830030642 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m372701337_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::Invoke(T)
#define Action_1_Invoke_m3316214194(__this, ___obj0, method) ((  void (*) (Action_1_t2830030642 *, KeyValuePair_2_t2434214506 , const MethodInfo*))Action_1_Invoke_m2500958603_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m2822209151(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2830030642 *, KeyValuePair_2_t2434214506 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m192152288_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m793918818(__this, ___result0, method) ((  void (*) (Action_1_t2830030642 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m629710633_gshared)(__this, ___result0, method)
