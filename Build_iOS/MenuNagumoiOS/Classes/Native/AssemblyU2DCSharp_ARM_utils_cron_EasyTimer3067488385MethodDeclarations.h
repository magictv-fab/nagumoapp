﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.cron.EasyTimer
struct EasyTimer_t3067488385;
// ARM.animation.ViewTimeControllAbstract/OnFinishedEvent
struct OnFinishedEvent_t3844636377;
// System.String
struct String_t;
// ARM.utils.cron.TimeoutItem
struct TimeoutItem_t109981810;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb3844636377.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.utils.cron.EasyTimer::.ctor()
extern "C"  void EasyTimer__ctor_m333603497 (EasyTimer_t3067488385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.EasyTimer::.cctor()
extern "C"  void EasyTimer__cctor_m1269677604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.cron.EasyTimer::SetTimeout(ARM.animation.ViewTimeControllAbstract/OnFinishedEvent,System.Int32,System.String)
extern "C"  int32_t EasyTimer_SetTimeout_m1252793348 (Il2CppObject * __this /* static, unused */, OnFinishedEvent_t3844636377 * ___method0, int32_t ___delayInMilliseconds1, String_t* ___timerName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.cron.TimeoutItem ARM.utils.cron.EasyTimer::GetTimeoutItem(System.Int32)
extern "C"  TimeoutItem_t109981810 * EasyTimer_GetTimeoutItem_m3839836772 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.cron.EasyTimer::SetInterval(ARM.animation.ViewTimeControllAbstract/OnFinishedEvent,System.Int32,System.String)
extern "C"  int32_t EasyTimer_SetInterval_m3480456300 (Il2CppObject * __this /* static, unused */, OnFinishedEvent_t3844636377 * ___method0, int32_t ___delayInMilliseconds1, String_t* ___timerName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.EasyTimer::ClearInterval(System.Int32)
extern "C"  void EasyTimer_ClearInterval_m3985016650 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
