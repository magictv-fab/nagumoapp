﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmBool
struct GetFsmBool_t3246450044;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::.ctor()
extern "C"  void GetFsmBool__ctor_m1985960554 (GetFsmBool_t3246450044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::Reset()
extern "C"  void GetFsmBool_Reset_m3927360791 (GetFsmBool_t3246450044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::OnEnter()
extern "C"  void GetFsmBool_OnEnter_m934341569 (GetFsmBool_t3246450044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::OnUpdate()
extern "C"  void GetFsmBool_OnUpdate_m2328344226 (GetFsmBool_t3246450044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::DoGetFsmBool()
extern "C"  void GetFsmBool_DoGetFsmBool_m1450491865 (GetFsmBool_t3246450044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
