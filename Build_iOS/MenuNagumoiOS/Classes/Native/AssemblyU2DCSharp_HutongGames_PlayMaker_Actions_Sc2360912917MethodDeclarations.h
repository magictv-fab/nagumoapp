﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScreenPick
struct ScreenPick_t2360912917;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScreenPick::.ctor()
extern "C"  void ScreenPick__ctor_m644542449 (ScreenPick_t2360912917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::Reset()
extern "C"  void ScreenPick_Reset_m2585942686 (ScreenPick_t2360912917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::OnEnter()
extern "C"  void ScreenPick_OnEnter_m321731464 (ScreenPick_t2360912917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::OnUpdate()
extern "C"  void ScreenPick_OnUpdate_m517300155 (ScreenPick_t2360912917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::DoScreenPick()
extern "C"  void ScreenPick_DoScreenPick_m1195817163 (ScreenPick_t2360912917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
