﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3373024719.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3718966496_gshared (Enumerator_t3373024719 * __this, Dictionary_2_t2758088665 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3718966496(__this, ___host0, method) ((  void (*) (Enumerator_t3373024719 *, Dictionary_2_t2758088665 *, const MethodInfo*))Enumerator__ctor_m3718966496_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3790098059_gshared (Enumerator_t3373024719 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3790098059(__this, method) ((  Il2CppObject * (*) (Enumerator_t3373024719 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3790098059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3341834197_gshared (Enumerator_t3373024719 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3341834197(__this, method) ((  void (*) (Enumerator_t3373024719 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3341834197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3304172418_gshared (Enumerator_t3373024719 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3304172418(__this, method) ((  void (*) (Enumerator_t3373024719 *, const MethodInfo*))Enumerator_Dispose_m3304172418_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3439392261_gshared (Enumerator_t3373024719 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3439392261(__this, method) ((  bool (*) (Enumerator_t3373024719 *, const MethodInfo*))Enumerator_MoveNext_m3439392261_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3518312819_gshared (Enumerator_t3373024719 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3518312819(__this, method) ((  int32_t (*) (Enumerator_t3373024719 *, const MethodInfo*))Enumerator_get_Current_m3518312819_gshared)(__this, method)
