﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3749965048.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m860548575_gshared (Enumerator_t3749965048 * __this, Dictionary_2_t3135028994 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m860548575(__this, ___host0, method) ((  void (*) (Enumerator_t3749965048 *, Dictionary_2_t3135028994 *, const MethodInfo*))Enumerator__ctor_m860548575_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1381494188_gshared (Enumerator_t3749965048 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1381494188(__this, method) ((  Il2CppObject * (*) (Enumerator_t3749965048 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1381494188_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3289044342_gshared (Enumerator_t3749965048 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3289044342(__this, method) ((  void (*) (Enumerator_t3749965048 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3289044342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m196475457_gshared (Enumerator_t3749965048 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m196475457(__this, method) ((  void (*) (Enumerator_t3749965048 *, const MethodInfo*))Enumerator_Dispose_m196475457_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3201538470_gshared (Enumerator_t3749965048 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3201538470(__this, method) ((  bool (*) (Enumerator_t3749965048 *, const MethodInfo*))Enumerator_MoveNext_m3201538470_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2472851634_gshared (Enumerator_t3749965048 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2472851634(__this, method) ((  int32_t (*) (Enumerator_t3749965048 *, const MethodInfo*))Enumerator_get_Current_m2472851634_gshared)(__this, method)
