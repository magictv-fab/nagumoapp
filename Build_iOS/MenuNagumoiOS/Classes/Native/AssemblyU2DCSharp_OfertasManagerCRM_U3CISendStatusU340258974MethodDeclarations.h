﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM/<ISendStatus>c__Iterator4
struct U3CISendStatusU3Ec__Iterator4_t340258974;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM/<ISendStatus>c__Iterator4::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator4__ctor_m3110059149 (U3CISendStatusU3Ec__Iterator4_t340258974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<ISendStatus>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1109240933 (U3CISendStatusU3Ec__Iterator4_t340258974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<ISendStatus>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3711375865 (U3CISendStatusU3Ec__Iterator4_t340258974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM/<ISendStatus>c__Iterator4::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator4_MoveNext_m4115933255 (U3CISendStatusU3Ec__Iterator4_t340258974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<ISendStatus>c__Iterator4::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator4_Dispose_m3662854538 (U3CISendStatusU3Ec__Iterator4_t340258974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<ISendStatus>c__Iterator4::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator4_Reset_m756492090 (U3CISendStatusU3Ec__Iterator4_t340258974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
