﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Maxicode.Internal.BitMatrixParser
struct BitMatrixParser_t4050453827;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Void ZXing.Maxicode.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern "C"  void BitMatrixParser__ctor_m2105456360 (BitMatrixParser_t4050453827 * __this, BitMatrix_t1058711404 * ___bitMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.Maxicode.Internal.BitMatrixParser::readCodewords()
extern "C"  ByteU5BU5D_t4260760469* BitMatrixParser_readCodewords_m819656075 (BitMatrixParser_t4050453827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Maxicode.Internal.BitMatrixParser::.cctor()
extern "C"  void BitMatrixParser__cctor_m3245837392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
