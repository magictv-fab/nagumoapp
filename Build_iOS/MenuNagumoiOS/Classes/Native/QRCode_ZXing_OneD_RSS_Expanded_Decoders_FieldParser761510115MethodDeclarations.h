﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.FieldParser::.cctor()
extern "C"  void FieldParser__cctor_m3917553631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::parseFieldsInGeneralPurpose(System.String)
extern "C"  String_t* FieldParser_parseFieldsInGeneralPurpose_m943892786 (Il2CppObject * __this /* static, unused */, String_t* ___rawInformation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::processFixedAI(System.Int32,System.Int32,System.String)
extern "C"  String_t* FieldParser_processFixedAI_m3321484732 (Il2CppObject * __this /* static, unused */, int32_t ___aiSize0, int32_t ___fieldSize1, String_t* ___rawInformation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::processVariableAI(System.Int32,System.Int32,System.String)
extern "C"  String_t* FieldParser_processVariableAI_m3677729318 (Il2CppObject * __this /* static, unused */, int32_t ___aiSize0, int32_t ___variableFieldSize1, String_t* ___rawInformation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
