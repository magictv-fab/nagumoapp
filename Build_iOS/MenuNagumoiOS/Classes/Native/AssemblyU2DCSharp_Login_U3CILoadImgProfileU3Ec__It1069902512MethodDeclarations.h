﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login/<ILoadImgProfile>c__Iterator64
struct U3CILoadImgProfileU3Ec__Iterator64_t1069902512;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Login/<ILoadImgProfile>c__Iterator64::.ctor()
extern "C"  void U3CILoadImgProfileU3Ec__Iterator64__ctor_m3999319531 (U3CILoadImgProfileU3Ec__Iterator64_t1069902512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<ILoadImgProfile>c__Iterator64::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgProfileU3Ec__Iterator64_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2815929937 (U3CILoadImgProfileU3Ec__Iterator64_t1069902512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<ILoadImgProfile>c__Iterator64::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgProfileU3Ec__Iterator64_System_Collections_IEnumerator_get_Current_m506098661 (U3CILoadImgProfileU3Ec__Iterator64_t1069902512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Login/<ILoadImgProfile>c__Iterator64::MoveNext()
extern "C"  bool U3CILoadImgProfileU3Ec__Iterator64_MoveNext_m4230404625 (U3CILoadImgProfileU3Ec__Iterator64_t1069902512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<ILoadImgProfile>c__Iterator64::Dispose()
extern "C"  void U3CILoadImgProfileU3Ec__Iterator64_Dispose_m3543589736 (U3CILoadImgProfileU3Ec__Iterator64_t1069902512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<ILoadImgProfile>c__Iterator64::Reset()
extern "C"  void U3CILoadImgProfileU3Ec__Iterator64_Reset_m1645752472 (U3CILoadImgProfileU3Ec__Iterator64_t1069902512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
