﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsManager
struct  NotificationsManager_t3392303557  : public MonoBehaviour_t667441552
{
public:
	// System.String NotificationsManager::oneSignalKey
	String_t* ___oneSignalKey_3;

public:
	inline static int32_t get_offset_of_oneSignalKey_3() { return static_cast<int32_t>(offsetof(NotificationsManager_t3392303557, ___oneSignalKey_3)); }
	inline String_t* get_oneSignalKey_3() const { return ___oneSignalKey_3; }
	inline String_t** get_address_of_oneSignalKey_3() { return &___oneSignalKey_3; }
	inline void set_oneSignalKey_3(String_t* value)
	{
		___oneSignalKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___oneSignalKey_3, value);
	}
};

struct NotificationsManager_t3392303557_StaticFields
{
public:
	// System.String NotificationsManager::oneSignalDeviceKey
	String_t* ___oneSignalDeviceKey_2;
	// System.String NotificationsManager::extraMessage
	String_t* ___extraMessage_4;

public:
	inline static int32_t get_offset_of_oneSignalDeviceKey_2() { return static_cast<int32_t>(offsetof(NotificationsManager_t3392303557_StaticFields, ___oneSignalDeviceKey_2)); }
	inline String_t* get_oneSignalDeviceKey_2() const { return ___oneSignalDeviceKey_2; }
	inline String_t** get_address_of_oneSignalDeviceKey_2() { return &___oneSignalDeviceKey_2; }
	inline void set_oneSignalDeviceKey_2(String_t* value)
	{
		___oneSignalDeviceKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___oneSignalDeviceKey_2, value);
	}

	inline static int32_t get_offset_of_extraMessage_4() { return static_cast<int32_t>(offsetof(NotificationsManager_t3392303557_StaticFields, ___extraMessage_4)); }
	inline String_t* get_extraMessage_4() const { return ___extraMessage_4; }
	inline String_t** get_address_of_extraMessage_4() { return &___extraMessage_4; }
	inline void set_extraMessage_4(String_t* value)
	{
		___extraMessage_4 = value;
		Il2CppCodeGenWriteBarrier(&___extraMessage_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
