﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JC_Slider
struct JC_Slider_t1089222343;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t3355659985;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"

// System.Void JC_Slider::.ctor()
extern "C"  void JC_Slider__ctor_m3902967428 (JC_Slider_t1089222343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JC_Slider::OnMove(UnityEngine.EventSystems.AxisEventData)
extern "C"  void JC_Slider_OnMove_m1278195706 (JC_Slider_t1089222343 * __this, AxisEventData_t3355659985 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
