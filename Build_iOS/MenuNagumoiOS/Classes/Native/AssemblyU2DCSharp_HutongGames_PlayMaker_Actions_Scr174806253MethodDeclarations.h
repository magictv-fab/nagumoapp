﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScreenToWorldPoint
struct ScreenToWorldPoint_t174806253;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::.ctor()
extern "C"  void ScreenToWorldPoint__ctor_m1969299993 (ScreenToWorldPoint_t174806253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::Reset()
extern "C"  void ScreenToWorldPoint_Reset_m3910700230 (ScreenToWorldPoint_t174806253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::OnEnter()
extern "C"  void ScreenToWorldPoint_OnEnter_m2103411632 (ScreenToWorldPoint_t174806253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::OnUpdate()
extern "C"  void ScreenToWorldPoint_OnUpdate_m4209777811 (ScreenToWorldPoint_t174806253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::DoScreenToWorldPoint()
extern "C"  void ScreenToWorldPoint_DoScreenToWorldPoint_m2784388219 (ScreenToWorldPoint_t174806253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
