﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler
struct OnVoidEventHandler_t2512456021;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.ScreenEvents/OnVoidEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVoidEventHandler__ctor_m3301966204 (OnVoidEventHandler_t2512456021 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnVoidEventHandler::Invoke()
extern "C"  void OnVoidEventHandler_Invoke_m670666966 (OnVoidEventHandler_t2512456021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.ScreenEvents/OnVoidEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVoidEventHandler_BeginInvoke_m66677357 (OnVoidEventHandler_t2512456021 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnVoidEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnVoidEventHandler_EndInvoke_m595038604 (OnVoidEventHandler_t2512456021 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
