﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GetInfoMenu
struct GetInfoMenu_t3923496067;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetInfoMenu
struct  GetInfoMenu_t3923496067  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image GetInfoMenu::fotoImg
	Image_t538875265 * ___fotoImg_3;
	// UnityEngine.UI.Text GetInfoMenu::nomeTxt
	Text_t9039225 * ___nomeTxt_4;
	// UnityEngine.UI.Text GetInfoMenu::girosTxt
	Text_t9039225 * ___girosTxt_5;
	// UnityEngine.UI.Text GetInfoMenu::pontosTxt
	Text_t9039225 * ___pontosTxt_6;
	// UnityEngine.UI.Text GetInfoMenu::cuponsTxt
	Text_t9039225 * ___cuponsTxt_7;

public:
	inline static int32_t get_offset_of_fotoImg_3() { return static_cast<int32_t>(offsetof(GetInfoMenu_t3923496067, ___fotoImg_3)); }
	inline Image_t538875265 * get_fotoImg_3() const { return ___fotoImg_3; }
	inline Image_t538875265 ** get_address_of_fotoImg_3() { return &___fotoImg_3; }
	inline void set_fotoImg_3(Image_t538875265 * value)
	{
		___fotoImg_3 = value;
		Il2CppCodeGenWriteBarrier(&___fotoImg_3, value);
	}

	inline static int32_t get_offset_of_nomeTxt_4() { return static_cast<int32_t>(offsetof(GetInfoMenu_t3923496067, ___nomeTxt_4)); }
	inline Text_t9039225 * get_nomeTxt_4() const { return ___nomeTxt_4; }
	inline Text_t9039225 ** get_address_of_nomeTxt_4() { return &___nomeTxt_4; }
	inline void set_nomeTxt_4(Text_t9039225 * value)
	{
		___nomeTxt_4 = value;
		Il2CppCodeGenWriteBarrier(&___nomeTxt_4, value);
	}

	inline static int32_t get_offset_of_girosTxt_5() { return static_cast<int32_t>(offsetof(GetInfoMenu_t3923496067, ___girosTxt_5)); }
	inline Text_t9039225 * get_girosTxt_5() const { return ___girosTxt_5; }
	inline Text_t9039225 ** get_address_of_girosTxt_5() { return &___girosTxt_5; }
	inline void set_girosTxt_5(Text_t9039225 * value)
	{
		___girosTxt_5 = value;
		Il2CppCodeGenWriteBarrier(&___girosTxt_5, value);
	}

	inline static int32_t get_offset_of_pontosTxt_6() { return static_cast<int32_t>(offsetof(GetInfoMenu_t3923496067, ___pontosTxt_6)); }
	inline Text_t9039225 * get_pontosTxt_6() const { return ___pontosTxt_6; }
	inline Text_t9039225 ** get_address_of_pontosTxt_6() { return &___pontosTxt_6; }
	inline void set_pontosTxt_6(Text_t9039225 * value)
	{
		___pontosTxt_6 = value;
		Il2CppCodeGenWriteBarrier(&___pontosTxt_6, value);
	}

	inline static int32_t get_offset_of_cuponsTxt_7() { return static_cast<int32_t>(offsetof(GetInfoMenu_t3923496067, ___cuponsTxt_7)); }
	inline Text_t9039225 * get_cuponsTxt_7() const { return ___cuponsTxt_7; }
	inline Text_t9039225 ** get_address_of_cuponsTxt_7() { return &___cuponsTxt_7; }
	inline void set_cuponsTxt_7(Text_t9039225 * value)
	{
		___cuponsTxt_7 = value;
		Il2CppCodeGenWriteBarrier(&___cuponsTxt_7, value);
	}
};

struct GetInfoMenu_t3923496067_StaticFields
{
public:
	// GetInfoMenu GetInfoMenu::instance
	GetInfoMenu_t3923496067 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GetInfoMenu_t3923496067_StaticFields, ___instance_2)); }
	inline GetInfoMenu_t3923496067 * get_instance_2() const { return ___instance_2; }
	inline GetInfoMenu_t3923496067 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GetInfoMenu_t3923496067 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
