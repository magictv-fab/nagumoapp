﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1439212047.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m849499697_gshared (InternalEnumerator_1_t1439212047 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m849499697(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1439212047 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m849499697_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m376055759_gshared (InternalEnumerator_1_t1439212047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m376055759(__this, method) ((  void (*) (InternalEnumerator_1_t1439212047 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m376055759_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m280736645_gshared (InternalEnumerator_1_t1439212047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m280736645(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1439212047 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m280736645_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3158030664_gshared (InternalEnumerator_1_t1439212047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3158030664(__this, method) ((  void (*) (InternalEnumerator_1_t1439212047 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3158030664_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m739732991_gshared (InternalEnumerator_1_t1439212047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m739732991(__this, method) ((  bool (*) (InternalEnumerator_1_t1439212047 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m739732991_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2656869371  InternalEnumerator_1_get_Current_m4016947226_gshared (InternalEnumerator_1_t1439212047 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4016947226(__this, method) ((  KeyValuePair_2_t2656869371  (*) (InternalEnumerator_1_t1439212047 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4016947226_gshared)(__this, method)
