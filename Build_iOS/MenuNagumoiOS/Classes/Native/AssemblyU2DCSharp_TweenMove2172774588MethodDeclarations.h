﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenMove
struct TweenMove_t2172774588;

#include "codegen/il2cpp-codegen.h"

// System.Void TweenMove::.ctor()
extern "C"  void TweenMove__ctor_m288178223 (TweenMove_t2172774588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenMove::Start()
extern "C"  void TweenMove_Start_m3530283311 (TweenMove_t2172774588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenMove::SlideIn()
extern "C"  void TweenMove_SlideIn_m911969155 (TweenMove_t2172774588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenMove::SlideOut()
extern "C"  void TweenMove_SlideOut_m2507062738 (TweenMove_t2172774588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenMove::OnEnable()
extern "C"  void TweenMove_OnEnable_m1549395191 (TweenMove_t2172774588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenMove::Update()
extern "C"  void TweenMove_Update_m2070452414 (TweenMove_t2172774588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
