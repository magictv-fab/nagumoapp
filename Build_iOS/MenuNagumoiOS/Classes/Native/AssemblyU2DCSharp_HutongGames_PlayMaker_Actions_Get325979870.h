﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTransform
struct  GetTransform_t325979870  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetTransform::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetTransform::storeTransform
	FsmObject_t821476169 * ___storeTransform_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTransform::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetTransform_t325979870, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_storeTransform_10() { return static_cast<int32_t>(offsetof(GetTransform_t325979870, ___storeTransform_10)); }
	inline FsmObject_t821476169 * get_storeTransform_10() const { return ___storeTransform_10; }
	inline FsmObject_t821476169 ** get_address_of_storeTransform_10() { return &___storeTransform_10; }
	inline void set_storeTransform_10(FsmObject_t821476169 * value)
	{
		___storeTransform_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeTransform_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetTransform_t325979870, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
