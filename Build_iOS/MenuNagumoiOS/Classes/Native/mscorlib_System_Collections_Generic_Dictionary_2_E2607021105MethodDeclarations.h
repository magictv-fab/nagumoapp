﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2607021105.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21188478419.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1649207152_gshared (Enumerator_t2607021105 * __this, Dictionary_2_t1289697713 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1649207152(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2607021105 *, Dictionary_2_t1289697713 *, const MethodInfo*))Enumerator__ctor_m1649207152_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m437284081_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m437284081(__this, method) ((  Il2CppObject * (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m437284081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m837083973_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m837083973(__this, method) ((  void (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m837083973_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1295832654_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1295832654(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1295832654_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m157053197_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m157053197(__this, method) ((  Il2CppObject * (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m157053197_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1652703327_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1652703327(__this, method) ((  Il2CppObject * (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1652703327_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2959870769_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2959870769(__this, method) ((  bool (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_MoveNext_m2959870769_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1188478419  Enumerator_get_Current_m1243001311_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1243001311(__this, method) ((  KeyValuePair_2_t1188478419  (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_get_Current_m1243001311_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m430159038_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m430159038(__this, method) ((  int32_t (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_get_CurrentKey_m430159038_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1317751906_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1317751906(__this, method) ((  Il2CppObject * (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_get_CurrentValue_m1317751906_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3150272962_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3150272962(__this, method) ((  void (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_Reset_m3150272962_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1730859083_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1730859083(__this, method) ((  void (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_VerifyState_m1730859083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3948106995_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3948106995(__this, method) ((  void (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_VerifyCurrent_m3948106995_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1983801874_gshared (Enumerator_t2607021105 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1983801874(__this, method) ((  void (*) (Enumerator_t2607021105 *, const MethodInfo*))Enumerator_Dispose_m1983801874_gshared)(__this, method)
