﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.EC.ModulusGF
struct ModulusGF_t2738856732;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.PDF417.Internal.EC.ModulusPoly
struct ModulusPoly_t3133325097;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.EC.ModulusGF
struct  ModulusGF_t2738856732  : public Il2CppObject
{
public:
	// System.Int32[] ZXing.PDF417.Internal.EC.ModulusGF::expTable
	Int32U5BU5D_t3230847821* ___expTable_1;
	// System.Int32[] ZXing.PDF417.Internal.EC.ModulusGF::logTable
	Int32U5BU5D_t3230847821* ___logTable_2;
	// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::modulus
	int32_t ___modulus_3;
	// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::<Zero>k__BackingField
	ModulusPoly_t3133325097 * ___U3CZeroU3Ek__BackingField_4;
	// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::<One>k__BackingField
	ModulusPoly_t3133325097 * ___U3COneU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_expTable_1() { return static_cast<int32_t>(offsetof(ModulusGF_t2738856732, ___expTable_1)); }
	inline Int32U5BU5D_t3230847821* get_expTable_1() const { return ___expTable_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_expTable_1() { return &___expTable_1; }
	inline void set_expTable_1(Int32U5BU5D_t3230847821* value)
	{
		___expTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___expTable_1, value);
	}

	inline static int32_t get_offset_of_logTable_2() { return static_cast<int32_t>(offsetof(ModulusGF_t2738856732, ___logTable_2)); }
	inline Int32U5BU5D_t3230847821* get_logTable_2() const { return ___logTable_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_logTable_2() { return &___logTable_2; }
	inline void set_logTable_2(Int32U5BU5D_t3230847821* value)
	{
		___logTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___logTable_2, value);
	}

	inline static int32_t get_offset_of_modulus_3() { return static_cast<int32_t>(offsetof(ModulusGF_t2738856732, ___modulus_3)); }
	inline int32_t get_modulus_3() const { return ___modulus_3; }
	inline int32_t* get_address_of_modulus_3() { return &___modulus_3; }
	inline void set_modulus_3(int32_t value)
	{
		___modulus_3 = value;
	}

	inline static int32_t get_offset_of_U3CZeroU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ModulusGF_t2738856732, ___U3CZeroU3Ek__BackingField_4)); }
	inline ModulusPoly_t3133325097 * get_U3CZeroU3Ek__BackingField_4() const { return ___U3CZeroU3Ek__BackingField_4; }
	inline ModulusPoly_t3133325097 ** get_address_of_U3CZeroU3Ek__BackingField_4() { return &___U3CZeroU3Ek__BackingField_4; }
	inline void set_U3CZeroU3Ek__BackingField_4(ModulusPoly_t3133325097 * value)
	{
		___U3CZeroU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CZeroU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3COneU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ModulusGF_t2738856732, ___U3COneU3Ek__BackingField_5)); }
	inline ModulusPoly_t3133325097 * get_U3COneU3Ek__BackingField_5() const { return ___U3COneU3Ek__BackingField_5; }
	inline ModulusPoly_t3133325097 ** get_address_of_U3COneU3Ek__BackingField_5() { return &___U3COneU3Ek__BackingField_5; }
	inline void set_U3COneU3Ek__BackingField_5(ModulusPoly_t3133325097 * value)
	{
		___U3COneU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3COneU3Ek__BackingField_5, value);
	}
};

struct ModulusGF_t2738856732_StaticFields
{
public:
	// ZXing.PDF417.Internal.EC.ModulusGF ZXing.PDF417.Internal.EC.ModulusGF::PDF417_GF
	ModulusGF_t2738856732 * ___PDF417_GF_0;

public:
	inline static int32_t get_offset_of_PDF417_GF_0() { return static_cast<int32_t>(offsetof(ModulusGF_t2738856732_StaticFields, ___PDF417_GF_0)); }
	inline ModulusGF_t2738856732 * get_PDF417_GF_0() const { return ___PDF417_GF_0; }
	inline ModulusGF_t2738856732 ** get_address_of_PDF417_GF_0() { return &___PDF417_GF_0; }
	inline void set_PDF417_GF_0(ModulusGF_t2738856732 * value)
	{
		___PDF417_GF_0 = value;
		Il2CppCodeGenWriteBarrier(&___PDF417_GF_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
