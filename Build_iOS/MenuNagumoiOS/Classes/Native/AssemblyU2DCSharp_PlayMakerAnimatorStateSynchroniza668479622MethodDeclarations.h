﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorStateSynchronization
struct PlayMakerAnimatorStateSynchronization_t668479622;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"

// System.Void PlayMakerAnimatorStateSynchronization::.ctor()
extern "C"  void PlayMakerAnimatorStateSynchronization__ctor_m88551333 (PlayMakerAnimatorStateSynchronization_t668479622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorStateSynchronization::Start()
extern "C"  void PlayMakerAnimatorStateSynchronization_Start_m3330656421 (PlayMakerAnimatorStateSynchronization_t668479622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorStateSynchronization::RegisterHash(System.String,HutongGames.PlayMaker.FsmState)
extern "C"  void PlayMakerAnimatorStateSynchronization_RegisterHash_m1704656247 (PlayMakerAnimatorStateSynchronization_t668479622 * __this, String_t* ___key0, FsmState_t2146334067 * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorStateSynchronization::Update()
extern "C"  void PlayMakerAnimatorStateSynchronization_Update_m176986120 (PlayMakerAnimatorStateSynchronization_t668479622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorStateSynchronization::Synchronize()
extern "C"  void PlayMakerAnimatorStateSynchronization_Synchronize_m3083291795 (PlayMakerAnimatorStateSynchronization_t668479622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorStateSynchronization::SwitchState(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmState)
extern "C"  void PlayMakerAnimatorStateSynchronization_SwitchState_m1419615275 (PlayMakerAnimatorStateSynchronization_t668479622 * __this, Fsm_t1527112426 * ___fsm0, FsmState_t2146334067 * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
