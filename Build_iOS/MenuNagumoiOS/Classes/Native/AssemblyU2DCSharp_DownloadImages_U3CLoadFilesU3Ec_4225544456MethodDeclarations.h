﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImages/<LoadFiles>c__Iterator1B
struct U3CLoadFilesU3Ec__Iterator1B_t4225544456;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DownloadImages/<LoadFiles>c__Iterator1B::.ctor()
extern "C"  void U3CLoadFilesU3Ec__Iterator1B__ctor_m783910755 (U3CLoadFilesU3Ec__Iterator1B_t4225544456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImages/<LoadFiles>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadFilesU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4293535951 (U3CLoadFilesU3Ec__Iterator1B_t4225544456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImages/<LoadFiles>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadFilesU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m2479729251 (U3CLoadFilesU3Ec__Iterator1B_t4225544456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DownloadImages/<LoadFiles>c__Iterator1B::MoveNext()
extern "C"  bool U3CLoadFilesU3Ec__Iterator1B_MoveNext_m1638050737 (U3CLoadFilesU3Ec__Iterator1B_t4225544456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/<LoadFiles>c__Iterator1B::Dispose()
extern "C"  void U3CLoadFilesU3Ec__Iterator1B_Dispose_m1617241824 (U3CLoadFilesU3Ec__Iterator1B_t4225544456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/<LoadFiles>c__Iterator1B::Reset()
extern "C"  void U3CLoadFilesU3Ec__Iterator1B_Reset_m2725310992 (U3CLoadFilesU3Ec__Iterator1B_t4225544456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
