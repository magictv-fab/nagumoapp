﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicApplicationSettings/<ResetHard>c__AnonStorey96
struct U3CResetHardU3Ec__AnonStorey96_t4115973581;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicApplicationSettings/<ResetHard>c__AnonStorey96::.ctor()
extern "C"  void U3CResetHardU3Ec__AnonStorey96__ctor_m1493230654 (U3CResetHardU3Ec__AnonStorey96_t4115973581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings/<ResetHard>c__AnonStorey96::<>m__27(System.String)
extern "C"  bool U3CResetHardU3Ec__AnonStorey96_U3CU3Em__27_m1446107842 (U3CResetHardU3Ec__AnonStorey96_t4115973581 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
