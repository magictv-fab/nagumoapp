﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackgroudCam/<openCamera>c__Iterator46
struct U3CopenCameraU3Ec__Iterator46_t2801674036;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BackgroudCam/<openCamera>c__Iterator46::.ctor()
extern "C"  void U3CopenCameraU3Ec__Iterator46__ctor_m872003559 (U3CopenCameraU3Ec__Iterator46_t2801674036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BackgroudCam/<openCamera>c__Iterator46::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CopenCameraU3Ec__Iterator46_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3528456341 (U3CopenCameraU3Ec__Iterator46_t2801674036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BackgroudCam/<openCamera>c__Iterator46::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CopenCameraU3Ec__Iterator46_System_Collections_IEnumerator_get_Current_m2355611177 (U3CopenCameraU3Ec__Iterator46_t2801674036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BackgroudCam/<openCamera>c__Iterator46::MoveNext()
extern "C"  bool U3CopenCameraU3Ec__Iterator46_MoveNext_m2933651861 (U3CopenCameraU3Ec__Iterator46_t2801674036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam/<openCamera>c__Iterator46::Dispose()
extern "C"  void U3CopenCameraU3Ec__Iterator46_Dispose_m375080548 (U3CopenCameraU3Ec__Iterator46_t2801674036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam/<openCamera>c__Iterator46::Reset()
extern "C"  void U3CopenCameraU3Ec__Iterator46_Reset_m2813403796 (U3CopenCameraU3Ec__Iterator46_t2801674036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
