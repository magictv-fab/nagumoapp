﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.Detector.WhiteRectangleDetector
struct  WhiteRectangleDetector_t453655296  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.Detector.WhiteRectangleDetector::image
	BitMatrix_t1058711404 * ___image_0;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::height
	int32_t ___height_1;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::width
	int32_t ___width_2;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::leftInit
	int32_t ___leftInit_3;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::rightInit
	int32_t ___rightInit_4;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::downInit
	int32_t ___downInit_5;
	// System.Int32 ZXing.Common.Detector.WhiteRectangleDetector::upInit
	int32_t ___upInit_6;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___image_0)); }
	inline BitMatrix_t1058711404 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t1058711404 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t1058711404 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_leftInit_3() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___leftInit_3)); }
	inline int32_t get_leftInit_3() const { return ___leftInit_3; }
	inline int32_t* get_address_of_leftInit_3() { return &___leftInit_3; }
	inline void set_leftInit_3(int32_t value)
	{
		___leftInit_3 = value;
	}

	inline static int32_t get_offset_of_rightInit_4() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___rightInit_4)); }
	inline int32_t get_rightInit_4() const { return ___rightInit_4; }
	inline int32_t* get_address_of_rightInit_4() { return &___rightInit_4; }
	inline void set_rightInit_4(int32_t value)
	{
		___rightInit_4 = value;
	}

	inline static int32_t get_offset_of_downInit_5() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___downInit_5)); }
	inline int32_t get_downInit_5() const { return ___downInit_5; }
	inline int32_t* get_address_of_downInit_5() { return &___downInit_5; }
	inline void set_downInit_5(int32_t value)
	{
		___downInit_5 = value;
	}

	inline static int32_t get_offset_of_upInit_6() { return static_cast<int32_t>(offsetof(WhiteRectangleDetector_t453655296, ___upInit_6)); }
	inline int32_t get_upInit_6() const { return ___upInit_6; }
	inline int32_t* get_address_of_upInit_6() { return &___upInit_6; }
	inline void set_upInit_6(int32_t value)
	{
		___upInit_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
