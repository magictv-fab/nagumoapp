﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TiltWindow
struct TiltWindow_t1575826733;

#include "codegen/il2cpp-codegen.h"

// System.Void TiltWindow::.ctor()
extern "C"  void TiltWindow__ctor_m3184715278 (TiltWindow_t1575826733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiltWindow::Start()
extern "C"  void TiltWindow_Start_m2131853070 (TiltWindow_t1575826733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiltWindow::Update()
extern "C"  void TiltWindow_Update_m1668787903 (TiltWindow_t1575826733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
