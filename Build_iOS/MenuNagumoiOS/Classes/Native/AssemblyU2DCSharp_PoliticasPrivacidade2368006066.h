﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PoliticasPrivacidade
struct  PoliticasPrivacidade_t2368006066  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PoliticasPrivacidade::politicas
	List_1_t747900261 * ___politicas_2;

public:
	inline static int32_t get_offset_of_politicas_2() { return static_cast<int32_t>(offsetof(PoliticasPrivacidade_t2368006066, ___politicas_2)); }
	inline List_1_t747900261 * get_politicas_2() const { return ___politicas_2; }
	inline List_1_t747900261 ** get_address_of_politicas_2() { return &___politicas_2; }
	inline void set_politicas_2(List_1_t747900261 * value)
	{
		___politicas_2 = value;
		Il2CppCodeGenWriteBarrier(&___politicas_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
