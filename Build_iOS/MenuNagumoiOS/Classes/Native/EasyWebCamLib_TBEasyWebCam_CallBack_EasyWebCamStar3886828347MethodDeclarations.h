﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBEasyWebCam.CallBack.EasyWebCamStartedDelegate
struct EasyWebCamStartedDelegate_t3886828347;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void EasyWebCamStartedDelegate__ctor_m2221384916 (EasyWebCamStartedDelegate_t3886828347 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::Invoke()
extern "C"  void EasyWebCamStartedDelegate_Invoke_m3553276462 (EasyWebCamStartedDelegate_t3886828347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EasyWebCamStartedDelegate_BeginInvoke_m2203446173 (EasyWebCamStartedDelegate_t3886828347 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void EasyWebCamStartedDelegate_EndInvoke_m2455150820 (EasyWebCamStartedDelegate_t3886828347 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
