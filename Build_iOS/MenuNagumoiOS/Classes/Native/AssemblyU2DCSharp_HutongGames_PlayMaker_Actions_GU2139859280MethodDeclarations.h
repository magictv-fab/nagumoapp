﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal
struct GUILayoutBeginHorizontal_t2139859280;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::.ctor()
extern "C"  void GUILayoutBeginHorizontal__ctor_m2092243734 (GUILayoutBeginHorizontal_t2139859280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::Reset()
extern "C"  void GUILayoutBeginHorizontal_Reset_m4033643971 (GUILayoutBeginHorizontal_t2139859280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::OnGUI()
extern "C"  void GUILayoutBeginHorizontal_OnGUI_m1587642384 (GUILayoutBeginHorizontal_t2139859280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
