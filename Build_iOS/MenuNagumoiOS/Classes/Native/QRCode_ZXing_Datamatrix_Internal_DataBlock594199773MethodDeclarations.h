﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.DataBlock
struct DataBlock_t594199773;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.Datamatrix.Internal.DataBlock[]
struct DataBlockU5BU5D_t1220297488;
// ZXing.Datamatrix.Internal.Version
struct Version_t2313761970;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version2313761970.h"

// System.Void ZXing.Datamatrix.Internal.DataBlock::.ctor(System.Int32,System.Byte[])
extern "C"  void DataBlock__ctor_m773218420 (DataBlock_t594199773 * __this, int32_t ___numDataCodewords0, ByteU5BU5D_t4260760469* ___codewords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.DataBlock[] ZXing.Datamatrix.Internal.DataBlock::getDataBlocks(System.Byte[],ZXing.Datamatrix.Internal.Version)
extern "C"  DataBlockU5BU5D_t1220297488* DataBlock_getDataBlocks_m3956138411 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___rawCodewords0, Version_t2313761970 * ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.DataBlock::get_NumDataCodewords()
extern "C"  int32_t DataBlock_get_NumDataCodewords_m4049927755 (DataBlock_t594199773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.Datamatrix.Internal.DataBlock::get_Codewords()
extern "C"  ByteU5BU5D_t4260760469* DataBlock_get_Codewords_m3441448027 (DataBlock_t594199773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
