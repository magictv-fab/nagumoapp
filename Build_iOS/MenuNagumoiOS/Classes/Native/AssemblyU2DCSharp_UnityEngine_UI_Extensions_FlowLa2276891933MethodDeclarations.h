﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.FlowLayoutGroup
struct FlowLayoutGroup_t2276891933;
// System.Collections.Generic.IList`1<UnityEngine.RectTransform>
struct IList_1_t3667291137;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.FlowLayoutGroup::.ctor()
extern "C"  void FlowLayoutGroup__ctor_m3046057979 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.FlowLayoutGroup::CalculateLayoutInputHorizontal()
extern "C"  void FlowLayoutGroup_CalculateLayoutInputHorizontal_m2618410919 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.FlowLayoutGroup::SetLayoutHorizontal()
extern "C"  void FlowLayoutGroup_SetLayoutHorizontal_m16145033 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.FlowLayoutGroup::SetLayoutVertical()
extern "C"  void FlowLayoutGroup_SetLayoutVertical_m1235808859 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.FlowLayoutGroup::CalculateLayoutInputVertical()
extern "C"  void FlowLayoutGroup_CalculateLayoutInputVertical_m3495497465 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::get_IsCenterAlign()
extern "C"  bool FlowLayoutGroup_get_IsCenterAlign_m4027412618 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::get_IsRightAlign()
extern "C"  bool FlowLayoutGroup_get_IsRightAlign_m2103197201 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::get_IsMiddleAlign()
extern "C"  bool FlowLayoutGroup_get_IsMiddleAlign_m299629386 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.FlowLayoutGroup::get_IsLowerAlign()
extern "C"  bool FlowLayoutGroup_get_IsLowerAlign_m2224421260 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Extensions.FlowLayoutGroup::SetLayout(System.Single,System.Int32,System.Boolean)
extern "C"  float FlowLayoutGroup_SetLayout_m1901261824 (FlowLayoutGroup_t2276891933 * __this, float ___width0, int32_t ___axis1, bool ___layoutInput2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Extensions.FlowLayoutGroup::CalculateRowVerticalOffset(System.Single,System.Single,System.Single)
extern "C"  float FlowLayoutGroup_CalculateRowVerticalOffset_m3613417915 (FlowLayoutGroup_t2276891933 * __this, float ___groupHeight0, float ___yOffset1, float ___currentRowHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.FlowLayoutGroup::LayoutRow(System.Collections.Generic.IList`1<UnityEngine.RectTransform>,System.Single,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void FlowLayoutGroup_LayoutRow_m298994453 (FlowLayoutGroup_t2276891933 * __this, Il2CppObject* ___contents0, float ___rowWidth1, float ___rowHeight2, float ___maxWidth3, float ___xOffset4, float ___yOffset5, int32_t ___axis6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Extensions.FlowLayoutGroup::GetGreatestMinimumChildWidth()
extern "C"  float FlowLayoutGroup_GetGreatestMinimumChildWidth_m2903820166 (FlowLayoutGroup_t2276891933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
