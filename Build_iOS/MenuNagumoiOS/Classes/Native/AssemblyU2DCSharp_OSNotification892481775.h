﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OSNotificationPayload
struct OSNotificationPayload_t1451568543;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_OSNotification_DisplayType1794322908.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSNotification
struct  OSNotification_t892481775  : public Il2CppObject
{
public:
	// System.Boolean OSNotification::isAppInFocus
	bool ___isAppInFocus_0;
	// System.Boolean OSNotification::shown
	bool ___shown_1;
	// System.Boolean OSNotification::silentNotification
	bool ___silentNotification_2;
	// System.Int32 OSNotification::androidNotificationId
	int32_t ___androidNotificationId_3;
	// OSNotification/DisplayType OSNotification::displayType
	int32_t ___displayType_4;
	// OSNotificationPayload OSNotification::payload
	OSNotificationPayload_t1451568543 * ___payload_5;

public:
	inline static int32_t get_offset_of_isAppInFocus_0() { return static_cast<int32_t>(offsetof(OSNotification_t892481775, ___isAppInFocus_0)); }
	inline bool get_isAppInFocus_0() const { return ___isAppInFocus_0; }
	inline bool* get_address_of_isAppInFocus_0() { return &___isAppInFocus_0; }
	inline void set_isAppInFocus_0(bool value)
	{
		___isAppInFocus_0 = value;
	}

	inline static int32_t get_offset_of_shown_1() { return static_cast<int32_t>(offsetof(OSNotification_t892481775, ___shown_1)); }
	inline bool get_shown_1() const { return ___shown_1; }
	inline bool* get_address_of_shown_1() { return &___shown_1; }
	inline void set_shown_1(bool value)
	{
		___shown_1 = value;
	}

	inline static int32_t get_offset_of_silentNotification_2() { return static_cast<int32_t>(offsetof(OSNotification_t892481775, ___silentNotification_2)); }
	inline bool get_silentNotification_2() const { return ___silentNotification_2; }
	inline bool* get_address_of_silentNotification_2() { return &___silentNotification_2; }
	inline void set_silentNotification_2(bool value)
	{
		___silentNotification_2 = value;
	}

	inline static int32_t get_offset_of_androidNotificationId_3() { return static_cast<int32_t>(offsetof(OSNotification_t892481775, ___androidNotificationId_3)); }
	inline int32_t get_androidNotificationId_3() const { return ___androidNotificationId_3; }
	inline int32_t* get_address_of_androidNotificationId_3() { return &___androidNotificationId_3; }
	inline void set_androidNotificationId_3(int32_t value)
	{
		___androidNotificationId_3 = value;
	}

	inline static int32_t get_offset_of_displayType_4() { return static_cast<int32_t>(offsetof(OSNotification_t892481775, ___displayType_4)); }
	inline int32_t get_displayType_4() const { return ___displayType_4; }
	inline int32_t* get_address_of_displayType_4() { return &___displayType_4; }
	inline void set_displayType_4(int32_t value)
	{
		___displayType_4 = value;
	}

	inline static int32_t get_offset_of_payload_5() { return static_cast<int32_t>(offsetof(OSNotification_t892481775, ___payload_5)); }
	inline OSNotificationPayload_t1451568543 * get_payload_5() const { return ___payload_5; }
	inline OSNotificationPayload_t1451568543 ** get_address_of_payload_5() { return &___payload_5; }
	inline void set_payload_5(OSNotificationPayload_t1451568543 * value)
	{
		___payload_5 = value;
		Il2CppCodeGenWriteBarrier(&___payload_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
