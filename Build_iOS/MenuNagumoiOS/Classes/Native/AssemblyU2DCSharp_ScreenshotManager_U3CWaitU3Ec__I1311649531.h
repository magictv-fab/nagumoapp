﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManager/<Wait>c__Iterator32
struct  U3CWaitU3Ec__Iterator32_t1311649531  : public Il2CppObject
{
public:
	// System.Single ScreenshotManager/<Wait>c__Iterator32::delay
	float ___delay_0;
	// System.Single ScreenshotManager/<Wait>c__Iterator32::<pauseTarget>__0
	float ___U3CpauseTargetU3E__0_1;
	// System.Int32 ScreenshotManager/<Wait>c__Iterator32::$PC
	int32_t ___U24PC_2;
	// System.Object ScreenshotManager/<Wait>c__Iterator32::$current
	Il2CppObject * ___U24current_3;
	// System.Single ScreenshotManager/<Wait>c__Iterator32::<$>delay
	float ___U3CU24U3Edelay_4;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator32_t1311649531, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U3CpauseTargetU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator32_t1311649531, ___U3CpauseTargetU3E__0_1)); }
	inline float get_U3CpauseTargetU3E__0_1() const { return ___U3CpauseTargetU3E__0_1; }
	inline float* get_address_of_U3CpauseTargetU3E__0_1() { return &___U3CpauseTargetU3E__0_1; }
	inline void set_U3CpauseTargetU3E__0_1(float value)
	{
		___U3CpauseTargetU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator32_t1311649531, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator32_t1311649531, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edelay_4() { return static_cast<int32_t>(offsetof(U3CWaitU3Ec__Iterator32_t1311649531, ___U3CU24U3Edelay_4)); }
	inline float get_U3CU24U3Edelay_4() const { return ___U3CU24U3Edelay_4; }
	inline float* get_address_of_U3CU24U3Edelay_4() { return &___U3CU24U3Edelay_4; }
	inline void set_U3CU24U3Edelay_4(float value)
	{
		___U3CU24U3Edelay_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
