﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AsyncRequestStrin3762303666.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookLogger492541878.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookLogger_Cu3263114681.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookLogger_IO2349260197.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HttpMethod1387929545.h"
#include "AssemblyU2DCSharp_Facebook_MiniJSON_Json310250316.h"
#include "AssemblyU2DCSharp_Facebook_MiniJSON_Json_Parser2592981919.h"
#include "AssemblyU2DCSharp_Facebook_MiniJSON_Json_Parser_TO1273419625.h"
#include "AssemblyU2DCSharp_Facebook_MiniJSON_Json_Serialize1722694930.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Utilities585790056.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientationT3688806624.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientation3424593542.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientation_O449305768.h"
#include "AssemblyU2DCSharp_AnimatorRotationScript2214770896.h"
#include "AssemblyU2DCSharp_ButtonAnimationSoundScript4202863368.h"
#include "AssemblyU2DCSharp_LoadingScreenScript2063433619.h"
#include "AssemblyU2DCSharp_Screenshot1577017734.h"
#include "AssemblyU2DCSharp_Screenshot_OnCompleteEventHandle2690410097.h"
#include "AssemblyU2DCSharp_Screenshot_U3CWaitFrameLockU3Ec__264131251.h"
#include "AssemblyU2DCSharp_ScreenshotManagerGUI3459897460.h"
#include "AssemblyU2DCSharp_ScreenshotManagerGUI_U3CSaveU3Ec3387133346.h"
#include "AssemblyU2DCSharp_ScreenshotManagerGUI_U3CSaveExis2015724046.h"
#include "AssemblyU2DCSharp_ScreenshotManagerGUI_U3CWaitU3Ec2879261596.h"
#include "AssemblyU2DCSharp_SoundController665516523.h"
#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"
#include "AssemblyU2DCSharp_ButtonAlertExample917118176.h"
#include "AssemblyU2DCSharp_ButtonConfirmExample1901613692.h"
#include "AssemblyU2DCSharp_ButtonPromptExample804079924.h"
#include "AssemblyU2DCSharp_LeftMenuControlScript2683180034.h"
#include "AssemblyU2DCSharp_ButtonShowWindowTriggerScript92132516.h"
#include "AssemblyU2DCSharp_MagicTVAbstractWindowConfirmContr227123142.h"
#include "AssemblyU2DCSharp_MagicTVAbstractWindowPopUpContro2050906514.h"
#include "AssemblyU2DCSharp_MagicTVLeftMenuControlScript3130123923.h"
#include "AssemblyU2DCSharp_MagicTVWindowAboutControllerScri2586885333.h"
#include "AssemblyU2DCSharp_MagicTVWindowAlertControllerScri1344644260.h"
#include "AssemblyU2DCSharp_MagicTVWindowAstraZenecaInAppPas3129986085.h"
#include "AssemblyU2DCSharp_MagicTVWindowConfirmControllerScr165107144.h"
#include "AssemblyU2DCSharp_MagicTVWindowDownloadProgressCon1435101755.h"
#include "AssemblyU2DCSharp_MagicTVWindowErrorMessage1185091614.h"
#include "AssemblyU2DCSharp_MagicTVWindowHelpControllerScript126279975.h"
#include "AssemblyU2DCSharp_MagicTVWindowHowToPopUpControlle3295654727.h"
#include "AssemblyU2DCSharp_MagicTVWindowInAppCloseController657370434.h"
#include "AssemblyU2DCSharp_MagicTVWindowInAppGUIControllerS4145218373.h"
#include "AssemblyU2DCSharp_MagicTVWindowInAppGUIControllerSc509502638.h"
#include "AssemblyU2DCSharp_MagicTVWindowInAppGUIControllerS3024603740.h"
#include "AssemblyU2DCSharp_MagicTVWindowInAppListController1120695362.h"
#include "AssemblyU2DCSharp_MagicTVWindowInAppListController3050884011.h"
#include "AssemblyU2DCSharp_MagicTVWindowInScreeShotControll1504951103.h"
#include "AssemblyU2DCSharp_MagicTVWindowInstructionsControl2425143435.h"
#include "AssemblyU2DCSharp_MagicTVWindowPromptControllerScr1472770442.h"
#include "AssemblyU2DCSharp_MagicTVWindowPromptControllerScr3728636616.h"
#include "AssemblyU2DCSharp_MagicTVWindowSplashDownloadAvail1255941250.h"
#include "AssemblyU2DCSharp_MagicTVWindowSplashDownloadProgr3918082434.h"
#include "AssemblyU2DCSharp_WindowControlListScript2639301142.h"
#include "AssemblyU2DCSharp_DemoScript545284270.h"
#include "AssemblyU2DCSharp_ScreenshotManager4227900231.h"
#include "AssemblyU2DCSharp_ScreenshotManager_ImageType30728781.h"
#include "AssemblyU2DCSharp_ScreenshotManager_SaveStatus3474830263.h"
#include "AssemblyU2DCSharp_ScreenshotManager_U3CGrabScreensh651971222.h"
#include "AssemblyU2DCSharp_ScreenshotManager_U3CSaveU3Ec__I1819521282.h"
#include "AssemblyU2DCSharp_ScreenshotManager_U3CWaitU3Ec__I1311649531.h"
#include "AssemblyU2DCSharp_ARM_utils_io_ARMFileManager1788821605.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZi857528669.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZ1757880094.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZ3661782558.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZ1830158201.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZi147045086.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZ2425423099.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Checksum2680377483.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Checksum3523361801.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Checksum1378628876.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca1070518092.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pro2021439644.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Dir2626056408.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca2731106168.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Fil1416526437.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Name123192849.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pat1707447339.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Ext3673500740.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Nam4109882615.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Str1137307715.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Win4172240375.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encrypti4278846018.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encryptio125954580.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encrypti1919402088.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encrypti2055300160.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encrypti1361666485.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encryptio513042148.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encryptio496812608.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_GZip_GZip510847963.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_GZip_GZi2414750427.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_GZip_GZi2489725366.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_GZip_GZi2632250695.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_GZip_GZip278189375.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (U3CStartU3Ec__Iterator2A_t3762303666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[10] = 
{
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CurlParamsU3E__0_0(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CU24s_54U3E__1_1(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CpairU3E__2_2(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CheadersU3E__3_3(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CwwwU3E__4_4(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CU24s_55U3E__5_5(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CpairU3E__6_6(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U24PC_7(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U24current_8(),
	U3CStartU3Ec__Iterator2A_t3762303666::get_offset_of_U3CU3Ef__this_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (FacebookLogger_t492541878), -1, sizeof(FacebookLogger_t492541878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3501[2] = 
{
	0,
	FacebookLogger_t492541878_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (CustomLogger_t3263114681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[1] = 
{
	CustomLogger_t3263114681::get_offset_of_logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (IOSLogger_t2349260197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (HttpMethod_t1387929545)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3504[4] = 
{
	HttpMethod_t1387929545::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (Json_t310250316), -1, sizeof(Json_t310250316_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3506[1] = 
{
	Json_t310250316_StaticFields::get_offset_of_numberFormat_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (Parser_t2592981919), -1, sizeof(Parser_t2592981919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3507[4] = 
{
	0,
	0,
	Parser_t2592981919::get_offset_of_json_2(),
	Parser_t2592981919_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (TOKEN_t1273419625)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3508[13] = 
{
	TOKEN_t1273419625::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (Serializer_t1722694930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3509[1] = 
{
	Serializer_t1722694930::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (Utilities_t585790056), -1, sizeof(Utilities_t585790056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3510[2] = 
{
	0,
	Utilities_t585790056_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (ScreenOrientationType_t3688806624)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3511[3] = 
{
	ScreenOrientationType_t3688806624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (ScreenOrientation_t3424593542), -1, sizeof(ScreenOrientation_t3424593542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3512[7] = 
{
	ScreenOrientation_t3424593542::get_offset_of__lastSize_2(),
	ScreenOrientation_t3424593542_StaticFields::get_offset_of__destroying_3(),
	ScreenOrientation_t3424593542::get_offset_of_OnChangeScreenOrientationType_4(),
	ScreenOrientation_t3424593542_StaticFields::get_offset_of__instanceScreenOrientation_5(),
	ScreenOrientation_t3424593542_StaticFields::get_offset_of_mSizeFrame_6(),
	ScreenOrientation_t3424593542_StaticFields::get_offset_of_s_GetSizeOfMainGameView_7(),
	ScreenOrientation_t3424593542_StaticFields::get_offset_of_mGameSize_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (OnScreenOrientationEventHandler_t449305768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (AnimatorRotationScript_t2214770896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3514[1] = 
{
	AnimatorRotationScript_t2214770896::get_offset_of_Speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (ButtonAnimationSoundScript_t4202863368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3515[2] = 
{
	ButtonAnimationSoundScript_t4202863368::get_offset_of_animationClip_2(),
	ButtonAnimationSoundScript_t4202863368::get_offset_of_audioClip_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (LoadingScreenScript_t2063433619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3518[1] = 
{
	LoadingScreenScript_t2063433619::get_offset_of_Loading_Screen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (Screenshot_t1577017734), -1, sizeof(Screenshot_t1577017734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3519[7] = 
{
	0,
	Screenshot_t1577017734_StaticFields::get_offset_of__selfGameObject_3(),
	Screenshot_t1577017734_StaticFields::get_offset_of_fotoPath_4(),
	Screenshot_t1577017734_StaticFields::get_offset_of__selfInstance_5(),
	Screenshot_t1577017734::get_offset_of_onComplete_6(),
	Screenshot_t1577017734::get_offset_of__path_7(),
	Screenshot_t1577017734_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (OnCompleteEventHandler_t2690410097), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (U3CWaitFrameLockU3Ec__Iterator2B_t264131251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3521[3] = 
{
	U3CWaitFrameLockU3Ec__Iterator2B_t264131251::get_offset_of_U3CmtvwiagcsU3E__0_0(),
	U3CWaitFrameLockU3Ec__Iterator2B_t264131251::get_offset_of_U24PC_1(),
	U3CWaitFrameLockU3Ec__Iterator2B_t264131251::get_offset_of_U24current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (ScreenshotManagerGUI_t3459897460), -1, sizeof(ScreenshotManagerGUI_t3459897460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3522[3] = 
{
	ScreenshotManagerGUI_t3459897460_StaticFields::get_offset_of_instance_2(),
	ScreenshotManagerGUI_t3459897460::get_offset_of_ScreenshotFinishedSaving_3(),
	ScreenshotManagerGUI_t3459897460::get_offset_of_ImageFinishedSaving_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (U3CSaveU3Ec__Iterator2C_t3387133346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[10] = 
{
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U3CphotoSavedU3E__0_0(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U3CdateU3E__1_1(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_fileName_2(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U3CscreenshotFilenameU3E__2_3(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U3CpathU3E__3_4(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_callback_5(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U24PC_6(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U24current_7(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U3CU24U3EfileName_8(),
	U3CSaveU3Ec__Iterator2C_t3387133346::get_offset_of_U3CU24U3Ecallback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (U3CSaveExistingU3Ec__Iterator2D_t2015724046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[10] = 
{
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U3CphotoSavedU3E__0_0(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_fileName_1(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U3CpathU3E__1_2(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_bytes_3(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_callback_4(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U24PC_5(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U24current_6(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U3CU24U3EfileName_7(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U3CU24U3Ebytes_8(),
	U3CSaveExistingU3Ec__Iterator2D_t2015724046::get_offset_of_U3CU24U3Ecallback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (U3CWaitU3Ec__Iterator2E_t2879261596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[5] = 
{
	U3CWaitU3Ec__Iterator2E_t2879261596::get_offset_of_delay_0(),
	U3CWaitU3Ec__Iterator2E_t2879261596::get_offset_of_U3CpauseTargetU3E__0_1(),
	U3CWaitU3Ec__Iterator2E_t2879261596::get_offset_of_U24PC_2(),
	U3CWaitU3Ec__Iterator2E_t2879261596::get_offset_of_U24current_3(),
	U3CWaitU3Ec__Iterator2E_t2879261596::get_offset_of_U3CU24U3Edelay_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (SoundController_t665516523), -1, sizeof(SoundController_t665516523_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3526[1] = 
{
	SoundController_t665516523_StaticFields::get_offset_of__soundController_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (GenericWindowControllerScript_t248075822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[2] = 
{
	GenericWindowControllerScript_t248075822::get_offset_of_onShow_2(),
	GenericWindowControllerScript_t248075822::get_offset_of_onHide_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (ButtonAlertExample_t917118176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (ButtonConfirmExample_t1901613692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (ButtonPromptExample_t804079924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (LeftMenuControlScript_t2683180034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3531[3] = 
{
	LeftMenuControlScript_t2683180034::get_offset_of_MenuGameObject_2(),
	LeftMenuControlScript_t2683180034::get_offset_of_MenuParentGameObject_3(),
	LeftMenuControlScript_t2683180034::get_offset_of_SetaParentGameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (ButtonShowWindowTriggerScript_t92132516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3532[1] = 
{
	ButtonShowWindowTriggerScript_t92132516::get_offset_of_TargetWindow_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (MagicTVAbstractWindowConfirmControllerScript_t227123142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3533[2] = 
{
	MagicTVAbstractWindowConfirmControllerScript_t227123142::get_offset_of_OnClickCancel_9(),
	MagicTVAbstractWindowConfirmControllerScript_t227123142::get_offset_of_LabelButtonCancel_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (MagicTVAbstractWindowPopUpControllerScript_t2050906514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3534[5] = 
{
	MagicTVAbstractWindowPopUpControllerScript_t2050906514::get_offset_of_OnClickOk_4(),
	MagicTVAbstractWindowPopUpControllerScript_t2050906514::get_offset_of_LabelTitle_5(),
	MagicTVAbstractWindowPopUpControllerScript_t2050906514::get_offset_of_LabelMessage_6(),
	MagicTVAbstractWindowPopUpControllerScript_t2050906514::get_offset_of_LabelButtonOk_7(),
	MagicTVAbstractWindowPopUpControllerScript_t2050906514::get_offset_of_OkButton_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (MagicTVLeftMenuControlScript_t3130123923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3535[3] = 
{
	MagicTVLeftMenuControlScript_t3130123923::get_offset_of_animationController_5(),
	MagicTVLeftMenuControlScript_t3130123923::get_offset_of_openAnimationClip_6(),
	MagicTVLeftMenuControlScript_t3130123923::get_offset_of_closeAnimationClip_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (MagicTVWindowAboutControllerScript_t2586885333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3536[8] = 
{
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_onClickFacebook_4(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_onClickTwitter_5(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_onClickSite_6(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_onClickEmail_7(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_onClickYouTubeChannel_8(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_onClickClearCache_9(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_LabelPINLand_10(),
	MagicTVWindowAboutControllerScript_t2586885333::get_offset_of_LabelPINPort_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (MagicTVWindowAlertControllerScript_t1344644260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (MagicTVWindowAstraZenecaInAppPassword_t3129986085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3538[4] = 
{
	MagicTVWindowAstraZenecaInAppPassword_t3129986085::get_offset_of_LandscapePasswordInput_4(),
	MagicTVWindowAstraZenecaInAppPassword_t3129986085::get_offset_of_PortraitPasswordInput_5(),
	MagicTVWindowAstraZenecaInAppPassword_t3129986085::get_offset_of_onClickLogin_6(),
	MagicTVWindowAstraZenecaInAppPassword_t3129986085::get_offset_of_PasswordValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (MagicTVWindowConfirmControllerScript_t165107144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (MagicTVWindowDownloadProgressControllerScript_t1435101755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (MagicTVWindowErrorMessage_t1185091614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3541[4] = 
{
	MagicTVWindowErrorMessage_t1185091614::get_offset_of__ButtonLabel_4(),
	MagicTVWindowErrorMessage_t1185091614::get_offset_of__ErrorMessagePort_5(),
	MagicTVWindowErrorMessage_t1185091614::get_offset_of__ErrorMessageLand_6(),
	MagicTVWindowErrorMessage_t1185091614::get_offset_of_onClickConfig_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (MagicTVWindowHelpControllerScript_t126279975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[2] = 
{
	MagicTVWindowHelpControllerScript_t126279975::get_offset_of_onClickPrint_4(),
	MagicTVWindowHelpControllerScript_t126279975::get_offset_of_onClickYouTubeVideo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (MagicTVWindowHowToPopUpControllerScript_t3295654727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[1] = 
{
	MagicTVWindowHowToPopUpControllerScript_t3295654727::get_offset_of_DontShowAnymoreToggle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (MagicTVWindowInAppCloseControllerScript_t657370434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (MagicTVWindowInAppGUIControllerScript_t4145218373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3545[15] = 
{
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_MenuContainer_4(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_CameraButton_5(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_InfoButton_6(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_UpdateButton_7(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_onUpdateClick_8(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_canvasObject_9(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_onClickCamera_10(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_ExitButton_11(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_isCloseBtnOn_12(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of__upButtonPressed_13(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of__timeout_14(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_onScreenShotComplete_15(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_onClickReset_16(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_onClickClearCache_17(),
	MagicTVWindowInAppGUIControllerScript_t4145218373::get_offset_of_onClickBack_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (ScreenShotCompleteDelegate_t509502638), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3547[2] = 
{
	U3CSaveScreenShotU3Ec__Iterator2F_t3024603740::get_offset_of_U24PC_0(),
	U3CSaveScreenShotU3Ec__Iterator2F_t3024603740::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (MagicTVWindowInAppListControllerScript_t1120695362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[1] = 
{
	MagicTVWindowInAppListControllerScript_t1120695362::get_offset_of_onClickStart_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (MagicTVWindowInAppListControllerScriptWaiting_t3050884011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[2] = 
{
	MagicTVWindowInAppListControllerScriptWaiting_t3050884011::get_offset_of_splashImage_4(),
	MagicTVWindowInAppListControllerScriptWaiting_t3050884011::get_offset_of_onClickStart_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (MagicTVWindowInScreeShotControllerScript_t1504951103), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (MagicTVWindowInstructionsControllerScript_t2425143435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (MagicTVWindowPromptControllerScript_t1472770442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3552[3] = 
{
	MagicTVWindowPromptControllerScript_t1472770442::get_offset_of_LabelInputPrompt_11(),
	MagicTVWindowPromptControllerScript_t1472770442::get_offset_of__defaultText_12(),
	MagicTVWindowPromptControllerScript_t1472770442::get_offset_of_OnClickOk_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (ClickOkPromptDelegate_t3728636616), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3554[2] = 
{
	MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250::get_offset_of_onClickConfirmDownload_4(),
	MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250::get_offset_of_onClickCancelDownload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3555[4] = 
{
	MagicTVWindowSplashDownloadProgressControllerScript_t3918082434::get_offset_of_ButtonCancelDownload_4(),
	MagicTVWindowSplashDownloadProgressControllerScript_t3918082434::get_offset_of_progressText_5(),
	MagicTVWindowSplashDownloadProgressControllerScript_t3918082434::get_offset_of_onClickCancelDownload_6(),
	MagicTVWindowSplashDownloadProgressControllerScript_t3918082434::get_offset_of__progress_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (WindowControlListScript_t2639301142), -1, sizeof(WindowControlListScript_t2639301142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3556[28] = 
{
	WindowControlListScript_t2639301142::get_offset_of_ButtonPrefab_2(),
	WindowControlListScript_t2639301142::get_offset_of_ButtonTable_3(),
	WindowControlListScript_t2639301142::get_offset_of_PopUpAlertPrefab_4(),
	WindowControlListScript_t2639301142::get_offset_of_PopUpConfirmPrefab_5(),
	WindowControlListScript_t2639301142::get_offset_of_PopUpPromptPrefab_6(),
	WindowControlListScript_t2639301142::get_offset_of_AminationForLabelChangeDefault_7(),
	WindowControlListScript_t2639301142::get_offset_of_AutoShowInAppGui_8(),
	WindowControlListScript_t2639301142::get_offset_of_CanvasRoot_9(),
	WindowControlListScript_t2639301142_StaticFields::get_offset_of_Instance_10(),
	WindowControlListScript_t2639301142::get_offset_of_freezeScreenRotation_11(),
	WindowControlListScript_t2639301142::get_offset_of_WindowInAppGUI_12(),
	WindowControlListScript_t2639301142::get_offset_of_WindowGuiControlers_13(),
	WindowControlListScript_t2639301142::get_offset_of_WindowSplashDownloadProgress_14(),
	WindowControlListScript_t2639301142::get_offset_of_WindowInstructions_15(),
	WindowControlListScript_t2639301142::get_offset_of_WindowAbout_16(),
	WindowControlListScript_t2639301142::get_offset_of_WindowInAppListWaiting_17(),
	WindowControlListScript_t2639301142::get_offset_of_WindowErrorMessage_18(),
	WindowControlListScript_t2639301142::get_offset_of_WindowDownloadProgress_19(),
	WindowControlListScript_t2639301142::get_offset_of_WindowScreenShot_20(),
	WindowControlListScript_t2639301142::get_offset_of_WindowScreenClose_21(),
	WindowControlListScript_t2639301142::get_offset_of_onClickYouTubeChannel_22(),
	WindowControlListScript_t2639301142::get_offset_of_onClickYouTubeVideo_23(),
	WindowControlListScript_t2639301142::get_offset_of_onClickTwitter_24(),
	WindowControlListScript_t2639301142::get_offset_of_onClickYouFacebook_25(),
	WindowControlListScript_t2639301142::get_offset_of_onClickEmail_26(),
	WindowControlListScript_t2639301142::get_offset_of_materialImagemInicial_27(),
	WindowControlListScript_t2639301142::get_offset_of_popUpControllerList_28(),
	WindowControlListScript_t2639301142::get_offset_of__currentPopUp_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (DemoScript_t545284270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[5] = 
{
	DemoScript_t545284270::get_offset_of_hideGUI_2(),
	DemoScript_t545284270::get_offset_of_texture_3(),
	DemoScript_t545284270::get_offset_of_console_4(),
	DemoScript_t545284270::get_offset_of_ui_5(),
	DemoScript_t545284270::get_offset_of_screenshot_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (ScreenshotManager_t4227900231), -1, sizeof(ScreenshotManager_t4227900231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3558[5] = 
{
	ScreenshotManager_t4227900231_StaticFields::get_offset_of_instance_2(),
	ScreenshotManager_t4227900231_StaticFields::get_offset_of_go_3(),
	ScreenshotManager_t4227900231_StaticFields::get_offset_of_OnScreenshotTaken_4(),
	ScreenshotManager_t4227900231_StaticFields::get_offset_of_OnScreenshotSaved_5(),
	ScreenshotManager_t4227900231_StaticFields::get_offset_of_OnImageSaved_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (ImageType_t30728781)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3559[3] = 
{
	ImageType_t30728781::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (SaveStatus_t3474830263)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3560[5] = 
{
	SaveStatus_t3474830263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (U3CGrabScreenshotU3Ec__Iterator30_t651971222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[14] = 
{
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_screenArea_0(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CtextureU3E__0_1(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_fileType_2(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CbytesU3E__1_3(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CfileExtU3E__2_4(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CdateU3E__3_5(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_fileName_6(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CscreenshotFilenameU3E__4_7(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CpathU3E__5_8(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U24PC_9(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U24current_10(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CU24U3EscreenArea_11(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CU24U3EfileType_12(),
	U3CGrabScreenshotU3Ec__Iterator30_t651971222::get_offset_of_U3CU24U3EfileName_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (U3CSaveU3Ec__Iterator31_t1819521282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3562[10] = 
{
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U3CcountU3E__0_0(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U3CsavedU3E__1_1(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_path_2(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_bytes_3(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_imageType_4(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U24PC_5(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U24current_6(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U3CU24U3Epath_7(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U3CU24U3Ebytes_8(),
	U3CSaveU3Ec__Iterator31_t1819521282::get_offset_of_U3CU24U3EimageType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (U3CWaitU3Ec__Iterator32_t1311649531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3563[5] = 
{
	U3CWaitU3Ec__Iterator32_t1311649531::get_offset_of_delay_0(),
	U3CWaitU3Ec__Iterator32_t1311649531::get_offset_of_U3CpauseTargetU3E__0_1(),
	U3CWaitU3Ec__Iterator32_t1311649531::get_offset_of_U24PC_2(),
	U3CWaitU3Ec__Iterator32_t1311649531::get_offset_of_U24current_3(),
	U3CWaitU3Ec__Iterator32_t1311649531::get_offset_of_U3CU24U3Edelay_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (ARMFileManager_t1788821605), -1, sizeof(ARMFileManager_t1788821605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3564[1] = 
{
	ARMFileManager_t1788821605_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (BZip2_t857528669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (BZip2Constants_t1757880094), -1, sizeof(BZip2Constants_t1757880094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3566[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	BZip2Constants_t1757880094_StaticFields::get_offset_of_RandomNumbers_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (BZip2Exception_t3661782558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (BZip2InputStream_t1830158201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3568[45] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	BZip2InputStream_t1830158201::get_offset_of_last_9(),
	BZip2InputStream_t1830158201::get_offset_of_origPtr_10(),
	BZip2InputStream_t1830158201::get_offset_of_blockSize100k_11(),
	BZip2InputStream_t1830158201::get_offset_of_blockRandomised_12(),
	BZip2InputStream_t1830158201::get_offset_of_bsBuff_13(),
	BZip2InputStream_t1830158201::get_offset_of_bsLive_14(),
	BZip2InputStream_t1830158201::get_offset_of_mCrc_15(),
	BZip2InputStream_t1830158201::get_offset_of_inUse_16(),
	BZip2InputStream_t1830158201::get_offset_of_nInUse_17(),
	BZip2InputStream_t1830158201::get_offset_of_seqToUnseq_18(),
	BZip2InputStream_t1830158201::get_offset_of_unseqToSeq_19(),
	BZip2InputStream_t1830158201::get_offset_of_selector_20(),
	BZip2InputStream_t1830158201::get_offset_of_selectorMtf_21(),
	BZip2InputStream_t1830158201::get_offset_of_tt_22(),
	BZip2InputStream_t1830158201::get_offset_of_ll8_23(),
	BZip2InputStream_t1830158201::get_offset_of_unzftab_24(),
	BZip2InputStream_t1830158201::get_offset_of_limit_25(),
	BZip2InputStream_t1830158201::get_offset_of_baseArray_26(),
	BZip2InputStream_t1830158201::get_offset_of_perm_27(),
	BZip2InputStream_t1830158201::get_offset_of_minLens_28(),
	BZip2InputStream_t1830158201::get_offset_of_baseStream_29(),
	BZip2InputStream_t1830158201::get_offset_of_streamEnd_30(),
	BZip2InputStream_t1830158201::get_offset_of_currentChar_31(),
	BZip2InputStream_t1830158201::get_offset_of_currentState_32(),
	BZip2InputStream_t1830158201::get_offset_of_storedBlockCRC_33(),
	BZip2InputStream_t1830158201::get_offset_of_storedCombinedCRC_34(),
	BZip2InputStream_t1830158201::get_offset_of_computedBlockCRC_35(),
	BZip2InputStream_t1830158201::get_offset_of_computedCombinedCRC_36(),
	BZip2InputStream_t1830158201::get_offset_of_count_37(),
	BZip2InputStream_t1830158201::get_offset_of_chPrev_38(),
	BZip2InputStream_t1830158201::get_offset_of_ch2_39(),
	BZip2InputStream_t1830158201::get_offset_of_tPos_40(),
	BZip2InputStream_t1830158201::get_offset_of_rNToGo_41(),
	BZip2InputStream_t1830158201::get_offset_of_rTPos_42(),
	BZip2InputStream_t1830158201::get_offset_of_i2_43(),
	BZip2InputStream_t1830158201::get_offset_of_j2_44(),
	BZip2InputStream_t1830158201::get_offset_of_z_45(),
	BZip2InputStream_t1830158201::get_offset_of_isStreamOwner_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (BZip2OutputStream_t147045086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3569[42] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	BZip2OutputStream_t147045086::get_offset_of_increments_9(),
	BZip2OutputStream_t147045086::get_offset_of_isStreamOwner_10(),
	BZip2OutputStream_t147045086::get_offset_of_last_11(),
	BZip2OutputStream_t147045086::get_offset_of_origPtr_12(),
	BZip2OutputStream_t147045086::get_offset_of_blockSize100k_13(),
	BZip2OutputStream_t147045086::get_offset_of_blockRandomised_14(),
	BZip2OutputStream_t147045086::get_offset_of_bytesOut_15(),
	BZip2OutputStream_t147045086::get_offset_of_bsBuff_16(),
	BZip2OutputStream_t147045086::get_offset_of_bsLive_17(),
	BZip2OutputStream_t147045086::get_offset_of_mCrc_18(),
	BZip2OutputStream_t147045086::get_offset_of_inUse_19(),
	BZip2OutputStream_t147045086::get_offset_of_nInUse_20(),
	BZip2OutputStream_t147045086::get_offset_of_seqToUnseq_21(),
	BZip2OutputStream_t147045086::get_offset_of_unseqToSeq_22(),
	BZip2OutputStream_t147045086::get_offset_of_selector_23(),
	BZip2OutputStream_t147045086::get_offset_of_selectorMtf_24(),
	BZip2OutputStream_t147045086::get_offset_of_block_25(),
	BZip2OutputStream_t147045086::get_offset_of_quadrant_26(),
	BZip2OutputStream_t147045086::get_offset_of_zptr_27(),
	BZip2OutputStream_t147045086::get_offset_of_szptr_28(),
	BZip2OutputStream_t147045086::get_offset_of_ftab_29(),
	BZip2OutputStream_t147045086::get_offset_of_nMTF_30(),
	BZip2OutputStream_t147045086::get_offset_of_mtfFreq_31(),
	BZip2OutputStream_t147045086::get_offset_of_workFactor_32(),
	BZip2OutputStream_t147045086::get_offset_of_workDone_33(),
	BZip2OutputStream_t147045086::get_offset_of_workLimit_34(),
	BZip2OutputStream_t147045086::get_offset_of_firstAttempt_35(),
	BZip2OutputStream_t147045086::get_offset_of_nBlocksRandomised_36(),
	BZip2OutputStream_t147045086::get_offset_of_currentChar_37(),
	BZip2OutputStream_t147045086::get_offset_of_runLength_38(),
	BZip2OutputStream_t147045086::get_offset_of_blockCRC_39(),
	BZip2OutputStream_t147045086::get_offset_of_combinedCRC_40(),
	BZip2OutputStream_t147045086::get_offset_of_allowableBlockSize_41(),
	BZip2OutputStream_t147045086::get_offset_of_baseStream_42(),
	BZip2OutputStream_t147045086::get_offset_of_disposed__43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (StackElement_t2425423099)+ sizeof (Il2CppObject), sizeof(StackElement_t2425423099_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3570[3] = 
{
	StackElement_t2425423099::get_offset_of_ll_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StackElement_t2425423099::get_offset_of_hh_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StackElement_t2425423099::get_offset_of_dd_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (Adler32_t2680377483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3571[2] = 
{
	0,
	Adler32_t2680377483::get_offset_of_checksum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (Crc32_t3523361801), -1, sizeof(Crc32_t3523361801_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3572[3] = 
{
	0,
	Crc32_t3523361801_StaticFields::get_offset_of_CrcTable_1(),
	Crc32_t3523361801::get_offset_of_crc_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (StrangeCRC_t1378628876), -1, sizeof(StrangeCRC_t1378628876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3574[2] = 
{
	StrangeCRC_t1378628876_StaticFields::get_offset_of_crc32Table_0(),
	StrangeCRC_t1378628876::get_offset_of_globalCrc_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (ScanEventArgs_t1070518092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[2] = 
{
	ScanEventArgs_t1070518092::get_offset_of_name__1(),
	ScanEventArgs_t1070518092::get_offset_of_continueRunning__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (ProgressEventArgs_t2021439644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3576[4] = 
{
	ProgressEventArgs_t2021439644::get_offset_of_name__1(),
	ProgressEventArgs_t2021439644::get_offset_of_processed__2(),
	ProgressEventArgs_t2021439644::get_offset_of_target__3(),
	ProgressEventArgs_t2021439644::get_offset_of_continueRunning__4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (DirectoryEventArgs_t2626056408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[1] = 
{
	DirectoryEventArgs_t2626056408::get_offset_of_hasMatchingFiles__3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (ScanFailureEventArgs_t2731106168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3578[3] = 
{
	ScanFailureEventArgs_t2731106168::get_offset_of_name__1(),
	ScanFailureEventArgs_t2731106168::get_offset_of_exception__2(),
	ScanFailureEventArgs_t2731106168::get_offset_of_continueRunning__3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (FileSystemScanner_t1416526437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[8] = 
{
	FileSystemScanner_t1416526437::get_offset_of_ProcessDirectory_0(),
	FileSystemScanner_t1416526437::get_offset_of_ProcessFile_1(),
	FileSystemScanner_t1416526437::get_offset_of_CompletedFile_2(),
	FileSystemScanner_t1416526437::get_offset_of_DirectoryFailure_3(),
	FileSystemScanner_t1416526437::get_offset_of_FileFailure_4(),
	FileSystemScanner_t1416526437::get_offset_of_fileFilter__5(),
	FileSystemScanner_t1416526437::get_offset_of_directoryFilter__6(),
	FileSystemScanner_t1416526437::get_offset_of_alive__7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (NameFilter_t123192849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3582[3] = 
{
	NameFilter_t123192849::get_offset_of_filter__0(),
	NameFilter_t123192849::get_offset_of_inclusions__1(),
	NameFilter_t123192849::get_offset_of_exclusions__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (PathFilter_t1707447339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3583[1] = 
{
	PathFilter_t1707447339::get_offset_of_nameFilter__0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (ExtendedPathFilter_t3673500740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3584[4] = 
{
	ExtendedPathFilter_t3673500740::get_offset_of_minSize__1(),
	ExtendedPathFilter_t3673500740::get_offset_of_maxSize__2(),
	ExtendedPathFilter_t3673500740::get_offset_of_minDate__3(),
	ExtendedPathFilter_t3673500740::get_offset_of_maxDate__4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (NameAndSizeFilter_t4109882615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3585[2] = 
{
	NameAndSizeFilter_t4109882615::get_offset_of_minSize__1(),
	NameAndSizeFilter_t4109882615::get_offset_of_maxSize__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (StreamUtils_t1137307715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (WindowsPathUtils_t4172240375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (PkzipClassic_t4278846018), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (PkzipClassicCryptoBase_t125954580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3589[1] = 
{
	PkzipClassicCryptoBase_t125954580::get_offset_of_keys_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (PkzipClassicEncryptCryptoTransform_t1919402088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (PkzipClassicDecryptCryptoTransform_t2055300160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (PkzipClassicManaged_t1361666485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[1] = 
{
	PkzipClassicManaged_t1361666485::get_offset_of_key__10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (ZipAESStream_t513042148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3593[8] = 
{
	0,
	0,
	ZipAESStream_t513042148::get_offset_of__stream_19(),
	ZipAESStream_t513042148::get_offset_of__transform_20(),
	ZipAESStream_t513042148::get_offset_of__slideBuffer_21(),
	ZipAESStream_t513042148::get_offset_of__slideBufStartPos_22(),
	ZipAESStream_t513042148::get_offset_of__slideBufFreePos_23(),
	ZipAESStream_t513042148::get_offset_of__blockAndAuth_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (ZipAESTransform_t496812608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3594[12] = 
{
	0,
	0,
	0,
	ZipAESTransform_t496812608::get_offset_of__blockSize_3(),
	ZipAESTransform_t496812608::get_offset_of__encryptor_4(),
	ZipAESTransform_t496812608::get_offset_of__counterNonce_5(),
	ZipAESTransform_t496812608::get_offset_of__encryptBuffer_6(),
	ZipAESTransform_t496812608::get_offset_of__encrPos_7(),
	ZipAESTransform_t496812608::get_offset_of__pwdVerifier_8(),
	ZipAESTransform_t496812608::get_offset_of__hmacsha1_9(),
	ZipAESTransform_t496812608::get_offset_of__finalised_10(),
	ZipAESTransform_t496812608::get_offset_of__writeMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (GZipConstants_t510847963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3595[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (GZipException_t2414750427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (GZipInputStream_t2489725366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3597[2] = 
{
	GZipInputStream_t2489725366::get_offset_of_crc_8(),
	GZipInputStream_t2489725366::get_offset_of_readGZIPHeader_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (GZipOutputStream_t2632250695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[2] = 
{
	GZipOutputStream_t2632250695::get_offset_of_crc_11(),
	GZipOutputStream_t2632250695::get_offset_of_state__12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (OutputState_t278189375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3599[5] = 
{
	OutputState_t278189375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
