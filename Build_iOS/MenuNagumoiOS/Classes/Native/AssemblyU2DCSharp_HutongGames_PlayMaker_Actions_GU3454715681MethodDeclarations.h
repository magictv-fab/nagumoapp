﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILabel
struct GUILabel_t3454715681;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILabel::.ctor()
extern "C"  void GUILabel__ctor_m3750176869 (GUILabel_t3454715681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILabel::OnGUI()
extern "C"  void GUILabel_OnGUI_m3245575519 (GUILabel_t3454715681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
