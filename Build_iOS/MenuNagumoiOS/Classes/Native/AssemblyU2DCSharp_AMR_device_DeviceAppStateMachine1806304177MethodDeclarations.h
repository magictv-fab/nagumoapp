﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler
struct OnAwakeEventHandler_t1806304177;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAwakeEventHandler__ctor_m650594776 (OnAwakeEventHandler_t1806304177 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler::Invoke()
extern "C"  void OnAwakeEventHandler_Invoke_m1130140722 (OnAwakeEventHandler_t1806304177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAwakeEventHandler_BeginInvoke_m1688287633 (OnAwakeEventHandler_t1806304177 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnAwakeEventHandler_EndInvoke_m2168023016 (OnAwakeEventHandler_t1806304177 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
