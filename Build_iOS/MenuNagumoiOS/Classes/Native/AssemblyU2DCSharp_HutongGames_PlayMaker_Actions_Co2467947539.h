﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertIntToFloat
struct  ConvertIntToFloat_t2467947539  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertIntToFloat::intVariable
	FsmInt_t1596138449 * ___intVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertIntToFloat::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_10;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertIntToFloat::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_intVariable_9() { return static_cast<int32_t>(offsetof(ConvertIntToFloat_t2467947539, ___intVariable_9)); }
	inline FsmInt_t1596138449 * get_intVariable_9() const { return ___intVariable_9; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_9() { return &___intVariable_9; }
	inline void set_intVariable_9(FsmInt_t1596138449 * value)
	{
		___intVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_9, value);
	}

	inline static int32_t get_offset_of_floatVariable_10() { return static_cast<int32_t>(offsetof(ConvertIntToFloat_t2467947539, ___floatVariable_10)); }
	inline FsmFloat_t2134102846 * get_floatVariable_10() const { return ___floatVariable_10; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_10() { return &___floatVariable_10; }
	inline void set_floatVariable_10(FsmFloat_t2134102846 * value)
	{
		___floatVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(ConvertIntToFloat_t2467947539, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
