﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3587651179;

#include "UnityEngine_UI_UnityEngine_UI_ScrollRect3606982749.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InfiniteScroll
struct  InfiniteScroll_t1391012385  : public ScrollRect_t3606982749
{
public:
	// System.Boolean InfiniteScroll::initOnAwake
	bool ___initOnAwake_38;
	// UnityEngine.RectTransform InfiniteScroll::_t
	RectTransform_t972643934 * ____t_39;
	// UnityEngine.RectTransform[] InfiniteScroll::prefabItems
	RectTransformU5BU5D_t3587651179* ___prefabItems_40;
	// System.Int32 InfiniteScroll::itemTypeStart
	int32_t ___itemTypeStart_41;
	// System.Int32 InfiniteScroll::itemTypeEnd
	int32_t ___itemTypeEnd_42;
	// System.Boolean InfiniteScroll::init
	bool ___init_43;
	// UnityEngine.Vector2 InfiniteScroll::dragOffset
	Vector2_t4282066565  ___dragOffset_44;

public:
	inline static int32_t get_offset_of_initOnAwake_38() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ___initOnAwake_38)); }
	inline bool get_initOnAwake_38() const { return ___initOnAwake_38; }
	inline bool* get_address_of_initOnAwake_38() { return &___initOnAwake_38; }
	inline void set_initOnAwake_38(bool value)
	{
		___initOnAwake_38 = value;
	}

	inline static int32_t get_offset_of__t_39() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ____t_39)); }
	inline RectTransform_t972643934 * get__t_39() const { return ____t_39; }
	inline RectTransform_t972643934 ** get_address_of__t_39() { return &____t_39; }
	inline void set__t_39(RectTransform_t972643934 * value)
	{
		____t_39 = value;
		Il2CppCodeGenWriteBarrier(&____t_39, value);
	}

	inline static int32_t get_offset_of_prefabItems_40() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ___prefabItems_40)); }
	inline RectTransformU5BU5D_t3587651179* get_prefabItems_40() const { return ___prefabItems_40; }
	inline RectTransformU5BU5D_t3587651179** get_address_of_prefabItems_40() { return &___prefabItems_40; }
	inline void set_prefabItems_40(RectTransformU5BU5D_t3587651179* value)
	{
		___prefabItems_40 = value;
		Il2CppCodeGenWriteBarrier(&___prefabItems_40, value);
	}

	inline static int32_t get_offset_of_itemTypeStart_41() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ___itemTypeStart_41)); }
	inline int32_t get_itemTypeStart_41() const { return ___itemTypeStart_41; }
	inline int32_t* get_address_of_itemTypeStart_41() { return &___itemTypeStart_41; }
	inline void set_itemTypeStart_41(int32_t value)
	{
		___itemTypeStart_41 = value;
	}

	inline static int32_t get_offset_of_itemTypeEnd_42() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ___itemTypeEnd_42)); }
	inline int32_t get_itemTypeEnd_42() const { return ___itemTypeEnd_42; }
	inline int32_t* get_address_of_itemTypeEnd_42() { return &___itemTypeEnd_42; }
	inline void set_itemTypeEnd_42(int32_t value)
	{
		___itemTypeEnd_42 = value;
	}

	inline static int32_t get_offset_of_init_43() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ___init_43)); }
	inline bool get_init_43() const { return ___init_43; }
	inline bool* get_address_of_init_43() { return &___init_43; }
	inline void set_init_43(bool value)
	{
		___init_43 = value;
	}

	inline static int32_t get_offset_of_dragOffset_44() { return static_cast<int32_t>(offsetof(InfiniteScroll_t1391012385, ___dragOffset_44)); }
	inline Vector2_t4282066565  get_dragOffset_44() const { return ___dragOffset_44; }
	inline Vector2_t4282066565 * get_address_of_dragOffset_44() { return &___dragOffset_44; }
	inline void set_dragOffset_44(Vector2_t4282066565  value)
	{
		___dragOffset_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
