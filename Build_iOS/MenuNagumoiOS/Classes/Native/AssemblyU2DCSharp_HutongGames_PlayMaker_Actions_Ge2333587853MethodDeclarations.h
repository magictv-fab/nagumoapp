﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVectorLength
struct GetVectorLength_t2333587853;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::.ctor()
extern "C"  void GetVectorLength__ctor_m2660363337 (GetVectorLength_t2333587853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::Reset()
extern "C"  void GetVectorLength_Reset_m306796278 (GetVectorLength_t2333587853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::OnEnter()
extern "C"  void GetVectorLength_OnEnter_m495354336 (GetVectorLength_t2333587853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::DoVectorLength()
extern "C"  void GetVectorLength_DoVectorLength_m1808744591 (GetVectorLength_t2333587853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
