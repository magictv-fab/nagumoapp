﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern "C"  void RenderSettings_set_fog_m1757489802 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m1507677940 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2299508764 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern "C"  void RenderSettings_set_fogDensity_m998518996 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientSkyColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientSkyColor_m1963190236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_ambientSkyColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientSkyColor_m2161476599 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientEquatorColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientEquatorColor_m276013214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_ambientEquatorColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientEquatorColor_m3763161333 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientGroundColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientGroundColor_m847528164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_ambientGroundColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientGroundColor_m2739647605 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientLight()
extern "C"  Color_t4194546905  RenderSettings_get_ambientLight_m929316510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_ambientLight(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientLight_m791635003 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientLight_m691711297 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientLight_m2620477877 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_ambientIntensity()
extern "C"  float RenderSettings_get_ambientIntensity_m1272966080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_ambientIntensity(System.Single)
extern "C"  void RenderSettings_set_ambientIntensity_m2896643971 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_reflectionIntensity()
extern "C"  float RenderSettings_get_reflectionIntensity_m1862510533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_reflectionIntensity(System.Single)
extern "C"  void RenderSettings_set_reflectionIntensity_m4083040622 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderSettings::get_reflectionBounces()
extern "C"  int32_t RenderSettings_get_reflectionBounces_m2858699465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_reflectionBounces(System.Int32)
extern "C"  void RenderSettings_set_reflectionBounces_m2938034214 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_haloStrength()
extern "C"  float RenderSettings_get_haloStrength_m4164965730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
extern "C"  void RenderSettings_set_haloStrength_m1179623713 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
extern "C"  void RenderSettings_set_flareStrength_m38596007 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m3777670233 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
