﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatClamp
struct  FloatClamp_t1735441703  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatClamp::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatClamp::minValue
	FsmFloat_t2134102846 * ___minValue_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatClamp::maxValue
	FsmFloat_t2134102846 * ___maxValue_11;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatClamp::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_floatVariable_9() { return static_cast<int32_t>(offsetof(FloatClamp_t1735441703, ___floatVariable_9)); }
	inline FsmFloat_t2134102846 * get_floatVariable_9() const { return ___floatVariable_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_9() { return &___floatVariable_9; }
	inline void set_floatVariable_9(FsmFloat_t2134102846 * value)
	{
		___floatVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_9, value);
	}

	inline static int32_t get_offset_of_minValue_10() { return static_cast<int32_t>(offsetof(FloatClamp_t1735441703, ___minValue_10)); }
	inline FsmFloat_t2134102846 * get_minValue_10() const { return ___minValue_10; }
	inline FsmFloat_t2134102846 ** get_address_of_minValue_10() { return &___minValue_10; }
	inline void set_minValue_10(FsmFloat_t2134102846 * value)
	{
		___minValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___minValue_10, value);
	}

	inline static int32_t get_offset_of_maxValue_11() { return static_cast<int32_t>(offsetof(FloatClamp_t1735441703, ___maxValue_11)); }
	inline FsmFloat_t2134102846 * get_maxValue_11() const { return ___maxValue_11; }
	inline FsmFloat_t2134102846 ** get_address_of_maxValue_11() { return &___maxValue_11; }
	inline void set_maxValue_11(FsmFloat_t2134102846 * value)
	{
		___maxValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___maxValue_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(FloatClamp_t1735441703, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
