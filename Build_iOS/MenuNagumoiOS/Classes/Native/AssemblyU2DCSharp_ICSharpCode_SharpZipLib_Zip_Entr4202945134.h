﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.EntryPatchData
struct  EntryPatchData_t4202945134  : public Il2CppObject
{
public:
	// System.Int64 ICSharpCode.SharpZipLib.Zip.EntryPatchData::sizePatchOffset_
	int64_t ___sizePatchOffset__0;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.EntryPatchData::crcPatchOffset_
	int64_t ___crcPatchOffset__1;

public:
	inline static int32_t get_offset_of_sizePatchOffset__0() { return static_cast<int32_t>(offsetof(EntryPatchData_t4202945134, ___sizePatchOffset__0)); }
	inline int64_t get_sizePatchOffset__0() const { return ___sizePatchOffset__0; }
	inline int64_t* get_address_of_sizePatchOffset__0() { return &___sizePatchOffset__0; }
	inline void set_sizePatchOffset__0(int64_t value)
	{
		___sizePatchOffset__0 = value;
	}

	inline static int32_t get_offset_of_crcPatchOffset__1() { return static_cast<int32_t>(offsetof(EntryPatchData_t4202945134, ___crcPatchOffset__1)); }
	inline int64_t get_crcPatchOffset__1() const { return ___crcPatchOffset__1; }
	inline int64_t* get_address_of_crcPatchOffset__1() { return &___crcPatchOffset__1; }
	inline void set_crcPatchOffset__1(int64_t value)
	{
		___crcPatchOffset__1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
