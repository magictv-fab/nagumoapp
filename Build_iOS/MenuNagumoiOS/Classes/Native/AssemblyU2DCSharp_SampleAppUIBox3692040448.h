﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;

#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUIBox
struct  SampleAppUIBox_t3692040448  : public ISampleAppUIElement_t2180050874
{
public:
	// UnityEngine.Rect SampleAppUIBox::mRect
	Rect_t4241904616  ___mRect_0;
	// UnityEngine.GUIStyle SampleAppUIBox::mStyle
	GUIStyle_t2990928826 * ___mStyle_1;

public:
	inline static int32_t get_offset_of_mRect_0() { return static_cast<int32_t>(offsetof(SampleAppUIBox_t3692040448, ___mRect_0)); }
	inline Rect_t4241904616  get_mRect_0() const { return ___mRect_0; }
	inline Rect_t4241904616 * get_address_of_mRect_0() { return &___mRect_0; }
	inline void set_mRect_0(Rect_t4241904616  value)
	{
		___mRect_0 = value;
	}

	inline static int32_t get_offset_of_mStyle_1() { return static_cast<int32_t>(offsetof(SampleAppUIBox_t3692040448, ___mStyle_1)); }
	inline GUIStyle_t2990928826 * get_mStyle_1() const { return ___mStyle_1; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyle_1() { return &___mStyle_1; }
	inline void set_mStyle_1(GUIStyle_t2990928826 * value)
	{
		___mStyle_1 = value;
		Il2CppCodeGenWriteBarrier(&___mStyle_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
