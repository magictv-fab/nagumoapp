﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OSNotificationAction
struct OSNotificationAction_t173272069;
// OSNotification
struct OSNotification_t892481775;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSNotificationOpenedResult
struct  OSNotificationOpenedResult_t158818421  : public Il2CppObject
{
public:
	// OSNotificationAction OSNotificationOpenedResult::action
	OSNotificationAction_t173272069 * ___action_0;
	// OSNotification OSNotificationOpenedResult::notification
	OSNotification_t892481775 * ___notification_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(OSNotificationOpenedResult_t158818421, ___action_0)); }
	inline OSNotificationAction_t173272069 * get_action_0() const { return ___action_0; }
	inline OSNotificationAction_t173272069 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(OSNotificationAction_t173272069 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_notification_1() { return static_cast<int32_t>(offsetof(OSNotificationOpenedResult_t158818421, ___notification_1)); }
	inline OSNotification_t892481775 * get_notification_1() const { return ___notification_1; }
	inline OSNotification_t892481775 ** get_address_of_notification_1() { return &___notification_1; }
	inline void set_notification_1(OSNotification_t892481775 * value)
	{
		___notification_1 = value;
		Il2CppCodeGenWriteBarrier(&___notification_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
