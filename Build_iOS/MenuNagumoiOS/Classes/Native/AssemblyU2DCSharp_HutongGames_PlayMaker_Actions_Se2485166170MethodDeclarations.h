﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRandomMaterial
struct SetRandomMaterial_t2485166170;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::.ctor()
extern "C"  void SetRandomMaterial__ctor_m3310404124 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::Reset()
extern "C"  void SetRandomMaterial_Reset_m956837065 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::OnEnter()
extern "C"  void SetRandomMaterial_OnEnter_m2414292723 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::DoSetRandomMaterial()
extern "C"  void SetRandomMaterial_DoSetRandomMaterial_m3026882043 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
