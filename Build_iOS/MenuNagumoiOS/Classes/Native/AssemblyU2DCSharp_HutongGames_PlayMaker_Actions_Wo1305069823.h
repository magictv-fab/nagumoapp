﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WorldToScreenPoint
struct  WorldToScreenPoint_t1305069823  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldPosition
	FsmVector3_t533912882 * ___worldPosition_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldX
	FsmFloat_t2134102846 * ___worldX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldY
	FsmFloat_t2134102846 * ___worldY_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldZ
	FsmFloat_t2134102846 * ___worldZ_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.WorldToScreenPoint::storeScreenPoint
	FsmVector3_t533912882 * ___storeScreenPoint_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::storeScreenX
	FsmFloat_t2134102846 * ___storeScreenX_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::storeScreenY
	FsmFloat_t2134102846 * ___storeScreenY_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.WorldToScreenPoint::normalize
	FsmBool_t1075959796 * ___normalize_16;
	// System.Boolean HutongGames.PlayMaker.Actions.WorldToScreenPoint::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_worldPosition_9() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___worldPosition_9)); }
	inline FsmVector3_t533912882 * get_worldPosition_9() const { return ___worldPosition_9; }
	inline FsmVector3_t533912882 ** get_address_of_worldPosition_9() { return &___worldPosition_9; }
	inline void set_worldPosition_9(FsmVector3_t533912882 * value)
	{
		___worldPosition_9 = value;
		Il2CppCodeGenWriteBarrier(&___worldPosition_9, value);
	}

	inline static int32_t get_offset_of_worldX_10() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___worldX_10)); }
	inline FsmFloat_t2134102846 * get_worldX_10() const { return ___worldX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_worldX_10() { return &___worldX_10; }
	inline void set_worldX_10(FsmFloat_t2134102846 * value)
	{
		___worldX_10 = value;
		Il2CppCodeGenWriteBarrier(&___worldX_10, value);
	}

	inline static int32_t get_offset_of_worldY_11() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___worldY_11)); }
	inline FsmFloat_t2134102846 * get_worldY_11() const { return ___worldY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_worldY_11() { return &___worldY_11; }
	inline void set_worldY_11(FsmFloat_t2134102846 * value)
	{
		___worldY_11 = value;
		Il2CppCodeGenWriteBarrier(&___worldY_11, value);
	}

	inline static int32_t get_offset_of_worldZ_12() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___worldZ_12)); }
	inline FsmFloat_t2134102846 * get_worldZ_12() const { return ___worldZ_12; }
	inline FsmFloat_t2134102846 ** get_address_of_worldZ_12() { return &___worldZ_12; }
	inline void set_worldZ_12(FsmFloat_t2134102846 * value)
	{
		___worldZ_12 = value;
		Il2CppCodeGenWriteBarrier(&___worldZ_12, value);
	}

	inline static int32_t get_offset_of_storeScreenPoint_13() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___storeScreenPoint_13)); }
	inline FsmVector3_t533912882 * get_storeScreenPoint_13() const { return ___storeScreenPoint_13; }
	inline FsmVector3_t533912882 ** get_address_of_storeScreenPoint_13() { return &___storeScreenPoint_13; }
	inline void set_storeScreenPoint_13(FsmVector3_t533912882 * value)
	{
		___storeScreenPoint_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeScreenPoint_13, value);
	}

	inline static int32_t get_offset_of_storeScreenX_14() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___storeScreenX_14)); }
	inline FsmFloat_t2134102846 * get_storeScreenX_14() const { return ___storeScreenX_14; }
	inline FsmFloat_t2134102846 ** get_address_of_storeScreenX_14() { return &___storeScreenX_14; }
	inline void set_storeScreenX_14(FsmFloat_t2134102846 * value)
	{
		___storeScreenX_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeScreenX_14, value);
	}

	inline static int32_t get_offset_of_storeScreenY_15() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___storeScreenY_15)); }
	inline FsmFloat_t2134102846 * get_storeScreenY_15() const { return ___storeScreenY_15; }
	inline FsmFloat_t2134102846 ** get_address_of_storeScreenY_15() { return &___storeScreenY_15; }
	inline void set_storeScreenY_15(FsmFloat_t2134102846 * value)
	{
		___storeScreenY_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeScreenY_15, value);
	}

	inline static int32_t get_offset_of_normalize_16() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___normalize_16)); }
	inline FsmBool_t1075959796 * get_normalize_16() const { return ___normalize_16; }
	inline FsmBool_t1075959796 ** get_address_of_normalize_16() { return &___normalize_16; }
	inline void set_normalize_16(FsmBool_t1075959796 * value)
	{
		___normalize_16 = value;
		Il2CppCodeGenWriteBarrier(&___normalize_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t1305069823, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
