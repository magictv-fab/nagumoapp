﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragMe
struct DragMe_t2055054860;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void DragMe::.ctor()
extern "C"  void DragMe__ctor_m1630915599 (DragMe_t2055054860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_OnBeginDrag_m2789565139 (DragMe_t2055054860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_OnDrag_m1039302966 (DragMe_t2055054860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::SetDraggedPosition(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_SetDraggedPosition_m796108560 (DragMe_t2055054860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMe::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragMe_OnEndDrag_m3411249441 (DragMe_t2055054860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
