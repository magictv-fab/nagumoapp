﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric
struct DecodedNumeric_t133612409;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void DecodedNumeric__ctor_m738288825 (DecodedNumeric_t133612409 * __this, int32_t ___newPosition0, int32_t ___firstDigit1, int32_t ___secondDigit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getFirstDigit()
extern "C"  int32_t DecodedNumeric_getFirstDigit_m671604327 (DecodedNumeric_t133612409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getSecondDigit()
extern "C"  int32_t DecodedNumeric_getSecondDigit_m752255153 (DecodedNumeric_t133612409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::isFirstDigitFNC1()
extern "C"  bool DecodedNumeric_isFirstDigitFNC1_m513836389 (DecodedNumeric_t133612409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::isSecondDigitFNC1()
extern "C"  bool DecodedNumeric_isSecondDigitFNC1_m339971807 (DecodedNumeric_t133612409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::.cctor()
extern "C"  void DecodedNumeric__cctor_m2736501093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
