﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Raycast
struct  Raycast_t2801925751  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Raycast::fromGameObject
	FsmOwnerDefault_t251897112 * ___fromGameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::fromPosition
	FsmVector3_t533912882 * ___fromPosition_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::direction
	FsmVector3_t533912882 * ___direction_11;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.Raycast::space
	int32_t ___space_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Raycast::distance
	FsmFloat_t2134102846 * ___distance_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Raycast::hitEvent
	FsmEvent_t2133468028 * ___hitEvent_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Raycast::storeDidHit
	FsmBool_t1075959796 * ___storeDidHit_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.Raycast::storeHitObject
	FsmGameObject_t1697147867 * ___storeHitObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::storeHitPoint
	FsmVector3_t533912882 * ___storeHitPoint_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::storeHitNormal
	FsmVector3_t533912882 * ___storeHitNormal_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Raycast::storeHitDistance
	FsmFloat_t2134102846 * ___storeHitDistance_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Raycast::repeatInterval
	FsmInt_t1596138449 * ___repeatInterval_20;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.Raycast::layerMask
	FsmIntU5BU5D_t1976821196* ___layerMask_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Raycast::invertMask
	FsmBool_t1075959796 * ___invertMask_22;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.Raycast::debugColor
	FsmColor_t2131419205 * ___debugColor_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Raycast::debug
	FsmBool_t1075959796 * ___debug_24;
	// System.Int32 HutongGames.PlayMaker.Actions.Raycast::repeat
	int32_t ___repeat_25;

public:
	inline static int32_t get_offset_of_fromGameObject_9() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___fromGameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_fromGameObject_9() const { return ___fromGameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_fromGameObject_9() { return &___fromGameObject_9; }
	inline void set_fromGameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___fromGameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___fromGameObject_9, value);
	}

	inline static int32_t get_offset_of_fromPosition_10() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___fromPosition_10)); }
	inline FsmVector3_t533912882 * get_fromPosition_10() const { return ___fromPosition_10; }
	inline FsmVector3_t533912882 ** get_address_of_fromPosition_10() { return &___fromPosition_10; }
	inline void set_fromPosition_10(FsmVector3_t533912882 * value)
	{
		___fromPosition_10 = value;
		Il2CppCodeGenWriteBarrier(&___fromPosition_10, value);
	}

	inline static int32_t get_offset_of_direction_11() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___direction_11)); }
	inline FsmVector3_t533912882 * get_direction_11() const { return ___direction_11; }
	inline FsmVector3_t533912882 ** get_address_of_direction_11() { return &___direction_11; }
	inline void set_direction_11(FsmVector3_t533912882 * value)
	{
		___direction_11 = value;
		Il2CppCodeGenWriteBarrier(&___direction_11, value);
	}

	inline static int32_t get_offset_of_space_12() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___space_12)); }
	inline int32_t get_space_12() const { return ___space_12; }
	inline int32_t* get_address_of_space_12() { return &___space_12; }
	inline void set_space_12(int32_t value)
	{
		___space_12 = value;
	}

	inline static int32_t get_offset_of_distance_13() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___distance_13)); }
	inline FsmFloat_t2134102846 * get_distance_13() const { return ___distance_13; }
	inline FsmFloat_t2134102846 ** get_address_of_distance_13() { return &___distance_13; }
	inline void set_distance_13(FsmFloat_t2134102846 * value)
	{
		___distance_13 = value;
		Il2CppCodeGenWriteBarrier(&___distance_13, value);
	}

	inline static int32_t get_offset_of_hitEvent_14() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___hitEvent_14)); }
	inline FsmEvent_t2133468028 * get_hitEvent_14() const { return ___hitEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_hitEvent_14() { return &___hitEvent_14; }
	inline void set_hitEvent_14(FsmEvent_t2133468028 * value)
	{
		___hitEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___hitEvent_14, value);
	}

	inline static int32_t get_offset_of_storeDidHit_15() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___storeDidHit_15)); }
	inline FsmBool_t1075959796 * get_storeDidHit_15() const { return ___storeDidHit_15; }
	inline FsmBool_t1075959796 ** get_address_of_storeDidHit_15() { return &___storeDidHit_15; }
	inline void set_storeDidHit_15(FsmBool_t1075959796 * value)
	{
		___storeDidHit_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeDidHit_15, value);
	}

	inline static int32_t get_offset_of_storeHitObject_16() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___storeHitObject_16)); }
	inline FsmGameObject_t1697147867 * get_storeHitObject_16() const { return ___storeHitObject_16; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeHitObject_16() { return &___storeHitObject_16; }
	inline void set_storeHitObject_16(FsmGameObject_t1697147867 * value)
	{
		___storeHitObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitObject_16, value);
	}

	inline static int32_t get_offset_of_storeHitPoint_17() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___storeHitPoint_17)); }
	inline FsmVector3_t533912882 * get_storeHitPoint_17() const { return ___storeHitPoint_17; }
	inline FsmVector3_t533912882 ** get_address_of_storeHitPoint_17() { return &___storeHitPoint_17; }
	inline void set_storeHitPoint_17(FsmVector3_t533912882 * value)
	{
		___storeHitPoint_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitPoint_17, value);
	}

	inline static int32_t get_offset_of_storeHitNormal_18() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___storeHitNormal_18)); }
	inline FsmVector3_t533912882 * get_storeHitNormal_18() const { return ___storeHitNormal_18; }
	inline FsmVector3_t533912882 ** get_address_of_storeHitNormal_18() { return &___storeHitNormal_18; }
	inline void set_storeHitNormal_18(FsmVector3_t533912882 * value)
	{
		___storeHitNormal_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitNormal_18, value);
	}

	inline static int32_t get_offset_of_storeHitDistance_19() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___storeHitDistance_19)); }
	inline FsmFloat_t2134102846 * get_storeHitDistance_19() const { return ___storeHitDistance_19; }
	inline FsmFloat_t2134102846 ** get_address_of_storeHitDistance_19() { return &___storeHitDistance_19; }
	inline void set_storeHitDistance_19(FsmFloat_t2134102846 * value)
	{
		___storeHitDistance_19 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitDistance_19, value);
	}

	inline static int32_t get_offset_of_repeatInterval_20() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___repeatInterval_20)); }
	inline FsmInt_t1596138449 * get_repeatInterval_20() const { return ___repeatInterval_20; }
	inline FsmInt_t1596138449 ** get_address_of_repeatInterval_20() { return &___repeatInterval_20; }
	inline void set_repeatInterval_20(FsmInt_t1596138449 * value)
	{
		___repeatInterval_20 = value;
		Il2CppCodeGenWriteBarrier(&___repeatInterval_20, value);
	}

	inline static int32_t get_offset_of_layerMask_21() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___layerMask_21)); }
	inline FsmIntU5BU5D_t1976821196* get_layerMask_21() const { return ___layerMask_21; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_layerMask_21() { return &___layerMask_21; }
	inline void set_layerMask_21(FsmIntU5BU5D_t1976821196* value)
	{
		___layerMask_21 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_21, value);
	}

	inline static int32_t get_offset_of_invertMask_22() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___invertMask_22)); }
	inline FsmBool_t1075959796 * get_invertMask_22() const { return ___invertMask_22; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_22() { return &___invertMask_22; }
	inline void set_invertMask_22(FsmBool_t1075959796 * value)
	{
		___invertMask_22 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_22, value);
	}

	inline static int32_t get_offset_of_debugColor_23() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___debugColor_23)); }
	inline FsmColor_t2131419205 * get_debugColor_23() const { return ___debugColor_23; }
	inline FsmColor_t2131419205 ** get_address_of_debugColor_23() { return &___debugColor_23; }
	inline void set_debugColor_23(FsmColor_t2131419205 * value)
	{
		___debugColor_23 = value;
		Il2CppCodeGenWriteBarrier(&___debugColor_23, value);
	}

	inline static int32_t get_offset_of_debug_24() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___debug_24)); }
	inline FsmBool_t1075959796 * get_debug_24() const { return ___debug_24; }
	inline FsmBool_t1075959796 ** get_address_of_debug_24() { return &___debug_24; }
	inline void set_debug_24(FsmBool_t1075959796 * value)
	{
		___debug_24 = value;
		Il2CppCodeGenWriteBarrier(&___debug_24, value);
	}

	inline static int32_t get_offset_of_repeat_25() { return static_cast<int32_t>(offsetof(Raycast_t2801925751, ___repeat_25)); }
	inline int32_t get_repeat_25() const { return ___repeat_25; }
	inline int32_t* get_address_of_repeat_25() { return &___repeat_25; }
	inline void set_repeat_25(int32_t value)
	{
		___repeat_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
