﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConsultaCep
struct ConsultaCep_t794160633;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ConsultaCep::.ctor()
extern "C"  void ConsultaCep__ctor_m748631442 (ConsultaCep_t794160633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConsultaCep::BuscaCep(System.String)
extern "C"  String_t* ConsultaCep_BuscaCep_m2523104611 (ConsultaCep_t794160633 * __this, String_t* ___cep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
