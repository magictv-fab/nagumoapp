﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t2012439129;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_PDF417_Internal_Compaction2012764861.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::.cctor()
extern "C"  void PDF417HighLevelEncoder__cctor_m3343690505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeHighLevel(System.String,ZXing.PDF417.Internal.Compaction,System.Text.Encoding,System.Boolean)
extern "C"  String_t* PDF417HighLevelEncoder_encodeHighLevel_m3507259969 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___compaction1, Encoding_t2012439129 * ___encoding2, bool ___disableEci3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::toBytes(System.String,System.Text.Encoding)
extern "C"  ByteU5BU5D_t4260760469* PDF417HighLevelEncoder_toBytes_m1471054327 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, Encoding_t2012439129 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeText(System.String,System.Int32,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t PDF417HighLevelEncoder_encodeText_m3571420418 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___startpos1, int32_t ___count2, StringBuilder_t243639308 * ___sb3, int32_t ___initialSubmode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeBinary(System.Byte[],System.Int32,System.Int32,System.Int32,System.Text.StringBuilder)
extern "C"  void PDF417HighLevelEncoder_encodeBinary_m483251305 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startpos1, int32_t ___count2, int32_t ___startmode3, StringBuilder_t243639308 * ___sb4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeNumeric(System.String,System.Int32,System.Int32,System.Text.StringBuilder)
extern "C"  void PDF417HighLevelEncoder_encodeNumeric_m3070418157 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___startpos1, int32_t ___count2, StringBuilder_t243639308 * ___sb3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isDigit(System.Char)
extern "C"  bool PDF417HighLevelEncoder_isDigit_m2530605356 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isAlphaUpper(System.Char)
extern "C"  bool PDF417HighLevelEncoder_isAlphaUpper_m240493563 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isAlphaLower(System.Char)
extern "C"  bool PDF417HighLevelEncoder_isAlphaLower_m4287095804 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isMixed(System.Char)
extern "C"  bool PDF417HighLevelEncoder_isMixed_m4116473118 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isPunctuation(System.Char)
extern "C"  bool PDF417HighLevelEncoder_isPunctuation_m1416144991 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isText(System.Char)
extern "C"  bool PDF417HighLevelEncoder_isText_m2875995538 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveDigitCount(System.String,System.Int32)
extern "C"  int32_t PDF417HighLevelEncoder_determineConsecutiveDigitCount_m3128349244 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___startpos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveTextCount(System.String,System.Int32)
extern "C"  int32_t PDF417HighLevelEncoder_determineConsecutiveTextCount_m2283157684 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___startpos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveBinaryCount(System.String,System.Byte[],System.Int32)
extern "C"  int32_t PDF417HighLevelEncoder_determineConsecutiveBinaryCount_m3982859645 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startpos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodingECI(System.Int32,System.Text.StringBuilder)
extern "C"  void PDF417HighLevelEncoder_encodingECI_m1831363983 (Il2CppObject * __this /* static, unused */, int32_t ___eci0, StringBuilder_t243639308 * ___sb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
