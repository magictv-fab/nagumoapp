﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmVariable
struct  SetFsmVariable_t311321858  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmVariable::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVariable::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVariable::variableName
	FsmString_t952858651 * ___variableName_11;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.SetFsmVariable::setValue
	FsmVar_t1596150537 * ___setValue_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmVariable::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmVariable::cachedGO
	GameObject_t3674682005 * ___cachedGO_14;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmVariable::sourceFsm
	PlayMakerFSM_t3799847376 * ___sourceFsm_15;
	// HutongGames.PlayMaker.INamedVariable HutongGames.PlayMaker.Actions.SetFsmVariable::sourceVariable
	Il2CppObject * ___sourceVariable_16;
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.Actions.SetFsmVariable::targetVariable
	NamedVariable_t3211770239 * ___targetVariable_17;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_variableName_11() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___variableName_11)); }
	inline FsmString_t952858651 * get_variableName_11() const { return ___variableName_11; }
	inline FsmString_t952858651 ** get_address_of_variableName_11() { return &___variableName_11; }
	inline void set_variableName_11(FsmString_t952858651 * value)
	{
		___variableName_11 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_11, value);
	}

	inline static int32_t get_offset_of_setValue_12() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___setValue_12)); }
	inline FsmVar_t1596150537 * get_setValue_12() const { return ___setValue_12; }
	inline FsmVar_t1596150537 ** get_address_of_setValue_12() { return &___setValue_12; }
	inline void set_setValue_12(FsmVar_t1596150537 * value)
	{
		___setValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___setValue_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_cachedGO_14() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___cachedGO_14)); }
	inline GameObject_t3674682005 * get_cachedGO_14() const { return ___cachedGO_14; }
	inline GameObject_t3674682005 ** get_address_of_cachedGO_14() { return &___cachedGO_14; }
	inline void set_cachedGO_14(GameObject_t3674682005 * value)
	{
		___cachedGO_14 = value;
		Il2CppCodeGenWriteBarrier(&___cachedGO_14, value);
	}

	inline static int32_t get_offset_of_sourceFsm_15() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___sourceFsm_15)); }
	inline PlayMakerFSM_t3799847376 * get_sourceFsm_15() const { return ___sourceFsm_15; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_sourceFsm_15() { return &___sourceFsm_15; }
	inline void set_sourceFsm_15(PlayMakerFSM_t3799847376 * value)
	{
		___sourceFsm_15 = value;
		Il2CppCodeGenWriteBarrier(&___sourceFsm_15, value);
	}

	inline static int32_t get_offset_of_sourceVariable_16() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___sourceVariable_16)); }
	inline Il2CppObject * get_sourceVariable_16() const { return ___sourceVariable_16; }
	inline Il2CppObject ** get_address_of_sourceVariable_16() { return &___sourceVariable_16; }
	inline void set_sourceVariable_16(Il2CppObject * value)
	{
		___sourceVariable_16 = value;
		Il2CppCodeGenWriteBarrier(&___sourceVariable_16, value);
	}

	inline static int32_t get_offset_of_targetVariable_17() { return static_cast<int32_t>(offsetof(SetFsmVariable_t311321858, ___targetVariable_17)); }
	inline NamedVariable_t3211770239 * get_targetVariable_17() const { return ___targetVariable_17; }
	inline NamedVariable_t3211770239 ** get_address_of_targetVariable_17() { return &___targetVariable_17; }
	inline void set_targetVariable_17(NamedVariable_t3211770239 * value)
	{
		___targetVariable_17 = value;
		Il2CppCodeGenWriteBarrier(&___targetVariable_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
