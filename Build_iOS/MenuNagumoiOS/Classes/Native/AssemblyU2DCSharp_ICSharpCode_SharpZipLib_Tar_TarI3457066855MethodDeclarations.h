﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarInputStream/EntryFactoryAdapter
struct EntryFactoryAdapter_t3457066855;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarInputStream/EntryFactoryAdapter::.ctor()
extern "C"  void EntryFactoryAdapter__ctor_m435026196 (EntryFactoryAdapter_t3457066855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarInputStream/EntryFactoryAdapter::CreateEntry(System.String)
extern "C"  TarEntry_t762185187 * EntryFactoryAdapter_CreateEntry_m2453730800 (EntryFactoryAdapter_t3457066855 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarInputStream/EntryFactoryAdapter::CreateEntryFromFile(System.String)
extern "C"  TarEntry_t762185187 * EntryFactoryAdapter_CreateEntryFromFile_m1439145418 (EntryFactoryAdapter_t3457066855 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarInputStream/EntryFactoryAdapter::CreateEntry(System.Byte[])
extern "C"  TarEntry_t762185187 * EntryFactoryAdapter_CreateEntry_m395925175 (EntryFactoryAdapter_t3457066855 * __this, ByteU5BU5D_t4260760469* ___headerBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
