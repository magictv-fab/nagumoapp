﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveAdd
struct iTweenMoveAdd_t1025491740;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::.ctor()
extern "C"  void iTweenMoveAdd__ctor_m286477722 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::Reset()
extern "C"  void iTweenMoveAdd_Reset_m2227877959 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::OnEnter()
extern "C"  void iTweenMoveAdd_OnEnter_m4113879793 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::OnExit()
extern "C"  void iTweenMoveAdd_OnExit_m1804193415 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::DoiTween()
extern "C"  void iTweenMoveAdd_DoiTween_m132386935 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
