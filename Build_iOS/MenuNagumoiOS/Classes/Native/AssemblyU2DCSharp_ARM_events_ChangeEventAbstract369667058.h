﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.events.ChangeEventAbstract/OnChangeEventHandler
struct OnChangeEventHandler_t2629944389;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.ChangeEventAbstract
struct  ChangeEventAbstract_t369667058  : public MonoBehaviour_t667441552
{
public:
	// ARM.events.ChangeEventAbstract/OnChangeEventHandler ARM.events.ChangeEventAbstract::onChangeEvent
	OnChangeEventHandler_t2629944389 * ___onChangeEvent_2;

public:
	inline static int32_t get_offset_of_onChangeEvent_2() { return static_cast<int32_t>(offsetof(ChangeEventAbstract_t369667058, ___onChangeEvent_2)); }
	inline OnChangeEventHandler_t2629944389 * get_onChangeEvent_2() const { return ___onChangeEvent_2; }
	inline OnChangeEventHandler_t2629944389 ** get_address_of_onChangeEvent_2() { return &___onChangeEvent_2; }
	inline void set_onChangeEvent_2(OnChangeEventHandler_t2629944389 * value)
	{
		___onChangeEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeEvent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
