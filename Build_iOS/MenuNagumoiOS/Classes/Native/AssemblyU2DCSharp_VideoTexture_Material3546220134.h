﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// AttachedAudio
struct AttachedAudio_t711869554;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_DigitsLocation3189585627.h"
#include "AssemblyU2DCSharp_UVT_PlayMode874588163.h"
#include "AssemblyU2DCSharp_TextureType3281615573.h"
#include "AssemblyU2DCSharp_LowMemoryMode1214027928.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_UVT_PlayState1348117041.h"
#include "AssemblyU2DCSharp_UVT_PlayDirection3288864031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoTexture_Material
struct  VideoTexture_Material_t3546220134  : public MonoBehaviour_t667441552
{
public:
	// System.Single VideoTexture_Material::FPS
	float ___FPS_2;
	// System.Int32 VideoTexture_Material::firstFrame
	int32_t ___firstFrame_3;
	// System.Int32 VideoTexture_Material::lastFrame
	int32_t ___lastFrame_4;
	// System.String VideoTexture_Material::FileName
	String_t* ___FileName_5;
	// System.String VideoTexture_Material::digitsFormat
	String_t* ___digitsFormat_6;
	// DigitsLocation VideoTexture_Material::digitsLocation
	int32_t ___digitsLocation_7;
	// UVT_PlayMode VideoTexture_Material::playMode
	int32_t ___playMode_8;
	// System.Boolean VideoTexture_Material::pingpongStartsWithReverse
	bool ___pingpongStartsWithReverse_9;
	// System.Int32 VideoTexture_Material::numberOfLoops
	int32_t ___numberOfLoops_10;
	// TextureType VideoTexture_Material::textureType
	int32_t ___textureType_11;
	// LowMemoryMode VideoTexture_Material::lowMemoryMode
	int32_t ___lowMemoryMode_12;
	// UnityEngine.GameObject VideoTexture_Material::scrollBar
	GameObject_t3674682005 * ___scrollBar_13;
	// UnityEngine.GameObject VideoTexture_Material::CTI
	GameObject_t3674682005 * ___CTI_14;
	// UnityEngine.TextMesh VideoTexture_Material::timeCode
	TextMesh_t2567681854 * ___timeCode_15;
	// UnityEngine.LayerMask VideoTexture_Material::controlLayer
	LayerMask_t3236759763  ___controlLayer_16;
	// UVT_PlayState VideoTexture_Material::playState
	int32_t ___playState_17;
	// UVT_PlayDirection VideoTexture_Material::playDirection
	int32_t ___playDirection_18;
	// System.Boolean VideoTexture_Material::sharedMaterial
	bool ___sharedMaterial_19;
	// System.Boolean VideoTexture_Material::enableAudio
	bool ___enableAudio_20;
	// System.Boolean VideoTexture_Material::forceAudioSync
	bool ___forceAudioSync_21;
	// System.Boolean VideoTexture_Material::enableControls
	bool ___enableControls_22;
	// System.Boolean VideoTexture_Material::autoPlay
	bool ___autoPlay_23;
	// System.Boolean VideoTexture_Material::autoDestruct
	bool ___autoDestruct_24;
	// System.Boolean VideoTexture_Material::autoLoadLevelWhenDone
	bool ___autoLoadLevelWhenDone_25;
	// System.String VideoTexture_Material::LevelToLoad
	String_t* ___LevelToLoad_26;
	// System.Single VideoTexture_Material::currentPosition
	float ___currentPosition_27;
	// System.Single VideoTexture_Material::ctiPosition
	float ___ctiPosition_28;
	// System.Int32 VideoTexture_Material::currentLoop
	int32_t ___currentLoop_29;
	// System.Single VideoTexture_Material::scrollBarLength
	float ___scrollBarLength_30;
	// System.Boolean VideoTexture_Material::scrubbing
	bool ___scrubbing_31;
	// System.Boolean VideoTexture_Material::audioAttached
	bool ___audioAttached_32;
	// AttachedAudio VideoTexture_Material::myAudio
	AttachedAudio_t711869554 * ___myAudio_33;
	// System.String VideoTexture_Material::texType
	String_t* ___texType_34;
	// System.String VideoTexture_Material::indexStr
	String_t* ___indexStr_35;
	// UnityEngine.Texture VideoTexture_Material::newTex
	Texture_t2526458961 * ___newTex_36;
	// UnityEngine.Texture VideoTexture_Material::lastTex
	Texture_t2526458961 * ___lastTex_37;
	// System.Single VideoTexture_Material::playFactor
	float ___playFactor_38;
	// System.Single VideoTexture_Material::index
	float ___index_39;
	// System.Int32 VideoTexture_Material::intIndex
	int32_t ___intIndex_40;
	// System.Int32 VideoTexture_Material::lastIndex
	int32_t ___lastIndex_41;

public:
	inline static int32_t get_offset_of_FPS_2() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___FPS_2)); }
	inline float get_FPS_2() const { return ___FPS_2; }
	inline float* get_address_of_FPS_2() { return &___FPS_2; }
	inline void set_FPS_2(float value)
	{
		___FPS_2 = value;
	}

	inline static int32_t get_offset_of_firstFrame_3() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___firstFrame_3)); }
	inline int32_t get_firstFrame_3() const { return ___firstFrame_3; }
	inline int32_t* get_address_of_firstFrame_3() { return &___firstFrame_3; }
	inline void set_firstFrame_3(int32_t value)
	{
		___firstFrame_3 = value;
	}

	inline static int32_t get_offset_of_lastFrame_4() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___lastFrame_4)); }
	inline int32_t get_lastFrame_4() const { return ___lastFrame_4; }
	inline int32_t* get_address_of_lastFrame_4() { return &___lastFrame_4; }
	inline void set_lastFrame_4(int32_t value)
	{
		___lastFrame_4 = value;
	}

	inline static int32_t get_offset_of_FileName_5() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___FileName_5)); }
	inline String_t* get_FileName_5() const { return ___FileName_5; }
	inline String_t** get_address_of_FileName_5() { return &___FileName_5; }
	inline void set_FileName_5(String_t* value)
	{
		___FileName_5 = value;
		Il2CppCodeGenWriteBarrier(&___FileName_5, value);
	}

	inline static int32_t get_offset_of_digitsFormat_6() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___digitsFormat_6)); }
	inline String_t* get_digitsFormat_6() const { return ___digitsFormat_6; }
	inline String_t** get_address_of_digitsFormat_6() { return &___digitsFormat_6; }
	inline void set_digitsFormat_6(String_t* value)
	{
		___digitsFormat_6 = value;
		Il2CppCodeGenWriteBarrier(&___digitsFormat_6, value);
	}

	inline static int32_t get_offset_of_digitsLocation_7() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___digitsLocation_7)); }
	inline int32_t get_digitsLocation_7() const { return ___digitsLocation_7; }
	inline int32_t* get_address_of_digitsLocation_7() { return &___digitsLocation_7; }
	inline void set_digitsLocation_7(int32_t value)
	{
		___digitsLocation_7 = value;
	}

	inline static int32_t get_offset_of_playMode_8() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___playMode_8)); }
	inline int32_t get_playMode_8() const { return ___playMode_8; }
	inline int32_t* get_address_of_playMode_8() { return &___playMode_8; }
	inline void set_playMode_8(int32_t value)
	{
		___playMode_8 = value;
	}

	inline static int32_t get_offset_of_pingpongStartsWithReverse_9() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___pingpongStartsWithReverse_9)); }
	inline bool get_pingpongStartsWithReverse_9() const { return ___pingpongStartsWithReverse_9; }
	inline bool* get_address_of_pingpongStartsWithReverse_9() { return &___pingpongStartsWithReverse_9; }
	inline void set_pingpongStartsWithReverse_9(bool value)
	{
		___pingpongStartsWithReverse_9 = value;
	}

	inline static int32_t get_offset_of_numberOfLoops_10() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___numberOfLoops_10)); }
	inline int32_t get_numberOfLoops_10() const { return ___numberOfLoops_10; }
	inline int32_t* get_address_of_numberOfLoops_10() { return &___numberOfLoops_10; }
	inline void set_numberOfLoops_10(int32_t value)
	{
		___numberOfLoops_10 = value;
	}

	inline static int32_t get_offset_of_textureType_11() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___textureType_11)); }
	inline int32_t get_textureType_11() const { return ___textureType_11; }
	inline int32_t* get_address_of_textureType_11() { return &___textureType_11; }
	inline void set_textureType_11(int32_t value)
	{
		___textureType_11 = value;
	}

	inline static int32_t get_offset_of_lowMemoryMode_12() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___lowMemoryMode_12)); }
	inline int32_t get_lowMemoryMode_12() const { return ___lowMemoryMode_12; }
	inline int32_t* get_address_of_lowMemoryMode_12() { return &___lowMemoryMode_12; }
	inline void set_lowMemoryMode_12(int32_t value)
	{
		___lowMemoryMode_12 = value;
	}

	inline static int32_t get_offset_of_scrollBar_13() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___scrollBar_13)); }
	inline GameObject_t3674682005 * get_scrollBar_13() const { return ___scrollBar_13; }
	inline GameObject_t3674682005 ** get_address_of_scrollBar_13() { return &___scrollBar_13; }
	inline void set_scrollBar_13(GameObject_t3674682005 * value)
	{
		___scrollBar_13 = value;
		Il2CppCodeGenWriteBarrier(&___scrollBar_13, value);
	}

	inline static int32_t get_offset_of_CTI_14() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___CTI_14)); }
	inline GameObject_t3674682005 * get_CTI_14() const { return ___CTI_14; }
	inline GameObject_t3674682005 ** get_address_of_CTI_14() { return &___CTI_14; }
	inline void set_CTI_14(GameObject_t3674682005 * value)
	{
		___CTI_14 = value;
		Il2CppCodeGenWriteBarrier(&___CTI_14, value);
	}

	inline static int32_t get_offset_of_timeCode_15() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___timeCode_15)); }
	inline TextMesh_t2567681854 * get_timeCode_15() const { return ___timeCode_15; }
	inline TextMesh_t2567681854 ** get_address_of_timeCode_15() { return &___timeCode_15; }
	inline void set_timeCode_15(TextMesh_t2567681854 * value)
	{
		___timeCode_15 = value;
		Il2CppCodeGenWriteBarrier(&___timeCode_15, value);
	}

	inline static int32_t get_offset_of_controlLayer_16() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___controlLayer_16)); }
	inline LayerMask_t3236759763  get_controlLayer_16() const { return ___controlLayer_16; }
	inline LayerMask_t3236759763 * get_address_of_controlLayer_16() { return &___controlLayer_16; }
	inline void set_controlLayer_16(LayerMask_t3236759763  value)
	{
		___controlLayer_16 = value;
	}

	inline static int32_t get_offset_of_playState_17() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___playState_17)); }
	inline int32_t get_playState_17() const { return ___playState_17; }
	inline int32_t* get_address_of_playState_17() { return &___playState_17; }
	inline void set_playState_17(int32_t value)
	{
		___playState_17 = value;
	}

	inline static int32_t get_offset_of_playDirection_18() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___playDirection_18)); }
	inline int32_t get_playDirection_18() const { return ___playDirection_18; }
	inline int32_t* get_address_of_playDirection_18() { return &___playDirection_18; }
	inline void set_playDirection_18(int32_t value)
	{
		___playDirection_18 = value;
	}

	inline static int32_t get_offset_of_sharedMaterial_19() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___sharedMaterial_19)); }
	inline bool get_sharedMaterial_19() const { return ___sharedMaterial_19; }
	inline bool* get_address_of_sharedMaterial_19() { return &___sharedMaterial_19; }
	inline void set_sharedMaterial_19(bool value)
	{
		___sharedMaterial_19 = value;
	}

	inline static int32_t get_offset_of_enableAudio_20() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___enableAudio_20)); }
	inline bool get_enableAudio_20() const { return ___enableAudio_20; }
	inline bool* get_address_of_enableAudio_20() { return &___enableAudio_20; }
	inline void set_enableAudio_20(bool value)
	{
		___enableAudio_20 = value;
	}

	inline static int32_t get_offset_of_forceAudioSync_21() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___forceAudioSync_21)); }
	inline bool get_forceAudioSync_21() const { return ___forceAudioSync_21; }
	inline bool* get_address_of_forceAudioSync_21() { return &___forceAudioSync_21; }
	inline void set_forceAudioSync_21(bool value)
	{
		___forceAudioSync_21 = value;
	}

	inline static int32_t get_offset_of_enableControls_22() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___enableControls_22)); }
	inline bool get_enableControls_22() const { return ___enableControls_22; }
	inline bool* get_address_of_enableControls_22() { return &___enableControls_22; }
	inline void set_enableControls_22(bool value)
	{
		___enableControls_22 = value;
	}

	inline static int32_t get_offset_of_autoPlay_23() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___autoPlay_23)); }
	inline bool get_autoPlay_23() const { return ___autoPlay_23; }
	inline bool* get_address_of_autoPlay_23() { return &___autoPlay_23; }
	inline void set_autoPlay_23(bool value)
	{
		___autoPlay_23 = value;
	}

	inline static int32_t get_offset_of_autoDestruct_24() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___autoDestruct_24)); }
	inline bool get_autoDestruct_24() const { return ___autoDestruct_24; }
	inline bool* get_address_of_autoDestruct_24() { return &___autoDestruct_24; }
	inline void set_autoDestruct_24(bool value)
	{
		___autoDestruct_24 = value;
	}

	inline static int32_t get_offset_of_autoLoadLevelWhenDone_25() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___autoLoadLevelWhenDone_25)); }
	inline bool get_autoLoadLevelWhenDone_25() const { return ___autoLoadLevelWhenDone_25; }
	inline bool* get_address_of_autoLoadLevelWhenDone_25() { return &___autoLoadLevelWhenDone_25; }
	inline void set_autoLoadLevelWhenDone_25(bool value)
	{
		___autoLoadLevelWhenDone_25 = value;
	}

	inline static int32_t get_offset_of_LevelToLoad_26() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___LevelToLoad_26)); }
	inline String_t* get_LevelToLoad_26() const { return ___LevelToLoad_26; }
	inline String_t** get_address_of_LevelToLoad_26() { return &___LevelToLoad_26; }
	inline void set_LevelToLoad_26(String_t* value)
	{
		___LevelToLoad_26 = value;
		Il2CppCodeGenWriteBarrier(&___LevelToLoad_26, value);
	}

	inline static int32_t get_offset_of_currentPosition_27() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___currentPosition_27)); }
	inline float get_currentPosition_27() const { return ___currentPosition_27; }
	inline float* get_address_of_currentPosition_27() { return &___currentPosition_27; }
	inline void set_currentPosition_27(float value)
	{
		___currentPosition_27 = value;
	}

	inline static int32_t get_offset_of_ctiPosition_28() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___ctiPosition_28)); }
	inline float get_ctiPosition_28() const { return ___ctiPosition_28; }
	inline float* get_address_of_ctiPosition_28() { return &___ctiPosition_28; }
	inline void set_ctiPosition_28(float value)
	{
		___ctiPosition_28 = value;
	}

	inline static int32_t get_offset_of_currentLoop_29() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___currentLoop_29)); }
	inline int32_t get_currentLoop_29() const { return ___currentLoop_29; }
	inline int32_t* get_address_of_currentLoop_29() { return &___currentLoop_29; }
	inline void set_currentLoop_29(int32_t value)
	{
		___currentLoop_29 = value;
	}

	inline static int32_t get_offset_of_scrollBarLength_30() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___scrollBarLength_30)); }
	inline float get_scrollBarLength_30() const { return ___scrollBarLength_30; }
	inline float* get_address_of_scrollBarLength_30() { return &___scrollBarLength_30; }
	inline void set_scrollBarLength_30(float value)
	{
		___scrollBarLength_30 = value;
	}

	inline static int32_t get_offset_of_scrubbing_31() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___scrubbing_31)); }
	inline bool get_scrubbing_31() const { return ___scrubbing_31; }
	inline bool* get_address_of_scrubbing_31() { return &___scrubbing_31; }
	inline void set_scrubbing_31(bool value)
	{
		___scrubbing_31 = value;
	}

	inline static int32_t get_offset_of_audioAttached_32() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___audioAttached_32)); }
	inline bool get_audioAttached_32() const { return ___audioAttached_32; }
	inline bool* get_address_of_audioAttached_32() { return &___audioAttached_32; }
	inline void set_audioAttached_32(bool value)
	{
		___audioAttached_32 = value;
	}

	inline static int32_t get_offset_of_myAudio_33() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___myAudio_33)); }
	inline AttachedAudio_t711869554 * get_myAudio_33() const { return ___myAudio_33; }
	inline AttachedAudio_t711869554 ** get_address_of_myAudio_33() { return &___myAudio_33; }
	inline void set_myAudio_33(AttachedAudio_t711869554 * value)
	{
		___myAudio_33 = value;
		Il2CppCodeGenWriteBarrier(&___myAudio_33, value);
	}

	inline static int32_t get_offset_of_texType_34() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___texType_34)); }
	inline String_t* get_texType_34() const { return ___texType_34; }
	inline String_t** get_address_of_texType_34() { return &___texType_34; }
	inline void set_texType_34(String_t* value)
	{
		___texType_34 = value;
		Il2CppCodeGenWriteBarrier(&___texType_34, value);
	}

	inline static int32_t get_offset_of_indexStr_35() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___indexStr_35)); }
	inline String_t* get_indexStr_35() const { return ___indexStr_35; }
	inline String_t** get_address_of_indexStr_35() { return &___indexStr_35; }
	inline void set_indexStr_35(String_t* value)
	{
		___indexStr_35 = value;
		Il2CppCodeGenWriteBarrier(&___indexStr_35, value);
	}

	inline static int32_t get_offset_of_newTex_36() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___newTex_36)); }
	inline Texture_t2526458961 * get_newTex_36() const { return ___newTex_36; }
	inline Texture_t2526458961 ** get_address_of_newTex_36() { return &___newTex_36; }
	inline void set_newTex_36(Texture_t2526458961 * value)
	{
		___newTex_36 = value;
		Il2CppCodeGenWriteBarrier(&___newTex_36, value);
	}

	inline static int32_t get_offset_of_lastTex_37() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___lastTex_37)); }
	inline Texture_t2526458961 * get_lastTex_37() const { return ___lastTex_37; }
	inline Texture_t2526458961 ** get_address_of_lastTex_37() { return &___lastTex_37; }
	inline void set_lastTex_37(Texture_t2526458961 * value)
	{
		___lastTex_37 = value;
		Il2CppCodeGenWriteBarrier(&___lastTex_37, value);
	}

	inline static int32_t get_offset_of_playFactor_38() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___playFactor_38)); }
	inline float get_playFactor_38() const { return ___playFactor_38; }
	inline float* get_address_of_playFactor_38() { return &___playFactor_38; }
	inline void set_playFactor_38(float value)
	{
		___playFactor_38 = value;
	}

	inline static int32_t get_offset_of_index_39() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___index_39)); }
	inline float get_index_39() const { return ___index_39; }
	inline float* get_address_of_index_39() { return &___index_39; }
	inline void set_index_39(float value)
	{
		___index_39 = value;
	}

	inline static int32_t get_offset_of_intIndex_40() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___intIndex_40)); }
	inline int32_t get_intIndex_40() const { return ___intIndex_40; }
	inline int32_t* get_address_of_intIndex_40() { return &___intIndex_40; }
	inline void set_intIndex_40(int32_t value)
	{
		___intIndex_40 = value;
	}

	inline static int32_t get_offset_of_lastIndex_41() { return static_cast<int32_t>(offsetof(VideoTexture_Material_t3546220134, ___lastIndex_41)); }
	inline int32_t get_lastIndex_41() const { return ___lastIndex_41; }
	inline int32_t* get_address_of_lastIndex_41() { return &___lastIndex_41; }
	inline void set_lastIndex_41(int32_t value)
	{
		___lastIndex_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
