﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// ZXing.Dimension
struct Dimension_t1395692488;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolShapeHint2380532246.h"
#include "QRCode_ZXing_Dimension1395692488.h"

// System.Char ZXing.Datamatrix.Encoder.HighLevelEncoder::randomize253State(System.Char,System.Int32)
extern "C"  Il2CppChar HighLevelEncoder_randomize253State_m3682802830 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, int32_t ___codewordPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.HighLevelEncoder::encodeHighLevel(System.String,ZXing.Datamatrix.Encoder.SymbolShapeHint,ZXing.Dimension,ZXing.Dimension,System.Int32)
extern "C"  String_t* HighLevelEncoder_encodeHighLevel_m316886629 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___shape1, Dimension_t1395692488 * ___minSize2, Dimension_t1395692488 * ___maxSize3, int32_t ___defaultEncodation4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::lookAheadTest(System.String,System.Int32,System.Int32)
extern "C"  int32_t HighLevelEncoder_lookAheadTest_m1700218268 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___startpos1, int32_t ___currentMode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::findMinimums(System.Single[],System.Int32[],System.Int32,System.Byte[])
extern "C"  int32_t HighLevelEncoder_findMinimums_m2329997792 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___charCounts0, Int32U5BU5D_t3230847821* ___intCharCounts1, int32_t ___min2, ByteU5BU5D_t4260760469* ___mins3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::getMinimumCount(System.Byte[])
extern "C"  int32_t HighLevelEncoder_getMinimumCount_m1153594336 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___mins0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isDigit(System.Char)
extern "C"  bool HighLevelEncoder_isDigit_m3186512834 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isExtendedASCII(System.Char)
extern "C"  bool HighLevelEncoder_isExtendedASCII_m3947342391 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeC40(System.Char)
extern "C"  bool HighLevelEncoder_isNativeC40_m1361768647 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeText(System.Char)
extern "C"  bool HighLevelEncoder_isNativeText_m1416344357 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeX12(System.Char)
extern "C"  bool HighLevelEncoder_isNativeX12_m2286647437 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isX12TermSep(System.Char)
extern "C"  bool HighLevelEncoder_isX12TermSep_m3862944176 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeEDIFACT(System.Char)
extern "C"  bool HighLevelEncoder_isNativeEDIFACT_m3184071216 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isSpecialB256(System.Char)
extern "C"  bool HighLevelEncoder_isSpecialB256_m4040688325 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::determineConsecutiveDigitCount(System.String,System.Int32)
extern "C"  int32_t HighLevelEncoder_determineConsecutiveDigitCount_m3756672990 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, int32_t ___startpos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.HighLevelEncoder::illegalCharacter(System.Char)
extern "C"  void HighLevelEncoder_illegalCharacter_m1095469612 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
