﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.FixedCameraBehavior
struct FixedCameraBehavior_t4278156937;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.SimpleEvents.FixedCameraBehavior::.ctor()
extern "C"  void FixedCameraBehavior__ctor_m2827097249 (FixedCameraBehavior_t4278156937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.SimpleEvents.FixedCameraBehavior InApp.SimpleEvents.FixedCameraBehavior::GetInstance()
extern "C"  FixedCameraBehavior_t4278156937 * FixedCameraBehavior_GetInstance_m2391998055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InApp.SimpleEvents.FixedCameraBehavior::get_isCameraFixed()
extern "C"  bool FixedCameraBehavior_get_isCameraFixed_m3787419975 (FixedCameraBehavior_t4278156937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::set_isCameraFixed(System.Boolean)
extern "C"  void FixedCameraBehavior_set_isCameraFixed_m1665839806 (FixedCameraBehavior_t4278156937 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::Start()
extern "C"  void FixedCameraBehavior_Start_m1774235041 (FixedCameraBehavior_t4278156937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 InApp.SimpleEvents.FixedCameraBehavior::cloneVector3(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  FixedCameraBehavior_cloneVector3_m2555086601 (FixedCameraBehavior_t4278156937 * __this, Vector3_t4282066566  ___vec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::onToggleArOn(System.String)
extern "C"  void FixedCameraBehavior_onToggleArOn_m2280634492 (FixedCameraBehavior_t4278156937 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::onToggleArOut(System.String)
extern "C"  void FixedCameraBehavior_onToggleArOut_m1429439801 (FixedCameraBehavior_t4278156937 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::turnOn(System.String)
extern "C"  void FixedCameraBehavior_turnOn_m4014949699 (FixedCameraBehavior_t4278156937 * __this, String_t* ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::changeSmoothMode(System.Boolean)
extern "C"  void FixedCameraBehavior_changeSmoothMode_m2561616859 (FixedCameraBehavior_t4278156937 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::configByString(System.String)
extern "C"  void FixedCameraBehavior_configByString_m1580506133 (FixedCameraBehavior_t4278156937 * __this, String_t* ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::CloseLockedPresentation()
extern "C"  void FixedCameraBehavior_CloseLockedPresentation_m88011419 (FixedCameraBehavior_t4278156937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.FixedCameraBehavior::Reset()
extern "C"  void FixedCameraBehavior_Reset_m473530190 (FixedCameraBehavior_t4278156937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
