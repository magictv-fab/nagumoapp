﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>
struct ShimEnumerator_t4003355771;
// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>
struct Dictionary_2_t4287577744;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2694716770_gshared (ShimEnumerator_t4003355771 * __this, Dictionary_2_t4287577744 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2694716770(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4003355771 *, Dictionary_2_t4287577744 *, const MethodInfo*))ShimEnumerator__ctor_m2694716770_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m594395263_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m594395263(__this, method) ((  bool (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))ShimEnumerator_MoveNext_m594395263_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2009877557_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2009877557(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))ShimEnumerator_get_Entry_m2009877557_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2489925456_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2489925456(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))ShimEnumerator_get_Key_m2489925456_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1570015714_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1570015714(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))ShimEnumerator_get_Value_m1570015714_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2676271082_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2676271082(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))ShimEnumerator_get_Current_m2676271082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2317075636_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2317075636(__this, method) ((  void (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))ShimEnumerator_Reset_m2317075636_gshared)(__this, method)
