﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Text.Encoding
struct Encoding_t2012439129;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// Cadastro
struct Cadastro_t3948724313;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cadastro/<IBuscaCep>c__Iterator49
struct  U3CIBuscaCepU3Ec__Iterator49_t2084848971  : public Il2CppObject
{
public:
	// System.String Cadastro/<IBuscaCep>c__Iterator49::cep
	String_t* ___cep_0;
	// UnityEngine.WWW Cadastro/<IBuscaCep>c__Iterator49::<wwwCep>__0
	WWW_t3134621005 * ___U3CwwwCepU3E__0_1;
	// System.Text.Encoding Cadastro/<IBuscaCep>c__Iterator49::<iso>__1
	Encoding_t2012439129 * ___U3CisoU3E__1_2;
	// System.Text.Encoding Cadastro/<IBuscaCep>c__Iterator49::<utf8>__2
	Encoding_t2012439129 * ___U3Cutf8U3E__2_3;
	// System.Byte[] Cadastro/<IBuscaCep>c__Iterator49::<isoBytes>__3
	ByteU5BU5D_t4260760469* ___U3CisoBytesU3E__3_4;
	// System.String Cadastro/<IBuscaCep>c__Iterator49::<result>__4
	String_t* ___U3CresultU3E__4_5;
	// System.String[] Cadastro/<IBuscaCep>c__Iterator49::<resp>__5
	StringU5BU5D_t4054002952* ___U3CrespU3E__5_6;
	// System.String[] Cadastro/<IBuscaCep>c__Iterator49::<$s_302>__6
	StringU5BU5D_t4054002952* ___U3CU24s_302U3E__6_7;
	// System.Int32 Cadastro/<IBuscaCep>c__Iterator49::<$s_303>__7
	int32_t ___U3CU24s_303U3E__7_8;
	// System.String Cadastro/<IBuscaCep>c__Iterator49::<field>__8
	String_t* ___U3CfieldU3E__8_9;
	// System.String Cadastro/<IBuscaCep>c__Iterator49::<chave>__9
	String_t* ___U3CchaveU3E__9_10;
	// System.String Cadastro/<IBuscaCep>c__Iterator49::<valor>__10
	String_t* ___U3CvalorU3E__10_11;
	// System.Int32 Cadastro/<IBuscaCep>c__Iterator49::$PC
	int32_t ___U24PC_12;
	// System.Object Cadastro/<IBuscaCep>c__Iterator49::$current
	Il2CppObject * ___U24current_13;
	// System.String Cadastro/<IBuscaCep>c__Iterator49::<$>cep
	String_t* ___U3CU24U3Ecep_14;
	// Cadastro Cadastro/<IBuscaCep>c__Iterator49::<>f__this
	Cadastro_t3948724313 * ___U3CU3Ef__this_15;

public:
	inline static int32_t get_offset_of_cep_0() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___cep_0)); }
	inline String_t* get_cep_0() const { return ___cep_0; }
	inline String_t** get_address_of_cep_0() { return &___cep_0; }
	inline void set_cep_0(String_t* value)
	{
		___cep_0 = value;
		Il2CppCodeGenWriteBarrier(&___cep_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwCepU3E__0_1() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CwwwCepU3E__0_1)); }
	inline WWW_t3134621005 * get_U3CwwwCepU3E__0_1() const { return ___U3CwwwCepU3E__0_1; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwCepU3E__0_1() { return &___U3CwwwCepU3E__0_1; }
	inline void set_U3CwwwCepU3E__0_1(WWW_t3134621005 * value)
	{
		___U3CwwwCepU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwCepU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CisoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CisoU3E__1_2)); }
	inline Encoding_t2012439129 * get_U3CisoU3E__1_2() const { return ___U3CisoU3E__1_2; }
	inline Encoding_t2012439129 ** get_address_of_U3CisoU3E__1_2() { return &___U3CisoU3E__1_2; }
	inline void set_U3CisoU3E__1_2(Encoding_t2012439129 * value)
	{
		___U3CisoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CisoU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3Cutf8U3E__2_3() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3Cutf8U3E__2_3)); }
	inline Encoding_t2012439129 * get_U3Cutf8U3E__2_3() const { return ___U3Cutf8U3E__2_3; }
	inline Encoding_t2012439129 ** get_address_of_U3Cutf8U3E__2_3() { return &___U3Cutf8U3E__2_3; }
	inline void set_U3Cutf8U3E__2_3(Encoding_t2012439129 * value)
	{
		___U3Cutf8U3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cutf8U3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CisoBytesU3E__3_4() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CisoBytesU3E__3_4)); }
	inline ByteU5BU5D_t4260760469* get_U3CisoBytesU3E__3_4() const { return ___U3CisoBytesU3E__3_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CisoBytesU3E__3_4() { return &___U3CisoBytesU3E__3_4; }
	inline void set_U3CisoBytesU3E__3_4(ByteU5BU5D_t4260760469* value)
	{
		___U3CisoBytesU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CisoBytesU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CresultU3E__4_5() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CresultU3E__4_5)); }
	inline String_t* get_U3CresultU3E__4_5() const { return ___U3CresultU3E__4_5; }
	inline String_t** get_address_of_U3CresultU3E__4_5() { return &___U3CresultU3E__4_5; }
	inline void set_U3CresultU3E__4_5(String_t* value)
	{
		___U3CresultU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresultU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CrespU3E__5_6() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CrespU3E__5_6)); }
	inline StringU5BU5D_t4054002952* get_U3CrespU3E__5_6() const { return ___U3CrespU3E__5_6; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CrespU3E__5_6() { return &___U3CrespU3E__5_6; }
	inline void set_U3CrespU3E__5_6(StringU5BU5D_t4054002952* value)
	{
		___U3CrespU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrespU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CU24s_302U3E__6_7() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CU24s_302U3E__6_7)); }
	inline StringU5BU5D_t4054002952* get_U3CU24s_302U3E__6_7() const { return ___U3CU24s_302U3E__6_7; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU24s_302U3E__6_7() { return &___U3CU24s_302U3E__6_7; }
	inline void set_U3CU24s_302U3E__6_7(StringU5BU5D_t4054002952* value)
	{
		___U3CU24s_302U3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_302U3E__6_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_303U3E__7_8() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CU24s_303U3E__7_8)); }
	inline int32_t get_U3CU24s_303U3E__7_8() const { return ___U3CU24s_303U3E__7_8; }
	inline int32_t* get_address_of_U3CU24s_303U3E__7_8() { return &___U3CU24s_303U3E__7_8; }
	inline void set_U3CU24s_303U3E__7_8(int32_t value)
	{
		___U3CU24s_303U3E__7_8 = value;
	}

	inline static int32_t get_offset_of_U3CfieldU3E__8_9() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CfieldU3E__8_9)); }
	inline String_t* get_U3CfieldU3E__8_9() const { return ___U3CfieldU3E__8_9; }
	inline String_t** get_address_of_U3CfieldU3E__8_9() { return &___U3CfieldU3E__8_9; }
	inline void set_U3CfieldU3E__8_9(String_t* value)
	{
		___U3CfieldU3E__8_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfieldU3E__8_9, value);
	}

	inline static int32_t get_offset_of_U3CchaveU3E__9_10() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CchaveU3E__9_10)); }
	inline String_t* get_U3CchaveU3E__9_10() const { return ___U3CchaveU3E__9_10; }
	inline String_t** get_address_of_U3CchaveU3E__9_10() { return &___U3CchaveU3E__9_10; }
	inline void set_U3CchaveU3E__9_10(String_t* value)
	{
		___U3CchaveU3E__9_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchaveU3E__9_10, value);
	}

	inline static int32_t get_offset_of_U3CvalorU3E__10_11() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CvalorU3E__10_11)); }
	inline String_t* get_U3CvalorU3E__10_11() const { return ___U3CvalorU3E__10_11; }
	inline String_t** get_address_of_U3CvalorU3E__10_11() { return &___U3CvalorU3E__10_11; }
	inline void set_U3CvalorU3E__10_11(String_t* value)
	{
		___U3CvalorU3E__10_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvalorU3E__10_11, value);
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecep_14() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CU24U3Ecep_14)); }
	inline String_t* get_U3CU24U3Ecep_14() const { return ___U3CU24U3Ecep_14; }
	inline String_t** get_address_of_U3CU24U3Ecep_14() { return &___U3CU24U3Ecep_14; }
	inline void set_U3CU24U3Ecep_14(String_t* value)
	{
		___U3CU24U3Ecep_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecep_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_15() { return static_cast<int32_t>(offsetof(U3CIBuscaCepU3Ec__Iterator49_t2084848971, ___U3CU3Ef__this_15)); }
	inline Cadastro_t3948724313 * get_U3CU3Ef__this_15() const { return ___U3CU3Ef__this_15; }
	inline Cadastro_t3948724313 ** get_address_of_U3CU3Ef__this_15() { return &___U3CU3Ef__this_15; }
	inline void set_U3CU3Ef__this_15(Cadastro_t3948724313 * value)
	{
		___U3CU3Ef__this_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
