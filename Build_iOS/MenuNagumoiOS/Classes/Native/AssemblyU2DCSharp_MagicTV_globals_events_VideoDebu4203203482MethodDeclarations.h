﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler
struct OnMetaEventHandler_t4203203482;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnMetaEventHandler__ctor_m2756514113 (OnMetaEventHandler_t4203203482 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler::Invoke(System.String,System.String,System.Single,System.Single)
extern "C"  void OnMetaEventHandler_Invoke_m2471832717 (OnMetaEventHandler_t4203203482 * __this, String_t* ___bundleID0, String_t* ___materialName1, float ___float12, float ___float23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler::BeginInvoke(System.String,System.String,System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnMetaEventHandler_BeginInvoke_m3679581958 (OnMetaEventHandler_t4203203482 * __this, String_t* ___bundleID0, String_t* ___materialName1, float ___float12, float ___float23, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnMetaEventHandler_EndInvoke_m2559921105 (OnMetaEventHandler_t4203203482 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
