﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoTexture_FullScreen
struct VideoTexture_FullScreen_t1562162522;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UVT_PlayDirection3288864031.h"
#include "AssemblyU2DCSharp_UVT_PlayState1348117041.h"

// System.Void VideoTexture_FullScreen::.ctor()
extern "C"  void VideoTexture_FullScreen__ctor_m658444113 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::Awake()
extern "C"  void VideoTexture_FullScreen_Awake_m896049332 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::Start()
extern "C"  void VideoTexture_FullScreen_Start_m3900549201 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::Update()
extern "C"  void VideoTexture_FullScreen_Update_m663793116 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::OnGUI()
extern "C"  void VideoTexture_FullScreen_OnGUI_m153842763 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::HandleControls()
extern "C"  void VideoTexture_FullScreen_HandleControls_m2619751121 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::ForceAudioSync()
extern "C"  void VideoTexture_FullScreen_ForceAudioSync_m1226534073 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::UpdatePlayFactor()
extern "C"  void VideoTexture_FullScreen_UpdatePlayFactor_m1728334111 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::EnableControls(System.Boolean)
extern "C"  void VideoTexture_FullScreen_EnableControls_m3758769379 (VideoTexture_FullScreen_t1562162522 * __this, bool ___EnableControls0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::UpdateAudio()
extern "C"  void VideoTexture_FullScreen_UpdateAudio_m1914454844 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::Sync()
extern "C"  void VideoTexture_FullScreen_Sync_m1793380270 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::Play()
extern "C"  void VideoTexture_FullScreen_Play_m1695120903 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::Stop()
extern "C"  void VideoTexture_FullScreen_Stop_m1788804949 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::TogglePlay()
extern "C"  void VideoTexture_FullScreen_TogglePlay_m881160443 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_FullScreen::ChangeDirection(UVT_PlayDirection)
extern "C"  void VideoTexture_FullScreen_ChangeDirection_m3546931925 (VideoTexture_FullScreen_t1562162522 * __this, int32_t ___newUVT_PlayDirection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UVT_PlayState VideoTexture_FullScreen::CurrentUVT_PlayState()
extern "C"  int32_t VideoTexture_FullScreen_CurrentUVT_PlayState_m377364009 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UVT_PlayDirection VideoTexture_FullScreen::CurrentUVT_PlayDirection()
extern "C"  int32_t VideoTexture_FullScreen_CurrentUVT_PlayDirection_m3097347973 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VideoTexture_FullScreen::CurrentPosition()
extern "C"  float VideoTexture_FullScreen_CurrentPosition_m869247485 (VideoTexture_FullScreen_t1562162522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
