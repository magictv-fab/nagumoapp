﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveRoletas/<Go>c__Iterator17
struct U3CGoU3Ec__Iterator17_t2914471256;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ActiveRoletas/<Go>c__Iterator17::.ctor()
extern "C"  void U3CGoU3Ec__Iterator17__ctor_m3595250963 (U3CGoU3Ec__Iterator17_t2914471256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActiveRoletas/<Go>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2813676575 (U3CGoU3Ec__Iterator17_t2914471256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActiveRoletas/<Go>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m946433459 (U3CGoU3Ec__Iterator17_t2914471256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveRoletas/<Go>c__Iterator17::MoveNext()
extern "C"  bool U3CGoU3Ec__Iterator17_MoveNext_m3944903681 (U3CGoU3Ec__Iterator17_t2914471256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveRoletas/<Go>c__Iterator17::Dispose()
extern "C"  void U3CGoU3Ec__Iterator17_Dispose_m1780752528 (U3CGoU3Ec__Iterator17_t2914471256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveRoletas/<Go>c__Iterator17::Reset()
extern "C"  void U3CGoU3Ec__Iterator17_Reset_m1241683904 (U3CGoU3Ec__Iterator17_t2914471256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
