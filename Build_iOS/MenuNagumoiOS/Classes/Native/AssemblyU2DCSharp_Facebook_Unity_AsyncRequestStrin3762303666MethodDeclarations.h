﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A
struct U3CStartU3Ec__Iterator2A_t3762303666;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A::.ctor()
extern "C"  void U3CStartU3Ec__Iterator2A__ctor_m1953401385 (U3CStartU3Ec__Iterator2A_t3762303666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator2A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m272630163 (U3CStartU3Ec__Iterator2A_t3762303666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator2A_System_Collections_IEnumerator_get_Current_m768969511 (U3CStartU3Ec__Iterator2A_t3762303666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator2A_MoveNext_m421134355 (U3CStartU3Ec__Iterator2A_t3762303666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A::Dispose()
extern "C"  void U3CStartU3Ec__Iterator2A_Dispose_m216305702 (U3CStartU3Ec__Iterator2A_t3762303666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AsyncRequestString/<Start>c__Iterator2A::Reset()
extern "C"  void U3CStartU3Ec__Iterator2A_Reset_m3894801622 (U3CStartU3Ec__Iterator2A_t3762303666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
