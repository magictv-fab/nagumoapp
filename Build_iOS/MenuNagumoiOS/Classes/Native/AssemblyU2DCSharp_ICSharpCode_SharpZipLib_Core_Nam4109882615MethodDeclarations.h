﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.NameAndSizeFilter
struct NameAndSizeFilter_t4109882615;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::.ctor(System.String,System.Int64,System.Int64)
extern "C"  void NameAndSizeFilter__ctor_m3863805870 (NameAndSizeFilter_t4109882615 * __this, String_t* ___filter0, int64_t ___minSize1, int64_t ___maxSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::IsMatch(System.String)
extern "C"  bool NameAndSizeFilter_IsMatch_m2696150441 (NameAndSizeFilter_t4109882615 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::get_MinSize()
extern "C"  int64_t NameAndSizeFilter_get_MinSize_m471599501 (NameAndSizeFilter_t4109882615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::set_MinSize(System.Int64)
extern "C"  void NameAndSizeFilter_set_MinSize_m3893354394 (NameAndSizeFilter_t4109882615 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::get_MaxSize()
extern "C"  int64_t NameAndSizeFilter_get_MaxSize_m3994088223 (NameAndSizeFilter_t4109882615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::set_MaxSize(System.Int64)
extern "C"  void NameAndSizeFilter_set_MaxSize_m3014196780 (NameAndSizeFilter_t4109882615 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
