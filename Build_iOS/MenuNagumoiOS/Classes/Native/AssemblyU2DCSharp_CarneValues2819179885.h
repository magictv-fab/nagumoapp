﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.UI.Image
struct Image_t538875265;
// System.Collections.Generic.List`1<CortesData>
struct List_1_t2010699158;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t1553873098;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CarneValues
struct  CarneValues_t2819179885  : public MonoBehaviour_t667441552
{
public:
	// System.String CarneValues::title
	String_t* ___title_2;
	// System.Int32 CarneValues::id
	int32_t ___id_3;
	// UnityEngine.Texture CarneValues::texture
	Texture_t2526458961 * ___texture_4;
	// UnityEngine.UI.Image CarneValues::img
	Image_t538875265 * ___img_5;
	// System.Collections.Generic.List`1<CortesData> CarneValues::tiposCortesLst
	List_1_t2010699158 * ___tiposCortesLst_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> CarneValues::optionDatas
	List_1_t1553873098 * ___optionDatas_7;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(CarneValues_t2819179885, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(CarneValues_t2819179885, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(CarneValues_t2819179885, ___texture_4)); }
	inline Texture_t2526458961 * get_texture_4() const { return ___texture_4; }
	inline Texture_t2526458961 ** get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(Texture_t2526458961 * value)
	{
		___texture_4 = value;
		Il2CppCodeGenWriteBarrier(&___texture_4, value);
	}

	inline static int32_t get_offset_of_img_5() { return static_cast<int32_t>(offsetof(CarneValues_t2819179885, ___img_5)); }
	inline Image_t538875265 * get_img_5() const { return ___img_5; }
	inline Image_t538875265 ** get_address_of_img_5() { return &___img_5; }
	inline void set_img_5(Image_t538875265 * value)
	{
		___img_5 = value;
		Il2CppCodeGenWriteBarrier(&___img_5, value);
	}

	inline static int32_t get_offset_of_tiposCortesLst_6() { return static_cast<int32_t>(offsetof(CarneValues_t2819179885, ___tiposCortesLst_6)); }
	inline List_1_t2010699158 * get_tiposCortesLst_6() const { return ___tiposCortesLst_6; }
	inline List_1_t2010699158 ** get_address_of_tiposCortesLst_6() { return &___tiposCortesLst_6; }
	inline void set_tiposCortesLst_6(List_1_t2010699158 * value)
	{
		___tiposCortesLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___tiposCortesLst_6, value);
	}

	inline static int32_t get_offset_of_optionDatas_7() { return static_cast<int32_t>(offsetof(CarneValues_t2819179885, ___optionDatas_7)); }
	inline List_1_t1553873098 * get_optionDatas_7() const { return ___optionDatas_7; }
	inline List_1_t1553873098 ** get_address_of_optionDatas_7() { return &___optionDatas_7; }
	inline void set_optionDatas_7(List_1_t1553873098 * value)
	{
		___optionDatas_7 = value;
		Il2CppCodeGenWriteBarrier(&___optionDatas_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
