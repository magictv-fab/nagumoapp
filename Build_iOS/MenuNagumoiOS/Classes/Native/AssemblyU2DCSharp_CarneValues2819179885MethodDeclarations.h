﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CarneValues
struct CarneValues_t2819179885;

#include "codegen/il2cpp-codegen.h"

// System.Void CarneValues::.ctor()
extern "C"  void CarneValues__ctor_m1946484894 (CarneValues_t2819179885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CarneValues::Start()
extern "C"  void CarneValues_Start_m893622686 (CarneValues_t2819179885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CarneValues::Update()
extern "C"  void CarneValues_Update_m1938351663 (CarneValues_t2819179885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
