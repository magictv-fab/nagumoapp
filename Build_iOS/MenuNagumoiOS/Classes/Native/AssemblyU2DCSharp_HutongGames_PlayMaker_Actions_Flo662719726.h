﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1085110523.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatOperator
struct  FloatOperator_t662719726  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatOperator::float1
	FsmFloat_t2134102846 * ___float1_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatOperator::float2
	FsmFloat_t2134102846 * ___float2_10;
	// HutongGames.PlayMaker.Actions.FloatOperator/Operation HutongGames.PlayMaker.Actions.FloatOperator::operation
	int32_t ___operation_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatOperator::storeResult
	FsmFloat_t2134102846 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatOperator::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_float1_9() { return static_cast<int32_t>(offsetof(FloatOperator_t662719726, ___float1_9)); }
	inline FsmFloat_t2134102846 * get_float1_9() const { return ___float1_9; }
	inline FsmFloat_t2134102846 ** get_address_of_float1_9() { return &___float1_9; }
	inline void set_float1_9(FsmFloat_t2134102846 * value)
	{
		___float1_9 = value;
		Il2CppCodeGenWriteBarrier(&___float1_9, value);
	}

	inline static int32_t get_offset_of_float2_10() { return static_cast<int32_t>(offsetof(FloatOperator_t662719726, ___float2_10)); }
	inline FsmFloat_t2134102846 * get_float2_10() const { return ___float2_10; }
	inline FsmFloat_t2134102846 ** get_address_of_float2_10() { return &___float2_10; }
	inline void set_float2_10(FsmFloat_t2134102846 * value)
	{
		___float2_10 = value;
		Il2CppCodeGenWriteBarrier(&___float2_10, value);
	}

	inline static int32_t get_offset_of_operation_11() { return static_cast<int32_t>(offsetof(FloatOperator_t662719726, ___operation_11)); }
	inline int32_t get_operation_11() const { return ___operation_11; }
	inline int32_t* get_address_of_operation_11() { return &___operation_11; }
	inline void set_operation_11(int32_t value)
	{
		___operation_11 = value;
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(FloatOperator_t662719726, ___storeResult_12)); }
	inline FsmFloat_t2134102846 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmFloat_t2134102846 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmFloat_t2134102846 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(FloatOperator_t662719726, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
