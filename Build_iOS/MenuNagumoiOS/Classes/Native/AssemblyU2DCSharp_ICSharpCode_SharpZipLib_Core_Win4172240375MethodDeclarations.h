﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.WindowsPathUtils
struct WindowsPathUtils_t4172240375;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.WindowsPathUtils::.ctor()
extern "C"  void WindowsPathUtils__ctor_m3717737764 (WindowsPathUtils_t4172240375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Core.WindowsPathUtils::DropPathRoot(System.String)
extern "C"  String_t* WindowsPathUtils_DropPathRoot_m2516419407 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
