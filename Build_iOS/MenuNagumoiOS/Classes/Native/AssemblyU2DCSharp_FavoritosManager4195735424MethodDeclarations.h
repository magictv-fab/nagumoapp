﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager
struct FavoritosManager_t4195735424;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void FavoritosManager::.ctor()
extern "C"  void FavoritosManager__ctor_m1218035483 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::.cctor()
extern "C"  void FavoritosManager__cctor_m2917265394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::Start()
extern "C"  void FavoritosManager_Start_m165173275 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::InstantiateNextItem()
extern "C"  void FavoritosManager_InstantiateNextItem_m1193712617 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::SendStatus(System.Int32)
extern "C"  void FavoritosManager_SendStatus_m1456564404 (FavoritosManager_t4195735424 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::Favoritar(System.String)
extern "C"  void FavoritosManager_Favoritar_m1743659599 (FavoritosManager_t4195735424 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::BtnAccept()
extern "C"  void FavoritosManager_BtnAccept_m3732873661 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::BtnRecuse()
extern "C"  void FavoritosManager_BtnRecuse_m790769644 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::DesFavoritar(System.String)
extern "C"  void FavoritosManager_DesFavoritar_m2859880433 (FavoritosManager_t4195735424 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FavoritosManager::ExcluirFavorito(System.String)
extern "C"  Il2CppObject * FavoritosManager_ExcluirFavorito_m1360547945 (FavoritosManager_t4195735424 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FavoritosManager::IFavoritar(System.String)
extern "C"  Il2CppObject * FavoritosManager_IFavoritar_m2183449088 (FavoritosManager_t4195735424 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FavoritosManager::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * FavoritosManager_ISendStatus_m1127930153 (FavoritosManager_t4195735424 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FavoritosManager::ILoadPublish()
extern "C"  Il2CppObject * FavoritosManager_ILoadPublish_m3982043697 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FavoritosManager::ILoadImgs()
extern "C"  Il2CppObject * FavoritosManager_ILoadImgs_m153208272 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FavoritosManager::LoadLoadeds()
extern "C"  Il2CppObject * FavoritosManager_LoadLoadeds_m3714373177 (FavoritosManager_t4195735424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager::PopUp(System.String)
extern "C"  void FavoritosManager_PopUp_m3137978653 (FavoritosManager_t4195735424 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
