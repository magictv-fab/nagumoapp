﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNProxyPool
struct MNProxyPool_t2453359369;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNProxyPool::.ctor()
extern "C"  void MNProxyPool__ctor_m772377218 (MNProxyPool_t2453359369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNProxyPool::CallStatic(System.String,System.String,System.Object[])
extern "C"  void MNProxyPool_CallStatic_m2753467452 (Il2CppObject * __this /* static, unused */, String_t* ___className0, String_t* ___methodName1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
