﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNAndroidDialog
struct MNAndroidDialog_t2972443766;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNAndroidDialog::.ctor()
extern "C"  void MNAndroidDialog__ctor_m1140230069 (MNAndroidDialog_t2972443766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidDialog MNAndroidDialog::Create(System.String,System.String)
extern "C"  MNAndroidDialog_t2972443766 * MNAndroidDialog_Create_m2849891152 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidDialog MNAndroidDialog::Create(System.String,System.String,System.String,System.String)
extern "C"  MNAndroidDialog_t2972443766 * MNAndroidDialog_Create_m3727426248 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidDialog::init()
extern "C"  void MNAndroidDialog_init_m4091039711 (MNAndroidDialog_t2972443766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidDialog::onPopUpCallBack(System.String)
extern "C"  void MNAndroidDialog_onPopUpCallBack_m1297566589 (MNAndroidDialog_t2972443766 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
