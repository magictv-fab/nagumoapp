﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImagesQueue
struct DownloadImagesQueue_t2595091377;
// MagicTV.vo.RequestImagesItemVO[]
struct RequestImagesItemVOU5BU5D_t245365736;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"

// System.Void DownloadImagesQueue::.ctor()
extern "C"  void DownloadImagesQueue__ctor_m1463334618 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::SetDownloadImagesList(MagicTV.vo.RequestImagesItemVO[])
extern "C"  void DownloadImagesQueue_SetDownloadImagesList_m1825815705 (DownloadImagesQueue_t2595091377 * __this, RequestImagesItemVOU5BU5D_t245365736* ___images_url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::prepare()
extern "C"  void DownloadImagesQueue_prepare_m2737242527 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::Play()
extern "C"  void DownloadImagesQueue_Play_m2968011102 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::doStartDownload()
extern "C"  void DownloadImagesQueue_doStartDownload_m1875397591 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::Download(System.String)
extern "C"  void DownloadImagesQueue_Download_m3392070416 (DownloadImagesQueue_t2595091377 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DownloadImagesQueue::FinishDownload(System.String)
extern "C"  Il2CppObject * DownloadImagesQueue_FinishDownload_m2729984005 (DownloadImagesQueue_t2595091377 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::errorDownloadImage(ARM.utils.request.ARMRequestVO)
extern "C"  void DownloadImagesQueue_errorDownloadImage_m1334732699 (DownloadImagesQueue_t2595091377 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::onProgressItem(System.Single)
extern "C"  void DownloadImagesQueue_onProgressItem_m3556123106 (DownloadImagesQueue_t2595091377 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::clearCurrentCachedImages()
extern "C"  void DownloadImagesQueue_clearCurrentCachedImages_m1856310736 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DownloadImagesQueue::GetSplashPathFolder()
extern "C"  String_t* DownloadImagesQueue_GetSplashPathFolder_m2109835339 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::onAssetBundleLoaded(System.Byte[])
extern "C"  void DownloadImagesQueue_onAssetBundleLoaded_m3124901337 (DownloadImagesQueue_t2595091377 * __this, ByteU5BU5D_t4260760469* ___bytesLoaded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] DownloadImagesQueue::GetImagensLoaded()
extern "C"  StringU5BU5D_t4054002952* DownloadImagesQueue_GetImagensLoaded_m1420803928 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::Pause()
extern "C"  void DownloadImagesQueue_Pause_m1517460590 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue::Stop()
extern "C"  void DownloadImagesQueue_Stop_m3061695148 (DownloadImagesQueue_t2595091377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
