﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DontDestroyOnLoad
struct DontDestroyOnLoad_t722253884;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DontDestroyOnLoad::.ctor()
extern "C"  void DontDestroyOnLoad__ctor_m2045012090 (DontDestroyOnLoad_t722253884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DontDestroyOnLoad::Reset()
extern "C"  void DontDestroyOnLoad_Reset_m3986412327 (DontDestroyOnLoad_t722253884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DontDestroyOnLoad::OnEnter()
extern "C"  void DontDestroyOnLoad_OnEnter_m1848292817 (DontDestroyOnLoad_t722253884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
