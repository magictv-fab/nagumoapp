﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com700434209.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayAnimation
struct  PlayAnimation_t3759522942  : public ComponentAction_1_t700434209
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlayAnimation::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PlayAnimation::animName
	FsmString_t952858651 * ___animName_12;
	// UnityEngine.PlayMode HutongGames.PlayMaker.Actions.PlayAnimation::playMode
	int32_t ___playMode_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlayAnimation::blendTime
	FsmFloat_t2134102846 * ___blendTime_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayAnimation::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayAnimation::loopEvent
	FsmEvent_t2133468028 * ___loopEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.PlayAnimation::stopOnExit
	bool ___stopOnExit_17;
	// UnityEngine.AnimationState HutongGames.PlayMaker.Actions.PlayAnimation::anim
	AnimationState_t3682323633 * ___anim_18;
	// System.Single HutongGames.PlayMaker.Actions.PlayAnimation::prevAnimtTime
	float ___prevAnimtTime_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_animName_12() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___animName_12)); }
	inline FsmString_t952858651 * get_animName_12() const { return ___animName_12; }
	inline FsmString_t952858651 ** get_address_of_animName_12() { return &___animName_12; }
	inline void set_animName_12(FsmString_t952858651 * value)
	{
		___animName_12 = value;
		Il2CppCodeGenWriteBarrier(&___animName_12, value);
	}

	inline static int32_t get_offset_of_playMode_13() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___playMode_13)); }
	inline int32_t get_playMode_13() const { return ___playMode_13; }
	inline int32_t* get_address_of_playMode_13() { return &___playMode_13; }
	inline void set_playMode_13(int32_t value)
	{
		___playMode_13 = value;
	}

	inline static int32_t get_offset_of_blendTime_14() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___blendTime_14)); }
	inline FsmFloat_t2134102846 * get_blendTime_14() const { return ___blendTime_14; }
	inline FsmFloat_t2134102846 ** get_address_of_blendTime_14() { return &___blendTime_14; }
	inline void set_blendTime_14(FsmFloat_t2134102846 * value)
	{
		___blendTime_14 = value;
		Il2CppCodeGenWriteBarrier(&___blendTime_14, value);
	}

	inline static int32_t get_offset_of_finishEvent_15() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___finishEvent_15)); }
	inline FsmEvent_t2133468028 * get_finishEvent_15() const { return ___finishEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_15() { return &___finishEvent_15; }
	inline void set_finishEvent_15(FsmEvent_t2133468028 * value)
	{
		___finishEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_15, value);
	}

	inline static int32_t get_offset_of_loopEvent_16() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___loopEvent_16)); }
	inline FsmEvent_t2133468028 * get_loopEvent_16() const { return ___loopEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_loopEvent_16() { return &___loopEvent_16; }
	inline void set_loopEvent_16(FsmEvent_t2133468028 * value)
	{
		___loopEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_16, value);
	}

	inline static int32_t get_offset_of_stopOnExit_17() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___stopOnExit_17)); }
	inline bool get_stopOnExit_17() const { return ___stopOnExit_17; }
	inline bool* get_address_of_stopOnExit_17() { return &___stopOnExit_17; }
	inline void set_stopOnExit_17(bool value)
	{
		___stopOnExit_17 = value;
	}

	inline static int32_t get_offset_of_anim_18() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___anim_18)); }
	inline AnimationState_t3682323633 * get_anim_18() const { return ___anim_18; }
	inline AnimationState_t3682323633 ** get_address_of_anim_18() { return &___anim_18; }
	inline void set_anim_18(AnimationState_t3682323633 * value)
	{
		___anim_18 = value;
		Il2CppCodeGenWriteBarrier(&___anim_18, value);
	}

	inline static int32_t get_offset_of_prevAnimtTime_19() { return static_cast<int32_t>(offsetof(PlayAnimation_t3759522942, ___prevAnimtTime_19)); }
	inline float get_prevAnimtTime_19() const { return ___prevAnimtTime_19; }
	inline float* get_address_of_prevAnimtTime_19() { return &___prevAnimtTime_19; }
	inline void set_prevAnimtTime_19(float value)
	{
		___prevAnimtTime_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
