﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Rotation
struct Rotation_t24343454;

#include "codegen/il2cpp-codegen.h"

// System.Void Rotation::.ctor()
extern "C"  void Rotation__ctor_m3146939581 (Rotation_t24343454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotation::Start()
extern "C"  void Rotation_Start_m2094077373 (Rotation_t24343454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotation::Update()
extern "C"  void Rotation_Update_m497741296 (Rotation_t24343454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
