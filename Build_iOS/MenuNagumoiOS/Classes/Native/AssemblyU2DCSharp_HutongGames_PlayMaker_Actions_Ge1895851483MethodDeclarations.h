﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetStringLength
struct GetStringLength_t1895851483;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetStringLength::.ctor()
extern "C"  void GetStringLength__ctor_m2752198587 (GetStringLength_t1895851483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::Reset()
extern "C"  void GetStringLength_Reset_m398631528 (GetStringLength_t1895851483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::OnEnter()
extern "C"  void GetStringLength_OnEnter_m2849683666 (GetStringLength_t1895851483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::OnUpdate()
extern "C"  void GetStringLength_OnUpdate_m1574407089 (GetStringLength_t1895851483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::DoGetStringLength()
extern "C"  void GetStringLength_DoGetStringLength_m1261640987 (GetStringLength_t1895851483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
