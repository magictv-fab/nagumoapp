﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder
struct AI013103decoder_t3403282884;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AI013103decoder__ctor_m800737728 (AI013103decoder_t3403282884 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern "C"  void AI013103decoder_addWeightCode_m1638244350 (AI013103decoder_t3403282884 * __this, StringBuilder_t243639308 * ___buf0, int32_t ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::checkWeight(System.Int32)
extern "C"  int32_t AI013103decoder_checkWeight_m3453981166 (AI013103decoder_t3403282884 * __this, int32_t ___weight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
