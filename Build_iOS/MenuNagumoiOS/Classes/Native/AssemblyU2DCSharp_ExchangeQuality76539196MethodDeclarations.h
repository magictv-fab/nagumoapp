﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExchangeQuality
struct ExchangeQuality_t76539196;

#include "codegen/il2cpp-codegen.h"

// System.Void ExchangeQuality::.ctor()
extern "C"  void ExchangeQuality__ctor_m1910506927 (ExchangeQuality_t76539196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExchangeQuality ExchangeQuality::GetInstance()
extern "C"  ExchangeQuality_t76539196 * ExchangeQuality_GetInstance_m504988463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExchangeQuality::prepare()
extern "C"  void ExchangeQuality_prepare_m2973101876 (ExchangeQuality_t76539196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExchangeQuality::changeUnityQuality(System.Int32)
extern "C"  void ExchangeQuality_changeUnityQuality_m1537041024 (ExchangeQuality_t76539196 * __this, int32_t ____quality0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExchangeQuality::externalChangeUnityQuality(System.Int32)
extern "C"  void ExchangeQuality_externalChangeUnityQuality_m1682049643 (ExchangeQuality_t76539196 * __this, int32_t ___quality0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExchangeQuality::Play()
extern "C"  void ExchangeQuality_Play_m627131369 (ExchangeQuality_t76539196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExchangeQuality::Stop()
extern "C"  void ExchangeQuality_Stop_m720815415 (ExchangeQuality_t76539196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExchangeQuality::Pause()
extern "C"  void ExchangeQuality_Pause_m1964632899 (ExchangeQuality_t76539196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
