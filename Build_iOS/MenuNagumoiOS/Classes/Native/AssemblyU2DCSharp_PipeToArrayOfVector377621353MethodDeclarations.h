﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// UnityEngine.Vector3[] PipeToArrayOfVector3::Parse(System.String)
extern "C"  Vector3U5BU5D_t215400611* PipeToArrayOfVector3_Parse_m3439755351 (Il2CppObject * __this /* static, unused */, String_t* ___string_with_pipe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
