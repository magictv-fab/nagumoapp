﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSubstring
struct GetSubstring_t4098822595;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSubstring::.ctor()
extern "C"  void GetSubstring__ctor_m2357454851 (GetSubstring_t4098822595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::Reset()
extern "C"  void GetSubstring_Reset_m3887792 (GetSubstring_t4098822595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::OnEnter()
extern "C"  void GetSubstring_OnEnter_m1458075418 (GetSubstring_t4098822595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::OnUpdate()
extern "C"  void GetSubstring_OnUpdate_m1384224361 (GetSubstring_t4098822595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::DoGetSubstring()
extern "C"  void GetSubstring_DoGetSubstring_m2596261671 (GetSubstring_t4098822595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
