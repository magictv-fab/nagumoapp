﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23617918191.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1958687228_gshared (KeyValuePair_2_t3617918191 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1958687228(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3617918191 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1958687228_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2030222956_gshared (KeyValuePair_2_t3617918191 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2030222956(__this, method) ((  int32_t (*) (KeyValuePair_2_t3617918191 *, const MethodInfo*))KeyValuePair_2_get_Key_m2030222956_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3131038381_gshared (KeyValuePair_2_t3617918191 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3131038381(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3617918191 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3131038381_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2456416172_gshared (KeyValuePair_2_t3617918191 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2456416172(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3617918191 *, const MethodInfo*))KeyValuePair_2_get_Value_m2456416172_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1359220781_gshared (KeyValuePair_2_t3617918191 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1359220781(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3617918191 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1359220781_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2460276693_gshared (KeyValuePair_2_t3617918191 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2460276693(__this, method) ((  String_t* (*) (KeyValuePair_2_t3617918191 *, const MethodInfo*))KeyValuePair_2_ToString_m2460276693_gshared)(__this, method)
