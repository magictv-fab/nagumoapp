﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Result
struct Result_t2610723219;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>
struct IDictionary_2_t2712902339;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "mscorlib_System_Object4170816371.h"

// System.String ZXing.Result::get_Text()
extern "C"  String_t* Result_get_Text_m2162198985 (Result_t2610723219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::set_Text(System.String)
extern "C"  void Result_set_Text_m3764316968 (Result_t2610723219 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.Result::get_RawBytes()
extern "C"  ByteU5BU5D_t4260760469* Result_get_RawBytes_m3803392774 (Result_t2610723219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::set_RawBytes(System.Byte[])
extern "C"  void Result_set_RawBytes_m1603873593 (Result_t2610723219 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Result::get_ResultPoints()
extern "C"  ResultPointU5BU5D_t1195164344* Result_get_ResultPoints_m3257302689 (Result_t2610723219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::set_ResultPoints(ZXing.ResultPoint[])
extern "C"  void Result_set_ResultPoints_m3335945786 (Result_t2610723219 * __this, ResultPointU5BU5D_t1195164344* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.BarcodeFormat ZXing.Result::get_BarcodeFormat()
extern "C"  int32_t Result_get_BarcodeFormat_m329519002 (Result_t2610723219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::set_BarcodeFormat(ZXing.BarcodeFormat)
extern "C"  void Result_set_BarcodeFormat_m1835069177 (Result_t2610723219 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.Result::get_ResultMetadata()
extern "C"  Il2CppObject* Result_get_ResultMetadata_m2127799040 (Result_t2610723219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::set_ResultMetadata(System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>)
extern "C"  void Result_set_ResultMetadata_m572373303 (Result_t2610723219 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::set_Timestamp(System.Int64)
extern "C"  void Result_set_Timestamp_m1223153981 (Result_t2610723219 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::.ctor(System.String,System.Byte[],ZXing.ResultPoint[],ZXing.BarcodeFormat)
extern "C"  void Result__ctor_m1665603057 (Result_t2610723219 * __this, String_t* ___text0, ByteU5BU5D_t4260760469* ___rawBytes1, ResultPointU5BU5D_t1195164344* ___resultPoints2, int32_t ___format3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::.ctor(System.String,System.Byte[],ZXing.ResultPoint[],ZXing.BarcodeFormat,System.Int64)
extern "C"  void Result__ctor_m714146311 (Result_t2610723219 * __this, String_t* ___text0, ByteU5BU5D_t4260760469* ___rawBytes1, ResultPointU5BU5D_t1195164344* ___resultPoints2, int32_t ___format3, int64_t ___timestamp4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::putMetadata(ZXing.ResultMetadataType,System.Object)
extern "C"  void Result_putMetadata_m3924971406 (Result_t2610723219 * __this, int32_t ___type0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::putAllMetadata(System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>)
extern "C"  void Result_putAllMetadata_m177690639 (Result_t2610723219 * __this, Il2CppObject* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Result::addResultPoints(ZXing.ResultPoint[])
extern "C"  void Result_addResultPoints_m2160095252 (Result_t2610723219 * __this, ResultPointU5BU5D_t1195164344* ___newPoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Result::ToString()
extern "C"  String_t* Result_ToString_m1348112959 (Result_t2610723219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
