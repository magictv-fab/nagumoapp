﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CameraFadeIn
struct  CameraFadeIn_t2254266990  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CameraFadeIn::color
	FsmColor_t2131419205 * ___color_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CameraFadeIn::time
	FsmFloat_t2134102846 * ___time_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.CameraFadeIn::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_11;
	// System.Boolean HutongGames.PlayMaker.Actions.CameraFadeIn::realTime
	bool ___realTime_12;
	// System.Single HutongGames.PlayMaker.Actions.CameraFadeIn::startTime
	float ___startTime_13;
	// System.Single HutongGames.PlayMaker.Actions.CameraFadeIn::currentTime
	float ___currentTime_14;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.CameraFadeIn::colorLerp
	Color_t4194546905  ___colorLerp_15;

public:
	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___color_9)); }
	inline FsmColor_t2131419205 * get_color_9() const { return ___color_9; }
	inline FsmColor_t2131419205 ** get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(FsmColor_t2131419205 * value)
	{
		___color_9 = value;
		Il2CppCodeGenWriteBarrier(&___color_9, value);
	}

	inline static int32_t get_offset_of_time_10() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___time_10)); }
	inline FsmFloat_t2134102846 * get_time_10() const { return ___time_10; }
	inline FsmFloat_t2134102846 ** get_address_of_time_10() { return &___time_10; }
	inline void set_time_10(FsmFloat_t2134102846 * value)
	{
		___time_10 = value;
		Il2CppCodeGenWriteBarrier(&___time_10, value);
	}

	inline static int32_t get_offset_of_finishEvent_11() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___finishEvent_11)); }
	inline FsmEvent_t2133468028 * get_finishEvent_11() const { return ___finishEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_11() { return &___finishEvent_11; }
	inline void set_finishEvent_11(FsmEvent_t2133468028 * value)
	{
		___finishEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_11, value);
	}

	inline static int32_t get_offset_of_realTime_12() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___realTime_12)); }
	inline bool get_realTime_12() const { return ___realTime_12; }
	inline bool* get_address_of_realTime_12() { return &___realTime_12; }
	inline void set_realTime_12(bool value)
	{
		___realTime_12 = value;
	}

	inline static int32_t get_offset_of_startTime_13() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___startTime_13)); }
	inline float get_startTime_13() const { return ___startTime_13; }
	inline float* get_address_of_startTime_13() { return &___startTime_13; }
	inline void set_startTime_13(float value)
	{
		___startTime_13 = value;
	}

	inline static int32_t get_offset_of_currentTime_14() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___currentTime_14)); }
	inline float get_currentTime_14() const { return ___currentTime_14; }
	inline float* get_address_of_currentTime_14() { return &___currentTime_14; }
	inline void set_currentTime_14(float value)
	{
		___currentTime_14 = value;
	}

	inline static int32_t get_offset_of_colorLerp_15() { return static_cast<int32_t>(offsetof(CameraFadeIn_t2254266990, ___colorLerp_15)); }
	inline Color_t4194546905  get_colorLerp_15() const { return ___colorLerp_15; }
	inline Color_t4194546905 * get_address_of_colorLerp_15() { return &___colorLerp_15; }
	inline void set_colorLerp_15(Color_t4194546905  value)
	{
		___colorLerp_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
