﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Camera
struct Camera_t2727095145;
// InfiniteHorizontalScroll
struct InfiniteHorizontalScroll_t3122519653;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoverFocus
struct  CoverFocus_t3223037825  : public MonoBehaviour_t667441552
{
public:
	// System.Single CoverFocus::scaleFront
	float ___scaleFront_3;
	// System.Single CoverFocus::scaleBack
	float ___scaleBack_4;
	// System.Single CoverFocus::scalePercent
	float ___scalePercent_5;
	// UnityEngine.UI.Button CoverFocus::caralhodebutton
	Button_t3896396478 * ___caralhodebutton_6;
	// UnityEngine.RectTransform CoverFocus::rect
	RectTransform_t972643934 * ___rect_7;
	// UnityEngine.Camera CoverFocus::camera
	Camera_t2727095145 * ___camera_8;
	// InfiniteHorizontalScroll CoverFocus::scrollRect
	InfiniteHorizontalScroll_t3122519653 * ___scrollRect_9;

public:
	inline static int32_t get_offset_of_scaleFront_3() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___scaleFront_3)); }
	inline float get_scaleFront_3() const { return ___scaleFront_3; }
	inline float* get_address_of_scaleFront_3() { return &___scaleFront_3; }
	inline void set_scaleFront_3(float value)
	{
		___scaleFront_3 = value;
	}

	inline static int32_t get_offset_of_scaleBack_4() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___scaleBack_4)); }
	inline float get_scaleBack_4() const { return ___scaleBack_4; }
	inline float* get_address_of_scaleBack_4() { return &___scaleBack_4; }
	inline void set_scaleBack_4(float value)
	{
		___scaleBack_4 = value;
	}

	inline static int32_t get_offset_of_scalePercent_5() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___scalePercent_5)); }
	inline float get_scalePercent_5() const { return ___scalePercent_5; }
	inline float* get_address_of_scalePercent_5() { return &___scalePercent_5; }
	inline void set_scalePercent_5(float value)
	{
		___scalePercent_5 = value;
	}

	inline static int32_t get_offset_of_caralhodebutton_6() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___caralhodebutton_6)); }
	inline Button_t3896396478 * get_caralhodebutton_6() const { return ___caralhodebutton_6; }
	inline Button_t3896396478 ** get_address_of_caralhodebutton_6() { return &___caralhodebutton_6; }
	inline void set_caralhodebutton_6(Button_t3896396478 * value)
	{
		___caralhodebutton_6 = value;
		Il2CppCodeGenWriteBarrier(&___caralhodebutton_6, value);
	}

	inline static int32_t get_offset_of_rect_7() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___rect_7)); }
	inline RectTransform_t972643934 * get_rect_7() const { return ___rect_7; }
	inline RectTransform_t972643934 ** get_address_of_rect_7() { return &___rect_7; }
	inline void set_rect_7(RectTransform_t972643934 * value)
	{
		___rect_7 = value;
		Il2CppCodeGenWriteBarrier(&___rect_7, value);
	}

	inline static int32_t get_offset_of_camera_8() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___camera_8)); }
	inline Camera_t2727095145 * get_camera_8() const { return ___camera_8; }
	inline Camera_t2727095145 ** get_address_of_camera_8() { return &___camera_8; }
	inline void set_camera_8(Camera_t2727095145 * value)
	{
		___camera_8 = value;
		Il2CppCodeGenWriteBarrier(&___camera_8, value);
	}

	inline static int32_t get_offset_of_scrollRect_9() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825, ___scrollRect_9)); }
	inline InfiniteHorizontalScroll_t3122519653 * get_scrollRect_9() const { return ___scrollRect_9; }
	inline InfiniteHorizontalScroll_t3122519653 ** get_address_of_scrollRect_9() { return &___scrollRect_9; }
	inline void set_scrollRect_9(InfiniteHorizontalScroll_t3122519653 * value)
	{
		___scrollRect_9 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_9, value);
	}
};

struct CoverFocus_t3223037825_StaticFields
{
public:
	// UnityEngine.Transform CoverFocus::tmpChild
	Transform_t1659122786 * ___tmpChild_2;

public:
	inline static int32_t get_offset_of_tmpChild_2() { return static_cast<int32_t>(offsetof(CoverFocus_t3223037825_StaticFields, ___tmpChild_2)); }
	inline Transform_t1659122786 * get_tmpChild_2() const { return ___tmpChild_2; }
	inline Transform_t1659122786 ** get_address_of_tmpChild_2() { return &___tmpChild_2; }
	inline void set_tmpChild_2(Transform_t1659122786 * value)
	{
		___tmpChild_2 = value;
		Il2CppCodeGenWriteBarrier(&___tmpChild_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
