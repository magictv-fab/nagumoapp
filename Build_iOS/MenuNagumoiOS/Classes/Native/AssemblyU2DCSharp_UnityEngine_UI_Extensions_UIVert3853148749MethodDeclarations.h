﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStoreyA8
struct U3CAddListenerU3Ec__AnonStoreyA8_t3853148749;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStoreyA8::.ctor()
extern "C"  void U3CAddListenerU3Ec__AnonStoreyA8__ctor_m2570762686 (U3CAddListenerU3Ec__AnonStoreyA8_t3853148749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStoreyA8::<>m__90()
extern "C"  void U3CAddListenerU3Ec__AnonStoreyA8_U3CU3Em__90_m3205024350 (U3CAddListenerU3Ec__AnonStoreyA8_t3853148749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
