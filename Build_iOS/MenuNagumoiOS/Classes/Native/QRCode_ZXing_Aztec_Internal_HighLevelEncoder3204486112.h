﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.HighLevelEncoder
struct  HighLevelEncoder_t3204486112  : public Il2CppObject
{
public:
	// System.Byte[] ZXing.Aztec.Internal.HighLevelEncoder::text
	ByteU5BU5D_t4260760469* ___text_4;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(HighLevelEncoder_t3204486112, ___text_4)); }
	inline ByteU5BU5D_t4260760469* get_text_4() const { return ___text_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(ByteU5BU5D_t4260760469* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}
};

struct HighLevelEncoder_t3204486112_StaticFields
{
public:
	// System.String[] ZXing.Aztec.Internal.HighLevelEncoder::MODE_NAMES
	StringU5BU5D_t4054002952* ___MODE_NAMES_0;
	// System.Int32[][] ZXing.Aztec.Internal.HighLevelEncoder::LATCH_TABLE
	Int32U5BU5DU5BU5D_t1820556512* ___LATCH_TABLE_1;
	// System.Int32[][] ZXing.Aztec.Internal.HighLevelEncoder::CHAR_MAP
	Int32U5BU5DU5BU5D_t1820556512* ___CHAR_MAP_2;
	// System.Int32[][] ZXing.Aztec.Internal.HighLevelEncoder::SHIFT_TABLE
	Int32U5BU5DU5BU5D_t1820556512* ___SHIFT_TABLE_3;

public:
	inline static int32_t get_offset_of_MODE_NAMES_0() { return static_cast<int32_t>(offsetof(HighLevelEncoder_t3204486112_StaticFields, ___MODE_NAMES_0)); }
	inline StringU5BU5D_t4054002952* get_MODE_NAMES_0() const { return ___MODE_NAMES_0; }
	inline StringU5BU5D_t4054002952** get_address_of_MODE_NAMES_0() { return &___MODE_NAMES_0; }
	inline void set_MODE_NAMES_0(StringU5BU5D_t4054002952* value)
	{
		___MODE_NAMES_0 = value;
		Il2CppCodeGenWriteBarrier(&___MODE_NAMES_0, value);
	}

	inline static int32_t get_offset_of_LATCH_TABLE_1() { return static_cast<int32_t>(offsetof(HighLevelEncoder_t3204486112_StaticFields, ___LATCH_TABLE_1)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_LATCH_TABLE_1() const { return ___LATCH_TABLE_1; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_LATCH_TABLE_1() { return &___LATCH_TABLE_1; }
	inline void set_LATCH_TABLE_1(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___LATCH_TABLE_1 = value;
		Il2CppCodeGenWriteBarrier(&___LATCH_TABLE_1, value);
	}

	inline static int32_t get_offset_of_CHAR_MAP_2() { return static_cast<int32_t>(offsetof(HighLevelEncoder_t3204486112_StaticFields, ___CHAR_MAP_2)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_CHAR_MAP_2() const { return ___CHAR_MAP_2; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_CHAR_MAP_2() { return &___CHAR_MAP_2; }
	inline void set_CHAR_MAP_2(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___CHAR_MAP_2 = value;
		Il2CppCodeGenWriteBarrier(&___CHAR_MAP_2, value);
	}

	inline static int32_t get_offset_of_SHIFT_TABLE_3() { return static_cast<int32_t>(offsetof(HighLevelEncoder_t3204486112_StaticFields, ___SHIFT_TABLE_3)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_SHIFT_TABLE_3() const { return ___SHIFT_TABLE_3; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_SHIFT_TABLE_3() { return &___SHIFT_TABLE_3; }
	inline void set_SHIFT_TABLE_3(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___SHIFT_TABLE_3 = value;
		Il2CppCodeGenWriteBarrier(&___SHIFT_TABLE_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
