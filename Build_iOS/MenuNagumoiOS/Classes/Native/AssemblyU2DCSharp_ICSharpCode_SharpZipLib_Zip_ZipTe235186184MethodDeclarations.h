﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler
struct ZipTestResultHandler_t235186184;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Zip.TestStatus
struct TestStatus_t2697759954;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Test2697759954.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipTestResultHandler__ctor_m3978255775 (ZipTestResultHandler_t235186184 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::Invoke(ICSharpCode.SharpZipLib.Zip.TestStatus,System.String)
extern "C"  void ZipTestResultHandler_Invoke_m2415266099 (ZipTestResultHandler_t235186184 * __this, TestStatus_t2697759954 * ___status0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::BeginInvoke(ICSharpCode.SharpZipLib.Zip.TestStatus,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipTestResultHandler_BeginInvoke_m4146633146 (ZipTestResultHandler_t235186184 * __this, TestStatus_t2697759954 * ___status0, String_t* ___message1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ZipTestResultHandler_EndInvoke_m1338537775 (ZipTestResultHandler_t235186184 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
