﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextChild
struct  GetNextChild_t464321883  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextChild::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextChild::storeNextChild
	FsmGameObject_t1697147867 * ___storeNextChild_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextChild::loopEvent
	FsmEvent_t2133468028 * ___loopEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextChild::finishedEvent
	FsmEvent_t2133468028 * ___finishedEvent_12;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetNextChild::go
	GameObject_t3674682005 * ___go_13;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextChild::nextChildIndex
	int32_t ___nextChildIndex_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetNextChild_t464321883, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_storeNextChild_10() { return static_cast<int32_t>(offsetof(GetNextChild_t464321883, ___storeNextChild_10)); }
	inline FsmGameObject_t1697147867 * get_storeNextChild_10() const { return ___storeNextChild_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeNextChild_10() { return &___storeNextChild_10; }
	inline void set_storeNextChild_10(FsmGameObject_t1697147867 * value)
	{
		___storeNextChild_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeNextChild_10, value);
	}

	inline static int32_t get_offset_of_loopEvent_11() { return static_cast<int32_t>(offsetof(GetNextChild_t464321883, ___loopEvent_11)); }
	inline FsmEvent_t2133468028 * get_loopEvent_11() const { return ___loopEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_loopEvent_11() { return &___loopEvent_11; }
	inline void set_loopEvent_11(FsmEvent_t2133468028 * value)
	{
		___loopEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_11, value);
	}

	inline static int32_t get_offset_of_finishedEvent_12() { return static_cast<int32_t>(offsetof(GetNextChild_t464321883, ___finishedEvent_12)); }
	inline FsmEvent_t2133468028 * get_finishedEvent_12() const { return ___finishedEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_finishedEvent_12() { return &___finishedEvent_12; }
	inline void set_finishedEvent_12(FsmEvent_t2133468028 * value)
	{
		___finishedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_12, value);
	}

	inline static int32_t get_offset_of_go_13() { return static_cast<int32_t>(offsetof(GetNextChild_t464321883, ___go_13)); }
	inline GameObject_t3674682005 * get_go_13() const { return ___go_13; }
	inline GameObject_t3674682005 ** get_address_of_go_13() { return &___go_13; }
	inline void set_go_13(GameObject_t3674682005 * value)
	{
		___go_13 = value;
		Il2CppCodeGenWriteBarrier(&___go_13, value);
	}

	inline static int32_t get_offset_of_nextChildIndex_14() { return static_cast<int32_t>(offsetof(GetNextChild_t464321883, ___nextChildIndex_14)); }
	inline int32_t get_nextChildIndex_14() const { return ___nextChildIndex_14; }
	inline int32_t* get_address_of_nextChildIndex_14() { return &___nextChildIndex_14; }
	inline void set_nextChildIndex_14(int32_t value)
	{
		___nextChildIndex_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
