﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.components.GroupProgressComponentsManager
struct GroupProgressComponentsManager_t1430043881;
// ARM.abstracts.ProgressEventsAbstract[]
struct ProgressEventsAbstractU5BU5D_t3177844181;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.components.GroupProgressComponentsManager::.ctor()
extern "C"  void GroupProgressComponentsManager__ctor_m2816024698 (GroupProgressComponentsManager_t1430043881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupProgressComponentsManager::SetProgressComponents(ARM.abstracts.ProgressEventsAbstract[])
extern "C"  void GroupProgressComponentsManager_SetProgressComponents_m3290945156 (GroupProgressComponentsManager_t1430043881 * __this, ProgressEventsAbstractU5BU5D_t3177844181* ___components0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupProgressComponentsManager::InitLoaderListener()
extern "C"  void GroupProgressComponentsManager_InitLoaderListener_m1759621921 (GroupProgressComponentsManager_t1430043881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupProgressComponentsManager::completeEvent()
extern "C"  void GroupProgressComponentsManager_completeEvent_m3715678041 (GroupProgressComponentsManager_t1430043881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupProgressComponentsManager::progressEvent(System.Single)
extern "C"  void GroupProgressComponentsManager_progressEvent_m3739568614 (GroupProgressComponentsManager_t1430043881 * __this, float ___subTotal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupProgressComponentsManager::prepare()
extern "C"  void GroupProgressComponentsManager_prepare_m1297318719 (GroupProgressComponentsManager_t1430043881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
