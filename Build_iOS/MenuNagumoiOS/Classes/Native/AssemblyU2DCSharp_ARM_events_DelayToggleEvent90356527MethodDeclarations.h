﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.DelayToggleEvent
struct DelayToggleEvent_t90356527;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.events.DelayToggleEvent::.ctor()
extern "C"  void DelayToggleEvent__ctor_m721538405 (DelayToggleEvent_t90356527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.DelayToggleEvent::Start()
extern "C"  void DelayToggleEvent_Start_m3963643493 (DelayToggleEvent_t90356527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
