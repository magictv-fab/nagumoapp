﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_5_gen2062143959MethodDeclarations.h"

// System.Void System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>::.ctor(System.Object,System.IntPtr)
#define Func_5__ctor_m646890362(__this, ___object0, ___method1, method) ((  void (*) (Func_5_t533482363 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_5__ctor_m1898550480_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>::Invoke(T1,T2,T3,T4)
#define Func_5_Invoke_m1787343502(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  LuminanceSource_t1231523093 * (*) (Func_5_t533482363 *, ByteU5BU5D_t4260760469*, int32_t, int32_t, int32_t, const MethodInfo*))Func_5_Invoke_m1781865976_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Func_5_BeginInvoke_m3000327153(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Func_5_t533482363 *, ByteU5BU5D_t4260760469*, int32_t, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_5_BeginInvoke_m2558481243_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TResult System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>::EndInvoke(System.IAsyncResult)
#define Func_5_EndInvoke_m1082946476(__this, ___result0, method) ((  LuminanceSource_t1231523093 * (*) (Func_5_t533482363 *, Il2CppObject *, const MethodInfo*))Func_5_EndInvoke_m1829609986_gshared)(__this, ___result0, method)
