﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// CustomECPNManager
struct CustomECPNManager_t1031774400;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// RefreshGalleryWrapper
struct RefreshGalleryWrapper_t3861461116;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CustomECPNManager1031774400.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CustomECPNManager1031774400MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_iOS_NotificationServices2332829491MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_iOS_NotificationType3233079183.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_BitConverter1260277767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RefreshGalleryWrappe3861461116.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RefreshGalleryWrappe3861461116MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CustomECPNManager::.ctor()
extern Il2CppCodeGenString* _stringLiteral522718654;
extern Il2CppCodeGenString* _stringLiteral2807606273;
extern const uint32_t CustomECPNManager__ctor_m1345344631_MetadataUsageId;
extern "C"  void CustomECPNManager__ctor_m1345344631 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomECPNManager__ctor_m1345344631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_GoogleCloudMessageProjectID_2(_stringLiteral522718654);
		__this->set_packageName_3(_stringLiteral2807606273);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CustomECPNManager::RequestDeviceToken()
extern "C"  void CustomECPNManager_RequestDeviceToken_m3121310497 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = NotificationServices_get_deviceToken_m1646897969(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		__this->set_pollIOSDeviceToken_5((bool)1);
		NotificationServices_RegisterForNotifications_m460427814(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_001c:
	{
		CustomECPNManager_RegisterIOSDevice_m1674902773(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void CustomECPNManager::RequestUnregisterDevice()
extern "C"  void CustomECPNManager_RequestUnregisterDevice_m2569527990 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method)
{
	{
		NotificationServices_UnregisterForRemoteNotifications_m3086124726(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String CustomECPNManager::GetDevToken()
extern "C"  String_t* CustomECPNManager_GetDevToken_m494469010 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_devToken_4();
		return L_0;
	}
}
// System.Void CustomECPNManager::Update()
extern "C"  void CustomECPNManager_Update_m482872694 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_pollIOSDeviceToken_5();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		CustomECPNManager_RegisterIOSDevice_m1674902773(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void CustomECPNManager::RegisterAndroidDevice(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2831426441;
extern const uint32_t CustomECPNManager_RegisterAndroidDevice_m998029195_MetadataUsageId;
extern "C"  void CustomECPNManager_RegisterAndroidDevice_m998029195 (CustomECPNManager_t1031774400 * __this, String_t* ___rID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomECPNManager_RegisterAndroidDevice_m998029195_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___rID0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2831426441, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___rID0;
		__this->set_devToken_4(L_2);
		return;
	}
}
// System.Void CustomECPNManager::UnregisterDevice(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3153721029;
extern const uint32_t CustomECPNManager_UnregisterDevice_m2193776707_MetadataUsageId;
extern "C"  void CustomECPNManager_UnregisterDevice_m2193776707 (CustomECPNManager_t1031774400 * __this, String_t* ___rID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomECPNManager_UnregisterDevice_m2193776707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___rID0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3153721029, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CustomECPNManager::RegisterIOSDevice()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t1260277767_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral45;
extern Il2CppCodeGenString* _stringLiteral104461;
extern const uint32_t CustomECPNManager_RegisterIOSDevice_m1674902773_MetadataUsageId;
extern "C"  void CustomECPNManager_RegisterIOSDevice_m1674902773 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CustomECPNManager_RegisterIOSDevice_m1674902773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NotificationServices_get_registrationError_m720449892(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_1 = NotificationServices_get_registrationError_m720449892(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		ByteU5BU5D_t4260760469* L_2 = NotificationServices_get_deviceToken_m1646897969(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		__this->set_pollIOSDeviceToken_5((bool)0);
		ByteU5BU5D_t4260760469* L_3 = NotificationServices_get_deviceToken_m1646897969(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1260277767_il2cpp_TypeInfo_var);
		String_t* L_4 = BitConverter_ToString_m1865594174(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_4);
		String_t* L_6 = String_Replace_m2915759397(L_4, _stringLiteral45, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		Il2CppObject * L_8 = CustomECPNManager_StoreDeviceID_m2619003175(__this, L_7, _stringLiteral104461, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CustomECPNManager::StoreDeviceID(System.String,System.String)
extern "C"  Il2CppObject * CustomECPNManager_StoreDeviceID_m2619003175 (CustomECPNManager_t1031774400 * __this, String_t* ___rID0, String_t* ___os1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___rID0;
		__this->set_devToken_4(L_0);
		return (Il2CppObject *)NULL;
	}
}
// System.Void RefreshGalleryWrapper::.ctor()
extern "C"  void RefreshGalleryWrapper__ctor_m248395323 (RefreshGalleryWrapper_t3861461116 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
