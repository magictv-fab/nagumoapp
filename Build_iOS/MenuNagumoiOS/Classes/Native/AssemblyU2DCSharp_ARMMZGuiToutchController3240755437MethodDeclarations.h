﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARMMZGuiToutchController
struct ARMMZGuiToutchController_t3240755437;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void ARMMZGuiToutchController::.ctor()
extern "C"  void ARMMZGuiToutchController__ctor_m2538948686 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::Start()
extern "C"  void ARMMZGuiToutchController_Start_m1486086478 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::CheckTouchObject(UnityEngine.Vector2)
extern "C"  void ARMMZGuiToutchController_CheckTouchObject_m2351802574 (ARMMZGuiToutchController_t3240755437 * __this, Vector2_t4282066565  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::Update()
extern "C"  void ARMMZGuiToutchController_Update_m3124860031 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARMMZGuiToutchController::CheckTouching()
extern "C"  bool ARMMZGuiToutchController_CheckTouching_m1177570531 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::CheckScaleTolerance()
extern "C"  void ARMMZGuiToutchController_CheckScaleTolerance_m3524107383 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::Click()
extern "C"  void ARMMZGuiToutchController_Click_m4243796212 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARMMZGuiToutchController::CalculateAngleBetweenTouchs()
extern "C"  float ARMMZGuiToutchController_CalculateAngleBetweenTouchs_m2470456399 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::Rotate(System.Single)
extern "C"  void ARMMZGuiToutchController_Rotate_m2375178426 (ARMMZGuiToutchController_t3240755437 * __this, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::CalculateScaleRate(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void ARMMZGuiToutchController_CalculateScaleRate_m1291114740 (ARMMZGuiToutchController_t3240755437 * __this, Vector2_t4282066565  ___newPoint10, Vector2_t4282066565  ___newPoint21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::OnScroll(System.Single)
extern "C"  void ARMMZGuiToutchController_OnScroll_m1224888745 (ARMMZGuiToutchController_t3240755437 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::Move()
extern "C"  void ARMMZGuiToutchController_Move_m2920197831 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 ARMMZGuiToutchController::GetLastTouch()
extern "C"  Vector2_t4282066565  ARMMZGuiToutchController_GetLastTouch_m2860826468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMMZGuiToutchController::OnSelect()
extern "C"  void ARMMZGuiToutchController_OnSelect_m772213169 (ARMMZGuiToutchController_t3240755437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
