﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTouchInfo
struct  GetTouchInfo_t3931743231  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTouchInfo::fingerId
	FsmInt_t1596138449 * ___fingerId_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetTouchInfo::normalize
	FsmBool_t1075959796 * ___normalize_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetTouchInfo::storePosition
	FsmVector3_t533912882 * ___storePosition_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeX
	FsmFloat_t2134102846 * ___storeX_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeY
	FsmFloat_t2134102846 * ___storeY_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaPosition
	FsmVector3_t533912882 * ___storeDeltaPosition_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaX
	FsmFloat_t2134102846 * ___storeDeltaX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaY
	FsmFloat_t2134102846 * ___storeDeltaY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaTime
	FsmFloat_t2134102846 * ___storeDeltaTime_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTouchInfo::storeTapCount
	FsmInt_t1596138449 * ___storeTapCount_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTouchInfo::everyFrame
	bool ___everyFrame_19;
	// System.Single HutongGames.PlayMaker.Actions.GetTouchInfo::screenWidth
	float ___screenWidth_20;
	// System.Single HutongGames.PlayMaker.Actions.GetTouchInfo::screenHeight
	float ___screenHeight_21;

public:
	inline static int32_t get_offset_of_fingerId_9() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___fingerId_9)); }
	inline FsmInt_t1596138449 * get_fingerId_9() const { return ___fingerId_9; }
	inline FsmInt_t1596138449 ** get_address_of_fingerId_9() { return &___fingerId_9; }
	inline void set_fingerId_9(FsmInt_t1596138449 * value)
	{
		___fingerId_9 = value;
		Il2CppCodeGenWriteBarrier(&___fingerId_9, value);
	}

	inline static int32_t get_offset_of_normalize_10() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___normalize_10)); }
	inline FsmBool_t1075959796 * get_normalize_10() const { return ___normalize_10; }
	inline FsmBool_t1075959796 ** get_address_of_normalize_10() { return &___normalize_10; }
	inline void set_normalize_10(FsmBool_t1075959796 * value)
	{
		___normalize_10 = value;
		Il2CppCodeGenWriteBarrier(&___normalize_10, value);
	}

	inline static int32_t get_offset_of_storePosition_11() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storePosition_11)); }
	inline FsmVector3_t533912882 * get_storePosition_11() const { return ___storePosition_11; }
	inline FsmVector3_t533912882 ** get_address_of_storePosition_11() { return &___storePosition_11; }
	inline void set_storePosition_11(FsmVector3_t533912882 * value)
	{
		___storePosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___storePosition_11, value);
	}

	inline static int32_t get_offset_of_storeX_12() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeX_12)); }
	inline FsmFloat_t2134102846 * get_storeX_12() const { return ___storeX_12; }
	inline FsmFloat_t2134102846 ** get_address_of_storeX_12() { return &___storeX_12; }
	inline void set_storeX_12(FsmFloat_t2134102846 * value)
	{
		___storeX_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeX_12, value);
	}

	inline static int32_t get_offset_of_storeY_13() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeY_13)); }
	inline FsmFloat_t2134102846 * get_storeY_13() const { return ___storeY_13; }
	inline FsmFloat_t2134102846 ** get_address_of_storeY_13() { return &___storeY_13; }
	inline void set_storeY_13(FsmFloat_t2134102846 * value)
	{
		___storeY_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeY_13, value);
	}

	inline static int32_t get_offset_of_storeDeltaPosition_14() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeDeltaPosition_14)); }
	inline FsmVector3_t533912882 * get_storeDeltaPosition_14() const { return ___storeDeltaPosition_14; }
	inline FsmVector3_t533912882 ** get_address_of_storeDeltaPosition_14() { return &___storeDeltaPosition_14; }
	inline void set_storeDeltaPosition_14(FsmVector3_t533912882 * value)
	{
		___storeDeltaPosition_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeDeltaPosition_14, value);
	}

	inline static int32_t get_offset_of_storeDeltaX_15() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeDeltaX_15)); }
	inline FsmFloat_t2134102846 * get_storeDeltaX_15() const { return ___storeDeltaX_15; }
	inline FsmFloat_t2134102846 ** get_address_of_storeDeltaX_15() { return &___storeDeltaX_15; }
	inline void set_storeDeltaX_15(FsmFloat_t2134102846 * value)
	{
		___storeDeltaX_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeDeltaX_15, value);
	}

	inline static int32_t get_offset_of_storeDeltaY_16() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeDeltaY_16)); }
	inline FsmFloat_t2134102846 * get_storeDeltaY_16() const { return ___storeDeltaY_16; }
	inline FsmFloat_t2134102846 ** get_address_of_storeDeltaY_16() { return &___storeDeltaY_16; }
	inline void set_storeDeltaY_16(FsmFloat_t2134102846 * value)
	{
		___storeDeltaY_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeDeltaY_16, value);
	}

	inline static int32_t get_offset_of_storeDeltaTime_17() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeDeltaTime_17)); }
	inline FsmFloat_t2134102846 * get_storeDeltaTime_17() const { return ___storeDeltaTime_17; }
	inline FsmFloat_t2134102846 ** get_address_of_storeDeltaTime_17() { return &___storeDeltaTime_17; }
	inline void set_storeDeltaTime_17(FsmFloat_t2134102846 * value)
	{
		___storeDeltaTime_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeDeltaTime_17, value);
	}

	inline static int32_t get_offset_of_storeTapCount_18() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___storeTapCount_18)); }
	inline FsmInt_t1596138449 * get_storeTapCount_18() const { return ___storeTapCount_18; }
	inline FsmInt_t1596138449 ** get_address_of_storeTapCount_18() { return &___storeTapCount_18; }
	inline void set_storeTapCount_18(FsmInt_t1596138449 * value)
	{
		___storeTapCount_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeTapCount_18, value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_screenWidth_20() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___screenWidth_20)); }
	inline float get_screenWidth_20() const { return ___screenWidth_20; }
	inline float* get_address_of_screenWidth_20() { return &___screenWidth_20; }
	inline void set_screenWidth_20(float value)
	{
		___screenWidth_20 = value;
	}

	inline static int32_t get_offset_of_screenHeight_21() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3931743231, ___screenHeight_21)); }
	inline float get_screenHeight_21() const { return ___screenHeight_21; }
	inline float* get_address_of_screenHeight_21() { return &___screenHeight_21; }
	inline void set_screenHeight_21(float value)
	{
		___screenHeight_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
