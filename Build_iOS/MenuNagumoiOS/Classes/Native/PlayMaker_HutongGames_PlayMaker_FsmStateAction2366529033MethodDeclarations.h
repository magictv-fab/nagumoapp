﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.FsmStateAction::Init(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmStateAction_Init_m1499552461 (FsmStateAction_t2366529033 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Reset()
extern "C"  void FsmStateAction_Reset_m4087448855 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Awake()
extern "C"  void FsmStateAction_Awake_m2383653837 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmStateAction_Event_m2033589220 (FsmStateAction_t2366529033 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Finish()
extern "C"  void FsmStateAction_Finish_m1833602861 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnEnter()
extern "C"  void FsmStateAction_OnEnter_m160148417 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnFixedUpdate()
extern "C"  void FsmStateAction_OnFixedUpdate_m3203575558 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnUpdate()
extern "C"  void FsmStateAction_OnUpdate_m4098160290 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI()
extern "C"  void FsmStateAction_OnGUI_m1641447268 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnLateUpdate()
extern "C"  void FsmStateAction_OnLateUpdate_m2014747496 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnExit()
extern "C"  void FsmStateAction_OnExit_m3616316343 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnDrawGizmos()
extern "C"  void FsmStateAction_OnDrawGizmos_m1856719734 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnDrawGizmosSelected()
extern "C"  void FsmStateAction_OnDrawGizmosSelected_m561093233 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void FsmStateAction_DoCollisionEnter_m3733369100 (FsmStateAction_t2366529033 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionStay(UnityEngine.Collision)
extern "C"  void FsmStateAction_DoCollisionStay_m2831230095 (FsmStateAction_t2366529033 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionExit(UnityEngine.Collision)
extern "C"  void FsmStateAction_DoCollisionExit_m3274843178 (FsmStateAction_t2366529033 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void FsmStateAction_DoTriggerEnter_m520123642 (FsmStateAction_t2366529033 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerStay(UnityEngine.Collider)
extern "C"  void FsmStateAction_DoTriggerStay_m3947358307 (FsmStateAction_t2366529033 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerExit(UnityEngine.Collider)
extern "C"  void FsmStateAction_DoTriggerExit_m1329269096 (FsmStateAction_t2366529033 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void FsmStateAction_DoControllerColliderHit_m2652537862 (FsmStateAction_t2366529033 * __this, ControllerColliderHit_t2416790841 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Log(System.String)
extern "C"  void FsmStateAction_Log_m3811764982 (FsmStateAction_t2366529033 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::LogWarning(System.String)
extern "C"  void FsmStateAction_LogWarning_m567309232 (FsmStateAction_t2366529033 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::LogError(System.String)
extern "C"  void FsmStateAction_LogError_m3478223492 (FsmStateAction_t2366529033 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmStateAction::ErrorCheck()
extern "C"  String_t* FsmStateAction_ErrorCheck_m1857837725 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmStateAction::get_Name()
extern "C"  String_t* FsmStateAction_get_Name_m3719143057 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Name(System.String)
extern "C"  void FsmStateAction_set_Name_m4135125728 (FsmStateAction_t2366529033 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::get_Fsm()
extern "C"  Fsm_t1527112426 * FsmStateAction_get_Fsm_m4090501600 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Fsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmStateAction_set_Fsm_m1775877875 (FsmStateAction_t2366529033 * __this, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::get_Owner()
extern "C"  GameObject_t3674682005 * FsmStateAction_get_Owner_m1855633209 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Owner(UnityEngine.GameObject)
extern "C"  void FsmStateAction_set_Owner_m2504977430 (FsmStateAction_t2366529033 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::get_State()
extern "C"  FsmState_t2146334067 * FsmStateAction_get_State_m763080396 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_State(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmStateAction_set_State_m1661712927 (FsmStateAction_t2366529033 * __this, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Enabled()
extern "C"  bool FsmStateAction_get_Enabled_m1786926760 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Enabled(System.Boolean)
extern "C"  void FsmStateAction_set_Enabled_m2181327395 (FsmStateAction_t2366529033 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_IsOpen()
extern "C"  bool FsmStateAction_get_IsOpen_m1958228783 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_IsOpen(System.Boolean)
extern "C"  void FsmStateAction_set_IsOpen_m2941220322 (FsmStateAction_t2366529033 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Entered()
extern "C"  bool FsmStateAction_get_Entered_m1561056094 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Entered(System.Boolean)
extern "C"  void FsmStateAction_set_Entered_m4198658137 (FsmStateAction_t2366529033 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Finished()
extern "C"  bool FsmStateAction_get_Finished_m3113497357 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Finished(System.Boolean)
extern "C"  void FsmStateAction_set_Finished_m434268736 (FsmStateAction_t2366529033 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Active()
extern "C"  bool FsmStateAction_get_Active_m638802721 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Active(System.Boolean)
extern "C"  void FsmStateAction_set_Active_m3288649300 (FsmStateAction_t2366529033 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::.ctor()
extern "C"  void FsmStateAction__ctor_m2146048618 (FsmStateAction_t2366529033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
