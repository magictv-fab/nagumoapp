﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AboutScreenView
struct AboutScreenView_t204522622;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AboutScreenView::.ctor()
extern "C"  void AboutScreenView__ctor_m4072222893 (AboutScreenView_t204522622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AboutScreenView::get_DeviceDependentScale()
extern "C"  float AboutScreenView_get_DeviceDependentScale_m2839202805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutScreenView::SetTitle(System.String)
extern "C"  void AboutScreenView_SetTitle_m1573654165 (AboutScreenView_t204522622 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutScreenView::LoadView()
extern "C"  void AboutScreenView_LoadView_m1759926754 (AboutScreenView_t204522622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutScreenView::UpdateUI(System.Boolean)
extern "C"  void AboutScreenView_UpdateUI_m924884043 (AboutScreenView_t204522622 * __this, bool ___tf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AboutScreenView::UnLoadView()
extern "C"  void AboutScreenView_UnLoadView_m568898523 (AboutScreenView_t204522622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
