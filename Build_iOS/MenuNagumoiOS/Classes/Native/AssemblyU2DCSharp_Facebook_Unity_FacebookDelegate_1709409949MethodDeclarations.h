﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_2257890132MethodDeclarations.h"

// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>::.ctor(System.Object,System.IntPtr)
#define FacebookDelegate_1__ctor_m1443001319(__this, ___object0, ___method1, method) ((  void (*) (FacebookDelegate_1_t709409949 *, Il2CppObject *, IntPtr_t, const MethodInfo*))FacebookDelegate_1__ctor_m823687686_gshared)(__this, ___object0, ___method1, method)
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>::Invoke(T)
#define FacebookDelegate_1_Invoke_m1921102461(__this, ___result0, method) ((  void (*) (FacebookDelegate_1_t709409949 *, Il2CppObject *, const MethodInfo*))FacebookDelegate_1_Invoke_m3541671806_gshared)(__this, ___result0, method)
// System.IAsyncResult Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define FacebookDelegate_1_BeginInvoke_m2587363402(__this, ___result0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (FacebookDelegate_1_t709409949 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))FacebookDelegate_1_BeginInvoke_m222181963_gshared)(__this, ___result0, ___callback1, ___object2, method)
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>::EndInvoke(System.IAsyncResult)
#define FacebookDelegate_1_EndInvoke_m3254907255(__this, ___result0, method) ((  void (*) (FacebookDelegate_1_t709409949 *, Il2CppObject *, const MethodInfo*))FacebookDelegate_1_EndInvoke_m3390430998_gshared)(__this, ___result0, method)
