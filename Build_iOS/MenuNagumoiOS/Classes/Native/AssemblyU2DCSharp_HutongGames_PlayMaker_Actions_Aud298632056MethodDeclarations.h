﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AudioPlay
struct AudioPlay_t298632056;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AudioPlay::.ctor()
extern "C"  void AudioPlay__ctor_m3566774462 (AudioPlay_t298632056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::Reset()
extern "C"  void AudioPlay_Reset_m1213207403 (AudioPlay_t298632056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::OnEnter()
extern "C"  void AudioPlay_OnEnter_m3973051669 (AudioPlay_t298632056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::OnUpdate()
extern "C"  void AudioPlay_OnUpdate_m2039076814 (AudioPlay_t298632056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
