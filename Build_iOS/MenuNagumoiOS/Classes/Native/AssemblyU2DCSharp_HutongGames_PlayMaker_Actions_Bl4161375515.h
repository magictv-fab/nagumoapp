﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BlendAnimation
struct  BlendAnimation_t4161375515  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.BlendAnimation::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BlendAnimation::animName
	FsmString_t952858651 * ___animName_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.BlendAnimation::targetWeight
	FsmFloat_t2134102846 * ___targetWeight_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.BlendAnimation::time
	FsmFloat_t2134102846 * ___time_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BlendAnimation::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_13;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.BlendAnimation::delayedFinishEvent
	DelayedEvent_t1938906778 * ___delayedFinishEvent_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(BlendAnimation_t4161375515, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_animName_10() { return static_cast<int32_t>(offsetof(BlendAnimation_t4161375515, ___animName_10)); }
	inline FsmString_t952858651 * get_animName_10() const { return ___animName_10; }
	inline FsmString_t952858651 ** get_address_of_animName_10() { return &___animName_10; }
	inline void set_animName_10(FsmString_t952858651 * value)
	{
		___animName_10 = value;
		Il2CppCodeGenWriteBarrier(&___animName_10, value);
	}

	inline static int32_t get_offset_of_targetWeight_11() { return static_cast<int32_t>(offsetof(BlendAnimation_t4161375515, ___targetWeight_11)); }
	inline FsmFloat_t2134102846 * get_targetWeight_11() const { return ___targetWeight_11; }
	inline FsmFloat_t2134102846 ** get_address_of_targetWeight_11() { return &___targetWeight_11; }
	inline void set_targetWeight_11(FsmFloat_t2134102846 * value)
	{
		___targetWeight_11 = value;
		Il2CppCodeGenWriteBarrier(&___targetWeight_11, value);
	}

	inline static int32_t get_offset_of_time_12() { return static_cast<int32_t>(offsetof(BlendAnimation_t4161375515, ___time_12)); }
	inline FsmFloat_t2134102846 * get_time_12() const { return ___time_12; }
	inline FsmFloat_t2134102846 ** get_address_of_time_12() { return &___time_12; }
	inline void set_time_12(FsmFloat_t2134102846 * value)
	{
		___time_12 = value;
		Il2CppCodeGenWriteBarrier(&___time_12, value);
	}

	inline static int32_t get_offset_of_finishEvent_13() { return static_cast<int32_t>(offsetof(BlendAnimation_t4161375515, ___finishEvent_13)); }
	inline FsmEvent_t2133468028 * get_finishEvent_13() const { return ___finishEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_13() { return &___finishEvent_13; }
	inline void set_finishEvent_13(FsmEvent_t2133468028 * value)
	{
		___finishEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_13, value);
	}

	inline static int32_t get_offset_of_delayedFinishEvent_14() { return static_cast<int32_t>(offsetof(BlendAnimation_t4161375515, ___delayedFinishEvent_14)); }
	inline DelayedEvent_t1938906778 * get_delayedFinishEvent_14() const { return ___delayedFinishEvent_14; }
	inline DelayedEvent_t1938906778 ** get_address_of_delayedFinishEvent_14() { return &___delayedFinishEvent_14; }
	inline void set_delayedFinishEvent_14(DelayedEvent_t1938906778 * value)
	{
		___delayedFinishEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___delayedFinishEvent_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
