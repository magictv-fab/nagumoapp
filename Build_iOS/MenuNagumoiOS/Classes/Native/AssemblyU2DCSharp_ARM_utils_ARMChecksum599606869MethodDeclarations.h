﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.ARMChecksum
struct ARMChecksum_t599606869;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.utils.ARMChecksum::.ctor()
extern "C"  void ARMChecksum__ctor_m294636987 (ARMChecksum_t599606869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.ARMChecksum::CheckFileMD5(System.String,System.String)
extern "C"  bool ARMChecksum_CheckFileMD5_m1119956623 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, String_t* ___md5Checksum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.ARMChecksum::MD5Checksum(System.Byte[])
extern "C"  String_t* ARMChecksum_MD5Checksum_m3105500588 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
