﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.device.DeviceCacheFileManagerAbstract
struct DeviceCacheFileManagerAbstract_t121423689;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.device.DeviceCacheFileManagerAbstract::.ctor()
extern "C"  void DeviceCacheFileManagerAbstract__ctor_m4228420456 (DeviceCacheFileManagerAbstract_t121423689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
