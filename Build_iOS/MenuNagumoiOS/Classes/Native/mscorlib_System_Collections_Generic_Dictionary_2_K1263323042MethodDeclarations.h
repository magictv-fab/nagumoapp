﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>
struct KeyCollection_t1263323042;
// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Collections.Generic.IEnumerator`1<UnityEngine.NetworkReachability>
struct IEnumerator_1_t2524268084;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.NetworkReachability[]
struct NetworkReachabilityU5BU5D_t2994299098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke251499645.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3053849912_gshared (KeyCollection_t1263323042 * __this, Dictionary_2_t3931530887 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3053849912(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1263323042 *, Dictionary_2_t3931530887 *, const MethodInfo*))KeyCollection__ctor_m3053849912_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4289325342_gshared (KeyCollection_t1263323042 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4289325342(__this, ___item0, method) ((  void (*) (KeyCollection_t1263323042 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4289325342_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1827913237_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1827913237(__this, method) ((  void (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1827913237_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m870847788_gshared (KeyCollection_t1263323042 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m870847788(__this, ___item0, method) ((  bool (*) (KeyCollection_t1263323042 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m870847788_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1766842769_gshared (KeyCollection_t1263323042 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1766842769(__this, ___item0, method) ((  bool (*) (KeyCollection_t1263323042 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1766842769_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3755318161_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3755318161(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3755318161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m677915399_gshared (KeyCollection_t1263323042 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m677915399(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1263323042 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m677915399_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3944613058_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3944613058(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3944613058_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2247160205_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2247160205(__this, method) ((  bool (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2247160205_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1031542399_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1031542399(__this, method) ((  bool (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1031542399_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4159261483_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4159261483(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4159261483_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4223030253_gshared (KeyCollection_t1263323042 * __this, NetworkReachabilityU5BU5D_t2994299098* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4223030253(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1263323042 *, NetworkReachabilityU5BU5D_t2994299098*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4223030253_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::GetEnumerator()
extern "C"  Enumerator_t251499645  KeyCollection_GetEnumerator_m202149008_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m202149008(__this, method) ((  Enumerator_t251499645  (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_GetEnumerator_m202149008_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m4040092101_gshared (KeyCollection_t1263323042 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m4040092101(__this, method) ((  int32_t (*) (KeyCollection_t1263323042 *, const MethodInfo*))KeyCollection_get_Count_m4040092101_gshared)(__this, method)
