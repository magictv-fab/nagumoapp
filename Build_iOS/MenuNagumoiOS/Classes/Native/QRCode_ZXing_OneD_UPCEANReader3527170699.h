﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ZXing.OneD.UPCEANExtensionSupport
struct UPCEANExtensionSupport_t269614168;
// ZXing.OneD.EANManufacturerOrgSupport
struct EANManufacturerOrgSupport_t3278460736;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANReader
struct  UPCEANReader_t3527170699  : public OneDReader_t3436042911
{
public:
	// System.Text.StringBuilder ZXing.OneD.UPCEANReader::decodeRowStringBuffer
	StringBuilder_t243639308 * ___decodeRowStringBuffer_8;
	// ZXing.OneD.UPCEANExtensionSupport ZXing.OneD.UPCEANReader::extensionReader
	UPCEANExtensionSupport_t269614168 * ___extensionReader_9;
	// ZXing.OneD.EANManufacturerOrgSupport ZXing.OneD.UPCEANReader::eanManSupport
	EANManufacturerOrgSupport_t3278460736 * ___eanManSupport_10;

public:
	inline static int32_t get_offset_of_decodeRowStringBuffer_8() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699, ___decodeRowStringBuffer_8)); }
	inline StringBuilder_t243639308 * get_decodeRowStringBuffer_8() const { return ___decodeRowStringBuffer_8; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowStringBuffer_8() { return &___decodeRowStringBuffer_8; }
	inline void set_decodeRowStringBuffer_8(StringBuilder_t243639308 * value)
	{
		___decodeRowStringBuffer_8 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowStringBuffer_8, value);
	}

	inline static int32_t get_offset_of_extensionReader_9() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699, ___extensionReader_9)); }
	inline UPCEANExtensionSupport_t269614168 * get_extensionReader_9() const { return ___extensionReader_9; }
	inline UPCEANExtensionSupport_t269614168 ** get_address_of_extensionReader_9() { return &___extensionReader_9; }
	inline void set_extensionReader_9(UPCEANExtensionSupport_t269614168 * value)
	{
		___extensionReader_9 = value;
		Il2CppCodeGenWriteBarrier(&___extensionReader_9, value);
	}

	inline static int32_t get_offset_of_eanManSupport_10() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699, ___eanManSupport_10)); }
	inline EANManufacturerOrgSupport_t3278460736 * get_eanManSupport_10() const { return ___eanManSupport_10; }
	inline EANManufacturerOrgSupport_t3278460736 ** get_address_of_eanManSupport_10() { return &___eanManSupport_10; }
	inline void set_eanManSupport_10(EANManufacturerOrgSupport_t3278460736 * value)
	{
		___eanManSupport_10 = value;
		Il2CppCodeGenWriteBarrier(&___eanManSupport_10, value);
	}
};

struct UPCEANReader_t3527170699_StaticFields
{
public:
	// System.Int32 ZXing.OneD.UPCEANReader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_2;
	// System.Int32 ZXing.OneD.UPCEANReader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_3;
	// System.Int32[] ZXing.OneD.UPCEANReader::START_END_PATTERN
	Int32U5BU5D_t3230847821* ___START_END_PATTERN_4;
	// System.Int32[] ZXing.OneD.UPCEANReader::MIDDLE_PATTERN
	Int32U5BU5D_t3230847821* ___MIDDLE_PATTERN_5;
	// System.Int32[][] ZXing.OneD.UPCEANReader::L_PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___L_PATTERNS_6;
	// System.Int32[][] ZXing.OneD.UPCEANReader::L_AND_G_PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___L_AND_G_PATTERNS_7;

public:
	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_2() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699_StaticFields, ___MAX_AVG_VARIANCE_2)); }
	inline int32_t get_MAX_AVG_VARIANCE_2() const { return ___MAX_AVG_VARIANCE_2; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_2() { return &___MAX_AVG_VARIANCE_2; }
	inline void set_MAX_AVG_VARIANCE_2(int32_t value)
	{
		___MAX_AVG_VARIANCE_2 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_3() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_3)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_3() const { return ___MAX_INDIVIDUAL_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_3() { return &___MAX_INDIVIDUAL_VARIANCE_3; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_3(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_3 = value;
	}

	inline static int32_t get_offset_of_START_END_PATTERN_4() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699_StaticFields, ___START_END_PATTERN_4)); }
	inline Int32U5BU5D_t3230847821* get_START_END_PATTERN_4() const { return ___START_END_PATTERN_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_START_END_PATTERN_4() { return &___START_END_PATTERN_4; }
	inline void set_START_END_PATTERN_4(Int32U5BU5D_t3230847821* value)
	{
		___START_END_PATTERN_4 = value;
		Il2CppCodeGenWriteBarrier(&___START_END_PATTERN_4, value);
	}

	inline static int32_t get_offset_of_MIDDLE_PATTERN_5() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699_StaticFields, ___MIDDLE_PATTERN_5)); }
	inline Int32U5BU5D_t3230847821* get_MIDDLE_PATTERN_5() const { return ___MIDDLE_PATTERN_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_MIDDLE_PATTERN_5() { return &___MIDDLE_PATTERN_5; }
	inline void set_MIDDLE_PATTERN_5(Int32U5BU5D_t3230847821* value)
	{
		___MIDDLE_PATTERN_5 = value;
		Il2CppCodeGenWriteBarrier(&___MIDDLE_PATTERN_5, value);
	}

	inline static int32_t get_offset_of_L_PATTERNS_6() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699_StaticFields, ___L_PATTERNS_6)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_L_PATTERNS_6() const { return ___L_PATTERNS_6; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_L_PATTERNS_6() { return &___L_PATTERNS_6; }
	inline void set_L_PATTERNS_6(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___L_PATTERNS_6 = value;
		Il2CppCodeGenWriteBarrier(&___L_PATTERNS_6, value);
	}

	inline static int32_t get_offset_of_L_AND_G_PATTERNS_7() { return static_cast<int32_t>(offsetof(UPCEANReader_t3527170699_StaticFields, ___L_AND_G_PATTERNS_7)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_L_AND_G_PATTERNS_7() const { return ___L_AND_G_PATTERNS_7; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_L_AND_G_PATTERNS_7() { return &___L_AND_G_PATTERNS_7; }
	inline void set_L_AND_G_PATTERNS_7(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___L_AND_G_PATTERNS_7 = value;
		Il2CppCodeGenWriteBarrier(&___L_AND_G_PATTERNS_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
