﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTriggerInfo
struct  GetTriggerInfo_t1760428088  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetTriggerInfo::gameObjectHit
	FsmGameObject_t1697147867 * ___gameObjectHit_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetTriggerInfo::physicsMaterialName
	FsmString_t952858651 * ___physicsMaterialName_10;

public:
	inline static int32_t get_offset_of_gameObjectHit_9() { return static_cast<int32_t>(offsetof(GetTriggerInfo_t1760428088, ___gameObjectHit_9)); }
	inline FsmGameObject_t1697147867 * get_gameObjectHit_9() const { return ___gameObjectHit_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObjectHit_9() { return &___gameObjectHit_9; }
	inline void set_gameObjectHit_9(FsmGameObject_t1697147867 * value)
	{
		___gameObjectHit_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectHit_9, value);
	}

	inline static int32_t get_offset_of_physicsMaterialName_10() { return static_cast<int32_t>(offsetof(GetTriggerInfo_t1760428088, ___physicsMaterialName_10)); }
	inline FsmString_t952858651 * get_physicsMaterialName_10() const { return ___physicsMaterialName_10; }
	inline FsmString_t952858651 ** get_address_of_physicsMaterialName_10() { return &___physicsMaterialName_10; }
	inline void set_physicsMaterialName_10(FsmString_t952858651 * value)
	{
		___physicsMaterialName_10 = value;
		Il2CppCodeGenWriteBarrier(&___physicsMaterialName_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
