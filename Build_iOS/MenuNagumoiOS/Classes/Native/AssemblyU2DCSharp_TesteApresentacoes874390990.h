﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TesteApresentacoes
struct  TesteApresentacoes_t874390990  : public MonoBehaviour_t667441552
{
public:
	// System.String[] TesteApresentacoes::tracksIds
	StringU5BU5D_t4054002952* ___tracksIds_2;
	// System.Collections.Generic.List`1<System.String> TesteApresentacoes::TrackingsIds
	List_1_t1375417109 * ___TrackingsIds_3;
	// System.Collections.Generic.List`1<System.String> TesteApresentacoes::Labels
	List_1_t1375417109 * ___Labels_4;
	// System.Boolean TesteApresentacoes::_hasTracks
	bool ____hasTracks_5;
	// System.String TesteApresentacoes::currentWindow
	String_t* ___currentWindow_6;

public:
	inline static int32_t get_offset_of_tracksIds_2() { return static_cast<int32_t>(offsetof(TesteApresentacoes_t874390990, ___tracksIds_2)); }
	inline StringU5BU5D_t4054002952* get_tracksIds_2() const { return ___tracksIds_2; }
	inline StringU5BU5D_t4054002952** get_address_of_tracksIds_2() { return &___tracksIds_2; }
	inline void set_tracksIds_2(StringU5BU5D_t4054002952* value)
	{
		___tracksIds_2 = value;
		Il2CppCodeGenWriteBarrier(&___tracksIds_2, value);
	}

	inline static int32_t get_offset_of_TrackingsIds_3() { return static_cast<int32_t>(offsetof(TesteApresentacoes_t874390990, ___TrackingsIds_3)); }
	inline List_1_t1375417109 * get_TrackingsIds_3() const { return ___TrackingsIds_3; }
	inline List_1_t1375417109 ** get_address_of_TrackingsIds_3() { return &___TrackingsIds_3; }
	inline void set_TrackingsIds_3(List_1_t1375417109 * value)
	{
		___TrackingsIds_3 = value;
		Il2CppCodeGenWriteBarrier(&___TrackingsIds_3, value);
	}

	inline static int32_t get_offset_of_Labels_4() { return static_cast<int32_t>(offsetof(TesteApresentacoes_t874390990, ___Labels_4)); }
	inline List_1_t1375417109 * get_Labels_4() const { return ___Labels_4; }
	inline List_1_t1375417109 ** get_address_of_Labels_4() { return &___Labels_4; }
	inline void set_Labels_4(List_1_t1375417109 * value)
	{
		___Labels_4 = value;
		Il2CppCodeGenWriteBarrier(&___Labels_4, value);
	}

	inline static int32_t get_offset_of__hasTracks_5() { return static_cast<int32_t>(offsetof(TesteApresentacoes_t874390990, ____hasTracks_5)); }
	inline bool get__hasTracks_5() const { return ____hasTracks_5; }
	inline bool* get_address_of__hasTracks_5() { return &____hasTracks_5; }
	inline void set__hasTracks_5(bool value)
	{
		____hasTracks_5 = value;
	}

	inline static int32_t get_offset_of_currentWindow_6() { return static_cast<int32_t>(offsetof(TesteApresentacoes_t874390990, ___currentWindow_6)); }
	inline String_t* get_currentWindow_6() const { return ___currentWindow_6; }
	inline String_t** get_address_of_currentWindow_6() { return &___currentWindow_6; }
	inline void set_currentWindow_6(String_t* value)
	{
		___currentWindow_6 = value;
		Il2CppCodeGenWriteBarrier(&___currentWindow_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
