﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"

// UnityEngine.Color HutongGames.PlayMaker.FsmColor::get_Value()
extern "C"  Color_t4194546905  FsmColor_get_Value_m1679829997 (FsmColor_t2131419205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::set_Value(UnityEngine.Color)
extern "C"  void FsmColor_set_Value_m1684002054 (FsmColor_t2131419205 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::.ctor()
extern "C"  void FsmColor__ctor_m4262627118 (FsmColor_t2131419205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::.ctor(System.String)
extern "C"  void FsmColor__ctor_m1354543764 (FsmColor_t2131419205 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::.ctor(HutongGames.PlayMaker.FsmColor)
extern "C"  void FsmColor__ctor_m2758546367 (FsmColor_t2131419205 * __this, FsmColor_t2131419205 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmColor::ToString()
extern "C"  String_t* FsmColor_ToString_m3783619621 (FsmColor_t2131419205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmColor::op_Implicit(UnityEngine.Color)
extern "C"  FsmColor_t2131419205 * FsmColor_op_Implicit_m2192961033 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
