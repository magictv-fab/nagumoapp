﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.BZip2.BZip2Constants
struct BZip2Constants_t1757880094;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2Constants::.ctor()
extern "C"  void BZip2Constants__ctor_m3221984819 (BZip2Constants_t1757880094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2Constants::.cctor()
extern "C"  void BZip2Constants__cctor_m615185370 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
