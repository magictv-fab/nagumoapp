﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DeviceOrientationEvent
struct DeviceOrientationEvent_t1490992904;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::.ctor()
extern "C"  void DeviceOrientationEvent__ctor_m3655801438 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::Reset()
extern "C"  void DeviceOrientationEvent_Reset_m1302234379 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::OnEnter()
extern "C"  void DeviceOrientationEvent_OnEnter_m3628629685 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::OnUpdate()
extern "C"  void DeviceOrientationEvent_OnUpdate_m4246897198 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::DoDetectDeviceOrientation()
extern "C"  void DeviceOrientationEvent_DoDetectDeviceOrientation_m3036551464 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
