﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream
struct BZip2OutputStream_t147045086;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::.ctor(System.IO.Stream)
extern "C"  void BZip2OutputStream__ctor_m2892113882 (BZip2OutputStream_t147045086 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void BZip2OutputStream__ctor_m311822141 (BZip2OutputStream_t147045086 * __this, Stream_t1561764144 * ___stream0, int32_t ___blockSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Finalize()
extern "C"  void BZip2OutputStream_Finalize_m1995861567 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_IsStreamOwner()
extern "C"  bool BZip2OutputStream_get_IsStreamOwner_m2139790197 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::set_IsStreamOwner(System.Boolean)
extern "C"  void BZip2OutputStream_set_IsStreamOwner_m405988996 (BZip2OutputStream_t147045086 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_CanRead()
extern "C"  bool BZip2OutputStream_get_CanRead_m3629970034 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_CanSeek()
extern "C"  bool BZip2OutputStream_get_CanSeek_m3658725076 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_CanWrite()
extern "C"  bool BZip2OutputStream_get_CanWrite_m1382574885 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_Length()
extern "C"  int64_t BZip2OutputStream_get_Length_m2717545121 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_Position()
extern "C"  int64_t BZip2OutputStream_get_Position_m442246564 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::set_Position(System.Int64)
extern "C"  void BZip2OutputStream_set_Position_m854682329 (BZip2OutputStream_t147045086 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t BZip2OutputStream_Seek_m1835744419 (BZip2OutputStream_t147045086 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::SetLength(System.Int64)
extern "C"  void BZip2OutputStream_SetLength_m828755867 (BZip2OutputStream_t147045086 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::ReadByte()
extern "C"  int32_t BZip2OutputStream_ReadByte_m2183224081 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t BZip2OutputStream_Read_m1652306912 (BZip2OutputStream_t147045086 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_Write_m3605439337 (BZip2OutputStream_t147045086 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::WriteByte(System.Byte)
extern "C"  void BZip2OutputStream_WriteByte_m224944707 (BZip2OutputStream_t147045086 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Close()
extern "C"  void BZip2OutputStream_Close_m1702128729 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::MakeMaps()
extern "C"  void BZip2OutputStream_MakeMaps_m3916375558 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::WriteRun()
extern "C"  void BZip2OutputStream_WriteRun_m3199676621 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::get_BytesWritten()
extern "C"  int32_t BZip2OutputStream_get_BytesWritten_m279733256 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Dispose(System.Boolean)
extern "C"  void BZip2OutputStream_Dispose_m282317879 (BZip2OutputStream_t147045086 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Flush()
extern "C"  void BZip2OutputStream_Flush_m75216485 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Initialize()
extern "C"  void BZip2OutputStream_Initialize_m3622478129 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::InitBlock()
extern "C"  void BZip2OutputStream_InitBlock_m319992446 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::EndBlock()
extern "C"  void BZip2OutputStream_EndBlock_m4053821843 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::EndCompression()
extern "C"  void BZip2OutputStream_EndCompression_m787997804 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::BsSetStream(System.IO.Stream)
extern "C"  void BZip2OutputStream_BsSetStream_m1011090409 (BZip2OutputStream_t147045086 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::BsFinishedWithStream()
extern "C"  void BZip2OutputStream_BsFinishedWithStream_m2508945290 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::BsW(System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_BsW_m951822335 (BZip2OutputStream_t147045086 * __this, int32_t ___n0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::BsPutUChar(System.Int32)
extern "C"  void BZip2OutputStream_BsPutUChar_m4075826623 (BZip2OutputStream_t147045086 * __this, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::BsPutint(System.Int32)
extern "C"  void BZip2OutputStream_BsPutint_m4049392099 (BZip2OutputStream_t147045086 * __this, int32_t ___u0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::BsPutIntVS(System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_BsPutIntVS_m4232935255 (BZip2OutputStream_t147045086 * __this, int32_t ___numBits0, int32_t ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::SendMTFValues()
extern "C"  void BZip2OutputStream_SendMTFValues_m2911499546 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::MoveToFrontCodeAndSend()
extern "C"  void BZip2OutputStream_MoveToFrontCodeAndSend_m3034695702 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::SimpleSort(System.Int32,System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_SimpleSort_m1222513346 (BZip2OutputStream_t147045086 * __this, int32_t ___lo0, int32_t ___hi1, int32_t ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Vswap(System.Int32,System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_Vswap_m2655771931 (BZip2OutputStream_t147045086 * __this, int32_t ___p10, int32_t ___p21, int32_t ___n2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::QSort3(System.Int32,System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_QSort3_m2894026934 (BZip2OutputStream_t147045086 * __this, int32_t ___loSt0, int32_t ___hiSt1, int32_t ___dSt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::MainSort()
extern "C"  void BZip2OutputStream_MainSort_m13118168 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::RandomiseBlock()
extern "C"  void BZip2OutputStream_RandomiseBlock_m1599576822 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::DoReversibleTransformation()
extern "C"  void BZip2OutputStream_DoReversibleTransformation_m2011570730 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::FullGtU(System.Int32,System.Int32)
extern "C"  bool BZip2OutputStream_FullGtU_m1361461016 (BZip2OutputStream_t147045086 * __this, int32_t ___i10, int32_t ___i21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::AllocateCompressStructures()
extern "C"  void BZip2OutputStream_AllocateCompressStructures_m810181216 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::GenerateMTFValues()
extern "C"  void BZip2OutputStream_GenerateMTFValues_m690878733 (BZip2OutputStream_t147045086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Panic()
extern "C"  void BZip2OutputStream_Panic_m38630680 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::HbMakeCodeLengths(System.Char[],System.Int32[],System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_HbMakeCodeLengths_m2194511123 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___len0, Int32U5BU5D_t3230847821* ___freq1, int32_t ___alphaSize2, int32_t ___maxLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::HbAssignCodes(System.Int32[],System.Char[],System.Int32,System.Int32,System.Int32)
extern "C"  void BZip2OutputStream_HbAssignCodes_m1780490261 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___code0, CharU5BU5D_t3324145743* ___length1, int32_t ___minLen2, int32_t ___maxLen3, int32_t ___alphaSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::Med3(System.Byte,System.Byte,System.Byte)
extern "C"  uint8_t BZip2OutputStream_Med3_m1556086905 (Il2CppObject * __this /* static, unused */, uint8_t ___a0, uint8_t ___b1, uint8_t ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
