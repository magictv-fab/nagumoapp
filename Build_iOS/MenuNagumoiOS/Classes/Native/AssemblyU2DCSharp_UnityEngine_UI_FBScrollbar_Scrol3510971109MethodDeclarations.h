﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.FBScrollbar/ScrollEvent
struct ScrollEvent_t3510971109;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.FBScrollbar/ScrollEvent::.ctor()
extern "C"  void ScrollEvent__ctor_m1823296342 (ScrollEvent_t3510971109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
