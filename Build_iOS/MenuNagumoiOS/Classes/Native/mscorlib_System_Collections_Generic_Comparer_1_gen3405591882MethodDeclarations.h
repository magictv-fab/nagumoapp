﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<System.Int64>
struct Comparer_1_t3405591882;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<System.Int64>::.ctor()
extern "C"  void Comparer_1__ctor_m2857189491_gshared (Comparer_1_t3405591882 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2857189491(__this, method) ((  void (*) (Comparer_1_t3405591882 *, const MethodInfo*))Comparer_1__ctor_m2857189491_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Int64>::.cctor()
extern "C"  void Comparer_1__cctor_m2191432090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m2191432090(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m2191432090_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Int64>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m94129368_gshared (Comparer_1_t3405591882 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m94129368(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3405591882 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m94129368_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int64>::get_Default()
extern "C"  Comparer_1_t3405591882 * Comparer_1_get_Default_m3119157339_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m3119157339(__this /* static, unused */, method) ((  Comparer_1_t3405591882 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m3119157339_gshared)(__this /* static, unused */, method)
