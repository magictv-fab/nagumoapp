﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.HistoryDelaySituationFilter
struct HistoryDelaySituationFilter_t3528974141;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.transform.filters.HistoryDelaySituationFilter::.ctor()
extern "C"  void HistoryDelaySituationFilter__ctor_m2313583851 (HistoryDelaySituationFilter_t3528974141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationFilter::Start()
extern "C"  void HistoryDelaySituationFilter_Start_m1260721643 (HistoryDelaySituationFilter_t3528974141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationFilter::trackIn()
extern "C"  void HistoryDelaySituationFilter_trackIn_m1228914681 (HistoryDelaySituationFilter_t3528974141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationFilter::trackOut()
extern "C"  void HistoryDelaySituationFilter_trackOut_m3742439452 (HistoryDelaySituationFilter_t3528974141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationFilter::Update()
extern "C"  void HistoryDelaySituationFilter_Update_m433517442 (HistoryDelaySituationFilter_t3528974141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.HistoryDelaySituationFilter::filter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  HistoryDelaySituationFilter_filter_m33353725 (HistoryDelaySituationFilter_t3528974141 * __this, Quaternion_t1553702882  ___currentValue0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.HistoryDelaySituationFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  HistoryDelaySituationFilter__doFilter_m218426003 (HistoryDelaySituationFilter_t3528974141 * __this, Quaternion_t1553702882  ___current0, Quaternion_t1553702882  ___next1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationFilter::save(UnityEngine.Quaternion)
extern "C"  void HistoryDelaySituationFilter_save_m2446911585 (HistoryDelaySituationFilter_t3528974141 * __this, Quaternion_t1553702882  ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.HistoryDelaySituationFilter::_filter(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  HistoryDelaySituationFilter__filter_m2093084627 (HistoryDelaySituationFilter_t3528974141 * __this, Quaternion_t1553702882  ___supoustCameraAngle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.HistoryDelaySituationFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HistoryDelaySituationFilter__doFilter_m3389054055 (HistoryDelaySituationFilter_t3528974141 * __this, Vector3_t4282066566  ___currentPosition0, Vector3_t4282066566  ___newPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationFilter::save(UnityEngine.Vector3)
extern "C"  void HistoryDelaySituationFilter_save_m2412456707 (HistoryDelaySituationFilter_t3528974141 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.HistoryDelaySituationFilter::_filter(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HistoryDelaySituationFilter__filter_m1392977951 (HistoryDelaySituationFilter_t3528974141 * __this, Vector3_t4282066566  ___supoustCameraPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
