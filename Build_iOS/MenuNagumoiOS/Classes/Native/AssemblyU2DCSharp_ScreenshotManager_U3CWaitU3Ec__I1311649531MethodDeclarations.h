﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManager/<Wait>c__Iterator32
struct U3CWaitU3Ec__Iterator32_t1311649531;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenshotManager/<Wait>c__Iterator32::.ctor()
extern "C"  void U3CWaitU3Ec__Iterator32__ctor_m2992612560 (U3CWaitU3Ec__Iterator32_t1311649531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManager/<Wait>c__Iterator32::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitU3Ec__Iterator32_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3311285314 (U3CWaitU3Ec__Iterator32_t1311649531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManager/<Wait>c__Iterator32::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitU3Ec__Iterator32_System_Collections_IEnumerator_get_Current_m196410326 (U3CWaitU3Ec__Iterator32_t1311649531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenshotManager/<Wait>c__Iterator32::MoveNext()
extern "C"  bool U3CWaitU3Ec__Iterator32_MoveNext_m3913661988 (U3CWaitU3Ec__Iterator32_t1311649531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager/<Wait>c__Iterator32::Dispose()
extern "C"  void U3CWaitU3Ec__Iterator32_Dispose_m2465832205 (U3CWaitU3Ec__Iterator32_t1311649531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager/<Wait>c__Iterator32::Reset()
extern "C"  void U3CWaitU3Ec__Iterator32_Reset_m639045501 (U3CWaitU3Ec__Iterator32_t1311649531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
