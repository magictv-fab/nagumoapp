﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenLookUpdate
struct  iTweenLookUpdate_t2645160178  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenLookUpdate::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenLookUpdate::transformTarget
	FsmGameObject_t1697147867 * ___transformTarget_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenLookUpdate::vectorTarget
	FsmVector3_t533912882 * ___vectorTarget_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenLookUpdate::time
	FsmFloat_t2134102846 * ___time_12;
	// HutongGames.PlayMaker.Actions.iTweenFsmAction/AxisRestriction HutongGames.PlayMaker.Actions.iTweenLookUpdate::axis
	int32_t ___axis_13;
	// System.Collections.Hashtable HutongGames.PlayMaker.Actions.iTweenLookUpdate::hash
	Hashtable_t1407064410 * ___hash_14;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.iTweenLookUpdate::go
	GameObject_t3674682005 * ___go_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_transformTarget_10() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___transformTarget_10)); }
	inline FsmGameObject_t1697147867 * get_transformTarget_10() const { return ___transformTarget_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_transformTarget_10() { return &___transformTarget_10; }
	inline void set_transformTarget_10(FsmGameObject_t1697147867 * value)
	{
		___transformTarget_10 = value;
		Il2CppCodeGenWriteBarrier(&___transformTarget_10, value);
	}

	inline static int32_t get_offset_of_vectorTarget_11() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___vectorTarget_11)); }
	inline FsmVector3_t533912882 * get_vectorTarget_11() const { return ___vectorTarget_11; }
	inline FsmVector3_t533912882 ** get_address_of_vectorTarget_11() { return &___vectorTarget_11; }
	inline void set_vectorTarget_11(FsmVector3_t533912882 * value)
	{
		___vectorTarget_11 = value;
		Il2CppCodeGenWriteBarrier(&___vectorTarget_11, value);
	}

	inline static int32_t get_offset_of_time_12() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___time_12)); }
	inline FsmFloat_t2134102846 * get_time_12() const { return ___time_12; }
	inline FsmFloat_t2134102846 ** get_address_of_time_12() { return &___time_12; }
	inline void set_time_12(FsmFloat_t2134102846 * value)
	{
		___time_12 = value;
		Il2CppCodeGenWriteBarrier(&___time_12, value);
	}

	inline static int32_t get_offset_of_axis_13() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___axis_13)); }
	inline int32_t get_axis_13() const { return ___axis_13; }
	inline int32_t* get_address_of_axis_13() { return &___axis_13; }
	inline void set_axis_13(int32_t value)
	{
		___axis_13 = value;
	}

	inline static int32_t get_offset_of_hash_14() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___hash_14)); }
	inline Hashtable_t1407064410 * get_hash_14() const { return ___hash_14; }
	inline Hashtable_t1407064410 ** get_address_of_hash_14() { return &___hash_14; }
	inline void set_hash_14(Hashtable_t1407064410 * value)
	{
		___hash_14 = value;
		Il2CppCodeGenWriteBarrier(&___hash_14, value);
	}

	inline static int32_t get_offset_of_go_15() { return static_cast<int32_t>(offsetof(iTweenLookUpdate_t2645160178, ___go_15)); }
	inline GameObject_t3674682005 * get_go_15() const { return ___go_15; }
	inline GameObject_t3674682005 ** get_address_of_go_15() { return &___go_15; }
	inline void set_go_15(GameObject_t3674682005 * value)
	{
		___go_15 = value;
		Il2CppCodeGenWriteBarrier(&___go_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
