﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Version/ECB
struct ECB_t2948426741;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.Datamatrix.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
extern "C"  void ECB__ctor_m1241506672 (ECB_t2948426741 * __this, int32_t ___count0, int32_t ___dataCodewords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_Count()
extern "C"  int32_t ECB_get_Count_m3208260936 (ECB_t2948426741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_DataCodewords()
extern "C"  int32_t ECB_get_DataCodewords_m273147851 (ECB_t2948426741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
