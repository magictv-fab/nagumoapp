﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloideManager/<ILoadImgs>c__Iterator81
struct U3CILoadImgsU3Ec__Iterator81_t781055235;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TabloideManager/<ILoadImgs>c__Iterator81::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator81__ctor_m2224315640 (U3CILoadImgsU3Ec__Iterator81_t781055235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TabloideManager/<ILoadImgs>c__Iterator81::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator81_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3943637732 (U3CILoadImgsU3Ec__Iterator81_t781055235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TabloideManager/<ILoadImgs>c__Iterator81::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator81_System_Collections_IEnumerator_get_Current_m3976949368 (U3CILoadImgsU3Ec__Iterator81_t781055235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TabloideManager/<ILoadImgs>c__Iterator81::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator81_MoveNext_m496718308 (U3CILoadImgsU3Ec__Iterator81_t781055235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager/<ILoadImgs>c__Iterator81::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator81_Dispose_m2866866997 (U3CILoadImgsU3Ec__Iterator81_t781055235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager/<ILoadImgs>c__Iterator81::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator81_Reset_m4165715877 (U3CILoadImgsU3Ec__Iterator81_t781055235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
