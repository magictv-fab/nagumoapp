﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRectFields
struct SetRectFields_t2990573965;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRectFields::.ctor()
extern "C"  void SetRectFields__ctor_m31209033 (SetRectFields_t2990573965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::Reset()
extern "C"  void SetRectFields_Reset_m1972609270 (SetRectFields_t2990573965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::OnEnter()
extern "C"  void SetRectFields_OnEnter_m3613805536 (SetRectFields_t2990573965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::OnUpdate()
extern "C"  void SetRectFields_OnUpdate_m3787348579 (SetRectFields_t2990573965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::DoSetRectFields()
extern "C"  void SetRectFields_DoSetRectFields_m2767049691 (SetRectFields_t2990573965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
