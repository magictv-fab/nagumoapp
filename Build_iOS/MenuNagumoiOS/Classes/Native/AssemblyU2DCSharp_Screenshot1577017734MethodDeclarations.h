﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Screenshot
struct Screenshot_t1577017734;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// Screenshot/OnCompleteEventHandler
struct OnCompleteEventHandler_t2690410097;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Screenshot1577017734.h"
#include "AssemblyU2DCSharp_Screenshot_OnCompleteEventHandle2690410097.h"
#include "mscorlib_System_String7231557.h"

// System.Void Screenshot::.ctor()
extern "C"  void Screenshot__ctor_m1056607701 (Screenshot_t1577017734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::.cctor()
extern "C"  void Screenshot__cctor_m2207971448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Screenshot::get_SelfGameObject()
extern "C"  GameObject_t3674682005 * Screenshot_get_SelfGameObject_m2675445706 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Screenshot Screenshot::get_SelfIsntance()
extern "C"  Screenshot_t1577017734 * Screenshot_get_SelfIsntance_m1848132282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::set_SelfIsntance(Screenshot)
extern "C"  void Screenshot_set_SelfIsntance_m1659157873 (Il2CppObject * __this /* static, unused */, Screenshot_t1577017734 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::Awake()
extern "C"  void Screenshot_Awake_m1294212920 (Screenshot_t1577017734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::AddCompleteEventHandler(Screenshot/OnCompleteEventHandler)
extern "C"  void Screenshot_AddCompleteEventHandler_m3072700708 (Il2CppObject * __this /* static, unused */, OnCompleteEventHandler_t2690410097 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::RemoveCompleteEventHandler(Screenshot/OnCompleteEventHandler)
extern "C"  void Screenshot_RemoveCompleteEventHandler_m3589845381 (Il2CppObject * __this /* static, unused */, OnCompleteEventHandler_t2690410097 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::RaiseComplete(System.String)
extern "C"  void Screenshot_RaiseComplete_m726383882 (Screenshot_t1577017734 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::Save()
extern "C"  void Screenshot_Save_m3169773324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::doSave(System.String,System.String)
extern "C"  void Screenshot_doSave_m819118471 (Screenshot_t1577017734 * __this, String_t* ___imagePrefix0, String_t* ___albumName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::ScreenshotSaved(System.String)
extern "C"  void Screenshot_ScreenshotSaved_m3178226318 (Screenshot_t1577017734 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Screenshot::WaitFrameLock()
extern "C"  Il2CppObject * Screenshot_WaitFrameLock_m935393070 (Screenshot_t1577017734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot::OnDestroy()
extern "C"  void Screenshot_OnDestroy_m2423746638 (Screenshot_t1577017734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Screenshot::<Awake>m__13(System.String)
extern "C"  bool Screenshot_U3CAwakeU3Em__13_m2009402853 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
