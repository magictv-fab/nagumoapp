﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1458694378MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1456656589(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1341880959 *, Dictionary_2_t2641275246 *, const MethodInfo*))ValueCollection__ctor_m2455720537_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1163369925(__this, ___item0, method) ((  void (*) (ValueCollection_t1341880959 *, StringU5BU5D_t4054002952*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4105091769_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3068474638(__this, method) ((  void (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3852004354_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1223126853(__this, ___item0, method) ((  bool (*) (ValueCollection_t1341880959 *, StringU5BU5D_t4054002952*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3827655505_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2263717162(__this, ___item0, method) ((  bool (*) (ValueCollection_t1341880959 *, StringU5BU5D_t4054002952*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2498829366_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4247452366(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2256307202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3865009298(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1341880959 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m308408966_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3601238817(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1137105045_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3793270008(__this, method) ((  bool (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2102831364_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m973909720(__this, method) ((  bool (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1945064676_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3990835786(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2646229526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1990330836(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1341880959 *, StringU5BU5DU5BU5D_t3538327001*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2089912416_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2123047997(__this, method) ((  Enumerator_t573108654  (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_GetEnumerator_m1286678665_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::get_Count()
#define ValueCollection_get_Count_m2028327954(__this, method) ((  int32_t (*) (ValueCollection_t1341880959 *, const MethodInfo*))ValueCollection_get_Count_m3029300382_gshared)(__this, method)
