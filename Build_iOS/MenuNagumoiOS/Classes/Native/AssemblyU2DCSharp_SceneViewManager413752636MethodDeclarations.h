﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneViewManager
struct SceneViewManager_t413752636;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"

// System.Void SceneViewManager::.ctor()
extern "C"  void SceneViewManager__ctor_m1830481119 (SceneViewManager_t413752636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneViewManager::Awake()
extern "C"  void SceneViewManager_Awake_m2068086338 (SceneViewManager_t413752636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneViewManager::Start()
extern "C"  void SceneViewManager_Start_m777618911 (SceneViewManager_t413752636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneViewManager::Update()
extern "C"  void SceneViewManager_Update_m2637201934 (SceneViewManager_t413752636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneViewManager::OnDestroy()
extern "C"  void SceneViewManager_OnDestroy_m3923589720 (SceneViewManager_t413752636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneViewManager::OnGUI()
extern "C"  void SceneViewManager_OnGUI_m1325879769 (SceneViewManager_t413752636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneViewManager::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C"  void SceneViewManager_OnVuforiaInitializationError_m2917092354 (SceneViewManager_t413752636 * __this, int32_t ___initError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
