﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugInt
struct DebugInt_t295189828;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugInt::.ctor()
extern "C"  void DebugInt__ctor_m2930305442 (DebugInt_t295189828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugInt::Reset()
extern "C"  void DebugInt_Reset_m576738383 (DebugInt_t295189828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugInt::OnEnter()
extern "C"  void DebugInt_OnEnter_m2211679481 (DebugInt_t295189828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
