﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion
struct GetAnimatorApplyRootMotion_t212715153;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::.ctor()
extern "C"  void GetAnimatorApplyRootMotion__ctor_m1420210421 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::Reset()
extern "C"  void GetAnimatorApplyRootMotion_Reset_m3361610658 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::OnEnter()
extern "C"  void GetAnimatorApplyRootMotion_OnEnter_m2709310348 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::GetApplyMotionRoot()
extern "C"  void GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
