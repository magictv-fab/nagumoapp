﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.Code39Reader
struct Code39Reader_t1346536404;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.OneD.Code39Reader::.ctor(System.Boolean,System.Boolean)
extern "C"  void Code39Reader__ctor_m3330296503 (Code39Reader_t1346536404 * __this, bool ___usingCheckDigit0, bool ___extendedMode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.Code39Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * Code39Reader_decodeRow_m2016894534 (Code39Reader_t1346536404 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.Code39Reader::findAsteriskPattern(ZXing.Common.BitArray,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* Code39Reader_findAsteriskPattern_m3333762150 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___counters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.Code39Reader::toNarrowWidePattern(System.Int32[])
extern "C"  int32_t Code39Reader_toNarrowWidePattern_m1178238833 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.Code39Reader::patternToChar(System.Int32,System.Char&)
extern "C"  bool Code39Reader_patternToChar_m3441129154 (Il2CppObject * __this /* static, unused */, int32_t ___pattern0, Il2CppChar* ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.Code39Reader::decodeExtended(System.String)
extern "C"  String_t* Code39Reader_decodeExtended_m3933189481 (Il2CppObject * __this /* static, unused */, String_t* ___encoded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code39Reader::.cctor()
extern "C"  void Code39Reader__cctor_m4028385470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
