﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip
struct CapturePoseAsAnimationClip_t1031263059;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"

// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::.ctor()
extern "C"  void CapturePoseAsAnimationClip__ctor_m4161920627 (CapturePoseAsAnimationClip_t1031263059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::Reset()
extern "C"  void CapturePoseAsAnimationClip_Reset_m1808353568 (CapturePoseAsAnimationClip_t1031263059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::OnEnter()
extern "C"  void CapturePoseAsAnimationClip_OnEnter_m382898570 (CapturePoseAsAnimationClip_t1031263059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::DoCaptureAnimationClip()
extern "C"  void CapturePoseAsAnimationClip_DoCaptureAnimationClip_m896255658 (CapturePoseAsAnimationClip_t1031263059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CaptureTransform(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CaptureTransform_m4031391249 (CapturePoseAsAnimationClip_t1031263059 * __this, Transform_t1659122786 * ___transform0, String_t* ___path1, AnimationClip_t2007702890 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CapturePosition(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CapturePosition_m4234139144 (CapturePoseAsAnimationClip_t1031263059 * __this, Transform_t1659122786 * ___transform0, String_t* ___path1, AnimationClip_t2007702890 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CaptureRotation(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CaptureRotation_m3499682067 (CapturePoseAsAnimationClip_t1031263059 * __this, Transform_t1659122786 * ___transform0, String_t* ___path1, AnimationClip_t2007702890 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CaptureScale(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CaptureScale_m941862771 (CapturePoseAsAnimationClip_t1031263059 * __this, Transform_t1659122786 * ___transform0, String_t* ___path1, AnimationClip_t2007702890 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::SetConstantCurve(UnityEngine.AnimationClip,System.String,System.String,System.Single)
extern "C"  void CapturePoseAsAnimationClip_SetConstantCurve_m1173257720 (CapturePoseAsAnimationClip_t1031263059 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___childPath1, String_t* ___propertyPath2, float ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
