﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.AztecResultMetadata
struct  AztecResultMetadata_t1409764443  : public Il2CppObject
{
public:
	// System.Boolean ZXing.Aztec.AztecResultMetadata::<Compact>k__BackingField
	bool ___U3CCompactU3Ek__BackingField_0;
	// System.Int32 ZXing.Aztec.AztecResultMetadata::<Datablocks>k__BackingField
	int32_t ___U3CDatablocksU3Ek__BackingField_1;
	// System.Int32 ZXing.Aztec.AztecResultMetadata::<Layers>k__BackingField
	int32_t ___U3CLayersU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCompactU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AztecResultMetadata_t1409764443, ___U3CCompactU3Ek__BackingField_0)); }
	inline bool get_U3CCompactU3Ek__BackingField_0() const { return ___U3CCompactU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCompactU3Ek__BackingField_0() { return &___U3CCompactU3Ek__BackingField_0; }
	inline void set_U3CCompactU3Ek__BackingField_0(bool value)
	{
		___U3CCompactU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDatablocksU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AztecResultMetadata_t1409764443, ___U3CDatablocksU3Ek__BackingField_1)); }
	inline int32_t get_U3CDatablocksU3Ek__BackingField_1() const { return ___U3CDatablocksU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CDatablocksU3Ek__BackingField_1() { return &___U3CDatablocksU3Ek__BackingField_1; }
	inline void set_U3CDatablocksU3Ek__BackingField_1(int32_t value)
	{
		___U3CDatablocksU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLayersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AztecResultMetadata_t1409764443, ___U3CLayersU3Ek__BackingField_2)); }
	inline int32_t get_U3CLayersU3Ek__BackingField_2() const { return ___U3CLayersU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLayersU3Ek__BackingField_2() { return &___U3CLayersU3Ek__BackingField_2; }
	inline void set_U3CLayersU3Ek__BackingField_2(int32_t value)
	{
		___U3CLayersU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
