﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cadastro/<IBuscaCep>c__Iterator49
struct U3CIBuscaCepU3Ec__Iterator49_t2084848971;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Cadastro/<IBuscaCep>c__Iterator49::.ctor()
extern "C"  void U3CIBuscaCepU3Ec__Iterator49__ctor_m272260224 (U3CIBuscaCepU3Ec__Iterator49_t2084848971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<IBuscaCep>c__Iterator49::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIBuscaCepU3Ec__Iterator49_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3487512594 (U3CIBuscaCepU3Ec__Iterator49_t2084848971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<IBuscaCep>c__Iterator49::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIBuscaCepU3Ec__Iterator49_System_Collections_IEnumerator_get_Current_m3889668006 (U3CIBuscaCepU3Ec__Iterator49_t2084848971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro/<IBuscaCep>c__Iterator49::MoveNext()
extern "C"  bool U3CIBuscaCepU3Ec__Iterator49_MoveNext_m1378578036 (U3CIBuscaCepU3Ec__Iterator49_t2084848971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<IBuscaCep>c__Iterator49::Dispose()
extern "C"  void U3CIBuscaCepU3Ec__Iterator49_Dispose_m3842320573 (U3CIBuscaCepU3Ec__Iterator49_t2084848971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<IBuscaCep>c__Iterator49::Reset()
extern "C"  void U3CIBuscaCepU3Ec__Iterator49_Reset_m2213660461 (U3CIBuscaCepU3Ec__Iterator49_t2084848971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
