﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.GridSampler
struct GridSampler_t228036704;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Common.PerspectiveTransform
struct PerspectiveTransform_t2438931808;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_Common_PerspectiveTransform2438931808.h"

// ZXing.Common.GridSampler ZXing.Common.GridSampler::get_Instance()
extern "C"  GridSampler_t228036704 * GridSampler_get_Instance_m1052726692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Common.GridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,ZXing.Common.PerspectiveTransform)
extern "C"  BitMatrix_t1058711404 * GridSampler_sampleGrid_m2137422488 (GridSampler_t228036704 * __this, BitMatrix_t1058711404 * ___image0, int32_t ___dimensionX1, int32_t ___dimensionY2, PerspectiveTransform_t2438931808 * ___transform3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.GridSampler::checkAndNudgePoints(ZXing.Common.BitMatrix,System.Single[])
extern "C"  bool GridSampler_checkAndNudgePoints_m4175884889 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, SingleU5BU5D_t2316563989* ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.GridSampler::.ctor()
extern "C"  void GridSampler__ctor_m1940107442 (GridSampler_t228036704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.GridSampler::.cctor()
extern "C"  void GridSampler__cctor_m3826659643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
