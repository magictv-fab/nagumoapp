﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PedidoEnvioData
struct PedidoEnvioData_t1309077304;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Exception
struct Exception_t3991598821;
// System.Object
struct Il2CppObject;
// AcougueAvaliacao
struct AcougueAvaliacao_t3448807114;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AcougueAvaliacao/<IAvaliarPedido>c__Iterator44
struct  U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344  : public Il2CppObject
{
public:
	// PedidoEnvioData AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<pedidoEnvioData>__0
	PedidoEnvioData_t1309077304 * ___U3CpedidoEnvioDataU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<$s_299>__1
	Enumerator_t767573031  ___U3CU24s_299U3E__1_1;
	// UnityEngine.GameObject AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<go>__2
	GameObject_t3674682005 * ___U3CgoU3E__2_2;
	// System.String AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<json>__3
	String_t* ___U3CjsonU3E__3_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<headers>__4
	Dictionary_2_t827649927 * ___U3CheadersU3E__4_4;
	// System.Byte[] AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<pData>__5
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__5_5;
	// UnityEngine.WWW AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<www>__6
	WWW_t3134621005 * ___U3CwwwU3E__6_6;
	// System.String AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<message>__7
	String_t* ___U3CmessageU3E__7_7;
	// System.Exception AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<ex>__8
	Exception_t3991598821 * ___U3CexU3E__8_8;
	// System.Int32 AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::$PC
	int32_t ___U24PC_9;
	// System.Object AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::$current
	Il2CppObject * ___U24current_10;
	// AcougueAvaliacao AcougueAvaliacao/<IAvaliarPedido>c__Iterator44::<>f__this
	AcougueAvaliacao_t3448807114 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CpedidoEnvioDataU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CpedidoEnvioDataU3E__0_0)); }
	inline PedidoEnvioData_t1309077304 * get_U3CpedidoEnvioDataU3E__0_0() const { return ___U3CpedidoEnvioDataU3E__0_0; }
	inline PedidoEnvioData_t1309077304 ** get_address_of_U3CpedidoEnvioDataU3E__0_0() { return &___U3CpedidoEnvioDataU3E__0_0; }
	inline void set_U3CpedidoEnvioDataU3E__0_0(PedidoEnvioData_t1309077304 * value)
	{
		___U3CpedidoEnvioDataU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpedidoEnvioDataU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_299U3E__1_1() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CU24s_299U3E__1_1)); }
	inline Enumerator_t767573031  get_U3CU24s_299U3E__1_1() const { return ___U3CU24s_299U3E__1_1; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_299U3E__1_1() { return &___U3CU24s_299U3E__1_1; }
	inline void set_U3CU24s_299U3E__1_1(Enumerator_t767573031  value)
	{
		___U3CU24s_299U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CgoU3E__2_2() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CgoU3E__2_2)); }
	inline GameObject_t3674682005 * get_U3CgoU3E__2_2() const { return ___U3CgoU3E__2_2; }
	inline GameObject_t3674682005 ** get_address_of_U3CgoU3E__2_2() { return &___U3CgoU3E__2_2; }
	inline void set_U3CgoU3E__2_2(GameObject_t3674682005 * value)
	{
		___U3CgoU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgoU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__3_3() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CjsonU3E__3_3)); }
	inline String_t* get_U3CjsonU3E__3_3() const { return ___U3CjsonU3E__3_3; }
	inline String_t** get_address_of_U3CjsonU3E__3_3() { return &___U3CjsonU3E__3_3; }
	inline void set_U3CjsonU3E__3_3(String_t* value)
	{
		___U3CjsonU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__4_4() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CheadersU3E__4_4)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__4_4() const { return ___U3CheadersU3E__4_4; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__4_4() { return &___U3CheadersU3E__4_4; }
	inline void set_U3CheadersU3E__4_4(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__5_5() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CpDataU3E__5_5)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__5_5() const { return ___U3CpDataU3E__5_5; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__5_5() { return &___U3CpDataU3E__5_5; }
	inline void set_U3CpDataU3E__5_5(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__6_6() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CwwwU3E__6_6)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__6_6() const { return ___U3CwwwU3E__6_6; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__6_6() { return &___U3CwwwU3E__6_6; }
	inline void set_U3CwwwU3E__6_6(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__7_7() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CmessageU3E__7_7)); }
	inline String_t* get_U3CmessageU3E__7_7() const { return ___U3CmessageU3E__7_7; }
	inline String_t** get_address_of_U3CmessageU3E__7_7() { return &___U3CmessageU3E__7_7; }
	inline void set_U3CmessageU3E__7_7(String_t* value)
	{
		___U3CmessageU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__8_8() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CexU3E__8_8)); }
	inline Exception_t3991598821 * get_U3CexU3E__8_8() const { return ___U3CexU3E__8_8; }
	inline Exception_t3991598821 ** get_address_of_U3CexU3E__8_8() { return &___U3CexU3E__8_8; }
	inline void set_U3CexU3E__8_8(Exception_t3991598821 * value)
	{
		___U3CexU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CIAvaliarPedidoU3Ec__Iterator44_t3152181344, ___U3CU3Ef__this_11)); }
	inline AcougueAvaliacao_t3448807114 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline AcougueAvaliacao_t3448807114 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(AcougueAvaliacao_t3448807114 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
