﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkInitializeServer
struct NetworkInitializeServer_t4294133583;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkInitializeServer::.ctor()
extern "C"  void NetworkInitializeServer__ctor_m3344519367 (NetworkInitializeServer_t4294133583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInitializeServer::Reset()
extern "C"  void NetworkInitializeServer_Reset_m990952308 (NetworkInitializeServer_t4294133583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInitializeServer::OnEnter()
extern "C"  void NetworkInitializeServer_OnEnter_m839302878 (NetworkInitializeServer_t4294133583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
