﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_AI01decode1016996677.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder
struct  AI01393xDecoder_t2363260858  : public AI01decoder_t1016996677
{
public:

public:
};

struct AI01393xDecoder_t2363260858_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::LAST_DIGIT_SIZE
	int32_t ___LAST_DIGIT_SIZE_4;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::FIRST_THREE_DIGITS_SIZE
	int32_t ___FIRST_THREE_DIGITS_SIZE_5;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI01393xDecoder_t2363260858_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}

	inline static int32_t get_offset_of_LAST_DIGIT_SIZE_4() { return static_cast<int32_t>(offsetof(AI01393xDecoder_t2363260858_StaticFields, ___LAST_DIGIT_SIZE_4)); }
	inline int32_t get_LAST_DIGIT_SIZE_4() const { return ___LAST_DIGIT_SIZE_4; }
	inline int32_t* get_address_of_LAST_DIGIT_SIZE_4() { return &___LAST_DIGIT_SIZE_4; }
	inline void set_LAST_DIGIT_SIZE_4(int32_t value)
	{
		___LAST_DIGIT_SIZE_4 = value;
	}

	inline static int32_t get_offset_of_FIRST_THREE_DIGITS_SIZE_5() { return static_cast<int32_t>(offsetof(AI01393xDecoder_t2363260858_StaticFields, ___FIRST_THREE_DIGITS_SIZE_5)); }
	inline int32_t get_FIRST_THREE_DIGITS_SIZE_5() const { return ___FIRST_THREE_DIGITS_SIZE_5; }
	inline int32_t* get_address_of_FIRST_THREE_DIGITS_SIZE_5() { return &___FIRST_THREE_DIGITS_SIZE_5; }
	inline void set_FIRST_THREE_DIGITS_SIZE_5(int32_t value)
	{
		___FIRST_THREE_DIGITS_SIZE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
