﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollRectTool
struct ScrollRectTool_t3827539881;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollRectTool::.ctor()
extern "C"  void ScrollRectTool__ctor_m1527823122 (ScrollRectTool_t3827539881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool::Start()
extern "C"  void ScrollRectTool_Start_m474960914 (ScrollRectTool_t3827539881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool::LateUpdate()
extern "C"  void ScrollRectTool_LateUpdate_m3233327745 (ScrollRectTool_t3827539881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool::Update()
extern "C"  void ScrollRectTool_Update_m1844738619 (ScrollRectTool_t3827539881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool::TouchDown()
extern "C"  void ScrollRectTool_TouchDown_m86938865 (ScrollRectTool_t3827539881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool::TouchUp()
extern "C"  void ScrollRectTool_TouchUp_m3665395754 (ScrollRectTool_t3827539881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
