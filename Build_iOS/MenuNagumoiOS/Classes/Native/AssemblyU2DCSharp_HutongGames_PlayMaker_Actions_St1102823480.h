﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringContains
struct  StringContains_t1102823480  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringContains::stringVariable
	FsmString_t952858651 * ___stringVariable_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringContains::containsString
	FsmString_t952858651 * ___containsString_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringContains::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringContains::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StringContains::storeResult
	FsmBool_t1075959796 * ___storeResult_13;
	// System.Boolean HutongGames.PlayMaker.Actions.StringContains::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_stringVariable_9() { return static_cast<int32_t>(offsetof(StringContains_t1102823480, ___stringVariable_9)); }
	inline FsmString_t952858651 * get_stringVariable_9() const { return ___stringVariable_9; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_9() { return &___stringVariable_9; }
	inline void set_stringVariable_9(FsmString_t952858651 * value)
	{
		___stringVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_9, value);
	}

	inline static int32_t get_offset_of_containsString_10() { return static_cast<int32_t>(offsetof(StringContains_t1102823480, ___containsString_10)); }
	inline FsmString_t952858651 * get_containsString_10() const { return ___containsString_10; }
	inline FsmString_t952858651 ** get_address_of_containsString_10() { return &___containsString_10; }
	inline void set_containsString_10(FsmString_t952858651 * value)
	{
		___containsString_10 = value;
		Il2CppCodeGenWriteBarrier(&___containsString_10, value);
	}

	inline static int32_t get_offset_of_trueEvent_11() { return static_cast<int32_t>(offsetof(StringContains_t1102823480, ___trueEvent_11)); }
	inline FsmEvent_t2133468028 * get_trueEvent_11() const { return ___trueEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_11() { return &___trueEvent_11; }
	inline void set_trueEvent_11(FsmEvent_t2133468028 * value)
	{
		___trueEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_11, value);
	}

	inline static int32_t get_offset_of_falseEvent_12() { return static_cast<int32_t>(offsetof(StringContains_t1102823480, ___falseEvent_12)); }
	inline FsmEvent_t2133468028 * get_falseEvent_12() const { return ___falseEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_12() { return &___falseEvent_12; }
	inline void set_falseEvent_12(FsmEvent_t2133468028 * value)
	{
		___falseEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(StringContains_t1102823480, ___storeResult_13)); }
	inline FsmBool_t1075959796 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmBool_t1075959796 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(StringContains_t1102823480, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
