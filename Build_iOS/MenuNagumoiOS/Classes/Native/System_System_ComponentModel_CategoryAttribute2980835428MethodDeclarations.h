﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.CategoryAttribute
struct CategoryAttribute_t2980835428;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.ComponentModel.CategoryAttribute::.ctor()
extern "C"  void CategoryAttribute__ctor_m929454319 (CategoryAttribute_t2980835428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.CategoryAttribute::.ctor(System.String)
extern "C"  void CategoryAttribute__ctor_m1880719987 (CategoryAttribute_t2980835428 * __this, String_t* ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.CategoryAttribute::.cctor()
extern "C"  void CategoryAttribute__cctor_m2561183902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.CategoryAttribute System.ComponentModel.CategoryAttribute::get_Default()
extern "C"  CategoryAttribute_t2980835428 * CategoryAttribute_get_Default_m4105027997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.CategoryAttribute::GetLocalizedString(System.String)
extern "C"  String_t* CategoryAttribute_GetLocalizedString_m1842471002 (CategoryAttribute_t2980835428 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.CategoryAttribute::get_Category()
extern "C"  String_t* CategoryAttribute_get_Category_m3369045689 (CategoryAttribute_t2980835428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.CategoryAttribute::Equals(System.Object)
extern "C"  bool CategoryAttribute_Equals_m3220843190 (CategoryAttribute_t2980835428 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.CategoryAttribute::GetHashCode()
extern "C"  int32_t CategoryAttribute_GetHashCode_m4091142798 (CategoryAttribute_t2980835428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.CategoryAttribute::IsDefaultAttribute()
extern "C"  bool CategoryAttribute_IsDefaultAttribute_m2333144068 (CategoryAttribute_t2980835428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
