﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMass
struct  SetMass_t3798673028  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMass::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetMass::mass
	FsmFloat_t2134102846 * ___mass_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetMass_t3798673028, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_mass_12() { return static_cast<int32_t>(offsetof(SetMass_t3798673028, ___mass_12)); }
	inline FsmFloat_t2134102846 * get_mass_12() const { return ___mass_12; }
	inline FsmFloat_t2134102846 ** get_address_of_mass_12() { return &___mass_12; }
	inline void set_mass_12(FsmFloat_t2134102846 * value)
	{
		___mass_12 = value;
		Il2CppCodeGenWriteBarrier(&___mass_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
