﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.Byte[]>
struct IList_1_t2660440376;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Byte[] ZXing.Common.DecoderResult::get_RawBytes()
extern "C"  ByteU5BU5D_t4260760469* DecoderResult_get_RawBytes_m376715911 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_RawBytes(System.Byte[])
extern "C"  void DecoderResult_set_RawBytes_m2342151128 (DecoderResult_t3752650303 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.DecoderResult::get_Text()
extern "C"  String_t* DecoderResult_get_Text_m818096650 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_Text(System.String)
extern "C"  void DecoderResult_set_Text_m3755690823 (DecoderResult_t3752650303 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Byte[]> ZXing.Common.DecoderResult::get_ByteSegments()
extern "C"  Il2CppObject* DecoderResult_get_ByteSegments_m2144217197 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_ByteSegments(System.Collections.Generic.IList`1<System.Byte[]>)
extern "C"  void DecoderResult_set_ByteSegments_m229889748 (DecoderResult_t3752650303 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.DecoderResult::get_ECLevel()
extern "C"  String_t* DecoderResult_get_ECLevel_m3737900171 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_ECLevel(System.String)
extern "C"  void DecoderResult_set_ECLevel_m1044324328 (DecoderResult_t3752650303 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.DecoderResult::get_StructuredAppend()
extern "C"  bool DecoderResult_get_StructuredAppend_m2125558457 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_ErrorsCorrected(System.Int32)
extern "C"  void DecoderResult_set_ErrorsCorrected_m1868503139 (DecoderResult_t3752650303 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.DecoderResult::get_StructuredAppendSequenceNumber()
extern "C"  int32_t DecoderResult_get_StructuredAppendSequenceNumber_m3977386013 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_StructuredAppendSequenceNumber(System.Int32)
extern "C"  void DecoderResult_set_StructuredAppendSequenceNumber_m4124546580 (DecoderResult_t3752650303 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_Erasures(System.Int32)
extern "C"  void DecoderResult_set_Erasures_m3683642409 (DecoderResult_t3752650303 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.DecoderResult::get_StructuredAppendParity()
extern "C"  int32_t DecoderResult_get_StructuredAppendParity_m4193084256 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_StructuredAppendParity(System.Int32)
extern "C"  void DecoderResult_set_StructuredAppendParity_m4109277911 (DecoderResult_t3752650303 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ZXing.Common.DecoderResult::get_Other()
extern "C"  Il2CppObject * DecoderResult_get_Other_m1493723555 (DecoderResult_t3752650303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::set_Other(System.Object)
extern "C"  void DecoderResult_set_Other_m2481915664 (DecoderResult_t3752650303 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String)
extern "C"  void DecoderResult__ctor_m2281176498 (DecoderResult_t3752650303 * __this, ByteU5BU5D_t4260760469* ___rawBytes0, String_t* ___text1, Il2CppObject* ___byteSegments2, String_t* ___ecLevel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String,System.Int32,System.Int32)
extern "C"  void DecoderResult__ctor_m3444914322 (DecoderResult_t3752650303 * __this, ByteU5BU5D_t4260760469* ___rawBytes0, String_t* ___text1, Il2CppObject* ___byteSegments2, String_t* ___ecLevel3, int32_t ___saSequence4, int32_t ___saParity5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
