﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Button
struct Button_t3896396478;
// IncrementalSliderController
struct IncrementalSliderController_t153649975;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DevGUIChromaPanel
struct  DevGUIChromaPanel_t1432993864  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Button DevGUIChromaPanel::_smoothBtn
	Button_t3896396478 * ____smoothBtn_6;
	// UnityEngine.UI.Button DevGUIChromaPanel::_smoothSensBtn
	Button_t3896396478 * ____smoothSensBtn_7;
	// UnityEngine.UI.Button DevGUIChromaPanel::_recorteBtn
	Button_t3896396478 * ____recorteBtn_8;
	// UnityEngine.UI.Button DevGUIChromaPanel::_recorteSensBtn
	Button_t3896396478 * ____recorteSensBtn_9;
	// System.String DevGUIChromaPanel::_shaderChoosed
	String_t* ____shaderChoosed_10;
	// System.Single DevGUIChromaPanel::_recorteSens
	float ____recorteSens_11;
	// System.Single DevGUIChromaPanel::_recorte
	float ____recorte_12;
	// System.Single DevGUIChromaPanel::_smooth
	float ____smooth_13;
	// System.Single DevGUIChromaPanel::_smoothSens
	float ____smoothSens_14;
	// UnityEngine.Color DevGUIChromaPanel::_selectedColor
	Color_t4194546905  ____selectedColor_15;
	// UnityEngine.Color DevGUIChromaPanel::_defaultColor
	Color_t4194546905  ____defaultColor_16;
	// IncrementalSliderController DevGUIChromaPanel::_slider
	IncrementalSliderController_t153649975 * ____slider_17;
	// System.String DevGUIChromaPanel::_currentProperty
	String_t* ____currentProperty_18;
	// System.String DevGUIChromaPanel::_bundleID
	String_t* ____bundleID_19;

public:
	inline static int32_t get_offset_of__smoothBtn_6() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____smoothBtn_6)); }
	inline Button_t3896396478 * get__smoothBtn_6() const { return ____smoothBtn_6; }
	inline Button_t3896396478 ** get_address_of__smoothBtn_6() { return &____smoothBtn_6; }
	inline void set__smoothBtn_6(Button_t3896396478 * value)
	{
		____smoothBtn_6 = value;
		Il2CppCodeGenWriteBarrier(&____smoothBtn_6, value);
	}

	inline static int32_t get_offset_of__smoothSensBtn_7() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____smoothSensBtn_7)); }
	inline Button_t3896396478 * get__smoothSensBtn_7() const { return ____smoothSensBtn_7; }
	inline Button_t3896396478 ** get_address_of__smoothSensBtn_7() { return &____smoothSensBtn_7; }
	inline void set__smoothSensBtn_7(Button_t3896396478 * value)
	{
		____smoothSensBtn_7 = value;
		Il2CppCodeGenWriteBarrier(&____smoothSensBtn_7, value);
	}

	inline static int32_t get_offset_of__recorteBtn_8() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____recorteBtn_8)); }
	inline Button_t3896396478 * get__recorteBtn_8() const { return ____recorteBtn_8; }
	inline Button_t3896396478 ** get_address_of__recorteBtn_8() { return &____recorteBtn_8; }
	inline void set__recorteBtn_8(Button_t3896396478 * value)
	{
		____recorteBtn_8 = value;
		Il2CppCodeGenWriteBarrier(&____recorteBtn_8, value);
	}

	inline static int32_t get_offset_of__recorteSensBtn_9() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____recorteSensBtn_9)); }
	inline Button_t3896396478 * get__recorteSensBtn_9() const { return ____recorteSensBtn_9; }
	inline Button_t3896396478 ** get_address_of__recorteSensBtn_9() { return &____recorteSensBtn_9; }
	inline void set__recorteSensBtn_9(Button_t3896396478 * value)
	{
		____recorteSensBtn_9 = value;
		Il2CppCodeGenWriteBarrier(&____recorteSensBtn_9, value);
	}

	inline static int32_t get_offset_of__shaderChoosed_10() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____shaderChoosed_10)); }
	inline String_t* get__shaderChoosed_10() const { return ____shaderChoosed_10; }
	inline String_t** get_address_of__shaderChoosed_10() { return &____shaderChoosed_10; }
	inline void set__shaderChoosed_10(String_t* value)
	{
		____shaderChoosed_10 = value;
		Il2CppCodeGenWriteBarrier(&____shaderChoosed_10, value);
	}

	inline static int32_t get_offset_of__recorteSens_11() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____recorteSens_11)); }
	inline float get__recorteSens_11() const { return ____recorteSens_11; }
	inline float* get_address_of__recorteSens_11() { return &____recorteSens_11; }
	inline void set__recorteSens_11(float value)
	{
		____recorteSens_11 = value;
	}

	inline static int32_t get_offset_of__recorte_12() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____recorte_12)); }
	inline float get__recorte_12() const { return ____recorte_12; }
	inline float* get_address_of__recorte_12() { return &____recorte_12; }
	inline void set__recorte_12(float value)
	{
		____recorte_12 = value;
	}

	inline static int32_t get_offset_of__smooth_13() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____smooth_13)); }
	inline float get__smooth_13() const { return ____smooth_13; }
	inline float* get_address_of__smooth_13() { return &____smooth_13; }
	inline void set__smooth_13(float value)
	{
		____smooth_13 = value;
	}

	inline static int32_t get_offset_of__smoothSens_14() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____smoothSens_14)); }
	inline float get__smoothSens_14() const { return ____smoothSens_14; }
	inline float* get_address_of__smoothSens_14() { return &____smoothSens_14; }
	inline void set__smoothSens_14(float value)
	{
		____smoothSens_14 = value;
	}

	inline static int32_t get_offset_of__selectedColor_15() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____selectedColor_15)); }
	inline Color_t4194546905  get__selectedColor_15() const { return ____selectedColor_15; }
	inline Color_t4194546905 * get_address_of__selectedColor_15() { return &____selectedColor_15; }
	inline void set__selectedColor_15(Color_t4194546905  value)
	{
		____selectedColor_15 = value;
	}

	inline static int32_t get_offset_of__defaultColor_16() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____defaultColor_16)); }
	inline Color_t4194546905  get__defaultColor_16() const { return ____defaultColor_16; }
	inline Color_t4194546905 * get_address_of__defaultColor_16() { return &____defaultColor_16; }
	inline void set__defaultColor_16(Color_t4194546905  value)
	{
		____defaultColor_16 = value;
	}

	inline static int32_t get_offset_of__slider_17() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____slider_17)); }
	inline IncrementalSliderController_t153649975 * get__slider_17() const { return ____slider_17; }
	inline IncrementalSliderController_t153649975 ** get_address_of__slider_17() { return &____slider_17; }
	inline void set__slider_17(IncrementalSliderController_t153649975 * value)
	{
		____slider_17 = value;
		Il2CppCodeGenWriteBarrier(&____slider_17, value);
	}

	inline static int32_t get_offset_of__currentProperty_18() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____currentProperty_18)); }
	inline String_t* get__currentProperty_18() const { return ____currentProperty_18; }
	inline String_t** get_address_of__currentProperty_18() { return &____currentProperty_18; }
	inline void set__currentProperty_18(String_t* value)
	{
		____currentProperty_18 = value;
		Il2CppCodeGenWriteBarrier(&____currentProperty_18, value);
	}

	inline static int32_t get_offset_of__bundleID_19() { return static_cast<int32_t>(offsetof(DevGUIChromaPanel_t1432993864, ____bundleID_19)); }
	inline String_t* get__bundleID_19() const { return ____bundleID_19; }
	inline String_t** get_address_of__bundleID_19() { return &____bundleID_19; }
	inline void set__bundleID_19(String_t* value)
	{
		____bundleID_19 = value;
		Il2CppCodeGenWriteBarrier(&____bundleID_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
