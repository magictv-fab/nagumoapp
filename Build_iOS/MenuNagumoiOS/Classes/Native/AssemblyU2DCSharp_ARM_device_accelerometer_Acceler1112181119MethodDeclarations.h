﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.accelerometer.AccelerometerEventToggleOnOff
struct AccelerometerEventToggleOnOff_t1112181119;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.device.accelerometer.AccelerometerEventToggleOnOff::.ctor()
extern "C"  void AccelerometerEventToggleOnOff__ctor_m2247419567 (AccelerometerEventToggleOnOff_t1112181119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.accelerometer.AccelerometerEventToggleOnOff::Start()
extern "C"  void AccelerometerEventToggleOnOff_Start_m1194557359 (AccelerometerEventToggleOnOff_t1112181119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.accelerometer.AccelerometerEventToggleOnOff::_onMoveInit()
extern "C"  void AccelerometerEventToggleOnOff__onMoveInit_m678642764 (AccelerometerEventToggleOnOff_t1112181119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.accelerometer.AccelerometerEventToggleOnOff::_onMoveEnd()
extern "C"  void AccelerometerEventToggleOnOff__onMoveEnd_m295285121 (AccelerometerEventToggleOnOff_t1112181119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
