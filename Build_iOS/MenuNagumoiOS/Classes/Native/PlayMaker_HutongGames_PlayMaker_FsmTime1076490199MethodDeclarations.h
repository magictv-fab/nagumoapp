﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Single HutongGames.PlayMaker.FsmTime::get_RealtimeSinceStartup()
extern "C"  float FsmTime_get_RealtimeSinceStartup_m372847495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTime::RealtimeBugFix()
extern "C"  void FsmTime_RealtimeBugFix_m1889121768 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTime::Update()
extern "C"  void FsmTime_Update_m3325222949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmTime::FormatTime(System.Single)
extern "C"  String_t* FsmTime_FormatTime_m116092110 (Il2CppObject * __this /* static, unused */, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTime::DebugLog()
extern "C"  void FsmTime_DebugLog_m2817938061 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
