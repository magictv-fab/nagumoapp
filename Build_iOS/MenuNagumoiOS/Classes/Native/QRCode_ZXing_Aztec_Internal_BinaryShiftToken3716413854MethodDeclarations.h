﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.BinaryShiftToken
struct BinaryShiftToken_t3716413854;
// ZXing.Aztec.Internal.Token
struct Token_t3066207355;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.Aztec.Internal.BinaryShiftToken::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32)
extern "C"  void BinaryShiftToken__ctor_m2178628890 (BinaryShiftToken_t3716413854 * __this, Token_t3066207355 * ___previous0, int32_t ___binaryShiftStart1, int32_t ___binaryShiftByteCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.BinaryShiftToken::appendTo(ZXing.Common.BitArray,System.Byte[])
extern "C"  void BinaryShiftToken_appendTo_m1147666648 (BinaryShiftToken_t3716413854 * __this, BitArray_t4163851164 * ___bitArray0, ByteU5BU5D_t4260760469* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Aztec.Internal.BinaryShiftToken::ToString()
extern "C"  String_t* BinaryShiftToken_ToString_m3630772652 (BinaryShiftToken_t3716413854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
