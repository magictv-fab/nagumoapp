﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler
struct OnErrorEventHandler_t1722022561;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnErrorEventHandler__ctor_m616276680 (OnErrorEventHandler_t1722022561 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler::Invoke(System.String)
extern "C"  void OnErrorEventHandler_Invoke_m4154772768 (OnErrorEventHandler_t1722022561 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnErrorEventHandler_BeginInvoke_m1628435877 (OnErrorEventHandler_t1722022561 * __this, String_t* ___message0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnErrorEventHandler_EndInvoke_m2912925912 (OnErrorEventHandler_t1722022561 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
