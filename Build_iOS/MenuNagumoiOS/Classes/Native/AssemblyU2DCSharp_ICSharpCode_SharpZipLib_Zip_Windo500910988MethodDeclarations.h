﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.WindowsNameTransform
struct WindowsNameTransform_t500910988;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::.ctor(System.String)
extern "C"  void WindowsNameTransform__ctor_m494467011 (WindowsNameTransform_t500910988 * __this, String_t* ___baseDirectory0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::.ctor()
extern "C"  void WindowsNameTransform__ctor_m2273477023 (WindowsNameTransform_t500910988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::.cctor()
extern "C"  void WindowsNameTransform__cctor_m1276214766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::get_BaseDirectory()
extern "C"  String_t* WindowsNameTransform_get_BaseDirectory_m3355143469 (WindowsNameTransform_t500910988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::set_BaseDirectory(System.String)
extern "C"  void WindowsNameTransform_set_BaseDirectory_m2728706566 (WindowsNameTransform_t500910988 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::get_TrimIncomingPaths()
extern "C"  bool WindowsNameTransform_get_TrimIncomingPaths_m976687622 (WindowsNameTransform_t500910988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::set_TrimIncomingPaths(System.Boolean)
extern "C"  void WindowsNameTransform_set_TrimIncomingPaths_m1213876285 (WindowsNameTransform_t500910988 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::TransformDirectory(System.String)
extern "C"  String_t* WindowsNameTransform_TransformDirectory_m103023833 (WindowsNameTransform_t500910988 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::TransformFile(System.String)
extern "C"  String_t* WindowsNameTransform_TransformFile_m1237928000 (WindowsNameTransform_t500910988 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::IsValidName(System.String)
extern "C"  bool WindowsNameTransform_IsValidName_m1108459740 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::MakeValidName(System.String,System.Char)
extern "C"  String_t* WindowsNameTransform_MakeValidName_m3672974566 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Il2CppChar ___replacement1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::get_Replacement()
extern "C"  Il2CppChar WindowsNameTransform_get_Replacement_m3253158120 (WindowsNameTransform_t500910988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::set_Replacement(System.Char)
extern "C"  void WindowsNameTransform_set_Replacement_m2094834539 (WindowsNameTransform_t500910988 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
