﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ObjectCompare
struct  ObjectCompare_t355571316  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.ObjectCompare::objectVariable
	FsmObject_t821476169 * ___objectVariable_9;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.ObjectCompare::compareTo
	FsmObject_t821476169 * ___compareTo_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ObjectCompare::equalEvent
	FsmEvent_t2133468028 * ___equalEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ObjectCompare::notEqualEvent
	FsmEvent_t2133468028 * ___notEqualEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ObjectCompare::storeResult
	FsmBool_t1075959796 * ___storeResult_13;
	// System.Boolean HutongGames.PlayMaker.Actions.ObjectCompare::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_objectVariable_9() { return static_cast<int32_t>(offsetof(ObjectCompare_t355571316, ___objectVariable_9)); }
	inline FsmObject_t821476169 * get_objectVariable_9() const { return ___objectVariable_9; }
	inline FsmObject_t821476169 ** get_address_of_objectVariable_9() { return &___objectVariable_9; }
	inline void set_objectVariable_9(FsmObject_t821476169 * value)
	{
		___objectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___objectVariable_9, value);
	}

	inline static int32_t get_offset_of_compareTo_10() { return static_cast<int32_t>(offsetof(ObjectCompare_t355571316, ___compareTo_10)); }
	inline FsmObject_t821476169 * get_compareTo_10() const { return ___compareTo_10; }
	inline FsmObject_t821476169 ** get_address_of_compareTo_10() { return &___compareTo_10; }
	inline void set_compareTo_10(FsmObject_t821476169 * value)
	{
		___compareTo_10 = value;
		Il2CppCodeGenWriteBarrier(&___compareTo_10, value);
	}

	inline static int32_t get_offset_of_equalEvent_11() { return static_cast<int32_t>(offsetof(ObjectCompare_t355571316, ___equalEvent_11)); }
	inline FsmEvent_t2133468028 * get_equalEvent_11() const { return ___equalEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_equalEvent_11() { return &___equalEvent_11; }
	inline void set_equalEvent_11(FsmEvent_t2133468028 * value)
	{
		___equalEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___equalEvent_11, value);
	}

	inline static int32_t get_offset_of_notEqualEvent_12() { return static_cast<int32_t>(offsetof(ObjectCompare_t355571316, ___notEqualEvent_12)); }
	inline FsmEvent_t2133468028 * get_notEqualEvent_12() const { return ___notEqualEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_notEqualEvent_12() { return &___notEqualEvent_12; }
	inline void set_notEqualEvent_12(FsmEvent_t2133468028 * value)
	{
		___notEqualEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___notEqualEvent_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(ObjectCompare_t355571316, ___storeResult_13)); }
	inline FsmBool_t1075959796 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmBool_t1075959796 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(ObjectCompare_t355571316, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
