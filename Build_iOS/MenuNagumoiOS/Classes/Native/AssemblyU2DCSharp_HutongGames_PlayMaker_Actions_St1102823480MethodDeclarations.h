﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringContains
struct StringContains_t1102823480;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringContains::.ctor()
extern "C"  void StringContains__ctor_m4246734638 (StringContains_t1102823480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::Reset()
extern "C"  void StringContains_Reset_m1893167579 (StringContains_t1102823480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::OnEnter()
extern "C"  void StringContains_OnEnter_m284784517 (StringContains_t1102823480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::OnUpdate()
extern "C"  void StringContains_OnUpdate_m3666912094 (StringContains_t1102823480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::DoStringContains()
extern "C"  void StringContains_DoStringContains_m804327633 (StringContains_t1102823480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
