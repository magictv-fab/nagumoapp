﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PastePositionAngleFilter
struct PastePositionAngleFilter_t1444485071;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void PastePositionAngleFilter::.ctor()
extern "C"  void PastePositionAngleFilter__ctor_m399822764 (PastePositionAngleFilter_t1444485071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PastePositionAngleFilter::Update()
extern "C"  void PastePositionAngleFilter_Update_m1236465889 (PastePositionAngleFilter_t1444485071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PastePositionAngleFilter::LateUpdate()
extern "C"  void PastePositionAngleFilter_LateUpdate_m2250991143 (PastePositionAngleFilter_t1444485071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PastePositionAngleFilter::doFollow()
extern "C"  void PastePositionAngleFilter_doFollow_m267567508 (PastePositionAngleFilter_t1444485071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion PastePositionAngleFilter::doCopyRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  PastePositionAngleFilter_doCopyRotation_m1055537225 (PastePositionAngleFilter_t1444485071 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PastePositionAngleFilter::doCopyPosition(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  PastePositionAngleFilter_doCopyPosition_m4021500446 (PastePositionAngleFilter_t1444485071 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
