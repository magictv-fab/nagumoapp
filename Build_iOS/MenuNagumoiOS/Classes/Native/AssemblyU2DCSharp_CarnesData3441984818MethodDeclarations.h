﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CarnesData
struct CarnesData_t3441984818;

#include "codegen/il2cpp-codegen.h"

// System.Void CarnesData::.ctor()
extern "C"  void CarnesData__ctor_m4190161833 (CarnesData_t3441984818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
