﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.ErrorResultVO
struct ErrorResultVO_t356660816;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.ErrorResultVO::.ctor()
extern "C"  void ErrorResultVO__ctor_m1824451891 (ErrorResultVO_t356660816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
