﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Raycast
struct Raycast_t2801925751;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Raycast::.ctor()
extern "C"  void Raycast__ctor_m1353774751 (Raycast_t2801925751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Raycast::Reset()
extern "C"  void Raycast_Reset_m3295174988 (Raycast_t2801925751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Raycast::OnEnter()
extern "C"  void Raycast_OnEnter_m3289140918 (Raycast_t2801925751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Raycast::OnUpdate()
extern "C"  void Raycast_OnUpdate_m2312680013 (Raycast_t2801925751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Raycast::DoRaycast()
extern "C"  void Raycast_DoRaycast_m3321673115 (Raycast_t2801925751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
