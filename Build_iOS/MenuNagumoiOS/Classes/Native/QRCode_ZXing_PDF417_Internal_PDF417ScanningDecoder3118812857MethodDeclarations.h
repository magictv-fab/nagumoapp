﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// ZXing.PDF417.Internal.DetectionResult
struct DetectionResult_t1261579248;
// ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn
struct DetectionResultRowIndicatorColumn_t1625390043;
// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.PDF417.Internal.BarcodeMetadata
struct BarcodeMetadata_t443526333;
// ZXing.PDF417.Internal.BarcodeValue[][]
struct BarcodeValueU5BU5DU5BU5D_t3694669629;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// ZXing.PDF417.Internal.Codeword
struct Codeword_t1124156527;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"
#include "QRCode_ZXing_PDF417_Internal_DetectionResultRowInd1625390043.h"
#include "QRCode_ZXing_PDF417_Internal_BoundingBox242606069.h"
#include "QRCode_ZXing_PDF417_Internal_DetectionResult1261579248.h"

// ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::decode(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern "C"  DecoderResult_t3752650303 * PDF417ScanningDecoder_decode_m2396193311 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, ResultPoint_t1538592853 * ___imageTopLeft1, ResultPoint_t1538592853 * ___imageBottomLeft2, ResultPoint_t1538592853 * ___imageTopRight3, ResultPoint_t1538592853 * ___imageBottomRight4, int32_t ___minCodewordWidth5, int32_t ___maxCodewordWidth6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.DetectionResult ZXing.PDF417.Internal.PDF417ScanningDecoder::merge(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn,ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern "C"  DetectionResult_t1261579248 * PDF417ScanningDecoder_merge_m770094845 (Il2CppObject * __this /* static, unused */, DetectionResultRowIndicatorColumn_t1625390043 * ___leftRowIndicatorColumn0, DetectionResultRowIndicatorColumn_t1625390043 * ___rightRowIndicatorColumn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustBoundingBox(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern "C"  BoundingBox_t242606069 * PDF417ScanningDecoder_adjustBoundingBox_m1165479086 (Il2CppObject * __this /* static, unused */, DetectionResultRowIndicatorColumn_t1625390043 * ___rowIndicatorColumn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getMax(System.Int32[])
extern "C"  int32_t PDF417ScanningDecoder_getMax_m1456836606 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.PDF417ScanningDecoder::getBarcodeMetadata(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn,ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern "C"  BarcodeMetadata_t443526333 * PDF417ScanningDecoder_getBarcodeMetadata_m2179561919 (Il2CppObject * __this /* static, unused */, DetectionResultRowIndicatorColumn_t1625390043 * ___leftRowIndicatorColumn0, DetectionResultRowIndicatorColumn_t1625390043 * ___rightRowIndicatorColumn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn ZXing.PDF417.Internal.PDF417ScanningDecoder::getRowIndicatorColumn(ZXing.Common.BitMatrix,ZXing.PDF417.Internal.BoundingBox,ZXing.ResultPoint,System.Boolean,System.Int32,System.Int32)
extern "C"  DetectionResultRowIndicatorColumn_t1625390043 * PDF417ScanningDecoder_getRowIndicatorColumn_m3715056454 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, BoundingBox_t242606069 * ___boundingBox1, ResultPoint_t1538592853 * ___startPoint2, bool ___leftToRight3, int32_t ___minCodewordWidth4, int32_t ___maxCodewordWidth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustCodewordCount(ZXing.PDF417.Internal.DetectionResult,ZXing.PDF417.Internal.BarcodeValue[][])
extern "C"  bool PDF417ScanningDecoder_adjustCodewordCount_m3236585863 (Il2CppObject * __this /* static, unused */, DetectionResult_t1261579248 * ___detectionResult0, BarcodeValueU5BU5DU5BU5D_t3694669629* ___barcodeMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::createDecoderResult(ZXing.PDF417.Internal.DetectionResult)
extern "C"  DecoderResult_t3752650303 * PDF417ScanningDecoder_createDecoderResult_m4230578520 (Il2CppObject * __this /* static, unused */, DetectionResult_t1261579248 * ___detectionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::createDecoderResultFromAmbiguousValues(System.Int32,System.Int32[],System.Int32[],System.Int32[],System.Int32[][])
extern "C"  DecoderResult_t3752650303 * PDF417ScanningDecoder_createDecoderResultFromAmbiguousValues_m2796005224 (Il2CppObject * __this /* static, unused */, int32_t ___ecLevel0, Int32U5BU5D_t3230847821* ___codewords1, Int32U5BU5D_t3230847821* ___erasureArray2, Int32U5BU5D_t3230847821* ___ambiguousIndexes3, Int32U5BU5DU5BU5D_t1820556512* ___ambiguousIndexValues4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BarcodeValue[][] ZXing.PDF417.Internal.PDF417ScanningDecoder::createBarcodeMatrix(ZXing.PDF417.Internal.DetectionResult)
extern "C"  BarcodeValueU5BU5DU5BU5D_t3694669629* PDF417ScanningDecoder_createBarcodeMatrix_m940478650 (Il2CppObject * __this /* static, unused */, DetectionResult_t1261579248 * ___detectionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::isValidBarcodeColumn(ZXing.PDF417.Internal.DetectionResult,System.Int32)
extern "C"  bool PDF417ScanningDecoder_isValidBarcodeColumn_m3715491505 (Il2CppObject * __this /* static, unused */, DetectionResult_t1261579248 * ___detectionResult0, int32_t ___barcodeColumn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getStartColumn(ZXing.PDF417.Internal.DetectionResult,System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t PDF417ScanningDecoder_getStartColumn_m1793021403 (Il2CppObject * __this /* static, unused */, DetectionResult_t1261579248 * ___detectionResult0, int32_t ___barcodeColumn1, int32_t ___imageRow2, bool ___leftToRight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.PDF417ScanningDecoder::detectCodeword(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Codeword_t1124156527 * PDF417ScanningDecoder_detectCodeword_m1632048602 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, int32_t ___minColumn1, int32_t ___maxColumn2, bool ___leftToRight3, int32_t ___startColumn4, int32_t ___imageRow5, int32_t ___minCodewordWidth6, int32_t ___maxCodewordWidth7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.PDF417ScanningDecoder::getModuleBitCount(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t3230847821* PDF417ScanningDecoder_getModuleBitCount_m4114861711 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, int32_t ___minColumn1, int32_t ___maxColumn2, bool ___leftToRight3, int32_t ___startColumn4, int32_t ___imageRow5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getNumberOfECCodeWords(System.Int32)
extern "C"  int32_t PDF417ScanningDecoder_getNumberOfECCodeWords_m1530748090 (Il2CppObject * __this /* static, unused */, int32_t ___barcodeECLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustCodewordStartColumn(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32)
extern "C"  int32_t PDF417ScanningDecoder_adjustCodewordStartColumn_m2453438367 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, int32_t ___minColumn1, int32_t ___maxColumn2, bool ___leftToRight3, int32_t ___codewordStartColumn4, int32_t ___imageRow5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::checkCodewordSkew(System.Int32,System.Int32,System.Int32)
extern "C"  bool PDF417ScanningDecoder_checkCodewordSkew_m3704418753 (Il2CppObject * __this /* static, unused */, int32_t ___codewordSize0, int32_t ___minCodewordWidth1, int32_t ___maxCodewordWidth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::decodeCodewords(System.Int32[],System.Int32,System.Int32[])
extern "C"  DecoderResult_t3752650303 * PDF417ScanningDecoder_decodeCodewords_m360040963 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, int32_t ___ecLevel1, Int32U5BU5D_t3230847821* ___erasures2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::correctErrors(System.Int32[],System.Int32[],System.Int32)
extern "C"  int32_t PDF417ScanningDecoder_correctErrors_m3711155911 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, Int32U5BU5D_t3230847821* ___erasures1, int32_t ___numECCodewords2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::verifyCodewordCount(System.Int32[],System.Int32)
extern "C"  bool PDF417ScanningDecoder_verifyCodewordCount_m3372637794 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, int32_t ___numECCodewords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.PDF417ScanningDecoder::getBitCountForCodeword(System.Int32)
extern "C"  Int32U5BU5D_t3230847821* PDF417ScanningDecoder_getBitCountForCodeword_m2294590300 (Il2CppObject * __this /* static, unused */, int32_t ___codeword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getCodewordBucketNumber(System.Int32)
extern "C"  int32_t PDF417ScanningDecoder_getCodewordBucketNumber_m3826358482 (Il2CppObject * __this /* static, unused */, int32_t ___codeword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getCodewordBucketNumber(System.Int32[])
extern "C"  int32_t PDF417ScanningDecoder_getCodewordBucketNumber_m638546800 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417ScanningDecoder::.cctor()
extern "C"  void PDF417ScanningDecoder__cctor_m1056766264 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
