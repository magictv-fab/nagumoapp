﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t1141403250;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2348681196.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Byte[])
extern "C"  void InflaterHuffmanTree__ctor_m4154168024 (InflaterHuffmanTree_t1141403250 * __this, ByteU5BU5D_t4260760469* ___codeLengths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern "C"  void InflaterHuffmanTree__cctor_m2568371516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Byte[])
extern "C"  void InflaterHuffmanTree_BuildTree_m4125561038 (InflaterHuffmanTree_t1141403250 * __this, ByteU5BU5D_t4260760469* ___codeLengths0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern "C"  int32_t InflaterHuffmanTree_GetSymbol_m1147139770 (InflaterHuffmanTree_t1141403250 * __this, StreamManipulator_t2348681196 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
