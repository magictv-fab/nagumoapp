﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Maxicode.MaxiCodeReader
struct MaxiCodeReader_t3092722715;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Result ZXing.Maxicode.MaxiCodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * MaxiCodeReader_decode_m1658505743 (MaxiCodeReader_t3092722715 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Maxicode.MaxiCodeReader::reset()
extern "C"  void MaxiCodeReader_reset_m3702472337 (MaxiCodeReader_t3092722715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Maxicode.MaxiCodeReader::extractPureBits(ZXing.Common.BitMatrix)
extern "C"  BitMatrix_t1058711404 * MaxiCodeReader_extractPureBits_m2113210068 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Maxicode.MaxiCodeReader::.ctor()
extern "C"  void MaxiCodeReader__ctor_m3425725380 (MaxiCodeReader_t3092722715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Maxicode.MaxiCodeReader::.cctor()
extern "C"  void MaxiCodeReader__cctor_m2636175465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
