﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t3927159007;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.FsmProperty::.ctor()
extern "C"  void FsmProperty__ctor_m1857631456 (FsmProperty_t3927159007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::.ctor(HutongGames.PlayMaker.FsmProperty)
extern "C"  void FsmProperty__ctor_m263693385 (FsmProperty_t3927159007 * __this, FsmProperty_t3927159007 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::SetPropertyName(System.String)
extern "C"  void FsmProperty_SetPropertyName_m3545040450 (FsmProperty_t3927159007 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::SetValue()
extern "C"  void FsmProperty_SetValue_m909800787 (FsmProperty_t3927159007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::GetValue()
extern "C"  void FsmProperty_GetValue_m3267962847 (FsmProperty_t3927159007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::Init()
extern "C"  void FsmProperty_Init_m4029332852 (FsmProperty_t3927159007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::CheckForReinitialize()
extern "C"  void FsmProperty_CheckForReinitialize_m1522370728 (FsmProperty_t3927159007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::ResetParameters()
extern "C"  void FsmProperty_ResetParameters_m1162093591 (FsmProperty_t3927159007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
