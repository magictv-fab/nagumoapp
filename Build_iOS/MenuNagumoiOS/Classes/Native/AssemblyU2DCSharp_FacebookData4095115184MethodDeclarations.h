﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookData
struct FacebookData_t4095115184;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookData::.ctor()
extern "C"  void FacebookData__ctor_m772675819 (FacebookData_t4095115184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
