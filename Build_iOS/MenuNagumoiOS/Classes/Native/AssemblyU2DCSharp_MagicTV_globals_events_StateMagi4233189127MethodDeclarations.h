﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler
struct OnChangeToStateEventHandler_t4233189127;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnChangeToStateEventHandler__ctor_m929572910 (OnChangeToStateEventHandler_t4233189127 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler::Invoke(MagicTV.globals.StateMachine)
extern "C"  void OnChangeToStateEventHandler_Invoke_m2677840277 (OnChangeToStateEventHandler_t4233189127 * __this, int32_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler::BeginInvoke(MagicTV.globals.StateMachine,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnChangeToStateEventHandler_BeginInvoke_m776096278 (OnChangeToStateEventHandler_t4233189127 * __this, int32_t ___p0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnChangeToStateEventHandler_EndInvoke_m2961367358 (OnChangeToStateEventHandler_t4233189127 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
