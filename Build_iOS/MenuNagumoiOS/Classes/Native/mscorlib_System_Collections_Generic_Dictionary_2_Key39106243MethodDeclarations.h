﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>
struct Dictionary_2_t3719137485;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key39106243.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3136035504_gshared (Enumerator_t39106243 * __this, Dictionary_2_t3719137485 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3136035504(__this, ___host0, method) ((  void (*) (Enumerator_t39106243 *, Dictionary_2_t3719137485 *, const MethodInfo*))Enumerator__ctor_m3136035504_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.EncodeHintType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3383851899_gshared (Enumerator_t39106243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3383851899(__this, method) ((  Il2CppObject * (*) (Enumerator_t39106243 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3383851899_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.EncodeHintType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1820600325_gshared (Enumerator_t39106243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1820600325(__this, method) ((  void (*) (Enumerator_t39106243 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1820600325_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.EncodeHintType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2136796498_gshared (Enumerator_t39106243 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2136796498(__this, method) ((  void (*) (Enumerator_t39106243 *, const MethodInfo*))Enumerator_Dispose_m2136796498_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.EncodeHintType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2347260341_gshared (Enumerator_t39106243 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2347260341(__this, method) ((  bool (*) (Enumerator_t39106243 *, const MethodInfo*))Enumerator_MoveNext_m2347260341_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.EncodeHintType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1897110339_gshared (Enumerator_t39106243 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1897110339(__this, method) ((  int32_t (*) (Enumerator_t39106243 *, const MethodInfo*))Enumerator_get_Current_m1897110339_gshared)(__this, method)
