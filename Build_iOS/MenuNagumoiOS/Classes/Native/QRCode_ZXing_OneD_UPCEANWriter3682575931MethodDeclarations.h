﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCEANWriter
struct UPCEANWriter_t3682575931;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.OneD.UPCEANWriter::get_DefaultMargin()
extern "C"  int32_t UPCEANWriter_get_DefaultMargin_m528456410 (UPCEANWriter_t3682575931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANWriter::.ctor()
extern "C"  void UPCEANWriter__ctor_m1484832328 (UPCEANWriter_t3682575931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
