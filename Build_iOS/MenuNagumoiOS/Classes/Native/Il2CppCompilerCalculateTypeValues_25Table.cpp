﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData3958426178.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog1596141350.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry2614866584.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType537852544.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust2484246752.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust4157095950.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility81143630.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility_BitConv1958851146.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeNicifie195153339.h"
#include "PlayMaker_HutongGames_PlayMaker_Actions_MissingActi834633994.h"
#include "PlayMaker_HutongGames_PlayMaker_ReflectionUtils1202855664.h"
#include "PlayMaker_PlayMakerProxyBase3469687535.h"
#include "PlayMaker_PlayMakerApplicationEvents1615127321.h"
#include "PlayMaker_PlayMakerCollisionEnter4046453814.h"
#include "PlayMaker_PlayMakerCollisionExit3871318016.h"
#include "PlayMaker_PlayMakerCollisionStay3871731003.h"
#include "PlayMaker_PlayMakerFixedUpdate997170669.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_PlayMakerGlobals3097244096.h"
#include "PlayMaker_PlayMakerGUI3799848395.h"
#include "PlayMaker_PlayMakerMouseEvents316289710.h"
#include "PlayMaker_PlayMakerOnGUI940239724.h"
#include "PlayMaker_PlayMakerTriggerEnter977448304.h"
#include "PlayMaker_PlayMakerTriggerExit3495223174.h"
#include "PlayMaker_PlayMakerTriggerStay3495636161.h"
#include "QRCode_U3CModuleU3E86524790.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_BarcodeReader240638149.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"
#include "QRCode_ZXing_BaseLuminanceSource803260164.h"
#include "QRCode_ZXing_Binarizer1492033400.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "QRCode_ZXing_Dimension1395692488.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "QRCode_ZXing_ReaderException1784959150.h"
#include "QRCode_ZXing_FormatException3082259450.h"
#include "QRCode_ZXing_InvertedLuminanceSource1075320672.h"
#include "QRCode_ZXing_MultiFormatReader4081038293.h"
#include "QRCode_ZXing_MultiFormatWriter4236443525.h"
#include "QRCode_ZXing_Result2610723219.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"
#include "QRCode_ZXing_ResultPointCallback207829946.h"
#include "QRCode_ZXing_RGBLuminanceSource3258519836.h"
#include "QRCode_ZXing_RGBLuminanceSource_BitmapFormat3665922245.h"
#include "QRCode_ZXing_SupportClass1360542943.h"
#include "QRCode_ZXing_WriterException2468555518.h"
#include "QRCode_ZXing_Common_DetectorResult3846928147.h"
#include "QRCode_ZXing_Aztec_Internal_AztecDetectorResult1662194462.h"
#include "QRCode_ZXing_Aztec_AztecReader3105399986.h"
#include "QRCode_ZXing_Aztec_AztecResultMetadata1409764443.h"
#include "QRCode_ZXing_Aztec_AztecWriter3260805218.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder16782150.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "QRCode_ZXing_Aztec_Internal_Detector3858377196.h"
#include "QRCode_ZXing_Aztec_Internal_Detector_Point330760921.h"
#include "QRCode_ZXing_Aztec_Internal_AztecCode1281541704.h"
#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"
#include "QRCode_ZXing_Aztec_Internal_BinaryShiftToken3716413854.h"
#include "QRCode_ZXing_Aztec_Internal_Encoder1161948190.h"
#include "QRCode_ZXing_Aztec_Internal_HighLevelEncoder3204486112.h"
#include "QRCode_ZXing_Aztec_Internal_SimpleToken3418679145.h"
#include "QRCode_ZXing_Aztec_Internal_State3065423635.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_Common_BitSource1243445190.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (NamedVariable_t3211770239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[5] = 
{
	NamedVariable_t3211770239::get_offset_of_useVariable_0(),
	NamedVariable_t3211770239::get_offset_of_name_1(),
	NamedVariable_t3211770239::get_offset_of_tooltip_2(),
	NamedVariable_t3211770239::get_offset_of_showInInspector_3(),
	NamedVariable_t3211770239::get_offset_of_networkSync_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (FsmBool_t1075959796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[1] = 
{
	FsmBool_t1075959796::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (FsmColor_t2131419205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	FsmColor_t2131419205::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (FsmFloat_t2134102846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	FsmFloat_t2134102846::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (FsmGameObject_t1697147867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	FsmGameObject_t1697147867::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (FsmInt_t1596138449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	FsmInt_t1596138449::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (FsmObject_t821476169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[3] = 
{
	FsmObject_t821476169::get_offset_of_typeName_5(),
	FsmObject_t821476169::get_offset_of_value_6(),
	FsmObject_t821476169::get_offset_of_objectType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (FsmMaterial_t924399665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (FsmOwnerDefault_t251897112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[2] = 
{
	FsmOwnerDefault_t251897112::get_offset_of_ownerOption_0(),
	FsmOwnerDefault_t251897112::get_offset_of_gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (OwnerDefaultOption_t1934292325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[3] = 
{
	OwnerDefaultOption_t1934292325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (FsmQuaternion_t3871136040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	FsmQuaternion_t3871136040::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (FsmRect_t1076426478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	FsmRect_t1076426478::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (FsmString_t952858651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	FsmString_t952858651::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (FsmTexture_t3073272573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (FsmVar_t1596150537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[10] = 
{
	FsmVar_t1596150537::get_offset_of_namedVar_0(),
	FsmVar_t1596150537::get_offset_of_variableName_1(),
	FsmVar_t1596150537::get_offset_of_useVariable_2(),
	FsmVar_t1596150537::get_offset_of_type_3(),
	FsmVar_t1596150537::get_offset_of_floatValue_4(),
	FsmVar_t1596150537::get_offset_of_intValue_5(),
	FsmVar_t1596150537::get_offset_of_boolValue_6(),
	FsmVar_t1596150537::get_offset_of_stringValue_7(),
	FsmVar_t1596150537::get_offset_of_vector4Value_8(),
	FsmVar_t1596150537::get_offset_of_objectReference_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (FsmVector2_t533912881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[1] = 
{
	FsmVector2_t533912881::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (FsmVector3_t533912882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[1] = 
{
	FsmVector3_t533912882::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (ActionData_t3958426178), -1, sizeof(ActionData_t3958426178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2517[40] = 
{
	ActionData_t3958426178_StaticFields::get_offset_of_ActionTypeLookup_0(),
	ActionData_t3958426178_StaticFields::get_offset_of_ActionFieldsLookup_1(),
	ActionData_t3958426178_StaticFields::get_offset_of_ActionHashCodeLookup_2(),
	ActionData_t3958426178_StaticFields::get_offset_of_currentFsm_3(),
	ActionData_t3958426178_StaticFields::get_offset_of_currentState_4(),
	ActionData_t3958426178_StaticFields::get_offset_of_currentAction_5(),
	ActionData_t3958426178_StaticFields::get_offset_of_currentActionIndex_6(),
	ActionData_t3958426178_StaticFields::get_offset_of_currentParameter_7(),
	ActionData_t3958426178_StaticFields::get_offset_of_resaveActionData_8(),
	ActionData_t3958426178_StaticFields::get_offset_of_UsedIndices_9(),
	ActionData_t3958426178_StaticFields::get_offset_of_InitFields_10(),
	ActionData_t3958426178::get_offset_of_actionNames_11(),
	ActionData_t3958426178::get_offset_of_customNames_12(),
	ActionData_t3958426178::get_offset_of_actionEnabled_13(),
	ActionData_t3958426178::get_offset_of_actionIsOpen_14(),
	ActionData_t3958426178::get_offset_of_actionStartIndex_15(),
	ActionData_t3958426178::get_offset_of_actionHashCodes_16(),
	ActionData_t3958426178::get_offset_of_unityObjectParams_17(),
	ActionData_t3958426178::get_offset_of_fsmGameObjectParams_18(),
	ActionData_t3958426178::get_offset_of_fsmOwnerDefaultParams_19(),
	ActionData_t3958426178::get_offset_of_animationCurveParams_20(),
	ActionData_t3958426178::get_offset_of_functionCallParams_21(),
	ActionData_t3958426178::get_offset_of_fsmTemplateControlParams_22(),
	ActionData_t3958426178::get_offset_of_fsmEventTargetParams_23(),
	ActionData_t3958426178::get_offset_of_fsmPropertyParams_24(),
	ActionData_t3958426178::get_offset_of_layoutOptionParams_25(),
	ActionData_t3958426178::get_offset_of_fsmStringParams_26(),
	ActionData_t3958426178::get_offset_of_fsmObjectParams_27(),
	ActionData_t3958426178::get_offset_of_fsmVarParams_28(),
	ActionData_t3958426178::get_offset_of_byteData_29(),
	ActionData_t3958426178::get_offset_of_byteDataAsArray_30(),
	ActionData_t3958426178::get_offset_of_arrayParamSizes_31(),
	ActionData_t3958426178::get_offset_of_arrayParamTypes_32(),
	ActionData_t3958426178::get_offset_of_customTypeSizes_33(),
	ActionData_t3958426178::get_offset_of_customTypeNames_34(),
	ActionData_t3958426178::get_offset_of_paramDataType_35(),
	ActionData_t3958426178::get_offset_of_paramName_36(),
	ActionData_t3958426178::get_offset_of_paramDataPos_37(),
	ActionData_t3958426178::get_offset_of_paramByteDataSize_38(),
	ActionData_t3958426178::get_offset_of_nextParamIndex_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (ParamDataType_t2672665179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2518[42] = 
{
	ParamDataType_t2672665179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (Fsm_t1527112426), -1, sizeof(Fsm_t1527112426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2519[69] = 
{
	0,
	0,
	Fsm_t1527112426_StaticFields::get_offset_of_EventData_2(),
	Fsm_t1527112426_StaticFields::get_offset_of_debugLookAtColor_3(),
	Fsm_t1527112426_StaticFields::get_offset_of_debugRaycastColor_4(),
	Fsm_t1527112426::get_offset_of_owner_5(),
	Fsm_t1527112426::get_offset_of_usedInTemplate_6(),
	Fsm_t1527112426::get_offset_of_name_7(),
	Fsm_t1527112426::get_offset_of_startState_8(),
	Fsm_t1527112426::get_offset_of_states_9(),
	Fsm_t1527112426::get_offset_of_events_10(),
	Fsm_t1527112426::get_offset_of_globalTransitions_11(),
	Fsm_t1527112426::get_offset_of_variables_12(),
	Fsm_t1527112426::get_offset_of_description_13(),
	Fsm_t1527112426::get_offset_of_docUrl_14(),
	Fsm_t1527112426::get_offset_of_showStateLabel_15(),
	Fsm_t1527112426::get_offset_of_maxLoopCount_16(),
	Fsm_t1527112426::get_offset_of_watermark_17(),
	Fsm_t1527112426::get_offset_of_version_18(),
	Fsm_t1527112426::get_offset_of_host_19(),
	Fsm_t1527112426::get_offset_of_rootFsm_20(),
	Fsm_t1527112426::get_offset_of_subFsmList_21(),
	Fsm_t1527112426::get_offset_of_activeStateEntered_22(),
	Fsm_t1527112426::get_offset_of_ExposedEvents_23(),
	Fsm_t1527112426::get_offset_of_myLog_24(),
	Fsm_t1527112426::get_offset_of_RestartOnEnable_25(),
	Fsm_t1527112426::get_offset_of_EnableDebugFlow_26(),
	Fsm_t1527112426::get_offset_of_StepFrame_27(),
	Fsm_t1527112426::get_offset_of_delayedEvents_28(),
	Fsm_t1527112426::get_offset_of_updateEvents_29(),
	Fsm_t1527112426::get_offset_of_removeEvents_30(),
	Fsm_t1527112426::get_offset_of_initialized_31(),
	Fsm_t1527112426::get_offset_of_activeStateName_32(),
	Fsm_t1527112426::get_offset_of_activeState_33(),
	Fsm_t1527112426::get_offset_of_switchToState_34(),
	Fsm_t1527112426::get_offset_of_previousActiveState_35(),
	Fsm_t1527112426_StaticFields::get_offset_of_StateColors_36(),
	Fsm_t1527112426::get_offset_of_editState_37(),
	Fsm_t1527112426::get_offset_of_mouseEvents_38(),
	Fsm_t1527112426::get_offset_of_handleTriggerEnter_39(),
	Fsm_t1527112426::get_offset_of_handleTriggerExit_40(),
	Fsm_t1527112426::get_offset_of_handleTriggerStay_41(),
	Fsm_t1527112426::get_offset_of_handleCollisionEnter_42(),
	Fsm_t1527112426::get_offset_of_handleCollisionExit_43(),
	Fsm_t1527112426::get_offset_of_handleCollisionStay_44(),
	Fsm_t1527112426::get_offset_of_handleOnGUI_45(),
	Fsm_t1527112426::get_offset_of_handleFixedUpdate_46(),
	Fsm_t1527112426::get_offset_of_handleApplicationEvents_47(),
	Fsm_t1527112426_StaticFields::get_offset_of_targetSelf_48(),
	Fsm_t1527112426::get_offset_of_U3CStartedU3Ek__BackingField_49(),
	Fsm_t1527112426::get_offset_of_U3CEventTargetU3Ek__BackingField_50(),
	Fsm_t1527112426::get_offset_of_U3CFinishedU3Ek__BackingField_51(),
	Fsm_t1527112426::get_offset_of_U3CLastTransitionU3Ek__BackingField_52(),
	Fsm_t1527112426::get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_53(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CLastClickedObjectU3Ek__BackingField_54(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_55(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CHitBreakpointU3Ek__BackingField_56(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CBreakAtFsmU3Ek__BackingField_57(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CBreakAtStateU3Ek__BackingField_58(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CIsBreakU3Ek__BackingField_59(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CIsErrorBreakU3Ek__BackingField_60(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CLastErrorU3Ek__BackingField_61(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CStepToStateChangeU3Ek__BackingField_62(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CStepFsmU3Ek__BackingField_63(),
	Fsm_t1527112426::get_offset_of_U3CSwitchedStateU3Ek__BackingField_64(),
	Fsm_t1527112426::get_offset_of_U3CCollisionInfoU3Ek__BackingField_65(),
	Fsm_t1527112426::get_offset_of_U3CTriggerColliderU3Ek__BackingField_66(),
	Fsm_t1527112426::get_offset_of_U3CControllerColliderU3Ek__BackingField_67(),
	Fsm_t1527112426::get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_68(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (FsmEvent_t2133468028), -1, sizeof(FsmEvent_t2133468028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2520[34] = 
{
	FsmEvent_t2133468028_StaticFields::get_offset_of_eventList_0(),
	FsmEvent_t2133468028::get_offset_of_name_1(),
	FsmEvent_t2133468028::get_offset_of_isSystemEvent_2(),
	FsmEvent_t2133468028::get_offset_of_isGlobal_3(),
	FsmEvent_t2133468028::get_offset_of_U3CPathU3Ek__BackingField_4(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CBecameInvisibleU3Ek__BackingField_5(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CBecameVisibleU3Ek__BackingField_6(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionEnterU3Ek__BackingField_7(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionExitU3Ek__BackingField_8(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionStayU3Ek__BackingField_9(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CControllerColliderHitU3Ek__BackingField_10(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CFinishedU3Ek__BackingField_11(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CLevelLoadedU3Ek__BackingField_12(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseDownU3Ek__BackingField_13(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseDragU3Ek__BackingField_14(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseEnterU3Ek__BackingField_15(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseExitU3Ek__BackingField_16(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseOverU3Ek__BackingField_17(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseUpU3Ek__BackingField_18(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerEnterU3Ek__BackingField_19(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerExitU3Ek__BackingField_20(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerStayU3Ek__BackingField_21(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CApplicationFocusU3Ek__BackingField_22(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CApplicationPauseU3Ek__BackingField_23(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CApplicationQuitU3Ek__BackingField_24(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CPlayerConnectedU3Ek__BackingField_25(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CServerInitializedU3Ek__BackingField_26(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CConnectedToServerU3Ek__BackingField_27(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CPlayerDisconnectedU3Ek__BackingField_28(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CDisconnectedFromServerU3Ek__BackingField_29(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CFailedToConnectU3Ek__BackingField_30(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_31(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMasterServerEventU3Ek__BackingField_32(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CNetworkInstantiateU3Ek__BackingField_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (FsmLog_t1596141350), -1, sizeof(FsmLog_t1596141350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2521[8] = 
{
	0,
	FsmLog_t1596141350_StaticFields::get_offset_of_Logs_1(),
	FsmLog_t1596141350_StaticFields::get_offset_of_loggingEnabled_2(),
	FsmLog_t1596141350::get_offset_of_entries_3(),
	FsmLog_t1596141350_StaticFields::get_offset_of_U3CMirrorDebugLogU3Ek__BackingField_4(),
	FsmLog_t1596141350_StaticFields::get_offset_of_U3CEnableDebugFlowU3Ek__BackingField_5(),
	FsmLog_t1596141350::get_offset_of_U3CFsmU3Ek__BackingField_6(),
	FsmLog_t1596141350::get_offset_of_U3CResizedU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (FsmLogEntry_t2614866584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[14] = 
{
	FsmLogEntry_t2614866584::get_offset_of_textWithTimecode_0(),
	FsmLogEntry_t2614866584::get_offset_of_U3CLogTypeU3Ek__BackingField_1(),
	FsmLogEntry_t2614866584::get_offset_of_U3CStateU3Ek__BackingField_2(),
	FsmLogEntry_t2614866584::get_offset_of_U3CSentByStateU3Ek__BackingField_3(),
	FsmLogEntry_t2614866584::get_offset_of_U3CActionU3Ek__BackingField_4(),
	FsmLogEntry_t2614866584::get_offset_of_U3CEventU3Ek__BackingField_5(),
	FsmLogEntry_t2614866584::get_offset_of_U3CTransitionU3Ek__BackingField_6(),
	FsmLogEntry_t2614866584::get_offset_of_U3CEventTargetU3Ek__BackingField_7(),
	FsmLogEntry_t2614866584::get_offset_of_U3CTimeU3Ek__BackingField_8(),
	FsmLogEntry_t2614866584::get_offset_of_U3CTextU3Ek__BackingField_9(),
	FsmLogEntry_t2614866584::get_offset_of_U3CText2U3Ek__BackingField_10(),
	FsmLogEntry_t2614866584::get_offset_of_U3CFrameCountU3Ek__BackingField_11(),
	FsmLogEntry_t2614866584::get_offset_of_U3CFsmVariablesCopyU3Ek__BackingField_12(),
	FsmLogEntry_t2614866584::get_offset_of_U3CGlobalVariablesCopyU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (FsmLogType_t537852544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[12] = 
{
	FsmLogType_t537852544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (FsmState_t2146334067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[20] = 
{
	FsmState_t2146334067::get_offset_of_active_0(),
	FsmState_t2146334067::get_offset_of_finished_1(),
	FsmState_t2146334067::get_offset_of_activeAction_2(),
	FsmState_t2146334067::get_offset_of_activeActionIndex_3(),
	FsmState_t2146334067::get_offset_of_fsm_4(),
	FsmState_t2146334067::get_offset_of_name_5(),
	FsmState_t2146334067::get_offset_of_description_6(),
	FsmState_t2146334067::get_offset_of_colorIndex_7(),
	FsmState_t2146334067::get_offset_of_position_8(),
	FsmState_t2146334067::get_offset_of_isBreakpoint_9(),
	FsmState_t2146334067::get_offset_of_isSequence_10(),
	FsmState_t2146334067::get_offset_of_hideUnused_11(),
	FsmState_t2146334067::get_offset_of_transitions_12(),
	FsmState_t2146334067::get_offset_of_actions_13(),
	FsmState_t2146334067::get_offset_of_actionData_14(),
	FsmState_t2146334067::get_offset_of_activeActions_15(),
	FsmState_t2146334067::get_offset_of_U3CStateTimeU3Ek__BackingField_16(),
	FsmState_t2146334067::get_offset_of_U3CRealStartTimeU3Ek__BackingField_17(),
	FsmState_t2146334067::get_offset_of_U3CloopCountU3Ek__BackingField_18(),
	FsmState_t2146334067::get_offset_of_U3CmaxLoopCountU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (FsmStateAction_t2366529033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[9] = 
{
	FsmStateAction_t2366529033::get_offset_of_name_0(),
	FsmStateAction_t2366529033::get_offset_of_enabled_1(),
	FsmStateAction_t2366529033::get_offset_of_isOpen_2(),
	FsmStateAction_t2366529033::get_offset_of_active_3(),
	FsmStateAction_t2366529033::get_offset_of_finished_4(),
	FsmStateAction_t2366529033::get_offset_of_owner_5(),
	FsmStateAction_t2366529033::get_offset_of_fsmState_6(),
	FsmStateAction_t2366529033::get_offset_of_fsm_7(),
	FsmStateAction_t2366529033::get_offset_of_U3CEnteredU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (FsmTransition_t3771611999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	FsmTransition_t3771611999::get_offset_of_fsmEvent_0(),
	FsmTransition_t3771611999::get_offset_of_toState_1(),
	FsmTransition_t3771611999::get_offset_of_linkStyle_2(),
	FsmTransition_t3771611999::get_offset_of_linkConstraint_3(),
	FsmTransition_t3771611999::get_offset_of_colorIndex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (CustomLinkStyle_t2484246752)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2528[4] = 
{
	CustomLinkStyle_t2484246752::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (CustomLinkConstraint_t4157095950)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2529[4] = 
{
	CustomLinkConstraint_t4157095950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (FsmUtility_t81143630), -1, sizeof(FsmUtility_t81143630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2530[2] = 
{
	FsmUtility_t81143630_StaticFields::get_offset_of_encoding_0(),
	FsmUtility_t81143630_StaticFields::get_offset_of_CurrentFsm_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (BitConverter_t1958851146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (FsmVariables_t963491929), -1, sizeof(FsmVariables_t963491929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[14] = 
{
	FsmVariables_t963491929::get_offset_of_floatVariables_0(),
	FsmVariables_t963491929::get_offset_of_intVariables_1(),
	FsmVariables_t963491929::get_offset_of_boolVariables_2(),
	FsmVariables_t963491929::get_offset_of_stringVariables_3(),
	FsmVariables_t963491929::get_offset_of_vector2Variables_4(),
	FsmVariables_t963491929::get_offset_of_vector3Variables_5(),
	FsmVariables_t963491929::get_offset_of_colorVariables_6(),
	FsmVariables_t963491929::get_offset_of_rectVariables_7(),
	FsmVariables_t963491929::get_offset_of_quaternionVariables_8(),
	FsmVariables_t963491929::get_offset_of_gameObjectVariables_9(),
	FsmVariables_t963491929::get_offset_of_objectVariables_10(),
	FsmVariables_t963491929::get_offset_of_materialVariables_11(),
	FsmVariables_t963491929::get_offset_of_textureVariables_12(),
	FsmVariables_t963491929_StaticFields::get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (VariableType_t3118725144)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2533[15] = 
{
	VariableType_t3118725144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (VariableTypeNicified_t195153339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2534[14] = 
{
	VariableTypeNicified_t195153339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (MissingAction_t834633994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[1] = 
{
	MissingAction_t834633994::get_offset_of_actionName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (ReflectionUtils_t1202855664), -1, sizeof(ReflectionUtils_t1202855664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2536[2] = 
{
	ReflectionUtils_t1202855664_StaticFields::get_offset_of_assemblies_0(),
	ReflectionUtils_t1202855664_StaticFields::get_offset_of_typeLookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (PlayMakerProxyBase_t3469687535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[1] = 
{
	PlayMakerProxyBase_t3469687535::get_offset_of_playMakerFSMs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (PlayMakerApplicationEvents_t1615127321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (PlayMakerCollisionEnter_t4046453814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (PlayMakerCollisionExit_t3871318016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (PlayMakerCollisionStay_t3871731003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (PlayMakerFixedUpdate_t997170669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (PlayMakerFSM_t3799847376), -1, sizeof(PlayMakerFSM_t3799847376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2543[7] = 
{
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_fsmList_2(),
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_ApplicationIsQuitting_3(),
	PlayMakerFSM_t3799847376::get_offset_of_fsm_4(),
	PlayMakerFSM_t3799847376::get_offset_of_fsmTemplate_5(),
	PlayMakerFSM_t3799847376::get_offset_of_U3CGuiTextureU3Ek__BackingField_6(),
	PlayMakerFSM_t3799847376::get_offset_of_U3CGuiTextU3Ek__BackingField_7(),
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_U3CDrawGizmosU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (PlayMakerGlobals_t3097244096), -1, sizeof(PlayMakerGlobals_t3097244096_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2544[3] = 
{
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_instance_2(),
	PlayMakerGlobals_t3097244096::get_offset_of_variables_3(),
	PlayMakerGlobals_t3097244096::get_offset_of_events_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (PlayMakerGUI_t3799848395), -1, sizeof(PlayMakerGUI_t3799848395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2545[27] = 
{
	0,
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_fsmList_3(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_SelectedFSM_4(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_labelContent_5(),
	PlayMakerGUI_t3799848395::get_offset_of_previewOnGUI_6(),
	PlayMakerGUI_t3799848395::get_offset_of_enableGUILayout_7(),
	PlayMakerGUI_t3799848395::get_offset_of_drawStateLabels_8(),
	PlayMakerGUI_t3799848395::get_offset_of_GUITextureStateLabels_9(),
	PlayMakerGUI_t3799848395::get_offset_of_GUITextStateLabels_10(),
	PlayMakerGUI_t3799848395::get_offset_of_filterLabelsWithDistance_11(),
	PlayMakerGUI_t3799848395::get_offset_of_maxLabelDistance_12(),
	PlayMakerGUI_t3799848395::get_offset_of_controlMouseCursor_13(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_SortedFsmList_14(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_labelGameObject_15(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_fsmLabelIndex_16(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_instance_17(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiSkin_18(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiColor_19(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiBackgroundColor_20(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiContentColor_21(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiMatrix_22(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_stateLabelStyle_23(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_stateLabelBackground_24(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_U3CMouseCursorU3Ek__BackingField_25(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_U3CLockCursorU3Ek__BackingField_26(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_U3CHideCursorU3Ek__BackingField_27(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (PlayMakerMouseEvents_t316289710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (PlayMakerOnGUI_t940239724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	PlayMakerOnGUI_t940239724::get_offset_of_playMakerFSM_2(),
	PlayMakerOnGUI_t940239724::get_offset_of_previewInEditMode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (PlayMakerTriggerEnter_t977448304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (PlayMakerTriggerExit_t3495223174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (PlayMakerTriggerStay_t3495636161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (U3CModuleU3E_t86524803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (BarcodeFormat_t4201805817)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2552[22] = 
{
	BarcodeFormat_t4201805817::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (BarcodeReader_t240638149), -1, sizeof(BarcodeReader_t240638149_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[2] = 
{
	BarcodeReader_t240638149_StaticFields::get_offset_of_defaultCreateLuminanceSource_14(),
	BarcodeReader_t240638149_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (LuminanceSource_t1231523093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[2] = 
{
	LuminanceSource_t1231523093::get_offset_of_width_0(),
	LuminanceSource_t1231523093::get_offset_of_height_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (BaseLuminanceSource_t803260164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[1] = 
{
	BaseLuminanceSource_t803260164::get_offset_of_luminances_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (Binarizer_t1492033400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[1] = 
{
	Binarizer_t1492033400::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (BinaryBitmap_t2444664454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[2] = 
{
	BinaryBitmap_t2444664454::get_offset_of_binarizer_0(),
	BinaryBitmap_t2444664454::get_offset_of_matrix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (DecodeHintType_t2095781349)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2563[16] = 
{
	DecodeHintType_t2095781349::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (Dimension_t1395692488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[2] = 
{
	Dimension_t1395692488::get_offset_of_width_0(),
	Dimension_t1395692488::get_offset_of_height_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (EncodeHintType_t3244562957)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2565[17] = 
{
	EncodeHintType_t3244562957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (ReaderException_t1784959150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (FormatException_t3082259450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (InvertedLuminanceSource_t1075320672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[2] = 
{
	InvertedLuminanceSource_t1075320672::get_offset_of_delegate_2(),
	InvertedLuminanceSource_t1075320672::get_offset_of_invertedMatrix_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (MultiFormatReader_t4081038293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[2] = 
{
	MultiFormatReader_t4081038293::get_offset_of_hints_0(),
	MultiFormatReader_t4081038293::get_offset_of_readers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (MultiFormatWriter_t4236443525), -1, sizeof(MultiFormatWriter_t4236443525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[14] = 
{
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_formatMap_0(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12(),
	MultiFormatWriter_t4236443525_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (Result_t2610723219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[6] = 
{
	Result_t2610723219::get_offset_of_U3CTextU3Ek__BackingField_0(),
	Result_t2610723219::get_offset_of_U3CRawBytesU3Ek__BackingField_1(),
	Result_t2610723219::get_offset_of_U3CResultPointsU3Ek__BackingField_2(),
	Result_t2610723219::get_offset_of_U3CBarcodeFormatU3Ek__BackingField_3(),
	Result_t2610723219::get_offset_of_U3CResultMetadataU3Ek__BackingField_4(),
	Result_t2610723219::get_offset_of_U3CTimestampU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (ResultMetadataType_t2923366972)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2574[13] = 
{
	ResultMetadataType_t2923366972::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (ResultPoint_t1538592853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[5] = 
{
	ResultPoint_t1538592853::get_offset_of_x_0(),
	ResultPoint_t1538592853::get_offset_of_y_1(),
	ResultPoint_t1538592853::get_offset_of_bytesX_2(),
	ResultPoint_t1538592853::get_offset_of_bytesY_3(),
	ResultPoint_t1538592853::get_offset_of_toString_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (ResultPointCallback_t207829946), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (RGBLuminanceSource_t3258519836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (BitmapFormat_t3665922245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2578[11] = 
{
	BitmapFormat_t3665922245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (SupportClass_t1360542943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (WriterException_t2468555518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (DetectorResult_t3846928147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[2] = 
{
	DetectorResult_t3846928147::get_offset_of_U3CBitsU3Ek__BackingField_0(),
	DetectorResult_t3846928147::get_offset_of_U3CPointsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (AztecDetectorResult_t1662194462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[3] = 
{
	AztecDetectorResult_t1662194462::get_offset_of_U3CCompactU3Ek__BackingField_2(),
	AztecDetectorResult_t1662194462::get_offset_of_U3CNbDatablocksU3Ek__BackingField_3(),
	AztecDetectorResult_t1662194462::get_offset_of_U3CNbLayersU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (AztecReader_t3105399986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (AztecResultMetadata_t1409764443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[3] = 
{
	AztecResultMetadata_t1409764443::get_offset_of_U3CCompactU3Ek__BackingField_0(),
	AztecResultMetadata_t1409764443::get_offset_of_U3CDatablocksU3Ek__BackingField_1(),
	AztecResultMetadata_t1409764443::get_offset_of_U3CLayersU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (AztecWriter_t3260805218), -1, sizeof(AztecWriter_t3260805218_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2585[1] = 
{
	AztecWriter_t3260805218_StaticFields::get_offset_of_DEFAULT_CHARSET_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (Decoder_t16782150), -1, sizeof(Decoder_t16782150_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2586[8] = 
{
	Decoder_t16782150_StaticFields::get_offset_of_UPPER_TABLE_0(),
	Decoder_t16782150_StaticFields::get_offset_of_LOWER_TABLE_1(),
	Decoder_t16782150_StaticFields::get_offset_of_MIXED_TABLE_2(),
	Decoder_t16782150_StaticFields::get_offset_of_PUNCT_TABLE_3(),
	Decoder_t16782150_StaticFields::get_offset_of_DIGIT_TABLE_4(),
	Decoder_t16782150_StaticFields::get_offset_of_codeTables_5(),
	Decoder_t16782150_StaticFields::get_offset_of_codeTableMap_6(),
	Decoder_t16782150::get_offset_of_ddata_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Table_t1007478513)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[7] = 
{
	Table_t1007478513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (Detector_t3858377196), -1, sizeof(Detector_t3858377196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2588[7] = 
{
	Detector_t3858377196::get_offset_of_image_0(),
	Detector_t3858377196::get_offset_of_compact_1(),
	Detector_t3858377196::get_offset_of_nbLayers_2(),
	Detector_t3858377196::get_offset_of_nbDataBlocks_3(),
	Detector_t3858377196::get_offset_of_nbCenterLayers_4(),
	Detector_t3858377196::get_offset_of_shift_5(),
	Detector_t3858377196_StaticFields::get_offset_of_EXPECTED_CORNER_BITS_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Point_t330760921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[2] = 
{
	Point_t330760921::get_offset_of_U3CXU3Ek__BackingField_0(),
	Point_t330760921::get_offset_of_U3CYU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (AztecCode_t1281541704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[5] = 
{
	AztecCode_t1281541704::get_offset_of_U3CisCompactU3Ek__BackingField_0(),
	AztecCode_t1281541704::get_offset_of_U3CSizeU3Ek__BackingField_1(),
	AztecCode_t1281541704::get_offset_of_U3CLayersU3Ek__BackingField_2(),
	AztecCode_t1281541704::get_offset_of_U3CCodeWordsU3Ek__BackingField_3(),
	AztecCode_t1281541704::get_offset_of_U3CMatrixU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (Token_t3066207355), -1, sizeof(Token_t3066207355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	Token_t3066207355_StaticFields::get_offset_of_EMPTY_0(),
	Token_t3066207355::get_offset_of_previous_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (BinaryShiftToken_t3716413854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[2] = 
{
	BinaryShiftToken_t3716413854::get_offset_of_binaryShiftStart_2(),
	BinaryShiftToken_t3716413854::get_offset_of_binaryShiftByteCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (Encoder_t1161948190), -1, sizeof(Encoder_t1161948190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2593[1] = 
{
	Encoder_t1161948190_StaticFields::get_offset_of_WORD_SIZE_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (HighLevelEncoder_t3204486112), -1, sizeof(HighLevelEncoder_t3204486112_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2594[5] = 
{
	HighLevelEncoder_t3204486112_StaticFields::get_offset_of_MODE_NAMES_0(),
	HighLevelEncoder_t3204486112_StaticFields::get_offset_of_LATCH_TABLE_1(),
	HighLevelEncoder_t3204486112_StaticFields::get_offset_of_CHAR_MAP_2(),
	HighLevelEncoder_t3204486112_StaticFields::get_offset_of_SHIFT_TABLE_3(),
	HighLevelEncoder_t3204486112::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (SimpleToken_t3418679145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[2] = 
{
	SimpleToken_t3418679145::get_offset_of_value_2(),
	SimpleToken_t3418679145::get_offset_of_bitCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (State_t3065423635), -1, sizeof(State_t3065423635_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[5] = 
{
	State_t3065423635_StaticFields::get_offset_of_INITIAL_STATE_0(),
	State_t3065423635::get_offset_of_mode_1(),
	State_t3065423635::get_offset_of_token_2(),
	State_t3065423635::get_offset_of_binaryShiftByteCount_3(),
	State_t3065423635::get_offset_of_bitCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (BitArray_t4163851164), -1, sizeof(BitArray_t4163851164_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2597[3] = 
{
	BitArray_t4163851164::get_offset_of_bits_0(),
	BitArray_t4163851164::get_offset_of_size_1(),
	BitArray_t4163851164_StaticFields::get_offset_of__lookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (BitMatrix_t1058711404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[4] = 
{
	BitMatrix_t1058711404::get_offset_of_width_0(),
	BitMatrix_t1058711404::get_offset_of_height_1(),
	BitMatrix_t1058711404::get_offset_of_rowSize_2(),
	BitMatrix_t1058711404::get_offset_of_bits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (BitSource_t1243445190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	BitSource_t1243445190::get_offset_of_bytes_0(),
	BitSource_t1243445190::get_offset_of_byteOffset_1(),
	BitSource_t1243445190::get_offset_of_bitOffset_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
