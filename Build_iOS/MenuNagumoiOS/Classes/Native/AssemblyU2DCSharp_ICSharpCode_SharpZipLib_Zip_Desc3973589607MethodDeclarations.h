﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.DescriptorData
struct DescriptorData_t3973589607;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::.ctor()
extern "C"  void DescriptorData__ctor_m3961625380 (DescriptorData_t3973589607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_CompressedSize()
extern "C"  int64_t DescriptorData_get_CompressedSize_m911940954 (DescriptorData_t3973589607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_CompressedSize(System.Int64)
extern "C"  void DescriptorData_set_CompressedSize_m358633105 (DescriptorData_t3973589607 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_Size()
extern "C"  int64_t DescriptorData_get_Size_m821253945 (DescriptorData_t3973589607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_Size(System.Int64)
extern "C"  void DescriptorData_set_Size_m3431971888 (DescriptorData_t3973589607 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_Crc()
extern "C"  int64_t DescriptorData_get_Crc_m2782906494 (DescriptorData_t3973589607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_Crc(System.Int64)
extern "C"  void DescriptorData_set_Crc_m1523761675 (DescriptorData_t3973589607 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
