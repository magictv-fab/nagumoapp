﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DeviceCameraController
struct DeviceCameraController_t2043460407;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.BarcodeReader
struct BarcodeReader_t240638149;
// QRCodeDecodeController/QRScanFinished
struct QRScanFinished_t1583106503;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QRCodeDecodeController
struct  QRCodeDecodeController_t2145328120  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean QRCodeDecodeController::decoding
	bool ___decoding_2;
	// System.Boolean QRCodeDecodeController::tempDecodeing
	bool ___tempDecodeing_3;
	// System.String QRCodeDecodeController::dataText
	String_t* ___dataText_4;
	// DeviceCameraController QRCodeDecodeController::e_DeviceController
	DeviceCameraController_t2043460407 * ___e_DeviceController_5;
	// UnityEngine.Color[] QRCodeDecodeController::orginalc
	ColorU5BU5D_t2441545636* ___orginalc_6;
	// UnityEngine.Color32[] QRCodeDecodeController::targetColorARR
	Color32U5BU5D_t2960766953* ___targetColorARR_7;
	// System.Byte[] QRCodeDecodeController::targetbyte
	ByteU5BU5D_t4260760469* ___targetbyte_8;
	// System.Int32 QRCodeDecodeController::W
	int32_t ___W_9;
	// System.Int32 QRCodeDecodeController::H
	int32_t ___H_10;
	// System.Int32 QRCodeDecodeController::WxH
	int32_t ___WxH_11;
	// System.Int32 QRCodeDecodeController::framerate
	int32_t ___framerate_12;
	// System.Int32 QRCodeDecodeController::blockWidth
	int32_t ___blockWidth_13;
	// System.Boolean QRCodeDecodeController::isInit
	bool ___isInit_14;
	// ZXing.BarcodeReader QRCodeDecodeController::barReader
	BarcodeReader_t240638149 * ___barReader_15;
	// QRCodeDecodeController/QRScanFinished QRCodeDecodeController::onQRScanFinished
	QRScanFinished_t1583106503 * ___onQRScanFinished_16;

public:
	inline static int32_t get_offset_of_decoding_2() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___decoding_2)); }
	inline bool get_decoding_2() const { return ___decoding_2; }
	inline bool* get_address_of_decoding_2() { return &___decoding_2; }
	inline void set_decoding_2(bool value)
	{
		___decoding_2 = value;
	}

	inline static int32_t get_offset_of_tempDecodeing_3() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___tempDecodeing_3)); }
	inline bool get_tempDecodeing_3() const { return ___tempDecodeing_3; }
	inline bool* get_address_of_tempDecodeing_3() { return &___tempDecodeing_3; }
	inline void set_tempDecodeing_3(bool value)
	{
		___tempDecodeing_3 = value;
	}

	inline static int32_t get_offset_of_dataText_4() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___dataText_4)); }
	inline String_t* get_dataText_4() const { return ___dataText_4; }
	inline String_t** get_address_of_dataText_4() { return &___dataText_4; }
	inline void set_dataText_4(String_t* value)
	{
		___dataText_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataText_4, value);
	}

	inline static int32_t get_offset_of_e_DeviceController_5() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___e_DeviceController_5)); }
	inline DeviceCameraController_t2043460407 * get_e_DeviceController_5() const { return ___e_DeviceController_5; }
	inline DeviceCameraController_t2043460407 ** get_address_of_e_DeviceController_5() { return &___e_DeviceController_5; }
	inline void set_e_DeviceController_5(DeviceCameraController_t2043460407 * value)
	{
		___e_DeviceController_5 = value;
		Il2CppCodeGenWriteBarrier(&___e_DeviceController_5, value);
	}

	inline static int32_t get_offset_of_orginalc_6() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___orginalc_6)); }
	inline ColorU5BU5D_t2441545636* get_orginalc_6() const { return ___orginalc_6; }
	inline ColorU5BU5D_t2441545636** get_address_of_orginalc_6() { return &___orginalc_6; }
	inline void set_orginalc_6(ColorU5BU5D_t2441545636* value)
	{
		___orginalc_6 = value;
		Il2CppCodeGenWriteBarrier(&___orginalc_6, value);
	}

	inline static int32_t get_offset_of_targetColorARR_7() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___targetColorARR_7)); }
	inline Color32U5BU5D_t2960766953* get_targetColorARR_7() const { return ___targetColorARR_7; }
	inline Color32U5BU5D_t2960766953** get_address_of_targetColorARR_7() { return &___targetColorARR_7; }
	inline void set_targetColorARR_7(Color32U5BU5D_t2960766953* value)
	{
		___targetColorARR_7 = value;
		Il2CppCodeGenWriteBarrier(&___targetColorARR_7, value);
	}

	inline static int32_t get_offset_of_targetbyte_8() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___targetbyte_8)); }
	inline ByteU5BU5D_t4260760469* get_targetbyte_8() const { return ___targetbyte_8; }
	inline ByteU5BU5D_t4260760469** get_address_of_targetbyte_8() { return &___targetbyte_8; }
	inline void set_targetbyte_8(ByteU5BU5D_t4260760469* value)
	{
		___targetbyte_8 = value;
		Il2CppCodeGenWriteBarrier(&___targetbyte_8, value);
	}

	inline static int32_t get_offset_of_W_9() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___W_9)); }
	inline int32_t get_W_9() const { return ___W_9; }
	inline int32_t* get_address_of_W_9() { return &___W_9; }
	inline void set_W_9(int32_t value)
	{
		___W_9 = value;
	}

	inline static int32_t get_offset_of_H_10() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___H_10)); }
	inline int32_t get_H_10() const { return ___H_10; }
	inline int32_t* get_address_of_H_10() { return &___H_10; }
	inline void set_H_10(int32_t value)
	{
		___H_10 = value;
	}

	inline static int32_t get_offset_of_WxH_11() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___WxH_11)); }
	inline int32_t get_WxH_11() const { return ___WxH_11; }
	inline int32_t* get_address_of_WxH_11() { return &___WxH_11; }
	inline void set_WxH_11(int32_t value)
	{
		___WxH_11 = value;
	}

	inline static int32_t get_offset_of_framerate_12() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___framerate_12)); }
	inline int32_t get_framerate_12() const { return ___framerate_12; }
	inline int32_t* get_address_of_framerate_12() { return &___framerate_12; }
	inline void set_framerate_12(int32_t value)
	{
		___framerate_12 = value;
	}

	inline static int32_t get_offset_of_blockWidth_13() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___blockWidth_13)); }
	inline int32_t get_blockWidth_13() const { return ___blockWidth_13; }
	inline int32_t* get_address_of_blockWidth_13() { return &___blockWidth_13; }
	inline void set_blockWidth_13(int32_t value)
	{
		___blockWidth_13 = value;
	}

	inline static int32_t get_offset_of_isInit_14() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___isInit_14)); }
	inline bool get_isInit_14() const { return ___isInit_14; }
	inline bool* get_address_of_isInit_14() { return &___isInit_14; }
	inline void set_isInit_14(bool value)
	{
		___isInit_14 = value;
	}

	inline static int32_t get_offset_of_barReader_15() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___barReader_15)); }
	inline BarcodeReader_t240638149 * get_barReader_15() const { return ___barReader_15; }
	inline BarcodeReader_t240638149 ** get_address_of_barReader_15() { return &___barReader_15; }
	inline void set_barReader_15(BarcodeReader_t240638149 * value)
	{
		___barReader_15 = value;
		Il2CppCodeGenWriteBarrier(&___barReader_15, value);
	}

	inline static int32_t get_offset_of_onQRScanFinished_16() { return static_cast<int32_t>(offsetof(QRCodeDecodeController_t2145328120, ___onQRScanFinished_16)); }
	inline QRScanFinished_t1583106503 * get_onQRScanFinished_16() const { return ___onQRScanFinished_16; }
	inline QRScanFinished_t1583106503 ** get_address_of_onQRScanFinished_16() { return &___onQRScanFinished_16; }
	inline void set_onQRScanFinished_16(QRScanFinished_t1583106503 * value)
	{
		___onQRScanFinished_16 = value;
		Il2CppCodeGenWriteBarrier(&___onQRScanFinished_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
