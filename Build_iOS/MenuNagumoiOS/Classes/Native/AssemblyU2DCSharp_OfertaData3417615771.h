﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertaData
struct  OfertaData_t3417615771  : public Il2CppObject
{
public:
	// System.Int32 OfertaData::id_oferta
	int32_t ___id_oferta_0;
	// System.Int32 OfertaData::desconto
	int32_t ___desconto_1;
	// System.Int32 OfertaData::pague
	int32_t ___pague_2;
	// System.Int32 OfertaData::leve
	int32_t ___leve_3;
	// System.Int32 OfertaData::unidade
	int32_t ___unidade_4;
	// System.Int32 OfertaData::peso
	int32_t ___peso_5;
	// System.String OfertaData::imagem
	String_t* ___imagem_6;
	// System.String OfertaData::titulo
	String_t* ___titulo_7;
	// System.String OfertaData::ean
	String_t* ___ean_8;
	// System.String OfertaData::texto
	String_t* ___texto_9;
	// System.String OfertaData::datainicial
	String_t* ___datainicial_10;
	// System.String OfertaData::datafinal
	String_t* ___datafinal_11;
	// System.String OfertaData::link
	String_t* ___link_12;
	// System.String OfertaData::dataAquisicao
	String_t* ___dataAquisicao_13;
	// System.Boolean OfertaData::aderiu
	bool ___aderiu_14;

public:
	inline static int32_t get_offset_of_id_oferta_0() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___id_oferta_0)); }
	inline int32_t get_id_oferta_0() const { return ___id_oferta_0; }
	inline int32_t* get_address_of_id_oferta_0() { return &___id_oferta_0; }
	inline void set_id_oferta_0(int32_t value)
	{
		___id_oferta_0 = value;
	}

	inline static int32_t get_offset_of_desconto_1() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___desconto_1)); }
	inline int32_t get_desconto_1() const { return ___desconto_1; }
	inline int32_t* get_address_of_desconto_1() { return &___desconto_1; }
	inline void set_desconto_1(int32_t value)
	{
		___desconto_1 = value;
	}

	inline static int32_t get_offset_of_pague_2() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___pague_2)); }
	inline int32_t get_pague_2() const { return ___pague_2; }
	inline int32_t* get_address_of_pague_2() { return &___pague_2; }
	inline void set_pague_2(int32_t value)
	{
		___pague_2 = value;
	}

	inline static int32_t get_offset_of_leve_3() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___leve_3)); }
	inline int32_t get_leve_3() const { return ___leve_3; }
	inline int32_t* get_address_of_leve_3() { return &___leve_3; }
	inline void set_leve_3(int32_t value)
	{
		___leve_3 = value;
	}

	inline static int32_t get_offset_of_unidade_4() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___unidade_4)); }
	inline int32_t get_unidade_4() const { return ___unidade_4; }
	inline int32_t* get_address_of_unidade_4() { return &___unidade_4; }
	inline void set_unidade_4(int32_t value)
	{
		___unidade_4 = value;
	}

	inline static int32_t get_offset_of_peso_5() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___peso_5)); }
	inline int32_t get_peso_5() const { return ___peso_5; }
	inline int32_t* get_address_of_peso_5() { return &___peso_5; }
	inline void set_peso_5(int32_t value)
	{
		___peso_5 = value;
	}

	inline static int32_t get_offset_of_imagem_6() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___imagem_6)); }
	inline String_t* get_imagem_6() const { return ___imagem_6; }
	inline String_t** get_address_of_imagem_6() { return &___imagem_6; }
	inline void set_imagem_6(String_t* value)
	{
		___imagem_6 = value;
		Il2CppCodeGenWriteBarrier(&___imagem_6, value);
	}

	inline static int32_t get_offset_of_titulo_7() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___titulo_7)); }
	inline String_t* get_titulo_7() const { return ___titulo_7; }
	inline String_t** get_address_of_titulo_7() { return &___titulo_7; }
	inline void set_titulo_7(String_t* value)
	{
		___titulo_7 = value;
		Il2CppCodeGenWriteBarrier(&___titulo_7, value);
	}

	inline static int32_t get_offset_of_ean_8() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___ean_8)); }
	inline String_t* get_ean_8() const { return ___ean_8; }
	inline String_t** get_address_of_ean_8() { return &___ean_8; }
	inline void set_ean_8(String_t* value)
	{
		___ean_8 = value;
		Il2CppCodeGenWriteBarrier(&___ean_8, value);
	}

	inline static int32_t get_offset_of_texto_9() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___texto_9)); }
	inline String_t* get_texto_9() const { return ___texto_9; }
	inline String_t** get_address_of_texto_9() { return &___texto_9; }
	inline void set_texto_9(String_t* value)
	{
		___texto_9 = value;
		Il2CppCodeGenWriteBarrier(&___texto_9, value);
	}

	inline static int32_t get_offset_of_datainicial_10() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___datainicial_10)); }
	inline String_t* get_datainicial_10() const { return ___datainicial_10; }
	inline String_t** get_address_of_datainicial_10() { return &___datainicial_10; }
	inline void set_datainicial_10(String_t* value)
	{
		___datainicial_10 = value;
		Il2CppCodeGenWriteBarrier(&___datainicial_10, value);
	}

	inline static int32_t get_offset_of_datafinal_11() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___datafinal_11)); }
	inline String_t* get_datafinal_11() const { return ___datafinal_11; }
	inline String_t** get_address_of_datafinal_11() { return &___datafinal_11; }
	inline void set_datafinal_11(String_t* value)
	{
		___datafinal_11 = value;
		Il2CppCodeGenWriteBarrier(&___datafinal_11, value);
	}

	inline static int32_t get_offset_of_link_12() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___link_12)); }
	inline String_t* get_link_12() const { return ___link_12; }
	inline String_t** get_address_of_link_12() { return &___link_12; }
	inline void set_link_12(String_t* value)
	{
		___link_12 = value;
		Il2CppCodeGenWriteBarrier(&___link_12, value);
	}

	inline static int32_t get_offset_of_dataAquisicao_13() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___dataAquisicao_13)); }
	inline String_t* get_dataAquisicao_13() const { return ___dataAquisicao_13; }
	inline String_t** get_address_of_dataAquisicao_13() { return &___dataAquisicao_13; }
	inline void set_dataAquisicao_13(String_t* value)
	{
		___dataAquisicao_13 = value;
		Il2CppCodeGenWriteBarrier(&___dataAquisicao_13, value);
	}

	inline static int32_t get_offset_of_aderiu_14() { return static_cast<int32_t>(offsetof(OfertaData_t3417615771, ___aderiu_14)); }
	inline bool get_aderiu_14() const { return ___aderiu_14; }
	inline bool* get_address_of_aderiu_14() { return &___aderiu_14; }
	inline void set_aderiu_14(bool value)
	{
		___aderiu_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
