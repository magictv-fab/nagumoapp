﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenericWindowControllerScript
struct GenericWindowControllerScript_t248075822;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientationT3688806624.h"
#include "mscorlib_System_String7231557.h"

// System.Void GenericWindowControllerScript::.ctor()
extern "C"  void GenericWindowControllerScript__ctor_m3934605053 (GenericWindowControllerScript_t248075822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::Start()
extern "C"  void GenericWindowControllerScript_Start_m2881742845 (GenericWindowControllerScript_t248075822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::OnDestroy()
extern "C"  void GenericWindowControllerScript_OnDestroy_m149773686 (GenericWindowControllerScript_t248075822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::Hide()
extern "C"  void GenericWindowControllerScript_Hide_m2677448553 (GenericWindowControllerScript_t248075822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::Show()
extern "C"  void GenericWindowControllerScript_Show_m2991790692 (GenericWindowControllerScript_t248075822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::OnEnable()
extern "C"  void GenericWindowControllerScript_OnEnable_m3938237289 (GenericWindowControllerScript_t248075822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::LabelAnimationStop(UnityEngine.UI.Text)
extern "C"  void GenericWindowControllerScript_LabelAnimationStop_m2557343273 (GenericWindowControllerScript_t248075822 * __this, Text_t9039225 * ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::LabelAnimationStart(UnityEngine.UI.Text)
extern "C"  void GenericWindowControllerScript_LabelAnimationStart_m1138256629 (GenericWindowControllerScript_t248075822 * __this, Text_t9039225 * ___label0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::OnChangeScreenOrientationType(UnityEngine.Vector2,ARM.gui.utils.ScreenOrientationType)
extern "C"  void GenericWindowControllerScript_OnChangeScreenOrientationType_m1881926592 (GenericWindowControllerScript_t248075822 * __this, Vector2_t4282066565  ___screenSize0, int32_t ___screenOrientationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> GenericWindowControllerScript::FindChildrensByName(System.String)
extern "C"  List_1_t747900261 * GenericWindowControllerScript_FindChildrensByName_m2337383855 (GenericWindowControllerScript_t248075822 * __this, String_t* ___childrenName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenericWindowControllerScript::SetCurrentOrientation(ARM.gui.utils.ScreenOrientationType)
extern "C"  void GenericWindowControllerScript_SetCurrentOrientation_m1341559778 (GenericWindowControllerScript_t248075822 * __this, int32_t ___screenOrientationType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
