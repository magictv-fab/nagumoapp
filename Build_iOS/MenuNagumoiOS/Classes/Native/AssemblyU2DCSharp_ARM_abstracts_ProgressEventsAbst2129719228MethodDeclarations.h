﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.ProgressEventsAbstract
struct ProgressEventsAbstract_t2129719228;
// ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler
struct OnProgressEventHandler_t3695678548;
// System.String
struct String_t;
// ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler
struct OnErrorEventHandler_t1722022561;
// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler
struct OnCompleteEventHandler_t2318258528;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst3695678548.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst1722022561.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2318258528.h"

// System.Void ARM.abstracts.ProgressEventsAbstract::.ctor()
extern "C"  void ProgressEventsAbstract__ctor_m2563033348 (ProgressEventsAbstract_t2129719228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.abstracts.ProgressEventsAbstract::GetCurrentProgress()
extern "C"  float ProgressEventsAbstract_GetCurrentProgress_m4090759132 (ProgressEventsAbstract_t2129719228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.abstracts.ProgressEventsAbstract::GetCurrentProgressWithWeight()
extern "C"  float ProgressEventsAbstract_GetCurrentProgressWithWeight_m3850647962 (ProgressEventsAbstract_t2129719228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RaiseProgress(System.Single)
extern "C"  void ProgressEventsAbstract_RaiseProgress_m1228468336 (ProgressEventsAbstract_t2129719228 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::AddProgressEventhandler(ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler)
extern "C"  void ProgressEventsAbstract_AddProgressEventhandler_m4095412030 (ProgressEventsAbstract_t2129719228 * __this, OnProgressEventHandler_t3695678548 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RemoveProgressEventhandler(ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler)
extern "C"  void ProgressEventsAbstract_RemoveProgressEventhandler_m3896820829 (ProgressEventsAbstract_t2129719228 * __this, OnProgressEventHandler_t3695678548 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RaiseError(System.String)
extern "C"  void ProgressEventsAbstract_RaiseError_m936644998 (ProgressEventsAbstract_t2129719228 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::AddErrorEventhandler(ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler)
extern "C"  void ProgressEventsAbstract_AddErrorEventhandler_m4007134838 (ProgressEventsAbstract_t2129719228 * __this, OnErrorEventHandler_t1722022561 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RemoveErrorEventhandler(ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler)
extern "C"  void ProgressEventsAbstract_RemoveErrorEventhandler_m1071761109 (ProgressEventsAbstract_t2129719228 * __this, OnErrorEventHandler_t1722022561 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RaiseCompleteAndTotalProgress()
extern "C"  void ProgressEventsAbstract_RaiseCompleteAndTotalProgress_m3661157313 (ProgressEventsAbstract_t2129719228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RaiseComplete()
extern "C"  void ProgressEventsAbstract_RaiseComplete_m1002787111 (ProgressEventsAbstract_t2129719228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::AddCompleteEventhandler(ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler)
extern "C"  void ProgressEventsAbstract_AddCompleteEventhandler_m2455538214 (ProgressEventsAbstract_t2129719228 * __this, OnCompleteEventHandler_t2318258528 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract::RemoveCompleteEventhandler(ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler)
extern "C"  void ProgressEventsAbstract_RemoveCompleteEventhandler_m2256947013 (ProgressEventsAbstract_t2129719228 * __this, OnCompleteEventHandler_t2318258528 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.abstracts.ProgressEventsAbstract::isComplete()
extern "C"  bool ProgressEventsAbstract_isComplete_m456274391 (ProgressEventsAbstract_t2129719228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
