﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SoundController
struct SoundController_t665516523;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundController
struct  SoundController_t665516523  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SoundController_t665516523_StaticFields
{
public:
	// SoundController SoundController::_soundController
	SoundController_t665516523 * ____soundController_2;

public:
	inline static int32_t get_offset_of__soundController_2() { return static_cast<int32_t>(offsetof(SoundController_t665516523_StaticFields, ____soundController_2)); }
	inline SoundController_t665516523 * get__soundController_2() const { return ____soundController_2; }
	inline SoundController_t665516523 ** get_address_of__soundController_2() { return &____soundController_2; }
	inline void set__soundController_2(SoundController_t665516523 * value)
	{
		____soundController_2 = value;
		Il2CppCodeGenWriteBarrier(&____soundController_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
