﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AvaliacaoRespData
struct  AvaliacaoRespData_t3851297587  : public Il2CppObject
{
public:
	// System.String AvaliacaoRespData::resposta
	String_t* ___resposta_0;

public:
	inline static int32_t get_offset_of_resposta_0() { return static_cast<int32_t>(offsetof(AvaliacaoRespData_t3851297587, ___resposta_0)); }
	inline String_t* get_resposta_0() const { return ___resposta_0; }
	inline String_t** get_address_of_resposta_0() { return &___resposta_0; }
	inline void set_resposta_0(String_t* value)
	{
		___resposta_0 = value;
		Il2CppCodeGenWriteBarrier(&___resposta_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
