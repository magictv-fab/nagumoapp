﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.screens.MenuScreenAbstract/OnVoidEventHandler
struct OnVoidEventHandler_t284867202;

#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_Genera2842388060.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.screens.MenuScreenAbstract
struct  MenuScreenAbstract_t4225613753  : public GeneralScreenAbstract_t2842388060
{
public:
	// MagicTV.abstracts.screens.MenuScreenAbstract/OnVoidEventHandler MagicTV.abstracts.screens.MenuScreenAbstract::onPrintScreenCalled
	OnVoidEventHandler_t284867202 * ___onPrintScreenCalled_5;
	// MagicTV.abstracts.screens.MenuScreenAbstract/OnVoidEventHandler MagicTV.abstracts.screens.MenuScreenAbstract::onClearCacheCalled
	OnVoidEventHandler_t284867202 * ___onClearCacheCalled_6;
	// MagicTV.abstracts.screens.MenuScreenAbstract/OnVoidEventHandler MagicTV.abstracts.screens.MenuScreenAbstract::onResetCalled
	OnVoidEventHandler_t284867202 * ___onResetCalled_7;

public:
	inline static int32_t get_offset_of_onPrintScreenCalled_5() { return static_cast<int32_t>(offsetof(MenuScreenAbstract_t4225613753, ___onPrintScreenCalled_5)); }
	inline OnVoidEventHandler_t284867202 * get_onPrintScreenCalled_5() const { return ___onPrintScreenCalled_5; }
	inline OnVoidEventHandler_t284867202 ** get_address_of_onPrintScreenCalled_5() { return &___onPrintScreenCalled_5; }
	inline void set_onPrintScreenCalled_5(OnVoidEventHandler_t284867202 * value)
	{
		___onPrintScreenCalled_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPrintScreenCalled_5, value);
	}

	inline static int32_t get_offset_of_onClearCacheCalled_6() { return static_cast<int32_t>(offsetof(MenuScreenAbstract_t4225613753, ___onClearCacheCalled_6)); }
	inline OnVoidEventHandler_t284867202 * get_onClearCacheCalled_6() const { return ___onClearCacheCalled_6; }
	inline OnVoidEventHandler_t284867202 ** get_address_of_onClearCacheCalled_6() { return &___onClearCacheCalled_6; }
	inline void set_onClearCacheCalled_6(OnVoidEventHandler_t284867202 * value)
	{
		___onClearCacheCalled_6 = value;
		Il2CppCodeGenWriteBarrier(&___onClearCacheCalled_6, value);
	}

	inline static int32_t get_offset_of_onResetCalled_7() { return static_cast<int32_t>(offsetof(MenuScreenAbstract_t4225613753, ___onResetCalled_7)); }
	inline OnVoidEventHandler_t284867202 * get_onResetCalled_7() const { return ___onResetCalled_7; }
	inline OnVoidEventHandler_t284867202 ** get_address_of_onResetCalled_7() { return &___onResetCalled_7; }
	inline void set_onResetCalled_7(OnVoidEventHandler_t284867202 * value)
	{
		___onResetCalled_7 = value;
		Il2CppCodeGenWriteBarrier(&___onResetCalled_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
