﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4158207179.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3460279177_gshared (InternalEnumerator_1_t4158207179 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3460279177(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4158207179 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3460279177_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1870422903_gshared (InternalEnumerator_1_t4158207179 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1870422903(__this, method) ((  void (*) (InternalEnumerator_1_t4158207179 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1870422903_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m660058605_gshared (InternalEnumerator_1_t4158207179 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m660058605(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4158207179 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m660058605_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3928710304_gshared (InternalEnumerator_1_t4158207179 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3928710304(__this, method) ((  void (*) (InternalEnumerator_1_t4158207179 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3928710304_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1581343015_gshared (InternalEnumerator_1_t4158207179 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1581343015(__this, method) ((  bool (*) (InternalEnumerator_1_t4158207179 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1581343015_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1080897207  InternalEnumerator_1_get_Current_m1964799282_gshared (InternalEnumerator_1_t4158207179 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1964799282(__this, method) ((  KeyValuePair_2_t1080897207  (*) (InternalEnumerator_1_t4158207179 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1964799282_gshared)(__this, method)
