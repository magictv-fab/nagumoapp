﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.EyewearUserCalibratorImpl
struct EyewearUserCalibratorImpl_t3502530982;
// Vuforia.Eyewear/EyewearCalibrationReading[]
struct EyewearCalibrationReadingU5BU5D_t4186969886;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// System.Boolean Vuforia.EyewearUserCalibratorImpl::init(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool EyewearUserCalibratorImpl_init_m2019069341 (EyewearUserCalibratorImpl_t3502530982 * __this, int32_t ___surfaceWidth0, int32_t ___surfaceHeight1, int32_t ___targetWidth2, int32_t ___targetHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.EyewearUserCalibratorImpl::getMinScaleHint()
extern "C"  float EyewearUserCalibratorImpl_getMinScaleHint_m2854591198 (EyewearUserCalibratorImpl_t3502530982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.EyewearUserCalibratorImpl::getMaxScaleHint()
extern "C"  float EyewearUserCalibratorImpl_getMaxScaleHint_m2197675148 (EyewearUserCalibratorImpl_t3502530982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearUserCalibratorImpl::isStereoStretched()
extern "C"  bool EyewearUserCalibratorImpl_isStereoStretched_m3480295307 (EyewearUserCalibratorImpl_t3502530982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearUserCalibratorImpl::getProjectionMatrix(Vuforia.Eyewear/EyewearCalibrationReading[],UnityEngine.Matrix4x4)
extern "C"  bool EyewearUserCalibratorImpl_getProjectionMatrix_m3355440302 (EyewearUserCalibratorImpl_t3502530982 * __this, EyewearCalibrationReadingU5BU5D_t4186969886* ___readings0, Matrix4x4_t1651859333  ___projectionMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearUserCalibratorImpl::.ctor()
extern "C"  void EyewearUserCalibratorImpl__ctor_m2136567007 (EyewearUserCalibratorImpl_t3502530982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
