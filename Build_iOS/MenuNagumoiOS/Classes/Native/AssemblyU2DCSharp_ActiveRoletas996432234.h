﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActiveRoletas
struct  ActiveRoletas_t996432234  : public MonoBehaviour_t667441552
{
public:
	// System.Single ActiveRoletas::time
	float ___time_2;
	// System.Boolean ActiveRoletas::active
	bool ___active_3;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(ActiveRoletas_t996432234, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(ActiveRoletas_t996432234, ___active_3)); }
	inline bool get_active_3() const { return ___active_3; }
	inline bool* get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(bool value)
	{
		___active_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
