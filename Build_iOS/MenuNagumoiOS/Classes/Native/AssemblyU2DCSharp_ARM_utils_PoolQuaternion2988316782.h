﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolFloat
struct PoolFloat_t382107510;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2399324759;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.PoolQuaternion
struct  PoolQuaternion_t2988316782  : public Il2CppObject
{
public:
	// ARM.utils.PoolFloat ARM.utils.PoolQuaternion::_x
	PoolFloat_t382107510 * ____x_0;
	// ARM.utils.PoolFloat ARM.utils.PoolQuaternion::_y
	PoolFloat_t382107510 * ____y_1;
	// ARM.utils.PoolFloat ARM.utils.PoolQuaternion::_z
	PoolFloat_t382107510 * ____z_2;
	// ARM.utils.PoolFloat ARM.utils.PoolQuaternion::_w
	PoolFloat_t382107510 * ____w_3;
	// System.Int32 ARM.utils.PoolQuaternion::_currentIndex
	int32_t ____currentIndex_4;
	// System.UInt32 ARM.utils.PoolQuaternion::_maxSize
	uint32_t ____maxSize_5;
	// System.Int32 ARM.utils.PoolQuaternion::_currentSize
	int32_t ____currentSize_6;
	// UnityEngine.Quaternion[] ARM.utils.PoolQuaternion::itens
	QuaternionU5BU5D_t2399324759* ___itens_7;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____x_0)); }
	inline PoolFloat_t382107510 * get__x_0() const { return ____x_0; }
	inline PoolFloat_t382107510 ** get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(PoolFloat_t382107510 * value)
	{
		____x_0 = value;
		Il2CppCodeGenWriteBarrier(&____x_0, value);
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____y_1)); }
	inline PoolFloat_t382107510 * get__y_1() const { return ____y_1; }
	inline PoolFloat_t382107510 ** get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(PoolFloat_t382107510 * value)
	{
		____y_1 = value;
		Il2CppCodeGenWriteBarrier(&____y_1, value);
	}

	inline static int32_t get_offset_of__z_2() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____z_2)); }
	inline PoolFloat_t382107510 * get__z_2() const { return ____z_2; }
	inline PoolFloat_t382107510 ** get_address_of__z_2() { return &____z_2; }
	inline void set__z_2(PoolFloat_t382107510 * value)
	{
		____z_2 = value;
		Il2CppCodeGenWriteBarrier(&____z_2, value);
	}

	inline static int32_t get_offset_of__w_3() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____w_3)); }
	inline PoolFloat_t382107510 * get__w_3() const { return ____w_3; }
	inline PoolFloat_t382107510 ** get_address_of__w_3() { return &____w_3; }
	inline void set__w_3(PoolFloat_t382107510 * value)
	{
		____w_3 = value;
		Il2CppCodeGenWriteBarrier(&____w_3, value);
	}

	inline static int32_t get_offset_of__currentIndex_4() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____currentIndex_4)); }
	inline int32_t get__currentIndex_4() const { return ____currentIndex_4; }
	inline int32_t* get_address_of__currentIndex_4() { return &____currentIndex_4; }
	inline void set__currentIndex_4(int32_t value)
	{
		____currentIndex_4 = value;
	}

	inline static int32_t get_offset_of__maxSize_5() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____maxSize_5)); }
	inline uint32_t get__maxSize_5() const { return ____maxSize_5; }
	inline uint32_t* get_address_of__maxSize_5() { return &____maxSize_5; }
	inline void set__maxSize_5(uint32_t value)
	{
		____maxSize_5 = value;
	}

	inline static int32_t get_offset_of__currentSize_6() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ____currentSize_6)); }
	inline int32_t get__currentSize_6() const { return ____currentSize_6; }
	inline int32_t* get_address_of__currentSize_6() { return &____currentSize_6; }
	inline void set__currentSize_6(int32_t value)
	{
		____currentSize_6 = value;
	}

	inline static int32_t get_offset_of_itens_7() { return static_cast<int32_t>(offsetof(PoolQuaternion_t2988316782, ___itens_7)); }
	inline QuaternionU5BU5D_t2399324759* get_itens_7() const { return ___itens_7; }
	inline QuaternionU5BU5D_t2399324759** get_address_of_itens_7() { return &___itens_7; }
	inline void set_itens_7(QuaternionU5BU5D_t2399324759* value)
	{
		___itens_7 = value;
		Il2CppCodeGenWriteBarrier(&___itens_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
