﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.EC.ErrorCorrection
struct ErrorCorrection_t2629846604;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.PDF417.Internal.EC.ModulusPoly[]
struct ModulusPolyU5BU5D_t731609620;
// ZXing.PDF417.Internal.EC.ModulusPoly
struct ModulusPoly_t3133325097;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_EC_ModulusPoly3133325097.h"

// System.Void ZXing.PDF417.Internal.EC.ErrorCorrection::.ctor()
extern "C"  void ErrorCorrection__ctor_m3886079536 (ErrorCorrection_t2629846604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.EC.ErrorCorrection::decode(System.Int32[],System.Int32,System.Int32[],System.Int32&)
extern "C"  bool ErrorCorrection_decode_m4006334168 (ErrorCorrection_t2629846604 * __this, Int32U5BU5D_t3230847821* ___received0, int32_t ___numECCodewords1, Int32U5BU5D_t3230847821* ___erasures2, int32_t* ___errorLocationsCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly[] ZXing.PDF417.Internal.EC.ErrorCorrection::runEuclideanAlgorithm(ZXing.PDF417.Internal.EC.ModulusPoly,ZXing.PDF417.Internal.EC.ModulusPoly,System.Int32)
extern "C"  ModulusPolyU5BU5D_t731609620* ErrorCorrection_runEuclideanAlgorithm_m2767340520 (ErrorCorrection_t2629846604 * __this, ModulusPoly_t3133325097 * ___a0, ModulusPoly_t3133325097 * ___b1, int32_t ___R2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.EC.ErrorCorrection::findErrorLocations(ZXing.PDF417.Internal.EC.ModulusPoly)
extern "C"  Int32U5BU5D_t3230847821* ErrorCorrection_findErrorLocations_m2310677927 (ErrorCorrection_t2629846604 * __this, ModulusPoly_t3133325097 * ___errorLocator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.EC.ErrorCorrection::findErrorMagnitudes(ZXing.PDF417.Internal.EC.ModulusPoly,ZXing.PDF417.Internal.EC.ModulusPoly,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* ErrorCorrection_findErrorMagnitudes_m4252152451 (ErrorCorrection_t2629846604 * __this, ModulusPoly_t3133325097 * ___errorEvaluator0, ModulusPoly_t3133325097 * ___errorLocator1, Int32U5BU5D_t3230847821* ___errorLocations2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
