﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen785513668MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.UI.Dropdown/OptionData,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3120974545(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2150945857 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m949542583_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UnityEngine.UI.Dropdown/OptionData,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2276803169(__this, ___arg10, method) ((  bool (*) (Func_2_t2150945857 *, OptionData_t185687546 *, const MethodInfo*))Func_2_Invoke_m1882130143_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Dropdown/OptionData,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3265391376(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2150945857 *, OptionData_t185687546 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1852288274_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UnityEngine.UI.Dropdown/OptionData,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2097674259(__this, ___result0, method) ((  bool (*) (Func_2_t2150945857 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1659014741_gshared)(__this, ___result0, method)
