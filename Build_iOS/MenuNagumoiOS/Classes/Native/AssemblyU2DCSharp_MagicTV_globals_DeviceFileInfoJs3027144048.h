﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>
struct Dictionary_2_t4173121995;
// System.Collections.Generic.Dictionary`2<System.String,MagicTV.vo.BundleVO[]>
struct Dictionary_2_t2983310470;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// System.Collections.Generic.List`1<MagicTV.vo.UrlInfoVO>
struct List_1_t3130173080;
// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>>
struct Func_2_t2880153227;
// System.Func`2<MagicTV.vo.MetadataVO,System.Boolean>
struct Func_2_t3914892389;
// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO>>
struct Func_2_t2130883757;
// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.FileVO>>
struct Func_2_t2304244888;
// System.Func`2<MagicTV.vo.BundleVO,MagicTV.vo.BundleVO>
struct Func_2_t3347468641;
// System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO>
struct Func_2_t1654383373;
// System.Func`2<MagicTV.vo.BundleVO,System.Boolean>
struct Func_2_t1839749286;
// System.Func`2<MagicTV.vo.UrlInfoVO,System.Boolean>
struct Func_2_t1358372731;
// System.Func`2<MagicTV.vo.FileVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO>>
struct Func_2_t541505419;
// System.Func`2<MagicTV.vo.FileVO,System.Boolean>
struct Func_2_t250370948;
// System.Func`2<MagicTV.vo.FileVO,MagicTV.vo.FileVO>
struct Func_2_t1708920889;
// System.Func`2<MagicTV.vo.UrlInfoVO,MagicTV.vo.UrlInfoVO>
struct Func_2_t2643561541;

#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceF3788299492.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.DeviceFileInfoJson
struct  DeviceFileInfoJson_t3027144048  : public DeviceFileInfoAbstract_t3788299492
{
public:
	// System.String MagicTV.globals.DeviceFileInfoJson::_localFolderOfJson
	String_t* ____localFolderOfJson_6;
	// System.String MagicTV.globals.DeviceFileInfoJson::_fileName
	String_t* ____fileName_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>> MagicTV.globals.DeviceFileInfoJson::_indexedBundles
	Dictionary_2_t4173121995 * ____indexedBundles_8;
	// System.Collections.Generic.Dictionary`2<System.String,MagicTV.vo.BundleVO[]> MagicTV.globals.DeviceFileInfoJson::_indexedBundlesArray
	Dictionary_2_t2983310470 * ____indexedBundlesArray_9;
	// MagicTV.vo.ResultRevisionVO MagicTV.globals.DeviceFileInfoJson::_currentRevisionResult
	ResultRevisionVO_t2445597391 * ____currentRevisionResult_10;
	// System.Boolean MagicTV.globals.DeviceFileInfoJson::_prepareAllreadyCalled
	bool ____prepareAllreadyCalled_11;
	// System.Collections.Generic.List`1<MagicTV.vo.UrlInfoVO> MagicTV.globals.DeviceFileInfoJson::_urlsToDelete
	List_1_t3130173080 * ____urlsToDelete_12;

public:
	inline static int32_t get_offset_of__localFolderOfJson_6() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____localFolderOfJson_6)); }
	inline String_t* get__localFolderOfJson_6() const { return ____localFolderOfJson_6; }
	inline String_t** get_address_of__localFolderOfJson_6() { return &____localFolderOfJson_6; }
	inline void set__localFolderOfJson_6(String_t* value)
	{
		____localFolderOfJson_6 = value;
		Il2CppCodeGenWriteBarrier(&____localFolderOfJson_6, value);
	}

	inline static int32_t get_offset_of__fileName_7() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____fileName_7)); }
	inline String_t* get__fileName_7() const { return ____fileName_7; }
	inline String_t** get_address_of__fileName_7() { return &____fileName_7; }
	inline void set__fileName_7(String_t* value)
	{
		____fileName_7 = value;
		Il2CppCodeGenWriteBarrier(&____fileName_7, value);
	}

	inline static int32_t get_offset_of__indexedBundles_8() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____indexedBundles_8)); }
	inline Dictionary_2_t4173121995 * get__indexedBundles_8() const { return ____indexedBundles_8; }
	inline Dictionary_2_t4173121995 ** get_address_of__indexedBundles_8() { return &____indexedBundles_8; }
	inline void set__indexedBundles_8(Dictionary_2_t4173121995 * value)
	{
		____indexedBundles_8 = value;
		Il2CppCodeGenWriteBarrier(&____indexedBundles_8, value);
	}

	inline static int32_t get_offset_of__indexedBundlesArray_9() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____indexedBundlesArray_9)); }
	inline Dictionary_2_t2983310470 * get__indexedBundlesArray_9() const { return ____indexedBundlesArray_9; }
	inline Dictionary_2_t2983310470 ** get_address_of__indexedBundlesArray_9() { return &____indexedBundlesArray_9; }
	inline void set__indexedBundlesArray_9(Dictionary_2_t2983310470 * value)
	{
		____indexedBundlesArray_9 = value;
		Il2CppCodeGenWriteBarrier(&____indexedBundlesArray_9, value);
	}

	inline static int32_t get_offset_of__currentRevisionResult_10() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____currentRevisionResult_10)); }
	inline ResultRevisionVO_t2445597391 * get__currentRevisionResult_10() const { return ____currentRevisionResult_10; }
	inline ResultRevisionVO_t2445597391 ** get_address_of__currentRevisionResult_10() { return &____currentRevisionResult_10; }
	inline void set__currentRevisionResult_10(ResultRevisionVO_t2445597391 * value)
	{
		____currentRevisionResult_10 = value;
		Il2CppCodeGenWriteBarrier(&____currentRevisionResult_10, value);
	}

	inline static int32_t get_offset_of__prepareAllreadyCalled_11() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____prepareAllreadyCalled_11)); }
	inline bool get__prepareAllreadyCalled_11() const { return ____prepareAllreadyCalled_11; }
	inline bool* get_address_of__prepareAllreadyCalled_11() { return &____prepareAllreadyCalled_11; }
	inline void set__prepareAllreadyCalled_11(bool value)
	{
		____prepareAllreadyCalled_11 = value;
	}

	inline static int32_t get_offset_of__urlsToDelete_12() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048, ____urlsToDelete_12)); }
	inline List_1_t3130173080 * get__urlsToDelete_12() const { return ____urlsToDelete_12; }
	inline List_1_t3130173080 ** get_address_of__urlsToDelete_12() { return &____urlsToDelete_12; }
	inline void set__urlsToDelete_12(List_1_t3130173080 * value)
	{
		____urlsToDelete_12 = value;
		Il2CppCodeGenWriteBarrier(&____urlsToDelete_12, value);
	}
};

struct DeviceFileInfoJson_t3027144048_StaticFields
{
public:
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache7
	Func_2_t2880153227 * ___U3CU3Ef__amU24cache7_13;
	// System.Func`2<MagicTV.vo.MetadataVO,System.Boolean> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache8
	Func_2_t3914892389 * ___U3CU3Ef__amU24cache8_14;
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO>> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache9
	Func_2_t2130883757 * ___U3CU3Ef__amU24cache9_15;
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.FileVO>> MagicTV.globals.DeviceFileInfoJson::<>f__am$cacheA
	Func_2_t2304244888 * ___U3CU3Ef__amU24cacheA_16;
	// System.Func`2<MagicTV.vo.BundleVO,MagicTV.vo.BundleVO> MagicTV.globals.DeviceFileInfoJson::<>f__am$cacheB
	Func_2_t3347468641 * ___U3CU3Ef__amU24cacheB_17;
	// System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO> MagicTV.globals.DeviceFileInfoJson::<>f__am$cacheC
	Func_2_t1654383373 * ___U3CU3Ef__amU24cacheC_18;
	// System.Func`2<MagicTV.vo.BundleVO,System.Boolean> MagicTV.globals.DeviceFileInfoJson::<>f__am$cacheD
	Func_2_t1839749286 * ___U3CU3Ef__amU24cacheD_19;
	// System.Func`2<MagicTV.vo.UrlInfoVO,System.Boolean> MagicTV.globals.DeviceFileInfoJson::<>f__am$cacheE
	Func_2_t1358372731 * ___U3CU3Ef__amU24cacheE_20;
	// System.Func`2<MagicTV.vo.FileVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO>> MagicTV.globals.DeviceFileInfoJson::<>f__am$cacheF
	Func_2_t541505419 * ___U3CU3Ef__amU24cacheF_21;
	// System.Func`2<MagicTV.vo.FileVO,System.Boolean> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache10
	Func_2_t250370948 * ___U3CU3Ef__amU24cache10_22;
	// System.Func`2<MagicTV.vo.FileVO,MagicTV.vo.FileVO> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache11
	Func_2_t1708920889 * ___U3CU3Ef__amU24cache11_23;
	// System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache12
	Func_2_t1654383373 * ___U3CU3Ef__amU24cache12_24;
	// System.Func`2<MagicTV.vo.FileVO,System.Boolean> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache13
	Func_2_t250370948 * ___U3CU3Ef__amU24cache13_25;
	// System.Func`2<MagicTV.vo.UrlInfoVO,System.Boolean> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache14
	Func_2_t1358372731 * ___U3CU3Ef__amU24cache14_26;
	// System.Func`2<MagicTV.vo.UrlInfoVO,MagicTV.vo.UrlInfoVO> MagicTV.globals.DeviceFileInfoJson::<>f__am$cache15
	Func_2_t2643561541 * ___U3CU3Ef__amU24cache15_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_13() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache7_13)); }
	inline Func_2_t2880153227 * get_U3CU3Ef__amU24cache7_13() const { return ___U3CU3Ef__amU24cache7_13; }
	inline Func_2_t2880153227 ** get_address_of_U3CU3Ef__amU24cache7_13() { return &___U3CU3Ef__amU24cache7_13; }
	inline void set_U3CU3Ef__amU24cache7_13(Func_2_t2880153227 * value)
	{
		___U3CU3Ef__amU24cache7_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_14() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache8_14)); }
	inline Func_2_t3914892389 * get_U3CU3Ef__amU24cache8_14() const { return ___U3CU3Ef__amU24cache8_14; }
	inline Func_2_t3914892389 ** get_address_of_U3CU3Ef__amU24cache8_14() { return &___U3CU3Ef__amU24cache8_14; }
	inline void set_U3CU3Ef__amU24cache8_14(Func_2_t3914892389 * value)
	{
		___U3CU3Ef__amU24cache8_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_15() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache9_15)); }
	inline Func_2_t2130883757 * get_U3CU3Ef__amU24cache9_15() const { return ___U3CU3Ef__amU24cache9_15; }
	inline Func_2_t2130883757 ** get_address_of_U3CU3Ef__amU24cache9_15() { return &___U3CU3Ef__amU24cache9_15; }
	inline void set_U3CU3Ef__amU24cache9_15(Func_2_t2130883757 * value)
	{
		___U3CU3Ef__amU24cache9_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_16() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cacheA_16)); }
	inline Func_2_t2304244888 * get_U3CU3Ef__amU24cacheA_16() const { return ___U3CU3Ef__amU24cacheA_16; }
	inline Func_2_t2304244888 ** get_address_of_U3CU3Ef__amU24cacheA_16() { return &___U3CU3Ef__amU24cacheA_16; }
	inline void set_U3CU3Ef__amU24cacheA_16(Func_2_t2304244888 * value)
	{
		___U3CU3Ef__amU24cacheA_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_17() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cacheB_17)); }
	inline Func_2_t3347468641 * get_U3CU3Ef__amU24cacheB_17() const { return ___U3CU3Ef__amU24cacheB_17; }
	inline Func_2_t3347468641 ** get_address_of_U3CU3Ef__amU24cacheB_17() { return &___U3CU3Ef__amU24cacheB_17; }
	inline void set_U3CU3Ef__amU24cacheB_17(Func_2_t3347468641 * value)
	{
		___U3CU3Ef__amU24cacheB_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_18() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cacheC_18)); }
	inline Func_2_t1654383373 * get_U3CU3Ef__amU24cacheC_18() const { return ___U3CU3Ef__amU24cacheC_18; }
	inline Func_2_t1654383373 ** get_address_of_U3CU3Ef__amU24cacheC_18() { return &___U3CU3Ef__amU24cacheC_18; }
	inline void set_U3CU3Ef__amU24cacheC_18(Func_2_t1654383373 * value)
	{
		___U3CU3Ef__amU24cacheC_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_19() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cacheD_19)); }
	inline Func_2_t1839749286 * get_U3CU3Ef__amU24cacheD_19() const { return ___U3CU3Ef__amU24cacheD_19; }
	inline Func_2_t1839749286 ** get_address_of_U3CU3Ef__amU24cacheD_19() { return &___U3CU3Ef__amU24cacheD_19; }
	inline void set_U3CU3Ef__amU24cacheD_19(Func_2_t1839749286 * value)
	{
		___U3CU3Ef__amU24cacheD_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_20() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cacheE_20)); }
	inline Func_2_t1358372731 * get_U3CU3Ef__amU24cacheE_20() const { return ___U3CU3Ef__amU24cacheE_20; }
	inline Func_2_t1358372731 ** get_address_of_U3CU3Ef__amU24cacheE_20() { return &___U3CU3Ef__amU24cacheE_20; }
	inline void set_U3CU3Ef__amU24cacheE_20(Func_2_t1358372731 * value)
	{
		___U3CU3Ef__amU24cacheE_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_21() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cacheF_21)); }
	inline Func_2_t541505419 * get_U3CU3Ef__amU24cacheF_21() const { return ___U3CU3Ef__amU24cacheF_21; }
	inline Func_2_t541505419 ** get_address_of_U3CU3Ef__amU24cacheF_21() { return &___U3CU3Ef__amU24cacheF_21; }
	inline void set_U3CU3Ef__amU24cacheF_21(Func_2_t541505419 * value)
	{
		___U3CU3Ef__amU24cacheF_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_22() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache10_22)); }
	inline Func_2_t250370948 * get_U3CU3Ef__amU24cache10_22() const { return ___U3CU3Ef__amU24cache10_22; }
	inline Func_2_t250370948 ** get_address_of_U3CU3Ef__amU24cache10_22() { return &___U3CU3Ef__amU24cache10_22; }
	inline void set_U3CU3Ef__amU24cache10_22(Func_2_t250370948 * value)
	{
		___U3CU3Ef__amU24cache10_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_23() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache11_23)); }
	inline Func_2_t1708920889 * get_U3CU3Ef__amU24cache11_23() const { return ___U3CU3Ef__amU24cache11_23; }
	inline Func_2_t1708920889 ** get_address_of_U3CU3Ef__amU24cache11_23() { return &___U3CU3Ef__amU24cache11_23; }
	inline void set_U3CU3Ef__amU24cache11_23(Func_2_t1708920889 * value)
	{
		___U3CU3Ef__amU24cache11_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_24() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache12_24)); }
	inline Func_2_t1654383373 * get_U3CU3Ef__amU24cache12_24() const { return ___U3CU3Ef__amU24cache12_24; }
	inline Func_2_t1654383373 ** get_address_of_U3CU3Ef__amU24cache12_24() { return &___U3CU3Ef__amU24cache12_24; }
	inline void set_U3CU3Ef__amU24cache12_24(Func_2_t1654383373 * value)
	{
		___U3CU3Ef__amU24cache12_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_25() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache13_25)); }
	inline Func_2_t250370948 * get_U3CU3Ef__amU24cache13_25() const { return ___U3CU3Ef__amU24cache13_25; }
	inline Func_2_t250370948 ** get_address_of_U3CU3Ef__amU24cache13_25() { return &___U3CU3Ef__amU24cache13_25; }
	inline void set_U3CU3Ef__amU24cache13_25(Func_2_t250370948 * value)
	{
		___U3CU3Ef__amU24cache13_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_26() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache14_26)); }
	inline Func_2_t1358372731 * get_U3CU3Ef__amU24cache14_26() const { return ___U3CU3Ef__amU24cache14_26; }
	inline Func_2_t1358372731 ** get_address_of_U3CU3Ef__amU24cache14_26() { return &___U3CU3Ef__amU24cache14_26; }
	inline void set_U3CU3Ef__amU24cache14_26(Func_2_t1358372731 * value)
	{
		___U3CU3Ef__amU24cache14_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_27() { return static_cast<int32_t>(offsetof(DeviceFileInfoJson_t3027144048_StaticFields, ___U3CU3Ef__amU24cache15_27)); }
	inline Func_2_t2643561541 * get_U3CU3Ef__amU24cache15_27() const { return ___U3CU3Ef__amU24cache15_27; }
	inline Func_2_t2643561541 ** get_address_of_U3CU3Ef__amU24cache15_27() { return &___U3CU3Ef__amU24cache15_27; }
	inline void set_U3CU3Ef__amU24cache15_27(Func_2_t2643561541 * value)
	{
		___U3CU3Ef__amU24cache15_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
