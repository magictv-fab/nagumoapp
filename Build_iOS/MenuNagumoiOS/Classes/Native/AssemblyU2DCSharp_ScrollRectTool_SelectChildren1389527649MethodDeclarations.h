﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollRectTool/SelectChildren
struct SelectChildren_t1389527649;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ScrollRectTool/SelectChildren::.ctor(System.Object,System.IntPtr)
extern "C"  void SelectChildren__ctor_m1507960248 (SelectChildren_t1389527649 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool/SelectChildren::Invoke(System.String)
extern "C"  void SelectChildren_Invoke_m407159856 (SelectChildren_t1389527649 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ScrollRectTool/SelectChildren::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SelectChildren_BeginInvoke_m3392434749 (SelectChildren_t1389527649 * __this, String_t* ___name0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectTool/SelectChildren::EndInvoke(System.IAsyncResult)
extern "C"  void SelectChildren_EndInvoke_m4036801480 (SelectChildren_t1389527649 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
