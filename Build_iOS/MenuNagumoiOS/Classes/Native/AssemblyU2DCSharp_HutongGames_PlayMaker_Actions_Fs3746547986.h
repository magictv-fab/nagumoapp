﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmEventOptions
struct  FsmEventOptions_t3746547986  : public FsmStateAction_t2366529033
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.Actions.FsmEventOptions::sendToFsmComponent
	PlayMakerFSM_t3799847376 * ___sendToFsmComponent_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FsmEventOptions::sendToGameObject
	FsmGameObject_t1697147867 * ___sendToGameObject_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmEventOptions::fsmName
	FsmString_t952858651 * ___fsmName_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FsmEventOptions::sendToChildren
	FsmBool_t1075959796 * ___sendToChildren_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FsmEventOptions::broadcastToAll
	FsmBool_t1075959796 * ___broadcastToAll_13;

public:
	inline static int32_t get_offset_of_sendToFsmComponent_9() { return static_cast<int32_t>(offsetof(FsmEventOptions_t3746547986, ___sendToFsmComponent_9)); }
	inline PlayMakerFSM_t3799847376 * get_sendToFsmComponent_9() const { return ___sendToFsmComponent_9; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_sendToFsmComponent_9() { return &___sendToFsmComponent_9; }
	inline void set_sendToFsmComponent_9(PlayMakerFSM_t3799847376 * value)
	{
		___sendToFsmComponent_9 = value;
		Il2CppCodeGenWriteBarrier(&___sendToFsmComponent_9, value);
	}

	inline static int32_t get_offset_of_sendToGameObject_10() { return static_cast<int32_t>(offsetof(FsmEventOptions_t3746547986, ___sendToGameObject_10)); }
	inline FsmGameObject_t1697147867 * get_sendToGameObject_10() const { return ___sendToGameObject_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_sendToGameObject_10() { return &___sendToGameObject_10; }
	inline void set_sendToGameObject_10(FsmGameObject_t1697147867 * value)
	{
		___sendToGameObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___sendToGameObject_10, value);
	}

	inline static int32_t get_offset_of_fsmName_11() { return static_cast<int32_t>(offsetof(FsmEventOptions_t3746547986, ___fsmName_11)); }
	inline FsmString_t952858651 * get_fsmName_11() const { return ___fsmName_11; }
	inline FsmString_t952858651 ** get_address_of_fsmName_11() { return &___fsmName_11; }
	inline void set_fsmName_11(FsmString_t952858651 * value)
	{
		___fsmName_11 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_11, value);
	}

	inline static int32_t get_offset_of_sendToChildren_12() { return static_cast<int32_t>(offsetof(FsmEventOptions_t3746547986, ___sendToChildren_12)); }
	inline FsmBool_t1075959796 * get_sendToChildren_12() const { return ___sendToChildren_12; }
	inline FsmBool_t1075959796 ** get_address_of_sendToChildren_12() { return &___sendToChildren_12; }
	inline void set_sendToChildren_12(FsmBool_t1075959796 * value)
	{
		___sendToChildren_12 = value;
		Il2CppCodeGenWriteBarrier(&___sendToChildren_12, value);
	}

	inline static int32_t get_offset_of_broadcastToAll_13() { return static_cast<int32_t>(offsetof(FsmEventOptions_t3746547986, ___broadcastToAll_13)); }
	inline FsmBool_t1075959796 * get_broadcastToAll_13() const { return ___broadcastToAll_13; }
	inline FsmBool_t1075959796 ** get_address_of_broadcastToAll_13() { return &___broadcastToAll_13; }
	inline void set_broadcastToAll_13(FsmBool_t1075959796 * value)
	{
		___broadcastToAll_13 = value;
		Il2CppCodeGenWriteBarrier(&___broadcastToAll_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
