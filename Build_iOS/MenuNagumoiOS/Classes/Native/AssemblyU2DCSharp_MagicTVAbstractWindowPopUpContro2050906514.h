﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVAbstractWindowPopUpControllerScript
struct  MagicTVAbstractWindowPopUpControllerScript_t2050906514  : public GenericWindowControllerScript_t248075822
{
public:
	// System.Action MagicTVAbstractWindowPopUpControllerScript::OnClickOk
	Action_t3771233898 * ___OnClickOk_4;
	// UnityEngine.UI.Text MagicTVAbstractWindowPopUpControllerScript::LabelTitle
	Text_t9039225 * ___LabelTitle_5;
	// UnityEngine.UI.Text MagicTVAbstractWindowPopUpControllerScript::LabelMessage
	Text_t9039225 * ___LabelMessage_6;
	// UnityEngine.UI.Text MagicTVAbstractWindowPopUpControllerScript::LabelButtonOk
	Text_t9039225 * ___LabelButtonOk_7;
	// System.Boolean MagicTVAbstractWindowPopUpControllerScript::OkButton
	bool ___OkButton_8;

public:
	inline static int32_t get_offset_of_OnClickOk_4() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowPopUpControllerScript_t2050906514, ___OnClickOk_4)); }
	inline Action_t3771233898 * get_OnClickOk_4() const { return ___OnClickOk_4; }
	inline Action_t3771233898 ** get_address_of_OnClickOk_4() { return &___OnClickOk_4; }
	inline void set_OnClickOk_4(Action_t3771233898 * value)
	{
		___OnClickOk_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnClickOk_4, value);
	}

	inline static int32_t get_offset_of_LabelTitle_5() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowPopUpControllerScript_t2050906514, ___LabelTitle_5)); }
	inline Text_t9039225 * get_LabelTitle_5() const { return ___LabelTitle_5; }
	inline Text_t9039225 ** get_address_of_LabelTitle_5() { return &___LabelTitle_5; }
	inline void set_LabelTitle_5(Text_t9039225 * value)
	{
		___LabelTitle_5 = value;
		Il2CppCodeGenWriteBarrier(&___LabelTitle_5, value);
	}

	inline static int32_t get_offset_of_LabelMessage_6() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowPopUpControllerScript_t2050906514, ___LabelMessage_6)); }
	inline Text_t9039225 * get_LabelMessage_6() const { return ___LabelMessage_6; }
	inline Text_t9039225 ** get_address_of_LabelMessage_6() { return &___LabelMessage_6; }
	inline void set_LabelMessage_6(Text_t9039225 * value)
	{
		___LabelMessage_6 = value;
		Il2CppCodeGenWriteBarrier(&___LabelMessage_6, value);
	}

	inline static int32_t get_offset_of_LabelButtonOk_7() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowPopUpControllerScript_t2050906514, ___LabelButtonOk_7)); }
	inline Text_t9039225 * get_LabelButtonOk_7() const { return ___LabelButtonOk_7; }
	inline Text_t9039225 ** get_address_of_LabelButtonOk_7() { return &___LabelButtonOk_7; }
	inline void set_LabelButtonOk_7(Text_t9039225 * value)
	{
		___LabelButtonOk_7 = value;
		Il2CppCodeGenWriteBarrier(&___LabelButtonOk_7, value);
	}

	inline static int32_t get_offset_of_OkButton_8() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowPopUpControllerScript_t2050906514, ___OkButton_8)); }
	inline bool get_OkButton_8() const { return ___OkButton_8; }
	inline bool* get_address_of_OkButton_8() { return &___OkButton_8; }
	inline void set_OkButton_8(bool value)
	{
		___OkButton_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
