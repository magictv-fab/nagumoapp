﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1561764144;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Base1865109560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage
struct  DiskArchiveStorage_t2562884100  : public BaseArchiveStorage_t1865109560
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::temporaryStream_
	Stream_t1561764144 * ___temporaryStream__1;
	// System.String ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::fileName_
	String_t* ___fileName__2;
	// System.String ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::temporaryName_
	String_t* ___temporaryName__3;

public:
	inline static int32_t get_offset_of_temporaryStream__1() { return static_cast<int32_t>(offsetof(DiskArchiveStorage_t2562884100, ___temporaryStream__1)); }
	inline Stream_t1561764144 * get_temporaryStream__1() const { return ___temporaryStream__1; }
	inline Stream_t1561764144 ** get_address_of_temporaryStream__1() { return &___temporaryStream__1; }
	inline void set_temporaryStream__1(Stream_t1561764144 * value)
	{
		___temporaryStream__1 = value;
		Il2CppCodeGenWriteBarrier(&___temporaryStream__1, value);
	}

	inline static int32_t get_offset_of_fileName__2() { return static_cast<int32_t>(offsetof(DiskArchiveStorage_t2562884100, ___fileName__2)); }
	inline String_t* get_fileName__2() const { return ___fileName__2; }
	inline String_t** get_address_of_fileName__2() { return &___fileName__2; }
	inline void set_fileName__2(String_t* value)
	{
		___fileName__2 = value;
		Il2CppCodeGenWriteBarrier(&___fileName__2, value);
	}

	inline static int32_t get_offset_of_temporaryName__3() { return static_cast<int32_t>(offsetof(DiskArchiveStorage_t2562884100, ___temporaryName__3)); }
	inline String_t* get_temporaryName__3() const { return ___temporaryName__3; }
	inline String_t** get_address_of_temporaryName__3() { return &___temporaryName__3; }
	inline void set_temporaryName__3(String_t* value)
	{
		___temporaryName__3 = value;
		Il2CppCodeGenWriteBarrier(&___temporaryName__3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
