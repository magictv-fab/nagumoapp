﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.MetadataVO::.ctor()
extern "C"  void MetadataVO__ctor_m860598637 (MetadataVO_t2511256998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
