﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.Aztec.Internal.Decoder/Table>
struct DefaultComparer_t3750222476;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.Aztec.Internal.Decoder/Table>::.ctor()
extern "C"  void DefaultComparer__ctor_m461916637_gshared (DefaultComparer_t3750222476 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m461916637(__this, method) ((  void (*) (DefaultComparer_t3750222476 *, const MethodInfo*))DefaultComparer__ctor_m461916637_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.Aztec.Internal.Decoder/Table>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3704617334_gshared (DefaultComparer_t3750222476 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3704617334(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3750222476 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3704617334_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.Aztec.Internal.Decoder/Table>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3662054130_gshared (DefaultComparer_t3750222476 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3662054130(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3750222476 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3662054130_gshared)(__this, ___x0, ___y1, method)
