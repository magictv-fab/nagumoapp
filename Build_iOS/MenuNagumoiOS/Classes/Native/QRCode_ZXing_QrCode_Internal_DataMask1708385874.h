﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.QrCode.Internal.DataMask[]
struct DataMaskU5BU5D_t404352039;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DataMask
struct  DataMask_t1708385874  : public Il2CppObject
{
public:

public:
};

struct DataMask_t1708385874_StaticFields
{
public:
	// ZXing.QrCode.Internal.DataMask[] ZXing.QrCode.Internal.DataMask::DATA_MASKS
	DataMaskU5BU5D_t404352039* ___DATA_MASKS_0;

public:
	inline static int32_t get_offset_of_DATA_MASKS_0() { return static_cast<int32_t>(offsetof(DataMask_t1708385874_StaticFields, ___DATA_MASKS_0)); }
	inline DataMaskU5BU5D_t404352039* get_DATA_MASKS_0() const { return ___DATA_MASKS_0; }
	inline DataMaskU5BU5D_t404352039** get_address_of_DATA_MASKS_0() { return &___DATA_MASKS_0; }
	inline void set_DATA_MASKS_0(DataMaskU5BU5D_t404352039* value)
	{
		___DATA_MASKS_0 = value;
		Il2CppCodeGenWriteBarrier(&___DATA_MASKS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
