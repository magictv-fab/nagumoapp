﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler
struct OnScreenOrientationEventHandler_t449305768;
// ARM.gui.utils.ScreenOrientation
struct ScreenOrientation_t3424593542;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.gui.utils.ScreenOrientation
struct  ScreenOrientation_t3424593542  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector2 ARM.gui.utils.ScreenOrientation::_lastSize
	Vector2_t4282066565  ____lastSize_2;
	// ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler ARM.gui.utils.ScreenOrientation::OnChangeScreenOrientationType
	OnScreenOrientationEventHandler_t449305768 * ___OnChangeScreenOrientationType_4;

public:
	inline static int32_t get_offset_of__lastSize_2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542, ____lastSize_2)); }
	inline Vector2_t4282066565  get__lastSize_2() const { return ____lastSize_2; }
	inline Vector2_t4282066565 * get_address_of__lastSize_2() { return &____lastSize_2; }
	inline void set__lastSize_2(Vector2_t4282066565  value)
	{
		____lastSize_2 = value;
	}

	inline static int32_t get_offset_of_OnChangeScreenOrientationType_4() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542, ___OnChangeScreenOrientationType_4)); }
	inline OnScreenOrientationEventHandler_t449305768 * get_OnChangeScreenOrientationType_4() const { return ___OnChangeScreenOrientationType_4; }
	inline OnScreenOrientationEventHandler_t449305768 ** get_address_of_OnChangeScreenOrientationType_4() { return &___OnChangeScreenOrientationType_4; }
	inline void set_OnChangeScreenOrientationType_4(OnScreenOrientationEventHandler_t449305768 * value)
	{
		___OnChangeScreenOrientationType_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnChangeScreenOrientationType_4, value);
	}
};

struct ScreenOrientation_t3424593542_StaticFields
{
public:
	// System.Boolean ARM.gui.utils.ScreenOrientation::_destroying
	bool ____destroying_3;
	// ARM.gui.utils.ScreenOrientation ARM.gui.utils.ScreenOrientation::_instanceScreenOrientation
	ScreenOrientation_t3424593542 * ____instanceScreenOrientation_5;
	// System.Int32 ARM.gui.utils.ScreenOrientation::mSizeFrame
	int32_t ___mSizeFrame_6;
	// System.Reflection.MethodInfo ARM.gui.utils.ScreenOrientation::s_GetSizeOfMainGameView
	MethodInfo_t * ___s_GetSizeOfMainGameView_7;
	// UnityEngine.Vector2 ARM.gui.utils.ScreenOrientation::mGameSize
	Vector2_t4282066565  ___mGameSize_8;

public:
	inline static int32_t get_offset_of__destroying_3() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542_StaticFields, ____destroying_3)); }
	inline bool get__destroying_3() const { return ____destroying_3; }
	inline bool* get_address_of__destroying_3() { return &____destroying_3; }
	inline void set__destroying_3(bool value)
	{
		____destroying_3 = value;
	}

	inline static int32_t get_offset_of__instanceScreenOrientation_5() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542_StaticFields, ____instanceScreenOrientation_5)); }
	inline ScreenOrientation_t3424593542 * get__instanceScreenOrientation_5() const { return ____instanceScreenOrientation_5; }
	inline ScreenOrientation_t3424593542 ** get_address_of__instanceScreenOrientation_5() { return &____instanceScreenOrientation_5; }
	inline void set__instanceScreenOrientation_5(ScreenOrientation_t3424593542 * value)
	{
		____instanceScreenOrientation_5 = value;
		Il2CppCodeGenWriteBarrier(&____instanceScreenOrientation_5, value);
	}

	inline static int32_t get_offset_of_mSizeFrame_6() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542_StaticFields, ___mSizeFrame_6)); }
	inline int32_t get_mSizeFrame_6() const { return ___mSizeFrame_6; }
	inline int32_t* get_address_of_mSizeFrame_6() { return &___mSizeFrame_6; }
	inline void set_mSizeFrame_6(int32_t value)
	{
		___mSizeFrame_6 = value;
	}

	inline static int32_t get_offset_of_s_GetSizeOfMainGameView_7() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542_StaticFields, ___s_GetSizeOfMainGameView_7)); }
	inline MethodInfo_t * get_s_GetSizeOfMainGameView_7() const { return ___s_GetSizeOfMainGameView_7; }
	inline MethodInfo_t ** get_address_of_s_GetSizeOfMainGameView_7() { return &___s_GetSizeOfMainGameView_7; }
	inline void set_s_GetSizeOfMainGameView_7(MethodInfo_t * value)
	{
		___s_GetSizeOfMainGameView_7 = value;
		Il2CppCodeGenWriteBarrier(&___s_GetSizeOfMainGameView_7, value);
	}

	inline static int32_t get_offset_of_mGameSize_8() { return static_cast<int32_t>(offsetof(ScreenOrientation_t3424593542_StaticFields, ___mGameSize_8)); }
	inline Vector2_t4282066565  get_mGameSize_8() const { return ___mGameSize_8; }
	inline Vector2_t4282066565 * get_address_of_mGameSize_8() { return &___mGameSize_8; }
	inline void set_mGameSize_8(Vector2_t4282066565  value)
	{
		___mGameSize_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
