﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties
struct NetworkGetMessagePlayerProperties_t906059777;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::.ctor()
extern "C"  void NetworkGetMessagePlayerProperties__ctor_m3920944725 (NetworkGetMessagePlayerProperties_t906059777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::Reset()
extern "C"  void NetworkGetMessagePlayerProperties_Reset_m1567377666 (NetworkGetMessagePlayerProperties_t906059777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::OnEnter()
extern "C"  void NetworkGetMessagePlayerProperties_OnEnter_m733290732 (NetworkGetMessagePlayerProperties_t906059777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::doGetOnPLayerConnectedProperties()
extern "C"  void NetworkGetMessagePlayerProperties_doGetOnPLayerConnectedProperties_m1740614816 (NetworkGetMessagePlayerProperties_t906059777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
