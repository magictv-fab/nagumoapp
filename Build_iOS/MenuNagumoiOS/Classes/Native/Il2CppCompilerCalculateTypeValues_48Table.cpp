﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DragMe2055054860.h"
#include "AssemblyU2DCSharp_DragPanel1678257424.h"
#include "AssemblyU2DCSharp_DropDownRemoveLastItem1997694494.h"
#include "AssemblyU2DCSharp_DropMe2055480583.h"
#include "AssemblyU2DCSharp_FacebookMain4095382975.h"
#include "AssemblyU2DCSharp_FacebookMain_U3CISendJsonEnviaSeC867600582.h"
#include "AssemblyU2DCSharp_FacebookData4095115184.h"
#include "AssemblyU2DCSharp_FavoritosManager4195735424.h"
#include "AssemblyU2DCSharp_FavoritosManager_U3CExcluirFavor3440772553.h"
#include "AssemblyU2DCSharp_FavoritosManager_U3CIFavoritarU32672104493.h"
#include "AssemblyU2DCSharp_FavoritosManager_U3CISendStatusU2468455590.h"
#include "AssemblyU2DCSharp_FavoritosManager_U3CILoadPublish1296913502.h"
#include "AssemblyU2DCSharp_FavoritosManager_U3CILoadImgsU3Ec655541284.h"
#include "AssemblyU2DCSharp_FavoritosManager_U3CLoadLoadedsU3265761870.h"
#include "AssemblyU2DCSharp_FrontCam3116790022.h"
#include "AssemblyU2DCSharp_FrontCam_U3CStartU3Ec__Iterator52620435555.h"
#include "AssemblyU2DCSharp_FrontCam_U3CopenCameraU3Ec__Iter3278153783.h"
#include "AssemblyU2DCSharp_FrontCam_U3CGoLevelU3Ec__Iterator533082815.h"
#include "AssemblyU2DCSharp_FrontCam_U3CRemoveHUDU3Ec__Itera2317328791.h"
#include "AssemblyU2DCSharp_PointsAngle2793040272.h"
#include "AssemblyU2DCSharp_GameMain2590236907.h"
#include "AssemblyU2DCSharp_GameMain_U3CcorrigeVoltaRoletaU3E946778943.h"
#include "AssemblyU2DCSharp_GameMain_U3CStartU3Ec__Iterator52168360362.h"
#include "AssemblyU2DCSharp_GerarCupom1397758263.h"
#include "AssemblyU2DCSharp_GerarCupom_U3CIGerarU3Ec__Iterat2374326817.h"
#include "AssemblyU2DCSharp_GetInfoMenu3923496067.h"
#include "AssemblyU2DCSharp_GetInfoMenu_U3CILoadImgU3Ec__Iter529063718.h"
#include "AssemblyU2DCSharp_GetUIEvents3946912259.h"
#include "AssemblyU2DCSharp_HidePlugin2251332405.h"
#include "AssemblyU2DCSharp_ImagemScroll3817704447.h"
#include "AssemblyU2DCSharp_ItemMoveDrag687865080.h"
#include "AssemblyU2DCSharp_LightsControl2444629088.h"
#include "AssemblyU2DCSharp_LightsControl_U3CITurnU3Ec__Iter1739934332.h"
#include "AssemblyU2DCSharp_LightsControl_U3CIBlinkAllU3Ec__It13658789.h"
#include "AssemblyU2DCSharp_LoadLevelName284939721.h"
#include "AssemblyU2DCSharp_LoadLevelName_U3CGoLevelU3Ec__Ite233841331.h"
#include "AssemblyU2DCSharp_LoadLevelName_U3CGoLevelAdditive2168713120.h"
#include "AssemblyU2DCSharp_LoaderPublishAnim3439243661.h"
#include "AssemblyU2DCSharp_Login73596745.h"
#include "AssemblyU2DCSharp_Login_U3CISendJsonEsqueceuSenhaU2793933217.h"
#include "AssemblyU2DCSharp_Login_U3CILoginU3Ec__Iterator633961520294.h"
#include "AssemblyU2DCSharp_Login_U3CILoadImgProfileU3Ec__It1069902512.h"
#include "AssemblyU2DCSharp_Login_U3CIEsqueceuSenhaU3Ec__Ite2577917268.h"
#include "AssemblyU2DCSharp_Login_U3CGoLevelU3Ec__Iterator662001881401.h"
#include "AssemblyU2DCSharp_TesteApresentacoes874390990.h"
#include "AssemblyU2DCSharp_RelativePositionOfPoint3585775620.h"
#include "AssemblyU2DCSharp_MenuNagumo1404218786.h"
#include "AssemblyU2DCSharp_MeuPedidoAcougue2271273197.h"
#include "AssemblyU2DCSharp_MeuPedidoAcougue_U3CICheckPedidoU642390674.h"
#include "AssemblyU2DCSharp_MeuPedidoAcougue_U3CIConfirmarRe1931009611.h"
#include "AssemblyU2DCSharp_NativeImagePicker1077436690.h"
#include "AssemblyU2DCSharp_NativeImagePicker_NativeImagePic3828351720.h"
#include "AssemblyU2DCSharp_NativeImagePicker_CallbackImagePi554520473.h"
#include "AssemblyU2DCSharp_OfertasManager821152011.h"
#include "AssemblyU2DCSharp_OfertasManager_U3CISendStatusU3E1960186977.h"
#include "AssemblyU2DCSharp_OfertasManager_U3CILoadOfertasU32130003369.h"
#include "AssemblyU2DCSharp_OfertasManager_U3CILoadImgsU3Ec_1691882790.h"
#include "AssemblyU2DCSharp_OfertasManager_U3CLoadLoadedsU3E2757493264.h"
#include "AssemblyU2DCSharp_PanelManager2965005609.h"
#include "AssemblyU2DCSharp_PanelManager_U3CDisablePanelDele1466402221.h"
#include "AssemblyU2DCSharp_PedidoAcougue9638794.h"
#include "AssemblyU2DCSharp_PedidoAcougue_U3CISendStatusU3Ec3429446286.h"
#include "AssemblyU2DCSharp_PedidoAcougue_U3CILoadCarnesU3Ec1280841667.h"
#include "AssemblyU2DCSharp_PedidoAcougue_U3CILoadImgsU3Ec__2033076116.h"
#include "AssemblyU2DCSharp_PedidoAcougue_U3CLoadLoadedsU3Ec4226752574.h"
#include "AssemblyU2DCSharp_PedidoValues2123607271.h"
#include "AssemblyU2DCSharp_PedidosRevisao3346969709.h"
#include "AssemblyU2DCSharp_PedidosRevisao_U3CIEnviarPedidoU3764597359.h"
#include "AssemblyU2DCSharp_Permissions2801572964.h"
#include "AssemblyU2DCSharp_PoliticasPrivacidade2368006066.h"
#include "AssemblyU2DCSharp_PopupAtualizarCadastro1794233116.h"
#include "AssemblyU2DCSharp_PopupManager2711269761.h"
#include "AssemblyU2DCSharp_PopupSugestaoOfertas1283182119.h"
#include "AssemblyU2DCSharp_PublishLink1052985737.h"
#include "AssemblyU2DCSharp_PublishLink_U3CcheckInternetConn3649015968.h"
#include "AssemblyU2DCSharp_PublishManager4010203070.h"
#include "AssemblyU2DCSharp_PublishManager_U3CIFavoritarU3Ec_983049004.h"
#include "AssemblyU2DCSharp_PublishManager_U3CISendStatusU3E1647341033.h"
#include "AssemblyU2DCSharp_PublishManager_U3CExcluirFavorit1974763023.h"
#include "AssemblyU2DCSharp_PublishManager_U3CILoadPublishU31612164062.h"
#include "AssemblyU2DCSharp_PublishManager_U3CILoadPublishUp2042484936.h"
#include "AssemblyU2DCSharp_PublishManager_U3CILoadImgsU3Ec_1709434345.h"
#include "AssemblyU2DCSharp_ResizePanel788198800.h"
#include "AssemblyU2DCSharp_RoletaPointAngle1105688326.h"
#include "AssemblyU2DCSharp_RoletaGame108100629.h"
#include "AssemblyU2DCSharp_RoletaGame_U3CShowPopupU3Ec__Ite1326136970.h"
#include "AssemblyU2DCSharp_Rotation24343454.h"
#include "AssemblyU2DCSharp_ScrollDetailTexture3150826909.h"
#include "AssemblyU2DCSharp_SendJson1311805616.h"
#include "AssemblyU2DCSharp_SendJson_U3CISendJsonU3Ec__Iterat281221434.h"
#include "AssemblyU2DCSharp_ServerControl2725829754.h"
#include "AssemblyU2DCSharp_ServerControl_OnPlayDelegate3097072301.h"
#include "AssemblyU2DCSharp_ServerControl_U3CIPlayU3Ec__Iter3776050517.h"
#include "AssemblyU2DCSharp_ServerControl_U3CIPlayRoletaU3Ec_361832729.h"
#include "AssemblyU2DCSharp_ServerControl_U3CConectandoLetrei539930572.h"
#include "AssemblyU2DCSharp_JsonPlay2384567388.h"
#include "AssemblyU2DCSharp_JsonPlayRequest2877984403.h"
#include "AssemblyU2DCSharp_ShowSliderValue1108427187.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (DragMe_t2055054860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4800[3] = 
{
	DragMe_t2055054860::get_offset_of_dragOnSurfaces_2(),
	DragMe_t2055054860::get_offset_of_m_DraggingIcons_3(),
	DragMe_t2055054860::get_offset_of_m_DraggingPlanes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (DragPanel_t1678257424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4801[4] = 
{
	DragPanel_t1678257424::get_offset_of_originalLocalPointerPosition_2(),
	DragPanel_t1678257424::get_offset_of_originalPanelLocalPosition_3(),
	DragPanel_t1678257424::get_offset_of_panelRectTransform_4(),
	DragPanel_t1678257424::get_offset_of_parentRectTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (DropDownRemoveLastItem_t1997694494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (DropMe_t2055480583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4803[4] = 
{
	DropMe_t2055480583::get_offset_of_containerImage_2(),
	DropMe_t2055480583::get_offset_of_receivingImage_3(),
	DropMe_t2055480583::get_offset_of_normalColor_4(),
	DropMe_t2055480583::get_offset_of_highlightColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (FacebookMain_t4095382975), -1, sizeof(FacebookMain_t4095382975_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4804[8] = 
{
	FacebookMain_t4095382975_StaticFields::get_offset_of_facebookData_2(),
	FacebookMain_t4095382975_StaticFields::get_offset_of_instance_3(),
	FacebookMain_t4095382975::get_offset_of_sharerInitTexture_4(),
	FacebookMain_t4095382975::get_offset_of_linkOferta_5(),
	FacebookMain_t4095382975::get_offset_of_linkImg_6(),
	FacebookMain_t4095382975::get_offset_of_title_7(),
	FacebookMain_t4095382975::get_offset_of_info_8(),
	FacebookMain_t4095382975_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4805[6] = 
{
	U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582::get_offset_of_U3CjsonU3E__0_0(),
	U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582::get_offset_of_U3CheadersU3E__1_1(),
	U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582::get_offset_of_U3CpDataU3E__2_2(),
	U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582::get_offset_of_U3CwwwU3E__3_3(),
	U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582::get_offset_of_U24PC_4(),
	U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582::get_offset_of_U24current_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (FacebookData_t4095115184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4806[5] = 
{
	FacebookData_t4095115184::get_offset_of_name_0(),
	FacebookData_t4095115184::get_offset_of_id_1(),
	FacebookData_t4095115184::get_offset_of_email_2(),
	FacebookData_t4095115184::get_offset_of_birthday_3(),
	FacebookData_t4095115184::get_offset_of_gender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (FavoritosManager_t4195735424), -1, sizeof(FavoritosManager_t4195735424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4807[17] = 
{
	FavoritosManager_t4195735424_StaticFields::get_offset_of_publicationsData_2(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_instance_3(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_titleLst_4(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_valueLst_5(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_infoLst_6(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_idLst_7(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_imgLst_8(),
	FavoritosManager_t4195735424::get_offset_of_loadingObj_9(),
	FavoritosManager_t4195735424::get_offset_of_popup_10(),
	FavoritosManager_t4195735424::get_offset_of_itemPrefab_11(),
	FavoritosManager_t4195735424::get_offset_of_containerItens_12(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_itensList_13(),
	FavoritosManager_t4195735424::get_offset_of_scrollRect_14(),
	FavoritosManager_t4195735424::get_offset_of_ofertaCounter_15(),
	FavoritosManager_t4195735424::get_offset_of_currentItem_16(),
	FavoritosManager_t4195735424::get_offset_of_selectedItem_17(),
	FavoritosManager_t4195735424_StaticFields::get_offset_of_currentJsonTxt_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (U3CExcluirFavoritoU3Ec__Iterator50_t3440772553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4808[8] = 
{
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_id_0(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U3CjsonU3E__0_1(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U3CheadersU3E__1_2(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U3CpDataU3E__2_3(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U3CwwwU3E__3_4(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U24PC_5(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U24current_6(),
	U3CExcluirFavoritoU3Ec__Iterator50_t3440772553::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (U3CIFavoritarU3Ec__Iterator51_t2672104493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4809[8] = 
{
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_id_0(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U3CjsonU3E__0_1(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U3CheadersU3E__1_2(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U3CpDataU3E__2_3(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U3CwwwU3E__3_4(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U24PC_5(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U24current_6(),
	U3CIFavoritarU3Ec__Iterator51_t2672104493::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (U3CISendStatusU3Ec__Iterator52_t2468455590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4810[10] = 
{
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_id_0(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_status_1(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U3CjsonU3E__0_2(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U3CheadersU3E__1_3(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U3CpDataU3E__2_4(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U3CwwwU3E__3_5(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U24PC_6(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U24current_7(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U3CU24U3Eid_8(),
	U3CISendStatusU3Ec__Iterator52_t2468455590::get_offset_of_U3CU24U3Estatus_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { sizeof (U3CILoadPublishU3Ec__Iterator53_t1296913502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4811[13] = 
{
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CU24s_309U3E__4_4(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CobjU3E__5_5(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CmessageU3E__6_6(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CU24s_310U3E__7_7(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CU24s_311U3E__8_8(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CpublicationDataU3E__9_9(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U24PC_10(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U24current_11(),
	U3CILoadPublishU3Ec__Iterator53_t1296913502::get_offset_of_U3CU3Ef__this_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (U3CILoadImgsU3Ec__Iterator54_t655541284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4812[8] = 
{
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U3CU24s_312U3E__0_0(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U3CU24s_313U3E__1_1(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U3CpublicationDataU3E__2_2(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U3CsptU3E__4_4(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U24PC_5(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U24current_6(),
	U3CILoadImgsU3Ec__Iterator54_t655541284::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (U3CLoadLoadedsU3Ec__Iterator55_t3265761870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4813[4] = 
{
	U3CLoadLoadedsU3Ec__Iterator55_t3265761870::get_offset_of_U3CiU3E__0_0(),
	U3CLoadLoadedsU3Ec__Iterator55_t3265761870::get_offset_of_U24PC_1(),
	U3CLoadLoadedsU3Ec__Iterator55_t3265761870::get_offset_of_U24current_2(),
	U3CLoadLoadedsU3Ec__Iterator55_t3265761870::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (FrontCam_t3116790022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4814[7] = 
{
	FrontCam_t3116790022::get_offset_of_mCamera_2(),
	FrontCam_t3116790022::get_offset_of_bias_3(),
	FrontCam_t3116790022::get_offset_of_frase_4(),
	FrontCam_t3116790022::get_offset_of_btnCam_5(),
	FrontCam_t3116790022::get_offset_of_btnFechar_6(),
	FrontCam_t3116790022::get_offset_of_back_7(),
	FrontCam_t3116790022::get_offset_of_devices_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (U3CStartU3Ec__Iterator56_t2620435555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4815[3] = 
{
	U3CStartU3Ec__Iterator56_t2620435555::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator56_t2620435555::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator56_t2620435555::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (U3CopenCameraU3Ec__Iterator57_t3278153783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4816[5] = 
{
	U3CopenCameraU3Ec__Iterator57_t3278153783::get_offset_of_U3CdeviceU3E__0_0(),
	U3CopenCameraU3Ec__Iterator57_t3278153783::get_offset_of_U3CdeviceU3E__1_1(),
	U3CopenCameraU3Ec__Iterator57_t3278153783::get_offset_of_U24PC_2(),
	U3CopenCameraU3Ec__Iterator57_t3278153783::get_offset_of_U24current_3(),
	U3CopenCameraU3Ec__Iterator57_t3278153783::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (U3CGoLevelU3Ec__Iterator58_t533082815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4817[4] = 
{
	U3CGoLevelU3Ec__Iterator58_t533082815::get_offset_of_U3CasyncU3E__0_0(),
	U3CGoLevelU3Ec__Iterator58_t533082815::get_offset_of_U24PC_1(),
	U3CGoLevelU3Ec__Iterator58_t533082815::get_offset_of_U24current_2(),
	U3CGoLevelU3Ec__Iterator58_t533082815::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (U3CRemoveHUDU3Ec__Iterator59_t2317328791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4818[3] = 
{
	U3CRemoveHUDU3Ec__Iterator59_t2317328791::get_offset_of_U24PC_0(),
	U3CRemoveHUDU3Ec__Iterator59_t2317328791::get_offset_of_U24current_1(),
	U3CRemoveHUDU3Ec__Iterator59_t2317328791::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (PointsAngle_t2793040272)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4819[7] = 
{
	PointsAngle_t2793040272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (GameMain_t2590236907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4820[18] = 
{
	GameMain_t2590236907::get_offset_of_buttonPlay_2(),
	GameMain_t2590236907::get_offset_of_premio_3(),
	GameMain_t2590236907::get_offset_of_roletaLst_4(),
	GameMain_t2590236907::get_offset_of_turnEaseType_5(),
	GameMain_t2590236907::get_offset_of_playSound_6(),
	GameMain_t2590236907::get_offset_of_winSound_7(),
	GameMain_t2590236907::get_offset_of_loseSound_8(),
	GameMain_t2590236907::get_offset_of_minTime_9(),
	GameMain_t2590236907::get_offset_of_delayTime_10(),
	GameMain_t2590236907::get_offset_of_letreiroDigital_11(),
	GameMain_t2590236907::get_offset_of_jogadasRestantes_12(),
	GameMain_t2590236907::get_offset_of_onGame_13(),
	GameMain_t2590236907::get_offset_of_alertLst_14(),
	GameMain_t2590236907::get_offset_of_serverControl_15(),
	GameMain_t2590236907::get_offset_of_lightControl_16(),
	GameMain_t2590236907::get_offset_of_audioSource_17(),
	GameMain_t2590236907::get_offset_of_counterStop_18(),
	GameMain_t2590236907::get_offset_of_startButtonColor_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4821[6] = 
{
	U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943::get_offset_of_U3CroletaU3E__0_0(),
	U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943::get_offset_of_U3Croleta1U3E__1_1(),
	U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943::get_offset_of_U3Croleta2U3E__2_2(),
	U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943::get_offset_of_U24PC_3(),
	U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943::get_offset_of_U24current_4(),
	U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (U3CStartU3Ec__Iterator5B_t2168360362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4822[3] = 
{
	U3CStartU3Ec__Iterator5B_t2168360362::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator5B_t2168360362::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator5B_t2168360362::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (GerarCupom_t1397758263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4823[1] = 
{
	GerarCupom_t1397758263::get_offset_of_cuponPontosValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (U3CIGerarU3Ec__Iterator5C_t2374326817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4824[10] = 
{
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CjsonU3E__0_0(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CheadersU3E__1_1(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CpDataU3E__2_2(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CwwwU3E__3_3(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CcodeU3E__4_4(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CmessageU3E__5_5(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CcuponNagumoDataU3E__6_6(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U24PC_7(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U24current_8(),
	U3CIGerarU3Ec__Iterator5C_t2374326817::get_offset_of_U3CU3Ef__this_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (GetInfoMenu_t3923496067), -1, sizeof(GetInfoMenu_t3923496067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4825[6] = 
{
	GetInfoMenu_t3923496067_StaticFields::get_offset_of_instance_2(),
	GetInfoMenu_t3923496067::get_offset_of_fotoImg_3(),
	GetInfoMenu_t3923496067::get_offset_of_nomeTxt_4(),
	GetInfoMenu_t3923496067::get_offset_of_girosTxt_5(),
	GetInfoMenu_t3923496067::get_offset_of_pontosTxt_6(),
	GetInfoMenu_t3923496067::get_offset_of_cuponsTxt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (U3CILoadImgU3Ec__Iterator5D_t529063718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4826[4] = 
{
	U3CILoadImgU3Ec__Iterator5D_t529063718::get_offset_of_U3CwwwU3E__0_0(),
	U3CILoadImgU3Ec__Iterator5D_t529063718::get_offset_of_U24PC_1(),
	U3CILoadImgU3Ec__Iterator5D_t529063718::get_offset_of_U24current_2(),
	U3CILoadImgU3Ec__Iterator5D_t529063718::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (GetUIEvents_t3946912259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4827[1] = 
{
	GetUIEvents_t3946912259::get_offset_of_onPointerClick_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (HidePlugin_t2251332405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (ImagemScroll_t3817704447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4829[3] = 
{
	ImagemScroll_t3817704447::get_offset_of_dropdown_2(),
	ImagemScroll_t3817704447::get_offset_of_scrollRect_3(),
	ImagemScroll_t3817704447::get_offset_of_infoRegionLst_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (ItemMoveDrag_t687865080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4830[16] = 
{
	ItemMoveDrag_t687865080::get_offset_of_velocity_2(),
	ItemMoveDrag_t687865080::get_offset_of_minVelToSelect_3(),
	ItemMoveDrag_t687865080::get_offset_of_rotFactor_4(),
	ItemMoveDrag_t687865080::get_offset_of_easetype_5(),
	ItemMoveDrag_t687865080::get_offset_of_easetypeTime_6(),
	ItemMoveDrag_t687865080::get_offset_of_allowImage_7(),
	ItemMoveDrag_t687865080::get_offset_of_denyImage_8(),
	ItemMoveDrag_t687865080::get_offset_of_onAllow_9(),
	ItemMoveDrag_t687865080::get_offset_of_onDeny_10(),
	ItemMoveDrag_t687865080::get_offset_of_onSelected_11(),
	ItemMoveDrag_t687865080::get_offset_of_rectTransform_12(),
	ItemMoveDrag_t687865080::get_offset_of_mousePosition_13(),
	ItemMoveDrag_t687865080::get_offset_of_onSelectArea_14(),
	ItemMoveDrag_t687865080::get_offset_of_lastPosition_15(),
	ItemMoveDrag_t687865080::get_offset_of_scrollRect_16(),
	ItemMoveDrag_t687865080::get_offset_of_isSelected_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (LightsControl_t2444629088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4831[7] = 
{
	LightsControl_t2444629088::get_offset_of_lights_2(),
	LightsControl_t2444629088::get_offset_of_velocity_3(),
	LightsControl_t2444629088::get_offset_of_lightOnColor_4(),
	LightsControl_t2444629088::get_offset_of_emissionColor_5(),
	LightsControl_t2444629088::get_offset_of_emissionColorLst_6(),
	LightsControl_t2444629088::get_offset_of_startMaterialColor_7(),
	LightsControl_t2444629088::get_offset_of_startEmissionColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (U3CITurnU3Ec__Iterator5E_t1739934332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4832[6] = 
{
	U3CITurnU3Ec__Iterator5E_t1739934332::get_offset_of_U3CiU3E__0_0(),
	U3CITurnU3Ec__Iterator5E_t1739934332::get_offset_of_U3CU24s_315U3E__1_1(),
	U3CITurnU3Ec__Iterator5E_t1739934332::get_offset_of_U3CobjU3E__2_2(),
	U3CITurnU3Ec__Iterator5E_t1739934332::get_offset_of_U24PC_3(),
	U3CITurnU3Ec__Iterator5E_t1739934332::get_offset_of_U24current_4(),
	U3CITurnU3Ec__Iterator5E_t1739934332::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (U3CIBlinkAllU3Ec__Iterator5F_t13658789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4833[8] = 
{
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U3CiU3E__0_0(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_repeat_1(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U3CU24s_316U3E__1_2(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U3CobjU3E__2_3(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U24PC_4(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U24current_5(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U3CU24U3Erepeat_6(),
	U3CIBlinkAllU3Ec__Iterator5F_t13658789::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (LoadLevelName_t284939721), -1, sizeof(LoadLevelName_t284939721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4834[6] = 
{
	LoadLevelName_t284939721_StaticFields::get_offset_of_loadedScene_2(),
	LoadLevelName_t284939721_StaticFields::get_offset_of_isAcougue_3(),
	LoadLevelName_t284939721::get_offset_of_disableObj_4(),
	LoadLevelName_t284939721::get_offset_of_enableAcougue_5(),
	LoadLevelName_t284939721::get_offset_of_back_6(),
	LoadLevelName_t284939721::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (U3CGoLevelU3Ec__Iterator60_t233841331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4835[4] = 
{
	U3CGoLevelU3Ec__Iterator60_t233841331::get_offset_of_U3CasyncU3E__0_0(),
	U3CGoLevelU3Ec__Iterator60_t233841331::get_offset_of_U24PC_1(),
	U3CGoLevelU3Ec__Iterator60_t233841331::get_offset_of_U24current_2(),
	U3CGoLevelU3Ec__Iterator60_t233841331::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4836[4] = 
{
	U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120::get_offset_of_U3CasyncU3E__0_0(),
	U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120::get_offset_of_U24PC_1(),
	U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120::get_offset_of_U24current_2(),
	U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (LoaderPublishAnim_t3439243661), -1, sizeof(LoaderPublishAnim_t3439243661_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4837[9] = 
{
	LoaderPublishAnim_t3439243661_StaticFields::get_offset_of_instance_2(),
	LoaderPublishAnim_t3439243661::get_offset_of_image_3(),
	LoaderPublishAnim_t3439243661::get_offset_of_yMax_4(),
	LoaderPublishAnim_t3439243661::get_offset_of_container_5(),
	LoaderPublishAnim_t3439243661::get_offset_of_multiplier_6(),
	LoaderPublishAnim_t3439243661::get_offset_of_onLoad_7(),
	LoaderPublishAnim_t3439243661::get_offset_of_isLoading_8(),
	LoaderPublishAnim_t3439243661::get_offset_of_startPos_9(),
	LoaderPublishAnim_t3439243661::get_offset_of_startDiff_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (Login_t73596745), -1, sizeof(Login_t73596745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4838[9] = 
{
	Login_t73596745_StaticFields::get_offset_of_userData_2(),
	Login_t73596745_StaticFields::get_offset_of_isLogged_3(),
	Login_t73596745_StaticFields::get_offset_of_photo_profile_4(),
	Login_t73596745::get_offset_of_loginInput_5(),
	Login_t73596745::get_offset_of_senhaInput_6(),
	Login_t73596745::get_offset_of_loadingObj_7(),
	Login_t73596745::get_offset_of_popup_8(),
	Login_t73596745_StaticFields::get_offset_of__analytics_9(),
	Login_t73596745::get_offset_of_back_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4839[7] = 
{
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U3CjsonU3E__0_0(),
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U3CheadersU3E__1_1(),
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U3CpDataU3E__2_2(),
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U3CwwwU3E__3_3(),
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U24PC_4(),
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U24current_5(),
	U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (U3CILoginU3Ec__Iterator63_t3961520294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4840[9] = 
{
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CcodeU3E__4_4(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CmessageU3E__5_5(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U24PC_6(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U24current_7(),
	U3CILoginU3Ec__Iterator63_t3961520294::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (U3CILoadImgProfileU3Ec__Iterator64_t1069902512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4841[5] = 
{
	U3CILoadImgProfileU3Ec__Iterator64_t1069902512::get_offset_of_photoName_0(),
	U3CILoadImgProfileU3Ec__Iterator64_t1069902512::get_offset_of_U3CwwwU3E__0_1(),
	U3CILoadImgProfileU3Ec__Iterator64_t1069902512::get_offset_of_U24PC_2(),
	U3CILoadImgProfileU3Ec__Iterator64_t1069902512::get_offset_of_U24current_3(),
	U3CILoadImgProfileU3Ec__Iterator64_t1069902512::get_offset_of_U3CU24U3EphotoName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4842[4] = 
{
	U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268::get_offset_of_U3CwwwU3E__0_0(),
	U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268::get_offset_of_U24PC_1(),
	U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268::get_offset_of_U24current_2(),
	U3CIEsqueceuSenhaU3Ec__Iterator65_t2577917268::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (U3CGoLevelU3Ec__Iterator66_t2001881401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4843[3] = 
{
	U3CGoLevelU3Ec__Iterator66_t2001881401::get_offset_of_U3CasyncU3E__0_0(),
	U3CGoLevelU3Ec__Iterator66_t2001881401::get_offset_of_U24PC_1(),
	U3CGoLevelU3Ec__Iterator66_t2001881401::get_offset_of_U24current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (TesteApresentacoes_t874390990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4844[5] = 
{
	TesteApresentacoes_t874390990::get_offset_of_tracksIds_2(),
	TesteApresentacoes_t874390990::get_offset_of_TrackingsIds_3(),
	TesteApresentacoes_t874390990::get_offset_of_Labels_4(),
	TesteApresentacoes_t874390990::get_offset_of__hasTracks_5(),
	TesteApresentacoes_t874390990::get_offset_of_currentWindow_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (RelativePositionOfPoint_t3585775620), -1, sizeof(RelativePositionOfPoint_t3585775620_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4845[8] = 
{
	RelativePositionOfPoint_t3585775620::get_offset_of_referenceInternalPoint_2(),
	RelativePositionOfPoint_t3585775620_StaticFields::get_offset_of_Instance_3(),
	RelativePositionOfPoint_t3585775620::get_offset_of_DebugSetRelativePoint_4(),
	RelativePositionOfPoint_t3585775620::get_offset_of_DebugPercentDistance_5(),
	RelativePositionOfPoint_t3585775620::get_offset_of_DebugGlobalPointResult_6(),
	RelativePositionOfPoint_t3585775620::get_offset_of_DebugCalcNow_7(),
	RelativePositionOfPoint_t3585775620::get_offset_of_DebugRecivedVector_8(),
	RelativePositionOfPoint_t3585775620::get_offset_of_DebugPercent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (MenuNagumo_t1404218786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (MeuPedidoAcougue_t2271273197), -1, sizeof(MeuPedidoAcougue_t2271273197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4849[10] = 
{
	MeuPedidoAcougue_t2271273197_StaticFields::get_offset_of_pedidoEnvioDatasLst_2(),
	MeuPedidoAcougue_t2271273197::get_offset_of_content_3(),
	MeuPedidoAcougue_t2271273197::get_offset_of_itemPrefab_4(),
	MeuPedidoAcougue_t2271273197::get_offset_of_loadingObj_5(),
	MeuPedidoAcougue_t2271273197::get_offset_of_popup_6(),
	MeuPedidoAcougue_t2271273197::get_offset_of_pedidoRealizado_7(),
	MeuPedidoAcougue_t2271273197::get_offset_of_pedidoEmProducao_8(),
	MeuPedidoAcougue_t2271273197::get_offset_of_pedidoPronto_9(),
	MeuPedidoAcougue_t2271273197::get_offset_of_textPosicao_10(),
	MeuPedidoAcougue_t2271273197::get_offset_of_pedidosAvaliarLst_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (U3CICheckPedidoU3Ec__Iterator67_t642390674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4850[21] = 
{
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CjsonU3E__0_0(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CheadersU3E__1_1(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CpDataU3E__2_2(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CwwwU3E__3_3(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CmessageU3E__4_4(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CmeuPedidoDataU3E__5_5(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CavaliouU3E__6_6(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CstatusDictU3E__7_7(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CU24s_321U3E__8_8(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CU24s_322U3E__9_9(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CpedidoDataU3E__10_10(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CinfoU3E__11_11(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CU24s_323U3E__12_12(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CU24s_324U3E__13_13(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CcarneDataU3E__14_14(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_isUpdate_15(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CexU3E__15_16(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U24PC_17(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U24current_18(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CU24U3EisUpdate_19(),
	U3CICheckPedidoU3Ec__Iterator67_t642390674::get_offset_of_U3CU3Ef__this_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4851[7] = 
{
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U3CjsonU3E__0_0(),
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U3CheadersU3E__1_1(),
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U3CpDataU3E__2_2(),
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U3CwwwU3E__3_3(),
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U24PC_4(),
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U24current_5(),
	U3CIConfirmarRetiradaU3Ec__Iterator68_t1931009611::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (NativeImagePicker_t1077436690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4852[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (NativeImagePickerBehaviour_t3828351720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4853[1] = 
{
	NativeImagePickerBehaviour_t3828351720::get_offset_of_Callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (CallbackImagePicked_t554520473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (OfertasManager_t821152011), -1, sizeof(OfertasManager_t821152011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4855[31] = 
{
	0,
	OfertasManager_t821152011_StaticFields::get_offset_of_ofertasData_3(),
	OfertasManager_t821152011_StaticFields::get_offset_of_instance_4(),
	OfertasManager_t821152011_StaticFields::get_offset_of_titleLst_5(),
	OfertasManager_t821152011_StaticFields::get_offset_of_valueLst_6(),
	OfertasManager_t821152011_StaticFields::get_offset_of_infoLst_7(),
	OfertasManager_t821152011_StaticFields::get_offset_of_idLst_8(),
	OfertasManager_t821152011_StaticFields::get_offset_of_validadeLst_9(),
	OfertasManager_t821152011_StaticFields::get_offset_of_linkLst_10(),
	OfertasManager_t821152011::get_offset_of_ofertaValuesLst_11(),
	OfertasManager_t821152011_StaticFields::get_offset_of_textureLst_12(),
	OfertasManager_t821152011_StaticFields::get_offset_of_imgLst_13(),
	OfertasManager_t821152011_StaticFields::get_offset_of_favoritouLst_14(),
	OfertasManager_t821152011::get_offset_of_loadingObj_15(),
	OfertasManager_t821152011::get_offset_of_popup_16(),
	OfertasManager_t821152011::get_offset_of_itemPrefab_17(),
	OfertasManager_t821152011::get_offset_of_containerItens_18(),
	OfertasManager_t821152011_StaticFields::get_offset_of_itensList_19(),
	OfertasManager_t821152011::get_offset_of_favoritouPrimeiraVezObj_20(),
	OfertasManager_t821152011::get_offset_of_popupSemConexao_21(),
	OfertasManager_t821152011::get_offset_of_scrollRect_22(),
	OfertasManager_t821152011::get_offset_of_ofertaCounter_23(),
	OfertasManager_t821152011_StaticFields::get_offset_of_currentItem_24(),
	OfertasManager_t821152011::get_offset_of_selectedItem_25(),
	OfertasManager_t821152011::get_offset_of_quantidadeOfertasText_26(),
	OfertasManager_t821152011::get_offset_of_horizontalScrollSnap_27(),
	OfertasManager_t821152011_StaticFields::get_offset_of_currentJsonTxt_28(),
	OfertasManager_t821152011_StaticFields::get_offset_of_loadedItensCount_29(),
	OfertasManager_t821152011::get_offset_of_rectContainer_30(),
	OfertasManager_t821152011::get_offset_of_onRectUpdate_31(),
	OfertasManager_t821152011_StaticFields::get_offset_of_loadedImgsCount_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (U3CISendStatusU3Ec__Iterator69_t1960186977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4856[10] = 
{
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_id_0(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_status_1(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U3CjsonU3E__0_2(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U3CheadersU3E__1_3(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U3CpDataU3E__2_4(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U3CwwwU3E__3_5(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U24PC_6(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U24current_7(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U3CU24U3Eid_8(),
	U3CISendStatusU3Ec__Iterator69_t1960186977::get_offset_of_U3CU24U3Estatus_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (U3CILoadOfertasU3Ec__Iterator6A_t2130003369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4857[13] = 
{
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CU24s_325U3E__4_4(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CobjU3E__5_5(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CmessageU3E__6_6(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CU24s_326U3E__7_7(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CU24s_327U3E__8_8(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CofertaDataU3E__9_9(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U24PC_10(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U24current_11(),
	U3CILoadOfertasU3Ec__Iterator6A_t2130003369::get_offset_of_U3CU3Ef__this_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (U3CILoadImgsU3Ec__Iterator6B_t1691882790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4858[7] = 
{
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U3CiU3E__0_0(),
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U3CwwwU3E__1_1(),
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U3CtextureU3E__2_2(),
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U3CsptU3E__3_3(),
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U24PC_4(),
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U24current_5(),
	U3CILoadImgsU3Ec__Iterator6B_t1691882790::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4859[4] = 
{
	U3CLoadLoadedsU3Ec__Iterator6C_t2757493264::get_offset_of_U3CiU3E__0_0(),
	U3CLoadLoadedsU3Ec__Iterator6C_t2757493264::get_offset_of_U24PC_1(),
	U3CLoadLoadedsU3Ec__Iterator6C_t2757493264::get_offset_of_U24current_2(),
	U3CLoadLoadedsU3Ec__Iterator6C_t2757493264::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (PanelManager_t2965005609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4860[6] = 
{
	0,
	0,
	PanelManager_t2965005609::get_offset_of_initiallyOpen_4(),
	PanelManager_t2965005609::get_offset_of_m_OpenParameterId_5(),
	PanelManager_t2965005609::get_offset_of_m_Open_6(),
	PanelManager_t2965005609::get_offset_of_m_PreviouslySelected_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4861[7] = 
{
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_U3CclosedStateReachedU3E__0_0(),
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_U3CwantToCloseU3E__1_1(),
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_anim_2(),
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_U24PC_3(),
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_U24current_4(),
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_U3CU24U3Eanim_5(),
	U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (PedidoAcougue_t9638794), -1, sizeof(PedidoAcougue_t9638794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4862[31] = 
{
	PedidoAcougue_t9638794_StaticFields::get_offset_of_acougueData_2(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_instance_3(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_posicao_4(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_titleLst_5(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_idLst_6(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_cortesLst_7(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_textureLst_8(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_imgLst_9(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_favoritouLst_10(),
	PedidoAcougue_t9638794::get_offset_of_loadingObj_11(),
	PedidoAcougue_t9638794::get_offset_of_popup_12(),
	PedidoAcougue_t9638794::get_offset_of_itemPrefab_13(),
	PedidoAcougue_t9638794::get_offset_of_containerItens_14(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_itensList_15(),
	PedidoAcougue_t9638794::get_offset_of_dropdownTipoDeCorte_16(),
	PedidoAcougue_t9638794::get_offset_of_dropdownPeso_17(),
	PedidoAcougue_t9638794::get_offset_of_positionText_18(),
	PedidoAcougue_t9638794::get_offset_of_quantidade_19(),
	PedidoAcougue_t9638794::get_offset_of_observacoes_20(),
	PedidoAcougue_t9638794::get_offset_of_textPeso_21(),
	PedidoAcougue_t9638794::get_offset_of_pedidoData_22(),
	PedidoAcougue_t9638794::get_offset_of_scrollRect_23(),
	PedidoAcougue_t9638794::get_offset_of_carnesCount_24(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_currentItem_25(),
	PedidoAcougue_t9638794::get_offset_of_selectedItem_26(),
	PedidoAcougue_t9638794::get_offset_of_quantidadeOfertasText_27(),
	PedidoAcougue_t9638794::get_offset_of_horizontalScrollSnap_28(),
	PedidoAcougue_t9638794::get_offset_of_carneTitle_29(),
	PedidoAcougue_t9638794::get_offset_of_categoria_carne_30(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_corteIdDict_31(),
	PedidoAcougue_t9638794_StaticFields::get_offset_of_currentJsonTxt_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (U3CISendStatusU3Ec__Iterator6E_t3429446286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4863[10] = 
{
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_id_0(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_status_1(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U3CjsonU3E__0_2(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U3CheadersU3E__1_3(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U3CpDataU3E__2_4(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U3CwwwU3E__3_5(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U24PC_6(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U24current_7(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U3CU24U3Eid_8(),
	U3CISendStatusU3Ec__Iterator6E_t3429446286::get_offset_of_U3CU24U3Estatus_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (U3CILoadCarnesU3Ec__Iterator6F_t1280841667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4864[15] = 
{
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_search_4(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CU24s_332U3E__4_5(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CobjU3E__5_6(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CmessageU3E__6_7(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CU24s_333U3E__7_8(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CU24s_334U3E__8_9(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CcarneDataU3E__9_10(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U24PC_11(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U24current_12(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CU24U3Esearch_13(),
	U3CILoadCarnesU3Ec__Iterator6F_t1280841667::get_offset_of_U3CU3Ef__this_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (U3CILoadImgsU3Ec__Iterator70_t2033076116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4865[9] = 
{
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CU24s_335U3E__0_0(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CU24s_336U3E__1_1(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CcarneDataU3E__2_2(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CtextureU3E__4_4(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CsptU3E__5_5(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U24PC_6(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U24current_7(),
	U3CILoadImgsU3Ec__Iterator70_t2033076116::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (U3CLoadLoadedsU3Ec__Iterator71_t4226752574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4866[4] = 
{
	U3CLoadLoadedsU3Ec__Iterator71_t4226752574::get_offset_of_U3CiU3E__0_0(),
	U3CLoadLoadedsU3Ec__Iterator71_t4226752574::get_offset_of_U24PC_1(),
	U3CLoadLoadedsU3Ec__Iterator71_t4226752574::get_offset_of_U24current_2(),
	U3CLoadLoadedsU3Ec__Iterator71_t4226752574::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (PedidoValues_t2123607271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4867[13] = 
{
	PedidoValues_t2123607271::get_offset_of_pedido_2(),
	PedidoValues_t2123607271::get_offset_of_nomeCarne_3(),
	PedidoValues_t2123607271::get_offset_of_peso_4(),
	PedidoValues_t2123607271::get_offset_of_tipoCarne_5(),
	PedidoValues_t2123607271::get_offset_of_unidade_6(),
	PedidoValues_t2123607271::get_offset_of_horarioData_7(),
	PedidoValues_t2123607271::get_offset_of_info_8(),
	PedidoValues_t2123607271::get_offset_of_codePedido_9(),
	PedidoValues_t2123607271::get_offset_of_quantidade_10(),
	PedidoValues_t2123607271::get_offset_of_sprite_11(),
	PedidoValues_t2123607271::get_offset_of_infoText_12(),
	PedidoValues_t2123607271::get_offset_of_titleText_13(),
	PedidoValues_t2123607271::get_offset_of_image_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { sizeof (PedidosRevisao_t3346969709), -1, sizeof(PedidosRevisao_t3346969709_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4868[7] = 
{
	PedidosRevisao_t3346969709_StaticFields::get_offset_of_pedidosLst_2(),
	PedidosRevisao_t3346969709_StaticFields::get_offset_of_idPedido_3(),
	PedidosRevisao_t3346969709::get_offset_of_content_4(),
	PedidosRevisao_t3346969709::get_offset_of_itemPrefab_5(),
	PedidosRevisao_t3346969709::get_offset_of_loadingObj_6(),
	PedidosRevisao_t3346969709::get_offset_of_popup_7(),
	PedidosRevisao_t3346969709::get_offset_of_popupPassarPeso_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (U3CIEnviarPedidoU3Ec__Iterator72_t764597359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4869[12] = 
{
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CpedidoEnvioDataU3E__0_0(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_aceitaPassarPeso_1(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CjsonU3E__1_2(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CheadersU3E__2_3(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CpDataU3E__3_4(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CwwwU3E__4_5(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CmessageU3E__5_6(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CexU3E__6_7(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U24PC_8(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U24current_9(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CU24U3EaceitaPassarPeso_10(),
	U3CIEnviarPedidoU3Ec__Iterator72_t764597359::get_offset_of_U3CU3Ef__this_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (Permissions_t2801572964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (PoliticasPrivacidade_t2368006066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4871[1] = 
{
	PoliticasPrivacidade_t2368006066::get_offset_of_politicas_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (PopupAtualizarCadastro_t1794233116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (PopupManager_t2711269761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4873[5] = 
{
	PopupManager_t2711269761::get_offset_of_time_2(),
	PopupManager_t2711269761::get_offset_of_lifeTime_3(),
	PopupManager_t2711269761::get_offset_of_easetype_4(),
	PopupManager_t2711269761::get_offset_of_fundo_5(),
	PopupManager_t2711269761::get_offset_of_automaticClose_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (PopupSugestaoOfertas_t1283182119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (PublishLink_t1052985737), -1, sizeof(PublishLink_t1052985737_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4875[4] = 
{
	PublishLink_t1052985737_StaticFields::get_offset_of_url_2(),
	PublishLink_t1052985737::get_offset_of_optionalUrl_3(),
	PublishLink_t1052985737::get_offset_of_htmlTxt_4(),
	PublishLink_t1052985737::get_offset_of_uniWebView_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4876[5] = 
{
	U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968::get_offset_of_U3CwwwU3E__0_0(),
	U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968::get_offset_of_action_1(),
	U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968::get_offset_of_U24PC_2(),
	U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968::get_offset_of_U24current_3(),
	U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968::get_offset_of_U3CU24U3Eaction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { sizeof (PublishManager_t4010203070), -1, sizeof(PublishManager_t4010203070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4877[22] = 
{
	PublishManager_t4010203070_StaticFields::get_offset_of_publicationsData_2(),
	PublishManager_t4010203070_StaticFields::get_offset_of_instance_3(),
	PublishManager_t4010203070::get_offset_of_toggleList_4(),
	PublishManager_t4010203070::get_offset_of_titleLst_5(),
	PublishManager_t4010203070::get_offset_of_valueLst_6(),
	PublishManager_t4010203070::get_offset_of_infoLst_7(),
	PublishManager_t4010203070::get_offset_of_idLst_8(),
	PublishManager_t4010203070::get_offset_of_linkLst_9(),
	PublishManager_t4010203070::get_offset_of_urlImgLst_10(),
	PublishManager_t4010203070::get_offset_of_favoritouLst_11(),
	PublishManager_t4010203070::get_offset_of_imgLst_12(),
	PublishManager_t4010203070::get_offset_of_loadingObj_13(),
	PublishManager_t4010203070::get_offset_of_popup_14(),
	PublishManager_t4010203070::get_offset_of_itemPrefab_15(),
	PublishManager_t4010203070::get_offset_of_containerItens_16(),
	PublishManager_t4010203070::get_offset_of_itensList_17(),
	PublishManager_t4010203070::get_offset_of_horizontalScrollSnap_18(),
	PublishManager_t4010203070::get_offset_of_scrollRect_19(),
	PublishManager_t4010203070::get_offset_of_ofertaCounter_20(),
	PublishManager_t4010203070::get_offset_of_currentItem_21(),
	PublishManager_t4010203070::get_offset_of_selectedItem_22(),
	PublishManager_t4010203070_StaticFields::get_offset_of_currentJsonTxt_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { sizeof (U3CIFavoritarU3Ec__Iterator74_t983049004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4878[8] = 
{
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_id_0(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U3CjsonU3E__0_1(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U3CheadersU3E__1_2(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U3CpDataU3E__2_3(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U3CwwwU3E__3_4(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U24PC_5(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U24current_6(),
	U3CIFavoritarU3Ec__Iterator74_t983049004::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { sizeof (U3CISendStatusU3Ec__Iterator75_t1647341033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4879[10] = 
{
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_id_0(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_status_1(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U3CjsonU3E__0_2(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U3CheadersU3E__1_3(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U3CpDataU3E__2_4(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U3CwwwU3E__3_5(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U24PC_6(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U24current_7(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U3CU24U3Eid_8(),
	U3CISendStatusU3Ec__Iterator75_t1647341033::get_offset_of_U3CU24U3Estatus_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { sizeof (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4880[8] = 
{
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_id_0(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U3CjsonU3E__0_1(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U3CheadersU3E__1_2(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U3CpDataU3E__2_3(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U3CwwwU3E__3_4(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U24PC_5(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U24current_6(),
	U3CExcluirFavoritoU3Ec__Iterator76_t1974763023::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { sizeof (U3CILoadPublishU3Ec__Iterator77_t1612164062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4881[12] = 
{
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CmessageU3E__4_4(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CU24s_340U3E__5_5(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CU24s_341U3E__6_6(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CpublicationDataU3E__7_7(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CexU3E__8_8(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U24PC_9(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U24current_10(),
	U3CILoadPublishU3Ec__Iterator77_t1612164062::get_offset_of_U3CU3Ef__this_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4882[14] = 
{
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CU24s_342U3E__4_4(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CobjU3E__5_5(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CmessageU3E__6_6(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CU24s_343U3E__7_7(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CU24s_344U3E__8_8(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CpublicationDataU3E__9_9(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CexU3E__10_10(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U24PC_11(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U24current_12(),
	U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936::get_offset_of_U3CU3Ef__this_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (U3CILoadImgsU3Ec__Iterator79_t1709434345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4883[9] = 
{
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CU24s_345U3E__0_0(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CU24s_346U3E__1_1(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CpublicationDataU3E__2_2(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CsptU3E__4_4(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CiU3E__5_5(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U24PC_6(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U24current_7(),
	U3CILoadImgsU3Ec__Iterator79_t1709434345::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { sizeof (ResizePanel_t788198800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4884[5] = 
{
	ResizePanel_t788198800::get_offset_of_minSize_2(),
	ResizePanel_t788198800::get_offset_of_maxSize_3(),
	ResizePanel_t788198800::get_offset_of_panelRectTransform_4(),
	ResizePanel_t788198800::get_offset_of_originalLocalPointerPosition_5(),
	ResizePanel_t788198800::get_offset_of_originalSizeDelta_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (RoletaPointAngle_t1105688326)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4885[6] = 
{
	RoletaPointAngle_t1105688326::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (RoletaGame_t108100629), -1, sizeof(RoletaGame_t108100629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4886[15] = 
{
	RoletaGame_t108100629_StaticFields::get_offset_of_instance_2(),
	RoletaGame_t108100629::get_offset_of_jogadasTxt_3(),
	RoletaGame_t108100629::get_offset_of_restaJogadasTxt_4(),
	RoletaGame_t108100629::get_offset_of_offline_5(),
	RoletaGame_t108100629::get_offset_of_points_6(),
	RoletaGame_t108100629::get_offset_of_time_7(),
	RoletaGame_t108100629::get_offset_of_speed_8(),
	RoletaGame_t108100629::get_offset_of_pointAngles_9(),
	RoletaGame_t108100629::get_offset_of_easetype_10(),
	RoletaGame_t108100629::get_offset_of_roletaGO_11(),
	RoletaGame_t108100629::get_offset_of_popups_12(),
	RoletaGame_t108100629::get_offset_of_popupTime_13(),
	RoletaGame_t108100629::get_offset_of_ptsIndex_14(),
	RoletaGame_t108100629::get_offset_of_inGame_15(),
	RoletaGame_t108100629::get_offset_of_startSpins_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (U3CShowPopupU3Ec__Iterator7A_t1326136970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4887[4] = 
{
	U3CShowPopupU3Ec__Iterator7A_t1326136970::get_offset_of_U3ChtU3E__0_0(),
	U3CShowPopupU3Ec__Iterator7A_t1326136970::get_offset_of_U24PC_1(),
	U3CShowPopupU3Ec__Iterator7A_t1326136970::get_offset_of_U24current_2(),
	U3CShowPopupU3Ec__Iterator7A_t1326136970::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (Rotation_t24343454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4888[1] = 
{
	Rotation_t24343454::get_offset_of_velocity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (ScrollDetailTexture_t3150826909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4889[7] = 
{
	ScrollDetailTexture_t3150826909::get_offset_of_uniqueMaterial_2(),
	ScrollDetailTexture_t3150826909::get_offset_of_scrollPerSecond_3(),
	ScrollDetailTexture_t3150826909::get_offset_of_m_Matrix_4(),
	ScrollDetailTexture_t3150826909::get_offset_of_mCopy_5(),
	ScrollDetailTexture_t3150826909::get_offset_of_mOriginal_6(),
	ScrollDetailTexture_t3150826909::get_offset_of_mSprite_7(),
	ScrollDetailTexture_t3150826909::get_offset_of_m_Mat_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (SendJson_t1311805616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4890[3] = 
{
	SendJson_t1311805616::get_offset_of_jsonInput_2(),
	SendJson_t1311805616::get_offset_of_resp_3(),
	SendJson_t1311805616::get_offset_of_url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (U3CISendJsonU3Ec__Iterator7B_t281221434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4891[7] = 
{
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U3CjsonU3E__0_0(),
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U3CheadersU3E__1_1(),
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U3CpDataU3E__2_2(),
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U3CwwwU3E__3_3(),
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U24PC_4(),
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U24current_5(),
	U3CISendJsonU3Ec__Iterator7B_t281221434::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (ServerControl_t2725829754), -1, sizeof(ServerControl_t2725829754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4892[14] = 
{
	ServerControl_t2725829754_StaticFields::get_offset_of_instance_2(),
	ServerControl_t2725829754_StaticFields::get_offset_of_url_3(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlCRM_4(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlIMGOfertas_5(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlIMGPublicacoes_6(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlIMGTabloides_7(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlIMGProfile_8(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlAcougue_9(),
	ServerControl_t2725829754_StaticFields::get_offset_of_urlIMGAcougue_10(),
	ServerControl_t2725829754::get_offset_of_popup_11(),
	ServerControl_t2725829754::get_offset_of_lightControl_12(),
	ServerControl_t2725829754::get_offset_of_gameMain_13(),
	ServerControl_t2725829754::get_offset_of_alertMessage_14(),
	ServerControl_t2725829754::get_offset_of_OnPlay_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (OnPlayDelegate_t3097072301), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (U3CIPlayU3Ec__Iterator7C_t3776050517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4894[13] = 
{
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CwwwU3E__0_0(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CjsonPlayRequestU3E__1_1(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CjsonU3E__2_2(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CheadersU3E__3_3(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CpDataU3E__4_4(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CfakeProgressU3E__5_5(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CstateU3E__6_6(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CjsonPlayU3E__7_7(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CcuponDataU3E__8_8(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3ClstU3E__9_9(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U24PC_10(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U24current_11(),
	U3CIPlayU3Ec__Iterator7C_t3776050517::get_offset_of_U3CU3Ef__this_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (U3CIPlayRoletaU3Ec__Iterator7D_t361832729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4895[12] = 
{
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CwwwU3E__0_0(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CjsonPlayRequestU3E__1_1(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CjsonU3E__2_2(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CheadersU3E__3_3(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CpDataU3E__4_4(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CfakeProgressU3E__5_5(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CjsonPlayU3E__6_6(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CcuponDataU3E__7_7(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3ClstU3E__8_8(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U24PC_9(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U24current_10(),
	U3CIPlayRoletaU3Ec__Iterator7D_t361832729::get_offset_of_U3CU3Ef__this_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4896[5] = 
{
	U3CConectandoLetreiroU3Ec__Iterator7E_t539930572::get_offset_of_value_0(),
	U3CConectandoLetreiroU3Ec__Iterator7E_t539930572::get_offset_of_U24PC_1(),
	U3CConectandoLetreiroU3Ec__Iterator7E_t539930572::get_offset_of_U24current_2(),
	U3CConectandoLetreiroU3Ec__Iterator7E_t539930572::get_offset_of_U3CU24U3Evalue_3(),
	U3CConectandoLetreiroU3Ec__Iterator7E_t539930572::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (JsonPlay_t2384567388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4897[4] = 
{
	JsonPlay_t2384567388::get_offset_of_pts_0(),
	JsonPlay_t2384567388::get_offset_of_jogadas_1(),
	JsonPlay_t2384567388::get_offset_of_cupom_2(),
	JsonPlay_t2384567388::get_offset_of_valor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (JsonPlayRequest_t2877984403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4898[3] = 
{
	JsonPlayRequest_t2877984403::get_offset_of_id_participante_0(),
	JsonPlayRequest_t2877984403::get_offset_of_senha_1(),
	JsonPlayRequest_t2877984403::get_offset_of_cpf_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (ShowSliderValue_t1108427187), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
