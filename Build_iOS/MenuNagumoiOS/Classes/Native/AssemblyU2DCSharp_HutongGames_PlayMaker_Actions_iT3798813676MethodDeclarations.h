﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenStop
struct iTweenStop_t3798813676;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenStop::.ctor()
extern "C"  void iTweenStop__ctor_m3257348602 (iTweenStop_t3798813676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::Reset()
extern "C"  void iTweenStop_Reset_m903781543 (iTweenStop_t3798813676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::OnEnter()
extern "C"  void iTweenStop_OnEnter_m2967543633 (iTweenStop_t3798813676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::DoiTween()
extern "C"  void iTweenStop_DoiTween_m3250671639 (iTweenStop_t3798813676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
