﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleUniversalAnalytics/<netActivity>c__Iterator33
struct U3CnetActivityU3Ec__Iterator33_t1845284094;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GoogleUniversalAnalytics/<netActivity>c__Iterator33::.ctor()
extern "C"  void U3CnetActivityU3Ec__Iterator33__ctor_m3783222317 (U3CnetActivityU3Ec__Iterator33_t1845284094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoogleUniversalAnalytics/<netActivity>c__Iterator33::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CnetActivityU3Ec__Iterator33_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3825167813 (U3CnetActivityU3Ec__Iterator33_t1845284094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoogleUniversalAnalytics/<netActivity>c__Iterator33::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CnetActivityU3Ec__Iterator33_System_Collections_IEnumerator_get_Current_m1183292249 (U3CnetActivityU3Ec__Iterator33_t1845284094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics/<netActivity>c__Iterator33::MoveNext()
extern "C"  bool U3CnetActivityU3Ec__Iterator33_MoveNext_m1150963879 (U3CnetActivityU3Ec__Iterator33_t1845284094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics/<netActivity>c__Iterator33::Dispose()
extern "C"  void U3CnetActivityU3Ec__Iterator33_Dispose_m2032597290 (U3CnetActivityU3Ec__Iterator33_t1845284094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics/<netActivity>c__Iterator33::Reset()
extern "C"  void U3CnetActivityU3Ec__Iterator33_Reset_m1429655258 (U3CnetActivityU3Ec__Iterator33_t1845284094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
