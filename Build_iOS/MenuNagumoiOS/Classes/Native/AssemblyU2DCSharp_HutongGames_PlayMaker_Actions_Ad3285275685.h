﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddExplosionForce
struct  AddExplosionForce_t3285275685  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddExplosionForce::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddExplosionForce::center
	FsmVector3_t533912882 * ___center_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddExplosionForce::force
	FsmFloat_t2134102846 * ___force_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddExplosionForce::radius
	FsmFloat_t2134102846 * ___radius_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddExplosionForce::upwardsModifier
	FsmFloat_t2134102846 * ___upwardsModifier_15;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.AddExplosionForce::forceMode
	int32_t ___forceMode_16;
	// System.Boolean HutongGames.PlayMaker.Actions.AddExplosionForce::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_center_12() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___center_12)); }
	inline FsmVector3_t533912882 * get_center_12() const { return ___center_12; }
	inline FsmVector3_t533912882 ** get_address_of_center_12() { return &___center_12; }
	inline void set_center_12(FsmVector3_t533912882 * value)
	{
		___center_12 = value;
		Il2CppCodeGenWriteBarrier(&___center_12, value);
	}

	inline static int32_t get_offset_of_force_13() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___force_13)); }
	inline FsmFloat_t2134102846 * get_force_13() const { return ___force_13; }
	inline FsmFloat_t2134102846 ** get_address_of_force_13() { return &___force_13; }
	inline void set_force_13(FsmFloat_t2134102846 * value)
	{
		___force_13 = value;
		Il2CppCodeGenWriteBarrier(&___force_13, value);
	}

	inline static int32_t get_offset_of_radius_14() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___radius_14)); }
	inline FsmFloat_t2134102846 * get_radius_14() const { return ___radius_14; }
	inline FsmFloat_t2134102846 ** get_address_of_radius_14() { return &___radius_14; }
	inline void set_radius_14(FsmFloat_t2134102846 * value)
	{
		___radius_14 = value;
		Il2CppCodeGenWriteBarrier(&___radius_14, value);
	}

	inline static int32_t get_offset_of_upwardsModifier_15() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___upwardsModifier_15)); }
	inline FsmFloat_t2134102846 * get_upwardsModifier_15() const { return ___upwardsModifier_15; }
	inline FsmFloat_t2134102846 ** get_address_of_upwardsModifier_15() { return &___upwardsModifier_15; }
	inline void set_upwardsModifier_15(FsmFloat_t2134102846 * value)
	{
		___upwardsModifier_15 = value;
		Il2CppCodeGenWriteBarrier(&___upwardsModifier_15, value);
	}

	inline static int32_t get_offset_of_forceMode_16() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___forceMode_16)); }
	inline int32_t get_forceMode_16() const { return ___forceMode_16; }
	inline int32_t* get_address_of_forceMode_16() { return &___forceMode_16; }
	inline void set_forceMode_16(int32_t value)
	{
		___forceMode_16 = value;
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3285275685, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
