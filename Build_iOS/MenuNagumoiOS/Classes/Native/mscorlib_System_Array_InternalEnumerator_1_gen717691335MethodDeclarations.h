﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<MagicTV.vo.FileVO>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2412711952(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t717691335 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MagicTV.vo.FileVO>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m732072848(__this, method) ((  void (*) (InternalEnumerator_1_t717691335 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MagicTV.vo.FileVO>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1219815622(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t717691335 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MagicTV.vo.FileVO>::Dispose()
#define InternalEnumerator_1_Dispose_m2553179623(__this, method) ((  void (*) (InternalEnumerator_1_t717691335 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MagicTV.vo.FileVO>::MoveNext()
#define InternalEnumerator_1_MoveNext_m967686080(__this, method) ((  bool (*) (InternalEnumerator_1_t717691335 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MagicTV.vo.FileVO>::get_Current()
#define InternalEnumerator_1_get_Current_m1197046393(__this, method) ((  FileVO_t1935348659 * (*) (InternalEnumerator_1_t717691335 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
