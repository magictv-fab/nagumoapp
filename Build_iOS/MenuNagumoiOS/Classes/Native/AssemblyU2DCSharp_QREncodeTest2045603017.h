﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// QRCodeEncodeController
struct QRCodeEncodeController_t2317858336;
// UnityEngine.UI.RawImage
struct RawImage_t821930207;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QREncodeTest
struct  QREncodeTest_t2045603017  : public MonoBehaviour_t667441552
{
public:
	// QRCodeEncodeController QREncodeTest::e_qrController
	QRCodeEncodeController_t2317858336 * ___e_qrController_2;
	// UnityEngine.UI.RawImage QREncodeTest::qrCodeImage
	RawImage_t821930207 * ___qrCodeImage_3;
	// UnityEngine.UI.InputField QREncodeTest::m_inputfield
	InputField_t609046876 * ___m_inputfield_4;
	// UnityEngine.UI.Text QREncodeTest::infoText
	Text_t9039225 * ___infoText_5;

public:
	inline static int32_t get_offset_of_e_qrController_2() { return static_cast<int32_t>(offsetof(QREncodeTest_t2045603017, ___e_qrController_2)); }
	inline QRCodeEncodeController_t2317858336 * get_e_qrController_2() const { return ___e_qrController_2; }
	inline QRCodeEncodeController_t2317858336 ** get_address_of_e_qrController_2() { return &___e_qrController_2; }
	inline void set_e_qrController_2(QRCodeEncodeController_t2317858336 * value)
	{
		___e_qrController_2 = value;
		Il2CppCodeGenWriteBarrier(&___e_qrController_2, value);
	}

	inline static int32_t get_offset_of_qrCodeImage_3() { return static_cast<int32_t>(offsetof(QREncodeTest_t2045603017, ___qrCodeImage_3)); }
	inline RawImage_t821930207 * get_qrCodeImage_3() const { return ___qrCodeImage_3; }
	inline RawImage_t821930207 ** get_address_of_qrCodeImage_3() { return &___qrCodeImage_3; }
	inline void set_qrCodeImage_3(RawImage_t821930207 * value)
	{
		___qrCodeImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___qrCodeImage_3, value);
	}

	inline static int32_t get_offset_of_m_inputfield_4() { return static_cast<int32_t>(offsetof(QREncodeTest_t2045603017, ___m_inputfield_4)); }
	inline InputField_t609046876 * get_m_inputfield_4() const { return ___m_inputfield_4; }
	inline InputField_t609046876 ** get_address_of_m_inputfield_4() { return &___m_inputfield_4; }
	inline void set_m_inputfield_4(InputField_t609046876 * value)
	{
		___m_inputfield_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_inputfield_4, value);
	}

	inline static int32_t get_offset_of_infoText_5() { return static_cast<int32_t>(offsetof(QREncodeTest_t2045603017, ___infoText_5)); }
	inline Text_t9039225 * get_infoText_5() const { return ___infoText_5; }
	inline Text_t9039225 ** get_address_of_infoText_5() { return &___infoText_5; }
	inline void set_infoText_5(Text_t9039225 * value)
	{
		___infoText_5 = value;
		Il2CppCodeGenWriteBarrier(&___infoText_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
