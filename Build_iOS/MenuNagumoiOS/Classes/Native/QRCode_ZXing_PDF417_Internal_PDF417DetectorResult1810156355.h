﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Collections.Generic.List`1<ZXing.ResultPoint[]>
struct List_1_t2563349896;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417DetectorResult
struct  PDF417DetectorResult_t1810156355  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.PDF417.Internal.PDF417DetectorResult::<Bits>k__BackingField
	BitMatrix_t1058711404 * ___U3CBitsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.PDF417DetectorResult::<Points>k__BackingField
	List_1_t2563349896 * ___U3CPointsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBitsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PDF417DetectorResult_t1810156355, ___U3CBitsU3Ek__BackingField_0)); }
	inline BitMatrix_t1058711404 * get_U3CBitsU3Ek__BackingField_0() const { return ___U3CBitsU3Ek__BackingField_0; }
	inline BitMatrix_t1058711404 ** get_address_of_U3CBitsU3Ek__BackingField_0() { return &___U3CBitsU3Ek__BackingField_0; }
	inline void set_U3CBitsU3Ek__BackingField_0(BitMatrix_t1058711404 * value)
	{
		___U3CBitsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBitsU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CPointsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PDF417DetectorResult_t1810156355, ___U3CPointsU3Ek__BackingField_1)); }
	inline List_1_t2563349896 * get_U3CPointsU3Ek__BackingField_1() const { return ___U3CPointsU3Ek__BackingField_1; }
	inline List_1_t2563349896 ** get_address_of_U3CPointsU3Ek__BackingField_1() { return &___U3CPointsU3Ek__BackingField_1; }
	inline void set_U3CPointsU3Ek__BackingField_1(List_1_t2563349896 * value)
	{
		___U3CPointsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPointsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
