﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransformEvents/OnBundleIDEventHandler
struct OnBundleIDEventHandler_t3610444662;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TransformEvents/OnBundleIDEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnBundleIDEventHandler__ctor_m3437629213 (OnBundleIDEventHandler_t3610444662 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents/OnBundleIDEventHandler::Invoke(System.Int32)
extern "C"  void OnBundleIDEventHandler_Invoke_m2820194440 (OnBundleIDEventHandler_t3610444662 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TransformEvents/OnBundleIDEventHandler::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnBundleIDEventHandler_BeginInvoke_m3443109121 (OnBundleIDEventHandler_t3610444662 * __this, int32_t ___value0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents/OnBundleIDEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnBundleIDEventHandler_EndInvoke_m1898944429 (OnBundleIDEventHandler_t3610444662 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
