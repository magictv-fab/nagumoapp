﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.PDF417ResultMetadata
struct  PDF417ResultMetadata_t2693798038  : public Il2CppObject
{
public:
	// System.Int32 ZXing.PDF417.PDF417ResultMetadata::<SegmentIndex>k__BackingField
	int32_t ___U3CSegmentIndexU3Ek__BackingField_0;
	// System.String ZXing.PDF417.PDF417ResultMetadata::<FileId>k__BackingField
	String_t* ___U3CFileIdU3Ek__BackingField_1;
	// System.Int32[] ZXing.PDF417.PDF417ResultMetadata::<OptionalData>k__BackingField
	Int32U5BU5D_t3230847821* ___U3COptionalDataU3Ek__BackingField_2;
	// System.Boolean ZXing.PDF417.PDF417ResultMetadata::<IsLastSegment>k__BackingField
	bool ___U3CIsLastSegmentU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSegmentIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t2693798038, ___U3CSegmentIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3CSegmentIndexU3Ek__BackingField_0() const { return ___U3CSegmentIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CSegmentIndexU3Ek__BackingField_0() { return &___U3CSegmentIndexU3Ek__BackingField_0; }
	inline void set_U3CSegmentIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3CSegmentIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFileIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t2693798038, ___U3CFileIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CFileIdU3Ek__BackingField_1() const { return ___U3CFileIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFileIdU3Ek__BackingField_1() { return &___U3CFileIdU3Ek__BackingField_1; }
	inline void set_U3CFileIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CFileIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFileIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3COptionalDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t2693798038, ___U3COptionalDataU3Ek__BackingField_2)); }
	inline Int32U5BU5D_t3230847821* get_U3COptionalDataU3Ek__BackingField_2() const { return ___U3COptionalDataU3Ek__BackingField_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3COptionalDataU3Ek__BackingField_2() { return &___U3COptionalDataU3Ek__BackingField_2; }
	inline void set_U3COptionalDataU3Ek__BackingField_2(Int32U5BU5D_t3230847821* value)
	{
		___U3COptionalDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COptionalDataU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CIsLastSegmentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t2693798038, ___U3CIsLastSegmentU3Ek__BackingField_3)); }
	inline bool get_U3CIsLastSegmentU3Ek__BackingField_3() const { return ___U3CIsLastSegmentU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsLastSegmentU3Ek__BackingField_3() { return &___U3CIsLastSegmentU3Ek__BackingField_3; }
	inline void set_U3CIsLastSegmentU3Ek__BackingField_3(bool value)
	{
		___U3CIsLastSegmentU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
