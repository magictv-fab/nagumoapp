﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGlobals
struct PlayMakerGlobals_t3097244096;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// HutongGames.PlayMaker.NamedVariable[]
struct NamedVariableU5BU5D_t2180779430;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// System.Type
struct Type_t;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3689162173;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t120994540;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t607182279;
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t4223261083;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t1443435833;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t530285832;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t1706220122;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t3873466740;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1409029100;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t997957744;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "mscorlib_System_Type2863145774.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "mscorlib_System_String7231557.h"

// PlayMakerGlobals HutongGames.PlayMaker.FsmVariables::get_GlobalsComponent()
extern "C"  PlayMakerGlobals_t3097244096 * FsmVariables_get_GlobalsComponent_m418589409 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmVariables::get_GlobalVariables()
extern "C"  FsmVariables_t963491929 * FsmVariables_get_GlobalVariables_m2551687717 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVariables::get_GlobalVariablesSynced()
extern "C"  bool FsmVariables_get_GlobalVariablesSynced_m2820414597 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_GlobalVariablesSynced(System.Boolean)
extern "C"  void FsmVariables_set_GlobalVariablesSynced_m997765184 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.FsmVariables::GetAllNamedVariables()
extern "C"  NamedVariableU5BU5D_t2180779430* FsmVariables_GetAllNamedVariables_m2060110715 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVariables::Contains(HutongGames.PlayMaker.NamedVariable)
extern "C"  bool FsmVariables_Contains_m19528552 (FsmVariables_t963491929 * __this, NamedVariable_t3211770239 * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.FsmVariables::GetNames(System.Type)
extern "C"  NamedVariableU5BU5D_t2180779430* FsmVariables_GetNames_m1637852725 (FsmVariables_t963491929 * __this, Type_t * ___ofType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor()
extern "C"  void FsmVariables__ctor_m3149252762 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables__ctor_m3281811991 (FsmVariables_t963491929 * __this, FsmVariables_t963491929 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::OverrideVariableValues(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables_OverrideVariableValues_m474538833 (FsmVariables_t963491929 * __this, FsmVariables_t963491929 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::ApplyVariableValues(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables_ApplyVariableValues_m4201147201 (FsmVariables_t963491929 * __this, FsmVariables_t963491929 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::get_FloatVariables()
extern "C"  FsmFloatU5BU5D_t2945380875* FsmVariables_get_FloatVariables_m1157149957 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_FloatVariables(HutongGames.PlayMaker.FsmFloat[])
extern "C"  void FsmVariables_set_FloatVariables_m4123394552 (FsmVariables_t963491929 * __this, FsmFloatU5BU5D_t2945380875* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::get_IntVariables()
extern "C"  FsmIntU5BU5D_t1976821196* FsmVariables_get_IntVariables_m1527799903 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_IntVariables(HutongGames.PlayMaker.FsmInt[])
extern "C"  void FsmVariables_set_IntVariables_m303406098 (FsmVariables_t963491929 * __this, FsmIntU5BU5D_t1976821196* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::get_BoolVariables()
extern "C"  FsmBoolU5BU5D_t3689162173* FsmVariables_get_BoolVariables_m4286069353 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_BoolVariables(HutongGames.PlayMaker.FsmBool[])
extern "C"  void FsmVariables_set_BoolVariables_m3279295114 (FsmVariables_t963491929 * __this, FsmBoolU5BU5D_t3689162173* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::get_StringVariables()
extern "C"  FsmStringU5BU5D_t2523845914* FsmVariables_get_StringVariables_m3261297161 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_StringVariables(HutongGames.PlayMaker.FsmString[])
extern "C"  void FsmVariables_set_StringVariables_m3741660650 (FsmVariables_t963491929 * __this, FsmStringU5BU5D_t2523845914* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::get_Vector2Variables()
extern "C"  FsmVector2U5BU5D_t120994540* FsmVariables_get_Vector2Variables_m862200095 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_Vector2Variables(HutongGames.PlayMaker.FsmVector2[])
extern "C"  void FsmVariables_set_Vector2Variables_m3758021970 (FsmVariables_t963491929 * __this, FsmVector2U5BU5D_t120994540* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::get_Vector3Variables()
extern "C"  FsmVector3U5BU5D_t607182279* FsmVariables_get_Vector3Variables_m3478402589 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_Vector3Variables(HutongGames.PlayMaker.FsmVector3[])
extern "C"  void FsmVariables_set_Vector3Variables_m2574704464 (FsmVariables_t963491929 * __this, FsmVector3U5BU5D_t607182279* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect[] HutongGames.PlayMaker.FsmVariables::get_RectVariables()
extern "C"  FsmRectU5BU5D_t4223261083* FsmVariables_get_RectVariables_m314404265 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_RectVariables(HutongGames.PlayMaker.FsmRect[])
extern "C"  void FsmVariables_set_RectVariables_m2487984074 (FsmVariables_t963491929 * __this, FsmRectU5BU5D_t4223261083* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::get_QuaternionVariables()
extern "C"  FsmQuaternionU5BU5D_t1443435833* FsmVariables_get_QuaternionVariables_m1625098601 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_QuaternionVariables(HutongGames.PlayMaker.FsmQuaternion[])
extern "C"  void FsmVariables_set_QuaternionVariables_m4078371786 (FsmVariables_t963491929 * __this, FsmQuaternionU5BU5D_t1443435833* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::get_ColorVariables()
extern "C"  FsmColorU5BU5D_t530285832* FsmVariables_get_ColorVariables_m3443159095 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_ColorVariables(HutongGames.PlayMaker.FsmColor[])
extern "C"  void FsmVariables_set_ColorVariables_m3457464362 (FsmVariables_t963491929 * __this, FsmColorU5BU5D_t530285832* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.FsmVariables::get_GameObjectVariables()
extern "C"  FsmGameObjectU5BU5D_t1706220122* FsmVariables_get_GameObjectVariables_m685069193 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_GameObjectVariables(HutongGames.PlayMaker.FsmGameObject[])
extern "C"  void FsmVariables_set_GameObjectVariables_m2666647786 (FsmVariables_t963491929 * __this, FsmGameObjectU5BU5D_t1706220122* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.FsmVariables::get_ObjectVariables()
extern "C"  FsmObjectU5BU5D_t3873466740* FsmVariables_get_ObjectVariables_m2841354313 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_ObjectVariables(HutongGames.PlayMaker.FsmObject[])
extern "C"  void FsmVariables_set_ObjectVariables_m284799530 (FsmVariables_t963491929 * __this, FsmObjectU5BU5D_t3873466740* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.FsmVariables::get_MaterialVariables()
extern "C"  FsmMaterialU5BU5D_t1409029100* FsmVariables_get_MaterialVariables_m135826057 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_MaterialVariables(HutongGames.PlayMaker.FsmMaterial[])
extern "C"  void FsmVariables_set_MaterialVariables_m2017323306 (FsmVariables_t963491929 * __this, FsmMaterialU5BU5D_t1409029100* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.FsmVariables::get_TextureVariables()
extern "C"  FsmTextureU5BU5D_t997957744* FsmVariables_get_TextureVariables_m4203659655 (FsmVariables_t963491929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_TextureVariables(HutongGames.PlayMaker.FsmTexture[])
extern "C"  void FsmVariables_set_TextureVariables_m1275287994 (FsmVariables_t963491929 * __this, FsmTextureU5BU5D_t997957744* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVariables::GetVariable(System.String)
extern "C"  NamedVariable_t3211770239 * FsmVariables_GetVariable_m1409842114 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmVariables::GetFsmFloat(System.String)
extern "C"  FsmFloat_t2134102846 * FsmVariables_GetFsmFloat_m1258803409 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmVariables::GetFsmObject(System.String)
extern "C"  FsmObject_t821476169 * FsmVariables_GetFsmObject_m678224879 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.FsmVariables::GetFsmMaterial(System.String)
extern "C"  FsmMaterial_t924399665 * FsmVariables_GetFsmMaterial_m2644826863 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.FsmVariables::GetFsmTexture(System.String)
extern "C"  FsmTexture_t3073272573 * FsmVariables_GetFsmTexture_m3423405971 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmVariables::GetFsmInt(System.String)
extern "C"  FsmInt_t1596138449 * FsmVariables_GetFsmInt_m1904423915 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmVariables::GetFsmBool(System.String)
extern "C"  FsmBool_t1075959796 * FsmVariables_GetFsmBool_m2932937039 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmVariables::GetFsmString(System.String)
extern "C"  FsmString_t952858651 * FsmVariables_GetFsmString_m4055225775 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmVariables::GetFsmVector2(System.String)
extern "C"  FsmVector2_t533912881 * FsmVariables_GetFsmVector2_m3581467435 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmVariables::GetFsmVector3(System.String)
extern "C"  FsmVector3_t533912882 * FsmVariables_GetFsmVector3_m557465897 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmVariables::GetFsmRect(System.String)
extern "C"  FsmRect_t1076426478 * FsmVariables_GetFsmRect_m1774888079 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmVariables::GetFsmQuaternion(System.String)
extern "C"  FsmQuaternion_t3871136040 * FsmVariables_GetFsmQuaternion_m3924878991 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmVariables::GetFsmColor(System.String)
extern "C"  FsmColor_t2131419205 * FsmVariables_GetFsmColor_m414491395 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmVariables::GetFsmGameObject(System.String)
extern "C"  FsmGameObject_t1697147867 * FsmVariables_GetFsmGameObject_m175579311 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::LogMissingGlobalVariable(System.String)
extern "C"  void FsmVariables_LogMissingGlobalVariable_m604575959 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVariables::FindVariable(System.String)
extern "C"  NamedVariable_t3211770239 * FsmVariables_FindVariable_m1706986425 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmVariables::FindFsmFloat(System.String)
extern "C"  FsmFloat_t2134102846 * FsmVariables_FindFsmFloat_m4255929226 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmVariables::FindFsmObject(System.String)
extern "C"  FsmObject_t821476169 * FsmVariables_FindFsmObject_m823222660 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.FsmVariables::FindFsmMaterial(System.String)
extern "C"  FsmMaterial_t924399665 * FsmVariables_FindFsmMaterial_m3429700084 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.FsmVariables::FindFsmTexture(System.String)
extern "C"  FsmTexture_t3073272573 * FsmVariables_FindFsmTexture_m3849299818 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmVariables::FindFsmInt(System.String)
extern "C"  FsmInt_t1596138449 * FsmVariables_FindFsmInt_m2978961002 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmVariables::FindFsmBool(System.String)
extern "C"  FsmBool_t1075959796 * FsmVariables_FindFsmBool_m4215612206 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmVariables::FindFsmString(System.String)
extern "C"  FsmString_t952858651 * FsmVariables_FindFsmString_m3304428512 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmVariables::FindFsmVector2(System.String)
extern "C"  FsmVector2_t533912881 * FsmVariables_FindFsmVector2_m3804699626 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmVariables::FindFsmVector3(System.String)
extern "C"  FsmVector3_t533912882 * FsmVariables_FindFsmVector3_m2686088586 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmVariables::FindFsmRect(System.String)
extern "C"  FsmRect_t1076426478 * FsmVariables_FindFsmRect_m1360779962 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmVariables::FindFsmQuaternion(System.String)
extern "C"  FsmQuaternion_t3871136040 * FsmVariables_FindFsmQuaternion_m3100239814 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmVariables::FindFsmColor(System.String)
extern "C"  FsmColor_t2131419205 * FsmVariables_FindFsmColor_m2841775274 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmVariables::FindFsmGameObject(System.String)
extern "C"  FsmGameObject_t1697147867 * FsmVariables_FindFsmGameObject_m2163832288 (FsmVariables_t963491929 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
