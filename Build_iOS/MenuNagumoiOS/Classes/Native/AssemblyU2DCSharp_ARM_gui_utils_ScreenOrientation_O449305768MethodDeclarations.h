﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler
struct OnScreenOrientationEventHandler_t449305768;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientationT3688806624.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnScreenOrientationEventHandler__ctor_m300294783 (OnScreenOrientationEventHandler_t449305768 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler::Invoke(UnityEngine.Vector2,ARM.gui.utils.ScreenOrientationType)
extern "C"  void OnScreenOrientationEventHandler_Invoke_m1186883985 (OnScreenOrientationEventHandler_t449305768 * __this, Vector2_t4282066565  ___screenSize0, int32_t ___screenOrientationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler::BeginInvoke(UnityEngine.Vector2,ARM.gui.utils.ScreenOrientationType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnScreenOrientationEventHandler_BeginInvoke_m488565070 (OnScreenOrientationEventHandler_t449305768 * __this, Vector2_t4282066565  ___screenSize0, int32_t ___screenOrientationType1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnScreenOrientationEventHandler_EndInvoke_m1098165263 (OnScreenOrientationEventHandler_t449305768 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
