﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TakeScreenshot
struct  TakeScreenshot_t3276931125  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.TakeScreenshot::filename
	FsmString_t952858651 * ___filename_9;
	// System.Boolean HutongGames.PlayMaker.Actions.TakeScreenshot::autoNumber
	bool ___autoNumber_10;
	// System.Int32 HutongGames.PlayMaker.Actions.TakeScreenshot::screenshotCount
	int32_t ___screenshotCount_11;

public:
	inline static int32_t get_offset_of_filename_9() { return static_cast<int32_t>(offsetof(TakeScreenshot_t3276931125, ___filename_9)); }
	inline FsmString_t952858651 * get_filename_9() const { return ___filename_9; }
	inline FsmString_t952858651 ** get_address_of_filename_9() { return &___filename_9; }
	inline void set_filename_9(FsmString_t952858651 * value)
	{
		___filename_9 = value;
		Il2CppCodeGenWriteBarrier(&___filename_9, value);
	}

	inline static int32_t get_offset_of_autoNumber_10() { return static_cast<int32_t>(offsetof(TakeScreenshot_t3276931125, ___autoNumber_10)); }
	inline bool get_autoNumber_10() const { return ___autoNumber_10; }
	inline bool* get_address_of_autoNumber_10() { return &___autoNumber_10; }
	inline void set_autoNumber_10(bool value)
	{
		___autoNumber_10 = value;
	}

	inline static int32_t get_offset_of_screenshotCount_11() { return static_cast<int32_t>(offsetof(TakeScreenshot_t3276931125, ___screenshotCount_11)); }
	inline int32_t get_screenshotCount_11() const { return ___screenshotCount_11; }
	inline int32_t* get_address_of_screenshotCount_11() { return &___screenshotCount_11; }
	inline void set_screenshotCount_11(int32_t value)
	{
		___screenshotCount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
