﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.FinderPatternInfo
struct FinderPatternInfo_t3964498398;
// ZXing.QrCode.Internal.FinderPattern[]
struct FinderPatternU5BU5D_t1610216241;
// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t4119758992;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.FinderPatternInfo::.ctor(ZXing.QrCode.Internal.FinderPattern[])
extern "C"  void FinderPatternInfo__ctor_m3718949843 (FinderPatternInfo_t3964498398 * __this, FinderPatternU5BU5D_t1610216241* ___patternCenters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_BottomLeft()
extern "C"  FinderPattern_t4119758992 * FinderPatternInfo_get_BottomLeft_m2439786285 (FinderPatternInfo_t3964498398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_TopLeft()
extern "C"  FinderPattern_t4119758992 * FinderPatternInfo_get_TopLeft_m1909801315 (FinderPatternInfo_t3964498398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_TopRight()
extern "C"  FinderPattern_t4119758992 * FinderPatternInfo_get_TopRight_m219509090 (FinderPatternInfo_t3964498398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
