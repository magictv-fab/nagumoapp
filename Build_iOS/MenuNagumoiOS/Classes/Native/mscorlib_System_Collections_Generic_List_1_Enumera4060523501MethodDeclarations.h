﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t4040850731;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4060523501.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m58948428_gshared (Enumerator_t4060523501 * __this, List_1_t4040850731 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m58948428(__this, ___l0, method) ((  void (*) (Enumerator_t4060523501 *, List_1_t4040850731 *, const MethodInfo*))Enumerator__ctor_m58948428_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2166798726_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2166798726(__this, method) ((  void (*) (Enumerator_t4060523501 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2166798726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m341195762_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m341195762(__this, method) ((  Il2CppObject * (*) (Enumerator_t4060523501 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m341195762_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::Dispose()
extern "C"  void Enumerator_Dispose_m82484273_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m82484273(__this, method) ((  void (*) (Enumerator_t4060523501 *, const MethodInfo*))Enumerator_Dispose_m82484273_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2183322346_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2183322346(__this, method) ((  void (*) (Enumerator_t4060523501 *, const MethodInfo*))Enumerator_VerifyState_m2183322346_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1548774386_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1548774386(__this, method) ((  bool (*) (Enumerator_t4060523501 *, const MethodInfo*))Enumerator_MoveNext_m1548774386_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m689559969_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m689559969(__this, method) ((  int32_t (*) (Enumerator_t4060523501 *, const MethodInfo*))Enumerator_get_Current_m689559969_gshared)(__this, method)
