﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyObject
struct  DestroyObject_t317818279  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DestroyObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DestroyObject::delay
	FsmFloat_t2134102846 * ___delay_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DestroyObject::detachChildren
	FsmBool_t1075959796 * ___detachChildren_11;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(DestroyObject_t317818279, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_delay_10() { return static_cast<int32_t>(offsetof(DestroyObject_t317818279, ___delay_10)); }
	inline FsmFloat_t2134102846 * get_delay_10() const { return ___delay_10; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_10() { return &___delay_10; }
	inline void set_delay_10(FsmFloat_t2134102846 * value)
	{
		___delay_10 = value;
		Il2CppCodeGenWriteBarrier(&___delay_10, value);
	}

	inline static int32_t get_offset_of_detachChildren_11() { return static_cast<int32_t>(offsetof(DestroyObject_t317818279, ___detachChildren_11)); }
	inline FsmBool_t1075959796 * get_detachChildren_11() const { return ___detachChildren_11; }
	inline FsmBool_t1075959796 ** get_address_of_detachChildren_11() { return &___detachChildren_11; }
	inline void set_detachChildren_11(FsmBool_t1075959796 * value)
	{
		___detachChildren_11 = value;
		Il2CppCodeGenWriteBarrier(&___detachChildren_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
