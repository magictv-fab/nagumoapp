﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.DataMatrixWriter
struct DataMatrixWriter_t1731856302;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// ZXing.Datamatrix.Encoder.DefaultPlacement
struct DefaultPlacement_t3907996320;
// ZXing.Datamatrix.Encoder.SymbolInfo
struct SymbolInfo_t553159586;
// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_Datamatrix_Encoder_DefaultPlacement3907996320.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolInfo553159586.h"
#include "QRCode_ZXing_QrCode_Internal_ByteMatrix2072255685.h"

// ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * DataMatrixWriter_encode_m3818244658 (DataMatrixWriter_t1731856302 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encodeLowLevel(ZXing.Datamatrix.Encoder.DefaultPlacement,ZXing.Datamatrix.Encoder.SymbolInfo)
extern "C"  BitMatrix_t1058711404 * DataMatrixWriter_encodeLowLevel_m2554776816 (Il2CppObject * __this /* static, unused */, DefaultPlacement_t3907996320 * ___placement0, SymbolInfo_t553159586 * ___symbolInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::convertByteMatrixToBitMatrix(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  BitMatrix_t1058711404 * DataMatrixWriter_convertByteMatrixToBitMatrix_m201588475 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.DataMatrixWriter::.ctor()
extern "C"  void DataMatrixWriter__ctor_m974837172 (DataMatrixWriter_t1731856302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
