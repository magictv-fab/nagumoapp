﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "AssemblyU2DCSharp_MagicTVAbstractWindowPopUpContro2050906514.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVAbstractWindowConfirmControllerScript
struct  MagicTVAbstractWindowConfirmControllerScript_t227123142  : public MagicTVAbstractWindowPopUpControllerScript_t2050906514
{
public:
	// System.Action MagicTVAbstractWindowConfirmControllerScript::OnClickCancel
	Action_t3771233898 * ___OnClickCancel_9;
	// UnityEngine.UI.Text MagicTVAbstractWindowConfirmControllerScript::LabelButtonCancel
	Text_t9039225 * ___LabelButtonCancel_10;

public:
	inline static int32_t get_offset_of_OnClickCancel_9() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowConfirmControllerScript_t227123142, ___OnClickCancel_9)); }
	inline Action_t3771233898 * get_OnClickCancel_9() const { return ___OnClickCancel_9; }
	inline Action_t3771233898 ** get_address_of_OnClickCancel_9() { return &___OnClickCancel_9; }
	inline void set_OnClickCancel_9(Action_t3771233898 * value)
	{
		___OnClickCancel_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnClickCancel_9, value);
	}

	inline static int32_t get_offset_of_LabelButtonCancel_10() { return static_cast<int32_t>(offsetof(MagicTVAbstractWindowConfirmControllerScript_t227123142, ___LabelButtonCancel_10)); }
	inline Text_t9039225 * get_LabelButtonCancel_10() const { return ___LabelButtonCancel_10; }
	inline Text_t9039225 ** get_address_of_LabelButtonCancel_10() { return &___LabelButtonCancel_10; }
	inline void set_LabelButtonCancel_10(Text_t9039225 * value)
	{
		___LabelButtonCancel_10 = value;
		Il2CppCodeGenWriteBarrier(&___LabelButtonCancel_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
