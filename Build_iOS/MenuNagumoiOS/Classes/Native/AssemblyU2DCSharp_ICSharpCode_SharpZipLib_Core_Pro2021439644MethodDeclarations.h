﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ProgressEventArgs
struct ProgressEventArgs_t2021439644;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.ProgressEventArgs::.ctor(System.String,System.Int64,System.Int64)
extern "C"  void ProgressEventArgs__ctor_m2299536723 (ProgressEventArgs_t2021439644 * __this, String_t* ___name0, int64_t ___processed1, int64_t ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Core.ProgressEventArgs::get_Name()
extern "C"  String_t* ProgressEventArgs_get_Name_m1541581644 (ProgressEventArgs_t2021439644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.ProgressEventArgs::get_ContinueRunning()
extern "C"  bool ProgressEventArgs_get_ContinueRunning_m3356277160 (ProgressEventArgs_t2021439644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProgressEventArgs::set_ContinueRunning(System.Boolean)
extern "C"  void ProgressEventArgs_set_ContinueRunning_m1097382239 (ProgressEventArgs_t2021439644 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ICSharpCode.SharpZipLib.Core.ProgressEventArgs::get_PercentComplete()
extern "C"  float ProgressEventArgs_get_PercentComplete_m2504044182 (ProgressEventArgs_t2021439644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Core.ProgressEventArgs::get_Processed()
extern "C"  int64_t ProgressEventArgs_get_Processed_m2427810147 (ProgressEventArgs_t2021439644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Core.ProgressEventArgs::get_Target()
extern "C"  int64_t ProgressEventArgs_get_Target_m1263796798 (ProgressEventArgs_t2021439644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
