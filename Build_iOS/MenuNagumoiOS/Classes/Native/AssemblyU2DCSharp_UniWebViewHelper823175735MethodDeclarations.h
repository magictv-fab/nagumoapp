﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebViewHelper
struct UniWebViewHelper_t823175735;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UniWebViewHelper::.ctor()
extern "C"  void UniWebViewHelper__ctor_m707463748 (UniWebViewHelper_t823175735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewHelper::StreamingAssetURLForPath(System.String)
extern "C"  String_t* UniWebViewHelper_StreamingAssetURLForPath_m2305237810 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewHelper::PersistentDataURLForPath(System.String)
extern "C"  String_t* UniWebViewHelper_PersistentDataURLForPath_m2199819103 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
