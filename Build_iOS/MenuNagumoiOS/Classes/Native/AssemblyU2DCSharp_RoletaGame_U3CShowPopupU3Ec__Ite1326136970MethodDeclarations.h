﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RoletaGame/<ShowPopup>c__Iterator7A
struct U3CShowPopupU3Ec__Iterator7A_t1326136970;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RoletaGame/<ShowPopup>c__Iterator7A::.ctor()
extern "C"  void U3CShowPopupU3Ec__Iterator7A__ctor_m1846329377 (U3CShowPopupU3Ec__Iterator7A_t1326136970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RoletaGame/<ShowPopup>c__Iterator7A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowPopupU3Ec__Iterator7A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3244611665 (U3CShowPopupU3Ec__Iterator7A_t1326136970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RoletaGame/<ShowPopup>c__Iterator7A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowPopupU3Ec__Iterator7A_System_Collections_IEnumerator_get_Current_m3142295013 (U3CShowPopupU3Ec__Iterator7A_t1326136970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RoletaGame/<ShowPopup>c__Iterator7A::MoveNext()
extern "C"  bool U3CShowPopupU3Ec__Iterator7A_MoveNext_m2950764595 (U3CShowPopupU3Ec__Iterator7A_t1326136970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame/<ShowPopup>c__Iterator7A::Dispose()
extern "C"  void U3CShowPopupU3Ec__Iterator7A_Dispose_m399321118 (U3CShowPopupU3Ec__Iterator7A_t1326136970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame/<ShowPopup>c__Iterator7A::Reset()
extern "C"  void U3CShowPopupU3Ec__Iterator7A_Reset_m3787729614 (U3CShowPopupU3Ec__Iterator7A_t1326136970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
