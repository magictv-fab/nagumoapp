﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.FixedPositionAngleFilter
struct  FixedPositionAngleFilter_t556916690  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.Vector3 ARM.transform.filters.FixedPositionAngleFilter::positionToLock
	Vector3_t4282066566  ___positionToLock_15;
	// UnityEngine.Vector3 ARM.transform.filters.FixedPositionAngleFilter::rotationToLock
	Vector3_t4282066566  ___rotationToLock_16;
	// UnityEngine.Quaternion ARM.transform.filters.FixedPositionAngleFilter::_lastHistoryRotation
	Quaternion_t1553702882  ____lastHistoryRotation_17;
	// UnityEngine.Vector3 ARM.transform.filters.FixedPositionAngleFilter::_lastHistoryPosition
	Vector3_t4282066566  ____lastHistoryPosition_18;
	// System.Boolean ARM.transform.filters.FixedPositionAngleFilter::keepHistory
	bool ___keepHistory_19;

public:
	inline static int32_t get_offset_of_positionToLock_15() { return static_cast<int32_t>(offsetof(FixedPositionAngleFilter_t556916690, ___positionToLock_15)); }
	inline Vector3_t4282066566  get_positionToLock_15() const { return ___positionToLock_15; }
	inline Vector3_t4282066566 * get_address_of_positionToLock_15() { return &___positionToLock_15; }
	inline void set_positionToLock_15(Vector3_t4282066566  value)
	{
		___positionToLock_15 = value;
	}

	inline static int32_t get_offset_of_rotationToLock_16() { return static_cast<int32_t>(offsetof(FixedPositionAngleFilter_t556916690, ___rotationToLock_16)); }
	inline Vector3_t4282066566  get_rotationToLock_16() const { return ___rotationToLock_16; }
	inline Vector3_t4282066566 * get_address_of_rotationToLock_16() { return &___rotationToLock_16; }
	inline void set_rotationToLock_16(Vector3_t4282066566  value)
	{
		___rotationToLock_16 = value;
	}

	inline static int32_t get_offset_of__lastHistoryRotation_17() { return static_cast<int32_t>(offsetof(FixedPositionAngleFilter_t556916690, ____lastHistoryRotation_17)); }
	inline Quaternion_t1553702882  get__lastHistoryRotation_17() const { return ____lastHistoryRotation_17; }
	inline Quaternion_t1553702882 * get_address_of__lastHistoryRotation_17() { return &____lastHistoryRotation_17; }
	inline void set__lastHistoryRotation_17(Quaternion_t1553702882  value)
	{
		____lastHistoryRotation_17 = value;
	}

	inline static int32_t get_offset_of__lastHistoryPosition_18() { return static_cast<int32_t>(offsetof(FixedPositionAngleFilter_t556916690, ____lastHistoryPosition_18)); }
	inline Vector3_t4282066566  get__lastHistoryPosition_18() const { return ____lastHistoryPosition_18; }
	inline Vector3_t4282066566 * get_address_of__lastHistoryPosition_18() { return &____lastHistoryPosition_18; }
	inline void set__lastHistoryPosition_18(Vector3_t4282066566  value)
	{
		____lastHistoryPosition_18 = value;
	}

	inline static int32_t get_offset_of_keepHistory_19() { return static_cast<int32_t>(offsetof(FixedPositionAngleFilter_t556916690, ___keepHistory_19)); }
	inline bool get_keepHistory_19() const { return ___keepHistory_19; }
	inline bool* get_address_of_keepHistory_19() { return &___keepHistory_19; }
	inline void set_keepHistory_19(bool value)
	{
		___keepHistory_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
