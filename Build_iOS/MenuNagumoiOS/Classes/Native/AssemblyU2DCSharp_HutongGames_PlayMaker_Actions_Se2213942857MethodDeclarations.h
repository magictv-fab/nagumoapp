﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorLookAt
struct SetAnimatorLookAt_t2213942857;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::.ctor()
extern "C"  void SetAnimatorLookAt__ctor_m1985972493 (SetAnimatorLookAt_t2213942857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::Reset()
extern "C"  void SetAnimatorLookAt_Reset_m3927372730 (SetAnimatorLookAt_t2213942857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::OnEnter()
extern "C"  void SetAnimatorLookAt_OnEnter_m945814948 (SetAnimatorLookAt_t2213942857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::OnAnimatorIKEvent(System.Int32)
extern "C"  void SetAnimatorLookAt_OnAnimatorIKEvent_m1514782318 (SetAnimatorLookAt_t2213942857 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::OnUpdate()
extern "C"  void SetAnimatorLookAt_OnUpdate_m2684018975 (SetAnimatorLookAt_t2213942857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::DoSetLookAt()
extern "C"  void SetAnimatorLookAt_DoSetLookAt_m4132899412 (SetAnimatorLookAt_t2213942857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLookAt::OnExit()
extern "C"  void SetAnimatorLookAt_OnExit_m2948923764 (SetAnimatorLookAt_t2213942857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
