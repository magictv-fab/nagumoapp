﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmObject
struct GetFsmObject_t2760364049;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::.ctor()
extern "C"  void GetFsmObject__ctor_m2342609269 (GetFsmObject_t2760364049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::Reset()
extern "C"  void GetFsmObject_Reset_m4284009506 (GetFsmObject_t2760364049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::OnEnter()
extern "C"  void GetFsmObject_OnEnter_m76373004 (GetFsmObject_t2760364049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::OnUpdate()
extern "C"  void GetFsmObject_OnUpdate_m1501122487 (GetFsmObject_t2760364049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::DoGetFsmVariable()
extern "C"  void GetFsmObject_DoGetFsmVariable_m555649024 (GetFsmObject_t2760364049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
