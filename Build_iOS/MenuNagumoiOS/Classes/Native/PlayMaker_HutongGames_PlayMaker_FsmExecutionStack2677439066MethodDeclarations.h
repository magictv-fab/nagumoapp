﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"

// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingFsm()
extern "C"  Fsm_t1527112426 * FsmExecutionStack_get_ExecutingFsm_m4145566551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingState()
extern "C"  FsmState_t2146334067 * FsmExecutionStack_get_ExecutingState_m657643011 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingStateName()
extern "C"  String_t* FsmExecutionStack_get_ExecutingStateName_m989458511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingAction()
extern "C"  FsmStateAction_t2366529033 * FsmExecutionStack_get_ExecutingAction_m3292914320 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmExecutionStack::get_StackCount()
extern "C"  int32_t FsmExecutionStack_get_StackCount_m933802045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmExecutionStack::get_MaxStackCount()
extern "C"  int32_t FsmExecutionStack_get_MaxStackCount_m525624439 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::set_MaxStackCount(System.Int32)
extern "C"  void FsmExecutionStack_set_MaxStackCount_m53218946 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PushFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmExecutionStack_PushFsm_m2702729589 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___executingFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PopFsm()
extern "C"  void FsmExecutionStack_PopFsm_m2354017614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::.cctor()
extern "C"  void FsmExecutionStack__cctor_m838087432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
