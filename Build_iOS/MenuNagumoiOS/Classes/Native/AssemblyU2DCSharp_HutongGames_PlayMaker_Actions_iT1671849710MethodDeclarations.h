﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateUpdate
struct iTweenRotateUpdate_t1671849710;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::.ctor()
extern "C"  void iTweenRotateUpdate__ctor_m1824768312 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::Reset()
extern "C"  void iTweenRotateUpdate_Reset_m3766168549 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnEnter()
extern "C"  void iTweenRotateUpdate_OnEnter_m647419663 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnExit()
extern "C"  void iTweenRotateUpdate_OnExit_m2246561449 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnUpdate()
extern "C"  void iTweenRotateUpdate_OnUpdate_m2023699732 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::DoiTween()
extern "C"  void iTweenRotateUpdate_DoiTween_m46305305 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
