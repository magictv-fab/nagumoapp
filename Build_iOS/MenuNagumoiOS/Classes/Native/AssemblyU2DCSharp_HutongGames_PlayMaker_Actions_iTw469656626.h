﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenMoveBy
struct  iTweenMoveBy_t469656626  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenMoveBy::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenMoveBy::id
	FsmString_t952858651 * ___id_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenMoveBy::vector
	FsmVector3_t533912882 * ___vector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveBy::time
	FsmFloat_t2134102846 * ___time_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveBy::delay
	FsmFloat_t2134102846 * ___delay_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveBy::speed
	FsmFloat_t2134102846 * ___speed_22;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenMoveBy::easeType
	int32_t ___easeType_23;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenMoveBy::loopType
	int32_t ___loopType_24;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenMoveBy::space
	int32_t ___space_25;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.iTweenMoveBy::orientToPath
	FsmBool_t1075959796 * ___orientToPath_26;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenMoveBy::lookAtObject
	FsmGameObject_t1697147867 * ___lookAtObject_27;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenMoveBy::lookAtVector
	FsmVector3_t533912882 * ___lookAtVector_28;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveBy::lookTime
	FsmFloat_t2134102846 * ___lookTime_29;
	// HutongGames.PlayMaker.Actions.iTweenFsmAction/AxisRestriction HutongGames.PlayMaker.Actions.iTweenMoveBy::axis
	int32_t ___axis_30;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_id_18() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___id_18)); }
	inline FsmString_t952858651 * get_id_18() const { return ___id_18; }
	inline FsmString_t952858651 ** get_address_of_id_18() { return &___id_18; }
	inline void set_id_18(FsmString_t952858651 * value)
	{
		___id_18 = value;
		Il2CppCodeGenWriteBarrier(&___id_18, value);
	}

	inline static int32_t get_offset_of_vector_19() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___vector_19)); }
	inline FsmVector3_t533912882 * get_vector_19() const { return ___vector_19; }
	inline FsmVector3_t533912882 ** get_address_of_vector_19() { return &___vector_19; }
	inline void set_vector_19(FsmVector3_t533912882 * value)
	{
		___vector_19 = value;
		Il2CppCodeGenWriteBarrier(&___vector_19, value);
	}

	inline static int32_t get_offset_of_time_20() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___time_20)); }
	inline FsmFloat_t2134102846 * get_time_20() const { return ___time_20; }
	inline FsmFloat_t2134102846 ** get_address_of_time_20() { return &___time_20; }
	inline void set_time_20(FsmFloat_t2134102846 * value)
	{
		___time_20 = value;
		Il2CppCodeGenWriteBarrier(&___time_20, value);
	}

	inline static int32_t get_offset_of_delay_21() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___delay_21)); }
	inline FsmFloat_t2134102846 * get_delay_21() const { return ___delay_21; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_21() { return &___delay_21; }
	inline void set_delay_21(FsmFloat_t2134102846 * value)
	{
		___delay_21 = value;
		Il2CppCodeGenWriteBarrier(&___delay_21, value);
	}

	inline static int32_t get_offset_of_speed_22() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___speed_22)); }
	inline FsmFloat_t2134102846 * get_speed_22() const { return ___speed_22; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_22() { return &___speed_22; }
	inline void set_speed_22(FsmFloat_t2134102846 * value)
	{
		___speed_22 = value;
		Il2CppCodeGenWriteBarrier(&___speed_22, value);
	}

	inline static int32_t get_offset_of_easeType_23() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___easeType_23)); }
	inline int32_t get_easeType_23() const { return ___easeType_23; }
	inline int32_t* get_address_of_easeType_23() { return &___easeType_23; }
	inline void set_easeType_23(int32_t value)
	{
		___easeType_23 = value;
	}

	inline static int32_t get_offset_of_loopType_24() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___loopType_24)); }
	inline int32_t get_loopType_24() const { return ___loopType_24; }
	inline int32_t* get_address_of_loopType_24() { return &___loopType_24; }
	inline void set_loopType_24(int32_t value)
	{
		___loopType_24 = value;
	}

	inline static int32_t get_offset_of_space_25() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___space_25)); }
	inline int32_t get_space_25() const { return ___space_25; }
	inline int32_t* get_address_of_space_25() { return &___space_25; }
	inline void set_space_25(int32_t value)
	{
		___space_25 = value;
	}

	inline static int32_t get_offset_of_orientToPath_26() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___orientToPath_26)); }
	inline FsmBool_t1075959796 * get_orientToPath_26() const { return ___orientToPath_26; }
	inline FsmBool_t1075959796 ** get_address_of_orientToPath_26() { return &___orientToPath_26; }
	inline void set_orientToPath_26(FsmBool_t1075959796 * value)
	{
		___orientToPath_26 = value;
		Il2CppCodeGenWriteBarrier(&___orientToPath_26, value);
	}

	inline static int32_t get_offset_of_lookAtObject_27() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___lookAtObject_27)); }
	inline FsmGameObject_t1697147867 * get_lookAtObject_27() const { return ___lookAtObject_27; }
	inline FsmGameObject_t1697147867 ** get_address_of_lookAtObject_27() { return &___lookAtObject_27; }
	inline void set_lookAtObject_27(FsmGameObject_t1697147867 * value)
	{
		___lookAtObject_27 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtObject_27, value);
	}

	inline static int32_t get_offset_of_lookAtVector_28() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___lookAtVector_28)); }
	inline FsmVector3_t533912882 * get_lookAtVector_28() const { return ___lookAtVector_28; }
	inline FsmVector3_t533912882 ** get_address_of_lookAtVector_28() { return &___lookAtVector_28; }
	inline void set_lookAtVector_28(FsmVector3_t533912882 * value)
	{
		___lookAtVector_28 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtVector_28, value);
	}

	inline static int32_t get_offset_of_lookTime_29() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___lookTime_29)); }
	inline FsmFloat_t2134102846 * get_lookTime_29() const { return ___lookTime_29; }
	inline FsmFloat_t2134102846 ** get_address_of_lookTime_29() { return &___lookTime_29; }
	inline void set_lookTime_29(FsmFloat_t2134102846 * value)
	{
		___lookTime_29 = value;
		Il2CppCodeGenWriteBarrier(&___lookTime_29, value);
	}

	inline static int32_t get_offset_of_axis_30() { return static_cast<int32_t>(offsetof(iTweenMoveBy_t469656626, ___axis_30)); }
	inline int32_t get_axis_30() const { return ___axis_30; }
	inline int32_t* get_address_of_axis_30() { return &___axis_30; }
	inline void set_axis_30(int32_t value)
	{
		___axis_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
