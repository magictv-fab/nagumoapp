﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidPermissionsUsageExample
struct AndroidPermissionsUsageExample_t3254568926;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AndroidPermissionsUsageExample::.ctor()
extern "C"  void AndroidPermissionsUsageExample__ctor_m119393917 (AndroidPermissionsUsageExample_t3254568926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionsUsageExample::Start()
extern "C"  void AndroidPermissionsUsageExample_Start_m3361499005 (AndroidPermissionsUsageExample_t3254568926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionsUsageExample::OnBrowseGalleryButtonPress()
extern "C"  void AndroidPermissionsUsageExample_OnBrowseGalleryButtonPress_m2459742031 (AndroidPermissionsUsageExample_t3254568926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidPermissionsUsageExample::CheckPermissions()
extern "C"  bool AndroidPermissionsUsageExample_CheckPermissions_m2906425143 (AndroidPermissionsUsageExample_t3254568926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionsUsageExample::OnGrantButtonPress()
extern "C"  void AndroidPermissionsUsageExample_OnGrantButtonPress_m854303003 (AndroidPermissionsUsageExample_t3254568926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionsUsageExample::<OnGrantButtonPress>m__81(System.String)
extern "C"  void AndroidPermissionsUsageExample_U3COnGrantButtonPressU3Em__81_m3246853903 (AndroidPermissionsUsageExample_t3254568926 * __this, String_t* ___grantedPermission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionsUsageExample::<OnGrantButtonPress>m__82(System.String)
extern "C"  void AndroidPermissionsUsageExample_U3COnGrantButtonPressU3Em__82_m2736319726 (Il2CppObject * __this /* static, unused */, String_t* ___deniedPermission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
