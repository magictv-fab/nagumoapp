﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// BackgroudCam
struct BackgroudCam_t4120779363;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroudCam/<openCamera>c__Iterator46
struct  U3CopenCameraU3Ec__Iterator46_t2801674036  : public Il2CppObject
{
public:
	// UnityEngine.WebCamDevice BackgroudCam/<openCamera>c__Iterator46::<device>__0
	WebCamDevice_t3274004757  ___U3CdeviceU3E__0_0;
	// System.Int32 BackgroudCam/<openCamera>c__Iterator46::$PC
	int32_t ___U24PC_1;
	// System.Object BackgroudCam/<openCamera>c__Iterator46::$current
	Il2CppObject * ___U24current_2;
	// BackgroudCam BackgroudCam/<openCamera>c__Iterator46::<>f__this
	BackgroudCam_t4120779363 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CdeviceU3E__0_0() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator46_t2801674036, ___U3CdeviceU3E__0_0)); }
	inline WebCamDevice_t3274004757  get_U3CdeviceU3E__0_0() const { return ___U3CdeviceU3E__0_0; }
	inline WebCamDevice_t3274004757 * get_address_of_U3CdeviceU3E__0_0() { return &___U3CdeviceU3E__0_0; }
	inline void set_U3CdeviceU3E__0_0(WebCamDevice_t3274004757  value)
	{
		___U3CdeviceU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator46_t2801674036, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator46_t2801674036, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator46_t2801674036, ___U3CU3Ef__this_3)); }
	inline BackgroudCam_t4120779363 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline BackgroudCam_t4120779363 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(BackgroudCam_t4120779363 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
