﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceCameraController
struct DeviceCameraController_t2043460407;
// DeviceCamera
struct DeviceCamera_t3055454523;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceCameraController::.ctor()
extern "C"  void DeviceCameraController__ctor_m2350458692 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DeviceCamera DeviceCameraController::get_dWebCam()
extern "C"  DeviceCamera_t3055454523 * DeviceCameraController_get_dWebCam_m3961265302 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceCameraController::get_isPlaying()
extern "C"  bool DeviceCameraController_get_isPlaying_m3513707913 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraController::Start()
extern "C"  void DeviceCameraController_Start_m1297596484 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraController::Update()
extern "C"  void DeviceCameraController_Update_m1576637513 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraController::StartWork()
extern "C"  void DeviceCameraController_StartWork_m3011090869 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraController::StopWork()
extern "C"  void DeviceCameraController_StopWork_m2378295155 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraController::correctScreenRatio()
extern "C"  void DeviceCameraController_correctScreenRatio_m2554090005 (DeviceCameraController_t2043460407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
