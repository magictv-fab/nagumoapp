﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QRDecodeTest
struct QRDecodeTest_t1329337505;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void QRDecodeTest::.ctor()
extern "C"  void QRDecodeTest__ctor_m4212294426 (QRDecodeTest_t1329337505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::Start()
extern "C"  void QRDecodeTest_Start_m3159432218 (QRDecodeTest_t1329337505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::Update()
extern "C"  void QRDecodeTest_Update_m3458970419 (QRDecodeTest_t1329337505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::qrScanFinished(System.String)
extern "C"  void QRDecodeTest_qrScanFinished_m995349224 (QRDecodeTest_t1329337505 * __this, String_t* ___dataText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::Reset()
extern "C"  void QRDecodeTest_Reset_m1858727367 (QRDecodeTest_t1329337505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::Play()
extern "C"  void QRDecodeTest_Play_m8645918 (QRDecodeTest_t1329337505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::Stop()
extern "C"  void QRDecodeTest_Stop_m102329964 (QRDecodeTest_t1329337505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRDecodeTest::GotoNextScene(System.String)
extern "C"  void QRDecodeTest_GotoNextScene_m2873756372 (QRDecodeTest_t1329337505 * __this, String_t* ___scenename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
