﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNIOSDialog
struct MNIOSDialog_t768531220;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNIOSDialog::.ctor()
extern "C"  void MNIOSDialog__ctor_m3063928535 (MNIOSDialog_t768531220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSDialog MNIOSDialog::Create(System.String,System.String)
extern "C"  MNIOSDialog_t768531220 * MNIOSDialog_Create_m1893431444 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSDialog MNIOSDialog::Create(System.String,System.String,System.String,System.String)
extern "C"  MNIOSDialog_t768531220 * MNIOSDialog_Create_m4041263628 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSDialog::init()
extern "C"  void MNIOSDialog_init_m689411197 (MNIOSDialog_t768531220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSDialog::onPopUpCallBack(System.String)
extern "C"  void MNIOSDialog_onPopUpCallBack_m3755611675 (MNIOSDialog_t768531220 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
