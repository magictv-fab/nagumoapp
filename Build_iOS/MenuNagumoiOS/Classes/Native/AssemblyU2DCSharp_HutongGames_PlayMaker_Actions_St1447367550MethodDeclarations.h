﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StartCoroutine
struct StartCoroutine_t1447367550;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::.ctor()
extern "C"  void StartCoroutine__ctor_m2390295208 (StartCoroutine_t1447367550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::Reset()
extern "C"  void StartCoroutine_Reset_m36728149 (StartCoroutine_t1447367550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::OnEnter()
extern "C"  void StartCoroutine_OnEnter_m2952887423 (StartCoroutine_t1447367550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::DoStartCoroutine()
extern "C"  void StartCoroutine_DoStartCoroutine_m3337068253 (StartCoroutine_t1447367550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::OnExit()
extern "C"  void StartCoroutine_OnExit_m2598026041 (StartCoroutine_t1447367550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
