﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlaySound
struct PlaySound_t2516791689;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlaySound::.ctor()
extern "C"  void PlaySound__ctor_m1956543949 (PlaySound_t2516791689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlaySound::Reset()
extern "C"  void PlaySound_Reset_m3897944186 (PlaySound_t2516791689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlaySound::OnEnter()
extern "C"  void PlaySound_OnEnter_m2729755236 (PlaySound_t2516791689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlaySound::DoPlaySound()
extern "C"  void PlaySound_DoPlaySound_m1401077083 (PlaySound_t2516791689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
