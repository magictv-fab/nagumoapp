﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Screenshot/<WaitFrameLock>c__Iterator2B
struct U3CWaitFrameLockU3Ec__Iterator2B_t264131251;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Screenshot/<WaitFrameLock>c__Iterator2B::.ctor()
extern "C"  void U3CWaitFrameLockU3Ec__Iterator2B__ctor_m1160484376 (U3CWaitFrameLockU3Ec__Iterator2B_t264131251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Screenshot/<WaitFrameLock>c__Iterator2B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitFrameLockU3Ec__Iterator2B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m51732794 (U3CWaitFrameLockU3Ec__Iterator2B_t264131251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Screenshot/<WaitFrameLock>c__Iterator2B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitFrameLockU3Ec__Iterator2B_System_Collections_IEnumerator_get_Current_m1351948494 (U3CWaitFrameLockU3Ec__Iterator2B_t264131251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Screenshot/<WaitFrameLock>c__Iterator2B::MoveNext()
extern "C"  bool U3CWaitFrameLockU3Ec__Iterator2B_MoveNext_m1688216796 (U3CWaitFrameLockU3Ec__Iterator2B_t264131251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot/<WaitFrameLock>c__Iterator2B::Dispose()
extern "C"  void U3CWaitFrameLockU3Ec__Iterator2B_Dispose_m2727238741 (U3CWaitFrameLockU3Ec__Iterator2B_t264131251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot/<WaitFrameLock>c__Iterator2B::Reset()
extern "C"  void U3CWaitFrameLockU3Ec__Iterator2B_Reset_m3101884613 (U3CWaitFrameLockU3Ec__Iterator2B_t264131251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
