﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va647158185MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3743131357(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1401552656 *, Dictionary_2_t2700946943 *, const MethodInfo*))ValueCollection__ctor_m2208833339_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2761147829(__this, ___item0, method) ((  void (*) (ValueCollection_t1401552656 *, OnChangeToAnStateEventHandler_t630243546 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1049086359_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m114480382(__this, method) ((  void (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1083709920_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3416785297(__this, ___item0, method) ((  bool (*) (ValueCollection_t1401552656 *, OnChangeToAnStateEventHandler_t630243546 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3585145011_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m755386998(__this, ___item0, method) ((  bool (*) (ValueCollection_t1401552656 *, OnChangeToAnStateEventHandler_t630243546 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1707516440_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4145427724(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m975201952_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2474233986(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1401552656 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1281352420_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2012539133(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1539681267_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1691961156(__this, method) ((  bool (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1860320870_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m96293668(__this, method) ((  bool (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2167827782_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2847489168(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3095027256_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3596213220(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1401552656 *, OnChangeToAnStateEventHandlerU5BU5D_t3921866367*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2278862018_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::GetEnumerator()
#define ValueCollection_GetEnumerator_m546779847(__this, method) ((  Enumerator_t632780351  (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_GetEnumerator_m845021867_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_Count()
#define ValueCollection_get_Count_m3591947626(__this, method) ((  int32_t (*) (ValueCollection_t1401552656 *, const MethodInfo*))ValueCollection_get_Count_m3263091328_gshared)(__this, method)
