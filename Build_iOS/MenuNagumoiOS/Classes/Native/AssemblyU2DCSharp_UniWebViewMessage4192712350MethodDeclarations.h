﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// UniWebViewMessage
struct UniWebViewMessage_t4192712350;
struct UniWebViewMessage_t4192712350_marshaled_pinvoke;
struct UniWebViewMessage_t4192712350_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UniWebViewMessage4192712350.h"
#include "mscorlib_System_String7231557.h"

// System.Void UniWebViewMessage::.ctor(System.String)
extern "C"  void UniWebViewMessage__ctor_m2795119509 (UniWebViewMessage_t4192712350 * __this, String_t* ___rawMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewMessage::get_RawMessage()
extern "C"  String_t* UniWebViewMessage_get_RawMessage_m4024052700 (UniWebViewMessage_t4192712350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewMessage::set_RawMessage(System.String)
extern "C"  void UniWebViewMessage_set_RawMessage_m108234383 (UniWebViewMessage_t4192712350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewMessage::get_Scheme()
extern "C"  String_t* UniWebViewMessage_get_Scheme_m4165905218 (UniWebViewMessage_t4192712350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewMessage::set_Scheme(System.String)
extern "C"  void UniWebViewMessage_set_Scheme_m1750145385 (UniWebViewMessage_t4192712350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewMessage::get_Path()
extern "C"  String_t* UniWebViewMessage_get_Path_m3277848770 (UniWebViewMessage_t4192712350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewMessage::set_Path(System.String)
extern "C"  void UniWebViewMessage_set_Path_m963131945 (UniWebViewMessage_t4192712350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UniWebViewMessage::get_Args()
extern "C"  Dictionary_2_t827649927 * UniWebViewMessage_get_Args_m784237455 (UniWebViewMessage_t4192712350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewMessage::set_Args(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UniWebViewMessage_set_Args_m553757410 (UniWebViewMessage_t4192712350 * __this, Dictionary_2_t827649927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct UniWebViewMessage_t4192712350;
struct UniWebViewMessage_t4192712350_marshaled_pinvoke;

extern "C" void UniWebViewMessage_t4192712350_marshal_pinvoke(const UniWebViewMessage_t4192712350& unmarshaled, UniWebViewMessage_t4192712350_marshaled_pinvoke& marshaled);
extern "C" void UniWebViewMessage_t4192712350_marshal_pinvoke_back(const UniWebViewMessage_t4192712350_marshaled_pinvoke& marshaled, UniWebViewMessage_t4192712350& unmarshaled);
extern "C" void UniWebViewMessage_t4192712350_marshal_pinvoke_cleanup(UniWebViewMessage_t4192712350_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct UniWebViewMessage_t4192712350;
struct UniWebViewMessage_t4192712350_marshaled_com;

extern "C" void UniWebViewMessage_t4192712350_marshal_com(const UniWebViewMessage_t4192712350& unmarshaled, UniWebViewMessage_t4192712350_marshaled_com& marshaled);
extern "C" void UniWebViewMessage_t4192712350_marshal_com_back(const UniWebViewMessage_t4192712350_marshaled_com& marshaled, UniWebViewMessage_t4192712350& unmarshaled);
extern "C" void UniWebViewMessage_t4192712350_marshal_com_cleanup(UniWebViewMessage_t4192712350_marshaled_com& marshaled);
