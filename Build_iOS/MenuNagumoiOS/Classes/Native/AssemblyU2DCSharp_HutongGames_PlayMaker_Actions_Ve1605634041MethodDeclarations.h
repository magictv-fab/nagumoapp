﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Interpolate
struct Vector3Interpolate_t1605634041;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::.ctor()
extern "C"  void Vector3Interpolate__ctor_m1587747981 (Vector3Interpolate_t1605634041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::Reset()
extern "C"  void Vector3Interpolate_Reset_m3529148218 (Vector3Interpolate_t1605634041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::OnEnter()
extern "C"  void Vector3Interpolate_OnEnter_m504148260 (Vector3Interpolate_t1605634041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::OnUpdate()
extern "C"  void Vector3Interpolate_OnUpdate_m1877253535 (Vector3Interpolate_t1605634041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
