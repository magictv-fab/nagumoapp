﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExchangeQuality
struct ExchangeQuality_t76539196;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExchangeQuality
struct  ExchangeQuality_t76539196  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// System.Int32 ExchangeQuality::_quality
	int32_t ____quality_9;

public:
	inline static int32_t get_offset_of__quality_9() { return static_cast<int32_t>(offsetof(ExchangeQuality_t76539196, ____quality_9)); }
	inline int32_t get__quality_9() const { return ____quality_9; }
	inline int32_t* get_address_of__quality_9() { return &____quality_9; }
	inline void set__quality_9(int32_t value)
	{
		____quality_9 = value;
	}
};

struct ExchangeQuality_t76539196_StaticFields
{
public:
	// ExchangeQuality ExchangeQuality::_instance
	ExchangeQuality_t76539196 * ____instance_8;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(ExchangeQuality_t76539196_StaticFields, ____instance_8)); }
	inline ExchangeQuality_t76539196 * get__instance_8() const { return ____instance_8; }
	inline ExchangeQuality_t76539196 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(ExchangeQuality_t76539196 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier(&____instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
