﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA
struct U3CILoadImgsU3Ec__IteratorA_t2371305409;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::.ctor()
extern "C"  void U3CILoadImgsU3Ec__IteratorA__ctor_m1784144890 (U3CILoadImgsU3Ec__IteratorA_t2371305409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m149434786 (U3CILoadImgsU3Ec__IteratorA_t2371305409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m2259007798 (U3CILoadImgsU3Ec__IteratorA_t2371305409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__IteratorA_MoveNext_m2260592034 (U3CILoadImgsU3Ec__IteratorA_t2371305409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::Dispose()
extern "C"  void U3CILoadImgsU3Ec__IteratorA_Dispose_m769571255 (U3CILoadImgsU3Ec__IteratorA_t2371305409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::Reset()
extern "C"  void U3CILoadImgsU3Ec__IteratorA_Reset_m3725545127 (U3CILoadImgsU3Ec__IteratorA_t2371305409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
