﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeValuesTest
struct ChangeValuesTest_t1677872324;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeValuesTest::.ctor()
extern "C"  void ChangeValuesTest__ctor_m3160602199 (ChangeValuesTest_t1677872324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeValuesTest::Start()
extern "C"  void ChangeValuesTest_Start_m2107739991 (ChangeValuesTest_t1677872324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeValuesTest::diminuir()
extern "C"  void ChangeValuesTest_diminuir_m2859214654 (ChangeValuesTest_t1677872324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeValuesTest::somar()
extern "C"  void ChangeValuesTest_somar_m310514839 (ChangeValuesTest_t1677872324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeValuesTest::Update()
extern "C"  void ChangeValuesTest_Update_m921282454 (ChangeValuesTest_t1677872324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
