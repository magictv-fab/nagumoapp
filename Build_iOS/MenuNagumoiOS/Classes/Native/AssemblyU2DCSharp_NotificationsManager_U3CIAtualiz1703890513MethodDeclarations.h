﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationsManager/<IAtualizaToken>c__Iterator21
struct U3CIAtualizaTokenU3Ec__Iterator21_t1703890513;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationsManager/<IAtualizaToken>c__Iterator21::.ctor()
extern "C"  void U3CIAtualizaTokenU3Ec__Iterator21__ctor_m797236586 (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NotificationsManager/<IAtualizaToken>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIAtualizaTokenU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2285311858 (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NotificationsManager/<IAtualizaToken>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIAtualizaTokenU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2744794886 (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NotificationsManager/<IAtualizaToken>c__Iterator21::MoveNext()
extern "C"  bool U3CIAtualizaTokenU3Ec__Iterator21_MoveNext_m1298223922 (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager/<IAtualizaToken>c__Iterator21::Dispose()
extern "C"  void U3CIAtualizaTokenU3Ec__Iterator21_Dispose_m1538463527 (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsManager/<IAtualizaToken>c__Iterator21::Reset()
extern "C"  void U3CIAtualizaTokenU3Ec__Iterator21_Reset_m2738636823 (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
