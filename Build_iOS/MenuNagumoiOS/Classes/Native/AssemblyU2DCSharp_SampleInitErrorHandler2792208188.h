﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// AppManager
struct AppManager_t3961229932;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_SampleInitErrorHandler_ErrorData135901855.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleInitErrorHandler
struct  SampleInitErrorHandler_t2792208188  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GUIStyle SampleInitErrorHandler::mErrorTitleMessage
	GUIStyle_t2990928826 * ___mErrorTitleMessage_2;
	// UnityEngine.GUIStyle SampleInitErrorHandler::mErrorBodyMessage
	GUIStyle_t2990928826 * ___mErrorBodyMessage_3;
	// UnityEngine.GUIStyle SampleInitErrorHandler::mErrorOkButton
	GUIStyle_t2990928826 * ___mErrorOkButton_4;
	// SampleInitErrorHandler/ErrorData SampleInitErrorHandler::mCurrentError
	ErrorData_t135901855  ___mCurrentError_5;
	// AppManager SampleInitErrorHandler::mManager
	AppManager_t3961229932 * ___mManager_6;
	// Vuforia.VuforiaUnity/InitError SampleInitErrorHandler::mErrorcode
	int32_t ___mErrorcode_7;

public:
	inline static int32_t get_offset_of_mErrorTitleMessage_2() { return static_cast<int32_t>(offsetof(SampleInitErrorHandler_t2792208188, ___mErrorTitleMessage_2)); }
	inline GUIStyle_t2990928826 * get_mErrorTitleMessage_2() const { return ___mErrorTitleMessage_2; }
	inline GUIStyle_t2990928826 ** get_address_of_mErrorTitleMessage_2() { return &___mErrorTitleMessage_2; }
	inline void set_mErrorTitleMessage_2(GUIStyle_t2990928826 * value)
	{
		___mErrorTitleMessage_2 = value;
		Il2CppCodeGenWriteBarrier(&___mErrorTitleMessage_2, value);
	}

	inline static int32_t get_offset_of_mErrorBodyMessage_3() { return static_cast<int32_t>(offsetof(SampleInitErrorHandler_t2792208188, ___mErrorBodyMessage_3)); }
	inline GUIStyle_t2990928826 * get_mErrorBodyMessage_3() const { return ___mErrorBodyMessage_3; }
	inline GUIStyle_t2990928826 ** get_address_of_mErrorBodyMessage_3() { return &___mErrorBodyMessage_3; }
	inline void set_mErrorBodyMessage_3(GUIStyle_t2990928826 * value)
	{
		___mErrorBodyMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___mErrorBodyMessage_3, value);
	}

	inline static int32_t get_offset_of_mErrorOkButton_4() { return static_cast<int32_t>(offsetof(SampleInitErrorHandler_t2792208188, ___mErrorOkButton_4)); }
	inline GUIStyle_t2990928826 * get_mErrorOkButton_4() const { return ___mErrorOkButton_4; }
	inline GUIStyle_t2990928826 ** get_address_of_mErrorOkButton_4() { return &___mErrorOkButton_4; }
	inline void set_mErrorOkButton_4(GUIStyle_t2990928826 * value)
	{
		___mErrorOkButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___mErrorOkButton_4, value);
	}

	inline static int32_t get_offset_of_mCurrentError_5() { return static_cast<int32_t>(offsetof(SampleInitErrorHandler_t2792208188, ___mCurrentError_5)); }
	inline ErrorData_t135901855  get_mCurrentError_5() const { return ___mCurrentError_5; }
	inline ErrorData_t135901855 * get_address_of_mCurrentError_5() { return &___mCurrentError_5; }
	inline void set_mCurrentError_5(ErrorData_t135901855  value)
	{
		___mCurrentError_5 = value;
	}

	inline static int32_t get_offset_of_mManager_6() { return static_cast<int32_t>(offsetof(SampleInitErrorHandler_t2792208188, ___mManager_6)); }
	inline AppManager_t3961229932 * get_mManager_6() const { return ___mManager_6; }
	inline AppManager_t3961229932 ** get_address_of_mManager_6() { return &___mManager_6; }
	inline void set_mManager_6(AppManager_t3961229932 * value)
	{
		___mManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___mManager_6, value);
	}

	inline static int32_t get_offset_of_mErrorcode_7() { return static_cast<int32_t>(offsetof(SampleInitErrorHandler_t2792208188, ___mErrorcode_7)); }
	inline int32_t get_mErrorcode_7() const { return ___mErrorcode_7; }
	inline int32_t* get_address_of_mErrorcode_7() { return &___mErrorcode_7; }
	inline void set_mErrorcode_7(int32_t value)
	{
		___mErrorcode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
