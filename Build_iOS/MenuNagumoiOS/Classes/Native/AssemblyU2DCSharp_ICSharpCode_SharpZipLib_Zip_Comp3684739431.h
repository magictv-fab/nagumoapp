﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int16[]
struct Int16U5BU5D_t801762735;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t1829109954;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman
struct DeflaterHuffman_t3769756376;
// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t2680377483;

#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2584813498.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1587604368.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine
struct  DeflaterEngine_t3684739431  : public DeflaterConstants_t2584813498
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::ins_h
	int32_t ___ins_h_28;
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::head
	Int16U5BU5D_t801762735* ___head_29;
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::prev
	Int16U5BU5D_t801762735* ___prev_30;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::matchStart
	int32_t ___matchStart_31;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::matchLen
	int32_t ___matchLen_32;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::prevAvailable
	bool ___prevAvailable_33;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::blockStart
	int32_t ___blockStart_34;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::strstart
	int32_t ___strstart_35;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::lookahead
	int32_t ___lookahead_36;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::window
	ByteU5BU5D_t4260760469* ___window_37;
	// ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::strategy
	int32_t ___strategy_38;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::max_chain
	int32_t ___max_chain_39;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::max_lazy
	int32_t ___max_lazy_40;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::niceLength
	int32_t ___niceLength_41;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::goodLength
	int32_t ___goodLength_42;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::compressionFunction
	int32_t ___compressionFunction_43;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::inputBuf
	ByteU5BU5D_t4260760469* ___inputBuf_44;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::totalIn
	int64_t ___totalIn_45;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::inputOff
	int32_t ___inputOff_46;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::inputEnd
	int32_t ___inputEnd_47;
	// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::pending
	DeflaterPending_t1829109954 * ___pending_48;
	// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::huffman
	DeflaterHuffman_t3769756376 * ___huffman_49;
	// ICSharpCode.SharpZipLib.Checksums.Adler32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::adler
	Adler32_t2680377483 * ___adler_50;

public:
	inline static int32_t get_offset_of_ins_h_28() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___ins_h_28)); }
	inline int32_t get_ins_h_28() const { return ___ins_h_28; }
	inline int32_t* get_address_of_ins_h_28() { return &___ins_h_28; }
	inline void set_ins_h_28(int32_t value)
	{
		___ins_h_28 = value;
	}

	inline static int32_t get_offset_of_head_29() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___head_29)); }
	inline Int16U5BU5D_t801762735* get_head_29() const { return ___head_29; }
	inline Int16U5BU5D_t801762735** get_address_of_head_29() { return &___head_29; }
	inline void set_head_29(Int16U5BU5D_t801762735* value)
	{
		___head_29 = value;
		Il2CppCodeGenWriteBarrier(&___head_29, value);
	}

	inline static int32_t get_offset_of_prev_30() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___prev_30)); }
	inline Int16U5BU5D_t801762735* get_prev_30() const { return ___prev_30; }
	inline Int16U5BU5D_t801762735** get_address_of_prev_30() { return &___prev_30; }
	inline void set_prev_30(Int16U5BU5D_t801762735* value)
	{
		___prev_30 = value;
		Il2CppCodeGenWriteBarrier(&___prev_30, value);
	}

	inline static int32_t get_offset_of_matchStart_31() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___matchStart_31)); }
	inline int32_t get_matchStart_31() const { return ___matchStart_31; }
	inline int32_t* get_address_of_matchStart_31() { return &___matchStart_31; }
	inline void set_matchStart_31(int32_t value)
	{
		___matchStart_31 = value;
	}

	inline static int32_t get_offset_of_matchLen_32() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___matchLen_32)); }
	inline int32_t get_matchLen_32() const { return ___matchLen_32; }
	inline int32_t* get_address_of_matchLen_32() { return &___matchLen_32; }
	inline void set_matchLen_32(int32_t value)
	{
		___matchLen_32 = value;
	}

	inline static int32_t get_offset_of_prevAvailable_33() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___prevAvailable_33)); }
	inline bool get_prevAvailable_33() const { return ___prevAvailable_33; }
	inline bool* get_address_of_prevAvailable_33() { return &___prevAvailable_33; }
	inline void set_prevAvailable_33(bool value)
	{
		___prevAvailable_33 = value;
	}

	inline static int32_t get_offset_of_blockStart_34() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___blockStart_34)); }
	inline int32_t get_blockStart_34() const { return ___blockStart_34; }
	inline int32_t* get_address_of_blockStart_34() { return &___blockStart_34; }
	inline void set_blockStart_34(int32_t value)
	{
		___blockStart_34 = value;
	}

	inline static int32_t get_offset_of_strstart_35() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___strstart_35)); }
	inline int32_t get_strstart_35() const { return ___strstart_35; }
	inline int32_t* get_address_of_strstart_35() { return &___strstart_35; }
	inline void set_strstart_35(int32_t value)
	{
		___strstart_35 = value;
	}

	inline static int32_t get_offset_of_lookahead_36() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___lookahead_36)); }
	inline int32_t get_lookahead_36() const { return ___lookahead_36; }
	inline int32_t* get_address_of_lookahead_36() { return &___lookahead_36; }
	inline void set_lookahead_36(int32_t value)
	{
		___lookahead_36 = value;
	}

	inline static int32_t get_offset_of_window_37() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___window_37)); }
	inline ByteU5BU5D_t4260760469* get_window_37() const { return ___window_37; }
	inline ByteU5BU5D_t4260760469** get_address_of_window_37() { return &___window_37; }
	inline void set_window_37(ByteU5BU5D_t4260760469* value)
	{
		___window_37 = value;
		Il2CppCodeGenWriteBarrier(&___window_37, value);
	}

	inline static int32_t get_offset_of_strategy_38() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___strategy_38)); }
	inline int32_t get_strategy_38() const { return ___strategy_38; }
	inline int32_t* get_address_of_strategy_38() { return &___strategy_38; }
	inline void set_strategy_38(int32_t value)
	{
		___strategy_38 = value;
	}

	inline static int32_t get_offset_of_max_chain_39() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___max_chain_39)); }
	inline int32_t get_max_chain_39() const { return ___max_chain_39; }
	inline int32_t* get_address_of_max_chain_39() { return &___max_chain_39; }
	inline void set_max_chain_39(int32_t value)
	{
		___max_chain_39 = value;
	}

	inline static int32_t get_offset_of_max_lazy_40() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___max_lazy_40)); }
	inline int32_t get_max_lazy_40() const { return ___max_lazy_40; }
	inline int32_t* get_address_of_max_lazy_40() { return &___max_lazy_40; }
	inline void set_max_lazy_40(int32_t value)
	{
		___max_lazy_40 = value;
	}

	inline static int32_t get_offset_of_niceLength_41() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___niceLength_41)); }
	inline int32_t get_niceLength_41() const { return ___niceLength_41; }
	inline int32_t* get_address_of_niceLength_41() { return &___niceLength_41; }
	inline void set_niceLength_41(int32_t value)
	{
		___niceLength_41 = value;
	}

	inline static int32_t get_offset_of_goodLength_42() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___goodLength_42)); }
	inline int32_t get_goodLength_42() const { return ___goodLength_42; }
	inline int32_t* get_address_of_goodLength_42() { return &___goodLength_42; }
	inline void set_goodLength_42(int32_t value)
	{
		___goodLength_42 = value;
	}

	inline static int32_t get_offset_of_compressionFunction_43() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___compressionFunction_43)); }
	inline int32_t get_compressionFunction_43() const { return ___compressionFunction_43; }
	inline int32_t* get_address_of_compressionFunction_43() { return &___compressionFunction_43; }
	inline void set_compressionFunction_43(int32_t value)
	{
		___compressionFunction_43 = value;
	}

	inline static int32_t get_offset_of_inputBuf_44() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___inputBuf_44)); }
	inline ByteU5BU5D_t4260760469* get_inputBuf_44() const { return ___inputBuf_44; }
	inline ByteU5BU5D_t4260760469** get_address_of_inputBuf_44() { return &___inputBuf_44; }
	inline void set_inputBuf_44(ByteU5BU5D_t4260760469* value)
	{
		___inputBuf_44 = value;
		Il2CppCodeGenWriteBarrier(&___inputBuf_44, value);
	}

	inline static int32_t get_offset_of_totalIn_45() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___totalIn_45)); }
	inline int64_t get_totalIn_45() const { return ___totalIn_45; }
	inline int64_t* get_address_of_totalIn_45() { return &___totalIn_45; }
	inline void set_totalIn_45(int64_t value)
	{
		___totalIn_45 = value;
	}

	inline static int32_t get_offset_of_inputOff_46() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___inputOff_46)); }
	inline int32_t get_inputOff_46() const { return ___inputOff_46; }
	inline int32_t* get_address_of_inputOff_46() { return &___inputOff_46; }
	inline void set_inputOff_46(int32_t value)
	{
		___inputOff_46 = value;
	}

	inline static int32_t get_offset_of_inputEnd_47() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___inputEnd_47)); }
	inline int32_t get_inputEnd_47() const { return ___inputEnd_47; }
	inline int32_t* get_address_of_inputEnd_47() { return &___inputEnd_47; }
	inline void set_inputEnd_47(int32_t value)
	{
		___inputEnd_47 = value;
	}

	inline static int32_t get_offset_of_pending_48() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___pending_48)); }
	inline DeflaterPending_t1829109954 * get_pending_48() const { return ___pending_48; }
	inline DeflaterPending_t1829109954 ** get_address_of_pending_48() { return &___pending_48; }
	inline void set_pending_48(DeflaterPending_t1829109954 * value)
	{
		___pending_48 = value;
		Il2CppCodeGenWriteBarrier(&___pending_48, value);
	}

	inline static int32_t get_offset_of_huffman_49() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___huffman_49)); }
	inline DeflaterHuffman_t3769756376 * get_huffman_49() const { return ___huffman_49; }
	inline DeflaterHuffman_t3769756376 ** get_address_of_huffman_49() { return &___huffman_49; }
	inline void set_huffman_49(DeflaterHuffman_t3769756376 * value)
	{
		___huffman_49 = value;
		Il2CppCodeGenWriteBarrier(&___huffman_49, value);
	}

	inline static int32_t get_offset_of_adler_50() { return static_cast<int32_t>(offsetof(DeflaterEngine_t3684739431, ___adler_50)); }
	inline Adler32_t2680377483 * get_adler_50() const { return ___adler_50; }
	inline Adler32_t2680377483 ** get_address_of_adler_50() { return &___adler_50; }
	inline void set_adler_50(Adler32_t2680377483 * value)
	{
		___adler_50 = value;
		Il2CppCodeGenWriteBarrier(&___adler_50, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
