﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TransformDirection
struct TransformDirection_t3682227675;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TransformDirection::.ctor()
extern "C"  void TransformDirection__ctor_m2172210411 (TransformDirection_t3682227675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::Reset()
extern "C"  void TransformDirection_Reset_m4113610648 (TransformDirection_t3682227675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::OnEnter()
extern "C"  void TransformDirection_OnEnter_m3826795010 (TransformDirection_t3682227675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::OnUpdate()
extern "C"  void TransformDirection_OnUpdate_m1800087681 (TransformDirection_t3682227675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::DoTransformDirection()
extern "C"  void TransformDirection_DoTransformDirection_m2822731991 (TransformDirection_t3682227675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
