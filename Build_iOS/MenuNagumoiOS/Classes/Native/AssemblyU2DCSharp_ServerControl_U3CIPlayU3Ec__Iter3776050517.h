﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// JsonPlayRequest
struct JsonPlayRequest_t2877984403;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// JsonPlay
struct JsonPlay_t2384567388;
// CuponData
struct CuponData_t807724487;
// System.Collections.Generic.List`1<CuponData>
struct List_1_t2175910039;
// System.Object
struct Il2CppObject;
// ServerControl
struct ServerControl_t2725829754;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PointsAngle2793040272.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerControl/<IPlay>c__Iterator7C
struct  U3CIPlayU3Ec__Iterator7C_t3776050517  : public Il2CppObject
{
public:
	// UnityEngine.WWW ServerControl/<IPlay>c__Iterator7C::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// JsonPlayRequest ServerControl/<IPlay>c__Iterator7C::<jsonPlayRequest>__1
	JsonPlayRequest_t2877984403 * ___U3CjsonPlayRequestU3E__1_1;
	// System.String ServerControl/<IPlay>c__Iterator7C::<json>__2
	String_t* ___U3CjsonU3E__2_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> ServerControl/<IPlay>c__Iterator7C::<headers>__3
	Dictionary_2_t827649927 * ___U3CheadersU3E__3_3;
	// System.Byte[] ServerControl/<IPlay>c__Iterator7C::<pData>__4
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__4_4;
	// System.Single ServerControl/<IPlay>c__Iterator7C::<fakeProgress>__5
	float ___U3CfakeProgressU3E__5_5;
	// PointsAngle ServerControl/<IPlay>c__Iterator7C::<state>__6
	int32_t ___U3CstateU3E__6_6;
	// JsonPlay ServerControl/<IPlay>c__Iterator7C::<jsonPlay>__7
	JsonPlay_t2384567388 * ___U3CjsonPlayU3E__7_7;
	// CuponData ServerControl/<IPlay>c__Iterator7C::<cuponData>__8
	CuponData_t807724487 * ___U3CcuponDataU3E__8_8;
	// System.Collections.Generic.List`1<CuponData> ServerControl/<IPlay>c__Iterator7C::<lst>__9
	List_1_t2175910039 * ___U3ClstU3E__9_9;
	// System.Int32 ServerControl/<IPlay>c__Iterator7C::$PC
	int32_t ___U24PC_10;
	// System.Object ServerControl/<IPlay>c__Iterator7C::$current
	Il2CppObject * ___U24current_11;
	// ServerControl ServerControl/<IPlay>c__Iterator7C::<>f__this
	ServerControl_t2725829754 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CjsonPlayRequestU3E__1_1() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CjsonPlayRequestU3E__1_1)); }
	inline JsonPlayRequest_t2877984403 * get_U3CjsonPlayRequestU3E__1_1() const { return ___U3CjsonPlayRequestU3E__1_1; }
	inline JsonPlayRequest_t2877984403 ** get_address_of_U3CjsonPlayRequestU3E__1_1() { return &___U3CjsonPlayRequestU3E__1_1; }
	inline void set_U3CjsonPlayRequestU3E__1_1(JsonPlayRequest_t2877984403 * value)
	{
		___U3CjsonPlayRequestU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonPlayRequestU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__2_2() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CjsonU3E__2_2)); }
	inline String_t* get_U3CjsonU3E__2_2() const { return ___U3CjsonU3E__2_2; }
	inline String_t** get_address_of_U3CjsonU3E__2_2() { return &___U3CjsonU3E__2_2; }
	inline void set_U3CjsonU3E__2_2(String_t* value)
	{
		___U3CjsonU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__3_3() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CheadersU3E__3_3)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__3_3() const { return ___U3CheadersU3E__3_3; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__3_3() { return &___U3CheadersU3E__3_3; }
	inline void set_U3CheadersU3E__3_3(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__4_4() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CpDataU3E__4_4)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__4_4() const { return ___U3CpDataU3E__4_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__4_4() { return &___U3CpDataU3E__4_4; }
	inline void set_U3CpDataU3E__4_4(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CfakeProgressU3E__5_5() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CfakeProgressU3E__5_5)); }
	inline float get_U3CfakeProgressU3E__5_5() const { return ___U3CfakeProgressU3E__5_5; }
	inline float* get_address_of_U3CfakeProgressU3E__5_5() { return &___U3CfakeProgressU3E__5_5; }
	inline void set_U3CfakeProgressU3E__5_5(float value)
	{
		___U3CfakeProgressU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CstateU3E__6_6() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CstateU3E__6_6)); }
	inline int32_t get_U3CstateU3E__6_6() const { return ___U3CstateU3E__6_6; }
	inline int32_t* get_address_of_U3CstateU3E__6_6() { return &___U3CstateU3E__6_6; }
	inline void set_U3CstateU3E__6_6(int32_t value)
	{
		___U3CstateU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CjsonPlayU3E__7_7() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CjsonPlayU3E__7_7)); }
	inline JsonPlay_t2384567388 * get_U3CjsonPlayU3E__7_7() const { return ___U3CjsonPlayU3E__7_7; }
	inline JsonPlay_t2384567388 ** get_address_of_U3CjsonPlayU3E__7_7() { return &___U3CjsonPlayU3E__7_7; }
	inline void set_U3CjsonPlayU3E__7_7(JsonPlay_t2384567388 * value)
	{
		___U3CjsonPlayU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonPlayU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CcuponDataU3E__8_8() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CcuponDataU3E__8_8)); }
	inline CuponData_t807724487 * get_U3CcuponDataU3E__8_8() const { return ___U3CcuponDataU3E__8_8; }
	inline CuponData_t807724487 ** get_address_of_U3CcuponDataU3E__8_8() { return &___U3CcuponDataU3E__8_8; }
	inline void set_U3CcuponDataU3E__8_8(CuponData_t807724487 * value)
	{
		___U3CcuponDataU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcuponDataU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3ClstU3E__9_9() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3ClstU3E__9_9)); }
	inline List_1_t2175910039 * get_U3ClstU3E__9_9() const { return ___U3ClstU3E__9_9; }
	inline List_1_t2175910039 ** get_address_of_U3ClstU3E__9_9() { return &___U3ClstU3E__9_9; }
	inline void set_U3ClstU3E__9_9(List_1_t2175910039 * value)
	{
		___U3ClstU3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClstU3E__9_9, value);
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CIPlayU3Ec__Iterator7C_t3776050517, ___U3CU3Ef__this_12)); }
	inline ServerControl_t2725829754 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline ServerControl_t2725829754 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(ServerControl_t2725829754 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
