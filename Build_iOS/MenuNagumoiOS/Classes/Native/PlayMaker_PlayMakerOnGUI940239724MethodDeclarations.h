﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerOnGUI
struct PlayMakerOnGUI_t940239724;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerOnGUI::Start()
extern "C"  void PlayMakerOnGUI_Start_m2713337841 (PlayMakerOnGUI_t940239724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::OnGUI()
extern "C"  void PlayMakerOnGUI_OnGUI_m3261598699 (PlayMakerOnGUI_t940239724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::DoEditGUI()
extern "C"  void PlayMakerOnGUI_DoEditGUI_m2461980501 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::.ctor()
extern "C"  void PlayMakerOnGUI__ctor_m3766200049 (PlayMakerOnGUI_t940239724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
