﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// DownloadImages
struct DownloadImages_t3644489024;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadImages/<LoadFiles>c__Iterator1B
struct  U3CLoadFilesU3Ec__Iterator1B_t4225544456  : public Il2CppObject
{
public:
	// System.String[] DownloadImages/<LoadFiles>c__Iterator1B::<$s_21>__0
	StringU5BU5D_t4054002952* ___U3CU24s_21U3E__0_0;
	// System.Int32 DownloadImages/<LoadFiles>c__Iterator1B::<$s_22>__1
	int32_t ___U3CU24s_22U3E__1_1;
	// System.String DownloadImages/<LoadFiles>c__Iterator1B::<folderPath>__2
	String_t* ___U3CfolderPathU3E__2_2;
	// System.String DownloadImages/<LoadFiles>c__Iterator1B::<folderName>__3
	String_t* ___U3CfolderNameU3E__3_3;
	// System.String[] DownloadImages/<LoadFiles>c__Iterator1B::<$s_23>__4
	StringU5BU5D_t4054002952* ___U3CU24s_23U3E__4_4;
	// System.Int32 DownloadImages/<LoadFiles>c__Iterator1B::<$s_24>__5
	int32_t ___U3CU24s_24U3E__5_5;
	// System.String DownloadImages/<LoadFiles>c__Iterator1B::<filePath>__6
	String_t* ___U3CfilePathU3E__6_6;
	// System.String DownloadImages/<LoadFiles>c__Iterator1B::<imgName>__7
	String_t* ___U3CimgNameU3E__7_7;
	// System.Byte[] DownloadImages/<LoadFiles>c__Iterator1B::<bytes>__8
	ByteU5BU5D_t4260760469* ___U3CbytesU3E__8_8;
	// UnityEngine.Texture2D DownloadImages/<LoadFiles>c__Iterator1B::<text>__9
	Texture2D_t3884108195 * ___U3CtextU3E__9_9;
	// UnityEngine.Sprite DownloadImages/<LoadFiles>c__Iterator1B::<sprite>__10
	Sprite_t3199167241 * ___U3CspriteU3E__10_10;
	// System.Int32 DownloadImages/<LoadFiles>c__Iterator1B::$PC
	int32_t ___U24PC_11;
	// System.Object DownloadImages/<LoadFiles>c__Iterator1B::$current
	Il2CppObject * ___U24current_12;
	// DownloadImages DownloadImages/<LoadFiles>c__Iterator1B::<>f__this
	DownloadImages_t3644489024 * ___U3CU3Ef__this_13;

public:
	inline static int32_t get_offset_of_U3CU24s_21U3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CU24s_21U3E__0_0)); }
	inline StringU5BU5D_t4054002952* get_U3CU24s_21U3E__0_0() const { return ___U3CU24s_21U3E__0_0; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU24s_21U3E__0_0() { return &___U3CU24s_21U3E__0_0; }
	inline void set_U3CU24s_21U3E__0_0(StringU5BU5D_t4054002952* value)
	{
		___U3CU24s_21U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_21U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_22U3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CU24s_22U3E__1_1)); }
	inline int32_t get_U3CU24s_22U3E__1_1() const { return ___U3CU24s_22U3E__1_1; }
	inline int32_t* get_address_of_U3CU24s_22U3E__1_1() { return &___U3CU24s_22U3E__1_1; }
	inline void set_U3CU24s_22U3E__1_1(int32_t value)
	{
		___U3CU24s_22U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CfolderPathU3E__2_2() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CfolderPathU3E__2_2)); }
	inline String_t* get_U3CfolderPathU3E__2_2() const { return ___U3CfolderPathU3E__2_2; }
	inline String_t** get_address_of_U3CfolderPathU3E__2_2() { return &___U3CfolderPathU3E__2_2; }
	inline void set_U3CfolderPathU3E__2_2(String_t* value)
	{
		___U3CfolderPathU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfolderPathU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CfolderNameU3E__3_3() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CfolderNameU3E__3_3)); }
	inline String_t* get_U3CfolderNameU3E__3_3() const { return ___U3CfolderNameU3E__3_3; }
	inline String_t** get_address_of_U3CfolderNameU3E__3_3() { return &___U3CfolderNameU3E__3_3; }
	inline void set_U3CfolderNameU3E__3_3(String_t* value)
	{
		___U3CfolderNameU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfolderNameU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_23U3E__4_4() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CU24s_23U3E__4_4)); }
	inline StringU5BU5D_t4054002952* get_U3CU24s_23U3E__4_4() const { return ___U3CU24s_23U3E__4_4; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU24s_23U3E__4_4() { return &___U3CU24s_23U3E__4_4; }
	inline void set_U3CU24s_23U3E__4_4(StringU5BU5D_t4054002952* value)
	{
		___U3CU24s_23U3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_23U3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_24U3E__5_5() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CU24s_24U3E__5_5)); }
	inline int32_t get_U3CU24s_24U3E__5_5() const { return ___U3CU24s_24U3E__5_5; }
	inline int32_t* get_address_of_U3CU24s_24U3E__5_5() { return &___U3CU24s_24U3E__5_5; }
	inline void set_U3CU24s_24U3E__5_5(int32_t value)
	{
		___U3CU24s_24U3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CfilePathU3E__6_6() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CfilePathU3E__6_6)); }
	inline String_t* get_U3CfilePathU3E__6_6() const { return ___U3CfilePathU3E__6_6; }
	inline String_t** get_address_of_U3CfilePathU3E__6_6() { return &___U3CfilePathU3E__6_6; }
	inline void set_U3CfilePathU3E__6_6(String_t* value)
	{
		___U3CfilePathU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilePathU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CimgNameU3E__7_7() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CimgNameU3E__7_7)); }
	inline String_t* get_U3CimgNameU3E__7_7() const { return ___U3CimgNameU3E__7_7; }
	inline String_t** get_address_of_U3CimgNameU3E__7_7() { return &___U3CimgNameU3E__7_7; }
	inline void set_U3CimgNameU3E__7_7(String_t* value)
	{
		___U3CimgNameU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimgNameU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__8_8() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CbytesU3E__8_8)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytesU3E__8_8() const { return ___U3CbytesU3E__8_8; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytesU3E__8_8() { return &___U3CbytesU3E__8_8; }
	inline void set_U3CbytesU3E__8_8(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytesU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3CtextU3E__9_9() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CtextU3E__9_9)); }
	inline Texture2D_t3884108195 * get_U3CtextU3E__9_9() const { return ___U3CtextU3E__9_9; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtextU3E__9_9() { return &___U3CtextU3E__9_9; }
	inline void set_U3CtextU3E__9_9(Texture2D_t3884108195 * value)
	{
		___U3CtextU3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextU3E__9_9, value);
	}

	inline static int32_t get_offset_of_U3CspriteU3E__10_10() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CspriteU3E__10_10)); }
	inline Sprite_t3199167241 * get_U3CspriteU3E__10_10() const { return ___U3CspriteU3E__10_10; }
	inline Sprite_t3199167241 ** get_address_of_U3CspriteU3E__10_10() { return &___U3CspriteU3E__10_10; }
	inline void set_U3CspriteU3E__10_10(Sprite_t3199167241 * value)
	{
		___U3CspriteU3E__10_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CspriteU3E__10_10, value);
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_13() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1B_t4225544456, ___U3CU3Ef__this_13)); }
	inline DownloadImages_t3644489024 * get_U3CU3Ef__this_13() const { return ___U3CU3Ef__this_13; }
	inline DownloadImages_t3644489024 ** get_address_of_U3CU3Ef__this_13() { return &___U3CU3Ef__this_13; }
	inline void set_U3CU3Ef__this_13(DownloadImages_t3644489024 * value)
	{
		___U3CU3Ef__this_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
