﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Collections.Generic.IList`1<ZXing.QrCode.Internal.AlignmentPattern>
struct IList_1_t186650476;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.ResultPointCallback
struct ResultPointCallback_t207829946;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.AlignmentPatternFinder
struct  AlignmentPatternFinder_t4197406735  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.AlignmentPatternFinder::image
	BitMatrix_t1058711404 * ___image_0;
	// System.Collections.Generic.IList`1<ZXing.QrCode.Internal.AlignmentPattern> ZXing.QrCode.Internal.AlignmentPatternFinder::possibleCenters
	Il2CppObject* ___possibleCenters_1;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::startX
	int32_t ___startX_2;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::startY
	int32_t ___startY_3;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::width
	int32_t ___width_4;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::height
	int32_t ___height_5;
	// System.Single ZXing.QrCode.Internal.AlignmentPatternFinder::moduleSize
	float ___moduleSize_6;
	// System.Int32[] ZXing.QrCode.Internal.AlignmentPatternFinder::crossCheckStateCount
	Int32U5BU5D_t3230847821* ___crossCheckStateCount_7;
	// ZXing.ResultPointCallback ZXing.QrCode.Internal.AlignmentPatternFinder::resultPointCallback
	ResultPointCallback_t207829946 * ___resultPointCallback_8;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___image_0)); }
	inline BitMatrix_t1058711404 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t1058711404 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t1058711404 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_possibleCenters_1() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___possibleCenters_1)); }
	inline Il2CppObject* get_possibleCenters_1() const { return ___possibleCenters_1; }
	inline Il2CppObject** get_address_of_possibleCenters_1() { return &___possibleCenters_1; }
	inline void set_possibleCenters_1(Il2CppObject* value)
	{
		___possibleCenters_1 = value;
		Il2CppCodeGenWriteBarrier(&___possibleCenters_1, value);
	}

	inline static int32_t get_offset_of_startX_2() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___startX_2)); }
	inline int32_t get_startX_2() const { return ___startX_2; }
	inline int32_t* get_address_of_startX_2() { return &___startX_2; }
	inline void set_startX_2(int32_t value)
	{
		___startX_2 = value;
	}

	inline static int32_t get_offset_of_startY_3() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___startY_3)); }
	inline int32_t get_startY_3() const { return ___startY_3; }
	inline int32_t* get_address_of_startY_3() { return &___startY_3; }
	inline void set_startY_3(int32_t value)
	{
		___startY_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_moduleSize_6() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___moduleSize_6)); }
	inline float get_moduleSize_6() const { return ___moduleSize_6; }
	inline float* get_address_of_moduleSize_6() { return &___moduleSize_6; }
	inline void set_moduleSize_6(float value)
	{
		___moduleSize_6 = value;
	}

	inline static int32_t get_offset_of_crossCheckStateCount_7() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___crossCheckStateCount_7)); }
	inline Int32U5BU5D_t3230847821* get_crossCheckStateCount_7() const { return ___crossCheckStateCount_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_crossCheckStateCount_7() { return &___crossCheckStateCount_7; }
	inline void set_crossCheckStateCount_7(Int32U5BU5D_t3230847821* value)
	{
		___crossCheckStateCount_7 = value;
		Il2CppCodeGenWriteBarrier(&___crossCheckStateCount_7, value);
	}

	inline static int32_t get_offset_of_resultPointCallback_8() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t4197406735, ___resultPointCallback_8)); }
	inline ResultPointCallback_t207829946 * get_resultPointCallback_8() const { return ___resultPointCallback_8; }
	inline ResultPointCallback_t207829946 ** get_address_of_resultPointCallback_8() { return &___resultPointCallback_8; }
	inline void set_resultPointCallback_8(ResultPointCallback_t207829946 * value)
	{
		___resultPointCallback_8 = value;
		Il2CppCodeGenWriteBarrier(&___resultPointCallback_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
