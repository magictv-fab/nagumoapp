﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Correios.Net.Http.Request
struct  Request_t788181  : public Il2CppObject
{
public:
	// System.String Correios.Net.Http.Request::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_0;
	// System.String Correios.Net.Http.Request::<DataToSend>k__BackingField
	String_t* ___U3CDataToSendU3Ek__BackingField_1;
	// System.String Correios.Net.Http.Request::<Method>k__BackingField
	String_t* ___U3CMethodU3Ek__BackingField_2;
	// System.String Correios.Net.Http.Request::<ContentType>k__BackingField
	String_t* ___U3CContentTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t788181, ___U3CUrlU3Ek__BackingField_0)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_0() const { return ___U3CUrlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_0() { return &___U3CUrlU3Ek__BackingField_0; }
	inline void set_U3CUrlU3Ek__BackingField_0(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUrlU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CDataToSendU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t788181, ___U3CDataToSendU3Ek__BackingField_1)); }
	inline String_t* get_U3CDataToSendU3Ek__BackingField_1() const { return ___U3CDataToSendU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDataToSendU3Ek__BackingField_1() { return &___U3CDataToSendU3Ek__BackingField_1; }
	inline void set_U3CDataToSendU3Ek__BackingField_1(String_t* value)
	{
		___U3CDataToSendU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataToSendU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t788181, ___U3CMethodU3Ek__BackingField_2)); }
	inline String_t* get_U3CMethodU3Ek__BackingField_2() const { return ___U3CMethodU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMethodU3Ek__BackingField_2() { return &___U3CMethodU3Ek__BackingField_2; }
	inline void set_U3CMethodU3Ek__BackingField_2(String_t* value)
	{
		___U3CMethodU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMethodU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CContentTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t788181, ___U3CContentTypeU3Ek__BackingField_3)); }
	inline String_t* get_U3CContentTypeU3Ek__BackingField_3() const { return ___U3CContentTypeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CContentTypeU3Ek__BackingField_3() { return &___U3CContentTypeU3Ek__BackingField_3; }
	inline void set_U3CContentTypeU3Ek__BackingField_3(String_t* value)
	{
		___U3CContentTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContentTypeU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
