﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.WriterException
struct WriterException_t2468555518;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void ZXing.WriterException::.ctor()
extern "C"  void WriterException__ctor_m27001369 (WriterException_t2468555518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.WriterException::.ctor(System.String)
extern "C"  void WriterException__ctor_m478252425 (WriterException_t2468555518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.WriterException::.ctor(System.String,System.Exception)
extern "C"  void WriterException__ctor_m4092582605 (WriterException_t2468555518 * __this, String_t* ___message0, Exception_t3991598821 * ___innerExc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
