﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MeuPedidoData
struct MeuPedidoData_t2688356908;

#include "codegen/il2cpp-codegen.h"

// System.Void MeuPedidoData::.ctor()
extern "C"  void MeuPedidoData__ctor_m1638990015 (MeuPedidoData_t2688356908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
