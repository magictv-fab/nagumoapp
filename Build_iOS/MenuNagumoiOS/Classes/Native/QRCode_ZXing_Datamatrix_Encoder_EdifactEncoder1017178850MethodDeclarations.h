﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.EdifactEncoder
struct EdifactEncoder_t1017178850;
// ZXing.Datamatrix.Encoder.EncoderContext
struct EncoderContext_t1774722223;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EncoderContext1774722223.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Int32 ZXing.Datamatrix.Encoder.EdifactEncoder::get_EncodingMode()
extern "C"  int32_t EdifactEncoder_get_EncodingMode_m3550848731 (EdifactEncoder_t1017178850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern "C"  void EdifactEncoder_encode_m1455528884 (EdifactEncoder_t1017178850 * __this, EncoderContext_t1774722223 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern "C"  void EdifactEncoder_handleEOD_m2643586030 (Il2CppObject * __this /* static, unused */, EncoderContext_t1774722223 * ___context0, StringBuilder_t243639308 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::encodeChar(System.Char,System.Text.StringBuilder)
extern "C"  void EdifactEncoder_encodeChar_m3734889355 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, StringBuilder_t243639308 * ___sb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.EdifactEncoder::encodeToCodewords(System.Text.StringBuilder,System.Int32)
extern "C"  String_t* EdifactEncoder_encodeToCodewords_m601242291 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___sb0, int32_t ___startPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::.ctor()
extern "C"  void EdifactEncoder__ctor_m2884328890 (EdifactEncoder_t1017178850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
