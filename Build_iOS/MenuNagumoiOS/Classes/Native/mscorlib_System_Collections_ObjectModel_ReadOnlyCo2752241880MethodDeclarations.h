﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m2018878373(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1036352271(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4068840347(__this, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3360339766(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, ResultPointU5BU5D_t1195164344*, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m156705284(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1234192636(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2586628608(__this, ___index0, method) ((  ResultPointU5BU5D_t1195164344* (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1280705165(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, ResultPointU5BU5D_t1195164344*, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m188419851(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2600350164(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2261660111(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4233663362(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2752241880 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3473379554(__this, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m709788554(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3728685274(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2752241880 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2901278213(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2210940675(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1405039893(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1181790098(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m393102334(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m406463097(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2545330528(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m879832581(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1308540060(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::Contains(T)
#define ReadOnlyCollection_1_Contains_m398721865(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2752241880 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1565543231(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2752241880 *, ResultPointU5BU5DU5BU5D_t2883781481*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1451918124(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m4241051395(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2752241880 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2440881880(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2752241880 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.ResultPoint[]>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3206050752(__this, ___index0, method) ((  ResultPointU5BU5D_t1195164344* (*) (ReadOnlyCollection_1_t2752241880 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
