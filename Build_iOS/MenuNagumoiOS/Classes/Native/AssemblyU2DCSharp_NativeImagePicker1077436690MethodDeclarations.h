﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NativeImagePicker/CallbackImagePicked
struct CallbackImagePicked_t554520473;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NativeImagePicker_CallbackImagePi554520473.h"

// System.Void NativeImagePicker::_CNativeImagePickerFromLibrary(System.Boolean)
extern "C"  void NativeImagePicker__CNativeImagePickerFromLibrary_m4276586533 (Il2CppObject * __this /* static, unused */, bool ___allowEditing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeImagePicker::_CNativeImagePickerFromCamera(System.Boolean)
extern "C"  void NativeImagePicker__CNativeImagePickerFromCamera_m3853716203 (Il2CppObject * __this /* static, unused */, bool ___allowEditing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeImagePicker::FromLibrary(NativeImagePicker/CallbackImagePicked,System.Boolean)
extern "C"  void NativeImagePicker_FromLibrary_m3315316428 (Il2CppObject * __this /* static, unused */, CallbackImagePicked_t554520473 * ___onPicked0, bool ___allowedEditing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeImagePicker::FromCamera(NativeImagePicker/CallbackImagePicked,System.Boolean)
extern "C"  void NativeImagePicker_FromCamera_m2896619742 (Il2CppObject * __this /* static, unused */, CallbackImagePicked_t554520473 * ___onPicked0, bool ___allowedEditing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
