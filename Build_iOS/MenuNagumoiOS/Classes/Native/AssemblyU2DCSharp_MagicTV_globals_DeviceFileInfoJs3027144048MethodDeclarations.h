﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.DeviceFileInfoJson
struct DeviceFileInfoJson_t3027144048;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Collections.Generic.List`1<MagicTV.vo.BundleVO>
struct List_1_t3352703625;
// ReturnDataVO
struct ReturnDataVO_t544837971;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>
struct IEnumerable_1_t1517202659;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO>
struct IEnumerable_1_t767933189;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.FileVO>
struct IEnumerable_1_t941294320;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ResultRevisionVO2445597391.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"

// System.Void MagicTV.globals.DeviceFileInfoJson::.ctor()
extern "C"  void DeviceFileInfoJson__ctor_m2987725398 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::prepare()
extern "C"  void DeviceFileInfoJson_prepare_m3092934171 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.globals.DeviceFileInfoJson::getJsonLocalFileUrl()
extern "C"  String_t* DeviceFileInfoJson_getJsonLocalFileUrl_m2591108343 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.globals.DeviceFileInfoJson::getJsonLocalFolder()
extern "C"  String_t* DeviceFileInfoJson_getJsonLocalFolder_m281905964 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::getResultRevisionFromFile()
extern "C"  void DeviceFileInfoJson_getResultRevisionFromFile_m1906532744 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.globals.DeviceFileInfoJson::DoReadFile(UnityEngine.WWW)
extern "C"  Il2CppObject * DeviceFileInfoJson_DoReadFile_m1901812415 (DeviceFileInfoJson_t3027144048 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::FinishedRead(UnityEngine.WWW)
extern "C"  void DeviceFileInfoJson_FinishedRead_m580675932 (DeviceFileInfoJson_t3027144048 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::populateIndexedBundles()
extern "C"  void DeviceFileInfoJson_populateIndexedBundles_m4181248472 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::addBundle(System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>)
extern "C"  void DeviceFileInfoJson_addBundle_m3795320176 (DeviceFileInfoJson_t3027144048 * __this, String_t* ___value0, List_1_t3352703625 * ___list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::cacheBundle(System.String)
extern "C"  void DeviceFileInfoJson_cacheBundle_m3720725258 (DeviceFileInfoJson_t3027144048 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO MagicTV.globals.DeviceFileInfoJson::commit(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoJson_commit_m1937271292 (DeviceFileInfoJson_t3027144048 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::setUrlsToDelete(MagicTV.vo.ResultRevisionVO,MagicTV.vo.ResultRevisionVO&)
extern "C"  void DeviceFileInfoJson_setUrlsToDelete_m584727580 (DeviceFileInfoJson_t3027144048 * __this, ResultRevisionVO_t2445597391 * ___current0, ResultRevisionVO_t2445597391 ** ___newOne1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.globals.DeviceFileInfoJson::filterUrlFromId(MagicTV.vo.UrlInfoVO[],System.String)
extern "C"  UrlInfoVO_t1761987528 * DeviceFileInfoJson_filterUrlFromId_m1235693485 (DeviceFileInfoJson_t3027144048 * __this, UrlInfoVOU5BU5D_t1681515545* ___urls0, String_t* ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO MagicTV.globals.DeviceFileInfoJson::getFilteredRevision(MagicTV.vo.ResultRevisionVO)
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfoJson_getFilteredRevision_m4041424155 (DeviceFileInfoJson_t3027144048 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] MagicTV.globals.DeviceFileInfoJson::revisionToUrls(MagicTV.vo.ResultRevisionVO)
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfoJson_revisionToUrls_m4248683912 (DeviceFileInfoJson_t3027144048 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.globals.DeviceFileInfoJson::getUrlLocalInfoFiltered(MagicTV.vo.UrlInfoVO,MagicTV.vo.UrlInfoVO[])
extern "C"  UrlInfoVO_t1761987528 * DeviceFileInfoJson_getUrlLocalInfoFiltered_m3229324502 (DeviceFileInfoJson_t3027144048 * __this, UrlInfoVO_t1761987528 * ___novaUrl0, UrlInfoVOU5BU5D_t1681515545* ___currents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO MagicTV.globals.DeviceFileInfoJson::commit(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoJson_commit_m2106673583 (DeviceFileInfoJson_t3027144048 * __this, UrlInfoVO_t1761987528 * ___urlInfoVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::upgradeUrlIntoRevision(MagicTV.vo.UrlInfoVO,MagicTV.vo.ResultRevisionVO&)
extern "C"  void DeviceFileInfoJson_upgradeUrlIntoRevision_m3480564395 (DeviceFileInfoJson_t3027144048 * __this, UrlInfoVO_t1761987528 * ___urlInfoVO0, ResultRevisionVO_t2445597391 ** ___revision1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO MagicTV.globals.DeviceFileInfoJson::delete(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoJson_delete_m2298812195 (DeviceFileInfoJson_t3027144048 * __this, UrlInfoVO_t1761987528 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.FileVO MagicTV.globals.DeviceFileInfoJson::getFileById(System.String)
extern "C"  FileVO_t1935348659 * DeviceFileInfoJson_getFileById_m2371445682 (DeviceFileInfoJson_t3027144048 * __this, String_t* ___bundle_file_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO MagicTV.globals.DeviceFileInfoJson::delete(MagicTV.vo.UrlInfoVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoJson_delete_m1545380225 (DeviceFileInfoJson_t3027144048 * __this, UrlInfoVOU5BU5D_t1681515545* ___objs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO[] MagicTV.globals.DeviceFileInfoJson::getBundles(System.String)
extern "C"  BundleVOU5BU5D_t2162892100* DeviceFileInfoJson_getBundles_m1239418073 (DeviceFileInfoJson_t3027144048 * __this, String_t* ___appType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO MagicTV.globals.DeviceFileInfoJson::getCurrentRevision()
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfoJson_getCurrentRevision_m1662840728 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO MagicTV.globals.DeviceFileInfoJson::getRevisionByRevisionId(System.String)
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfoJson_getRevisionByRevisionId_m2683751804 (DeviceFileInfoJson_t3027144048 * __this, String_t* ___revision_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO MagicTV.globals.DeviceFileInfoJson::getUncachedRevision()
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfoJson_getUncachedRevision_m1695385268 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] MagicTV.globals.DeviceFileInfoJson::getUrlInfoVOToDelete()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfoJson_getUrlInfoVOToDelete_m2587243887 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] MagicTV.globals.DeviceFileInfoJson::getAllUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfoJson_getAllUrlInfoVO_m29517572 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] MagicTV.globals.DeviceFileInfoJson::getCachedUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfoJson_getCachedUrlInfoVO_m3914021575 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::SetCurrentRevision(MagicTV.vo.ResultRevisionVO)
extern "C"  void DeviceFileInfoJson_SetCurrentRevision_m3970913099 (DeviceFileInfoJson_t3027144048 * __this, ResultRevisionVO_t2445597391 * ___revisionVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson::Save()
extern "C"  void DeviceFileInfoJson_Save_m2954972779 (DeviceFileInfoJson_t3027144048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO> MagicTV.globals.DeviceFileInfoJson::<populateIndexedBundles>m__37(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfoJson_U3CpopulateIndexedBundlesU3Em__37_m3492261726 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson::<populateIndexedBundles>m__38(MagicTV.vo.MetadataVO)
extern "C"  bool DeviceFileInfoJson_U3CpopulateIndexedBundlesU3Em__38_m2558372634 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO> MagicTV.globals.DeviceFileInfoJson::<revisionToUrls>m__3B(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfoJson_U3CrevisionToUrlsU3Em__3B_m2413748673 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.FileVO> MagicTV.globals.DeviceFileInfoJson::<getFileById>m__3C(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfoJson_U3CgetFileByIdU3Em__3C_m1269254517 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__3E(MagicTV.vo.BundleVO)
extern "C"  BundleVO_t1984518073 * DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__3E_m1899121216 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___bx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__3F(MagicTV.vo.MetadataVO)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__3F_m1320890303 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___mx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__40(MagicTV.vo.BundleVO)
extern "C"  bool DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__40_m3635356516 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson::<getCachedUrlInfoVO>m__41(MagicTV.vo.UrlInfoVO)
extern "C"  bool DeviceFileInfoJson_U3CgetCachedUrlInfoVOU3Em__41_m3841050246 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___u0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.UrlInfoVO> MagicTV.globals.DeviceFileInfoJson::<revisionToUrls>m__42(MagicTV.vo.FileVO)
extern "C"  Il2CppObject* DeviceFileInfoJson_U3CrevisionToUrlsU3Em__42_m292647640 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__43(MagicTV.vo.FileVO)
extern "C"  bool DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__43_m1610986695 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___fx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.FileVO MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__44(MagicTV.vo.FileVO)
extern "C"  FileVO_t1935348659 * DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__44_m1191409522 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___fx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__45(MagicTV.vo.MetadataVO)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__45_m382795761 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___mx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__46(MagicTV.vo.FileVO)
extern "C"  bool DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__46_m384514020 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__47(MagicTV.vo.UrlInfoVO)
extern "C"  bool DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__47_m1219444132 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___ux0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.globals.DeviceFileInfoJson::<getUncachedRevision>m__48(MagicTV.vo.UrlInfoVO)
extern "C"  UrlInfoVO_t1761987528 * DeviceFileInfoJson_U3CgetUncachedRevisionU3Em__48_m3535228686 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___ux0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
