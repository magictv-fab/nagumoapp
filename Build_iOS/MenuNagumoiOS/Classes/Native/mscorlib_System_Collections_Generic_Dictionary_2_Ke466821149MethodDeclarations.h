﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>
struct KeyCollection_t466821149;
// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Collections.Generic.IEnumerator`1<ZXing.ResultMetadataType>
struct IEnumerator_1_t540264725;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.ResultMetadataType[]
struct ResultMetadataTypeU5BU5D_t2197797205;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3749965048.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1475468842_gshared (KeyCollection_t466821149 * __this, Dictionary_2_t3135028994 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1475468842(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t466821149 *, Dictionary_2_t3135028994 *, const MethodInfo*))KeyCollection__ctor_m1475468842_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3256338668_gshared (KeyCollection_t466821149 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3256338668(__this, ___item0, method) ((  void (*) (KeyCollection_t466821149 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3256338668_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3377674595_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3377674595(__this, method) ((  void (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3377674595_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1890999650_gshared (KeyCollection_t466821149 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1890999650(__this, ___item0, method) ((  bool (*) (KeyCollection_t466821149 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1890999650_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2925444935_gshared (KeyCollection_t466821149 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2925444935(__this, ___item0, method) ((  bool (*) (KeyCollection_t466821149 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2925444935_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1477277493_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1477277493(__this, method) ((  Il2CppObject* (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1477277493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2387812693_gshared (KeyCollection_t466821149 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2387812693(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t466821149 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2387812693_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3378895908_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3378895908(__this, method) ((  Il2CppObject * (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3378895908_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3360556099_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3360556099(__this, method) ((  bool (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3360556099_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3080254645_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3080254645(__this, method) ((  bool (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3080254645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3608160039_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3608160039(__this, method) ((  Il2CppObject * (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3608160039_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3725749471_gshared (KeyCollection_t466821149 * __this, ResultMetadataTypeU5BU5D_t2197797205* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3725749471(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t466821149 *, ResultMetadataTypeU5BU5D_t2197797205*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3725749471_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3749965048  KeyCollection_GetEnumerator_m1807819308_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1807819308(__this, method) ((  Enumerator_t3749965048  (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_GetEnumerator_m1807819308_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2184843375_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2184843375(__this, method) ((  int32_t (*) (KeyCollection_t466821149 *, const MethodInfo*))KeyCollection_get_Count_m2184843375_gshared)(__this, method)
