﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/PageErrorReceivedDelegate
struct PageErrorReceivedDelegate_t4149620697;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/PageErrorReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void PageErrorReceivedDelegate__ctor_m96608256 (PageErrorReceivedDelegate_t4149620697 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/PageErrorReceivedDelegate::Invoke(UniWebView,System.Int32,System.String)
extern "C"  void PageErrorReceivedDelegate_Invoke_m1917544226 (PageErrorReceivedDelegate_t4149620697 * __this, UniWebView_t424341801 * ___webView0, int32_t ___errorCode1, String_t* ___errorMessage2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/PageErrorReceivedDelegate::BeginInvoke(UniWebView,System.Int32,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PageErrorReceivedDelegate_BeginInvoke_m2609522163 (PageErrorReceivedDelegate_t4149620697 * __this, UniWebView_t424341801 * ___webView0, int32_t ___errorCode1, String_t* ___errorMessage2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/PageErrorReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void PageErrorReceivedDelegate_EndInvoke_m1680183824 (PageErrorReceivedDelegate_t4149620697 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
