﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// cameraFake/<delay>c__Iterator3
struct U3CdelayU3Ec__Iterator3_t616445860;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void cameraFake/<delay>c__Iterator3::.ctor()
extern "C"  void U3CdelayU3Ec__Iterator3__ctor_m4132879735 (U3CdelayU3Ec__Iterator3_t616445860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraFake/<delay>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdelayU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3066778117 (U3CdelayU3Ec__Iterator3_t616445860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraFake/<delay>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdelayU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1277427609 (U3CdelayU3Ec__Iterator3_t616445860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean cameraFake/<delay>c__Iterator3::MoveNext()
extern "C"  bool U3CdelayU3Ec__Iterator3_MoveNext_m711278085 (U3CdelayU3Ec__Iterator3_t616445860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake/<delay>c__Iterator3::Dispose()
extern "C"  void U3CdelayU3Ec__Iterator3_Dispose_m3045926900 (U3CdelayU3Ec__Iterator3_t616445860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake/<delay>c__Iterator3::Reset()
extern "C"  void U3CdelayU3Ec__Iterator3_Reset_m1779312676 (U3CdelayU3Ec__Iterator3_t616445860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
