﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.events.ChangeEventAbstract
struct ChangeEventAbstract_t369667058;

#include "AssemblyU2DCSharp_ARM_events_BaseDelayToggleEventA1686333410.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.ChangeEventToggleOnOff
struct  ChangeEventToggleOnOff_t892756926  : public BaseDelayToggleEventAbstract_t1686333410
{
public:
	// ARM.events.ChangeEventAbstract ARM.events.ChangeEventToggleOnOff::PluginChange
	ChangeEventAbstract_t369667058 * ___PluginChange_17;
	// System.Boolean ARM.events.ChangeEventToggleOnOff::_hasPluginChange
	bool ____hasPluginChange_18;

public:
	inline static int32_t get_offset_of_PluginChange_17() { return static_cast<int32_t>(offsetof(ChangeEventToggleOnOff_t892756926, ___PluginChange_17)); }
	inline ChangeEventAbstract_t369667058 * get_PluginChange_17() const { return ___PluginChange_17; }
	inline ChangeEventAbstract_t369667058 ** get_address_of_PluginChange_17() { return &___PluginChange_17; }
	inline void set_PluginChange_17(ChangeEventAbstract_t369667058 * value)
	{
		___PluginChange_17 = value;
		Il2CppCodeGenWriteBarrier(&___PluginChange_17, value);
	}

	inline static int32_t get_offset_of__hasPluginChange_18() { return static_cast<int32_t>(offsetof(ChangeEventToggleOnOff_t892756926, ____hasPluginChange_18)); }
	inline bool get__hasPluginChange_18() const { return ____hasPluginChange_18; }
	inline bool* get_address_of__hasPluginChange_18() { return &____hasPluginChange_18; }
	inline void set__hasPluginChange_18(bool value)
	{
		____hasPluginChange_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
