﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNIOSRateUsPopUp
struct MNIOSRateUsPopUp_t2151207298;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNIOSRateUsPopUp::.ctor()
extern "C"  void MNIOSRateUsPopUp__ctor_m700639065 (MNIOSRateUsPopUp_t2151207298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create()
extern "C"  MNIOSRateUsPopUp_t2151207298 * MNIOSRateUsPopUp_Create_m781992194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create(System.String,System.String)
extern "C"  MNIOSRateUsPopUp_t2151207298 * MNIOSRateUsPopUp_Create_m674531708 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String)
extern "C"  MNIOSRateUsPopUp_t2151207298 * MNIOSRateUsPopUp_Create_m2747980848 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___rate2, String_t* ___remind3, String_t* ___declined4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSRateUsPopUp::init()
extern "C"  void MNIOSRateUsPopUp_init_m2968480699 (MNIOSRateUsPopUp_t2151207298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSRateUsPopUp::onPopUpCallBack(System.String)
extern "C"  void MNIOSRateUsPopUp_onPopUpCallBack_m2802815833 (MNIOSRateUsPopUp_t2151207298 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
