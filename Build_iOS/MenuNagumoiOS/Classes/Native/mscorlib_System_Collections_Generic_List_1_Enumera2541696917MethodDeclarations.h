﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int64>
struct List_1_t2522024147;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696917.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int64>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3798643073_gshared (Enumerator_t2541696917 * __this, List_1_t2522024147 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3798643073(__this, ___l0, method) ((  void (*) (Enumerator_t2541696917 *, List_1_t2522024147 *, const MethodInfo*))Enumerator__ctor_m3798643073_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m411678257_gshared (Enumerator_t2541696917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m411678257(__this, method) ((  void (*) (Enumerator_t2541696917 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m411678257_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2178112231_gshared (Enumerator_t2541696917 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2178112231(__this, method) ((  Il2CppObject * (*) (Enumerator_t2541696917 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2178112231_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m1201074086_gshared (Enumerator_t2541696917 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1201074086(__this, method) ((  void (*) (Enumerator_t2541696917 *, const MethodInfo*))Enumerator_Dispose_m1201074086_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int64>::VerifyState()
extern "C"  void Enumerator_VerifyState_m652110815_gshared (Enumerator_t2541696917 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m652110815(__this, method) ((  void (*) (Enumerator_t2541696917 *, const MethodInfo*))Enumerator_VerifyState_m652110815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2703926369_gshared (Enumerator_t2541696917 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2703926369(__this, method) ((  bool (*) (Enumerator_t2541696917 *, const MethodInfo*))Enumerator_MoveNext_m2703926369_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int64>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m982325240_gshared (Enumerator_t2541696917 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m982325240(__this, method) ((  int64_t (*) (Enumerator_t2541696917 *, const MethodInfo*))Enumerator_get_Current_m982325240_gshared)(__this, method)
