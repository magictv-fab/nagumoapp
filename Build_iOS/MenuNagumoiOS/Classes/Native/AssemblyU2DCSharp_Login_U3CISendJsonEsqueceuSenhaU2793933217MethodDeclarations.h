﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login/<ISendJsonEsqueceuSenha>c__Iterator62
struct U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Login/<ISendJsonEsqueceuSenha>c__Iterator62::.ctor()
extern "C"  void U3CISendJsonEsqueceuSenhaU3Ec__Iterator62__ctor_m3087537898 (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<ISendJsonEsqueceuSenha>c__Iterator62::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1123455528 (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<ISendJsonEsqueceuSenha>c__Iterator62::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_System_Collections_IEnumerator_get_Current_m3449939388 (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Login/<ISendJsonEsqueceuSenha>c__Iterator62::MoveNext()
extern "C"  bool U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_MoveNext_m3200242890 (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<ISendJsonEsqueceuSenha>c__Iterator62::Dispose()
extern "C"  void U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_Dispose_m3494768807 (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<ISendJsonEsqueceuSenha>c__Iterator62::Reset()
extern "C"  void U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_Reset_m733970839 (U3CISendJsonEsqueceuSenhaU3Ec__Iterator62_t2793933217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
