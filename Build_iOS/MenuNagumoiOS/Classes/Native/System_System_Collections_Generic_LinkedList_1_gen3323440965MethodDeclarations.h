﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t3323440965;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t770573207;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_LinkedList_1_Enum511628229.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C"  void LinkedList_1__ctor_m2399516069_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m2399516069(__this, method) ((  void (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1__ctor_m2399516069_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1__ctor_m3250722177_gshared (LinkedList_1_t3323440965 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define LinkedList_1__ctor_m3250722177(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t3323440965 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1__ctor_m3250722177_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m626514495_gshared (LinkedList_1_t3323440965 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m626514495(__this, ___value0, method) ((  void (*) (LinkedList_1_t3323440965 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m626514495_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m1204564228_gshared (LinkedList_1_t3323440965 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m1204564228(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t3323440965 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m1204564228_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2862642506_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2862642506(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2862642506_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2334691967_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2334691967(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2334691967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1831363171_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1831363171(__this, method) ((  bool (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1831363171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m821292858_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m821292858(__this, method) ((  bool (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m821292858_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m1431399256_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m1431399256(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m1431399256_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_VerifyReferencedNode_m4084278763_gshared (LinkedList_1_t3323440965 * __this, LinkedListNode_1_t770573207 * ___node0, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m4084278763(__this, ___node0, method) ((  void (*) (LinkedList_1_t3323440965 *, LinkedListNode_1_t770573207 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m4084278763_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddFirst(T)
extern "C"  LinkedListNode_1_t770573207 * LinkedList_1_AddFirst_m1153550589_gshared (LinkedList_1_t3323440965 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_AddFirst_m1153550589(__this, ___value0, method) ((  LinkedListNode_1_t770573207 * (*) (LinkedList_1_t3323440965 *, int32_t, const MethodInfo*))LinkedList_1_AddFirst_m1153550589_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C"  LinkedListNode_1_t770573207 * LinkedList_1_AddLast_m4188686418_gshared (LinkedList_1_t3323440965 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_AddLast_m4188686418(__this, ___value0, method) ((  LinkedListNode_1_t770573207 * (*) (LinkedList_1_t3323440965 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m4188686418_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C"  void LinkedList_1_Clear_m1056829547_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m1056829547(__this, method) ((  void (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_Clear_m1056829547_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m706601251_gshared (LinkedList_1_t3323440965 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_Contains_m706601251(__this, ___value0, method) ((  bool (*) (LinkedList_1_t3323440965 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m706601251_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void LinkedList_1_CopyTo_m1139882607_gshared (LinkedList_1_t3323440965 * __this, Int32U5BU5D_t3230847821* ___array0, int32_t ___index1, const MethodInfo* method);
#define LinkedList_1_CopyTo_m1139882607(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t3323440965 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m1139882607_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C"  LinkedListNode_1_t770573207 * LinkedList_1_Find_m29770003_gshared (LinkedList_1_t3323440965 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_Find_m29770003(__this, ___value0, method) ((  LinkedListNode_1_t770573207 * (*) (LinkedList_1_t3323440965 *, int32_t, const MethodInfo*))LinkedList_1_Find_m29770003_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t511628229  LinkedList_1_GetEnumerator_m3435228161_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m3435228161(__this, method) ((  Enumerator_t511628229  (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3435228161_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1_GetObjectData_m3013233630_gshared (LinkedList_1_t3323440965 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m3013233630(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t3323440965 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1_GetObjectData_m3013233630_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C"  void LinkedList_1_OnDeserialization_m734131526_gshared (LinkedList_1_t3323440965 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m734131526(__this, ___sender0, method) ((  void (*) (LinkedList_1_t3323440965 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m734131526_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m4228642184_gshared (LinkedList_1_t3323440965 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_Remove_m4228642184(__this, ___value0, method) ((  bool (*) (LinkedList_1_t3323440965 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m4228642184_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m2999103299_gshared (LinkedList_1_t3323440965 * __this, LinkedListNode_1_t770573207 * ___node0, const MethodInfo* method);
#define LinkedList_1_Remove_m2999103299(__this, ___node0, method) ((  void (*) (LinkedList_1_t3323440965 *, LinkedListNode_1_t770573207 *, const MethodInfo*))LinkedList_1_Remove_m2999103299_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m1940347262_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m1940347262(__this, method) ((  void (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_RemoveLast_m1940347262_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m2874146351_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m2874146351(__this, method) ((  int32_t (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_get_Count_m2874146351_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C"  LinkedListNode_1_t770573207 * LinkedList_1_get_First_m1553068049_gshared (LinkedList_1_t3323440965 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m1553068049(__this, method) ((  LinkedListNode_1_t770573207 * (*) (LinkedList_1_t3323440965 *, const MethodInfo*))LinkedList_1_get_First_m1553068049_gshared)(__this, method)
