﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>
struct ValueCollection_t867596585;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2166990872;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1908816725;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Val98824280.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3711676323_gshared (ValueCollection_t867596585 * __this, Dictionary_2_t2166990872 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3711676323(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t867596585 *, Dictionary_2_t2166990872 *, const MethodInfo*))ValueCollection__ctor_m3711676323_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3314322479_gshared (ValueCollection_t867596585 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3314322479(__this, ___item0, method) ((  void (*) (ValueCollection_t867596585 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3314322479_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3470125176_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3470125176(__this, method) ((  void (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3470125176_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m489571671_gshared (ValueCollection_t867596585 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m489571671(__this, ___item0, method) ((  bool (*) (ValueCollection_t867596585 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m489571671_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2830550972_gshared (ValueCollection_t867596585 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2830550972(__this, ___item0, method) ((  bool (*) (ValueCollection_t867596585 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2830550972_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2329808262_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2329808262(__this, method) ((  Il2CppObject* (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2329808262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1644057468_gshared (ValueCollection_t867596585 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1644057468(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t867596585 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1644057468_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3441146999_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3441146999(__this, method) ((  Il2CppObject * (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3441146999_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3059714826_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3059714826(__this, method) ((  bool (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3059714826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m501649386_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m501649386(__this, method) ((  bool (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m501649386_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2031938262_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2031938262(__this, method) ((  Il2CppObject * (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2031938262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3682165546_gshared (ValueCollection_t867596585 * __this, SingleU5BU5D_t2316563989* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3682165546(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t867596585 *, SingleU5BU5D_t2316563989*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3682165546_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::GetEnumerator()
extern "C"  Enumerator_t98824280  ValueCollection_GetEnumerator_m2317742477_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2317742477(__this, method) ((  Enumerator_t98824280  (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_GetEnumerator_m2317742477_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1032615472_gshared (ValueCollection_t867596585 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1032615472(__this, method) ((  int32_t (*) (ValueCollection_t867596585 *, const MethodInfo*))ValueCollection_get_Count_m1032615472_gshared)(__this, method)
