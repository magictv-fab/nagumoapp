﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage
struct DiskArchiveStorage_t2562884100;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t2937401711;
// System.IO.Stream
struct Stream_t1561764144;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2937401711.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_File4232823638.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile,ICSharpCode.SharpZipLib.Zip.FileUpdateMode)
extern "C"  void DiskArchiveStorage__ctor_m841331796 (DiskArchiveStorage_t2562884100 * __this, ZipFile_t2937401711 * ___file0, int32_t ___updateMode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile)
extern "C"  void DiskArchiveStorage__ctor_m1036299500 (DiskArchiveStorage_t2562884100 * __this, ZipFile_t2937401711 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::GetTemporaryOutput()
extern "C"  Stream_t1561764144 * DiskArchiveStorage_GetTemporaryOutput_m2813600525 (DiskArchiveStorage_t2562884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::ConvertTemporaryToFinal()
extern "C"  Stream_t1561764144 * DiskArchiveStorage_ConvertTemporaryToFinal_m2798574382 (DiskArchiveStorage_t2562884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::MakeTemporaryCopy(System.IO.Stream)
extern "C"  Stream_t1561764144 * DiskArchiveStorage_MakeTemporaryCopy_m848279328 (DiskArchiveStorage_t2562884100 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::OpenForDirectUpdate(System.IO.Stream)
extern "C"  Stream_t1561764144 * DiskArchiveStorage_OpenForDirectUpdate_m1339788953 (DiskArchiveStorage_t2562884100 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::Dispose()
extern "C"  void DiskArchiveStorage_Dispose_m3620746916 (DiskArchiveStorage_t2562884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.DiskArchiveStorage::GetTempFileName(System.String,System.Boolean)
extern "C"  String_t* DiskArchiveStorage_GetTempFileName_m2811341966 (Il2CppObject * __this /* static, unused */, String_t* ___original0, bool ___makeTempFile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
