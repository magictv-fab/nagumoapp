﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// HutongGames.PlayMaker.Actions.SetAudioPitch
struct SetAudioPitch_t607907034;
// HutongGames.PlayMaker.Actions.SetAudioVolume
struct SetAudioVolume_t1193383862;
// HutongGames.PlayMaker.Actions.SetBackgroundColor
struct SetBackgroundColor_t3440566907;
// HutongGames.PlayMaker.Actions.SetBoolValue
struct SetBoolValue_t2866875533;
// HutongGames.PlayMaker.Actions.SetCameraCullingMask
struct SetCameraCullingMask_t2588626109;
// HutongGames.PlayMaker.Actions.SetCameraFOV
struct SetCameraFOV_t480289614;
// HutongGames.PlayMaker.Actions.SetColorRGBA
struct SetColorRGBA_t2803139325;
// HutongGames.PlayMaker.Actions.SetColorValue
struct SetColorValue_t353523614;
// HutongGames.PlayMaker.Actions.SetDrag
struct SetDrag_t3798420676;
// HutongGames.PlayMaker.Actions.SetEventData
struct SetEventData_t853237066;
// HutongGames.PlayMaker.Actions.SetEventTarget
struct SetEventTarget_t788190705;
// HutongGames.PlayMaker.Actions.SetFlareStrength
struct SetFlareStrength_t2048001173;
// HutongGames.PlayMaker.Actions.SetFloatValue
struct SetFloatValue_t2341951557;
// HutongGames.PlayMaker.Actions.SetFogColor
struct SetFogColor_t1692261973;
// HutongGames.PlayMaker.Actions.SetFogDensity
struct SetFogDensity_t4077399578;
// HutongGames.PlayMaker.Actions.SetFsmBool
struct SetFsmBool_t888287984;
// HutongGames.PlayMaker.Actions.SetFsmColor
struct SetFsmColor_t1119084307;
// HutongGames.PlayMaker.Actions.SetFsmFloat
struct SetFsmFloat_t1121767948;
// HutongGames.PlayMaker.Actions.SetFsmGameObject
struct SetFsmGameObject_t2293859735;
// HutongGames.PlayMaker.Actions.SetFsmInt
struct SetFsmInt_t326690015;
// HutongGames.PlayMaker.Actions.SetFsmMaterial
struct SetFsmMaterial_t1859975085;
// HutongGames.PlayMaker.Actions.SetFsmObject
struct SetFsmObject_t14389381;
// HutongGames.PlayMaker.Actions.SetFsmQuaternion
struct SetFsmQuaternion_t172880612;
// HutongGames.PlayMaker.Actions.SetFsmRect
struct SetFsmRect_t888754666;
// HutongGames.PlayMaker.Actions.SetFsmString
struct SetFsmString_t145771863;
// HutongGames.PlayMaker.Actions.SetFsmTexture
struct SetFsmTexture_t38942603;
// HutongGames.PlayMaker.Actions.SetFsmVariable
struct SetFsmVariable_t311321858;
// HutongGames.PlayMaker.Actions.SetFsmVector2
struct SetFsmVector2_t1794550207;
// HutongGames.PlayMaker.Actions.SetFsmVector3
struct SetFsmVector3_t1794550208;
// HutongGames.PlayMaker.Actions.SetGameObject
struct SetGameObject_t3864026529;
// HutongGames.PlayMaker.Actions.SetGameVolume
struct SetGameVolume_t4076511612;
// HutongGames.PlayMaker.Actions.SetGravity
struct SetGravity_t1737623796;
// HutongGames.PlayMaker.Actions.SetGUIAlpha
struct SetGUIAlpha_t974005747;
// HutongGames.PlayMaker.Actions.SetGUIBackgroundColor
struct SetGUIBackgroundColor_t221834986;
// HutongGames.PlayMaker.Actions.SetGUIColor
struct SetGUIColor_t975938552;
// HutongGames.PlayMaker.Actions.SetGUIContentColor
struct SetGUIContentColor_t3816209131;
// HutongGames.PlayMaker.Actions.SetGUIDepth
struct SetGUIDepth_t976568152;
// HutongGames.PlayMaker.Actions.SetGUISkin
struct SetGUISkin_t884172798;
// HutongGames.PlayMaker.Actions.SetGUIText
struct SetGUIText_t884197294;
// HutongGames.PlayMaker.Actions.SetGUITexture
struct SetGUITexture_t4209792816;
// HutongGames.PlayMaker.Actions.SetGUITextureAlpha
struct SetGUITextureAlpha_t1478996356;
// HutongGames.PlayMaker.Actions.SetGUITextureColor
struct SetGUITextureColor_t1480929161;
// HutongGames.PlayMaker.Actions.SetHaloStrength
struct SetHaloStrength_t22635309;
// HutongGames.PlayMaker.Actions.SetIntFromFloat
struct SetIntFromFloat_t4253369395;
// HutongGames.PlayMaker.Actions.SetIntValue
struct SetIntValue_t2127530962;
// HutongGames.PlayMaker.Actions.SetIsKinematic
struct SetIsKinematic_t1872148257;
// HutongGames.PlayMaker.Actions.SetJointConnectedBody
struct SetJointConnectedBody_t2876069553;
// UnityEngine.Joint
struct Joint_t4201008640;
// System.Object
struct Il2CppObject;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// HutongGames.PlayMaker.Actions.SetLayer
struct SetLayer_t1144868535;
// HutongGames.PlayMaker.Actions.SetLightColor
struct SetLightColor_t3133114301;
// HutongGames.PlayMaker.Actions.SetLightCookie
struct SetLightCookie_t1988387904;
// HutongGames.PlayMaker.Actions.SetLightFlare
struct SetLightFlare_t3135785000;
// HutongGames.PlayMaker.Actions.SetLightIntensity
struct SetLightIntensity_t4270899469;
// HutongGames.PlayMaker.Actions.SetLightRange
struct SetLightRange_t3146551703;
// HutongGames.PlayMaker.Actions.SetLightSpotAngle
struct SetLightSpotAngle_t2812053963;
// HutongGames.PlayMaker.Actions.SetLightType
struct SetLightType_t1507991894;
// HutongGames.PlayMaker.Actions.SetMainCamera
struct SetMainCamera_t3356853774;
// HutongGames.PlayMaker.Actions.SetMass
struct SetMass_t3798673028;
// HutongGames.PlayMaker.Actions.SetMaterial
struct SetMaterial_t1870547511;
// HutongGames.PlayMaker.Actions.SetMaterialColor
struct SetMaterialColor_t3420784866;
// HutongGames.PlayMaker.Actions.SetMaterialFloat
struct SetMaterialFloat_t3423468507;
// HutongGames.PlayMaker.Actions.SetMaterialTexture
struct SetMaterialTexture_t65022362;
// HutongGames.PlayMaker.Actions.SetMouseCursor
struct SetMouseCursor_t1046826369;
// HutongGames.PlayMaker.Actions.SetName
struct SetName_t3798702619;
// HutongGames.PlayMaker.Actions.SetObjectValue
struct SetObjectValue_t3678867096;
// HutongGames.PlayMaker.Actions.SetParent
struct SetParent_t596534010;
// HutongGames.PlayMaker.Actions.SetPosition
struct SetPosition_t2319285817;
// HutongGames.PlayMaker.Actions.SetProceduralBoolean
struct SetProceduralBoolean_t1677897425;
// HutongGames.PlayMaker.Actions.SetProceduralColor
struct SetProceduralColor_t3246619020;
// HutongGames.PlayMaker.Actions.SetProceduralFloat
struct SetProceduralFloat_t3249302661;
// HutongGames.PlayMaker.Actions.SetProperty
struct SetProperty_t578339557;
// HutongGames.PlayMaker.Actions.SetRandomMaterial
struct SetRandomMaterial_t2485166170;
// HutongGames.PlayMaker.Actions.SetRandomRotation
struct SetRandomRotation_t2145798833;
// HutongGames.PlayMaker.Actions.SetRectFields
struct SetRectFields_t2990573965;
// HutongGames.PlayMaker.Actions.SetRectValue
struct SetRectValue_t1933064659;
// HutongGames.PlayMaker.Actions.SetRotation
struct SetRotation_t1531180174;
// HutongGames.PlayMaker.Actions.SetScale
struct SetScale_t1151369904;
// HutongGames.PlayMaker.Actions.SetShadowStrength
struct SetShadowStrength_t3140252369;
// HutongGames.PlayMaker.Actions.SetSkybox
struct SetSkybox_t691862362;
// HutongGames.PlayMaker.Actions.SetStringValue
struct SetStringValue_t3445720326;
// HutongGames.PlayMaker.Actions.SetTag
struct SetTag_t3330067392;
// HutongGames.PlayMaker.Actions.SetTagsOnChildren
struct SetTagsOnChildren_t932473671;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.Actions.SetTextureOffset
struct SetTextureOffset_t467579892;
// HutongGames.PlayMaker.Actions.SetTextureScale
struct SetTextureScale_t1425090431;
// HutongGames.PlayMaker.Actions.SetVector3Value
struct SetVector3Value_t2514376337;
// HutongGames.PlayMaker.Actions.SetVector3XYZ
struct SetVector3XYZ_t3385117689;
// HutongGames.PlayMaker.Actions.SetVelocity
struct SetVelocity_t3705741805;
// HutongGames.PlayMaker.Actions.SetVisibility
struct SetVisibility_t1014047458;
// HutongGames.PlayMaker.Actions.Sleep
struct Sleep_t1652571045;
// HutongGames.PlayMaker.Actions.SmoothFollowAction
struct SmoothFollowAction_t1760465533;
// HutongGames.PlayMaker.Actions.SmoothLookAt
struct SmoothLookAt_t4236557480;
// HutongGames.PlayMaker.Actions.SmoothLookAtDirection
struct SmoothLookAtDirection_t4235776941;
// HutongGames.PlayMaker.Actions.StartCoroutine
struct StartCoroutine_t1447367550;
// HutongGames.PlayMaker.Actions.StartLocationServiceUpdates
struct StartLocationServiceUpdates_t407515898;
// HutongGames.PlayMaker.Actions.StartServer
struct StartServer_t1997133011;
// HutongGames.PlayMaker.Actions.StopAnimation
struct StopAnimation_t1998114672;
// HutongGames.PlayMaker.Actions.StopLocationServiceUpdates
struct StopLocationServiceUpdates_t2528039284;
// HutongGames.PlayMaker.Actions.StringChanged
struct StringChanged_t3723251505;
// HutongGames.PlayMaker.Actions.StringCompare
struct StringCompare_t3934792034;
// HutongGames.PlayMaker.Actions.StringContains
struct StringContains_t1102823480;
// HutongGames.PlayMaker.Actions.StringReplace
struct StringReplace_t4078804785;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set607907034.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set607907034MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com715545838MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Boolean476798718.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1193383862.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1193383862MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3440566907.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3440566907MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1702563344MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2866875533.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2866875533MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2588626109.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2588626109MethodDeclarations.h"
#include "PlayMaker_ArrayTypes.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set480289614.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set480289614MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2803139325.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2803139325MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set353523614.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set353523614MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3798420676.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3798420676MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set853237066.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set853237066MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set788190705.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set788190705MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2048001173.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2048001173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2341951557.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2341951557MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1692261973.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1692261973MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4077399578.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4077399578MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set888287984.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set888287984MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1119084307.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1119084307MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1121767948.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1121767948MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2293859735.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2293859735MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set326690015.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set326690015MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1859975085.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1859975085MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetF14389381.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetF14389381MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set172880612.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set172880612MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set888754666.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set888754666MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set145771863.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set145771863MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetF38942603.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetF38942603MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set311321858.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set311321858MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility81143630MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1794550207.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1794550207MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1794550208.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1794550208MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3864026529.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3864026529MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4076511612.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4076511612MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1737623796.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1737623796MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set974005747.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set974005747MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "PlayMaker_PlayMakerGUI3799848395MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set221834986.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set221834986MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set975938552.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set975938552MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3816209131.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3816209131MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set976568152.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set976568152MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set884172798.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set884172798MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set884197294.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set884197294MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2346840805MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIText3371372606MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4209792816.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4209792816MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2995916491MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1478996356.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1478996356MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1480929161.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1480929161MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetH22635309.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetH22635309MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4253369395.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4253369395MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2127530962.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2127530962MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1872148257.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1872148257MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2876069553.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2876069553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint4201008640MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint4201008640.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1144868535.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1144868535MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3133114301.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3133114301MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3178143027MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Light4202674828MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Light4202674828.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1988387904.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1988387904MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3135785000.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3135785000MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Flare4197217604.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4270899469.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se4270899469MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3146551703.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3146551703MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2812053963.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2812053963MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1507991894.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1507991894MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LightType1292142182.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3356853774.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3356853774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3798673028.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3798673028MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1870547511.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1870547511MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3420784866.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3420784866MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3423468507.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3423468507MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetM65022362.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_SetM65022362MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1046826369.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1046826369MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3798702619.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3798702619MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3678867096.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3678867096MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set596534010.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set596534010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2319285817.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2319285817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1677897425.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1677897425MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3246619020.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3246619020MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3249302661.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3249302661MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set578339557.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set578339557MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2485166170.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2485166170MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2145798833.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2145798833MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2990573965.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2990573965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1933064659.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1933064659MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1531180174.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1531180174MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1151369904.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1151369904MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3140252369.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3140252369MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set691862362.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set691862362MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3445720326.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3445720326MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3330067392.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3330067392MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set932473671.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set932473671MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "PlayMaker_HutongGames_PlayMaker_ReflectionUtils1202855664MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set467579892.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Set467579892MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1425090431.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1425090431MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2514376337.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se2514376337MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3385117689.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3385117689MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3705741805.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se3705741805MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1014047458.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Se1014047458MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sl1652571045.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sl1652571045MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sm1760465533.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sm1760465533MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sm4236557480.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sm4236557480MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sm4235776941.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sm4235776941MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1447367550.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1447367550MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall3279845016.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall3279845016MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sta407515898.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sta407515898MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService3853025142MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService3853025142.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus1153071304.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1997133011.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1997133011MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Network1492793700MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1998114672.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1998114672MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com700434209MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation1724966010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St2528039284.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St2528039284MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St3723251505.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St3723251505MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St3934792034.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St3934792034MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1102823480.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St1102823480MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St4078804785.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_St4078804785MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2925956997(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Joint>()
#define GameObject_GetComponent_TisJoint_t4201008640_m884734414(__this, method) ((  Joint_t4201008640 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405(__this, method) ((  Rigidbody_t3346577219 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2635026914_MethodInfo_var;
extern const uint32_t SetAudioPitch__ctor_m2065119644_MetadataUsageId;
extern "C"  void SetAudioPitch__ctor_m2065119644 (SetAudioPitch_t607907034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetAudioPitch__ctor_m2065119644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2635026914(__this, /*hidden argument*/ComponentAction_1__ctor_m2635026914_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::Reset()
extern "C"  void SetAudioPitch_Reset_m4006519881 (SetAudioPitch_t607907034 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_pitch_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::OnEnter()
extern "C"  void SetAudioPitch_OnEnter_m3991783027 (SetAudioPitch_t607907034 * __this, const MethodInfo* method)
{
	{
		SetAudioPitch_DoSetAudioPitch_m3847671675(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::OnUpdate()
extern "C"  void SetAudioPitch_OnUpdate_m2619748912 (SetAudioPitch_t607907034 * __this, const MethodInfo* method)
{
	{
		SetAudioPitch_DoSetAudioPitch_m3847671675(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::DoSetAudioPitch()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2158617149_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_audio_m3438095188_MethodInfo_var;
extern const uint32_t SetAudioPitch_DoSetAudioPitch_m3847671675_MetadataUsageId;
extern "C"  void SetAudioPitch_DoSetAudioPitch_m3847671675 (SetAudioPitch_t607907034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetAudioPitch_DoSetAudioPitch_m3847671675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2158617149(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2158617149_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		FsmFloat_t2134102846 * L_5 = __this->get_pitch_12();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0044;
		}
	}
	{
		AudioSource_t1740077639 * L_7 = ComponentAction_1_get_audio_m3438095188(__this, /*hidden argument*/ComponentAction_1_get_audio_m3438095188_MethodInfo_var);
		FsmFloat_t2134102846 * L_8 = __this->get_pitch_12();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		AudioSource_set_pitch_m1518407234(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2635026914_MethodInfo_var;
extern const uint32_t SetAudioVolume__ctor_m595705200_MetadataUsageId;
extern "C"  void SetAudioVolume__ctor_m595705200 (SetAudioVolume_t1193383862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetAudioVolume__ctor_m595705200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2635026914(__this, /*hidden argument*/ComponentAction_1__ctor_m2635026914_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::Reset()
extern "C"  void SetAudioVolume_Reset_m2537105437 (SetAudioVolume_t1193383862 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_volume_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::OnEnter()
extern "C"  void SetAudioVolume_OnEnter_m633775431 (SetAudioVolume_t1193383862 * __this, const MethodInfo* method)
{
	{
		SetAudioVolume_DoSetAudioVolume_m2107387213(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::OnUpdate()
extern "C"  void SetAudioVolume_OnUpdate_m1600728540 (SetAudioVolume_t1193383862 * __this, const MethodInfo* method)
{
	{
		SetAudioVolume_DoSetAudioVolume_m2107387213(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::DoSetAudioVolume()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2158617149_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_audio_m3438095188_MethodInfo_var;
extern const uint32_t SetAudioVolume_DoSetAudioVolume_m2107387213_MetadataUsageId;
extern "C"  void SetAudioVolume_DoSetAudioVolume_m2107387213 (SetAudioVolume_t1193383862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetAudioVolume_DoSetAudioVolume_m2107387213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2158617149(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2158617149_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		FsmFloat_t2134102846 * L_5 = __this->get_volume_12();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0044;
		}
	}
	{
		AudioSource_t1740077639 * L_7 = ComponentAction_1_get_audio_m3438095188(__this, /*hidden argument*/ComponentAction_1_get_audio_m3438095188_MethodInfo_var);
		FsmFloat_t2134102846 * L_8 = __this->get_volume_12();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		AudioSource_set_volume_m1410546616(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2615529950_MethodInfo_var;
extern const uint32_t SetBackgroundColor__ctor_m2997154891_MetadataUsageId;
extern "C"  void SetBackgroundColor__ctor_m2997154891 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetBackgroundColor__ctor_m2997154891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2615529950(__this, /*hidden argument*/ComponentAction_1__ctor_m2615529950_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::Reset()
extern "C"  void SetBackgroundColor_Reset_m643587832 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		Color_t4194546905  L_0 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_backgroundColor_12(L_1);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::OnEnter()
extern "C"  void SetBackgroundColor_OnEnter_m2029490530 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method)
{
	{
		SetBackgroundColor_DoSetBackgroundColor_m1204554263(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::OnUpdate()
extern "C"  void SetBackgroundColor_OnUpdate_m1918223649 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method)
{
	{
		SetBackgroundColor_DoSetBackgroundColor_m1204554263(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::DoSetBackgroundColor()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2525451553_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_camera_m4240159667_MethodInfo_var;
extern const uint32_t SetBackgroundColor_DoSetBackgroundColor_m1204554263_MetadataUsageId;
extern "C"  void SetBackgroundColor_DoSetBackgroundColor_m1204554263 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetBackgroundColor_DoSetBackgroundColor_m1204554263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2525451553(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2525451553_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Camera_t2727095145 * L_5 = ComponentAction_1_get_camera_m4240159667(__this, /*hidden argument*/ComponentAction_1_get_camera_m4240159667_MethodInfo_var);
		FsmColor_t2131419205 * L_6 = __this->get_backgroundColor_12();
		NullCheck(L_6);
		Color_t4194546905  L_7 = FsmColor_get_Value_m1679829997(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_backgroundColor_m501006344(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::.ctor()
extern "C"  void SetBoolValue__ctor_m595488889 (SetBoolValue_t2866875533 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::Reset()
extern "C"  void SetBoolValue_Reset_m2536889126 (SetBoolValue_t2866875533 * __this, const MethodInfo* method)
{
	{
		__this->set_boolVariable_9((FsmBool_t1075959796 *)NULL);
		__this->set_boolValue_10((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::OnEnter()
extern "C"  void SetBoolValue_OnEnter_m425900560 (SetBoolValue_t2866875533 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_boolVariable_9();
		FsmBool_t1075959796 * L_1 = __this->get_boolValue_10();
		NullCheck(L_1);
		bool L_2 = FsmBool_get_Value_m3101329097(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::OnUpdate()
extern "C"  void SetBoolValue_OnUpdate_m3746542131 (SetBoolValue_t2866875533 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_boolVariable_9();
		FsmBool_t1075959796 * L_1 = __this->get_boolValue_10();
		NullCheck(L_1);
		bool L_2 = FsmBool_get_Value_m3101329097(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2615529950_MethodInfo_var;
extern const uint32_t SetCameraCullingMask__ctor_m236703305_MetadataUsageId;
extern "C"  void SetCameraCullingMask__ctor_m236703305 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetCameraCullingMask__ctor_m236703305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2615529950(__this, /*hidden argument*/ComponentAction_1__ctor_m2615529950_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::Reset()
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t SetCameraCullingMask_Reset_m2178103542_MetadataUsageId;
extern "C"  void SetCameraCullingMask_Reset_m2178103542 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetCameraCullingMask_Reset_m2178103542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_cullingMask_12(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_13(L_0);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::OnEnter()
extern "C"  void SetCameraCullingMask_OnEnter_m3525305312 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method)
{
	{
		SetCameraCullingMask_DoSetCameraCullingMask_m3459216411(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::OnUpdate()
extern "C"  void SetCameraCullingMask_OnUpdate_m1043841635 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method)
{
	{
		SetCameraCullingMask_DoSetCameraCullingMask_m3459216411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::DoSetCameraCullingMask()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2525451553_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_camera_m4240159667_MethodInfo_var;
extern const uint32_t SetCameraCullingMask_DoSetCameraCullingMask_m3459216411_MetadataUsageId;
extern "C"  void SetCameraCullingMask_DoSetCameraCullingMask_m3459216411 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetCameraCullingMask_DoSetCameraCullingMask_m3459216411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2525451553(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2525451553_MethodInfo_var);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		Camera_t2727095145 * L_5 = ComponentAction_1_get_camera_m4240159667(__this, /*hidden argument*/ComponentAction_1_get_camera_m4240159667_MethodInfo_var);
		FsmIntU5BU5D_t1976821196* L_6 = __this->get_cullingMask_12();
		FsmBool_t1075959796 * L_7 = __this->get_invertMask_13();
		NullCheck(L_7);
		bool L_8 = FsmBool_get_Value_m3101329097(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_cullingMask_m2181279574(L_5, L_9, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2615529950_MethodInfo_var;
extern const uint32_t SetCameraFOV__ctor_m1816519384_MetadataUsageId;
extern "C"  void SetCameraFOV__ctor_m1816519384 (SetCameraFOV_t480289614 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetCameraFOV__ctor_m1816519384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2615529950(__this, /*hidden argument*/ComponentAction_1__ctor_m2615529950_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::Reset()
extern "C"  void SetCameraFOV_Reset_m3757919621 (SetCameraFOV_t480289614 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (50.0f), /*hidden argument*/NULL);
		__this->set_fieldOfView_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::OnEnter()
extern "C"  void SetCameraFOV_OnEnter_m1310134447 (SetCameraFOV_t480289614 * __this, const MethodInfo* method)
{
	{
		SetCameraFOV_DoSetCameraFOV_m937791165(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::OnUpdate()
extern "C"  void SetCameraFOV_OnUpdate_m1093021556 (SetCameraFOV_t480289614 * __this, const MethodInfo* method)
{
	{
		SetCameraFOV_DoSetCameraFOV_m937791165(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::DoSetCameraFOV()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2525451553_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_camera_m4240159667_MethodInfo_var;
extern const uint32_t SetCameraFOV_DoSetCameraFOV_m937791165_MetadataUsageId;
extern "C"  void SetCameraFOV_DoSetCameraFOV_m937791165 (SetCameraFOV_t480289614 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetCameraFOV_DoSetCameraFOV_m937791165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2525451553(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2525451553_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Camera_t2727095145 * L_5 = ComponentAction_1_get_camera_m4240159667(__this, /*hidden argument*/ComponentAction_1_get_camera_m4240159667_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_fieldOfView_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_fieldOfView_m809388684(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::.ctor()
extern "C"  void SetColorRGBA__ctor_m3941677065 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::Reset()
extern "C"  void SetColorRGBA_Reset_m1588110006 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method)
{
	{
		__this->set_colorVariable_9((FsmColor_t2131419205 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_red_10(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_green_11(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_blue_12(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_alpha_13(L_3);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::OnEnter()
extern "C"  void SetColorRGBA_OnEnter_m3477200288 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method)
{
	{
		SetColorRGBA_DoSetColorRGBA_m3946000539(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::OnUpdate()
extern "C"  void SetColorRGBA_OnUpdate_m3847553187 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method)
{
	{
		SetColorRGBA_DoSetColorRGBA_m3946000539(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::DoSetColorRGBA()
extern "C"  void SetColorRGBA_DoSetColorRGBA_m3946000539 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmColor_t2131419205 * L_0 = __this->get_colorVariable_9();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmColor_t2131419205 * L_1 = __this->get_colorVariable_9();
		NullCheck(L_1);
		Color_t4194546905  L_2 = FsmColor_get_Value_m1679829997(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmFloat_t2134102846 * L_3 = __this->get_red_10();
		NullCheck(L_3);
		bool L_4 = NamedVariable_get_IsNone_m281035543(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		FsmFloat_t2134102846 * L_5 = __this->get_red_10();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		(&V_0)->set_r_0(L_6);
	}

IL_003a:
	{
		FsmFloat_t2134102846 * L_7 = __this->get_green_11();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005c;
		}
	}
	{
		FsmFloat_t2134102846 * L_9 = __this->get_green_11();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		(&V_0)->set_g_1(L_10);
	}

IL_005c:
	{
		FsmFloat_t2134102846 * L_11 = __this->get_blue_12();
		NullCheck(L_11);
		bool L_12 = NamedVariable_get_IsNone_m281035543(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_007e;
		}
	}
	{
		FsmFloat_t2134102846 * L_13 = __this->get_blue_12();
		NullCheck(L_13);
		float L_14 = FsmFloat_get_Value_m4137923823(L_13, /*hidden argument*/NULL);
		(&V_0)->set_b_2(L_14);
	}

IL_007e:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_alpha_13();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a0;
		}
	}
	{
		FsmFloat_t2134102846 * L_17 = __this->get_alpha_13();
		NullCheck(L_17);
		float L_18 = FsmFloat_get_Value_m4137923823(L_17, /*hidden argument*/NULL);
		(&V_0)->set_a_3(L_18);
	}

IL_00a0:
	{
		FsmColor_t2131419205 * L_19 = __this->get_colorVariable_9();
		Color_t4194546905  L_20 = V_0;
		NullCheck(L_19);
		FsmColor_set_Value_m1684002054(L_19, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::.ctor()
extern "C"  void SetColorValue__ctor_m2350033752 (SetColorValue_t353523614 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::Reset()
extern "C"  void SetColorValue_Reset_m4291433989 (SetColorValue_t353523614 * __this, const MethodInfo* method)
{
	{
		__this->set_colorVariable_9((FsmColor_t2131419205 *)NULL);
		__this->set_color_10((FsmColor_t2131419205 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::OnEnter()
extern "C"  void SetColorValue_OnEnter_m2916333871 (SetColorValue_t353523614 * __this, const MethodInfo* method)
{
	{
		SetColorValue_DoSetColorValue_m2555580411(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::OnUpdate()
extern "C"  void SetColorValue_OnUpdate_m3640563444 (SetColorValue_t353523614 * __this, const MethodInfo* method)
{
	{
		SetColorValue_DoSetColorValue_m2555580411(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::DoSetColorValue()
extern "C"  void SetColorValue_DoSetColorValue_m2555580411 (SetColorValue_t353523614 * __this, const MethodInfo* method)
{
	{
		FsmColor_t2131419205 * L_0 = __this->get_colorVariable_9();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		FsmColor_t2131419205 * L_1 = __this->get_colorVariable_9();
		FsmColor_t2131419205 * L_2 = __this->get_color_10();
		NullCheck(L_2);
		Color_t4194546905  L_3 = FsmColor_get_Value_m1679829997(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmColor_set_Value_m1684002054(L_1, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetDrag::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t SetDrag__ctor_m4051332850_MetadataUsageId;
extern "C"  void SetDrag__ctor_m4051332850 (SetDrag_t3798420676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetDrag__ctor_m4051332850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetDrag::Reset()
extern "C"  void SetDrag_Reset_m1697765791 (SetDrag_t3798420676 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_drag_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetDrag::OnEnter()
extern "C"  void SetDrag_OnEnter_m1482227273 (SetDrag_t3798420676 * __this, const MethodInfo* method)
{
	{
		SetDrag_DoSetDrag_m1898363771(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetDrag::OnUpdate()
extern "C"  void SetDrag_OnUpdate_m2132931866 (SetDrag_t3798420676 * __this, const MethodInfo* method)
{
	{
		SetDrag_DoSetDrag_m1898363771(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetDrag::DoSetDrag()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t SetDrag_DoSetDrag_m1898363771_MetadataUsageId;
extern "C"  void SetDrag_DoSetDrag_m1898363771 (SetDrag_t3798420676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetDrag_DoSetDrag_m1898363771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_drag_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_drag_m4061586082(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetEventData::.ctor()
extern "C"  void SetEventData__ctor_m1112974428 (SetEventData_t853237066 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetEventData::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmRect_t1076426478_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmQuaternion_t3871136040_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmColor_t2131419205_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmMaterial_t924399665_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmTexture_t3073272573_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmObject_t821476169_il2cpp_TypeInfo_var;
extern const uint32_t SetEventData_Reset_m3054374665_MetadataUsageId;
extern "C"  void SetEventData_Reset_m3054374665 (SetEventData_t853237066 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetEventData_Reset_m3054374665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	FsmString_t952858651 * V_3 = NULL;
	FsmBool_t1075959796 * V_4 = NULL;
	FsmVector2_t533912881 * V_5 = NULL;
	FsmVector3_t533912882 * V_6 = NULL;
	FsmRect_t1076426478 * V_7 = NULL;
	FsmQuaternion_t3871136040 * V_8 = NULL;
	FsmColor_t2131419205 * V_9 = NULL;
	FsmMaterial_t924399665 * V_10 = NULL;
	FsmTexture_t3073272573 * V_11 = NULL;
	FsmObject_t821476169 * V_12 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_setGameObjectData_9(L_2);
		FsmInt_t1596138449 * L_3 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmInt_t1596138449 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_5 = V_1;
		__this->set_setIntData_10(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmFloat_t2134102846 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_2;
		__this->set_setFloatData_11(L_8);
		FsmString_t952858651 * L_9 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_9, /*hidden argument*/NULL);
		V_3 = L_9;
		FsmString_t952858651 * L_10 = V_3;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_11 = V_3;
		__this->set_setStringData_12(L_11);
		FsmBool_t1075959796 * L_12 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_12, /*hidden argument*/NULL);
		V_4 = L_12;
		FsmBool_t1075959796 * L_13 = V_4;
		NullCheck(L_13);
		NamedVariable_set_UseVariable_m4266138971(L_13, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_14 = V_4;
		__this->set_setBoolData_13(L_14);
		FsmVector2_t533912881 * L_15 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_15, /*hidden argument*/NULL);
		V_5 = L_15;
		FsmVector2_t533912881 * L_16 = V_5;
		NullCheck(L_16);
		NamedVariable_set_UseVariable_m4266138971(L_16, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_17 = V_5;
		__this->set_setVector2Data_14(L_17);
		FsmVector3_t533912882 * L_18 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_18, /*hidden argument*/NULL);
		V_6 = L_18;
		FsmVector3_t533912882 * L_19 = V_6;
		NullCheck(L_19);
		NamedVariable_set_UseVariable_m4266138971(L_19, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_20 = V_6;
		__this->set_setVector3Data_15(L_20);
		FsmRect_t1076426478 * L_21 = (FsmRect_t1076426478 *)il2cpp_codegen_object_new(FsmRect_t1076426478_il2cpp_TypeInfo_var);
		FsmRect__ctor_m2674586289(L_21, /*hidden argument*/NULL);
		V_7 = L_21;
		FsmRect_t1076426478 * L_22 = V_7;
		NullCheck(L_22);
		NamedVariable_set_UseVariable_m4266138971(L_22, (bool)1, /*hidden argument*/NULL);
		FsmRect_t1076426478 * L_23 = V_7;
		__this->set_setRectData_16(L_23);
		FsmQuaternion_t3871136040 * L_24 = (FsmQuaternion_t3871136040 *)il2cpp_codegen_object_new(FsmQuaternion_t3871136040_il2cpp_TypeInfo_var);
		FsmQuaternion__ctor_m44600631(L_24, /*hidden argument*/NULL);
		V_8 = L_24;
		FsmQuaternion_t3871136040 * L_25 = V_8;
		NullCheck(L_25);
		NamedVariable_set_UseVariable_m4266138971(L_25, (bool)1, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_26 = V_8;
		__this->set_setQuaternionData_17(L_26);
		FsmColor_t2131419205 * L_27 = (FsmColor_t2131419205 *)il2cpp_codegen_object_new(FsmColor_t2131419205_il2cpp_TypeInfo_var);
		FsmColor__ctor_m4262627118(L_27, /*hidden argument*/NULL);
		V_9 = L_27;
		FsmColor_t2131419205 * L_28 = V_9;
		NullCheck(L_28);
		NamedVariable_set_UseVariable_m4266138971(L_28, (bool)1, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_29 = V_9;
		__this->set_setColorData_18(L_29);
		FsmMaterial_t924399665 * L_30 = (FsmMaterial_t924399665 *)il2cpp_codegen_object_new(FsmMaterial_t924399665_il2cpp_TypeInfo_var);
		FsmMaterial__ctor_m4051814862(L_30, /*hidden argument*/NULL);
		V_10 = L_30;
		FsmMaterial_t924399665 * L_31 = V_10;
		NullCheck(L_31);
		NamedVariable_set_UseVariable_m4266138971(L_31, (bool)1, /*hidden argument*/NULL);
		FsmMaterial_t924399665 * L_32 = V_10;
		__this->set_setMaterialData_19(L_32);
		FsmTexture_t3073272573 * L_33 = (FsmTexture_t3073272573 *)il2cpp_codegen_object_new(FsmTexture_t3073272573_il2cpp_TypeInfo_var);
		FsmTexture__ctor_m4043693302(L_33, /*hidden argument*/NULL);
		V_11 = L_33;
		FsmTexture_t3073272573 * L_34 = V_11;
		NullCheck(L_34);
		NamedVariable_set_UseVariable_m4266138971(L_34, (bool)1, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_35 = V_11;
		__this->set_setTextureData_20(L_35);
		FsmObject_t821476169 * L_36 = (FsmObject_t821476169 *)il2cpp_codegen_object_new(FsmObject_t821476169_il2cpp_TypeInfo_var);
		FsmObject__ctor_m3316802358(L_36, /*hidden argument*/NULL);
		V_12 = L_36;
		FsmObject_t821476169 * L_37 = V_12;
		NullCheck(L_37);
		NamedVariable_set_UseVariable_m4266138971(L_37, (bool)1, /*hidden argument*/NULL);
		FsmObject_t821476169 * L_38 = V_12;
		__this->set_setObjectData_21(L_38);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetEventData::OnEnter()
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t SetEventData_OnEnter_m3808264499_MetadataUsageId;
extern "C"  void SetEventData_OnEnter_m3808264499 (SetEventData_t853237066 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetEventData_OnEnter_m3808264499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmBool_t1075959796 * L_1 = __this->get_setBoolData_13();
		NullCheck(L_1);
		bool L_2 = FsmBool_get_Value_m3101329097(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_BoolData_3(L_2);
		FsmEventData_t1076900934 * L_3 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmInt_t1596138449 * L_4 = __this->get_setIntData_10();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_IntData_4(L_5);
		FsmEventData_t1076900934 * L_6 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmFloat_t2134102846 * L_7 = __this->get_setFloatData_11();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_FloatData_5(L_8);
		FsmEventData_t1076900934 * L_9 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmVector2_t533912881 * L_10 = __this->get_setVector2Data_14();
		NullCheck(L_10);
		Vector2_t4282066565  L_11 = FsmVector2_get_Value_m1313754285(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_Vector2Data_6(L_11);
		FsmEventData_t1076900934 * L_12 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmVector3_t533912882 * L_13 = __this->get_setVector3Data_15();
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = FsmVector3_get_Value_m2779135117(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_Vector3Data_7(L_14);
		FsmEventData_t1076900934 * L_15 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmString_t952858651 * L_16 = __this->get_setStringData_12();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_StringData_8(L_17);
		FsmEventData_t1076900934 * L_18 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmGameObject_t1697147867 * L_19 = __this->get_setGameObjectData_9();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		L_18->set_GameObjectData_13(L_20);
		FsmEventData_t1076900934 * L_21 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmRect_t1076426478 * L_22 = __this->get_setRectData_16();
		NullCheck(L_22);
		Rect_t4241904616  L_23 = FsmRect_get_Value_m1002500317(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_RectData_10(L_23);
		FsmEventData_t1076900934 * L_24 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmQuaternion_t3871136040 * L_25 = __this->get_setQuaternionData_17();
		NullCheck(L_25);
		Quaternion_t1553702882  L_26 = FsmQuaternion_get_Value_m3393858025(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_QuaternionData_9(L_26);
		FsmEventData_t1076900934 * L_27 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmColor_t2131419205 * L_28 = __this->get_setColorData_18();
		NullCheck(L_28);
		Color_t4194546905  L_29 = FsmColor_get_Value_m1679829997(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		L_27->set_ColorData_11(L_29);
		FsmEventData_t1076900934 * L_30 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmMaterial_t924399665 * L_31 = __this->get_setMaterialData_19();
		NullCheck(L_31);
		Material_t3870600107 * L_32 = FsmMaterial_get_Value_m1376213207(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		L_30->set_MaterialData_14(L_32);
		FsmEventData_t1076900934 * L_33 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmTexture_t3073272573 * L_34 = __this->get_setTextureData_20();
		NullCheck(L_34);
		Texture_t2526458961 * L_35 = FsmTexture_get_Value_m3156202285(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_TextureData_15(L_35);
		FsmEventData_t1076900934 * L_36 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_2();
		FsmObject_t821476169 * L_37 = __this->get_setObjectData_21();
		NullCheck(L_37);
		Object_t3071478659 * L_38 = FsmObject_get_Value_m188501991(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		L_36->set_ObjectData_12(L_38);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetEventTarget::.ctor()
extern "C"  void SetEventTarget__ctor_m370889109 (SetEventTarget_t788190705 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetEventTarget::Reset()
extern "C"  void SetEventTarget_Reset_m2312289346 (SetEventTarget_t788190705 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetEventTarget::OnEnter()
extern "C"  void SetEventTarget_OnEnter_m3628844076 (SetEventTarget_t788190705 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventTarget_t1823904941 * L_1 = __this->get_eventTarget_9();
		NullCheck(L_0);
		Fsm_set_EventTarget_m3950818474(L_0, L_1, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::.ctor()
extern "C"  void SetFlareStrength__ctor_m648689009 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::Reset()
extern "C"  void SetFlareStrength_Reset_m2590089246 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.2f), /*hidden argument*/NULL);
		__this->set_flareStrength_9(L_0);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::OnEnter()
extern "C"  void SetFlareStrength_OnEnter_m11608328 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method)
{
	{
		SetFlareStrength_DoSetFlareStrength_m1645466571(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_10();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::OnUpdate()
extern "C"  void SetFlareStrength_OnUpdate_m3788384827 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method)
{
	{
		SetFlareStrength_DoSetFlareStrength_m1645466571(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::DoSetFlareStrength()
extern "C"  void SetFlareStrength_DoSetFlareStrength_m1645466571 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_flareStrength_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		RenderSettings_set_flareStrength_m38596007(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::.ctor()
extern "C"  void SetFloatValue__ctor_m4145444497 (SetFloatValue_t2341951557 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::Reset()
extern "C"  void SetFloatValue_Reset_m1791877438 (SetFloatValue_t2341951557 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_floatValue_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::OnEnter()
extern "C"  void SetFloatValue_OnEnter_m1729206824 (SetFloatValue_t2341951557 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = __this->get_floatValue_10();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::OnUpdate()
extern "C"  void SetFloatValue_OnUpdate_m1199363355 (SetFloatValue_t2341951557 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = __this->get_floatValue_10();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::.ctor()
extern "C"  void SetFogColor__ctor_m1466260609 (SetFogColor_t1692261973 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::Reset()
extern "C"  void SetFogColor_Reset_m3407660846 (SetFogColor_t1692261973 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fogColor_9(L_1);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::OnEnter()
extern "C"  void SetFogColor_OnEnter_m4013868056 (SetFogColor_t1692261973 * __this, const MethodInfo* method)
{
	{
		SetFogColor_DoSetFogColor_m1891050203(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_10();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::OnUpdate()
extern "C"  void SetFogColor_OnUpdate_m3304384811 (SetFogColor_t1692261973 * __this, const MethodInfo* method)
{
	{
		SetFogColor_DoSetFogColor_m1891050203(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::DoSetFogColor()
extern "C"  void SetFogColor_DoSetFogColor_m1891050203 (SetFogColor_t1692261973 * __this, const MethodInfo* method)
{
	{
		FsmColor_t2131419205 * L_0 = __this->get_fogColor_9();
		NullCheck(L_0);
		Color_t4194546905  L_1 = FsmColor_get_Value_m1679829997(L_0, /*hidden argument*/NULL);
		RenderSettings_set_fogColor_m1507677940(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::.ctor()
extern "C"  void SetFogDensity__ctor_m1239441500 (SetFogDensity_t4077399578 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::Reset()
extern "C"  void SetFogDensity_Reset_m3180841737 (SetFogDensity_t4077399578 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.5f), /*hidden argument*/NULL);
		__this->set_fogDensity_9(L_0);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::OnEnter()
extern "C"  void SetFogDensity_OnEnter_m789069107 (SetFogDensity_t4077399578 * __this, const MethodInfo* method)
{
	{
		SetFogDensity_DoSetFogDensity_m2007988091(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_10();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::OnUpdate()
extern "C"  void SetFogDensity_OnUpdate_m2119865200 (SetFogDensity_t4077399578 * __this, const MethodInfo* method)
{
	{
		SetFogDensity_DoSetFogDensity_m2007988091(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::DoSetFogDensity()
extern "C"  void SetFogDensity_DoSetFogDensity_m2007988091 (SetFogDensity_t4077399578 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_fogDensity_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		RenderSettings_set_fogDensity_m998518996(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::.ctor()
extern "C"  void SetFsmBool__ctor_m442232694 (SetFsmBool_t888287984 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmBool_Reset_m2383632931_MetadataUsageId;
extern "C"  void SetFsmBool_Reset_m2383632931 (SetFsmBool_t888287984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmBool_Reset_m2383632931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::OnEnter()
extern "C"  void SetFsmBool_OnEnter_m3470552525 (SetFsmBool_t888287984 * __this, const MethodInfo* method)
{
	{
		SetFsmBool_DoSetFsmBool_m3371722177(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::DoSetFsmBool()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmBool_DoSetFsmBool_m3371722177_MetadataUsageId;
extern "C"  void SetFsmBool_DoSetFsmBool_m3371722177 (SetFsmBool_t888287984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmBool_DoSetFsmBool_m3371722177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmBool_t1075959796 * V_1 = NULL;
	{
		FsmBool_t1075959796 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmBool_t1075959796 * L_23 = FsmVariables_FindFsmBool_m4215612206(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmBool_t1075959796 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmBool_t1075959796 * L_25 = V_1;
		FsmBool_t1075959796 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		bool L_27 = FsmBool_get_Value_m3101329097(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmBool_set_Value_m1126216340(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::OnUpdate()
extern "C"  void SetFsmBool_OnUpdate_m3641472534 (SetFsmBool_t888287984 * __this, const MethodInfo* method)
{
	{
		SetFsmBool_DoSetFsmBool_m3371722177(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::.ctor()
extern "C"  void SetFsmColor__ctor_m4174467459 (SetFsmColor_t1119084307 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmColor_Reset_m1820900400_MetadataUsageId;
extern "C"  void SetFsmColor_Reset_m1820900400 (SetFsmColor_t1119084307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmColor_Reset_m1820900400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmColor_t2131419205 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::OnEnter()
extern "C"  void SetFsmColor_OnEnter_m3850469530 (SetFsmColor_t1119084307 * __this, const MethodInfo* method)
{
	{
		SetFsmColor_DoSetFsmColor_m3728719643(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::DoSetFsmColor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmColor_DoSetFsmColor_m3728719643_MetadataUsageId;
extern "C"  void SetFsmColor_DoSetFsmColor_m3728719643 (SetFsmColor_t1119084307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmColor_DoSetFsmColor_m3728719643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmColor_t2131419205 * V_1 = NULL;
	{
		FsmColor_t2131419205 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmColor_t2131419205 * L_23 = FsmVariables_GetFsmColor_m414491395(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmColor_t2131419205 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmColor_t2131419205 * L_25 = V_1;
		FsmColor_t2131419205 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Color_t4194546905  L_27 = FsmColor_get_Value_m1679829997(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmColor_set_Value_m1684002054(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::OnUpdate()
extern "C"  void SetFsmColor_OnUpdate_m2533997801 (SetFsmColor_t1119084307 * __this, const MethodInfo* method)
{
	{
		SetFsmColor_DoSetFsmColor_m3728719643(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::.ctor()
extern "C"  void SetFsmFloat__ctor_m2919737002 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmFloat_Reset_m566169943_MetadataUsageId;
extern "C"  void SetFsmFloat_Reset_m566169943 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmFloat_Reset_m566169943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::OnEnter()
extern "C"  void SetFsmFloat_OnEnter_m645343233 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method)
{
	{
		SetFsmFloat_DoSetFsmFloat_m3866498555(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::DoSetFsmFloat()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmFloat_DoSetFsmFloat_m3866498555_MetadataUsageId;
extern "C"  void SetFsmFloat_DoSetFsmFloat_m3866498555 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmFloat_DoSetFsmFloat_m3866498555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmFloat_t2134102846 * V_1 = NULL;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmFloat_t2134102846 * L_23 = FsmVariables_GetFsmFloat_m1258803409(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmFloat_t2134102846 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmFloat_t2134102846 * L_25 = V_1;
		FsmFloat_t2134102846 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		float L_27 = FsmFloat_get_Value_m4137923823(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmFloat_set_Value_m1568963140(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::OnUpdate()
extern "C"  void SetFsmFloat_OnUpdate_m1959330402 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method)
{
	{
		SetFsmFloat_DoSetFsmFloat_m3866498555(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::.ctor()
extern "C"  void SetFsmGameObject__ctor_m2390144687 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmGameObject_Reset_m36577628_MetadataUsageId;
extern "C"  void SetFsmGameObject_Reset_m36577628 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmGameObject_Reset_m36577628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::OnEnter()
extern "C"  void SetFsmGameObject_OnEnter_m2808236742 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method)
{
	{
		SetFsmGameObject_DoSetFsmGameObject_m2128809935(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::DoSetFsmGameObject()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmGameObject_DoSetFsmGameObject_m2128809935_MetadataUsageId;
extern "C"  void SetFsmGameObject_DoSetFsmGameObject_m2128809935 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmGameObject_DoSetFsmGameObject_m2128809935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmGameObject_t1697147867 * G_B9_0 = NULL;
	FsmGameObject_t1697147867 * G_B8_0 = NULL;
	GameObject_t3674682005 * G_B10_0 = NULL;
	FsmGameObject_t1697147867 * G_B10_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_goLastFrame_14();
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		__this->set_goLastFrame_14(L_8);
		GameObject_t3674682005 * L_9 = V_0;
		FsmString_t952858651 * L_10 = __this->get_fsmName_10();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_12 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_fsm_15(L_12);
	}

IL_004e:
	{
		PlayMakerFSM_t3799847376 * L_13 = __this->get_fsm_15();
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		PlayMakerFSM_t3799847376 * L_15 = __this->get_fsm_15();
		NullCheck(L_15);
		FsmVariables_t963491929 * L_16 = PlayMakerFSM_get_FsmVariables_m1986570741(L_15, /*hidden argument*/NULL);
		FsmString_t952858651 * L_17 = __this->get_variableName_11();
		NullCheck(L_17);
		String_t* L_18 = FsmString_get_Value_m872383149(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmGameObject_t1697147867 * L_19 = FsmVariables_FindFsmGameObject_m2163832288(L_16, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		FsmGameObject_t1697147867 * L_20 = V_1;
		if (!L_20)
		{
			goto IL_00a9;
		}
	}
	{
		FsmGameObject_t1697147867 * L_21 = V_1;
		FsmGameObject_t1697147867 * L_22 = __this->get_setValue_12();
		G_B8_0 = L_21;
		if (L_22)
		{
			G_B9_0 = L_21;
			goto IL_0094;
		}
	}
	{
		G_B10_0 = ((GameObject_t3674682005 *)(NULL));
		G_B10_1 = G_B8_0;
		goto IL_009f;
	}

IL_0094:
	{
		FsmGameObject_t1697147867 * L_23 = __this->get_setValue_12();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = FsmGameObject_get_Value_m673294275(L_23, /*hidden argument*/NULL);
		G_B10_0 = L_24;
		G_B10_1 = G_B9_0;
	}

IL_009f:
	{
		NullCheck(G_B10_1);
		FsmGameObject_set_Value_m297051598(G_B10_1, G_B10_0, /*hidden argument*/NULL);
		goto IL_00c4;
	}

IL_00a9:
	{
		FsmString_t952858651 * L_25 = __this->get_variableName_11();
		NullCheck(L_25);
		String_t* L_26 = FsmString_get_Value_m872383149(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_26, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_27, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::OnUpdate()
extern "C"  void SetFsmGameObject_OnUpdate_m289552445 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method)
{
	{
		SetFsmGameObject_DoSetFsmGameObject_m2128809935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::.ctor()
extern "C"  void SetFsmInt__ctor_m3045993271 (SetFsmInt_t326690015 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmInt_Reset_m692426212_MetadataUsageId;
extern "C"  void SetFsmInt_Reset_m692426212 (SetFsmInt_t326690015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmInt_Reset_m692426212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::OnEnter()
extern "C"  void SetFsmInt_OnEnter_m1718533454 (SetFsmInt_t326690015 * __this, const MethodInfo* method)
{
	{
		SetFsmInt_DoSetFsmInt_m1499949339(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::DoSetFsmInt()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmInt_DoSetFsmInt_m1499949339_MetadataUsageId;
extern "C"  void SetFsmInt_DoSetFsmInt_m1499949339 (SetFsmInt_t326690015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmInt_DoSetFsmInt_m1499949339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		FsmInt_t1596138449 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmInt_t1596138449 * L_23 = FsmVariables_GetFsmInt_m1904423915(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmInt_t1596138449 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmInt_t1596138449 * L_25 = V_1;
		FsmInt_t1596138449 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmInt_set_Value_m2087583461(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::OnUpdate()
extern "C"  void SetFsmInt_OnUpdate_m868488885 (SetFsmInt_t326690015 * __this, const MethodInfo* method)
{
	{
		SetFsmInt_DoSetFsmInt_m1499949339(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::.ctor()
extern "C"  void SetFsmMaterial__ctor_m1912431449 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmMaterial_Reset_m3853831686_MetadataUsageId;
extern "C"  void SetFsmMaterial_Reset_m3853831686 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmMaterial_Reset_m3853831686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_11(L_3);
		__this->set_setValue_12((FsmMaterial_t924399665 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::OnEnter()
extern "C"  void SetFsmMaterial_OnEnter_m3287315696 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method)
{
	{
		SetFsmMaterial_DoSetFsmBool_m235560062(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::DoSetFsmBool()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmMaterial_DoSetFsmBool_m235560062_MetadataUsageId;
extern "C"  void SetFsmMaterial_DoSetFsmBool_m235560062 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmMaterial_DoSetFsmBool_m235560062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmMaterial_t924399665 * V_1 = NULL;
	{
		FsmMaterial_t924399665 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmMaterial_t924399665 * L_23 = FsmVariables_GetFsmMaterial_m2644826863(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmMaterial_t924399665 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmMaterial_t924399665 * L_25 = V_1;
		FsmMaterial_t924399665 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Material_t3870600107 * L_27 = FsmMaterial_get_Value_m1376213207(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmMaterial_set_Value_m3341549218(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::OnUpdate()
extern "C"  void SetFsmMaterial_OnUpdate_m2256098131 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method)
{
	{
		SetFsmMaterial_DoSetFsmBool_m235560062(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::.ctor()
extern "C"  void SetFsmObject__ctor_m583852929 (SetFsmObject_t14389381 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmObject_Reset_m2525253166_MetadataUsageId;
extern "C"  void SetFsmObject_Reset_m2525253166 (SetFsmObject_t14389381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmObject_Reset_m2525253166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_11(L_3);
		__this->set_setValue_12((FsmObject_t821476169 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::OnEnter()
extern "C"  void SetFsmObject_OnEnter_m2128644888 (SetFsmObject_t14389381 * __this, const MethodInfo* method)
{
	{
		SetFsmObject_DoSetFsmBool_m2012864342(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::DoSetFsmBool()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmObject_DoSetFsmBool_m2012864342_MetadataUsageId;
extern "C"  void SetFsmObject_DoSetFsmBool_m2012864342 (SetFsmObject_t14389381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmObject_DoSetFsmBool_m2012864342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmObject_t821476169 * V_1 = NULL;
	{
		FsmObject_t821476169 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmObject_t821476169 * L_23 = FsmVariables_GetFsmObject_m678224879(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmObject_t821476169 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmObject_t821476169 * L_25 = V_1;
		FsmObject_t821476169 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Object_t3071478659 * L_27 = FsmObject_get_Value_m188501991(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmObject_set_Value_m867520242(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::OnUpdate()
extern "C"  void SetFsmObject_OnUpdate_m697041451 (SetFsmObject_t14389381 * __this, const MethodInfo* method)
{
	{
		SetFsmObject_DoSetFsmBool_m2012864342(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::.ctor()
extern "C"  void SetFsmQuaternion__ctor_m1386475522 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmQuaternion_Reset_m3327875759_MetadataUsageId;
extern "C"  void SetFsmQuaternion_Reset_m3327875759 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmQuaternion_Reset_m3327875759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_11(L_3);
		__this->set_setValue_12((FsmQuaternion_t3871136040 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::OnEnter()
extern "C"  void SetFsmQuaternion_OnEnter_m354843481 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method)
{
	{
		SetFsmQuaternion_DoSetFsmQuaternion_m3067159401(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::DoSetFsmQuaternion()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmQuaternion_DoSetFsmQuaternion_m3067159401_MetadataUsageId;
extern "C"  void SetFsmQuaternion_DoSetFsmQuaternion_m3067159401 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmQuaternion_DoSetFsmQuaternion_m3067159401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmQuaternion_t3871136040 * V_1 = NULL;
	{
		FsmQuaternion_t3871136040 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmQuaternion_t3871136040 * L_23 = FsmVariables_GetFsmQuaternion_m3924878991(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmQuaternion_t3871136040 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_25 = V_1;
		FsmQuaternion_t3871136040 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Quaternion_t1553702882  L_27 = FsmQuaternion_get_Value_m3393858025(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmQuaternion_set_Value_m446581172(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::OnUpdate()
extern "C"  void SetFsmQuaternion_OnUpdate_m1543772682 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method)
{
	{
		SetFsmQuaternion_DoSetFsmQuaternion_m3067159401(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::.ctor()
extern "C"  void SetFsmRect__ctor_m1563363772 (SetFsmRect_t888754666 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmRect_Reset_m3504764009_MetadataUsageId;
extern "C"  void SetFsmRect_Reset_m3504764009 (SetFsmRect_t888754666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmRect_Reset_m3504764009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_11(L_3);
		__this->set_setValue_12((FsmRect_t1076426478 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::OnEnter()
extern "C"  void SetFsmRect_OnEnter_m2840727187 (SetFsmRect_t888754666 * __this, const MethodInfo* method)
{
	{
		SetFsmRect_DoSetFsmBool_m2296795323(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::DoSetFsmBool()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmRect_DoSetFsmBool_m2296795323_MetadataUsageId;
extern "C"  void SetFsmRect_DoSetFsmBool_m2296795323 (SetFsmRect_t888754666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmRect_DoSetFsmBool_m2296795323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmRect_t1076426478 * V_1 = NULL;
	{
		FsmRect_t1076426478 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmRect_t1076426478 * L_23 = FsmVariables_GetFsmRect_m1774888079(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmRect_t1076426478 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmRect_t1076426478 * L_25 = V_1;
		FsmRect_t1076426478 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Rect_t4241904616  L_27 = FsmRect_get_Value_m1002500317(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmRect_set_Value_m1159518952(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::OnUpdate()
extern "C"  void SetFsmRect_OnUpdate_m1296756240 (SetFsmRect_t888754666 * __this, const MethodInfo* method)
{
	{
		SetFsmRect_DoSetFsmBool_m2296795323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::.ctor()
extern "C"  void SetFsmString__ctor_m4241126127 (SetFsmString_t145771863 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmString_Reset_m1887559068_MetadataUsageId;
extern "C"  void SetFsmString_Reset_m1887559068 (SetFsmString_t145771863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmString_Reset_m1887559068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::OnEnter()
extern "C"  void SetFsmString_OnEnter_m3484940038 (SetFsmString_t145771863 * __this, const MethodInfo* method)
{
	{
		SetFsmString_DoSetFsmString_m1693021263(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::DoSetFsmString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmString_DoSetFsmString_m1693021263_MetadataUsageId;
extern "C"  void SetFsmString_DoSetFsmString_m1693021263 (SetFsmString_t145771863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmString_DoSetFsmString_m1693021263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmString_t952858651 * V_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmString_t952858651 * L_23 = FsmVariables_GetFsmString_m4055225775(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmString_t952858651 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmString_t952858651 * L_25 = V_1;
		FsmString_t952858651 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		String_t* L_27 = FsmString_get_Value_m872383149(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmString_set_Value_m829393196(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::OnUpdate()
extern "C"  void SetFsmString_OnUpdate_m4087485437 (SetFsmString_t145771863 * __this, const MethodInfo* method)
{
	{
		SetFsmString_DoSetFsmString_m1693021263(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::.ctor()
extern "C"  void SetFsmTexture__ctor_m926639627 (SetFsmTexture_t38942603 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmTexture_Reset_m2868039864_MetadataUsageId;
extern "C"  void SetFsmTexture_Reset_m2868039864 (SetFsmTexture_t38942603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmTexture_Reset_m2868039864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_11(L_3);
		__this->set_setValue_12((FsmTexture_t3073272573 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::OnEnter()
extern "C"  void SetFsmTexture_OnEnter_m834179874 (SetFsmTexture_t38942603 * __this, const MethodInfo* method)
{
	{
		SetFsmTexture_DoSetFsmBool_m2214022540(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::DoSetFsmBool()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmTexture_DoSetFsmBool_m2214022540_MetadataUsageId;
extern "C"  void SetFsmTexture_DoSetFsmBool_m2214022540 (SetFsmTexture_t38942603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmTexture_DoSetFsmBool_m2214022540_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmTexture_t3073272573 * V_1 = NULL;
	{
		FsmTexture_t3073272573 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmTexture_t3073272573 * L_23 = FsmVariables_FindFsmTexture_m3849299818(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmTexture_t3073272573 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmTexture_t3073272573 * L_25 = V_1;
		FsmTexture_t3073272573 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Texture_t2526458961 * L_27 = FsmTexture_get_Value_m3156202285(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmTexture_set_Value_m2261522310(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::OnUpdate()
extern "C"  void SetFsmTexture_OnUpdate_m3518298977 (SetFsmTexture_t38942603 * __this, const MethodInfo* method)
{
	{
		SetFsmTexture_DoSetFsmBool_m2214022540(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::.ctor()
extern "C"  void SetFsmVariable__ctor_m3338216868 (SetFsmVariable_t311321858 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVar_t1596150537_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmVariable_Reset_m984649809_MetadataUsageId;
extern "C"  void SetFsmVariable_Reset_m984649809 (SetFsmVariable_t311321858 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmVariable_Reset_m984649809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		FsmVar_t1596150537 * L_2 = (FsmVar_t1596150537 *)il2cpp_codegen_object_new(FsmVar_t1596150537_il2cpp_TypeInfo_var);
		FsmVar__ctor_m2050768746(L_2, /*hidden argument*/NULL);
		__this->set_setValue_12(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::OnEnter()
extern "C"  void SetFsmVariable_OnEnter_m3372535931 (SetFsmVariable_t311321858 * __this, const MethodInfo* method)
{
	{
		SetFsmVariable_InitFsmVar_m2705460503(__this, /*hidden argument*/NULL);
		SetFsmVariable_DoGetFsmVariable_m2778833009(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::OnUpdate()
extern "C"  void SetFsmVariable_OnUpdate_m602958120 (SetFsmVariable_t311321858 * __this, const MethodInfo* method)
{
	{
		SetFsmVariable_DoGetFsmVariable_m2778833009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::InitFsmVar()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2607078876;
extern const uint32_t SetFsmVariable_InitFsmVar_m2705460503_MetadataUsageId;
extern "C"  void SetFsmVariable_InitFsmVar_m2705460503 (SetFsmVariable_t311321858 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmVariable_InitFsmVar_m2705460503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_cachedGO_14();
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00e1;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		FsmString_t952858651 * L_9 = __this->get_fsmName_10();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_11 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		__this->set_sourceFsm_15(L_11);
		PlayMakerFSM_t3799847376 * L_12 = __this->get_sourceFsm_15();
		NullCheck(L_12);
		FsmVariables_t963491929 * L_13 = PlayMakerFSM_get_FsmVariables_m1986570741(L_12, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_14 = __this->get_setValue_12();
		NullCheck(L_14);
		String_t* L_15 = L_14->get_variableName_1();
		NullCheck(L_13);
		NamedVariable_t3211770239 * L_16 = FsmVariables_GetVariable_m1409842114(L_13, L_15, /*hidden argument*/NULL);
		__this->set_sourceVariable_16(L_16);
		Fsm_t1527112426 * L_17 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmVariables_t963491929 * L_18 = Fsm_get_Variables_m2281949087(L_17, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_19 = __this->get_setValue_12();
		NullCheck(L_19);
		String_t* L_20 = L_19->get_variableName_1();
		NullCheck(L_18);
		NamedVariable_t3211770239 * L_21 = FsmVariables_GetVariable_m1409842114(L_18, L_20, /*hidden argument*/NULL);
		__this->set_targetVariable_17(L_21);
		FsmVar_t1596150537 * L_22 = __this->get_setValue_12();
		NamedVariable_t3211770239 * L_23 = __this->get_targetVariable_17();
		int32_t L_24 = FsmUtility_GetVariableType_m1423105976(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmVar_set_Type_m4211541807(L_22, L_24, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_25 = __this->get_setValue_12();
		NullCheck(L_25);
		String_t* L_26 = L_25->get_variableName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00da;
		}
	}
	{
		Il2CppObject * L_28 = __this->get_sourceVariable_16();
		if (L_28)
		{
			goto IL_00da;
		}
	}
	{
		FsmVar_t1596150537 * L_29 = __this->get_setValue_12();
		NullCheck(L_29);
		String_t* L_30 = L_29->get_variableName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2607078876, L_30, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_31, /*hidden argument*/NULL);
	}

IL_00da:
	{
		GameObject_t3674682005 * L_32 = V_0;
		__this->set_cachedGO_14(L_32);
	}

IL_00e1:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::DoGetFsmVariable()
extern "C"  void SetFsmVariable_DoGetFsmVariable_m2778833009 (SetFsmVariable_t311321858 * __this, const MethodInfo* method)
{
	{
		FsmVar_t1596150537 * L_0 = __this->get_setValue_12();
		NullCheck(L_0);
		bool L_1 = FsmVar_get_IsNone_m3892161437(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		SetFsmVariable_InitFsmVar_m2705460503(__this, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_2 = __this->get_setValue_12();
		Il2CppObject * L_3 = __this->get_sourceVariable_16();
		NullCheck(L_2);
		FsmVar_GetValueFrom_m764615143(L_2, L_3, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_4 = __this->get_setValue_12();
		NamedVariable_t3211770239 * L_5 = __this->get_targetVariable_17();
		NullCheck(L_4);
		FsmVar_ApplyValueTo_m1826228640(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::.ctor()
extern "C"  void SetFsmVector2__ctor_m2590125655 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmVector2_Reset_m236558596_MetadataUsageId;
extern "C"  void SetFsmVector2_Reset_m236558596 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmVector2_Reset_m236558596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmVector2_t533912881 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::OnEnter()
extern "C"  void SetFsmVector2_OnEnter_m1716418670 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method)
{
	{
		SetFsmVector2_DoSetFsmVector2_m814640667(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::DoSetFsmVector2()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmVector2_DoSetFsmVector2_m814640667_MetadataUsageId;
extern "C"  void SetFsmVector2_DoSetFsmVector2_m814640667 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmVector2_DoSetFsmVector2_m814640667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmVector2_t533912881 * V_1 = NULL;
	{
		FsmVector2_t533912881 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmVector2_t533912881 * L_23 = FsmVariables_GetFsmVector2_m3581467435(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmVector2_t533912881 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmVector2_t533912881 * L_25 = V_1;
		FsmVector2_t533912881 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Vector2_t4282066565  L_27 = FsmVector2_get_Value_m1313754285(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmVector2_set_Value_m2900659718(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::OnUpdate()
extern "C"  void SetFsmVector2_OnUpdate_m802930581 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method)
{
	{
		SetFsmVector2_DoSetFsmVector2_m814640667(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::.ctor()
extern "C"  void SetFsmVector3__ctor_m2393612150 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetFsmVector3_Reset_m40045091_MetadataUsageId;
extern "C"  void SetFsmVector3_Reset_m40045091 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmVector3_Reset_m40045091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		__this->set_setValue_12((FsmVector3_t533912882 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::OnEnter()
extern "C"  void SetFsmVector3_OnEnter_m1845501389 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method)
{
	{
		SetFsmVector3_DoSetFsmVector3_m405817403(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::DoSetFsmVector3()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t SetFsmVector3_DoSetFsmVector3_m405817403_MetadataUsageId;
extern "C"  void SetFsmVector3_DoSetFsmVector3_m405817403 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetFsmVector3_DoSetFsmVector3_m405817403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmVector3_t533912882 * L_0 = __this->get_setValue_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_14();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_14(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_10();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_15(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_15();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_11();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmVector3_t533912882 * L_23 = FsmVariables_GetFsmVector3_m557465897(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmVector3_t533912882 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmVector3_t533912882 * L_25 = V_1;
		FsmVector3_t533912882 * L_26 = __this->get_setValue_12();
		NullCheck(L_26);
		Vector3_t4282066566  L_27 = FsmVector3_get_Value_m2779135117(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmVector3_set_Value_m716982822(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_11();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::OnUpdate()
extern "C"  void SetFsmVector3_OnUpdate_m509527574 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method)
{
	{
		SetFsmVector3_DoSetFsmVector3_m405817403(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::.ctor()
extern "C"  void SetGameObject__ctor_m3591878837 (SetGameObject_t3864026529 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::Reset()
extern "C"  void SetGameObject_Reset_m1238311778 (SetGameObject_t3864026529 * __this, const MethodInfo* method)
{
	{
		__this->set_variable_9((FsmGameObject_t1697147867 *)NULL);
		__this->set_gameObject_10((FsmGameObject_t1697147867 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::OnEnter()
extern "C"  void SetGameObject_OnEnter_m2328552268 (SetGameObject_t3864026529 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_variable_9();
		FsmGameObject_t1697147867 * L_1 = __this->get_gameObject_10();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = FsmGameObject_get_Value_m673294275(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::OnUpdate()
extern "C"  void SetGameObject_OnUpdate_m2599202935 (SetGameObject_t3864026529 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_variable_9();
		FsmGameObject_t1697147867 * L_1 = __this->get_gameObject_10();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = FsmGameObject_get_Value_m673294275(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::.ctor()
extern "C"  void SetGameVolume__ctor_m2619120442 (SetGameVolume_t4076511612 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::Reset()
extern "C"  void SetGameVolume_Reset_m265553383 (SetGameVolume_t4076511612 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_volume_9(L_0);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::OnEnter()
extern "C"  void SetGameVolume_OnEnter_m3810605201 (SetGameVolume_t4076511612 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_volume_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_everyFrame_10();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::OnUpdate()
extern "C"  void SetGameVolume_OnUpdate_m1298203602 (SetGameVolume_t4076511612 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_volume_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGravity::.ctor()
extern "C"  void SetGravity__ctor_m2689768946 (SetGravity_t1737623796 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGravity::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetGravity_Reset_m336201887_MetadataUsageId;
extern "C"  void SetGravity_Reset_m336201887 (SetGravity_t1737623796 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGravity_Reset_m336201887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_vector_9((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_10(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_11(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_z_12(L_8);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGravity::OnEnter()
extern "C"  void SetGravity_OnEnter_m2984340809 (SetGravity_t1737623796 * __this, const MethodInfo* method)
{
	{
		SetGravity_DoSetGravity_m701896905(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGravity::OnUpdate()
extern "C"  void SetGravity_OnUpdate_m1453811226 (SetGravity_t1737623796 * __this, const MethodInfo* method)
{
	{
		SetGravity_DoSetGravity_m701896905(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGravity::DoSetGravity()
extern "C"  void SetGravity_DoSetGravity_m701896905 (SetGravity_t1737623796 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector_9();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = FsmVector3_get_Value_m2779135117(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmFloat_t2134102846 * L_2 = __this->get_x_10();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		FsmFloat_t2134102846 * L_4 = __this->get_x_10();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_5);
	}

IL_002e:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_y_11();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		FsmFloat_t2134102846 * L_8 = __this->get_y_11();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		(&V_0)->set_y_2(L_9);
	}

IL_0050:
	{
		FsmFloat_t2134102846 * L_10 = __this->get_z_12();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0072;
		}
	}
	{
		FsmFloat_t2134102846 * L_12 = __this->get_z_12();
		NullCheck(L_12);
		float L_13 = FsmFloat_get_Value_m4137923823(L_12, /*hidden argument*/NULL);
		(&V_0)->set_z_3(L_13);
	}

IL_0072:
	{
		Vector3_t4282066566  L_14 = V_0;
		Physics_set_gravity_m2814881048(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::.ctor()
extern "C"  void SetGUIAlpha__ctor_m2078852771 (SetGUIAlpha_t974005747 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::Reset()
extern "C"  void SetGUIAlpha_Reset_m4020253008 (SetGUIAlpha_t974005747 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_alpha_9(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t SetGUIAlpha_OnGUI_m1574251421_MetadataUsageId;
extern "C"  void SetGUIAlpha_OnGUI_m1574251421 (SetGUIAlpha_t974005747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIAlpha_OnGUI_m1574251421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_0 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_r_0();
		Color_t4194546905  L_2 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_g_1();
		Color_t4194546905  L_4 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_b_2();
		FsmFloat_t2134102846 * L_6 = __this->get_alpha_9();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		Color_t4194546905  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m2252924356(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		GUI_set_color_m2304110692(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_9 = __this->get_applyGlobally_10();
		NullCheck(L_9);
		bool L_10 = FsmBool_get_Value_m3101329097(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_11 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIColor_m2497813007(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::.ctor()
extern "C"  void SetGUIBackgroundColor__ctor_m1422270860 (SetGUIBackgroundColor_t221834986 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::Reset()
extern "C"  void SetGUIBackgroundColor_Reset_m3363671097 (SetGUIBackgroundColor_t221834986 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_backgroundColor_9(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t SetGUIBackgroundColor_OnGUI_m917669510_MetadataUsageId;
extern "C"  void SetGUIBackgroundColor_OnGUI_m917669510 (SetGUIBackgroundColor_t221834986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIBackgroundColor_OnGUI_m917669510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmColor_t2131419205 * L_0 = __this->get_backgroundColor_9();
		NullCheck(L_0);
		Color_t4194546905  L_1 = FsmColor_get_Value_m1679829997(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_backgroundColor_m885419314(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_2 = __this->get_applyGlobally_10();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_4 = GUI_get_backgroundColor_m542505775(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIBackgroundColor_m4250287389(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::.ctor()
extern "C"  void SetGUIColor__ctor_m931675710 (SetGUIColor_t975938552 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::Reset()
extern "C"  void SetGUIColor_Reset_m2873075947 (SetGUIColor_t975938552 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_color_9(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t SetGUIColor_OnGUI_m427074360_MetadataUsageId;
extern "C"  void SetGUIColor_OnGUI_m427074360 (SetGUIColor_t975938552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIColor_OnGUI_m427074360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmColor_t2131419205 * L_0 = __this->get_color_9();
		NullCheck(L_0);
		Color_t4194546905  L_1 = FsmColor_get_Value_m1679829997(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_color_m2304110692(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_2 = __this->get_applyGlobally_10();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_4 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIColor_m2497813007(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::.ctor()
extern "C"  void SetGUIContentColor__ctor_m4080376283 (SetGUIContentColor_t3816209131 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::Reset()
extern "C"  void SetGUIContentColor_Reset_m1726809224 (SetGUIContentColor_t3816209131 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_contentColor_9(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t SetGUIContentColor_OnGUI_m3575774933_MetadataUsageId;
extern "C"  void SetGUIContentColor_OnGUI_m3575774933 (SetGUIContentColor_t3816209131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIContentColor_OnGUI_m3575774933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmColor_t2131419205 * L_0 = __this->get_contentColor_9();
		NullCheck(L_0);
		Color_t4194546905  L_1 = FsmColor_get_Value_m1679829997(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_2 = __this->get_applyGlobally_10();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_4 = GUI_get_contentColor_m3999665906(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIContentColor_m3079522542(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::.ctor()
extern "C"  void SetGUIDepth__ctor_m1151823582 (SetGUIDepth_t976568152 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::Reset()
extern "C"  void SetGUIDepth_Reset_m3093223819 (SetGUIDepth_t976568152 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_depth_9(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::Awake()
extern "C"  void SetGUIDepth_Awake_m1389428801 (SetGUIDepth_t976568152 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleOnGUI_m1667829569(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SetGUIDepth_OnGUI_m647222232_MetadataUsageId;
extern "C"  void SetGUIDepth_OnGUI_m647222232 (SetGUIDepth_t976568152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIDepth_OnGUI_m647222232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmInt_t1596138449 * L_0 = __this->get_depth_9();
		NullCheck(L_0);
		int32_t L_1 = FsmInt_get_Value_m27059446(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_depth_m4181267379(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::.ctor()
extern "C"  void SetGUISkin__ctor_m3559557672 (SetGUISkin_t884172798 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::Reset()
extern "C"  void SetGUISkin_Reset_m1205990613 (SetGUISkin_t884172798 * __this, const MethodInfo* method)
{
	{
		__this->set_skin_9((GUISkin_t3371348110 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_applyGlobally_10(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t SetGUISkin_OnGUI_m3054956322_MetadataUsageId;
extern "C"  void SetGUISkin_OnGUI_m3054956322 (SetGUISkin_t884172798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUISkin_OnGUI_m3054956322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t3371348110 * L_0 = __this->get_skin_9();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GUISkin_t3371348110 * L_2 = __this->get_skin_9();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_skin_m1213959601(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		FsmBool_t1075959796 * L_3 = __this->get_applyGlobally_10();
		NullCheck(L_3);
		bool L_4 = FsmBool_get_Value_m3101329097(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		GUISkin_t3371348110 * L_5 = __this->get_skin_9();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUISkin_m2923764070(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2313799833_MethodInfo_var;
extern const uint32_t SetGUIText__ctor_m128110712_MetadataUsageId;
extern "C"  void SetGUIText__ctor_m128110712 (SetGUIText_t884197294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIText__ctor_m128110712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2313799833(__this, /*hidden argument*/ComponentAction_1__ctor_m2313799833_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetGUIText_Reset_m2069510949_MetadataUsageId;
extern "C"  void SetGUIText_Reset_m2069510949 (SetGUIText_t884197294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIText_Reset_m2069510949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_12(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::OnEnter()
extern "C"  void SetGUIText_OnEnter_m2247038543 (SetGUIText_t884197294 * __this, const MethodInfo* method)
{
	{
		SetGUIText_DoSetGUIText_m1322523581(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::OnUpdate()
extern "C"  void SetGUIText_OnUpdate_m72277460 (SetGUIText_t884197294 * __this, const MethodInfo* method)
{
	{
		SetGUIText_DoSetGUIText_m1322523581(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::DoSetGUIText()
extern const MethodInfo* ComponentAction_1_UpdateCache_m4044781172_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_guiText_m1496058036_MethodInfo_var;
extern const uint32_t SetGUIText_DoSetGUIText_m1322523581_MetadataUsageId;
extern "C"  void SetGUIText_DoSetGUIText_m1322523581 (SetGUIText_t884197294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUIText_DoSetGUIText_m1322523581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m4044781172(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m4044781172_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		GUIText_t3371372606 * L_5 = ComponentAction_1_get_guiText_m1496058036(__this, /*hidden argument*/ComponentAction_1_get_guiText_m1496058036_MethodInfo_var);
		FsmString_t952858651 * L_6 = __this->get_text_12();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIText_set_text_m1963534853(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2230533721_MethodInfo_var;
extern const uint32_t SetGUITexture__ctor_m2750025734_MetadataUsageId;
extern "C"  void SetGUITexture__ctor_m2750025734 (SetGUITexture_t4209792816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUITexture__ctor_m2750025734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2230533721(__this, /*hidden argument*/ComponentAction_1__ctor_m2230533721_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::Reset()
extern "C"  void SetGUITexture_Reset_m396458675 (SetGUITexture_t4209792816 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_texture_12((FsmTexture_t3073272573 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::OnEnter()
extern const MethodInfo* ComponentAction_1_UpdateCache_m1836126492_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var;
extern const uint32_t SetGUITexture_OnEnter_m761571933_MetadataUsageId;
extern "C"  void SetGUITexture_OnEnter_m761571933 (SetGUITexture_t4209792816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUITexture_OnEnter_m761571933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m1836126492(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m1836126492_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		GUITexture_t4020448292 * L_5 = ComponentAction_1_get_guiTexture_m2636015758(__this, /*hidden argument*/ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var);
		FsmTexture_t3073272573 * L_6 = __this->get_texture_12();
		NullCheck(L_6);
		Texture_t2526458961 * L_7 = FsmTexture_get_Value_m3156202285(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUITexture_set_texture_m1714285765(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2230533721_MethodInfo_var;
extern const uint32_t SetGUITextureAlpha__ctor_m1461583714_MetadataUsageId;
extern "C"  void SetGUITextureAlpha__ctor_m1461583714 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUITextureAlpha__ctor_m1461583714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2230533721(__this, /*hidden argument*/ComponentAction_1__ctor_m2230533721_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::Reset()
extern "C"  void SetGUITextureAlpha_Reset_m3402983951 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_alpha_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::OnEnter()
extern "C"  void SetGUITextureAlpha_OnEnter_m3814339257 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method)
{
	{
		SetGUITextureAlpha_DoGUITextureAlpha_m1856069395(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::OnUpdate()
extern "C"  void SetGUITextureAlpha_OnUpdate_m1413959338 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method)
{
	{
		SetGUITextureAlpha_DoGUITextureAlpha_m1856069395(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::DoGUITextureAlpha()
extern const MethodInfo* ComponentAction_1_UpdateCache_m1836126492_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var;
extern const uint32_t SetGUITextureAlpha_DoGUITextureAlpha_m1856069395_MetadataUsageId;
extern "C"  void SetGUITextureAlpha_DoGUITextureAlpha_m1856069395 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUITextureAlpha_DoGUITextureAlpha_m1856069395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m1836126492(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m1836126492_MethodInfo_var);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		GUITexture_t4020448292 * L_5 = ComponentAction_1_get_guiTexture_m2636015758(__this, /*hidden argument*/ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var);
		NullCheck(L_5);
		Color_t4194546905  L_6 = GUITexture_get_color_m683993630(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GUITexture_t4020448292 * L_7 = ComponentAction_1_get_guiTexture_m2636015758(__this, /*hidden argument*/ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var);
		float L_8 = (&V_1)->get_r_0();
		float L_9 = (&V_1)->get_g_1();
		float L_10 = (&V_1)->get_b_2();
		FsmFloat_t2134102846 * L_11 = __this->get_alpha_12();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		Color_t4194546905  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m2252924356(&L_13, L_8, L_9, L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUITexture_set_color_m3589887669(L_7, L_13, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2230533721_MethodInfo_var;
extern const uint32_t SetGUITextureColor__ctor_m314406653_MetadataUsageId;
extern "C"  void SetGUITextureColor__ctor_m314406653 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUITextureColor__ctor_m314406653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2230533721(__this, /*hidden argument*/ComponentAction_1__ctor_m2230533721_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::Reset()
extern "C"  void SetGUITextureColor_Reset_m2255806890 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_color_12(L_1);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::OnEnter()
extern "C"  void SetGUITextureColor_OnEnter_m888811412 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method)
{
	{
		SetGUITextureColor_DoSetGUITextureColor_m647941811(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::OnUpdate()
extern "C"  void SetGUITextureColor_OnUpdate_m916909359 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method)
{
	{
		SetGUITextureColor_DoSetGUITextureColor_m647941811(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::DoSetGUITextureColor()
extern const MethodInfo* ComponentAction_1_UpdateCache_m1836126492_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var;
extern const uint32_t SetGUITextureColor_DoSetGUITextureColor_m647941811_MetadataUsageId;
extern "C"  void SetGUITextureColor_DoSetGUITextureColor_m647941811 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetGUITextureColor_DoSetGUITextureColor_m647941811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m1836126492(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m1836126492_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		GUITexture_t4020448292 * L_5 = ComponentAction_1_get_guiTexture_m2636015758(__this, /*hidden argument*/ComponentAction_1_get_guiTexture_m2636015758_MethodInfo_var);
		FsmColor_t2131419205 * L_6 = __this->get_color_12();
		NullCheck(L_6);
		Color_t4194546905  L_7 = FsmColor_get_Value_m1679829997(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUITexture_set_color_m3589887669(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::.ctor()
extern "C"  void SetHaloStrength__ctor_m3406108329 (SetHaloStrength_t22635309 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::Reset()
extern "C"  void SetHaloStrength_Reset_m1052541270 (SetHaloStrength_t22635309 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.5f), /*hidden argument*/NULL);
		__this->set_haloStrength_9(L_0);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::OnEnter()
extern "C"  void SetHaloStrength_OnEnter_m4191720512 (SetHaloStrength_t22635309 * __this, const MethodInfo* method)
{
	{
		SetHaloStrength_DoSetHaloStrength_m3078107(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_10();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::OnUpdate()
extern "C"  void SetHaloStrength_OnUpdate_m227876355 (SetHaloStrength_t22635309 * __this, const MethodInfo* method)
{
	{
		SetHaloStrength_DoSetHaloStrength_m3078107(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::DoSetHaloStrength()
extern "C"  void SetHaloStrength_DoSetHaloStrength_m3078107 (SetHaloStrength_t22635309 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_haloStrength_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		RenderSettings_set_haloStrength_m1179623713(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::.ctor()
extern "C"  void SetIntFromFloat__ctor_m2506030179 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::Reset()
extern "C"  void SetIntFromFloat_Reset_m152463120 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method)
{
	{
		__this->set_intVariable_9((FsmInt_t1596138449 *)NULL);
		__this->set_floatValue_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::OnEnter()
extern "C"  void SetIntFromFloat_OnEnter_m2505044858 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmFloat_t2134102846 * L_1 = __this->get_floatValue_10();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, (((int32_t)((int32_t)L_2))), /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::OnUpdate()
extern "C"  void SetIntFromFloat_OnUpdate_m3775505929 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmFloat_t2134102846 * L_1 = __this->get_floatValue_10();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, (((int32_t)((int32_t)L_2))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::.ctor()
extern "C"  void SetIntValue__ctor_m3319958436 (SetIntValue_t2127530962 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::Reset()
extern "C"  void SetIntValue_Reset_m966391377 (SetIntValue_t2127530962 * __this, const MethodInfo* method)
{
	{
		__this->set_intVariable_9((FsmInt_t1596138449 *)NULL);
		__this->set_intValue_10((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::OnEnter()
extern "C"  void SetIntValue_OnEnter_m3006051963 (SetIntValue_t2127530962 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmInt_t1596138449 * L_1 = __this->get_intValue_10();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::OnUpdate()
extern "C"  void SetIntValue_OnUpdate_m2126857000 (SetIntValue_t2127530962 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmInt_t1596138449 * L_1 = __this->get_intValue_10();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t SetIsKinematic__ctor_m2920400485_MetadataUsageId;
extern "C"  void SetIsKinematic__ctor_m2920400485 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetIsKinematic__ctor_m2920400485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::Reset()
extern "C"  void SetIsKinematic_Reset_m566833426 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_isKinematic_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::OnEnter()
extern "C"  void SetIsKinematic_OnEnter_m1282950396 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method)
{
	{
		SetIsKinematic_DoSetIsKinematic_m2196068835(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::DoSetIsKinematic()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t SetIsKinematic_DoSetIsKinematic_m2196068835_MetadataUsageId;
extern "C"  void SetIsKinematic_DoSetIsKinematic_m2196068835 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetIsKinematic_DoSetIsKinematic_m2196068835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		FsmBool_t1075959796 * L_6 = __this->get_isKinematic_12();
		NullCheck(L_6);
		bool L_7 = FsmBool_get_Value_m3101329097(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_isKinematic_m294703295(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetJointConnectedBody::.ctor()
extern "C"  void SetJointConnectedBody__ctor_m3802958245 (SetJointConnectedBody_t2876069553 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetJointConnectedBody::Reset()
extern "C"  void SetJointConnectedBody_Reset_m1449391186 (SetJointConnectedBody_t2876069553 * __this, const MethodInfo* method)
{
	{
		__this->set_joint_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_rigidBody_10((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetJointConnectedBody::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisJoint_t4201008640_m884734414_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405_MethodInfo_var;
extern const uint32_t SetJointConnectedBody_OnEnter_m3312400444_MetadataUsageId;
extern "C"  void SetJointConnectedBody_OnEnter_m3312400444 (SetJointConnectedBody_t2876069553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetJointConnectedBody_OnEnter_m3312400444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Joint_t4201008640 * V_1 = NULL;
	Joint_t4201008640 * G_B4_0 = NULL;
	Joint_t4201008640 * G_B3_0 = NULL;
	Rigidbody_t3346577219 * G_B5_0 = NULL;
	Joint_t4201008640 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_joint_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0063;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Joint_t4201008640 * L_6 = GameObject_GetComponent_TisJoint_t4201008640_m884734414(L_5, /*hidden argument*/GameObject_GetComponent_TisJoint_t4201008640_m884734414_MethodInfo_var);
		V_1 = L_6;
		Joint_t4201008640 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0063;
		}
	}
	{
		Joint_t4201008640 * L_9 = V_1;
		FsmGameObject_t1697147867 * L_10 = __this->get_rigidBody_10();
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = FsmGameObject_get_Value_m673294275(L_10, /*hidden argument*/NULL);
		bool L_12 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = L_9;
		if (!L_12)
		{
			G_B4_0 = L_9;
			goto IL_004e;
		}
	}
	{
		G_B5_0 = ((Rigidbody_t3346577219 *)(NULL));
		G_B5_1 = G_B3_0;
		goto IL_005e;
	}

IL_004e:
	{
		FsmGameObject_t1697147867 * L_13 = __this->get_rigidBody_10();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = FsmGameObject_get_Value_m673294275(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Rigidbody_t3346577219 * L_15 = GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405(L_14, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405_MethodInfo_var);
		G_B5_0 = L_15;
		G_B5_1 = G_B4_0;
	}

IL_005e:
	{
		NullCheck(G_B5_1);
		Joint_set_connectedBody_m2794572257(G_B5_1, G_B5_0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLayer::.ctor()
extern "C"  void SetLayer__ctor_m1126484367 (SetLayer_t1144868535 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLayer::Reset()
extern "C"  void SetLayer_Reset_m3067884604 (SetLayer_t1144868535 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layer_10(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLayer::OnEnter()
extern "C"  void SetLayer_OnEnter_m3906413990 (SetLayer_t1144868535 * __this, const MethodInfo* method)
{
	{
		SetLayer_DoSetLayer_m131681295(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLayer::DoSetLayer()
extern "C"  void SetLayer_DoSetLayer_m131681295 (SetLayer_t1144868535 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		int32_t L_6 = __this->get_layer_10();
		NullCheck(L_5);
		GameObject_set_layer_m1872241535(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightColor__ctor_m1394617881_MetadataUsageId;
extern "C"  void SetLightColor__ctor_m1394617881 (SetLightColor_t3133114301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightColor__ctor_m1394617881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::Reset()
extern "C"  void SetLightColor_Reset_m3336018118 (SetLightColor_t3133114301 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_lightColor_12(L_1);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::OnEnter()
extern "C"  void SetLightColor_OnEnter_m3884683184 (SetLightColor_t3133114301 * __this, const MethodInfo* method)
{
	{
		SetLightColor_DoSetLightColor_m91388379(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::OnUpdate()
extern "C"  void SetLightColor_OnUpdate_m3594621075 (SetLightColor_t3133114301 * __this, const MethodInfo* method)
{
	{
		SetLightColor_DoSetLightColor_m91388379(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::DoSetLightColor()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightColor_DoSetLightColor_m91388379_MetadataUsageId;
extern "C"  void SetLightColor_DoSetLightColor_m91388379 (SetLightColor_t3133114301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightColor_DoSetLightColor_m91388379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		FsmColor_t2131419205 * L_6 = __this->get_lightColor_12();
		NullCheck(L_6);
		Color_t4194546905  L_7 = FsmColor_get_Value_m1679829997(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Light_set_color_m763171967(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightCookie__ctor_m1478192166_MetadataUsageId;
extern "C"  void SetLightCookie__ctor_m1478192166 (SetLightCookie_t1988387904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightCookie__ctor_m1478192166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::Reset()
extern "C"  void SetLightCookie_Reset_m3419592403 (SetLightCookie_t1988387904 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_lightCookie_12((FsmTexture_t3073272573 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::OnEnter()
extern "C"  void SetLightCookie_OnEnter_m2595192445 (SetLightCookie_t1988387904 * __this, const MethodInfo* method)
{
	{
		SetLightCookie_DoSetLightCookie_m3086426849(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::DoSetLightCookie()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightCookie_DoSetLightCookie_m3086426849_MetadataUsageId;
extern "C"  void SetLightCookie_DoSetLightCookie_m3086426849 (SetLightCookie_t1988387904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightCookie_DoSetLightCookie_m3086426849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		FsmTexture_t3073272573 * L_6 = __this->get_lightCookie_12();
		NullCheck(L_6);
		Texture_t2526458961 * L_7 = FsmTexture_get_Value_m3156202285(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Light_set_cookie_m2487166540(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightFlare__ctor_m797029902_MetadataUsageId;
extern "C"  void SetLightFlare__ctor_m797029902 (SetLightFlare_t3135785000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightFlare__ctor_m797029902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::Reset()
extern "C"  void SetLightFlare_Reset_m2738430139 (SetLightFlare_t3135785000 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_lightFlare_12((Flare_t4197217604 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::OnEnter()
extern "C"  void SetLightFlare_OnEnter_m833285733 (SetLightFlare_t3135785000 * __this, const MethodInfo* method)
{
	{
		SetLightFlare_DoSetLightRange_m3077066474(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::DoSetLightRange()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightFlare_DoSetLightRange_m3077066474_MetadataUsageId;
extern "C"  void SetLightFlare_DoSetLightRange_m3077066474 (SetLightFlare_t3135785000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightFlare_DoSetLightRange_m3077066474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		Flare_t4197217604 * L_6 = __this->get_lightFlare_12();
		NullCheck(L_5);
		Light_set_flare_m3803200297(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightIntensity__ctor_m375306953_MetadataUsageId;
extern "C"  void SetLightIntensity__ctor_m375306953 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightIntensity__ctor_m375306953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::Reset()
extern "C"  void SetLightIntensity_Reset_m2316707190 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_lightIntensity_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::OnEnter()
extern "C"  void SetLightIntensity_OnEnter_m3579424864 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method)
{
	{
		SetLightIntensity_DoSetLightIntensity_m2178421723(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::OnUpdate()
extern "C"  void SetLightIntensity_OnUpdate_m2721547747 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method)
{
	{
		SetLightIntensity_DoSetLightIntensity_m2178421723(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::DoSetLightIntensity()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightIntensity_DoSetLightIntensity_m2178421723_MetadataUsageId;
extern "C"  void SetLightIntensity_DoSetLightIntensity_m2178421723 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightIntensity_DoSetLightIntensity_m2178421723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_lightIntensity_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Light_set_intensity_m2689709876(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightRange__ctor_m2222430591_MetadataUsageId;
extern "C"  void SetLightRange__ctor_m2222430591 (SetLightRange_t3146551703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightRange__ctor_m2222430591_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::Reset()
extern "C"  void SetLightRange_Reset_m4163830828 (SetLightRange_t3146551703 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (20.0f), /*hidden argument*/NULL);
		__this->set_lightRange_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::OnEnter()
extern "C"  void SetLightRange_OnEnter_m548780438 (SetLightRange_t3146551703 * __this, const MethodInfo* method)
{
	{
		SetLightRange_DoSetLightRange_m2710657307(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::OnUpdate()
extern "C"  void SetLightRange_OnUpdate_m3260851053 (SetLightRange_t3146551703 * __this, const MethodInfo* method)
{
	{
		SetLightRange_DoSetLightRange_m2710657307(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::DoSetLightRange()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightRange_DoSetLightRange_m2710657307_MetadataUsageId;
extern "C"  void SetLightRange_DoSetLightRange_m2710657307 (SetLightRange_t3146551703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightRange_DoSetLightRange_m2710657307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_lightRange_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Light_set_range_m1834313578(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightSpotAngle__ctor_m412477387_MetadataUsageId;
extern "C"  void SetLightSpotAngle__ctor_m412477387 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightSpotAngle__ctor_m412477387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::Reset()
extern "C"  void SetLightSpotAngle_Reset_m2353877624 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (20.0f), /*hidden argument*/NULL);
		__this->set_lightSpotAngle_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::OnEnter()
extern "C"  void SetLightSpotAngle_OnEnter_m645506274 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method)
{
	{
		SetLightSpotAngle_DoSetLightRange_m176896103(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::OnUpdate()
extern "C"  void SetLightSpotAngle_OnUpdate_m1964384673 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method)
{
	{
		SetLightSpotAngle_DoSetLightRange_m176896103(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::DoSetLightRange()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightSpotAngle_DoSetLightRange_m176896103_MetadataUsageId;
extern "C"  void SetLightSpotAngle_DoSetLightRange_m176896103 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightSpotAngle_DoSetLightRange_m176896103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_lightSpotAngle_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Light_set_spotAngle_m3212810934(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightType::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetLightType__ctor_m1344122320_MetadataUsageId;
extern "C"  void SetLightType__ctor_m1344122320 (SetLightType_t1507991894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightType__ctor_m1344122320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightType::Reset()
extern "C"  void SetLightType_Reset_m3285522557 (SetLightType_t1507991894 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_lightType_12(2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightType::OnEnter()
extern "C"  void SetLightType_OnEnter_m2603089319 (SetLightType_t1507991894 * __this, const MethodInfo* method)
{
	{
		SetLightType_DoSetLightType_m1127265997(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetLightType::DoSetLightType()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetLightType_DoSetLightType_m1127265997_MetadataUsageId;
extern "C"  void SetLightType_DoSetLightType_m1127265997 (SetLightType_t1507991894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetLightType_DoSetLightType_m1127265997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		int32_t L_6 = __this->get_lightType_12();
		NullCheck(L_5);
		Light_set_type_m1196490817(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::.ctor()
extern "C"  void SetMainCamera__ctor_m2665790184 (SetMainCamera_t3356853774 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::Reset()
extern "C"  void SetMainCamera_Reset_m312223125 (SetMainCamera_t3356853774 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::OnEnter()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern Il2CppCodeGenString* _stringLiteral1982636638;
extern const uint32_t SetMainCamera_OnEnter_m1415587007_MetadataUsageId;
extern "C"  void SetMainCamera_OnEnter_m1415587007 (SetMainCamera_t3356853774 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMainCamera_OnEnter_m1415587007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		Camera_t2727095145 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		Camera_t2727095145 * L_5 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_set_tag_m859036203(L_6, _stringLiteral69913957, /*hidden argument*/NULL);
	}

IL_003a:
	{
		FsmGameObject_t1697147867 * L_7 = __this->get_gameObject_9();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = FsmGameObject_get_Value_m673294275(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_set_tag_m859036203(L_8, _stringLiteral1982636638, /*hidden argument*/NULL);
	}

IL_004f:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMass::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t SetMass__ctor_m3167718706_MetadataUsageId;
extern "C"  void SetMass__ctor_m3167718706 (SetMass_t3798673028 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMass__ctor_m3167718706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMass::Reset()
extern "C"  void SetMass_Reset_m814151647 (SetMass_t3798673028 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_mass_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMass::OnEnter()
extern "C"  void SetMass_OnEnter_m2732559497 (SetMass_t3798673028 * __this, const MethodInfo* method)
{
	{
		SetMass_DoSetMass_m1119298427(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMass::DoSetMass()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t SetMass_DoSetMass_m1119298427_MetadataUsageId;
extern "C"  void SetMass_DoSetMass_m1119298427 (SetMass_t3798673028 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMass_DoSetMass_m1119298427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_mass_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_mass_m1579962594(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetMaterial__ctor_m2615357151_MetadataUsageId;
extern "C"  void SetMaterial__ctor_m2615357151 (SetMaterial_t1870547511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterial__ctor_m2615357151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::Reset()
extern "C"  void SetMaterial_Reset_m261790092 (SetMaterial_t1870547511 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_material_13((FsmMaterial_t924399665 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::OnEnter()
extern "C"  void SetMaterial_OnEnter_m194082550 (SetMaterial_t1870547511 * __this, const MethodInfo* method)
{
	{
		SetMaterial_DoSetMaterial_m1620619931(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::DoSetMaterial()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t SetMaterial_DoSetMaterial_m1620619931_MetadataUsageId;
extern "C"  void SetMaterial_DoSetMaterial_m1620619931 (SetMaterial_t1870547511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterial_DoSetMaterial_m1620619931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MaterialU5BU5D_t170856778* V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmInt_t1596138449 * L_5 = __this->get_materialIndex_12();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004a;
		}
	}
	{
		Renderer_t3076687687 * L_7 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		FsmMaterial_t924399665 * L_8 = __this->get_material_13();
		NullCheck(L_8);
		Material_t3870600107 * L_9 = FsmMaterial_get_Value_m1376213207(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Renderer_set_material_m1012580896(L_7, L_9, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_004a:
	{
		Renderer_t3076687687 * L_10 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_10);
		MaterialU5BU5D_t170856778* L_11 = Renderer_get_materials_m3755041148(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmInt_t1596138449 * L_12 = __this->get_materialIndex_12();
		NullCheck(L_12);
		int32_t L_13 = FsmInt_get_Value_m27059446(L_12, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) <= ((int32_t)L_13)))
		{
			goto IL_0097;
		}
	}
	{
		Renderer_t3076687687 * L_14 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_14);
		MaterialU5BU5D_t170856778* L_15 = Renderer_get_materials_m3755041148(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		MaterialU5BU5D_t170856778* L_16 = V_1;
		FsmInt_t1596138449 * L_17 = __this->get_materialIndex_12();
		NullCheck(L_17);
		int32_t L_18 = FsmInt_get_Value_m27059446(L_17, /*hidden argument*/NULL);
		FsmMaterial_t924399665 * L_19 = __this->get_material_13();
		NullCheck(L_19);
		Material_t3870600107 * L_20 = FsmMaterial_get_Value_m1376213207(L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_18);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Material_t3870600107 *)L_20);
		Renderer_t3076687687 * L_21 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_22 = V_1;
		NullCheck(L_21);
		Renderer_set_materials_m268031319(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetMaterialColor__ctor_m3524442052_MetadataUsageId;
extern "C"  void SetMaterialColor__ctor_m3524442052 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialColor__ctor_m3524442052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::Reset()
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern const uint32_t SetMaterialColor_Reset_m1170874993_MetadataUsageId;
extern "C"  void SetMaterialColor_Reset_m1170874993 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialColor_Reset_m1170874993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		__this->set_material_13((FsmMaterial_t924399665 *)NULL);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral2785059396, /*hidden argument*/NULL);
		__this->set_namedColor_14(L_1);
		Color_t4194546905  L_2 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_3 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_color_15(L_3);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::OnEnter()
extern "C"  void SetMaterialColor_OnEnter_m1946311323 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method)
{
	{
		SetMaterialColor_DoSetMaterialColor_m2216301925(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::OnUpdate()
extern "C"  void SetMaterialColor_OnUpdate_m3634635528 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method)
{
	{
		SetMaterialColor_DoSetMaterialColor_m2216301925(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::DoSetMaterialColor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern Il2CppCodeGenString* _stringLiteral3480108384;
extern const uint32_t SetMaterialColor_DoSetMaterialColor_m2216301925_MetadataUsageId;
extern "C"  void SetMaterialColor_DoSetMaterialColor_m2216301925 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialColor_DoSetMaterialColor_m2216301925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	MaterialU5BU5D_t170856778* V_2 = NULL;
	{
		FsmColor_t2131419205 * L_0 = __this->get_color_15();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmString_t952858651 * L_2 = __this->get_namedColor_14();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		V_0 = _stringLiteral2785059396;
	}

IL_0033:
	{
		FsmMaterial_t924399665 * L_7 = __this->get_material_13();
		NullCheck(L_7);
		Material_t3870600107 * L_8 = FsmMaterial_get_Value_m1376213207(L_7, /*hidden argument*/NULL);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0066;
		}
	}
	{
		FsmMaterial_t924399665 * L_10 = __this->get_material_13();
		NullCheck(L_10);
		Material_t3870600107 * L_11 = FsmMaterial_get_Value_m1376213207(L_10, /*hidden argument*/NULL);
		String_t* L_12 = V_0;
		FsmColor_t2131419205 * L_13 = __this->get_color_15();
		NullCheck(L_13);
		Color_t4194546905  L_14 = FsmColor_get_Value_m1679829997(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetColor_m1918430019(L_11, L_12, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0066:
	{
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_16 = __this->get_gameObject_11();
		NullCheck(L_15);
		GameObject_t3674682005 * L_17 = Fsm_GetOwnerDefaultTarget_m846013999(L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		GameObject_t3674682005 * L_18 = V_1;
		bool L_19 = ComponentAction_1_UpdateCache_m3595067967(__this, L_18, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_19)
		{
			goto IL_0085;
		}
	}
	{
		return;
	}

IL_0085:
	{
		Renderer_t3076687687 * L_20 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_20);
		Material_t3870600107 * L_21 = Renderer_get_material_m2720864603(L_20, /*hidden argument*/NULL);
		bool L_22 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_21, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a7;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral3480108384, /*hidden argument*/NULL);
		return;
	}

IL_00a7:
	{
		FsmInt_t1596138449 * L_23 = __this->get_materialIndex_12();
		NullCheck(L_23);
		int32_t L_24 = FsmInt_get_Value_m27059446(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00d8;
		}
	}
	{
		Renderer_t3076687687 * L_25 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_25);
		Material_t3870600107 * L_26 = Renderer_get_material_m2720864603(L_25, /*hidden argument*/NULL);
		String_t* L_27 = V_0;
		FsmColor_t2131419205 * L_28 = __this->get_color_15();
		NullCheck(L_28);
		Color_t4194546905  L_29 = FsmColor_get_Value_m1679829997(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		Material_SetColor_m1918430019(L_26, L_27, L_29, /*hidden argument*/NULL);
		goto IL_012b;
	}

IL_00d8:
	{
		Renderer_t3076687687 * L_30 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_30);
		MaterialU5BU5D_t170856778* L_31 = Renderer_get_materials_m3755041148(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		FsmInt_t1596138449 * L_32 = __this->get_materialIndex_12();
		NullCheck(L_32);
		int32_t L_33 = FsmInt_get_Value_m27059446(L_32, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length))))) <= ((int32_t)L_33)))
		{
			goto IL_012b;
		}
	}
	{
		Renderer_t3076687687 * L_34 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_34);
		MaterialU5BU5D_t170856778* L_35 = Renderer_get_materials_m3755041148(L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		MaterialU5BU5D_t170856778* L_36 = V_2;
		FsmInt_t1596138449 * L_37 = __this->get_materialIndex_12();
		NullCheck(L_37);
		int32_t L_38 = FsmInt_get_Value_m27059446(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_38);
		int32_t L_39 = L_38;
		Material_t3870600107 * L_40 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		String_t* L_41 = V_0;
		FsmColor_t2131419205 * L_42 = __this->get_color_15();
		NullCheck(L_42);
		Color_t4194546905  L_43 = FsmColor_get_Value_m1679829997(L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		Material_SetColor_m1918430019(L_40, L_41, L_43, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_44 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_45 = V_2;
		NullCheck(L_44);
		Renderer_set_materials_m268031319(L_44, L_45, /*hidden argument*/NULL);
	}

IL_012b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetMaterialFloat__ctor_m2269711595_MetadataUsageId;
extern "C"  void SetMaterialFloat__ctor_m2269711595 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialFloat__ctor_m2269711595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetMaterialFloat_Reset_m4211111832_MetadataUsageId;
extern "C"  void SetMaterialFloat_Reset_m4211111832 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialFloat_Reset_m4211111832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		__this->set_material_13((FsmMaterial_t924399665 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_namedFloat_14(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_floatValue_15(L_3);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::OnEnter()
extern "C"  void SetMaterialFloat_OnEnter_m3036152322 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method)
{
	{
		SetMaterialFloat_DoSetMaterialFloat_m488091735(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::OnUpdate()
extern "C"  void SetMaterialFloat_OnUpdate_m3059968129 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method)
{
	{
		SetMaterialFloat_DoSetMaterialFloat_m488091735(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::DoSetMaterialFloat()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3480108384;
extern const uint32_t SetMaterialFloat_DoSetMaterialFloat_m488091735_MetadataUsageId;
extern "C"  void SetMaterialFloat_DoSetMaterialFloat_m488091735 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialFloat_DoSetMaterialFloat_m488091735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MaterialU5BU5D_t170856778* V_1 = NULL;
	{
		FsmMaterial_t924399665 * L_0 = __this->get_material_13();
		NullCheck(L_0);
		Material_t3870600107 * L_1 = FsmMaterial_get_Value_m1376213207(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		FsmMaterial_t924399665 * L_3 = __this->get_material_13();
		NullCheck(L_3);
		Material_t3870600107 * L_4 = FsmMaterial_get_Value_m1376213207(L_3, /*hidden argument*/NULL);
		FsmString_t952858651 * L_5 = __this->get_namedFloat_14();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_7 = __this->get_floatValue_15();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_SetFloat_m981710063(L_4, L_6, L_8, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_10 = __this->get_gameObject_11();
		NullCheck(L_9);
		GameObject_t3674682005 * L_11 = Fsm_GetOwnerDefaultTarget_m846013999(L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		GameObject_t3674682005 * L_12 = V_0;
		bool L_13 = ComponentAction_1_UpdateCache_m3595067967(__this, L_12, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_13)
		{
			goto IL_005c;
		}
	}
	{
		return;
	}

IL_005c:
	{
		Renderer_t3076687687 * L_14 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_14);
		Material_t3870600107 * L_15 = Renderer_get_material_m2720864603(L_14, /*hidden argument*/NULL);
		bool L_16 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_15, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_007e;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral3480108384, /*hidden argument*/NULL);
		return;
	}

IL_007e:
	{
		FsmInt_t1596138449 * L_17 = __this->get_materialIndex_12();
		NullCheck(L_17);
		int32_t L_18 = FsmInt_get_Value_m27059446(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00b9;
		}
	}
	{
		Renderer_t3076687687 * L_19 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_19);
		Material_t3870600107 * L_20 = Renderer_get_material_m2720864603(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_namedFloat_14();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_23 = __this->get_floatValue_15();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		Material_SetFloat_m981710063(L_20, L_22, L_24, /*hidden argument*/NULL);
		goto IL_0116;
	}

IL_00b9:
	{
		Renderer_t3076687687 * L_25 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_25);
		MaterialU5BU5D_t170856778* L_26 = Renderer_get_materials_m3755041148(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		FsmInt_t1596138449 * L_27 = __this->get_materialIndex_12();
		NullCheck(L_27);
		int32_t L_28 = FsmInt_get_Value_m27059446(L_27, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))) <= ((int32_t)L_28)))
		{
			goto IL_0116;
		}
	}
	{
		Renderer_t3076687687 * L_29 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_29);
		MaterialU5BU5D_t170856778* L_30 = Renderer_get_materials_m3755041148(L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		MaterialU5BU5D_t170856778* L_31 = V_1;
		FsmInt_t1596138449 * L_32 = __this->get_materialIndex_12();
		NullCheck(L_32);
		int32_t L_33 = FsmInt_get_Value_m27059446(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_33);
		int32_t L_34 = L_33;
		Material_t3870600107 * L_35 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		FsmString_t952858651 * L_36 = __this->get_namedFloat_14();
		NullCheck(L_36);
		String_t* L_37 = FsmString_get_Value_m872383149(L_36, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_38 = __this->get_floatValue_15();
		NullCheck(L_38);
		float L_39 = FsmFloat_get_Value_m4137923823(L_38, /*hidden argument*/NULL);
		NullCheck(L_35);
		Material_SetFloat_m981710063(L_35, L_37, L_39, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_40 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_41 = V_1;
		NullCheck(L_40);
		Renderer_set_materials_m268031319(L_40, L_41, /*hidden argument*/NULL);
	}

IL_0116:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetMaterialTexture__ctor_m3317448716_MetadataUsageId;
extern "C"  void SetMaterialTexture__ctor_m3317448716 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialTexture__ctor_m3317448716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::Reset()
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t SetMaterialTexture_Reset_m963881657_MetadataUsageId;
extern "C"  void SetMaterialTexture_Reset_m963881657 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialTexture_Reset_m963881657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		__this->set_material_13((FsmMaterial_t924399665 *)NULL);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral558922319, /*hidden argument*/NULL);
		__this->set_namedTexture_14(L_1);
		__this->set_texture_15((FsmTexture_t3073272573 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::OnEnter()
extern "C"  void SetMaterialTexture_OnEnter_m594211043 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method)
{
	{
		SetMaterialTexture_DoSetMaterialTexture_m1197842325(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::DoSetMaterialTexture()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern Il2CppCodeGenString* _stringLiteral3480108384;
extern const uint32_t SetMaterialTexture_DoSetMaterialTexture_m1197842325_MetadataUsageId;
extern "C"  void SetMaterialTexture_DoSetMaterialTexture_m1197842325 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMaterialTexture_DoSetMaterialTexture_m1197842325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	MaterialU5BU5D_t170856778* V_2 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_namedTexture_14();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		V_0 = _stringLiteral558922319;
	}

IL_0022:
	{
		FsmMaterial_t924399665 * L_5 = __this->get_material_13();
		NullCheck(L_5);
		Material_t3870600107 * L_6 = FsmMaterial_get_Value_m1376213207(L_5, /*hidden argument*/NULL);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}
	{
		FsmMaterial_t924399665 * L_8 = __this->get_material_13();
		NullCheck(L_8);
		Material_t3870600107 * L_9 = FsmMaterial_get_Value_m1376213207(L_8, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		FsmTexture_t3073272573 * L_11 = __this->get_texture_15();
		NullCheck(L_11);
		Texture_t2526458961 * L_12 = FsmTexture_get_Value_m3156202285(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetTexture_m1833724755(L_9, L_10, L_12, /*hidden argument*/NULL);
		return;
	}

IL_0055:
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_14 = __this->get_gameObject_11();
		NullCheck(L_13);
		GameObject_t3674682005 * L_15 = Fsm_GetOwnerDefaultTarget_m846013999(L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		GameObject_t3674682005 * L_16 = V_1;
		bool L_17 = ComponentAction_1_UpdateCache_m3595067967(__this, L_16, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_17)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		Renderer_t3076687687 * L_18 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_18);
		Material_t3870600107 * L_19 = Renderer_get_material_m2720864603(L_18, /*hidden argument*/NULL);
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0096;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral3480108384, /*hidden argument*/NULL);
		return;
	}

IL_0096:
	{
		FsmInt_t1596138449 * L_21 = __this->get_materialIndex_12();
		NullCheck(L_21);
		int32_t L_22 = FsmInt_get_Value_m27059446(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00c7;
		}
	}
	{
		Renderer_t3076687687 * L_23 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_23);
		Material_t3870600107 * L_24 = Renderer_get_material_m2720864603(L_23, /*hidden argument*/NULL);
		String_t* L_25 = V_0;
		FsmTexture_t3073272573 * L_26 = __this->get_texture_15();
		NullCheck(L_26);
		Texture_t2526458961 * L_27 = FsmTexture_get_Value_m3156202285(L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		Material_SetTexture_m1833724755(L_24, L_25, L_27, /*hidden argument*/NULL);
		goto IL_011a;
	}

IL_00c7:
	{
		Renderer_t3076687687 * L_28 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_28);
		MaterialU5BU5D_t170856778* L_29 = Renderer_get_materials_m3755041148(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		FsmInt_t1596138449 * L_30 = __this->get_materialIndex_12();
		NullCheck(L_30);
		int32_t L_31 = FsmInt_get_Value_m27059446(L_30, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length))))) <= ((int32_t)L_31)))
		{
			goto IL_011a;
		}
	}
	{
		Renderer_t3076687687 * L_32 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_32);
		MaterialU5BU5D_t170856778* L_33 = Renderer_get_materials_m3755041148(L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		MaterialU5BU5D_t170856778* L_34 = V_2;
		FsmInt_t1596138449 * L_35 = __this->get_materialIndex_12();
		NullCheck(L_35);
		int32_t L_36 = FsmInt_get_Value_m27059446(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_36);
		int32_t L_37 = L_36;
		Material_t3870600107 * L_38 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		String_t* L_39 = V_0;
		FsmTexture_t3073272573 * L_40 = __this->get_texture_15();
		NullCheck(L_40);
		Texture_t2526458961 * L_41 = FsmTexture_get_Value_m3156202285(L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		Material_SetTexture_m1833724755(L_38, L_39, L_41, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_42 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_43 = V_2;
		NullCheck(L_42);
		Renderer_set_materials_m268031319(L_42, L_43, /*hidden argument*/NULL);
	}

IL_011a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::.ctor()
extern "C"  void SetMouseCursor__ctor_m1255562245 (SetMouseCursor_t1046826369 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::Reset()
extern "C"  void SetMouseCursor_Reset_m3196962482 (SetMouseCursor_t1046826369 * __this, const MethodInfo* method)
{
	{
		__this->set_cursorTexture_9((FsmTexture_t3073272573 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_hideCursor_10(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_lockCursor_11(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::OnEnter()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t SetMouseCursor_OnEnter_m3396203164_MetadataUsageId;
extern "C"  void SetMouseCursor_OnEnter_m3396203164 (SetMouseCursor_t1046826369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetMouseCursor_OnEnter_m3396203164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_lockCursor_11();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_LockCursor_m2221249063(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_2 = __this->get_hideCursor_10();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		PlayMakerGUI_set_HideCursor_m2729864958(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_4 = __this->get_cursorTexture_9();
		NullCheck(L_4);
		Texture_t2526458961 * L_5 = FsmTexture_get_Value_m3156202285(L_4, /*hidden argument*/NULL);
		PlayMakerGUI_set_MouseCursor_m346590176(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetName::.ctor()
extern "C"  void SetName__ctor_m3522311035 (SetName_t3798702619 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetName::Reset()
extern "C"  void SetName_Reset_m1168743976 (SetName_t3798702619 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_name_10((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetName::OnEnter()
extern "C"  void SetName_OnEnter_m4193371282 (SetName_t3798702619 * __this, const MethodInfo* method)
{
	{
		SetName_DoSetLayer_m1891448227(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetName::DoSetLayer()
extern "C"  void SetName_DoSetLayer_m1891448227 (SetName_t3798702619 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		FsmString_t952858651 * L_6 = __this->get_name_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Object_set_name_m1123518500(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::.ctor()
extern "C"  void SetObjectValue__ctor_m3958826190 (SetObjectValue_t3678867096 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::Reset()
extern "C"  void SetObjectValue_Reset_m1605259131 (SetObjectValue_t3678867096 * __this, const MethodInfo* method)
{
	{
		__this->set_objectVariable_9((FsmObject_t821476169 *)NULL);
		__this->set_objectValue_10((FsmObject_t821476169 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::OnEnter()
extern "C"  void SetObjectValue_OnEnter_m2777640229 (SetObjectValue_t3678867096 * __this, const MethodInfo* method)
{
	{
		FsmObject_t821476169 * L_0 = __this->get_objectVariable_9();
		FsmObject_t821476169 * L_1 = __this->get_objectValue_10();
		NullCheck(L_1);
		Object_t3071478659 * L_2 = FsmObject_get_Value_m188501991(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmObject_set_Value_m867520242(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::OnUpdate()
extern "C"  void SetObjectValue_OnUpdate_m3636027838 (SetObjectValue_t3678867096 * __this, const MethodInfo* method)
{
	{
		FsmObject_t821476169 * L_0 = __this->get_objectVariable_9();
		FsmObject_t821476169 * L_1 = __this->get_objectValue_10();
		NullCheck(L_1);
		Object_t3071478659 * L_2 = FsmObject_get_Value_m188501991(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmObject_set_Value_m867520242(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetParent::.ctor()
extern "C"  void SetParent__ctor_m3599063932 (SetParent_t596534010 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetParent::Reset()
extern "C"  void SetParent_Reset_m1245496873 (SetParent_t596534010 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parent_10((FsmGameObject_t1697147867 *)NULL);
		__this->set_resetLocalPosition_11((FsmBool_t1075959796 *)NULL);
		__this->set_resetLocalRotation_12((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetParent::OnEnter()
extern "C"  void SetParent_OnEnter_m643493971 (SetParent_t596534010 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Transform_t1659122786 * G_B3_0 = NULL;
	Transform_t1659122786 * G_B2_0 = NULL;
	Transform_t1659122786 * G_B4_0 = NULL;
	Transform_t1659122786 * G_B4_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0095;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_7 = __this->get_parent_10();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = FsmGameObject_get_Value_m673294275(L_7, /*hidden argument*/NULL);
		bool L_9 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = L_6;
		if (!L_9)
		{
			G_B3_0 = L_6;
			goto IL_0040;
		}
	}
	{
		G_B4_0 = ((Transform_t1659122786 *)(NULL));
		G_B4_1 = G_B2_0;
		goto IL_0050;
	}

IL_0040:
	{
		FsmGameObject_t1697147867 * L_10 = __this->get_parent_10();
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = FsmGameObject_get_Value_m673294275(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0050:
	{
		NullCheck(G_B4_1);
		Transform_set_parent_m3231272063(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_13 = __this->get_resetLocalPosition_11();
		NullCheck(L_13);
		bool L_14 = FsmBool_get_Value_m3101329097(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0075;
		}
	}
	{
		GameObject_t3674682005 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		Vector3_t4282066566  L_17 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localPosition_m3504330903(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0075:
	{
		FsmBool_t1075959796 * L_18 = __this->get_resetLocalRotation_12();
		NullCheck(L_18);
		bool L_19 = FsmBool_get_Value_m3101329097(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0095;
		}
	}
	{
		GameObject_t3674682005 * L_20 = V_0;
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_22 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localRotation_m3719981474(L_21, L_22, /*hidden argument*/NULL);
	}

IL_0095:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetPosition::.ctor()
extern "C"  void SetPosition__ctor_m4220607773 (SetPosition_t2319285817 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetPosition::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetPosition_Reset_m1867040714_MetadataUsageId;
extern "C"  void SetPosition_Reset_m1867040714 (SetPosition_t2319285817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetPosition_Reset_m1867040714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_10((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_11(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_12(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_z_13(L_8);
		__this->set_space_14(1);
		__this->set_everyFrame_15((bool)0);
		__this->set_lateUpdate_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnEnter()
extern "C"  void SetPosition_OnEnter_m946671028 (SetPosition_t2319285817 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		bool L_1 = __this->get_lateUpdate_16();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		SetPosition_DoSetPosition_m3465412187(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnUpdate()
extern "C"  void SetPosition_OnUpdate_m2710557455 (SetPosition_t2319285817 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_16();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetPosition_DoSetPosition_m3465412187(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnLateUpdate()
extern "C"  void SetPosition_OnLateUpdate_m2459138389 (SetPosition_t2319285817 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_16();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetPosition_DoSetPosition_m3465412187(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_everyFrame_15();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetPosition::DoSetPosition()
extern "C"  void SetPosition_DoSetPosition_m3465412187 (SetPosition_t2319285817 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vector_10();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_7 = __this->get_space_14();
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		G_B6_0 = L_10;
		goto IL_0055;
	}

IL_004a:
	{
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_localPosition_m668140784(L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
	}

IL_0055:
	{
		V_1 = G_B6_0;
		goto IL_0067;
	}

IL_005b:
	{
		FsmVector3_t533912882 * L_14 = __this->get_vector_10();
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = FsmVector3_get_Value_m2779135117(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_0067:
	{
		FsmFloat_t2134102846 * L_16 = __this->get_x_11();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0089;
		}
	}
	{
		FsmFloat_t2134102846 * L_18 = __this->get_x_11();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_19);
	}

IL_0089:
	{
		FsmFloat_t2134102846 * L_20 = __this->get_y_12();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00ab;
		}
	}
	{
		FsmFloat_t2134102846 * L_22 = __this->get_y_12();
		NullCheck(L_22);
		float L_23 = FsmFloat_get_Value_m4137923823(L_22, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_23);
	}

IL_00ab:
	{
		FsmFloat_t2134102846 * L_24 = __this->get_z_13();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00cd;
		}
	}
	{
		FsmFloat_t2134102846 * L_26 = __this->get_z_13();
		NullCheck(L_26);
		float L_27 = FsmFloat_get_Value_m4137923823(L_26, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_27);
	}

IL_00cd:
	{
		int32_t L_28 = __this->get_space_14();
		if (L_28)
		{
			goto IL_00e9;
		}
	}
	{
		GameObject_t3674682005 * L_29 = V_0;
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = GameObject_get_transform_m1278640159(L_29, /*hidden argument*/NULL);
		Vector3_t4282066566  L_31 = V_1;
		NullCheck(L_30);
		Transform_set_position_m3111394108(L_30, L_31, /*hidden argument*/NULL);
		goto IL_00f5;
	}

IL_00e9:
	{
		GameObject_t3674682005 * L_32 = V_0;
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = V_1;
		NullCheck(L_33);
		Transform_set_localPosition_m3504330903(L_33, L_34, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::.ctor()
extern "C"  void SetProceduralBoolean__ctor_m1968145077 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetProceduralBoolean_Reset_m3909545314_MetadataUsageId;
extern "C"  void SetProceduralBoolean_Reset_m3909545314 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetProceduralBoolean_Reset_m3909545314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_substanceMaterial_9((FsmMaterial_t924399665 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_boolProperty_10(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_boolValue_11(L_2);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::OnEnter()
extern "C"  void SetProceduralBoolean_OnEnter_m993537356 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method)
{
	{
		SetProceduralBoolean_DoSetProceduralFloat_m532204023(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::OnUpdate()
extern "C"  void SetProceduralBoolean_OnUpdate_m4163413623 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method)
{
	{
		SetProceduralBoolean_DoSetProceduralFloat_m532204023(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::DoSetProceduralFloat()
extern "C"  void SetProceduralBoolean_DoSetProceduralFloat_m532204023 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::.ctor()
extern "C"  void SetProceduralColor__ctor_m1187954778 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetProceduralColor_Reset_m3129355015_MetadataUsageId;
extern "C"  void SetProceduralColor_Reset_m3129355015 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetProceduralColor_Reset_m3129355015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_substanceMaterial_9((FsmMaterial_t924399665 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_colorProperty_10(L_1);
		Color_t4194546905  L_2 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_3 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_colorValue_11(L_3);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::OnEnter()
extern "C"  void SetProceduralColor_OnEnter_m2849936817 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method)
{
	{
		SetProceduralColor_DoSetProceduralFloat_m1698426290(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::OnUpdate()
extern "C"  void SetProceduralColor_OnUpdate_m1582254770 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method)
{
	{
		SetProceduralColor_DoSetProceduralFloat_m1698426290(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::DoSetProceduralFloat()
extern "C"  void SetProceduralColor_DoSetProceduralFloat_m1698426290 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::.ctor()
extern "C"  void SetProceduralFloat__ctor_m4228191617 (SetProceduralFloat_t3249302661 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SetProceduralFloat_Reset_m1874624558_MetadataUsageId;
extern "C"  void SetProceduralFloat_Reset_m1874624558 (SetProceduralFloat_t3249302661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetProceduralFloat_Reset_m1874624558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_substanceMaterial_9((FsmMaterial_t924399665 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_floatProperty_10(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_floatValue_11(L_2);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::OnEnter()
extern "C"  void SetProceduralFloat_OnEnter_m3939777816 (SetProceduralFloat_t3249302661 * __this, const MethodInfo* method)
{
	{
		SetProceduralFloat_DoSetProceduralFloat_m2838087083(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::OnUpdate()
extern "C"  void SetProceduralFloat_OnUpdate_m1007587371 (SetProceduralFloat_t3249302661 * __this, const MethodInfo* method)
{
	{
		SetProceduralFloat_DoSetProceduralFloat_m2838087083(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::DoSetProceduralFloat()
extern "C"  void SetProceduralFloat_DoSetProceduralFloat_m2838087083 (SetProceduralFloat_t3249302661 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProperty::.ctor()
extern "C"  void SetProperty__ctor_m421173745 (SetProperty_t578339557 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProperty::Reset()
extern Il2CppClass* FsmProperty_t3927159007_il2cpp_TypeInfo_var;
extern const uint32_t SetProperty_Reset_m2362573982_MetadataUsageId;
extern "C"  void SetProperty_Reset_m2362573982 (SetProperty_t578339557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetProperty_Reset_m2362573982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmProperty_t3927159007 * V_0 = NULL;
	{
		FsmProperty_t3927159007 * L_0 = (FsmProperty_t3927159007 *)il2cpp_codegen_object_new(FsmProperty_t3927159007_il2cpp_TypeInfo_var);
		FsmProperty__ctor_m1857631456(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmProperty_t3927159007 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_setProperty_18((bool)1);
		FsmProperty_t3927159007 * L_2 = V_0;
		__this->set_targetProperty_9(L_2);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProperty::OnEnter()
extern "C"  void SetProperty_OnEnter_m412771720 (SetProperty_t578339557 * __this, const MethodInfo* method)
{
	{
		FsmProperty_t3927159007 * L_0 = __this->get_targetProperty_9();
		NullCheck(L_0);
		FsmProperty_SetValue_m909800787(L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_everyFrame_10();
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetProperty::OnUpdate()
extern "C"  void SetProperty_OnUpdate_m3339548091 (SetProperty_t578339557 * __this, const MethodInfo* method)
{
	{
		FsmProperty_t3927159007 * L_0 = __this->get_targetProperty_9();
		NullCheck(L_0);
		FsmProperty_SetValue_m909800787(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetRandomMaterial__ctor_m3310404124_MetadataUsageId;
extern "C"  void SetRandomMaterial__ctor_m3310404124 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetRandomMaterial__ctor_m3310404124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::Reset()
extern Il2CppClass* FsmMaterialU5BU5D_t1409029100_il2cpp_TypeInfo_var;
extern const uint32_t SetRandomMaterial_Reset_m956837065_MetadataUsageId;
extern "C"  void SetRandomMaterial_Reset_m956837065 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetRandomMaterial_Reset_m956837065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		__this->set_materials_13(((FsmMaterialU5BU5D_t1409029100*)SZArrayNew(FsmMaterialU5BU5D_t1409029100_il2cpp_TypeInfo_var, (uint32_t)3)));
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::OnEnter()
extern "C"  void SetRandomMaterial_OnEnter_m2414292723 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method)
{
	{
		SetRandomMaterial_DoSetRandomMaterial_m3026882043(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::DoSetRandomMaterial()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3480108384;
extern const uint32_t SetRandomMaterial_DoSetRandomMaterial_m3026882043_MetadataUsageId;
extern "C"  void SetRandomMaterial_DoSetRandomMaterial_m3026882043 (SetRandomMaterial_t2485166170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetRandomMaterial_DoSetRandomMaterial_m3026882043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MaterialU5BU5D_t170856778* V_1 = NULL;
	{
		FsmMaterialU5BU5D_t1409029100* L_0 = __this->get_materials_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmMaterialU5BU5D_t1409029100* L_1 = __this->get_materials_13();
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}

IL_001a:
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_11();
		NullCheck(L_2);
		GameObject_t3674682005 * L_4 = Fsm_GetOwnerDefaultTarget_m846013999(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		bool L_6 = ComponentAction_1_UpdateCache_m3595067967(__this, L_5, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_6)
		{
			goto IL_0039;
		}
	}
	{
		return;
	}

IL_0039:
	{
		Renderer_t3076687687 * L_7 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_7);
		Material_t3870600107 * L_8 = Renderer_get_material_m2720864603(L_7, /*hidden argument*/NULL);
		bool L_9 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral3480108384, /*hidden argument*/NULL);
		return;
	}

IL_005b:
	{
		FsmInt_t1596138449 * L_10 = __this->get_materialIndex_12();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0095;
		}
	}
	{
		Renderer_t3076687687 * L_12 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		FsmMaterialU5BU5D_t1409029100* L_13 = __this->get_materials_13();
		FsmMaterialU5BU5D_t1409029100* L_14 = __this->get_materials_13();
		NullCheck(L_14);
		int32_t L_15 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_15);
		int32_t L_16 = L_15;
		FsmMaterial_t924399665 * L_17 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Material_t3870600107 * L_18 = FsmMaterial_get_Value_m1376213207(L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		Renderer_set_material_m1012580896(L_12, L_18, /*hidden argument*/NULL);
		goto IL_00f1;
	}

IL_0095:
	{
		Renderer_t3076687687 * L_19 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_19);
		MaterialU5BU5D_t170856778* L_20 = Renderer_get_materials_m3755041148(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmInt_t1596138449 * L_21 = __this->get_materialIndex_12();
		NullCheck(L_21);
		int32_t L_22 = FsmInt_get_Value_m27059446(L_21, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))) <= ((int32_t)L_22)))
		{
			goto IL_00f1;
		}
	}
	{
		Renderer_t3076687687 * L_23 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_23);
		MaterialU5BU5D_t170856778* L_24 = Renderer_get_materials_m3755041148(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		MaterialU5BU5D_t170856778* L_25 = V_1;
		FsmInt_t1596138449 * L_26 = __this->get_materialIndex_12();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		FsmMaterialU5BU5D_t1409029100* L_28 = __this->get_materials_13();
		FsmMaterialU5BU5D_t1409029100* L_29 = __this->get_materials_13();
		NullCheck(L_29);
		int32_t L_30 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_30);
		int32_t L_31 = L_30;
		FsmMaterial_t924399665 * L_32 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		Material_t3870600107 * L_33 = FsmMaterial_get_Value_m1376213207(L_32, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_27);
		ArrayElementTypeCheck (L_25, L_33);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (Material_t3870600107 *)L_33);
		Renderer_t3076687687 * L_34 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_35 = V_1;
		NullCheck(L_34);
		Renderer_set_materials_m268031319(L_34, L_35, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::.ctor()
extern "C"  void SetRandomRotation__ctor_m289393061 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::Reset()
extern "C"  void SetRandomRotation_Reset_m2230793298 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_x_10(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_y_11(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_z_12(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::OnEnter()
extern "C"  void SetRandomRotation_OnEnter_m2620553276 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method)
{
	{
		SetRandomRotation_DoRandomRotation_m2423224779(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::DoRandomRotation()
extern "C"  void SetRandomRotation_DoRandomRotation_m2423224779 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_localEulerAngles_m3489183428(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_1();
		V_2 = L_8;
		float L_9 = (&V_1)->get_y_2();
		V_3 = L_9;
		float L_10 = (&V_1)->get_z_3();
		V_4 = L_10;
		FsmBool_t1075959796 * L_11 = __this->get_x_10();
		NullCheck(L_11);
		bool L_12 = FsmBool_get_Value_m3101329097(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_13 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		V_2 = (((float)((float)L_13)));
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_14 = __this->get_y_11();
		NullCheck(L_14);
		bool L_15 = FsmBool_get_Value_m3101329097(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_16 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		V_3 = (((float)((float)L_16)));
	}

IL_007e:
	{
		FsmBool_t1075959796 * L_17 = __this->get_z_12();
		NullCheck(L_17);
		bool L_18 = FsmBool_get_Value_m3101329097(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_19 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		V_4 = (((float)((float)L_19)));
	}

IL_009c:
	{
		GameObject_t3674682005 * L_20 = V_0;
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		float L_22 = V_2;
		float L_23 = V_3;
		float L_24 = V_4;
		Vector3_t4282066566  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2926210380(&L_25, L_22, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localEulerAngles_m3898859559(L_21, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::.ctor()
extern "C"  void SetRectFields__ctor_m31209033 (SetRectFields_t2990573965 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetRectFields_Reset_m1972609270_MetadataUsageId;
extern "C"  void SetRectFields_Reset_m1972609270 (SetRectFields_t2990573965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetRectFields_Reset_m1972609270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_rectVariable_9((FsmRect_t1076426478 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_10(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_11(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_width_12(L_8);
		FsmFloat_t2134102846 * L_9 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_9, /*hidden argument*/NULL);
		V_0 = L_9;
		FsmFloat_t2134102846 * L_10 = V_0;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_11 = V_0;
		__this->set_height_13(L_11);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::OnEnter()
extern "C"  void SetRectFields_OnEnter_m3613805536 (SetRectFields_t2990573965 * __this, const MethodInfo* method)
{
	{
		SetRectFields_DoSetRectFields_m2767049691(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::OnUpdate()
extern "C"  void SetRectFields_OnUpdate_m3787348579 (SetRectFields_t2990573965 * __this, const MethodInfo* method)
{
	{
		SetRectFields_DoSetRectFields_m2767049691(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::DoSetRectFields()
extern "C"  void SetRectFields_DoSetRectFields_m2767049691 (SetRectFields_t2990573965 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmRect_t1076426478 * L_0 = __this->get_rectVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmRect_t1076426478 * L_2 = __this->get_rectVariable_9();
		NullCheck(L_2);
		Rect_t4241904616  L_3 = FsmRect_get_Value_m1002500317(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = __this->get_x_10();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		FsmFloat_t2134102846 * L_6 = __this->get_x_10();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		Rect_set_x_m577970569((&V_0), L_7, /*hidden argument*/NULL);
	}

IL_003f:
	{
		FsmFloat_t2134102846 * L_8 = __this->get_y_11();
		NullCheck(L_8);
		bool L_9 = NamedVariable_get_IsNone_m281035543(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0061;
		}
	}
	{
		FsmFloat_t2134102846 * L_10 = __this->get_y_11();
		NullCheck(L_10);
		float L_11 = FsmFloat_get_Value_m4137923823(L_10, /*hidden argument*/NULL);
		Rect_set_y_m67436392((&V_0), L_11, /*hidden argument*/NULL);
	}

IL_0061:
	{
		FsmFloat_t2134102846 * L_12 = __this->get_width_12();
		NullCheck(L_12);
		bool L_13 = NamedVariable_get_IsNone_m281035543(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0083;
		}
	}
	{
		FsmFloat_t2134102846 * L_14 = __this->get_width_12();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		Rect_set_width_m3771513595((&V_0), L_15, /*hidden argument*/NULL);
	}

IL_0083:
	{
		FsmFloat_t2134102846 * L_16 = __this->get_height_13();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00a5;
		}
	}
	{
		FsmFloat_t2134102846 * L_18 = __this->get_height_13();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		Rect_set_height_m3398820332((&V_0), L_19, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		FsmRect_t1076426478 * L_20 = __this->get_rectVariable_9();
		Rect_t4241904616  L_21 = V_0;
		NullCheck(L_20);
		FsmRect_set_Value_m1159518952(L_20, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::.ctor()
extern "C"  void SetRectValue__ctor_m2180960755 (SetRectValue_t1933064659 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::Reset()
extern "C"  void SetRectValue_Reset_m4122360992 (SetRectValue_t1933064659 * __this, const MethodInfo* method)
{
	{
		__this->set_rectVariable_9((FsmRect_t1076426478 *)NULL);
		__this->set_rectValue_10((FsmRect_t1076426478 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::OnEnter()
extern "C"  void SetRectValue_OnEnter_m3645941002 (SetRectValue_t1933064659 * __this, const MethodInfo* method)
{
	{
		FsmRect_t1076426478 * L_0 = __this->get_rectVariable_9();
		FsmRect_t1076426478 * L_1 = __this->get_rectValue_10();
		NullCheck(L_1);
		Rect_t4241904616  L_2 = FsmRect_get_Value_m1002500317(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmRect_set_Value_m1159518952(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::OnUpdate()
extern "C"  void SetRectValue_OnUpdate_m488580729 (SetRectValue_t1933064659 * __this, const MethodInfo* method)
{
	{
		FsmRect_t1076426478 * L_0 = __this->get_rectVariable_9();
		FsmRect_t1076426478 * L_1 = __this->get_rectValue_10();
		NullCheck(L_1);
		Rect_t4241904616  L_2 = FsmRect_get_Value_m1002500317(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmRect_set_Value_m1159518952(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRotation::.ctor()
extern "C"  void SetRotation__ctor_m3889313384 (SetRotation_t1531180174 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRotation::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetRotation_Reset_m1535746325_MetadataUsageId;
extern "C"  void SetRotation_Reset_m1535746325 (SetRotation_t1531180174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetRotation_Reset_m1535746325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_quaternion_10((FsmQuaternion_t3871136040 *)NULL);
		__this->set_vector_11((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_xAngle_12(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_yAngle_13(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_zAngle_14(L_8);
		__this->set_space_15(0);
		__this->set_everyFrame_16((bool)0);
		__this->set_lateUpdate_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnEnter()
extern "C"  void SetRotation_OnEnter_m400343103 (SetRotation_t1531180174 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		bool L_1 = __this->get_lateUpdate_17();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		SetRotation_DoSetRotation_m2520027579(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnUpdate()
extern "C"  void SetRotation_OnUpdate_m2954260964 (SetRotation_t1531180174 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_17();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetRotation_DoSetRotation_m2520027579(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnLateUpdate()
extern "C"  void SetRotation_OnLateUpdate_m2891228586 (SetRotation_t1531180174 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_17();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetRotation_DoSetRotation_m2520027579(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_everyFrame_16();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetRotation::DoSetRotation()
extern "C"  void SetRotation_DoSetRotation_m2520027579 (SetRotation_t1531180174 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t1553702882  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmQuaternion_t3871136040 * L_5 = __this->get_quaternion_10();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_7 = __this->get_quaternion_10();
		NullCheck(L_7);
		Quaternion_t1553702882  L_8 = FsmQuaternion_get_Value_m3393858025(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t4282066566  L_9 = Quaternion_get_eulerAngles_m997303795((&V_2), /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0091;
	}

IL_0048:
	{
		FsmVector3_t533912882 * L_10 = __this->get_vector_11();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0069;
		}
	}
	{
		FsmVector3_t533912882 * L_12 = __this->get_vector_11();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = FsmVector3_get_Value_m2779135117(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		goto IL_0091;
	}

IL_0069:
	{
		int32_t L_14 = __this->get_space_15();
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t3674682005 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_localEulerAngles_m3489183428(L_16, /*hidden argument*/NULL);
		G_B9_0 = L_17;
		goto IL_0090;
	}

IL_0085:
	{
		GameObject_t3674682005 * L_18 = V_0;
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_eulerAngles_m1058084741(L_19, /*hidden argument*/NULL);
		G_B9_0 = L_20;
	}

IL_0090:
	{
		V_1 = G_B9_0;
	}

IL_0091:
	{
		FsmFloat_t2134102846 * L_21 = __this->get_xAngle_12();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00b3;
		}
	}
	{
		FsmFloat_t2134102846 * L_23 = __this->get_xAngle_12();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_24);
	}

IL_00b3:
	{
		FsmFloat_t2134102846 * L_25 = __this->get_yAngle_13();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00d5;
		}
	}
	{
		FsmFloat_t2134102846 * L_27 = __this->get_yAngle_13();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_28);
	}

IL_00d5:
	{
		FsmFloat_t2134102846 * L_29 = __this->get_zAngle_14();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00f7;
		}
	}
	{
		FsmFloat_t2134102846 * L_31 = __this->get_zAngle_14();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_32);
	}

IL_00f7:
	{
		int32_t L_33 = __this->get_space_15();
		if ((!(((uint32_t)L_33) == ((uint32_t)1))))
		{
			goto IL_0114;
		}
	}
	{
		GameObject_t3674682005 * L_34 = V_0;
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = GameObject_get_transform_m1278640159(L_34, /*hidden argument*/NULL);
		Vector3_t4282066566  L_36 = V_1;
		NullCheck(L_35);
		Transform_set_localEulerAngles_m3898859559(L_35, L_36, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_0114:
	{
		GameObject_t3674682005 * L_37 = V_0;
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = GameObject_get_transform_m1278640159(L_37, /*hidden argument*/NULL);
		Vector3_t4282066566  L_39 = V_1;
		NullCheck(L_38);
		Transform_set_eulerAngles_m1704681314(L_38, L_39, /*hidden argument*/NULL);
	}

IL_0120:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetScale::.ctor()
extern "C"  void SetScale__ctor_m1058667958 (SetScale_t1151369904 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetScale::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetScale_Reset_m3000068195_MetadataUsageId;
extern "C"  void SetScale_Reset_m3000068195 (SetScale_t1151369904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetScale_Reset_m3000068195_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_10((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_11(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_12(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_z_13(L_8);
		__this->set_everyFrame_14((bool)0);
		__this->set_lateUpdate_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnEnter()
extern "C"  void SetScale_OnEnter_m3159354381 (SetScale_t1151369904 * __this, const MethodInfo* method)
{
	{
		SetScale_DoSetScale_m2952245761(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnUpdate()
extern "C"  void SetScale_OnUpdate_m2584264662 (SetScale_t1151369904 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_15();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetScale_DoSetScale_m2952245761(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnLateUpdate()
extern "C"  void SetScale_OnLateUpdate_m2544544412 (SetScale_t1151369904 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_15();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetScale_DoSetScale_m2952245761(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_everyFrame_14();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetScale::DoSetScale()
extern "C"  void SetScale_DoSetScale_m2952245761 (SetScale_t1151369904 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vector_10();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = GameObject_get_transform_m1278640159(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_localScale_m3886572677(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
		goto IL_004a;
	}

IL_003f:
	{
		FsmVector3_t533912882 * L_10 = __this->get_vector_10();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = FsmVector3_get_Value_m2779135117(L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
	}

IL_004a:
	{
		V_1 = G_B5_0;
		FsmFloat_t2134102846 * L_12 = __this->get_x_11();
		NullCheck(L_12);
		bool L_13 = NamedVariable_get_IsNone_m281035543(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_006d;
		}
	}
	{
		FsmFloat_t2134102846 * L_14 = __this->get_x_11();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_15);
	}

IL_006d:
	{
		FsmFloat_t2134102846 * L_16 = __this->get_y_12();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_008f;
		}
	}
	{
		FsmFloat_t2134102846 * L_18 = __this->get_y_12();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_19);
	}

IL_008f:
	{
		FsmFloat_t2134102846 * L_20 = __this->get_z_13();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00b1;
		}
	}
	{
		FsmFloat_t2134102846 * L_22 = __this->get_z_13();
		NullCheck(L_22);
		float L_23 = FsmFloat_get_Value_m4137923823(L_22, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_23);
	}

IL_00b1:
	{
		GameObject_t3674682005 * L_24 = V_0;
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = GameObject_get_transform_m1278640159(L_24, /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = V_1;
		NullCheck(L_25);
		Transform_set_localScale_m310756934(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2899772967_MethodInfo_var;
extern const uint32_t SetShadowStrength__ctor_m664061317_MetadataUsageId;
extern "C"  void SetShadowStrength__ctor_m664061317 (SetShadowStrength_t3140252369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetShadowStrength__ctor_m664061317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2899772967(__this, /*hidden argument*/ComponentAction_1__ctor_m2899772967_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::Reset()
extern "C"  void SetShadowStrength_Reset_m2605461554 (SetShadowStrength_t3140252369 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.8f), /*hidden argument*/NULL);
		__this->set_shadowStrength_12(L_0);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::OnEnter()
extern "C"  void SetShadowStrength_OnEnter_m1899494428 (SetShadowStrength_t3140252369 * __this, const MethodInfo* method)
{
	{
		SetShadowStrength_DoSetShadowStrength_m3494704731(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::OnUpdate()
extern "C"  void SetShadowStrength_OnUpdate_m2183311783 (SetShadowStrength_t3140252369 * __this, const MethodInfo* method)
{
	{
		SetShadowStrength_DoSetShadowStrength_m3494704731(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::DoSetShadowStrength()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_light_m401565086_MethodInfo_var;
extern const uint32_t SetShadowStrength_DoSetShadowStrength_m3494704731_MetadataUsageId;
extern "C"  void SetShadowStrength_DoSetShadowStrength_m3494704731 (SetShadowStrength_t3140252369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetShadowStrength_DoSetShadowStrength_m3494704731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3906958850(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3906958850_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Light_t4202674828 * L_5 = ComponentAction_1_get_light_m401565086(__this, /*hidden argument*/ComponentAction_1_get_light_m401565086_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_shadowStrength_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Light_set_shadowStrength_m561788748(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::.ctor()
extern "C"  void SetSkybox__ctor_m2337025820 (SetSkybox_t691862362 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::Reset()
extern "C"  void SetSkybox_Reset_m4278426057 (SetSkybox_t691862362 * __this, const MethodInfo* method)
{
	{
		__this->set_skybox_9((FsmMaterial_t924399665 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::OnEnter()
extern "C"  void SetSkybox_OnEnter_m3300613107 (SetSkybox_t691862362 * __this, const MethodInfo* method)
{
	{
		FsmMaterial_t924399665 * L_0 = __this->get_skybox_9();
		NullCheck(L_0);
		Material_t3870600107 * L_1 = FsmMaterial_get_Value_m1376213207(L_0, /*hidden argument*/NULL);
		RenderSettings_set_skybox_m3777670233(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_everyFrame_10();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::OnUpdate()
extern "C"  void SetSkybox_OnUpdate_m2668317872 (SetSkybox_t691862362 * __this, const MethodInfo* method)
{
	{
		FsmMaterial_t924399665 * L_0 = __this->get_skybox_9();
		NullCheck(L_0);
		Material_t3870600107 * L_1 = FsmMaterial_get_Value_m1376213207(L_0, /*hidden argument*/NULL);
		RenderSettings_set_skybox_m3777670233(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::.ctor()
extern "C"  void SetStringValue__ctor_m2295319072 (SetStringValue_t3445720326 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::Reset()
extern "C"  void SetStringValue_Reset_m4236719309 (SetStringValue_t3445720326 * __this, const MethodInfo* method)
{
	{
		__this->set_stringVariable_9((FsmString_t952858651 *)NULL);
		__this->set_stringValue_10((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::OnEnter()
extern "C"  void SetStringValue_OnEnter_m1875133943 (SetStringValue_t3445720326 * __this, const MethodInfo* method)
{
	{
		SetStringValue_DoSetStringValue_m4016712685(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::OnUpdate()
extern "C"  void SetStringValue_OnUpdate_m1428136748 (SetStringValue_t3445720326 * __this, const MethodInfo* method)
{
	{
		SetStringValue_DoSetStringValue_m4016712685(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::DoSetStringValue()
extern "C"  void SetStringValue_DoSetStringValue_m4016712685 (SetStringValue_t3445720326 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_9();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmString_t952858651 * L_1 = __this->get_stringValue_10();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		FsmString_t952858651 * L_2 = __this->get_stringVariable_9();
		FsmString_t952858651 * L_3 = __this->get_stringValue_10();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmString_set_Value_m829393196(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTag::.ctor()
extern "C"  void SetTag__ctor_m565644454 (SetTag_t3330067392 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTag::Reset()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t SetTag_Reset_m2507044691_MetadataUsageId;
extern "C"  void SetTag_Reset_m2507044691 (SetTag_t3330067392 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTag_Reset_m2507044691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_tag_10(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTag::OnEnter()
extern "C"  void SetTag_OnEnter_m1810169597 (SetTag_t3330067392 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		FsmString_t952858651 * L_6 = __this->get_tag_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_set_tag_m859036203(L_5, L_7, /*hidden argument*/NULL);
	}

IL_002f:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::.ctor()
extern "C"  void SetTagsOnChildren__ctor_m1853747663 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::Reset()
extern "C"  void SetTagsOnChildren_Reset_m3795147900 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_tag_10((FsmString_t952858651 *)NULL);
		__this->set_filterByComponent_11((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::OnEnter()
extern "C"  void SetTagsOnChildren_OnEnter_m2726772198 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		SetTagsOnChildren_SetTag_m639115813(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::SetTag(UnityEngine.GameObject)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t1659122786_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t SetTagsOnChildren_SetTag_m639115813_MetadataUsageId;
extern "C"  void SetTagsOnChildren_SetTag_m639115813 (SetTagsOnChildren_t932473671 * __this, GameObject_t3674682005 * ___parent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTagsOnChildren_SetTag_m639115813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ComponentU5BU5D_t663911650* V_2 = NULL;
	Component_t3501516275 * V_3 = NULL;
	ComponentU5BU5D_t663911650* V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t3674682005 * L_0 = ___parent0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		FsmString_t952858651 * L_2 = __this->get_filterByComponent_11();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t3674682005 * L_5 = ___parent0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Il2CppObject * L_7 = Transform_GetEnumerator_m688365631(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0055;
		}

IL_0033:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_8);
			V_0 = ((Transform_t1659122786 *)CastclassClass(L_9, Transform_t1659122786_il2cpp_TypeInfo_var));
			Transform_t1659122786 * L_10 = V_0;
			NullCheck(L_10);
			GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
			FsmString_t952858651 * L_12 = __this->get_tag_10();
			NullCheck(L_12);
			String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
			NullCheck(L_11);
			GameObject_set_tag_m859036203(L_11, L_13, /*hidden argument*/NULL);
		}

IL_0055:
		{
			Il2CppObject * L_14 = V_1;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0033;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x7A, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_1;
			V_6 = ((Il2CppObject *)IsInst(L_16, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_17 = V_6;
			if (L_17)
			{
				goto IL_0072;
			}
		}

IL_0071:
		{
			IL2CPP_END_FINALLY(101)
		}

IL_0072:
		{
			Il2CppObject * L_18 = V_6;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(101)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_007a:
	{
		goto IL_00d5;
	}

IL_007f:
	{
		SetTagsOnChildren_UpdateComponentFilter_m2711655097(__this, /*hidden argument*/NULL);
		Type_t * L_19 = __this->get_componentFilter_12();
		if (!L_19)
		{
			goto IL_00d5;
		}
	}
	{
		GameObject_t3674682005 * L_20 = ___parent0;
		Type_t * L_21 = __this->get_componentFilter_12();
		NullCheck(L_20);
		ComponentU5BU5D_t663911650* L_22 = GameObject_GetComponentsInChildren_m2801747258(L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		ComponentU5BU5D_t663911650* L_23 = V_2;
		V_4 = L_23;
		V_5 = 0;
		goto IL_00ca;
	}

IL_00a8:
	{
		ComponentU5BU5D_t663911650* L_24 = V_4;
		int32_t L_25 = V_5;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		Component_t3501516275 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		V_3 = L_27;
		Component_t3501516275 * L_28 = V_3;
		NullCheck(L_28);
		GameObject_t3674682005 * L_29 = Component_get_gameObject_m1170635899(L_28, /*hidden argument*/NULL);
		FsmString_t952858651 * L_30 = __this->get_tag_10();
		NullCheck(L_30);
		String_t* L_31 = FsmString_get_Value_m872383149(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		GameObject_set_tag_m859036203(L_29, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_5;
		V_5 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_33 = V_5;
		ComponentU5BU5D_t663911650* L_34 = V_4;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_00a8;
		}
	}

IL_00d5:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::UpdateComponentFilter()
extern Il2CppClass* ReflectionUtils_t1202855664_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral108037399;
extern Il2CppCodeGenString* _stringLiteral4030584304;
extern const uint32_t SetTagsOnChildren_UpdateComponentFilter_m2711655097_MetadataUsageId;
extern "C"  void SetTagsOnChildren_UpdateComponentFilter_m2711655097 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTagsOnChildren_UpdateComponentFilter_m2711655097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_filterByComponent_11();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t1202855664_il2cpp_TypeInfo_var);
		Type_t * L_2 = ReflectionUtils_GetGlobalType_m3454401768(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_componentFilter_12(L_2);
		Type_t * L_3 = __this->get_componentFilter_12();
		if (L_3)
		{
			goto IL_0041;
		}
	}
	{
		FsmString_t952858651 * L_4 = __this->get_filterByComponent_11();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral108037399, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t1202855664_il2cpp_TypeInfo_var);
		Type_t * L_7 = ReflectionUtils_GetGlobalType_m3454401768(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_componentFilter_12(L_7);
	}

IL_0041:
	{
		Type_t * L_8 = __this->get_componentFilter_12();
		if (L_8)
		{
			goto IL_0066;
		}
	}
	{
		FsmString_t952858651 * L_9 = __this->get_filterByComponent_11();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral4030584304, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetTextureOffset__ctor_m2784190706_MetadataUsageId;
extern "C"  void SetTextureOffset__ctor_m2784190706 (SetTextureOffset_t467579892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTextureOffset__ctor_m2784190706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::Reset()
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t SetTextureOffset_Reset_m430623647_MetadataUsageId;
extern "C"  void SetTextureOffset_Reset_m430623647 (SetTextureOffset_t467579892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTextureOffset_Reset_m430623647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral558922319, /*hidden argument*/NULL);
		__this->set_namedTexture_13(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_offsetX_14(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_offsetY_15(L_3);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::OnEnter()
extern "C"  void SetTextureOffset_OnEnter_m3529338953 (SetTextureOffset_t467579892 * __this, const MethodInfo* method)
{
	{
		SetTextureOffset_DoSetTextureOffset_m879326601(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::OnUpdate()
extern "C"  void SetTextureOffset_OnUpdate_m1168884506 (SetTextureOffset_t467579892 * __this, const MethodInfo* method)
{
	{
		SetTextureOffset_DoSetTextureOffset_m879326601(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::DoSetTextureOffset()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3480108384;
extern const uint32_t SetTextureOffset_DoSetTextureOffset_m879326601_MetadataUsageId;
extern "C"  void SetTextureOffset_DoSetTextureOffset_m879326601 (SetTextureOffset_t467579892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTextureOffset_DoSetTextureOffset_m879326601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MaterialU5BU5D_t170856778* V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Renderer_t3076687687 * L_5 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_5);
		Material_t3870600107 * L_6 = Renderer_get_material_m2720864603(L_5, /*hidden argument*/NULL);
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral3480108384, /*hidden argument*/NULL);
		return;
	}

IL_0041:
	{
		FsmInt_t1596138449 * L_8 = __this->get_materialIndex_12();
		NullCheck(L_8);
		int32_t L_9 = FsmInt_get_Value_m27059446(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_008c;
		}
	}
	{
		Renderer_t3076687687 * L_10 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_10);
		Material_t3870600107 * L_11 = Renderer_get_material_m2720864603(L_10, /*hidden argument*/NULL);
		FsmString_t952858651 * L_12 = __this->get_namedTexture_13();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_offsetX_14();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_offsetY_15();
		NullCheck(L_16);
		float L_17 = FsmFloat_get_Value_m4137923823(L_16, /*hidden argument*/NULL);
		Vector2_t4282066565  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m1517109030(&L_18, L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetTextureOffset_m1301408396(L_11, L_13, L_18, /*hidden argument*/NULL);
		goto IL_00f9;
	}

IL_008c:
	{
		Renderer_t3076687687 * L_19 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_19);
		MaterialU5BU5D_t170856778* L_20 = Renderer_get_materials_m3755041148(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmInt_t1596138449 * L_21 = __this->get_materialIndex_12();
		NullCheck(L_21);
		int32_t L_22 = FsmInt_get_Value_m27059446(L_21, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))) <= ((int32_t)L_22)))
		{
			goto IL_00f9;
		}
	}
	{
		Renderer_t3076687687 * L_23 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_23);
		MaterialU5BU5D_t170856778* L_24 = Renderer_get_materials_m3755041148(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		MaterialU5BU5D_t170856778* L_25 = V_1;
		FsmInt_t1596138449 * L_26 = __this->get_materialIndex_12();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_27);
		int32_t L_28 = L_27;
		Material_t3870600107 * L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		FsmString_t952858651 * L_30 = __this->get_namedTexture_13();
		NullCheck(L_30);
		String_t* L_31 = FsmString_get_Value_m872383149(L_30, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_32 = __this->get_offsetX_14();
		NullCheck(L_32);
		float L_33 = FsmFloat_get_Value_m4137923823(L_32, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_34 = __this->get_offsetY_15();
		NullCheck(L_34);
		float L_35 = FsmFloat_get_Value_m4137923823(L_34, /*hidden argument*/NULL);
		Vector2_t4282066565  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector2__ctor_m1517109030(&L_36, L_33, L_35, /*hidden argument*/NULL);
		NullCheck(L_29);
		Material_SetTextureOffset_m1301408396(L_29, L_31, L_36, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_37 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_38 = V_1;
		NullCheck(L_37);
		Renderer_set_materials_m268031319(L_37, L_38, /*hidden argument*/NULL);
	}

IL_00f9:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetTextureScale__ctor_m191893143_MetadataUsageId;
extern "C"  void SetTextureScale__ctor_m191893143 (SetTextureScale_t1425090431 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTextureScale__ctor_m191893143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::Reset()
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t SetTextureScale_Reset_m2133293380_MetadataUsageId;
extern "C"  void SetTextureScale_Reset_m2133293380 (SetTextureScale_t1425090431 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTextureScale_Reset_m2133293380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_12(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral558922319, /*hidden argument*/NULL);
		__this->set_namedTexture_13(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_scaleX_14(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_scaleY_15(L_3);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::OnEnter()
extern "C"  void SetTextureScale_OnEnter_m3412412590 (SetTextureScale_t1425090431 * __this, const MethodInfo* method)
{
	{
		SetTextureScale_DoSetTextureScale_m3882316443(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::OnUpdate()
extern "C"  void SetTextureScale_OnUpdate_m1839134549 (SetTextureScale_t1425090431 * __this, const MethodInfo* method)
{
	{
		SetTextureScale_DoSetTextureScale_m3882316443(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::DoSetTextureScale()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3480108384;
extern const uint32_t SetTextureScale_DoSetTextureScale_m3882316443_MetadataUsageId;
extern "C"  void SetTextureScale_DoSetTextureScale_m3882316443 (SetTextureScale_t1425090431 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetTextureScale_DoSetTextureScale_m3882316443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MaterialU5BU5D_t170856778* V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Renderer_t3076687687 * L_5 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_5);
		Material_t3870600107 * L_6 = Renderer_get_material_m2720864603(L_5, /*hidden argument*/NULL);
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral3480108384, /*hidden argument*/NULL);
		return;
	}

IL_0041:
	{
		FsmInt_t1596138449 * L_8 = __this->get_materialIndex_12();
		NullCheck(L_8);
		int32_t L_9 = FsmInt_get_Value_m27059446(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_008c;
		}
	}
	{
		Renderer_t3076687687 * L_10 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_10);
		Material_t3870600107 * L_11 = Renderer_get_material_m2720864603(L_10, /*hidden argument*/NULL);
		FsmString_t952858651 * L_12 = __this->get_namedTexture_13();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_scaleX_14();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_scaleY_15();
		NullCheck(L_16);
		float L_17 = FsmFloat_get_Value_m4137923823(L_16, /*hidden argument*/NULL);
		Vector2_t4282066565  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m1517109030(&L_18, L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetTextureScale_m1752758881(L_11, L_13, L_18, /*hidden argument*/NULL);
		goto IL_00f9;
	}

IL_008c:
	{
		Renderer_t3076687687 * L_19 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_19);
		MaterialU5BU5D_t170856778* L_20 = Renderer_get_materials_m3755041148(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmInt_t1596138449 * L_21 = __this->get_materialIndex_12();
		NullCheck(L_21);
		int32_t L_22 = FsmInt_get_Value_m27059446(L_21, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))) <= ((int32_t)L_22)))
		{
			goto IL_00f9;
		}
	}
	{
		Renderer_t3076687687 * L_23 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_23);
		MaterialU5BU5D_t170856778* L_24 = Renderer_get_materials_m3755041148(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		MaterialU5BU5D_t170856778* L_25 = V_1;
		FsmInt_t1596138449 * L_26 = __this->get_materialIndex_12();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_27);
		int32_t L_28 = L_27;
		Material_t3870600107 * L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		FsmString_t952858651 * L_30 = __this->get_namedTexture_13();
		NullCheck(L_30);
		String_t* L_31 = FsmString_get_Value_m872383149(L_30, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_32 = __this->get_scaleX_14();
		NullCheck(L_32);
		float L_33 = FsmFloat_get_Value_m4137923823(L_32, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_34 = __this->get_scaleY_15();
		NullCheck(L_34);
		float L_35 = FsmFloat_get_Value_m4137923823(L_34, /*hidden argument*/NULL);
		Vector2_t4282066565  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector2__ctor_m1517109030(&L_36, L_33, L_35, /*hidden argument*/NULL);
		NullCheck(L_29);
		Material_SetTextureScale_m1752758881(L_29, L_31, L_36, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_37 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_38 = V_1;
		NullCheck(L_37);
		Renderer_set_materials_m268031319(L_37, L_38, /*hidden argument*/NULL);
	}

IL_00f9:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::.ctor()
extern "C"  void SetVector3Value__ctor_m772912069 (SetVector3Value_t2514376337 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::Reset()
extern "C"  void SetVector3Value_Reset_m2714312306 (SetVector3Value_t2514376337 * __this, const MethodInfo* method)
{
	{
		__this->set_vector3Variable_9((FsmVector3_t533912882 *)NULL);
		__this->set_vector3Value_10((FsmVector3_t533912882 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::OnEnter()
extern "C"  void SetVector3Value_OnEnter_m3425851996 (SetVector3Value_t2514376337 * __this, const MethodInfo* method)
{
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector3Variable_9();
		FsmVector3_t533912882 * L_1 = __this->get_vector3Value_10();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = FsmVector3_get_Value_m2779135117(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmVector3_set_Value_m716982822(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_11();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::OnUpdate()
extern "C"  void SetVector3Value_OnUpdate_m2255756135 (SetVector3Value_t2514376337 * __this, const MethodInfo* method)
{
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector3Variable_9();
		FsmVector3_t533912882 * L_1 = __this->get_vector3Value_10();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = FsmVector3_get_Value_m2779135117(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmVector3_set_Value_m716982822(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::.ctor()
extern "C"  void SetVector3XYZ__ctor_m486403421 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetVector3XYZ_Reset_m2427803658_MetadataUsageId;
extern "C"  void SetVector3XYZ_Reset_m2427803658 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVector3XYZ_Reset_m2427803658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_vector3Variable_9((FsmVector3_t533912882 *)NULL);
		__this->set_vector3Value_10((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_11(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_12(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_z_13(L_8);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::OnEnter()
extern "C"  void SetVector3XYZ_OnEnter_m2968948212 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method)
{
	{
		SetVector3XYZ_DoSetVector3XYZ_m1878137179(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::OnUpdate()
extern "C"  void SetVector3XYZ_OnUpdate_m976640719 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method)
{
	{
		SetVector3XYZ_DoSetVector3XYZ_m1878137179(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::DoSetVector3XYZ()
extern "C"  void SetVector3XYZ_DoSetVector3XYZ_m1878137179 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector3Variable_9();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmVector3_t533912882 * L_1 = __this->get_vector3Variable_9();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = FsmVector3_get_Value_m2779135117(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmVector3_t533912882 * L_3 = __this->get_vector3Value_10();
		NullCheck(L_3);
		bool L_4 = NamedVariable_get_IsNone_m281035543(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		FsmVector3_t533912882 * L_5 = __this->get_vector3Value_10();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = FsmVector3_get_Value_m2779135117(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0034:
	{
		FsmFloat_t2134102846 * L_7 = __this->get_x_11();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0056;
		}
	}
	{
		FsmFloat_t2134102846 * L_9 = __this->get_x_11();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_10);
	}

IL_0056:
	{
		FsmFloat_t2134102846 * L_11 = __this->get_y_12();
		NullCheck(L_11);
		bool L_12 = NamedVariable_get_IsNone_m281035543(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0078;
		}
	}
	{
		FsmFloat_t2134102846 * L_13 = __this->get_y_12();
		NullCheck(L_13);
		float L_14 = FsmFloat_get_Value_m4137923823(L_13, /*hidden argument*/NULL);
		(&V_0)->set_y_2(L_14);
	}

IL_0078:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_z_13();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_009a;
		}
	}
	{
		FsmFloat_t2134102846 * L_17 = __this->get_z_13();
		NullCheck(L_17);
		float L_18 = FsmFloat_get_Value_m4137923823(L_17, /*hidden argument*/NULL);
		(&V_0)->set_z_3(L_18);
	}

IL_009a:
	{
		FsmVector3_t533912882 * L_19 = __this->get_vector3Variable_9();
		Vector3_t4282066566  L_20 = V_0;
		NullCheck(L_19);
		FsmVector3_set_Value_m716982822(L_19, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t SetVelocity__ctor_m1996006377_MetadataUsageId;
extern "C"  void SetVelocity__ctor_m1996006377 (SetVelocity_t3705741805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVelocity__ctor_m1996006377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t SetVelocity_Reset_m3937406614_MetadataUsageId;
extern "C"  void SetVelocity_Reset_m3937406614 (SetVelocity_t3705741805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVelocity_Reset_m3937406614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_12((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_13(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_14(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_z_15(L_8);
		__this->set_space_16(1);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::Awake()
extern "C"  void SetVelocity_Awake_m2233611596 (SetVelocity_t3705741805 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnEnter()
extern "C"  void SetVelocity_OnEnter_m1998442880 (SetVelocity_t3705741805 * __this, const MethodInfo* method)
{
	{
		SetVelocity_DoSetVelocity_m136396763(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnFixedUpdate()
extern "C"  void SetVelocity_OnFixedUpdate_m4116400517 (SetVelocity_t3705741805 * __this, const MethodInfo* method)
{
	{
		SetVelocity_DoSetVelocity_m136396763(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::DoSetVelocity()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t SetVelocity_DoSetVelocity_m136396763_MetadataUsageId;
extern "C"  void SetVelocity_DoSetVelocity_m136396763 (SetVelocity_t3705741805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVelocity_DoSetVelocity_m136396763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	Rigidbody_t3346577219 * G_B16_0 = NULL;
	Rigidbody_t3346577219 * G_B15_0 = NULL;
	Vector3_t4282066566  G_B17_0;
	memset(&G_B17_0, 0, sizeof(G_B17_0));
	Rigidbody_t3346577219 * G_B17_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vector_12();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_7 = __this->get_space_16();
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		Rigidbody_t3346577219 * L_8 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Rigidbody_get_velocity_m2696244068(L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0060;
	}

IL_004a:
	{
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		Rigidbody_t3346577219 * L_12 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Rigidbody_get_velocity_m2696244068(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_14 = Transform_InverseTransformDirection_m416562129(L_11, L_13, /*hidden argument*/NULL);
		G_B6_0 = L_14;
	}

IL_0060:
	{
		V_1 = G_B6_0;
		goto IL_0072;
	}

IL_0066:
	{
		FsmVector3_t533912882 * L_15 = __this->get_vector_12();
		NullCheck(L_15);
		Vector3_t4282066566  L_16 = FsmVector3_get_Value_m2779135117(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
	}

IL_0072:
	{
		FsmFloat_t2134102846 * L_17 = __this->get_x_13();
		NullCheck(L_17);
		bool L_18 = NamedVariable_get_IsNone_m281035543(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0094;
		}
	}
	{
		FsmFloat_t2134102846 * L_19 = __this->get_x_13();
		NullCheck(L_19);
		float L_20 = FsmFloat_get_Value_m4137923823(L_19, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_20);
	}

IL_0094:
	{
		FsmFloat_t2134102846 * L_21 = __this->get_y_14();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00b6;
		}
	}
	{
		FsmFloat_t2134102846 * L_23 = __this->get_y_14();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_24);
	}

IL_00b6:
	{
		FsmFloat_t2134102846 * L_25 = __this->get_z_15();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00d8;
		}
	}
	{
		FsmFloat_t2134102846 * L_27 = __this->get_z_15();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_28);
	}

IL_00d8:
	{
		Rigidbody_t3346577219 * L_29 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		int32_t L_30 = __this->get_space_16();
		G_B15_0 = L_29;
		if (L_30)
		{
			G_B16_0 = L_29;
			goto IL_00ef;
		}
	}
	{
		Vector3_t4282066566  L_31 = V_1;
		G_B17_0 = L_31;
		G_B17_1 = G_B15_0;
		goto IL_00fb;
	}

IL_00ef:
	{
		GameObject_t3674682005 * L_32 = V_0;
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = V_1;
		NullCheck(L_33);
		Vector3_t4282066566  L_35 = Transform_TransformDirection_m83001769(L_33, L_34, /*hidden argument*/NULL);
		G_B17_0 = L_35;
		G_B17_1 = G_B16_0;
	}

IL_00fb:
	{
		NullCheck(G_B17_1);
		Rigidbody_set_velocity_m799562119(G_B17_1, G_B17_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t SetVisibility__ctor_m4004138132_MetadataUsageId;
extern "C"  void SetVisibility__ctor_m4004138132 (SetVisibility_t1014047458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVisibility__ctor_m4004138132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::Reset()
extern "C"  void SetVisibility_Reset_m1650571073 (SetVisibility_t1014047458 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_toggle_12(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_visible_13(L_1);
		__this->set_resetOnExit_14((bool)1);
		__this->set_initialVisibility_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::OnEnter()
extern "C"  void SetVisibility_OnEnter_m3372743531 (SetVisibility_t1014047458 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		SetVisibility_DoSetVisibility_m3730534835(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::DoSetVisibility(UnityEngine.GameObject)
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t SetVisibility_DoSetVisibility_m3730534835_MetadataUsageId;
extern "C"  void SetVisibility_DoSetVisibility_m3730534835 (SetVisibility_t1014047458 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVisibility_DoSetVisibility_m3730534835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = ComponentAction_1_UpdateCache_m3595067967(__this, L_0, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Renderer_t3076687687 * L_2 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = Renderer_get_enabled_m1971819706(L_2, /*hidden argument*/NULL);
		__this->set_initialVisibility_15(L_3);
		FsmBool_t1075959796 * L_4 = __this->get_toggle_12();
		NullCheck(L_4);
		bool L_5 = FsmBool_get_Value_m3101329097(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0045;
		}
	}
	{
		Renderer_t3076687687 * L_6 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		FsmBool_t1075959796 * L_7 = __this->get_visible_13();
		NullCheck(L_7);
		bool L_8 = FsmBool_get_Value_m3101329097(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Renderer_set_enabled_m2514140131(L_6, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0045:
	{
		Renderer_t3076687687 * L_9 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		Renderer_t3076687687 * L_10 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_10);
		bool L_11 = Renderer_get_enabled_m1971819706(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Renderer_set_enabled_m2514140131(L_9, (bool)((((int32_t)L_11) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::OnExit()
extern "C"  void SetVisibility_OnExit_m1087549133 (SetVisibility_t1014047458 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_resetOnExit_14();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SetVisibility_ResetVisibility_m1141711347(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::ResetVisibility()
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t SetVisibility_ResetVisibility_m1141711347_MetadataUsageId;
extern "C"  void SetVisibility_ResetVisibility_m1141711347 (SetVisibility_t1014047458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetVisibility_ResetVisibility_m1141711347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t3076687687 * L_0 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Renderer_t3076687687 * L_2 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		bool L_3 = __this->get_initialVisibility_15();
		NullCheck(L_2);
		Renderer_set_enabled_m2514140131(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Sleep::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t Sleep__ctor_m1499252529_MetadataUsageId;
extern "C"  void Sleep__ctor_m1499252529 (Sleep_t1652571045 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sleep__ctor_m1499252529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Sleep::Reset()
extern "C"  void Sleep_Reset_m3440652766 (Sleep_t1652571045 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Sleep::OnEnter()
extern "C"  void Sleep_OnEnter_m1359364808 (Sleep_t1652571045 * __this, const MethodInfo* method)
{
	{
		Sleep_DoSleep_m978374363(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Sleep::DoSleep()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t Sleep_DoSleep_m978374363_MetadataUsageId;
extern "C"  void Sleep_DoSleep_m978374363 (Sleep_t1652571045 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sleep_DoSleep_m978374363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_5);
		Rigidbody_Sleep_m4049131361(L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothFollowAction::.ctor()
extern "C"  void SmoothFollowAction__ctor_m2743215753 (SmoothFollowAction_t1760465533 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothFollowAction::Reset()
extern "C"  void SmoothFollowAction_Reset_m389648694 (SmoothFollowAction_t1760465533 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetObject_10((FsmGameObject_t1697147867 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		__this->set_distance_11(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		__this->set_height_12(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (2.0f), /*hidden argument*/NULL);
		__this->set_heightDamping_13(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (3.0f), /*hidden argument*/NULL);
		__this->set_rotationDamping_14(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothFollowAction::OnLateUpdate()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SmoothFollowAction_OnLateUpdate_m3151855209_MetadataUsageId;
extern "C"  void SmoothFollowAction_OnLateUpdate_m3151855209 (SmoothFollowAction_t1760465533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothFollowAction_OnLateUpdate_m3151855209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Quaternion_t1553702882  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_targetObject_10();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_4 = __this->get_gameObject_9();
		NullCheck(L_3);
		GameObject_t3674682005 * L_5 = Fsm_GetOwnerDefaultTarget_m846013999(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t3674682005 * L_6 = V_0;
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		GameObject_t3674682005 * L_8 = __this->get_cachedObect_15();
		GameObject_t3674682005 * L_9 = V_0;
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		GameObject_t3674682005 * L_11 = V_0;
		__this->set_cachedObect_15(L_11);
		GameObject_t3674682005 * L_12 = V_0;
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		__this->set_myTransform_16(L_13);
		FsmGameObject_t1697147867 * L_14 = __this->get_targetObject_10();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = FsmGameObject_get_Value_m673294275(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		__this->set_targetTransform_17(L_16);
	}

IL_0070:
	{
		Transform_t1659122786 * L_17 = __this->get_targetTransform_17();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_eulerAngles_m1058084741(L_17, /*hidden argument*/NULL);
		V_6 = L_18;
		float L_19 = (&V_6)->get_y_2();
		V_1 = L_19;
		Transform_t1659122786 * L_20 = __this->get_targetTransform_17();
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = (&V_7)->get_y_2();
		FsmFloat_t2134102846 * L_23 = __this->get_height_12();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_22+(float)L_24));
		Transform_t1659122786 * L_25 = __this->get_myTransform_16();
		NullCheck(L_25);
		Vector3_t4282066566  L_26 = Transform_get_eulerAngles_m1058084741(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		float L_27 = (&V_8)->get_y_2();
		V_3 = L_27;
		Transform_t1659122786 * L_28 = __this->get_myTransform_16();
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		V_9 = L_29;
		float L_30 = (&V_9)->get_y_2();
		V_4 = L_30;
		float L_31 = V_3;
		float L_32 = V_1;
		FsmFloat_t2134102846 * L_33 = __this->get_rotationDamping_14();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		float L_35 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_36 = Mathf_LerpAngle_m1852538964(NULL /*static, unused*/, L_31, L_32, ((float)((float)L_34*(float)L_35)), /*hidden argument*/NULL);
		V_3 = L_36;
		float L_37 = V_4;
		float L_38 = V_2;
		FsmFloat_t2134102846 * L_39 = __this->get_heightDamping_13();
		NullCheck(L_39);
		float L_40 = FsmFloat_get_Value_m4137923823(L_39, /*hidden argument*/NULL);
		float L_41 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_42 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_37, L_38, ((float)((float)L_40*(float)L_41)), /*hidden argument*/NULL);
		V_4 = L_42;
		float L_43 = V_3;
		Quaternion_t1553702882  L_44 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), L_43, (0.0f), /*hidden argument*/NULL);
		V_5 = L_44;
		Transform_t1659122786 * L_45 = __this->get_myTransform_16();
		Transform_t1659122786 * L_46 = __this->get_targetTransform_17();
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = Transform_get_position_m2211398607(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_position_m3111394108(L_45, L_47, /*hidden argument*/NULL);
		Transform_t1659122786 * L_48 = __this->get_myTransform_16();
		Transform_t1659122786 * L_49 = L_48;
		NullCheck(L_49);
		Vector3_t4282066566  L_50 = Transform_get_position_m2211398607(L_49, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_51 = V_5;
		Vector3_t4282066566  L_52 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_53 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_54 = __this->get_distance_11();
		NullCheck(L_54);
		float L_55 = FsmFloat_get_Value_m4137923823(L_54, /*hidden argument*/NULL);
		Vector3_t4282066566  L_56 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_53, L_55, /*hidden argument*/NULL);
		Vector3_t4282066566  L_57 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_50, L_56, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_position_m3111394108(L_49, L_57, /*hidden argument*/NULL);
		Transform_t1659122786 * L_58 = __this->get_myTransform_16();
		Transform_t1659122786 * L_59 = __this->get_myTransform_16();
		NullCheck(L_59);
		Vector3_t4282066566  L_60 = Transform_get_position_m2211398607(L_59, /*hidden argument*/NULL);
		V_10 = L_60;
		float L_61 = (&V_10)->get_x_1();
		float L_62 = V_4;
		Transform_t1659122786 * L_63 = __this->get_myTransform_16();
		NullCheck(L_63);
		Vector3_t4282066566  L_64 = Transform_get_position_m2211398607(L_63, /*hidden argument*/NULL);
		V_11 = L_64;
		float L_65 = (&V_11)->get_z_3();
		Vector3_t4282066566  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Vector3__ctor_m2926210380(&L_66, L_61, L_62, L_65, /*hidden argument*/NULL);
		NullCheck(L_58);
		Transform_set_position_m3111394108(L_58, L_66, /*hidden argument*/NULL);
		Transform_t1659122786 * L_67 = __this->get_myTransform_16();
		Transform_t1659122786 * L_68 = __this->get_targetTransform_17();
		NullCheck(L_67);
		Transform_LookAt_m2663225588(L_67, L_68, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::.ctor()
extern "C"  void SmoothLookAt__ctor_m985640638 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t SmoothLookAt_Reset_m2927040875_MetadataUsageId;
extern "C"  void SmoothLookAt_Reset_m2927040875 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothLookAt_Reset_m2927040875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetObject_10((FsmGameObject_t1697147867 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_targetPosition_11(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmVector3_t533912882 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_0;
		__this->set_upVector_12(L_5);
		FsmBool_t1075959796 * L_6 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_keepVertical_13(L_6);
		FsmBool_t1075959796 * L_7 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_debug_15(L_7);
		FsmFloat_t2134102846 * L_8 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		__this->set_speed_14(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_finishTolerance_16(L_9);
		__this->set_finishEvent_17((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::OnEnter()
extern "C"  void SmoothLookAt_OnEnter_m1699576597 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method)
{
	{
		__this->set_previousGo_18((GameObject_t3674682005 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::OnLateUpdate()
extern "C"  void SmoothLookAt_OnLateUpdate_m393560212 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method)
{
	{
		SmoothLookAt_DoSmoothLookAt_m3481202801(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::DoSmoothLookAt()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SmoothLookAt_DoSmoothLookAt_m3481202801_MetadataUsageId;
extern "C"  void SmoothLookAt_DoSmoothLookAt_m3481202801 (SmoothLookAt_t4236557480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothLookAt_DoSmoothLookAt_m3481202801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	Vector3_t4282066566  G_B19_0;
	memset(&G_B19_0, 0, sizeof(G_B19_0));
	SmoothLookAt_t4236557480 * G_B19_1 = NULL;
	Vector3_t4282066566  G_B18_0;
	memset(&G_B18_0, 0, sizeof(G_B18_0));
	SmoothLookAt_t4236557480 * G_B18_1 = NULL;
	Vector3_t4282066566  G_B20_0;
	memset(&G_B20_0, 0, sizeof(G_B20_0));
	Vector3_t4282066566  G_B20_1;
	memset(&G_B20_1, 0, sizeof(G_B20_1));
	SmoothLookAt_t4236557480 * G_B20_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_targetObject_10();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t3674682005 * L_7 = V_1;
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		FsmVector3_t533912882 * L_9 = __this->get_targetPosition_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		return;
	}

IL_0048:
	{
		GameObject_t3674682005 * L_11 = __this->get_previousGo_18();
		GameObject_t3674682005 * L_12 = V_0;
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_007d;
		}
	}
	{
		GameObject_t3674682005 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t1659122786 * L_15 = GameObject_get_transform_m1278640159(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Quaternion_t1553702882  L_16 = Transform_get_rotation_m11483428(L_15, /*hidden argument*/NULL);
		__this->set_lastRotation_19(L_16);
		Quaternion_t1553702882  L_17 = __this->get_lastRotation_19();
		__this->set_desiredRotation_20(L_17);
		GameObject_t3674682005 * L_18 = V_0;
		__this->set_previousGo_18(L_18);
	}

IL_007d:
	{
		GameObject_t3674682005 * L_19 = V_1;
		bool L_20 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c5;
		}
	}
	{
		FsmVector3_t533912882 * L_21 = __this->get_targetPosition_11();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00b4;
		}
	}
	{
		GameObject_t3674682005 * L_23 = V_1;
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_25 = __this->get_targetPosition_11();
		NullCheck(L_25);
		Vector3_t4282066566  L_26 = FsmVector3_get_Value_m2779135117(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_27 = Transform_TransformPoint_m437395512(L_24, L_26, /*hidden argument*/NULL);
		G_B11_0 = L_27;
		goto IL_00bf;
	}

IL_00b4:
	{
		GameObject_t3674682005 * L_28 = V_1;
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = GameObject_get_transform_m1278640159(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t4282066566  L_30 = Transform_get_position_m2211398607(L_29, /*hidden argument*/NULL);
		G_B11_0 = L_30;
	}

IL_00bf:
	{
		V_2 = G_B11_0;
		goto IL_00d1;
	}

IL_00c5:
	{
		FsmVector3_t533912882 * L_31 = __this->get_targetPosition_11();
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = FsmVector3_get_Value_m2779135117(L_31, /*hidden argument*/NULL);
		V_2 = L_32;
	}

IL_00d1:
	{
		FsmBool_t1075959796 * L_33 = __this->get_keepVertical_13();
		NullCheck(L_33);
		bool L_34 = FsmBool_get_Value_m3101329097(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00fc;
		}
	}
	{
		GameObject_t3674682005 * L_35 = V_0;
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = GameObject_get_transform_m1278640159(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t4282066566  L_37 = Transform_get_position_m2211398607(L_36, /*hidden argument*/NULL);
		V_6 = L_37;
		float L_38 = (&V_6)->get_y_2();
		(&V_2)->set_y_2(L_38);
	}

IL_00fc:
	{
		Vector3_t4282066566  L_39 = V_2;
		GameObject_t3674682005 * L_40 = V_0;
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = GameObject_get_transform_m1278640159(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4282066566  L_42 = Transform_get_position_m2211398607(L_41, /*hidden argument*/NULL);
		Vector3_t4282066566  L_43 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_39, L_42, /*hidden argument*/NULL);
		V_3 = L_43;
		Vector3_t4282066566  L_44 = V_3;
		Vector3_t4282066566  L_45 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_46 = Vector3_op_Inequality_m231387234(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0160;
		}
	}
	{
		float L_47 = Vector3_get_sqrMagnitude_m1207423764((&V_3), /*hidden argument*/NULL);
		if ((!(((float)L_47) > ((float)(0.0f)))))
		{
			goto IL_0160;
		}
	}
	{
		Vector3_t4282066566  L_48 = V_3;
		FsmVector3_t533912882 * L_49 = __this->get_upVector_12();
		NullCheck(L_49);
		bool L_50 = NamedVariable_get_IsNone_m281035543(L_49, /*hidden argument*/NULL);
		G_B18_0 = L_48;
		G_B18_1 = __this;
		if (!L_50)
		{
			G_B19_0 = L_48;
			G_B19_1 = __this;
			goto IL_014b;
		}
	}
	{
		Vector3_t4282066566  L_51 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B20_0 = L_51;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		goto IL_0156;
	}

IL_014b:
	{
		FsmVector3_t533912882 * L_52 = __this->get_upVector_12();
		NullCheck(L_52);
		Vector3_t4282066566  L_53 = FsmVector3_get_Value_m2779135117(L_52, /*hidden argument*/NULL);
		G_B20_0 = L_53;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
	}

IL_0156:
	{
		Quaternion_t1553702882  L_54 = Quaternion_LookRotation_m2869326048(NULL /*static, unused*/, G_B20_1, G_B20_0, /*hidden argument*/NULL);
		NullCheck(G_B20_2);
		G_B20_2->set_desiredRotation_20(L_54);
	}

IL_0160:
	{
		Quaternion_t1553702882  L_55 = __this->get_lastRotation_19();
		Quaternion_t1553702882  L_56 = __this->get_desiredRotation_20();
		FsmFloat_t2134102846 * L_57 = __this->get_speed_14();
		NullCheck(L_57);
		float L_58 = FsmFloat_get_Value_m4137923823(L_57, /*hidden argument*/NULL);
		float L_59 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_60 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_55, L_56, ((float)((float)L_58*(float)L_59)), /*hidden argument*/NULL);
		__this->set_lastRotation_19(L_60);
		GameObject_t3674682005 * L_61 = V_0;
		NullCheck(L_61);
		Transform_t1659122786 * L_62 = GameObject_get_transform_m1278640159(L_61, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_63 = __this->get_lastRotation_19();
		NullCheck(L_62);
		Transform_set_rotation_m1525803229(L_62, L_63, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_64 = __this->get_finishEvent_17();
		if (!L_64)
		{
			goto IL_01f3;
		}
	}
	{
		Vector3_t4282066566  L_65 = V_2;
		GameObject_t3674682005 * L_66 = V_0;
		NullCheck(L_66);
		Transform_t1659122786 * L_67 = GameObject_get_transform_m1278640159(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t4282066566  L_68 = Transform_get_position_m2211398607(L_67, /*hidden argument*/NULL);
		Vector3_t4282066566  L_69 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_65, L_68, /*hidden argument*/NULL);
		V_4 = L_69;
		Vector3_t4282066566  L_70 = V_4;
		GameObject_t3674682005 * L_71 = V_0;
		NullCheck(L_71);
		Transform_t1659122786 * L_72 = GameObject_get_transform_m1278640159(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t4282066566  L_73 = Transform_get_forward_m877665793(L_72, /*hidden argument*/NULL);
		float L_74 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		V_5 = L_74;
		float L_75 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_76 = fabsf(L_75);
		FsmFloat_t2134102846 * L_77 = __this->get_finishTolerance_16();
		NullCheck(L_77);
		float L_78 = FsmFloat_get_Value_m4137923823(L_77, /*hidden argument*/NULL);
		if ((!(((float)L_76) <= ((float)L_78))))
		{
			goto IL_01f3;
		}
	}
	{
		Fsm_t1527112426 * L_79 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_80 = __this->get_finishEvent_17();
		NullCheck(L_79);
		Fsm_Event_m625948263(L_79, L_80, /*hidden argument*/NULL);
	}

IL_01f3:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::.ctor()
extern "C"  void SmoothLookAtDirection__ctor_m2568480809 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t SmoothLookAtDirection_Reset_m214913750_MetadataUsageId;
extern "C"  void SmoothLookAtDirection_Reset_m214913750 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothLookAtDirection_Reset_m214913750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_targetDirection_10(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.1f), /*hidden argument*/NULL);
		__this->set_minMagnitude_11(L_3);
		FsmVector3_t533912882 * L_4 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmVector3_t533912882 * L_5 = V_0;
		NullCheck(L_5);
		NamedVariable_set_UseVariable_m4266138971(L_5, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_6 = V_0;
		__this->set_upVector_12(L_6);
		FsmBool_t1075959796 * L_7 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_keepVertical_13(L_7);
		FsmFloat_t2134102846 * L_8 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		__this->set_speed_14(L_8);
		__this->set_lateUpdate_15((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnEnter()
extern "C"  void SmoothLookAtDirection_OnEnter_m2390558144 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method)
{
	{
		__this->set_previousGo_16((GameObject_t3674682005 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnUpdate()
extern "C"  void SmoothLookAtDirection_OnUpdate_m226417795 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_15();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		SmoothLookAtDirection_DoSmoothLookAtDirection_m1773377499(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnLateUpdate()
extern "C"  void SmoothLookAtDirection_OnLateUpdate_m4097353929 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lateUpdate_15();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SmoothLookAtDirection_DoSmoothLookAtDirection_m1773377499(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::DoSmoothLookAtDirection()
extern "C"  void SmoothLookAtDirection_DoSmoothLookAtDirection_m1773377499 (SmoothLookAtDirection_t4235776941 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	SmoothLookAtDirection_t4235776941 * G_B11_1 = NULL;
	Vector3_t4282066566  G_B10_0;
	memset(&G_B10_0, 0, sizeof(G_B10_0));
	SmoothLookAtDirection_t4235776941 * G_B10_1 = NULL;
	Vector3_t4282066566  G_B12_0;
	memset(&G_B12_0, 0, sizeof(G_B12_0));
	Vector3_t4282066566  G_B12_1;
	memset(&G_B12_1, 0, sizeof(G_B12_1));
	SmoothLookAtDirection_t4235776941 * G_B12_2 = NULL;
	{
		FsmVector3_t533912882 * L_0 = __this->get_targetDirection_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_9();
		NullCheck(L_2);
		GameObject_t3674682005 * L_4 = Fsm_GetOwnerDefaultTarget_m846013999(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0030;
		}
	}
	{
		return;
	}

IL_0030:
	{
		GameObject_t3674682005 * L_7 = __this->get_previousGo_16();
		GameObject_t3674682005 * L_8 = V_0;
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0065;
		}
	}
	{
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t1553702882  L_12 = Transform_get_rotation_m11483428(L_11, /*hidden argument*/NULL);
		__this->set_lastRotation_17(L_12);
		Quaternion_t1553702882  L_13 = __this->get_lastRotation_17();
		__this->set_desiredRotation_18(L_13);
		GameObject_t3674682005 * L_14 = V_0;
		__this->set_previousGo_16(L_14);
	}

IL_0065:
	{
		FsmVector3_t533912882 * L_15 = __this->get_targetDirection_10();
		NullCheck(L_15);
		Vector3_t4282066566  L_16 = FsmVector3_get_Value_m2779135117(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		FsmBool_t1075959796 * L_17 = __this->get_keepVertical_13();
		NullCheck(L_17);
		bool L_18 = FsmBool_get_Value_m3101329097(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_008d;
		}
	}
	{
		(&V_1)->set_y_2((0.0f));
	}

IL_008d:
	{
		float L_19 = Vector3_get_sqrMagnitude_m1207423764((&V_1), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_minMagnitude_11();
		NullCheck(L_20);
		float L_21 = FsmFloat_get_Value_m4137923823(L_20, /*hidden argument*/NULL);
		if ((!(((float)L_19) > ((float)L_21))))
		{
			goto IL_00d5;
		}
	}
	{
		Vector3_t4282066566  L_22 = V_1;
		FsmVector3_t533912882 * L_23 = __this->get_upVector_12();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		G_B10_0 = L_22;
		G_B10_1 = __this;
		if (!L_24)
		{
			G_B11_0 = L_22;
			G_B11_1 = __this;
			goto IL_00c0;
		}
	}
	{
		Vector3_t4282066566  L_25 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B12_0 = L_25;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00cb;
	}

IL_00c0:
	{
		FsmVector3_t533912882 * L_26 = __this->get_upVector_12();
		NullCheck(L_26);
		Vector3_t4282066566  L_27 = FsmVector3_get_Value_m2779135117(L_26, /*hidden argument*/NULL);
		G_B12_0 = L_27;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00cb:
	{
		Quaternion_t1553702882  L_28 = Quaternion_LookRotation_m2869326048(NULL /*static, unused*/, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		NullCheck(G_B12_2);
		G_B12_2->set_desiredRotation_18(L_28);
	}

IL_00d5:
	{
		Quaternion_t1553702882  L_29 = __this->get_lastRotation_17();
		Quaternion_t1553702882  L_30 = __this->get_desiredRotation_18();
		FsmFloat_t2134102846 * L_31 = __this->get_speed_14();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		float L_33 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_34 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_29, L_30, ((float)((float)L_32*(float)L_33)), /*hidden argument*/NULL);
		__this->set_lastRotation_17(L_34);
		GameObject_t3674682005 * L_35 = V_0;
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = GameObject_get_transform_m1278640159(L_35, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_37 = __this->get_lastRotation_17();
		NullCheck(L_36);
		Transform_set_rotation_m1525803229(L_36, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::.ctor()
extern "C"  void StartCoroutine__ctor_m2390295208 (StartCoroutine_t1447367550 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::Reset()
extern "C"  void StartCoroutine_Reset_m36728149 (StartCoroutine_t1447367550 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_behaviour_10((FsmString_t952858651 *)NULL);
		__this->set_functionCall_11((FunctionCall_t3279845016 *)NULL);
		__this->set_stopOnExit_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::OnEnter()
extern "C"  void StartCoroutine_OnEnter_m2952887423 (StartCoroutine_t1447367550 * __this, const MethodInfo* method)
{
	{
		StartCoroutine_DoStartCoroutine_m3337068253(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::DoStartCoroutine()
extern Il2CppClass* MonoBehaviour_t667441552_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StartCoroutine_t1447367550_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1974256870_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4235384975_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m337170132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral260926044;
extern Il2CppCodeGenString* _stringLiteral2683139273;
extern Il2CppCodeGenString* _stringLiteral2433880;
extern Il2CppCodeGenString* _stringLiteral104431;
extern Il2CppCodeGenString* _stringLiteral97526364;
extern Il2CppCodeGenString* _stringLiteral3402981393;
extern Il2CppCodeGenString* _stringLiteral3029738;
extern Il2CppCodeGenString* _stringLiteral2002444079;
extern Il2CppCodeGenString* _stringLiteral2002444080;
extern Il2CppCodeGenString* _stringLiteral2543108;
extern Il2CppCodeGenString* _stringLiteral2489809393;
extern Il2CppCodeGenString* _stringLiteral363710791;
extern Il2CppCodeGenString* _stringLiteral246836475;
extern Il2CppCodeGenString* _stringLiteral368830270;
extern Il2CppCodeGenString* _stringLiteral2355466079;
extern const uint32_t StartCoroutine_DoStartCoroutine_m3337068253_MetadataUsageId;
extern "C"  void StartCoroutine_DoStartCoroutine_m3337068253 (StartCoroutine_t1447367550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartCoroutine_DoStartCoroutine_m3337068253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	String_t* V_1 = NULL;
	Dictionary_2_t1974256870 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		FsmString_t952858651 * L_6 = __this->get_behaviour_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3501516275 * L_8 = GameObject_GetComponent_m2525409030(L_5, L_7, /*hidden argument*/NULL);
		__this->set_component_13(((MonoBehaviour_t667441552 *)IsInstClass(L_8, MonoBehaviour_t667441552_il2cpp_TypeInfo_var)));
		MonoBehaviour_t667441552 * L_9 = __this->get_component_13();
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m3709440845(L_11, /*hidden argument*/NULL);
		FsmString_t952858651 * L_13 = __this->get_behaviour_10();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral260926044, L_12, _stringLiteral2683139273, L_14, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_15, /*hidden argument*/NULL);
		return;
	}

IL_0073:
	{
		FunctionCall_t3279845016 * L_16 = __this->get_functionCall_11();
		NullCheck(L_16);
		String_t* L_17 = FunctionCall_get_ParameterType_m540713776(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		String_t* L_18 = V_1;
		if (!L_18)
		{
			goto IL_03b5;
		}
	}
	{
		Dictionary_2_t1974256870 * L_19 = ((StartCoroutine_t1447367550_StaticFields*)StartCoroutine_t1447367550_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map6_14();
		if (L_19)
		{
			goto IL_013d;
		}
	}
	{
		Dictionary_2_t1974256870 * L_20 = (Dictionary_2_t1974256870 *)il2cpp_codegen_object_new(Dictionary_2_t1974256870_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_20, ((int32_t)13), /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_2 = L_20;
		Dictionary_2_t1974256870 * L_21 = V_2;
		NullCheck(L_21);
		Dictionary_2_Add_m4235384975(L_21, _stringLiteral2433880, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_22 = V_2;
		NullCheck(L_22);
		Dictionary_2_Add_m4235384975(L_22, _stringLiteral104431, 1, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_23 = V_2;
		NullCheck(L_23);
		Dictionary_2_Add_m4235384975(L_23, _stringLiteral97526364, 2, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_24 = V_2;
		NullCheck(L_24);
		Dictionary_2_Add_m4235384975(L_24, _stringLiteral3402981393, 3, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_25 = V_2;
		NullCheck(L_25);
		Dictionary_2_Add_m4235384975(L_25, _stringLiteral3029738, 4, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_26 = V_2;
		NullCheck(L_26);
		Dictionary_2_Add_m4235384975(L_26, _stringLiteral2002444079, 5, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_27 = V_2;
		NullCheck(L_27);
		Dictionary_2_Add_m4235384975(L_27, _stringLiteral2002444080, 6, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_28 = V_2;
		NullCheck(L_28);
		Dictionary_2_Add_m4235384975(L_28, _stringLiteral2543108, 7, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_29 = V_2;
		NullCheck(L_29);
		Dictionary_2_Add_m4235384975(L_29, _stringLiteral2489809393, 8, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_30 = V_2;
		NullCheck(L_30);
		Dictionary_2_Add_m4235384975(L_30, _stringLiteral363710791, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_31 = V_2;
		NullCheck(L_31);
		Dictionary_2_Add_m4235384975(L_31, _stringLiteral246836475, ((int32_t)10), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_32 = V_2;
		NullCheck(L_32);
		Dictionary_2_Add_m4235384975(L_32, _stringLiteral368830270, ((int32_t)11), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_33 = V_2;
		NullCheck(L_33);
		Dictionary_2_Add_m4235384975(L_33, _stringLiteral2355466079, ((int32_t)12), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_34 = V_2;
		((StartCoroutine_t1447367550_StaticFields*)StartCoroutine_t1447367550_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map6_14(L_34);
	}

IL_013d:
	{
		Dictionary_2_t1974256870 * L_35 = ((StartCoroutine_t1447367550_StaticFields*)StartCoroutine_t1447367550_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map6_14();
		String_t* L_36 = V_1;
		NullCheck(L_35);
		bool L_37 = Dictionary_2_TryGetValue_m337170132(L_35, L_36, (&V_3), /*hidden argument*/Dictionary_2_TryGetValue_m337170132_MethodInfo_var);
		if (!L_37)
		{
			goto IL_03b5;
		}
	}
	{
		int32_t L_38 = V_3;
		if (L_38 == 0)
		{
			goto IL_018e;
		}
		if (L_38 == 1)
		{
			goto IL_01a6;
		}
		if (L_38 == 2)
		{
			goto IL_01d3;
		}
		if (L_38 == 3)
		{
			goto IL_0200;
		}
		if (L_38 == 4)
		{
			goto IL_0228;
		}
		if (L_38 == 5)
		{
			goto IL_0255;
		}
		if (L_38 == 6)
		{
			goto IL_0282;
		}
		if (L_38 == 7)
		{
			goto IL_02af;
		}
		if (L_38 == 8)
		{
			goto IL_02dc;
		}
		if (L_38 == 9)
		{
			goto IL_0304;
		}
		if (L_38 == 10)
		{
			goto IL_0330;
		}
		if (L_38 == 11)
		{
			goto IL_035c;
		}
		if (L_38 == 12)
		{
			goto IL_038d;
		}
	}
	{
		goto IL_03b5;
	}

IL_018e:
	{
		MonoBehaviour_t667441552 * L_39 = __this->get_component_13();
		FunctionCall_t3279845016 * L_40 = __this->get_functionCall_11();
		NullCheck(L_40);
		String_t* L_41 = L_40->get_FunctionName_0();
		NullCheck(L_39);
		MonoBehaviour_StartCoroutine_m2272783641(L_39, L_41, /*hidden argument*/NULL);
		return;
	}

IL_01a6:
	{
		MonoBehaviour_t667441552 * L_42 = __this->get_component_13();
		FunctionCall_t3279845016 * L_43 = __this->get_functionCall_11();
		NullCheck(L_43);
		String_t* L_44 = L_43->get_FunctionName_0();
		FunctionCall_t3279845016 * L_45 = __this->get_functionCall_11();
		NullCheck(L_45);
		FsmInt_t1596138449 * L_46 = L_45->get_IntParameter_4();
		NullCheck(L_46);
		int32_t L_47 = FsmInt_get_Value_m27059446(L_46, /*hidden argument*/NULL);
		int32_t L_48 = L_47;
		Il2CppObject * L_49 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_42);
		MonoBehaviour_StartCoroutine_m2964903975(L_42, L_44, L_49, /*hidden argument*/NULL);
		return;
	}

IL_01d3:
	{
		MonoBehaviour_t667441552 * L_50 = __this->get_component_13();
		FunctionCall_t3279845016 * L_51 = __this->get_functionCall_11();
		NullCheck(L_51);
		String_t* L_52 = L_51->get_FunctionName_0();
		FunctionCall_t3279845016 * L_53 = __this->get_functionCall_11();
		NullCheck(L_53);
		FsmFloat_t2134102846 * L_54 = L_53->get_FloatParameter_3();
		NullCheck(L_54);
		float L_55 = FsmFloat_get_Value_m4137923823(L_54, /*hidden argument*/NULL);
		float L_56 = L_55;
		Il2CppObject * L_57 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_50);
		MonoBehaviour_StartCoroutine_m2964903975(L_50, L_52, L_57, /*hidden argument*/NULL);
		return;
	}

IL_0200:
	{
		MonoBehaviour_t667441552 * L_58 = __this->get_component_13();
		FunctionCall_t3279845016 * L_59 = __this->get_functionCall_11();
		NullCheck(L_59);
		String_t* L_60 = L_59->get_FunctionName_0();
		FunctionCall_t3279845016 * L_61 = __this->get_functionCall_11();
		NullCheck(L_61);
		FsmString_t952858651 * L_62 = L_61->get_StringParameter_7();
		NullCheck(L_62);
		String_t* L_63 = FsmString_get_Value_m872383149(L_62, /*hidden argument*/NULL);
		NullCheck(L_58);
		MonoBehaviour_StartCoroutine_m2964903975(L_58, L_60, L_63, /*hidden argument*/NULL);
		return;
	}

IL_0228:
	{
		MonoBehaviour_t667441552 * L_64 = __this->get_component_13();
		FunctionCall_t3279845016 * L_65 = __this->get_functionCall_11();
		NullCheck(L_65);
		String_t* L_66 = L_65->get_FunctionName_0();
		FunctionCall_t3279845016 * L_67 = __this->get_functionCall_11();
		NullCheck(L_67);
		FsmBool_t1075959796 * L_68 = L_67->get_BoolParameter_2();
		NullCheck(L_68);
		bool L_69 = FsmBool_get_Value_m3101329097(L_68, /*hidden argument*/NULL);
		bool L_70 = L_69;
		Il2CppObject * L_71 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_70);
		NullCheck(L_64);
		MonoBehaviour_StartCoroutine_m2964903975(L_64, L_66, L_71, /*hidden argument*/NULL);
		return;
	}

IL_0255:
	{
		MonoBehaviour_t667441552 * L_72 = __this->get_component_13();
		FunctionCall_t3279845016 * L_73 = __this->get_functionCall_11();
		NullCheck(L_73);
		String_t* L_74 = L_73->get_FunctionName_0();
		FunctionCall_t3279845016 * L_75 = __this->get_functionCall_11();
		NullCheck(L_75);
		FsmVector2_t533912881 * L_76 = L_75->get_Vector2Parameter_8();
		NullCheck(L_76);
		Vector2_t4282066565  L_77 = FsmVector2_get_Value_m1313754285(L_76, /*hidden argument*/NULL);
		Vector2_t4282066565  L_78 = L_77;
		Il2CppObject * L_79 = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_72);
		MonoBehaviour_StartCoroutine_m2964903975(L_72, L_74, L_79, /*hidden argument*/NULL);
		return;
	}

IL_0282:
	{
		MonoBehaviour_t667441552 * L_80 = __this->get_component_13();
		FunctionCall_t3279845016 * L_81 = __this->get_functionCall_11();
		NullCheck(L_81);
		String_t* L_82 = L_81->get_FunctionName_0();
		FunctionCall_t3279845016 * L_83 = __this->get_functionCall_11();
		NullCheck(L_83);
		FsmVector3_t533912882 * L_84 = L_83->get_Vector3Parameter_9();
		NullCheck(L_84);
		Vector3_t4282066566  L_85 = FsmVector3_get_Value_m2779135117(L_84, /*hidden argument*/NULL);
		Vector3_t4282066566  L_86 = L_85;
		Il2CppObject * L_87 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_86);
		NullCheck(L_80);
		MonoBehaviour_StartCoroutine_m2964903975(L_80, L_82, L_87, /*hidden argument*/NULL);
		return;
	}

IL_02af:
	{
		MonoBehaviour_t667441552 * L_88 = __this->get_component_13();
		FunctionCall_t3279845016 * L_89 = __this->get_functionCall_11();
		NullCheck(L_89);
		String_t* L_90 = L_89->get_FunctionName_0();
		FunctionCall_t3279845016 * L_91 = __this->get_functionCall_11();
		NullCheck(L_91);
		FsmRect_t1076426478 * L_92 = L_91->get_RectParamater_10();
		NullCheck(L_92);
		Rect_t4241904616  L_93 = FsmRect_get_Value_m1002500317(L_92, /*hidden argument*/NULL);
		Rect_t4241904616  L_94 = L_93;
		Il2CppObject * L_95 = Box(Rect_t4241904616_il2cpp_TypeInfo_var, &L_94);
		NullCheck(L_88);
		MonoBehaviour_StartCoroutine_m2964903975(L_88, L_90, L_95, /*hidden argument*/NULL);
		return;
	}

IL_02dc:
	{
		MonoBehaviour_t667441552 * L_96 = __this->get_component_13();
		FunctionCall_t3279845016 * L_97 = __this->get_functionCall_11();
		NullCheck(L_97);
		String_t* L_98 = L_97->get_FunctionName_0();
		FunctionCall_t3279845016 * L_99 = __this->get_functionCall_11();
		NullCheck(L_99);
		FsmGameObject_t1697147867 * L_100 = L_99->get_GameObjectParameter_5();
		NullCheck(L_100);
		GameObject_t3674682005 * L_101 = FsmGameObject_get_Value_m673294275(L_100, /*hidden argument*/NULL);
		NullCheck(L_96);
		MonoBehaviour_StartCoroutine_m2964903975(L_96, L_98, L_101, /*hidden argument*/NULL);
		return;
	}

IL_0304:
	{
		MonoBehaviour_t667441552 * L_102 = __this->get_component_13();
		FunctionCall_t3279845016 * L_103 = __this->get_functionCall_11();
		NullCheck(L_103);
		String_t* L_104 = L_103->get_FunctionName_0();
		FunctionCall_t3279845016 * L_105 = __this->get_functionCall_11();
		NullCheck(L_105);
		FsmMaterial_t924399665 * L_106 = L_105->get_MaterialParameter_12();
		NullCheck(L_106);
		Material_t3870600107 * L_107 = FsmMaterial_get_Value_m1376213207(L_106, /*hidden argument*/NULL);
		NullCheck(L_102);
		MonoBehaviour_StartCoroutine_m2964903975(L_102, L_104, L_107, /*hidden argument*/NULL);
		goto IL_03b5;
	}

IL_0330:
	{
		MonoBehaviour_t667441552 * L_108 = __this->get_component_13();
		FunctionCall_t3279845016 * L_109 = __this->get_functionCall_11();
		NullCheck(L_109);
		String_t* L_110 = L_109->get_FunctionName_0();
		FunctionCall_t3279845016 * L_111 = __this->get_functionCall_11();
		NullCheck(L_111);
		FsmTexture_t3073272573 * L_112 = L_111->get_TextureParameter_13();
		NullCheck(L_112);
		Texture_t2526458961 * L_113 = FsmTexture_get_Value_m3156202285(L_112, /*hidden argument*/NULL);
		NullCheck(L_108);
		MonoBehaviour_StartCoroutine_m2964903975(L_108, L_110, L_113, /*hidden argument*/NULL);
		goto IL_03b5;
	}

IL_035c:
	{
		MonoBehaviour_t667441552 * L_114 = __this->get_component_13();
		FunctionCall_t3279845016 * L_115 = __this->get_functionCall_11();
		NullCheck(L_115);
		String_t* L_116 = L_115->get_FunctionName_0();
		FunctionCall_t3279845016 * L_117 = __this->get_functionCall_11();
		NullCheck(L_117);
		FsmQuaternion_t3871136040 * L_118 = L_117->get_QuaternionParameter_11();
		NullCheck(L_118);
		Quaternion_t1553702882  L_119 = FsmQuaternion_get_Value_m3393858025(L_118, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_120 = L_119;
		Il2CppObject * L_121 = Box(Quaternion_t1553702882_il2cpp_TypeInfo_var, &L_120);
		NullCheck(L_114);
		MonoBehaviour_StartCoroutine_m2964903975(L_114, L_116, L_121, /*hidden argument*/NULL);
		goto IL_03b5;
	}

IL_038d:
	{
		MonoBehaviour_t667441552 * L_122 = __this->get_component_13();
		FunctionCall_t3279845016 * L_123 = __this->get_functionCall_11();
		NullCheck(L_123);
		String_t* L_124 = L_123->get_FunctionName_0();
		FunctionCall_t3279845016 * L_125 = __this->get_functionCall_11();
		NullCheck(L_125);
		FsmObject_t821476169 * L_126 = L_125->get_ObjectParameter_6();
		NullCheck(L_126);
		Object_t3071478659 * L_127 = FsmObject_get_Value_m188501991(L_126, /*hidden argument*/NULL);
		NullCheck(L_122);
		MonoBehaviour_StartCoroutine_m2964903975(L_122, L_124, L_127, /*hidden argument*/NULL);
		return;
	}

IL_03b5:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::OnExit()
extern "C"  void StartCoroutine_OnExit_m2598026041 (StartCoroutine_t1447367550 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_t667441552 * L_0 = __this->get_component_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_stopOnExit_12();
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		MonoBehaviour_t667441552 * L_3 = __this->get_component_13();
		FunctionCall_t3279845016 * L_4 = __this->get_functionCall_11();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_FunctionName_0();
		NullCheck(L_3);
		MonoBehaviour_StopCoroutine_m2790918991(L_3, L_5, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::.ctor()
extern "C"  void StartLocationServiceUpdates__ctor_m3095830908 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::Reset()
extern "C"  void StartLocationServiceUpdates_Reset_m742263849 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (20.0f), /*hidden argument*/NULL);
		__this->set_maxWait_9(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		__this->set_desiredAccuracy_10(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		__this->set_updateDistance_11(L_2);
		__this->set_successEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_failedEvent_13((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::OnEnter()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t StartLocationServiceUpdates_OnEnter_m2367862355_MetadataUsageId;
extern "C"  void StartLocationServiceUpdates_OnEnter_m2367862355 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartLocationServiceUpdates_OnEnter_m2367862355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_14(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_1 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_desiredAccuracy_10();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_updateDistance_11();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		LocationService_Start_m4263831959(L_1, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t StartLocationServiceUpdates_OnUpdate_m3817815632_MetadataUsageId;
extern "C"  void StartLocationServiceUpdates_OnUpdate_m3817815632 (StartLocationServiceUpdates_t407515898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartLocationServiceUpdates_OnUpdate_m3817815632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = LocationService_get_status_m435232686(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_2 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = LocationService_get_status_m435232686(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		float L_4 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_startTime_14();
		FsmFloat_t2134102846 * L_6 = __this->get_maxWait_9();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_4-(float)L_5))) > ((float)L_7))))
		{
			goto IL_0052;
		}
	}

IL_003b:
	{
		Fsm_t1527112426 * L_8 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_9 = __this->get_failedEvent_13();
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_10 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = LocationService_get_status_m435232686(L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_0079;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_successEvent_12();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartServer::.ctor()
extern "C"  void StartServer__ctor_m1416876995 (StartServer_t1997133011 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartServer::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StartServer_Reset_m3358277232_MetadataUsageId;
extern "C"  void StartServer_Reset_m3358277232 (StartServer_t1997133011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartServer_Reset_m3358277232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		__this->set_connections_9(L_0);
		FsmInt_t1596138449 * L_1 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)25001), /*hidden argument*/NULL);
		__this->set_listenPort_10(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_incomingPassword_11(L_3);
		__this->set_errorEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_errorString_16((FsmString_t952858651 *)NULL);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_useNAT_12(L_4);
		FsmBool_t1075959796 * L_5 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_useSecurityLayer_13(L_5);
		FsmBool_t1075959796 * L_6 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_runInBackground_14(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StartServer::OnEnter()
extern Il2CppClass* NetworkConnectionError_t1049203712_il2cpp_TypeInfo_var;
extern const uint32_t StartServer_OnEnter_m3800855258_MetadataUsageId;
extern "C"  void StartServer_OnEnter_m3800855258 (StartServer_t1997133011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartServer_OnEnter_m3800855258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		FsmString_t952858651 * L_0 = __this->get_incomingPassword_11();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		Network_set_incomingPassword_m515962091(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_2 = __this->get_useSecurityLayer_13();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		Network_InitializeSecurity_m64123113(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0025:
	{
		FsmBool_t1075959796 * L_4 = __this->get_runInBackground_14();
		NullCheck(L_4);
		bool L_5 = FsmBool_get_Value_m3101329097(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		Application_set_runInBackground_m2333211263(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		FsmInt_t1596138449 * L_6 = __this->get_connections_9();
		NullCheck(L_6);
		int32_t L_7 = FsmInt_get_Value_m27059446(L_6, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = __this->get_listenPort_10();
		NullCheck(L_8);
		int32_t L_9 = FsmInt_get_Value_m27059446(L_8, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_10 = __this->get_useNAT_12();
		NullCheck(L_10);
		bool L_11 = FsmBool_get_Value_m3101329097(L_10, /*hidden argument*/NULL);
		int32_t L_12 = Network_InitializeServer_m1086070119(NULL /*static, unused*/, L_7, L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_0;
		if (!L_13)
		{
			goto IL_00a0;
		}
	}
	{
		FsmString_t952858651 * L_14 = __this->get_errorString_16();
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(NetworkConnectionError_t1049203712_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Enum_t2862688501 *)L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_17);
		NullCheck(L_14);
		FsmString_set_Value_m829393196(L_14, L_18, /*hidden argument*/NULL);
		FsmString_t952858651 * L_19 = __this->get_errorString_16();
		NullCheck(L_19);
		String_t* L_20 = FsmString_get_Value_m872383149(L_19, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_20, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_21 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_22 = __this->get_errorEvent_15();
		NullCheck(L_21);
		Fsm_Event_m625948263(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m1381897493_MethodInfo_var;
extern const uint32_t StopAnimation__ctor_m2560679878_MetadataUsageId;
extern "C"  void StopAnimation__ctor_m2560679878 (StopAnimation_t1998114672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StopAnimation__ctor_m2560679878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m1381897493(__this, /*hidden argument*/ComponentAction_1__ctor_m1381897493_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::Reset()
extern "C"  void StopAnimation_Reset_m207112819 (StopAnimation_t1998114672 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_animName_12((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::OnEnter()
extern "C"  void StopAnimation_OnEnter_m3483798045 (StopAnimation_t1998114672 * __this, const MethodInfo* method)
{
	{
		StopAnimation_DoStopAnimation_m876404283(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::DoStopAnimation()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m3952061936_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_animation_m4211453288_MethodInfo_var;
extern const uint32_t StopAnimation_DoStopAnimation_m876404283_MetadataUsageId;
extern "C"  void StopAnimation_DoStopAnimation_m876404283 (StopAnimation_t1998114672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StopAnimation_DoStopAnimation_m876404283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3952061936(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3952061936_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmString_t952858651 * L_5 = __this->get_animName_12();
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		FsmString_t952858651 * L_6 = __this->get_animName_12();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004f;
		}
	}

IL_003f:
	{
		Animation_t1724966010 * L_9 = ComponentAction_1_get_animation_m4211453288(__this, /*hidden argument*/ComponentAction_1_get_animation_m4211453288_MethodInfo_var);
		NullCheck(L_9);
		Animation_Stop_m3675770961(L_9, /*hidden argument*/NULL);
		goto IL_0065;
	}

IL_004f:
	{
		Animation_t1724966010 * L_10 = ComponentAction_1_get_animation_m4211453288(__this, /*hidden argument*/ComponentAction_1_get_animation_m4211453288_MethodInfo_var);
		FsmString_t952858651 * L_11 = __this->get_animName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Animation_Stop_m2745735761(L_10, L_12, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::.ctor()
extern "C"  void StopLocationServiceUpdates__ctor_m4149228914 (StopLocationServiceUpdates_t2528039284 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::Reset()
extern "C"  void StopLocationServiceUpdates_Reset_m1795661855 (StopLocationServiceUpdates_t2528039284 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::OnEnter()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t StopLocationServiceUpdates_OnEnter_m1071064265_MetadataUsageId;
extern "C"  void StopLocationServiceUpdates_OnEnter_m1071064265 (StopLocationServiceUpdates_t2528039284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StopLocationServiceUpdates_OnEnter_m1071064265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocationService_Stop_m4216060557(L_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringChanged::.ctor()
extern "C"  void StringChanged__ctor_m3383396645 (StringChanged_t3723251505 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringChanged::Reset()
extern "C"  void StringChanged_Reset_m1029829586 (StringChanged_t3723251505 * __this, const MethodInfo* method)
{
	{
		__this->set_stringVariable_9((FsmString_t952858651 *)NULL);
		__this->set_changedEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_11((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringChanged::OnEnter()
extern "C"  void StringChanged_OnEnter_m3840628668 (StringChanged_t3723251505 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FsmString_t952858651 * L_2 = __this->get_stringVariable_9();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		__this->set_previousValue_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringChanged::OnUpdate()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringChanged_OnUpdate_m2228931079_MetadataUsageId;
extern "C"  void StringChanged_OnUpdate_m2228931079 (StringChanged_t3723251505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StringChanged_OnUpdate_m2228931079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_previousValue_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		FsmBool_t1075959796 * L_4 = __this->get_storeResult_11();
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_5 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_6 = __this->get_changedEvent_10();
		NullCheck(L_5);
		Fsm_Event_m625948263(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringCompare::.ctor()
extern "C"  void StringCompare__ctor_m131241492 (StringCompare_t3934792034 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringCompare::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCompare_Reset_m2072641729_MetadataUsageId;
extern "C"  void StringCompare_Reset_m2072641729 (StringCompare_t3934792034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StringCompare_Reset_m2072641729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_stringVariable_9((FsmString_t952858651 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_compareTo_10(L_1);
		__this->set_equalEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_notEqualEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringCompare::OnEnter()
extern "C"  void StringCompare_OnEnter_m960750827 (StringCompare_t3934792034 * __this, const MethodInfo* method)
{
	{
		StringCompare_DoStringCompare_m1676816507(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringCompare::OnUpdate()
extern "C"  void StringCompare_OnUpdate_m3147031224 (StringCompare_t3934792034 * __this, const MethodInfo* method)
{
	{
		StringCompare_DoStringCompare_m1676816507(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringCompare::DoStringCompare()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringCompare_DoStringCompare_m1676816507_MetadataUsageId;
extern "C"  void StringCompare_DoStringCompare_m1676816507 (StringCompare_t3934792034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StringCompare_DoStringCompare_m1676816507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_9();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		FsmString_t952858651 * L_1 = __this->get_compareTo_10();
		if (L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		FsmString_t952858651 * L_2 = __this->get_stringVariable_9();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_compareTo_10();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_storeResult_13();
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		FsmBool_t1075959796 * L_8 = __this->get_storeResult_13();
		bool L_9 = V_0;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_9, /*hidden argument*/NULL);
	}

IL_004a:
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		FsmEvent_t2133468028 * L_11 = __this->get_equalEvent_11();
		if (!L_11)
		{
			goto IL_006d;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_equalEvent_11();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_006d:
	{
		bool L_14 = V_0;
		if (L_14)
		{
			goto IL_008f;
		}
	}
	{
		FsmEvent_t2133468028 * L_15 = __this->get_notEqualEvent_12();
		if (!L_15)
		{
			goto IL_008f;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_notEqualEvent_12();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringContains::.ctor()
extern "C"  void StringContains__ctor_m4246734638 (StringContains_t1102823480 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringContains::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringContains_Reset_m1893167579_MetadataUsageId;
extern "C"  void StringContains_Reset_m1893167579 (StringContains_t1102823480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StringContains_Reset_m1893167579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_stringVariable_9((FsmString_t952858651 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_containsString_10(L_1);
		__this->set_trueEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringContains::OnEnter()
extern "C"  void StringContains_OnEnter_m284784517 (StringContains_t1102823480 * __this, const MethodInfo* method)
{
	{
		StringContains_DoStringContains_m804327633(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringContains::OnUpdate()
extern "C"  void StringContains_OnUpdate_m3666912094 (StringContains_t1102823480 * __this, const MethodInfo* method)
{
	{
		StringContains_DoStringContains_m804327633(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringContains::DoStringContains()
extern "C"  void StringContains_DoStringContains_m804327633 (StringContains_t1102823480 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		FsmString_t952858651 * L_2 = __this->get_containsString_10();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}

IL_0020:
	{
		return;
	}

IL_0021:
	{
		FsmString_t952858651 * L_4 = __this->get_stringVariable_9();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		FsmString_t952858651 * L_6 = __this->get_containsString_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = String_Contains_m3032019141(L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		FsmBool_t1075959796 * L_9 = __this->get_storeResult_13();
		if (!L_9)
		{
			goto IL_0054;
		}
	}
	{
		FsmBool_t1075959796 * L_10 = __this->get_storeResult_13();
		bool L_11 = V_0;
		NullCheck(L_10);
		FsmBool_set_Value_m1126216340(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0054:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0077;
		}
	}
	{
		FsmEvent_t2133468028 * L_13 = __this->get_trueEvent_11();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = __this->get_trueEvent_11();
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_15, /*hidden argument*/NULL);
		return;
	}

IL_0077:
	{
		bool L_16 = V_0;
		if (L_16)
		{
			goto IL_0099;
		}
	}
	{
		FsmEvent_t2133468028 * L_17 = __this->get_falseEvent_12();
		if (!L_17)
		{
			goto IL_0099;
		}
	}
	{
		Fsm_t1527112426 * L_18 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_19 = __this->get_falseEvent_12();
		NullCheck(L_18);
		Fsm_Event_m625948263(L_18, L_19, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringReplace::.ctor()
extern "C"  void StringReplace__ctor_m4008917285 (StringReplace_t4078804785 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringReplace::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t StringReplace_Reset_m1655350226_MetadataUsageId;
extern "C"  void StringReplace_Reset_m1655350226 (StringReplace_t4078804785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StringReplace_Reset_m1655350226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_stringVariable_9((FsmString_t952858651 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_replace_10(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_with_11(L_3);
		__this->set_storeResult_12((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringReplace::OnEnter()
extern "C"  void StringReplace_OnEnter_m3670542268 (StringReplace_t4078804785 * __this, const MethodInfo* method)
{
	{
		StringReplace_DoReplace_m4017029804(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringReplace::OnUpdate()
extern "C"  void StringReplace_OnUpdate_m1251219975 (StringReplace_t4078804785 * __this, const MethodInfo* method)
{
	{
		StringReplace_DoReplace_m4017029804(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.StringReplace::DoReplace()
extern "C"  void StringReplace_DoReplace_m4017029804 (StringReplace_t4078804785 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_9();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmString_t952858651 * L_1 = __this->get_storeResult_12();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		FsmString_t952858651 * L_2 = __this->get_storeResult_12();
		FsmString_t952858651 * L_3 = __this->get_stringVariable_9();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmString_t952858651 * L_5 = __this->get_replace_10();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_with_11();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_9 = String_Replace_m2915759397(L_4, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmString_set_Value_m829393196(L_2, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
