﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.FileVO[]
struct FileVOU5BU5D_t573513762;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;

#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.BundleVO
struct  BundleVO_t1984518073  : public VOWithId_t2461972856
{
public:
	// System.String MagicTV.vo.BundleVO::revision_id
	String_t* ___revision_id_1;
	// System.String MagicTV.vo.BundleVO::name
	String_t* ___name_2;
	// System.String MagicTV.vo.BundleVO::context
	String_t* ___context_3;
	// MagicTV.vo.FileVO[] MagicTV.vo.BundleVO::files
	FileVOU5BU5D_t573513762* ___files_4;
	// MagicTV.vo.MetadataVO[] MagicTV.vo.BundleVO::metadata
	MetadataVOU5BU5D_t4238035203* ___metadata_5;
	// System.Nullable`1<System.DateTime> MagicTV.vo.BundleVO::published_at
	Nullable_1_t72820554  ___published_at_6;
	// System.Nullable`1<System.DateTime> MagicTV.vo.BundleVO::expired_at
	Nullable_1_t72820554  ___expired_at_7;
	// MagicTV.vo.BundleVO[] MagicTV.vo.BundleVO::children
	BundleVOU5BU5D_t2162892100* ___children_8;

public:
	inline static int32_t get_offset_of_revision_id_1() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___revision_id_1)); }
	inline String_t* get_revision_id_1() const { return ___revision_id_1; }
	inline String_t** get_address_of_revision_id_1() { return &___revision_id_1; }
	inline void set_revision_id_1(String_t* value)
	{
		___revision_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___revision_id_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___context_3)); }
	inline String_t* get_context_3() const { return ___context_3; }
	inline String_t** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(String_t* value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier(&___context_3, value);
	}

	inline static int32_t get_offset_of_files_4() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___files_4)); }
	inline FileVOU5BU5D_t573513762* get_files_4() const { return ___files_4; }
	inline FileVOU5BU5D_t573513762** get_address_of_files_4() { return &___files_4; }
	inline void set_files_4(FileVOU5BU5D_t573513762* value)
	{
		___files_4 = value;
		Il2CppCodeGenWriteBarrier(&___files_4, value);
	}

	inline static int32_t get_offset_of_metadata_5() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___metadata_5)); }
	inline MetadataVOU5BU5D_t4238035203* get_metadata_5() const { return ___metadata_5; }
	inline MetadataVOU5BU5D_t4238035203** get_address_of_metadata_5() { return &___metadata_5; }
	inline void set_metadata_5(MetadataVOU5BU5D_t4238035203* value)
	{
		___metadata_5 = value;
		Il2CppCodeGenWriteBarrier(&___metadata_5, value);
	}

	inline static int32_t get_offset_of_published_at_6() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___published_at_6)); }
	inline Nullable_1_t72820554  get_published_at_6() const { return ___published_at_6; }
	inline Nullable_1_t72820554 * get_address_of_published_at_6() { return &___published_at_6; }
	inline void set_published_at_6(Nullable_1_t72820554  value)
	{
		___published_at_6 = value;
	}

	inline static int32_t get_offset_of_expired_at_7() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___expired_at_7)); }
	inline Nullable_1_t72820554  get_expired_at_7() const { return ___expired_at_7; }
	inline Nullable_1_t72820554 * get_address_of_expired_at_7() { return &___expired_at_7; }
	inline void set_expired_at_7(Nullable_1_t72820554  value)
	{
		___expired_at_7 = value;
	}

	inline static int32_t get_offset_of_children_8() { return static_cast<int32_t>(offsetof(BundleVO_t1984518073, ___children_8)); }
	inline BundleVOU5BU5D_t2162892100* get_children_8() const { return ___children_8; }
	inline BundleVOU5BU5D_t2162892100** get_address_of_children_8() { return &___children_8; }
	inline void set_children_8(BundleVOU5BU5D_t2162892100* value)
	{
		___children_8 = value;
		Il2CppCodeGenWriteBarrier(&___children_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
