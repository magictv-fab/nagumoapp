﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectIsVisible
struct GameObjectIsVisible_t1570039973;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::.ctor()
extern "C"  void GameObjectIsVisible__ctor_m3292890161 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::Reset()
extern "C"  void GameObjectIsVisible_Reset_m939323102 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnEnter()
extern "C"  void GameObjectIsVisible_OnEnter_m2763243464 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnUpdate()
extern "C"  void GameObjectIsVisible_OnUpdate_m3189728123 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::DoIsVisible()
extern "C"  void GameObjectIsVisible_DoIsVisible_m1769454380 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
