﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void DeviceRotation::.cctor()
extern "C"  void DeviceRotation__cctor_m154677638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceRotation::get_HasGyroscope()
extern "C"  bool DeviceRotation_get_HasGyroscope_m1000044005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion DeviceRotation::Get()
extern "C"  Quaternion_t1553702882  DeviceRotation_Get_m1606502643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceRotation::InitGyro()
extern "C"  void DeviceRotation_InitGyro_m3928761212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion DeviceRotation::ReadGyroscopeRotation()
extern "C"  Quaternion_t1553702882  DeviceRotation_ReadGyroscopeRotation_m1824293258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
