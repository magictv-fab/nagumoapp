﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs
struct AI01AndOtherAIs_t4077022355;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::.ctor(ZXing.Common.BitArray)
extern "C"  void AI01AndOtherAIs__ctor_m860432463 (AI01AndOtherAIs_t4077022355 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::parseInformation()
extern "C"  String_t* AI01AndOtherAIs_parseInformation_m1800250594 (AI01AndOtherAIs_t4077022355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::.cctor()
extern "C"  void AI01AndOtherAIs__cctor_m779195279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
