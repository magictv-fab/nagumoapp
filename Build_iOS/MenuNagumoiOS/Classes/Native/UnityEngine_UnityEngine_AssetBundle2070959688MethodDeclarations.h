﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
extern "C"  Object_t3071478659 * AssetBundle_get_mainAsset_m3597227404 (AssetBundle_t2070959688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern "C"  void AssetBundle_Unload_m913727763 (AssetBundle_t2070959688 * __this, bool ___unloadAllLoadedObjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
