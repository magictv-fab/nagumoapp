﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Examples.ExampleSceneController
struct ExampleSceneController_t2779674540;

#include "codegen/il2cpp-codegen.h"

// System.Void Area730.Examples.ExampleSceneController::.ctor()
extern "C"  void ExampleSceneController__ctor_m515417615 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::Start()
extern "C"  void ExampleSceneController_Start_m3757522703 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::scheduleAction()
extern "C"  void ExampleSceneController_scheduleAction_m824911010 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::ScheduleFromList()
extern "C"  void ExampleSceneController_ScheduleFromList_m109061812 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::cancelAction()
extern "C"  void ExampleSceneController_cancelAction_m823814789 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::clearCurrent()
extern "C"  void ExampleSceneController_clearCurrent_m3658521761 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::clearAll()
extern "C"  void ExampleSceneController_clearAll_m3793238601 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Examples.ExampleSceneController::updateValues()
extern "C"  void ExampleSceneController_updateValues_m3871263168 (ExampleSceneController_t2779674540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
