﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.BaseLuminanceSource
struct BaseLuminanceSource_t803260164;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.BaseLuminanceSource::.ctor(System.Int32,System.Int32)
extern "C"  void BaseLuminanceSource__ctor_m3948391859 (BaseLuminanceSource_t803260164 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.BaseLuminanceSource::getRow(System.Int32,System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* BaseLuminanceSource_getRow_m2355439935 (BaseLuminanceSource_t803260164 * __this, int32_t ___y0, ByteU5BU5D_t4260760469* ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.BaseLuminanceSource::get_Matrix()
extern "C"  ByteU5BU5D_t4260760469* BaseLuminanceSource_get_Matrix_m3262904881 (BaseLuminanceSource_t803260164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.BaseLuminanceSource::rotateCounterClockwise()
extern "C"  LuminanceSource_t1231523093 * BaseLuminanceSource_rotateCounterClockwise_m3566555672 (BaseLuminanceSource_t803260164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.BaseLuminanceSource::get_RotateSupported()
extern "C"  bool BaseLuminanceSource_get_RotateSupported_m674294287 (BaseLuminanceSource_t803260164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.BaseLuminanceSource::get_InversionSupported()
extern "C"  bool BaseLuminanceSource_get_InversionSupported_m403483137 (BaseLuminanceSource_t803260164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.BaseLuminanceSource::invert()
extern "C"  LuminanceSource_t1231523093 * BaseLuminanceSource_invert_m3594443293 (BaseLuminanceSource_t803260164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
