﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// ZXing.Reader
struct Reader_t2610170425;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// ZXing.Aztec.Internal.State
struct State_t3065423635;
// ZXing.Aztec.Internal.Token
struct Token_t3066207355;
// ZXing.Common.CharacterSetECI
struct CharacterSetECI_t2542265168;
// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t755870220;
// ZXing.Datamatrix.Internal.DataBlock
struct DataBlock_t594199773;
// ZXing.Datamatrix.Internal.Version/ECB
struct ECB_t2948426741;
// ZXing.Datamatrix.Internal.Version
struct Version_t2313761970;
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct ResultPointsAndTransitions_t1206419768;
// ZXing.Datamatrix.Encoder.SymbolInfo
struct SymbolInfo_t553159586;
// ZXing.Datamatrix.Encoder.Encoder
struct Encoder_t1508201762;
// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t4119758992;
// ZXing.OneD.OneDReader
struct OneDReader_t3436042911;
// ZXing.OneD.UPCEANReader
struct UPCEANReader_t3527170699;
// ZXing.OneD.RSS.Pair
struct Pair_t1045409632;
// ZXing.OneD.RSS.Expanded.ExpandedPair
struct ExpandedPair_t3413366687;
// ZXing.OneD.RSS.Expanded.ExpandedRow
struct ExpandedRow_t1562831751;
// ZXing.Result
struct Result_t2610723219;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// BigIntegerLibrary.BigInteger
struct BigInteger_t416298622;
// ZXing.PDF417.Internal.DetectionResultColumn
struct DetectionResultColumn_t3195057574;
// ZXing.PDF417.Internal.Codeword
struct Codeword_t1124156527;
// ZXing.PDF417.Internal.BarcodeValue[]
struct BarcodeValueU5BU5D_t3614862452;
// ZXing.PDF417.Internal.BarcodeValue
struct BarcodeValue_t1597289545;
// ZXing.PDF417.Internal.EC.ModulusPoly
struct ModulusPoly_t3133325097;
// ZXing.PDF417.Internal.BarcodeRow
struct BarcodeRow_t3616570866;
// ZXing.QrCode.Internal.DataBlock
struct DataBlock_t3210726793;
// ZXing.QrCode.Internal.Version/ECB
struct ECB_t2022291218;
// ZXing.QrCode.Internal.DataMask
struct DataMask_t1708385874;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// ZXing.QrCode.Internal.Version/ECBlocks
struct ECBlocks_t3771581654;
// ZXing.QrCode.Internal.AlignmentPattern
struct AlignmentPattern_t1786970569;
// ZXing.QrCode.Internal.BlockPair
struct BlockPair_t178469773;

#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "QRCode_ZXing_Aztec_Internal_State3065423635.h"
#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"
#include "QRCode_ZXing_Common_CharacterSetECI2542265168.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGFPoly755870220.h"
#include "QRCode_ZXing_Datamatrix_Internal_DataBlock594199773.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version_ECB2948426741.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version2313761970.h"
#include "QRCode_ZXing_Datamatrix_Internal_Detector_ResultPo1206419768.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolInfo553159586.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPattern4119758992.h"
#include "QRCode_ZXing_OneD_OneDReader3436042911.h"
#include "QRCode_ZXing_OneD_UPCEANReader3527170699.h"
#include "QRCode_ZXing_OneD_RSS_Pair1045409632.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_ExpandedPair3413366687.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_ExpandedRow1562831751.h"
#include "QRCode_ZXing_Result2610723219.h"
#include "QRCode_ArrayTypes.h"
#include "QRCode_BigIntegerLibrary_BigInteger416298622.h"
#include "QRCode_ZXing_PDF417_Internal_DetectionResultColumn3195057574.h"
#include "QRCode_ZXing_PDF417_Internal_Codeword1124156527.h"
#include "QRCode_ZXing_PDF417_Internal_BarcodeValue1597289545.h"
#include "QRCode_ZXing_PDF417_Internal_EC_ModulusPoly3133325097.h"
#include "QRCode_ZXing_PDF417_Internal_BarcodeRow3616570866.h"
#include "QRCode_ZXing_QrCode_Internal_DataBlock3210726793.h"
#include "QRCode_ZXing_QrCode_Internal_Version_ECB2022291218.h"
#include "QRCode_ZXing_QrCode_Internal_DataMask1708385874.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"
#include "QRCode_ZXing_QrCode_Internal_Version_ECBlocks3771581654.h"
#include "QRCode_ZXing_QrCode_Internal_AlignmentPattern1786970569.h"
#include "QRCode_ZXing_QrCode_Internal_BlockPair178469773.h"

#pragma once
// ZXing.DecodeHintType[]
struct DecodeHintTypeU5BU5D_t244884712  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ZXing.ResultMetadataType[]
struct ResultMetadataTypeU5BU5D_t2197797205  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ZXing.BarcodeFormat[]
struct BarcodeFormatU5BU5D_t352465924  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ZXing.Reader[]
struct ReaderU5BU5D_t4257665220  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.EncodeHintType[]
struct EncodeHintTypeU5BU5D_t2781905696  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResultPoint_t1538592853 * m_Items[1];

public:
	inline ResultPoint_t1538592853 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResultPoint_t1538592853 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResultPoint_t1538592853 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Aztec.Internal.Decoder/Table[]
struct TableU5BU5D_t1820856876  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ZXing.Aztec.Internal.State[]
struct StateU5BU5D_t545846082  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) State_t3065423635 * m_Items[1];

public:
	inline State_t3065423635 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline State_t3065423635 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, State_t3065423635 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Aztec.Internal.Token[]
struct TokenU5BU5D_t3282023226  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Token_t3066207355 * m_Items[1];

public:
	inline Token_t3066207355 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Token_t3066207355 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Token_t3066207355 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Common.CharacterSetECI[]
struct CharacterSetECIU5BU5D_t1530402161  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CharacterSetECI_t2542265168 * m_Items[1];

public:
	inline CharacterSetECI_t2542265168 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CharacterSetECI_t2542265168 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CharacterSetECI_t2542265168 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Common.ReedSolomon.GenericGFPoly[]
struct GenericGFPolyU5BU5D_t2416185413  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GenericGFPoly_t755870220 * m_Items[1];

public:
	inline GenericGFPoly_t755870220 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GenericGFPoly_t755870220 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GenericGFPoly_t755870220 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Datamatrix.Internal.DataBlock[]
struct DataBlockU5BU5D_t1220297488  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataBlock_t594199773 * m_Items[1];

public:
	inline DataBlock_t594199773 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataBlock_t594199773 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataBlock_t594199773 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Datamatrix.Internal.Version/ECB[]
struct ECBU5BU5D_t1855874200  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ECB_t2948426741 * m_Items[1];

public:
	inline ECB_t2948426741 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ECB_t2948426741 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ECB_t2948426741 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Datamatrix.Internal.Version[]
struct VersionU5BU5D_t2074916935  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Version_t2313761970 * m_Items[1];

public:
	inline Version_t2313761970 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Version_t2313761970 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Version_t2313761970 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions[]
struct ResultPointsAndTransitionsU5BU5D_t4133223145  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResultPointsAndTransitions_t1206419768 * m_Items[1];

public:
	inline ResultPointsAndTransitions_t1206419768 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResultPointsAndTransitions_t1206419768 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResultPointsAndTransitions_t1206419768 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Datamatrix.Encoder.SymbolInfo[]
struct SymbolInfoU5BU5D_t2435839895  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SymbolInfo_t553159586 * m_Items[1];

public:
	inline SymbolInfo_t553159586 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SymbolInfo_t553159586 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SymbolInfo_t553159586 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Datamatrix.Encoder.Encoder[]
struct EncoderU5BU5D_t3796064279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.FinderPattern[]
struct FinderPatternU5BU5D_t1610216241  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FinderPattern_t4119758992 * m_Items[1];

public:
	inline FinderPattern_t4119758992 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FinderPattern_t4119758992 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FinderPattern_t4119758992 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.OneD.OneDReader[]
struct OneDReaderU5BU5D_t1538827270  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OneDReader_t3436042911 * m_Items[1];

public:
	inline OneDReader_t3436042911 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OneDReader_t3436042911 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OneDReader_t3436042911 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.OneD.UPCEANReader[]
struct UPCEANReaderU5BU5D_t2863360746  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UPCEANReader_t3527170699 * m_Items[1];

public:
	inline UPCEANReader_t3527170699 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UPCEANReader_t3527170699 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UPCEANReader_t3527170699 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.OneD.RSS.Pair[]
struct PairU5BU5D_t2128987937  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Pair_t1045409632 * m_Items[1];

public:
	inline Pair_t1045409632 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Pair_t1045409632 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Pair_t1045409632 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.OneD.RSS.Expanded.ExpandedPair[]
struct ExpandedPairU5BU5D_t1339167494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExpandedPair_t3413366687 * m_Items[1];

public:
	inline ExpandedPair_t3413366687 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExpandedPair_t3413366687 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExpandedPair_t3413366687 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.OneD.RSS.Expanded.ExpandedRow[]
struct ExpandedRowU5BU5D_t388819582  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExpandedRow_t1562831751 * m_Items[1];

public:
	inline ExpandedRow_t1562831751 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExpandedRow_t1562831751 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExpandedRow_t1562831751 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.Result[]
struct ResultU5BU5D_t4049143490  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Result_t2610723219 * m_Items[1];

public:
	inline Result_t2610723219 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Result_t2610723219 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Result_t2610723219 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.ResultPoint[][]
struct ResultPointU5BU5DU5BU5D_t2883781481  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResultPointU5BU5D_t1195164344* m_Items[1];

public:
	inline ResultPointU5BU5D_t1195164344* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResultPointU5BU5D_t1195164344** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResultPointU5BU5D_t1195164344* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BigIntegerLibrary.BigInteger[]
struct BigIntegerU5BU5D_t4221983179  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BigInteger_t416298622 * m_Items[1];

public:
	inline BigInteger_t416298622 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BigInteger_t416298622 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BigInteger_t416298622 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.PDF417.Internal.DetectionResultColumn[]
struct DetectionResultColumnU5BU5D_t3380244739  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DetectionResultColumn_t3195057574 * m_Items[1];

public:
	inline DetectionResultColumn_t3195057574 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DetectionResultColumn_t3195057574 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DetectionResultColumn_t3195057574 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.PDF417.Internal.Codeword[]
struct CodewordU5BU5D_t399100150  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Codeword_t1124156527 * m_Items[1];

public:
	inline Codeword_t1124156527 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Codeword_t1124156527 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Codeword_t1124156527 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.PDF417.Internal.BarcodeValue[][]
struct BarcodeValueU5BU5DU5BU5D_t3694669629  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BarcodeValueU5BU5D_t3614862452* m_Items[1];

public:
	inline BarcodeValueU5BU5D_t3614862452* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BarcodeValueU5BU5D_t3614862452** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BarcodeValueU5BU5D_t3614862452* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.PDF417.Internal.BarcodeValue[]
struct BarcodeValueU5BU5D_t3614862452  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BarcodeValue_t1597289545 * m_Items[1];

public:
	inline BarcodeValue_t1597289545 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BarcodeValue_t1597289545 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BarcodeValue_t1597289545 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.PDF417.Internal.EC.ModulusPoly[]
struct ModulusPolyU5BU5D_t731609620  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ModulusPoly_t3133325097 * m_Items[1];

public:
	inline ModulusPoly_t3133325097 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ModulusPoly_t3133325097 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ModulusPoly_t3133325097 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.PDF417.Internal.BarcodeRow[]
struct BarcodeRowU5BU5D_t1319297543  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BarcodeRow_t3616570866 * m_Items[1];

public:
	inline BarcodeRow_t3616570866 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BarcodeRow_t3616570866 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BarcodeRow_t3616570866 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.DataBlock[]
struct DataBlockU5BU5D_t181372468  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataBlock_t3210726793 * m_Items[1];

public:
	inline DataBlock_t3210726793 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataBlock_t3210726793 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataBlock_t3210726793 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.Version/ECB[]
struct ECBU5BU5D_t54329447  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ECB_t2022291218 * m_Items[1];

public:
	inline ECB_t2022291218 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ECB_t2022291218 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ECB_t2022291218 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.DataMask[]
struct DataMaskU5BU5D_t404352039  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataMask_t1708385874 * m_Items[1];

public:
	inline DataMask_t1708385874 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataMask_t1708385874 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataMask_t1708385874 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.ErrorCorrectionLevel[]
struct ErrorCorrectionLevelU5BU5D_t232463391  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ErrorCorrectionLevel_t1225927610 * m_Items[1];

public:
	inline ErrorCorrectionLevel_t1225927610 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ErrorCorrectionLevel_t1225927610 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ErrorCorrectionLevel_t1225927610 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.Version[]
struct VersionU5BU5D_t1560908587  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Version_t1953509534 * m_Items[1];

public:
	inline Version_t1953509534 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Version_t1953509534 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Version_t1953509534 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.Version/ECBlocks[]
struct ECBlocksU5BU5D_t663932691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ECBlocks_t3771581654 * m_Items[1];

public:
	inline ECBlocks_t3771581654 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ECBlocks_t3771581654 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ECBlocks_t3771581654 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.AlignmentPattern[]
struct AlignmentPatternU5BU5D_t3212687604  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AlignmentPattern_t1786970569 * m_Items[1];

public:
	inline AlignmentPattern_t1786970569 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AlignmentPattern_t1786970569 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AlignmentPattern_t1786970569 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZXing.QrCode.Internal.BlockPair[]
struct BlockPairU5BU5D_t4177865632  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BlockPair_t178469773 * m_Items[1];

public:
	inline BlockPair_t178469773 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BlockPair_t178469773 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BlockPair_t178469773 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
