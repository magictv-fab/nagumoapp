﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolFlip
struct  BoolFlip_t1752556319  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolFlip::boolVariable
	FsmBool_t1075959796 * ___boolVariable_9;

public:
	inline static int32_t get_offset_of_boolVariable_9() { return static_cast<int32_t>(offsetof(BoolFlip_t1752556319, ___boolVariable_9)); }
	inline FsmBool_t1075959796 * get_boolVariable_9() const { return ___boolVariable_9; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_9() { return &___boolVariable_9; }
	inline void set_boolVariable_9(FsmBool_t1075959796 * value)
	{
		___boolVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
