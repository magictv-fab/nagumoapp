﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoAcougue
struct PedidoAcougue_t9638794;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void PedidoAcougue::.ctor()
extern "C"  void PedidoAcougue__ctor_m4270041377 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::.cctor()
extern "C"  void PedidoAcougue__cctor_m3040167596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::Start()
extern "C"  void PedidoAcougue_Start_m3217179169 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::BtnSelectPeso(System.String)
extern "C"  void PedidoAcougue_BtnSelectPeso_m781832282 (PedidoAcougue_t9638794 * __this, String_t* ___peso0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::BtnTipoCarne(System.Int32)
extern "C"  void PedidoAcougue_BtnTipoCarne_m762195535 (PedidoAcougue_t9638794 * __this, int32_t ___tipo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::BtnSearch(System.String)
extern "C"  void PedidoAcougue_BtnSearch_m703764095 (PedidoAcougue_t9638794 * __this, String_t* ___search0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::ShowNumberSelected(System.Int32)
extern "C"  void PedidoAcougue_ShowNumberSelected_m4250336501 (PedidoAcougue_t9638794 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::InstantiateNextItem()
extern "C"  void PedidoAcougue_InstantiateNextItem_m2610135919 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::SendStatus(System.Int32)
extern "C"  void PedidoAcougue_SendStatus_m69471086 (PedidoAcougue_t9638794 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::BtnAccept()
extern "C"  void PedidoAcougue_BtnAccept_m1505269955 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::BtnRecuse()
extern "C"  void PedidoAcougue_BtnRecuse_m2858133234 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PedidoAcougue::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * PedidoAcougue_ISendStatus_m1296130239 (PedidoAcougue_t9638794 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PedidoAcougue::ILoadCarnes(System.String)
extern "C"  Il2CppObject * PedidoAcougue_ILoadCarnes_m3076998244 (PedidoAcougue_t9638794 * __this, String_t* ___search0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PedidoAcougue::GetDateString(System.String)
extern "C"  String_t* PedidoAcougue_GetDateString_m1948142891 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PedidoAcougue::ILoadImgs()
extern "C"  Il2CppObject * PedidoAcougue_ILoadImgs_m1979083366 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PedidoAcougue::LoadLoadeds()
extern "C"  Il2CppObject * PedidoAcougue_LoadLoadeds_m1738714447 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::PopUp(System.String)
extern "C"  void PedidoAcougue_PopUp_m2906575063 (PedidoAcougue_t9638794 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue::BtnAvancar()
extern "C"  void PedidoAcougue_BtnAvancar_m937507961 (PedidoAcougue_t9638794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PedidoAcougue::GetTimestamp(System.DateTime)
extern "C"  String_t* PedidoAcougue_GetTimestamp_m127211800 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
