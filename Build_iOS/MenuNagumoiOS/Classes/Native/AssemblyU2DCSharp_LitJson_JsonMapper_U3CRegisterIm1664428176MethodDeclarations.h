﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterImporter>c__AnonStoreyA3`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStoreyA3_2_t1664428176;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStoreyA3`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStoreyA3_2__ctor_m907507080_gshared (U3CRegisterImporterU3Ec__AnonStoreyA3_2_t1664428176 * __this, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStoreyA3_2__ctor_m907507080(__this, method) ((  void (*) (U3CRegisterImporterU3Ec__AnonStoreyA3_2_t1664428176 *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStoreyA3_2__ctor_m907507080_gshared)(__this, method)
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStoreyA3`2<System.Object,System.Object>::<>m__72(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStoreyA3_2_U3CU3Em__72_m2938667731_gshared (U3CRegisterImporterU3Ec__AnonStoreyA3_2_t1664428176 * __this, Il2CppObject * ___input0, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStoreyA3_2_U3CU3Em__72_m2938667731(__this, ___input0, method) ((  Il2CppObject * (*) (U3CRegisterImporterU3Ec__AnonStoreyA3_2_t1664428176 *, Il2CppObject *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStoreyA3_2_U3CU3Em__72_m2938667731_gshared)(__this, ___input0, method)
