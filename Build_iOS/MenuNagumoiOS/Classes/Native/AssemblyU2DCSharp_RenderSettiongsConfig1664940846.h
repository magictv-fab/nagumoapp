﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderSettiongsConfig
struct  RenderSettiongsConfig_t1664940846  : public MonoBehaviour_t667441552
{
public:
	// System.Single RenderSettiongsConfig::_initialAmbientIntensity
	float ____initialAmbientIntensity_2;
	// UnityEngine.Color RenderSettiongsConfig::_initialAmbientSkyColor
	Color_t4194546905  ____initialAmbientSkyColor_3;
	// UnityEngine.Color RenderSettiongsConfig::_initialAmbientEquatorColor
	Color_t4194546905  ____initialAmbientEquatorColor_4;
	// UnityEngine.Color RenderSettiongsConfig::_initialAmbientLight
	Color_t4194546905  ____initialAmbientLight_5;
	// UnityEngine.Color RenderSettiongsConfig::_initialAmbientGroundColor
	Color_t4194546905  ____initialAmbientGroundColor_6;
	// System.Single RenderSettiongsConfig::_initialReflectionIntensity
	float ____initialReflectionIntensity_7;
	// System.Int32 RenderSettiongsConfig::_initialReflectionBounces
	int32_t ____initialReflectionBounces_8;
	// System.Single RenderSettiongsConfig::_initialHaloStrength
	float ____initialHaloStrength_9;
	// System.String RenderSettiongsConfig::DebugMetadata
	String_t* ___DebugMetadata_10;
	// System.Boolean RenderSettiongsConfig::DebugSendNow
	bool ___DebugSendNow_11;

public:
	inline static int32_t get_offset_of__initialAmbientIntensity_2() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialAmbientIntensity_2)); }
	inline float get__initialAmbientIntensity_2() const { return ____initialAmbientIntensity_2; }
	inline float* get_address_of__initialAmbientIntensity_2() { return &____initialAmbientIntensity_2; }
	inline void set__initialAmbientIntensity_2(float value)
	{
		____initialAmbientIntensity_2 = value;
	}

	inline static int32_t get_offset_of__initialAmbientSkyColor_3() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialAmbientSkyColor_3)); }
	inline Color_t4194546905  get__initialAmbientSkyColor_3() const { return ____initialAmbientSkyColor_3; }
	inline Color_t4194546905 * get_address_of__initialAmbientSkyColor_3() { return &____initialAmbientSkyColor_3; }
	inline void set__initialAmbientSkyColor_3(Color_t4194546905  value)
	{
		____initialAmbientSkyColor_3 = value;
	}

	inline static int32_t get_offset_of__initialAmbientEquatorColor_4() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialAmbientEquatorColor_4)); }
	inline Color_t4194546905  get__initialAmbientEquatorColor_4() const { return ____initialAmbientEquatorColor_4; }
	inline Color_t4194546905 * get_address_of__initialAmbientEquatorColor_4() { return &____initialAmbientEquatorColor_4; }
	inline void set__initialAmbientEquatorColor_4(Color_t4194546905  value)
	{
		____initialAmbientEquatorColor_4 = value;
	}

	inline static int32_t get_offset_of__initialAmbientLight_5() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialAmbientLight_5)); }
	inline Color_t4194546905  get__initialAmbientLight_5() const { return ____initialAmbientLight_5; }
	inline Color_t4194546905 * get_address_of__initialAmbientLight_5() { return &____initialAmbientLight_5; }
	inline void set__initialAmbientLight_5(Color_t4194546905  value)
	{
		____initialAmbientLight_5 = value;
	}

	inline static int32_t get_offset_of__initialAmbientGroundColor_6() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialAmbientGroundColor_6)); }
	inline Color_t4194546905  get__initialAmbientGroundColor_6() const { return ____initialAmbientGroundColor_6; }
	inline Color_t4194546905 * get_address_of__initialAmbientGroundColor_6() { return &____initialAmbientGroundColor_6; }
	inline void set__initialAmbientGroundColor_6(Color_t4194546905  value)
	{
		____initialAmbientGroundColor_6 = value;
	}

	inline static int32_t get_offset_of__initialReflectionIntensity_7() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialReflectionIntensity_7)); }
	inline float get__initialReflectionIntensity_7() const { return ____initialReflectionIntensity_7; }
	inline float* get_address_of__initialReflectionIntensity_7() { return &____initialReflectionIntensity_7; }
	inline void set__initialReflectionIntensity_7(float value)
	{
		____initialReflectionIntensity_7 = value;
	}

	inline static int32_t get_offset_of__initialReflectionBounces_8() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialReflectionBounces_8)); }
	inline int32_t get__initialReflectionBounces_8() const { return ____initialReflectionBounces_8; }
	inline int32_t* get_address_of__initialReflectionBounces_8() { return &____initialReflectionBounces_8; }
	inline void set__initialReflectionBounces_8(int32_t value)
	{
		____initialReflectionBounces_8 = value;
	}

	inline static int32_t get_offset_of__initialHaloStrength_9() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ____initialHaloStrength_9)); }
	inline float get__initialHaloStrength_9() const { return ____initialHaloStrength_9; }
	inline float* get_address_of__initialHaloStrength_9() { return &____initialHaloStrength_9; }
	inline void set__initialHaloStrength_9(float value)
	{
		____initialHaloStrength_9 = value;
	}

	inline static int32_t get_offset_of_DebugMetadata_10() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ___DebugMetadata_10)); }
	inline String_t* get_DebugMetadata_10() const { return ___DebugMetadata_10; }
	inline String_t** get_address_of_DebugMetadata_10() { return &___DebugMetadata_10; }
	inline void set_DebugMetadata_10(String_t* value)
	{
		___DebugMetadata_10 = value;
		Il2CppCodeGenWriteBarrier(&___DebugMetadata_10, value);
	}

	inline static int32_t get_offset_of_DebugSendNow_11() { return static_cast<int32_t>(offsetof(RenderSettiongsConfig_t1664940846, ___DebugSendNow_11)); }
	inline bool get_DebugSendNow_11() const { return ___DebugSendNow_11; }
	inline bool* get_address_of_DebugSendNow_11() { return &___DebugSendNow_11; }
	inline void set_DebugSendNow_11(bool value)
	{
		___DebugSendNow_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
