﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.ReedSolomon.ReedSolomonDecoder
struct ReedSolomonDecoder_t4166992619;
// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.ReedSolomon.GenericGFPoly[]
struct GenericGFPolyU5BU5D_t2416185413;
// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t755870220;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGF2563420960.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGFPoly755870220.h"

// System.Void ZXing.Common.ReedSolomon.ReedSolomonDecoder::.ctor(ZXing.Common.ReedSolomon.GenericGF)
extern "C"  void ReedSolomonDecoder__ctor_m2946762892 (ReedSolomonDecoder_t4166992619 * __this, GenericGF_t2563420960 * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.ReedSolomon.ReedSolomonDecoder::decode(System.Int32[],System.Int32)
extern "C"  bool ReedSolomonDecoder_decode_m1523003520 (ReedSolomonDecoder_t4166992619 * __this, Int32U5BU5D_t3230847821* ___received0, int32_t ___twoS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::runEuclideanAlgorithm(ZXing.Common.ReedSolomon.GenericGFPoly,ZXing.Common.ReedSolomon.GenericGFPoly,System.Int32)
extern "C"  GenericGFPolyU5BU5D_t2416185413* ReedSolomonDecoder_runEuclideanAlgorithm_m1179906854 (ReedSolomonDecoder_t4166992619 * __this, GenericGFPoly_t755870220 * ___a0, GenericGFPoly_t755870220 * ___b1, int32_t ___R2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::findErrorLocations(ZXing.Common.ReedSolomon.GenericGFPoly)
extern "C"  Int32U5BU5D_t3230847821* ReedSolomonDecoder_findErrorLocations_m4164302395 (ReedSolomonDecoder_t4166992619 * __this, GenericGFPoly_t755870220 * ___errorLocator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::findErrorMagnitudes(ZXing.Common.ReedSolomon.GenericGFPoly,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* ReedSolomonDecoder_findErrorMagnitudes_m2036544089 (ReedSolomonDecoder_t4166992619 * __this, GenericGFPoly_t755870220 * ___errorEvaluator0, Int32U5BU5D_t3230847821* ___errorLocations1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
