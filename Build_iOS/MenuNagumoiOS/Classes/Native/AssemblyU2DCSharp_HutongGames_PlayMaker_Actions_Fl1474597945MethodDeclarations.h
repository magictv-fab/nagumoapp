﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatSignTest
struct FloatSignTest_t1474597945;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::.ctor()
extern "C"  void FloatSignTest__ctor_m1954245917 (FloatSignTest_t1474597945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::Reset()
extern "C"  void FloatSignTest_Reset_m3895646154 (FloatSignTest_t1474597945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::OnEnter()
extern "C"  void FloatSignTest_OnEnter_m521346484 (FloatSignTest_t1474597945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::OnUpdate()
extern "C"  void FloatSignTest_OnUpdate_m2410398479 (FloatSignTest_t1474597945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::DoSignTest()
extern "C"  void FloatSignTest_DoSignTest_m1316262689 (FloatSignTest_t1474597945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.FloatSignTest::ErrorCheck()
extern "C"  String_t* FloatSignTest_ErrorCheck_m2356136740 (FloatSignTest_t1474597945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
