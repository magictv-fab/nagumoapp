﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmInt
struct GetFsmInt_t541307091;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::.ctor()
extern "C"  void GetFsmInt__ctor_m2125959619 (GetFsmInt_t541307091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::Reset()
extern "C"  void GetFsmInt_Reset_m4067359856 (GetFsmInt_t541307091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::OnEnter()
extern "C"  void GetFsmInt_OnEnter_m2329456858 (GetFsmInt_t541307091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::OnUpdate()
extern "C"  void GetFsmInt_OnUpdate_m2627245225 (GetFsmInt_t541307091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::DoGetFsmInt()
extern "C"  void GetFsmInt_DoGetFsmInt_m2392610203 (GetFsmInt_t541307091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
