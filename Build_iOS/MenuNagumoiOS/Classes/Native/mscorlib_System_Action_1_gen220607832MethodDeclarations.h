﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen271665211MethodDeclarations.h"

// System.Void System.Action`1<ZXing.QrCode.Internal.FinderPattern>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3555578037(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t220607832 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m881151526_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<ZXing.QrCode.Internal.FinderPattern>::Invoke(T)
#define Action_1_Invoke_m3209902575(__this, ___obj0, method) ((  void (*) (Action_1_t220607832 *, FinderPattern_t4119758992 *, const MethodInfo*))Action_1_Invoke_m663971678_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<ZXing.QrCode.Internal.FinderPattern>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m229452476(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t220607832 *, FinderPattern_t4119758992 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m917692971_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<ZXing.QrCode.Internal.FinderPattern>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m3200771397(__this, ___result0, method) ((  void (*) (Action_1_t220607832 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3562128182_gshared)(__this, ___result0, method)
