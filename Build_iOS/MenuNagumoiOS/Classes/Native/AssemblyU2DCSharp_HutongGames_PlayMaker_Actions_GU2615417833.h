﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.LayoutOption[]
struct LayoutOptionU5BU5D_t3507996764;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutAction
struct  GUILayoutAction_t2615417833  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::layoutOptions
	LayoutOptionU5BU5D_t3507996764* ___layoutOptions_9;
	// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::options
	GUILayoutOptionU5BU5D_t2977405297* ___options_10;

public:
	inline static int32_t get_offset_of_layoutOptions_9() { return static_cast<int32_t>(offsetof(GUILayoutAction_t2615417833, ___layoutOptions_9)); }
	inline LayoutOptionU5BU5D_t3507996764* get_layoutOptions_9() const { return ___layoutOptions_9; }
	inline LayoutOptionU5BU5D_t3507996764** get_address_of_layoutOptions_9() { return &___layoutOptions_9; }
	inline void set_layoutOptions_9(LayoutOptionU5BU5D_t3507996764* value)
	{
		___layoutOptions_9 = value;
		Il2CppCodeGenWriteBarrier(&___layoutOptions_9, value);
	}

	inline static int32_t get_offset_of_options_10() { return static_cast<int32_t>(offsetof(GUILayoutAction_t2615417833, ___options_10)); }
	inline GUILayoutOptionU5BU5D_t2977405297* get_options_10() const { return ___options_10; }
	inline GUILayoutOptionU5BU5D_t2977405297** get_address_of_options_10() { return &___options_10; }
	inline void set_options_10(GUILayoutOptionU5BU5D_t2977405297* value)
	{
		___options_10 = value;
		Il2CppCodeGenWriteBarrier(&___options_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
