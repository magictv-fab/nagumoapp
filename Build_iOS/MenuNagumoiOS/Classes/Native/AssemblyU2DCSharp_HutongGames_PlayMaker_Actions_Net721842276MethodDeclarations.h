﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetSendRate
struct NetworkSetSendRate_t721842276;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetSendRate::.ctor()
extern "C"  void NetworkSetSendRate__ctor_m540540034 (NetworkSetSendRate_t721842276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetSendRate::Reset()
extern "C"  void NetworkSetSendRate_Reset_m2481940271 (NetworkSetSendRate_t721842276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetSendRate::OnEnter()
extern "C"  void NetworkSetSendRate_OnEnter_m3454625753 (NetworkSetSendRate_t721842276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetSendRate::DoSetSendRate()
extern "C"  void NetworkSetSendRate_DoSetSendRate_m3036479583 (NetworkSetSendRate_t721842276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
