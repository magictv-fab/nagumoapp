﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendMessage
struct SendMessage_t273614317;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendMessage::.ctor()
extern "C"  void SendMessage__ctor_m921386985 (SendMessage_t273614317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendMessage::Reset()
extern "C"  void SendMessage_Reset_m2862787222 (SendMessage_t273614317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendMessage::OnEnter()
extern "C"  void SendMessage_OnEnter_m81358208 (SendMessage_t273614317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendMessage::DoSendMessage()
extern "C"  void SendMessage_DoSendMessage_m2034024411 (SendMessage_t273614317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
