﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVisibility
struct SetVisibility_t1014047458;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.SetVisibility::.ctor()
extern "C"  void SetVisibility__ctor_m4004138132 (SetVisibility_t1014047458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::Reset()
extern "C"  void SetVisibility_Reset_m1650571073 (SetVisibility_t1014047458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::OnEnter()
extern "C"  void SetVisibility_OnEnter_m3372743531 (SetVisibility_t1014047458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::DoSetVisibility(UnityEngine.GameObject)
extern "C"  void SetVisibility_DoSetVisibility_m3730534835 (SetVisibility_t1014047458 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::OnExit()
extern "C"  void SetVisibility_OnExit_m1087549133 (SetVisibility_t1014047458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::ResetVisibility()
extern "C"  void SetVisibility_ResetVisibility_m1141711347 (SetVisibility_t1014047458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
