﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.Component
struct Component_t3501516275;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddScript
struct  AddScript_t95714330  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddScript::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddScript::script
	FsmString_t952858651 * ___script_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddScript::removeOnExit
	FsmBool_t1075959796 * ___removeOnExit_11;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.AddScript::addedComponent
	Component_t3501516275 * ___addedComponent_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AddScript_t95714330, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_script_10() { return static_cast<int32_t>(offsetof(AddScript_t95714330, ___script_10)); }
	inline FsmString_t952858651 * get_script_10() const { return ___script_10; }
	inline FsmString_t952858651 ** get_address_of_script_10() { return &___script_10; }
	inline void set_script_10(FsmString_t952858651 * value)
	{
		___script_10 = value;
		Il2CppCodeGenWriteBarrier(&___script_10, value);
	}

	inline static int32_t get_offset_of_removeOnExit_11() { return static_cast<int32_t>(offsetof(AddScript_t95714330, ___removeOnExit_11)); }
	inline FsmBool_t1075959796 * get_removeOnExit_11() const { return ___removeOnExit_11; }
	inline FsmBool_t1075959796 ** get_address_of_removeOnExit_11() { return &___removeOnExit_11; }
	inline void set_removeOnExit_11(FsmBool_t1075959796 * value)
	{
		___removeOnExit_11 = value;
		Il2CppCodeGenWriteBarrier(&___removeOnExit_11, value);
	}

	inline static int32_t get_offset_of_addedComponent_12() { return static_cast<int32_t>(offsetof(AddScript_t95714330, ___addedComponent_12)); }
	inline Component_t3501516275 * get_addedComponent_12() const { return ___addedComponent_12; }
	inline Component_t3501516275 ** get_address_of_addedComponent_12() { return &___addedComponent_12; }
	inline void set_addedComponent_12(Component_t3501516275 * value)
	{
		___addedComponent_12 = value;
		Il2CppCodeGenWriteBarrier(&___addedComponent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
