﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3502354656.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall2972625667.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup4062675100.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList3597236437.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram3749225885.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1017505774.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt1844602099.h"
#include "UnityEngine_UnityEngine_Logger2997509588.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable1289602340.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"
#include "CallNative_U3CModuleU3E86524790.h"
#include "CallNative_CallNative3421361205.h"
#include "CaptureAndSaveLib_U3CModuleU3E86524790.h"
#include "CaptureAndSaveLib_ImageType1125820181.h"
#include "CaptureAndSaveLib_WatermarkAlignment1574127103.h"
#include "CaptureAndSaveLib_CaptureAndSave700313070.h"
#include "CaptureAndSaveLib_CaptureAndSave_U3CSaveToAlbumU3Ec371193057.h"
#include "CaptureAndSaveLib_CaptureAndSave_U3CGetScreenPixel3827175978.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener2941400608.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnEr1516540506.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSu1013330901.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSc3427048564.h"
#include "EasyWebCamLib_U3CModuleU3E86524790.h"
#include "EasyWebCamLib_TBEasyWebCam_EasyWebCamBase3084723642.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStar3886828347.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStop2264447425.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamUpda1731309161.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FocusMode2918518777.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_ResolutionMode805264687.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FlashMode142630673.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_TorchMode3286129949.h"
#include "EasyWebCamLib_RenderListenerUtility2682722690.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_Utilities2464936040.h"
#include "PlayMaker_U3CModuleU3E86524790.h"
#include "PlayMaker_HutongGames_Extensions_RectExtensions1980939226.h"
#include "PlayMaker_HutongGames_PlayMaker_CollisionType1355765302.h"
#include "PlayMaker_HutongGames_PlayMaker_TriggerType3348422332.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseEventType1876570897.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategory2692830870.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHint1985716317.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseButton82682337.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport662142796.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategoryAttri321770322.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionSection3062711417.h"
#include "PlayMaker_HutongGames_PlayMaker_CheckForComponentA4040605154.h"
#include "PlayMaker_HutongGames_PlayMaker_CompoundArrayAttri2338634768.h"
#include "PlayMaker_HutongGames_PlayMaker_HasFloatSliderAttr3315538467.h"
#include "PlayMaker_HutongGames_PlayMaker_HelpUrlAttribute2247792144.h"
#include "PlayMaker_HutongGames_PlayMaker_HideTypeFilter4178726774.h"
#include "PlayMaker_HutongGames_PlayMaker_NoteAttribute294051700.h"
#include "PlayMaker_HutongGames_PlayMaker_ObjectTypeAttribut1425126701.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeFilter3331841584.h"
#include "PlayMaker_HutongGames_PlayMaker_RequiredFieldAttrib353655171.h"
#include "PlayMaker_HutongGames_PlayMaker_TitleAttribute3939375206.h"
#include "PlayMaker_HutongGames_PlayMaker_TooltipAttribute177137691.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHintAttribute149723499.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl1823032379.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride3235106805.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall3279845016.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption964995201.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption_Layou2090144509.h"
#include "PlayMaker_HutongGames_PlayMaker_DebugUtils257587552.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1938906778.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmDebugUtility965175459.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmExecutionStack2677439066.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007.h"
#include "PlayMaker_FsmTemplate1237263802.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmAnimationCurve2685995989.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (UnityEventCallState_t3502354656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[4] = 
{
	UnityEventCallState_t3502354656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (PersistentCall_t2972625667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[5] = 
{
	PersistentCall_t2972625667::get_offset_of_m_Target_0(),
	PersistentCall_t2972625667::get_offset_of_m_MethodName_1(),
	PersistentCall_t2972625667::get_offset_of_m_Mode_2(),
	PersistentCall_t2972625667::get_offset_of_m_Arguments_3(),
	PersistentCall_t2972625667::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (PersistentCallGroup_t4062675100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	PersistentCallGroup_t4062675100::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (InvokableCallList_t3597236437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[4] = 
{
	InvokableCallList_t3597236437::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t3597236437::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t3597236437::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t3597236437::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (UnityEventBase_t1020378628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[4] = 
{
	UnityEventBase_t1020378628::get_offset_of_m_Calls_0(),
	UnityEventBase_t1020378628::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t1020378628::get_offset_of_m_TypeName_2(),
	UnityEventBase_t1020378628::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (UnityEvent_t1266085011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[1] = 
{
	UnityEvent_t1266085011::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (FrameData_t3749225885)+ sizeof (Il2CppObject), sizeof(FrameData_t3749225885_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2410[4] = 
{
	FrameData_t3749225885::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (DefaultValueAttribute_t1017505774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[1] = 
{
	DefaultValueAttribute_t1017505774::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (ExcludeFromDocsAttribute_t1844602099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (Logger_t2997509588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[3] = 
{
	Logger_t2997509588::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t2997509588::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t2997509588::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (RequiredByNativeCodeAttribute_t3165457172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (UsedByNativeCodeAttribute_t3197444790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (FormerlySerializedAsAttribute_t2216353654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[1] = 
{
	FormerlySerializedAsAttribute_t2216353654::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (TypeInferenceRules_t2889237774)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2419[5] = 
{
	TypeInferenceRules_t2889237774::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (TypeInferenceRuleAttribute_t1657757719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[1] = 
{
	TypeInferenceRuleAttribute_t1657757719::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (GenericStack_t931085639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (NetFxCoreExtensions_t2541795172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (AndroidJavaRunnable_t1289602340), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (UnityAction_t594794173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (U3CModuleU3E_t86524799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (CallNative_t3421361205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (U3CModuleU3E_t86524800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (ImageType_t1125820181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2432[3] = 
{
	ImageType_t1125820181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (WatermarkAlignment_t1574127103)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2433[6] = 
{
	WatermarkAlignment_t1574127103::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (CaptureAndSave_t700313070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[5] = 
{
	CaptureAndSave_t700313070::get_offset_of_FILENAME_PREFIX_2(),
	CaptureAndSave_t700313070::get_offset_of_ALBUM_NAME_3(),
	CaptureAndSave_t700313070::get_offset_of_androidImagePath_4(),
	CaptureAndSave_t700313070::get_offset_of_absAlbumPath_5(),
	CaptureAndSave_t700313070::get_offset_of_tex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (U3CSaveToAlbumU3Ec__Iterator0_t371193057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[22] = 
{
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_x_0(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_y_1(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_width_2(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_height_3(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_imgType_4(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CtU3E__0_5(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_path_6(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_saveToGallery_7(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_watermark_8(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_align_9(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U24PC_10(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U24current_11(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Ex_12(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Ey_13(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Ewidth_14(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Eheight_15(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3EimgType_16(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Epath_17(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3EsaveToGallery_18(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Ewatermark_19(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU24U3Ealign_20(),
	U3CSaveToAlbumU3Ec__Iterator0_t371193057::get_offset_of_U3CU3Ef__this_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[19] = 
{
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_imgType_0(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CtFormatU3E__0_1(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_width_2(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_height_3(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_x_4(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_y_5(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_watermark_6(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CtempU3E__1_7(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_align_8(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U24PC_9(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U24current_10(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3EimgType_11(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3Ewidth_12(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3Eheight_13(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3Ex_14(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3Ey_15(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3Ewatermark_16(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU24U3Ealign_17(),
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978::get_offset_of_U3CU3Ef__this_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (CaptureAndSaveEventListener_t2941400608), -1, sizeof(CaptureAndSaveEventListener_t2941400608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2437[3] = 
{
	CaptureAndSaveEventListener_t2941400608_StaticFields::get_offset_of_onErrorInvoker_0(),
	CaptureAndSaveEventListener_t2941400608_StaticFields::get_offset_of_onSuccessInvoker_1(),
	CaptureAndSaveEventListener_t2941400608_StaticFields::get_offset_of_onScreenShotInvoker_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (OnError_t1516540506), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (OnSuccess_t1013330901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (OnScreenShot_t3427048564), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (U3CModuleU3E_t86524801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (EasyWebCamBase_t3084723642), -1, sizeof(EasyWebCamBase_t3084723642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2443[4] = 
{
	EasyWebCamBase_t3084723642_StaticFields::get_offset_of_onEasyWebCamStart_0(),
	EasyWebCamBase_t3084723642_StaticFields::get_offset_of_OnEasyWebCamUpdate_1(),
	EasyWebCamBase_t3084723642_StaticFields::get_offset_of_OnEasyWebCamStoped_2(),
	EasyWebCamBase_t3084723642_StaticFields::get_offset_of_isRunning_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (EasyWebCamStartedDelegate_t3886828347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (EasyWebCamStopedDelegate_t2264447425), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (EasyWebCamUpdateDelegate_t1731309161), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (FocusMode_t2918518777)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[6] = 
{
	FocusMode_t2918518777::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (ResolutionMode_t805264687)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2448[7] = 
{
	ResolutionMode_t805264687::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (FlashMode_t142630673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2449[4] = 
{
	FlashMode_t142630673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (TorchMode_t3286129949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2450[3] = 
{
	TorchMode_t3286129949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (RenderListenerUtility_t2682722690), -1, sizeof(RenderListenerUtility_t2682722690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2451[3] = 
{
	RenderListenerUtility_t2682722690_StaticFields::get_offset_of_onQuit_2(),
	RenderListenerUtility_t2682722690_StaticFields::get_offset_of_onPause_3(),
	RenderListenerUtility_t2682722690_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (Utilities_t2464936040), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (U3CModuleU3E_t86524802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (RectExtensions_t1980939226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (CollisionType_t1355765302)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2457[5] = 
{
	CollisionType_t1355765302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (TriggerType_t3348422332)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2458[4] = 
{
	TriggerType_t3348422332::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (InterpolationType_t930981288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2459[3] = 
{
	InterpolationType_t930981288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (MouseEventType_t1876570897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2460[7] = 
{
	MouseEventType_t1876570897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (ActionCategory_t2692830870)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2461[47] = 
{
	ActionCategory_t2692830870::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (UIHint_t1985716317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2462[31] = 
{
	UIHint_t1985716317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (MouseButton_t82682337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2463[5] = 
{
	MouseButton_t82682337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (LogLevel_t284580066)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2464[4] = 
{
	LogLevel_t284580066::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (ActionHelpers_t1898294297), -1, sizeof(ActionHelpers_t1898294297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2465[5] = 
{
	ActionHelpers_t1898294297_StaticFields::get_offset_of_whiteTexture_0(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickInfo_1(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickRaycastTime_2(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickDistanceUsed_3(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickLayerMaskUsed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (ActionReport_t662142796), -1, sizeof(ActionReport_t662142796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2466[10] = 
{
	ActionReport_t662142796_StaticFields::get_offset_of_ActionReportList_0(),
	ActionReport_t662142796_StaticFields::get_offset_of_InfoCount_1(),
	ActionReport_t662142796_StaticFields::get_offset_of_ErrorCount_2(),
	ActionReport_t662142796::get_offset_of_fsm_3(),
	ActionReport_t662142796::get_offset_of_state_4(),
	ActionReport_t662142796::get_offset_of_action_5(),
	ActionReport_t662142796::get_offset_of_actionIndex_6(),
	ActionReport_t662142796::get_offset_of_logText_7(),
	ActionReport_t662142796::get_offset_of_isError_8(),
	ActionReport_t662142796::get_offset_of_parameter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (ActionCategoryAttribute_t321770322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[1] = 
{
	ActionCategoryAttribute_t321770322::get_offset_of_category_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (ActionSection_t3062711417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[1] = 
{
	ActionSection_t3062711417::get_offset_of_section_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (CheckForComponentAttribute_t4040605154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[3] = 
{
	CheckForComponentAttribute_t4040605154::get_offset_of_type0_0(),
	CheckForComponentAttribute_t4040605154::get_offset_of_type1_1(),
	CheckForComponentAttribute_t4040605154::get_offset_of_type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (CompoundArrayAttribute_t2338634768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[3] = 
{
	CompoundArrayAttribute_t2338634768::get_offset_of_name_0(),
	CompoundArrayAttribute_t2338634768::get_offset_of_firstArrayName_1(),
	CompoundArrayAttribute_t2338634768::get_offset_of_secondArrayName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (HasFloatSliderAttribute_t3315538467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[2] = 
{
	HasFloatSliderAttribute_t3315538467::get_offset_of_minValue_0(),
	HasFloatSliderAttribute_t3315538467::get_offset_of_maxValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (HelpUrlAttribute_t2247792144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[1] = 
{
	HelpUrlAttribute_t2247792144::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (HideTypeFilter_t4178726774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (NoteAttribute_t294051700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[1] = 
{
	NoteAttribute_t294051700::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (ObjectTypeAttribute_t1425126701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[1] = 
{
	ObjectTypeAttribute_t1425126701::get_offset_of_objectType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (VariableTypeFilter_t3331841584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (RequiredFieldAttribute_t353655171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (TitleAttribute_t3939375206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[1] = 
{
	TitleAttribute_t3939375206::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (TooltipAttribute_t177137691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[1] = 
{
	TooltipAttribute_t177137691::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (UIHintAttribute_t149723499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[1] = 
{
	UIHintAttribute_t149723499::get_offset_of_hint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (FsmTemplateControl_t2786508133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[4] = 
{
	FsmTemplateControl_t2786508133::get_offset_of_fsmTemplate_0(),
	FsmTemplateControl_t2786508133::get_offset_of_fsmVarOverrides_1(),
	FsmTemplateControl_t2786508133::get_offset_of_runFsm_2(),
	FsmTemplateControl_t2786508133::get_offset_of_U3CIDU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (U3CU3Ec__DisplayClass2_t1823032379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[1] = 
{
	U3CU3Ec__DisplayClass2_t1823032379::get_offset_of_namedVariable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (FsmEventTarget_t1823904941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[6] = 
{
	FsmEventTarget_t1823904941::get_offset_of_target_0(),
	FsmEventTarget_t1823904941::get_offset_of_excludeSelf_1(),
	FsmEventTarget_t1823904941::get_offset_of_gameObject_2(),
	FsmEventTarget_t1823904941::get_offset_of_fsmName_3(),
	FsmEventTarget_t1823904941::get_offset_of_sendToChildren_4(),
	FsmEventTarget_t1823904941::get_offset_of_fsmComponent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (EventTarget_t3998278035)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2484[8] = 
{
	EventTarget_t3998278035::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (FsmVarOverride_t3235106805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[3] = 
{
	FsmVarOverride_t3235106805::get_offset_of_variable_0(),
	FsmVarOverride_t3235106805::get_offset_of_fsmVar_1(),
	FsmVarOverride_t3235106805::get_offset_of_isEdited_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (FunctionCall_t3279845016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[15] = 
{
	FunctionCall_t3279845016::get_offset_of_FunctionName_0(),
	FunctionCall_t3279845016::get_offset_of_parameterType_1(),
	FunctionCall_t3279845016::get_offset_of_BoolParameter_2(),
	FunctionCall_t3279845016::get_offset_of_FloatParameter_3(),
	FunctionCall_t3279845016::get_offset_of_IntParameter_4(),
	FunctionCall_t3279845016::get_offset_of_GameObjectParameter_5(),
	FunctionCall_t3279845016::get_offset_of_ObjectParameter_6(),
	FunctionCall_t3279845016::get_offset_of_StringParameter_7(),
	FunctionCall_t3279845016::get_offset_of_Vector2Parameter_8(),
	FunctionCall_t3279845016::get_offset_of_Vector3Parameter_9(),
	FunctionCall_t3279845016::get_offset_of_RectParamater_10(),
	FunctionCall_t3279845016::get_offset_of_QuaternionParameter_11(),
	FunctionCall_t3279845016::get_offset_of_MaterialParameter_12(),
	FunctionCall_t3279845016::get_offset_of_TextureParameter_13(),
	FunctionCall_t3279845016::get_offset_of_ColorParameter_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (LayoutOption_t964995201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[3] = 
{
	LayoutOption_t964995201::get_offset_of_option_0(),
	LayoutOption_t964995201::get_offset_of_floatParam_1(),
	LayoutOption_t964995201::get_offset_of_boolParam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (LayoutOptionType_t2090144509)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2488[9] = 
{
	LayoutOptionType_t2090144509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (DebugUtils_t257587552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (DelayedEvent_t1938906778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[6] = 
{
	DelayedEvent_t1938906778::get_offset_of_fsm_0(),
	DelayedEvent_t1938906778::get_offset_of_fsmEvent_1(),
	DelayedEvent_t1938906778::get_offset_of_eventTarget_2(),
	DelayedEvent_t1938906778::get_offset_of_eventData_3(),
	DelayedEvent_t1938906778::get_offset_of_timer_4(),
	DelayedEvent_t1938906778::get_offset_of_eventFired_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (FsmDebugUtility_t965175459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (FsmEventData_t1076900934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[21] = 
{
	FsmEventData_t1076900934::get_offset_of_SentByFsm_0(),
	FsmEventData_t1076900934::get_offset_of_SentByState_1(),
	FsmEventData_t1076900934::get_offset_of_SentByAction_2(),
	FsmEventData_t1076900934::get_offset_of_BoolData_3(),
	FsmEventData_t1076900934::get_offset_of_IntData_4(),
	FsmEventData_t1076900934::get_offset_of_FloatData_5(),
	FsmEventData_t1076900934::get_offset_of_Vector2Data_6(),
	FsmEventData_t1076900934::get_offset_of_Vector3Data_7(),
	FsmEventData_t1076900934::get_offset_of_StringData_8(),
	FsmEventData_t1076900934::get_offset_of_QuaternionData_9(),
	FsmEventData_t1076900934::get_offset_of_RectData_10(),
	FsmEventData_t1076900934::get_offset_of_ColorData_11(),
	FsmEventData_t1076900934::get_offset_of_ObjectData_12(),
	FsmEventData_t1076900934::get_offset_of_GameObjectData_13(),
	FsmEventData_t1076900934::get_offset_of_MaterialData_14(),
	FsmEventData_t1076900934::get_offset_of_TextureData_15(),
	FsmEventData_t1076900934::get_offset_of_Player_16(),
	FsmEventData_t1076900934::get_offset_of_DisconnectionInfo_17(),
	FsmEventData_t1076900934::get_offset_of_ConnectionError_18(),
	FsmEventData_t1076900934::get_offset_of_NetworkMessageInfo_19(),
	FsmEventData_t1076900934::get_offset_of_MasterServerEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (FsmExecutionStack_t2677439066), -1, sizeof(FsmExecutionStack_t2677439066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2493[2] = 
{
	FsmExecutionStack_t2677439066_StaticFields::get_offset_of_fsmExecutionStack_0(),
	FsmExecutionStack_t2677439066_StaticFields::get_offset_of_U3CMaxStackCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (FsmProperty_t3927159007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[22] = 
{
	FsmProperty_t3927159007::get_offset_of_TargetObject_0(),
	FsmProperty_t3927159007::get_offset_of_TargetTypeName_1(),
	FsmProperty_t3927159007::get_offset_of_TargetType_2(),
	FsmProperty_t3927159007::get_offset_of_PropertyName_3(),
	FsmProperty_t3927159007::get_offset_of_PropertyType_4(),
	FsmProperty_t3927159007::get_offset_of_BoolParameter_5(),
	FsmProperty_t3927159007::get_offset_of_FloatParameter_6(),
	FsmProperty_t3927159007::get_offset_of_IntParameter_7(),
	FsmProperty_t3927159007::get_offset_of_GameObjectParameter_8(),
	FsmProperty_t3927159007::get_offset_of_StringParameter_9(),
	FsmProperty_t3927159007::get_offset_of_Vector2Parameter_10(),
	FsmProperty_t3927159007::get_offset_of_Vector3Parameter_11(),
	FsmProperty_t3927159007::get_offset_of_RectParamater_12(),
	FsmProperty_t3927159007::get_offset_of_QuaternionParameter_13(),
	FsmProperty_t3927159007::get_offset_of_ObjectParameter_14(),
	FsmProperty_t3927159007::get_offset_of_MaterialParameter_15(),
	FsmProperty_t3927159007::get_offset_of_TextureParameter_16(),
	FsmProperty_t3927159007::get_offset_of_ColorParameter_17(),
	FsmProperty_t3927159007::get_offset_of_setProperty_18(),
	FsmProperty_t3927159007::get_offset_of_initialized_19(),
	FsmProperty_t3927159007::get_offset_of_targetObjectCached_20(),
	FsmProperty_t3927159007::get_offset_of_memberInfo_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (FsmTemplate_t1237263802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[2] = 
{
	FsmTemplate_t1237263802::get_offset_of_category_2(),
	FsmTemplate_t1237263802::get_offset_of_fsm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (FsmTime_t1076490199), -1, sizeof(FsmTime_t1076490199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2496[4] = 
{
	FsmTime_t1076490199_StaticFields::get_offset_of_firstUpdateHasHappened_0(),
	FsmTime_t1076490199_StaticFields::get_offset_of_totalEditorPlayerPausedTime_1(),
	FsmTime_t1076490199_StaticFields::get_offset_of_realtimeLastUpdate_2(),
	FsmTime_t1076490199_StaticFields::get_offset_of_frameCountLastUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (FsmAnimationCurve_t2685995989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[1] = 
{
	FsmAnimationCurve_t2685995989::get_offset_of_curve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
