﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3178143027.h"
#include "UnityEngine_UnityEngine_LightType1292142182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightType
struct  SetLightType_t1507991894  : public ComponentAction_1_t3178143027
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightType::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// UnityEngine.LightType HutongGames.PlayMaker.Actions.SetLightType::lightType
	int32_t ___lightType_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetLightType_t1507991894, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_lightType_12() { return static_cast<int32_t>(offsetof(SetLightType_t1507991894, ___lightType_12)); }
	inline int32_t get_lightType_12() const { return ___lightType_12; }
	inline int32_t* get_address_of_lightType_12() { return &___lightType_12; }
	inline void set_lightType_12(int32_t value)
	{
		___lightType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
