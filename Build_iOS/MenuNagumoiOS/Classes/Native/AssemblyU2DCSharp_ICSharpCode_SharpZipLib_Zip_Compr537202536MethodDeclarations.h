﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream
struct DeflaterOutputStream_t537202536;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct Deflater_t2454063301;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2454063301.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream)
extern "C"  void DeflaterOutputStream__ctor_m4175761449 (DeflaterOutputStream_t537202536 * __this, Stream_t1561764144 * ___baseOutputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater)
extern "C"  void DeflaterOutputStream__ctor_m1087129776 (DeflaterOutputStream_t537202536 * __this, Stream_t1561764144 * ___baseOutputStream0, Deflater_t2454063301 * ___deflater1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater,System.Int32)
extern "C"  void DeflaterOutputStream__ctor_m50754087 (DeflaterOutputStream_t537202536 * __this, Stream_t1561764144 * ___baseOutputStream0, Deflater_t2454063301 * ___deflater1, int32_t ___bufferSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
extern "C"  void DeflaterOutputStream_Finish_m1956176133 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_IsStreamOwner()
extern "C"  bool DeflaterOutputStream_get_IsStreamOwner_m608178780 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_IsStreamOwner(System.Boolean)
extern "C"  void DeflaterOutputStream_set_IsStreamOwner_m1590097939 (DeflaterOutputStream_t537202536 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanPatchEntries()
extern "C"  bool DeflaterOutputStream_get_CanPatchEntries_m3991332267 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Password()
extern "C"  String_t* DeflaterOutputStream_get_Password_m2535897977 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Password(System.String)
extern "C"  void DeflaterOutputStream_set_Password_m592636664 (DeflaterOutputStream_t537202536 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflaterOutputStream_EncryptBlock_m1433702159 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::InitializePassword(System.String)
extern "C"  void DeflaterOutputStream_InitializePassword_m3851332325 (DeflaterOutputStream_t537202536 * __this, String_t* ___password0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::InitializeAESPassword(ICSharpCode.SharpZipLib.Zip.ZipEntry,System.String,System.Byte[]&,System.Byte[]&)
extern "C"  void DeflaterOutputStream_InitializeAESPassword_m2290404887 (DeflaterOutputStream_t537202536 * __this, ZipEntry_t3141689087 * ___entry0, String_t* ___rawPassword1, ByteU5BU5D_t4260760469** ___salt2, ByteU5BU5D_t4260760469** ___pwdVerifier3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
extern "C"  void DeflaterOutputStream_Deflate_m3010138203 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
extern "C"  bool DeflaterOutputStream_get_CanRead_m882074265 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
extern "C"  bool DeflaterOutputStream_get_CanSeek_m910829307 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
extern "C"  bool DeflaterOutputStream_get_CanWrite_m2097151966 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
extern "C"  int64_t DeflaterOutputStream_get_Length_m2930895024 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
extern "C"  int64_t DeflaterOutputStream_get_Position_m3608040435 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
extern "C"  void DeflaterOutputStream_set_Position_m3905514538 (DeflaterOutputStream_t537202536 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t DeflaterOutputStream_Seek_m2616816948 (DeflaterOutputStream_t537202536 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::SetLength(System.Int64)
extern "C"  void DeflaterOutputStream_SetLength_m2112403434 (DeflaterOutputStream_t537202536 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
extern "C"  int32_t DeflaterOutputStream_ReadByte_m4175247166 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t DeflaterOutputStream_Read_m493863059 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DeflaterOutputStream_BeginRead_m3472481972 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DeflaterOutputStream_BeginWrite_m2745255317 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
extern "C"  void DeflaterOutputStream_Flush_m1679760564 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Close()
extern "C"  void DeflaterOutputStream_Close_m3306672808 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
extern "C"  void DeflaterOutputStream_GetAuthCodeIfAES_m3633276473 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
extern "C"  void DeflaterOutputStream_WriteByte_m3452941332 (DeflaterOutputStream_t537202536 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflaterOutputStream_Write_m3323580986 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
