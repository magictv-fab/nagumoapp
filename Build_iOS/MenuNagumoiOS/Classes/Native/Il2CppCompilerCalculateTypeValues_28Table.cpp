﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184952446.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184952512.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184952599.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907576.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907667.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907578.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146222373790486.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184951580.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184951481.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907541.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221359890752.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907603.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184951642.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907543.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907605.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221359890755.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907609.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221572012639.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907673.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907665.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146222373822015.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184951489.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184952541.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184955296.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146222373755735.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146222373785592.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146223501907789.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184951518.h"
#include "QRCode_U3CPrivateImplementationDetailsU3EU7BA146221184953593.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3E86524790.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySe2197853779.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearCal846303360.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearCa2094571968.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Backgroun2608219151.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearUse945107814.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearUs3502530982.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamReco785660528.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MonoCamer3733412120.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StereoCam2173582563.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullCamer2143544308.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCamer984266456.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTr3340678586.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTarg637796117.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi877546845.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev1437122227.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev3311506418.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev2052521376.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudReco1213605353.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru2270629190.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcess465046465.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable3777854735.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTar1698426226.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlay3307683808.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityP531631577.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeU2903303273.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru3107149249.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstruc147246886.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr3365905913.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear3657261146.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E1319805025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E3010462567.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearIm3622106266.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerra587327548.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2826579942.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr1152863025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbs142368528.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderT1139674014.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_S3337644306.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseL1625730020.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Rectangle2265684451.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Rectangle1840530764.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedB2236164205.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBou90420510.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Behaviour2065637972.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourC877350706.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (__StaticArrayInitTypeSizeU3D224_t1184952446)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D224_t1184952446_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (__StaticArrayInitTypeSizeU3D248_t1184952512)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D248_t1184952512_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (__StaticArrayInitTypeSizeU3D272_t1184952599)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D272_t1184952599_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3501907576)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3501907576_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (__StaticArrayInitTypeSizeU3D52_t3501907667)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D52_t3501907667_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (__StaticArrayInitTypeSizeU3D26_t3501907578)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D26_t3501907578_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (__StaticArrayInitTypeSizeU3D2574_t2373790486)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D2574_t2373790486_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (__StaticArrayInitTypeSizeU3D156_t1184951580)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D156_t1184951580_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (__StaticArrayInitTypeSizeU3D120_t1184951481)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D120_t1184951481_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (__StaticArrayInitTypeSizeU3D10_t3501907541)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (__StaticArrayInitTypeSizeU3D6_t1359890752)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D6_t1359890752_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (__StaticArrayInitTypeSizeU3D30_t3501907603)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D30_t3501907603_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (__StaticArrayInitTypeSizeU3D176_t1184951642)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D176_t1184951642_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (__StaticArrayInitTypeSizeU3D12_t3501907543)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D12_t3501907543_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (__StaticArrayInitTypeSizeU3D32_t3501907605)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D32_t3501907605_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (__StaticArrayInitTypeSizeU3D9_t1359890755)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D9_t1359890755_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (__StaticArrayInitTypeSizeU3D36_t3501907609)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D36_t3501907609_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (__StaticArrayInitTypeSizeU3D11148_t572012639)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D11148_t572012639_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (__StaticArrayInitTypeSizeU3D58_t3501907673)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D58_t3501907673_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (__StaticArrayInitTypeSizeU3D50_t3501907665)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D50_t3501907665_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (__StaticArrayInitTypeSizeU3D3716_t2373822015)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D3716_t2373822015_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (__StaticArrayInitTypeSizeU3D128_t1184951489)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D128_t1184951489_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (__StaticArrayInitTypeSizeU3D256_t1184952541)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D256_t1184952541_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (__StaticArrayInitTypeSizeU3D512_t1184955296)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D512_t1184955296_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (__StaticArrayInitTypeSizeU3D1024_t2373755735)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D1024_t2373755735_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (__StaticArrayInitTypeSizeU3D2048_t2373785592)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D2048_t2373785592_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (__StaticArrayInitTypeSizeU3D90_t3501907789)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D90_t3501907789_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (__StaticArrayInitTypeSizeU3D136_t1184951518)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D136_t1184951518_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (__StaticArrayInitTypeSizeU3D384_t1184953593)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D384_t1184953593_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (U3CModuleU3E_t86524804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (FactorySetter_t2197853779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (EyewearCalibrationProfileManager_t846303360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (EyewearCalibrationProfileManagerImpl_t2094571968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (BackgroundPlaneAbstractBehaviour_t2608219151), -1, sizeof(BackgroundPlaneAbstractBehaviour_t2608219151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2835[11] = 
{
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mVideoBgConfigChanged_5(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mQBehaviour_7(),
	BackgroundPlaneAbstractBehaviour_t2608219151_StaticFields::get_offset_of_maxDisplacement_8(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_defaultNumDivisions_9(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mMesh_10(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mStereoDepth_11(),
	BackgroundPlaneAbstractBehaviour_t2608219151::get_offset_of_mNumDivisions_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (EyewearUserCalibrator_t945107814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (EyewearUserCalibratorImpl_t3502530982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (IOSCamRecoveringHelper_t785660528), -1, sizeof(IOSCamRecoveringHelper_t785660528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2841[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t785660528_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (MonoCameraConfiguration_t3733412120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[12] = 
{
	MonoCameraConfiguration_t3733412120::get_offset_of_mPrimaryCamera_0(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mCameraDeviceMode_1(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mLastVideoBackGroundMirroredFromSDK_2(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mOnVideoBackgroundConfigChanged_3(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mVideoBackgroundBehaviours_4(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mViewportRect_5(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mRenderVideoBackground_6(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mCameraWidthFactor_7(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mViewPortWidth_8(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mViewPortHeight_9(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mProjectionOrientation_10(),
	MonoCameraConfiguration_t3733412120::get_offset_of_mInitialReflection_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (StereoCameraConfiguration_t2173582563), -1, sizeof(StereoCameraConfiguration_t2173582563_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2844[16] = 
{
	0,
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MIN_CENTER_13(),
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MAX_CENTER_14(),
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MAX_BOTTOM_15(),
	StereoCameraConfiguration_t2173582563_StaticFields::get_offset_of_MAX_TOP_16(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mSecondaryCamera_17(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mSkewFrustum_18(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNeedToCheckStereo_19(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mLastAppliedNearClipPlane_20(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mLastAppliedFarClipPlane_21(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mLastAppliedVirtualFoV_22(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNewNearClipPlane_23(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNewFarClipPlane_24(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mNewVirtualFoV_25(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mCameraOffset_26(),
	StereoCameraConfiguration_t2173582563::get_offset_of_mEyewearUserCalibrationProfileId_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (NullCameraConfiguration_t2143544308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	NullCameraConfiguration_t2143544308::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (UnityCameraExtensions_t984266456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (TrackableBehaviour_t4179556250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[7] = 
{
	TrackableBehaviour_t4179556250::get_offset_of_mTrackableName_2(),
	TrackableBehaviour_t4179556250::get_offset_of_mPreviousScale_3(),
	TrackableBehaviour_t4179556250::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t4179556250::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t4179556250::get_offset_of_mStatus_6(),
	TrackableBehaviour_t4179556250::get_offset_of_mTrackable_7(),
	TrackableBehaviour_t4179556250::get_offset_of_mTrackableEventHandlers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (Status_t835151357)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2851[7] = 
{
	Status_t835151357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (DataSetTrackableBehaviour_t3340678586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[11] = 
{
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mDataSetPath_9(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mExtendedTracking_10(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mInitializeSmartTerrain_11(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mReconstructionToInitialize_12(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderBoundsMin_13(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderBoundsMax_14(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mIsSmartTerrainOccluderOffset_15(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderOffset_16(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderRotation_17(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mSmartTerrainOccluderLockedInPlace_18(),
	DataSetTrackableBehaviour_t3340678586::get_offset_of_mAutoSetOccluderFromTargetSize_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (ObjectTargetAbstractBehaviour_t637796117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[7] = 
{
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t637796117::get_offset_of_mPreviewImage_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (CameraDevice_t877546845), -1, sizeof(CameraDevice_t877546845_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2855[1] = 
{
	CameraDevice_t877546845_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (CameraDeviceMode_t134001414)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2856[4] = 
{
	CameraDeviceMode_t134001414::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (FocusMode_t1437122227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2857[6] = 
{
	FocusMode_t1437122227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (CameraDirection_t3311506418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2858[4] = 
{
	CameraDirection_t3311506418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (VideoModeData_t2052521376)+ sizeof (Il2CppObject), sizeof(VideoModeData_t2052521376_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2859[4] = 
{
	VideoModeData_t2052521376::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2052521376::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2052521376::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t2052521376::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (CloudRecoAbstractBehaviour_t1213605353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[11] = 
{
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_SecretKey_10(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_ScanlineColor_11(),
	CloudRecoAbstractBehaviour_t1213605353::get_offset_of_FeaturePointColor_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (ReconstructionImpl_t2270629190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[5] = 
{
	ReconstructionImpl_t2270629190::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t2270629190::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t2270629190::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t2270629190::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t2270629190::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (HideExcessAreaAbstractBehaviour_t465046465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[22] = 
{
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_matteShader_2(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_disableMattes_3(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mBgPlane_4(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mLeftPlane_5(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mRightPlane_6(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mTopPlane_7(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mBottomPlane_8(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mCamera_9(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mSceneIsScaledDown_10(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mBgPlaneLocalPos_11(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mBgPlaneLocalScale_12(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mCameraNearPlane_13(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mCameraPixelRect_14(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mCameraFieldOFView_15(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mVuforiaBehaviour_16(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mHideBehaviours_17(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mDeactivatedHideBehaviours_18(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mPlanesActivated_19(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mLeftPlaneCachedScale_20(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mRightPlaneCachedScale_21(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mBottomPlaneCachedScale_22(),
	HideExcessAreaAbstractBehaviour_t465046465::get_offset_of_mTopPlaneCachedScale_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (TrackableImpl_t3777854735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[2] = 
{
	TrackableImpl_t3777854735::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3777854735::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (ObjectTargetImpl_t1698426226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[2] = 
{
	ObjectTargetImpl_t1698426226::get_offset_of_mSize_2(),
	ObjectTargetImpl_t1698426226::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (UnityPlayer_t3307683808), -1, sizeof(UnityPlayer_t3307683808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2866[1] = 
{
	UnityPlayer_t3307683808_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (NullUnityPlayer_t531631577), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (PlayModeUnityPlayer_t2903303273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (ReconstructionFromTargetImpl_t3107149249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[6] = 
{
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t3107149249::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (ReconstructionFromTargetAbstractBehaviour_t147246886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[2] = 
{
	ReconstructionFromTargetAbstractBehaviour_t147246886::get_offset_of_mReconstructionFromTarget_2(),
	ReconstructionFromTargetAbstractBehaviour_t147246886::get_offset_of_mReconstructionBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (SmartTerrainBuilder_t3365905913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (Eyewear_t3657261146), -1, sizeof(Eyewear_t3657261146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2875[3] = 
{
	Eyewear_t3657261146_StaticFields::get_offset_of_mInstance_0(),
	Eyewear_t3657261146::get_offset_of_mProfileManager_1(),
	Eyewear_t3657261146::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (EyeID_t1319805025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2876[4] = 
{
	EyeID_t1319805025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (EyewearCalibrationReading_t3010462567)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t3010462567_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2877[5] = 
{
	EyewearCalibrationReading_t3010462567::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t3010462567::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (EyewearImpl_t3622106266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (SmartTerrainInitializationInfo_t587327548)+ sizeof (Il2CppObject), sizeof(SmartTerrainInitializationInfo_t587327548_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (SmartTerrainTrackableBehaviour_t2826579942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[4] = 
{
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mSmartTerrainTrackable_9(),
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mDisableAutomaticUpdates_10(),
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mMeshFilterToUpdate_11(),
	SmartTerrainTrackableBehaviour_t2826579942::get_offset_of_mMeshColliderToUpdate_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (SmartTerrainTrackerAbstractBehaviour_t1152863025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[7] = 
{
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mAutoInitTracker_2(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mAutoStartTracker_3(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mAutoInitBuilder_4(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mSceneUnitsToMillimeter_5(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mTrackerStarted_6(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mTrackerWasActiveBeforePause_7(),
	SmartTerrainTrackerAbstractBehaviour_t1152863025::get_offset_of_mTrackerWasActiveBeforeDisabling_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (SurfaceAbstractBehaviour_t142368528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[1] = 
{
	SurfaceAbstractBehaviour_t142368528::get_offset_of_mSurface_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (CylinderTargetAbstractBehaviour_t1139674014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[6] = 
{
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mCylinderTarget_20(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mTopDiameterRatio_21(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mBottomDiameterRatio_22(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mFrameIndex_23(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mUpdateFrameIndex_24(),
	CylinderTargetAbstractBehaviour_t1139674014::get_offset_of_mFutureScale_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (DataSet_t2095838082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (StorageType_t3337644306)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2888[4] = 
{
	StorageType_t3337644306::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (DatabaseLoadAbstractBehaviour_t1625730020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[4] = 
{
	DatabaseLoadAbstractBehaviour_t1625730020::get_offset_of_mDatasetsLoaded_2(),
	DatabaseLoadAbstractBehaviour_t1625730020::get_offset_of_mDataSetsToLoad_3(),
	DatabaseLoadAbstractBehaviour_t1625730020::get_offset_of_mDataSetsToActivate_4(),
	DatabaseLoadAbstractBehaviour_t1625730020::get_offset_of_mExternalDatasetRoots_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (RectangleData_t2265684451)+ sizeof (Il2CppObject), sizeof(RectangleData_t2265684451_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2890[4] = 
{
	RectangleData_t2265684451::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t2265684451::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t2265684451::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t2265684451::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (RectangleIntData_t1840530764)+ sizeof (Il2CppObject), sizeof(RectangleIntData_t1840530764_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2891[4] = 
{
	RectangleIntData_t1840530764::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t1840530764::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t1840530764::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t1840530764::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (OrientedBoundingBox_t2236164205)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox_t2236164205_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2892[3] = 
{
	OrientedBoundingBox_t2236164205::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t2236164205::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t2236164205::get_offset_of_U3CRotationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (OrientedBoundingBox3D_t90420510)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox3D_t90420510_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2893[3] = 
{
	OrientedBoundingBox3D_t90420510::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t90420510::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t90420510::get_offset_of_U3CRotationYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (BehaviourComponentFactory_t2065637972), -1, sizeof(BehaviourComponentFactory_t2065637972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2896[1] = 
{
	BehaviourComponentFactory_t2065637972_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (NullBehaviourComponentFactory_t877350706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
