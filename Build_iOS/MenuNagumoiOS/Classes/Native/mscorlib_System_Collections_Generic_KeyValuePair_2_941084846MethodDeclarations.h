﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1616363641(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t941084846 *, int32_t, OnChangeAnToPerspectiveEventHandler_t2702236419 *, const MethodInfo*))KeyValuePair_2__ctor_m1794069694_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_Key()
#define KeyValuePair_2_get_Key_m2594301647(__this, method) ((  int32_t (*) (KeyValuePair_2_t941084846 *, const MethodInfo*))KeyValuePair_2_get_Key_m1012642026_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m568160784(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t941084846 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m892438187_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_Value()
#define KeyValuePair_2_get_Value_m3007329715(__this, method) ((  OnChangeAnToPerspectiveEventHandler_t2702236419 * (*) (KeyValuePair_2_t941084846 *, const MethodInfo*))KeyValuePair_2_get_Value_m3824131982_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m655441424(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t941084846 *, OnChangeAnToPerspectiveEventHandler_t2702236419 *, const MethodInfo*))KeyValuePair_2_set_Value_m2462289195_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::ToString()
#define KeyValuePair_2_ToString_m1065773048(__this, method) ((  String_t* (*) (KeyValuePair_2_t941084846 *, const MethodInfo*))KeyValuePair_2_ToString_m4140516093_gshared)(__this, method)
