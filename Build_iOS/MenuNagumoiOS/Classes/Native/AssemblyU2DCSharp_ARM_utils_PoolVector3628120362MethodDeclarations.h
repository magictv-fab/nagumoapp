﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.PoolVector3
struct PoolVector3_t628120362;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.utils.PoolVector3::.ctor(System.UInt32)
extern "C"  void PoolVector3__ctor_m3247546932 (PoolVector3_t628120362 * __this, uint32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.PoolVector3::addItem(UnityEngine.Vector3)
extern "C"  void PoolVector3_addItem_m3178255745 (PoolVector3_t628120362 * __this, Vector3_t4282066566  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.PoolVector3::getCurrentSize()
extern "C"  int32_t PoolVector3_getCurrentSize_m2695390388 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.PoolVector3::isFull()
extern "C"  bool PoolVector3_isFull_m1798759427 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.PoolVector3::reset()
extern "C"  void PoolVector3_reset_m1470966611 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] ARM.utils.PoolVector3::getItens()
extern "C"  Vector3U5BU5D_t215400611* PoolVector3_getItens_m1769602671 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.PoolVector3::getMedian()
extern "C"  Vector3_t4282066566  PoolVector3_getMedian_m2620576574 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.PoolVector3::getMean()
extern "C"  Vector3_t4282066566  PoolVector3_getMean_m3055151097 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.PoolVector3::getLastPosition()
extern "C"  Vector3_t4282066566  PoolVector3_getLastPosition_m3463851155 (PoolVector3_t628120362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] ARM.utils.PoolVector3::getLastsPosition(System.UInt32)
extern "C"  Vector3U5BU5D_t215400611* PoolVector3_getLastsPosition_m586024452 (PoolVector3_t628120362 * __this, uint32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
