﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.HasFloatSliderAttribute
struct HasFloatSliderAttribute_t3315538467;

#include "codegen/il2cpp-codegen.h"

// System.Single HutongGames.PlayMaker.HasFloatSliderAttribute::get_MinValue()
extern "C"  float HasFloatSliderAttribute_get_MinValue_m3478778084 (HasFloatSliderAttribute_t3315538467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.HasFloatSliderAttribute::get_MaxValue()
extern "C"  float HasFloatSliderAttribute_get_MaxValue_m1006778770 (HasFloatSliderAttribute_t3315538467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.HasFloatSliderAttribute::.ctor(System.Single,System.Single)
extern "C"  void HasFloatSliderAttribute__ctor_m395584692 (HasFloatSliderAttribute_t3315538467 * __this, float ___minValue0, float ___maxValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
