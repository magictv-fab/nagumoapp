﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.request.ServerRequestDeviceVO
struct  ServerRequestDeviceVO_t3297042857  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.request.ServerRequestDeviceVO::jsonrpc
	String_t* ___jsonrpc_0;
	// System.Int32 MagicTV.vo.request.ServerRequestDeviceVO::id
	int32_t ___id_1;
	// System.Boolean MagicTV.vo.request.ServerRequestDeviceVO::result
	bool ___result_2;

public:
	inline static int32_t get_offset_of_jsonrpc_0() { return static_cast<int32_t>(offsetof(ServerRequestDeviceVO_t3297042857, ___jsonrpc_0)); }
	inline String_t* get_jsonrpc_0() const { return ___jsonrpc_0; }
	inline String_t** get_address_of_jsonrpc_0() { return &___jsonrpc_0; }
	inline void set_jsonrpc_0(String_t* value)
	{
		___jsonrpc_0 = value;
		Il2CppCodeGenWriteBarrier(&___jsonrpc_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ServerRequestDeviceVO_t3297042857, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(ServerRequestDeviceVO_t3297042857, ___result_2)); }
	inline bool get_result_2() const { return ___result_2; }
	inline bool* get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(bool value)
	{
		___result_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
