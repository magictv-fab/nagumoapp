﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TesteEasyTimer
struct TesteEasyTimer_t1834723248;

#include "codegen/il2cpp-codegen.h"

// System.Void TesteEasyTimer::.ctor()
extern "C"  void TesteEasyTimer__ctor_m2858666731 (TesteEasyTimer_t1834723248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteEasyTimer::Start()
extern "C"  void TesteEasyTimer_Start_m1805804523 (TesteEasyTimer_t1834723248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteEasyTimer::teste()
extern "C"  void TesteEasyTimer_teste_m615886204 (TesteEasyTimer_t1834723248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteEasyTimer::Update()
extern "C"  void TesteEasyTimer_Update_m151217538 (TesteEasyTimer_t1834723248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
