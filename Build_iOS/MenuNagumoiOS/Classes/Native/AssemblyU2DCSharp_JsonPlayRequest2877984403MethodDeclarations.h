﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonPlayRequest
struct JsonPlayRequest_t2877984403;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonPlayRequest::.ctor()
extern "C"  void JsonPlayRequest__ctor_m2227760696 (JsonPlayRequest_t2877984403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
