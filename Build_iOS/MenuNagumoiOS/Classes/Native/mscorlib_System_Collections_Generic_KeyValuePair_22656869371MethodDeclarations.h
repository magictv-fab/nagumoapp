﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4068774668_gshared (KeyValuePair_2_t2656869371 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m4068774668(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2656869371 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m4068774668_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m548895580_gshared (KeyValuePair_2_t2656869371 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m548895580(__this, method) ((  int32_t (*) (KeyValuePair_2_t2656869371 *, const MethodInfo*))KeyValuePair_2_get_Key_m548895580_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3592818589_gshared (KeyValuePair_2_t2656869371 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3592818589(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2656869371 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3592818589_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2031527964_gshared (KeyValuePair_2_t2656869371 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2031527964(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2656869371 *, const MethodInfo*))KeyValuePair_2_get_Value_m2031527964_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m596004125_gshared (KeyValuePair_2_t2656869371 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m596004125(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2656869371 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m596004125_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m637364773_gshared (KeyValuePair_2_t2656869371 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m637364773(__this, method) ((  String_t* (*) (KeyValuePair_2_t2656869371 *, const MethodInfo*))KeyValuePair_2_ToString_m637364773_gshared)(__this, method)
