﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatSwitch
struct  FloatSwitch_t2078594878  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSwitch::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_9;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.FloatSwitch::lessThan
	FsmFloatU5BU5D_t2945380875* ___lessThan_10;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.FloatSwitch::sendEvent
	FsmEventU5BU5D_t2862142229* ___sendEvent_11;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSwitch::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_floatVariable_9() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___floatVariable_9)); }
	inline FsmFloat_t2134102846 * get_floatVariable_9() const { return ___floatVariable_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_9() { return &___floatVariable_9; }
	inline void set_floatVariable_9(FsmFloat_t2134102846 * value)
	{
		___floatVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_9, value);
	}

	inline static int32_t get_offset_of_lessThan_10() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___lessThan_10)); }
	inline FsmFloatU5BU5D_t2945380875* get_lessThan_10() const { return ___lessThan_10; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_lessThan_10() { return &___lessThan_10; }
	inline void set_lessThan_10(FsmFloatU5BU5D_t2945380875* value)
	{
		___lessThan_10 = value;
		Il2CppCodeGenWriteBarrier(&___lessThan_10, value);
	}

	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___sendEvent_11)); }
	inline FsmEventU5BU5D_t2862142229* get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmEventU5BU5D_t2862142229* value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
