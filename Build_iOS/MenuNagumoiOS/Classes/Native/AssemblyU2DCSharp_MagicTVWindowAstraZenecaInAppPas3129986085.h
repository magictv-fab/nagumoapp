﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;
// System.Action
struct Action_t3771233898;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowAstraZenecaInAppPassword
struct  MagicTVWindowAstraZenecaInAppPassword_t3129986085  : public GenericWindowControllerScript_t248075822
{
public:
	// UnityEngine.UI.InputField MagicTVWindowAstraZenecaInAppPassword::LandscapePasswordInput
	InputField_t609046876 * ___LandscapePasswordInput_4;
	// UnityEngine.UI.InputField MagicTVWindowAstraZenecaInAppPassword::PortraitPasswordInput
	InputField_t609046876 * ___PortraitPasswordInput_5;
	// System.Action MagicTVWindowAstraZenecaInAppPassword::onClickLogin
	Action_t3771233898 * ___onClickLogin_6;
	// System.String MagicTVWindowAstraZenecaInAppPassword::PasswordValue
	String_t* ___PasswordValue_7;

public:
	inline static int32_t get_offset_of_LandscapePasswordInput_4() { return static_cast<int32_t>(offsetof(MagicTVWindowAstraZenecaInAppPassword_t3129986085, ___LandscapePasswordInput_4)); }
	inline InputField_t609046876 * get_LandscapePasswordInput_4() const { return ___LandscapePasswordInput_4; }
	inline InputField_t609046876 ** get_address_of_LandscapePasswordInput_4() { return &___LandscapePasswordInput_4; }
	inline void set_LandscapePasswordInput_4(InputField_t609046876 * value)
	{
		___LandscapePasswordInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___LandscapePasswordInput_4, value);
	}

	inline static int32_t get_offset_of_PortraitPasswordInput_5() { return static_cast<int32_t>(offsetof(MagicTVWindowAstraZenecaInAppPassword_t3129986085, ___PortraitPasswordInput_5)); }
	inline InputField_t609046876 * get_PortraitPasswordInput_5() const { return ___PortraitPasswordInput_5; }
	inline InputField_t609046876 ** get_address_of_PortraitPasswordInput_5() { return &___PortraitPasswordInput_5; }
	inline void set_PortraitPasswordInput_5(InputField_t609046876 * value)
	{
		___PortraitPasswordInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___PortraitPasswordInput_5, value);
	}

	inline static int32_t get_offset_of_onClickLogin_6() { return static_cast<int32_t>(offsetof(MagicTVWindowAstraZenecaInAppPassword_t3129986085, ___onClickLogin_6)); }
	inline Action_t3771233898 * get_onClickLogin_6() const { return ___onClickLogin_6; }
	inline Action_t3771233898 ** get_address_of_onClickLogin_6() { return &___onClickLogin_6; }
	inline void set_onClickLogin_6(Action_t3771233898 * value)
	{
		___onClickLogin_6 = value;
		Il2CppCodeGenWriteBarrier(&___onClickLogin_6, value);
	}

	inline static int32_t get_offset_of_PasswordValue_7() { return static_cast<int32_t>(offsetof(MagicTVWindowAstraZenecaInAppPassword_t3129986085, ___PasswordValue_7)); }
	inline String_t* get_PasswordValue_7() const { return ___PasswordValue_7; }
	inline String_t** get_address_of_PasswordValue_7() { return &___PasswordValue_7; }
	inline void set_PasswordValue_7(String_t* value)
	{
		___PasswordValue_7 = value;
		Il2CppCodeGenWriteBarrier(&___PasswordValue_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
