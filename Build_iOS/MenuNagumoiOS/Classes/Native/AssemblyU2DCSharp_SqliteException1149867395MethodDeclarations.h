﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SqliteException
struct SqliteException_t1149867395;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void SqliteException::.ctor(System.String)
extern "C"  void SqliteException__ctor_m3898982074 (SqliteException_t1149867395 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
