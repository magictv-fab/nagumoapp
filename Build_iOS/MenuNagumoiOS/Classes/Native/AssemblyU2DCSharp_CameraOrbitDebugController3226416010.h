﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraOrbitDebugController
struct  CameraOrbitDebugController_t3226416010  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 CameraOrbitDebugController::orbitPivot
	Vector3_t4282066566  ___orbitPivot_2;
	// System.Single CameraOrbitDebugController::upAngle
	float ___upAngle_3;
	// System.Single CameraOrbitDebugController::leftAngle
	float ___leftAngle_4;
	// System.Single CameraOrbitDebugController::backAngle
	float ___backAngle_5;

public:
	inline static int32_t get_offset_of_orbitPivot_2() { return static_cast<int32_t>(offsetof(CameraOrbitDebugController_t3226416010, ___orbitPivot_2)); }
	inline Vector3_t4282066566  get_orbitPivot_2() const { return ___orbitPivot_2; }
	inline Vector3_t4282066566 * get_address_of_orbitPivot_2() { return &___orbitPivot_2; }
	inline void set_orbitPivot_2(Vector3_t4282066566  value)
	{
		___orbitPivot_2 = value;
	}

	inline static int32_t get_offset_of_upAngle_3() { return static_cast<int32_t>(offsetof(CameraOrbitDebugController_t3226416010, ___upAngle_3)); }
	inline float get_upAngle_3() const { return ___upAngle_3; }
	inline float* get_address_of_upAngle_3() { return &___upAngle_3; }
	inline void set_upAngle_3(float value)
	{
		___upAngle_3 = value;
	}

	inline static int32_t get_offset_of_leftAngle_4() { return static_cast<int32_t>(offsetof(CameraOrbitDebugController_t3226416010, ___leftAngle_4)); }
	inline float get_leftAngle_4() const { return ___leftAngle_4; }
	inline float* get_address_of_leftAngle_4() { return &___leftAngle_4; }
	inline void set_leftAngle_4(float value)
	{
		___leftAngle_4 = value;
	}

	inline static int32_t get_offset_of_backAngle_5() { return static_cast<int32_t>(offsetof(CameraOrbitDebugController_t3226416010, ___backAngle_5)); }
	inline float get_backAngle_5() const { return ___backAngle_5; }
	inline float* get_address_of_backAngle_5() { return &___backAngle_5; }
	inline void set_backAngle_5(float value)
	{
		___backAngle_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
