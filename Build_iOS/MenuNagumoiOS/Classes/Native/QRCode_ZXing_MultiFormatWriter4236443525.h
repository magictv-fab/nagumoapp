﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>
struct IDictionary_2_t587491918;
// System.Func`1<ZXing.Writer>
struct Func_1_t3890737231;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.MultiFormatWriter
struct  MultiFormatWriter_t4236443525  : public Il2CppObject
{
public:

public:
};

struct MultiFormatWriter_t4236443525_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>> ZXing.MultiFormatWriter::formatMap
	Il2CppObject* ___formatMap_0;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegatee
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegatef
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate10
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate11
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate12
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate13
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate14
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate15
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate16
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate17
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate18
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate19
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12;
	// System.Func`1<ZXing.Writer> ZXing.MultiFormatWriter::CS$<>9__CachedAnonymousMethodDelegate1a
	Func_1_t3890737231 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13;

public:
	inline static int32_t get_offset_of_formatMap_0() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___formatMap_0)); }
	inline Il2CppObject* get_formatMap_0() const { return ___formatMap_0; }
	inline Il2CppObject** get_address_of_formatMap_0() { return &___formatMap_0; }
	inline void set_formatMap_0(Il2CppObject* value)
	{
		___formatMap_0 = value;
		Il2CppCodeGenWriteBarrier(&___formatMap_0, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegatee_1, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegatef_2, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate10_3, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate11_4, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate12_5, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate13_6, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate14_7, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate15_8, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate16_9, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate17_10, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate18_11, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate19_12, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13() { return static_cast<int32_t>(offsetof(MultiFormatWriter_t4236443525_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13)); }
	inline Func_1_t3890737231 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13; }
	inline Func_1_t3890737231 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13(Func_1_t3890737231 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate1a_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
