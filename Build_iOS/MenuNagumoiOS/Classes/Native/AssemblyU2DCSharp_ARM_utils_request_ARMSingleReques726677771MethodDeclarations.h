﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler
struct OnStartErrorEventHandler_t726677771;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnStartErrorEventHandler__ctor_m458954082 (OnStartErrorEventHandler_t726677771 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler::Invoke(System.String)
extern "C"  void OnStartErrorEventHandler_Invoke_m1369253958 (OnStartErrorEventHandler_t726677771 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnStartErrorEventHandler_BeginInvoke_m3312191827 (OnStartErrorEventHandler_t726677771 * __this, String_t* ___message0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnStartErrorEventHandler_EndInvoke_m1757710450 (OnStartErrorEventHandler_t726677771 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
