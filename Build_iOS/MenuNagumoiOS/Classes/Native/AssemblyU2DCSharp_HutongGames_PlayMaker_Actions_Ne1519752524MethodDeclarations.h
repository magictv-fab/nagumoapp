﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties
struct NetworkGetOnFailedToConnectProperties_t1519752524;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::.ctor()
extern "C"  void NetworkGetOnFailedToConnectProperties__ctor_m389986666 (NetworkGetOnFailedToConnectProperties_t1519752524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::Reset()
extern "C"  void NetworkGetOnFailedToConnectProperties_Reset_m2331386903 (NetworkGetOnFailedToConnectProperties_t1519752524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::OnEnter()
extern "C"  void NetworkGetOnFailedToConnectProperties_OnEnter_m506759873 (NetworkGetOnFailedToConnectProperties_t1519752524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::doGetNetworkErrorInfo()
extern "C"  void NetworkGetOnFailedToConnectProperties_doGetNetworkErrorInfo_m3388580123 (NetworkGetOnFailedToConnectProperties_t1519752524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
