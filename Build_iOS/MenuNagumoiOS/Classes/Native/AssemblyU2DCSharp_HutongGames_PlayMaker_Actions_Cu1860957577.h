﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2975001167.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveVector3
struct  CurveVector3_t1860957577  : public CurveFsmAction_t2975001167
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::vectorVariable
	FsmVector3_t533912882 * ___vectorVariable_33;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::fromValue
	FsmVector3_t533912882 * ___fromValue_34;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::toValue
	FsmVector3_t533912882 * ___toValue_35;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveX
	FsmAnimationCurve_t2685995989 * ___curveX_36;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationX
	int32_t ___calculationX_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveY
	FsmAnimationCurve_t2685995989 * ___curveY_38;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationY
	int32_t ___calculationY_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveZ
	FsmAnimationCurve_t2685995989 * ___curveZ_40;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationZ
	int32_t ___calculationZ_41;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.CurveVector3::vct
	Vector3_t4282066566  ___vct_42;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveVector3::finishInNextStep
	bool ___finishInNextStep_43;

public:
	inline static int32_t get_offset_of_vectorVariable_33() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___vectorVariable_33)); }
	inline FsmVector3_t533912882 * get_vectorVariable_33() const { return ___vectorVariable_33; }
	inline FsmVector3_t533912882 ** get_address_of_vectorVariable_33() { return &___vectorVariable_33; }
	inline void set_vectorVariable_33(FsmVector3_t533912882 * value)
	{
		___vectorVariable_33 = value;
		Il2CppCodeGenWriteBarrier(&___vectorVariable_33, value);
	}

	inline static int32_t get_offset_of_fromValue_34() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___fromValue_34)); }
	inline FsmVector3_t533912882 * get_fromValue_34() const { return ___fromValue_34; }
	inline FsmVector3_t533912882 ** get_address_of_fromValue_34() { return &___fromValue_34; }
	inline void set_fromValue_34(FsmVector3_t533912882 * value)
	{
		___fromValue_34 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_34, value);
	}

	inline static int32_t get_offset_of_toValue_35() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___toValue_35)); }
	inline FsmVector3_t533912882 * get_toValue_35() const { return ___toValue_35; }
	inline FsmVector3_t533912882 ** get_address_of_toValue_35() { return &___toValue_35; }
	inline void set_toValue_35(FsmVector3_t533912882 * value)
	{
		___toValue_35 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_35, value);
	}

	inline static int32_t get_offset_of_curveX_36() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___curveX_36)); }
	inline FsmAnimationCurve_t2685995989 * get_curveX_36() const { return ___curveX_36; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveX_36() { return &___curveX_36; }
	inline void set_curveX_36(FsmAnimationCurve_t2685995989 * value)
	{
		___curveX_36 = value;
		Il2CppCodeGenWriteBarrier(&___curveX_36, value);
	}

	inline static int32_t get_offset_of_calculationX_37() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___calculationX_37)); }
	inline int32_t get_calculationX_37() const { return ___calculationX_37; }
	inline int32_t* get_address_of_calculationX_37() { return &___calculationX_37; }
	inline void set_calculationX_37(int32_t value)
	{
		___calculationX_37 = value;
	}

	inline static int32_t get_offset_of_curveY_38() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___curveY_38)); }
	inline FsmAnimationCurve_t2685995989 * get_curveY_38() const { return ___curveY_38; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveY_38() { return &___curveY_38; }
	inline void set_curveY_38(FsmAnimationCurve_t2685995989 * value)
	{
		___curveY_38 = value;
		Il2CppCodeGenWriteBarrier(&___curveY_38, value);
	}

	inline static int32_t get_offset_of_calculationY_39() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___calculationY_39)); }
	inline int32_t get_calculationY_39() const { return ___calculationY_39; }
	inline int32_t* get_address_of_calculationY_39() { return &___calculationY_39; }
	inline void set_calculationY_39(int32_t value)
	{
		___calculationY_39 = value;
	}

	inline static int32_t get_offset_of_curveZ_40() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___curveZ_40)); }
	inline FsmAnimationCurve_t2685995989 * get_curveZ_40() const { return ___curveZ_40; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveZ_40() { return &___curveZ_40; }
	inline void set_curveZ_40(FsmAnimationCurve_t2685995989 * value)
	{
		___curveZ_40 = value;
		Il2CppCodeGenWriteBarrier(&___curveZ_40, value);
	}

	inline static int32_t get_offset_of_calculationZ_41() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___calculationZ_41)); }
	inline int32_t get_calculationZ_41() const { return ___calculationZ_41; }
	inline int32_t* get_address_of_calculationZ_41() { return &___calculationZ_41; }
	inline void set_calculationZ_41(int32_t value)
	{
		___calculationZ_41 = value;
	}

	inline static int32_t get_offset_of_vct_42() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___vct_42)); }
	inline Vector3_t4282066566  get_vct_42() const { return ___vct_42; }
	inline Vector3_t4282066566 * get_address_of_vct_42() { return &___vct_42; }
	inline void set_vct_42(Vector3_t4282066566  value)
	{
		___vct_42 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_43() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___finishInNextStep_43)); }
	inline bool get_finishInNextStep_43() const { return ___finishInNextStep_43; }
	inline bool* get_address_of_finishInNextStep_43() { return &___finishInNextStep_43; }
	inline void set_finishInNextStep_43(bool value)
	{
		___finishInNextStep_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
