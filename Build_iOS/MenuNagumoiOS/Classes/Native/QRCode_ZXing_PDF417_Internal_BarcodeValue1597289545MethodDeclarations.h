﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.BarcodeValue
struct BarcodeValue_t1597289545;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.PDF417.Internal.BarcodeValue::setValue(System.Int32)
extern "C"  void BarcodeValue_setValue_m2942752335 (BarcodeValue_t1597289545 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.BarcodeValue::getValue()
extern "C"  Int32U5BU5D_t3230847821* BarcodeValue_getValue_m4231949878 (BarcodeValue_t1597289545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeValue::.ctor()
extern "C"  void BarcodeValue__ctor_m2396769269 (BarcodeValue_t1597289545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
