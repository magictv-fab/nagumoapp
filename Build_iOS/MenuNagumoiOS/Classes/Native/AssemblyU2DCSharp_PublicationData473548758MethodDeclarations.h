﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublicationData
struct PublicationData_t473548758;

#include "codegen/il2cpp-codegen.h"

// System.Void PublicationData::.ctor()
extern "C"  void PublicationData__ctor_m2992795733 (PublicationData_t473548758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
