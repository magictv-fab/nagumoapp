﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCEANExtension5Support
struct UPCEANExtension5Support_t3604833281;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>
struct IDictionary_2_t2712902339;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_String7231557.h"

// ZXing.Result ZXing.OneD.UPCEANExtension5Support::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[])
extern "C"  Result_t2610723219 * UPCEANExtension5Support_decodeRow_m390251735 (UPCEANExtension5Support_t3604833281 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Int32U5BU5D_t3230847821* ___extensionStartRange2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.UPCEANExtension5Support::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern "C"  int32_t UPCEANExtension5Support_decodeMiddle_m2743509615 (UPCEANExtension5Support_t3604833281 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___startRange1, StringBuilder_t243639308 * ___resultString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.UPCEANExtension5Support::extensionChecksum(System.String)
extern "C"  int32_t UPCEANExtension5Support_extensionChecksum_m2027201278 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.UPCEANExtension5Support::determineCheckDigit(System.Int32,System.Int32&)
extern "C"  bool UPCEANExtension5Support_determineCheckDigit_m66255706 (Il2CppObject * __this /* static, unused */, int32_t ___lgPatternFound0, int32_t* ___checkDigit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.OneD.UPCEANExtension5Support::parseExtensionString(System.String)
extern "C"  Il2CppObject* UPCEANExtension5Support_parseExtensionString_m1581220728 (Il2CppObject * __this /* static, unused */, String_t* ___raw0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.UPCEANExtension5Support::parseExtension5String(System.String)
extern "C"  String_t* UPCEANExtension5Support_parseExtension5String_m3439644571 (Il2CppObject * __this /* static, unused */, String_t* ___raw0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANExtension5Support::.ctor()
extern "C"  void UPCEANExtension5Support__ctor_m419669426 (UPCEANExtension5Support_t3604833281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEANExtension5Support::.cctor()
extern "C"  void UPCEANExtension5Support__cctor_m3937721403 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
