﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.OneDimensionalCodeWriter
struct OneDimensionalCodeWriter_t4068326409;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * OneDimensionalCodeWriter_encode_m320371820 (OneDimensionalCodeWriter_t4068326409 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::renderResult(System.Boolean[],System.Int32,System.Int32,System.Int32)
extern "C"  BitMatrix_t1058711404 * OneDimensionalCodeWriter_renderResult_m4044188493 (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3456302923* ___code0, int32_t ___width1, int32_t ___height2, int32_t ___sidesMargin3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.OneDimensionalCodeWriter::appendPattern(System.Boolean[],System.Int32,System.Int32[],System.Boolean)
extern "C"  int32_t OneDimensionalCodeWriter_appendPattern_m3647846254 (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3456302923* ___target0, int32_t ___pos1, Int32U5BU5D_t3230847821* ___pattern2, bool ___startColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.OneDimensionalCodeWriter::get_DefaultMargin()
extern "C"  int32_t OneDimensionalCodeWriter_get_DefaultMargin_m2332336332 (OneDimensionalCodeWriter_t4068326409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.OneDimensionalCodeWriter::CalculateChecksumDigitModulo10(System.String)
extern "C"  String_t* OneDimensionalCodeWriter_CalculateChecksumDigitModulo10_m1743288546 (Il2CppObject * __this /* static, unused */, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.OneDimensionalCodeWriter::.ctor()
extern "C"  void OneDimensionalCodeWriter__ctor_m3112182842 (OneDimensionalCodeWriter_t4068326409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
