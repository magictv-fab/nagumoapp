﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MinhasOfertasManager/<LoadLoadeds>c__Iterator20
struct U3CLoadLoadedsU3Ec__Iterator20_t3716337497;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MinhasOfertasManager/<LoadLoadeds>c__Iterator20::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator20__ctor_m1844481074 (U3CLoadLoadedsU3Ec__Iterator20_t3716337497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<LoadLoadeds>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2631625824 (U3CLoadLoadedsU3Ec__Iterator20_t3716337497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<LoadLoadeds>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m2284990452 (U3CLoadLoadedsU3Ec__Iterator20_t3716337497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MinhasOfertasManager/<LoadLoadeds>c__Iterator20::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator20_MoveNext_m3425154178 (U3CLoadLoadedsU3Ec__Iterator20_t3716337497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<LoadLoadeds>c__Iterator20::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator20_Dispose_m2918069231 (U3CLoadLoadedsU3Ec__Iterator20_t3716337497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<LoadLoadeds>c__Iterator20::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator20_Reset_m3785881311 (U3CLoadLoadedsU3Ec__Iterator20_t3716337497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
