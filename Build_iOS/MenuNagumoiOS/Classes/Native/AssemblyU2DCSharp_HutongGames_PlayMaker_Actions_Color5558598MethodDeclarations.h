﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ColorInterpolate
struct ColorInterpolate_t5558598;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ColorInterpolate::.ctor()
extern "C"  void ColorInterpolate__ctor_m4153679840 (ColorInterpolate_t5558598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorInterpolate::Reset()
extern "C"  void ColorInterpolate_Reset_m1800112781 (ColorInterpolate_t5558598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorInterpolate::OnEnter()
extern "C"  void ColorInterpolate_OnEnter_m1053436855 (ColorInterpolate_t5558598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorInterpolate::OnUpdate()
extern "C"  void ColorInterpolate_OnUpdate_m1725330796 (ColorInterpolate_t5558598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.ColorInterpolate::ErrorCheck()
extern "C"  String_t* ColorInterpolate_ErrorCheck_m3250411175 (ColorInterpolate_t5558598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
