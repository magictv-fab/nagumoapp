﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString
struct  ZipString_t3473946306  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::comment_
	String_t* ___comment__0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::rawComment_
	ByteU5BU5D_t4260760469* ___rawComment__1;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::isSourceString_
	bool ___isSourceString__2;

public:
	inline static int32_t get_offset_of_comment__0() { return static_cast<int32_t>(offsetof(ZipString_t3473946306, ___comment__0)); }
	inline String_t* get_comment__0() const { return ___comment__0; }
	inline String_t** get_address_of_comment__0() { return &___comment__0; }
	inline void set_comment__0(String_t* value)
	{
		___comment__0 = value;
		Il2CppCodeGenWriteBarrier(&___comment__0, value);
	}

	inline static int32_t get_offset_of_rawComment__1() { return static_cast<int32_t>(offsetof(ZipString_t3473946306, ___rawComment__1)); }
	inline ByteU5BU5D_t4260760469* get_rawComment__1() const { return ___rawComment__1; }
	inline ByteU5BU5D_t4260760469** get_address_of_rawComment__1() { return &___rawComment__1; }
	inline void set_rawComment__1(ByteU5BU5D_t4260760469* value)
	{
		___rawComment__1 = value;
		Il2CppCodeGenWriteBarrier(&___rawComment__1, value);
	}

	inline static int32_t get_offset_of_isSourceString__2() { return static_cast<int32_t>(offsetof(ZipString_t3473946306, ___isSourceString__2)); }
	inline bool get_isSourceString__2() const { return ___isSourceString__2; }
	inline bool* get_address_of_isSourceString__2() { return &___isSourceString__2; }
	inline void set_isSourceString__2(bool value)
	{
		___isSourceString__2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
