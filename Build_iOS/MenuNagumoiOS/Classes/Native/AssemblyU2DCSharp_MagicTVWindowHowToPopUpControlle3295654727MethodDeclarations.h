﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowHowToPopUpControllerScript
struct MagicTVWindowHowToPopUpControllerScript_t3295654727;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowHowToPopUpControllerScript::.ctor()
extern "C"  void MagicTVWindowHowToPopUpControllerScript__ctor_m209427460 (MagicTVWindowHowToPopUpControllerScript_t3295654727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTVWindowHowToPopUpControllerScript::get_DontShowAnymore()
extern "C"  bool MagicTVWindowHowToPopUpControllerScript_get_DontShowAnymore_m808788704 (MagicTVWindowHowToPopUpControllerScript_t3295654727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
