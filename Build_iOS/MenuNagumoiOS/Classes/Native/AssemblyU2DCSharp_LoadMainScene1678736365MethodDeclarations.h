﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadMainScene
struct LoadMainScene_t1678736365;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void LoadMainScene::.ctor()
extern "C"  void LoadMainScene__ctor_m350421022 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::.cctor()
extern "C"  void LoadMainScene__cctor_m1791020879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::Start()
extern "C"  void LoadMainScene_Start_m3592526110 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::AppStarted()
extern "C"  void LoadMainScene_AppStarted_m3884069062 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::DoStartMagic()
extern "C"  void LoadMainScene_DoStartMagic_m2322028604 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadMainScene::FirstStart()
extern "C"  Il2CppObject * LoadMainScene_FirstStart_m1904605552 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::ButtonStart()
extern "C"  void LoadMainScene_ButtonStart_m3889206604 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadMainScene::StartApp()
extern "C"  Il2CppObject * LoadMainScene_StartApp_m2228766781 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::ResetPlayerPrefs()
extern "C"  void LoadMainScene_ResetPlayerPrefs_m3595768166 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::Stompagic()
extern "C"  void LoadMainScene_Stompagic_m4042407693 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::StopMagic()
extern "C"  void LoadMainScene_StopMagic_m3978013639 (LoadMainScene_t1678736365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::BackScene(System.String)
extern "C"  void LoadMainScene_BackScene_m295845441 (LoadMainScene_t1678736365 * __this, String_t* ___scene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene::onChange(System.Boolean)
extern "C"  void LoadMainScene_onChange_m4191889964 (LoadMainScene_t1678736365 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
