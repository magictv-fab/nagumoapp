﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.InitialSplashScreen
struct InitialSplashScreen_t2241678589;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.abstracts.screens.InitialSplashScreen::.ctor()
extern "C"  void InitialSplashScreen__ctor_m1310425495 (InitialSplashScreen_t2241678589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::prepare()
extern "C"  void InitialSplashScreen_prepare_m1820463388 (InitialSplashScreen_t2241678589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::Start()
extern "C"  void InitialSplashScreen_Start_m257563287 (InitialSplashScreen_t2241678589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::onStateChange(MagicTV.globals.StateMachine)
extern "C"  void InitialSplashScreen_onStateChange_m1273272740 (InitialSplashScreen_t2241678589 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::setMessage(System.String)
extern "C"  void InitialSplashScreen_setMessage_m2862785680 (InitialSplashScreen_t2241678589 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::setPercentLoaded(System.Single)
extern "C"  void InitialSplashScreen_setPercentLoaded_m3651738550 (InitialSplashScreen_t2241678589 * __this, float ___total0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::hide()
extern "C"  void InitialSplashScreen_hide_m2954741103 (InitialSplashScreen_t2241678589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::HideNow()
extern "C"  void InitialSplashScreen_HideNow_m1378730729 (InitialSplashScreen_t2241678589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.InitialSplashScreen::show()
extern "C"  void InitialSplashScreen_show_m3269083242 (InitialSplashScreen_t2241678589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
