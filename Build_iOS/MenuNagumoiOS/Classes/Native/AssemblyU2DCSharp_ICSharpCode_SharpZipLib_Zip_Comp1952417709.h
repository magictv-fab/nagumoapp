﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t1141403250;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct  InflaterDynHeader_t1952417709  : public Il2CppObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blLens
	ByteU5BU5D_t4260760469* ___blLens_9;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::litdistLens
	ByteU5BU5D_t4260760469* ___litdistLens_10;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blTree
	InflaterHuffmanTree_t1141403250 * ___blTree_11;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::mode
	int32_t ___mode_12;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::lnum
	int32_t ___lnum_13;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::dnum
	int32_t ___dnum_14;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blnum
	int32_t ___blnum_15;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::num
	int32_t ___num_16;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repSymbol
	int32_t ___repSymbol_17;
	// System.Byte ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::lastLen
	uint8_t ___lastLen_18;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::ptr
	int32_t ___ptr_19;

public:
	inline static int32_t get_offset_of_blLens_9() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___blLens_9)); }
	inline ByteU5BU5D_t4260760469* get_blLens_9() const { return ___blLens_9; }
	inline ByteU5BU5D_t4260760469** get_address_of_blLens_9() { return &___blLens_9; }
	inline void set_blLens_9(ByteU5BU5D_t4260760469* value)
	{
		___blLens_9 = value;
		Il2CppCodeGenWriteBarrier(&___blLens_9, value);
	}

	inline static int32_t get_offset_of_litdistLens_10() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___litdistLens_10)); }
	inline ByteU5BU5D_t4260760469* get_litdistLens_10() const { return ___litdistLens_10; }
	inline ByteU5BU5D_t4260760469** get_address_of_litdistLens_10() { return &___litdistLens_10; }
	inline void set_litdistLens_10(ByteU5BU5D_t4260760469* value)
	{
		___litdistLens_10 = value;
		Il2CppCodeGenWriteBarrier(&___litdistLens_10, value);
	}

	inline static int32_t get_offset_of_blTree_11() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___blTree_11)); }
	inline InflaterHuffmanTree_t1141403250 * get_blTree_11() const { return ___blTree_11; }
	inline InflaterHuffmanTree_t1141403250 ** get_address_of_blTree_11() { return &___blTree_11; }
	inline void set_blTree_11(InflaterHuffmanTree_t1141403250 * value)
	{
		___blTree_11 = value;
		Il2CppCodeGenWriteBarrier(&___blTree_11, value);
	}

	inline static int32_t get_offset_of_mode_12() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___mode_12)); }
	inline int32_t get_mode_12() const { return ___mode_12; }
	inline int32_t* get_address_of_mode_12() { return &___mode_12; }
	inline void set_mode_12(int32_t value)
	{
		___mode_12 = value;
	}

	inline static int32_t get_offset_of_lnum_13() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___lnum_13)); }
	inline int32_t get_lnum_13() const { return ___lnum_13; }
	inline int32_t* get_address_of_lnum_13() { return &___lnum_13; }
	inline void set_lnum_13(int32_t value)
	{
		___lnum_13 = value;
	}

	inline static int32_t get_offset_of_dnum_14() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___dnum_14)); }
	inline int32_t get_dnum_14() const { return ___dnum_14; }
	inline int32_t* get_address_of_dnum_14() { return &___dnum_14; }
	inline void set_dnum_14(int32_t value)
	{
		___dnum_14 = value;
	}

	inline static int32_t get_offset_of_blnum_15() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___blnum_15)); }
	inline int32_t get_blnum_15() const { return ___blnum_15; }
	inline int32_t* get_address_of_blnum_15() { return &___blnum_15; }
	inline void set_blnum_15(int32_t value)
	{
		___blnum_15 = value;
	}

	inline static int32_t get_offset_of_num_16() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___num_16)); }
	inline int32_t get_num_16() const { return ___num_16; }
	inline int32_t* get_address_of_num_16() { return &___num_16; }
	inline void set_num_16(int32_t value)
	{
		___num_16 = value;
	}

	inline static int32_t get_offset_of_repSymbol_17() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___repSymbol_17)); }
	inline int32_t get_repSymbol_17() const { return ___repSymbol_17; }
	inline int32_t* get_address_of_repSymbol_17() { return &___repSymbol_17; }
	inline void set_repSymbol_17(int32_t value)
	{
		___repSymbol_17 = value;
	}

	inline static int32_t get_offset_of_lastLen_18() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___lastLen_18)); }
	inline uint8_t get_lastLen_18() const { return ___lastLen_18; }
	inline uint8_t* get_address_of_lastLen_18() { return &___lastLen_18; }
	inline void set_lastLen_18(uint8_t value)
	{
		___lastLen_18 = value;
	}

	inline static int32_t get_offset_of_ptr_19() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709, ___ptr_19)); }
	inline int32_t get_ptr_19() const { return ___ptr_19; }
	inline int32_t* get_address_of_ptr_19() { return &___ptr_19; }
	inline void set_ptr_19(int32_t value)
	{
		___ptr_19 = value;
	}
};

struct InflaterDynHeader_t1952417709_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repMin
	Int32U5BU5D_t3230847821* ___repMin_6;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repBits
	Int32U5BU5D_t3230847821* ___repBits_7;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BL_ORDER
	Int32U5BU5D_t3230847821* ___BL_ORDER_8;

public:
	inline static int32_t get_offset_of_repMin_6() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709_StaticFields, ___repMin_6)); }
	inline Int32U5BU5D_t3230847821* get_repMin_6() const { return ___repMin_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_repMin_6() { return &___repMin_6; }
	inline void set_repMin_6(Int32U5BU5D_t3230847821* value)
	{
		___repMin_6 = value;
		Il2CppCodeGenWriteBarrier(&___repMin_6, value);
	}

	inline static int32_t get_offset_of_repBits_7() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709_StaticFields, ___repBits_7)); }
	inline Int32U5BU5D_t3230847821* get_repBits_7() const { return ___repBits_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_repBits_7() { return &___repBits_7; }
	inline void set_repBits_7(Int32U5BU5D_t3230847821* value)
	{
		___repBits_7 = value;
		Il2CppCodeGenWriteBarrier(&___repBits_7, value);
	}

	inline static int32_t get_offset_of_BL_ORDER_8() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t1952417709_StaticFields, ___BL_ORDER_8)); }
	inline Int32U5BU5D_t3230847821* get_BL_ORDER_8() const { return ___BL_ORDER_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_BL_ORDER_8() { return &___BL_ORDER_8; }
	inline void set_BL_ORDER_8(Int32U5BU5D_t3230847821* value)
	{
		___BL_ORDER_8 = value;
		Il2CppCodeGenWriteBarrier(&___BL_ORDER_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
