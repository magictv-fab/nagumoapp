﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C
struct U3CdoLoadU3Ec__Iterator3C_t1908116762;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::.ctor()
extern "C"  void U3CdoLoadU3Ec__Iterator3C__ctor_m3544956609 (U3CdoLoadU3Ec__Iterator3C_t1908116762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdoLoadU3Ec__Iterator3C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542261947 (U3CdoLoadU3Ec__Iterator3C_t1908116762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdoLoadU3Ec__Iterator3C_System_Collections_IEnumerator_get_Current_m513743951 (U3CdoLoadU3Ec__Iterator3C_t1908116762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::MoveNext()
extern "C"  bool U3CdoLoadU3Ec__Iterator3C_MoveNext_m2356135803 (U3CdoLoadU3Ec__Iterator3C_t1908116762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::Dispose()
extern "C"  void U3CdoLoadU3Ec__Iterator3C_Dispose_m692518590 (U3CdoLoadU3Ec__Iterator3C_t1908116762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest/<doLoad>c__Iterator3C::Reset()
extern "C"  void U3CdoLoadU3Ec__Iterator3C_Reset_m1191389550 (U3CdoLoadU3Ec__Iterator3C_t1908116762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
