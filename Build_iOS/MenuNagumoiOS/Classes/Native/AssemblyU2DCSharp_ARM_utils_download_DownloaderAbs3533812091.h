﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler
struct OnDownloadCompleteEventHandler_t1994402001;

#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.download.DownloaderAbstract
struct  DownloaderAbstract_t3533812091  : public ProgressEventsAbstract_t2129719228
{
public:
	// ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler ARM.utils.download.DownloaderAbstract::onDownloadComplete
	OnDownloadCompleteEventHandler_t1994402001 * ___onDownloadComplete_14;

public:
	inline static int32_t get_offset_of_onDownloadComplete_14() { return static_cast<int32_t>(offsetof(DownloaderAbstract_t3533812091, ___onDownloadComplete_14)); }
	inline OnDownloadCompleteEventHandler_t1994402001 * get_onDownloadComplete_14() const { return ___onDownloadComplete_14; }
	inline OnDownloadCompleteEventHandler_t1994402001 ** get_address_of_onDownloadComplete_14() { return &___onDownloadComplete_14; }
	inline void set_onDownloadComplete_14(OnDownloadCompleteEventHandler_t1994402001 * value)
	{
		___onDownloadComplete_14 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadComplete_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
