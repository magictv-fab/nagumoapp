﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManager/<Save>c__Iterator31
struct U3CSaveU3Ec__Iterator31_t1819521282;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenshotManager/<Save>c__Iterator31::.ctor()
extern "C"  void U3CSaveU3Ec__Iterator31__ctor_m3408820905 (U3CSaveU3Ec__Iterator31_t1819521282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManager/<Save>c__Iterator31::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveU3Ec__Iterator31_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2465780745 (U3CSaveU3Ec__Iterator31_t1819521282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenshotManager/<Save>c__Iterator31::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveU3Ec__Iterator31_System_Collections_IEnumerator_get_Current_m922618269 (U3CSaveU3Ec__Iterator31_t1819521282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenshotManager/<Save>c__Iterator31::MoveNext()
extern "C"  bool U3CSaveU3Ec__Iterator31_MoveNext_m3605884331 (U3CSaveU3Ec__Iterator31_t1819521282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager/<Save>c__Iterator31::Dispose()
extern "C"  void U3CSaveU3Ec__Iterator31_Dispose_m3010093222 (U3CSaveU3Ec__Iterator31_t1819521282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager/<Save>c__Iterator31::Reset()
extern "C"  void U3CSaveU3Ec__Iterator31_Reset_m1055253846 (U3CSaveU3Ec__Iterator31_t1819521282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
