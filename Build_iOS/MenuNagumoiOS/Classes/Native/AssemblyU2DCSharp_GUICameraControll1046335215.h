﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PinchToutchController
struct PinchToutchController_t1962414581;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUICameraControll
struct  GUICameraControll_t1046335215  : public MonoBehaviour_t667441552
{
public:
	// PinchToutchController GUICameraControll::_pinch
	PinchToutchController_t1962414581 * ____pinch_2;
	// System.Single GUICameraControll::_zoomFactor
	float ____zoomFactor_3;
	// System.Single GUICameraControll::_rotateFactor
	float ____rotateFactor_4;
	// System.Single GUICameraControll::_verticalFactor
	float ____verticalFactor_5;
	// System.Single GUICameraControll::_sideFactor
	float ____sideFactor_6;
	// UnityEngine.Vector3 GUICameraControll::_originalPos
	Vector3_t4282066566  ____originalPos_7;
	// UnityEngine.Quaternion GUICameraControll::_originaRot
	Quaternion_t1553702882  ____originaRot_8;

public:
	inline static int32_t get_offset_of__pinch_2() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____pinch_2)); }
	inline PinchToutchController_t1962414581 * get__pinch_2() const { return ____pinch_2; }
	inline PinchToutchController_t1962414581 ** get_address_of__pinch_2() { return &____pinch_2; }
	inline void set__pinch_2(PinchToutchController_t1962414581 * value)
	{
		____pinch_2 = value;
		Il2CppCodeGenWriteBarrier(&____pinch_2, value);
	}

	inline static int32_t get_offset_of__zoomFactor_3() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____zoomFactor_3)); }
	inline float get__zoomFactor_3() const { return ____zoomFactor_3; }
	inline float* get_address_of__zoomFactor_3() { return &____zoomFactor_3; }
	inline void set__zoomFactor_3(float value)
	{
		____zoomFactor_3 = value;
	}

	inline static int32_t get_offset_of__rotateFactor_4() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____rotateFactor_4)); }
	inline float get__rotateFactor_4() const { return ____rotateFactor_4; }
	inline float* get_address_of__rotateFactor_4() { return &____rotateFactor_4; }
	inline void set__rotateFactor_4(float value)
	{
		____rotateFactor_4 = value;
	}

	inline static int32_t get_offset_of__verticalFactor_5() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____verticalFactor_5)); }
	inline float get__verticalFactor_5() const { return ____verticalFactor_5; }
	inline float* get_address_of__verticalFactor_5() { return &____verticalFactor_5; }
	inline void set__verticalFactor_5(float value)
	{
		____verticalFactor_5 = value;
	}

	inline static int32_t get_offset_of__sideFactor_6() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____sideFactor_6)); }
	inline float get__sideFactor_6() const { return ____sideFactor_6; }
	inline float* get_address_of__sideFactor_6() { return &____sideFactor_6; }
	inline void set__sideFactor_6(float value)
	{
		____sideFactor_6 = value;
	}

	inline static int32_t get_offset_of__originalPos_7() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____originalPos_7)); }
	inline Vector3_t4282066566  get__originalPos_7() const { return ____originalPos_7; }
	inline Vector3_t4282066566 * get_address_of__originalPos_7() { return &____originalPos_7; }
	inline void set__originalPos_7(Vector3_t4282066566  value)
	{
		____originalPos_7 = value;
	}

	inline static int32_t get_offset_of__originaRot_8() { return static_cast<int32_t>(offsetof(GUICameraControll_t1046335215, ____originaRot_8)); }
	inline Quaternion_t1553702882  get__originaRot_8() const { return ____originaRot_8; }
	inline Quaternion_t1553702882 * get_address_of__originaRot_8() { return &____originaRot_8; }
	inline void set__originaRot_8(Quaternion_t1553702882  value)
	{
		____originaRot_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
