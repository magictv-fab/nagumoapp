﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C
struct U3CClickRepeatU3Ec__Iterator1C_t1079251060;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::.ctor()
extern "C"  void U3CClickRepeatU3Ec__Iterator1C__ctor_m3886975863 (U3CClickRepeatU3Ec__Iterator1C_t1079251060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CClickRepeatU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4141675003 (U3CClickRepeatU3Ec__Iterator1C_t1079251060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CClickRepeatU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m1559251855 (U3CClickRepeatU3Ec__Iterator1C_t1079251060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::MoveNext()
extern "C"  bool U3CClickRepeatU3Ec__Iterator1C_MoveNext_m3931105821 (U3CClickRepeatU3Ec__Iterator1C_t1079251060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::Dispose()
extern "C"  void U3CClickRepeatU3Ec__Iterator1C_Dispose_m2955507188 (U3CClickRepeatU3Ec__Iterator1C_t1079251060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::Reset()
extern "C"  void U3CClickRepeatU3Ec__Iterator1C_Reset_m1533408804 (U3CClickRepeatU3Ec__Iterator1C_t1079251060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
