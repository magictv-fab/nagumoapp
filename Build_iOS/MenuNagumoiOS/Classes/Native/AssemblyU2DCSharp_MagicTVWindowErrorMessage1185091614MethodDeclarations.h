﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowErrorMessage
struct MagicTVWindowErrorMessage_t1185091614;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVWindowErrorMessage::.ctor()
extern "C"  void MagicTVWindowErrorMessage__ctor_m3571349261 (MagicTVWindowErrorMessage_t1185091614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowErrorMessage::CongigClick()
extern "C"  void MagicTVWindowErrorMessage_CongigClick_m816549808 (MagicTVWindowErrorMessage_t1185091614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowErrorMessage::SetErroMessage(System.String)
extern "C"  void MagicTVWindowErrorMessage_SetErroMessage_m382985904 (MagicTVWindowErrorMessage_t1185091614 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowErrorMessage::SetButtonLabel(System.String)
extern "C"  void MagicTVWindowErrorMessage_SetButtonLabel_m3868199467 (MagicTVWindowErrorMessage_t1185091614 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
