﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StaticPinchCameraZoomFilter
struct StaticPinchCameraZoomFilter_t2892807452;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void StaticPinchCameraZoomFilter::.ctor()
extern "C"  void StaticPinchCameraZoomFilter__ctor_m4271435215 (StaticPinchCameraZoomFilter_t2892807452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StaticPinchCameraZoomFilter::LateUpdate()
extern "C"  void StaticPinchCameraZoomFilter_LateUpdate_m2802034148 (StaticPinchCameraZoomFilter_t2892807452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String StaticPinchCameraZoomFilter::getUniqueName()
extern "C"  String_t* StaticPinchCameraZoomFilter_getUniqueName_m3818090018 (StaticPinchCameraZoomFilter_t2892807452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StaticPinchCameraZoomFilter::configByString(System.String)
extern "C"  void StaticPinchCameraZoomFilter_configByString_m3214687939 (StaticPinchCameraZoomFilter_t2892807452 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StaticPinchCameraZoomFilter::Reset()
extern "C"  void StaticPinchCameraZoomFilter_Reset_m1917868156 (StaticPinchCameraZoomFilter_t2892807452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject StaticPinchCameraZoomFilter::getGameObjectReference()
extern "C"  GameObject_t3674682005 * StaticPinchCameraZoomFilter_getGameObjectReference_m3320917284 (StaticPinchCameraZoomFilter_t2892807452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
