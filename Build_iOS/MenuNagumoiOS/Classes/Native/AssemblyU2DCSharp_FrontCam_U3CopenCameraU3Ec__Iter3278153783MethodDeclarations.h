﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrontCam/<openCamera>c__Iterator57
struct U3CopenCameraU3Ec__Iterator57_t3278153783;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FrontCam/<openCamera>c__Iterator57::.ctor()
extern "C"  void U3CopenCameraU3Ec__Iterator57__ctor_m2906814020 (U3CopenCameraU3Ec__Iterator57_t3278153783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<openCamera>c__Iterator57::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CopenCameraU3Ec__Iterator57_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3596115160 (U3CopenCameraU3Ec__Iterator57_t3278153783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<openCamera>c__Iterator57::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CopenCameraU3Ec__Iterator57_System_Collections_IEnumerator_get_Current_m131010668 (U3CopenCameraU3Ec__Iterator57_t3278153783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FrontCam/<openCamera>c__Iterator57::MoveNext()
extern "C"  bool U3CopenCameraU3Ec__Iterator57_MoveNext_m1570094104 (U3CopenCameraU3Ec__Iterator57_t3278153783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<openCamera>c__Iterator57::Dispose()
extern "C"  void U3CopenCameraU3Ec__Iterator57_Dispose_m1617813889 (U3CopenCameraU3Ec__Iterator57_t3278153783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<openCamera>c__Iterator57::Reset()
extern "C"  void U3CopenCameraU3Ec__Iterator57_Reset_m553246961 (U3CopenCameraU3Ec__Iterator57_t3278153783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
