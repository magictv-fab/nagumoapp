﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// cameraFake/<GoLevel>c__Iterator2
struct U3CGoLevelU3Ec__Iterator2_t4018543786;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void cameraFake/<GoLevel>c__Iterator2::.ctor()
extern "C"  void U3CGoLevelU3Ec__Iterator2__ctor_m2745465137 (U3CGoLevelU3Ec__Iterator2_t4018543786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraFake/<GoLevel>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3285351755 (U3CGoLevelU3Ec__Iterator2_t4018543786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object cameraFake/<GoLevel>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1485207263 (U3CGoLevelU3Ec__Iterator2_t4018543786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean cameraFake/<GoLevel>c__Iterator2::MoveNext()
extern "C"  bool U3CGoLevelU3Ec__Iterator2_MoveNext_m32609035 (U3CGoLevelU3Ec__Iterator2_t4018543786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake/<GoLevel>c__Iterator2::Dispose()
extern "C"  void U3CGoLevelU3Ec__Iterator2_Dispose_m1180359982 (U3CGoLevelU3Ec__Iterator2_t4018543786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraFake/<GoLevel>c__Iterator2::Reset()
extern "C"  void U3CGoLevelU3Ec__Iterator2_Reset_m391898078 (U3CGoLevelU3Ec__Iterator2_t4018543786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
