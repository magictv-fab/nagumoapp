﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TransformPoint
struct  TransformPoint_t1395025004  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TransformPoint::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformPoint::localPosition
	FsmVector3_t533912882 * ___localPosition_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformPoint::storeResult
	FsmVector3_t533912882 * ___storeResult_11;
	// System.Boolean HutongGames.PlayMaker.Actions.TransformPoint::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(TransformPoint_t1395025004, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_localPosition_10() { return static_cast<int32_t>(offsetof(TransformPoint_t1395025004, ___localPosition_10)); }
	inline FsmVector3_t533912882 * get_localPosition_10() const { return ___localPosition_10; }
	inline FsmVector3_t533912882 ** get_address_of_localPosition_10() { return &___localPosition_10; }
	inline void set_localPosition_10(FsmVector3_t533912882 * value)
	{
		___localPosition_10 = value;
		Il2CppCodeGenWriteBarrier(&___localPosition_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(TransformPoint_t1395025004, ___storeResult_11)); }
	inline FsmVector3_t533912882 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmVector3_t533912882 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmVector3_t533912882 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(TransformPoint_t1395025004, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
