﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler
struct ProcessDirectoryHandler_t4285218014;
// ICSharpCode.SharpZipLib.Core.ProcessFileHandler
struct ProcessFileHandler_t83050093;
// ICSharpCode.SharpZipLib.Core.ProgressHandler
struct ProgressHandler_t2724445839;
// ICSharpCode.SharpZipLib.Core.CompletedFileHandler
struct CompletedFileHandler_t164988305;
// ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler
struct DirectoryFailureHandler_t1659888223;
// ICSharpCode.SharpZipLib.Core.FileFailureHandler
struct FileFailureHandler_t725938474;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_TimeSpan413522987.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.FastZipEvents
struct  FastZipEvents_t381075024  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler ICSharpCode.SharpZipLib.Zip.FastZipEvents::ProcessDirectory
	ProcessDirectoryHandler_t4285218014 * ___ProcessDirectory_0;
	// ICSharpCode.SharpZipLib.Core.ProcessFileHandler ICSharpCode.SharpZipLib.Zip.FastZipEvents::ProcessFile
	ProcessFileHandler_t83050093 * ___ProcessFile_1;
	// ICSharpCode.SharpZipLib.Core.ProgressHandler ICSharpCode.SharpZipLib.Zip.FastZipEvents::Progress
	ProgressHandler_t2724445839 * ___Progress_2;
	// ICSharpCode.SharpZipLib.Core.CompletedFileHandler ICSharpCode.SharpZipLib.Zip.FastZipEvents::CompletedFile
	CompletedFileHandler_t164988305 * ___CompletedFile_3;
	// ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler ICSharpCode.SharpZipLib.Zip.FastZipEvents::DirectoryFailure
	DirectoryFailureHandler_t1659888223 * ___DirectoryFailure_4;
	// ICSharpCode.SharpZipLib.Core.FileFailureHandler ICSharpCode.SharpZipLib.Zip.FastZipEvents::FileFailure
	FileFailureHandler_t725938474 * ___FileFailure_5;
	// System.TimeSpan ICSharpCode.SharpZipLib.Zip.FastZipEvents::progressInterval_
	TimeSpan_t413522987  ___progressInterval__6;

public:
	inline static int32_t get_offset_of_ProcessDirectory_0() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___ProcessDirectory_0)); }
	inline ProcessDirectoryHandler_t4285218014 * get_ProcessDirectory_0() const { return ___ProcessDirectory_0; }
	inline ProcessDirectoryHandler_t4285218014 ** get_address_of_ProcessDirectory_0() { return &___ProcessDirectory_0; }
	inline void set_ProcessDirectory_0(ProcessDirectoryHandler_t4285218014 * value)
	{
		___ProcessDirectory_0 = value;
		Il2CppCodeGenWriteBarrier(&___ProcessDirectory_0, value);
	}

	inline static int32_t get_offset_of_ProcessFile_1() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___ProcessFile_1)); }
	inline ProcessFileHandler_t83050093 * get_ProcessFile_1() const { return ___ProcessFile_1; }
	inline ProcessFileHandler_t83050093 ** get_address_of_ProcessFile_1() { return &___ProcessFile_1; }
	inline void set_ProcessFile_1(ProcessFileHandler_t83050093 * value)
	{
		___ProcessFile_1 = value;
		Il2CppCodeGenWriteBarrier(&___ProcessFile_1, value);
	}

	inline static int32_t get_offset_of_Progress_2() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___Progress_2)); }
	inline ProgressHandler_t2724445839 * get_Progress_2() const { return ___Progress_2; }
	inline ProgressHandler_t2724445839 ** get_address_of_Progress_2() { return &___Progress_2; }
	inline void set_Progress_2(ProgressHandler_t2724445839 * value)
	{
		___Progress_2 = value;
		Il2CppCodeGenWriteBarrier(&___Progress_2, value);
	}

	inline static int32_t get_offset_of_CompletedFile_3() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___CompletedFile_3)); }
	inline CompletedFileHandler_t164988305 * get_CompletedFile_3() const { return ___CompletedFile_3; }
	inline CompletedFileHandler_t164988305 ** get_address_of_CompletedFile_3() { return &___CompletedFile_3; }
	inline void set_CompletedFile_3(CompletedFileHandler_t164988305 * value)
	{
		___CompletedFile_3 = value;
		Il2CppCodeGenWriteBarrier(&___CompletedFile_3, value);
	}

	inline static int32_t get_offset_of_DirectoryFailure_4() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___DirectoryFailure_4)); }
	inline DirectoryFailureHandler_t1659888223 * get_DirectoryFailure_4() const { return ___DirectoryFailure_4; }
	inline DirectoryFailureHandler_t1659888223 ** get_address_of_DirectoryFailure_4() { return &___DirectoryFailure_4; }
	inline void set_DirectoryFailure_4(DirectoryFailureHandler_t1659888223 * value)
	{
		___DirectoryFailure_4 = value;
		Il2CppCodeGenWriteBarrier(&___DirectoryFailure_4, value);
	}

	inline static int32_t get_offset_of_FileFailure_5() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___FileFailure_5)); }
	inline FileFailureHandler_t725938474 * get_FileFailure_5() const { return ___FileFailure_5; }
	inline FileFailureHandler_t725938474 ** get_address_of_FileFailure_5() { return &___FileFailure_5; }
	inline void set_FileFailure_5(FileFailureHandler_t725938474 * value)
	{
		___FileFailure_5 = value;
		Il2CppCodeGenWriteBarrier(&___FileFailure_5, value);
	}

	inline static int32_t get_offset_of_progressInterval__6() { return static_cast<int32_t>(offsetof(FastZipEvents_t381075024, ___progressInterval__6)); }
	inline TimeSpan_t413522987  get_progressInterval__6() const { return ___progressInterval__6; }
	inline TimeSpan_t413522987 * get_address_of_progressInterval__6() { return &___progressInterval__6; }
	inline void set_progressInterval__6(TimeSpan_t413522987  value)
	{
		___progressInterval__6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
