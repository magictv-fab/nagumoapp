﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetLastEvent
struct GetLastEvent_t1262666006;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetLastEvent::.ctor()
extern "C"  void GetLastEvent__ctor_m3205809680 (GetLastEvent_t1262666006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLastEvent::Reset()
extern "C"  void GetLastEvent_Reset_m852242621 (GetLastEvent_t1262666006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLastEvent::OnEnter()
extern "C"  void GetLastEvent_OnEnter_m683279847 (GetLastEvent_t1262666006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
