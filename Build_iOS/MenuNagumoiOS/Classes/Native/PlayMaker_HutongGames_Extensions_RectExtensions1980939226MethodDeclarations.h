﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Boolean HutongGames.Extensions.RectExtensions::Contains(UnityEngine.Rect,System.Single,System.Single)
extern "C"  bool RectExtensions_Contains_m3181311701 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.Extensions.RectExtensions::Contains(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectExtensions_Contains_m314639712 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect10, Rect_t4241904616  ___rect21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.Extensions.RectExtensions::IntersectsWith(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectExtensions_IntersectsWith_m3187481605 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect10, Rect_t4241904616  ___rect21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::Union(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectExtensions_Union_m2565005230 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect10, Rect_t4241904616  ___rect21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::Scale(UnityEngine.Rect,System.Single)
extern "C"  Rect_t4241904616  RectExtensions_Scale_m1254362845 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, float ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::MinSize(UnityEngine.Rect,System.Single,System.Single)
extern "C"  Rect_t4241904616  RectExtensions_MinSize_m1006989675 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, float ___minWidth1, float ___minHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::MinSize(UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  Rect_t4241904616  RectExtensions_MinSize_m3329500885 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, Vector2_t4282066565  ___minSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
