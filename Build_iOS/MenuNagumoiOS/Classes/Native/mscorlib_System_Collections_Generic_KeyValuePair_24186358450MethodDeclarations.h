﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4287554263_gshared (KeyValuePair_2_t4186358450 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m4287554263(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4186358450 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m4287554263_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m965051057_gshared (KeyValuePair_2_t4186358450 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m965051057(__this, method) ((  int32_t (*) (KeyValuePair_2_t4186358450 *, const MethodInfo*))KeyValuePair_2_get_Key_m965051057_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1467337970_gshared (KeyValuePair_2_t4186358450 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1467337970(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4186358450 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1467337970_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3958869269_gshared (KeyValuePair_2_t4186358450 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3958869269(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t4186358450 *, const MethodInfo*))KeyValuePair_2_get_Value_m3958869269_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3512555506_gshared (KeyValuePair_2_t4186358450 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3512555506(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4186358450 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m3512555506_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1044167254_gshared (KeyValuePair_2_t4186358450 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1044167254(__this, method) ((  String_t* (*) (KeyValuePair_2_t4186358450 *, const MethodInfo*))KeyValuePair_2_ToString_m1044167254_gshared)(__this, method)
