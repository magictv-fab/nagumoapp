﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveRoletas
struct ActiveRoletas_t996432234;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ActiveRoletas::.ctor()
extern "C"  void ActiveRoletas__ctor_m3935502145 (ActiveRoletas_t996432234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveRoletas::Start()
extern "C"  void ActiveRoletas_Start_m2882639937 (ActiveRoletas_t996432234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ActiveRoletas::Go()
extern "C"  Il2CppObject * ActiveRoletas_Go_m2390397443 (ActiveRoletas_t996432234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveRoletas::Update()
extern "C"  void ActiveRoletas_Update_m3468344300 (ActiveRoletas_t996432234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
