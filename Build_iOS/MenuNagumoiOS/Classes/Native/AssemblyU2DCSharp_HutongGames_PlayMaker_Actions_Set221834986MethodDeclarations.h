﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIBackgroundColor
struct SetGUIBackgroundColor_t221834986;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::.ctor()
extern "C"  void SetGUIBackgroundColor__ctor_m1422270860 (SetGUIBackgroundColor_t221834986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::Reset()
extern "C"  void SetGUIBackgroundColor_Reset_m3363671097 (SetGUIBackgroundColor_t221834986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::OnGUI()
extern "C"  void SetGUIBackgroundColor_OnGUI_m917669510 (SetGUIBackgroundColor_t221834986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
