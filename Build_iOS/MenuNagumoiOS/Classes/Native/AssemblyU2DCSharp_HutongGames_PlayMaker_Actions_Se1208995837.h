﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendRandomEvent
struct  SendRandomEvent_t1208995837  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.SendRandomEvent::events
	FsmEventU5BU5D_t2862142229* ___events_9;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SendRandomEvent::weights
	FsmFloatU5BU5D_t2945380875* ___weights_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendRandomEvent::delay
	FsmFloat_t2134102846 * ___delay_11;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendRandomEvent::delayedEvent
	DelayedEvent_t1938906778 * ___delayedEvent_12;

public:
	inline static int32_t get_offset_of_events_9() { return static_cast<int32_t>(offsetof(SendRandomEvent_t1208995837, ___events_9)); }
	inline FsmEventU5BU5D_t2862142229* get_events_9() const { return ___events_9; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_events_9() { return &___events_9; }
	inline void set_events_9(FsmEventU5BU5D_t2862142229* value)
	{
		___events_9 = value;
		Il2CppCodeGenWriteBarrier(&___events_9, value);
	}

	inline static int32_t get_offset_of_weights_10() { return static_cast<int32_t>(offsetof(SendRandomEvent_t1208995837, ___weights_10)); }
	inline FsmFloatU5BU5D_t2945380875* get_weights_10() const { return ___weights_10; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_weights_10() { return &___weights_10; }
	inline void set_weights_10(FsmFloatU5BU5D_t2945380875* value)
	{
		___weights_10 = value;
		Il2CppCodeGenWriteBarrier(&___weights_10, value);
	}

	inline static int32_t get_offset_of_delay_11() { return static_cast<int32_t>(offsetof(SendRandomEvent_t1208995837, ___delay_11)); }
	inline FsmFloat_t2134102846 * get_delay_11() const { return ___delay_11; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_11() { return &___delay_11; }
	inline void set_delay_11(FsmFloat_t2134102846 * value)
	{
		___delay_11 = value;
		Il2CppCodeGenWriteBarrier(&___delay_11, value);
	}

	inline static int32_t get_offset_of_delayedEvent_12() { return static_cast<int32_t>(offsetof(SendRandomEvent_t1208995837, ___delayedEvent_12)); }
	inline DelayedEvent_t1938906778 * get_delayedEvent_12() const { return ___delayedEvent_12; }
	inline DelayedEvent_t1938906778 ** get_address_of_delayedEvent_12() { return &___delayedEvent_12; }
	inline void set_delayedEvent_12(DelayedEvent_t1938906778 * value)
	{
		___delayedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___delayedEvent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
