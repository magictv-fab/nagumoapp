﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupAtualizarCadastro
struct PopupAtualizarCadastro_t1794233116;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupAtualizarCadastro::.ctor()
extern "C"  void PopupAtualizarCadastro__ctor_m862986495 (PopupAtualizarCadastro_t1794233116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupAtualizarCadastro::Awake()
extern "C"  void PopupAtualizarCadastro_Awake_m1100591714 (PopupAtualizarCadastro_t1794233116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupAtualizarCadastro::Update()
extern "C"  void PopupAtualizarCadastro_Update_m2709639662 (PopupAtualizarCadastro_t1794233116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupAtualizarCadastro::FecharPopup()
extern "C"  void PopupAtualizarCadastro_FecharPopup_m873514100 (PopupAtualizarCadastro_t1794233116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
