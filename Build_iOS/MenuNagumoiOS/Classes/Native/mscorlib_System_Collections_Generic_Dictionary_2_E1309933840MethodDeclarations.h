﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>
struct Dictionary_2_t4287577744;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1309933840.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m661894921_gshared (Enumerator_t1309933840 * __this, Dictionary_2_t4287577744 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m661894921(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1309933840 *, Dictionary_2_t4287577744 *, const MethodInfo*))Enumerator__ctor_m661894921_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m186784184_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m186784184(__this, method) ((  Il2CppObject * (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m186784184_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2125948428_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2125948428(__this, method) ((  void (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2125948428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1611766933_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1611766933(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1611766933_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4168428692_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4168428692(__this, method) ((  Il2CppObject * (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4168428692_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3998889510_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3998889510(__this, method) ((  Il2CppObject * (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3998889510_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2347244280_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2347244280(__this, method) ((  bool (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_MoveNext_m2347244280_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t4186358450  Enumerator_get_Current_m534300216_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m534300216(__this, method) ((  KeyValuePair_2_t4186358450  (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_get_Current_m534300216_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m791811781_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m791811781(__this, method) ((  int32_t (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_get_CurrentKey_m791811781_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1176045609_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1176045609(__this, method) ((  Il2CppObject * (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_get_CurrentValue_m1176045609_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1067045851_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1067045851(__this, method) ((  void (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_Reset_m1067045851_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2662831012_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2662831012(__this, method) ((  void (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_VerifyState_m2662831012_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1924965900_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1924965900(__this, method) ((  void (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_VerifyCurrent_m1924965900_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1457308139_gshared (Enumerator_t1309933840 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1457308139(__this, method) ((  void (*) (Enumerator_t1309933840 *, const MethodInfo*))Enumerator_Dispose_m1457308139_gshared)(__this, method)
