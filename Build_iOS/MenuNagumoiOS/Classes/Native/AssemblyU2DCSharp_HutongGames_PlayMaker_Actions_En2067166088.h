﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// UnityEngine.Component
struct Component_t3501516275;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.Behaviour
struct Behaviour_t200106419;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableBehaviour
struct  EnableBehaviour_t2067166088  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableBehaviour::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableBehaviour::behaviour
	FsmString_t952858651 * ___behaviour_10;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.EnableBehaviour::component
	Component_t3501516275 * ___component_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableBehaviour::enable
	FsmBool_t1075959796 * ___enable_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableBehaviour::resetOnExit
	FsmBool_t1075959796 * ___resetOnExit_13;
	// UnityEngine.Behaviour HutongGames.PlayMaker.Actions.EnableBehaviour::componentTarget
	Behaviour_t200106419 * ___componentTarget_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_behaviour_10() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___behaviour_10)); }
	inline FsmString_t952858651 * get_behaviour_10() const { return ___behaviour_10; }
	inline FsmString_t952858651 ** get_address_of_behaviour_10() { return &___behaviour_10; }
	inline void set_behaviour_10(FsmString_t952858651 * value)
	{
		___behaviour_10 = value;
		Il2CppCodeGenWriteBarrier(&___behaviour_10, value);
	}

	inline static int32_t get_offset_of_component_11() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___component_11)); }
	inline Component_t3501516275 * get_component_11() const { return ___component_11; }
	inline Component_t3501516275 ** get_address_of_component_11() { return &___component_11; }
	inline void set_component_11(Component_t3501516275 * value)
	{
		___component_11 = value;
		Il2CppCodeGenWriteBarrier(&___component_11, value);
	}

	inline static int32_t get_offset_of_enable_12() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___enable_12)); }
	inline FsmBool_t1075959796 * get_enable_12() const { return ___enable_12; }
	inline FsmBool_t1075959796 ** get_address_of_enable_12() { return &___enable_12; }
	inline void set_enable_12(FsmBool_t1075959796 * value)
	{
		___enable_12 = value;
		Il2CppCodeGenWriteBarrier(&___enable_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___resetOnExit_13)); }
	inline FsmBool_t1075959796 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t1075959796 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t1075959796 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_componentTarget_14() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___componentTarget_14)); }
	inline Behaviour_t200106419 * get_componentTarget_14() const { return ___componentTarget_14; }
	inline Behaviour_t200106419 ** get_address_of_componentTarget_14() { return &___componentTarget_14; }
	inline void set_componentTarget_14(Behaviour_t200106419 * value)
	{
		___componentTarget_14 = value;
		Il2CppCodeGenWriteBarrier(&___componentTarget_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
