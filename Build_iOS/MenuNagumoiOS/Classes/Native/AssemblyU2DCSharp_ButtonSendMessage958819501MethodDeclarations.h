﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonSendMessage
struct ButtonSendMessage_t958819501;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonSendMessage::.ctor()
extern "C"  void ButtonSendMessage__ctor_m1523069790 (ButtonSendMessage_t958819501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonSendMessage::Start()
extern "C"  void ButtonSendMessage_Start_m470207582 (ButtonSendMessage_t958819501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonSendMessage::Update()
extern "C"  void ButtonSendMessage_Update_m1697385327 (ButtonSendMessage_t958819501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonSendMessage::OnMouseUp()
extern "C"  void ButtonSendMessage_OnMouseUp_m3338488317 (ButtonSendMessage_t958819501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
