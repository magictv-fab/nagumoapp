﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetObjectValue
struct  SetObjectValue_t3678867096  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetObjectValue::objectVariable
	FsmObject_t821476169 * ___objectVariable_9;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetObjectValue::objectValue
	FsmObject_t821476169 * ___objectValue_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetObjectValue::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_objectVariable_9() { return static_cast<int32_t>(offsetof(SetObjectValue_t3678867096, ___objectVariable_9)); }
	inline FsmObject_t821476169 * get_objectVariable_9() const { return ___objectVariable_9; }
	inline FsmObject_t821476169 ** get_address_of_objectVariable_9() { return &___objectVariable_9; }
	inline void set_objectVariable_9(FsmObject_t821476169 * value)
	{
		___objectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___objectVariable_9, value);
	}

	inline static int32_t get_offset_of_objectValue_10() { return static_cast<int32_t>(offsetof(SetObjectValue_t3678867096, ___objectValue_10)); }
	inline FsmObject_t821476169 * get_objectValue_10() const { return ___objectValue_10; }
	inline FsmObject_t821476169 ** get_address_of_objectValue_10() { return &___objectValue_10; }
	inline void set_objectValue_10(FsmObject_t821476169 * value)
	{
		___objectValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___objectValue_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetObjectValue_t3678867096, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
