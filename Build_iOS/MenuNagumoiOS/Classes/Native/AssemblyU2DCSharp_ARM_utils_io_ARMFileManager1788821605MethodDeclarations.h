﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.io.ARMFileManager
struct ARMFileManager_t1788821605;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t3358688287;
// System.IO.DirectoryInfo
struct DirectoryInfo_t89154617;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"

// System.Void ARM.utils.io.ARMFileManager::.ctor()
extern "C"  void ARMFileManager__ctor_m1378295901 (ARMFileManager_t1788821605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.io.ARMFileManager::UncompressFile(System.String,System.String)
extern "C"  bool ARMFileManager_UncompressFile_m1880599188 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, String_t* ___pathToUnzip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.io.ARMFileManager::UncompressFile(System.String,System.String,System.Boolean)
extern "C"  bool ARMFileManager_UncompressFile_m3488853609 (Il2CppObject * __this /* static, unused */, String_t* ___zipFilePath0, String_t* ___pathToUnzip1, bool ___deleteZipAfterFinished2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.io.ARMFileManager::_MTV_RemoveFromBackup(System.String)
extern "C"  void ARMFileManager__MTV_RemoveFromBackup_m642217992 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.io.ARMFileManager::WriteBites(System.String,System.Byte[])
extern "C"  bool ARMFileManager_WriteBites_m2676548534 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::RemoveLastPathNode(System.String)
extern "C"  String_t* ARMFileManager_RemoveLastPathNode_m527824189 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.io.ARMFileManager::DeleteFolder(System.String)
extern "C"  bool ARMFileManager_DeleteFolder_m2448265910 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.io.ARMFileManager::DeleteFile(System.String)
extern "C"  bool ARMFileManager_DeleteFile_m2024943784 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.io.ARMFileManager::CreateFolderRecursivelyForPath(System.String)
extern "C"  void ARMFileManager_CreateFolderRecursivelyForPath_m2203295490 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.io.ARMFileManager::Exists(System.String)
extern "C"  bool ARMFileManager_Exists_m48580723 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetPersitentDataPath()
extern "C"  String_t* ARMFileManager_GetPersitentDataPath_m1542066311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetGetFileName(System.String)
extern "C"  String_t* ARMFileManager_GetGetFileName_m2329804727 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetFileNameWithoutExtention(System.String)
extern "C"  String_t* ARMFileManager_GetFileNameWithoutExtention_m1513557649 (Il2CppObject * __this /* static, unused */, String_t* ___fileNameOrPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetExtentionOfFile(System.String)
extern "C"  String_t* ARMFileManager_GetExtentionOfFile_m2838095139 (Il2CppObject * __this /* static, unused */, String_t* ___local_url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetDirectoryOfFilePath(System.String)
extern "C"  String_t* ARMFileManager_GetDirectoryOfFilePath_m463049295 (Il2CppObject * __this /* static, unused */, String_t* ___fullPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetDirectoryOfFilePath(System.String,System.Boolean)
extern "C"  String_t* ARMFileManager_GetDirectoryOfFilePath_m4145281358 (Il2CppObject * __this /* static, unused */, String_t* ___fullPath0, bool ___justParent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::GetParentFolder(System.String)
extern "C"  String_t* ARMFileManager_GetParentFolder_m439832566 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ARM.utils.io.ARMFileManager::GetChildDirectoryNames(System.String)
extern "C"  StringU5BU5D_t4054002952* ARMFileManager_GetChildDirectoryNames_m125438619 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] ARM.utils.io.ARMFileManager::GetFilesOfDirectory(System.String,System.String)
extern "C"  FileInfoU5BU5D_t3358688287* ARMFileManager_GetFilesOfDirectory_m3791954694 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, String_t* ___pattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] ARM.utils.io.ARMFileManager::GetFilesOfDirectory(System.String)
extern "C"  FileInfoU5BU5D_t3358688287* ARMFileManager_GetFilesOfDirectory_m3884303434 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ARM.utils.io.ARMFileManager::GetFilesPathOfDirectory(System.String)
extern "C"  StringU5BU5D_t4054002952* ARMFileManager_GetFilesPathOfDirectory_m2705605654 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ARM.utils.io.ARMFileManager::GetBytes(System.String)
extern "C"  ByteU5BU5D_t4260760469* ARMFileManager_GetBytes_m2959532720 (Il2CppObject * __this /* static, unused */, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.utils.io.ARMFileManager::<GetChildDirectoryNames>m__15(System.IO.DirectoryInfo)
extern "C"  String_t* ARMFileManager_U3CGetChildDirectoryNamesU3Em__15_m290563122 (Il2CppObject * __this /* static, unused */, DirectoryInfo_t89154617 * ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
