﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder
struct GeneralAppIdDecoder_t1619283386;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder
struct  AbstractExpandedDecoder_t1111398899  : public Il2CppObject
{
public:
	// ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::information
	BitArray_t4163851164 * ___information_0;
	// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::generalDecoder
	GeneralAppIdDecoder_t1619283386 * ___generalDecoder_1;

public:
	inline static int32_t get_offset_of_information_0() { return static_cast<int32_t>(offsetof(AbstractExpandedDecoder_t1111398899, ___information_0)); }
	inline BitArray_t4163851164 * get_information_0() const { return ___information_0; }
	inline BitArray_t4163851164 ** get_address_of_information_0() { return &___information_0; }
	inline void set_information_0(BitArray_t4163851164 * value)
	{
		___information_0 = value;
		Il2CppCodeGenWriteBarrier(&___information_0, value);
	}

	inline static int32_t get_offset_of_generalDecoder_1() { return static_cast<int32_t>(offsetof(AbstractExpandedDecoder_t1111398899, ___generalDecoder_1)); }
	inline GeneralAppIdDecoder_t1619283386 * get_generalDecoder_1() const { return ___generalDecoder_1; }
	inline GeneralAppIdDecoder_t1619283386 ** get_address_of_generalDecoder_1() { return &___generalDecoder_1; }
	inline void set_generalDecoder_1(GeneralAppIdDecoder_t1619283386 * value)
	{
		___generalDecoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___generalDecoder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
