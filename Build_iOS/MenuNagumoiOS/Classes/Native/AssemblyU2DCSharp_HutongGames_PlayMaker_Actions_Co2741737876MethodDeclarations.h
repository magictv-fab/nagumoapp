﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerIsGrounded
struct ControllerIsGrounded_t2741737876;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::.ctor()
extern "C"  void ControllerIsGrounded__ctor_m1077871442 (ControllerIsGrounded_t2741737876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::Reset()
extern "C"  void ControllerIsGrounded_Reset_m3019271679 (ControllerIsGrounded_t2741737876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::OnEnter()
extern "C"  void ControllerIsGrounded_OnEnter_m139066025 (ControllerIsGrounded_t2741737876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::OnUpdate()
extern "C"  void ControllerIsGrounded_OnUpdate_m3444606138 (ControllerIsGrounded_t2741737876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::DoControllerIsGrounded()
extern "C"  void ControllerIsGrounded_DoControllerIsGrounded_m631129417 (ControllerIsGrounded_t2741737876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
