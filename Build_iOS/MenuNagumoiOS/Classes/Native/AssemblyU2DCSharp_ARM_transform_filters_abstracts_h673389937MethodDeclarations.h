﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.abstracts.histories.HistoryPositionFilterAbstract
struct HistoryPositionFilterAbstract_t673389937;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.transform.filters.abstracts.histories.HistoryPositionFilterAbstract::.ctor()
extern "C"  void HistoryPositionFilterAbstract__ctor_m4245702004 (HistoryPositionFilterAbstract_t673389937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
