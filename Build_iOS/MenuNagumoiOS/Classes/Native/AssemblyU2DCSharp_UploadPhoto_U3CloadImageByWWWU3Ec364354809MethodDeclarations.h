﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UploadPhoto/<loadImageByWWW>c__Iterator85
struct U3CloadImageByWWWU3Ec__Iterator85_t364354809;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UploadPhoto/<loadImageByWWW>c__Iterator85::.ctor()
extern "C"  void U3CloadImageByWWWU3Ec__Iterator85__ctor_m2280939666 (U3CloadImageByWWWU3Ec__Iterator85_t364354809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UploadPhoto/<loadImageByWWW>c__Iterator85::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CloadImageByWWWU3Ec__Iterator85_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1557184576 (U3CloadImageByWWWU3Ec__Iterator85_t364354809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UploadPhoto/<loadImageByWWW>c__Iterator85::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CloadImageByWWWU3Ec__Iterator85_System_Collections_IEnumerator_get_Current_m3926606804 (U3CloadImageByWWWU3Ec__Iterator85_t364354809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UploadPhoto/<loadImageByWWW>c__Iterator85::MoveNext()
extern "C"  bool U3CloadImageByWWWU3Ec__Iterator85_MoveNext_m140414242 (U3CloadImageByWWWU3Ec__Iterator85_t364354809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto/<loadImageByWWW>c__Iterator85::Dispose()
extern "C"  void U3CloadImageByWWWU3Ec__Iterator85_Dispose_m1447981135 (U3CloadImageByWWWU3Ec__Iterator85_t364354809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto/<loadImageByWWW>c__Iterator85::Reset()
extern "C"  void U3CloadImageByWWWU3Ec__Iterator85_Reset_m4222339903 (U3CloadImageByWWWU3Ec__Iterator85_t364354809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
