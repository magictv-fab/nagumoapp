﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetScale
struct  GetScale_t1712482364  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetScale::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetScale::vector
	FsmVector3_t533912882 * ___vector_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScale::xScale
	FsmFloat_t2134102846 * ___xScale_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScale::yScale
	FsmFloat_t2134102846 * ___yScale_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScale::zScale
	FsmFloat_t2134102846 * ___zScale_13;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetScale::space
	int32_t ___space_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetScale::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_vector_10() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___vector_10)); }
	inline FsmVector3_t533912882 * get_vector_10() const { return ___vector_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector_10() { return &___vector_10; }
	inline void set_vector_10(FsmVector3_t533912882 * value)
	{
		___vector_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector_10, value);
	}

	inline static int32_t get_offset_of_xScale_11() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___xScale_11)); }
	inline FsmFloat_t2134102846 * get_xScale_11() const { return ___xScale_11; }
	inline FsmFloat_t2134102846 ** get_address_of_xScale_11() { return &___xScale_11; }
	inline void set_xScale_11(FsmFloat_t2134102846 * value)
	{
		___xScale_11 = value;
		Il2CppCodeGenWriteBarrier(&___xScale_11, value);
	}

	inline static int32_t get_offset_of_yScale_12() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___yScale_12)); }
	inline FsmFloat_t2134102846 * get_yScale_12() const { return ___yScale_12; }
	inline FsmFloat_t2134102846 ** get_address_of_yScale_12() { return &___yScale_12; }
	inline void set_yScale_12(FsmFloat_t2134102846 * value)
	{
		___yScale_12 = value;
		Il2CppCodeGenWriteBarrier(&___yScale_12, value);
	}

	inline static int32_t get_offset_of_zScale_13() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___zScale_13)); }
	inline FsmFloat_t2134102846 * get_zScale_13() const { return ___zScale_13; }
	inline FsmFloat_t2134102846 ** get_address_of_zScale_13() { return &___zScale_13; }
	inline void set_zScale_13(FsmFloat_t2134102846 * value)
	{
		___zScale_13 = value;
		Il2CppCodeGenWriteBarrier(&___zScale_13, value);
	}

	inline static int32_t get_offset_of_space_14() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___space_14)); }
	inline int32_t get_space_14() const { return ___space_14; }
	inline int32_t* get_address_of_space_14() { return &___space_14; }
	inline void set_space_14(int32_t value)
	{
		___space_14 = value;
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(GetScale_t1712482364, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
