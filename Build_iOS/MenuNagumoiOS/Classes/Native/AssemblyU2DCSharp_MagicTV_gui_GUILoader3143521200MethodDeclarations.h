﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.gui.GUILoader
struct GUILoader_t3143521200;
// System.String
struct String_t;
// MagicTV.abstracts.screens.LoadScreenAbstract
struct LoadScreenAbstract_t4059313920;
// MagicTV.abstracts.screens.MenuScreenAbstract
struct MenuScreenAbstract_t4225613753;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.gui.GUILoader::.ctor()
extern "C"  void GUILoader__ctor_m2495241135 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::prepare()
extern "C"  void GUILoader_prepare_m2261959988 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::setGUIPin(System.String)
extern "C"  void GUILoader_setGUIPin_m1106025561 (GUILoader_t3143521200 * __this, String_t* ___pin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::initInternalComponentListeners()
extern "C"  void GUILoader_initInternalComponentListeners_m270210916 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::StopPresentation()
extern "C"  void GUILoader_StopPresentation_m3556539121 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::showButtonClose()
extern "C"  void GUILoader_showButtonClose_m1725144662 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::hideButtonClose()
extern "C"  void GUILoader_hideButtonClose_m320025649 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::ResetMagicTV()
extern "C"  void GUILoader_ResetMagicTV_m1822340181 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.GUILoader::ClearMagicTVCache()
extern "C"  void GUILoader_ClearMagicTVCache_m682769805 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.screens.LoadScreenAbstract MagicTV.gui.GUILoader::getLoadScreen()
extern "C"  LoadScreenAbstract_t4059313920 * GUILoader_getLoadScreen_m724866075 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.screens.LoadScreenAbstract MagicTV.gui.GUILoader::getInitialSplashScreen()
extern "C"  LoadScreenAbstract_t4059313920 * GUILoader_getInitialSplashScreen_m1538348208 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.screens.MenuScreenAbstract MagicTV.gui.GUILoader::getMenuScreen()
extern "C"  MenuScreenAbstract_t4225613753 * GUILoader_getMenuScreen_m2429412539 (GUILoader_t3143521200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
