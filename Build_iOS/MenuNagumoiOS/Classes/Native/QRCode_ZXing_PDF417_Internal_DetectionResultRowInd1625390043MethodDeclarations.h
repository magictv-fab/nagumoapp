﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn
struct DetectionResultRowIndicatorColumn_t1625390043;
// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;
// ZXing.PDF417.Internal.BarcodeMetadata
struct BarcodeMetadata_t443526333;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.PDF417.Internal.Codeword[]
struct CodewordU5BU5D_t399100150;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_BoundingBox242606069.h"
#include "QRCode_ZXing_PDF417_Internal_BarcodeMetadata443526333.h"

// System.Boolean ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::get_IsLeft()
extern "C"  bool DetectionResultRowIndicatorColumn_get_IsLeft_m751790103 (DetectionResultRowIndicatorColumn_t1625390043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::set_IsLeft(System.Boolean)
extern "C"  void DetectionResultRowIndicatorColumn_set_IsLeft_m1141972790 (DetectionResultRowIndicatorColumn_t1625390043 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::.ctor(ZXing.PDF417.Internal.BoundingBox,System.Boolean)
extern "C"  void DetectionResultRowIndicatorColumn__ctor_m1519104496 (DetectionResultRowIndicatorColumn_t1625390043 * __this, BoundingBox_t242606069 * ___box0, bool ___isLeft1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::setRowNumbers()
extern "C"  void DetectionResultRowIndicatorColumn_setRowNumbers_m1654107139 (DetectionResultRowIndicatorColumn_t1625390043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::adjustCompleteIndicatorColumnRowNumbers(ZXing.PDF417.Internal.BarcodeMetadata)
extern "C"  int32_t DetectionResultRowIndicatorColumn_adjustCompleteIndicatorColumnRowNumbers_m2072822476 (DetectionResultRowIndicatorColumn_t1625390043 * __this, BarcodeMetadata_t443526333 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::getRowHeights()
extern "C"  Int32U5BU5D_t3230847821* DetectionResultRowIndicatorColumn_getRowHeights_m2510836997 (DetectionResultRowIndicatorColumn_t1625390043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::adjustIncompleteIndicatorColumnRowNumbers(ZXing.PDF417.Internal.BarcodeMetadata)
extern "C"  int32_t DetectionResultRowIndicatorColumn_adjustIncompleteIndicatorColumnRowNumbers_m4133137073 (DetectionResultRowIndicatorColumn_t1625390043 * __this, BarcodeMetadata_t443526333 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::getBarcodeMetadata()
extern "C"  BarcodeMetadata_t443526333 * DetectionResultRowIndicatorColumn_getBarcodeMetadata_m377686513 (DetectionResultRowIndicatorColumn_t1625390043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::removeIncorrectCodewords(ZXing.PDF417.Internal.Codeword[],ZXing.PDF417.Internal.BarcodeMetadata)
extern "C"  void DetectionResultRowIndicatorColumn_removeIncorrectCodewords_m1966373776 (DetectionResultRowIndicatorColumn_t1625390043 * __this, CodewordU5BU5D_t399100150* ___codewords0, BarcodeMetadata_t443526333 * ___metadata1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::ToString()
extern "C"  String_t* DetectionResultRowIndicatorColumn_ToString_m2676901658 (DetectionResultRowIndicatorColumn_t1625390043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
