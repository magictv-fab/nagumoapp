﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code128Writer
struct  Code128Writer_t1361534863  : public OneDimensionalCodeWriter_t4068326409
{
public:
	// System.Boolean ZXing.OneD.Code128Writer::forceCodesetB
	bool ___forceCodesetB_0;

public:
	inline static int32_t get_offset_of_forceCodesetB_0() { return static_cast<int32_t>(offsetof(Code128Writer_t1361534863, ___forceCodesetB_0)); }
	inline bool get_forceCodesetB_0() const { return ___forceCodesetB_0; }
	inline bool* get_address_of_forceCodesetB_0() { return &___forceCodesetB_0; }
	inline void set_forceCodesetB_0(bool value)
	{
		___forceCodesetB_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
