﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "mscorlib_System_Object4170816371.h"

// HutongGames.PlayMaker.INamedVariable HutongGames.PlayMaker.FsmVar::get_NamedVar()
extern "C"  Il2CppObject * FsmVar_get_NamedVar_m1814205914 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_NamedVar(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_set_NamedVar_m4254401485 (FsmVar_t1596150537 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVar::get_Type()
extern "C"  int32_t FsmVar_get_Type_m3418558332 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_Type(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmVar_set_Type_m4211541807 (FsmVar_t1596150537 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmVar::get_RealType()
extern "C"  Type_t * FsmVar_get_RealType_m2936384565 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVar::get_IsNone()
extern "C"  bool FsmVar_get_IsNone_m3892161437 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVar::get_vector2Value()
extern "C"  Vector2_t4282066565  FsmVar_get_vector2Value_m2229739328 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_vector2Value(UnityEngine.Vector2)
extern "C"  void FsmVar_set_vector2Value_m4019909569 (FsmVar_t1596150537 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVar::get_vector3Value()
extern "C"  Vector3_t4282066566  FsmVar_get_vector3Value_m3732009534 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_vector3Value(UnityEngine.Vector3)
extern "C"  void FsmVar_set_vector3Value_m686556321 (FsmVar_t1596150537 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.FsmVar::get_colorValue()
extern "C"  Color_t4194546905  FsmVar_get_colorValue_m78368088 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_colorValue(UnityEngine.Color)
extern "C"  void FsmVar_set_colorValue_m2777668225 (FsmVar_t1596150537 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.FsmVar::get_rectValue()
extern "C"  Rect_t4241904616  FsmVar_get_rectValue_m4188216422 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_rectValue(UnityEngine.Rect)
extern "C"  void FsmVar_set_rectValue_m3733561789 (FsmVar_t1596150537 * __this, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion HutongGames.PlayMaker.FsmVar::get_quaternionValue()
extern "C"  Quaternion_t1553702882  FsmVar_get_quaternionValue_m4156885030 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_quaternionValue(UnityEngine.Quaternion)
extern "C"  void FsmVar_set_quaternionValue_m1453166409 (FsmVar_t1596150537 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.FsmVar::get_gameObjectValue()
extern "C"  GameObject_t3674682005 * FsmVar_get_gameObjectValue_m1899251014 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_gameObjectValue(UnityEngine.GameObject)
extern "C"  void FsmVar_set_gameObjectValue_m2511733027 (FsmVar_t1596150537 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material HutongGames.PlayMaker.FsmVar::get_materialValue()
extern "C"  Material_t3870600107 * FsmVar_get_materialValue_m1184833606 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_materialValue(UnityEngine.Material)
extern "C"  void FsmVar_set_materialValue_m2481459383 (FsmVar_t1596150537 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture HutongGames.PlayMaker.FsmVar::get_textureValue()
extern "C"  Texture_t2526458961 * FsmVar_get_textureValue_m671557544 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_textureValue(UnityEngine.Texture)
extern "C"  void FsmVar_set_textureValue_m1646249537 (FsmVar_t1596150537 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor()
extern "C"  void FsmVar__ctor_m2050768746 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor(HutongGames.PlayMaker.FsmVar)
extern "C"  void FsmVar__ctor_m963284215 (FsmVar_t1596150537 * __this, FsmVar_t1596150537 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar__ctor_m1083948914 (FsmVar_t1596150537 * __this, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::UpdateType(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_UpdateType_m1330374469 (FsmVar_t1596150537 * __this, Il2CppObject * ___sourceVar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::Init()
extern "C"  void FsmVar_Init_m2788637098 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmVar::GetValue()
extern "C"  Il2CppObject * FsmVar_GetValue_m2309870922 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::GetValueFrom(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_GetValueFrom_m764615143 (FsmVar_t1596150537 * __this, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::UpdateValue()
extern "C"  void FsmVar_UpdateValue_m1579759472 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::ApplyValueTo(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_ApplyValueTo_m1826228640 (FsmVar_t1596150537 * __this, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVar::DebugString()
extern "C"  String_t* FsmVar_DebugString_m3885228041 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVar::ToString()
extern "C"  String_t* FsmVar_ToString_m3801243433 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::SetValue(System.Object)
extern "C"  void FsmVar_SetValue_m2004058539 (FsmVar_t1596150537 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::DebugLog()
extern "C"  void FsmVar_DebugLog_m2933373899 (FsmVar_t1596150537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
