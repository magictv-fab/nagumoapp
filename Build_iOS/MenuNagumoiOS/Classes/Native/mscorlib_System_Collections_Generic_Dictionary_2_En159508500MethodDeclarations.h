﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3027605202(__this, ___dictionary0, method) ((  void (*) (Enumerator_t159508500 *, Dictionary_2_t3137152404 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m867656217(__this, method) ((  Il2CppObject * (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4063173283(__this, method) ((  void (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2487829594(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1764812853(__this, method) ((  Il2CppObject * (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m521506183(__this, method) ((  Il2CppObject * (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::MoveNext()
#define Enumerator_MoveNext_m1106286163(__this, method) ((  bool (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::get_Current()
#define Enumerator_get_Current_m2725855241(__this, method) ((  KeyValuePair_2_t3035933110  (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1616650012(__this, method) ((  String_t* (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3057977052(__this, method) ((  InAppAnalytics_t2316734034 * (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::Reset()
#define Enumerator_Reset_m1109136932(__this, method) ((  void (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::VerifyState()
#define Enumerator_VerifyState_m2829531949(__this, method) ((  void (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3210776405(__this, method) ((  void (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.globals.InAppAnalytics>::Dispose()
#define Enumerator_Dispose_m3252131316(__this, method) ((  void (*) (Enumerator_t159508500 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
