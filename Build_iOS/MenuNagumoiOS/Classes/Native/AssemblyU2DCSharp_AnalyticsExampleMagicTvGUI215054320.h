﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Analytics
struct Analytics_t310950758;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsExampleMagicTvGUI
struct  AnalyticsExampleMagicTvGUI_t215054320  : public MonoBehaviour_t667441552
{
public:
	// Analytics AnalyticsExampleMagicTvGUI::_Analytics
	Analytics_t310950758 * ____Analytics_2;
	// System.String AnalyticsExampleMagicTvGUI::CurrentPageName
	String_t* ___CurrentPageName_3;
	// System.String AnalyticsExampleMagicTvGUI::trackingID
	String_t* ___trackingID_4;
	// System.String AnalyticsExampleMagicTvGUI::appName
	String_t* ___appName_5;

public:
	inline static int32_t get_offset_of__Analytics_2() { return static_cast<int32_t>(offsetof(AnalyticsExampleMagicTvGUI_t215054320, ____Analytics_2)); }
	inline Analytics_t310950758 * get__Analytics_2() const { return ____Analytics_2; }
	inline Analytics_t310950758 ** get_address_of__Analytics_2() { return &____Analytics_2; }
	inline void set__Analytics_2(Analytics_t310950758 * value)
	{
		____Analytics_2 = value;
		Il2CppCodeGenWriteBarrier(&____Analytics_2, value);
	}

	inline static int32_t get_offset_of_CurrentPageName_3() { return static_cast<int32_t>(offsetof(AnalyticsExampleMagicTvGUI_t215054320, ___CurrentPageName_3)); }
	inline String_t* get_CurrentPageName_3() const { return ___CurrentPageName_3; }
	inline String_t** get_address_of_CurrentPageName_3() { return &___CurrentPageName_3; }
	inline void set_CurrentPageName_3(String_t* value)
	{
		___CurrentPageName_3 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentPageName_3, value);
	}

	inline static int32_t get_offset_of_trackingID_4() { return static_cast<int32_t>(offsetof(AnalyticsExampleMagicTvGUI_t215054320, ___trackingID_4)); }
	inline String_t* get_trackingID_4() const { return ___trackingID_4; }
	inline String_t** get_address_of_trackingID_4() { return &___trackingID_4; }
	inline void set_trackingID_4(String_t* value)
	{
		___trackingID_4 = value;
		Il2CppCodeGenWriteBarrier(&___trackingID_4, value);
	}

	inline static int32_t get_offset_of_appName_5() { return static_cast<int32_t>(offsetof(AnalyticsExampleMagicTvGUI_t215054320, ___appName_5)); }
	inline String_t* get_appName_5() const { return ___appName_5; }
	inline String_t** get_address_of_appName_5() { return &___appName_5; }
	inline void set_appName_5(String_t* value)
	{
		___appName_5 = value;
		Il2CppCodeGenWriteBarrier(&___appName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
