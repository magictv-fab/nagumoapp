﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption_Layou2090144509.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.LayoutOption
struct  LayoutOption_t964995201  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.LayoutOption/LayoutOptionType HutongGames.PlayMaker.LayoutOption::option
	int32_t ___option_0;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.LayoutOption::floatParam
	FsmFloat_t2134102846 * ___floatParam_1;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.LayoutOption::boolParam
	FsmBool_t1075959796 * ___boolParam_2;

public:
	inline static int32_t get_offset_of_option_0() { return static_cast<int32_t>(offsetof(LayoutOption_t964995201, ___option_0)); }
	inline int32_t get_option_0() const { return ___option_0; }
	inline int32_t* get_address_of_option_0() { return &___option_0; }
	inline void set_option_0(int32_t value)
	{
		___option_0 = value;
	}

	inline static int32_t get_offset_of_floatParam_1() { return static_cast<int32_t>(offsetof(LayoutOption_t964995201, ___floatParam_1)); }
	inline FsmFloat_t2134102846 * get_floatParam_1() const { return ___floatParam_1; }
	inline FsmFloat_t2134102846 ** get_address_of_floatParam_1() { return &___floatParam_1; }
	inline void set_floatParam_1(FsmFloat_t2134102846 * value)
	{
		___floatParam_1 = value;
		Il2CppCodeGenWriteBarrier(&___floatParam_1, value);
	}

	inline static int32_t get_offset_of_boolParam_2() { return static_cast<int32_t>(offsetof(LayoutOption_t964995201, ___boolParam_2)); }
	inline FsmBool_t1075959796 * get_boolParam_2() const { return ___boolParam_2; }
	inline FsmBool_t1075959796 ** get_address_of_boolParam_2() { return &___boolParam_2; }
	inline void set_boolParam_2(FsmBool_t1075959796 * value)
	{
		___boolParam_2 = value;
		Il2CppCodeGenWriteBarrier(&___boolParam_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
