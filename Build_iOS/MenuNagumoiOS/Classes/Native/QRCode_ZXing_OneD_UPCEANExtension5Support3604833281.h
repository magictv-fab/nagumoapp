﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANExtension5Support
struct  UPCEANExtension5Support_t3604833281  : public Il2CppObject
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtension5Support::decodeMiddleCounters
	Int32U5BU5D_t3230847821* ___decodeMiddleCounters_1;
	// System.Text.StringBuilder ZXing.OneD.UPCEANExtension5Support::decodeRowStringBuffer
	StringBuilder_t243639308 * ___decodeRowStringBuffer_2;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_1() { return static_cast<int32_t>(offsetof(UPCEANExtension5Support_t3604833281, ___decodeMiddleCounters_1)); }
	inline Int32U5BU5D_t3230847821* get_decodeMiddleCounters_1() const { return ___decodeMiddleCounters_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_decodeMiddleCounters_1() { return &___decodeMiddleCounters_1; }
	inline void set_decodeMiddleCounters_1(Int32U5BU5D_t3230847821* value)
	{
		___decodeMiddleCounters_1 = value;
		Il2CppCodeGenWriteBarrier(&___decodeMiddleCounters_1, value);
	}

	inline static int32_t get_offset_of_decodeRowStringBuffer_2() { return static_cast<int32_t>(offsetof(UPCEANExtension5Support_t3604833281, ___decodeRowStringBuffer_2)); }
	inline StringBuilder_t243639308 * get_decodeRowStringBuffer_2() const { return ___decodeRowStringBuffer_2; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowStringBuffer_2() { return &___decodeRowStringBuffer_2; }
	inline void set_decodeRowStringBuffer_2(StringBuilder_t243639308 * value)
	{
		___decodeRowStringBuffer_2 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowStringBuffer_2, value);
	}
};

struct UPCEANExtension5Support_t3604833281_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtension5Support::CHECK_DIGIT_ENCODINGS
	Int32U5BU5D_t3230847821* ___CHECK_DIGIT_ENCODINGS_0;

public:
	inline static int32_t get_offset_of_CHECK_DIGIT_ENCODINGS_0() { return static_cast<int32_t>(offsetof(UPCEANExtension5Support_t3604833281_StaticFields, ___CHECK_DIGIT_ENCODINGS_0)); }
	inline Int32U5BU5D_t3230847821* get_CHECK_DIGIT_ENCODINGS_0() const { return ___CHECK_DIGIT_ENCODINGS_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_CHECK_DIGIT_ENCODINGS_0() { return &___CHECK_DIGIT_ENCODINGS_0; }
	inline void set_CHECK_DIGIT_ENCODINGS_0(Int32U5BU5D_t3230847821* value)
	{
		___CHECK_DIGIT_ENCODINGS_0 = value;
		Il2CppCodeGenWriteBarrier(&___CHECK_DIGIT_ENCODINGS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
