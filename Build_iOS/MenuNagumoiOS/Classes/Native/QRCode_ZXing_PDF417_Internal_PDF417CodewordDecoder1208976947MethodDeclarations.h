﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.PDF417.Internal.PDF417CodewordDecoder::.cctor()
extern "C"  void PDF417CodewordDecoder__cctor_m3087769650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getDecodedValue(System.Int32[])
extern "C"  int32_t PDF417CodewordDecoder_getDecodedValue_m3557401639 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.PDF417CodewordDecoder::sampleBitCounts(System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* PDF417CodewordDecoder_sampleBitCounts_m326185819 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getDecodedCodewordValue(System.Int32[])
extern "C"  int32_t PDF417CodewordDecoder_getDecodedCodewordValue_m2382108656 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getBitValue(System.Int32[])
extern "C"  int32_t PDF417CodewordDecoder_getBitValue_m1935138224 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getClosestDecodedValue(System.Int32[])
extern "C"  int32_t PDF417CodewordDecoder_getClosestDecodedValue_m640301032 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___moduleBitCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
