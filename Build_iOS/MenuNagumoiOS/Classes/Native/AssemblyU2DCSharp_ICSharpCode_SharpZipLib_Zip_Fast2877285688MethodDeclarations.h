﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate
struct ConfirmOverwriteDelegate_t2877285688;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ConfirmOverwriteDelegate__ctor_m3271072223 (ConfirmOverwriteDelegate_t2877285688 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate::Invoke(System.String)
extern "C"  bool ConfirmOverwriteDelegate_Invoke_m1875560821 (ConfirmOverwriteDelegate_t2877285688 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConfirmOverwriteDelegate_BeginInvoke_m1334438446 (ConfirmOverwriteDelegate_t2877285688 * __this, String_t* ___fileName0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool ConfirmOverwriteDelegate_EndInvoke_m4141337635 (ConfirmOverwriteDelegate_t2877285688 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
