﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseButtonUp
struct GetMouseButtonUp_t1832714532;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::.ctor()
extern "C"  void GetMouseButtonUp__ctor_m3810585026 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::Reset()
extern "C"  void GetMouseButtonUp_Reset_m1457017967 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::OnUpdate()
extern "C"  void GetMouseButtonUp_OnUpdate_m2609891402 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
