﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_OSNotificationAction_ActionType2045309690.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSNotificationAction
struct  OSNotificationAction_t173272069  : public Il2CppObject
{
public:
	// System.String OSNotificationAction::actionID
	String_t* ___actionID_0;
	// OSNotificationAction/ActionType OSNotificationAction::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_actionID_0() { return static_cast<int32_t>(offsetof(OSNotificationAction_t173272069, ___actionID_0)); }
	inline String_t* get_actionID_0() const { return ___actionID_0; }
	inline String_t** get_address_of_actionID_0() { return &___actionID_0; }
	inline void set_actionID_0(String_t* value)
	{
		___actionID_0 = value;
		Il2CppCodeGenWriteBarrier(&___actionID_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(OSNotificationAction_t173272069, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
