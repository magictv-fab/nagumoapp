﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveRect
struct CurveRect_t881836737;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveRect::.ctor()
extern "C"  void CurveRect__ctor_m2092288917 (CurveRect_t881836737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::Reset()
extern "C"  void CurveRect_Reset_m4033689154 (CurveRect_t881836737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::OnEnter()
extern "C"  void CurveRect_OnEnter_m36683308 (CurveRect_t881836737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::OnExit()
extern "C"  void CurveRect_OnExit_m1949765612 (CurveRect_t881836737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::OnUpdate()
extern "C"  void CurveRect_OnUpdate_m270741911 (CurveRect_t881836737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
