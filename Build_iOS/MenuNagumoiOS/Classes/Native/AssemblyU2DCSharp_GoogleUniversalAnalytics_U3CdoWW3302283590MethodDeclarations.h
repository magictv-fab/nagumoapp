﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34
struct U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::.ctor()
extern "C"  void U3CdoWWWRequestAndCheckResultU3Ec__Iterator34__ctor_m1105241621 (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1815337383 (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_System_Collections_IEnumerator_get_Current_m1770261307 (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::MoveNext()
extern "C"  bool U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_MoveNext_m261643943 (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::Dispose()
extern "C"  void U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_Dispose_m1178558738 (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::Reset()
extern "C"  void U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_Reset_m3046641858 (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
