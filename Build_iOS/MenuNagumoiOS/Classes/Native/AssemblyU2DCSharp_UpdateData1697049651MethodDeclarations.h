﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateData
struct UpdateData_t1697049651;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UpdateData::.ctor()
extern "C"  void UpdateData__ctor_m5223880 (UpdateData_t1697049651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateData::.cctor()
extern "C"  void UpdateData__cctor_m3974811365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateData::Start()
extern "C"  void UpdateData_Start_m3247328968 (UpdateData_t1697049651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UpdateData::ILogin()
extern "C"  Il2CppObject * UpdateData_ILogin_m3634491012 (UpdateData_t1697049651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UpdateData::Base64Encode(System.String)
extern "C"  String_t* UpdateData_Base64Encode_m3255838366 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
