﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAmbientLight
struct SetAmbientLight_t3560184814;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAmbientLight::.ctor()
extern "C"  void SetAmbientLight__ctor_m2182588168 (SetAmbientLight_t3560184814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAmbientLight::Reset()
extern "C"  void SetAmbientLight_Reset_m4123988405 (SetAmbientLight_t3560184814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAmbientLight::OnEnter()
extern "C"  void SetAmbientLight_OnEnter_m914917599 (SetAmbientLight_t3560184814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAmbientLight::OnUpdate()
extern "C"  void SetAmbientLight_OnUpdate_m1726201156 (SetAmbientLight_t3560184814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAmbientLight::DoSetAmbientColor()
extern "C"  void SetAmbientLight_DoSetAmbientColor_m2550602056 (SetAmbientLight_t3560184814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
