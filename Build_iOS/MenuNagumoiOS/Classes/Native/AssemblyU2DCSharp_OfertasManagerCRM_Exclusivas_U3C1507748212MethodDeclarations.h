﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8
struct U3CISendStatusU3Ec__Iterator8_t1507748212;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator8__ctor_m2316619175 (U3CISendStatusU3Ec__Iterator8_t1507748212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1107685589 (U3CISendStatusU3Ec__Iterator8_t1507748212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1301692009 (U3CISendStatusU3Ec__Iterator8_t1507748212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator8_MoveNext_m2019697621 (U3CISendStatusU3Ec__Iterator8_t1507748212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator8_Dispose_m1376250916 (U3CISendStatusU3Ec__Iterator8_t1507748212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<ISendStatus>c__Iterator8::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator8_Reset_m4258019412 (U3CISendStatusU3Ec__Iterator8_t1507748212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
