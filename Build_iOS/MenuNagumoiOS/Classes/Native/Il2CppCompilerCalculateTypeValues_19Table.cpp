﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_BaseMachine768653060.h"
#include "System_System_Text_RegularExpressions_BaseMachine_1285000982.h"
#include "System_System_Text_RegularExpressions_FactoryCache2263956512.h"
#include "System_System_Text_RegularExpressions_FactoryCache_447833941.h"
#include "System_System_Text_RegularExpressions_MRUList583528572.h"
#include "System_System_Text_RegularExpressions_MRUList_Node1521789168.h"
#include "System_System_Text_RegularExpressions_CaptureColle1173899026.h"
#include "System_System_Text_RegularExpressions_Capture754001812.h"
#include "System_System_Text_RegularExpressions_CILCompiler2504238675.h"
#include "System_System_Text_RegularExpressions_CILCompiler_2599208150.h"
#include "System_System_Text_RegularExpressions_Category1962353414.h"
#include "System_System_Text_RegularExpressions_CategoryUtils734718049.h"
#include "System_System_Text_RegularExpressions_LinkRef378484359.h"
#include "System_System_Text_RegularExpressions_InterpreterF3485052664.h"
#include "System_System_Text_RegularExpressions_PatternCompi3989440925.h"
#include "System_System_Text_RegularExpressions_PatternCompi3356653547.h"
#include "System_System_Text_RegularExpressions_PatternCompil396179390.h"
#include "System_System_Text_RegularExpressions_LinkStack1760044604.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "System_System_Text_RegularExpressions_GroupCollecti982584267.h"
#include "System_System_Text_RegularExpressions_Group2151468941.h"
#include "System_System_Text_RegularExpressions_Interpreter4223808840.h"
#include "System_System_Text_RegularExpressions_Interpreter_3630763131.h"
#include "System_System_Text_RegularExpressions_Interpreter_1764265010.h"
#include "System_System_Text_RegularExpressions_Interpreter_1939935045.h"
#include "System_System_Text_RegularExpressions_Interval2482260685.h"
#include "System_System_Text_RegularExpressions_IntervalColl1888974603.h"
#include "System_System_Text_RegularExpressions_IntervalColl3676602947.h"
#include "System_System_Text_RegularExpressions_IntervalColl1292950321.h"
#include "System_System_Text_RegularExpressions_MatchCollect3437694865.h"
#include "System_System_Text_RegularExpressions_MatchCollect2292349437.h"
#include "System_System_Text_RegularExpressions_Match2156507859.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse3926544077.h"
#include "System_System_Text_RegularExpressions_QuickSearch2109051075.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "System_System_Text_RegularExpressions_RegexOptions3066443743.h"
#include "System_System_Text_RegularExpressions_RegexRunnerF1605900411.h"
#include "System_System_Text_RegularExpressions_RxInterprete3581763170.h"
#include "System_System_Text_RegularExpressions_RxInterprete1403093153.h"
#include "System_System_Text_RegularExpressions_RxInterprete1676722124.h"
#include "System_System_Text_RegularExpressions_RxLinkRef4029611169.h"
#include "System_System_Text_RegularExpressions_RxCompiler2828095437.h"
#include "System_System_Text_RegularExpressions_RxInterpreter984413342.h"
#include "System_System_Text_RegularExpressions_RxOp3811709775.h"
#include "System_System_Text_RegularExpressions_ReplacementEv697641157.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre2279826820.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3183027782.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo2628834993.h"
#include "System_System_Text_RegularExpressions_Syntax_Group3733269553.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul1862766086.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu2867563114.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan1061768724.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBa3370744674.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet2377834527.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser3981028276.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3434259338.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3534504316.h"
#include "System_System_Text_RegularExpressions_Syntax_Alter3434519311.h"
#include "System_System_Text_RegularExpressions_Syntax_Liter2061497825.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit3788287627.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1741476861.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs2734841617.h"
#include "System_System_Text_RegularExpressions_Syntax_Chara2058232957.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho3681078449.h"
#include "System_System_Timers_ElapsedEventArgs2035959611.h"
#include "System_System_Timers_Timer3701448099.h"
#include "System_System_Timers_TimersDescriptionAttribute326150766.h"
#include "System_System_UriBuilder1899340099.h"
#include "System_System_Uri1116831938.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "System_System_UriFormatException308538560.h"
#include "System_System_UriHostNameType959572879.h"
#include "System_System_UriKind238866934.h"
#include "System_System_UriParser3685110593.h"
#include "System_System_UriPartial875461417.h"
#include "System_System_UriTypeConverter2523083502.h"
#include "System_System_Runtime_InteropServices_HandleCollec3854517599.h"
#include "System_System_Runtime_InteropServices_StandardOleMar25375932.h"
#include "System_System_Net_BindIPEndPoint3006124499.h"
#include "System_System_Net_HttpContinueDelegate1707598350.h"
#include "System_System_Net_Security_LocalCertificateSelecti2431285719.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "System_System_Text_RegularExpressions_MatchEvaluat1719977010.h"
#include "System_System_Text_RegularExpressions_EvalDelegate4101713353.h"
#include "System_System_Timers_ElapsedEventHandler630471434.h"
#include "System_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220352.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1676615740.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220348.h"
#include "Mono_Posix_U3CModuleU3E86524790.h"
#include "System_Core_U3CModuleU3E86524790.h"
#include "System_Core_System_Runtime_CompilerServices_Extens2299149759.h"
#include "System_Core_Locale2281372282.h"
#include "System_Core_System_MonoTODOAttribute2091695241.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder373726640.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTra131863657.h"
#include "System_Core_System_Linq_Check10677726.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "System_Core_System_Linq_Enumerable_Fallback3964967226.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (BaseMachine_t768653060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[1] = 
{
	BaseMachine_t768653060::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (MatchAppendEvaluator_t1285000982), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (FactoryCache_t2263956512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[3] = 
{
	FactoryCache_t2263956512::get_offset_of_capacity_0(),
	FactoryCache_t2263956512::get_offset_of_factories_1(),
	FactoryCache_t2263956512::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (Key_t447833941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[2] = 
{
	Key_t447833941::get_offset_of_pattern_0(),
	Key_t447833941::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (MRUList_t583528572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[2] = 
{
	MRUList_t583528572::get_offset_of_head_0(),
	MRUList_t583528572::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (Node_t1521789168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	Node_t1521789168::get_offset_of_value_0(),
	Node_t1521789168::get_offset_of_previous_1(),
	Node_t1521789168::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (CaptureCollection_t1173899026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[1] = 
{
	CaptureCollection_t1173899026::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (Capture_t754001812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[3] = 
{
	Capture_t754001812::get_offset_of_index_0(),
	Capture_t754001812::get_offset_of_length_1(),
	Capture_t754001812::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (CILCompiler_t2504238675), -1, sizeof(CILCompiler_t2504238675_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1908[32] = 
{
	CILCompiler_t2504238675::get_offset_of_eval_methods_2(),
	CILCompiler_t2504238675::get_offset_of_eval_methods_defined_3(),
	CILCompiler_t2504238675::get_offset_of_generic_ops_4(),
	CILCompiler_t2504238675::get_offset_of_op_flags_5(),
	CILCompiler_t2504238675::get_offset_of_labels_6(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_str_7(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_string_start_8(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_string_end_9(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_program_10(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_marks_11(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_groups_12(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_deep_13(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_stack_14(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_mark_start_15(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_mark_end_16(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_fi_mark_index_17(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_stack_get_count_18(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_stack_set_count_19(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_stack_push_20(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_stack_pop_21(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_set_start_of_match_22(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_is_word_char_23(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_reset_groups_24(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_checkpoint_25(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_backtrack_26(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_open_27(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_close_28(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_get_last_defined_29(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_mark_get_index_30(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_mi_mark_get_length_31(),
	CILCompiler_t2504238675_StaticFields::get_offset_of_trace_compile_32(),
	CILCompiler_t2504238675::get_offset_of_local_textinfo_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (Frame_t2599208150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[2] = 
{
	Frame_t2599208150::get_offset_of_label_pass_0(),
	Frame_t2599208150::get_offset_of_label_fail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Category_t1962353414)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[146] = 
{
	Category_t1962353414::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (CategoryUtils_t734718049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (LinkRef_t378484359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (InterpreterFactory_t3485052664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[4] = 
{
	InterpreterFactory_t3485052664::get_offset_of_mapping_0(),
	InterpreterFactory_t3485052664::get_offset_of_pattern_1(),
	InterpreterFactory_t3485052664::get_offset_of_namesMapping_2(),
	InterpreterFactory_t3485052664::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (PatternCompiler_t3989440925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[1] = 
{
	PatternCompiler_t3989440925::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (PatternLinkStack_t3356653547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[1] = 
{
	PatternLinkStack_t3356653547::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (Link_t396179390)+ sizeof (Il2CppObject), sizeof(Link_t396179390_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1917[2] = 
{
	Link_t396179390::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t396179390::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (LinkStack_t1760044604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[1] = 
{
	LinkStack_t1760044604::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (Mark_t3811539797)+ sizeof (Il2CppObject), sizeof(Mark_t3811539797_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[3] = 
{
	Mark_t3811539797::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3811539797::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3811539797::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (GroupCollection_t982584267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	GroupCollection_t982584267::get_offset_of_list_0(),
	GroupCollection_t982584267::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (Group_t2151468941), -1, sizeof(Group_t2151468941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1921[3] = 
{
	Group_t2151468941_StaticFields::get_offset_of_Fail_3(),
	Group_t2151468941::get_offset_of_success_4(),
	Group_t2151468941::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Interpreter_t4223808840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[16] = 
{
	Interpreter_t4223808840::get_offset_of_program_1(),
	Interpreter_t4223808840::get_offset_of_program_start_2(),
	Interpreter_t4223808840::get_offset_of_text_3(),
	Interpreter_t4223808840::get_offset_of_text_end_4(),
	Interpreter_t4223808840::get_offset_of_group_count_5(),
	Interpreter_t4223808840::get_offset_of_match_min_6(),
	Interpreter_t4223808840::get_offset_of_qs_7(),
	Interpreter_t4223808840::get_offset_of_scan_ptr_8(),
	Interpreter_t4223808840::get_offset_of_repeat_9(),
	Interpreter_t4223808840::get_offset_of_fast_10(),
	Interpreter_t4223808840::get_offset_of_stack_11(),
	Interpreter_t4223808840::get_offset_of_deep_12(),
	Interpreter_t4223808840::get_offset_of_marks_13(),
	Interpreter_t4223808840::get_offset_of_mark_start_14(),
	Interpreter_t4223808840::get_offset_of_mark_end_15(),
	Interpreter_t4223808840::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (IntStack_t3630763131)+ sizeof (Il2CppObject), sizeof(IntStack_t3630763131_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[2] = 
{
	IntStack_t3630763131::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t3630763131::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (RepeatContext_t1764265010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[7] = 
{
	RepeatContext_t1764265010::get_offset_of_start_0(),
	RepeatContext_t1764265010::get_offset_of_min_1(),
	RepeatContext_t1764265010::get_offset_of_max_2(),
	RepeatContext_t1764265010::get_offset_of_lazy_3(),
	RepeatContext_t1764265010::get_offset_of_expr_pc_4(),
	RepeatContext_t1764265010::get_offset_of_previous_5(),
	RepeatContext_t1764265010::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (Mode_t1939935045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[4] = 
{
	Mode_t1939935045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Interval_t2482260685)+ sizeof (Il2CppObject), sizeof(Interval_t2482260685_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1926[3] = 
{
	Interval_t2482260685::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2482260685::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2482260685::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (IntervalCollection_t1888974603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[1] = 
{
	IntervalCollection_t1888974603::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Enumerator_t3676602947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	Enumerator_t3676602947::get_offset_of_list_0(),
	Enumerator_t3676602947::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (CostDelegate_t1292950321), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (MatchCollection_t3437694865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[2] = 
{
	MatchCollection_t3437694865::get_offset_of_current_0(),
	MatchCollection_t3437694865::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (Enumerator_t2292349437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[2] = 
{
	Enumerator_t2292349437::get_offset_of_index_0(),
	Enumerator_t2292349437::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (Match_t2156507859), -1, sizeof(Match_t2156507859_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1932[5] = 
{
	Match_t2156507859::get_offset_of_regex_6(),
	Match_t2156507859::get_offset_of_machine_7(),
	Match_t2156507859::get_offset_of_text_length_8(),
	Match_t2156507859::get_offset_of_groups_9(),
	Match_t2156507859_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (Parser_t3926544077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[6] = 
{
	Parser_t3926544077::get_offset_of_pattern_0(),
	Parser_t3926544077::get_offset_of_ptr_1(),
	Parser_t3926544077::get_offset_of_caps_2(),
	Parser_t3926544077::get_offset_of_refs_3(),
	Parser_t3926544077::get_offset_of_num_groups_4(),
	Parser_t3926544077::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (QuickSearch_t2109051075), -1, sizeof(QuickSearch_t2109051075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1934[7] = 
{
	QuickSearch_t2109051075::get_offset_of_str_0(),
	QuickSearch_t2109051075::get_offset_of_len_1(),
	QuickSearch_t2109051075::get_offset_of_ignore_2(),
	QuickSearch_t2109051075::get_offset_of_reverse_3(),
	QuickSearch_t2109051075::get_offset_of_shift_4(),
	QuickSearch_t2109051075::get_offset_of_shiftExtended_5(),
	QuickSearch_t2109051075_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (Regex_t2161232213), -1, sizeof(Regex_t2161232213_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1935[16] = 
{
	Regex_t2161232213_StaticFields::get_offset_of_cache_0(),
	Regex_t2161232213_StaticFields::get_offset_of_old_rx_1(),
	Regex_t2161232213::get_offset_of_machineFactory_2(),
	Regex_t2161232213::get_offset_of_mapping_3(),
	Regex_t2161232213::get_offset_of_group_count_4(),
	Regex_t2161232213::get_offset_of_gap_5(),
	Regex_t2161232213::get_offset_of_refsInitialized_6(),
	Regex_t2161232213::get_offset_of_group_names_7(),
	Regex_t2161232213::get_offset_of_group_numbers_8(),
	Regex_t2161232213::get_offset_of_pattern_9(),
	Regex_t2161232213::get_offset_of_roptions_10(),
	Regex_t2161232213::get_offset_of_capnames_11(),
	Regex_t2161232213::get_offset_of_caps_12(),
	Regex_t2161232213::get_offset_of_factory_13(),
	Regex_t2161232213::get_offset_of_capsize_14(),
	Regex_t2161232213::get_offset_of_capslist_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (RegexOptions_t3066443743)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1936[11] = 
{
	RegexOptions_t3066443743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (RegexRunnerFactory_t1605900411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (RxInterpreter_t3581763170), -1, sizeof(RxInterpreter_t3581763170_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1938[14] = 
{
	RxInterpreter_t3581763170::get_offset_of_program_1(),
	RxInterpreter_t3581763170::get_offset_of_str_2(),
	RxInterpreter_t3581763170::get_offset_of_string_start_3(),
	RxInterpreter_t3581763170::get_offset_of_string_end_4(),
	RxInterpreter_t3581763170::get_offset_of_group_count_5(),
	RxInterpreter_t3581763170::get_offset_of_groups_6(),
	RxInterpreter_t3581763170::get_offset_of_eval_del_7(),
	RxInterpreter_t3581763170::get_offset_of_marks_8(),
	RxInterpreter_t3581763170::get_offset_of_mark_start_9(),
	RxInterpreter_t3581763170::get_offset_of_mark_end_10(),
	RxInterpreter_t3581763170::get_offset_of_stack_11(),
	RxInterpreter_t3581763170::get_offset_of_repeat_12(),
	RxInterpreter_t3581763170::get_offset_of_deep_13(),
	RxInterpreter_t3581763170_StaticFields::get_offset_of_trace_rx_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (IntStack_t1403093153)+ sizeof (Il2CppObject), sizeof(IntStack_t1403093153_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[2] = 
{
	IntStack_t1403093153::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t1403093153::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (RepeatContext_t1676722124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[7] = 
{
	RepeatContext_t1676722124::get_offset_of_start_0(),
	RepeatContext_t1676722124::get_offset_of_min_1(),
	RepeatContext_t1676722124::get_offset_of_max_2(),
	RepeatContext_t1676722124::get_offset_of_lazy_3(),
	RepeatContext_t1676722124::get_offset_of_expr_pc_4(),
	RepeatContext_t1676722124::get_offset_of_previous_5(),
	RepeatContext_t1676722124::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (RxLinkRef_t4029611169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[2] = 
{
	RxLinkRef_t4029611169::get_offset_of_offsets_0(),
	RxLinkRef_t4029611169::get_offset_of_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (RxCompiler_t2828095437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[2] = 
{
	RxCompiler_t2828095437::get_offset_of_program_0(),
	RxCompiler_t2828095437::get_offset_of_curpos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (RxInterpreterFactory_t984413342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[5] = 
{
	RxInterpreterFactory_t984413342::get_offset_of_mapping_0(),
	RxInterpreterFactory_t984413342::get_offset_of_program_1(),
	RxInterpreterFactory_t984413342::get_offset_of_eval_del_2(),
	RxInterpreterFactory_t984413342::get_offset_of_namesMapping_3(),
	RxInterpreterFactory_t984413342::get_offset_of_gap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (RxOp_t3811709775)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1944[160] = 
{
	RxOp_t3811709775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (ReplacementEvaluator_t697641157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[4] = 
{
	ReplacementEvaluator_t697641157::get_offset_of_regex_0(),
	ReplacementEvaluator_t697641157::get_offset_of_n_pieces_1(),
	ReplacementEvaluator_t697641157::get_offset_of_pieces_2(),
	ReplacementEvaluator_t697641157::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (ExpressionCollection_t2279826820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (Expression_t3183027782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (CompositeExpression_t2628834993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[1] = 
{
	CompositeExpression_t2628834993::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (Group_t3733269553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (RegularExpression_t1862766086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[1] = 
{
	RegularExpression_t1862766086::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (CapturingGroup_t2867563114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	CapturingGroup_t2867563114::get_offset_of_gid_1(),
	CapturingGroup_t2867563114::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (BalancingGroup_t1061768724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[1] = 
{
	BalancingGroup_t1061768724::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (NonBacktrackingGroup_t3370744674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (Repetition_t2377834527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[3] = 
{
	Repetition_t2377834527::get_offset_of_min_1(),
	Repetition_t2377834527::get_offset_of_max_2(),
	Repetition_t2377834527::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (Assertion_t3981028276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (CaptureAssertion_t3434259338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[3] = 
{
	CaptureAssertion_t3434259338::get_offset_of_alternate_1(),
	CaptureAssertion_t3434259338::get_offset_of_group_2(),
	CaptureAssertion_t3434259338::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (ExpressionAssertion_t3534504316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[2] = 
{
	ExpressionAssertion_t3534504316::get_offset_of_reverse_1(),
	ExpressionAssertion_t3534504316::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (Alternation_t3434519311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (Literal_t2061497825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[2] = 
{
	Literal_t2061497825::get_offset_of_str_0(),
	Literal_t2061497825::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (PositionAssertion_t3788287627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[1] = 
{
	PositionAssertion_t3788287627::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (Reference_t1741476861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[2] = 
{
	Reference_t1741476861::get_offset_of_group_0(),
	Reference_t1741476861::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (BackslashNumber_t2734841617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[2] = 
{
	BackslashNumber_t2734841617::get_offset_of_literal_2(),
	BackslashNumber_t2734841617::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (CharacterClass_t2058232957), -1, sizeof(CharacterClass_t2058232957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1963[6] = 
{
	CharacterClass_t2058232957_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t2058232957::get_offset_of_negate_1(),
	CharacterClass_t2058232957::get_offset_of_ignore_2(),
	CharacterClass_t2058232957::get_offset_of_pos_cats_3(),
	CharacterClass_t2058232957::get_offset_of_neg_cats_4(),
	CharacterClass_t2058232957::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (AnchorInfo_t3681078449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[6] = 
{
	AnchorInfo_t3681078449::get_offset_of_expr_0(),
	AnchorInfo_t3681078449::get_offset_of_pos_1(),
	AnchorInfo_t3681078449::get_offset_of_offset_2(),
	AnchorInfo_t3681078449::get_offset_of_str_3(),
	AnchorInfo_t3681078449::get_offset_of_width_4(),
	AnchorInfo_t3681078449::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (ElapsedEventArgs_t2035959611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[1] = 
{
	ElapsedEventArgs_t2035959611::get_offset_of_time_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (Timer_t3701448099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[6] = 
{
	Timer_t3701448099::get_offset_of_interval_4(),
	Timer_t3701448099::get_offset_of_autoReset_5(),
	Timer_t3701448099::get_offset_of_timer_6(),
	Timer_t3701448099::get_offset_of__lock_7(),
	Timer_t3701448099::get_offset_of_so_8(),
	Timer_t3701448099::get_offset_of_Elapsed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (TimersDescriptionAttribute_t326150766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (UriBuilder_t1899340099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[10] = 
{
	UriBuilder_t1899340099::get_offset_of_scheme_0(),
	UriBuilder_t1899340099::get_offset_of_host_1(),
	UriBuilder_t1899340099::get_offset_of_port_2(),
	UriBuilder_t1899340099::get_offset_of_path_3(),
	UriBuilder_t1899340099::get_offset_of_query_4(),
	UriBuilder_t1899340099::get_offset_of_fragment_5(),
	UriBuilder_t1899340099::get_offset_of_username_6(),
	UriBuilder_t1899340099::get_offset_of_password_7(),
	UriBuilder_t1899340099::get_offset_of_uri_8(),
	UriBuilder_t1899340099::get_offset_of_modified_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (Uri_t1116831938), -1, sizeof(Uri_t1116831938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1969[38] = 
{
	0,
	Uri_t1116831938::get_offset_of_isUnixFilePath_1(),
	Uri_t1116831938::get_offset_of_source_2(),
	Uri_t1116831938::get_offset_of_scheme_3(),
	Uri_t1116831938::get_offset_of_host_4(),
	Uri_t1116831938::get_offset_of_port_5(),
	Uri_t1116831938::get_offset_of_path_6(),
	Uri_t1116831938::get_offset_of_query_7(),
	Uri_t1116831938::get_offset_of_fragment_8(),
	Uri_t1116831938::get_offset_of_userinfo_9(),
	Uri_t1116831938::get_offset_of_isUnc_10(),
	Uri_t1116831938::get_offset_of_isOpaquePart_11(),
	Uri_t1116831938::get_offset_of_isAbsoluteUri_12(),
	Uri_t1116831938::get_offset_of_segments_13(),
	Uri_t1116831938::get_offset_of_userEscaped_14(),
	Uri_t1116831938::get_offset_of_cachedAbsoluteUri_15(),
	Uri_t1116831938::get_offset_of_cachedToString_16(),
	Uri_t1116831938::get_offset_of_cachedLocalPath_17(),
	Uri_t1116831938::get_offset_of_cachedHashCode_18(),
	Uri_t1116831938_StaticFields::get_offset_of_hexUpperChars_19(),
	Uri_t1116831938_StaticFields::get_offset_of_SchemeDelimiter_20(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeFile_21(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeFtp_22(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeGopher_23(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeHttp_24(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeHttps_25(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeMailto_26(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNews_27(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNntp_28(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNetPipe_29(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNetTcp_30(),
	Uri_t1116831938_StaticFields::get_offset_of_schemes_31(),
	Uri_t1116831938::get_offset_of_parser_32(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_33(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_34(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_35(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_36(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (UriScheme_t1290668975)+ sizeof (Il2CppObject), sizeof(UriScheme_t1290668975_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1970[3] = 
{
	UriScheme_t1290668975::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1290668975::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1290668975::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (UriFormatException_t308538560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (UriHostNameType_t959572879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[6] = 
{
	UriHostNameType_t959572879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (UriKind_t238866934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[4] = 
{
	UriKind_t238866934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (UriParser_t3685110593), -1, sizeof(UriParser_t3685110593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1974[6] = 
{
	UriParser_t3685110593_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t3685110593_StaticFields::get_offset_of_table_1(),
	UriParser_t3685110593::get_offset_of_scheme_name_2(),
	UriParser_t3685110593::get_offset_of_default_port_3(),
	UriParser_t3685110593_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t3685110593_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (UriPartial_t875461417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[5] = 
{
	UriPartial_t875461417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (UriTypeConverter_t2523083502), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (HandleCollector_t3854517599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[5] = 
{
	HandleCollector_t3854517599::get_offset_of_count_0(),
	HandleCollector_t3854517599::get_offset_of_init_1(),
	HandleCollector_t3854517599::get_offset_of_max_2(),
	HandleCollector_t3854517599::get_offset_of_name_3(),
	HandleCollector_t3854517599::get_offset_of_previous_collection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (StandardOleMarshalObject_t25375932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (BindIPEndPoint_t3006124499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (HttpContinueDelegate_t1707598350), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (LocalCertificateSelectionCallback_t2431285719), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (RemoteCertificateValidationCallback_t1894914657), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (MatchEvaluator_t1719977010), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (EvalDelegate_t4101713353), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (ElapsedEventHandler_t630471434), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238936), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1986[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D3_1(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D4_2(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D5_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (U24ArrayTypeU2416_t3379220354)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220354_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (U24ArrayTypeU24128_t1676615741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t1676615741_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (U3CModuleU3E_t86524797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (ExtensionAttribute_t2299149759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (Locale_t2281372285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (MonoTODOAttribute_t2091695245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (KeyBuilder_t373726642), -1, sizeof(KeyBuilder_t373726642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1995[1] = 
{
	KeyBuilder_t373726642_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (SymmetricTransform_t131863658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[12] = 
{
	SymmetricTransform_t131863658::get_offset_of_algo_0(),
	SymmetricTransform_t131863658::get_offset_of_encrypt_1(),
	SymmetricTransform_t131863658::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t131863658::get_offset_of_temp_3(),
	SymmetricTransform_t131863658::get_offset_of_temp2_4(),
	SymmetricTransform_t131863658::get_offset_of_workBuff_5(),
	SymmetricTransform_t131863658::get_offset_of_workout_6(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t131863658::get_offset_of_m_disposed_9(),
	SymmetricTransform_t131863658::get_offset_of_lastBlock_10(),
	SymmetricTransform_t131863658::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (Check_t10677726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (Enumerable_t839044124), -1, sizeof(Enumerable_t839044124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1998[1] = 
{
	Enumerable_t839044124_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (Fallback_t3964967226)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1999[3] = 
{
	Fallback_t3964967226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
