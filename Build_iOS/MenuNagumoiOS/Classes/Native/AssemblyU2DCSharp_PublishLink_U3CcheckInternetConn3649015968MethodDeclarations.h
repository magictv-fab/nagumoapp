﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishLink/<checkInternetConnection>c__Iterator73
struct U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishLink/<checkInternetConnection>c__Iterator73::.ctor()
extern "C"  void U3CcheckInternetConnectionU3Ec__Iterator73__ctor_m2070463483 (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishLink/<checkInternetConnection>c__Iterator73::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcheckInternetConnectionU3Ec__Iterator73_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3783895425 (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishLink/<checkInternetConnection>c__Iterator73::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcheckInternetConnectionU3Ec__Iterator73_System_Collections_IEnumerator_get_Current_m1914228501 (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishLink/<checkInternetConnection>c__Iterator73::MoveNext()
extern "C"  bool U3CcheckInternetConnectionU3Ec__Iterator73_MoveNext_m3144521473 (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishLink/<checkInternetConnection>c__Iterator73::Dispose()
extern "C"  void U3CcheckInternetConnectionU3Ec__Iterator73_Dispose_m1043832184 (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishLink/<checkInternetConnection>c__Iterator73::Reset()
extern "C"  void U3CcheckInternetConnectionU3Ec__Iterator73_Reset_m4011863720 (U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
