﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2253448098.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour1845338141.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour2450634316.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour433318935.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH3463640687.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour2034767101.h"
#include "AssemblyU2DCSharp_iTween3087282050.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "AssemblyU2DCSharp_iTween_NamedValueColor1694108638.h"
#include "AssemblyU2DCSharp_iTween_Defaults4166900319.h"
#include "AssemblyU2DCSharp_iTween_CRSpline2211016973.h"
#include "AssemblyU2DCSharp_iTween_EasingFunction1323017328.h"
#include "AssemblyU2DCSharp_iTween_ApplyTween882368618.h"
#include "AssemblyU2DCSharp_iTween_U3CTweenDelayU3Ec__Iterat1287629881.h"
#include "AssemblyU2DCSharp_iTween_U3CTweenRestartU3Ec__Iter3643232966.h"
#include "AssemblyU2DCSharp_iTween_U3CStartU3Ec__Iterator891062998983.h"
#include "AssemblyU2DCSharp_AutoSwapScroll2597364079.h"
#include "AssemblyU2DCSharp_AutoSwapScroll_U3CStartU3Ec__Ite3846047746.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Curved2079821603.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_FlowLa2276891933.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Horizo2651831999.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Radial1907465325.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_ScrollS531428411.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll1065660817.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll3629032906.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll1686927404.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll2472858916.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll2882117233.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_ScrollSn16447307.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_TableL3201339275.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_TableL4192955334.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_TileSiz255948707.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_UIVert2397627752.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_UIVert3853148749.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Vertic3205272913.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_OnInAppCommonEve1653412278.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_OnInAppProgressE3139301368.h"
#include "AssemblyU2DCSharp_ARM_utils_request_OnRequestEventH153308946.h"
#include "AssemblyU2DCSharp_ARM_utils_request_OnProgressEven1956336554.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventHandlerFunc2672185540.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_DataEventHandlerF438967822.h"
#include "AssemblyU2DCSharp_Facebook_Unity_InitDelegate5726901.h"
#include "AssemblyU2DCSharp_Facebook_Unity_HideUnityDelegate3175190102.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pro4285218014.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Proce83050093.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pro2724445839.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Comp164988305.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Dir1659888223.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_File725938474.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_Progre76602726.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipTe235186184.h"
#include "AssemblyU2DCSharp_OnEvent314892251.h"
#include "AssemblyU2DCSharp_ARM_interfaces_OnComponentIsRead1240798929.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc3330360473.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc2138319818.h"
#include "AssemblyU2DCSharp_LitJson_WrapperFactory3264289803.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3435508189.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220476.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3435478332.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220348.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220439.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220538.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220352.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615707.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615732.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615703.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220447.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220472.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220414.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RefreshGalleryWrappe3861461116.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CustomECPNManager1031774400.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5000 = { sizeof (VirtualButtonBehaviour_t2253448098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5001 = { sizeof (VuforiaBehaviour_t1845338141), -1, sizeof(VuforiaBehaviour_t1845338141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5001[1] = 
{
	VuforiaBehaviour_t1845338141_StaticFields::get_offset_of_mVuforiaBehaviour_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5002 = { sizeof (WebCamBehaviour_t2450634316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5003 = { sizeof (WireframeBehaviour_t433318935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5003[3] = 
{
	WireframeBehaviour_t433318935::get_offset_of_mLineMaterial_2(),
	WireframeBehaviour_t433318935::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t433318935::get_offset_of_LineColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5004 = { sizeof (WireframeTrackableEventHandler_t3463640687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5004[1] = 
{
	WireframeTrackableEventHandler_t3463640687::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5005 = { sizeof (WordBehaviour_t2034767101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5006 = { sizeof (iTween_t3087282050), -1, sizeof(iTween_t3087282050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5006[51] = 
{
	iTween_t3087282050_StaticFields::get_offset_of_tweens_2(),
	iTween_t3087282050_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t3087282050::get_offset_of_id_4(),
	iTween_t3087282050::get_offset_of_type_5(),
	iTween_t3087282050::get_offset_of_method_6(),
	iTween_t3087282050::get_offset_of_easeType_7(),
	iTween_t3087282050::get_offset_of_time_8(),
	iTween_t3087282050::get_offset_of_delay_9(),
	iTween_t3087282050::get_offset_of_loopType_10(),
	iTween_t3087282050::get_offset_of_isRunning_11(),
	iTween_t3087282050::get_offset_of_isPaused_12(),
	iTween_t3087282050::get_offset_of__name_13(),
	iTween_t3087282050::get_offset_of_runningTime_14(),
	iTween_t3087282050::get_offset_of_percentage_15(),
	iTween_t3087282050::get_offset_of_delayStarted_16(),
	iTween_t3087282050::get_offset_of_kinematic_17(),
	iTween_t3087282050::get_offset_of_isLocal_18(),
	iTween_t3087282050::get_offset_of_loop_19(),
	iTween_t3087282050::get_offset_of_reverse_20(),
	iTween_t3087282050::get_offset_of_wasPaused_21(),
	iTween_t3087282050::get_offset_of_physics_22(),
	iTween_t3087282050::get_offset_of_tweenArguments_23(),
	iTween_t3087282050::get_offset_of_space_24(),
	iTween_t3087282050::get_offset_of_ease_25(),
	iTween_t3087282050::get_offset_of_apply_26(),
	iTween_t3087282050::get_offset_of_audioSource_27(),
	iTween_t3087282050::get_offset_of_vector3s_28(),
	iTween_t3087282050::get_offset_of_vector2s_29(),
	iTween_t3087282050::get_offset_of_colors_30(),
	iTween_t3087282050::get_offset_of_floats_31(),
	iTween_t3087282050::get_offset_of_rects_32(),
	iTween_t3087282050::get_offset_of_path_33(),
	iTween_t3087282050::get_offset_of_preUpdate_34(),
	iTween_t3087282050::get_offset_of_postUpdate_35(),
	iTween_t3087282050::get_offset_of_namedcolorvalue_36(),
	iTween_t3087282050::get_offset_of_lastRealTime_37(),
	iTween_t3087282050::get_offset_of_useRealTime_38(),
	iTween_t3087282050::get_offset_of_thisTransform_39(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_40(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_41(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_42(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_43(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_44(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_45(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_46(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_47(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_48(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_49(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_50(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map12_51(),
	iTween_t3087282050_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5007 = { sizeof (EaseType_t2734598229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5007[34] = 
{
	EaseType_t2734598229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5008 = { sizeof (LoopType_t1485160459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5008[4] = 
{
	LoopType_t1485160459::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5009 = { sizeof (NamedValueColor_t1694108638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5009[5] = 
{
	NamedValueColor_t1694108638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5010 = { sizeof (Defaults_t4166900319), -1, sizeof(Defaults_t4166900319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5010[16] = 
{
	Defaults_t4166900319_StaticFields::get_offset_of_time_0(),
	Defaults_t4166900319_StaticFields::get_offset_of_delay_1(),
	Defaults_t4166900319_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4166900319_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4166900319_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4166900319_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4166900319_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4166900319_StaticFields::get_offset_of_space_7(),
	Defaults_t4166900319_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4166900319_StaticFields::get_offset_of_color_9(),
	Defaults_t4166900319_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4166900319_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4166900319_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4166900319_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4166900319_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4166900319_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5011 = { sizeof (CRSpline_t2211016973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5011[1] = 
{
	CRSpline_t2211016973::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5012 = { sizeof (EasingFunction_t1323017328), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5013 = { sizeof (ApplyTween_t882368618), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5014 = { sizeof (U3CTweenDelayU3Ec__Iterator87_t1287629881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5014[3] = 
{
	U3CTweenDelayU3Ec__Iterator87_t1287629881::get_offset_of_U24PC_0(),
	U3CTweenDelayU3Ec__Iterator87_t1287629881::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator87_t1287629881::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5015 = { sizeof (U3CTweenRestartU3Ec__Iterator88_t3643232966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5015[3] = 
{
	U3CTweenRestartU3Ec__Iterator88_t3643232966::get_offset_of_U24PC_0(),
	U3CTweenRestartU3Ec__Iterator88_t3643232966::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator88_t3643232966::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5016 = { sizeof (U3CStartU3Ec__Iterator89_t1062998983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5016[3] = 
{
	U3CStartU3Ec__Iterator89_t1062998983::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator89_t1062998983::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator89_t1062998983::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5017 = { sizeof (AutoSwapScroll_t2597364079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5017[6] = 
{
	AutoSwapScroll_t2597364079::get_offset_of_HorizontalScrollSnap_2(),
	AutoSwapScroll_t2597364079::get_offset_of_time_3(),
	AutoSwapScroll_t2597364079::get_offset_of_inativityTime_4(),
	AutoSwapScroll_t2597364079::get_offset_of_counter_5(),
	AutoSwapScroll_t2597364079::get_offset_of_dir_6(),
	AutoSwapScroll_t2597364079::get_offset_of_inativityTimeCtrl_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5018 = { sizeof (U3CStartU3Ec__Iterator8A_t3846047746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5018[3] = 
{
	U3CStartU3Ec__Iterator8A_t3846047746::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator8A_t3846047746::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator8A_t3846047746::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5019 = { sizeof (CurvedLayout_t2079821603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5019[4] = 
{
	CurvedLayout_t2079821603::get_offset_of_CurveOffset_10(),
	CurvedLayout_t2079821603::get_offset_of_itemAxis_11(),
	CurvedLayout_t2079821603::get_offset_of_itemSize_12(),
	CurvedLayout_t2079821603::get_offset_of_centerpoint_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5020 = { sizeof (FlowLayoutGroup_t2276891933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5020[7] = 
{
	FlowLayoutGroup_t2276891933::get_offset_of_SpacingX_10(),
	FlowLayoutGroup_t2276891933::get_offset_of_SpacingY_11(),
	FlowLayoutGroup_t2276891933::get_offset_of_ExpandHorizontalSpacing_12(),
	FlowLayoutGroup_t2276891933::get_offset_of_ChildForceExpandWidth_13(),
	FlowLayoutGroup_t2276891933::get_offset_of_ChildForceExpandHeight_14(),
	FlowLayoutGroup_t2276891933::get_offset_of__layoutHeight_15(),
	FlowLayoutGroup_t2276891933::get_offset_of__rowList_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5021 = { sizeof (HorizontalScrollSnap_t2651831999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5021[3] = 
{
	HorizontalScrollSnap_t2651831999::get_offset_of_sizeContentPlus_38(),
	HorizontalScrollSnap_t2651831999::get_offset_of_sizeCellX_39(),
	HorizontalScrollSnap_t2651831999::get_offset_of_updateCounter_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5022 = { sizeof (RadialLayout_t1907465325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5022[4] = 
{
	RadialLayout_t1907465325::get_offset_of_fDistance_10(),
	RadialLayout_t1907465325::get_offset_of_MinAngle_11(),
	RadialLayout_t1907465325::get_offset_of_MaxAngle_12(),
	RadialLayout_t1907465325::get_offset_of_StartAngle_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5023 = { sizeof (ScrollSnap_t531428411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5023[33] = 
{
	ScrollSnap_t531428411::get_offset_of_direction_2(),
	ScrollSnap_t531428411::get_offset_of_scrollRect_3(),
	ScrollSnap_t531428411::get_offset_of_scrollRectTransform_4(),
	ScrollSnap_t531428411::get_offset_of_listContainerTransform_5(),
	ScrollSnap_t531428411::get_offset_of_rectTransform_6(),
	ScrollSnap_t531428411::get_offset_of_pages_7(),
	ScrollSnap_t531428411::get_offset_of_startingPage_8(),
	ScrollSnap_t531428411::get_offset_of_pageAnchorPositions_9(),
	ScrollSnap_t531428411::get_offset_of_lerpTarget_10(),
	ScrollSnap_t531428411::get_offset_of_lerp_11(),
	ScrollSnap_t531428411::get_offset_of_listContainerMinPosition_12(),
	ScrollSnap_t531428411::get_offset_of_listContainerMaxPosition_13(),
	ScrollSnap_t531428411::get_offset_of_listContainerSize_14(),
	ScrollSnap_t531428411::get_offset_of_listContainerRectTransform_15(),
	ScrollSnap_t531428411::get_offset_of_listContainerCachedSize_16(),
	ScrollSnap_t531428411::get_offset_of_itemSize_17(),
	ScrollSnap_t531428411::get_offset_of_itemsCount_18(),
	ScrollSnap_t531428411::get_offset_of_nextButton_19(),
	ScrollSnap_t531428411::get_offset_of_prevButton_20(),
	ScrollSnap_t531428411::get_offset_of_itemsVisibleAtOnce_21(),
	ScrollSnap_t531428411::get_offset_of_autoLayoutItems_22(),
	ScrollSnap_t531428411::get_offset_of_linkScrolbarSteps_23(),
	ScrollSnap_t531428411::get_offset_of_linkScrolrectScrollSensitivity_24(),
	ScrollSnap_t531428411::get_offset_of_useFastSwipe_25(),
	ScrollSnap_t531428411::get_offset_of_fastSwipeThreshold_26(),
	ScrollSnap_t531428411::get_offset_of_startDrag_27(),
	ScrollSnap_t531428411::get_offset_of_positionOnDragStart_28(),
	ScrollSnap_t531428411::get_offset_of_pageOnDragStart_29(),
	ScrollSnap_t531428411::get_offset_of_fastSwipeTimer_30(),
	ScrollSnap_t531428411::get_offset_of_fastSwipeCounter_31(),
	ScrollSnap_t531428411::get_offset_of_fastSwipeTarget_32(),
	ScrollSnap_t531428411::get_offset_of_fastSwipe_33(),
	ScrollSnap_t531428411::get_offset_of_onPageChange_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5024 = { sizeof (ScrollDirection_t1065660817)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5024[3] = 
{
	ScrollDirection_t1065660817::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5025 = { sizeof (PageSnapChange_t3629032906), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5026 = { sizeof (ScrollSnapBase_t1686927404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5026[36] = 
{
	ScrollSnapBase_t1686927404::get_offset_of__screensContainer_2(),
	ScrollSnapBase_t1686927404::get_offset_of__isVertical_3(),
	ScrollSnapBase_t1686927404::get_offset_of__screens_4(),
	ScrollSnapBase_t1686927404::get_offset_of__scrollStartPosition_5(),
	ScrollSnapBase_t1686927404::get_offset_of__childSize_6(),
	ScrollSnapBase_t1686927404::get_offset_of__childPos_7(),
	ScrollSnapBase_t1686927404::get_offset_of__maskSize_8(),
	ScrollSnapBase_t1686927404::get_offset_of__childAnchorPoint_9(),
	ScrollSnapBase_t1686927404::get_offset_of__scroll_rect_10(),
	ScrollSnapBase_t1686927404::get_offset_of__lerp_target_11(),
	ScrollSnapBase_t1686927404::get_offset_of__lerp_12(),
	ScrollSnapBase_t1686927404::get_offset_of__pointerDown_13(),
	ScrollSnapBase_t1686927404::get_offset_of__settled_14(),
	ScrollSnapBase_t1686927404::get_offset_of__startPosition_15(),
	ScrollSnapBase_t1686927404::get_offset_of__currentPage_16(),
	ScrollSnapBase_t1686927404::get_offset_of__previousPage_17(),
	ScrollSnapBase_t1686927404::get_offset_of__halfNoVisibleItems_18(),
	ScrollSnapBase_t1686927404::get_offset_of__bottomItem_19(),
	ScrollSnapBase_t1686927404::get_offset_of__topItem_20(),
	ScrollSnapBase_t1686927404::get_offset_of_StartingScreen_21(),
	ScrollSnapBase_t1686927404::get_offset_of_enableEnded_22(),
	ScrollSnapBase_t1686927404::get_offset_of_PageStep_23(),
	ScrollSnapBase_t1686927404::get_offset_of_Pagination_24(),
	ScrollSnapBase_t1686927404::get_offset_of_NextButton_25(),
	ScrollSnapBase_t1686927404::get_offset_of_PrevButton_26(),
	ScrollSnapBase_t1686927404::get_offset_of_transitionSpeed_27(),
	ScrollSnapBase_t1686927404::get_offset_of_UseFastSwipe_28(),
	ScrollSnapBase_t1686927404::get_offset_of_FastSwipeThreshold_29(),
	ScrollSnapBase_t1686927404::get_offset_of_SwipeVelocityThreshold_30(),
	ScrollSnapBase_t1686927404::get_offset_of_MaskArea_31(),
	ScrollSnapBase_t1686927404::get_offset_of_MaskBuffer_32(),
	ScrollSnapBase_t1686927404::get_offset_of_UseParentTransform_33(),
	ScrollSnapBase_t1686927404::get_offset_of_ChildObjects_34(),
	ScrollSnapBase_t1686927404::get_offset_of_m_OnSelectionChangeStartEvent_35(),
	ScrollSnapBase_t1686927404::get_offset_of_m_OnSelectionPageChangedEvent_36(),
	ScrollSnapBase_t1686927404::get_offset_of_m_OnSelectionChangeEndEvent_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5027 = { sizeof (SelectionChangeStartEvent_t2472858916), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5028 = { sizeof (SelectionPageChangedEvent_t2882117233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5029 = { sizeof (SelectionChangeEndEvent_t16447307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5030 = { sizeof (TableLayoutGroup_t3201339275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5030[7] = 
{
	TableLayoutGroup_t3201339275::get_offset_of_startCorner_10(),
	TableLayoutGroup_t3201339275::get_offset_of_columnWidths_11(),
	TableLayoutGroup_t3201339275::get_offset_of_minimumRowHeight_12(),
	TableLayoutGroup_t3201339275::get_offset_of_flexibleRowHeight_13(),
	TableLayoutGroup_t3201339275::get_offset_of_columnSpacing_14(),
	TableLayoutGroup_t3201339275::get_offset_of_rowSpacing_15(),
	TableLayoutGroup_t3201339275::get_offset_of_preferredRowHeights_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5031 = { sizeof (Corner_t4192955334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5031[5] = 
{
	Corner_t4192955334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5032 = { sizeof (TileSizeFitter_t255948707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5032[4] = 
{
	TileSizeFitter_t255948707::get_offset_of_m_Border_2(),
	TileSizeFitter_t255948707::get_offset_of_m_TileSize_3(),
	TileSizeFitter_t255948707::get_offset_of_m_Rect_4(),
	TileSizeFitter_t255948707::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5033 = { sizeof (UIVerticalScroller_t2397627752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5033[13] = 
{
	UIVerticalScroller_t2397627752::get_offset_of__scrollingPanel_2(),
	UIVerticalScroller_t2397627752::get_offset_of__arrayOfElements_3(),
	UIVerticalScroller_t2397627752::get_offset_of__center_4(),
	UIVerticalScroller_t2397627752::get_offset_of_StartingIndex_5(),
	UIVerticalScroller_t2397627752::get_offset_of_ScrollUpButton_6(),
	UIVerticalScroller_t2397627752::get_offset_of_ScrollDownButton_7(),
	UIVerticalScroller_t2397627752::get_offset_of_ButtonClicked_8(),
	UIVerticalScroller_t2397627752::get_offset_of_distReposition_9(),
	UIVerticalScroller_t2397627752::get_offset_of_distance_10(),
	UIVerticalScroller_t2397627752::get_offset_of_minElementsNum_11(),
	UIVerticalScroller_t2397627752::get_offset_of_elementLength_12(),
	UIVerticalScroller_t2397627752::get_offset_of_deltaY_13(),
	UIVerticalScroller_t2397627752::get_offset_of_result_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5034 = { sizeof (U3CAddListenerU3Ec__AnonStoreyA8_t3853148749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5034[2] = 
{
	U3CAddListenerU3Ec__AnonStoreyA8_t3853148749::get_offset_of_index_0(),
	U3CAddListenerU3Ec__AnonStoreyA8_t3853148749::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5035 = { sizeof (VerticalScrollSnap_t3205272913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5036 = { sizeof (OnInAppCommonEventHandler_t1653412278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5037 = { sizeof (OnInAppProgressEventHandler_t3139301368), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5038 = { sizeof (OnRequestEventHandler_t153308946), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5039 = { sizeof (OnProgressEventHandler_t1956336554), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5040 = { sizeof (EventHandlerFunction_t2672185540), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5041 = { sizeof (DataEventHandlerFunction_t438967822), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5042 = { sizeof (InitDelegate_t5726901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5043 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5044 = { sizeof (HideUnityDelegate_t3175190102), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5045 = { sizeof (ProcessDirectoryHandler_t4285218014), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5046 = { sizeof (ProcessFileHandler_t83050093), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5047 = { sizeof (ProgressHandler_t2724445839), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5048 = { sizeof (CompletedFileHandler_t164988305), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5049 = { sizeof (DirectoryFailureHandler_t1659888223), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5050 = { sizeof (FileFailureHandler_t725938474), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5051 = { sizeof (ProgressMessageHandler_t76602726), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5052 = { sizeof (ZipTestResultHandler_t235186184), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5053 = { sizeof (OnEvent_t314892251), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5054 = { sizeof (OnComponentIsReadyEventHandler_t1240798929), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5055 = { sizeof (ExporterFunc_t3330360473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5056 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5057 = { sizeof (ImporterFunc_t2138319818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5058 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5059 = { sizeof (WrapperFactory_t3264289803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5060 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238939), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5060[22] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D12_12(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D13_13(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D14_14(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D15_15(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D16_16(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D17_17(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D18_18(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D19_19(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D20_20(),
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D21_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5061 = { sizeof (U24ArrayTypeU242048_t435508190)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242048_t435508190_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5062 = { sizeof (U24ArrayTypeU2456_t3379220477)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2456_t3379220477_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5063 = { sizeof (U24ArrayTypeU241024_t435478334)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t435478334_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5064 = { sizeof (U24ArrayTypeU2412_t3379220356)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220356_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5065 = { sizeof (U24ArrayTypeU2440_t3379220439)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2440_t3379220439_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5066 = { sizeof (U24ArrayTypeU2476_t3379220538)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2476_t3379220538_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5067 = { sizeof (U24ArrayTypeU2416_t3379220357)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220357_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5068 = { sizeof (U24ArrayTypeU24116_t1676615707)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24116_t1676615707_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5069 = { sizeof (U24ArrayTypeU24120_t1676615734)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1676615734_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5070 = { sizeof (U24ArrayTypeU24112_t1676615703)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24112_t1676615703_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5071 = { sizeof (U24ArrayTypeU2448_t3379220449)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t3379220449_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5072 = { sizeof (U24ArrayTypeU2452_t3379220473)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t3379220473_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5073 = { sizeof (U24ArrayTypeU2436_t3379220414)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2436_t3379220414_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5074 = { sizeof (U3CModuleU3E_t86524807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5075 = { sizeof (RefreshGalleryWrapper_t3861461116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5076 = { sizeof (CustomECPNManager_t1031774400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5076[4] = 
{
	CustomECPNManager_t1031774400::get_offset_of_GoogleCloudMessageProjectID_2(),
	CustomECPNManager_t1031774400::get_offset_of_packageName_3(),
	CustomECPNManager_t1031774400::get_offset_of_devToken_4(),
	CustomECPNManager_t1031774400::get_offset_of_pollIOSDeviceToken_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
