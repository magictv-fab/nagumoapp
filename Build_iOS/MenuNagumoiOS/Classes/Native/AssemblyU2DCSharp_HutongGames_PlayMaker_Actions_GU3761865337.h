﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginArea
struct  GUILayoutBeginArea_t3761865337  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GUILayoutBeginArea::screenRect
	FsmRect_t1076426478 * ___screenRect_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::left
	FsmFloat_t2134102846 * ___left_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::top
	FsmFloat_t2134102846 * ___top_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::width
	FsmFloat_t2134102846 * ___width_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::height
	FsmFloat_t2134102846 * ___height_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginArea::normalized
	FsmBool_t1075959796 * ___normalized_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginArea::style
	FsmString_t952858651 * ___style_15;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.GUILayoutBeginArea::rect
	Rect_t4241904616  ___rect_16;

public:
	inline static int32_t get_offset_of_screenRect_9() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___screenRect_9)); }
	inline FsmRect_t1076426478 * get_screenRect_9() const { return ___screenRect_9; }
	inline FsmRect_t1076426478 ** get_address_of_screenRect_9() { return &___screenRect_9; }
	inline void set_screenRect_9(FsmRect_t1076426478 * value)
	{
		___screenRect_9 = value;
		Il2CppCodeGenWriteBarrier(&___screenRect_9, value);
	}

	inline static int32_t get_offset_of_left_10() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___left_10)); }
	inline FsmFloat_t2134102846 * get_left_10() const { return ___left_10; }
	inline FsmFloat_t2134102846 ** get_address_of_left_10() { return &___left_10; }
	inline void set_left_10(FsmFloat_t2134102846 * value)
	{
		___left_10 = value;
		Il2CppCodeGenWriteBarrier(&___left_10, value);
	}

	inline static int32_t get_offset_of_top_11() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___top_11)); }
	inline FsmFloat_t2134102846 * get_top_11() const { return ___top_11; }
	inline FsmFloat_t2134102846 ** get_address_of_top_11() { return &___top_11; }
	inline void set_top_11(FsmFloat_t2134102846 * value)
	{
		___top_11 = value;
		Il2CppCodeGenWriteBarrier(&___top_11, value);
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___width_12)); }
	inline FsmFloat_t2134102846 * get_width_12() const { return ___width_12; }
	inline FsmFloat_t2134102846 ** get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(FsmFloat_t2134102846 * value)
	{
		___width_12 = value;
		Il2CppCodeGenWriteBarrier(&___width_12, value);
	}

	inline static int32_t get_offset_of_height_13() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___height_13)); }
	inline FsmFloat_t2134102846 * get_height_13() const { return ___height_13; }
	inline FsmFloat_t2134102846 ** get_address_of_height_13() { return &___height_13; }
	inline void set_height_13(FsmFloat_t2134102846 * value)
	{
		___height_13 = value;
		Il2CppCodeGenWriteBarrier(&___height_13, value);
	}

	inline static int32_t get_offset_of_normalized_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___normalized_14)); }
	inline FsmBool_t1075959796 * get_normalized_14() const { return ___normalized_14; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_14() { return &___normalized_14; }
	inline void set_normalized_14(FsmBool_t1075959796 * value)
	{
		___normalized_14 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___style_15)); }
	inline FsmString_t952858651 * get_style_15() const { return ___style_15; }
	inline FsmString_t952858651 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(FsmString_t952858651 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_rect_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t3761865337, ___rect_16)); }
	inline Rect_t4241904616  get_rect_16() const { return ___rect_16; }
	inline Rect_t4241904616 * get_address_of_rect_16() { return &___rect_16; }
	inline void set_rect_16(Rect_t4241904616  value)
	{
		___rect_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
