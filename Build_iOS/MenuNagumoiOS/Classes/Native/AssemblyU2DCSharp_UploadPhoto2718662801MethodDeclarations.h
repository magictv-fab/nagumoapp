﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UploadPhoto
struct UploadPhoto_t2718662801;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UploadPhoto::.ctor()
extern "C"  void UploadPhoto__ctor_m3819507194 (UploadPhoto_t2718662801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UploadPhoto::IUploadPhotoToServer(System.String,System.String)
extern "C"  Il2CppObject * UploadPhoto_IUploadPhotoToServer_m2741873462 (UploadPhoto_t2718662801 * __this, String_t* ___localFileName0, String_t* ___uploadURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto::UploadFile(System.String,System.String)
extern "C"  void UploadPhoto_UploadFile_m677171767 (UploadPhoto_t2718662801 * __this, String_t* ___localFileName0, String_t* ___uploadURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto::UploadPhotoToServer()
extern "C"  void UploadPhoto_UploadPhotoToServer_m562101383 (UploadPhoto_t2718662801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto::BtnUploadPhotoAuto()
extern "C"  void UploadPhoto_BtnUploadPhotoAuto_m2790269774 (UploadPhoto_t2718662801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto::BtnUploadLocalPhoto()
extern "C"  void UploadPhoto_BtnUploadLocalPhoto_m1846597308 (UploadPhoto_t2718662801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UploadPhoto::loadImageByWWW(System.String)
extern "C"  Il2CppObject * UploadPhoto_loadImageByWWW_m1508068565 (UploadPhoto_t2718662801 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto::<BtnUploadPhotoAuto>m__87(System.String)
extern "C"  void UploadPhoto_U3CBtnUploadPhotoAutoU3Em__87_m429755868 (UploadPhoto_t2718662801 * __this, String_t* ___filepath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto::<BtnUploadLocalPhoto>m__88(System.String)
extern "C"  void UploadPhoto_U3CBtnUploadLocalPhotoU3Em__88_m1707359969 (UploadPhoto_t2718662801 * __this, String_t* ___filepath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
