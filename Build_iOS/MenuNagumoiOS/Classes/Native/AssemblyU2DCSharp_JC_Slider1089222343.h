﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JC_Slider
struct  JC_Slider_t1089222343  : public Slider_t79469677
{
public:
	// System.Single JC_Slider::StepsIncrement
	float ___StepsIncrement_31;

public:
	inline static int32_t get_offset_of_StepsIncrement_31() { return static_cast<int32_t>(offsetof(JC_Slider_t1089222343, ___StepsIncrement_31)); }
	inline float get_StepsIncrement_31() const { return ___StepsIncrement_31; }
	inline float* get_address_of_StepsIncrement_31() { return &___StepsIncrement_31; }
	inline void set_StepsIncrement_31(float value)
	{
		___StepsIncrement_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
