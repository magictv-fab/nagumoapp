﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas595986710.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseRect
struct  EaseRect_t3254272922  : public EaseFsmAction_t595986710
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.EaseRect::fromValue
	FsmRect_t1076426478 * ___fromValue_30;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.EaseRect::toValue
	FsmRect_t1076426478 * ___toValue_31;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.EaseRect::rectVariable
	FsmRect_t1076426478 * ___rectVariable_32;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseRect::finishInNextStep
	bool ___finishInNextStep_33;

public:
	inline static int32_t get_offset_of_fromValue_30() { return static_cast<int32_t>(offsetof(EaseRect_t3254272922, ___fromValue_30)); }
	inline FsmRect_t1076426478 * get_fromValue_30() const { return ___fromValue_30; }
	inline FsmRect_t1076426478 ** get_address_of_fromValue_30() { return &___fromValue_30; }
	inline void set_fromValue_30(FsmRect_t1076426478 * value)
	{
		___fromValue_30 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_30, value);
	}

	inline static int32_t get_offset_of_toValue_31() { return static_cast<int32_t>(offsetof(EaseRect_t3254272922, ___toValue_31)); }
	inline FsmRect_t1076426478 * get_toValue_31() const { return ___toValue_31; }
	inline FsmRect_t1076426478 ** get_address_of_toValue_31() { return &___toValue_31; }
	inline void set_toValue_31(FsmRect_t1076426478 * value)
	{
		___toValue_31 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_31, value);
	}

	inline static int32_t get_offset_of_rectVariable_32() { return static_cast<int32_t>(offsetof(EaseRect_t3254272922, ___rectVariable_32)); }
	inline FsmRect_t1076426478 * get_rectVariable_32() const { return ___rectVariable_32; }
	inline FsmRect_t1076426478 ** get_address_of_rectVariable_32() { return &___rectVariable_32; }
	inline void set_rectVariable_32(FsmRect_t1076426478 * value)
	{
		___rectVariable_32 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariable_32, value);
	}

	inline static int32_t get_offset_of_finishInNextStep_33() { return static_cast<int32_t>(offsetof(EaseRect_t3254272922, ___finishInNextStep_33)); }
	inline bool get_finishInNextStep_33() const { return ___finishInNextStep_33; }
	inline bool* get_address_of_finishInNextStep_33() { return &___finishInNextStep_33; }
	inline void set_finishInNextStep_33(bool value)
	{
		___finishInNextStep_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
