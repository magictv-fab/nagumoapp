﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler
struct OnChangeToPerspectiveEventHandler_t1587175024;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnChangeToPerspectiveEventHandler__ctor_m3785813575 (OnChangeToPerspectiveEventHandler_t1587175024 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler::Invoke(MagicTV.globals.Perspective)
extern "C"  void OnChangeToPerspectiveEventHandler_Invoke_m558600466 (OnChangeToPerspectiveEventHandler_t1587175024 * __this, int32_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler::BeginInvoke(MagicTV.globals.Perspective,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnChangeToPerspectiveEventHandler_BeginInvoke_m770763293 (OnChangeToPerspectiveEventHandler_t1587175024 * __this, int32_t ___p0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnChangeToPerspectiveEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnChangeToPerspectiveEventHandler_EndInvoke_m1287822295 (OnChangeToPerspectiveEventHandler_t1587175024 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
