﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetHostCount
struct MasterServerGetHostCount_t240962846;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostCount::.ctor()
extern "C"  void MasterServerGetHostCount__ctor_m2794741512 (MasterServerGetHostCount_t240962846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostCount::OnEnter()
extern "C"  void MasterServerGetHostCount_OnEnter_m783761631 (MasterServerGetHostCount_t240962846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
