﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetCollisionInfo
struct  GetCollisionInfo_t3783581426  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetCollisionInfo::gameObjectHit
	FsmGameObject_t1697147867 * ___gameObjectHit_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollisionInfo::relativeVelocity
	FsmVector3_t533912882 * ___relativeVelocity_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCollisionInfo::relativeSpeed
	FsmFloat_t2134102846 * ___relativeSpeed_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollisionInfo::contactPoint
	FsmVector3_t533912882 * ___contactPoint_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollisionInfo::contactNormal
	FsmVector3_t533912882 * ___contactNormal_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetCollisionInfo::physicsMaterialName
	FsmString_t952858651 * ___physicsMaterialName_14;

public:
	inline static int32_t get_offset_of_gameObjectHit_9() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3783581426, ___gameObjectHit_9)); }
	inline FsmGameObject_t1697147867 * get_gameObjectHit_9() const { return ___gameObjectHit_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObjectHit_9() { return &___gameObjectHit_9; }
	inline void set_gameObjectHit_9(FsmGameObject_t1697147867 * value)
	{
		___gameObjectHit_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectHit_9, value);
	}

	inline static int32_t get_offset_of_relativeVelocity_10() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3783581426, ___relativeVelocity_10)); }
	inline FsmVector3_t533912882 * get_relativeVelocity_10() const { return ___relativeVelocity_10; }
	inline FsmVector3_t533912882 ** get_address_of_relativeVelocity_10() { return &___relativeVelocity_10; }
	inline void set_relativeVelocity_10(FsmVector3_t533912882 * value)
	{
		___relativeVelocity_10 = value;
		Il2CppCodeGenWriteBarrier(&___relativeVelocity_10, value);
	}

	inline static int32_t get_offset_of_relativeSpeed_11() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3783581426, ___relativeSpeed_11)); }
	inline FsmFloat_t2134102846 * get_relativeSpeed_11() const { return ___relativeSpeed_11; }
	inline FsmFloat_t2134102846 ** get_address_of_relativeSpeed_11() { return &___relativeSpeed_11; }
	inline void set_relativeSpeed_11(FsmFloat_t2134102846 * value)
	{
		___relativeSpeed_11 = value;
		Il2CppCodeGenWriteBarrier(&___relativeSpeed_11, value);
	}

	inline static int32_t get_offset_of_contactPoint_12() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3783581426, ___contactPoint_12)); }
	inline FsmVector3_t533912882 * get_contactPoint_12() const { return ___contactPoint_12; }
	inline FsmVector3_t533912882 ** get_address_of_contactPoint_12() { return &___contactPoint_12; }
	inline void set_contactPoint_12(FsmVector3_t533912882 * value)
	{
		___contactPoint_12 = value;
		Il2CppCodeGenWriteBarrier(&___contactPoint_12, value);
	}

	inline static int32_t get_offset_of_contactNormal_13() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3783581426, ___contactNormal_13)); }
	inline FsmVector3_t533912882 * get_contactNormal_13() const { return ___contactNormal_13; }
	inline FsmVector3_t533912882 ** get_address_of_contactNormal_13() { return &___contactNormal_13; }
	inline void set_contactNormal_13(FsmVector3_t533912882 * value)
	{
		___contactNormal_13 = value;
		Il2CppCodeGenWriteBarrier(&___contactNormal_13, value);
	}

	inline static int32_t get_offset_of_physicsMaterialName_14() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3783581426, ___physicsMaterialName_14)); }
	inline FsmString_t952858651 * get_physicsMaterialName_14() const { return ___physicsMaterialName_14; }
	inline FsmString_t952858651 ** get_address_of_physicsMaterialName_14() { return &___physicsMaterialName_14; }
	inline void set_physicsMaterialName_14(FsmString_t952858651 * value)
	{
		___physicsMaterialName_14 = value;
		Il2CppCodeGenWriteBarrier(&___physicsMaterialName_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
