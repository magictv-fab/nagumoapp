﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManagerGUI
struct ScreenshotManagerGUI_t3459897460;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ScreenshotManagerGUI::.ctor()
extern "C"  void ScreenshotManagerGUI__ctor_m3788031143 (ScreenshotManagerGUI_t3459897460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI::add_ScreenshotFinishedSaving(System.Action`1<System.String>)
extern "C"  void ScreenshotManagerGUI_add_ScreenshotFinishedSaving_m1218418057 (ScreenshotManagerGUI_t3459897460 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI::remove_ScreenshotFinishedSaving(System.Action`1<System.String>)
extern "C"  void ScreenshotManagerGUI_remove_ScreenshotFinishedSaving_m3843640910 (ScreenshotManagerGUI_t3459897460 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI::add_ImageFinishedSaving(System.Action`1<System.String>)
extern "C"  void ScreenshotManagerGUI_add_ImageFinishedSaving_m3508841802 (ScreenshotManagerGUI_t3459897460 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI::remove_ImageFinishedSaving(System.Action`1<System.String>)
extern "C"  void ScreenshotManagerGUI_remove_ImageFinishedSaving_m4126264677 (ScreenshotManagerGUI_t3459897460 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenshotManagerGUI::saveToGallery(System.String)
extern "C"  bool ScreenshotManagerGUI_saveToGallery_m1702239319 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenshotManagerGUI::Save(System.String,System.String,System.Boolean)
extern "C"  Il2CppObject * ScreenshotManagerGUI_Save_m2480808385 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, String_t* ___albumName1, bool ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenshotManagerGUI::SaveExisting(System.Byte[],System.String,System.Boolean)
extern "C"  Il2CppObject * ScreenshotManagerGUI_SaveExisting_m4010450565 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___fileName1, bool ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenshotManagerGUI::Wait(System.Single)
extern "C"  Il2CppObject * ScreenshotManagerGUI_Wait_m47639985 (Il2CppObject * __this /* static, unused */, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManagerGUI::set_ScreenShotNumber(System.Int32)
extern "C"  void ScreenshotManagerGUI_set_ScreenShotNumber_m1748627258 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScreenshotManagerGUI::get_ScreenShotNumber()
extern "C"  int32_t ScreenshotManagerGUI_get_ScreenShotNumber_m2750092547 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
