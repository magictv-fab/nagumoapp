﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Pair
struct Pair_t1045409632;
// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_OneD_RSS_FinderPattern3366792524.h"

// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Pair::get_FinderPattern()
extern "C"  FinderPattern_t3366792524 * Pair_get_FinderPattern_m3843413091 (Pair_t1045409632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Pair::set_FinderPattern(ZXing.OneD.RSS.FinderPattern)
extern "C"  void Pair_set_FinderPattern_m1239554418 (Pair_t1045409632 * __this, FinderPattern_t3366792524 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Pair::get_Count()
extern "C"  int32_t Pair_get_Count_m857879617 (Pair_t1045409632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Pair::set_Count(System.Int32)
extern "C"  void Pair_set_Count_m504462544 (Pair_t1045409632 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Pair::.ctor(System.Int32,System.Int32,ZXing.OneD.RSS.FinderPattern)
extern "C"  void Pair__ctor_m2207482375 (Pair_t1045409632 * __this, int32_t ___value0, int32_t ___checksumPortion1, FinderPattern_t3366792524 * ___finderPattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Pair::incrementCount()
extern "C"  void Pair_incrementCount_m4073370805 (Pair_t1045409632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
