﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight
struct  GetAnimatorLayerWeight_t3333517428  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::layerWeight
	FsmFloat_t2134102846 * ___layerWeight_12;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::_animator
	Animator_t2776330603 * ____animator_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t3333517428, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t3333517428, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t3333517428, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_layerWeight_12() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t3333517428, ___layerWeight_12)); }
	inline FsmFloat_t2134102846 * get_layerWeight_12() const { return ___layerWeight_12; }
	inline FsmFloat_t2134102846 ** get_address_of_layerWeight_12() { return &___layerWeight_12; }
	inline void set_layerWeight_12(FsmFloat_t2134102846 * value)
	{
		___layerWeight_12 = value;
		Il2CppCodeGenWriteBarrier(&___layerWeight_12, value);
	}

	inline static int32_t get_offset_of__animatorProxy_13() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t3333517428, ____animatorProxy_13)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_13() const { return ____animatorProxy_13; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_13() { return &____animatorProxy_13; }
	inline void set__animatorProxy_13(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_13 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t3333517428, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
