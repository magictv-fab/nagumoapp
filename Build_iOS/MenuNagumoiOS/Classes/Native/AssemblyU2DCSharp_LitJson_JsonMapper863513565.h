﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>
struct IDictionary_2_t3013656303;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t1504911478;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>
struct IDictionary_2_t3741638740;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct IDictionary_2_t3980295021;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>
struct IDictionary_2_t1692590328;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>
struct IDictionary_2_t2149610353;
// LitJson.JsonWriter
struct JsonWriter_t1165300239;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_t3963570234;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t785513668;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t2244419209;
// LitJson.ExporterFunc
struct ExporterFunc_t3330360473;
// LitJson.ImporterFunc
struct ImporterFunc_t2138319818;
// LitJson.WrapperFactory
struct WrapperFactory_t3264289803;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper
struct  JsonMapper_t863513565  : public Il2CppObject
{
public:

public:
};

struct JsonMapper_t863513565_StaticFields
{
public:
	// System.Int32 LitJson.JsonMapper::max_nesting_depth
	int32_t ___max_nesting_depth_1;
	// System.IFormatProvider LitJson.JsonMapper::datetime_format
	Il2CppObject * ___datetime_format_2;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::base_exporters_table
	Il2CppObject* ___base_exporters_table_3;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::custom_exporters_table
	Il2CppObject* ___custom_exporters_table_4;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::base_importers_table
	Il2CppObject* ___base_importers_table_5;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::custom_importers_table
	Il2CppObject* ___custom_importers_table_6;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata> LitJson.JsonMapper::array_metadata
	Il2CppObject* ___array_metadata_7;
	// System.Object LitJson.JsonMapper::array_metadata_lock
	Il2CppObject * ___array_metadata_lock_8;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>> LitJson.JsonMapper::conv_ops
	Il2CppObject* ___conv_ops_9;
	// System.Object LitJson.JsonMapper::conv_ops_lock
	Il2CppObject * ___conv_ops_lock_10;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata> LitJson.JsonMapper::object_metadata
	Il2CppObject* ___object_metadata_11;
	// System.Object LitJson.JsonMapper::object_metadata_lock
	Il2CppObject * ___object_metadata_lock_12;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>> LitJson.JsonMapper::type_properties
	Il2CppObject* ___type_properties_13;
	// System.Object LitJson.JsonMapper::type_properties_lock
	Il2CppObject * ___type_properties_lock_14;
	// LitJson.JsonWriter LitJson.JsonMapper::static_writer
	JsonWriter_t1165300239 * ___static_writer_15;
	// System.Object LitJson.JsonMapper::static_writer_lock
	Il2CppObject * ___static_writer_lock_16;
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> LitJson.JsonMapper::<>f__am$cache10
	Func_2_t3963570234 * ___U3CU3Ef__amU24cache10_17;
	// System.Func`2<System.Object,System.Boolean> LitJson.JsonMapper::<>f__am$cache11
	Func_2_t785513668 * ___U3CU3Ef__amU24cache11_18;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> LitJson.JsonMapper::<>f__am$cache12
	Func_2_t2244419209 * ___U3CU3Ef__amU24cache12_19;
	// System.Func`2<System.Object,System.Boolean> LitJson.JsonMapper::<>f__am$cache13
	Func_2_t785513668 * ___U3CU3Ef__amU24cache13_20;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache14
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache14_21;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache15
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache15_22;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache16
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache16_23;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache17
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache17_24;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache18
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache18_25;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache19
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache19_26;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache1A
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache1A_27;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache1B
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache1B_28;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache1C
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache1C_29;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache1D
	ExporterFunc_t3330360473 * ___U3CU3Ef__amU24cache1D_30;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1E
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache1E_31;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1F
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache1F_32;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache20
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache20_33;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache21
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache21_34;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache22
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache22_35;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache23
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache23_36;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache24
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache24_37;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache25
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache25_38;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache26
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache26_39;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache27
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache27_40;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache28
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache28_41;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache29
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache29_42;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache2A
	ImporterFunc_t2138319818 * ___U3CU3Ef__amU24cache2A_43;
	// System.Func`2<System.Object,System.Boolean> LitJson.JsonMapper::<>f__am$cache2B
	Func_2_t785513668 * ___U3CU3Ef__amU24cache2B_44;
	// System.Func`2<System.Object,System.Boolean> LitJson.JsonMapper::<>f__am$cache2C
	Func_2_t785513668 * ___U3CU3Ef__amU24cache2C_45;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache2D
	WrapperFactory_t3264289803 * ___U3CU3Ef__amU24cache2D_46;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache2E
	WrapperFactory_t3264289803 * ___U3CU3Ef__amU24cache2E_47;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache2F
	WrapperFactory_t3264289803 * ___U3CU3Ef__amU24cache2F_48;

public:
	inline static int32_t get_offset_of_max_nesting_depth_1() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___max_nesting_depth_1)); }
	inline int32_t get_max_nesting_depth_1() const { return ___max_nesting_depth_1; }
	inline int32_t* get_address_of_max_nesting_depth_1() { return &___max_nesting_depth_1; }
	inline void set_max_nesting_depth_1(int32_t value)
	{
		___max_nesting_depth_1 = value;
	}

	inline static int32_t get_offset_of_datetime_format_2() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___datetime_format_2)); }
	inline Il2CppObject * get_datetime_format_2() const { return ___datetime_format_2; }
	inline Il2CppObject ** get_address_of_datetime_format_2() { return &___datetime_format_2; }
	inline void set_datetime_format_2(Il2CppObject * value)
	{
		___datetime_format_2 = value;
		Il2CppCodeGenWriteBarrier(&___datetime_format_2, value);
	}

	inline static int32_t get_offset_of_base_exporters_table_3() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___base_exporters_table_3)); }
	inline Il2CppObject* get_base_exporters_table_3() const { return ___base_exporters_table_3; }
	inline Il2CppObject** get_address_of_base_exporters_table_3() { return &___base_exporters_table_3; }
	inline void set_base_exporters_table_3(Il2CppObject* value)
	{
		___base_exporters_table_3 = value;
		Il2CppCodeGenWriteBarrier(&___base_exporters_table_3, value);
	}

	inline static int32_t get_offset_of_custom_exporters_table_4() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___custom_exporters_table_4)); }
	inline Il2CppObject* get_custom_exporters_table_4() const { return ___custom_exporters_table_4; }
	inline Il2CppObject** get_address_of_custom_exporters_table_4() { return &___custom_exporters_table_4; }
	inline void set_custom_exporters_table_4(Il2CppObject* value)
	{
		___custom_exporters_table_4 = value;
		Il2CppCodeGenWriteBarrier(&___custom_exporters_table_4, value);
	}

	inline static int32_t get_offset_of_base_importers_table_5() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___base_importers_table_5)); }
	inline Il2CppObject* get_base_importers_table_5() const { return ___base_importers_table_5; }
	inline Il2CppObject** get_address_of_base_importers_table_5() { return &___base_importers_table_5; }
	inline void set_base_importers_table_5(Il2CppObject* value)
	{
		___base_importers_table_5 = value;
		Il2CppCodeGenWriteBarrier(&___base_importers_table_5, value);
	}

	inline static int32_t get_offset_of_custom_importers_table_6() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___custom_importers_table_6)); }
	inline Il2CppObject* get_custom_importers_table_6() const { return ___custom_importers_table_6; }
	inline Il2CppObject** get_address_of_custom_importers_table_6() { return &___custom_importers_table_6; }
	inline void set_custom_importers_table_6(Il2CppObject* value)
	{
		___custom_importers_table_6 = value;
		Il2CppCodeGenWriteBarrier(&___custom_importers_table_6, value);
	}

	inline static int32_t get_offset_of_array_metadata_7() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___array_metadata_7)); }
	inline Il2CppObject* get_array_metadata_7() const { return ___array_metadata_7; }
	inline Il2CppObject** get_address_of_array_metadata_7() { return &___array_metadata_7; }
	inline void set_array_metadata_7(Il2CppObject* value)
	{
		___array_metadata_7 = value;
		Il2CppCodeGenWriteBarrier(&___array_metadata_7, value);
	}

	inline static int32_t get_offset_of_array_metadata_lock_8() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___array_metadata_lock_8)); }
	inline Il2CppObject * get_array_metadata_lock_8() const { return ___array_metadata_lock_8; }
	inline Il2CppObject ** get_address_of_array_metadata_lock_8() { return &___array_metadata_lock_8; }
	inline void set_array_metadata_lock_8(Il2CppObject * value)
	{
		___array_metadata_lock_8 = value;
		Il2CppCodeGenWriteBarrier(&___array_metadata_lock_8, value);
	}

	inline static int32_t get_offset_of_conv_ops_9() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___conv_ops_9)); }
	inline Il2CppObject* get_conv_ops_9() const { return ___conv_ops_9; }
	inline Il2CppObject** get_address_of_conv_ops_9() { return &___conv_ops_9; }
	inline void set_conv_ops_9(Il2CppObject* value)
	{
		___conv_ops_9 = value;
		Il2CppCodeGenWriteBarrier(&___conv_ops_9, value);
	}

	inline static int32_t get_offset_of_conv_ops_lock_10() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___conv_ops_lock_10)); }
	inline Il2CppObject * get_conv_ops_lock_10() const { return ___conv_ops_lock_10; }
	inline Il2CppObject ** get_address_of_conv_ops_lock_10() { return &___conv_ops_lock_10; }
	inline void set_conv_ops_lock_10(Il2CppObject * value)
	{
		___conv_ops_lock_10 = value;
		Il2CppCodeGenWriteBarrier(&___conv_ops_lock_10, value);
	}

	inline static int32_t get_offset_of_object_metadata_11() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___object_metadata_11)); }
	inline Il2CppObject* get_object_metadata_11() const { return ___object_metadata_11; }
	inline Il2CppObject** get_address_of_object_metadata_11() { return &___object_metadata_11; }
	inline void set_object_metadata_11(Il2CppObject* value)
	{
		___object_metadata_11 = value;
		Il2CppCodeGenWriteBarrier(&___object_metadata_11, value);
	}

	inline static int32_t get_offset_of_object_metadata_lock_12() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___object_metadata_lock_12)); }
	inline Il2CppObject * get_object_metadata_lock_12() const { return ___object_metadata_lock_12; }
	inline Il2CppObject ** get_address_of_object_metadata_lock_12() { return &___object_metadata_lock_12; }
	inline void set_object_metadata_lock_12(Il2CppObject * value)
	{
		___object_metadata_lock_12 = value;
		Il2CppCodeGenWriteBarrier(&___object_metadata_lock_12, value);
	}

	inline static int32_t get_offset_of_type_properties_13() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___type_properties_13)); }
	inline Il2CppObject* get_type_properties_13() const { return ___type_properties_13; }
	inline Il2CppObject** get_address_of_type_properties_13() { return &___type_properties_13; }
	inline void set_type_properties_13(Il2CppObject* value)
	{
		___type_properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___type_properties_13, value);
	}

	inline static int32_t get_offset_of_type_properties_lock_14() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___type_properties_lock_14)); }
	inline Il2CppObject * get_type_properties_lock_14() const { return ___type_properties_lock_14; }
	inline Il2CppObject ** get_address_of_type_properties_lock_14() { return &___type_properties_lock_14; }
	inline void set_type_properties_lock_14(Il2CppObject * value)
	{
		___type_properties_lock_14 = value;
		Il2CppCodeGenWriteBarrier(&___type_properties_lock_14, value);
	}

	inline static int32_t get_offset_of_static_writer_15() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___static_writer_15)); }
	inline JsonWriter_t1165300239 * get_static_writer_15() const { return ___static_writer_15; }
	inline JsonWriter_t1165300239 ** get_address_of_static_writer_15() { return &___static_writer_15; }
	inline void set_static_writer_15(JsonWriter_t1165300239 * value)
	{
		___static_writer_15 = value;
		Il2CppCodeGenWriteBarrier(&___static_writer_15, value);
	}

	inline static int32_t get_offset_of_static_writer_lock_16() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___static_writer_lock_16)); }
	inline Il2CppObject * get_static_writer_lock_16() const { return ___static_writer_lock_16; }
	inline Il2CppObject ** get_address_of_static_writer_lock_16() { return &___static_writer_lock_16; }
	inline void set_static_writer_lock_16(Il2CppObject * value)
	{
		___static_writer_lock_16 = value;
		Il2CppCodeGenWriteBarrier(&___static_writer_lock_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_17() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache10_17)); }
	inline Func_2_t3963570234 * get_U3CU3Ef__amU24cache10_17() const { return ___U3CU3Ef__amU24cache10_17; }
	inline Func_2_t3963570234 ** get_address_of_U3CU3Ef__amU24cache10_17() { return &___U3CU3Ef__amU24cache10_17; }
	inline void set_U3CU3Ef__amU24cache10_17(Func_2_t3963570234 * value)
	{
		___U3CU3Ef__amU24cache10_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_18() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache11_18)); }
	inline Func_2_t785513668 * get_U3CU3Ef__amU24cache11_18() const { return ___U3CU3Ef__amU24cache11_18; }
	inline Func_2_t785513668 ** get_address_of_U3CU3Ef__amU24cache11_18() { return &___U3CU3Ef__amU24cache11_18; }
	inline void set_U3CU3Ef__amU24cache11_18(Func_2_t785513668 * value)
	{
		___U3CU3Ef__amU24cache11_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_19() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache12_19)); }
	inline Func_2_t2244419209 * get_U3CU3Ef__amU24cache12_19() const { return ___U3CU3Ef__amU24cache12_19; }
	inline Func_2_t2244419209 ** get_address_of_U3CU3Ef__amU24cache12_19() { return &___U3CU3Ef__amU24cache12_19; }
	inline void set_U3CU3Ef__amU24cache12_19(Func_2_t2244419209 * value)
	{
		___U3CU3Ef__amU24cache12_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_20() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache13_20)); }
	inline Func_2_t785513668 * get_U3CU3Ef__amU24cache13_20() const { return ___U3CU3Ef__amU24cache13_20; }
	inline Func_2_t785513668 ** get_address_of_U3CU3Ef__amU24cache13_20() { return &___U3CU3Ef__amU24cache13_20; }
	inline void set_U3CU3Ef__amU24cache13_20(Func_2_t785513668 * value)
	{
		___U3CU3Ef__amU24cache13_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_21() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache14_21)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache14_21() const { return ___U3CU3Ef__amU24cache14_21; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache14_21() { return &___U3CU3Ef__amU24cache14_21; }
	inline void set_U3CU3Ef__amU24cache14_21(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache14_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_22() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache15_22)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache15_22() const { return ___U3CU3Ef__amU24cache15_22; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache15_22() { return &___U3CU3Ef__amU24cache15_22; }
	inline void set_U3CU3Ef__amU24cache15_22(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache15_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_23() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache16_23)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache16_23() const { return ___U3CU3Ef__amU24cache16_23; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache16_23() { return &___U3CU3Ef__amU24cache16_23; }
	inline void set_U3CU3Ef__amU24cache16_23(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache16_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_24() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache17_24)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache17_24() const { return ___U3CU3Ef__amU24cache17_24; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache17_24() { return &___U3CU3Ef__amU24cache17_24; }
	inline void set_U3CU3Ef__amU24cache17_24(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache17_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_25() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache18_25)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache18_25() const { return ___U3CU3Ef__amU24cache18_25; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache18_25() { return &___U3CU3Ef__amU24cache18_25; }
	inline void set_U3CU3Ef__amU24cache18_25(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache18_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_26() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache19_26)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache19_26() const { return ___U3CU3Ef__amU24cache19_26; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache19_26() { return &___U3CU3Ef__amU24cache19_26; }
	inline void set_U3CU3Ef__amU24cache19_26(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache19_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_27() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache1A_27)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache1A_27() const { return ___U3CU3Ef__amU24cache1A_27; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache1A_27() { return &___U3CU3Ef__amU24cache1A_27; }
	inline void set_U3CU3Ef__amU24cache1A_27(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache1A_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_28() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache1B_28)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache1B_28() const { return ___U3CU3Ef__amU24cache1B_28; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache1B_28() { return &___U3CU3Ef__amU24cache1B_28; }
	inline void set_U3CU3Ef__amU24cache1B_28(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache1B_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_29() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache1C_29)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache1C_29() const { return ___U3CU3Ef__amU24cache1C_29; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache1C_29() { return &___U3CU3Ef__amU24cache1C_29; }
	inline void set_U3CU3Ef__amU24cache1C_29(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache1C_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_30() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache1D_30)); }
	inline ExporterFunc_t3330360473 * get_U3CU3Ef__amU24cache1D_30() const { return ___U3CU3Ef__amU24cache1D_30; }
	inline ExporterFunc_t3330360473 ** get_address_of_U3CU3Ef__amU24cache1D_30() { return &___U3CU3Ef__amU24cache1D_30; }
	inline void set_U3CU3Ef__amU24cache1D_30(ExporterFunc_t3330360473 * value)
	{
		___U3CU3Ef__amU24cache1D_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_31() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache1E_31)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache1E_31() const { return ___U3CU3Ef__amU24cache1E_31; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache1E_31() { return &___U3CU3Ef__amU24cache1E_31; }
	inline void set_U3CU3Ef__amU24cache1E_31(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache1E_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_32() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache1F_32)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache1F_32() const { return ___U3CU3Ef__amU24cache1F_32; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache1F_32() { return &___U3CU3Ef__amU24cache1F_32; }
	inline void set_U3CU3Ef__amU24cache1F_32(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache1F_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache20_33() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache20_33)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache20_33() const { return ___U3CU3Ef__amU24cache20_33; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache20_33() { return &___U3CU3Ef__amU24cache20_33; }
	inline void set_U3CU3Ef__amU24cache20_33(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache20_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache20_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache21_34() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache21_34)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache21_34() const { return ___U3CU3Ef__amU24cache21_34; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache21_34() { return &___U3CU3Ef__amU24cache21_34; }
	inline void set_U3CU3Ef__amU24cache21_34(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache21_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache21_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_35() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache22_35)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache22_35() const { return ___U3CU3Ef__amU24cache22_35; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache22_35() { return &___U3CU3Ef__amU24cache22_35; }
	inline void set_U3CU3Ef__amU24cache22_35(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache22_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_35, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache23_36() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache23_36)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache23_36() const { return ___U3CU3Ef__amU24cache23_36; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache23_36() { return &___U3CU3Ef__amU24cache23_36; }
	inline void set_U3CU3Ef__amU24cache23_36(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache23_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache23_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache24_37() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache24_37)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache24_37() const { return ___U3CU3Ef__amU24cache24_37; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache24_37() { return &___U3CU3Ef__amU24cache24_37; }
	inline void set_U3CU3Ef__amU24cache24_37(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache24_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache24_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache25_38() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache25_38)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache25_38() const { return ___U3CU3Ef__amU24cache25_38; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache25_38() { return &___U3CU3Ef__amU24cache25_38; }
	inline void set_U3CU3Ef__amU24cache25_38(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache25_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache25_38, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache26_39() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache26_39)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache26_39() const { return ___U3CU3Ef__amU24cache26_39; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache26_39() { return &___U3CU3Ef__amU24cache26_39; }
	inline void set_U3CU3Ef__amU24cache26_39(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache26_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache26_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache27_40() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache27_40)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache27_40() const { return ___U3CU3Ef__amU24cache27_40; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache27_40() { return &___U3CU3Ef__amU24cache27_40; }
	inline void set_U3CU3Ef__amU24cache27_40(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache27_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache27_40, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache28_41() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache28_41)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache28_41() const { return ___U3CU3Ef__amU24cache28_41; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache28_41() { return &___U3CU3Ef__amU24cache28_41; }
	inline void set_U3CU3Ef__amU24cache28_41(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache28_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache28_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache29_42() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache29_42)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache29_42() const { return ___U3CU3Ef__amU24cache29_42; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache29_42() { return &___U3CU3Ef__amU24cache29_42; }
	inline void set_U3CU3Ef__amU24cache29_42(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache29_42 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache29_42, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2A_43() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache2A_43)); }
	inline ImporterFunc_t2138319818 * get_U3CU3Ef__amU24cache2A_43() const { return ___U3CU3Ef__amU24cache2A_43; }
	inline ImporterFunc_t2138319818 ** get_address_of_U3CU3Ef__amU24cache2A_43() { return &___U3CU3Ef__amU24cache2A_43; }
	inline void set_U3CU3Ef__amU24cache2A_43(ImporterFunc_t2138319818 * value)
	{
		___U3CU3Ef__amU24cache2A_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2A_43, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2B_44() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache2B_44)); }
	inline Func_2_t785513668 * get_U3CU3Ef__amU24cache2B_44() const { return ___U3CU3Ef__amU24cache2B_44; }
	inline Func_2_t785513668 ** get_address_of_U3CU3Ef__amU24cache2B_44() { return &___U3CU3Ef__amU24cache2B_44; }
	inline void set_U3CU3Ef__amU24cache2B_44(Func_2_t785513668 * value)
	{
		___U3CU3Ef__amU24cache2B_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2B_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2C_45() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache2C_45)); }
	inline Func_2_t785513668 * get_U3CU3Ef__amU24cache2C_45() const { return ___U3CU3Ef__amU24cache2C_45; }
	inline Func_2_t785513668 ** get_address_of_U3CU3Ef__amU24cache2C_45() { return &___U3CU3Ef__amU24cache2C_45; }
	inline void set_U3CU3Ef__amU24cache2C_45(Func_2_t785513668 * value)
	{
		___U3CU3Ef__amU24cache2C_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2C_45, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2D_46() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache2D_46)); }
	inline WrapperFactory_t3264289803 * get_U3CU3Ef__amU24cache2D_46() const { return ___U3CU3Ef__amU24cache2D_46; }
	inline WrapperFactory_t3264289803 ** get_address_of_U3CU3Ef__amU24cache2D_46() { return &___U3CU3Ef__amU24cache2D_46; }
	inline void set_U3CU3Ef__amU24cache2D_46(WrapperFactory_t3264289803 * value)
	{
		___U3CU3Ef__amU24cache2D_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2D_46, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2E_47() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache2E_47)); }
	inline WrapperFactory_t3264289803 * get_U3CU3Ef__amU24cache2E_47() const { return ___U3CU3Ef__amU24cache2E_47; }
	inline WrapperFactory_t3264289803 ** get_address_of_U3CU3Ef__amU24cache2E_47() { return &___U3CU3Ef__amU24cache2E_47; }
	inline void set_U3CU3Ef__amU24cache2E_47(WrapperFactory_t3264289803 * value)
	{
		___U3CU3Ef__amU24cache2E_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2E_47, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2F_48() { return static_cast<int32_t>(offsetof(JsonMapper_t863513565_StaticFields, ___U3CU3Ef__amU24cache2F_48)); }
	inline WrapperFactory_t3264289803 * get_U3CU3Ef__amU24cache2F_48() const { return ___U3CU3Ef__amU24cache2F_48; }
	inline WrapperFactory_t3264289803 ** get_address_of_U3CU3Ef__amU24cache2F_48() { return &___U3CU3Ef__amU24cache2F_48; }
	inline void set_U3CU3Ef__amU24cache2F_48(WrapperFactory_t3264289803 * value)
	{
		___U3CU3Ef__amU24cache2F_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2F_48, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
