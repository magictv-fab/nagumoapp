﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeUpdateFilesProcess
struct FakeUpdateFilesProcess_t1656501174;

#include "codegen/il2cpp-codegen.h"

// System.Void FakeUpdateFilesProcess::.ctor()
extern "C"  void FakeUpdateFilesProcess__ctor_m3716140453 (FakeUpdateFilesProcess_t1656501174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateFilesProcess::prepare()
extern "C"  void FakeUpdateFilesProcess_prepare_m3020132778 (FakeUpdateFilesProcess_t1656501174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateFilesProcess::Play()
extern "C"  void FakeUpdateFilesProcess_Play_m4149060915 (FakeUpdateFilesProcess_t1656501174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateFilesProcess::Stop()
extern "C"  void FakeUpdateFilesProcess_Stop_m4242744961 (FakeUpdateFilesProcess_t1656501174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateFilesProcess::Pause()
extern "C"  void FakeUpdateFilesProcess_Pause_m3770266425 (FakeUpdateFilesProcess_t1656501174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
