﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Button
struct Button_t3896396478;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// IncrementalSliderController
struct IncrementalSliderController_t153649975;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DevGUITransformPanel
struct  DevGUITransformPanel_t2319713438  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Color DevGUITransformPanel::_selectedColor
	Color_t4194546905  ____selectedColor_8;
	// UnityEngine.Color DevGUITransformPanel::_defaultColor
	Color_t4194546905  ____defaultColor_9;
	// UnityEngine.UI.Button DevGUITransformPanel::_positionBtn
	Button_t3896396478 * ____positionBtn_10;
	// UnityEngine.UI.Button DevGUITransformPanel::_rotationBtn
	Button_t3896396478 * ____rotationBtn_11;
	// UnityEngine.UI.Button DevGUITransformPanel::_scaleBtn
	Button_t3896396478 * ____scaleBtn_12;
	// UnityEngine.UI.Button DevGUITransformPanel::_propertyXBtn
	Button_t3896396478 * ____propertyXBtn_13;
	// UnityEngine.UI.Button DevGUITransformPanel::_propertyYBtn
	Button_t3896396478 * ____propertyYBtn_14;
	// UnityEngine.UI.Button DevGUITransformPanel::_propertyZBtn
	Button_t3896396478 * ____propertyZBtn_15;
	// System.String DevGUITransformPanel::_currentTransform
	String_t* ____currentTransform_16;
	// System.String DevGUITransformPanel::_currentProperty
	String_t* ____currentProperty_17;
	// MagicTV.abstracts.InAppAbstract DevGUITransformPanel::currentInApp
	InAppAbstract_t382673128 * ___currentInApp_18;
	// IncrementalSliderController DevGUITransformPanel::_slider
	IncrementalSliderController_t153649975 * ____slider_19;

public:
	inline static int32_t get_offset_of__selectedColor_8() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____selectedColor_8)); }
	inline Color_t4194546905  get__selectedColor_8() const { return ____selectedColor_8; }
	inline Color_t4194546905 * get_address_of__selectedColor_8() { return &____selectedColor_8; }
	inline void set__selectedColor_8(Color_t4194546905  value)
	{
		____selectedColor_8 = value;
	}

	inline static int32_t get_offset_of__defaultColor_9() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____defaultColor_9)); }
	inline Color_t4194546905  get__defaultColor_9() const { return ____defaultColor_9; }
	inline Color_t4194546905 * get_address_of__defaultColor_9() { return &____defaultColor_9; }
	inline void set__defaultColor_9(Color_t4194546905  value)
	{
		____defaultColor_9 = value;
	}

	inline static int32_t get_offset_of__positionBtn_10() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____positionBtn_10)); }
	inline Button_t3896396478 * get__positionBtn_10() const { return ____positionBtn_10; }
	inline Button_t3896396478 ** get_address_of__positionBtn_10() { return &____positionBtn_10; }
	inline void set__positionBtn_10(Button_t3896396478 * value)
	{
		____positionBtn_10 = value;
		Il2CppCodeGenWriteBarrier(&____positionBtn_10, value);
	}

	inline static int32_t get_offset_of__rotationBtn_11() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____rotationBtn_11)); }
	inline Button_t3896396478 * get__rotationBtn_11() const { return ____rotationBtn_11; }
	inline Button_t3896396478 ** get_address_of__rotationBtn_11() { return &____rotationBtn_11; }
	inline void set__rotationBtn_11(Button_t3896396478 * value)
	{
		____rotationBtn_11 = value;
		Il2CppCodeGenWriteBarrier(&____rotationBtn_11, value);
	}

	inline static int32_t get_offset_of__scaleBtn_12() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____scaleBtn_12)); }
	inline Button_t3896396478 * get__scaleBtn_12() const { return ____scaleBtn_12; }
	inline Button_t3896396478 ** get_address_of__scaleBtn_12() { return &____scaleBtn_12; }
	inline void set__scaleBtn_12(Button_t3896396478 * value)
	{
		____scaleBtn_12 = value;
		Il2CppCodeGenWriteBarrier(&____scaleBtn_12, value);
	}

	inline static int32_t get_offset_of__propertyXBtn_13() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____propertyXBtn_13)); }
	inline Button_t3896396478 * get__propertyXBtn_13() const { return ____propertyXBtn_13; }
	inline Button_t3896396478 ** get_address_of__propertyXBtn_13() { return &____propertyXBtn_13; }
	inline void set__propertyXBtn_13(Button_t3896396478 * value)
	{
		____propertyXBtn_13 = value;
		Il2CppCodeGenWriteBarrier(&____propertyXBtn_13, value);
	}

	inline static int32_t get_offset_of__propertyYBtn_14() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____propertyYBtn_14)); }
	inline Button_t3896396478 * get__propertyYBtn_14() const { return ____propertyYBtn_14; }
	inline Button_t3896396478 ** get_address_of__propertyYBtn_14() { return &____propertyYBtn_14; }
	inline void set__propertyYBtn_14(Button_t3896396478 * value)
	{
		____propertyYBtn_14 = value;
		Il2CppCodeGenWriteBarrier(&____propertyYBtn_14, value);
	}

	inline static int32_t get_offset_of__propertyZBtn_15() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____propertyZBtn_15)); }
	inline Button_t3896396478 * get__propertyZBtn_15() const { return ____propertyZBtn_15; }
	inline Button_t3896396478 ** get_address_of__propertyZBtn_15() { return &____propertyZBtn_15; }
	inline void set__propertyZBtn_15(Button_t3896396478 * value)
	{
		____propertyZBtn_15 = value;
		Il2CppCodeGenWriteBarrier(&____propertyZBtn_15, value);
	}

	inline static int32_t get_offset_of__currentTransform_16() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____currentTransform_16)); }
	inline String_t* get__currentTransform_16() const { return ____currentTransform_16; }
	inline String_t** get_address_of__currentTransform_16() { return &____currentTransform_16; }
	inline void set__currentTransform_16(String_t* value)
	{
		____currentTransform_16 = value;
		Il2CppCodeGenWriteBarrier(&____currentTransform_16, value);
	}

	inline static int32_t get_offset_of__currentProperty_17() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____currentProperty_17)); }
	inline String_t* get__currentProperty_17() const { return ____currentProperty_17; }
	inline String_t** get_address_of__currentProperty_17() { return &____currentProperty_17; }
	inline void set__currentProperty_17(String_t* value)
	{
		____currentProperty_17 = value;
		Il2CppCodeGenWriteBarrier(&____currentProperty_17, value);
	}

	inline static int32_t get_offset_of_currentInApp_18() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ___currentInApp_18)); }
	inline InAppAbstract_t382673128 * get_currentInApp_18() const { return ___currentInApp_18; }
	inline InAppAbstract_t382673128 ** get_address_of_currentInApp_18() { return &___currentInApp_18; }
	inline void set_currentInApp_18(InAppAbstract_t382673128 * value)
	{
		___currentInApp_18 = value;
		Il2CppCodeGenWriteBarrier(&___currentInApp_18, value);
	}

	inline static int32_t get_offset_of__slider_19() { return static_cast<int32_t>(offsetof(DevGUITransformPanel_t2319713438, ____slider_19)); }
	inline IncrementalSliderController_t153649975 * get__slider_19() const { return ____slider_19; }
	inline IncrementalSliderController_t153649975 ** get_address_of__slider_19() { return &____slider_19; }
	inline void set__slider_19(IncrementalSliderController_t153649975 * value)
	{
		____slider_19 = value;
		Il2CppCodeGenWriteBarrier(&____slider_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
