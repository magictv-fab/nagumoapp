﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LoadLevel
struct  LoadLevel_t1482164620  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.LoadLevel::levelName
	FsmString_t952858651 * ___levelName_9;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadLevel::additive
	bool ___additive_10;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadLevel::async
	bool ___async_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadLevel::loadedEvent
	FsmEvent_t2133468028 * ___loadedEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadLevel::dontDestroyOnLoad
	FsmBool_t1075959796 * ___dontDestroyOnLoad_13;
	// UnityEngine.AsyncOperation HutongGames.PlayMaker.Actions.LoadLevel::asyncOperation
	AsyncOperation_t3699081103 * ___asyncOperation_14;

public:
	inline static int32_t get_offset_of_levelName_9() { return static_cast<int32_t>(offsetof(LoadLevel_t1482164620, ___levelName_9)); }
	inline FsmString_t952858651 * get_levelName_9() const { return ___levelName_9; }
	inline FsmString_t952858651 ** get_address_of_levelName_9() { return &___levelName_9; }
	inline void set_levelName_9(FsmString_t952858651 * value)
	{
		___levelName_9 = value;
		Il2CppCodeGenWriteBarrier(&___levelName_9, value);
	}

	inline static int32_t get_offset_of_additive_10() { return static_cast<int32_t>(offsetof(LoadLevel_t1482164620, ___additive_10)); }
	inline bool get_additive_10() const { return ___additive_10; }
	inline bool* get_address_of_additive_10() { return &___additive_10; }
	inline void set_additive_10(bool value)
	{
		___additive_10 = value;
	}

	inline static int32_t get_offset_of_async_11() { return static_cast<int32_t>(offsetof(LoadLevel_t1482164620, ___async_11)); }
	inline bool get_async_11() const { return ___async_11; }
	inline bool* get_address_of_async_11() { return &___async_11; }
	inline void set_async_11(bool value)
	{
		___async_11 = value;
	}

	inline static int32_t get_offset_of_loadedEvent_12() { return static_cast<int32_t>(offsetof(LoadLevel_t1482164620, ___loadedEvent_12)); }
	inline FsmEvent_t2133468028 * get_loadedEvent_12() const { return ___loadedEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_loadedEvent_12() { return &___loadedEvent_12; }
	inline void set_loadedEvent_12(FsmEvent_t2133468028 * value)
	{
		___loadedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___loadedEvent_12, value);
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_13() { return static_cast<int32_t>(offsetof(LoadLevel_t1482164620, ___dontDestroyOnLoad_13)); }
	inline FsmBool_t1075959796 * get_dontDestroyOnLoad_13() const { return ___dontDestroyOnLoad_13; }
	inline FsmBool_t1075959796 ** get_address_of_dontDestroyOnLoad_13() { return &___dontDestroyOnLoad_13; }
	inline void set_dontDestroyOnLoad_13(FsmBool_t1075959796 * value)
	{
		___dontDestroyOnLoad_13 = value;
		Il2CppCodeGenWriteBarrier(&___dontDestroyOnLoad_13, value);
	}

	inline static int32_t get_offset_of_asyncOperation_14() { return static_cast<int32_t>(offsetof(LoadLevel_t1482164620, ___asyncOperation_14)); }
	inline AsyncOperation_t3699081103 * get_asyncOperation_14() const { return ___asyncOperation_14; }
	inline AsyncOperation_t3699081103 ** get_address_of_asyncOperation_14() { return &___asyncOperation_14; }
	inline void set_asyncOperation_14(AsyncOperation_t3699081103 * value)
	{
		___asyncOperation_14 = value;
		Il2CppCodeGenWriteBarrier(&___asyncOperation_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
