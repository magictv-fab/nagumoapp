﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t530285832;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ColorInterpolate
struct  ColorInterpolate_t5558598  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.Actions.ColorInterpolate::colors
	FsmColorU5BU5D_t530285832* ___colors_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ColorInterpolate::time
	FsmFloat_t2134102846 * ___time_10;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ColorInterpolate::storeColor
	FsmColor_t2131419205 * ___storeColor_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ColorInterpolate::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_12;
	// System.Boolean HutongGames.PlayMaker.Actions.ColorInterpolate::realTime
	bool ___realTime_13;
	// System.Single HutongGames.PlayMaker.Actions.ColorInterpolate::startTime
	float ___startTime_14;
	// System.Single HutongGames.PlayMaker.Actions.ColorInterpolate::currentTime
	float ___currentTime_15;

public:
	inline static int32_t get_offset_of_colors_9() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___colors_9)); }
	inline FsmColorU5BU5D_t530285832* get_colors_9() const { return ___colors_9; }
	inline FsmColorU5BU5D_t530285832** get_address_of_colors_9() { return &___colors_9; }
	inline void set_colors_9(FsmColorU5BU5D_t530285832* value)
	{
		___colors_9 = value;
		Il2CppCodeGenWriteBarrier(&___colors_9, value);
	}

	inline static int32_t get_offset_of_time_10() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___time_10)); }
	inline FsmFloat_t2134102846 * get_time_10() const { return ___time_10; }
	inline FsmFloat_t2134102846 ** get_address_of_time_10() { return &___time_10; }
	inline void set_time_10(FsmFloat_t2134102846 * value)
	{
		___time_10 = value;
		Il2CppCodeGenWriteBarrier(&___time_10, value);
	}

	inline static int32_t get_offset_of_storeColor_11() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___storeColor_11)); }
	inline FsmColor_t2131419205 * get_storeColor_11() const { return ___storeColor_11; }
	inline FsmColor_t2131419205 ** get_address_of_storeColor_11() { return &___storeColor_11; }
	inline void set_storeColor_11(FsmColor_t2131419205 * value)
	{
		___storeColor_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeColor_11, value);
	}

	inline static int32_t get_offset_of_finishEvent_12() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___finishEvent_12)); }
	inline FsmEvent_t2133468028 * get_finishEvent_12() const { return ___finishEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_12() { return &___finishEvent_12; }
	inline void set_finishEvent_12(FsmEvent_t2133468028 * value)
	{
		___finishEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_12, value);
	}

	inline static int32_t get_offset_of_realTime_13() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___realTime_13)); }
	inline bool get_realTime_13() const { return ___realTime_13; }
	inline bool* get_address_of_realTime_13() { return &___realTime_13; }
	inline void set_realTime_13(bool value)
	{
		___realTime_13 = value;
	}

	inline static int32_t get_offset_of_startTime_14() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___startTime_14)); }
	inline float get_startTime_14() const { return ___startTime_14; }
	inline float* get_address_of_startTime_14() { return &___startTime_14; }
	inline void set_startTime_14(float value)
	{
		___startTime_14 = value;
	}

	inline static int32_t get_offset_of_currentTime_15() { return static_cast<int32_t>(offsetof(ColorInterpolate_t5558598, ___currentTime_15)); }
	inline float get_currentTime_15() const { return ___currentTime_15; }
	inline float* get_address_of_currentTime_15() { return &___currentTime_15; }
	inline void set_currentTime_15(float value)
	{
		___currentTime_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
