﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TouchEvent
struct TouchEvent_t1157390339;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TouchEvent::.ctor()
extern "C"  void TouchEvent__ctor_m2145896387 (TouchEvent_t1157390339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchEvent::Reset()
extern "C"  void TouchEvent_Reset_m4087296624 (TouchEvent_t1157390339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchEvent::OnUpdate()
extern "C"  void TouchEvent_OnUpdate_m3858013865 (TouchEvent_t1157390339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
