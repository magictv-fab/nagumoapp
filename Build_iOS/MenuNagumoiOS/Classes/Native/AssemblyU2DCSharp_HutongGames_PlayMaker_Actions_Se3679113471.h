﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t607182279;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomVector3
struct  SelectRandomVector3_t3679113471  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.Actions.SelectRandomVector3::vector3Array
	FsmVector3U5BU5D_t607182279* ___vector3Array_9;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomVector3::weights
	FsmFloatU5BU5D_t2945380875* ___weights_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SelectRandomVector3::storeVector3
	FsmVector3_t533912882 * ___storeVector3_11;

public:
	inline static int32_t get_offset_of_vector3Array_9() { return static_cast<int32_t>(offsetof(SelectRandomVector3_t3679113471, ___vector3Array_9)); }
	inline FsmVector3U5BU5D_t607182279* get_vector3Array_9() const { return ___vector3Array_9; }
	inline FsmVector3U5BU5D_t607182279** get_address_of_vector3Array_9() { return &___vector3Array_9; }
	inline void set_vector3Array_9(FsmVector3U5BU5D_t607182279* value)
	{
		___vector3Array_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Array_9, value);
	}

	inline static int32_t get_offset_of_weights_10() { return static_cast<int32_t>(offsetof(SelectRandomVector3_t3679113471, ___weights_10)); }
	inline FsmFloatU5BU5D_t2945380875* get_weights_10() const { return ___weights_10; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_weights_10() { return &___weights_10; }
	inline void set_weights_10(FsmFloatU5BU5D_t2945380875* value)
	{
		___weights_10 = value;
		Il2CppCodeGenWriteBarrier(&___weights_10, value);
	}

	inline static int32_t get_offset_of_storeVector3_11() { return static_cast<int32_t>(offsetof(SelectRandomVector3_t3679113471, ___storeVector3_11)); }
	inline FsmVector3_t533912882 * get_storeVector3_11() const { return ___storeVector3_11; }
	inline FsmVector3_t533912882 ** get_address_of_storeVector3_11() { return &___storeVector3_11; }
	inline void set_storeVector3_11(FsmVector3_t533912882 * value)
	{
		___storeVector3_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeVector3_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
