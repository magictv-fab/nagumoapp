﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utility
struct Utility_t1549674828;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Utility::.ctor()
extern "C"  void Utility__ctor_m2826353567 (Utility_t1549674828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utility::CheckIsUrlFormat(System.String)
extern "C"  bool Utility_CheckIsUrlFormat_m4066754941 (Il2CppObject * __this /* static, unused */, String_t* ___strValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Utility::CheckIsFormat(System.String,System.String)
extern "C"  bool Utility_CheckIsFormat_m2044809060 (Il2CppObject * __this /* static, unused */, String_t* ___strRegex0, String_t* ___strValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
