﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BussolaSimuladaDebug
struct BussolaSimuladaDebug_t1344610386;

#include "codegen/il2cpp-codegen.h"

// System.Void BussolaSimuladaDebug::.ctor()
extern "C"  void BussolaSimuladaDebug__ctor_m2387441289 (BussolaSimuladaDebug_t1344610386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BussolaSimuladaDebug::Start()
extern "C"  void BussolaSimuladaDebug_Start_m1334579081 (BussolaSimuladaDebug_t1344610386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BussolaSimuladaDebug::Update()
extern "C"  void BussolaSimuladaDebug_Update_m2723098020 (BussolaSimuladaDebug_t1344610386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BussolaSimuladaDebug::clock()
extern "C"  void BussolaSimuladaDebug_clock_m2433176661 (BussolaSimuladaDebug_t1344610386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
