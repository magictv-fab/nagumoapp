﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataBlock
struct DataBlock_t3210726793;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.QrCode.Internal.DataBlock[]
struct DataBlockU5BU5D_t181372468;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"

// System.Void ZXing.QrCode.Internal.DataBlock::.ctor(System.Int32,System.Byte[])
extern "C"  void DataBlock__ctor_m38277367 (DataBlock_t3210726793 * __this, int32_t ___numDataCodewords0, ByteU5BU5D_t4260760469* ___codewords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.DataBlock[] ZXing.QrCode.Internal.DataBlock::getDataBlocks(System.Byte[],ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern "C"  DataBlockU5BU5D_t181372468* DataBlock_getDataBlocks_m2116514789 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___rawCodewords0, Version_t1953509534 * ___version1, ErrorCorrectionLevel_t1225927610 * ___ecLevel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.DataBlock::get_NumDataCodewords()
extern "C"  int32_t DataBlock_get_NumDataCodewords_m1729776296 (DataBlock_t3210726793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.QrCode.Internal.DataBlock::get_Codewords()
extern "C"  ByteU5BU5D_t4260760469* DataBlock_get_Codewords_m57455774 (DataBlock_t3210726793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
