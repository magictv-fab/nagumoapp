﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertasData
struct OfertasData_t2909397772;
// OfertasManager
struct OfertasManager_t821152011;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<OfertaValues>
struct List_1_t562068899;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t957326451;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// System.String
struct String_t;
// UnityEngine.RectTransform
struct RectTransform_t972643934;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManager
struct  OfertasManager_t821152011  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<OfertaValues> OfertasManager::ofertaValuesLst
	List_1_t562068899 * ___ofertaValuesLst_11;
	// UnityEngine.GameObject OfertasManager::loadingObj
	GameObject_t3674682005 * ___loadingObj_15;
	// UnityEngine.GameObject OfertasManager::popup
	GameObject_t3674682005 * ___popup_16;
	// UnityEngine.GameObject OfertasManager::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_17;
	// UnityEngine.GameObject OfertasManager::containerItens
	GameObject_t3674682005 * ___containerItens_18;
	// UnityEngine.GameObject OfertasManager::favoritouPrimeiraVezObj
	GameObject_t3674682005 * ___favoritouPrimeiraVezObj_20;
	// UnityEngine.GameObject OfertasManager::popupSemConexao
	GameObject_t3674682005 * ___popupSemConexao_21;
	// UnityEngine.UI.ScrollRect OfertasManager::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_22;
	// System.Int32 OfertasManager::ofertaCounter
	int32_t ___ofertaCounter_23;
	// UnityEngine.GameObject OfertasManager::selectedItem
	GameObject_t3674682005 * ___selectedItem_25;
	// UnityEngine.UI.Text OfertasManager::quantidadeOfertasText
	Text_t9039225 * ___quantidadeOfertasText_26;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap OfertasManager::horizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnap_27;
	// UnityEngine.RectTransform OfertasManager::rectContainer
	RectTransform_t972643934 * ___rectContainer_30;
	// System.Boolean OfertasManager::onRectUpdate
	bool ___onRectUpdate_31;

public:
	inline static int32_t get_offset_of_ofertaValuesLst_11() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___ofertaValuesLst_11)); }
	inline List_1_t562068899 * get_ofertaValuesLst_11() const { return ___ofertaValuesLst_11; }
	inline List_1_t562068899 ** get_address_of_ofertaValuesLst_11() { return &___ofertaValuesLst_11; }
	inline void set_ofertaValuesLst_11(List_1_t562068899 * value)
	{
		___ofertaValuesLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___ofertaValuesLst_11, value);
	}

	inline static int32_t get_offset_of_loadingObj_15() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___loadingObj_15)); }
	inline GameObject_t3674682005 * get_loadingObj_15() const { return ___loadingObj_15; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_15() { return &___loadingObj_15; }
	inline void set_loadingObj_15(GameObject_t3674682005 * value)
	{
		___loadingObj_15 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_15, value);
	}

	inline static int32_t get_offset_of_popup_16() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___popup_16)); }
	inline GameObject_t3674682005 * get_popup_16() const { return ___popup_16; }
	inline GameObject_t3674682005 ** get_address_of_popup_16() { return &___popup_16; }
	inline void set_popup_16(GameObject_t3674682005 * value)
	{
		___popup_16 = value;
		Il2CppCodeGenWriteBarrier(&___popup_16, value);
	}

	inline static int32_t get_offset_of_itemPrefab_17() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___itemPrefab_17)); }
	inline GameObject_t3674682005 * get_itemPrefab_17() const { return ___itemPrefab_17; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_17() { return &___itemPrefab_17; }
	inline void set_itemPrefab_17(GameObject_t3674682005 * value)
	{
		___itemPrefab_17 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_17, value);
	}

	inline static int32_t get_offset_of_containerItens_18() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___containerItens_18)); }
	inline GameObject_t3674682005 * get_containerItens_18() const { return ___containerItens_18; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_18() { return &___containerItens_18; }
	inline void set_containerItens_18(GameObject_t3674682005 * value)
	{
		___containerItens_18 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_18, value);
	}

	inline static int32_t get_offset_of_favoritouPrimeiraVezObj_20() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___favoritouPrimeiraVezObj_20)); }
	inline GameObject_t3674682005 * get_favoritouPrimeiraVezObj_20() const { return ___favoritouPrimeiraVezObj_20; }
	inline GameObject_t3674682005 ** get_address_of_favoritouPrimeiraVezObj_20() { return &___favoritouPrimeiraVezObj_20; }
	inline void set_favoritouPrimeiraVezObj_20(GameObject_t3674682005 * value)
	{
		___favoritouPrimeiraVezObj_20 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouPrimeiraVezObj_20, value);
	}

	inline static int32_t get_offset_of_popupSemConexao_21() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___popupSemConexao_21)); }
	inline GameObject_t3674682005 * get_popupSemConexao_21() const { return ___popupSemConexao_21; }
	inline GameObject_t3674682005 ** get_address_of_popupSemConexao_21() { return &___popupSemConexao_21; }
	inline void set_popupSemConexao_21(GameObject_t3674682005 * value)
	{
		___popupSemConexao_21 = value;
		Il2CppCodeGenWriteBarrier(&___popupSemConexao_21, value);
	}

	inline static int32_t get_offset_of_scrollRect_22() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___scrollRect_22)); }
	inline ScrollRect_t3606982749 * get_scrollRect_22() const { return ___scrollRect_22; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_22() { return &___scrollRect_22; }
	inline void set_scrollRect_22(ScrollRect_t3606982749 * value)
	{
		___scrollRect_22 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_22, value);
	}

	inline static int32_t get_offset_of_ofertaCounter_23() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___ofertaCounter_23)); }
	inline int32_t get_ofertaCounter_23() const { return ___ofertaCounter_23; }
	inline int32_t* get_address_of_ofertaCounter_23() { return &___ofertaCounter_23; }
	inline void set_ofertaCounter_23(int32_t value)
	{
		___ofertaCounter_23 = value;
	}

	inline static int32_t get_offset_of_selectedItem_25() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___selectedItem_25)); }
	inline GameObject_t3674682005 * get_selectedItem_25() const { return ___selectedItem_25; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_25() { return &___selectedItem_25; }
	inline void set_selectedItem_25(GameObject_t3674682005 * value)
	{
		___selectedItem_25 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_25, value);
	}

	inline static int32_t get_offset_of_quantidadeOfertasText_26() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___quantidadeOfertasText_26)); }
	inline Text_t9039225 * get_quantidadeOfertasText_26() const { return ___quantidadeOfertasText_26; }
	inline Text_t9039225 ** get_address_of_quantidadeOfertasText_26() { return &___quantidadeOfertasText_26; }
	inline void set_quantidadeOfertasText_26(Text_t9039225 * value)
	{
		___quantidadeOfertasText_26 = value;
		Il2CppCodeGenWriteBarrier(&___quantidadeOfertasText_26, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnap_27() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___horizontalScrollSnap_27)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnap_27() const { return ___horizontalScrollSnap_27; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnap_27() { return &___horizontalScrollSnap_27; }
	inline void set_horizontalScrollSnap_27(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnap_27 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnap_27, value);
	}

	inline static int32_t get_offset_of_rectContainer_30() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___rectContainer_30)); }
	inline RectTransform_t972643934 * get_rectContainer_30() const { return ___rectContainer_30; }
	inline RectTransform_t972643934 ** get_address_of_rectContainer_30() { return &___rectContainer_30; }
	inline void set_rectContainer_30(RectTransform_t972643934 * value)
	{
		___rectContainer_30 = value;
		Il2CppCodeGenWriteBarrier(&___rectContainer_30, value);
	}

	inline static int32_t get_offset_of_onRectUpdate_31() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011, ___onRectUpdate_31)); }
	inline bool get_onRectUpdate_31() const { return ___onRectUpdate_31; }
	inline bool* get_address_of_onRectUpdate_31() { return &___onRectUpdate_31; }
	inline void set_onRectUpdate_31(bool value)
	{
		___onRectUpdate_31 = value;
	}
};

struct OfertasManager_t821152011_StaticFields
{
public:
	// OfertasData OfertasManager::ofertasData
	OfertasData_t2909397772 * ___ofertasData_3;
	// OfertasManager OfertasManager::instance
	OfertasManager_t821152011 * ___instance_4;
	// System.Collections.Generic.List`1<System.String> OfertasManager::titleLst
	List_1_t1375417109 * ___titleLst_5;
	// System.Collections.Generic.List`1<System.String> OfertasManager::valueLst
	List_1_t1375417109 * ___valueLst_6;
	// System.Collections.Generic.List`1<System.String> OfertasManager::infoLst
	List_1_t1375417109 * ___infoLst_7;
	// System.Collections.Generic.List`1<System.String> OfertasManager::idLst
	List_1_t1375417109 * ___idLst_8;
	// System.Collections.Generic.List`1<System.String> OfertasManager::validadeLst
	List_1_t1375417109 * ___validadeLst_9;
	// System.Collections.Generic.List`1<System.String> OfertasManager::linkLst
	List_1_t1375417109 * ___linkLst_10;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> OfertasManager::textureLst
	List_1_t957326451 * ___textureLst_12;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> OfertasManager::imgLst
	List_1_t272385497 * ___imgLst_13;
	// System.Collections.Generic.List`1<System.Boolean> OfertasManager::favoritouLst
	List_1_t1844984270 * ___favoritouLst_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> OfertasManager::itensList
	List_1_t747900261 * ___itensList_19;
	// UnityEngine.GameObject OfertasManager::currentItem
	GameObject_t3674682005 * ___currentItem_24;
	// System.String OfertasManager::currentJsonTxt
	String_t* ___currentJsonTxt_28;
	// System.Int32 OfertasManager::loadedItensCount
	int32_t ___loadedItensCount_29;
	// System.Int32 OfertasManager::loadedImgsCount
	int32_t ___loadedImgsCount_32;

public:
	inline static int32_t get_offset_of_ofertasData_3() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___ofertasData_3)); }
	inline OfertasData_t2909397772 * get_ofertasData_3() const { return ___ofertasData_3; }
	inline OfertasData_t2909397772 ** get_address_of_ofertasData_3() { return &___ofertasData_3; }
	inline void set_ofertasData_3(OfertasData_t2909397772 * value)
	{
		___ofertasData_3 = value;
		Il2CppCodeGenWriteBarrier(&___ofertasData_3, value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___instance_4)); }
	inline OfertasManager_t821152011 * get_instance_4() const { return ___instance_4; }
	inline OfertasManager_t821152011 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(OfertasManager_t821152011 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}

	inline static int32_t get_offset_of_titleLst_5() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___titleLst_5)); }
	inline List_1_t1375417109 * get_titleLst_5() const { return ___titleLst_5; }
	inline List_1_t1375417109 ** get_address_of_titleLst_5() { return &___titleLst_5; }
	inline void set_titleLst_5(List_1_t1375417109 * value)
	{
		___titleLst_5 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_5, value);
	}

	inline static int32_t get_offset_of_valueLst_6() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___valueLst_6)); }
	inline List_1_t1375417109 * get_valueLst_6() const { return ___valueLst_6; }
	inline List_1_t1375417109 ** get_address_of_valueLst_6() { return &___valueLst_6; }
	inline void set_valueLst_6(List_1_t1375417109 * value)
	{
		___valueLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_6, value);
	}

	inline static int32_t get_offset_of_infoLst_7() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___infoLst_7)); }
	inline List_1_t1375417109 * get_infoLst_7() const { return ___infoLst_7; }
	inline List_1_t1375417109 ** get_address_of_infoLst_7() { return &___infoLst_7; }
	inline void set_infoLst_7(List_1_t1375417109 * value)
	{
		___infoLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_7, value);
	}

	inline static int32_t get_offset_of_idLst_8() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___idLst_8)); }
	inline List_1_t1375417109 * get_idLst_8() const { return ___idLst_8; }
	inline List_1_t1375417109 ** get_address_of_idLst_8() { return &___idLst_8; }
	inline void set_idLst_8(List_1_t1375417109 * value)
	{
		___idLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_8, value);
	}

	inline static int32_t get_offset_of_validadeLst_9() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___validadeLst_9)); }
	inline List_1_t1375417109 * get_validadeLst_9() const { return ___validadeLst_9; }
	inline List_1_t1375417109 ** get_address_of_validadeLst_9() { return &___validadeLst_9; }
	inline void set_validadeLst_9(List_1_t1375417109 * value)
	{
		___validadeLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___validadeLst_9, value);
	}

	inline static int32_t get_offset_of_linkLst_10() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___linkLst_10)); }
	inline List_1_t1375417109 * get_linkLst_10() const { return ___linkLst_10; }
	inline List_1_t1375417109 ** get_address_of_linkLst_10() { return &___linkLst_10; }
	inline void set_linkLst_10(List_1_t1375417109 * value)
	{
		___linkLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___linkLst_10, value);
	}

	inline static int32_t get_offset_of_textureLst_12() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___textureLst_12)); }
	inline List_1_t957326451 * get_textureLst_12() const { return ___textureLst_12; }
	inline List_1_t957326451 ** get_address_of_textureLst_12() { return &___textureLst_12; }
	inline void set_textureLst_12(List_1_t957326451 * value)
	{
		___textureLst_12 = value;
		Il2CppCodeGenWriteBarrier(&___textureLst_12, value);
	}

	inline static int32_t get_offset_of_imgLst_13() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___imgLst_13)); }
	inline List_1_t272385497 * get_imgLst_13() const { return ___imgLst_13; }
	inline List_1_t272385497 ** get_address_of_imgLst_13() { return &___imgLst_13; }
	inline void set_imgLst_13(List_1_t272385497 * value)
	{
		___imgLst_13 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_13, value);
	}

	inline static int32_t get_offset_of_favoritouLst_14() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___favoritouLst_14)); }
	inline List_1_t1844984270 * get_favoritouLst_14() const { return ___favoritouLst_14; }
	inline List_1_t1844984270 ** get_address_of_favoritouLst_14() { return &___favoritouLst_14; }
	inline void set_favoritouLst_14(List_1_t1844984270 * value)
	{
		___favoritouLst_14 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouLst_14, value);
	}

	inline static int32_t get_offset_of_itensList_19() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___itensList_19)); }
	inline List_1_t747900261 * get_itensList_19() const { return ___itensList_19; }
	inline List_1_t747900261 ** get_address_of_itensList_19() { return &___itensList_19; }
	inline void set_itensList_19(List_1_t747900261 * value)
	{
		___itensList_19 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_19, value);
	}

	inline static int32_t get_offset_of_currentItem_24() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___currentItem_24)); }
	inline GameObject_t3674682005 * get_currentItem_24() const { return ___currentItem_24; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_24() { return &___currentItem_24; }
	inline void set_currentItem_24(GameObject_t3674682005 * value)
	{
		___currentItem_24 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_24, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_28() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___currentJsonTxt_28)); }
	inline String_t* get_currentJsonTxt_28() const { return ___currentJsonTxt_28; }
	inline String_t** get_address_of_currentJsonTxt_28() { return &___currentJsonTxt_28; }
	inline void set_currentJsonTxt_28(String_t* value)
	{
		___currentJsonTxt_28 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_28, value);
	}

	inline static int32_t get_offset_of_loadedItensCount_29() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___loadedItensCount_29)); }
	inline int32_t get_loadedItensCount_29() const { return ___loadedItensCount_29; }
	inline int32_t* get_address_of_loadedItensCount_29() { return &___loadedItensCount_29; }
	inline void set_loadedItensCount_29(int32_t value)
	{
		___loadedItensCount_29 = value;
	}

	inline static int32_t get_offset_of_loadedImgsCount_32() { return static_cast<int32_t>(offsetof(OfertasManager_t821152011_StaticFields, ___loadedImgsCount_32)); }
	inline int32_t get_loadedImgsCount_32() const { return ___loadedImgsCount_32; }
	inline int32_t* get_address_of_loadedImgsCount_32() { return &___loadedImgsCount_32; }
	inline void set_loadedImgsCount_32(int32_t value)
	{
		___loadedImgsCount_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
