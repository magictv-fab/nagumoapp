﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.CronWatcher/OnCronEventHandler
struct OnCronEventHandler_t343176941;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.CronWatcher
struct  CronWatcher_t1902937828  : public MonoBehaviour_t667441552
{
public:

public:
};

struct CronWatcher_t1902937828_StaticFields
{
public:
	// ARM.utils.CronWatcher/OnCronEventHandler ARM.utils.CronWatcher::onUpdate
	OnCronEventHandler_t343176941 * ___onUpdate_2;
	// ARM.utils.CronWatcher/OnCronEventHandler ARM.utils.CronWatcher::onLateUpdate
	OnCronEventHandler_t343176941 * ___onLateUpdate_3;

public:
	inline static int32_t get_offset_of_onUpdate_2() { return static_cast<int32_t>(offsetof(CronWatcher_t1902937828_StaticFields, ___onUpdate_2)); }
	inline OnCronEventHandler_t343176941 * get_onUpdate_2() const { return ___onUpdate_2; }
	inline OnCronEventHandler_t343176941 ** get_address_of_onUpdate_2() { return &___onUpdate_2; }
	inline void set_onUpdate_2(OnCronEventHandler_t343176941 * value)
	{
		___onUpdate_2 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdate_2, value);
	}

	inline static int32_t get_offset_of_onLateUpdate_3() { return static_cast<int32_t>(offsetof(CronWatcher_t1902937828_StaticFields, ___onLateUpdate_3)); }
	inline OnCronEventHandler_t343176941 * get_onLateUpdate_3() const { return ___onLateUpdate_3; }
	inline OnCronEventHandler_t343176941 ** get_address_of_onLateUpdate_3() { return &___onLateUpdate_3; }
	inline void set_onLateUpdate_3(OnCronEventHandler_t343176941 * value)
	{
		___onLateUpdate_3 = value;
		Il2CppCodeGenWriteBarrier(&___onLateUpdate_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
