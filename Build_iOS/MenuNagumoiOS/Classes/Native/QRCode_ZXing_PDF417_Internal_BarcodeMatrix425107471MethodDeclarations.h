﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.BarcodeMatrix
struct BarcodeMatrix_t425107471;
// ZXing.PDF417.Internal.BarcodeRow
struct BarcodeRow_t3616570866;
// System.SByte[][]
struct SByteU5BU5DU5BU5D_t694789605;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.PDF417.Internal.BarcodeMatrix::.ctor(System.Int32,System.Int32)
extern "C"  void BarcodeMatrix__ctor_m1920467847 (BarcodeMatrix_t425107471 * __this, int32_t ___height0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMatrix::startRow()
extern "C"  void BarcodeMatrix_startRow_m1504611037 (BarcodeMatrix_t425107471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BarcodeRow ZXing.PDF417.Internal.BarcodeMatrix::getCurrentRow()
extern "C"  BarcodeRow_t3616570866 * BarcodeMatrix_getCurrentRow_m2675324354 (BarcodeMatrix_t425107471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte[][] ZXing.PDF417.Internal.BarcodeMatrix::getScaledMatrix(System.Int32,System.Int32)
extern "C"  SByteU5BU5DU5BU5D_t694789605* BarcodeMatrix_getScaledMatrix_m1316092539 (BarcodeMatrix_t425107471 * __this, int32_t ___xScale0, int32_t ___yScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
