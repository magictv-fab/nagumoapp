﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeDeviceFileInfo
struct FakeDeviceFileInfo_t2440618901;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// System.String
struct String_t;
// ReturnDataVO
struct ReturnDataVO_t544837971;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.KeyValueVO
struct KeyValueVO_t1780100105;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ResultRevisionVO2445597391.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyValueVO1780100105.h"

// System.Void FakeDeviceFileInfo::.ctor()
extern "C"  void FakeDeviceFileInfo__ctor_m3149616294 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeDeviceFileInfo::populateIndexedBundles()
extern "C"  void FakeDeviceFileInfo_populateIndexedBundles_m3124064648 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] FakeDeviceFileInfo::getAllUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* FakeDeviceFileInfo_getAllUrlInfoVO_m1433431892 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] FakeDeviceFileInfo::getCachedUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* FakeDeviceFileInfo_getCachedUrlInfoVO_m3534000247 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeDeviceFileInfo::prepare()
extern "C"  void FakeDeviceFileInfo_prepare_m4051262571 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO FakeDeviceFileInfo::getRevisionByRevisionId(System.String)
extern "C"  ResultRevisionVO_t2445597391 * FakeDeviceFileInfo_getRevisionByRevisionId_m1161222444 (FakeDeviceFileInfo_t2440618901 * __this, String_t* ___revision_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] FakeDeviceFileInfo::getUrlInfoVOToDelete()
extern "C"  UrlInfoVOU5BU5D_t1681515545* FakeDeviceFileInfo_getUrlInfoVOToDelete_m2458967839 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO FakeDeviceFileInfo::delete(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * FakeDeviceFileInfo_delete_m3167369939 (FakeDeviceFileInfo_t2440618901 * __this, UrlInfoVO_t1761987528 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO FakeDeviceFileInfo::delete(MagicTV.vo.UrlInfoVO[])
extern "C"  ReturnDataVO_t544837971 * FakeDeviceFileInfo_delete_m3005716785 (FakeDeviceFileInfo_t2440618901 * __this, UrlInfoVOU5BU5D_t1681515545* ___objs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO FakeDeviceFileInfo::getCurrentRevision()
extern "C"  ResultRevisionVO_t2445597391 * FakeDeviceFileInfo_getCurrentRevision_m412190536 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO FakeDeviceFileInfo::commit(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * FakeDeviceFileInfo_commit_m2617557068 (FakeDeviceFileInfo_t2440618901 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO FakeDeviceFileInfo::commit(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * FakeDeviceFileInfo_commit_m2975231327 (FakeDeviceFileInfo_t2440618901 * __this, UrlInfoVO_t1761987528 * ___urlInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO FakeDeviceFileInfo::commit(MagicTV.vo.KeyValueVO)
extern "C"  ReturnDataVO_t544837971 * FakeDeviceFileInfo_commit_m1771961810 (FakeDeviceFileInfo_t2440618901 * __this, KeyValueVO_t1780100105 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO[] FakeDeviceFileInfo::getBundles(System.String)
extern "C"  BundleVOU5BU5D_t2162892100* FakeDeviceFileInfo_getBundles_m4072526633 (FakeDeviceFileInfo_t2440618901 * __this, String_t* ___appContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO FakeDeviceFileInfo::getUncachedRevision()
extern "C"  ResultRevisionVO_t2445597391 * FakeDeviceFileInfo_getUncachedRevision_m1579934980 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeDeviceFileInfo::setImagesSplash(System.String[])
extern "C"  void FakeDeviceFileInfo_setImagesSplash_m885327003 (FakeDeviceFileInfo_t2440618901 * __this, StringU5BU5D_t4054002952* ___imagesUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FakeDeviceFileInfo::getImageSplash()
extern "C"  String_t* FakeDeviceFileInfo_getImageSplash_m3521365901 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeDeviceFileInfo::Save()
extern "C"  void FakeDeviceFileInfo_Save_m4068573723 (FakeDeviceFileInfo_t2440618901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
