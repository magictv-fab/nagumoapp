﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerSetProperties
struct MasterServerSetProperties_t3940320318;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::.ctor()
extern "C"  void MasterServerSetProperties__ctor_m1697879736 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::Reset()
extern "C"  void MasterServerSetProperties_Reset_m3639279973 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::OnEnter()
extern "C"  void MasterServerSetProperties_OnEnter_m3261549711 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::SetMasterServerProperties()
extern "C"  void MasterServerSetProperties_SetMasterServerProperties_m164245296 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
