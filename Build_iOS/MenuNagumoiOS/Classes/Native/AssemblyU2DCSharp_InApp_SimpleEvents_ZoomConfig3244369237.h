﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InApp.SimpleEvents.ZoomConfig
struct ZoomConfig_t3244369237;
// ARM.transform.filters.CameraZoomFilter
struct CameraZoomFilter_t2989246420;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.ZoomConfig
struct  ZoomConfig_t3244369237  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// System.Boolean InApp.SimpleEvents.ZoomConfig::DebugMode
	bool ___DebugMode_3;
	// ARM.transform.filters.CameraZoomFilter InApp.SimpleEvents.ZoomConfig::cameraZoomFilter
	CameraZoomFilter_t2989246420 * ___cameraZoomFilter_4;
	// System.Single InApp.SimpleEvents.ZoomConfig::percentZoom
	float ___percentZoom_5;
	// System.Single InApp.SimpleEvents.ZoomConfig::scaleDeltaMinimum
	float ___scaleDeltaMinimum_6;
	// System.Single InApp.SimpleEvents.ZoomConfig::scaleDeltaMaximum
	float ___scaleDeltaMaximum_7;
	// System.Single InApp.SimpleEvents.ZoomConfig::deltaZoomRatio
	float ___deltaZoomRatio_8;
	// System.Single InApp.SimpleEvents.ZoomConfig::zoomMax
	float ___zoomMax_9;
	// System.Single InApp.SimpleEvents.ZoomConfig::zoomMin
	float ___zoomMin_10;
	// System.Single InApp.SimpleEvents.ZoomConfig::_initialPercentZoom
	float ____initialPercentZoom_11;
	// System.Single InApp.SimpleEvents.ZoomConfig::_initialScaleDeltaMinimum
	float ____initialScaleDeltaMinimum_12;
	// System.Single InApp.SimpleEvents.ZoomConfig::_initialScaleDeltaMaximum
	float ____initialScaleDeltaMaximum_13;
	// System.Single InApp.SimpleEvents.ZoomConfig::_initialDeltaZoomRatio
	float ____initialDeltaZoomRatio_14;
	// System.Single InApp.SimpleEvents.ZoomConfig::_initialZoomMax
	float ____initialZoomMax_15;
	// System.Single InApp.SimpleEvents.ZoomConfig::_initialZoomMin
	float ____initialZoomMin_16;

public:
	inline static int32_t get_offset_of_DebugMode_3() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___DebugMode_3)); }
	inline bool get_DebugMode_3() const { return ___DebugMode_3; }
	inline bool* get_address_of_DebugMode_3() { return &___DebugMode_3; }
	inline void set_DebugMode_3(bool value)
	{
		___DebugMode_3 = value;
	}

	inline static int32_t get_offset_of_cameraZoomFilter_4() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___cameraZoomFilter_4)); }
	inline CameraZoomFilter_t2989246420 * get_cameraZoomFilter_4() const { return ___cameraZoomFilter_4; }
	inline CameraZoomFilter_t2989246420 ** get_address_of_cameraZoomFilter_4() { return &___cameraZoomFilter_4; }
	inline void set_cameraZoomFilter_4(CameraZoomFilter_t2989246420 * value)
	{
		___cameraZoomFilter_4 = value;
		Il2CppCodeGenWriteBarrier(&___cameraZoomFilter_4, value);
	}

	inline static int32_t get_offset_of_percentZoom_5() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___percentZoom_5)); }
	inline float get_percentZoom_5() const { return ___percentZoom_5; }
	inline float* get_address_of_percentZoom_5() { return &___percentZoom_5; }
	inline void set_percentZoom_5(float value)
	{
		___percentZoom_5 = value;
	}

	inline static int32_t get_offset_of_scaleDeltaMinimum_6() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___scaleDeltaMinimum_6)); }
	inline float get_scaleDeltaMinimum_6() const { return ___scaleDeltaMinimum_6; }
	inline float* get_address_of_scaleDeltaMinimum_6() { return &___scaleDeltaMinimum_6; }
	inline void set_scaleDeltaMinimum_6(float value)
	{
		___scaleDeltaMinimum_6 = value;
	}

	inline static int32_t get_offset_of_scaleDeltaMaximum_7() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___scaleDeltaMaximum_7)); }
	inline float get_scaleDeltaMaximum_7() const { return ___scaleDeltaMaximum_7; }
	inline float* get_address_of_scaleDeltaMaximum_7() { return &___scaleDeltaMaximum_7; }
	inline void set_scaleDeltaMaximum_7(float value)
	{
		___scaleDeltaMaximum_7 = value;
	}

	inline static int32_t get_offset_of_deltaZoomRatio_8() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___deltaZoomRatio_8)); }
	inline float get_deltaZoomRatio_8() const { return ___deltaZoomRatio_8; }
	inline float* get_address_of_deltaZoomRatio_8() { return &___deltaZoomRatio_8; }
	inline void set_deltaZoomRatio_8(float value)
	{
		___deltaZoomRatio_8 = value;
	}

	inline static int32_t get_offset_of_zoomMax_9() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___zoomMax_9)); }
	inline float get_zoomMax_9() const { return ___zoomMax_9; }
	inline float* get_address_of_zoomMax_9() { return &___zoomMax_9; }
	inline void set_zoomMax_9(float value)
	{
		___zoomMax_9 = value;
	}

	inline static int32_t get_offset_of_zoomMin_10() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ___zoomMin_10)); }
	inline float get_zoomMin_10() const { return ___zoomMin_10; }
	inline float* get_address_of_zoomMin_10() { return &___zoomMin_10; }
	inline void set_zoomMin_10(float value)
	{
		___zoomMin_10 = value;
	}

	inline static int32_t get_offset_of__initialPercentZoom_11() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ____initialPercentZoom_11)); }
	inline float get__initialPercentZoom_11() const { return ____initialPercentZoom_11; }
	inline float* get_address_of__initialPercentZoom_11() { return &____initialPercentZoom_11; }
	inline void set__initialPercentZoom_11(float value)
	{
		____initialPercentZoom_11 = value;
	}

	inline static int32_t get_offset_of__initialScaleDeltaMinimum_12() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ____initialScaleDeltaMinimum_12)); }
	inline float get__initialScaleDeltaMinimum_12() const { return ____initialScaleDeltaMinimum_12; }
	inline float* get_address_of__initialScaleDeltaMinimum_12() { return &____initialScaleDeltaMinimum_12; }
	inline void set__initialScaleDeltaMinimum_12(float value)
	{
		____initialScaleDeltaMinimum_12 = value;
	}

	inline static int32_t get_offset_of__initialScaleDeltaMaximum_13() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ____initialScaleDeltaMaximum_13)); }
	inline float get__initialScaleDeltaMaximum_13() const { return ____initialScaleDeltaMaximum_13; }
	inline float* get_address_of__initialScaleDeltaMaximum_13() { return &____initialScaleDeltaMaximum_13; }
	inline void set__initialScaleDeltaMaximum_13(float value)
	{
		____initialScaleDeltaMaximum_13 = value;
	}

	inline static int32_t get_offset_of__initialDeltaZoomRatio_14() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ____initialDeltaZoomRatio_14)); }
	inline float get__initialDeltaZoomRatio_14() const { return ____initialDeltaZoomRatio_14; }
	inline float* get_address_of__initialDeltaZoomRatio_14() { return &____initialDeltaZoomRatio_14; }
	inline void set__initialDeltaZoomRatio_14(float value)
	{
		____initialDeltaZoomRatio_14 = value;
	}

	inline static int32_t get_offset_of__initialZoomMax_15() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ____initialZoomMax_15)); }
	inline float get__initialZoomMax_15() const { return ____initialZoomMax_15; }
	inline float* get_address_of__initialZoomMax_15() { return &____initialZoomMax_15; }
	inline void set__initialZoomMax_15(float value)
	{
		____initialZoomMax_15 = value;
	}

	inline static int32_t get_offset_of__initialZoomMin_16() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237, ____initialZoomMin_16)); }
	inline float get__initialZoomMin_16() const { return ____initialZoomMin_16; }
	inline float* get_address_of__initialZoomMin_16() { return &____initialZoomMin_16; }
	inline void set__initialZoomMin_16(float value)
	{
		____initialZoomMin_16 = value;
	}
};

struct ZoomConfig_t3244369237_StaticFields
{
public:
	// InApp.SimpleEvents.ZoomConfig InApp.SimpleEvents.ZoomConfig::_instance
	ZoomConfig_t3244369237 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(ZoomConfig_t3244369237_StaticFields, ____instance_2)); }
	inline ZoomConfig_t3244369237 * get__instance_2() const { return ____instance_2; }
	inline ZoomConfig_t3244369237 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ZoomConfig_t3244369237 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
