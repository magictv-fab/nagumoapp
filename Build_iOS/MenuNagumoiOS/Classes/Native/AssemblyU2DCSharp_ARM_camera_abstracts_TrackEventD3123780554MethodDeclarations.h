﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.camera.abstracts.TrackEventDispatcherAbstract
struct TrackEventDispatcherAbstract_t3123780554;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.camera.abstracts.TrackEventDispatcherAbstract::.ctor()
extern "C"  void TrackEventDispatcherAbstract__ctor_m1322559871 (TrackEventDispatcherAbstract_t3123780554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.camera.abstracts.TrackEventDispatcherAbstract::_onTrackIn()
extern "C"  void TrackEventDispatcherAbstract__onTrackIn_m1483735991 (TrackEventDispatcherAbstract_t3123780554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.camera.abstracts.TrackEventDispatcherAbstract::_onTrackOut()
extern "C"  void TrackEventDispatcherAbstract__onTrackOut_m3051965470 (TrackEventDispatcherAbstract_t3123780554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
