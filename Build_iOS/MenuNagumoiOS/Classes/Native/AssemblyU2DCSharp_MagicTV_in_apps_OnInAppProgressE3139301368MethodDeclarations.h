﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.OnInAppProgressEventHandler
struct OnInAppProgressEventHandler_t3139301368;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.in_apps.OnInAppProgressEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInAppProgressEventHandler__ctor_m781685712 (OnInAppProgressEventHandler_t3139301368 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.OnInAppProgressEventHandler::Invoke(System.Single)
extern "C"  void OnInAppProgressEventHandler_Invoke_m1510945217 (OnInAppProgressEventHandler_t3139301368 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.in_apps.OnInAppProgressEventHandler::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInAppProgressEventHandler_BeginInvoke_m1485771484 (OnInAppProgressEventHandler_t3139301368 * __this, float ___progress0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.OnInAppProgressEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnInAppProgressEventHandler_EndInvoke_m787312096 (OnInAppProgressEventHandler_t3139301368 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
