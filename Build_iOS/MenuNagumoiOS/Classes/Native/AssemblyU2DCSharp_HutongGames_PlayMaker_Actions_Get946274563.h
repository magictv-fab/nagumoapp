﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorDelta
struct  GetAnimatorDelta_t946274563  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorDelta::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorDelta::deltaPosition
	FsmVector3_t533912882 * ___deltaPosition_10;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorDelta::deltaRotation
	FsmQuaternion_t3871136040 * ___deltaRotation_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorDelta::everyFrame
	bool ___everyFrame_12;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorDelta::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_13;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorDelta::_transform
	Transform_t1659122786 * ____transform_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorDelta::_animator
	Animator_t2776330603 * ____animator_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_deltaPosition_10() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ___deltaPosition_10)); }
	inline FsmVector3_t533912882 * get_deltaPosition_10() const { return ___deltaPosition_10; }
	inline FsmVector3_t533912882 ** get_address_of_deltaPosition_10() { return &___deltaPosition_10; }
	inline void set_deltaPosition_10(FsmVector3_t533912882 * value)
	{
		___deltaPosition_10 = value;
		Il2CppCodeGenWriteBarrier(&___deltaPosition_10, value);
	}

	inline static int32_t get_offset_of_deltaRotation_11() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ___deltaRotation_11)); }
	inline FsmQuaternion_t3871136040 * get_deltaRotation_11() const { return ___deltaRotation_11; }
	inline FsmQuaternion_t3871136040 ** get_address_of_deltaRotation_11() { return &___deltaRotation_11; }
	inline void set_deltaRotation_11(FsmQuaternion_t3871136040 * value)
	{
		___deltaRotation_11 = value;
		Il2CppCodeGenWriteBarrier(&___deltaRotation_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of__animatorProxy_13() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ____animatorProxy_13)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_13() const { return ____animatorProxy_13; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_13() { return &____animatorProxy_13; }
	inline void set__animatorProxy_13(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_13 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_13, value);
	}

	inline static int32_t get_offset_of__transform_14() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ____transform_14)); }
	inline Transform_t1659122786 * get__transform_14() const { return ____transform_14; }
	inline Transform_t1659122786 ** get_address_of__transform_14() { return &____transform_14; }
	inline void set__transform_14(Transform_t1659122786 * value)
	{
		____transform_14 = value;
		Il2CppCodeGenWriteBarrier(&____transform_14, value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t946274563, ____animator_15)); }
	inline Animator_t2776330603 * get__animator_15() const { return ____animator_15; }
	inline Animator_t2776330603 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t2776330603 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier(&____animator_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
