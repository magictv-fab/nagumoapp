﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// QRCodeEncodeController/QREncodeFinished
struct QREncodeFinished_t363463992;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_QRCodeEncodeController_CodeMode3120468063.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QRCodeEncodeController
struct  QRCodeEncodeController_t2317858336  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D QRCodeEncodeController::m_EncodedTex
	Texture2D_t3884108195 * ___m_EncodedTex_2;
	// System.Int32 QRCodeEncodeController::e_QRCodeWidth
	int32_t ___e_QRCodeWidth_3;
	// System.Int32 QRCodeEncodeController::e_QRCodeHeight
	int32_t ___e_QRCodeHeight_4;
	// ZXing.Common.BitMatrix QRCodeEncodeController::byteMatrix
	BitMatrix_t1058711404 * ___byteMatrix_5;
	// QRCodeEncodeController/CodeMode QRCodeEncodeController::eCodeFormat
	int32_t ___eCodeFormat_6;
	// UnityEngine.Texture2D QRCodeEncodeController::e_QRLogoTex
	Texture2D_t3884108195 * ___e_QRLogoTex_7;
	// UnityEngine.Texture2D QRCodeEncodeController::tempLogoTex
	Texture2D_t3884108195 * ___tempLogoTex_8;
	// System.Single QRCodeEncodeController::e_EmbedLogoRatio
	float ___e_EmbedLogoRatio_9;
	// QRCodeEncodeController/QREncodeFinished QRCodeEncodeController::onQREncodeFinished
	QREncodeFinished_t363463992 * ___onQREncodeFinished_10;

public:
	inline static int32_t get_offset_of_m_EncodedTex_2() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___m_EncodedTex_2)); }
	inline Texture2D_t3884108195 * get_m_EncodedTex_2() const { return ___m_EncodedTex_2; }
	inline Texture2D_t3884108195 ** get_address_of_m_EncodedTex_2() { return &___m_EncodedTex_2; }
	inline void set_m_EncodedTex_2(Texture2D_t3884108195 * value)
	{
		___m_EncodedTex_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_EncodedTex_2, value);
	}

	inline static int32_t get_offset_of_e_QRCodeWidth_3() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___e_QRCodeWidth_3)); }
	inline int32_t get_e_QRCodeWidth_3() const { return ___e_QRCodeWidth_3; }
	inline int32_t* get_address_of_e_QRCodeWidth_3() { return &___e_QRCodeWidth_3; }
	inline void set_e_QRCodeWidth_3(int32_t value)
	{
		___e_QRCodeWidth_3 = value;
	}

	inline static int32_t get_offset_of_e_QRCodeHeight_4() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___e_QRCodeHeight_4)); }
	inline int32_t get_e_QRCodeHeight_4() const { return ___e_QRCodeHeight_4; }
	inline int32_t* get_address_of_e_QRCodeHeight_4() { return &___e_QRCodeHeight_4; }
	inline void set_e_QRCodeHeight_4(int32_t value)
	{
		___e_QRCodeHeight_4 = value;
	}

	inline static int32_t get_offset_of_byteMatrix_5() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___byteMatrix_5)); }
	inline BitMatrix_t1058711404 * get_byteMatrix_5() const { return ___byteMatrix_5; }
	inline BitMatrix_t1058711404 ** get_address_of_byteMatrix_5() { return &___byteMatrix_5; }
	inline void set_byteMatrix_5(BitMatrix_t1058711404 * value)
	{
		___byteMatrix_5 = value;
		Il2CppCodeGenWriteBarrier(&___byteMatrix_5, value);
	}

	inline static int32_t get_offset_of_eCodeFormat_6() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___eCodeFormat_6)); }
	inline int32_t get_eCodeFormat_6() const { return ___eCodeFormat_6; }
	inline int32_t* get_address_of_eCodeFormat_6() { return &___eCodeFormat_6; }
	inline void set_eCodeFormat_6(int32_t value)
	{
		___eCodeFormat_6 = value;
	}

	inline static int32_t get_offset_of_e_QRLogoTex_7() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___e_QRLogoTex_7)); }
	inline Texture2D_t3884108195 * get_e_QRLogoTex_7() const { return ___e_QRLogoTex_7; }
	inline Texture2D_t3884108195 ** get_address_of_e_QRLogoTex_7() { return &___e_QRLogoTex_7; }
	inline void set_e_QRLogoTex_7(Texture2D_t3884108195 * value)
	{
		___e_QRLogoTex_7 = value;
		Il2CppCodeGenWriteBarrier(&___e_QRLogoTex_7, value);
	}

	inline static int32_t get_offset_of_tempLogoTex_8() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___tempLogoTex_8)); }
	inline Texture2D_t3884108195 * get_tempLogoTex_8() const { return ___tempLogoTex_8; }
	inline Texture2D_t3884108195 ** get_address_of_tempLogoTex_8() { return &___tempLogoTex_8; }
	inline void set_tempLogoTex_8(Texture2D_t3884108195 * value)
	{
		___tempLogoTex_8 = value;
		Il2CppCodeGenWriteBarrier(&___tempLogoTex_8, value);
	}

	inline static int32_t get_offset_of_e_EmbedLogoRatio_9() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___e_EmbedLogoRatio_9)); }
	inline float get_e_EmbedLogoRatio_9() const { return ___e_EmbedLogoRatio_9; }
	inline float* get_address_of_e_EmbedLogoRatio_9() { return &___e_EmbedLogoRatio_9; }
	inline void set_e_EmbedLogoRatio_9(float value)
	{
		___e_EmbedLogoRatio_9 = value;
	}

	inline static int32_t get_offset_of_onQREncodeFinished_10() { return static_cast<int32_t>(offsetof(QRCodeEncodeController_t2317858336, ___onQREncodeFinished_10)); }
	inline QREncodeFinished_t363463992 * get_onQREncodeFinished_10() const { return ___onQREncodeFinished_10; }
	inline QREncodeFinished_t363463992 ** get_address_of_onQREncodeFinished_10() { return &___onQREncodeFinished_10; }
	inline void set_onQREncodeFinished_10(QREncodeFinished_t363463992 * value)
	{
		___onQREncodeFinished_10 = value;
		Il2CppCodeGenWriteBarrier(&___onQREncodeFinished_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
