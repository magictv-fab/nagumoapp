﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManager
struct OfertasManager_t821152011;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertasManager::.ctor()
extern "C"  void OfertasManager__ctor_m3711827952 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::.cctor()
extern "C"  void OfertasManager__cctor_m2915420605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::Start()
extern "C"  void OfertasManager_Start_m2658965744 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::ShowNumberSelected(System.Int32)
extern "C"  void OfertasManager_ShowNumberSelected_m3296959750 (OfertasManager_t821152011 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::InstantiateNextItem()
extern "C"  void OfertasManager_InstantiateNextItem_m2330608638 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::SendStatus(System.Int32)
extern "C"  void OfertasManager_SendStatus_m613829759 (OfertasManager_t821152011 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::BtnAccept()
extern "C"  void OfertasManager_BtnAccept_m314372114 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::BtnRecuse()
extern "C"  void OfertasManager_BtnRecuse_m1667235393 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManager::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * OfertasManager_ISendStatus_m4167029758 (OfertasManager_t821152011 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManager::ILoadOfertas()
extern "C"  Il2CppObject * OfertasManager_ILoadOfertas_m1151788111 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::Update()
extern "C"  void OfertasManager_Update_m829411613 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::LoadMoreItens()
extern "C"  void OfertasManager_LoadMoreItens_m4039003730 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManager::GetDateString(System.String)
extern "C"  String_t* OfertasManager_GetDateString_m3526132066 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManager::GetDateTimeString(System.String)
extern "C"  String_t* OfertasManager_GetDateTimeString_m512681877 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManager::ILoadImgs()
extern "C"  Il2CppObject * OfertasManager_ILoadImgs_m2042749477 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManager::LoadLoadeds()
extern "C"  Il2CppObject * OfertasManager_LoadLoadeds_m2792304974 (OfertasManager_t821152011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager::PopUp(System.String)
extern "C"  void OfertasManager_PopUp_m819348072 (OfertasManager_t821152011 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
