﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloidesData
struct TabloidesData_t2107312651;

#include "codegen/il2cpp-codegen.h"

// System.Void TabloidesData::.ctor()
extern "C"  void TabloidesData__ctor_m3843191744 (TabloidesData_t2107312651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
