﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2166990872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2781926926.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2196025020_gshared (Enumerator_t2781926926 * __this, Dictionary_2_t2166990872 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2196025020(__this, ___host0, method) ((  void (*) (Enumerator_t2781926926 *, Dictionary_2_t2166990872 *, const MethodInfo*))Enumerator__ctor_m2196025020_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2645416997_gshared (Enumerator_t2781926926 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2645416997(__this, method) ((  Il2CppObject * (*) (Enumerator_t2781926926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2645416997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3634996345_gshared (Enumerator_t2781926926 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3634996345(__this, method) ((  void (*) (Enumerator_t2781926926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3634996345_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m4278836318_gshared (Enumerator_t2781926926 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4278836318(__this, method) ((  void (*) (Enumerator_t2781926926 *, const MethodInfo*))Enumerator_Dispose_m4278836318_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1048101989_gshared (Enumerator_t2781926926 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1048101989(__this, method) ((  bool (*) (Enumerator_t2781926926 *, const MethodInfo*))Enumerator_MoveNext_m1048101989_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2836090063_gshared (Enumerator_t2781926926 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2836090063(__this, method) ((  Il2CppObject * (*) (Enumerator_t2781926926 *, const MethodInfo*))Enumerator_get_Current_m2836090063_gshared)(__this, method)
