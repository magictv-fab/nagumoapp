﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t496812608;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Security_Cryptography_CryptoStream2166124941.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct  ZipAESStream_t513042148  : public CryptoStream_t2166124941
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_stream
	Stream_t1561764144 * ____stream_19;
	// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_transform
	ZipAESTransform_t496812608 * ____transform_20;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBuffer
	ByteU5BU5D_t4260760469* ____slideBuffer_21;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufStartPos
	int32_t ____slideBufStartPos_22;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufFreePos
	int32_t ____slideBufFreePos_23;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_blockAndAuth
	int32_t ____blockAndAuth_24;

public:
	inline static int32_t get_offset_of__stream_19() { return static_cast<int32_t>(offsetof(ZipAESStream_t513042148, ____stream_19)); }
	inline Stream_t1561764144 * get__stream_19() const { return ____stream_19; }
	inline Stream_t1561764144 ** get_address_of__stream_19() { return &____stream_19; }
	inline void set__stream_19(Stream_t1561764144 * value)
	{
		____stream_19 = value;
		Il2CppCodeGenWriteBarrier(&____stream_19, value);
	}

	inline static int32_t get_offset_of__transform_20() { return static_cast<int32_t>(offsetof(ZipAESStream_t513042148, ____transform_20)); }
	inline ZipAESTransform_t496812608 * get__transform_20() const { return ____transform_20; }
	inline ZipAESTransform_t496812608 ** get_address_of__transform_20() { return &____transform_20; }
	inline void set__transform_20(ZipAESTransform_t496812608 * value)
	{
		____transform_20 = value;
		Il2CppCodeGenWriteBarrier(&____transform_20, value);
	}

	inline static int32_t get_offset_of__slideBuffer_21() { return static_cast<int32_t>(offsetof(ZipAESStream_t513042148, ____slideBuffer_21)); }
	inline ByteU5BU5D_t4260760469* get__slideBuffer_21() const { return ____slideBuffer_21; }
	inline ByteU5BU5D_t4260760469** get_address_of__slideBuffer_21() { return &____slideBuffer_21; }
	inline void set__slideBuffer_21(ByteU5BU5D_t4260760469* value)
	{
		____slideBuffer_21 = value;
		Il2CppCodeGenWriteBarrier(&____slideBuffer_21, value);
	}

	inline static int32_t get_offset_of__slideBufStartPos_22() { return static_cast<int32_t>(offsetof(ZipAESStream_t513042148, ____slideBufStartPos_22)); }
	inline int32_t get__slideBufStartPos_22() const { return ____slideBufStartPos_22; }
	inline int32_t* get_address_of__slideBufStartPos_22() { return &____slideBufStartPos_22; }
	inline void set__slideBufStartPos_22(int32_t value)
	{
		____slideBufStartPos_22 = value;
	}

	inline static int32_t get_offset_of__slideBufFreePos_23() { return static_cast<int32_t>(offsetof(ZipAESStream_t513042148, ____slideBufFreePos_23)); }
	inline int32_t get__slideBufFreePos_23() const { return ____slideBufFreePos_23; }
	inline int32_t* get_address_of__slideBufFreePos_23() { return &____slideBufFreePos_23; }
	inline void set__slideBufFreePos_23(int32_t value)
	{
		____slideBufFreePos_23 = value;
	}

	inline static int32_t get_offset_of__blockAndAuth_24() { return static_cast<int32_t>(offsetof(ZipAESStream_t513042148, ____blockAndAuth_24)); }
	inline int32_t get__blockAndAuth_24() const { return ____blockAndAuth_24; }
	inline int32_t* get_address_of__blockAndAuth_24() { return &____blockAndAuth_24; }
	inline void set__blockAndAuth_24(int32_t value)
	{
		____blockAndAuth_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
