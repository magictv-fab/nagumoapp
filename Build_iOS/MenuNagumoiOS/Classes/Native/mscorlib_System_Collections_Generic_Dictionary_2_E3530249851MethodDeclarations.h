﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2325949070(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3530249851 *, Dictionary_2_t2212926459 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m522470365(__this, method) ((  Il2CppObject * (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m416885095(__this, method) ((  void (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1680064030(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3992348409(__this, method) ((  Il2CppObject * (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2289462091(__this, method) ((  Il2CppObject * (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::MoveNext()
#define Enumerator_MoveNext_m1612541719(__this, method) ((  bool (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::get_Current()
#define Enumerator_get_Current_m4000150725(__this, method) ((  KeyValuePair_2_t2111707165  (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3168082144(__this, method) ((  String_t* (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1579975584(__this, method) ((  Action_1_t1392508089 * (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::Reset()
#define Enumerator_Reset_m325701600(__this, method) ((  void (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::VerifyState()
#define Enumerator_VerifyState_m3323574249(__this, method) ((  void (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1244056849(__this, method) ((  void (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action`1<UniWebViewNativeResultPayload>>::Dispose()
#define Enumerator_Dispose_m1990054064(__this, method) ((  void (*) (Enumerator_t3530249851 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
