﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSPermissionState
struct OSPermissionState_t1777959294;

#include "codegen/il2cpp-codegen.h"

// System.Void OSPermissionState::.ctor()
extern "C"  void OSPermissionState__ctor_m2485189037 (OSPermissionState_t1777959294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
