﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasValuesCRM/<IFavoritar>c__Iterator15
struct U3CIFavoritarU3Ec__Iterator15_t3323870959;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasValuesCRM/<IFavoritar>c__Iterator15::.ctor()
extern "C"  void U3CIFavoritarU3Ec__Iterator15__ctor_m895309452 (U3CIFavoritarU3Ec__Iterator15_t3323870959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasValuesCRM/<IFavoritar>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086821520 (U3CIFavoritarU3Ec__Iterator15_t3323870959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasValuesCRM/<IFavoritar>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m1381283876 (U3CIFavoritarU3Ec__Iterator15_t3323870959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasValuesCRM/<IFavoritar>c__Iterator15::MoveNext()
extern "C"  bool U3CIFavoritarU3Ec__Iterator15_MoveNext_m3645547216 (U3CIFavoritarU3Ec__Iterator15_t3323870959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM/<IFavoritar>c__Iterator15::Dispose()
extern "C"  void U3CIFavoritarU3Ec__Iterator15_Dispose_m1297207241 (U3CIFavoritarU3Ec__Iterator15_t3323870959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM/<IFavoritar>c__Iterator15::Reset()
extern "C"  void U3CIFavoritarU3Ec__Iterator15_Reset_m2836709689 (U3CIFavoritarU3Ec__Iterator15_t3323870959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
