﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;
// BigIntegerLibrary.BigInteger[]
struct BigIntegerU5BU5D_t4221983179;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t1431880590  : public Il2CppObject
{
public:

public:
};

struct DecodedBitStreamParser_t1431880590_StaticFields
{
public:
	// System.Char[] ZXing.PDF417.Internal.DecodedBitStreamParser::PUNCT_CHARS
	CharU5BU5D_t3324145743* ___PUNCT_CHARS_0;
	// System.Char[] ZXing.PDF417.Internal.DecodedBitStreamParser::MIXED_CHARS
	CharU5BU5D_t3324145743* ___MIXED_CHARS_1;
	// BigIntegerLibrary.BigInteger[] ZXing.PDF417.Internal.DecodedBitStreamParser::EXP900
	BigIntegerU5BU5D_t4221983179* ___EXP900_2;

public:
	inline static int32_t get_offset_of_PUNCT_CHARS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1431880590_StaticFields, ___PUNCT_CHARS_0)); }
	inline CharU5BU5D_t3324145743* get_PUNCT_CHARS_0() const { return ___PUNCT_CHARS_0; }
	inline CharU5BU5D_t3324145743** get_address_of_PUNCT_CHARS_0() { return &___PUNCT_CHARS_0; }
	inline void set_PUNCT_CHARS_0(CharU5BU5D_t3324145743* value)
	{
		___PUNCT_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier(&___PUNCT_CHARS_0, value);
	}

	inline static int32_t get_offset_of_MIXED_CHARS_1() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1431880590_StaticFields, ___MIXED_CHARS_1)); }
	inline CharU5BU5D_t3324145743* get_MIXED_CHARS_1() const { return ___MIXED_CHARS_1; }
	inline CharU5BU5D_t3324145743** get_address_of_MIXED_CHARS_1() { return &___MIXED_CHARS_1; }
	inline void set_MIXED_CHARS_1(CharU5BU5D_t3324145743* value)
	{
		___MIXED_CHARS_1 = value;
		Il2CppCodeGenWriteBarrier(&___MIXED_CHARS_1, value);
	}

	inline static int32_t get_offset_of_EXP900_2() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1431880590_StaticFields, ___EXP900_2)); }
	inline BigIntegerU5BU5D_t4221983179* get_EXP900_2() const { return ___EXP900_2; }
	inline BigIntegerU5BU5D_t4221983179** get_address_of_EXP900_2() { return &___EXP900_2; }
	inline void set_EXP900_2(BigIntegerU5BU5D_t4221983179* value)
	{
		___EXP900_2 = value;
		Il2CppCodeGenWriteBarrier(&___EXP900_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
