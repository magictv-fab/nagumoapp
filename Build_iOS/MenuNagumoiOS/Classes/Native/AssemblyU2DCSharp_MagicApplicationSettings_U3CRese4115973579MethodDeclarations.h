﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicApplicationSettings/<ResetHard>c__AnonStorey94
struct U3CResetHardU3Ec__AnonStorey94_t4115973579;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicApplicationSettings/<ResetHard>c__AnonStorey94::.ctor()
extern "C"  void U3CResetHardU3Ec__AnonStorey94__ctor_m1886257664 (U3CResetHardU3Ec__AnonStorey94_t4115973579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings/<ResetHard>c__AnonStorey94::<>m__26(System.String)
extern "C"  bool U3CResetHardU3Ec__AnonStorey94_U3CU3Em__26_m1816093409 (U3CResetHardU3Ec__AnonStorey94_t4115973579 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
