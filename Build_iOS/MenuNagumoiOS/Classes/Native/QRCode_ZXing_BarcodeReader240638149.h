﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>
struct Func_4_t4289141939;

#include "QRCode_ZXing_BarcodeReaderGeneric_1_gen2271663345.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeReader
struct  BarcodeReader_t240638149  : public BarcodeReaderGeneric_1_t2271663345
{
public:

public:
};

struct BarcodeReader_t240638149_StaticFields
{
public:
	// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReader::defaultCreateLuminanceSource
	Func_4_t4289141939 * ___defaultCreateLuminanceSource_14;
	// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReader::CS$<>9__CachedAnonymousMethodDelegate5
	Func_4_t4289141939 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15;

public:
	inline static int32_t get_offset_of_defaultCreateLuminanceSource_14() { return static_cast<int32_t>(offsetof(BarcodeReader_t240638149_StaticFields, ___defaultCreateLuminanceSource_14)); }
	inline Func_4_t4289141939 * get_defaultCreateLuminanceSource_14() const { return ___defaultCreateLuminanceSource_14; }
	inline Func_4_t4289141939 ** get_address_of_defaultCreateLuminanceSource_14() { return &___defaultCreateLuminanceSource_14; }
	inline void set_defaultCreateLuminanceSource_14(Func_4_t4289141939 * value)
	{
		___defaultCreateLuminanceSource_14 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCreateLuminanceSource_14, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15() { return static_cast<int32_t>(offsetof(BarcodeReader_t240638149_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15)); }
	inline Func_4_t4289141939 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15; }
	inline Func_4_t4289141939 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15(Func_4_t4289141939 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate5_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
