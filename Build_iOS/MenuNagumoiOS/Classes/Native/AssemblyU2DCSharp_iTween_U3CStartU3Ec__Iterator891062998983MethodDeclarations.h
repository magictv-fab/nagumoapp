﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<Start>c__Iterator89
struct U3CStartU3Ec__Iterator89_t1062998983;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<Start>c__Iterator89::.ctor()
extern "C"  void U3CStartU3Ec__Iterator89__ctor_m563668868 (U3CStartU3Ec__Iterator89_t1062998983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator89::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator89_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2483233742 (U3CStartU3Ec__Iterator89_t1062998983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator89::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator89_System_Collections_IEnumerator_get_Current_m593658722 (U3CStartU3Ec__Iterator89_t1062998983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<Start>c__Iterator89::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator89_MoveNext_m2210536944 (U3CStartU3Ec__Iterator89_t1062998983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator89::Dispose()
extern "C"  void U3CStartU3Ec__Iterator89_Dispose_m418185921 (U3CStartU3Ec__Iterator89_t1062998983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator89::Reset()
extern "C"  void U3CStartU3Ec__Iterator89_Reset_m2505069105 (U3CStartU3Ec__Iterator89_t1062998983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
