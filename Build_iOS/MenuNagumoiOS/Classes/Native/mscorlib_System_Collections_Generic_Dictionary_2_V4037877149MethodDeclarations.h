﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1211489805MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3506669152(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4037877149 *, Dictionary_2_t1042304140 *, const MethodInfo*))ValueCollection__ctor_m1189758011_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m675310034(__this, ___item0, method) ((  void (*) (ValueCollection_t4037877149 *, OnChangeAnToPerspectiveEventHandler_t2702236419 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2817328279_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1929908123(__this, method) ((  void (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2345850080_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3216926612(__this, ___item0, method) ((  bool (*) (ValueCollection_t4037877149 *, OnChangeAnToPerspectiveEventHandler_t2702236419 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m515246447_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1148474681(__this, ___item0, method) ((  bool (*) (ValueCollection_t4037877149 *, OnChangeAnToPerspectiveEventHandler_t2702236419 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1494266324_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3446244009(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4033103278_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m573582047(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4037877149 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1705827812_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m491327642(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1102254687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1492102471(__this, method) ((  bool (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3085389602_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m768763303(__this, method) ((  bool (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2325854210_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m27471187(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m836801070_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3209409191(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4037877149 *, OnChangeAnToPerspectiveEventHandlerU5BU5D_t2834383250*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m133534146_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4171287242(__this, method) ((  Enumerator_t3269104844  (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_GetEnumerator_m4076504933_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_Count()
#define ValueCollection_get_Count_m4183611117(__this, method) ((  int32_t (*) (ValueCollection_t4037877149 *, const MethodInfo*))ValueCollection_get_Count_m371776456_gshared)(__this, method)
