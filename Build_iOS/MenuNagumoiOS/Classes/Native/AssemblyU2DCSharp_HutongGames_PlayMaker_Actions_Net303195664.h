﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix
struct  NetworkSetLevelPrefix_t303195664  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix::levelPrefix
	FsmInt_t1596138449 * ___levelPrefix_9;

public:
	inline static int32_t get_offset_of_levelPrefix_9() { return static_cast<int32_t>(offsetof(NetworkSetLevelPrefix_t303195664, ___levelPrefix_9)); }
	inline FsmInt_t1596138449 * get_levelPrefix_9() const { return ___levelPrefix_9; }
	inline FsmInt_t1596138449 ** get_address_of_levelPrefix_9() { return &___levelPrefix_9; }
	inline void set_levelPrefix_9(FsmInt_t1596138449 * value)
	{
		___levelPrefix_9 = value;
		Il2CppCodeGenWriteBarrier(&___levelPrefix_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
