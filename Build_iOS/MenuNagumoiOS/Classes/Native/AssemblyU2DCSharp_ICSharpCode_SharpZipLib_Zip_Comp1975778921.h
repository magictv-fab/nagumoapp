﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t617095569;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_t1952417709;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t1141403250;
// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t2680377483;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct  Inflater_t1975778921  : public Il2CppObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::mode
	int32_t ___mode_17;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::readAdler
	int32_t ___readAdler_18;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::neededBits
	int32_t ___neededBits_19;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repLength
	int32_t ___repLength_20;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repDist
	int32_t ___repDist_21;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::uncomprLen
	int32_t ___uncomprLen_22;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::isLastBlock
	bool ___isLastBlock_23;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalOut
	int64_t ___totalOut_24;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalIn
	int64_t ___totalIn_25;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::noHeader
	bool ___noHeader_26;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator ICSharpCode.SharpZipLib.Zip.Compression.Inflater::input
	StreamManipulator_t2348681196 * ___input_27;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow ICSharpCode.SharpZipLib.Zip.Compression.Inflater::outputWindow
	OutputWindow_t617095569 * ___outputWindow_28;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader ICSharpCode.SharpZipLib.Zip.Compression.Inflater::dynHeader
	InflaterDynHeader_t1952417709 * ___dynHeader_29;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::litlenTree
	InflaterHuffmanTree_t1141403250 * ___litlenTree_30;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::distTree
	InflaterHuffmanTree_t1141403250 * ___distTree_31;
	// ICSharpCode.SharpZipLib.Checksums.Adler32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::adler
	Adler32_t2680377483 * ___adler_32;

public:
	inline static int32_t get_offset_of_mode_17() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___mode_17)); }
	inline int32_t get_mode_17() const { return ___mode_17; }
	inline int32_t* get_address_of_mode_17() { return &___mode_17; }
	inline void set_mode_17(int32_t value)
	{
		___mode_17 = value;
	}

	inline static int32_t get_offset_of_readAdler_18() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___readAdler_18)); }
	inline int32_t get_readAdler_18() const { return ___readAdler_18; }
	inline int32_t* get_address_of_readAdler_18() { return &___readAdler_18; }
	inline void set_readAdler_18(int32_t value)
	{
		___readAdler_18 = value;
	}

	inline static int32_t get_offset_of_neededBits_19() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___neededBits_19)); }
	inline int32_t get_neededBits_19() const { return ___neededBits_19; }
	inline int32_t* get_address_of_neededBits_19() { return &___neededBits_19; }
	inline void set_neededBits_19(int32_t value)
	{
		___neededBits_19 = value;
	}

	inline static int32_t get_offset_of_repLength_20() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___repLength_20)); }
	inline int32_t get_repLength_20() const { return ___repLength_20; }
	inline int32_t* get_address_of_repLength_20() { return &___repLength_20; }
	inline void set_repLength_20(int32_t value)
	{
		___repLength_20 = value;
	}

	inline static int32_t get_offset_of_repDist_21() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___repDist_21)); }
	inline int32_t get_repDist_21() const { return ___repDist_21; }
	inline int32_t* get_address_of_repDist_21() { return &___repDist_21; }
	inline void set_repDist_21(int32_t value)
	{
		___repDist_21 = value;
	}

	inline static int32_t get_offset_of_uncomprLen_22() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___uncomprLen_22)); }
	inline int32_t get_uncomprLen_22() const { return ___uncomprLen_22; }
	inline int32_t* get_address_of_uncomprLen_22() { return &___uncomprLen_22; }
	inline void set_uncomprLen_22(int32_t value)
	{
		___uncomprLen_22 = value;
	}

	inline static int32_t get_offset_of_isLastBlock_23() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___isLastBlock_23)); }
	inline bool get_isLastBlock_23() const { return ___isLastBlock_23; }
	inline bool* get_address_of_isLastBlock_23() { return &___isLastBlock_23; }
	inline void set_isLastBlock_23(bool value)
	{
		___isLastBlock_23 = value;
	}

	inline static int32_t get_offset_of_totalOut_24() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___totalOut_24)); }
	inline int64_t get_totalOut_24() const { return ___totalOut_24; }
	inline int64_t* get_address_of_totalOut_24() { return &___totalOut_24; }
	inline void set_totalOut_24(int64_t value)
	{
		___totalOut_24 = value;
	}

	inline static int32_t get_offset_of_totalIn_25() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___totalIn_25)); }
	inline int64_t get_totalIn_25() const { return ___totalIn_25; }
	inline int64_t* get_address_of_totalIn_25() { return &___totalIn_25; }
	inline void set_totalIn_25(int64_t value)
	{
		___totalIn_25 = value;
	}

	inline static int32_t get_offset_of_noHeader_26() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___noHeader_26)); }
	inline bool get_noHeader_26() const { return ___noHeader_26; }
	inline bool* get_address_of_noHeader_26() { return &___noHeader_26; }
	inline void set_noHeader_26(bool value)
	{
		___noHeader_26 = value;
	}

	inline static int32_t get_offset_of_input_27() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___input_27)); }
	inline StreamManipulator_t2348681196 * get_input_27() const { return ___input_27; }
	inline StreamManipulator_t2348681196 ** get_address_of_input_27() { return &___input_27; }
	inline void set_input_27(StreamManipulator_t2348681196 * value)
	{
		___input_27 = value;
		Il2CppCodeGenWriteBarrier(&___input_27, value);
	}

	inline static int32_t get_offset_of_outputWindow_28() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___outputWindow_28)); }
	inline OutputWindow_t617095569 * get_outputWindow_28() const { return ___outputWindow_28; }
	inline OutputWindow_t617095569 ** get_address_of_outputWindow_28() { return &___outputWindow_28; }
	inline void set_outputWindow_28(OutputWindow_t617095569 * value)
	{
		___outputWindow_28 = value;
		Il2CppCodeGenWriteBarrier(&___outputWindow_28, value);
	}

	inline static int32_t get_offset_of_dynHeader_29() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___dynHeader_29)); }
	inline InflaterDynHeader_t1952417709 * get_dynHeader_29() const { return ___dynHeader_29; }
	inline InflaterDynHeader_t1952417709 ** get_address_of_dynHeader_29() { return &___dynHeader_29; }
	inline void set_dynHeader_29(InflaterDynHeader_t1952417709 * value)
	{
		___dynHeader_29 = value;
		Il2CppCodeGenWriteBarrier(&___dynHeader_29, value);
	}

	inline static int32_t get_offset_of_litlenTree_30() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___litlenTree_30)); }
	inline InflaterHuffmanTree_t1141403250 * get_litlenTree_30() const { return ___litlenTree_30; }
	inline InflaterHuffmanTree_t1141403250 ** get_address_of_litlenTree_30() { return &___litlenTree_30; }
	inline void set_litlenTree_30(InflaterHuffmanTree_t1141403250 * value)
	{
		___litlenTree_30 = value;
		Il2CppCodeGenWriteBarrier(&___litlenTree_30, value);
	}

	inline static int32_t get_offset_of_distTree_31() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___distTree_31)); }
	inline InflaterHuffmanTree_t1141403250 * get_distTree_31() const { return ___distTree_31; }
	inline InflaterHuffmanTree_t1141403250 ** get_address_of_distTree_31() { return &___distTree_31; }
	inline void set_distTree_31(InflaterHuffmanTree_t1141403250 * value)
	{
		___distTree_31 = value;
		Il2CppCodeGenWriteBarrier(&___distTree_31, value);
	}

	inline static int32_t get_offset_of_adler_32() { return static_cast<int32_t>(offsetof(Inflater_t1975778921, ___adler_32)); }
	inline Adler32_t2680377483 * get_adler_32() const { return ___adler_32; }
	inline Adler32_t2680377483 ** get_address_of_adler_32() { return &___adler_32; }
	inline void set_adler_32(Adler32_t2680377483 * value)
	{
		___adler_32 = value;
		Il2CppCodeGenWriteBarrier(&___adler_32, value);
	}
};

struct Inflater_t1975778921_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLENS
	Int32U5BU5D_t3230847821* ___CPLENS_13;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLEXT
	Int32U5BU5D_t3230847821* ___CPLEXT_14;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDIST
	Int32U5BU5D_t3230847821* ___CPDIST_15;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDEXT
	Int32U5BU5D_t3230847821* ___CPDEXT_16;

public:
	inline static int32_t get_offset_of_CPLENS_13() { return static_cast<int32_t>(offsetof(Inflater_t1975778921_StaticFields, ___CPLENS_13)); }
	inline Int32U5BU5D_t3230847821* get_CPLENS_13() const { return ___CPLENS_13; }
	inline Int32U5BU5D_t3230847821** get_address_of_CPLENS_13() { return &___CPLENS_13; }
	inline void set_CPLENS_13(Int32U5BU5D_t3230847821* value)
	{
		___CPLENS_13 = value;
		Il2CppCodeGenWriteBarrier(&___CPLENS_13, value);
	}

	inline static int32_t get_offset_of_CPLEXT_14() { return static_cast<int32_t>(offsetof(Inflater_t1975778921_StaticFields, ___CPLEXT_14)); }
	inline Int32U5BU5D_t3230847821* get_CPLEXT_14() const { return ___CPLEXT_14; }
	inline Int32U5BU5D_t3230847821** get_address_of_CPLEXT_14() { return &___CPLEXT_14; }
	inline void set_CPLEXT_14(Int32U5BU5D_t3230847821* value)
	{
		___CPLEXT_14 = value;
		Il2CppCodeGenWriteBarrier(&___CPLEXT_14, value);
	}

	inline static int32_t get_offset_of_CPDIST_15() { return static_cast<int32_t>(offsetof(Inflater_t1975778921_StaticFields, ___CPDIST_15)); }
	inline Int32U5BU5D_t3230847821* get_CPDIST_15() const { return ___CPDIST_15; }
	inline Int32U5BU5D_t3230847821** get_address_of_CPDIST_15() { return &___CPDIST_15; }
	inline void set_CPDIST_15(Int32U5BU5D_t3230847821* value)
	{
		___CPDIST_15 = value;
		Il2CppCodeGenWriteBarrier(&___CPDIST_15, value);
	}

	inline static int32_t get_offset_of_CPDEXT_16() { return static_cast<int32_t>(offsetof(Inflater_t1975778921_StaticFields, ___CPDEXT_16)); }
	inline Int32U5BU5D_t3230847821* get_CPDEXT_16() const { return ___CPDEXT_16; }
	inline Int32U5BU5D_t3230847821** get_address_of_CPDEXT_16() { return &___CPDEXT_16; }
	inline void set_CPDEXT_16(Int32U5BU5D_t3230847821* value)
	{
		___CPDEXT_16 = value;
		Il2CppCodeGenWriteBarrier(&___CPDEXT_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
