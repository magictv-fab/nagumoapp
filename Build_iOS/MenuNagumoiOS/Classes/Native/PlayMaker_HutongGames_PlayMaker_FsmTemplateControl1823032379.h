﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2
struct  U3CU3Ec__DisplayClass2_t1823032379  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2::namedVariable
	NamedVariable_t3211770239 * ___namedVariable_0;

public:
	inline static int32_t get_offset_of_namedVariable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t1823032379, ___namedVariable_0)); }
	inline NamedVariable_t3211770239 * get_namedVariable_0() const { return ___namedVariable_0; }
	inline NamedVariable_t3211770239 ** get_address_of_namedVariable_0() { return &___namedVariable_0; }
	inline void set_namedVariable_0(NamedVariable_t3211770239 * value)
	{
		___namedVariable_0 = value;
		Il2CppCodeGenWriteBarrier(&___namedVariable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
