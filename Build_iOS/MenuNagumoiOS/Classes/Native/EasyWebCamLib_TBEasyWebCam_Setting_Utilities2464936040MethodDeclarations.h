﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_ResolutionMode805264687.h"

// System.Void TBEasyWebCam.Setting.Utilities::Dimensions(TBEasyWebCam.Setting.ResolutionMode,System.Int32&,System.Int32&)
extern "C"  void Utilities_Dimensions_m3724800167 (Il2CppObject * __this /* static, unused */, uint8_t ___preset0, int32_t* ___width1, int32_t* ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
