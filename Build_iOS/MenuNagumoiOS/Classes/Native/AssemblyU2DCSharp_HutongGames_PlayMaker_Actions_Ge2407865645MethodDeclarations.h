﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetPosition
struct GetPosition_t2407865645;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetPosition::.ctor()
extern "C"  void GetPosition__ctor_m536563881 (GetPosition_t2407865645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::Reset()
extern "C"  void GetPosition_Reset_m2477964118 (GetPosition_t2407865645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::OnEnter()
extern "C"  void GetPosition_OnEnter_m3928510016 (GetPosition_t2407865645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::OnUpdate()
extern "C"  void GetPosition_OnUpdate_m658285571 (GetPosition_t2407865645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::DoGetPosition()
extern "C"  void GetPosition_DoGetPosition_m1453152219 (GetPosition_t2407865645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
