﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// PlayMakerAnimatorIKProxy
struct PlayMakerAnimatorIKProxy_t1024752181;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorBody
struct  SetAnimatorBody_t4287297465  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorBody::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetAnimatorBody::target
	FsmGameObject_t1697147867 * ___target_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetAnimatorBody::position
	FsmVector3_t533912882 * ___position_11;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetAnimatorBody::rotation
	FsmQuaternion_t3871136040 * ___rotation_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorBody::everyFrame
	bool ___everyFrame_13;
	// PlayMakerAnimatorIKProxy HutongGames.PlayMaker.Actions.SetAnimatorBody::_animatorProxy
	PlayMakerAnimatorIKProxy_t1024752181 * ____animatorProxy_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorBody::_animator
	Animator_t2776330603 * ____animator_15;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SetAnimatorBody::_transform
	Transform_t1659122786 * ____transform_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ___target_10)); }
	inline FsmGameObject_t1697147867 * get_target_10() const { return ___target_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(FsmGameObject_t1697147867 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier(&___target_10, value);
	}

	inline static int32_t get_offset_of_position_11() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ___position_11)); }
	inline FsmVector3_t533912882 * get_position_11() const { return ___position_11; }
	inline FsmVector3_t533912882 ** get_address_of_position_11() { return &___position_11; }
	inline void set_position_11(FsmVector3_t533912882 * value)
	{
		___position_11 = value;
		Il2CppCodeGenWriteBarrier(&___position_11, value);
	}

	inline static int32_t get_offset_of_rotation_12() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ___rotation_12)); }
	inline FsmQuaternion_t3871136040 * get_rotation_12() const { return ___rotation_12; }
	inline FsmQuaternion_t3871136040 ** get_address_of_rotation_12() { return &___rotation_12; }
	inline void set_rotation_12(FsmQuaternion_t3871136040 * value)
	{
		___rotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__animatorProxy_14() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ____animatorProxy_14)); }
	inline PlayMakerAnimatorIKProxy_t1024752181 * get__animatorProxy_14() const { return ____animatorProxy_14; }
	inline PlayMakerAnimatorIKProxy_t1024752181 ** get_address_of__animatorProxy_14() { return &____animatorProxy_14; }
	inline void set__animatorProxy_14(PlayMakerAnimatorIKProxy_t1024752181 * value)
	{
		____animatorProxy_14 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_14, value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ____animator_15)); }
	inline Animator_t2776330603 * get__animator_15() const { return ____animator_15; }
	inline Animator_t2776330603 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t2776330603 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier(&____animator_15, value);
	}

	inline static int32_t get_offset_of__transform_16() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t4287297465, ____transform_16)); }
	inline Transform_t1659122786 * get__transform_16() const { return ____transform_16; }
	inline Transform_t1659122786 ** get_address_of__transform_16() { return &____transform_16; }
	inline void set__transform_16(Transform_t1659122786 * value)
	{
		____transform_16 = value;
		Il2CppCodeGenWriteBarrier(&____transform_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
