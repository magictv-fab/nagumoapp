﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.ECI
struct ECI_t1296865545;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.Common.ECI::get_Value()
extern "C"  int32_t ECI_get_Value_m447345921 (ECI_t1296865545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.ECI::.ctor(System.Int32)
extern "C"  void ECI__ctor_m1284132346 (ECI_t1296865545 * __this, int32_t ___value_Renamed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
