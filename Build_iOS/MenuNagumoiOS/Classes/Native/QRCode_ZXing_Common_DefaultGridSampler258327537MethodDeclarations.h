﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DefaultGridSampler
struct DefaultGridSampler_t258327537;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Common.PerspectiveTransform
struct PerspectiveTransform_t2438931808;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_Common_PerspectiveTransform2438931808.h"

// ZXing.Common.BitMatrix ZXing.Common.DefaultGridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  BitMatrix_t1058711404 * DefaultGridSampler_sampleGrid_m2738484790 (DefaultGridSampler_t258327537 * __this, BitMatrix_t1058711404 * ___image0, int32_t ___dimensionX1, int32_t ___dimensionY2, float ___p1ToX3, float ___p1ToY4, float ___p2ToX5, float ___p2ToY6, float ___p3ToX7, float ___p3ToY8, float ___p4ToX9, float ___p4ToY10, float ___p1FromX11, float ___p1FromY12, float ___p2FromX13, float ___p2FromY14, float ___p3FromX15, float ___p3FromY16, float ___p4FromX17, float ___p4FromY18, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Common.DefaultGridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,ZXing.Common.PerspectiveTransform)
extern "C"  BitMatrix_t1058711404 * DefaultGridSampler_sampleGrid_m849764169 (DefaultGridSampler_t258327537 * __this, BitMatrix_t1058711404 * ___image0, int32_t ___dimensionX1, int32_t ___dimensionY2, PerspectiveTransform_t2438931808 * ___transform3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DefaultGridSampler::.ctor()
extern "C"  void DefaultGridSampler__ctor_m3212745681 (DefaultGridSampler_t258327537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
