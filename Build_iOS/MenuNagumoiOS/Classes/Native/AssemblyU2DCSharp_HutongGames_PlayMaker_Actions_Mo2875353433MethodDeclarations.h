﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MoveTowards
struct MoveTowards_t2875353433;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void HutongGames.PlayMaker.Actions.MoveTowards::.ctor()
extern "C"  void MoveTowards__ctor_m3606246909 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::Reset()
extern "C"  void MoveTowards_Reset_m1252679850 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::OnUpdate()
extern "C"  void MoveTowards_OnUpdate_m1141706287 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::DoMoveTowards()
extern "C"  void MoveTowards_DoMoveTowards_m2186238555 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.MoveTowards::UpdateTargetPos()
extern "C"  bool MoveTowards_UpdateTargetPos_m1445359881 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::GetTargetPos()
extern "C"  Vector3_t4282066566  MoveTowards_GetTargetPos_m23384698 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::GetTargetPosWithVertical()
extern "C"  Vector3_t4282066566  MoveTowards_GetTargetPosWithVertical_m3323879542 (MoveTowards_t2875353433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
