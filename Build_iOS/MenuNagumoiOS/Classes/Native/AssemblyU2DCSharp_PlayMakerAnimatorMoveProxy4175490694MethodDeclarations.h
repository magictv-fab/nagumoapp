﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PlayMakerAnimatorMoveProxy::.ctor()
extern "C"  void PlayMakerAnimatorMoveProxy__ctor_m273969365 (PlayMakerAnimatorMoveProxy_t4175490694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMoveProxy::add_OnAnimatorMoveEvent(System.Action)
extern "C"  void PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533 (PlayMakerAnimatorMoveProxy_t4175490694 * __this, Action_t3771233898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMoveProxy::remove_OnAnimatorMoveEvent(System.Action)
extern "C"  void PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206 (PlayMakerAnimatorMoveProxy_t4175490694 * __this, Action_t3771233898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMoveProxy::Start()
extern "C"  void PlayMakerAnimatorMoveProxy_Start_m3516074453 (PlayMakerAnimatorMoveProxy_t4175490694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMoveProxy::Update()
extern "C"  void PlayMakerAnimatorMoveProxy_Update_m1629977816 (PlayMakerAnimatorMoveProxy_t4175490694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMoveProxy::OnAnimatorMove()
extern "C"  void PlayMakerAnimatorMoveProxy_OnAnimatorMove_m296183142 (PlayMakerAnimatorMoveProxy_t4175490694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
