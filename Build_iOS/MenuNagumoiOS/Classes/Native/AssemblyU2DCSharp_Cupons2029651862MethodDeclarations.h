﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cupons
struct Cupons_t2029651862;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void Cupons::.ctor()
extern "C"  void Cupons__ctor_m1907197381 (Cupons_t2029651862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cupons::Start()
extern "C"  void Cupons_Start_m854335173 (Cupons_t2029651862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cupons::SetOfertasTabloidByCupomValue()
extern "C"  void Cupons_SetOfertasTabloidByCupomValue_m1800959970 (Cupons_t2029651862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cupons::Update()
extern "C"  void Cupons_Update_m720438760 (Cupons_t2029651862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cupons::QREncodeFinished(UnityEngine.Texture2D)
extern "C"  void Cupons_QREncodeFinished_m1164462708 (Cupons_t2029651862 * __this, Texture2D_t3884108195 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cupons::Encode()
extern "C"  void Cupons_Encode_m1109060597 (Cupons_t2029651862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
