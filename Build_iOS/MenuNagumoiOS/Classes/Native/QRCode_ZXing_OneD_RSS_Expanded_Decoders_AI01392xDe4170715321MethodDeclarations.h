﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder
struct AI01392xDecoder_t4170715321;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AI01392xDecoder__ctor_m1089832437 (AI01392xDecoder_t4170715321 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder::parseInformation()
extern "C"  String_t* AI01392xDecoder_parseInformation_m2958932488 (AI01392xDecoder_t4170715321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
