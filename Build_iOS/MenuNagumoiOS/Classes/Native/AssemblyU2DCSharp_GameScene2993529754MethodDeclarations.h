﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameScene
struct GameScene_t2993529754;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void GameScene::.ctor()
extern "C"  void GameScene__ctor_m3967999761 (GameScene_t2993529754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene::.cctor()
extern "C"  void GameScene__cctor_m2266812092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene::Start()
extern "C"  void GameScene_Start_m2915137553 (GameScene_t2993529754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene::Update()
extern "C"  void GameScene_Update_m180803100 (GameScene_t2993529754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameScene::changeScore()
extern "C"  Il2CppObject * GameScene_changeScore_m914718809 (GameScene_t2993529754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameScene::changeDifficulty()
extern "C"  Il2CppObject * GameScene_changeDifficulty_m4049615990 (GameScene_t2993529754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
