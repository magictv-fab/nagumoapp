﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E
struct U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E::.ctor()
extern "C"  void U3CWaitForRequestPingU3Ec__Iterator3E__ctor_m1708179509 (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestPingU3Ec__Iterator3E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m208270461 (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestPingU3Ec__Iterator3E_System_Collections_IEnumerator_get_Current_m4119237137 (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E::MoveNext()
extern "C"  bool U3CWaitForRequestPingU3Ec__Iterator3E_MoveNext_m2999039647 (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E::Dispose()
extern "C"  void U3CWaitForRequestPingU3Ec__Iterator3E_Dispose_m781284146 (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestPing>c__Iterator3E::Reset()
extern "C"  void U3CWaitForRequestPingU3Ec__Iterator3E_Reset_m3649579746 (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
