﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>
struct KeyCollection_t2916457164;
// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Collections.Generic.IEnumerator`1<ZXing.BarcodeFormat>
struct IEnumerator_1_t1818703570;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.BarcodeFormat[]
struct BarcodeFormatU5BU5D_t352465924;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1904633767.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1378423703_gshared (KeyCollection_t2916457164 * __this, Dictionary_2_t1289697713 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1378423703(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2916457164 *, Dictionary_2_t1289697713 *, const MethodInfo*))KeyCollection__ctor_m1378423703_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3305513823_gshared (KeyCollection_t2916457164 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3305513823(__this, ___item0, method) ((  void (*) (KeyCollection_t2916457164 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3305513823_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1357616278_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1357616278(__this, method) ((  void (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1357616278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3749742731_gshared (KeyCollection_t2916457164 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3749742731(__this, ___item0, method) ((  bool (*) (KeyCollection_t2916457164 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3749742731_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3369836720_gshared (KeyCollection_t2916457164 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3369836720(__this, ___item0, method) ((  bool (*) (KeyCollection_t2916457164 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3369836720_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4043272338_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4043272338(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4043272338_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2304067592_gshared (KeyCollection_t2916457164 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2304067592(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2916457164 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2304067592_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3830333699_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3830333699(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3830333699_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2906261804_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2906261804(__this, method) ((  bool (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2906261804_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2752600286_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2752600286(__this, method) ((  bool (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2752600286_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1476451402_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1476451402(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1476451402_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3058865740_gshared (KeyCollection_t2916457164 * __this, BarcodeFormatU5BU5D_t352465924* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3058865740(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2916457164 *, BarcodeFormatU5BU5D_t352465924*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3058865740_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1904633767  KeyCollection_GetEnumerator_m947525103_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m947525103(__this, method) ((  Enumerator_t1904633767  (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_GetEnumerator_m947525103_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m774029348_gshared (KeyCollection_t2916457164 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m774029348(__this, method) ((  int32_t (*) (KeyCollection_t2916457164 *, const MethodInfo*))KeyCollection_get_Count_m774029348_gshared)(__this, method)
