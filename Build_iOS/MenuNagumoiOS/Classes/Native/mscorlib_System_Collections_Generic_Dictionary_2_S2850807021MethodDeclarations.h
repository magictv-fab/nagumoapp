﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>
struct ShimEnumerator_t2850807021;
// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m655804152_gshared (ShimEnumerator_t2850807021 * __this, Dictionary_2_t3135028994 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m655804152(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2850807021 *, Dictionary_2_t3135028994 *, const MethodInfo*))ShimEnumerator__ctor_m655804152_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2518629869_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2518629869(__this, method) ((  bool (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))ShimEnumerator_MoveNext_m2518629869_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1096164445_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1096164445(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))ShimEnumerator_get_Entry_m1096164445_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m726253276_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m726253276(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))ShimEnumerator_get_Key_m726253276_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3193132654_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3193132654(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))ShimEnumerator_get_Value_m3193132654_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3418521974_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3418521974(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))ShimEnumerator_get_Current_m3418521974_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3887361354_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3887361354(__this, method) ((  void (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))ShimEnumerator_Reset_m3887361354_gshared)(__this, method)
