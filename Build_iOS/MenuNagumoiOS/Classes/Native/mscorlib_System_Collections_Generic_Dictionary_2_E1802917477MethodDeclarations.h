﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2279740856(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1802917477 *, Dictionary_2_t485594085 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4073118889(__this, method) ((  Il2CppObject * (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m849238013(__this, method) ((  void (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1199898630(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3669898949(__this, method) ((  Il2CppObject * (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1653176343(__this, method) ((  Il2CppObject * (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::MoveNext()
#define Enumerator_MoveNext_m4138149353(__this, method) ((  bool (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::get_Current()
#define Enumerator_get_Current_m999342631(__this, method) ((  KeyValuePair_2_t384374791  (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3168912246(__this, method) ((  String_t* (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2660752154(__this, method) ((  Il2CppObject * (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::Reset()
#define Enumerator_Reset_m253074442(__this, method) ((  void (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::VerifyState()
#define Enumerator_VerifyState_m981613715(__this, method) ((  void (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1182846779(__this, method) ((  void (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,OnOffListenerEventInterface>::Dispose()
#define Enumerator_Dispose_m914831962(__this, method) ((  void (*) (Enumerator_t1802917477 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
