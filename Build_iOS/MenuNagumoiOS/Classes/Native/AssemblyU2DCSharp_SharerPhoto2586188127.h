﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharerPhoto
struct  SharerPhoto_t2586188127  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SharerPhoto::frameLst
	List_1_t747900261 * ___frameLst_3;
	// UnityEngine.Texture2D SharerPhoto::tex
	Texture2D_t3884108195 * ___tex_4;
	// UnityEngine.UI.Image SharerPhoto::imageRefCut
	Image_t538875265 * ___imageRefCut_5;
	// System.Boolean SharerPhoto::take
	bool ___take_6;
	// UnityEngine.GameObject SharerPhoto::parabensPanel
	GameObject_t3674682005 * ___parabensPanel_7;
	// UnityEngine.GameObject SharerPhoto::loadingPanel
	GameObject_t3674682005 * ___loadingPanel_8;

public:
	inline static int32_t get_offset_of_frameLst_3() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127, ___frameLst_3)); }
	inline List_1_t747900261 * get_frameLst_3() const { return ___frameLst_3; }
	inline List_1_t747900261 ** get_address_of_frameLst_3() { return &___frameLst_3; }
	inline void set_frameLst_3(List_1_t747900261 * value)
	{
		___frameLst_3 = value;
		Il2CppCodeGenWriteBarrier(&___frameLst_3, value);
	}

	inline static int32_t get_offset_of_tex_4() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127, ___tex_4)); }
	inline Texture2D_t3884108195 * get_tex_4() const { return ___tex_4; }
	inline Texture2D_t3884108195 ** get_address_of_tex_4() { return &___tex_4; }
	inline void set_tex_4(Texture2D_t3884108195 * value)
	{
		___tex_4 = value;
		Il2CppCodeGenWriteBarrier(&___tex_4, value);
	}

	inline static int32_t get_offset_of_imageRefCut_5() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127, ___imageRefCut_5)); }
	inline Image_t538875265 * get_imageRefCut_5() const { return ___imageRefCut_5; }
	inline Image_t538875265 ** get_address_of_imageRefCut_5() { return &___imageRefCut_5; }
	inline void set_imageRefCut_5(Image_t538875265 * value)
	{
		___imageRefCut_5 = value;
		Il2CppCodeGenWriteBarrier(&___imageRefCut_5, value);
	}

	inline static int32_t get_offset_of_take_6() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127, ___take_6)); }
	inline bool get_take_6() const { return ___take_6; }
	inline bool* get_address_of_take_6() { return &___take_6; }
	inline void set_take_6(bool value)
	{
		___take_6 = value;
	}

	inline static int32_t get_offset_of_parabensPanel_7() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127, ___parabensPanel_7)); }
	inline GameObject_t3674682005 * get_parabensPanel_7() const { return ___parabensPanel_7; }
	inline GameObject_t3674682005 ** get_address_of_parabensPanel_7() { return &___parabensPanel_7; }
	inline void set_parabensPanel_7(GameObject_t3674682005 * value)
	{
		___parabensPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___parabensPanel_7, value);
	}

	inline static int32_t get_offset_of_loadingPanel_8() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127, ___loadingPanel_8)); }
	inline GameObject_t3674682005 * get_loadingPanel_8() const { return ___loadingPanel_8; }
	inline GameObject_t3674682005 ** get_address_of_loadingPanel_8() { return &___loadingPanel_8; }
	inline void set_loadingPanel_8(GameObject_t3674682005 * value)
	{
		___loadingPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___loadingPanel_8, value);
	}
};

struct SharerPhoto_t2586188127_StaticFields
{
public:
	// System.Int32 SharerPhoto::awardIndex
	int32_t ___awardIndex_2;

public:
	inline static int32_t get_offset_of_awardIndex_2() { return static_cast<int32_t>(offsetof(SharerPhoto_t2586188127_StaticFields, ___awardIndex_2)); }
	inline int32_t get_awardIndex_2() const { return ___awardIndex_2; }
	inline int32_t* get_address_of_awardIndex_2() { return &___awardIndex_2; }
	inline void set_awardIndex_2(int32_t value)
	{
		___awardIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
