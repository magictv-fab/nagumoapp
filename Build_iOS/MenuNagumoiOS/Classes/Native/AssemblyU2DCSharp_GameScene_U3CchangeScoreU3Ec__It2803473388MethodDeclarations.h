﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameScene/<changeScore>c__Iterator42
struct U3CchangeScoreU3Ec__Iterator42_t2803473388;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameScene/<changeScore>c__Iterator42::.ctor()
extern "C"  void U3CchangeScoreU3Ec__Iterator42__ctor_m3580508207 (U3CchangeScoreU3Ec__Iterator42_t2803473388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameScene/<changeScore>c__Iterator42::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CchangeScoreU3Ec__Iterator42_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3256819341 (U3CchangeScoreU3Ec__Iterator42_t2803473388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameScene/<changeScore>c__Iterator42::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CchangeScoreU3Ec__Iterator42_System_Collections_IEnumerator_get_Current_m3161578529 (U3CchangeScoreU3Ec__Iterator42_t2803473388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameScene/<changeScore>c__Iterator42::MoveNext()
extern "C"  bool U3CchangeScoreU3Ec__Iterator42_MoveNext_m7278925 (U3CchangeScoreU3Ec__Iterator42_t2803473388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene/<changeScore>c__Iterator42::Dispose()
extern "C"  void U3CchangeScoreU3Ec__Iterator42_Dispose_m497865900 (U3CchangeScoreU3Ec__Iterator42_t2803473388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene/<changeScore>c__Iterator42::Reset()
extern "C"  void U3CchangeScoreU3Ec__Iterator42_Reset_m1226941148 (U3CchangeScoreU3Ec__Iterator42_t2803473388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
