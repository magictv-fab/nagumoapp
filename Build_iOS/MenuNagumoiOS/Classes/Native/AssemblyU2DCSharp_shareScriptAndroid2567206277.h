﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// shareScriptAndroid
struct  shareScriptAndroid_t2567206277  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject shareScriptAndroid::CanvasShareObj
	GameObject_t3674682005 * ___CanvasShareObj_2;
	// System.Boolean shareScriptAndroid::isProcessing
	bool ___isProcessing_3;
	// System.Boolean shareScriptAndroid::isFocus
	bool ___isFocus_4;

public:
	inline static int32_t get_offset_of_CanvasShareObj_2() { return static_cast<int32_t>(offsetof(shareScriptAndroid_t2567206277, ___CanvasShareObj_2)); }
	inline GameObject_t3674682005 * get_CanvasShareObj_2() const { return ___CanvasShareObj_2; }
	inline GameObject_t3674682005 ** get_address_of_CanvasShareObj_2() { return &___CanvasShareObj_2; }
	inline void set_CanvasShareObj_2(GameObject_t3674682005 * value)
	{
		___CanvasShareObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasShareObj_2, value);
	}

	inline static int32_t get_offset_of_isProcessing_3() { return static_cast<int32_t>(offsetof(shareScriptAndroid_t2567206277, ___isProcessing_3)); }
	inline bool get_isProcessing_3() const { return ___isProcessing_3; }
	inline bool* get_address_of_isProcessing_3() { return &___isProcessing_3; }
	inline void set_isProcessing_3(bool value)
	{
		___isProcessing_3 = value;
	}

	inline static int32_t get_offset_of_isFocus_4() { return static_cast<int32_t>(offsetof(shareScriptAndroid_t2567206277, ___isFocus_4)); }
	inline bool get_isFocus_4() const { return ___isFocus_4; }
	inline bool* get_address_of_isFocus_4() { return &___isFocus_4; }
	inline void set_isFocus_4(bool value)
	{
		___isFocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
