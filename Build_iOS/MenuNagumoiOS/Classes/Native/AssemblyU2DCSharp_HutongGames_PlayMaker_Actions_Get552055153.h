﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetButtonUp
struct  GetButtonUp_t552055153  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetButtonUp::buttonName
	FsmString_t952858651 * ___buttonName_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetButtonUp::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetButtonUp::storeResult
	FsmBool_t1075959796 * ___storeResult_11;

public:
	inline static int32_t get_offset_of_buttonName_9() { return static_cast<int32_t>(offsetof(GetButtonUp_t552055153, ___buttonName_9)); }
	inline FsmString_t952858651 * get_buttonName_9() const { return ___buttonName_9; }
	inline FsmString_t952858651 ** get_address_of_buttonName_9() { return &___buttonName_9; }
	inline void set_buttonName_9(FsmString_t952858651 * value)
	{
		___buttonName_9 = value;
		Il2CppCodeGenWriteBarrier(&___buttonName_9, value);
	}

	inline static int32_t get_offset_of_sendEvent_10() { return static_cast<int32_t>(offsetof(GetButtonUp_t552055153, ___sendEvent_10)); }
	inline FsmEvent_t2133468028 * get_sendEvent_10() const { return ___sendEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_10() { return &___sendEvent_10; }
	inline void set_sendEvent_10(FsmEvent_t2133468028 * value)
	{
		___sendEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(GetButtonUp_t552055153, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
