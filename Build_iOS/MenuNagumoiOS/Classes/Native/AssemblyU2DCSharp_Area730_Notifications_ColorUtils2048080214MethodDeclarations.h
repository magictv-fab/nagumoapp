﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.ColorUtils
struct ColorUtils_t2048080214;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void Area730.Notifications.ColorUtils::.ctor()
extern "C"  void ColorUtils__ctor_m4505056 (ColorUtils_t2048080214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.ColorUtils::GetHex(System.Int32)
extern "C"  String_t* ColorUtils_GetHex_m852195069 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.ColorUtils::ToHtmlStringRGB(UnityEngine.Color)
extern "C"  String_t* ColorUtils_ToHtmlStringRGB_m2779037493 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
