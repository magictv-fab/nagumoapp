﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2109337660(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1654383373 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO>::Invoke(T)
#define Func_2_Invoke_m3448632598(__this, ___arg10, method) ((  MetadataVO_t2511256998 * (*) (Func_2_t1654383373 *, MetadataVO_t2511256998 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2583364421(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1654383373 *, MetadataVO_t2511256998 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<MagicTV.vo.MetadataVO,MagicTV.vo.MetadataVO>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3867113598(__this, ___result0, method) ((  MetadataVO_t2511256998 * (*) (Func_2_t1654383373 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
