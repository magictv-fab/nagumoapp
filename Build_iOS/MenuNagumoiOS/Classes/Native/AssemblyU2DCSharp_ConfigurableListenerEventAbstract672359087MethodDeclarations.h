﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfigurableListenerEventAbstract
struct ConfigurableListenerEventAbstract_t672359087;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// System.Void ConfigurableListenerEventAbstract::.ctor()
extern "C"  void ConfigurableListenerEventAbstract__ctor_m4224454556 (ConfigurableListenerEventAbstract_t672359087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigurableListenerEventAbstract::getUniqueName()
extern "C"  String_t* ConfigurableListenerEventAbstract_getUniqueName_m360279215 (ConfigurableListenerEventAbstract_t672359087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ConfigurableListenerEventAbstract::getGameObjectReference()
extern "C"  GameObject_t3674682005 * ConfigurableListenerEventAbstract_getGameObjectReference_m2202010295 (ConfigurableListenerEventAbstract_t672359087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
