﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MousePick
struct  MousePick_t696801716  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MousePick::rayDistance
	FsmFloat_t2134102846 * ___rayDistance_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick::storeDidPickObject
	FsmBool_t1075959796 * ___storeDidPickObject_10;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.MousePick::storeGameObject
	FsmGameObject_t1697147867 * ___storeGameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MousePick::storePoint
	FsmVector3_t533912882 * ___storePoint_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MousePick::storeNormal
	FsmVector3_t533912882 * ___storeNormal_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MousePick::storeDistance
	FsmFloat_t2134102846 * ___storeDistance_14;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.MousePick::layerMask
	FsmIntU5BU5D_t1976821196* ___layerMask_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick::invertMask
	FsmBool_t1075959796 * ___invertMask_16;
	// System.Boolean HutongGames.PlayMaker.Actions.MousePick::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_rayDistance_9() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___rayDistance_9)); }
	inline FsmFloat_t2134102846 * get_rayDistance_9() const { return ___rayDistance_9; }
	inline FsmFloat_t2134102846 ** get_address_of_rayDistance_9() { return &___rayDistance_9; }
	inline void set_rayDistance_9(FsmFloat_t2134102846 * value)
	{
		___rayDistance_9 = value;
		Il2CppCodeGenWriteBarrier(&___rayDistance_9, value);
	}

	inline static int32_t get_offset_of_storeDidPickObject_10() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___storeDidPickObject_10)); }
	inline FsmBool_t1075959796 * get_storeDidPickObject_10() const { return ___storeDidPickObject_10; }
	inline FsmBool_t1075959796 ** get_address_of_storeDidPickObject_10() { return &___storeDidPickObject_10; }
	inline void set_storeDidPickObject_10(FsmBool_t1075959796 * value)
	{
		___storeDidPickObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeDidPickObject_10, value);
	}

	inline static int32_t get_offset_of_storeGameObject_11() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___storeGameObject_11)); }
	inline FsmGameObject_t1697147867 * get_storeGameObject_11() const { return ___storeGameObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeGameObject_11() { return &___storeGameObject_11; }
	inline void set_storeGameObject_11(FsmGameObject_t1697147867 * value)
	{
		___storeGameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeGameObject_11, value);
	}

	inline static int32_t get_offset_of_storePoint_12() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___storePoint_12)); }
	inline FsmVector3_t533912882 * get_storePoint_12() const { return ___storePoint_12; }
	inline FsmVector3_t533912882 ** get_address_of_storePoint_12() { return &___storePoint_12; }
	inline void set_storePoint_12(FsmVector3_t533912882 * value)
	{
		___storePoint_12 = value;
		Il2CppCodeGenWriteBarrier(&___storePoint_12, value);
	}

	inline static int32_t get_offset_of_storeNormal_13() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___storeNormal_13)); }
	inline FsmVector3_t533912882 * get_storeNormal_13() const { return ___storeNormal_13; }
	inline FsmVector3_t533912882 ** get_address_of_storeNormal_13() { return &___storeNormal_13; }
	inline void set_storeNormal_13(FsmVector3_t533912882 * value)
	{
		___storeNormal_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeNormal_13, value);
	}

	inline static int32_t get_offset_of_storeDistance_14() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___storeDistance_14)); }
	inline FsmFloat_t2134102846 * get_storeDistance_14() const { return ___storeDistance_14; }
	inline FsmFloat_t2134102846 ** get_address_of_storeDistance_14() { return &___storeDistance_14; }
	inline void set_storeDistance_14(FsmFloat_t2134102846 * value)
	{
		___storeDistance_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeDistance_14, value);
	}

	inline static int32_t get_offset_of_layerMask_15() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___layerMask_15)); }
	inline FsmIntU5BU5D_t1976821196* get_layerMask_15() const { return ___layerMask_15; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_layerMask_15() { return &___layerMask_15; }
	inline void set_layerMask_15(FsmIntU5BU5D_t1976821196* value)
	{
		___layerMask_15 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_15, value);
	}

	inline static int32_t get_offset_of_invertMask_16() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___invertMask_16)); }
	inline FsmBool_t1075959796 * get_invertMask_16() const { return ___invertMask_16; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_16() { return &___invertMask_16; }
	inline void set_invertMask_16(FsmBool_t1075959796 * value)
	{
		___invertMask_16 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(MousePick_t696801716, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
