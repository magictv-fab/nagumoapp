﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Version/ECBlocks
struct ECBlocks_t3710297555;
// ZXing.Datamatrix.Internal.Version/ECB
struct ECB_t2948426741;
// ZXing.Datamatrix.Internal.Version/ECB[]
struct ECBU5BU5D_t1855874200;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version_ECB2948426741.h"

// System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB)
extern "C"  void ECBlocks__ctor_m2299007984 (ECBlocks_t3710297555 * __this, int32_t ___ecCodewords0, ECB_t2948426741 * ___ecBlocks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB,ZXing.Datamatrix.Internal.Version/ECB)
extern "C"  void ECBlocks__ctor_m2839320711 (ECBlocks_t3710297555 * __this, int32_t ___ecCodewords0, ECB_t2948426741 * ___ecBlocks11, ECB_t2948426741 * ___ecBlocks22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECCodewords()
extern "C"  int32_t ECBlocks_get_ECCodewords_m3754913901 (ECBlocks_t3710297555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECBlocksValue()
extern "C"  ECBU5BU5D_t1855874200* ECBlocks_get_ECBlocksValue_m501753614 (ECBlocks_t3710297555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
