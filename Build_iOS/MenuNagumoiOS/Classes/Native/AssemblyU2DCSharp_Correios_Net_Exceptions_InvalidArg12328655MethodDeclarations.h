﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Correios.Net.Exceptions.InvalidArgumentException
struct InvalidArgumentException_t12328655;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Correios.Net.Exceptions.InvalidArgumentException::.ctor(System.String)
extern "C"  void InvalidArgumentException__ctor_m2841408961 (InvalidArgumentException_t12328655 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
