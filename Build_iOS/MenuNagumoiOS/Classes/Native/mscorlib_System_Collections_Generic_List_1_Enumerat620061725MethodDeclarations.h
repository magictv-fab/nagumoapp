﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2865401178(__this, ___l0, method) ((  void (*) (Enumerator_t620061725 *, List_1_t600388955 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m604247736(__this, method) ((  void (*) (Enumerator_t620061725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2109223972(__this, method) ((  Il2CppObject * (*) (Enumerator_t620061725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::Dispose()
#define Enumerator_Dispose_m3803484607(__this, method) ((  void (*) (Enumerator_t620061725 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::VerifyState()
#define Enumerator_VerifyState_m3619379576(__this, method) ((  void (*) (Enumerator_t620061725 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::MoveNext()
#define Enumerator_MoveNext_m585176356(__this, method) ((  bool (*) (Enumerator_t620061725 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.OneD.UPCEANReader>::get_Current()
#define Enumerator_get_Current_m525549231(__this, method) ((  UPCEANReader_t3527170699 * (*) (Enumerator_t620061725 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
