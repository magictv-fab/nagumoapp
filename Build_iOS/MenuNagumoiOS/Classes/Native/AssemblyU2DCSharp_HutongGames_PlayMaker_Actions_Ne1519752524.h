﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties
struct  NetworkGetOnFailedToConnectProperties_t1519752524  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::errorLabel
	FsmString_t952858651 * ___errorLabel_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NoErrorEvent
	FsmEvent_t2133468028 * ___NoErrorEvent_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::RSAPublicKeyMismatchEvent
	FsmEvent_t2133468028 * ___RSAPublicKeyMismatchEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::InvalidPasswordEvent
	FsmEvent_t2133468028 * ___InvalidPasswordEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::ConnectionFailedEvent
	FsmEvent_t2133468028 * ___ConnectionFailedEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::TooManyConnectedPlayersEvent
	FsmEvent_t2133468028 * ___TooManyConnectedPlayersEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::ConnectionBannedEvent
	FsmEvent_t2133468028 * ___ConnectionBannedEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::AlreadyConnectedToServerEvent
	FsmEvent_t2133468028 * ___AlreadyConnectedToServerEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::AlreadyConnectedToAnotherServerEvent
	FsmEvent_t2133468028 * ___AlreadyConnectedToAnotherServerEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::CreateSocketOrThreadFailureEvent
	FsmEvent_t2133468028 * ___CreateSocketOrThreadFailureEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::IncorrectParametersEvent
	FsmEvent_t2133468028 * ___IncorrectParametersEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::EmptyConnectTargetEvent
	FsmEvent_t2133468028 * ___EmptyConnectTargetEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::InternalDirectConnectFailedEvent
	FsmEvent_t2133468028 * ___InternalDirectConnectFailedEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NATTargetNotConnectedEvent
	FsmEvent_t2133468028 * ___NATTargetNotConnectedEvent_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NATTargetConnectionLostEvent
	FsmEvent_t2133468028 * ___NATTargetConnectionLostEvent_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NATPunchthroughFailedEvent
	FsmEvent_t2133468028 * ___NATPunchthroughFailedEvent_24;

public:
	inline static int32_t get_offset_of_errorLabel_9() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___errorLabel_9)); }
	inline FsmString_t952858651 * get_errorLabel_9() const { return ___errorLabel_9; }
	inline FsmString_t952858651 ** get_address_of_errorLabel_9() { return &___errorLabel_9; }
	inline void set_errorLabel_9(FsmString_t952858651 * value)
	{
		___errorLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___errorLabel_9, value);
	}

	inline static int32_t get_offset_of_NoErrorEvent_10() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NoErrorEvent_10)); }
	inline FsmEvent_t2133468028 * get_NoErrorEvent_10() const { return ___NoErrorEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_NoErrorEvent_10() { return &___NoErrorEvent_10; }
	inline void set_NoErrorEvent_10(FsmEvent_t2133468028 * value)
	{
		___NoErrorEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___NoErrorEvent_10, value);
	}

	inline static int32_t get_offset_of_RSAPublicKeyMismatchEvent_11() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___RSAPublicKeyMismatchEvent_11)); }
	inline FsmEvent_t2133468028 * get_RSAPublicKeyMismatchEvent_11() const { return ___RSAPublicKeyMismatchEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_RSAPublicKeyMismatchEvent_11() { return &___RSAPublicKeyMismatchEvent_11; }
	inline void set_RSAPublicKeyMismatchEvent_11(FsmEvent_t2133468028 * value)
	{
		___RSAPublicKeyMismatchEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___RSAPublicKeyMismatchEvent_11, value);
	}

	inline static int32_t get_offset_of_InvalidPasswordEvent_12() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___InvalidPasswordEvent_12)); }
	inline FsmEvent_t2133468028 * get_InvalidPasswordEvent_12() const { return ___InvalidPasswordEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_InvalidPasswordEvent_12() { return &___InvalidPasswordEvent_12; }
	inline void set_InvalidPasswordEvent_12(FsmEvent_t2133468028 * value)
	{
		___InvalidPasswordEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidPasswordEvent_12, value);
	}

	inline static int32_t get_offset_of_ConnectionFailedEvent_13() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___ConnectionFailedEvent_13)); }
	inline FsmEvent_t2133468028 * get_ConnectionFailedEvent_13() const { return ___ConnectionFailedEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_ConnectionFailedEvent_13() { return &___ConnectionFailedEvent_13; }
	inline void set_ConnectionFailedEvent_13(FsmEvent_t2133468028 * value)
	{
		___ConnectionFailedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionFailedEvent_13, value);
	}

	inline static int32_t get_offset_of_TooManyConnectedPlayersEvent_14() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___TooManyConnectedPlayersEvent_14)); }
	inline FsmEvent_t2133468028 * get_TooManyConnectedPlayersEvent_14() const { return ___TooManyConnectedPlayersEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_TooManyConnectedPlayersEvent_14() { return &___TooManyConnectedPlayersEvent_14; }
	inline void set_TooManyConnectedPlayersEvent_14(FsmEvent_t2133468028 * value)
	{
		___TooManyConnectedPlayersEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___TooManyConnectedPlayersEvent_14, value);
	}

	inline static int32_t get_offset_of_ConnectionBannedEvent_15() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___ConnectionBannedEvent_15)); }
	inline FsmEvent_t2133468028 * get_ConnectionBannedEvent_15() const { return ___ConnectionBannedEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_ConnectionBannedEvent_15() { return &___ConnectionBannedEvent_15; }
	inline void set_ConnectionBannedEvent_15(FsmEvent_t2133468028 * value)
	{
		___ConnectionBannedEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionBannedEvent_15, value);
	}

	inline static int32_t get_offset_of_AlreadyConnectedToServerEvent_16() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___AlreadyConnectedToServerEvent_16)); }
	inline FsmEvent_t2133468028 * get_AlreadyConnectedToServerEvent_16() const { return ___AlreadyConnectedToServerEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_AlreadyConnectedToServerEvent_16() { return &___AlreadyConnectedToServerEvent_16; }
	inline void set_AlreadyConnectedToServerEvent_16(FsmEvent_t2133468028 * value)
	{
		___AlreadyConnectedToServerEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___AlreadyConnectedToServerEvent_16, value);
	}

	inline static int32_t get_offset_of_AlreadyConnectedToAnotherServerEvent_17() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___AlreadyConnectedToAnotherServerEvent_17)); }
	inline FsmEvent_t2133468028 * get_AlreadyConnectedToAnotherServerEvent_17() const { return ___AlreadyConnectedToAnotherServerEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_AlreadyConnectedToAnotherServerEvent_17() { return &___AlreadyConnectedToAnotherServerEvent_17; }
	inline void set_AlreadyConnectedToAnotherServerEvent_17(FsmEvent_t2133468028 * value)
	{
		___AlreadyConnectedToAnotherServerEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___AlreadyConnectedToAnotherServerEvent_17, value);
	}

	inline static int32_t get_offset_of_CreateSocketOrThreadFailureEvent_18() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___CreateSocketOrThreadFailureEvent_18)); }
	inline FsmEvent_t2133468028 * get_CreateSocketOrThreadFailureEvent_18() const { return ___CreateSocketOrThreadFailureEvent_18; }
	inline FsmEvent_t2133468028 ** get_address_of_CreateSocketOrThreadFailureEvent_18() { return &___CreateSocketOrThreadFailureEvent_18; }
	inline void set_CreateSocketOrThreadFailureEvent_18(FsmEvent_t2133468028 * value)
	{
		___CreateSocketOrThreadFailureEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___CreateSocketOrThreadFailureEvent_18, value);
	}

	inline static int32_t get_offset_of_IncorrectParametersEvent_19() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___IncorrectParametersEvent_19)); }
	inline FsmEvent_t2133468028 * get_IncorrectParametersEvent_19() const { return ___IncorrectParametersEvent_19; }
	inline FsmEvent_t2133468028 ** get_address_of_IncorrectParametersEvent_19() { return &___IncorrectParametersEvent_19; }
	inline void set_IncorrectParametersEvent_19(FsmEvent_t2133468028 * value)
	{
		___IncorrectParametersEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___IncorrectParametersEvent_19, value);
	}

	inline static int32_t get_offset_of_EmptyConnectTargetEvent_20() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___EmptyConnectTargetEvent_20)); }
	inline FsmEvent_t2133468028 * get_EmptyConnectTargetEvent_20() const { return ___EmptyConnectTargetEvent_20; }
	inline FsmEvent_t2133468028 ** get_address_of_EmptyConnectTargetEvent_20() { return &___EmptyConnectTargetEvent_20; }
	inline void set_EmptyConnectTargetEvent_20(FsmEvent_t2133468028 * value)
	{
		___EmptyConnectTargetEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyConnectTargetEvent_20, value);
	}

	inline static int32_t get_offset_of_InternalDirectConnectFailedEvent_21() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___InternalDirectConnectFailedEvent_21)); }
	inline FsmEvent_t2133468028 * get_InternalDirectConnectFailedEvent_21() const { return ___InternalDirectConnectFailedEvent_21; }
	inline FsmEvent_t2133468028 ** get_address_of_InternalDirectConnectFailedEvent_21() { return &___InternalDirectConnectFailedEvent_21; }
	inline void set_InternalDirectConnectFailedEvent_21(FsmEvent_t2133468028 * value)
	{
		___InternalDirectConnectFailedEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___InternalDirectConnectFailedEvent_21, value);
	}

	inline static int32_t get_offset_of_NATTargetNotConnectedEvent_22() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NATTargetNotConnectedEvent_22)); }
	inline FsmEvent_t2133468028 * get_NATTargetNotConnectedEvent_22() const { return ___NATTargetNotConnectedEvent_22; }
	inline FsmEvent_t2133468028 ** get_address_of_NATTargetNotConnectedEvent_22() { return &___NATTargetNotConnectedEvent_22; }
	inline void set_NATTargetNotConnectedEvent_22(FsmEvent_t2133468028 * value)
	{
		___NATTargetNotConnectedEvent_22 = value;
		Il2CppCodeGenWriteBarrier(&___NATTargetNotConnectedEvent_22, value);
	}

	inline static int32_t get_offset_of_NATTargetConnectionLostEvent_23() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NATTargetConnectionLostEvent_23)); }
	inline FsmEvent_t2133468028 * get_NATTargetConnectionLostEvent_23() const { return ___NATTargetConnectionLostEvent_23; }
	inline FsmEvent_t2133468028 ** get_address_of_NATTargetConnectionLostEvent_23() { return &___NATTargetConnectionLostEvent_23; }
	inline void set_NATTargetConnectionLostEvent_23(FsmEvent_t2133468028 * value)
	{
		___NATTargetConnectionLostEvent_23 = value;
		Il2CppCodeGenWriteBarrier(&___NATTargetConnectionLostEvent_23, value);
	}

	inline static int32_t get_offset_of_NATPunchthroughFailedEvent_24() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NATPunchthroughFailedEvent_24)); }
	inline FsmEvent_t2133468028 * get_NATPunchthroughFailedEvent_24() const { return ___NATPunchthroughFailedEvent_24; }
	inline FsmEvent_t2133468028 ** get_address_of_NATPunchthroughFailedEvent_24() { return &___NATPunchthroughFailedEvent_24; }
	inline void set_NATPunchthroughFailedEvent_24(FsmEvent_t2133468028 * value)
	{
		___NATPunchthroughFailedEvent_24 = value;
		Il2CppCodeGenWriteBarrier(&___NATPunchthroughFailedEvent_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
