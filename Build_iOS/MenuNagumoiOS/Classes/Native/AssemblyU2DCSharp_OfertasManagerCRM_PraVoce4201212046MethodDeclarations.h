﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_PraVoce
struct OfertasManagerCRM_PraVoce_t4201212046;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertasManagerCRM_PraVoce::.ctor()
extern "C"  void OfertasManagerCRM_PraVoce__ctor_m517774493 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::.cctor()
extern "C"  void OfertasManagerCRM_PraVoce__cctor_m2684011184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::Start()
extern "C"  void OfertasManagerCRM_PraVoce_Start_m3759879581 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::ShowNumberSelected(System.Int32)
extern "C"  void OfertasManagerCRM_PraVoce_ShowNumberSelected_m2670308601 (OfertasManagerCRM_PraVoce_t4201212046 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::InstantiateNextItem()
extern "C"  void OfertasManagerCRM_PraVoce_InstantiateNextItem_m235843051 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::SendStatus(System.Int32)
extern "C"  void OfertasManagerCRM_PraVoce_SendStatus_m1327038322 (OfertasManagerCRM_PraVoce_t4201212046 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::BtnAccept()
extern "C"  void OfertasManagerCRM_PraVoce_BtnAccept_m4113788479 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::BtnRecuse()
extern "C"  void OfertasManagerCRM_PraVoce_BtnRecuse_m1171684462 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_PraVoce::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * OfertasManagerCRM_PraVoce_ISendStatus_m2134063163 (OfertasManagerCRM_PraVoce_t4201212046 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_PraVoce::ILoadOfertas()
extern "C"  Il2CppObject * OfertasManagerCRM_PraVoce_ILoadOfertas_m784858482 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::Update()
extern "C"  void OfertasManagerCRM_PraVoce_Update_m598002192 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::LoadMoreItens()
extern "C"  void OfertasManagerCRM_PraVoce_LoadMoreItens_m2587880959 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM_PraVoce::GetDateString(System.String)
extern "C"  String_t* OfertasManagerCRM_PraVoce_GetDateString_m2465434543 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM_PraVoce::GetDateTimeString(System.String)
extern "C"  String_t* OfertasManagerCRM_PraVoce_GetDateTimeString_m3036545890 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_PraVoce::ILoadImgs()
extern "C"  Il2CppObject * OfertasManagerCRM_PraVoce_ILoadImgs_m3697087458 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_PraVoce::LoadLoadeds()
extern "C"  Il2CppObject * OfertasManagerCRM_PraVoce_LoadLoadeds_m3473205195 (OfertasManagerCRM_PraVoce_t4201212046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce::PopUp(System.String)
extern "C"  void OfertasManagerCRM_PraVoce_PopUp_m3384174299 (OfertasManagerCRM_PraVoce_t4201212046 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
