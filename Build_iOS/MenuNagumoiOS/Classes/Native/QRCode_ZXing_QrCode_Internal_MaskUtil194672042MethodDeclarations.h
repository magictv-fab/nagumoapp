﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t2421305976;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_ByteMatrix2072255685.h"

// System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule1(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  int32_t MaskUtil_applyMaskPenaltyRule1_m1879493156 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule2(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  int32_t MaskUtil_applyMaskPenaltyRule2_m3604974053 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule3(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  int32_t MaskUtil_applyMaskPenaltyRule3_m1035487654 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.MaskUtil::isWhiteHorizontal(System.Byte[],System.Int32,System.Int32)
extern "C"  bool MaskUtil_isWhiteHorizontal_m4088658250 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___rowArray0, int32_t ___from1, int32_t ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.MaskUtil::isWhiteVertical(System.Byte[][],System.Int32,System.Int32,System.Int32)
extern "C"  bool MaskUtil_isWhiteVertical_m3907738945 (Il2CppObject * __this /* static, unused */, ByteU5BU5DU5BU5D_t2421305976* ___array0, int32_t ___col1, int32_t ___from2, int32_t ___to3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule4(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  int32_t MaskUtil_applyMaskPenaltyRule4_m2760968551 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.MaskUtil::getDataMaskBit(System.Int32,System.Int32,System.Int32)
extern "C"  bool MaskUtil_getDataMaskBit_m499937336 (Il2CppObject * __this /* static, unused */, int32_t ___maskPattern0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule1Internal(ZXing.QrCode.Internal.ByteMatrix,System.Boolean)
extern "C"  int32_t MaskUtil_applyMaskPenaltyRule1Internal_m2620019356 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, bool ___isHorizontal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
