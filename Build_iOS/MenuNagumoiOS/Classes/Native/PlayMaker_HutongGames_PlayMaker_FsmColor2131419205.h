﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmColor
struct  FsmColor_t2131419205  : public NamedVariable_t3211770239
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmColor::value
	Color_t4194546905  ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmColor_t2131419205, ___value_5)); }
	inline Color_t4194546905  get_value_5() const { return ___value_5; }
	inline Color_t4194546905 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Color_t4194546905  value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
