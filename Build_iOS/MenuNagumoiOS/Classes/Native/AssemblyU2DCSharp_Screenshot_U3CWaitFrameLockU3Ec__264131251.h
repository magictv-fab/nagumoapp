﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTVWindowInAppGUIControllerScript
struct MagicTVWindowInAppGUIControllerScript_t4145218373;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Screenshot/<WaitFrameLock>c__Iterator2B
struct  U3CWaitFrameLockU3Ec__Iterator2B_t264131251  : public Il2CppObject
{
public:
	// MagicTVWindowInAppGUIControllerScript Screenshot/<WaitFrameLock>c__Iterator2B::<mtvwiagcs>__0
	MagicTVWindowInAppGUIControllerScript_t4145218373 * ___U3CmtvwiagcsU3E__0_0;
	// System.Int32 Screenshot/<WaitFrameLock>c__Iterator2B::$PC
	int32_t ___U24PC_1;
	// System.Object Screenshot/<WaitFrameLock>c__Iterator2B::$current
	Il2CppObject * ___U24current_2;

public:
	inline static int32_t get_offset_of_U3CmtvwiagcsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWaitFrameLockU3Ec__Iterator2B_t264131251, ___U3CmtvwiagcsU3E__0_0)); }
	inline MagicTVWindowInAppGUIControllerScript_t4145218373 * get_U3CmtvwiagcsU3E__0_0() const { return ___U3CmtvwiagcsU3E__0_0; }
	inline MagicTVWindowInAppGUIControllerScript_t4145218373 ** get_address_of_U3CmtvwiagcsU3E__0_0() { return &___U3CmtvwiagcsU3E__0_0; }
	inline void set_U3CmtvwiagcsU3E__0_0(MagicTVWindowInAppGUIControllerScript_t4145218373 * value)
	{
		___U3CmtvwiagcsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmtvwiagcsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CWaitFrameLockU3Ec__Iterator2B_t264131251, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitFrameLockU3Ec__Iterator2B_t264131251, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
