﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowHelpControllerScript
struct  MagicTVWindowHelpControllerScript_t126279975  : public GenericWindowControllerScript_t248075822
{
public:
	// System.Action MagicTVWindowHelpControllerScript::onClickPrint
	Action_t3771233898 * ___onClickPrint_4;
	// System.Action MagicTVWindowHelpControllerScript::onClickYouTubeVideo
	Action_t3771233898 * ___onClickYouTubeVideo_5;

public:
	inline static int32_t get_offset_of_onClickPrint_4() { return static_cast<int32_t>(offsetof(MagicTVWindowHelpControllerScript_t126279975, ___onClickPrint_4)); }
	inline Action_t3771233898 * get_onClickPrint_4() const { return ___onClickPrint_4; }
	inline Action_t3771233898 ** get_address_of_onClickPrint_4() { return &___onClickPrint_4; }
	inline void set_onClickPrint_4(Action_t3771233898 * value)
	{
		___onClickPrint_4 = value;
		Il2CppCodeGenWriteBarrier(&___onClickPrint_4, value);
	}

	inline static int32_t get_offset_of_onClickYouTubeVideo_5() { return static_cast<int32_t>(offsetof(MagicTVWindowHelpControllerScript_t126279975, ___onClickYouTubeVideo_5)); }
	inline Action_t3771233898 * get_onClickYouTubeVideo_5() const { return ___onClickYouTubeVideo_5; }
	inline Action_t3771233898 ** get_address_of_onClickYouTubeVideo_5() { return &___onClickYouTubeVideo_5; }
	inline void set_onClickYouTubeVideo_5(Action_t3771233898 * value)
	{
		___onClickYouTubeVideo_5 = value;
		Il2CppCodeGenWriteBarrier(&___onClickYouTubeVideo_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
