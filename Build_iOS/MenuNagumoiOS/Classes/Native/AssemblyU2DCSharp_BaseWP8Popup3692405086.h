﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher4168900583.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseWP8Popup
struct  BaseWP8Popup_t3692405086  : public EventDispatcher_t4168900583
{
public:
	// System.String BaseWP8Popup::title
	String_t* ___title_4;
	// System.String BaseWP8Popup::message
	String_t* ___message_5;

public:
	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(BaseWP8Popup_t3692405086, ___title_4)); }
	inline String_t* get_title_4() const { return ___title_4; }
	inline String_t** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(String_t* value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier(&___title_4, value);
	}

	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(BaseWP8Popup_t3692405086, ___message_5)); }
	inline String_t* get_message_5() const { return ___message_5; }
	inline String_t** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(String_t* value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
