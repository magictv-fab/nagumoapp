﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUIButton
struct  SampleAppUIButton_t4060007389  : public ISampleAppUIElement_t2180050874
{
public:
	// UnityEngine.Texture SampleAppUIButton::mButtonImage
	Texture_t2526458961 * ___mButtonImage_0;
	// UnityEngine.Rect SampleAppUIButton::mRect
	Rect_t4241904616  ___mRect_1;
	// UnityEngine.GUIStyle SampleAppUIButton::mStyle
	GUIStyle_t2990928826 * ___mStyle_2;
	// System.String SampleAppUIButton::mTitle
	String_t* ___mTitle_3;
	// System.Action SampleAppUIButton::TappedOn
	Action_t3771233898 * ___TappedOn_4;

public:
	inline static int32_t get_offset_of_mButtonImage_0() { return static_cast<int32_t>(offsetof(SampleAppUIButton_t4060007389, ___mButtonImage_0)); }
	inline Texture_t2526458961 * get_mButtonImage_0() const { return ___mButtonImage_0; }
	inline Texture_t2526458961 ** get_address_of_mButtonImage_0() { return &___mButtonImage_0; }
	inline void set_mButtonImage_0(Texture_t2526458961 * value)
	{
		___mButtonImage_0 = value;
		Il2CppCodeGenWriteBarrier(&___mButtonImage_0, value);
	}

	inline static int32_t get_offset_of_mRect_1() { return static_cast<int32_t>(offsetof(SampleAppUIButton_t4060007389, ___mRect_1)); }
	inline Rect_t4241904616  get_mRect_1() const { return ___mRect_1; }
	inline Rect_t4241904616 * get_address_of_mRect_1() { return &___mRect_1; }
	inline void set_mRect_1(Rect_t4241904616  value)
	{
		___mRect_1 = value;
	}

	inline static int32_t get_offset_of_mStyle_2() { return static_cast<int32_t>(offsetof(SampleAppUIButton_t4060007389, ___mStyle_2)); }
	inline GUIStyle_t2990928826 * get_mStyle_2() const { return ___mStyle_2; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyle_2() { return &___mStyle_2; }
	inline void set_mStyle_2(GUIStyle_t2990928826 * value)
	{
		___mStyle_2 = value;
		Il2CppCodeGenWriteBarrier(&___mStyle_2, value);
	}

	inline static int32_t get_offset_of_mTitle_3() { return static_cast<int32_t>(offsetof(SampleAppUIButton_t4060007389, ___mTitle_3)); }
	inline String_t* get_mTitle_3() const { return ___mTitle_3; }
	inline String_t** get_address_of_mTitle_3() { return &___mTitle_3; }
	inline void set_mTitle_3(String_t* value)
	{
		___mTitle_3 = value;
		Il2CppCodeGenWriteBarrier(&___mTitle_3, value);
	}

	inline static int32_t get_offset_of_TappedOn_4() { return static_cast<int32_t>(offsetof(SampleAppUIButton_t4060007389, ___TappedOn_4)); }
	inline Action_t3771233898 * get_TappedOn_4() const { return ___TappedOn_4; }
	inline Action_t3771233898 ** get_address_of_TappedOn_4() { return &___TappedOn_4; }
	inline void set_TappedOn_4(Action_t3771233898 * value)
	{
		___TappedOn_4 = value;
		Il2CppCodeGenWriteBarrier(&___TappedOn_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
