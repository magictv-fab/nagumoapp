﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Collections.Generic.IEqualityComparer`1<ZXing.ResultMetadataType>
struct IEqualityComparer_1_t3714401376;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<ZXing.ResultMetadataType>
struct ICollection_1_t3817956959;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2235185101;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>
struct IEnumerator_1_t650707453;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>
struct KeyCollection_t466821149;
// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>
struct ValueCollection_t1835634707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23033809700.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En157385090.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2355725268_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2355725268(__this, method) ((  void (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2__ctor_m2355725268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3271247604_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3271247604(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3271247604_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3924158158_gshared (Dictionary_2_t3135028994 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3924158158(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3135028994 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3924158158_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m504688830_gshared (Dictionary_2_t3135028994 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m504688830(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3135028994 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m504688830_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4096921403_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4096921403(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4096921403_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1271079931_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1271079931(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1271079931_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4104656425_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4104656425(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4104656425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m372328384_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m372328384(__this, method) ((  bool (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m372328384_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3375513401_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3375513401(__this, method) ((  bool (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3375513401_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2219576033_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2219576033(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2219576033_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1360651600_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1360651600(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1360651600_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m801523457_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m801523457(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m801523457_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2238732177_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2238732177(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2238732177_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1952513934_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1952513934(__this, ___key0, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1952513934_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1941301603_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1941301603(__this, method) ((  bool (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1941301603_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1407945621_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1407945621(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1407945621_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m738381799_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m738381799(__this, method) ((  bool (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m738381799_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2632256804_gshared (Dictionary_2_t3135028994 * __this, KeyValuePair_2_t3033809700  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2632256804(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3135028994 *, KeyValuePair_2_t3033809700 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2632256804_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4232955490_gshared (Dictionary_2_t3135028994 * __this, KeyValuePair_2_t3033809700  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4232955490(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3135028994 *, KeyValuePair_2_t3033809700 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4232955490_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1840151560_gshared (Dictionary_2_t3135028994 * __this, KeyValuePair_2U5BU5D_t2235185101* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1840151560(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3135028994 *, KeyValuePair_2U5BU5D_t2235185101*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1840151560_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1274945031_gshared (Dictionary_2_t3135028994 * __this, KeyValuePair_2_t3033809700  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1274945031(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3135028994 *, KeyValuePair_2_t3033809700 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1274945031_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4168135399_gshared (Dictionary_2_t3135028994 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4168135399(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4168135399_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m273645494_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m273645494(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m273645494_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2159050093_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2159050093(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2159050093_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3354876794_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3354876794(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3354876794_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m878084253_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m878084253(__this, method) ((  int32_t (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_get_Count_m878084253_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3567199946_gshared (Dictionary_2_t3135028994 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3567199946(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3567199946_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1040579581_gshared (Dictionary_2_t3135028994 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1040579581(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3135028994 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1040579581_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3979293813_gshared (Dictionary_2_t3135028994 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3979293813(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3135028994 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3979293813_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2095468514_gshared (Dictionary_2_t3135028994 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2095468514(__this, ___size0, method) ((  void (*) (Dictionary_2_t3135028994 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2095468514_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3345397854_gshared (Dictionary_2_t3135028994 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3345397854(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3345397854_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3033809700  Dictionary_2_make_pair_m3925733106_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3925733106(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3033809700  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3925733106_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3756174220_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3756174220(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3756174220_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2933179624_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2933179624(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2933179624_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3453128113_gshared (Dictionary_2_t3135028994 * __this, KeyValuePair_2U5BU5D_t2235185101* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3453128113(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3135028994 *, KeyValuePair_2U5BU5D_t2235185101*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3453128113_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1151976155_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1151976155(__this, method) ((  void (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_Resize_m1151976155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m878165080_gshared (Dictionary_2_t3135028994 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m878165080(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3135028994 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m878165080_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2152038696_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2152038696(__this, method) ((  void (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_Clear_m2152038696_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2613281746_gshared (Dictionary_2_t3135028994 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2613281746(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3135028994 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2613281746_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m942972626_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m942972626(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m942972626_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2100337179_gshared (Dictionary_2_t3135028994 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2100337179(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3135028994 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2100337179_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3029254953_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3029254953(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3029254953_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2682919390_gshared (Dictionary_2_t3135028994 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2682919390(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3135028994 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2682919390_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3711953451_gshared (Dictionary_2_t3135028994 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3711953451(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3135028994 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3711953451_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::get_Keys()
extern "C"  KeyCollection_t466821149 * Dictionary_2_get_Keys_m1147673552_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1147673552(__this, method) ((  KeyCollection_t466821149 * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_get_Keys_m1147673552_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::get_Values()
extern "C"  ValueCollection_t1835634707 * Dictionary_2_get_Values_m3704396268_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3704396268(__this, method) ((  ValueCollection_t1835634707 * (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_get_Values_m3704396268_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3206033127_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3206033127(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3206033127_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1245493763_gshared (Dictionary_2_t3135028994 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1245493763(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3135028994 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1245493763_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2966247361_gshared (Dictionary_2_t3135028994 * __this, KeyValuePair_2_t3033809700  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2966247361(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3135028994 *, KeyValuePair_2_t3033809700 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2966247361_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t157385090  Dictionary_2_GetEnumerator_m3770762376_gshared (Dictionary_2_t3135028994 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3770762376(__this, method) ((  Enumerator_t157385090  (*) (Dictionary_2_t3135028994 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3770762376_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m2919989565_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m2919989565(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m2919989565_gshared)(__this /* static, unused */, ___key0, ___value1, method)
