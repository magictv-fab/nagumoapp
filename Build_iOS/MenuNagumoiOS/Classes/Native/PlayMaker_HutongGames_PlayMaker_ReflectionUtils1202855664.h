﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t3683564144;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ReflectionUtils
struct  ReflectionUtils_t1202855664  : public Il2CppObject
{
public:

public:
};

struct ReflectionUtils_t1202855664_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ReflectionUtils::assemblies
	List_1_t1375417109 * ___assemblies_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> HutongGames.PlayMaker.ReflectionUtils::typeLookup
	Dictionary_2_t3683564144 * ___typeLookup_1;

public:
	inline static int32_t get_offset_of_assemblies_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t1202855664_StaticFields, ___assemblies_0)); }
	inline List_1_t1375417109 * get_assemblies_0() const { return ___assemblies_0; }
	inline List_1_t1375417109 ** get_address_of_assemblies_0() { return &___assemblies_0; }
	inline void set_assemblies_0(List_1_t1375417109 * value)
	{
		___assemblies_0 = value;
		Il2CppCodeGenWriteBarrier(&___assemblies_0, value);
	}

	inline static int32_t get_offset_of_typeLookup_1() { return static_cast<int32_t>(offsetof(ReflectionUtils_t1202855664_StaticFields, ___typeLookup_1)); }
	inline Dictionary_2_t3683564144 * get_typeLookup_1() const { return ___typeLookup_1; }
	inline Dictionary_2_t3683564144 ** get_address_of_typeLookup_1() { return &___typeLookup_1; }
	inline void set_typeLookup_1(Dictionary_2_t3683564144 * value)
	{
		___typeLookup_1 = value;
		Il2CppCodeGenWriteBarrier(&___typeLookup_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
