﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1536434148;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleAutoDestruction
struct  ParticleAutoDestruction_t2314186749  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.ParticleSystem[] ParticleAutoDestruction::particleSystems
	ParticleSystemU5BU5D_t1536434148* ___particleSystems_2;

public:
	inline static int32_t get_offset_of_particleSystems_2() { return static_cast<int32_t>(offsetof(ParticleAutoDestruction_t2314186749, ___particleSystems_2)); }
	inline ParticleSystemU5BU5D_t1536434148* get_particleSystems_2() const { return ___particleSystems_2; }
	inline ParticleSystemU5BU5D_t1536434148** get_address_of_particleSystems_2() { return &___particleSystems_2; }
	inline void set_particleSystems_2(ParticleSystemU5BU5D_t1536434148* value)
	{
		___particleSystems_2 = value;
		Il2CppCodeGenWriteBarrier(&___particleSystems_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
