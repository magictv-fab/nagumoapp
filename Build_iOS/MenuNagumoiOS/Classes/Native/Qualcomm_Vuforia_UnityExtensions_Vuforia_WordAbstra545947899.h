﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Vuforia.Word
struct Word_t2165514892;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTempl3973358313.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordAbstractBehaviour
struct  WordAbstractBehaviour_t545947899  : public TrackableBehaviour_t4179556250
{
public:
	// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::mMode
	int32_t ___mMode_9;
	// System.String Vuforia.WordAbstractBehaviour::mSpecificWord
	String_t* ___mSpecificWord_10;
	// Vuforia.Word Vuforia.WordAbstractBehaviour::mWord
	Il2CppObject * ___mWord_11;

public:
	inline static int32_t get_offset_of_mMode_9() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t545947899, ___mMode_9)); }
	inline int32_t get_mMode_9() const { return ___mMode_9; }
	inline int32_t* get_address_of_mMode_9() { return &___mMode_9; }
	inline void set_mMode_9(int32_t value)
	{
		___mMode_9 = value;
	}

	inline static int32_t get_offset_of_mSpecificWord_10() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t545947899, ___mSpecificWord_10)); }
	inline String_t* get_mSpecificWord_10() const { return ___mSpecificWord_10; }
	inline String_t** get_address_of_mSpecificWord_10() { return &___mSpecificWord_10; }
	inline void set_mSpecificWord_10(String_t* value)
	{
		___mSpecificWord_10 = value;
		Il2CppCodeGenWriteBarrier(&___mSpecificWord_10, value);
	}

	inline static int32_t get_offset_of_mWord_11() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t545947899, ___mWord_11)); }
	inline Il2CppObject * get_mWord_11() const { return ___mWord_11; }
	inline Il2CppObject ** get_address_of_mWord_11() { return &___mWord_11; }
	inline void set_mWord_11(Il2CppObject * value)
	{
		___mWord_11 = value;
		Il2CppCodeGenWriteBarrier(&___mWord_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
