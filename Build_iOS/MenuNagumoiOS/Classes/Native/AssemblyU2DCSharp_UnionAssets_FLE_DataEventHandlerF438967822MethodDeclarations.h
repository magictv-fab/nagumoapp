﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t438967822;
// System.Object
struct Il2CppObject;
// UnionAssets.FLE.CEvent
struct CEvent_t44106931;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_CEvent44106931.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UnionAssets.FLE.DataEventHandlerFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void DataEventHandlerFunction__ctor_m3348484228 (DataEventHandlerFunction_t438967822 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.DataEventHandlerFunction::Invoke(UnionAssets.FLE.CEvent)
extern "C"  void DataEventHandlerFunction_Invoke_m779741020 (DataEventHandlerFunction_t438967822 * __this, CEvent_t44106931 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UnionAssets.FLE.DataEventHandlerFunction::BeginInvoke(UnionAssets.FLE.CEvent,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DataEventHandlerFunction_BeginInvoke_m3902557211 (DataEventHandlerFunction_t438967822 * __this, CEvent_t44106931 * ___e0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.DataEventHandlerFunction::EndInvoke(System.IAsyncResult)
extern "C"  void DataEventHandlerFunction_EndInvoke_m3914212500 (DataEventHandlerFunction_t438967822 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
