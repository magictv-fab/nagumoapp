﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1076900934;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"

// System.Void HutongGames.PlayMaker.FsmEventData::.ctor()
extern "C"  void FsmEventData__ctor_m2693490509 (FsmEventData_t1076900934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventData::.ctor(HutongGames.PlayMaker.FsmEventData)
extern "C"  void FsmEventData__ctor_m3030756797 (FsmEventData_t1076900934 * __this, FsmEventData_t1076900934 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventData::DebugLog()
extern "C"  void FsmEventData_DebugLog_m3293209864 (FsmEventData_t1076900934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
