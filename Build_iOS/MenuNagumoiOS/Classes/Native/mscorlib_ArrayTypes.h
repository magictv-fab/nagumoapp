﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Il2CppObject;
// System.IConvertible
struct IConvertible_t2116191568;
// System.IComparable
struct IComparable_t1391370361;
// System.IComparable`1<System.Char>
struct IComparable_1_t2772552329;
// System.IEquatable`1<System.Char>
struct IEquatable_1_t2411901105;
// System.ValueType
struct ValueType_t1744280289;
// System.String
struct String_t;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.ICloneable
struct ICloneable_t1025544834;
// System.IComparable`1<System.String>
struct IComparable_1_t4212128644;
// System.IEquatable`1<System.String>
struct IEquatable_1_t3851477420;
// System.Type
struct Type_t;
// System.Reflection.IReflect
struct IReflect_t2853506214;
// System.Runtime.InteropServices._Type
struct _Type_t2149739635;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1425685797;
// System.Runtime.InteropServices._MemberInfo
struct _MemberInfo_t3353101921;
// System.IFormattable
struct IFormattable_t382002946;
// System.IComparable`1<System.Int32>
struct IComparable_1_t1063768291;
// System.IEquatable`1<System.Int32>
struct IEquatable_1_t703117067;
// System.IComparable`1<System.Double>
struct IComparable_1_t3778156356;
// System.IEquatable`1<System.Double>
struct IEquatable_1_t3417505132;
// System.IComparable`1<System.UInt32>
struct IComparable_1_t4229565068;
// System.IEquatable`1<System.UInt32>
struct IEquatable_1_t3868913844;
// System.IComparable`1<System.Byte>
struct IComparable_1_t2772539451;
// System.IEquatable`1<System.Byte>
struct IEquatable_1_t2411888227;
// System.IComparable`1<System.Single>
struct IComparable_1_t4201848763;
// System.IEquatable`1<System.Single>
struct IEquatable_1_t3841197539;
// System.Delegate
struct Delegate_t3310234105;
// System.Runtime.Serialization.ISerializable
struct ISerializable_t867484142;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// System.Runtime.InteropServices._ParameterInfo
struct _ParameterInfo_t2787166306;
// System.IComparable`1<System.UInt16>
struct IComparable_1_t4229565010;
// System.IEquatable`1<System.UInt16>
struct IEquatable_1_t3868913786;
// System.IComparable`1<System.UInt64>
struct IComparable_1_t4229565163;
// System.IEquatable`1<System.UInt64>
struct IEquatable_1_t3868913939;
// System.IComparable`1<System.Int16>
struct IComparable_1_t1063768233;
// System.IEquatable`1<System.Int16>
struct IEquatable_1_t703117009;
// System.IComparable`1<System.SByte>
struct IComparable_1_t1071699568;
// System.IEquatable`1<System.SByte>
struct IEquatable_1_t711048344;
// System.IComparable`1<System.Int64>
struct IComparable_1_t1063768386;
// System.IEquatable`1<System.Int64>
struct IEquatable_1_t703117162;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Runtime.InteropServices._FieldInfo
struct _FieldInfo_t209867187;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.InteropServices._MethodInfo
struct _MethodInfo_t3971289384;
// System.Reflection.MethodBase
struct MethodBase_t318515428;
// System.Runtime.InteropServices._MethodBase
struct _MethodBase_t3971068747;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.InteropServices._PropertyInfo
struct _PropertyInfo_t3711326812;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Runtime.InteropServices._ConstructorInfo
struct _ConstructorInfo_t3408715251;
// Mono.Globalization.Unicode.TailoringInfo
struct TailoringInfo_t3025807515;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t3998770676;
// Mono.Globalization.Unicode.Level2Map
struct Level2Map_t3664214860;
// Mono.Math.BigInteger
struct BigInteger_t3334373498;
// System.Security.Cryptography.KeySizes
struct KeySizes_t2106826975;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Runtime.InteropServices._Assembly
struct _Assembly_t3789461407;
// System.Security.IEvidenceFactory
struct IEvidenceFactory_t3943896478;
// System.IComparable`1<System.Boolean>
struct IComparable_1_t386728509;
// System.IEquatable`1<System.Boolean>
struct IEquatable_1_t26077285;
// System.Reflection.CustomAttributeData
struct CustomAttributeData_t2955630591;
// System.Enum
struct Enum_t2862688501;
// System.Diagnostics.StackFrame
struct StackFrame_t1034942277;
// System.Globalization.Calendar
struct Calendar_t3558528576;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.IO.FileInfo
struct FileInfo_t3233670074;
// System.IO.FileSystemInfo
struct FileSystemInfo_t2605906633;
// System.MarshalByRefObject
struct MarshalByRefObject_t1219038801;
// System.IO.DirectoryInfo
struct DirectoryInfo_t89154617;
// System.Reflection.Module
struct Module_t1394482686;
// System.Runtime.InteropServices._Module
struct _Module_t2601912805;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t595214213;
// System.Runtime.InteropServices._ModuleBuilder
struct _ModuleBuilder_t1764509690;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t3159962230;
// System.Runtime.InteropServices._ParameterBuilder
struct _ParameterBuilder_t4122453611;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Array
struct Il2CppArray;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Collections.IList
struct IList_t1751339649;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t194563060;
// System.Runtime.InteropServices._LocalBuilder
struct _LocalBuilder_t3375243241;
// System.Reflection.LocalVariableInfo
struct LocalVariableInfo_t962988767;
// System.Reflection.Emit.GenericTypeParameterBuilder
struct GenericTypeParameterBuilder_t553556921;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1918497079;
// System.Runtime.InteropServices._TypeBuilder
struct _TypeBuilder_t3501492652;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t302405488;
// System.Runtime.InteropServices._MethodBuilder
struct _MethodBuilder_t1471700965;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t3217839941;
// System.Runtime.InteropServices._ConstructorBuilder
struct _ConstructorBuilder_t788093754;
// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t2012258748;
// System.Runtime.InteropServices._PropertyBuilder
struct _PropertyBuilder_t752753201;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1754069893;
// System.Runtime.InteropServices._FieldBuilder
struct _FieldBuilder_t639782778;
// System.Runtime.Remoting.Contexts.IContextAttribute
struct IContextAttribute_t3913746816;
// System.Runtime.Remoting.Contexts.IContextProperty
struct IContextProperty_t82913453;
// System.Runtime.Remoting.Messaging.Header
struct Header_t1689611527;
// System.Runtime.Remoting.Services.ITrackingHandler
struct ITrackingHandler_t2228500544;
// System.IComparable`1<System.DateTime>
struct IComparable_1_t4193591118;
// System.IEquatable`1<System.DateTime>
struct IEquatable_1_t3832939894;
// System.IComparable`1<System.Decimal>
struct IComparable_1_t1864280422;
// System.IEquatable`1<System.Decimal>
struct IEquatable_1_t1503629198;
// System.IComparable`1<System.TimeSpan>
struct IComparable_1_t323452778;
// System.IEquatable`1<System.TimeSpan>
struct IEquatable_1_t4257768850;
// System.MonoType
struct MonoType_t;
// System.Security.Permissions.KeyContainerPermissionAccessEntry
struct KeyContainerPermissionAccessEntry_t2372748443;
// System.Security.Policy.StrongName
struct StrongName_t2878058698;
// System.Security.Policy.CodeConnectAccess
struct CodeConnectAccess_t2328951823;
// System.Text.EncodingInfo
struct EncodingInfo_t1898473639;
// System.Threading.WaitHandle
struct WaitHandle_t1661568373;
// System.IDisposable
struct IDisposable_t1423340799;
// System.SystemException
struct SystemException_t4206535862;
// System.Exception
struct Exception_t3991598821;
// System.Runtime.InteropServices._Exception
struct _Exception_t426620218;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Runtime.Serialization.IDeserializationCallback
struct IDeserializationCallback_t675596727;
// System.Attribute
struct Attribute_t2523058482;
// System.Runtime.InteropServices._Attribute
struct _Attribute_t3253047175;
// System.Attribute[]
struct AttributeU5BU5D_t4055800263;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t2315379190;
// System.IEquatable`1<HutongGames.PlayMaker.FsmTransition>
struct IEquatable_1_t3320890566;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int64[]
struct Int64U5BU5D_t2174042770;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.SByte[]
struct SByteU5BU5D_t2505034988;
// System.IEquatable`1<BigIntegerLibrary.BigInteger>
struct IEquatable_1_t4260544485;
// System.IComparable`1<BigIntegerLibrary.BigInteger>
struct IComparable_1_t326228413;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t1914133451;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t2498711176;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t574734531;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t4095326316;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t1377224777;
// System.Collections.Generic.List`1<UnityEngine.UI.Mask>
struct List_1_t1377012232;
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
struct List_1_t430297630;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1355284823;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1317283468;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>
struct List_1_t4040371092;
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>
struct List_1_t1807153374;
// System.Collections.Generic.List`1<MagicTV.vo.BundleVO>
struct List_1_t3352703625;
// System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>
struct IDictionary_2_t2031895;
// System.Collections.Generic.IList`1<LitJson.PropertyMetadata>
struct IList_1_t2466314523;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>
struct IDictionary_2_t1821615648;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>
struct IDictionary_2_t2805984405;
// System.MulticastDelegate
struct MulticastDelegate_t3389745971;
// System.Action`1<UniWebViewNativeResultPayload>
struct Action_1_t1392508089;
// System.Collections.Hashtable
struct Hashtable_t1407064410;

#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_ValueType1744280289.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3372848153.h"
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfo3025807515.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_Link2063667470.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21873037576.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction3998770676.h"
#include "mscorlib_Mono_Globalization_Unicode_Level2Map3664214860.h"
#include "mscorlib_Mono_Math_BigInteger3334373498.h"
#include "mscorlib_Mono_Security_Uri_UriScheme3372318283.h"
#include "mscorlib_System_Security_Cryptography_KeySizes2106826975.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Reflection_CustomAttributeData2955630591.h"
#include "mscorlib_System_TermInfoStrings2002624446.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_Collections_Hashtable_Slot2260530181.h"
#include "mscorlib_System_Collections_SortedList_Slot2072023290.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277.h"
#include "mscorlib_System_Globalization_Calendar3558528576.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"
#include "mscorlib_System_IO_FileSystemInfo2605906633.h"
#include "mscorlib_System_MarshalByRefObject1219038801.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"
#include "mscorlib_System_Reflection_Module1394482686.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder595214213.h"
#include "mscorlib_System_Reflection_Emit_MonoResource1505432149.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS3880501745.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder3159962230.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_Emit_LocalBuilder194563060.h"
#include "mscorlib_System_Reflection_LocalVariableInfo962988767.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo1354080954.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3207823784.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi660379442.h"
#include "mscorlib_System_Reflection_Emit_GenericTypeParamete553556921.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder1918497079.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder302405488.h"
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder3217839941.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder2012258748.h"
#include "mscorlib_System_Reflection_Emit_FieldBuilder1754069893.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4013605874.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC2113902833.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header1689611527.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2420703430.h"
#include "mscorlib_System_MonoType2166710577.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP2372748443.h"
#include "mscorlib_System_Security_Policy_StrongName2878058698.h"
#include "mscorlib_System_Security_Policy_CodeConnectAccess2328951823.h"
#include "mscorlib_System_Text_EncodingInfo1898473639.h"
#include "mscorlib_System_Threading_WaitHandle1661568373.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_SystemException4206535862.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22758969756.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21421923377.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21472936237.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21627469341.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22571132604.h"
#include "mscorlib_System_ArraySegment_1_gen2188033608.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21195997794.h"
#include "mscorlib_System_Reflection_Emit_DynamicMethod2315379190.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22164509075.h"
#include "mscorlib_System_Reflection_Emit_Label2268465130.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_775952400.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23710127902.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21289591971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23582344850.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22571765214.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21158041691.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23033809700.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23617918191.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21188478419.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_908399279.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_996820380.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22540055952.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22438309113.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23261464244.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23309166264.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21827855558.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21857232484.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21718082560.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24089910802.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23677105400.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_600250352.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_512738917.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g28871114.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_975906802.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_441991844.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1914133451.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22633332527.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22990433664.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38412473.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22061353102.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21189512043.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24075600195.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_291920669.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_355424280.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23416499615.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030510692.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22680889866.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21087282249.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2498711176.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen574734531.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21744794968.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4095326316.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1377224777.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21496360359.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23226014568.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21354567995.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23983107678.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1377012232.h"
#include "mscorlib_System_Collections_Generic_List_1_gen430297630.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21329917009.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284823.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468.h"
#include "mscorlib_System_Collections_Generic_List_1_gen272385497.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_991584573.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_595048151.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4040371092.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23936415037.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1807153374.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21703197319.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23430729120.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23570725950.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22704828336.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_384374791.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3352703625.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24071902701.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22882091176.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23035933110.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22116912562.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_941084846.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22599727649.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22790158764.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23490093394.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_716150752.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22479660279.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_235549625.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21458533870.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21101872204.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22434214506.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21840487222.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21832195516.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24078114400.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24062546101.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_ge6235086.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22013497689.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22470517714.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23334563664.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21825818839.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_490866396.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_322939256.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22142523009.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22702028350.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23126891766.h"
#include "mscorlib_System_MulticastDelegate3389745971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22042378012.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g98913785.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_868687879.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_195465678.h"
#include "mscorlib_System_Action_1_gen1392508089.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22111707165.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21514770136.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"

#pragma once
// System.Object[]
struct ObjectU5BU5D_t1108656482  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3324145743  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.IConvertible[]
struct IConvertibleU5BU5D_t2953096049  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable[]
struct IComparableU5BU5D_t3856259460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t3342660916  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t406268268  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ValueType[]
struct ValueTypeU5BU5D_t2089672828  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ValueType_t1744280289 * m_Items[1];

public:
	inline ValueType_t1744280289 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ValueType_t1744280289 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ValueType_t1744280289 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t4054002952  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline String_t** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t617364234  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ICloneable[]
struct ICloneableU5BU5D_t847747383  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t4072518125  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t1136125477  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3339007067  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t1411309059  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t3798043746  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MemberInfo_t * m_Items[1];

public:
	inline MemberInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t3471161512  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t3031668476  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t3230847821  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.IFormattable[]
struct IFormattableU5BU5D_t3834962615  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t3249362994  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t312970346  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Double[]
struct DoubleU5BU5D_t2145413704  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) double m_Items[1];

public:
	inline double GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline double* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, double value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t2163928877  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t3522503525  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t3230734560  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t3249249733  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t312857085  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4260760469  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t4279275642  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t1342882994  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[,]
struct SingleU5BU2CU5D_t2316563990  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline float* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
	inline float GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, float value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t2316563989  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline float* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t2335079162  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t3693653810  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t2039970308  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Delegate_t3310234105 * m_Items[1];

public:
	inline Delegate_t3310234105 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Delegate_t3310234105 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Delegate_t3310234105 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t147227291  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t2015293532  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t2235474049 * m_Items[1];

public:
	inline ParameterInfo_t2235474049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterInfo_t2235474049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t2235474049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t1150119895  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3896472559  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t741930026  m_Items[1];

public:
	inline ParameterModifier_t741930026  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterModifier_t741930026 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t741930026  value)
	{
		m_Items[index] = value;
	}
};
// System.UInt16[]
struct UInt16U5BU5D_t801649474  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint16_t m_Items[1];

public:
	inline uint16_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint16_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint16_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t820164647  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t2178739295  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt64[]
struct UInt64U5BU5D_t2173929509  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint64_t m_Items[1];

public:
	inline uint64_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint64_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint64_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t2192444682  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t3551019330  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int16[]
struct Int16U5BU5D_t801762735  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int16_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t820277908  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t2178852556  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.SByte[]
struct SByteU5BU5D_t2505034988  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int8_t m_Items[1];

public:
	inline int8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int8_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t2523550161  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t3882124809  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int64[]
struct Int64U5BU5D_t2174042770  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int64_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t2192557943  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t3551132591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t2281515554  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2824366364  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t886111545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t2923381517  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodBase_t318515428 * m_Items[1];

public:
	inline MethodBase_t318515428 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodBase_t318515428 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodBase_t318515428 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t985126698  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t4286713048  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyInfo_t * m_Items[1];

public:
	inline PropertyInfo_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyInfo_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t4035282101  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t2079826215  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstructorInfo_t4136801618 * m_Items[1];

public:
	inline ConstructorInfo_t4136801618 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstructorInfo_t4136801618 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstructorInfo_t4136801618 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t2267260130  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IntPtr_t m_Items[1];

public:
	inline IntPtr_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IntPtr_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IntPtr_t value)
	{
		m_Items[index] = value;
	}
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1733526372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TableRange_t3372848153  m_Items[1];

public:
	inline TableRange_t3372848153  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TableRange_t3372848153 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TableRange_t3372848153  value)
	{
		m_Items[index] = value;
	}
};
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t4063285914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TailoringInfo_t3025807515 * m_Items[1];

public:
	inline TailoringInfo_t3025807515 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TailoringInfo_t3025807515 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TailoringInfo_t3025807515 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t310404823  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3222658402  m_Items[1];

public:
	inline KeyValuePair_2_t3222658402  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3222658402 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3222658402  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t375419643  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2063667470  m_Items[1];

public:
	inline Link_t2063667470  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2063667470 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2063667470  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t479206547  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DictionaryEntry_t1751606614  m_Items[1];

public:
	inline DictionaryEntry_t1751606614  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DictionaryEntry_t1751606614 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DictionaryEntry_t1751606614  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2844887513  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1873037576  m_Items[1];

public:
	inline KeyValuePair_2_t1873037576  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1873037576 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1873037576  value)
	{
		m_Items[index] = value;
	}
};
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t376151997  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Contraction_t3998770676 * m_Items[1];

public:
	inline Contraction_t3998770676 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Contraction_t3998770676 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Contraction_t3998770676 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t180654597  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Level2Map_t3664214860 * m_Items[1];

public:
	inline Level2Map_t3664214860 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Level2Map_t3664214860 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Level2Map_t3664214860 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1634278495  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BigInteger_t3334373498 * m_Items[1];

public:
	inline BigInteger_t3334373498 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BigInteger_t3334373498 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BigInteger_t3334373498 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Security.Uri/UriScheme[]
struct UriSchemeU5BU5D_t1869643818  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UriScheme_t3372318283  m_Items[1];

public:
	inline UriScheme_t3372318283  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UriScheme_t3372318283 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UriScheme_t3372318283  value)
	{
		m_Items[index] = value;
	}
};
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1457372358  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeySizes_t2106826975 * m_Items[1];

public:
	inline KeySizes_t2106826975 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeySizes_t2106826975 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeySizes_t2106826975 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t4221342377  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Assembly_t1418687608 * m_Items[1];

public:
	inline Assembly_t1418687608 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Assembly_t1418687608 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Assembly_t1418687608 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Assembly[]
struct _AssemblyU5BU5D_t2326024966  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.IEvidenceFactory[]
struct IEvidenceFactoryU5BU5D_t2437773867  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t3456302923  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline bool* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t3474818096  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t538425448  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t4123632934  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeData_t2955630591 * m_Items[1];

public:
	inline CustomAttributeData_t2955630591 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomAttributeData_t2955630591 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeData_t2955630591 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.TermInfoStrings[]
struct TermInfoStringsU5BU5D_t599384971  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Enum[]
struct EnumU5BU5D_t3205174168  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Enum_t2862688501 * m_Items[1];

public:
	inline Enum_t2862688501 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Enum_t2862688501 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Enum_t2862688501 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t2935089736  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Slot_t2260530181  m_Items[1];

public:
	inline Slot_t2260530181  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Slot_t2260530181 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Slot_t2260530181  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t4007541215  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Slot_t2072023290  m_Items[1];

public:
	inline Slot_t2072023290  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Slot_t2072023290 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Slot_t2072023290  value)
	{
		m_Items[index] = value;
	}
};
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t627323400  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StackFrame_t1034942277 * m_Items[1];

public:
	inline StackFrame_t1034942277 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StackFrame_t1034942277 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StackFrame_t1034942277 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t2835990721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Calendar_t3558528576 * m_Items[1];

public:
	inline Calendar_t3558528576 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Calendar_t3558528576 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Calendar_t3558528576 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Globalization.CultureInfo[]
struct CultureInfoU5BU5D_t1427656963  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CultureInfo_t1065375142 * m_Items[1];

public:
	inline CultureInfo_t1065375142 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CultureInfo_t1065375142 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CultureInfo_t1065375142 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IFormatProvider[]
struct IFormatProviderU5BU5D_t3850808286  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t3358688287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileInfo_t3233670074 * m_Items[1];

public:
	inline FileInfo_t3233670074 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileInfo_t3233670074 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileInfo_t3233670074 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IO.FileSystemInfo[]
struct FileSystemInfoU5BU5D_t1190771700  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileSystemInfo_t2605906633 * m_Items[1];

public:
	inline FileSystemInfo_t2605906633 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileSystemInfo_t2605906633 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileSystemInfo_t2605906633 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.MarshalByRefObject[]
struct MarshalByRefObjectU5BU5D_t4207444300  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MarshalByRefObject_t1219038801 * m_Items[1];

public:
	inline MarshalByRefObject_t1219038801 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MarshalByRefObject_t1219038801 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MarshalByRefObject_t1219038801 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IO.DirectoryInfo[]
struct DirectoryInfoU5BU5D_t2262552260  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DirectoryInfo_t89154617 * m_Items[1];

public:
	inline DirectoryInfo_t89154617 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DirectoryInfo_t89154617 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DirectoryInfo_t89154617 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Module[]
struct ModuleU5BU5D_t1003119691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Module_t1394482686 * m_Items[1];

public:
	inline Module_t1394482686 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Module_t1394482686 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Module_t1394482686 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t1405051112  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t3301293422  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t3301293422  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomAttributeTypedArgument_t3301293422 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t3301293422  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t3059612989  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t3059612989  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CustomAttributeNamedArgument_t3059612989 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t3059612989  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1235779784  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ModuleBuilder_t595214213 * m_Items[1];

public:
	inline ModuleBuilder_t595214213 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ModuleBuilder_t595214213 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ModuleBuilder_t595214213 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t2693908191  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t1470396600  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MonoResource_t1505432149  m_Items[1];

public:
	inline MonoResource_t1505432149  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MonoResource_t1505432149 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MonoResource_t1505432149  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t2414045996  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RefEmitPermissionSet_t3880501745  m_Items[1];

public:
	inline RefEmitPermissionSet_t3880501745  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RefEmitPermissionSet_t3880501745 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RefEmitPermissionSet_t3880501745  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t3245922035  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterBuilder_t3159962230 * m_Items[1];

public:
	inline ParameterBuilder_t3159962230 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterBuilder_t3159962230 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterBuilder_t3159962230 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t2749961098  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[][]
struct TypeU5BU5DU5BU5D_t2708692954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeU5BU5D_t3339007067* m_Items[1];

public:
	inline TypeU5BU5D_t3339007067* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeU5BU5D_t3339007067** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeU5BU5D_t3339007067* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Array[]
struct ArrayU5BU5D_t3094558710  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppArray * m_Items[1];

public:
	inline Il2CppArray * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppArray ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppArray * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t3870071836  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.IList[]
struct IListU5BU5D_t3576116828  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.LocalBuilder[]
struct LocalBuilderU5BU5D_t2031018429  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalBuilder_t194563060 * m_Items[1];

public:
	inline LocalBuilder_t194563060 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalBuilder_t194563060 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalBuilder_t194563060 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._LocalBuilder[]
struct _LocalBuilderU5BU5D_t4029726292  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.LocalVariableInfo[]
struct LocalVariableInfoU5BU5D_t2935122630  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalVariableInfo_t962988767 * m_Items[1];

public:
	inline LocalVariableInfo_t962988767 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalVariableInfo_t962988767 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalVariableInfo_t962988767 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1040181535  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ILTokenInfo_t1354080954  m_Items[1];

public:
	inline ILTokenInfo_t1354080954  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ILTokenInfo_t1354080954 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ILTokenInfo_t1354080954  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t657210041  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LabelData_t3207823784  m_Items[1];

public:
	inline LabelData_t3207823784  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LabelData_t3207823784 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LabelData_t3207823784  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1656159175  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LabelFixup_t660379442  m_Items[1];

public:
	inline LabelFixup_t660379442  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LabelFixup_t660379442 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LabelFixup_t660379442  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t2802075972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GenericTypeParameterBuilder_t553556921 * m_Items[1];

public:
	inline GenericTypeParameterBuilder_t553556921 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GenericTypeParameterBuilder_t553556921 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GenericTypeParameterBuilder_t553556921 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1363945486  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeBuilder_t1918497079 * m_Items[1];

public:
	inline TypeBuilder_t1918497079 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeBuilder_t1918497079 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeBuilder_t1918497079 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t193502757  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t304196817  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodBuilder_t302405488 * m_Items[1];

public:
	inline MethodBuilder_t302405488 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodBuilder_t302405488 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodBuilder_t302405488 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t1762325224  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t678056456  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstructorBuilder_t3217839941 * m_Items[1];

public:
	inline ConstructorBuilder_t3217839941 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstructorBuilder_t3217839941 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstructorBuilder_t3217839941 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t1773125279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.PropertyBuilder[]
struct PropertyBuilderU5BU5D_t1657492437  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyBuilder_t2012258748 * m_Items[1];

public:
	inline PropertyBuilder_t2012258748 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyBuilder_t2012258748 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyBuilder_t2012258748 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._PropertyBuilder[]
struct _PropertyBuilderU5BU5D_t3731712492  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t3550958792  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FieldBuilder_t1754069893 * m_Items[1];

public:
	inline FieldBuilder_t1754069893 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FieldBuilder_t1754069893 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FieldBuilder_t1754069893 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t1254699359  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t2845361159  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResourceInfo_t4013605874  m_Items[1];

public:
	inline ResourceInfo_t4013605874  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResourceInfo_t4013605874 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResourceInfo_t4013605874  value)
	{
		m_Items[index] = value;
	}
};
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t1070225452  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResourceCacheItem_t2113902833  m_Items[1];

public:
	inline ResourceCacheItem_t2113902833  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResourceCacheItem_t2113902833 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResourceCacheItem_t2113902833  value)
	{
		m_Items[index] = value;
	}
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t3402113153  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t2063418880  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t856208126  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Header_t1689611527 * m_Items[1];

public:
	inline Header_t1689611527 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Header_t1689611527 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Header_t1689611527 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t3684998849  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.DateTime[]
struct DateTimeU5BU5D_t194158294  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DateTime_t4283661327  m_Items[1];

public:
	inline DateTime_t4283661327  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DateTime_t4283661327 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DateTime_t4283661327  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t212673467  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t1571248115  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Decimal[]
struct DecimalU5BU5D_t183431518  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Decimal_t1954350631  m_Items[1];

public:
	inline Decimal_t1954350631  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Decimal_t1954350631 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Decimal_t1954350631  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t201946691  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t1560521339  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.TimeSpan[]
struct TimeSpanU5BU5D_t3241447114  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TimeSpan_t413522987  m_Items[1];

public:
	inline TimeSpan_t413522987  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TimeSpan_t413522987 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TimeSpan_t413522987  value)
	{
		m_Items[index] = value;
	}
};
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t3259962287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t323569639  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t172815715  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.MonoType[]
struct MonoTypeU5BU5D_t1750410988  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MonoType_t * m_Items[1];

public:
	inline MonoType_t * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MonoType_t ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MonoType_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[,]
struct ByteU5BU2CU5D_t4260760470  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
	inline uint8_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, uint8_t value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
	}
};
// System.Security.Permissions.KeyContainerPermissionAccessEntry[]
struct KeyContainerPermissionAccessEntryU5BU5D_t356632218  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyContainerPermissionAccessEntry_t2372748443 * m_Items[1];

public:
	inline KeyContainerPermissionAccessEntry_t2372748443 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyContainerPermissionAccessEntry_t2372748443 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyContainerPermissionAccessEntry_t2372748443 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t3819180239  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StrongName_t2878058698 * m_Items[1];

public:
	inline StrongName_t2878058698 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StrongName_t2878058698 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StrongName_t2878058698 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Policy.CodeConnectAccess[]
struct CodeConnectAccessU5BU5D_t3404668630  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CodeConnectAccess_t2328951823 * m_Items[1];

public:
	inline CodeConnectAccess_t2328951823 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CodeConnectAccess_t2328951823 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CodeConnectAccess_t2328951823 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.EncodingInfo[]
struct EncodingInfoU5BU5D_t3605083358  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EncodingInfo_t1898473639 * m_Items[1];

public:
	inline EncodingInfo_t1898473639 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EncodingInfo_t1898473639 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EncodingInfo_t1898473639 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t3755685144  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WaitHandle_t1661568373 * m_Items[1];

public:
	inline WaitHandle_t1661568373 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WaitHandle_t1661568373 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WaitHandle_t1661568373 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IDisposable[]
struct IDisposableU5BU5D_t1854239782  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1944668977  m_Items[1];

public:
	inline KeyValuePair_2_t1944668977  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1944668977 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1944668977  value)
	{
		m_Items[index] = value;
	}
};
// System.SystemException[]
struct SystemExceptionU5BU5D_t1537348531  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SystemException_t4206535862 * m_Items[1];

public:
	inline SystemException_t4206535862 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SystemException_t4206535862 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SystemException_t4206535862 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Exception[]
struct ExceptionU5BU5D_t1608523752  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Exception_t3991598821 * m_Items[1];

public:
	inline Exception_t3991598821 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Exception_t3991598821 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Exception_t3991598821 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Exception[]
struct _ExceptionU5BU5D_t2150415519  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t2607313013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2758969756  m_Items[1];

public:
	inline KeyValuePair_2_t2758969756  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2758969756 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2758969756  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t2421305976  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ByteU5BU5D_t4260760469* m_Items[1];

public:
	inline ByteU5BU5D_t4260760469* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ByteU5BU5D_t4260760469** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ByteU5BU5D_t4260760469* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t3275955254  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Certificate_t3076817455 * m_Items[1];

public:
	inline X509Certificate_t3076817455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Certificate_t3076817455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Certificate_t3076817455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t4214513038  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Attribute[]
struct AttributeU5BU5D_t4055800263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Attribute_t2523058482 * m_Items[1];

public:
	inline Attribute_t2523058482 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Attribute_t2523058482 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Attribute_t2523058482 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t302724734  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Attribute[][]
struct AttributeU5BU5DU5BU5D_t1538489150  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AttributeU5BU5D_t4055800263* m_Items[1];

public:
	inline AttributeU5BU5D_t4055800263* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AttributeU5BU5D_t4055800263** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AttributeU5BU5D_t4055800263* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>>[]
struct KeyValuePair_2U5BU5D_t454461420  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1421923377  m_Items[1];

public:
	inline KeyValuePair_2_t1421923377  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1421923377 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1421923377  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.ComponentModel.WeakObjectWrapper,System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>>[]
struct KeyValuePair_2U5BU5D_t1996120960  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1472936237  m_Items[1];

public:
	inline KeyValuePair_2_t1472936237  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1472936237 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1472936237  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Net.NetworkInformation.LinuxNetworkInterface>[]
struct KeyValuePair_2U5BU5D_t3298403536  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1627469341  m_Items[1];

public:
	inline KeyValuePair_2_t1627469341  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1627469341 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1627469341  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Net.NetworkInformation.MacOsNetworkInterface>[]
struct KeyValuePair_2U5BU5D_t3919315669  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2571132604  m_Items[1];

public:
	inline KeyValuePair_2_t2571132604  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2571132604 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2571132604  value)
	{
		m_Items[index] = value;
	}
};
// System.ArraySegment`1<System.Byte>[]
struct ArraySegment_1U5BU5D_t3490338713  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ArraySegment_1_t2188033608  m_Items[1];

public:
	inline ArraySegment_1_t2188033608  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArraySegment_1_t2188033608 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArraySegment_1_t2188033608  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t535859925  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2545618620  m_Items[1];

public:
	inline KeyValuePair_2_t2545618620  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2545618620 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2545618620  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3070342615  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1195997794  m_Items[1];

public:
	inline KeyValuePair_2_t1195997794  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1195997794 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1195997794  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.DynamicMethod[]
struct DynamicMethodU5BU5D_t3537238387  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DynamicMethod_t2315379190 * m_Items[1];

public:
	inline DynamicMethod_t2315379190 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DynamicMethod_t2315379190 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DynamicMethod_t2315379190 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1127117024  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1049882445  m_Items[1];

public:
	inline KeyValuePair_2_t1049882445  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1049882445 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1049882445  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>[]
struct KeyValuePair_2U5BU5D_t2975495874  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2164509075  m_Items[1];

public:
	inline KeyValuePair_2_t2164509075  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2164509075 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2164509075  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t784259375  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Label_t2268465130  m_Items[1];

public:
	inline Label_t2268465130  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Label_t2268465130 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Label_t2268465130  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t3668042644  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t726430633  m_Items[1];

public:
	inline KeyValuePair_2_t726430633  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t726430633 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t726430633  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3299892981  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4066860316  m_Items[1];

public:
	inline KeyValuePair_2_t4066860316  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4066860316 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4066860316  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t1605285297  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t775952400  m_Items[1];

public:
	inline KeyValuePair_2_t775952400  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t775952400 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t775952400  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t179694251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3710127902  m_Items[1];

public:
	inline KeyValuePair_2_t3710127902  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3710127902 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3710127902  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3614328797  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1919813716  m_Items[1];

public:
	inline KeyValuePair_2_t1919813716  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1919813716 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1919813716  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3387041138  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1289591971  m_Items[1];

public:
	inline KeyValuePair_2_t1289591971  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1289591971 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1289591971  value)
	{
		m_Items[index] = value;
	}
};
// System.IEquatable`1<HutongGames.PlayMaker.FsmTransition>[]
struct IEquatable_1U5BU5D_t2195300707  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
struct KeyValuePair_2U5BU5D_t2953046759  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3582344850  m_Items[1];

public:
	inline KeyValuePair_2_t3582344850  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3582344850 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3582344850  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.FieldInfo[][]
struct FieldInfoU5BU5DU5BU5D_t1162477662  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FieldInfoU5BU5D_t2567562023* m_Items[1];

public:
	inline FieldInfoU5BU5D_t2567562023* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FieldInfoU5BU5D_t2567562023** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FieldInfoU5BU5D_t2567562023* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>[]
struct KeyValuePair_2U5BU5D_t4241850603  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2571765214  m_Items[1];

public:
	inline KeyValuePair_2_t2571765214  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2571765214 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2571765214  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2015253466  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1158041691  m_Items[1];

public:
	inline KeyValuePair_2_t1158041691  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1158041691 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1158041691  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3618423950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1080897207  m_Items[1];

public:
	inline KeyValuePair_2_t1080897207  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1080897207 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1080897207  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2235185101  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3033809700  m_Items[1];

public:
	inline KeyValuePair_2_t3033809700  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3033809700 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3033809700  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>[]
struct KeyValuePair_2U5BU5D_t2982628982  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3617918191  m_Items[1];

public:
	inline KeyValuePair_2_t3617918191  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3617918191 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3617918191  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Object>[]
struct KeyValuePair_2U5BU5D_t4058114434  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1188478419  m_Items[1];

public:
	inline KeyValuePair_2_t1188478419  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1188478419 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1188478419  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>[]
struct KeyValuePair_2U5BU5D_t711965110  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t908399279  m_Items[1];

public:
	inline KeyValuePair_2_t908399279  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t908399279 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t908399279  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>[]
struct KeyValuePair_2U5BU5D_t3675014586  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2656869371  m_Items[1];

public:
	inline KeyValuePair_2_t2656869371  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2656869371 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2656869371  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>[]
struct KeyValuePair_2U5BU5D_t508445813  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t996820380  m_Items[1];

public:
	inline KeyValuePair_2_t996820380  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t996820380 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t996820380  value)
	{
		m_Items[index] = value;
	}
};
// System.String[][]
struct StringU5BU5DU5BU5D_t3538327001  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StringU5BU5D_t4054002952* m_Items[1];

public:
	inline StringU5BU5D_t4054002952* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StringU5BU5D_t4054002952** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StringU5BU5D_t4054002952* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>[]
struct KeyValuePair_2U5BU5D_t1809717809  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2540055952  m_Items[1];

public:
	inline KeyValuePair_2_t2540055952  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2540055952 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2540055952  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Int32U5BU5D_t3230847821* m_Items[1];

public:
	inline Int32U5BU5D_t3230847821* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Int32U5BU5D_t3230847821** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Int32U5BU5D_t3230847821* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,ZXing.Common.CharacterSetECI>[]
struct KeyValuePair_2U5BU5D_t3721638660  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2438309113  m_Items[1];

public:
	inline KeyValuePair_2_t2438309113  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2438309113 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2438309113  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>[]
struct KeyValuePair_2U5BU5D_t1144441853  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3261464244  m_Items[1];

public:
	inline KeyValuePair_2_t3261464244  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3261464244 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3261464244  value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[][]
struct Int64U5BU5DU5BU5D_t1231312615  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Int64U5BU5D_t2174042770* m_Items[1];

public:
	inline Int64U5BU5D_t2174042770* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Int64U5BU5D_t2174042770** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Int64U5BU5D_t2174042770* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2190341993  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3309166264  m_Items[1];

public:
	inline KeyValuePair_2_t3309166264  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3309166264 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3309166264  value)
	{
		m_Items[index] = value;
	}
};
// System.Char[][]
struct CharU5BU5DU5BU5D_t2611876246  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CharU5BU5D_t3324145743* m_Items[1];

public:
	inline CharU5BU5D_t3324145743* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CharU5BU5D_t3324145743** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CharU5BU5D_t3324145743* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.StringBuilder[]
struct StringBuilderU5BU5D_t2178608709  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StringBuilder_t243639308 * m_Items[1];

public:
	inline StringBuilder_t243639308 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StringBuilder_t243639308 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StringBuilder_t243639308 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[][]
struct ObjectU5BU5DU5BU5D_t1003844311  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectU5BU5D_t1108656482* m_Items[1];

public:
	inline ObjectU5BU5D_t1108656482* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjectU5BU5D_t1108656482** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjectU5BU5D_t1108656482* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object[]>[]
struct KeyValuePair_2U5BU5D_t617884003  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1827855558  m_Items[1];

public:
	inline KeyValuePair_2_t1827855558  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1827855558 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1827855558  value)
	{
		m_Items[index] = value;
	}
};
// System.SByte[][]
struct SByteU5BU5DU5BU5D_t694789605  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SByteU5BU5D_t2505034988* m_Items[1];

public:
	inline SByteU5BU5D_t2505034988* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SByteU5BU5D_t2505034988** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SByteU5BU5D_t2505034988* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IEquatable`1<BigIntegerLibrary.BigInteger>[]
struct IEquatable_1U5BU5D_t1304105704  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IComparable`1<BigIntegerLibrary.BigInteger>[]
struct IComparable_1U5BU5D_t4240498352  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[][]
struct SingleU5BU5DU5BU5D_t1565445624  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SingleU5BU5D_t2316563989* m_Items[1];

public:
	inline SingleU5BU5D_t2316563989* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SingleU5BU5D_t2316563989** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SingleU5BU5D_t2316563989* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t2854111117  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1857232484  m_Items[1];

public:
	inline KeyValuePair_2_t1857232484  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1857232484 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1857232484  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct KeyValuePair_2U5BU5D_t739324929  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1718082560  m_Items[1];

public:
	inline KeyValuePair_2_t1718082560  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1718082560 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1718082560  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct KeyValuePair_2U5BU5D_t215696743  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4089910802  m_Items[1];

public:
	inline KeyValuePair_2_t4089910802  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4089910802 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4089910802  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct KeyValuePair_2U5BU5D_t2104453161  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3677105400  m_Items[1];

public:
	inline KeyValuePair_2_t3677105400  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3677105400 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3677105400  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct KeyValuePair_2U5BU5D_t953637457  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t600250352  m_Items[1];

public:
	inline KeyValuePair_2_t600250352  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t600250352 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t600250352  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct KeyValuePair_2U5BU5D_t3945570920  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t512738917  m_Items[1];

public:
	inline KeyValuePair_2_t512738917  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t512738917 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t512738917  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t2176173772  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2093487825  m_Items[1];

public:
	inline KeyValuePair_2_t2093487825  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2093487825 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2093487825  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3881022415  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t28871114  m_Items[1];

public:
	inline KeyValuePair_2_t28871114  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t28871114 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t28871114  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct KeyValuePair_2U5BU5D_t2357612551  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t975906802  m_Items[1];

public:
	inline KeyValuePair_2_t975906802  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t975906802 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t975906802  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t1741998669  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t441991844  m_Items[1];

public:
	inline KeyValuePair_2_t441991844  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t441991844 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t441991844  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct List_1U5BU5D_t978320554  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1914133451 * m_Items[1];

public:
	inline List_1_t1914133451 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1914133451 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1914133451 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t592360246  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2633332527  m_Items[1];

public:
	inline KeyValuePair_2_t2633332527  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2633332527 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2633332527  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct KeyValuePair_2U5BU5D_t379311233  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2990433664  m_Items[1];

public:
	inline KeyValuePair_2_t2990433664  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2990433664 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2990433664  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t248057924  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t38412473  m_Items[1];

public:
	inline KeyValuePair_2_t38412473  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t38412473 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t38412473  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct KeyValuePair_2U5BU5D_t37328251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2061353102  m_Items[1];

public:
	inline KeyValuePair_2_t2061353102  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2061353102 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2061353102  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t2430214794  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1189512043  m_Items[1];

public:
	inline KeyValuePair_2_t1189512043  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1189512043 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1189512043  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct KeyValuePair_2U5BU5D_t3710706258  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4075600195  m_Items[1];

public:
	inline KeyValuePair_2_t4075600195  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4075600195 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4075600195  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t3465169872  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t291920669  m_Items[1];

public:
	inline KeyValuePair_2_t291920669  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t291920669 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t291920669  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t3165621385  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t355424280  m_Items[1];

public:
	inline KeyValuePair_2_t355424280  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t355424280 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t355424280  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct KeyValuePair_2U5BU5D_t3843247366  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3416499615  m_Items[1];

public:
	inline KeyValuePair_2_t3416499615  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3416499615 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3416499615  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t4244852621  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4030510692  m_Items[1];

public:
	inline KeyValuePair_2_t4030510692  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4030510692 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4030510692  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t2484368015  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2680889866  m_Items[1];

public:
	inline KeyValuePair_2_t2680889866  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2680889866 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2680889866  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3404719220  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1087282249  m_Items[1];

public:
	inline KeyValuePair_2_t1087282249  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1087282249 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1087282249  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct List_1U5BU5D_t1765424729  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2498711176 * m_Items[1];

public:
	inline List_1_t2498711176 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2498711176 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2498711176 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<System.Object>[]
struct List_1U5BU5D_t2536214866  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1244034627 * m_Items[1];

public:
	inline List_1_t1244034627 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1244034627 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1244034627 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct List_1U5BU5D_t2091470034  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t574734531 * m_Items[1];

public:
	inline List_1_t574734531 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t574734531 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t574734531 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct KeyValuePair_2U5BU5D_t3619361353  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1744794968  m_Items[1];

public:
	inline KeyValuePair_2_t1744794968  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1744794968 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1744794968  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct List_1U5BU5D_t36510821  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t4095326316 * m_Items[1];

public:
	inline List_1_t4095326316 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t4095326316 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t4095326316 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct List_1U5BU5D_t931498100  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1377224777 * m_Items[1];

public:
	inline List_1_t1377224777 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1377224777 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1377224777 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct KeyValuePair_2U5BU5D_t512189406  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1496360359  m_Items[1];

public:
	inline KeyValuePair_2_t1496360359  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1496360359 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1496360359  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct KeyValuePair_2U5BU5D_t274426361  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3226014568  m_Items[1];

public:
	inline KeyValuePair_2_t3226014568  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3226014568 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3226014568  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4265808762  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1354567995  m_Items[1];

public:
	inline KeyValuePair_2_t1354567995  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1354567995 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1354567995  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t197004907  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3983107678  m_Items[1];

public:
	inline KeyValuePair_2_t3983107678  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3983107678 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3983107678  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.UI.Mask>[]
struct List_1U5BU5D_t1071654105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1377012232 * m_Items[1];

public:
	inline List_1_t1377012232 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1377012232 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1377012232 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>[]
struct List_1U5BU5D_t1495120811  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t430297630 * m_Items[1];

public:
	inline List_1_t430297630 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t430297630 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t430297630 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
struct KeyValuePair_2U5BU5D_t278621004  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1329917009  m_Items[1];

public:
	inline KeyValuePair_2_t1329917009  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1329917009 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1329917009  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct List_1U5BU5D_t1642958995  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1355284822 * m_Items[1];

public:
	inline List_1_t1355284822 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1355284822 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1355284822 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
struct List_1U5BU5D_t93358041  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1967039240 * m_Items[1];

public:
	inline List_1_t1967039240 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1967039240 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1967039240 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
struct List_1U5BU5D_t1156771256  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1355284821 * m_Items[1];

public:
	inline List_1_t1355284821 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1355284821 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1355284821 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
struct List_1U5BU5D_t2129146734  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1355284823 * m_Items[1];

public:
	inline List_1_t1355284823 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1355284823 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1355284823 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_t363438909  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t2522024052 * m_Items[1];

public:
	inline List_1_t2522024052 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t2522024052 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t2522024052 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct List_1U5BU5D_t3223949765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1317283468 * m_Items[1];

public:
	inline List_1_t1317283468 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1317283468 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1317283468 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<UnityEngine.Sprite>[]
struct List_1U5BU5D_t4188869284  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t272385497 * m_Items[1];

public:
	inline List_1_t272385497 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t272385497 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t272385497 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<UnityEngine.Sprite>>[]
struct KeyValuePair_2U5BU5D_t3802908976  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t991584573  m_Items[1];

public:
	inline KeyValuePair_2_t991584573  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t991584573 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t991584573  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t722696174  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t595048151  m_Items[1];

public:
	inline KeyValuePair_2_t595048151  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t595048151 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t595048151  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>[]
struct List_1U5BU5D_t1690431389  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t4040371092 * m_Items[1];

public:
	inline List_1_t4040371092 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t4040371092 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t4040371092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>>[]
struct KeyValuePair_2U5BU5D_t3881667888  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3936415037  m_Items[1];

public:
	inline KeyValuePair_2_t3936415037  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3936415037 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3936415037  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>[]
struct List_1U5BU5D_t1070555627  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1807153374 * m_Items[1];

public:
	inline List_1_t1807153374 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t1807153374 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t1807153374 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>[]
struct KeyValuePair_2U5BU5D_t3261792126  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1703197319  m_Items[1];

public:
	inline KeyValuePair_2_t1703197319  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1703197319 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1703197319  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>[]
struct KeyValuePair_2U5BU5D_t1110487188  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3830311593  m_Items[1];

public:
	inline KeyValuePair_2_t3830311593  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3830311593 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3830311593  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>[]
struct KeyValuePair_2U5BU5D_t1644974049  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3430729120  m_Items[1];

public:
	inline KeyValuePair_2_t3430729120  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3430729120 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3430729120  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>[]
struct KeyValuePair_2U5BU5D_t558378251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3570725950  m_Items[1];

public:
	inline KeyValuePair_2_t3570725950  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3570725950 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3570725950  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ConfigurableListenerEventInterface>[]
struct KeyValuePair_2U5BU5D_t515773329  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2704828336  m_Items[1];

public:
	inline KeyValuePair_2_t2704828336  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2704828336 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2704828336  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,OnOffListenerEventInterface>[]
struct KeyValuePair_2U5BU5D_t1433533438  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t384374791  m_Items[1];

public:
	inline KeyValuePair_2_t384374791  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t384374791 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t384374791  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.List`1<MagicTV.vo.BundleVO>[]
struct List_1U5BU5D_t3590450484  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t3352703625 * m_Items[1];

public:
	inline List_1_t3352703625 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline List_1_t3352703625 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, List_1_t3352703625 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>[]
struct KeyValuePair_2U5BU5D_t3204490176  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4071902701  m_Items[1];

public:
	inline KeyValuePair_2_t4071902701  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4071902701 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4071902701  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.vo.BundleVO[]>[]
struct KeyValuePair_2U5BU5D_t4279093177  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2882091176  m_Items[1];

public:
	inline KeyValuePair_2_t2882091176  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2882091176 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2882091176  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.globals.InAppAnalytics>[]
struct KeyValuePair_2U5BU5D_t1148082867  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3035933110  m_Items[1];

public:
	inline KeyValuePair_2_t3035933110  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3035933110 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3035933110  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>[]
struct KeyValuePair_2U5BU5D_t3344958279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2116912562  m_Items[1];

public:
	inline KeyValuePair_2_t2116912562  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2116912562 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2116912562  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>[]
struct KeyValuePair_2U5BU5D_t2038243499  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2409664798  m_Items[1];

public:
	inline KeyValuePair_2_t2409664798  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2409664798 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2409664798  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>[]
struct KeyValuePair_2U5BU5D_t3763970267  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t941084846  m_Items[1];

public:
	inline KeyValuePair_2_t941084846  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t941084846 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t941084846  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>[]
struct KeyValuePair_2U5BU5D_t1352210207  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1845333178  m_Items[1];

public:
	inline KeyValuePair_2_t1845333178  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1845333178 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1845333178  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>[]
struct KeyValuePair_2U5BU5D_t4165420092  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2599727649  m_Items[1];

public:
	inline KeyValuePair_2_t2599727649  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2599727649 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2599727649  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AssetBundle>[]
struct KeyValuePair_2U5BU5D_t3137476133  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2790158764  m_Items[1];

public:
	inline KeyValuePair_2_t2790158764  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2790158764 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2790158764  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>[]
struct KeyValuePair_2U5BU5D_t1464939079  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4186358450  m_Items[1];

public:
	inline KeyValuePair_2_t4186358450  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4186358450 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4186358450  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>[]
struct KeyValuePair_2U5BU5D_t3569290535  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3490093394  m_Items[1];

public:
	inline KeyValuePair_2_t3490093394  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3490093394 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3490093394  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
struct KeyValuePair_2U5BU5D_t3691088287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2065771578  m_Items[1];

public:
	inline KeyValuePair_2_t2065771578  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2065771578 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2065771578  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Single>[]
struct KeyValuePair_2U5BU5D_t1930603681  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t716150752  m_Items[1];

public:
	inline KeyValuePair_2_t716150752  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t716150752 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t716150752  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>[]
struct KeyValuePair_2U5BU5D_t949261646  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2479660279  m_Items[1];

public:
	inline KeyValuePair_2_t2479660279  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2479660279 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2479660279  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.GenericParameterEventDispatcher>[]
struct KeyValuePair_2U5BU5D_t908022084  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t235549625  m_Items[1];

public:
	inline KeyValuePair_2_t235549625  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t235549625 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t235549625  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>[]
struct KeyValuePair_2U5BU5D_t434536091  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1458533870  m_Items[1];

public:
	inline KeyValuePair_2_t1458533870  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1458533870 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1458533870  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.abstracts.InAppAbstract>[]
struct KeyValuePair_2U5BU5D_t26389765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1101872204  m_Items[1];

public:
	inline KeyValuePair_2_t1101872204  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1101872204 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1101872204  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>[]
struct KeyValuePair_2U5BU5D_t3408682159  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2434214506  m_Items[1];

public:
	inline KeyValuePair_2_t2434214506  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2434214506 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2434214506  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>[]
struct KeyValuePair_2U5BU5D_t4221170483  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1840487222  m_Items[1];

public:
	inline KeyValuePair_2_t1840487222  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1840487222 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1840487222  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>[]
struct KeyValuePair_2U5BU5D_t3452080085  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1832195516  m_Items[1];

public:
	inline KeyValuePair_2_t1832195516  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1832195516 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1832195516  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>[]
struct KeyValuePair_2U5BU5D_t1597826081  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4078114400  m_Items[1];

public:
	inline KeyValuePair_2_t4078114400  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4078114400 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4078114400  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.ArrayMetadata>[]
struct KeyValuePair_2U5BU5D_t861961432  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t4062546101  m_Items[1];

public:
	inline KeyValuePair_2_t4062546101  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t4062546101 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t4062546101  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>[]
struct IDictionary_2U5BU5D_t1303149742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>[]
struct KeyValuePair_2U5BU5D_t87555387  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t6235086  m_Items[1];

public:
	inline KeyValuePair_2_t6235086  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t6235086 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t6235086  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.ObjectMetadata>[]
struct KeyValuePair_2U5BU5D_t3302674724  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2013497689  m_Items[1];

public:
	inline KeyValuePair_2_t2013497689  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2013497689 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2013497689  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IList`1<LitJson.PropertyMetadata>[]
struct IList_1U5BU5D_t2547396634  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>[]
struct KeyValuePair_2U5BU5D_t1331802279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2470517714  m_Items[1];

public:
	inline KeyValuePair_2_t2470517714  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2470517714 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2470517714  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.ExporterFunc>[]
struct KeyValuePair_2U5BU5D_t250872177  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3334563664  m_Items[1];

public:
	inline KeyValuePair_2_t3334563664  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3334563664 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3334563664  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>[]
struct IDictionary_2U5BU5D_t212819297  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>[]
struct KeyValuePair_2U5BU5D_t3292192238  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1825818839  m_Items[1];

public:
	inline KeyValuePair_2_t1825818839  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1825818839 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1825818839  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,LitJson.PropertyMetadata>[]
struct KeyValuePair_2U5BU5D_t2460685877  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t490866396  m_Items[1];

public:
	inline KeyValuePair_2_t490866396  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t490866396 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t490866396  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.MethodInfo>[]
struct KeyValuePair_2U5BU5D_t1608772009  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t322939256  m_Items[1];

public:
	inline KeyValuePair_2_t322939256  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t322939256 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t322939256  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.ImporterFunc>[]
struct KeyValuePair_2U5BU5D_t518441564  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2142523009  m_Items[1];

public:
	inline KeyValuePair_2_t2142523009  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2142523009 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2142523009  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>[]
struct IDictionary_2U5BU5D_t3706170744  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>[]
struct KeyValuePair_2U5BU5D_t1602439947  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2702028350  m_Items[1];

public:
	inline KeyValuePair_2_t2702028350  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2702028350 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2702028350  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32[]>[]
struct KeyValuePair_2U5BU5D_t4011793011  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t3126891766  m_Items[1];

public:
	inline KeyValuePair_2_t3126891766  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t3126891766 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t3126891766  value)
	{
		m_Items[index] = value;
	}
};
// System.MulticastDelegate[]
struct MulticastDelegateU5BU5D_t1432943266  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MulticastDelegate_t3389745971 * m_Items[1];

public:
	inline MulticastDelegate_t3389745971 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MulticastDelegate_t3389745971 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MulticastDelegate_t3389745971 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,HutongGames.PlayMaker.FsmState>[]
struct KeyValuePair_2U5BU5D_t540728565  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2042378012  m_Items[1];

public:
	inline KeyValuePair_2_t2042378012  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2042378012 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2042378012  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>[]
struct KeyValuePair_2U5BU5D_t2276148740  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t98913785  m_Items[1];

public:
	inline KeyValuePair_2_t98913785  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t98913785 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t98913785  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.RectTransform>[]
struct KeyValuePair_2U5BU5D_t1483920382  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t868687879  m_Items[1];

public:
	inline KeyValuePair_2_t868687879  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t868687879 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t868687879  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Action>[]
struct KeyValuePair_2U5BU5D_t1257183035  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t195465678  m_Items[1];

public:
	inline KeyValuePair_2_t195465678  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t195465678 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t195465678  value)
	{
		m_Items[index] = value;
	}
};
// System.Action`1<UniWebViewNativeResultPayload>[]
struct Action_1U5BU5D_t127157316  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_1_t1392508089 * m_Items[1];

public:
	inline Action_1_t1392508089 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_1_t1392508089 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_1_t1392508089 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>[]
struct KeyValuePair_2U5BU5D_t4036164304  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2111707165  m_Items[1];

public:
	inline KeyValuePair_2_t2111707165  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t2111707165 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2111707165  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>[]
struct KeyValuePair_2U5BU5D_t3896632009  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t1514770136  m_Items[1];

public:
	inline KeyValuePair_2_t1514770136  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValuePair_2_t1514770136 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t1514770136  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Hashtable[]
struct HashtableU5BU5D_t3840844799  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Hashtable_t1407064410 * m_Items[1];

public:
	inline Hashtable_t1407064410 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Hashtable_t1407064410 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Hashtable_t1407064410 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
