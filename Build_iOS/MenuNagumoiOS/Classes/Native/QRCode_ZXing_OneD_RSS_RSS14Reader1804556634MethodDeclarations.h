﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.RSS14Reader
struct RSS14Reader_t1804556634;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Collections.Generic.IList`1<ZXing.OneD.RSS.Pair>
struct IList_1_t3740056835;
// ZXing.OneD.RSS.Pair
struct Pair_t1045409632;
// ZXing.OneD.RSS.DataCharacter
struct DataCharacter_t770728801;
// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "QRCode_ZXing_OneD_RSS_Pair1045409632.h"
#include "QRCode_ZXing_OneD_RSS_FinderPattern3366792524.h"

// System.Void ZXing.OneD.RSS.RSS14Reader::.ctor()
extern "C"  void RSS14Reader__ctor_m472301093 (RSS14Reader_t1804556634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.RSS.RSS14Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * RSS14Reader_decodeRow_m2003387932 (RSS14Reader_t1804556634 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.RSS14Reader::addOrTally(System.Collections.Generic.IList`1<ZXing.OneD.RSS.Pair>,ZXing.OneD.RSS.Pair)
extern "C"  void RSS14Reader_addOrTally_m2555971910 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___possiblePairs0, Pair_t1045409632 * ___pair1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.RSS14Reader::reset()
extern "C"  void RSS14Reader_reset_m749048050 (RSS14Reader_t1804556634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.RSS.RSS14Reader::constructResult(ZXing.OneD.RSS.Pair,ZXing.OneD.RSS.Pair)
extern "C"  Result_t2610723219 * RSS14Reader_constructResult_m453682469 (Il2CppObject * __this /* static, unused */, Pair_t1045409632 * ___leftPair0, Pair_t1045409632 * ___rightPair1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.RSS14Reader::checkChecksum(ZXing.OneD.RSS.Pair,ZXing.OneD.RSS.Pair)
extern "C"  bool RSS14Reader_checkChecksum_m719004428 (Il2CppObject * __this /* static, unused */, Pair_t1045409632 * ___leftPair0, Pair_t1045409632 * ___rightPair1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Pair ZXing.OneD.RSS.RSS14Reader::decodePair(ZXing.Common.BitArray,System.Boolean,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Pair_t1045409632 * RSS14Reader_decodePair_m1110347204 (RSS14Reader_t1804556634 * __this, BitArray_t4163851164 * ___row0, bool ___right1, int32_t ___rowNumber2, Il2CppObject* ___hints3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.RSS14Reader::decodeDataCharacter(ZXing.Common.BitArray,ZXing.OneD.RSS.FinderPattern,System.Boolean)
extern "C"  DataCharacter_t770728801 * RSS14Reader_decodeDataCharacter_m602263608 (RSS14Reader_t1804556634 * __this, BitArray_t4163851164 * ___row0, FinderPattern_t3366792524 * ___pattern1, bool ___outsideChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.RSS.RSS14Reader::findFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean)
extern "C"  Int32U5BU5D_t3230847821* RSS14Reader_findFinderPattern_m2551296875 (RSS14Reader_t1804556634 * __this, BitArray_t4163851164 * ___row0, int32_t ___rowOffset1, bool ___rightFinderPattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.RSS14Reader::parseFoundFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[])
extern "C"  FinderPattern_t3366792524 * RSS14Reader_parseFoundFinderPattern_m1430700267 (RSS14Reader_t1804556634 * __this, BitArray_t4163851164 * ___row0, int32_t ___rowNumber1, bool ___right2, Int32U5BU5D_t3230847821* ___startEnd3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.RSS14Reader::adjustOddEvenCounts(System.Boolean,System.Int32)
extern "C"  bool RSS14Reader_adjustOddEvenCounts_m4243583859 (RSS14Reader_t1804556634 * __this, bool ___outsideChar0, int32_t ___numModules1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.RSS14Reader::.cctor()
extern "C"  void RSS14Reader__cctor_m1274335784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
