﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidPermissionsManager
struct AndroidPermissionsManager_t2951777080;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// AndroidPermissionCallback
struct AndroidPermissionCallback_t1784561635;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AndroidPermissionCallback1784561635.h"

// System.Void AndroidPermissionsManager::.ctor()
extern "C"  void AndroidPermissionsManager__ctor_m2868959539 (AndroidPermissionsManager_t2951777080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject AndroidPermissionsManager::GetActivity()
extern "C"  AndroidJavaObject_t2362096582 * AndroidPermissionsManager_GetActivity_m1601074704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject AndroidPermissionsManager::GetPermissionsService()
extern "C"  AndroidJavaObject_t2362096582 * AndroidPermissionsManager_GetPermissionsService_m3201550354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidPermissionsManager::IsPermissionGranted(System.String)
extern "C"  bool AndroidPermissionsManager_IsPermissionGranted_m639313915 (Il2CppObject * __this /* static, unused */, String_t* ___permissionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionsManager::RequestPermission(System.String[],AndroidPermissionCallback)
extern "C"  void AndroidPermissionsManager_RequestPermission_m3772237018 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___permissionNames0, AndroidPermissionCallback_t1784561635 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
