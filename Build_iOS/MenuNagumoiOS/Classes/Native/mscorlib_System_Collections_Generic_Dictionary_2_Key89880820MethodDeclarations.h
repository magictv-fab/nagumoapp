﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct KeyCollection_t89880820;
// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Collections.Generic.IEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>
struct IEnumerator_1_t2919343562;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.Aztec.Internal.Decoder/Table[]
struct TableU5BU5D_t1820856876;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3373024719.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4050008939_gshared (KeyCollection_t89880820 * __this, Dictionary_2_t2758088665 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4050008939(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t89880820 *, Dictionary_2_t2758088665 *, const MethodInfo*))KeyCollection__ctor_m4050008939_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m565995275_gshared (KeyCollection_t89880820 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m565995275(__this, ___item0, method) ((  void (*) (KeyCollection_t89880820 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m565995275_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m594989890_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m594989890(__this, method) ((  void (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m594989890_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2185592867_gshared (KeyCollection_t89880820 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2185592867(__this, ___item0, method) ((  bool (*) (KeyCollection_t89880820 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2185592867_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3341393480_gshared (KeyCollection_t89880820 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3341393480(__this, ___item0, method) ((  bool (*) (KeyCollection_t89880820 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3341393480_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1777894484_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1777894484(__this, method) ((  Il2CppObject* (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1777894484_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3741050036_gshared (KeyCollection_t89880820 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3741050036(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t89880820 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3741050036_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1777573059_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1777573059(__this, method) ((  Il2CppObject * (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1777573059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2996796100_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2996796100(__this, method) ((  bool (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2996796100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4256393334_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4256393334(__this, method) ((  bool (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4256393334_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1335749608_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1335749608(__this, method) ((  Il2CppObject * (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1335749608_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3666087456_gshared (KeyCollection_t89880820 * __this, TableU5BU5D_t1820856876* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3666087456(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t89880820 *, TableU5BU5D_t1820856876*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3666087456_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3373024719  KeyCollection_GetEnumerator_m596646573_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m596646573(__this, method) ((  Enumerator_t3373024719  (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_GetEnumerator_m596646573_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1517854896_gshared (KeyCollection_t89880820 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1517854896(__this, method) ((  int32_t (*) (KeyCollection_t89880820 *, const MethodInfo*))KeyCollection_get_Count_m1517854896_gshared)(__this, method)
