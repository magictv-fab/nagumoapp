﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingScreenScript
struct  LoadingScreenScript_t2063433619  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D LoadingScreenScript::Loading_Screen
	Texture2D_t3884108195 * ___Loading_Screen_2;

public:
	inline static int32_t get_offset_of_Loading_Screen_2() { return static_cast<int32_t>(offsetof(LoadingScreenScript_t2063433619, ___Loading_Screen_2)); }
	inline Texture2D_t3884108195 * get_Loading_Screen_2() const { return ___Loading_Screen_2; }
	inline Texture2D_t3884108195 ** get_address_of_Loading_Screen_2() { return &___Loading_Screen_2; }
	inline void set_Loading_Screen_2(Texture2D_t3884108195 * value)
	{
		___Loading_Screen_2 = value;
		Il2CppCodeGenWriteBarrier(&___Loading_Screen_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
