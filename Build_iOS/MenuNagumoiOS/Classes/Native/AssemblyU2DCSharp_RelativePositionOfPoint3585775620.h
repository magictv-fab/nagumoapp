﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// RelativePositionOfPoint
struct RelativePositionOfPoint_t3585775620;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RelativePositionOfPoint
struct  RelativePositionOfPoint_t3585775620  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject RelativePositionOfPoint::referenceInternalPoint
	GameObject_t3674682005 * ___referenceInternalPoint_2;
	// UnityEngine.Vector3 RelativePositionOfPoint::DebugSetRelativePoint
	Vector3_t4282066566  ___DebugSetRelativePoint_4;
	// System.Single RelativePositionOfPoint::DebugPercentDistance
	float ___DebugPercentDistance_5;
	// UnityEngine.Vector3 RelativePositionOfPoint::DebugGlobalPointResult
	Vector3_t4282066566  ___DebugGlobalPointResult_6;
	// System.Boolean RelativePositionOfPoint::DebugCalcNow
	bool ___DebugCalcNow_7;
	// UnityEngine.Vector3 RelativePositionOfPoint::DebugRecivedVector
	Vector3_t4282066566  ___DebugRecivedVector_8;
	// System.Single RelativePositionOfPoint::DebugPercent
	float ___DebugPercent_9;

public:
	inline static int32_t get_offset_of_referenceInternalPoint_2() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___referenceInternalPoint_2)); }
	inline GameObject_t3674682005 * get_referenceInternalPoint_2() const { return ___referenceInternalPoint_2; }
	inline GameObject_t3674682005 ** get_address_of_referenceInternalPoint_2() { return &___referenceInternalPoint_2; }
	inline void set_referenceInternalPoint_2(GameObject_t3674682005 * value)
	{
		___referenceInternalPoint_2 = value;
		Il2CppCodeGenWriteBarrier(&___referenceInternalPoint_2, value);
	}

	inline static int32_t get_offset_of_DebugSetRelativePoint_4() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___DebugSetRelativePoint_4)); }
	inline Vector3_t4282066566  get_DebugSetRelativePoint_4() const { return ___DebugSetRelativePoint_4; }
	inline Vector3_t4282066566 * get_address_of_DebugSetRelativePoint_4() { return &___DebugSetRelativePoint_4; }
	inline void set_DebugSetRelativePoint_4(Vector3_t4282066566  value)
	{
		___DebugSetRelativePoint_4 = value;
	}

	inline static int32_t get_offset_of_DebugPercentDistance_5() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___DebugPercentDistance_5)); }
	inline float get_DebugPercentDistance_5() const { return ___DebugPercentDistance_5; }
	inline float* get_address_of_DebugPercentDistance_5() { return &___DebugPercentDistance_5; }
	inline void set_DebugPercentDistance_5(float value)
	{
		___DebugPercentDistance_5 = value;
	}

	inline static int32_t get_offset_of_DebugGlobalPointResult_6() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___DebugGlobalPointResult_6)); }
	inline Vector3_t4282066566  get_DebugGlobalPointResult_6() const { return ___DebugGlobalPointResult_6; }
	inline Vector3_t4282066566 * get_address_of_DebugGlobalPointResult_6() { return &___DebugGlobalPointResult_6; }
	inline void set_DebugGlobalPointResult_6(Vector3_t4282066566  value)
	{
		___DebugGlobalPointResult_6 = value;
	}

	inline static int32_t get_offset_of_DebugCalcNow_7() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___DebugCalcNow_7)); }
	inline bool get_DebugCalcNow_7() const { return ___DebugCalcNow_7; }
	inline bool* get_address_of_DebugCalcNow_7() { return &___DebugCalcNow_7; }
	inline void set_DebugCalcNow_7(bool value)
	{
		___DebugCalcNow_7 = value;
	}

	inline static int32_t get_offset_of_DebugRecivedVector_8() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___DebugRecivedVector_8)); }
	inline Vector3_t4282066566  get_DebugRecivedVector_8() const { return ___DebugRecivedVector_8; }
	inline Vector3_t4282066566 * get_address_of_DebugRecivedVector_8() { return &___DebugRecivedVector_8; }
	inline void set_DebugRecivedVector_8(Vector3_t4282066566  value)
	{
		___DebugRecivedVector_8 = value;
	}

	inline static int32_t get_offset_of_DebugPercent_9() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620, ___DebugPercent_9)); }
	inline float get_DebugPercent_9() const { return ___DebugPercent_9; }
	inline float* get_address_of_DebugPercent_9() { return &___DebugPercent_9; }
	inline void set_DebugPercent_9(float value)
	{
		___DebugPercent_9 = value;
	}
};

struct RelativePositionOfPoint_t3585775620_StaticFields
{
public:
	// RelativePositionOfPoint RelativePositionOfPoint::Instance
	RelativePositionOfPoint_t3585775620 * ___Instance_3;

public:
	inline static int32_t get_offset_of_Instance_3() { return static_cast<int32_t>(offsetof(RelativePositionOfPoint_t3585775620_StaticFields, ___Instance_3)); }
	inline RelativePositionOfPoint_t3585775620 * get_Instance_3() const { return ___Instance_3; }
	inline RelativePositionOfPoint_t3585775620 ** get_address_of_Instance_3() { return &___Instance_3; }
	inline void set_Instance_3(RelativePositionOfPoint_t3585775620 * value)
	{
		___Instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
