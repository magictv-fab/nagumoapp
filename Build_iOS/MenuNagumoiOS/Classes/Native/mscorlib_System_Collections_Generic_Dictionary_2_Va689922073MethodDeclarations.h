﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va689922073.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2188367154_gshared (Enumerator_t689922073 * __this, Dictionary_2_t2758088665 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2188367154(__this, ___host0, method) ((  void (*) (Enumerator_t689922073 *, Dictionary_2_t2758088665 *, const MethodInfo*))Enumerator__ctor_m2188367154_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3525919545_gshared (Enumerator_t689922073 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3525919545(__this, method) ((  Il2CppObject * (*) (Enumerator_t689922073 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3525919545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m756981827_gshared (Enumerator_t689922073 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m756981827(__this, method) ((  void (*) (Enumerator_t689922073 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m756981827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m272116820_gshared (Enumerator_t689922073 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m272116820(__this, method) ((  void (*) (Enumerator_t689922073 *, const MethodInfo*))Enumerator_Dispose_m272116820_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2623715059_gshared (Enumerator_t689922073 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2623715059(__this, method) ((  bool (*) (Enumerator_t689922073 *, const MethodInfo*))Enumerator_MoveNext_m2623715059_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3883894615_gshared (Enumerator_t689922073 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3883894615(__this, method) ((  Il2CppObject * (*) (Enumerator_t689922073 *, const MethodInfo*))Enumerator_get_Current_m3883894615_gshared)(__this, method)
