﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_PDF417_Internal_BoundingBox242606069.h"

// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_TopLeft()
extern "C"  ResultPoint_t1538592853 * BoundingBox_get_TopLeft_m796783732 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_TopLeft(ZXing.ResultPoint)
extern "C"  void BoundingBox_set_TopLeft_m1557773939 (BoundingBox_t242606069 * __this, ResultPoint_t1538592853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_TopRight()
extern "C"  ResultPoint_t1538592853 * BoundingBox_get_TopRight_m75702385 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_TopRight(ZXing.ResultPoint)
extern "C"  void BoundingBox_set_TopRight_m1714096922 (BoundingBox_t242606069 * __this, ResultPoint_t1538592853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_BottomLeft()
extern "C"  ResultPoint_t1538592853 * BoundingBox_get_BottomLeft_m1680496252 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_BottomLeft(ZXing.ResultPoint)
extern "C"  void BoundingBox_set_BottomLeft_m168000495 (BoundingBox_t242606069 * __this, ResultPoint_t1538592853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_BottomRight()
extern "C"  ResultPoint_t1538592853 * BoundingBox_get_BottomRight_m1700986729 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_BottomRight(ZXing.ResultPoint)
extern "C"  void BoundingBox_set_BottomRight_m1580793118 (BoundingBox_t242606069 * __this, ResultPoint_t1538592853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MinX()
extern "C"  int32_t BoundingBox_get_MinX_m3017558796 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_MinX(System.Int32)
extern "C"  void BoundingBox_set_MinX_m1924407647 (BoundingBox_t242606069 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MaxX()
extern "C"  int32_t BoundingBox_get_MaxX_m3010468538 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_MaxX(System.Int32)
extern "C"  void BoundingBox_set_MaxX_m3172457485 (BoundingBox_t242606069 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MinY()
extern "C"  int32_t BoundingBox_get_MinY_m3017559757 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_MinY(System.Int32)
extern "C"  void BoundingBox_set_MinY_m3431959456 (BoundingBox_t242606069 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MaxY()
extern "C"  int32_t BoundingBox_get_MaxY_m3010469499 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::set_MaxY(System.Int32)
extern "C"  void BoundingBox_set_MaxY_m385041998 (BoundingBox_t242606069 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::Create(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  BoundingBox_t242606069 * BoundingBox_Create_m727342205 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, ResultPoint_t1538592853 * ___topLeft1, ResultPoint_t1538592853 * ___bottomLeft2, ResultPoint_t1538592853 * ___topRight3, ResultPoint_t1538592853 * ___bottomRight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::Create(ZXing.PDF417.Internal.BoundingBox)
extern "C"  BoundingBox_t242606069 * BoundingBox_Create_m2641557690 (Il2CppObject * __this /* static, unused */, BoundingBox_t242606069 * ___box0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  void BoundingBox__ctor_m806490320 (BoundingBox_t242606069 * __this, BitMatrix_t1058711404 * ___image0, ResultPoint_t1538592853 * ___topLeft1, ResultPoint_t1538592853 * ___bottomLeft2, ResultPoint_t1538592853 * ___topRight3, ResultPoint_t1538592853 * ___bottomRight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::merge(ZXing.PDF417.Internal.BoundingBox,ZXing.PDF417.Internal.BoundingBox)
extern "C"  BoundingBox_t242606069 * BoundingBox_merge_m637430346 (Il2CppObject * __this /* static, unused */, BoundingBox_t242606069 * ___leftBox0, BoundingBox_t242606069 * ___rightBox1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::addMissingRows(System.Int32,System.Int32,System.Boolean)
extern "C"  BoundingBox_t242606069 * BoundingBox_addMissingRows_m589842367 (BoundingBox_t242606069 * __this, int32_t ___missingStartRows0, int32_t ___missingEndRows1, bool ___isLeft2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BoundingBox::calculateMinMaxValues()
extern "C"  void BoundingBox_calculateMinMaxValues_m2146010129 (BoundingBox_t242606069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
