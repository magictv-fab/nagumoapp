﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ServerControl/<IPlay>c__Iterator7C
struct U3CIPlayU3Ec__Iterator7C_t3776050517;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ServerControl/<IPlay>c__Iterator7C::.ctor()
extern "C"  void U3CIPlayU3Ec__Iterator7C__ctor_m1507265254 (U3CIPlayU3Ec__Iterator7C_t3776050517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ServerControl/<IPlay>c__Iterator7C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIPlayU3Ec__Iterator7C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1222535542 (U3CIPlayU3Ec__Iterator7C_t3776050517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ServerControl/<IPlay>c__Iterator7C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIPlayU3Ec__Iterator7C_System_Collections_IEnumerator_get_Current_m3230537994 (U3CIPlayU3Ec__Iterator7C_t3776050517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ServerControl/<IPlay>c__Iterator7C::MoveNext()
extern "C"  bool U3CIPlayU3Ec__Iterator7C_MoveNext_m3155315766 (U3CIPlayU3Ec__Iterator7C_t3776050517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/<IPlay>c__Iterator7C::Dispose()
extern "C"  void U3CIPlayU3Ec__Iterator7C_Dispose_m976213411 (U3CIPlayU3Ec__Iterator7C_t3776050517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/<IPlay>c__Iterator7C::Reset()
extern "C"  void U3CIPlayU3Ec__Iterator7C_Reset_m3448665491 (U3CIPlayU3Ec__Iterator7C_t3776050517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
