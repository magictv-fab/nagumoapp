﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MNFeaturePreview1881393139.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MNUseExample
struct  MNUseExample_t1970404964  : public MNFeaturePreview_t1881393139
{
public:
	// System.String MNUseExample::appleId
	String_t* ___appleId_12;
	// System.String MNUseExample::apdroidAppUrl
	String_t* ___apdroidAppUrl_13;

public:
	inline static int32_t get_offset_of_appleId_12() { return static_cast<int32_t>(offsetof(MNUseExample_t1970404964, ___appleId_12)); }
	inline String_t* get_appleId_12() const { return ___appleId_12; }
	inline String_t** get_address_of_appleId_12() { return &___appleId_12; }
	inline void set_appleId_12(String_t* value)
	{
		___appleId_12 = value;
		Il2CppCodeGenWriteBarrier(&___appleId_12, value);
	}

	inline static int32_t get_offset_of_apdroidAppUrl_13() { return static_cast<int32_t>(offsetof(MNUseExample_t1970404964, ___apdroidAppUrl_13)); }
	inline String_t* get_apdroidAppUrl_13() const { return ___apdroidAppUrl_13; }
	inline String_t** get_address_of_apdroidAppUrl_13() { return &___apdroidAppUrl_13; }
	inline void set_apdroidAppUrl_13(String_t* value)
	{
		___apdroidAppUrl_13 = value;
		Il2CppCodeGenWriteBarrier(&___apdroidAppUrl_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
