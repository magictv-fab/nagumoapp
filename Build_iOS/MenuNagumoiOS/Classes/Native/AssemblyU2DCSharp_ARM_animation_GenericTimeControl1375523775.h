﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnEvent
struct OnEvent_t314892251;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.animation.GenericTimeControllAbstract
struct  GenericTimeControllAbstract_t1375523775  : public MonoBehaviour_t667441552
{
public:
	// OnEvent ARM.animation.GenericTimeControllAbstract::onFinished
	OnEvent_t314892251 * ___onFinished_2;
	// OnEvent ARM.animation.GenericTimeControllAbstract::onStart
	OnEvent_t314892251 * ___onStart_3;
	// System.Boolean ARM.animation.GenericTimeControllAbstract::_isPlaying
	bool ____isPlaying_4;

public:
	inline static int32_t get_offset_of_onFinished_2() { return static_cast<int32_t>(offsetof(GenericTimeControllAbstract_t1375523775, ___onFinished_2)); }
	inline OnEvent_t314892251 * get_onFinished_2() const { return ___onFinished_2; }
	inline OnEvent_t314892251 ** get_address_of_onFinished_2() { return &___onFinished_2; }
	inline void set_onFinished_2(OnEvent_t314892251 * value)
	{
		___onFinished_2 = value;
		Il2CppCodeGenWriteBarrier(&___onFinished_2, value);
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(GenericTimeControllAbstract_t1375523775, ___onStart_3)); }
	inline OnEvent_t314892251 * get_onStart_3() const { return ___onStart_3; }
	inline OnEvent_t314892251 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(OnEvent_t314892251 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier(&___onStart_3, value);
	}

	inline static int32_t get_offset_of__isPlaying_4() { return static_cast<int32_t>(offsetof(GenericTimeControllAbstract_t1375523775, ____isPlaying_4)); }
	inline bool get__isPlaying_4() const { return ____isPlaying_4; }
	inline bool* get_address_of__isPlaying_4() { return &____isPlaying_4; }
	inline void set__isPlaying_4(bool value)
	{
		____isPlaying_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
