﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenShakeRotation
struct iTweenShakeRotation_t559909872;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::.ctor()
extern "C"  void iTweenShakeRotation__ctor_m4165363014 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::Reset()
extern "C"  void iTweenShakeRotation_Reset_m1811795955 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::OnEnter()
extern "C"  void iTweenShakeRotation_OnEnter_m3691032477 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::OnExit()
extern "C"  void iTweenShakeRotation_OnExit_m1790553179 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::DoiTween()
extern "C"  void iTweenShakeRotation_DoiTween_m4203989323 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
