﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.InAppDirector
struct InAppDirector_t1593455082;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.InAppDirector::.ctor()
extern "C"  void InAppDirector__ctor_m2230859189 (InAppDirector_t1593455082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppDirector::Start()
extern "C"  void InAppDirector_Start_m1177996981 (InAppDirector_t1593455082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppDirector::Test(System.String)
extern "C"  void InAppDirector_Test_m1198044065 (InAppDirector_t1593455082 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppDirector::Update()
extern "C"  void InAppDirector_Update_m2164020216 (InAppDirector_t1593455082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
