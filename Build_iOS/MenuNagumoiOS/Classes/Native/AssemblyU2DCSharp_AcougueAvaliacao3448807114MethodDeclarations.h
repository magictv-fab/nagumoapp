﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AcougueAvaliacao
struct AcougueAvaliacao_t3448807114;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AcougueAvaliacao::.ctor()
extern "C"  void AcougueAvaliacao__ctor_m1921616657 (AcougueAvaliacao_t3448807114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao::.cctor()
extern "C"  void AcougueAvaliacao__cctor_m3253445308 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao::Start()
extern "C"  void AcougueAvaliacao_Start_m868754449 (AcougueAvaliacao_t3448807114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao::InstantiateObj(System.String,System.String)
extern "C"  void AcougueAvaliacao_InstantiateObj_m3015799742 (AcougueAvaliacao_t3448807114 * __this, String_t* ___code0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao::AvaliarPedido()
extern "C"  void AcougueAvaliacao_AvaliarPedido_m4092373390 (AcougueAvaliacao_t3448807114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AcougueAvaliacao::IAvaliarPedido()
extern "C"  Il2CppObject * AcougueAvaliacao_IAvaliarPedido_m2270574993 (AcougueAvaliacao_t3448807114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AcougueAvaliacao::PopUp(System.String)
extern "C"  void AcougueAvaliacao_PopUp_m1467716839 (AcougueAvaliacao_t3448807114 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
