﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToFloat
struct ConvertBoolToFloat_t316188428;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToFloat::.ctor()
extern "C"  void ConvertBoolToFloat__ctor_m2998190810 (ConvertBoolToFloat_t316188428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToFloat::Reset()
extern "C"  void ConvertBoolToFloat_Reset_m644623751 (ConvertBoolToFloat_t316188428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToFloat::OnEnter()
extern "C"  void ConvertBoolToFloat_OnEnter_m3025008689 (ConvertBoolToFloat_t316188428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToFloat::OnUpdate()
extern "C"  void ConvertBoolToFloat_OnUpdate_m2714515506 (ConvertBoolToFloat_t316188428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToFloat::DoConvertBoolToFloat()
extern "C"  void ConvertBoolToFloat_DoConvertBoolToFloat_m2487767033 (ConvertBoolToFloat_t316188428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
