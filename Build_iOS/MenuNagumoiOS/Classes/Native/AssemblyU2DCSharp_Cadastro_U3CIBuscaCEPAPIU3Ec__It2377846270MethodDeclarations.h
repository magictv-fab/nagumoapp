﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cadastro/<IBuscaCEPAPI>c__Iterator48
struct U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Cadastro/<IBuscaCEPAPI>c__Iterator48::.ctor()
extern "C"  void U3CIBuscaCEPAPIU3Ec__Iterator48__ctor_m3044211293 (U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<IBuscaCEPAPI>c__Iterator48::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIBuscaCEPAPIU3Ec__Iterator48_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1576819999 (U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<IBuscaCEPAPI>c__Iterator48::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIBuscaCEPAPIU3Ec__Iterator48_System_Collections_IEnumerator_get_Current_m3639770803 (U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro/<IBuscaCEPAPI>c__Iterator48::MoveNext()
extern "C"  bool U3CIBuscaCEPAPIU3Ec__Iterator48_MoveNext_m464255071 (U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<IBuscaCEPAPI>c__Iterator48::Dispose()
extern "C"  void U3CIBuscaCEPAPIU3Ec__Iterator48_Dispose_m512607066 (U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<IBuscaCEPAPI>c__Iterator48::Reset()
extern "C"  void U3CIBuscaCEPAPIU3Ec__Iterator48_Reset_m690644234 (U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
