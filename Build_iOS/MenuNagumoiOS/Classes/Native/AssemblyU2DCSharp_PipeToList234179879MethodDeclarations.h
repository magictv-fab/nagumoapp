﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Collections.Generic.List`1<System.String> PipeToList::Parse(System.String)
extern "C"  List_1_t1375417109 * PipeToList_Parse_m3989659310 (Il2CppObject * __this /* static, unused */, String_t* ___string_with_pipe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Single> PipeToList::ParseReturningFloats(System.String)
extern "C"  List_1_t1365137228 * PipeToList_ParseReturningFloats_m3885384116 (Il2CppObject * __this /* static, unused */, String_t* ___string_with_pipe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> PipeToList::ParseReturningInts(System.String)
extern "C"  List_1_t2522024052 * PipeToList_ParseReturningInts_m3374416077 (Il2CppObject * __this /* static, unused */, String_t* ___string_with_pipe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
