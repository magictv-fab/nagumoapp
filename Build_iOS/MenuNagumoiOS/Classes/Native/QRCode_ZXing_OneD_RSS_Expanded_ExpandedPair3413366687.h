﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.OneD.RSS.DataCharacter
struct DataCharacter_t770728801;
// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.ExpandedPair
struct  ExpandedPair_t3413366687  : public Il2CppObject
{
public:
	// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::<MayBeLast>k__BackingField
	bool ___U3CMayBeLastU3Ek__BackingField_0;
	// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::<LeftChar>k__BackingField
	DataCharacter_t770728801 * ___U3CLeftCharU3Ek__BackingField_1;
	// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::<RightChar>k__BackingField
	DataCharacter_t770728801 * ___U3CRightCharU3Ek__BackingField_2;
	// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.ExpandedPair::<FinderPattern>k__BackingField
	FinderPattern_t3366792524 * ___U3CFinderPatternU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMayBeLastU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExpandedPair_t3413366687, ___U3CMayBeLastU3Ek__BackingField_0)); }
	inline bool get_U3CMayBeLastU3Ek__BackingField_0() const { return ___U3CMayBeLastU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CMayBeLastU3Ek__BackingField_0() { return &___U3CMayBeLastU3Ek__BackingField_0; }
	inline void set_U3CMayBeLastU3Ek__BackingField_0(bool value)
	{
		___U3CMayBeLastU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLeftCharU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExpandedPair_t3413366687, ___U3CLeftCharU3Ek__BackingField_1)); }
	inline DataCharacter_t770728801 * get_U3CLeftCharU3Ek__BackingField_1() const { return ___U3CLeftCharU3Ek__BackingField_1; }
	inline DataCharacter_t770728801 ** get_address_of_U3CLeftCharU3Ek__BackingField_1() { return &___U3CLeftCharU3Ek__BackingField_1; }
	inline void set_U3CLeftCharU3Ek__BackingField_1(DataCharacter_t770728801 * value)
	{
		___U3CLeftCharU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLeftCharU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRightCharU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExpandedPair_t3413366687, ___U3CRightCharU3Ek__BackingField_2)); }
	inline DataCharacter_t770728801 * get_U3CRightCharU3Ek__BackingField_2() const { return ___U3CRightCharU3Ek__BackingField_2; }
	inline DataCharacter_t770728801 ** get_address_of_U3CRightCharU3Ek__BackingField_2() { return &___U3CRightCharU3Ek__BackingField_2; }
	inline void set_U3CRightCharU3Ek__BackingField_2(DataCharacter_t770728801 * value)
	{
		___U3CRightCharU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRightCharU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CFinderPatternU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ExpandedPair_t3413366687, ___U3CFinderPatternU3Ek__BackingField_3)); }
	inline FinderPattern_t3366792524 * get_U3CFinderPatternU3Ek__BackingField_3() const { return ___U3CFinderPatternU3Ek__BackingField_3; }
	inline FinderPattern_t3366792524 ** get_address_of_U3CFinderPatternU3Ek__BackingField_3() { return &___U3CFinderPatternU3Ek__BackingField_3; }
	inline void set_U3CFinderPatternU3Ek__BackingField_3(FinderPattern_t3366792524 * value)
	{
		___U3CFinderPatternU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFinderPatternU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
