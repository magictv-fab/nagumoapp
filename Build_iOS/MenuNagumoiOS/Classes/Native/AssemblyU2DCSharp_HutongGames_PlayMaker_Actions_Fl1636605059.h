﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatDivide
struct  FloatDivide_t1636605059  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatDivide::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatDivide::divideBy
	FsmFloat_t2134102846 * ___divideBy_10;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatDivide::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_floatVariable_9() { return static_cast<int32_t>(offsetof(FloatDivide_t1636605059, ___floatVariable_9)); }
	inline FsmFloat_t2134102846 * get_floatVariable_9() const { return ___floatVariable_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_9() { return &___floatVariable_9; }
	inline void set_floatVariable_9(FsmFloat_t2134102846 * value)
	{
		___floatVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_9, value);
	}

	inline static int32_t get_offset_of_divideBy_10() { return static_cast<int32_t>(offsetof(FloatDivide_t1636605059, ___divideBy_10)); }
	inline FsmFloat_t2134102846 * get_divideBy_10() const { return ___divideBy_10; }
	inline FsmFloat_t2134102846 ** get_address_of_divideBy_10() { return &___divideBy_10; }
	inline void set_divideBy_10(FsmFloat_t2134102846 * value)
	{
		___divideBy_10 = value;
		Il2CppCodeGenWriteBarrier(&___divideBy_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(FloatDivide_t1636605059, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
