﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/PromptForPushNotificationsUserResponse
struct PromptForPushNotificationsUserResponse_t2193543734;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/PromptForPushNotificationsUserResponse::.ctor(System.Object,System.IntPtr)
extern "C"  void PromptForPushNotificationsUserResponse__ctor_m2968681437 (PromptForPushNotificationsUserResponse_t2193543734 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/PromptForPushNotificationsUserResponse::Invoke(System.Boolean)
extern "C"  void PromptForPushNotificationsUserResponse_Invoke_m2232769454 (PromptForPushNotificationsUserResponse_t2193543734 * __this, bool ___accepted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/PromptForPushNotificationsUserResponse::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PromptForPushNotificationsUserResponse_BeginInvoke_m1661645531 (PromptForPushNotificationsUserResponse_t2193543734 * __this, bool ___accepted0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/PromptForPushNotificationsUserResponse::EndInvoke(System.IAsyncResult)
extern "C"  void PromptForPushNotificationsUserResponse_EndInvoke_m1878025325 (PromptForPushNotificationsUserResponse_t2193543734 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
