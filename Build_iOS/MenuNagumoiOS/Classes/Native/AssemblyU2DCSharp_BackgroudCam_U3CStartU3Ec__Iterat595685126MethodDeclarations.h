﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackgroudCam/<Start>c__Iterator45
struct U3CStartU3Ec__Iterator45_t595685126;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BackgroudCam/<Start>c__Iterator45::.ctor()
extern "C"  void U3CStartU3Ec__Iterator45__ctor_m1918956837 (U3CStartU3Ec__Iterator45_t595685126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BackgroudCam/<Start>c__Iterator45::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator45_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2690661517 (U3CStartU3Ec__Iterator45_t595685126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BackgroudCam/<Start>c__Iterator45::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator45_System_Collections_IEnumerator_get_Current_m3434019361 (U3CStartU3Ec__Iterator45_t595685126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BackgroudCam/<Start>c__Iterator45::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator45_MoveNext_m1000921007 (U3CStartU3Ec__Iterator45_t595685126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam/<Start>c__Iterator45::Dispose()
extern "C"  void U3CStartU3Ec__Iterator45_Dispose_m1474833442 (U3CStartU3Ec__Iterator45_t595685126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam/<Start>c__Iterator45::Reset()
extern "C"  void U3CStartU3Ec__Iterator45_Reset_m3860357074 (U3CStartU3Ec__Iterator45_t595685126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
