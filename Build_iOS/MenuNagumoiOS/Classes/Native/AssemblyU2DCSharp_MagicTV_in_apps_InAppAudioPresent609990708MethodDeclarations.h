﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppAudioPresentation
struct InAppAudioPresentation_t609990708;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.in_apps.InAppAudioPresentation::.ctor()
extern "C"  void InAppAudioPresentation__ctor_m3180646790 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Start()
extern "C"  void InAppAudioPresentation_Start_m2127784582 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::DoRun()
extern "C"  void InAppAudioPresentation_DoRun_m1543216292 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::startAudioSourceComponent()
extern "C"  void InAppAudioPresentation_startAudioSourceComponent_m3938764722 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::SetBundleVO(MagicTV.vo.BundleVO)
extern "C"  void InAppAudioPresentation_SetBundleVO_m3361608000 (InAppAudioPresentation_t609990708 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::prepare()
extern "C"  void InAppAudioPresentation_prepare_m3806798155 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::prepareAudio()
extern "C"  void InAppAudioPresentation_prepareAudio_m1463786797 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Update()
extern "C"  void InAppAudioPresentation_Update_m1542664775 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::onAudioComplete(ARM.utils.request.ARMRequestVO)
extern "C"  void InAppAudioPresentation_onAudioComplete_m874955994 (InAppAudioPresentation_t609990708 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::DoStop()
extern "C"  void InAppAudioPresentation_DoStop_m622869419 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Dispose()
extern "C"  void InAppAudioPresentation_Dispose_m2778100803 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.in_apps.InAppAudioPresentation::GetInAppName()
extern "C"  String_t* InAppAudioPresentation_GetInAppName_m269486162 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Play()
extern "C"  void InAppAudioPresentation_Play_m4270334258 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Pause()
extern "C"  void InAppAudioPresentation_Pause_m3234772762 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::RewindAndPause()
extern "C"  void InAppAudioPresentation_RewindAndPause_m3265987704 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::RewindAndPlay()
extern "C"  void InAppAudioPresentation_RewindAndPlay_m392015892 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Hide()
extern "C"  void InAppAudioPresentation_Hide_m4038600640 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::Show()
extern "C"  void InAppAudioPresentation_Show_m57975483 (InAppAudioPresentation_t609990708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAudioPresentation::CustomAction(System.String)
extern "C"  void InAppAudioPresentation_CustomAction_m3402242525 (InAppAudioPresentation_t609990708 * __this, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
