﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<ZXing.PDF417.Internal.BarcodeValue[]>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1667771897(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2397205128 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.PDF417.Internal.BarcodeValue[]>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3204344583(__this, method) ((  void (*) (InternalEnumerator_1_t2397205128 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZXing.PDF417.Internal.BarcodeValue[]>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m911712883(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2397205128 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.PDF417.Internal.BarcodeValue[]>::Dispose()
#define InternalEnumerator_1_Dispose_m2066688784(__this, method) ((  void (*) (InternalEnumerator_1_t2397205128 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZXing.PDF417.Internal.BarcodeValue[]>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2465812083(__this, method) ((  bool (*) (InternalEnumerator_1_t2397205128 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZXing.PDF417.Internal.BarcodeValue[]>::get_Current()
#define InternalEnumerator_1_get_Current_m2153935808(__this, method) ((  BarcodeValueU5BU5D_t3614862452* (*) (InternalEnumerator_1_t2397205128 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
