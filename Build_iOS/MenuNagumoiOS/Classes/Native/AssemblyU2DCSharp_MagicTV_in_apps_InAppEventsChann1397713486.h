﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t817370046;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.Collections.Generic.Dictionary`2<System.String,ARM.events.VoidEventDispatcher>
struct Dictionary_2_t2580879573;
// System.Collections.Generic.Dictionary`2<System.String,ARM.events.GenericParameterEventDispatcher>
struct Dictionary_2_t336768919;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppEventsChannel
struct  InAppEventsChannel_t1397713486  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> MagicTV.in_apps.InAppEventsChannel::_floatValues
	Dictionary_2_t817370046 * ____floatValues_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MagicTV.in_apps.InAppEventsChannel::_stringValues
	Dictionary_2_t827649927 * ____stringValues_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MagicTV.in_apps.InAppEventsChannel::_intValues
	Dictionary_2_t1974256870 * ____intValues_2;
	// System.Collections.Generic.Dictionary`2<System.String,ARM.events.VoidEventDispatcher> MagicTV.in_apps.InAppEventsChannel::_voidEvents
	Dictionary_2_t2580879573 * ____voidEvents_3;
	// System.Collections.Generic.Dictionary`2<System.String,ARM.events.GenericParameterEventDispatcher> MagicTV.in_apps.InAppEventsChannel::_genericEvents
	Dictionary_2_t336768919 * ____genericEvents_4;

public:
	inline static int32_t get_offset_of__floatValues_0() { return static_cast<int32_t>(offsetof(InAppEventsChannel_t1397713486, ____floatValues_0)); }
	inline Dictionary_2_t817370046 * get__floatValues_0() const { return ____floatValues_0; }
	inline Dictionary_2_t817370046 ** get_address_of__floatValues_0() { return &____floatValues_0; }
	inline void set__floatValues_0(Dictionary_2_t817370046 * value)
	{
		____floatValues_0 = value;
		Il2CppCodeGenWriteBarrier(&____floatValues_0, value);
	}

	inline static int32_t get_offset_of__stringValues_1() { return static_cast<int32_t>(offsetof(InAppEventsChannel_t1397713486, ____stringValues_1)); }
	inline Dictionary_2_t827649927 * get__stringValues_1() const { return ____stringValues_1; }
	inline Dictionary_2_t827649927 ** get_address_of__stringValues_1() { return &____stringValues_1; }
	inline void set__stringValues_1(Dictionary_2_t827649927 * value)
	{
		____stringValues_1 = value;
		Il2CppCodeGenWriteBarrier(&____stringValues_1, value);
	}

	inline static int32_t get_offset_of__intValues_2() { return static_cast<int32_t>(offsetof(InAppEventsChannel_t1397713486, ____intValues_2)); }
	inline Dictionary_2_t1974256870 * get__intValues_2() const { return ____intValues_2; }
	inline Dictionary_2_t1974256870 ** get_address_of__intValues_2() { return &____intValues_2; }
	inline void set__intValues_2(Dictionary_2_t1974256870 * value)
	{
		____intValues_2 = value;
		Il2CppCodeGenWriteBarrier(&____intValues_2, value);
	}

	inline static int32_t get_offset_of__voidEvents_3() { return static_cast<int32_t>(offsetof(InAppEventsChannel_t1397713486, ____voidEvents_3)); }
	inline Dictionary_2_t2580879573 * get__voidEvents_3() const { return ____voidEvents_3; }
	inline Dictionary_2_t2580879573 ** get_address_of__voidEvents_3() { return &____voidEvents_3; }
	inline void set__voidEvents_3(Dictionary_2_t2580879573 * value)
	{
		____voidEvents_3 = value;
		Il2CppCodeGenWriteBarrier(&____voidEvents_3, value);
	}

	inline static int32_t get_offset_of__genericEvents_4() { return static_cast<int32_t>(offsetof(InAppEventsChannel_t1397713486, ____genericEvents_4)); }
	inline Dictionary_2_t336768919 * get__genericEvents_4() const { return ____genericEvents_4; }
	inline Dictionary_2_t336768919 ** get_address_of__genericEvents_4() { return &____genericEvents_4; }
	inline void set__genericEvents_4(Dictionary_2_t336768919 * value)
	{
		____genericEvents_4 = value;
		Il2CppCodeGenWriteBarrier(&____genericEvents_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
