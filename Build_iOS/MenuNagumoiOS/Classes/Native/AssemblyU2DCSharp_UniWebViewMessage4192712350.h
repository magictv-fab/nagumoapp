﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewMessage
struct  UniWebViewMessage_t4192712350 
{
public:
	// System.String UniWebViewMessage::<RawMessage>k__BackingField
	String_t* ___U3CRawMessageU3Ek__BackingField_0;
	// System.String UniWebViewMessage::<Scheme>k__BackingField
	String_t* ___U3CSchemeU3Ek__BackingField_1;
	// System.String UniWebViewMessage::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniWebViewMessage::<Args>k__BackingField
	Dictionary_2_t827649927 * ___U3CArgsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRawMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t4192712350, ___U3CRawMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CRawMessageU3Ek__BackingField_0() const { return ___U3CRawMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CRawMessageU3Ek__BackingField_0() { return &___U3CRawMessageU3Ek__BackingField_0; }
	inline void set_U3CRawMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CRawMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRawMessageU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CSchemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t4192712350, ___U3CSchemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CSchemeU3Ek__BackingField_1() const { return ___U3CSchemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSchemeU3Ek__BackingField_1() { return &___U3CSchemeU3Ek__BackingField_1; }
	inline void set_U3CSchemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CSchemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSchemeU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t4192712350, ___U3CPathU3Ek__BackingField_2)); }
	inline String_t* get_U3CPathU3Ek__BackingField_2() const { return ___U3CPathU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_2() { return &___U3CPathU3Ek__BackingField_2; }
	inline void set_U3CPathU3Ek__BackingField_2(String_t* value)
	{
		___U3CPathU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPathU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UniWebViewMessage_t4192712350, ___U3CArgsU3Ek__BackingField_3)); }
	inline Dictionary_2_t827649927 * get_U3CArgsU3Ek__BackingField_3() const { return ___U3CArgsU3Ek__BackingField_3; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CArgsU3Ek__BackingField_3() { return &___U3CArgsU3Ek__BackingField_3; }
	inline void set_U3CArgsU3Ek__BackingField_3(Dictionary_2_t827649927 * value)
	{
		___U3CArgsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CArgsU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UniWebViewMessage
struct UniWebViewMessage_t4192712350_marshaled_pinvoke
{
	char* ___U3CRawMessageU3Ek__BackingField_0;
	char* ___U3CSchemeU3Ek__BackingField_1;
	char* ___U3CPathU3Ek__BackingField_2;
	Dictionary_2_t827649927 * ___U3CArgsU3Ek__BackingField_3;
};
// Native definition for marshalling of: UniWebViewMessage
struct UniWebViewMessage_t4192712350_marshaled_com
{
	Il2CppChar* ___U3CRawMessageU3Ek__BackingField_0;
	Il2CppChar* ___U3CSchemeU3Ek__BackingField_1;
	Il2CppChar* ___U3CPathU3Ek__BackingField_2;
	Dictionary_2_t827649927 * ___U3CArgsU3Ek__BackingField_3;
};
