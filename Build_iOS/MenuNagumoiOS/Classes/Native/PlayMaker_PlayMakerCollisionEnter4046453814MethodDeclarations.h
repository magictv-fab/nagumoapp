﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionEnter
struct PlayMakerCollisionEnter_t4046453814;
// UnityEngine.Collision
struct Collision_t2494107688;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"

// System.Void PlayMakerCollisionEnter::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionEnter_OnCollisionEnter_m1529344961 (PlayMakerCollisionEnter_t4046453814 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionEnter::.ctor()
extern "C"  void PlayMakerCollisionEnter__ctor_m1235992051 (PlayMakerCollisionEnter_t4046453814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
