﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.cron.EasyTimerDisposable/<SetTimeout>c__AnonStorey93
struct  U3CSetTimeoutU3Ec__AnonStorey93_t1854685057  : public Il2CppObject
{
public:
	// System.Action ARM.utils.cron.EasyTimerDisposable/<SetTimeout>c__AnonStorey93::method
	Action_t3771233898 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CSetTimeoutU3Ec__AnonStorey93_t1854685057, ___method_0)); }
	inline Action_t3771233898 * get_method_0() const { return ___method_0; }
	inline Action_t3771233898 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Action_t3771233898 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
