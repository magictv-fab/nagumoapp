﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimationSettings
struct AnimationSettings_t3134957589;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::.ctor()
extern "C"  void AnimationSettings__ctor_m1957903553 (AnimationSettings_t3134957589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::Reset()
extern "C"  void AnimationSettings_Reset_m3899303790 (AnimationSettings_t3134957589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::OnEnter()
extern "C"  void AnimationSettings_OnEnter_m4036334680 (AnimationSettings_t3134957589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::DoAnimationSettings()
extern "C"  void AnimationSettings_DoAnimationSettings_m2943551707 (AnimationSettings_t3134957589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
