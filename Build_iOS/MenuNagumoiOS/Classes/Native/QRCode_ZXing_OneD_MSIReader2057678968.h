﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MSIReader
struct  MSIReader_t2057678968  : public OneDReader_t3436042911
{
public:
	// System.Boolean ZXing.OneD.MSIReader::usingCheckDigit
	bool ___usingCheckDigit_5;
	// System.Text.StringBuilder ZXing.OneD.MSIReader::decodeRowResult
	StringBuilder_t243639308 * ___decodeRowResult_6;
	// System.Int32[] ZXing.OneD.MSIReader::counters
	Int32U5BU5D_t3230847821* ___counters_7;
	// System.Int32 ZXing.OneD.MSIReader::averageCounterWidth
	int32_t ___averageCounterWidth_8;

public:
	inline static int32_t get_offset_of_usingCheckDigit_5() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968, ___usingCheckDigit_5)); }
	inline bool get_usingCheckDigit_5() const { return ___usingCheckDigit_5; }
	inline bool* get_address_of_usingCheckDigit_5() { return &___usingCheckDigit_5; }
	inline void set_usingCheckDigit_5(bool value)
	{
		___usingCheckDigit_5 = value;
	}

	inline static int32_t get_offset_of_decodeRowResult_6() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968, ___decodeRowResult_6)); }
	inline StringBuilder_t243639308 * get_decodeRowResult_6() const { return ___decodeRowResult_6; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowResult_6() { return &___decodeRowResult_6; }
	inline void set_decodeRowResult_6(StringBuilder_t243639308 * value)
	{
		___decodeRowResult_6 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowResult_6, value);
	}

	inline static int32_t get_offset_of_counters_7() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968, ___counters_7)); }
	inline Int32U5BU5D_t3230847821* get_counters_7() const { return ___counters_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_counters_7() { return &___counters_7; }
	inline void set_counters_7(Int32U5BU5D_t3230847821* value)
	{
		___counters_7 = value;
		Il2CppCodeGenWriteBarrier(&___counters_7, value);
	}

	inline static int32_t get_offset_of_averageCounterWidth_8() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968, ___averageCounterWidth_8)); }
	inline int32_t get_averageCounterWidth_8() const { return ___averageCounterWidth_8; }
	inline int32_t* get_address_of_averageCounterWidth_8() { return &___averageCounterWidth_8; }
	inline void set_averageCounterWidth_8(int32_t value)
	{
		___averageCounterWidth_8 = value;
	}
};

struct MSIReader_t2057678968_StaticFields
{
public:
	// System.String ZXing.OneD.MSIReader::ALPHABET_STRING
	String_t* ___ALPHABET_STRING_2;
	// System.Char[] ZXing.OneD.MSIReader::ALPHABET
	CharU5BU5D_t3324145743* ___ALPHABET_3;
	// System.Int32[] ZXing.OneD.MSIReader::CHARACTER_ENCODINGS
	Int32U5BU5D_t3230847821* ___CHARACTER_ENCODINGS_4;
	// System.Int32[] ZXing.OneD.MSIReader::doubleAndCrossSum
	Int32U5BU5D_t3230847821* ___doubleAndCrossSum_9;

public:
	inline static int32_t get_offset_of_ALPHABET_STRING_2() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968_StaticFields, ___ALPHABET_STRING_2)); }
	inline String_t* get_ALPHABET_STRING_2() const { return ___ALPHABET_STRING_2; }
	inline String_t** get_address_of_ALPHABET_STRING_2() { return &___ALPHABET_STRING_2; }
	inline void set_ALPHABET_STRING_2(String_t* value)
	{
		___ALPHABET_STRING_2 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHABET_STRING_2, value);
	}

	inline static int32_t get_offset_of_ALPHABET_3() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968_StaticFields, ___ALPHABET_3)); }
	inline CharU5BU5D_t3324145743* get_ALPHABET_3() const { return ___ALPHABET_3; }
	inline CharU5BU5D_t3324145743** get_address_of_ALPHABET_3() { return &___ALPHABET_3; }
	inline void set_ALPHABET_3(CharU5BU5D_t3324145743* value)
	{
		___ALPHABET_3 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHABET_3, value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_4() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968_StaticFields, ___CHARACTER_ENCODINGS_4)); }
	inline Int32U5BU5D_t3230847821* get_CHARACTER_ENCODINGS_4() const { return ___CHARACTER_ENCODINGS_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_CHARACTER_ENCODINGS_4() { return &___CHARACTER_ENCODINGS_4; }
	inline void set_CHARACTER_ENCODINGS_4(Int32U5BU5D_t3230847821* value)
	{
		___CHARACTER_ENCODINGS_4 = value;
		Il2CppCodeGenWriteBarrier(&___CHARACTER_ENCODINGS_4, value);
	}

	inline static int32_t get_offset_of_doubleAndCrossSum_9() { return static_cast<int32_t>(offsetof(MSIReader_t2057678968_StaticFields, ___doubleAndCrossSum_9)); }
	inline Int32U5BU5D_t3230847821* get_doubleAndCrossSum_9() const { return ___doubleAndCrossSum_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_doubleAndCrossSum_9() { return &___doubleAndCrossSum_9; }
	inline void set_doubleAndCrossSum_9(Int32U5BU5D_t3230847821* value)
	{
		___doubleAndCrossSum_9 = value;
		Il2CppCodeGenWriteBarrier(&___doubleAndCrossSum_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
