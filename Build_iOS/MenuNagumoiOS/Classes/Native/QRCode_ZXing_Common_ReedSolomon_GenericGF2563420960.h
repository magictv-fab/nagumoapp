﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t755870220;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.GenericGF
struct  GenericGF_t2563420960  : public Il2CppObject
{
public:
	// System.Int32[] ZXing.Common.ReedSolomon.GenericGF::expTable
	Int32U5BU5D_t3230847821* ___expTable_8;
	// System.Int32[] ZXing.Common.ReedSolomon.GenericGF::logTable
	Int32U5BU5D_t3230847821* ___logTable_9;
	// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::zero
	GenericGFPoly_t755870220 * ___zero_10;
	// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::one
	GenericGFPoly_t755870220 * ___one_11;
	// System.Int32 ZXing.Common.ReedSolomon.GenericGF::size
	int32_t ___size_12;
	// System.Int32 ZXing.Common.ReedSolomon.GenericGF::primitive
	int32_t ___primitive_13;
	// System.Int32 ZXing.Common.ReedSolomon.GenericGF::generatorBase
	int32_t ___generatorBase_14;

public:
	inline static int32_t get_offset_of_expTable_8() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___expTable_8)); }
	inline Int32U5BU5D_t3230847821* get_expTable_8() const { return ___expTable_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_expTable_8() { return &___expTable_8; }
	inline void set_expTable_8(Int32U5BU5D_t3230847821* value)
	{
		___expTable_8 = value;
		Il2CppCodeGenWriteBarrier(&___expTable_8, value);
	}

	inline static int32_t get_offset_of_logTable_9() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___logTable_9)); }
	inline Int32U5BU5D_t3230847821* get_logTable_9() const { return ___logTable_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_logTable_9() { return &___logTable_9; }
	inline void set_logTable_9(Int32U5BU5D_t3230847821* value)
	{
		___logTable_9 = value;
		Il2CppCodeGenWriteBarrier(&___logTable_9, value);
	}

	inline static int32_t get_offset_of_zero_10() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___zero_10)); }
	inline GenericGFPoly_t755870220 * get_zero_10() const { return ___zero_10; }
	inline GenericGFPoly_t755870220 ** get_address_of_zero_10() { return &___zero_10; }
	inline void set_zero_10(GenericGFPoly_t755870220 * value)
	{
		___zero_10 = value;
		Il2CppCodeGenWriteBarrier(&___zero_10, value);
	}

	inline static int32_t get_offset_of_one_11() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___one_11)); }
	inline GenericGFPoly_t755870220 * get_one_11() const { return ___one_11; }
	inline GenericGFPoly_t755870220 ** get_address_of_one_11() { return &___one_11; }
	inline void set_one_11(GenericGFPoly_t755870220 * value)
	{
		___one_11 = value;
		Il2CppCodeGenWriteBarrier(&___one_11, value);
	}

	inline static int32_t get_offset_of_size_12() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___size_12)); }
	inline int32_t get_size_12() const { return ___size_12; }
	inline int32_t* get_address_of_size_12() { return &___size_12; }
	inline void set_size_12(int32_t value)
	{
		___size_12 = value;
	}

	inline static int32_t get_offset_of_primitive_13() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___primitive_13)); }
	inline int32_t get_primitive_13() const { return ___primitive_13; }
	inline int32_t* get_address_of_primitive_13() { return &___primitive_13; }
	inline void set_primitive_13(int32_t value)
	{
		___primitive_13 = value;
	}

	inline static int32_t get_offset_of_generatorBase_14() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960, ___generatorBase_14)); }
	inline int32_t get_generatorBase_14() const { return ___generatorBase_14; }
	inline int32_t* get_address_of_generatorBase_14() { return &___generatorBase_14; }
	inline void set_generatorBase_14(int32_t value)
	{
		___generatorBase_14 = value;
	}
};

struct GenericGF_t2563420960_StaticFields
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_12
	GenericGF_t2563420960 * ___AZTEC_DATA_12_0;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_10
	GenericGF_t2563420960 * ___AZTEC_DATA_10_1;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_6
	GenericGF_t2563420960 * ___AZTEC_DATA_6_2;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_PARAM
	GenericGF_t2563420960 * ___AZTEC_PARAM_3;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::QR_CODE_FIELD_256
	GenericGF_t2563420960 * ___QR_CODE_FIELD_256_4;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::DATA_MATRIX_FIELD_256
	GenericGF_t2563420960 * ___DATA_MATRIX_FIELD_256_5;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::AZTEC_DATA_8
	GenericGF_t2563420960 * ___AZTEC_DATA_8_6;
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.GenericGF::MAXICODE_FIELD_64
	GenericGF_t2563420960 * ___MAXICODE_FIELD_64_7;

public:
	inline static int32_t get_offset_of_AZTEC_DATA_12_0() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___AZTEC_DATA_12_0)); }
	inline GenericGF_t2563420960 * get_AZTEC_DATA_12_0() const { return ___AZTEC_DATA_12_0; }
	inline GenericGF_t2563420960 ** get_address_of_AZTEC_DATA_12_0() { return &___AZTEC_DATA_12_0; }
	inline void set_AZTEC_DATA_12_0(GenericGF_t2563420960 * value)
	{
		___AZTEC_DATA_12_0 = value;
		Il2CppCodeGenWriteBarrier(&___AZTEC_DATA_12_0, value);
	}

	inline static int32_t get_offset_of_AZTEC_DATA_10_1() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___AZTEC_DATA_10_1)); }
	inline GenericGF_t2563420960 * get_AZTEC_DATA_10_1() const { return ___AZTEC_DATA_10_1; }
	inline GenericGF_t2563420960 ** get_address_of_AZTEC_DATA_10_1() { return &___AZTEC_DATA_10_1; }
	inline void set_AZTEC_DATA_10_1(GenericGF_t2563420960 * value)
	{
		___AZTEC_DATA_10_1 = value;
		Il2CppCodeGenWriteBarrier(&___AZTEC_DATA_10_1, value);
	}

	inline static int32_t get_offset_of_AZTEC_DATA_6_2() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___AZTEC_DATA_6_2)); }
	inline GenericGF_t2563420960 * get_AZTEC_DATA_6_2() const { return ___AZTEC_DATA_6_2; }
	inline GenericGF_t2563420960 ** get_address_of_AZTEC_DATA_6_2() { return &___AZTEC_DATA_6_2; }
	inline void set_AZTEC_DATA_6_2(GenericGF_t2563420960 * value)
	{
		___AZTEC_DATA_6_2 = value;
		Il2CppCodeGenWriteBarrier(&___AZTEC_DATA_6_2, value);
	}

	inline static int32_t get_offset_of_AZTEC_PARAM_3() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___AZTEC_PARAM_3)); }
	inline GenericGF_t2563420960 * get_AZTEC_PARAM_3() const { return ___AZTEC_PARAM_3; }
	inline GenericGF_t2563420960 ** get_address_of_AZTEC_PARAM_3() { return &___AZTEC_PARAM_3; }
	inline void set_AZTEC_PARAM_3(GenericGF_t2563420960 * value)
	{
		___AZTEC_PARAM_3 = value;
		Il2CppCodeGenWriteBarrier(&___AZTEC_PARAM_3, value);
	}

	inline static int32_t get_offset_of_QR_CODE_FIELD_256_4() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___QR_CODE_FIELD_256_4)); }
	inline GenericGF_t2563420960 * get_QR_CODE_FIELD_256_4() const { return ___QR_CODE_FIELD_256_4; }
	inline GenericGF_t2563420960 ** get_address_of_QR_CODE_FIELD_256_4() { return &___QR_CODE_FIELD_256_4; }
	inline void set_QR_CODE_FIELD_256_4(GenericGF_t2563420960 * value)
	{
		___QR_CODE_FIELD_256_4 = value;
		Il2CppCodeGenWriteBarrier(&___QR_CODE_FIELD_256_4, value);
	}

	inline static int32_t get_offset_of_DATA_MATRIX_FIELD_256_5() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___DATA_MATRIX_FIELD_256_5)); }
	inline GenericGF_t2563420960 * get_DATA_MATRIX_FIELD_256_5() const { return ___DATA_MATRIX_FIELD_256_5; }
	inline GenericGF_t2563420960 ** get_address_of_DATA_MATRIX_FIELD_256_5() { return &___DATA_MATRIX_FIELD_256_5; }
	inline void set_DATA_MATRIX_FIELD_256_5(GenericGF_t2563420960 * value)
	{
		___DATA_MATRIX_FIELD_256_5 = value;
		Il2CppCodeGenWriteBarrier(&___DATA_MATRIX_FIELD_256_5, value);
	}

	inline static int32_t get_offset_of_AZTEC_DATA_8_6() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___AZTEC_DATA_8_6)); }
	inline GenericGF_t2563420960 * get_AZTEC_DATA_8_6() const { return ___AZTEC_DATA_8_6; }
	inline GenericGF_t2563420960 ** get_address_of_AZTEC_DATA_8_6() { return &___AZTEC_DATA_8_6; }
	inline void set_AZTEC_DATA_8_6(GenericGF_t2563420960 * value)
	{
		___AZTEC_DATA_8_6 = value;
		Il2CppCodeGenWriteBarrier(&___AZTEC_DATA_8_6, value);
	}

	inline static int32_t get_offset_of_MAXICODE_FIELD_64_7() { return static_cast<int32_t>(offsetof(GenericGF_t2563420960_StaticFields, ___MAXICODE_FIELD_64_7)); }
	inline GenericGF_t2563420960 * get_MAXICODE_FIELD_64_7() const { return ___MAXICODE_FIELD_64_7; }
	inline GenericGF_t2563420960 ** get_address_of_MAXICODE_FIELD_64_7() { return &___MAXICODE_FIELD_64_7; }
	inline void set_MAXICODE_FIELD_64_7(GenericGF_t2563420960 * value)
	{
		___MAXICODE_FIELD_64_7 = value;
		Il2CppCodeGenWriteBarrier(&___MAXICODE_FIELD_64_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
