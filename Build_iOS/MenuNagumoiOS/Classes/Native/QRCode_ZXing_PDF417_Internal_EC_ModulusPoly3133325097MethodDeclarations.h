﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.EC.ModulusPoly
struct ModulusPoly_t3133325097;
// ZXing.PDF417.Internal.EC.ModulusGF
struct ModulusGF_t2738856732;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_EC_ModulusGF2738856732.h"
#include "QRCode_ZXing_PDF417_Internal_EC_ModulusPoly3133325097.h"

// System.Void ZXing.PDF417.Internal.EC.ModulusPoly::.ctor(ZXing.PDF417.Internal.EC.ModulusGF,System.Int32[])
extern "C"  void ModulusPoly__ctor_m232616605 (ModulusPoly_t3133325097 * __this, ModulusGF_t2738856732 * ___field0, Int32U5BU5D_t3230847821* ___coefficients1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::get_Degree()
extern "C"  int32_t ModulusPoly_get_Degree_m3426915156 (ModulusPoly_t3133325097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.PDF417.Internal.EC.ModulusPoly::get_isZero()
extern "C"  bool ModulusPoly_get_isZero_m2944506112 (ModulusPoly_t3133325097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::getCoefficient(System.Int32)
extern "C"  int32_t ModulusPoly_getCoefficient_m3625616943 (ModulusPoly_t3133325097 * __this, int32_t ___degree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::evaluateAt(System.Int32)
extern "C"  int32_t ModulusPoly_evaluateAt_m974804252 (ModulusPoly_t3133325097 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::add(ZXing.PDF417.Internal.EC.ModulusPoly)
extern "C"  ModulusPoly_t3133325097 * ModulusPoly_add_m3609013749 (ModulusPoly_t3133325097 * __this, ModulusPoly_t3133325097 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::subtract(ZXing.PDF417.Internal.EC.ModulusPoly)
extern "C"  ModulusPoly_t3133325097 * ModulusPoly_subtract_m1448095314 (ModulusPoly_t3133325097 * __this, ModulusPoly_t3133325097 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiply(ZXing.PDF417.Internal.EC.ModulusPoly)
extern "C"  ModulusPoly_t3133325097 * ModulusPoly_multiply_m2460282210 (ModulusPoly_t3133325097 * __this, ModulusPoly_t3133325097 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::getNegative()
extern "C"  ModulusPoly_t3133325097 * ModulusPoly_getNegative_m2424688839 (ModulusPoly_t3133325097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiply(System.Int32)
extern "C"  ModulusPoly_t3133325097 * ModulusPoly_multiply_m4011187579 (ModulusPoly_t3133325097 * __this, int32_t ___scalar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiplyByMonomial(System.Int32,System.Int32)
extern "C"  ModulusPoly_t3133325097 * ModulusPoly_multiplyByMonomial_m566045723 (ModulusPoly_t3133325097 * __this, int32_t ___degree0, int32_t ___coefficient1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.EC.ModulusPoly::ToString()
extern "C"  String_t* ModulusPoly_ToString_m2302302336 (ModulusPoly_t3133325097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
