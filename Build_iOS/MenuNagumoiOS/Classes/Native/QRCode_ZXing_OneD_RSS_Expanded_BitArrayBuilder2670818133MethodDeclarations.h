﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>
struct List_1_t486584943;

#include "codegen/il2cpp-codegen.h"

// ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.BitArrayBuilder::buildBitArray(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern "C"  BitArray_t4163851164 * BitArrayBuilder_buildBitArray_m3713733667 (Il2CppObject * __this /* static, unused */, List_1_t486584943 * ___pairs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
