﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;

#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceF3788299492.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FakeDeviceFileInfo
struct  FakeDeviceFileInfo_t2440618901  : public DeviceFileInfoAbstract_t3788299492
{
public:
	// MagicTV.vo.ResultRevisionVO FakeDeviceFileInfo::_ResultRevisionVO
	ResultRevisionVO_t2445597391 * ____ResultRevisionVO_6;

public:
	inline static int32_t get_offset_of__ResultRevisionVO_6() { return static_cast<int32_t>(offsetof(FakeDeviceFileInfo_t2440618901, ____ResultRevisionVO_6)); }
	inline ResultRevisionVO_t2445597391 * get__ResultRevisionVO_6() const { return ____ResultRevisionVO_6; }
	inline ResultRevisionVO_t2445597391 ** get_address_of__ResultRevisionVO_6() { return &____ResultRevisionVO_6; }
	inline void set__ResultRevisionVO_6(ResultRevisionVO_t2445597391 * value)
	{
		____ResultRevisionVO_6 = value;
		Il2CppCodeGenWriteBarrier(&____ResultRevisionVO_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
