﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUIRect
struct SampleAppUIRect_t2784570703;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void SampleAppUIRect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void SampleAppUIRect__ctor_m2015854558 (SampleAppUIRect_t2784570703 * __this, float ___x0, float ___y1, float ___W2, float ___H3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect SampleAppUIRect::get_rect()
extern "C"  Rect_t4241904616  SampleAppUIRect_get_rect_m87013459 (SampleAppUIRect_t2784570703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
