﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1207765775.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZ2425423099.h"

// System.Void System.Array/InternalEnumerator`1<ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1087477452_gshared (InternalEnumerator_1_t1207765775 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1087477452(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1207765775 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1087477452_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1483715412_gshared (InternalEnumerator_1_t1207765775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1483715412(__this, method) ((  void (*) (InternalEnumerator_1_t1207765775 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1483715412_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m566111232_gshared (InternalEnumerator_1_t1207765775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m566111232(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1207765775 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m566111232_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4264226723_gshared (InternalEnumerator_1_t1207765775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4264226723(__this, method) ((  void (*) (InternalEnumerator_1_t1207765775 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4264226723_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3664265792_gshared (InternalEnumerator_1_t1207765775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3664265792(__this, method) ((  bool (*) (InternalEnumerator_1_t1207765775 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3664265792_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement>::get_Current()
extern "C"  StackElement_t2425423099  InternalEnumerator_1_get_Current_m3425102291_gshared (InternalEnumerator_1_t1207765775 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3425102291(__this, method) ((  StackElement_t2425423099  (*) (InternalEnumerator_1_t1207765775 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3425102291_gshared)(__this, method)
