﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerRequestMobile
struct ServerRequestMobile_t675100796;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.request.ServerRequestMobile::.ctor()
extern "C"  void ServerRequestMobile__ctor_m3002206690 (ServerRequestMobile_t675100796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
