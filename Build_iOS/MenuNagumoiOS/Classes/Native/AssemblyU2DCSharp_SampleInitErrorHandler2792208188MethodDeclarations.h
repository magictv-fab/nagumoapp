﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleInitErrorHandler
struct SampleInitErrorHandler_t2792208188;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"

// System.Void SampleInitErrorHandler::.ctor()
extern "C"  void SampleInitErrorHandler__ctor_m2755846367 (SampleInitErrorHandler_t2792208188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleInitErrorHandler::InitPopUp()
extern "C"  void SampleInitErrorHandler_InitPopUp_m1719284377 (SampleInitErrorHandler_t2792208188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleInitErrorHandler::Draw()
extern "C"  void SampleInitErrorHandler_Draw_m2256052425 (SampleInitErrorHandler_t2792208188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleInitErrorHandler::DrawPopUp()
extern "C"  void SampleInitErrorHandler_DrawPopUp_m3836250085 (SampleInitErrorHandler_t2792208188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleInitErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C"  void SampleInitErrorHandler_SetErrorCode_m2587396848 (SampleInitErrorHandler_t2792208188 * __this, int32_t ___errorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
