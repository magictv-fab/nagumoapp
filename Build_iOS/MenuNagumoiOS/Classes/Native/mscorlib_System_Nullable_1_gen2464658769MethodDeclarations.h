﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2464658769.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolShapeHint2380532246.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2981560184_gshared (Nullable_1_t2464658769 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2981560184(__this, ___value0, method) ((  void (*) (Nullable_1_t2464658769 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2981560184_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3505038835_gshared (Nullable_1_t2464658769 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3505038835(__this, method) ((  bool (*) (Nullable_1_t2464658769 *, const MethodInfo*))Nullable_1_get_HasValue_m3505038835_gshared)(__this, method)
// T System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m944224721_gshared (Nullable_1_t2464658769 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m944224721(__this, method) ((  int32_t (*) (Nullable_1_t2464658769 *, const MethodInfo*))Nullable_1_get_Value_m944224721_gshared)(__this, method)
// System.Boolean System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1840336025_gshared (Nullable_1_t2464658769 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1840336025(__this, ___other0, method) ((  bool (*) (Nullable_1_t2464658769 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1840336025_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m999725190_gshared (Nullable_1_t2464658769 * __this, Nullable_1_t2464658769  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m999725190(__this, ___other0, method) ((  bool (*) (Nullable_1_t2464658769 *, Nullable_1_t2464658769 , const MethodInfo*))Nullable_1_Equals_m999725190_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3162338289_gshared (Nullable_1_t2464658769 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3162338289(__this, method) ((  int32_t (*) (Nullable_1_t2464658769 *, const MethodInfo*))Nullable_1_GetHashCode_m3162338289_gshared)(__this, method)
// T System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1533499558_gshared (Nullable_1_t2464658769 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1533499558(__this, method) ((  int32_t (*) (Nullable_1_t2464658769 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1533499558_gshared)(__this, method)
// System.String System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1346051463_gshared (Nullable_1_t2464658769 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1346051463(__this, method) ((  String_t* (*) (Nullable_1_t2464658769 *, const MethodInfo*))Nullable_1_ToString_m1346051463_gshared)(__this, method)
