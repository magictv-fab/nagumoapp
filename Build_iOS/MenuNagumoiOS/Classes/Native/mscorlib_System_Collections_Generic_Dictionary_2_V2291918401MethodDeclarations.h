﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2988183457MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3121602735(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2291918401 *, Dictionary_2_t3591312688 *, const MethodInfo*))ValueCollection__ctor_m2430935490_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3156740515(__this, ___item0, method) ((  void (*) (ValueCollection_t2291918401 *, InAppEventDispatcherOnOff_t3474551315 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m502061616_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1930575852(__this, method) ((  void (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2177326841_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1696732199(__this, ___item0, method) ((  bool (*) (ValueCollection_t2291918401 *, InAppEventDispatcherOnOff_t3474551315 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3142858230_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1508903564(__this, ___item0, method) ((  bool (*) (ValueCollection_t2291918401 *, InAppEventDispatcherOnOff_t3474551315 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1586385947_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3669748396(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3157097991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m463137264(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2291918401 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1297243325_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3787121151(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2190893560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4266875354(__this, method) ((  bool (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1418034089_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m813569722(__this, method) ((  bool (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m535901961_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2771508396(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2655919669_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2392895798(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2291918401 *, InAppEventDispatcherOnOffU5BU5D_t3213007938*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3397911945_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::GetEnumerator()
#define ValueCollection_GetEnumerator_m333533215(__this, method) ((  Enumerator_t1523146096  (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_GetEnumerator_m52846380_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_Count()
#define ValueCollection_get_Count_m500671476(__this, method) ((  int32_t (*) (ValueCollection_t2291918401 *, const MethodInfo*))ValueCollection_get_Count_m2638042703_gshared)(__this, method)
