﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataTable
struct DataTable_t1629861412;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReturnDataVO
struct  ReturnDataVO_t544837971  : public Il2CppObject
{
public:
	// System.Boolean ReturnDataVO::success
	bool ___success_0;
	// DataTable ReturnDataVO::result
	DataTable_t1629861412 * ___result_1;
	// System.String ReturnDataVO::message
	String_t* ___message_2;
	// System.Boolean ReturnDataVO::hasResult
	bool ___hasResult_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(ReturnDataVO_t544837971, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(ReturnDataVO_t544837971, ___result_1)); }
	inline DataTable_t1629861412 * get_result_1() const { return ___result_1; }
	inline DataTable_t1629861412 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(DataTable_t1629861412 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier(&___result_1, value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(ReturnDataVO_t544837971, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}

	inline static int32_t get_offset_of_hasResult_3() { return static_cast<int32_t>(offsetof(ReturnDataVO_t544837971, ___hasResult_3)); }
	inline bool get_hasResult_3() const { return ___hasResult_3; }
	inline bool* get_address_of_hasResult_3() { return &___hasResult_3; }
	inline void set_hasResult_3(bool value)
	{
		___hasResult_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
