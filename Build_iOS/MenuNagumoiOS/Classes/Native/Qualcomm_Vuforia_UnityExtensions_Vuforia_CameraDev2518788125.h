﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t4191130096;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t1722560608;
// Vuforia.WebCamImpl
struct WebCamImpl_t1072092957;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi877546845.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev3311506418.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev2052521376.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDeviceImpl
struct  CameraDeviceImpl_t2518788125  : public CameraDevice_t877546845
{
public:
	// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDeviceImpl::mCameraImages
	Dictionary_2_t4191130096 * ___mCameraImages_1;
	// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT> Vuforia.CameraDeviceImpl::mForcedCameraFormats
	List_1_t1722560608 * ___mForcedCameraFormats_2;
	// System.Boolean Vuforia.CameraDeviceImpl::mCameraReady
	bool ___mCameraReady_4;
	// System.Boolean Vuforia.CameraDeviceImpl::mIsDirty
	bool ___mIsDirty_5;
	// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::mCameraDirection
	int32_t ___mCameraDirection_6;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.CameraDeviceImpl::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_7;
	// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::mVideoModeData
	VideoModeData_t2052521376  ___mVideoModeData_8;
	// System.Boolean Vuforia.CameraDeviceImpl::mVideoModeDataNeedsUpdate
	bool ___mVideoModeDataNeedsUpdate_9;
	// System.Boolean Vuforia.CameraDeviceImpl::mHasCameraDeviceModeBeenSet
	bool ___mHasCameraDeviceModeBeenSet_10;

public:
	inline static int32_t get_offset_of_mCameraImages_1() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mCameraImages_1)); }
	inline Dictionary_2_t4191130096 * get_mCameraImages_1() const { return ___mCameraImages_1; }
	inline Dictionary_2_t4191130096 ** get_address_of_mCameraImages_1() { return &___mCameraImages_1; }
	inline void set_mCameraImages_1(Dictionary_2_t4191130096 * value)
	{
		___mCameraImages_1 = value;
		Il2CppCodeGenWriteBarrier(&___mCameraImages_1, value);
	}

	inline static int32_t get_offset_of_mForcedCameraFormats_2() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mForcedCameraFormats_2)); }
	inline List_1_t1722560608 * get_mForcedCameraFormats_2() const { return ___mForcedCameraFormats_2; }
	inline List_1_t1722560608 ** get_address_of_mForcedCameraFormats_2() { return &___mForcedCameraFormats_2; }
	inline void set_mForcedCameraFormats_2(List_1_t1722560608 * value)
	{
		___mForcedCameraFormats_2 = value;
		Il2CppCodeGenWriteBarrier(&___mForcedCameraFormats_2, value);
	}

	inline static int32_t get_offset_of_mCameraReady_4() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mCameraReady_4)); }
	inline bool get_mCameraReady_4() const { return ___mCameraReady_4; }
	inline bool* get_address_of_mCameraReady_4() { return &___mCameraReady_4; }
	inline void set_mCameraReady_4(bool value)
	{
		___mCameraReady_4 = value;
	}

	inline static int32_t get_offset_of_mIsDirty_5() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mIsDirty_5)); }
	inline bool get_mIsDirty_5() const { return ___mIsDirty_5; }
	inline bool* get_address_of_mIsDirty_5() { return &___mIsDirty_5; }
	inline void set_mIsDirty_5(bool value)
	{
		___mIsDirty_5 = value;
	}

	inline static int32_t get_offset_of_mCameraDirection_6() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mCameraDirection_6)); }
	inline int32_t get_mCameraDirection_6() const { return ___mCameraDirection_6; }
	inline int32_t* get_address_of_mCameraDirection_6() { return &___mCameraDirection_6; }
	inline void set_mCameraDirection_6(int32_t value)
	{
		___mCameraDirection_6 = value;
	}

	inline static int32_t get_offset_of_mCameraDeviceMode_7() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mCameraDeviceMode_7)); }
	inline int32_t get_mCameraDeviceMode_7() const { return ___mCameraDeviceMode_7; }
	inline int32_t* get_address_of_mCameraDeviceMode_7() { return &___mCameraDeviceMode_7; }
	inline void set_mCameraDeviceMode_7(int32_t value)
	{
		___mCameraDeviceMode_7 = value;
	}

	inline static int32_t get_offset_of_mVideoModeData_8() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mVideoModeData_8)); }
	inline VideoModeData_t2052521376  get_mVideoModeData_8() const { return ___mVideoModeData_8; }
	inline VideoModeData_t2052521376 * get_address_of_mVideoModeData_8() { return &___mVideoModeData_8; }
	inline void set_mVideoModeData_8(VideoModeData_t2052521376  value)
	{
		___mVideoModeData_8 = value;
	}

	inline static int32_t get_offset_of_mVideoModeDataNeedsUpdate_9() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mVideoModeDataNeedsUpdate_9)); }
	inline bool get_mVideoModeDataNeedsUpdate_9() const { return ___mVideoModeDataNeedsUpdate_9; }
	inline bool* get_address_of_mVideoModeDataNeedsUpdate_9() { return &___mVideoModeDataNeedsUpdate_9; }
	inline void set_mVideoModeDataNeedsUpdate_9(bool value)
	{
		___mVideoModeDataNeedsUpdate_9 = value;
	}

	inline static int32_t get_offset_of_mHasCameraDeviceModeBeenSet_10() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125, ___mHasCameraDeviceModeBeenSet_10)); }
	inline bool get_mHasCameraDeviceModeBeenSet_10() const { return ___mHasCameraDeviceModeBeenSet_10; }
	inline bool* get_address_of_mHasCameraDeviceModeBeenSet_10() { return &___mHasCameraDeviceModeBeenSet_10; }
	inline void set_mHasCameraDeviceModeBeenSet_10(bool value)
	{
		___mHasCameraDeviceModeBeenSet_10 = value;
	}
};

struct CameraDeviceImpl_t2518788125_StaticFields
{
public:
	// Vuforia.WebCamImpl Vuforia.CameraDeviceImpl::mWebCam
	WebCamImpl_t1072092957 * ___mWebCam_3;

public:
	inline static int32_t get_offset_of_mWebCam_3() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t2518788125_StaticFields, ___mWebCam_3)); }
	inline WebCamImpl_t1072092957 * get_mWebCam_3() const { return ___mWebCam_3; }
	inline WebCamImpl_t1072092957 ** get_address_of_mWebCam_3() { return &___mWebCam_3; }
	inline void set_mWebCam_3(WebCamImpl_t1072092957 * value)
	{
		___mWebCam_3 = value;
		Il2CppCodeGenWriteBarrier(&___mWebCam_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
