﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange
struct PageSnapChange_t3629032906;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll1065660817.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollSnap
struct  ScrollSnap_t531428411  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Extensions.ScrollSnap/ScrollDirection UnityEngine.UI.Extensions.ScrollSnap::direction
	int32_t ___direction_2;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ScrollSnap::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnap::scrollRectTransform
	RectTransform_t972643934 * ___scrollRectTransform_4;
	// UnityEngine.Transform UnityEngine.UI.Extensions.ScrollSnap::listContainerTransform
	Transform_t1659122786 * ___listContainerTransform_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnap::rectTransform
	RectTransform_t972643934 * ___rectTransform_6;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::pages
	int32_t ___pages_7;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::startingPage
	int32_t ___startingPage_8;
	// UnityEngine.Vector3[] UnityEngine.UI.Extensions.ScrollSnap::pageAnchorPositions
	Vector3U5BU5D_t215400611* ___pageAnchorPositions_9;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.ScrollSnap::lerpTarget
	Vector3_t4282066566  ___lerpTarget_10;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::lerp
	bool ___lerp_11;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::listContainerMinPosition
	float ___listContainerMinPosition_12;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::listContainerMaxPosition
	float ___listContainerMaxPosition_13;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::listContainerSize
	float ___listContainerSize_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ScrollSnap::listContainerRectTransform
	RectTransform_t972643934 * ___listContainerRectTransform_15;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.ScrollSnap::listContainerCachedSize
	Vector2_t4282066565  ___listContainerCachedSize_16;
	// System.Single UnityEngine.UI.Extensions.ScrollSnap::itemSize
	float ___itemSize_17;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::itemsCount
	int32_t ___itemsCount_18;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.ScrollSnap::nextButton
	Button_t3896396478 * ___nextButton_19;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.ScrollSnap::prevButton
	Button_t3896396478 * ___prevButton_20;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::itemsVisibleAtOnce
	int32_t ___itemsVisibleAtOnce_21;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::autoLayoutItems
	bool ___autoLayoutItems_22;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::linkScrolbarSteps
	bool ___linkScrolbarSteps_23;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::linkScrolrectScrollSensitivity
	bool ___linkScrolrectScrollSensitivity_24;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::useFastSwipe
	bool ___useFastSwipe_25;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::fastSwipeThreshold
	int32_t ___fastSwipeThreshold_26;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::startDrag
	bool ___startDrag_27;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.ScrollSnap::positionOnDragStart
	Vector3_t4282066566  ___positionOnDragStart_28;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::pageOnDragStart
	int32_t ___pageOnDragStart_29;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::fastSwipeTimer
	bool ___fastSwipeTimer_30;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::fastSwipeCounter
	int32_t ___fastSwipeCounter_31;
	// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::fastSwipeTarget
	int32_t ___fastSwipeTarget_32;
	// System.Boolean UnityEngine.UI.Extensions.ScrollSnap::fastSwipe
	bool ___fastSwipe_33;
	// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange UnityEngine.UI.Extensions.ScrollSnap::onPageChange
	PageSnapChange_t3629032906 * ___onPageChange_34;

public:
	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_scrollRect_3() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___scrollRect_3)); }
	inline ScrollRect_t3606982749 * get_scrollRect_3() const { return ___scrollRect_3; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_3() { return &___scrollRect_3; }
	inline void set_scrollRect_3(ScrollRect_t3606982749 * value)
	{
		___scrollRect_3 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_3, value);
	}

	inline static int32_t get_offset_of_scrollRectTransform_4() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___scrollRectTransform_4)); }
	inline RectTransform_t972643934 * get_scrollRectTransform_4() const { return ___scrollRectTransform_4; }
	inline RectTransform_t972643934 ** get_address_of_scrollRectTransform_4() { return &___scrollRectTransform_4; }
	inline void set_scrollRectTransform_4(RectTransform_t972643934 * value)
	{
		___scrollRectTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRectTransform_4, value);
	}

	inline static int32_t get_offset_of_listContainerTransform_5() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___listContainerTransform_5)); }
	inline Transform_t1659122786 * get_listContainerTransform_5() const { return ___listContainerTransform_5; }
	inline Transform_t1659122786 ** get_address_of_listContainerTransform_5() { return &___listContainerTransform_5; }
	inline void set_listContainerTransform_5(Transform_t1659122786 * value)
	{
		___listContainerTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___listContainerTransform_5, value);
	}

	inline static int32_t get_offset_of_rectTransform_6() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___rectTransform_6)); }
	inline RectTransform_t972643934 * get_rectTransform_6() const { return ___rectTransform_6; }
	inline RectTransform_t972643934 ** get_address_of_rectTransform_6() { return &___rectTransform_6; }
	inline void set_rectTransform_6(RectTransform_t972643934 * value)
	{
		___rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___rectTransform_6, value);
	}

	inline static int32_t get_offset_of_pages_7() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___pages_7)); }
	inline int32_t get_pages_7() const { return ___pages_7; }
	inline int32_t* get_address_of_pages_7() { return &___pages_7; }
	inline void set_pages_7(int32_t value)
	{
		___pages_7 = value;
	}

	inline static int32_t get_offset_of_startingPage_8() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___startingPage_8)); }
	inline int32_t get_startingPage_8() const { return ___startingPage_8; }
	inline int32_t* get_address_of_startingPage_8() { return &___startingPage_8; }
	inline void set_startingPage_8(int32_t value)
	{
		___startingPage_8 = value;
	}

	inline static int32_t get_offset_of_pageAnchorPositions_9() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___pageAnchorPositions_9)); }
	inline Vector3U5BU5D_t215400611* get_pageAnchorPositions_9() const { return ___pageAnchorPositions_9; }
	inline Vector3U5BU5D_t215400611** get_address_of_pageAnchorPositions_9() { return &___pageAnchorPositions_9; }
	inline void set_pageAnchorPositions_9(Vector3U5BU5D_t215400611* value)
	{
		___pageAnchorPositions_9 = value;
		Il2CppCodeGenWriteBarrier(&___pageAnchorPositions_9, value);
	}

	inline static int32_t get_offset_of_lerpTarget_10() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___lerpTarget_10)); }
	inline Vector3_t4282066566  get_lerpTarget_10() const { return ___lerpTarget_10; }
	inline Vector3_t4282066566 * get_address_of_lerpTarget_10() { return &___lerpTarget_10; }
	inline void set_lerpTarget_10(Vector3_t4282066566  value)
	{
		___lerpTarget_10 = value;
	}

	inline static int32_t get_offset_of_lerp_11() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___lerp_11)); }
	inline bool get_lerp_11() const { return ___lerp_11; }
	inline bool* get_address_of_lerp_11() { return &___lerp_11; }
	inline void set_lerp_11(bool value)
	{
		___lerp_11 = value;
	}

	inline static int32_t get_offset_of_listContainerMinPosition_12() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___listContainerMinPosition_12)); }
	inline float get_listContainerMinPosition_12() const { return ___listContainerMinPosition_12; }
	inline float* get_address_of_listContainerMinPosition_12() { return &___listContainerMinPosition_12; }
	inline void set_listContainerMinPosition_12(float value)
	{
		___listContainerMinPosition_12 = value;
	}

	inline static int32_t get_offset_of_listContainerMaxPosition_13() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___listContainerMaxPosition_13)); }
	inline float get_listContainerMaxPosition_13() const { return ___listContainerMaxPosition_13; }
	inline float* get_address_of_listContainerMaxPosition_13() { return &___listContainerMaxPosition_13; }
	inline void set_listContainerMaxPosition_13(float value)
	{
		___listContainerMaxPosition_13 = value;
	}

	inline static int32_t get_offset_of_listContainerSize_14() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___listContainerSize_14)); }
	inline float get_listContainerSize_14() const { return ___listContainerSize_14; }
	inline float* get_address_of_listContainerSize_14() { return &___listContainerSize_14; }
	inline void set_listContainerSize_14(float value)
	{
		___listContainerSize_14 = value;
	}

	inline static int32_t get_offset_of_listContainerRectTransform_15() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___listContainerRectTransform_15)); }
	inline RectTransform_t972643934 * get_listContainerRectTransform_15() const { return ___listContainerRectTransform_15; }
	inline RectTransform_t972643934 ** get_address_of_listContainerRectTransform_15() { return &___listContainerRectTransform_15; }
	inline void set_listContainerRectTransform_15(RectTransform_t972643934 * value)
	{
		___listContainerRectTransform_15 = value;
		Il2CppCodeGenWriteBarrier(&___listContainerRectTransform_15, value);
	}

	inline static int32_t get_offset_of_listContainerCachedSize_16() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___listContainerCachedSize_16)); }
	inline Vector2_t4282066565  get_listContainerCachedSize_16() const { return ___listContainerCachedSize_16; }
	inline Vector2_t4282066565 * get_address_of_listContainerCachedSize_16() { return &___listContainerCachedSize_16; }
	inline void set_listContainerCachedSize_16(Vector2_t4282066565  value)
	{
		___listContainerCachedSize_16 = value;
	}

	inline static int32_t get_offset_of_itemSize_17() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___itemSize_17)); }
	inline float get_itemSize_17() const { return ___itemSize_17; }
	inline float* get_address_of_itemSize_17() { return &___itemSize_17; }
	inline void set_itemSize_17(float value)
	{
		___itemSize_17 = value;
	}

	inline static int32_t get_offset_of_itemsCount_18() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___itemsCount_18)); }
	inline int32_t get_itemsCount_18() const { return ___itemsCount_18; }
	inline int32_t* get_address_of_itemsCount_18() { return &___itemsCount_18; }
	inline void set_itemsCount_18(int32_t value)
	{
		___itemsCount_18 = value;
	}

	inline static int32_t get_offset_of_nextButton_19() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___nextButton_19)); }
	inline Button_t3896396478 * get_nextButton_19() const { return ___nextButton_19; }
	inline Button_t3896396478 ** get_address_of_nextButton_19() { return &___nextButton_19; }
	inline void set_nextButton_19(Button_t3896396478 * value)
	{
		___nextButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___nextButton_19, value);
	}

	inline static int32_t get_offset_of_prevButton_20() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___prevButton_20)); }
	inline Button_t3896396478 * get_prevButton_20() const { return ___prevButton_20; }
	inline Button_t3896396478 ** get_address_of_prevButton_20() { return &___prevButton_20; }
	inline void set_prevButton_20(Button_t3896396478 * value)
	{
		___prevButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___prevButton_20, value);
	}

	inline static int32_t get_offset_of_itemsVisibleAtOnce_21() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___itemsVisibleAtOnce_21)); }
	inline int32_t get_itemsVisibleAtOnce_21() const { return ___itemsVisibleAtOnce_21; }
	inline int32_t* get_address_of_itemsVisibleAtOnce_21() { return &___itemsVisibleAtOnce_21; }
	inline void set_itemsVisibleAtOnce_21(int32_t value)
	{
		___itemsVisibleAtOnce_21 = value;
	}

	inline static int32_t get_offset_of_autoLayoutItems_22() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___autoLayoutItems_22)); }
	inline bool get_autoLayoutItems_22() const { return ___autoLayoutItems_22; }
	inline bool* get_address_of_autoLayoutItems_22() { return &___autoLayoutItems_22; }
	inline void set_autoLayoutItems_22(bool value)
	{
		___autoLayoutItems_22 = value;
	}

	inline static int32_t get_offset_of_linkScrolbarSteps_23() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___linkScrolbarSteps_23)); }
	inline bool get_linkScrolbarSteps_23() const { return ___linkScrolbarSteps_23; }
	inline bool* get_address_of_linkScrolbarSteps_23() { return &___linkScrolbarSteps_23; }
	inline void set_linkScrolbarSteps_23(bool value)
	{
		___linkScrolbarSteps_23 = value;
	}

	inline static int32_t get_offset_of_linkScrolrectScrollSensitivity_24() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___linkScrolrectScrollSensitivity_24)); }
	inline bool get_linkScrolrectScrollSensitivity_24() const { return ___linkScrolrectScrollSensitivity_24; }
	inline bool* get_address_of_linkScrolrectScrollSensitivity_24() { return &___linkScrolrectScrollSensitivity_24; }
	inline void set_linkScrolrectScrollSensitivity_24(bool value)
	{
		___linkScrolrectScrollSensitivity_24 = value;
	}

	inline static int32_t get_offset_of_useFastSwipe_25() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___useFastSwipe_25)); }
	inline bool get_useFastSwipe_25() const { return ___useFastSwipe_25; }
	inline bool* get_address_of_useFastSwipe_25() { return &___useFastSwipe_25; }
	inline void set_useFastSwipe_25(bool value)
	{
		___useFastSwipe_25 = value;
	}

	inline static int32_t get_offset_of_fastSwipeThreshold_26() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___fastSwipeThreshold_26)); }
	inline int32_t get_fastSwipeThreshold_26() const { return ___fastSwipeThreshold_26; }
	inline int32_t* get_address_of_fastSwipeThreshold_26() { return &___fastSwipeThreshold_26; }
	inline void set_fastSwipeThreshold_26(int32_t value)
	{
		___fastSwipeThreshold_26 = value;
	}

	inline static int32_t get_offset_of_startDrag_27() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___startDrag_27)); }
	inline bool get_startDrag_27() const { return ___startDrag_27; }
	inline bool* get_address_of_startDrag_27() { return &___startDrag_27; }
	inline void set_startDrag_27(bool value)
	{
		___startDrag_27 = value;
	}

	inline static int32_t get_offset_of_positionOnDragStart_28() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___positionOnDragStart_28)); }
	inline Vector3_t4282066566  get_positionOnDragStart_28() const { return ___positionOnDragStart_28; }
	inline Vector3_t4282066566 * get_address_of_positionOnDragStart_28() { return &___positionOnDragStart_28; }
	inline void set_positionOnDragStart_28(Vector3_t4282066566  value)
	{
		___positionOnDragStart_28 = value;
	}

	inline static int32_t get_offset_of_pageOnDragStart_29() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___pageOnDragStart_29)); }
	inline int32_t get_pageOnDragStart_29() const { return ___pageOnDragStart_29; }
	inline int32_t* get_address_of_pageOnDragStart_29() { return &___pageOnDragStart_29; }
	inline void set_pageOnDragStart_29(int32_t value)
	{
		___pageOnDragStart_29 = value;
	}

	inline static int32_t get_offset_of_fastSwipeTimer_30() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___fastSwipeTimer_30)); }
	inline bool get_fastSwipeTimer_30() const { return ___fastSwipeTimer_30; }
	inline bool* get_address_of_fastSwipeTimer_30() { return &___fastSwipeTimer_30; }
	inline void set_fastSwipeTimer_30(bool value)
	{
		___fastSwipeTimer_30 = value;
	}

	inline static int32_t get_offset_of_fastSwipeCounter_31() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___fastSwipeCounter_31)); }
	inline int32_t get_fastSwipeCounter_31() const { return ___fastSwipeCounter_31; }
	inline int32_t* get_address_of_fastSwipeCounter_31() { return &___fastSwipeCounter_31; }
	inline void set_fastSwipeCounter_31(int32_t value)
	{
		___fastSwipeCounter_31 = value;
	}

	inline static int32_t get_offset_of_fastSwipeTarget_32() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___fastSwipeTarget_32)); }
	inline int32_t get_fastSwipeTarget_32() const { return ___fastSwipeTarget_32; }
	inline int32_t* get_address_of_fastSwipeTarget_32() { return &___fastSwipeTarget_32; }
	inline void set_fastSwipeTarget_32(int32_t value)
	{
		___fastSwipeTarget_32 = value;
	}

	inline static int32_t get_offset_of_fastSwipe_33() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___fastSwipe_33)); }
	inline bool get_fastSwipe_33() const { return ___fastSwipe_33; }
	inline bool* get_address_of_fastSwipe_33() { return &___fastSwipe_33; }
	inline void set_fastSwipe_33(bool value)
	{
		___fastSwipe_33 = value;
	}

	inline static int32_t get_offset_of_onPageChange_34() { return static_cast<int32_t>(offsetof(ScrollSnap_t531428411, ___onPageChange_34)); }
	inline PageSnapChange_t3629032906 * get_onPageChange_34() const { return ___onPageChange_34; }
	inline PageSnapChange_t3629032906 ** get_address_of_onPageChange_34() { return &___onPageChange_34; }
	inline void set_onPageChange_34(PageSnapChange_t3629032906 * value)
	{
		___onPageChange_34 = value;
		Il2CppCodeGenWriteBarrier(&___onPageChange_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
