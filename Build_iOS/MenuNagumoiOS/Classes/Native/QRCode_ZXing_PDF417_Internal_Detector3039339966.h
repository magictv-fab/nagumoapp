﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.Detector
struct  Detector_t3039339966  : public Il2CppObject
{
public:

public:
};

struct Detector_t3039339966_StaticFields
{
public:
	// System.Int32[] ZXing.PDF417.Internal.Detector::INDEXES_START_PATTERN
	Int32U5BU5D_t3230847821* ___INDEXES_START_PATTERN_0;
	// System.Int32[] ZXing.PDF417.Internal.Detector::INDEXES_STOP_PATTERN
	Int32U5BU5D_t3230847821* ___INDEXES_STOP_PATTERN_1;
	// System.Int32[] ZXing.PDF417.Internal.Detector::START_PATTERN
	Int32U5BU5D_t3230847821* ___START_PATTERN_2;
	// System.Int32[] ZXing.PDF417.Internal.Detector::STOP_PATTERN
	Int32U5BU5D_t3230847821* ___STOP_PATTERN_3;

public:
	inline static int32_t get_offset_of_INDEXES_START_PATTERN_0() { return static_cast<int32_t>(offsetof(Detector_t3039339966_StaticFields, ___INDEXES_START_PATTERN_0)); }
	inline Int32U5BU5D_t3230847821* get_INDEXES_START_PATTERN_0() const { return ___INDEXES_START_PATTERN_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_INDEXES_START_PATTERN_0() { return &___INDEXES_START_PATTERN_0; }
	inline void set_INDEXES_START_PATTERN_0(Int32U5BU5D_t3230847821* value)
	{
		___INDEXES_START_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier(&___INDEXES_START_PATTERN_0, value);
	}

	inline static int32_t get_offset_of_INDEXES_STOP_PATTERN_1() { return static_cast<int32_t>(offsetof(Detector_t3039339966_StaticFields, ___INDEXES_STOP_PATTERN_1)); }
	inline Int32U5BU5D_t3230847821* get_INDEXES_STOP_PATTERN_1() const { return ___INDEXES_STOP_PATTERN_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_INDEXES_STOP_PATTERN_1() { return &___INDEXES_STOP_PATTERN_1; }
	inline void set_INDEXES_STOP_PATTERN_1(Int32U5BU5D_t3230847821* value)
	{
		___INDEXES_STOP_PATTERN_1 = value;
		Il2CppCodeGenWriteBarrier(&___INDEXES_STOP_PATTERN_1, value);
	}

	inline static int32_t get_offset_of_START_PATTERN_2() { return static_cast<int32_t>(offsetof(Detector_t3039339966_StaticFields, ___START_PATTERN_2)); }
	inline Int32U5BU5D_t3230847821* get_START_PATTERN_2() const { return ___START_PATTERN_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_START_PATTERN_2() { return &___START_PATTERN_2; }
	inline void set_START_PATTERN_2(Int32U5BU5D_t3230847821* value)
	{
		___START_PATTERN_2 = value;
		Il2CppCodeGenWriteBarrier(&___START_PATTERN_2, value);
	}

	inline static int32_t get_offset_of_STOP_PATTERN_3() { return static_cast<int32_t>(offsetof(Detector_t3039339966_StaticFields, ___STOP_PATTERN_3)); }
	inline Int32U5BU5D_t3230847821* get_STOP_PATTERN_3() const { return ___STOP_PATTERN_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_STOP_PATTERN_3() { return &___STOP_PATTERN_3; }
	inline void set_STOP_PATTERN_3(Int32U5BU5D_t3230847821* value)
	{
		___STOP_PATTERN_3 = value;
		Il2CppCodeGenWriteBarrier(&___STOP_PATTERN_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
