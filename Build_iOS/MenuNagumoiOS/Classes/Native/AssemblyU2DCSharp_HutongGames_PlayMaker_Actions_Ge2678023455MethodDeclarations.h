﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextCollisionEvent
struct GetNextCollisionEvent_t2678023455;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextCollisionEvent::.ctor()
extern "C"  void GetNextCollisionEvent__ctor_m2188957431 (GetNextCollisionEvent_t2678023455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextCollisionEvent::Reset()
extern "C"  void GetNextCollisionEvent_Reset_m4130357668 (GetNextCollisionEvent_t2678023455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextCollisionEvent::OnEnter()
extern "C"  void GetNextCollisionEvent_OnEnter_m2740812046 (GetNextCollisionEvent_t2678023455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextCollisionEvent::DoGetNextCollisionEvent()
extern "C"  void GetNextCollisionEvent_DoGetNextCollisionEvent_m349678619 (GetNextCollisionEvent_t2678023455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
