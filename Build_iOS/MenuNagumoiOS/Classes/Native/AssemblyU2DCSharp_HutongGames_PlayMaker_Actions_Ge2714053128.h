﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get992085865.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAxisVector
struct  GetAxisVector_t2714053128  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAxisVector::horizontalAxis
	FsmString_t952858651 * ___horizontalAxis_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAxisVector::verticalAxis
	FsmString_t952858651 * ___verticalAxis_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxisVector::multiplier
	FsmFloat_t2134102846 * ___multiplier_11;
	// HutongGames.PlayMaker.Actions.GetAxisVector/AxisPlane HutongGames.PlayMaker.Actions.GetAxisVector::mapToPlane
	int32_t ___mapToPlane_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAxisVector::relativeTo
	FsmGameObject_t1697147867 * ___relativeTo_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAxisVector::storeVector
	FsmVector3_t533912882 * ___storeVector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxisVector::storeMagnitude
	FsmFloat_t2134102846 * ___storeMagnitude_15;

public:
	inline static int32_t get_offset_of_horizontalAxis_9() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___horizontalAxis_9)); }
	inline FsmString_t952858651 * get_horizontalAxis_9() const { return ___horizontalAxis_9; }
	inline FsmString_t952858651 ** get_address_of_horizontalAxis_9() { return &___horizontalAxis_9; }
	inline void set_horizontalAxis_9(FsmString_t952858651 * value)
	{
		___horizontalAxis_9 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalAxis_9, value);
	}

	inline static int32_t get_offset_of_verticalAxis_10() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___verticalAxis_10)); }
	inline FsmString_t952858651 * get_verticalAxis_10() const { return ___verticalAxis_10; }
	inline FsmString_t952858651 ** get_address_of_verticalAxis_10() { return &___verticalAxis_10; }
	inline void set_verticalAxis_10(FsmString_t952858651 * value)
	{
		___verticalAxis_10 = value;
		Il2CppCodeGenWriteBarrier(&___verticalAxis_10, value);
	}

	inline static int32_t get_offset_of_multiplier_11() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___multiplier_11)); }
	inline FsmFloat_t2134102846 * get_multiplier_11() const { return ___multiplier_11; }
	inline FsmFloat_t2134102846 ** get_address_of_multiplier_11() { return &___multiplier_11; }
	inline void set_multiplier_11(FsmFloat_t2134102846 * value)
	{
		___multiplier_11 = value;
		Il2CppCodeGenWriteBarrier(&___multiplier_11, value);
	}

	inline static int32_t get_offset_of_mapToPlane_12() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___mapToPlane_12)); }
	inline int32_t get_mapToPlane_12() const { return ___mapToPlane_12; }
	inline int32_t* get_address_of_mapToPlane_12() { return &___mapToPlane_12; }
	inline void set_mapToPlane_12(int32_t value)
	{
		___mapToPlane_12 = value;
	}

	inline static int32_t get_offset_of_relativeTo_13() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___relativeTo_13)); }
	inline FsmGameObject_t1697147867 * get_relativeTo_13() const { return ___relativeTo_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_relativeTo_13() { return &___relativeTo_13; }
	inline void set_relativeTo_13(FsmGameObject_t1697147867 * value)
	{
		___relativeTo_13 = value;
		Il2CppCodeGenWriteBarrier(&___relativeTo_13, value);
	}

	inline static int32_t get_offset_of_storeVector_14() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___storeVector_14)); }
	inline FsmVector3_t533912882 * get_storeVector_14() const { return ___storeVector_14; }
	inline FsmVector3_t533912882 ** get_address_of_storeVector_14() { return &___storeVector_14; }
	inline void set_storeVector_14(FsmVector3_t533912882 * value)
	{
		___storeVector_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeVector_14, value);
	}

	inline static int32_t get_offset_of_storeMagnitude_15() { return static_cast<int32_t>(offsetof(GetAxisVector_t2714053128, ___storeMagnitude_15)); }
	inline FsmFloat_t2134102846 * get_storeMagnitude_15() const { return ___storeMagnitude_15; }
	inline FsmFloat_t2134102846 ** get_address_of_storeMagnitude_15() { return &___storeMagnitude_15; }
	inline void set_storeMagnitude_15(FsmFloat_t2134102846 * value)
	{
		___storeMagnitude_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeMagnitude_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
