﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RoletaGame
struct RoletaGame_t108100629;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoletaGame
struct  RoletaGame_t108100629  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text RoletaGame::jogadasTxt
	Text_t9039225 * ___jogadasTxt_3;
	// UnityEngine.UI.Text RoletaGame::restaJogadasTxt
	Text_t9039225 * ___restaJogadasTxt_4;
	// System.Boolean RoletaGame::offline
	bool ___offline_5;
	// System.Int32 RoletaGame::points
	int32_t ___points_6;
	// System.Single RoletaGame::time
	float ___time_7;
	// System.Single RoletaGame::speed
	float ___speed_8;
	// System.Collections.Generic.List`1<System.Single> RoletaGame::pointAngles
	List_1_t1365137228 * ___pointAngles_9;
	// iTween/EaseType RoletaGame::easetype
	int32_t ___easetype_10;
	// UnityEngine.GameObject RoletaGame::roletaGO
	GameObject_t3674682005 * ___roletaGO_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> RoletaGame::popups
	List_1_t747900261 * ___popups_12;
	// System.Single RoletaGame::popupTime
	float ___popupTime_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> RoletaGame::ptsIndex
	Dictionary_2_t1151101739 * ___ptsIndex_14;
	// System.Boolean RoletaGame::inGame
	bool ___inGame_15;
	// System.Int32 RoletaGame::startSpins
	int32_t ___startSpins_16;

public:
	inline static int32_t get_offset_of_jogadasTxt_3() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___jogadasTxt_3)); }
	inline Text_t9039225 * get_jogadasTxt_3() const { return ___jogadasTxt_3; }
	inline Text_t9039225 ** get_address_of_jogadasTxt_3() { return &___jogadasTxt_3; }
	inline void set_jogadasTxt_3(Text_t9039225 * value)
	{
		___jogadasTxt_3 = value;
		Il2CppCodeGenWriteBarrier(&___jogadasTxt_3, value);
	}

	inline static int32_t get_offset_of_restaJogadasTxt_4() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___restaJogadasTxt_4)); }
	inline Text_t9039225 * get_restaJogadasTxt_4() const { return ___restaJogadasTxt_4; }
	inline Text_t9039225 ** get_address_of_restaJogadasTxt_4() { return &___restaJogadasTxt_4; }
	inline void set_restaJogadasTxt_4(Text_t9039225 * value)
	{
		___restaJogadasTxt_4 = value;
		Il2CppCodeGenWriteBarrier(&___restaJogadasTxt_4, value);
	}

	inline static int32_t get_offset_of_offline_5() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___offline_5)); }
	inline bool get_offline_5() const { return ___offline_5; }
	inline bool* get_address_of_offline_5() { return &___offline_5; }
	inline void set_offline_5(bool value)
	{
		___offline_5 = value;
	}

	inline static int32_t get_offset_of_points_6() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___points_6)); }
	inline int32_t get_points_6() const { return ___points_6; }
	inline int32_t* get_address_of_points_6() { return &___points_6; }
	inline void set_points_6(int32_t value)
	{
		___points_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_pointAngles_9() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___pointAngles_9)); }
	inline List_1_t1365137228 * get_pointAngles_9() const { return ___pointAngles_9; }
	inline List_1_t1365137228 ** get_address_of_pointAngles_9() { return &___pointAngles_9; }
	inline void set_pointAngles_9(List_1_t1365137228 * value)
	{
		___pointAngles_9 = value;
		Il2CppCodeGenWriteBarrier(&___pointAngles_9, value);
	}

	inline static int32_t get_offset_of_easetype_10() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___easetype_10)); }
	inline int32_t get_easetype_10() const { return ___easetype_10; }
	inline int32_t* get_address_of_easetype_10() { return &___easetype_10; }
	inline void set_easetype_10(int32_t value)
	{
		___easetype_10 = value;
	}

	inline static int32_t get_offset_of_roletaGO_11() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___roletaGO_11)); }
	inline GameObject_t3674682005 * get_roletaGO_11() const { return ___roletaGO_11; }
	inline GameObject_t3674682005 ** get_address_of_roletaGO_11() { return &___roletaGO_11; }
	inline void set_roletaGO_11(GameObject_t3674682005 * value)
	{
		___roletaGO_11 = value;
		Il2CppCodeGenWriteBarrier(&___roletaGO_11, value);
	}

	inline static int32_t get_offset_of_popups_12() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___popups_12)); }
	inline List_1_t747900261 * get_popups_12() const { return ___popups_12; }
	inline List_1_t747900261 ** get_address_of_popups_12() { return &___popups_12; }
	inline void set_popups_12(List_1_t747900261 * value)
	{
		___popups_12 = value;
		Il2CppCodeGenWriteBarrier(&___popups_12, value);
	}

	inline static int32_t get_offset_of_popupTime_13() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___popupTime_13)); }
	inline float get_popupTime_13() const { return ___popupTime_13; }
	inline float* get_address_of_popupTime_13() { return &___popupTime_13; }
	inline void set_popupTime_13(float value)
	{
		___popupTime_13 = value;
	}

	inline static int32_t get_offset_of_ptsIndex_14() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___ptsIndex_14)); }
	inline Dictionary_2_t1151101739 * get_ptsIndex_14() const { return ___ptsIndex_14; }
	inline Dictionary_2_t1151101739 ** get_address_of_ptsIndex_14() { return &___ptsIndex_14; }
	inline void set_ptsIndex_14(Dictionary_2_t1151101739 * value)
	{
		___ptsIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___ptsIndex_14, value);
	}

	inline static int32_t get_offset_of_inGame_15() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___inGame_15)); }
	inline bool get_inGame_15() const { return ___inGame_15; }
	inline bool* get_address_of_inGame_15() { return &___inGame_15; }
	inline void set_inGame_15(bool value)
	{
		___inGame_15 = value;
	}

	inline static int32_t get_offset_of_startSpins_16() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629, ___startSpins_16)); }
	inline int32_t get_startSpins_16() const { return ___startSpins_16; }
	inline int32_t* get_address_of_startSpins_16() { return &___startSpins_16; }
	inline void set_startSpins_16(int32_t value)
	{
		___startSpins_16 = value;
	}
};

struct RoletaGame_t108100629_StaticFields
{
public:
	// RoletaGame RoletaGame::instance
	RoletaGame_t108100629 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(RoletaGame_t108100629_StaticFields, ___instance_2)); }
	inline RoletaGame_t108100629 * get_instance_2() const { return ___instance_2; }
	inline RoletaGame_t108100629 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RoletaGame_t108100629 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
