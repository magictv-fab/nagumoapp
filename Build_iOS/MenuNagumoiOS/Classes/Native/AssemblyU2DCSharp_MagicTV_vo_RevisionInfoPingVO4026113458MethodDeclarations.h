﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.RevisionInfoPingVO
struct RevisionInfoPingVO_t4026113458;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.RevisionInfoPingVO::.ctor()
extern "C"  void RevisionInfoPingVO__ctor_m356693729 (RevisionInfoPingVO_t4026113458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.RevisionInfoPingVO::ToString()
extern "C"  String_t* RevisionInfoPingVO_ToString_m2854409708 (RevisionInfoPingVO_t4026113458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
