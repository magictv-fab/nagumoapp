﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct ResultPointsAndTransitions_t1206419768;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"

// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_From()
extern "C"  ResultPoint_t1538592853 * ResultPointsAndTransitions_get_From_m908680602 (ResultPointsAndTransitions_t1206419768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_From(ZXing.ResultPoint)
extern "C"  void ResultPointsAndTransitions_set_From_m2085284113 (ResultPointsAndTransitions_t1206419768 * __this, ResultPoint_t1538592853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_To()
extern "C"  ResultPoint_t1538592853 * ResultPointsAndTransitions_get_To_m4256101355 (ResultPointsAndTransitions_t1206419768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_To(ZXing.ResultPoint)
extern "C"  void ResultPointsAndTransitions_set_To_m1629458272 (ResultPointsAndTransitions_t1206419768 * __this, ResultPoint_t1538592853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::get_Transitions()
extern "C"  int32_t ResultPointsAndTransitions_get_Transitions_m1178702868 (ResultPointsAndTransitions_t1206419768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::set_Transitions(System.Int32)
extern "C"  void ResultPointsAndTransitions_set_Transitions_m4293088035 (ResultPointsAndTransitions_t1206419768 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::.ctor(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern "C"  void ResultPointsAndTransitions__ctor_m2253976798 (ResultPointsAndTransitions_t1206419768 * __this, ResultPoint_t1538592853 * ___from0, ResultPoint_t1538592853 * ___to1, int32_t ___transitions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::ToString()
extern "C"  String_t* ResultPointsAndTransitions_ToString_m2726866170 (ResultPointsAndTransitions_t1206419768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
