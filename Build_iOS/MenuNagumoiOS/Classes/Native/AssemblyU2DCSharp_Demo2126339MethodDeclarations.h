﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Demo
struct Demo_t2126339;

#include "codegen/il2cpp-codegen.h"

// System.Void Demo::.ctor()
extern "C"  void Demo__ctor_m2996546552 (Demo_t2126339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Demo::Start()
extern "C"  void Demo_Start_m1943684344 (Demo_t2126339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Demo::OnGUI()
extern "C"  void Demo_OnGUI_m2491945202 (Demo_t2126339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Demo::OnSwitch()
extern "C"  void Demo_OnSwitch_m3862159871 (Demo_t2126339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
