﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3435508189.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220476.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3435478332.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220348.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220439.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220538.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220352.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615707.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615732.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615703.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220447.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220472.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220414.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3053238939  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$2048 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU242048_t435508190  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$56 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU2456_t3379220477  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU241024_t435478334  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU241024_t435478334  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU2412_t3379220356  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU2440_t3379220439  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU2440_t3379220439  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU2440_t3379220439  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU2440_t3379220439  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU2440_t3379220439  ___U24U24fieldU2D9_9;
	// <PrivateImplementationDetails>/$ArrayType$76 <PrivateImplementationDetails>::$$field-10
	U24ArrayTypeU2476_t3379220538  ___U24U24fieldU2D10_10;
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU2416_t3379220357  ___U24U24fieldU2D11_11;
	// <PrivateImplementationDetails>/$ArrayType$116 <PrivateImplementationDetails>::$$field-12
	U24ArrayTypeU24116_t1676615707  ___U24U24fieldU2D12_12;
	// <PrivateImplementationDetails>/$ArrayType$116 <PrivateImplementationDetails>::$$field-13
	U24ArrayTypeU24116_t1676615707  ___U24U24fieldU2D13_13;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-14
	U24ArrayTypeU24120_t1676615734  ___U24U24fieldU2D14_14;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-15
	U24ArrayTypeU24120_t1676615734  ___U24U24fieldU2D15_15;
	// <PrivateImplementationDetails>/$ArrayType$76 <PrivateImplementationDetails>::$$field-16
	U24ArrayTypeU2476_t3379220538  ___U24U24fieldU2D16_16;
	// <PrivateImplementationDetails>/$ArrayType$112 <PrivateImplementationDetails>::$$field-17
	U24ArrayTypeU24112_t1676615703  ___U24U24fieldU2D17_17;
	// <PrivateImplementationDetails>/$ArrayType$48 <PrivateImplementationDetails>::$$field-18
	U24ArrayTypeU2448_t3379220449  ___U24U24fieldU2D18_18;
	// <PrivateImplementationDetails>/$ArrayType$52 <PrivateImplementationDetails>::$$field-19
	U24ArrayTypeU2452_t3379220473  ___U24U24fieldU2D19_19;
	// <PrivateImplementationDetails>/$ArrayType$36 <PrivateImplementationDetails>::$$field-20
	U24ArrayTypeU2436_t3379220414  ___U24U24fieldU2D20_20;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-21
	U24ArrayTypeU2440_t3379220439  ___U24U24fieldU2D21_21;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU242048_t435508190  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU242048_t435508190 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU242048_t435508190  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU2456_t3379220477  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU2456_t3379220477 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU2456_t3379220477  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU241024_t435478334  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU241024_t435478334 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU241024_t435478334  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU241024_t435478334  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU241024_t435478334 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU241024_t435478334  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU2412_t3379220356  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU2412_t3379220356 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU2412_t3379220356  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU2440_t3379220439  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU2440_t3379220439 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU2440_t3379220439  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU2440_t3379220439  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU2440_t3379220439 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU2440_t3379220439  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU2440_t3379220439  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU2440_t3379220439 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU2440_t3379220439  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU2440_t3379220439  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU2440_t3379220439 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU2440_t3379220439  value)
	{
		___U24U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D9_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D9_9)); }
	inline U24ArrayTypeU2440_t3379220439  get_U24U24fieldU2D9_9() const { return ___U24U24fieldU2D9_9; }
	inline U24ArrayTypeU2440_t3379220439 * get_address_of_U24U24fieldU2D9_9() { return &___U24U24fieldU2D9_9; }
	inline void set_U24U24fieldU2D9_9(U24ArrayTypeU2440_t3379220439  value)
	{
		___U24U24fieldU2D9_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D10_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D10_10)); }
	inline U24ArrayTypeU2476_t3379220538  get_U24U24fieldU2D10_10() const { return ___U24U24fieldU2D10_10; }
	inline U24ArrayTypeU2476_t3379220538 * get_address_of_U24U24fieldU2D10_10() { return &___U24U24fieldU2D10_10; }
	inline void set_U24U24fieldU2D10_10(U24ArrayTypeU2476_t3379220538  value)
	{
		___U24U24fieldU2D10_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D11_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D11_11)); }
	inline U24ArrayTypeU2416_t3379220357  get_U24U24fieldU2D11_11() const { return ___U24U24fieldU2D11_11; }
	inline U24ArrayTypeU2416_t3379220357 * get_address_of_U24U24fieldU2D11_11() { return &___U24U24fieldU2D11_11; }
	inline void set_U24U24fieldU2D11_11(U24ArrayTypeU2416_t3379220357  value)
	{
		___U24U24fieldU2D11_11 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D12_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D12_12)); }
	inline U24ArrayTypeU24116_t1676615707  get_U24U24fieldU2D12_12() const { return ___U24U24fieldU2D12_12; }
	inline U24ArrayTypeU24116_t1676615707 * get_address_of_U24U24fieldU2D12_12() { return &___U24U24fieldU2D12_12; }
	inline void set_U24U24fieldU2D12_12(U24ArrayTypeU24116_t1676615707  value)
	{
		___U24U24fieldU2D12_12 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D13_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D13_13)); }
	inline U24ArrayTypeU24116_t1676615707  get_U24U24fieldU2D13_13() const { return ___U24U24fieldU2D13_13; }
	inline U24ArrayTypeU24116_t1676615707 * get_address_of_U24U24fieldU2D13_13() { return &___U24U24fieldU2D13_13; }
	inline void set_U24U24fieldU2D13_13(U24ArrayTypeU24116_t1676615707  value)
	{
		___U24U24fieldU2D13_13 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D14_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D14_14)); }
	inline U24ArrayTypeU24120_t1676615734  get_U24U24fieldU2D14_14() const { return ___U24U24fieldU2D14_14; }
	inline U24ArrayTypeU24120_t1676615734 * get_address_of_U24U24fieldU2D14_14() { return &___U24U24fieldU2D14_14; }
	inline void set_U24U24fieldU2D14_14(U24ArrayTypeU24120_t1676615734  value)
	{
		___U24U24fieldU2D14_14 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D15_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D15_15)); }
	inline U24ArrayTypeU24120_t1676615734  get_U24U24fieldU2D15_15() const { return ___U24U24fieldU2D15_15; }
	inline U24ArrayTypeU24120_t1676615734 * get_address_of_U24U24fieldU2D15_15() { return &___U24U24fieldU2D15_15; }
	inline void set_U24U24fieldU2D15_15(U24ArrayTypeU24120_t1676615734  value)
	{
		___U24U24fieldU2D15_15 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D16_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D16_16)); }
	inline U24ArrayTypeU2476_t3379220538  get_U24U24fieldU2D16_16() const { return ___U24U24fieldU2D16_16; }
	inline U24ArrayTypeU2476_t3379220538 * get_address_of_U24U24fieldU2D16_16() { return &___U24U24fieldU2D16_16; }
	inline void set_U24U24fieldU2D16_16(U24ArrayTypeU2476_t3379220538  value)
	{
		___U24U24fieldU2D16_16 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D17_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D17_17)); }
	inline U24ArrayTypeU24112_t1676615703  get_U24U24fieldU2D17_17() const { return ___U24U24fieldU2D17_17; }
	inline U24ArrayTypeU24112_t1676615703 * get_address_of_U24U24fieldU2D17_17() { return &___U24U24fieldU2D17_17; }
	inline void set_U24U24fieldU2D17_17(U24ArrayTypeU24112_t1676615703  value)
	{
		___U24U24fieldU2D17_17 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D18_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D18_18)); }
	inline U24ArrayTypeU2448_t3379220449  get_U24U24fieldU2D18_18() const { return ___U24U24fieldU2D18_18; }
	inline U24ArrayTypeU2448_t3379220449 * get_address_of_U24U24fieldU2D18_18() { return &___U24U24fieldU2D18_18; }
	inline void set_U24U24fieldU2D18_18(U24ArrayTypeU2448_t3379220449  value)
	{
		___U24U24fieldU2D18_18 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D19_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D19_19)); }
	inline U24ArrayTypeU2452_t3379220473  get_U24U24fieldU2D19_19() const { return ___U24U24fieldU2D19_19; }
	inline U24ArrayTypeU2452_t3379220473 * get_address_of_U24U24fieldU2D19_19() { return &___U24U24fieldU2D19_19; }
	inline void set_U24U24fieldU2D19_19(U24ArrayTypeU2452_t3379220473  value)
	{
		___U24U24fieldU2D19_19 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D20_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D20_20)); }
	inline U24ArrayTypeU2436_t3379220414  get_U24U24fieldU2D20_20() const { return ___U24U24fieldU2D20_20; }
	inline U24ArrayTypeU2436_t3379220414 * get_address_of_U24U24fieldU2D20_20() { return &___U24U24fieldU2D20_20; }
	inline void set_U24U24fieldU2D20_20(U24ArrayTypeU2436_t3379220414  value)
	{
		___U24U24fieldU2D20_20 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D21_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields, ___U24U24fieldU2D21_21)); }
	inline U24ArrayTypeU2440_t3379220439  get_U24U24fieldU2D21_21() const { return ___U24U24fieldU2D21_21; }
	inline U24ArrayTypeU2440_t3379220439 * get_address_of_U24U24fieldU2D21_21() { return &___U24U24fieldU2D21_21; }
	inline void set_U24U24fieldU2D21_21(U24ArrayTypeU2440_t3379220439  value)
	{
		___U24U24fieldU2D21_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
