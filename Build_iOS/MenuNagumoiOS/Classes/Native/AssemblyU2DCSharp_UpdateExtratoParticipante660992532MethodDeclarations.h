﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateExtratoParticipante
struct UpdateExtratoParticipante_t660992532;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UpdateExtratoParticipante::.ctor()
extern "C"  void UpdateExtratoParticipante__ctor_m4183517655 (UpdateExtratoParticipante_t660992532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateExtratoParticipante::.cctor()
extern "C"  void UpdateExtratoParticipante__cctor_m357932214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateExtratoParticipante::OnEnable()
extern "C"  void UpdateExtratoParticipante_OnEnable_m1885043279 (UpdateExtratoParticipante_t660992532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UpdateExtratoParticipante::ILogin()
extern "C"  Il2CppObject * UpdateExtratoParticipante_ILogin_m3273707205 (UpdateExtratoParticipante_t660992532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UpdateExtratoParticipante::Base64Encode(System.String)
extern "C"  String_t* UpdateExtratoParticipante_Base64Encode_m2602117779 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
