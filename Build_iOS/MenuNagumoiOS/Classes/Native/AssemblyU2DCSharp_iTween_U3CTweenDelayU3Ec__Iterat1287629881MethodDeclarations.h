﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<TweenDelay>c__Iterator87
struct U3CTweenDelayU3Ec__Iterator87_t1287629881;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<TweenDelay>c__Iterator87::.ctor()
extern "C"  void U3CTweenDelayU3Ec__Iterator87__ctor_m2445408898 (U3CTweenDelayU3Ec__Iterator87_t1287629881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__Iterator87::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__Iterator87_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m190229658 (U3CTweenDelayU3Ec__Iterator87_t1287629881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__Iterator87::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__Iterator87_System_Collections_IEnumerator_get_Current_m1419322414 (U3CTweenDelayU3Ec__Iterator87_t1287629881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenDelay>c__Iterator87::MoveNext()
extern "C"  bool U3CTweenDelayU3Ec__Iterator87_MoveNext_m3179103258 (U3CTweenDelayU3Ec__Iterator87_t1287629881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__Iterator87::Dispose()
extern "C"  void U3CTweenDelayU3Ec__Iterator87_Dispose_m589123135 (U3CTweenDelayU3Ec__Iterator87_t1287629881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__Iterator87::Reset()
extern "C"  void U3CTweenDelayU3Ec__Iterator87_Reset_m91841839 (U3CTweenDelayU3Ec__Iterator87_t1287629881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
