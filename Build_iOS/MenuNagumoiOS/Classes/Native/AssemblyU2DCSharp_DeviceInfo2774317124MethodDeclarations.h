﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceInfo
struct DeviceInfo_t2774317124;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void DeviceInfo::.ctor()
extern "C"  void DeviceInfo__ctor_m3938446551 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceInfo::Start()
extern "C"  void DeviceInfo_Start_m2885584343 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceInfo::prepare()
extern "C"  void DeviceInfo_prepare_m1907928156 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceInfo::GetUserAgent()
extern "C"  String_t* DeviceInfo_GetUserAgent_m2582593588 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceInfo::getPin()
extern "C"  String_t* DeviceInfo_getPin_m3339944879 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceInfo::setPin(System.String)
extern "C"  void DeviceInfo_setPin_m1434581890 (DeviceInfo_t2774317124 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceInfo::hasNotificationToken()
extern "C"  bool DeviceInfo_hasNotificationToken_m3369112181 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceInfo::getNotificationToken()
extern "C"  String_t* DeviceInfo_getNotificationToken_m834450408 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceInfo::setNotificationToken(System.String)
extern "C"  void DeviceInfo_setNotificationToken_m1869371817 (DeviceInfo_t2774317124 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DeviceInfo::GetCurrentInfoVersionControlId()
extern "C"  int32_t DeviceInfo_GetCurrentInfoVersionControlId_m1347122028 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceInfo::SetCurrentInfoVersionControlId(System.Int32)
extern "C"  void DeviceInfo_SetCurrentInfoVersionControlId_m3125983011 (DeviceInfo_t2774317124 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceInfo::SaveCurrentInfoVersionControlId()
extern "C"  void DeviceInfo_SaveCurrentInfoVersionControlId_m1639626751 (DeviceInfo_t2774317124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
