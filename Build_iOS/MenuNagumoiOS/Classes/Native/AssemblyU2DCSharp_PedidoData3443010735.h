﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CarnesData[]
struct CarnesDataU5BU5D_t2965870023;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoData
struct  PedidoData_t3443010735  : public Il2CppObject
{
public:
	// System.String PedidoData::nome
	String_t* ___nome_0;
	// System.String PedidoData::timeStamp
	String_t* ___timeStamp_1;
	// System.String PedidoData::peso
	String_t* ___peso_2;
	// System.String PedidoData::observacoes
	String_t* ___observacoes_3;
	// System.String PedidoData::nome_loja
	String_t* ___nome_loja_4;
	// System.Int32 PedidoData::idCarne
	int32_t ___idCarne_5;
	// System.Int32 PedidoData::idCorte
	int32_t ___idCorte_6;
	// System.Int32 PedidoData::quantidade
	int32_t ___quantidade_7;
	// System.Int32 PedidoData::status_pedido
	int32_t ___status_pedido_8;
	// System.Int32 PedidoData::avaliou
	int32_t ___avaliou_9;
	// System.Int32 PedidoData::id_pedido
	int32_t ___id_pedido_10;
	// System.Int32 PedidoData::idPedido
	int32_t ___idPedido_11;
	// CarnesData[] PedidoData::carnes
	CarnesDataU5BU5D_t2965870023* ___carnes_12;
	// System.String PedidoData::info
	String_t* ___info_13;
	// UnityEngine.Sprite PedidoData::sprite
	Sprite_t3199167241 * ___sprite_14;

public:
	inline static int32_t get_offset_of_nome_0() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___nome_0)); }
	inline String_t* get_nome_0() const { return ___nome_0; }
	inline String_t** get_address_of_nome_0() { return &___nome_0; }
	inline void set_nome_0(String_t* value)
	{
		___nome_0 = value;
		Il2CppCodeGenWriteBarrier(&___nome_0, value);
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___timeStamp_1)); }
	inline String_t* get_timeStamp_1() const { return ___timeStamp_1; }
	inline String_t** get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(String_t* value)
	{
		___timeStamp_1 = value;
		Il2CppCodeGenWriteBarrier(&___timeStamp_1, value);
	}

	inline static int32_t get_offset_of_peso_2() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___peso_2)); }
	inline String_t* get_peso_2() const { return ___peso_2; }
	inline String_t** get_address_of_peso_2() { return &___peso_2; }
	inline void set_peso_2(String_t* value)
	{
		___peso_2 = value;
		Il2CppCodeGenWriteBarrier(&___peso_2, value);
	}

	inline static int32_t get_offset_of_observacoes_3() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___observacoes_3)); }
	inline String_t* get_observacoes_3() const { return ___observacoes_3; }
	inline String_t** get_address_of_observacoes_3() { return &___observacoes_3; }
	inline void set_observacoes_3(String_t* value)
	{
		___observacoes_3 = value;
		Il2CppCodeGenWriteBarrier(&___observacoes_3, value);
	}

	inline static int32_t get_offset_of_nome_loja_4() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___nome_loja_4)); }
	inline String_t* get_nome_loja_4() const { return ___nome_loja_4; }
	inline String_t** get_address_of_nome_loja_4() { return &___nome_loja_4; }
	inline void set_nome_loja_4(String_t* value)
	{
		___nome_loja_4 = value;
		Il2CppCodeGenWriteBarrier(&___nome_loja_4, value);
	}

	inline static int32_t get_offset_of_idCarne_5() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___idCarne_5)); }
	inline int32_t get_idCarne_5() const { return ___idCarne_5; }
	inline int32_t* get_address_of_idCarne_5() { return &___idCarne_5; }
	inline void set_idCarne_5(int32_t value)
	{
		___idCarne_5 = value;
	}

	inline static int32_t get_offset_of_idCorte_6() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___idCorte_6)); }
	inline int32_t get_idCorte_6() const { return ___idCorte_6; }
	inline int32_t* get_address_of_idCorte_6() { return &___idCorte_6; }
	inline void set_idCorte_6(int32_t value)
	{
		___idCorte_6 = value;
	}

	inline static int32_t get_offset_of_quantidade_7() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___quantidade_7)); }
	inline int32_t get_quantidade_7() const { return ___quantidade_7; }
	inline int32_t* get_address_of_quantidade_7() { return &___quantidade_7; }
	inline void set_quantidade_7(int32_t value)
	{
		___quantidade_7 = value;
	}

	inline static int32_t get_offset_of_status_pedido_8() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___status_pedido_8)); }
	inline int32_t get_status_pedido_8() const { return ___status_pedido_8; }
	inline int32_t* get_address_of_status_pedido_8() { return &___status_pedido_8; }
	inline void set_status_pedido_8(int32_t value)
	{
		___status_pedido_8 = value;
	}

	inline static int32_t get_offset_of_avaliou_9() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___avaliou_9)); }
	inline int32_t get_avaliou_9() const { return ___avaliou_9; }
	inline int32_t* get_address_of_avaliou_9() { return &___avaliou_9; }
	inline void set_avaliou_9(int32_t value)
	{
		___avaliou_9 = value;
	}

	inline static int32_t get_offset_of_id_pedido_10() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___id_pedido_10)); }
	inline int32_t get_id_pedido_10() const { return ___id_pedido_10; }
	inline int32_t* get_address_of_id_pedido_10() { return &___id_pedido_10; }
	inline void set_id_pedido_10(int32_t value)
	{
		___id_pedido_10 = value;
	}

	inline static int32_t get_offset_of_idPedido_11() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___idPedido_11)); }
	inline int32_t get_idPedido_11() const { return ___idPedido_11; }
	inline int32_t* get_address_of_idPedido_11() { return &___idPedido_11; }
	inline void set_idPedido_11(int32_t value)
	{
		___idPedido_11 = value;
	}

	inline static int32_t get_offset_of_carnes_12() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___carnes_12)); }
	inline CarnesDataU5BU5D_t2965870023* get_carnes_12() const { return ___carnes_12; }
	inline CarnesDataU5BU5D_t2965870023** get_address_of_carnes_12() { return &___carnes_12; }
	inline void set_carnes_12(CarnesDataU5BU5D_t2965870023* value)
	{
		___carnes_12 = value;
		Il2CppCodeGenWriteBarrier(&___carnes_12, value);
	}

	inline static int32_t get_offset_of_info_13() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___info_13)); }
	inline String_t* get_info_13() const { return ___info_13; }
	inline String_t** get_address_of_info_13() { return &___info_13; }
	inline void set_info_13(String_t* value)
	{
		___info_13 = value;
		Il2CppCodeGenWriteBarrier(&___info_13, value);
	}

	inline static int32_t get_offset_of_sprite_14() { return static_cast<int32_t>(offsetof(PedidoData_t3443010735, ___sprite_14)); }
	inline Sprite_t3199167241 * get_sprite_14() const { return ___sprite_14; }
	inline Sprite_t3199167241 ** get_address_of_sprite_14() { return &___sprite_14; }
	inline void set_sprite_14(Sprite_t3199167241 * value)
	{
		___sprite_14 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
