﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.OnProgressEventHandler
struct OnProgressEventHandler_t1956336554;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.utils.request.OnProgressEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnProgressEventHandler__ctor_m2027358705 (OnProgressEventHandler_t1956336554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.OnProgressEventHandler::Invoke(System.Single)
extern "C"  void OnProgressEventHandler_Invoke_m816142848 (OnProgressEventHandler_t1956336554 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.utils.request.OnProgressEventHandler::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnProgressEventHandler_BeginInvoke_m1956997523 (OnProgressEventHandler_t1956336554 * __this, float ___progress0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.OnProgressEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnProgressEventHandler_EndInvoke_m4174847105 (OnProgressEventHandler_t1956336554 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
