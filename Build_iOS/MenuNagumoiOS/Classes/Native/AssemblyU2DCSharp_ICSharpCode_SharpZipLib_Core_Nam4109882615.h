﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pat1707447339.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.NameAndSizeFilter
struct  NameAndSizeFilter_t4109882615  : public PathFilter_t1707447339
{
public:
	// System.Int64 ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::minSize_
	int64_t ___minSize__1;
	// System.Int64 ICSharpCode.SharpZipLib.Core.NameAndSizeFilter::maxSize_
	int64_t ___maxSize__2;

public:
	inline static int32_t get_offset_of_minSize__1() { return static_cast<int32_t>(offsetof(NameAndSizeFilter_t4109882615, ___minSize__1)); }
	inline int64_t get_minSize__1() const { return ___minSize__1; }
	inline int64_t* get_address_of_minSize__1() { return &___minSize__1; }
	inline void set_minSize__1(int64_t value)
	{
		___minSize__1 = value;
	}

	inline static int32_t get_offset_of_maxSize__2() { return static_cast<int32_t>(offsetof(NameAndSizeFilter_t4109882615, ___maxSize__2)); }
	inline int64_t get_maxSize__2() const { return ___maxSize__2; }
	inline int64_t* get_address_of_maxSize__2() { return &___maxSize__2; }
	inline void set_maxSize__2(int64_t value)
	{
		___maxSize__2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
