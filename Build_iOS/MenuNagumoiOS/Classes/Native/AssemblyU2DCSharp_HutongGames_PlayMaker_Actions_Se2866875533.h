﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetBoolValue
struct  SetBoolValue_t2866875533  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetBoolValue::boolVariable
	FsmBool_t1075959796 * ___boolVariable_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetBoolValue::boolValue
	FsmBool_t1075959796 * ___boolValue_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetBoolValue::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_boolVariable_9() { return static_cast<int32_t>(offsetof(SetBoolValue_t2866875533, ___boolVariable_9)); }
	inline FsmBool_t1075959796 * get_boolVariable_9() const { return ___boolVariable_9; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_9() { return &___boolVariable_9; }
	inline void set_boolVariable_9(FsmBool_t1075959796 * value)
	{
		___boolVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_9, value);
	}

	inline static int32_t get_offset_of_boolValue_10() { return static_cast<int32_t>(offsetof(SetBoolValue_t2866875533, ___boolValue_10)); }
	inline FsmBool_t1075959796 * get_boolValue_10() const { return ___boolValue_10; }
	inline FsmBool_t1075959796 ** get_address_of_boolValue_10() { return &___boolValue_10; }
	inline void set_boolValue_10(FsmBool_t1075959796 * value)
	{
		___boolValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___boolValue_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetBoolValue_t2866875533, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
