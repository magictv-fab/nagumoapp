﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenFsmAction
struct iTweenFsmAction_t410382178;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::.ctor()
extern "C"  void iTweenFsmAction__ctor_m2960815636 (iTweenFsmAction_t410382178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::Reset()
extern "C"  void iTweenFsmAction_Reset_m607248577 (iTweenFsmAction_t410382178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::OnEnteriTween(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  void iTweenFsmAction_OnEnteriTween_m3024539459 (iTweenFsmAction_t410382178 * __this, FsmOwnerDefault_t251897112 * ___anOwner0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::IsLoop(System.Boolean)
extern "C"  void iTweenFsmAction_IsLoop_m2539277653 (iTweenFsmAction_t410382178 * __this, bool ___aValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::OnExitiTween(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  void iTweenFsmAction_OnExitiTween_m1981625761 (iTweenFsmAction_t410382178 * __this, FsmOwnerDefault_t251897112 * ___anOwner0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
