﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.QRCodeReader
struct QRCodeReader_t2288465583;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Result ZXing.QrCode.QRCodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * QRCodeReader_decode_m3095535311 (QRCodeReader_t2288465583 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.QRCodeReader::reset()
extern "C"  void QRCodeReader_reset_m3263232081 (QRCodeReader_t2288465583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.QrCode.QRCodeReader::extractPureBits(ZXing.Common.BitMatrix)
extern "C"  BitMatrix_t1058711404 * QRCodeReader_extractPureBits_m3278443668 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.QRCodeReader::moduleSize(System.Int32[],ZXing.Common.BitMatrix,System.Single&)
extern "C"  bool QRCodeReader_moduleSize_m2752062174 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___leftTopBlack0, BitMatrix_t1058711404 * ___image1, float* ___msize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.QRCodeReader::.ctor()
extern "C"  void QRCodeReader__ctor_m2986485124 (QRCodeReader_t2288465583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.QRCodeReader::.cctor()
extern "C"  void QRCodeReader__cctor_m1904629417 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
