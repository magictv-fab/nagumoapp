﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// ScreenShotME
struct ScreenShotME_t2769423806;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenShotME/<takeScreenShot>c__Iterator0
struct  U3CtakeScreenShotU3Ec__Iterator0_t2638072101  : public Il2CppObject
{
public:
	// System.Single ScreenShotME/<takeScreenShot>c__Iterator0::<startX>__0
	float ___U3CstartXU3E__0_0;
	// System.Single ScreenShotME/<takeScreenShot>c__Iterator0::<startY>__1
	float ___U3CstartYU3E__1_1;
	// UnityEngine.Texture2D ScreenShotME/<takeScreenShot>c__Iterator0::<tex>__2
	Texture2D_t3884108195 * ___U3CtexU3E__2_2;
	// System.Byte[] ScreenShotME/<takeScreenShot>c__Iterator0::<bytes>__3
	ByteU5BU5D_t4260760469* ___U3CbytesU3E__3_3;
	// System.String ScreenShotME/<takeScreenShot>c__Iterator0::<imgsrc>__4
	String_t* ___U3CimgsrcU3E__4_4;
	// UnityEngine.Texture2D ScreenShotME/<takeScreenShot>c__Iterator0::<scrnShot>__5
	Texture2D_t3884108195 * ___U3CscrnShotU3E__5_5;
	// UnityEngine.Sprite ScreenShotME/<takeScreenShot>c__Iterator0::<sprite>__6
	Sprite_t3199167241 * ___U3CspriteU3E__6_6;
	// System.Int32 ScreenShotME/<takeScreenShot>c__Iterator0::$PC
	int32_t ___U24PC_7;
	// System.Object ScreenShotME/<takeScreenShot>c__Iterator0::$current
	Il2CppObject * ___U24current_8;
	// ScreenShotME ScreenShotME/<takeScreenShot>c__Iterator0::<>f__this
	ScreenShotME_t2769423806 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CstartXU3E__0_0() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CstartXU3E__0_0)); }
	inline float get_U3CstartXU3E__0_0() const { return ___U3CstartXU3E__0_0; }
	inline float* get_address_of_U3CstartXU3E__0_0() { return &___U3CstartXU3E__0_0; }
	inline void set_U3CstartXU3E__0_0(float value)
	{
		___U3CstartXU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartYU3E__1_1() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CstartYU3E__1_1)); }
	inline float get_U3CstartYU3E__1_1() const { return ___U3CstartYU3E__1_1; }
	inline float* get_address_of_U3CstartYU3E__1_1() { return &___U3CstartYU3E__1_1; }
	inline void set_U3CstartYU3E__1_1(float value)
	{
		___U3CstartYU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__2_2() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CtexU3E__2_2)); }
	inline Texture2D_t3884108195 * get_U3CtexU3E__2_2() const { return ___U3CtexU3E__2_2; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtexU3E__2_2() { return &___U3CtexU3E__2_2; }
	inline void set_U3CtexU3E__2_2(Texture2D_t3884108195 * value)
	{
		___U3CtexU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__3_3() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CbytesU3E__3_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytesU3E__3_3() const { return ___U3CbytesU3E__3_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytesU3E__3_3() { return &___U3CbytesU3E__3_3; }
	inline void set_U3CbytesU3E__3_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytesU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CimgsrcU3E__4_4() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CimgsrcU3E__4_4)); }
	inline String_t* get_U3CimgsrcU3E__4_4() const { return ___U3CimgsrcU3E__4_4; }
	inline String_t** get_address_of_U3CimgsrcU3E__4_4() { return &___U3CimgsrcU3E__4_4; }
	inline void set_U3CimgsrcU3E__4_4(String_t* value)
	{
		___U3CimgsrcU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimgsrcU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CscrnShotU3E__5_5() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CscrnShotU3E__5_5)); }
	inline Texture2D_t3884108195 * get_U3CscrnShotU3E__5_5() const { return ___U3CscrnShotU3E__5_5; }
	inline Texture2D_t3884108195 ** get_address_of_U3CscrnShotU3E__5_5() { return &___U3CscrnShotU3E__5_5; }
	inline void set_U3CscrnShotU3E__5_5(Texture2D_t3884108195 * value)
	{
		___U3CscrnShotU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscrnShotU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CspriteU3E__6_6() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CspriteU3E__6_6)); }
	inline Sprite_t3199167241 * get_U3CspriteU3E__6_6() const { return ___U3CspriteU3E__6_6; }
	inline Sprite_t3199167241 ** get_address_of_U3CspriteU3E__6_6() { return &___U3CspriteU3E__6_6; }
	inline void set_U3CspriteU3E__6_6(Sprite_t3199167241 * value)
	{
		___U3CspriteU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CspriteU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CtakeScreenShotU3Ec__Iterator0_t2638072101, ___U3CU3Ef__this_9)); }
	inline ScreenShotME_t2769423806 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline ScreenShotME_t2769423806 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(ScreenShotME_t2769423806 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
