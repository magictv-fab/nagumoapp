﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2026905633.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"

// System.Void System.Array/InternalEnumerator`1<ZXing.EncodeHintType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m610418002_gshared (InternalEnumerator_1_t2026905633 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m610418002(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2026905633 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m610418002_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.EncodeHintType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1519878926_gshared (InternalEnumerator_1_t2026905633 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1519878926(__this, method) ((  void (*) (InternalEnumerator_1_t2026905633 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1519878926_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZXing.EncodeHintType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1074728762_gshared (InternalEnumerator_1_t2026905633 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1074728762(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2026905633 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1074728762_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.EncodeHintType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m257615273_gshared (InternalEnumerator_1_t2026905633 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m257615273(__this, method) ((  void (*) (InternalEnumerator_1_t2026905633 *, const MethodInfo*))InternalEnumerator_1_Dispose_m257615273_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZXing.EncodeHintType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1908679418_gshared (InternalEnumerator_1_t2026905633 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1908679418(__this, method) ((  bool (*) (InternalEnumerator_1_t2026905633 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1908679418_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZXing.EncodeHintType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1430656985_gshared (InternalEnumerator_1_t2026905633 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1430656985(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2026905633 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1430656985_gshared)(__this, method)
