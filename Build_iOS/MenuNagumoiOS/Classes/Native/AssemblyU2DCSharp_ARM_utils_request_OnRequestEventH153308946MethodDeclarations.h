﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.OnRequestEventHandler
struct OnRequestEventHandler_t153308946;
// System.Object
struct Il2CppObject;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.utils.request.OnRequestEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRequestEventHandler__ctor_m3620880009 (OnRequestEventHandler_t153308946 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.OnRequestEventHandler::Invoke(ARM.utils.request.ARMRequestVO)
extern "C"  void OnRequestEventHandler_Invoke_m2205524521 (OnRequestEventHandler_t153308946 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.utils.request.OnRequestEventHandler::BeginInvoke(ARM.utils.request.ARMRequestVO,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnRequestEventHandler_BeginInvoke_m3632328144 (OnRequestEventHandler_t153308946 * __this, ARMRequestVO_t2431191322 * ___vo0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.OnRequestEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnRequestEventHandler_EndInvoke_m2954124569 (OnRequestEventHandler_t153308946 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
