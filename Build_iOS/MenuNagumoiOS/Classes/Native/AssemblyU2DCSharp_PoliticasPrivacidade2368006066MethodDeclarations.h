﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PoliticasPrivacidade
struct PoliticasPrivacidade_t2368006066;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void PoliticasPrivacidade::.ctor()
extern "C"  void PoliticasPrivacidade__ctor_m849869609 (PoliticasPrivacidade_t2368006066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoliticasPrivacidade::Start()
extern "C"  void PoliticasPrivacidade_Start_m4091974697 (PoliticasPrivacidade_t2368006066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoliticasPrivacidade::Update()
extern "C"  void PoliticasPrivacidade_Update_m2303016196 (PoliticasPrivacidade_t2368006066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoliticasPrivacidade::FechaTudo()
extern "C"  void PoliticasPrivacidade_FechaTudo_m1424861936 (PoliticasPrivacidade_t2368006066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoliticasPrivacidade::Inversor(UnityEngine.GameObject)
extern "C"  void PoliticasPrivacidade_Inversor_m1623044523 (PoliticasPrivacidade_t2368006066 * __this, GameObject_t3674682005 * ___inverterObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
