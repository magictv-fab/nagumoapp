﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.SlotFilterPlugin
struct  SlotFilterPlugin_t3610247053  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::_lastPosition
	Vector3_t4282066566  ____lastPosition_15;
	// UnityEngine.Quaternion ARM.transform.filters.SlotFilterPlugin::_lastAngle
	Quaternion_t1553702882  ____lastAngle_16;
	// System.Boolean ARM.transform.filters.SlotFilterPlugin::applyInThisObject
	bool ___applyInThisObject_17;
	// System.Boolean ARM.transform.filters.SlotFilterPlugin::returnThisValues
	bool ___returnThisValues_18;
	// System.Boolean ARM.transform.filters.SlotFilterPlugin::_isLoaded
	bool ____isLoaded_19;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::position
	Vector3_t4282066566  ___position_20;
	// UnityEngine.Quaternion ARM.transform.filters.SlotFilterPlugin::rotation
	Quaternion_t1553702882  ___rotation_21;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::filteredNextValuePosition
	Vector3_t4282066566  ___filteredNextValuePosition_22;
	// UnityEngine.Quaternion ARM.transform.filters.SlotFilterPlugin::filteredNextValueRotation
	Quaternion_t1553702882  ___filteredNextValueRotation_23;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::DebugRotationLast
	Vector3_t4282066566  ___DebugRotationLast_24;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::DebugRotationNext
	Vector3_t4282066566  ___DebugRotationNext_25;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::DebugRotationOut
	Vector3_t4282066566  ___DebugRotationOut_26;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::DebugPositionLast
	Vector3_t4282066566  ___DebugPositionLast_27;
	// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::DebugPositionNew
	Vector3_t4282066566  ___DebugPositionNew_28;

public:
	inline static int32_t get_offset_of__lastPosition_15() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ____lastPosition_15)); }
	inline Vector3_t4282066566  get__lastPosition_15() const { return ____lastPosition_15; }
	inline Vector3_t4282066566 * get_address_of__lastPosition_15() { return &____lastPosition_15; }
	inline void set__lastPosition_15(Vector3_t4282066566  value)
	{
		____lastPosition_15 = value;
	}

	inline static int32_t get_offset_of__lastAngle_16() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ____lastAngle_16)); }
	inline Quaternion_t1553702882  get__lastAngle_16() const { return ____lastAngle_16; }
	inline Quaternion_t1553702882 * get_address_of__lastAngle_16() { return &____lastAngle_16; }
	inline void set__lastAngle_16(Quaternion_t1553702882  value)
	{
		____lastAngle_16 = value;
	}

	inline static int32_t get_offset_of_applyInThisObject_17() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___applyInThisObject_17)); }
	inline bool get_applyInThisObject_17() const { return ___applyInThisObject_17; }
	inline bool* get_address_of_applyInThisObject_17() { return &___applyInThisObject_17; }
	inline void set_applyInThisObject_17(bool value)
	{
		___applyInThisObject_17 = value;
	}

	inline static int32_t get_offset_of_returnThisValues_18() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___returnThisValues_18)); }
	inline bool get_returnThisValues_18() const { return ___returnThisValues_18; }
	inline bool* get_address_of_returnThisValues_18() { return &___returnThisValues_18; }
	inline void set_returnThisValues_18(bool value)
	{
		___returnThisValues_18 = value;
	}

	inline static int32_t get_offset_of__isLoaded_19() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ____isLoaded_19)); }
	inline bool get__isLoaded_19() const { return ____isLoaded_19; }
	inline bool* get_address_of__isLoaded_19() { return &____isLoaded_19; }
	inline void set__isLoaded_19(bool value)
	{
		____isLoaded_19 = value;
	}

	inline static int32_t get_offset_of_position_20() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___position_20)); }
	inline Vector3_t4282066566  get_position_20() const { return ___position_20; }
	inline Vector3_t4282066566 * get_address_of_position_20() { return &___position_20; }
	inline void set_position_20(Vector3_t4282066566  value)
	{
		___position_20 = value;
	}

	inline static int32_t get_offset_of_rotation_21() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___rotation_21)); }
	inline Quaternion_t1553702882  get_rotation_21() const { return ___rotation_21; }
	inline Quaternion_t1553702882 * get_address_of_rotation_21() { return &___rotation_21; }
	inline void set_rotation_21(Quaternion_t1553702882  value)
	{
		___rotation_21 = value;
	}

	inline static int32_t get_offset_of_filteredNextValuePosition_22() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___filteredNextValuePosition_22)); }
	inline Vector3_t4282066566  get_filteredNextValuePosition_22() const { return ___filteredNextValuePosition_22; }
	inline Vector3_t4282066566 * get_address_of_filteredNextValuePosition_22() { return &___filteredNextValuePosition_22; }
	inline void set_filteredNextValuePosition_22(Vector3_t4282066566  value)
	{
		___filteredNextValuePosition_22 = value;
	}

	inline static int32_t get_offset_of_filteredNextValueRotation_23() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___filteredNextValueRotation_23)); }
	inline Quaternion_t1553702882  get_filteredNextValueRotation_23() const { return ___filteredNextValueRotation_23; }
	inline Quaternion_t1553702882 * get_address_of_filteredNextValueRotation_23() { return &___filteredNextValueRotation_23; }
	inline void set_filteredNextValueRotation_23(Quaternion_t1553702882  value)
	{
		___filteredNextValueRotation_23 = value;
	}

	inline static int32_t get_offset_of_DebugRotationLast_24() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___DebugRotationLast_24)); }
	inline Vector3_t4282066566  get_DebugRotationLast_24() const { return ___DebugRotationLast_24; }
	inline Vector3_t4282066566 * get_address_of_DebugRotationLast_24() { return &___DebugRotationLast_24; }
	inline void set_DebugRotationLast_24(Vector3_t4282066566  value)
	{
		___DebugRotationLast_24 = value;
	}

	inline static int32_t get_offset_of_DebugRotationNext_25() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___DebugRotationNext_25)); }
	inline Vector3_t4282066566  get_DebugRotationNext_25() const { return ___DebugRotationNext_25; }
	inline Vector3_t4282066566 * get_address_of_DebugRotationNext_25() { return &___DebugRotationNext_25; }
	inline void set_DebugRotationNext_25(Vector3_t4282066566  value)
	{
		___DebugRotationNext_25 = value;
	}

	inline static int32_t get_offset_of_DebugRotationOut_26() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___DebugRotationOut_26)); }
	inline Vector3_t4282066566  get_DebugRotationOut_26() const { return ___DebugRotationOut_26; }
	inline Vector3_t4282066566 * get_address_of_DebugRotationOut_26() { return &___DebugRotationOut_26; }
	inline void set_DebugRotationOut_26(Vector3_t4282066566  value)
	{
		___DebugRotationOut_26 = value;
	}

	inline static int32_t get_offset_of_DebugPositionLast_27() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___DebugPositionLast_27)); }
	inline Vector3_t4282066566  get_DebugPositionLast_27() const { return ___DebugPositionLast_27; }
	inline Vector3_t4282066566 * get_address_of_DebugPositionLast_27() { return &___DebugPositionLast_27; }
	inline void set_DebugPositionLast_27(Vector3_t4282066566  value)
	{
		___DebugPositionLast_27 = value;
	}

	inline static int32_t get_offset_of_DebugPositionNew_28() { return static_cast<int32_t>(offsetof(SlotFilterPlugin_t3610247053, ___DebugPositionNew_28)); }
	inline Vector3_t4282066566  get_DebugPositionNew_28() const { return ___DebugPositionNew_28; }
	inline Vector3_t4282066566 * get_address_of_DebugPositionNew_28() { return &___DebugPositionNew_28; }
	inline void set_DebugPositionNew_28(Vector3_t4282066566  value)
	{
		___DebugPositionNew_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
