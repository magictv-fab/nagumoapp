﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseBundle/<Dest>c__Iterator4C
struct U3CDestU3Ec__Iterator4C_t1110153035;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseBundle/<Dest>c__Iterator4C::.ctor()
extern "C"  void U3CDestU3Ec__Iterator4C__ctor_m3612304512 (U3CDestU3Ec__Iterator4C_t1110153035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CloseBundle/<Dest>c__Iterator4C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDestU3Ec__Iterator4C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1192910546 (U3CDestU3Ec__Iterator4C_t1110153035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CloseBundle/<Dest>c__Iterator4C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDestU3Ec__Iterator4C_System_Collections_IEnumerator_get_Current_m2042119782 (U3CDestU3Ec__Iterator4C_t1110153035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CloseBundle/<Dest>c__Iterator4C::MoveNext()
extern "C"  bool U3CDestU3Ec__Iterator4C_MoveNext_m886073716 (U3CDestU3Ec__Iterator4C_t1110153035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseBundle/<Dest>c__Iterator4C::Dispose()
extern "C"  void U3CDestU3Ec__Iterator4C_Dispose_m989343933 (U3CDestU3Ec__Iterator4C_t1110153035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseBundle/<Dest>c__Iterator4C::Reset()
extern "C"  void U3CDestU3Ec__Iterator4C_Reset_m1258737453 (U3CDestU3Ec__Iterator4C_t1110153035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
