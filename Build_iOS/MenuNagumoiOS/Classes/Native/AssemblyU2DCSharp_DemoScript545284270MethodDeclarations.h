﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoScript
struct DemoScript_t545284270;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_String7231557.h"

// System.Void DemoScript::.ctor()
extern "C"  void DemoScript__ctor_m2678201261 (DemoScript_t545284270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::OnEnable()
extern "C"  void DemoScript_OnEnable_m757887161 (DemoScript_t545284270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::OnDisable()
extern "C"  void DemoScript_OnDisable_m2460602772 (DemoScript_t545284270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::OnSaveScreenshotPress()
extern "C"  void DemoScript_OnSaveScreenshotPress_m1876990828 (DemoScript_t545284270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::OnSaveImagePress()
extern "C"  void DemoScript_OnSaveImagePress_m2728459963 (DemoScript_t545284270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::ScreenshotTaken(UnityEngine.Texture2D)
extern "C"  void DemoScript_ScreenshotTaken_m4059241104 (DemoScript_t545284270 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::ScreenshotSaved(System.String)
extern "C"  void DemoScript_ScreenshotSaved_m233456054 (DemoScript_t545284270 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoScript::ImageSaved(System.String)
extern "C"  void DemoScript_ImageSaved_m2633880959 (DemoScript_t545284270 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
