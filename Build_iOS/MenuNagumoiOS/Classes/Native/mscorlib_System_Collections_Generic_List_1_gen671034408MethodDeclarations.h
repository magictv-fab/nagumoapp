﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::.ctor()
#define List_1__ctor_m3470071464(__this, method) ((  void (*) (List_1_t671034408 *, const MethodInfo*))List_1__ctor_m574172797_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4010527469(__this, ___collection0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::.ctor(System.Int32)
#define List_1__ctor_m2684710051(__this, ___capacity0, method) ((  void (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::.ctor(T[],System.Int32)
#define List_1__ctor_m2298996653(__this, ___data0, ___size1, method) ((  void (*) (List_1_t671034408 *, Win32_IP_ADAPTER_ADDRESSESU5BU5D_t3677701705*, int32_t, const MethodInfo*))List_1__ctor_m4134761583_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::.cctor()
#define List_1__cctor_m3006624667(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2013994460(__this, method) ((  Il2CppObject* (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2512266034(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t671034408 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3788787137(__this, method) ((  Il2CppObject * (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2409113116(__this, ___item0, method) ((  int32_t (*) (List_1_t671034408 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1896512676(__this, ___item0, method) ((  bool (*) (List_1_t671034408 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3420450420(__this, ___item0, method) ((  int32_t (*) (List_1_t671034408 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2054695399(__this, ___index0, ___item1, method) ((  void (*) (List_1_t671034408 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3833230305(__this, ___item0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4115525541(__this, method) ((  bool (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3004295224(__this, method) ((  bool (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1559419306(__this, method) ((  Il2CppObject * (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m893744915(__this, method) ((  bool (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m67197318(__this, method) ((  bool (*) (List_1_t671034408 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m723978417(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3786242046(__this, ___index0, ___value1, method) ((  void (*) (List_1_t671034408 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Add(T)
#define List_1_Add_m3164432280(__this, ___item0, method) ((  void (*) (List_1_t671034408 *, Win32_IP_ADAPTER_ADDRESSES_t3597816152 *, const MethodInfo*))List_1_Add_m268533613_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2668151208(__this, ___newCount0, method) ((  void (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m4137896255(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t671034408 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3863509158(__this, ___collection0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3124481382(__this, ___enumerable0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4274510193(__this, ___collection0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::AsReadOnly()
#define List_1_AsReadOnly_m2918880884(__this, method) ((  ReadOnlyCollection_1_t859926392 * (*) (List_1_t671034408 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Clear()
#define List_1_Clear_m566713981(__this, method) ((  void (*) (List_1_t671034408 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Contains(T)
#define List_1_Contains_m2501335535(__this, ___item0, method) ((  bool (*) (List_1_t671034408 *, Win32_IP_ADAPTER_ADDRESSES_t3597816152 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2914218653(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t671034408 *, Win32_IP_ADAPTER_ADDRESSESU5BU5D_t3677701705*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Find(System.Predicate`1<T>)
#define List_1_Find_m2848760393(__this, ___match0, method) ((  Win32_IP_ADAPTER_ADDRESSES_t3597816152 * (*) (List_1_t671034408 *, Predicate_1_t3208873035 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m4284042470(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3208873035 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3510912506(__this, ___match0, method) ((  List_1_t671034408 * (*) (List_1_t671034408 *, Predicate_1_t3208873035 *, const MethodInfo*))List_1_FindAll_m754611998_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m2933832800(__this, ___match0, method) ((  List_1_t671034408 * (*) (List_1_t671034408 *, Predicate_1_t3208873035 *, const MethodInfo*))List_1_FindAllStackBits_m2296615868_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m401995900(__this, ___match0, method) ((  List_1_t671034408 * (*) (List_1_t671034408 *, Predicate_1_t3208873035 *, const MethodInfo*))List_1_FindAllList_m1562834848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3041058499(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t671034408 *, int32_t, int32_t, Predicate_1_t3208873035 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m4187049178(__this, ___action0, method) ((  void (*) (List_1_t671034408 *, Action_1_t3993632288 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::GetEnumerator()
#define List_1_GetEnumerator_m2983889836(__this, method) ((  Enumerator_t690707178  (*) (List_1_t671034408 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m2574465904(__this, ___index0, ___count1, method) ((  List_1_t671034408 * (*) (List_1_t671034408 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m4200734924_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::IndexOf(T)
#define List_1_IndexOf_m1409423657(__this, ___item0, method) ((  int32_t (*) (List_1_t671034408 *, Win32_IP_ADAPTER_ADDRESSES_t3597816152 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m510745588(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t671034408 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2660585837(__this, ___index0, method) ((  void (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Insert(System.Int32,T)
#define List_1_Insert_m735546452(__this, ___index0, ___item1, method) ((  void (*) (List_1_t671034408 *, int32_t, Win32_IP_ADAPTER_ADDRESSES_t3597816152 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2507225545(__this, ___collection0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Remove(T)
#define List_1_Remove_m2397555114(__this, ___item0, method) ((  bool (*) (List_1_t671034408 *, Win32_IP_ADAPTER_ADDRESSES_t3597816152 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2105912364(__this, ___match0, method) ((  int32_t (*) (List_1_t671034408 *, Predicate_1_t3208873035 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2904366618(__this, ___index0, method) ((  void (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m3455944253(__this, ___index0, ___count1, method) ((  void (*) (List_1_t671034408 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Reverse()
#define List_1_Reverse_m813976082(__this, method) ((  void (*) (List_1_t671034408 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Sort()
#define List_1_Sort_m1726447312(__this, method) ((  void (*) (List_1_t671034408 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2868818196(__this, ___comparer0, method) ((  void (*) (List_1_t671034408 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2330431267(__this, ___comparison0, method) ((  void (*) (List_1_t671034408 *, Comparison_1_t2314177339 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::ToArray()
#define List_1_ToArray_m2400210386(__this, method) ((  Win32_IP_ADAPTER_ADDRESSESU5BU5D_t3677701705* (*) (List_1_t671034408 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::TrimExcess()
#define List_1_TrimExcess_m2450539113(__this, method) ((  void (*) (List_1_t671034408 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::TrueForAll(System.Predicate`1<T>)
#define List_1_TrueForAll_m810921119(__this, ___match0, method) ((  bool (*) (List_1_t671034408 *, Predicate_1_t3208873035 *, const MethodInfo*))List_1_TrueForAll_m2945946909_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::get_Capacity()
#define List_1_get_Capacity_m3316602073(__this, method) ((  int32_t (*) (List_1_t671034408 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4173307322(__this, ___value0, method) ((  void (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::get_Count()
#define List_1_get_Count_m559767922(__this, method) ((  int32_t (*) (List_1_t671034408 *, const MethodInfo*))List_1_get_Count_m2594154675_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::get_Item(System.Int32)
#define List_1_get_Item_m2116144832(__this, ___index0, method) ((  Win32_IP_ADAPTER_ADDRESSES_t3597816152 * (*) (List_1_t671034408 *, int32_t, const MethodInfo*))List_1_get_Item_m850128002_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES>::set_Item(System.Int32,T)
#define List_1_set_Item_m133163(__this, ___index0, ___value1, method) ((  void (*) (List_1_t671034408 *, int32_t, Win32_IP_ADAPTER_ADDRESSES_t3597816152 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
