﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Wait
struct  Wait_t3399487645  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Wait::time
	FsmFloat_t2134102846 * ___time_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Wait::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_10;
	// System.Boolean HutongGames.PlayMaker.Actions.Wait::realTime
	bool ___realTime_11;
	// System.Single HutongGames.PlayMaker.Actions.Wait::startTime
	float ___startTime_12;
	// System.Single HutongGames.PlayMaker.Actions.Wait::timer
	float ___timer_13;

public:
	inline static int32_t get_offset_of_time_9() { return static_cast<int32_t>(offsetof(Wait_t3399487645, ___time_9)); }
	inline FsmFloat_t2134102846 * get_time_9() const { return ___time_9; }
	inline FsmFloat_t2134102846 ** get_address_of_time_9() { return &___time_9; }
	inline void set_time_9(FsmFloat_t2134102846 * value)
	{
		___time_9 = value;
		Il2CppCodeGenWriteBarrier(&___time_9, value);
	}

	inline static int32_t get_offset_of_finishEvent_10() { return static_cast<int32_t>(offsetof(Wait_t3399487645, ___finishEvent_10)); }
	inline FsmEvent_t2133468028 * get_finishEvent_10() const { return ___finishEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_10() { return &___finishEvent_10; }
	inline void set_finishEvent_10(FsmEvent_t2133468028 * value)
	{
		___finishEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_10, value);
	}

	inline static int32_t get_offset_of_realTime_11() { return static_cast<int32_t>(offsetof(Wait_t3399487645, ___realTime_11)); }
	inline bool get_realTime_11() const { return ___realTime_11; }
	inline bool* get_address_of_realTime_11() { return &___realTime_11; }
	inline void set_realTime_11(bool value)
	{
		___realTime_11 = value;
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(Wait_t3399487645, ___startTime_12)); }
	inline float get_startTime_12() const { return ___startTime_12; }
	inline float* get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(float value)
	{
		___startTime_12 = value;
	}

	inline static int32_t get_offset_of_timer_13() { return static_cast<int32_t>(offsetof(Wait_t3399487645, ___timer_13)); }
	inline float get_timer_13() const { return ___timer_13; }
	inline float* get_address_of_timer_13() { return &___timer_13; }
	inline void set_timer_13(float value)
	{
		___timer_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
