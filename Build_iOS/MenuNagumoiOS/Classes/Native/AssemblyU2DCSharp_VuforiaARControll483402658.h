﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VuforiaARControll
struct VuforiaARControll_t483402658;
// Vuforia.ObjectTracker
struct ObjectTracker_t455954211;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaARControll
struct  VuforiaARControll_t483402658  : public Il2CppObject
{
public:
	// Vuforia.ObjectTracker VuforiaARControll::_vuforiaImageTracker
	ObjectTracker_t455954211 * ____vuforiaImageTracker_1;
	// System.Boolean VuforiaARControll::_chekStartVuforia
	bool ____chekStartVuforia_2;

public:
	inline static int32_t get_offset_of__vuforiaImageTracker_1() { return static_cast<int32_t>(offsetof(VuforiaARControll_t483402658, ____vuforiaImageTracker_1)); }
	inline ObjectTracker_t455954211 * get__vuforiaImageTracker_1() const { return ____vuforiaImageTracker_1; }
	inline ObjectTracker_t455954211 ** get_address_of__vuforiaImageTracker_1() { return &____vuforiaImageTracker_1; }
	inline void set__vuforiaImageTracker_1(ObjectTracker_t455954211 * value)
	{
		____vuforiaImageTracker_1 = value;
		Il2CppCodeGenWriteBarrier(&____vuforiaImageTracker_1, value);
	}

	inline static int32_t get_offset_of__chekStartVuforia_2() { return static_cast<int32_t>(offsetof(VuforiaARControll_t483402658, ____chekStartVuforia_2)); }
	inline bool get__chekStartVuforia_2() const { return ____chekStartVuforia_2; }
	inline bool* get_address_of__chekStartVuforia_2() { return &____chekStartVuforia_2; }
	inline void set__chekStartVuforia_2(bool value)
	{
		____chekStartVuforia_2 = value;
	}
};

struct VuforiaARControll_t483402658_StaticFields
{
public:
	// VuforiaARControll VuforiaARControll::_instance
	VuforiaARControll_t483402658 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(VuforiaARControll_t483402658_StaticFields, ____instance_0)); }
	inline VuforiaARControll_t483402658 * get__instance_0() const { return ____instance_0; }
	inline VuforiaARControll_t483402658 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(VuforiaARControll_t483402658 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
