﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.internet.InternetInfo
struct InternetInfo_t59940211;
// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Action>
struct Dictionary_2_t3531948414;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

// System.Void ARM.device.internet.InternetInfo::.ctor()
extern "C"  void InternetInfo__ctor_m2715509061 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Action> ARM.device.internet.InternetInfo::get_wifiReachabilityDictionary()
extern "C"  Dictionary_2_t3531948414 * InternetInfo_get_wifiReachabilityDictionary_m864234555 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.internet.InternetInfo::prepare()
extern "C"  void InternetInfo_prepare_m3486039370 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.internet.InternetInfo::testWifi()
extern "C"  void InternetInfo_testWifi_m3814100294 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.internet.InternetInfo::onConnectionHandler(UnityEngine.NetworkReachability)
extern "C"  void InternetInfo_onConnectionHandler_m1225382900 (InternetInfo_t59940211 * __this, int32_t ___wifiReachability0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.internet.InternetInfo::NotReachable()
extern "C"  void InternetInfo_NotReachable_m3432495769 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.internet.InternetInfo::ReachableViaCarrierDataNetwork()
extern "C"  void InternetInfo_ReachableViaCarrierDataNetwork_m1761897740 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.internet.InternetInfo::ReachableViaLocalAreaNetwork()
extern "C"  void InternetInfo_ReachableViaLocalAreaNetwork_m4235256246 (InternetInfo_t59940211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
