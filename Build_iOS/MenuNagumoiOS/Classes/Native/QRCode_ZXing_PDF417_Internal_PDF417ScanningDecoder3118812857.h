﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.EC.ErrorCorrection
struct ErrorCorrection_t2629846604;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417ScanningDecoder
struct  PDF417ScanningDecoder_t3118812857  : public Il2CppObject
{
public:

public:
};

struct PDF417ScanningDecoder_t3118812857_StaticFields
{
public:
	// ZXing.PDF417.Internal.EC.ErrorCorrection ZXing.PDF417.Internal.PDF417ScanningDecoder::errorCorrection
	ErrorCorrection_t2629846604 * ___errorCorrection_0;

public:
	inline static int32_t get_offset_of_errorCorrection_0() { return static_cast<int32_t>(offsetof(PDF417ScanningDecoder_t3118812857_StaticFields, ___errorCorrection_0)); }
	inline ErrorCorrection_t2629846604 * get_errorCorrection_0() const { return ___errorCorrection_0; }
	inline ErrorCorrection_t2629846604 ** get_address_of_errorCorrection_0() { return &___errorCorrection_0; }
	inline void set_errorCorrection_0(ErrorCorrection_t2629846604 * value)
	{
		___errorCorrection_0 = value;
		Il2CppCodeGenWriteBarrier(&___errorCorrection_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
