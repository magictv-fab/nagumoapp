﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_AbstractEx1111398899.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01decoder
struct  AI01decoder_t1016996677  : public AbstractExpandedDecoder_t1111398899
{
public:

public:
};

struct AI01decoder_t1016996677_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::GTIN_SIZE
	int32_t ___GTIN_SIZE_2;

public:
	inline static int32_t get_offset_of_GTIN_SIZE_2() { return static_cast<int32_t>(offsetof(AI01decoder_t1016996677_StaticFields, ___GTIN_SIZE_2)); }
	inline int32_t get_GTIN_SIZE_2() const { return ___GTIN_SIZE_2; }
	inline int32_t* get_address_of_GTIN_SIZE_2() { return &___GTIN_SIZE_2; }
	inline void set_GTIN_SIZE_2(int32_t value)
	{
		___GTIN_SIZE_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
