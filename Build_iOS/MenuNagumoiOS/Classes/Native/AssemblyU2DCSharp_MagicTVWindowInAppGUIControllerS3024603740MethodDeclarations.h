﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F
struct U3CSaveScreenShotU3Ec__Iterator2F_t3024603740;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F::.ctor()
extern "C"  void U3CSaveScreenShotU3Ec__Iterator2F__ctor_m4212506767 (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenShotU3Ec__Iterator2F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3116487843 (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenShotU3Ec__Iterator2F_System_Collections_IEnumerator_get_Current_m16743991 (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F::MoveNext()
extern "C"  bool U3CSaveScreenShotU3Ec__Iterator2F_MoveNext_m4048283909 (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F::Dispose()
extern "C"  void U3CSaveScreenShotU3Ec__Iterator2F_Dispose_m2258093324 (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript/<SaveScreenShot>c__Iterator2F::Reset()
extern "C"  void U3CSaveScreenShotU3Ec__Iterator2F_Reset_m1858939708 (U3CSaveScreenShotU3Ec__Iterator2F_t3024603740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
