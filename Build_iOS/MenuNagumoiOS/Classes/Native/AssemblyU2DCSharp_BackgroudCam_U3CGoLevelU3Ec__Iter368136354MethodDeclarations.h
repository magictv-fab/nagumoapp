﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackgroudCam/<GoLevel>c__Iterator47
struct U3CGoLevelU3Ec__Iterator47_t368136354;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BackgroudCam/<GoLevel>c__Iterator47::.ctor()
extern "C"  void U3CGoLevelU3Ec__Iterator47__ctor_m448629001 (U3CGoLevelU3Ec__Iterator47_t368136354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BackgroudCam/<GoLevel>c__Iterator47::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator47_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3331051625 (U3CGoLevelU3Ec__Iterator47_t368136354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BackgroudCam/<GoLevel>c__Iterator47::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator47_System_Collections_IEnumerator_get_Current_m4191536637 (U3CGoLevelU3Ec__Iterator47_t368136354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BackgroudCam/<GoLevel>c__Iterator47::MoveNext()
extern "C"  bool U3CGoLevelU3Ec__Iterator47_MoveNext_m3766797899 (U3CGoLevelU3Ec__Iterator47_t368136354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam/<GoLevel>c__Iterator47::Dispose()
extern "C"  void U3CGoLevelU3Ec__Iterator47_Dispose_m1534023430 (U3CGoLevelU3Ec__Iterator47_t368136354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam/<GoLevel>c__Iterator47::Reset()
extern "C"  void U3CGoLevelU3Ec__Iterator47_Reset_m2390029238 (U3CGoLevelU3Ec__Iterator47_t368136354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
