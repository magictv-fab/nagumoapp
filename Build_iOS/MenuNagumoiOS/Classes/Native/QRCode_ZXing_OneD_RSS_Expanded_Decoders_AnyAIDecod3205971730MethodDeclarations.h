﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder
struct AnyAIDecoder_t3205971730;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AnyAIDecoder__ctor_m3868538622 (AnyAIDecoder_t3205971730 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::parseInformation()
extern "C"  String_t* AnyAIDecoder_parseInformation_m4281330667 (AnyAIDecoder_t3205971730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::.cctor()
extern "C"  void AnyAIDecoder__cctor_m1040093118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
