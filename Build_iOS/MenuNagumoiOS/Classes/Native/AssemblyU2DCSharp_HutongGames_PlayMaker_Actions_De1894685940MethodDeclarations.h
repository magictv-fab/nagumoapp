﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroySelf
struct DestroySelf_t1894685940;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DestroySelf::.ctor()
extern "C"  void DestroySelf__ctor_m417833154 (DestroySelf_t1894685940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroySelf::Reset()
extern "C"  void DestroySelf_Reset_m2359233391 (DestroySelf_t1894685940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroySelf::OnEnter()
extern "C"  void DestroySelf_OnEnter_m1497431065 (DestroySelf_t1894685940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
