﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonAnimationSoundScript
struct  ButtonAnimationSoundScript_t4202863368  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AnimationClip ButtonAnimationSoundScript::animationClip
	AnimationClip_t2007702890 * ___animationClip_2;
	// UnityEngine.AudioClip ButtonAnimationSoundScript::audioClip
	AudioClip_t794140988 * ___audioClip_3;

public:
	inline static int32_t get_offset_of_animationClip_2() { return static_cast<int32_t>(offsetof(ButtonAnimationSoundScript_t4202863368, ___animationClip_2)); }
	inline AnimationClip_t2007702890 * get_animationClip_2() const { return ___animationClip_2; }
	inline AnimationClip_t2007702890 ** get_address_of_animationClip_2() { return &___animationClip_2; }
	inline void set_animationClip_2(AnimationClip_t2007702890 * value)
	{
		___animationClip_2 = value;
		Il2CppCodeGenWriteBarrier(&___animationClip_2, value);
	}

	inline static int32_t get_offset_of_audioClip_3() { return static_cast<int32_t>(offsetof(ButtonAnimationSoundScript_t4202863368, ___audioClip_3)); }
	inline AudioClip_t794140988 * get_audioClip_3() const { return ___audioClip_3; }
	inline AudioClip_t794140988 ** get_address_of_audioClip_3() { return &___audioClip_3; }
	inline void set_audioClip_3(AudioClip_t794140988 * value)
	{
		___audioClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
