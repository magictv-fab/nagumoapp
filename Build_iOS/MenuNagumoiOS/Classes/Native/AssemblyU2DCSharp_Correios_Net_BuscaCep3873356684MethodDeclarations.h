﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Correios.Net.BuscaCep
struct BuscaCep_t3873356684;
// Correios.Net.Address
struct Address_t2296282618;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Correios.Net.BuscaCep::.ctor()
extern "C"  void BuscaCep__ctor_m756994416 (BuscaCep_t3873356684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Correios.Net.Address Correios.Net.BuscaCep::GetAddress(System.String)
extern "C"  Address_t2296282618 * BuscaCep_GetAddress_m1940983300 (Il2CppObject * __this /* static, unused */, String_t* ___cep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
