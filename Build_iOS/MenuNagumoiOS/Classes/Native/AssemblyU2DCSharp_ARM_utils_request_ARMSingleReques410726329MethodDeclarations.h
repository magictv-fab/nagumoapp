﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.ARMSingleRequest
struct ARMSingleRequest_t410726329;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;
// ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler
struct OnStartErrorEventHandler_t726677771;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMSingleReques726677771.h"

// System.Void ARM.utils.request.ARMSingleRequest::.ctor()
extern "C"  void ARMSingleRequest__ctor_m2160170594 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::.cctor()
extern "C"  void ARMSingleRequest__cctor_m2058682763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.request.ARMSingleRequest ARM.utils.request.ARMSingleRequest::GetNewInstance(ARM.utils.request.ARMRequestVO)
extern "C"  ARMSingleRequest_t410726329 * ARMSingleRequest_GetNewInstance_m3701429387 (Il2CppObject * __this /* static, unused */, ARMRequestVO_t2431191322 * ___requestVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.request.ARMSingleRequest ARM.utils.request.ARMSingleRequest::generateGameObjectWithComponent()
extern "C"  ARMSingleRequest_t410726329 * ARMSingleRequest_generateGameObjectWithComponent_m3269799725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::Init()
extern "C"  void ARMSingleRequest_Init_m2930713522 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::Load()
extern "C"  void ARMSingleRequest_Load_m3017270792 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ARM.utils.request.ARMSingleRequest::doLoad()
extern "C"  Il2CppObject * ARMSingleRequest_doLoad_m3228628059 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::Update()
extern "C"  void ARMSingleRequest_Update_m4267641067 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::Dispose()
extern "C"  void ARMSingleRequest_Dispose_m1353019935 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::SetWeight(System.Single)
extern "C"  void ARMSingleRequest_SetWeight_m2342969585 (ARMSingleRequest_t410726329 * __this, float ___weight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::SetPostData(UnityEngine.WWWForm)
extern "C"  void ARMSingleRequest_SetPostData_m274496994 (ARMSingleRequest_t410726329 * __this, WWWForm_t461342257 * ___postData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::setData(ARM.utils.request.ARMRequestVO)
extern "C"  void ARMSingleRequest_setData_m4286798930 (ARMSingleRequest_t410726329 * __this, ARMRequestVO_t2431191322 * ___requestVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW ARM.utils.request.ARMSingleRequest::getWWWRequest()
extern "C"  WWW_t3134621005 * ARMSingleRequest_getWWWRequest_m1252631067 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.request.ARMSingleRequest::IsComplete()
extern "C"  bool ARMSingleRequest_IsComplete_m3138601049 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.request.ARMRequestVO ARM.utils.request.ARMSingleRequest::GetVO()
extern "C"  ARMRequestVO_t2431191322 * ARMSingleRequest_GetVO_m2222064108 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::errorCheck()
extern "C"  void ARMSingleRequest_errorCheck_m3951862178 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::onCompleteCheck()
extern "C"  void ARMSingleRequest_onCompleteCheck_m3518840976 (ARMSingleRequest_t410726329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::RaiseStartErrorEventHandler(System.String)
extern "C"  void ARMSingleRequest_RaiseStartErrorEventHandler_m268074048 (ARMSingleRequest_t410726329 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::AddStartErrorEventHandler(ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler)
extern "C"  void ARMSingleRequest_AddStartErrorEventHandler_m3109258352 (ARMSingleRequest_t410726329 * __this, OnStartErrorEventHandler_t726677771 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMSingleRequest::RemoveStartErrorEventHandler(ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler)
extern "C"  void ARMSingleRequest_RemoveStartErrorEventHandler_m2405498059 (ARMSingleRequest_t410726329 * __this, OnStartErrorEventHandler_t726677771 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
