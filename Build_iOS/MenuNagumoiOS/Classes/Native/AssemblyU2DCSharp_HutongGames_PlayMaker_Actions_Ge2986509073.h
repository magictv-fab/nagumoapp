﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetKey
struct  GetKey_t2986509073  : public FsmStateAction_t2366529033
{
public:
	// UnityEngine.KeyCode HutongGames.PlayMaker.Actions.GetKey::key
	int32_t ___key_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetKey::storeResult
	FsmBool_t1075959796 * ___storeResult_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetKey::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(GetKey_t2986509073, ___key_9)); }
	inline int32_t get_key_9() const { return ___key_9; }
	inline int32_t* get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(int32_t value)
	{
		___key_9 = value;
	}

	inline static int32_t get_offset_of_storeResult_10() { return static_cast<int32_t>(offsetof(GetKey_t2986509073, ___storeResult_10)); }
	inline FsmBool_t1075959796 * get_storeResult_10() const { return ___storeResult_10; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_10() { return &___storeResult_10; }
	inline void set_storeResult_10(FsmBool_t1075959796 * value)
	{
		___storeResult_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetKey_t2986509073, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
