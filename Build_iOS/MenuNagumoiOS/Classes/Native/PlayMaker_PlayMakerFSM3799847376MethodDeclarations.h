﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t873065632;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// FsmTemplate
struct FsmTemplate_t1237263802;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.GUIText
struct GUIText_t3371372606;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// UnityEngine.BitStream
struct BitStream_t239125475;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t2644459362;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t818210886;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "PlayMaker_FsmTemplate1237263802.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_BitStream239125475.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2733244747.h"

// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern "C"  List_1_t873065632 * PlayMakerFSM_get_FsmList_m1444651517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM PlayMakerFSM::FindFsmOnGameObject(UnityEngine.GameObject,System.String)
extern "C"  PlayMakerFSM_t3799847376 * PlayMakerFSM_FindFsmOnGameObject_m3929556549 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FsmTemplate PlayMakerFSM::get_FsmTemplate()
extern "C"  FsmTemplate_t1237263802 * PlayMakerFSM_get_FsmTemplate_m821320227 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C"  GUITexture_t4020448292 * PlayMakerFSM_get_GuiTexture_m219309322 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_GuiTexture(UnityEngine.GUITexture)
extern "C"  void PlayMakerFSM_set_GuiTexture_m229602685 (PlayMakerFSM_t3799847376 * __this, GUITexture_t4020448292 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C"  GUIText_t3371372606 * PlayMakerFSM_get_GuiText_m1922746504 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_GuiText(UnityEngine.GUIText)
extern "C"  void PlayMakerFSM_set_GuiText_m3473618123 (PlayMakerFSM_t3799847376 * __this, GUIText_t3371372606 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerFSM::get_DrawGizmos()
extern "C"  bool PlayMakerFSM_get_DrawGizmos_m976637493 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_DrawGizmos(System.Boolean)
extern "C"  void PlayMakerFSM_set_DrawGizmos_m3647490152 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Reset()
extern "C"  void PlayMakerFSM_Reset_m865086010 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Awake()
extern "C"  void PlayMakerFSM_Awake_m3456258288 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Init()
extern "C"  void PlayMakerFSM_Init_m4073236775 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SetFsmTemplate(FsmTemplate)
extern "C"  void PlayMakerFSM_SetFsmTemplate_m2282548073 (PlayMakerFSM_t3799847376 * __this, FsmTemplate_t1237263802 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Start()
extern "C"  void PlayMakerFSM_Start_m2165790861 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnEnable()
extern "C"  void PlayMakerFSM_OnEnable_m3820273881 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Update()
extern "C"  void PlayMakerFSM_Update_m2720859424 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::LateUpdate()
extern "C"  void PlayMakerFSM_LateUpdate_m3191290598 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDisable()
extern "C"  void PlayMakerFSM_OnDisable_m2905310580 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDestroy()
extern "C"  void PlayMakerFSM_OnDestroy_m787875334 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnApplicationQuit()
extern "C"  void PlayMakerFSM_OnApplicationQuit_m3370467723 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDrawGizmos()
extern "C"  void PlayMakerFSM_OnDrawGizmos_m1555179763 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::ChangeState(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_ChangeState_m3790102374 (PlayMakerFSM_t3799847376 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::ChangeState(System.String)
extern "C"  void PlayMakerFSM_ChangeState_m1344427734 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SendEvent(System.String)
extern "C"  void PlayMakerFSM_SendEvent_m1191977733 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SendRemoteFsmEvent(System.String)
extern "C"  void PlayMakerFSM_SendRemoteFsmEvent_m1166116995 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::BroadcastEvent(System.String)
extern "C"  void PlayMakerFSM_BroadcastEvent_m3077906514 (Il2CppObject * __this /* static, unused */, String_t* ___fsmEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_BroadcastEvent_m2315926890 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnBecameVisible()
extern "C"  void PlayMakerFSM_OnBecameVisible_m3186968325 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnBecameInvisible()
extern "C"  void PlayMakerFSM_OnBecameInvisible_m2256533248 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnLevelWasLoaded()
extern "C"  void PlayMakerFSM_OnLevelWasLoaded_m3548116416 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void PlayMakerFSM_OnControllerColliderHit_m1277480791 (PlayMakerFSM_t3799847376 * __this, ControllerColliderHit_t2416790841 * ___hitCollider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern "C"  void PlayMakerFSM_OnPlayerConnected_m3502723622 (PlayMakerFSM_t3799847376 * __this, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnServerInitialized()
extern "C"  void PlayMakerFSM_OnServerInitialized_m1710961181 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnConnectedToServer()
extern "C"  void PlayMakerFSM_OnConnectedToServer_m1863472339 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern "C"  void PlayMakerFSM_OnPlayerDisconnected_m3176107848 (PlayMakerFSM_t3799847376 * __this, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern "C"  void PlayMakerFSM_OnDisconnectedFromServer_m2633773257 (PlayMakerFSM_t3799847376 * __this, int32_t ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern "C"  void PlayMakerFSM_OnFailedToConnect_m2978582699 (PlayMakerFSM_t3799847376 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern "C"  void PlayMakerFSM_OnNetworkInstantiate_m123436980 (PlayMakerFSM_t3799847376 * __this, NetworkMessageInfo_t3807997963  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern "C"  void PlayMakerFSM_OnSerializeNetworkView_m3151096693 (PlayMakerFSM_t3799847376 * __this, BitStream_t239125475 * ___stream0, NetworkMessageInfo_t3807997963  ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::NetworkSyncVariables(UnityEngine.BitStream,HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerFSM_NetworkSyncVariables_m4130023628 (Il2CppObject * __this /* static, unused */, BitStream_t239125475 * ___stream0, FsmVariables_t963491929 * ___variables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern "C"  void PlayMakerFSM_OnMasterServerEvent_m4092191539 (PlayMakerFSM_t3799847376 * __this, int32_t ___masterServerEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C"  Fsm_t1527112426 * PlayMakerFSM_get_Fsm_m886945091 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_FsmName()
extern "C"  String_t* PlayMakerFSM_get_FsmName_m1703792682 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_FsmName(System.String)
extern "C"  void PlayMakerFSM_set_FsmName_m2603037929 (PlayMakerFSM_t3799847376 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_FsmDescription()
extern "C"  String_t* PlayMakerFSM_get_FsmDescription_m3630583007 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_FsmDescription(System.String)
extern "C"  void PlayMakerFSM_set_FsmDescription_m2232657298 (PlayMakerFSM_t3799847376 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerFSM::get_Active()
extern "C"  bool PlayMakerFSM_get_Active_m964418526 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_ActiveStateName()
extern "C"  String_t* PlayMakerFSM_get_ActiveStateName_m1548898773 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState[] PlayMakerFSM::get_FsmStates()
extern "C"  FsmStateU5BU5D_t2644459362* PlayMakerFSM_get_FsmStates_m1358962914 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent[] PlayMakerFSM::get_FsmEvents()
extern "C"  FsmEventU5BU5D_t2862142229* PlayMakerFSM_get_FsmEvents_m1956320322 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] PlayMakerFSM::get_FsmGlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t818210886* PlayMakerFSM_get_FsmGlobalTransitions_m1561155331 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables PlayMakerFSM::get_FsmVariables()
extern "C"  FsmVariables_t963491929 * PlayMakerFSM_get_FsmVariables_m1986570741 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerFSM::get_UsesTemplate()
extern "C"  bool PlayMakerFSM_get_UsesTemplate_m2822797310 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::.ctor()
extern "C"  void PlayMakerFSM__ctor_m3218653069 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::.cctor()
extern "C"  void PlayMakerFSM__cctor_m511901120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
