﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1289697713MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor()
#define Dictionary_2__ctor_m4065117810(__this, method) ((  void (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2__ctor_m3570390478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1881941856(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2876064069_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m4126188346(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1009618573 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m515196447_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1417147690(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1009618573 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3929179663_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4219516485(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768771968_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3311394543(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2427604756_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2046944285(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3128483586_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m866891288(__this, method) ((  bool (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070376659_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1174709729(__this, method) ((  bool (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m446763974_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1228924799(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1009618573 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m297087802_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3420503652(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1534981663_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m528028013(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1059107026_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1006937065(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1009618573 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m208714340_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2936307874(__this, ___key0, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1984335453_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3726949387(__this, method) ((  bool (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m291352432_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1886741815(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1605888796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m708149391(__this, method) ((  bool (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2424470452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2415260728(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1009618573 *, KeyValuePair_2_t908399279 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3964512371_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1521321738(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1009618573 *, KeyValuePair_2_t908399279 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1373357103_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2353219100(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1009618573 *, KeyValuePair_2U5BU5D_t711965110*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1406828951_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1428547759(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1009618573 *, KeyValuePair_2_t908399279 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1419455252_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4279711227(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3563291446_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3664434614(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2211260977_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1692385459(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4037686574_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4147638350(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1927728713_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Count()
#define Dictionary_2_get_Count_m3346175057(__this, method) ((  int32_t (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_get_Count_m3281021750_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Item(TKey)
#define Dictionary_2_get_Item_m2913850746(__this, ___key0, method) ((  Func_1_t3890737231 * (*) (Dictionary_2_t1009618573 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3507498997_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1918061929(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1009618573 *, int32_t, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_set_Item_m2088422158_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m2539049697(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1009618573 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m535772486_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m4001134326(__this, ___size0, method) ((  void (*) (Dictionary_2_t1009618573 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2551372593_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m751889522(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3271150957_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m2759719358(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t908399279  (*) (Il2CppObject * /* static, unused */, int32_t, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_make_pair_m2020831673_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m486507640(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_pick_key_m3996357149_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m48733240(__this /* static, unused */, ___key0, ___value1, method) ((  Func_1_t3890737231 * (*) (Il2CppObject * /* static, unused */, int32_t, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_pick_value_m3201555485_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m1751952157(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1009618573 *, KeyValuePair_2U5BU5D_t711965110*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1382217154_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Resize()
#define Dictionary_2_Resize_m1786286575(__this, method) ((  void (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_Resize_m3365719082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Add(TKey,TValue)
#define Dictionary_2_Add_m3060659316(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1009618573 *, int32_t, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_Add_m1045431975_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Clear()
#define Dictionary_2_Clear_m3835068308(__this, method) ((  void (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_Clear_m976523769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3069411450(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1009618573 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2727337247_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m205916026(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1009618573 *, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_ContainsValue_m3815344543_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1060008583(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1009618573 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2593148524_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m845583933(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1009618573 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3441332088_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Remove(TKey)
#define Dictionary_2_Remove_m3272135734(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1009618573 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1557423217_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m60604115(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1009618573 *, int32_t, Func_1_t3890737231 **, const MethodInfo*))Dictionary_2_TryGetValue_m3141052024_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Keys()
#define Dictionary_2_get_Keys_m2578528012(__this, method) ((  KeyCollection_t2636378024 * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_get_Keys_m969535879_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Values()
#define Dictionary_2_get_Values_m1649700748(__this, method) ((  ValueCollection_t4005191582 * (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_get_Values_m3522920135_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m4231333843(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1009618573 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3446216056_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2656014675(__this, ___value0, method) ((  Func_1_t3890737231 * (*) (Dictionary_2_t1009618573 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1513869624_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3136115225(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1009618573 *, KeyValuePair_2_t908399279 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m458852948_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m4128401902(__this, method) ((  Enumerator_t2326941965  (*) (Dictionary_2_t1009618573 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1603070483_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::<CopyTo>m__2(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__2_m669005307(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Func_1_t3890737231 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m979069152_gshared)(__this /* static, unused */, ___key0, ___value1, method)
