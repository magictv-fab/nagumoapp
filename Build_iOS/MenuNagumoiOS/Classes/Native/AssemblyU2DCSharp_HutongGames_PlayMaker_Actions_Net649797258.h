﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount
struct  NetworkGetConnectionsCount_t649797258  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::connectionsCount
	FsmInt_t1596138449 * ___connectionsCount_9;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::everyFrame
	bool ___everyFrame_10;

public:
	inline static int32_t get_offset_of_connectionsCount_9() { return static_cast<int32_t>(offsetof(NetworkGetConnectionsCount_t649797258, ___connectionsCount_9)); }
	inline FsmInt_t1596138449 * get_connectionsCount_9() const { return ___connectionsCount_9; }
	inline FsmInt_t1596138449 ** get_address_of_connectionsCount_9() { return &___connectionsCount_9; }
	inline void set_connectionsCount_9(FsmInt_t1596138449 * value)
	{
		___connectionsCount_9 = value;
		Il2CppCodeGenWriteBarrier(&___connectionsCount_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(NetworkGetConnectionsCount_t649797258, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
