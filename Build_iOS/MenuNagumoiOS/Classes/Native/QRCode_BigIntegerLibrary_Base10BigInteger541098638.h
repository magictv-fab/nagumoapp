﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BigIntegerLibrary.Base10BigInteger
struct Base10BigInteger_t541098638;
// BigIntegerLibrary.Base10BigInteger/DigitContainer
struct DigitContainer_t3576869316;

#include "mscorlib_System_Object4170816371.h"
#include "QRCode_BigIntegerLibrary_Sign1848424669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.Base10BigInteger
struct  Base10BigInteger_t541098638  : public Il2CppObject
{
public:
	// BigIntegerLibrary.Base10BigInteger/DigitContainer BigIntegerLibrary.Base10BigInteger::digits
	DigitContainer_t3576869316 * ___digits_2;
	// System.Int32 BigIntegerLibrary.Base10BigInteger::size
	int32_t ___size_3;
	// BigIntegerLibrary.Sign BigIntegerLibrary.Base10BigInteger::sign
	int32_t ___sign_4;

public:
	inline static int32_t get_offset_of_digits_2() { return static_cast<int32_t>(offsetof(Base10BigInteger_t541098638, ___digits_2)); }
	inline DigitContainer_t3576869316 * get_digits_2() const { return ___digits_2; }
	inline DigitContainer_t3576869316 ** get_address_of_digits_2() { return &___digits_2; }
	inline void set_digits_2(DigitContainer_t3576869316 * value)
	{
		___digits_2 = value;
		Il2CppCodeGenWriteBarrier(&___digits_2, value);
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(Base10BigInteger_t541098638, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_sign_4() { return static_cast<int32_t>(offsetof(Base10BigInteger_t541098638, ___sign_4)); }
	inline int32_t get_sign_4() const { return ___sign_4; }
	inline int32_t* get_address_of_sign_4() { return &___sign_4; }
	inline void set_sign_4(int32_t value)
	{
		___sign_4 = value;
	}
};

struct Base10BigInteger_t541098638_StaticFields
{
public:
	// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Zero
	Base10BigInteger_t541098638 * ___Zero_0;
	// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::One
	Base10BigInteger_t541098638 * ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Base10BigInteger_t541098638_StaticFields, ___Zero_0)); }
	inline Base10BigInteger_t541098638 * get_Zero_0() const { return ___Zero_0; }
	inline Base10BigInteger_t541098638 ** get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Base10BigInteger_t541098638 * value)
	{
		___Zero_0 = value;
		Il2CppCodeGenWriteBarrier(&___Zero_0, value);
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Base10BigInteger_t541098638_StaticFields, ___One_1)); }
	inline Base10BigInteger_t541098638 * get_One_1() const { return ___One_1; }
	inline Base10BigInteger_t541098638 ** get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Base10BigInteger_t541098638 * value)
	{
		___One_1 = value;
		Il2CppCodeGenWriteBarrier(&___One_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
