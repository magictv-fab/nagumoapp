﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoSwapScroll
struct AutoSwapScroll_t2597364079;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoSwapScroll::.ctor()
extern "C"  void AutoSwapScroll__ctor_m289560588 (AutoSwapScroll_t2597364079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoSwapScroll::Awake()
extern "C"  void AutoSwapScroll_Awake_m527165807 (AutoSwapScroll_t2597364079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoSwapScroll::OnEnable()
extern "C"  void AutoSwapScroll_OnEnable_m4076725242 (AutoSwapScroll_t2597364079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AutoSwapScroll::Start()
extern "C"  Il2CppObject * AutoSwapScroll_Start_m464970180 (AutoSwapScroll_t2597364079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoSwapScroll::Update()
extern "C"  void AutoSwapScroll_Update_m2113305729 (AutoSwapScroll_t2597364079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
