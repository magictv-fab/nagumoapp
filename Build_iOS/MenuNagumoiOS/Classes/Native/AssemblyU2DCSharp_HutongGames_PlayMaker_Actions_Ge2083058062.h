﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmVariable
struct  GetFsmVariable_t2083058062  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmVariable::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVariable::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.GetFsmVariable::storeValue
	FsmVar_t1596150537 * ___storeValue_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmVariable::everyFrame
	bool ___everyFrame_12;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmVariable::cachedGO
	GameObject_t3674682005 * ___cachedGO_13;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmVariable::sourceFsm
	PlayMakerFSM_t3799847376 * ___sourceFsm_14;
	// HutongGames.PlayMaker.INamedVariable HutongGames.PlayMaker.Actions.GetFsmVariable::sourceVariable
	Il2CppObject * ___sourceVariable_15;
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.Actions.GetFsmVariable::targetVariable
	NamedVariable_t3211770239 * ___targetVariable_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_storeValue_11() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___storeValue_11)); }
	inline FsmVar_t1596150537 * get_storeValue_11() const { return ___storeValue_11; }
	inline FsmVar_t1596150537 ** get_address_of_storeValue_11() { return &___storeValue_11; }
	inline void set_storeValue_11(FsmVar_t1596150537 * value)
	{
		___storeValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeValue_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of_cachedGO_13() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___cachedGO_13)); }
	inline GameObject_t3674682005 * get_cachedGO_13() const { return ___cachedGO_13; }
	inline GameObject_t3674682005 ** get_address_of_cachedGO_13() { return &___cachedGO_13; }
	inline void set_cachedGO_13(GameObject_t3674682005 * value)
	{
		___cachedGO_13 = value;
		Il2CppCodeGenWriteBarrier(&___cachedGO_13, value);
	}

	inline static int32_t get_offset_of_sourceFsm_14() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___sourceFsm_14)); }
	inline PlayMakerFSM_t3799847376 * get_sourceFsm_14() const { return ___sourceFsm_14; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_sourceFsm_14() { return &___sourceFsm_14; }
	inline void set_sourceFsm_14(PlayMakerFSM_t3799847376 * value)
	{
		___sourceFsm_14 = value;
		Il2CppCodeGenWriteBarrier(&___sourceFsm_14, value);
	}

	inline static int32_t get_offset_of_sourceVariable_15() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___sourceVariable_15)); }
	inline Il2CppObject * get_sourceVariable_15() const { return ___sourceVariable_15; }
	inline Il2CppObject ** get_address_of_sourceVariable_15() { return &___sourceVariable_15; }
	inline void set_sourceVariable_15(Il2CppObject * value)
	{
		___sourceVariable_15 = value;
		Il2CppCodeGenWriteBarrier(&___sourceVariable_15, value);
	}

	inline static int32_t get_offset_of_targetVariable_16() { return static_cast<int32_t>(offsetof(GetFsmVariable_t2083058062, ___targetVariable_16)); }
	inline NamedVariable_t3211770239 * get_targetVariable_16() const { return ___targetVariable_16; }
	inline NamedVariable_t3211770239 ** get_address_of_targetVariable_16() { return &___targetVariable_16; }
	inline void set_targetVariable_16(NamedVariable_t3211770239 * value)
	{
		___targetVariable_16 = value;
		Il2CppCodeGenWriteBarrier(&___targetVariable_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
