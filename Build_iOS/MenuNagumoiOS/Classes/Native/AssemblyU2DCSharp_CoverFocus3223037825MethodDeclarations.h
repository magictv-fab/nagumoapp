﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoverFocus
struct CoverFocus_t3223037825;

#include "codegen/il2cpp-codegen.h"

// System.Void CoverFocus::.ctor()
extern "C"  void CoverFocus__ctor_m3420312122 (CoverFocus_t3223037825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoverFocus::Start()
extern "C"  void CoverFocus_Start_m2367449914 (CoverFocus_t3223037825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoverFocus::OnGUI()
extern "C"  void CoverFocus_OnGUI_m2915710772 (CoverFocus_t3223037825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
