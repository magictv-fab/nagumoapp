﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.FastZip
struct FastZip_t2142290519;
// ICSharpCode.SharpZipLib.Zip.FastZipEvents
struct FastZipEvents_t381075024;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t2173030;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory
struct IEntryFactory_t131367667;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate
struct ConfirmOverwriteDelegate_t2877285688;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.DirectoryEventArgs
struct DirectoryEventArgs_t2626056408;
// ICSharpCode.SharpZipLib.Core.ScanEventArgs
struct ScanEventArgs_t1070518092;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.IO.FileInfo
struct FileInfo_t3233670074;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_FastZ381075024.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Fast1147420323.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Fast2877285688.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Dir2626056408.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca1070518092.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"

// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::.ctor()
extern "C"  void FastZip__ctor_m3402824708 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::.ctor(ICSharpCode.SharpZipLib.Zip.FastZipEvents)
extern "C"  void FastZip__ctor_m3690183534 (FastZip_t2142290519 * __this, FastZipEvents_t381075024 * ___events0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::get_CreateEmptyDirectories()
extern "C"  bool FastZip_get_CreateEmptyDirectories_m1219880687 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_CreateEmptyDirectories(System.Boolean)
extern "C"  void FastZip_set_CreateEmptyDirectories_m3965769934 (FastZip_t2142290519 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.FastZip::get_Password()
extern "C"  String_t* FastZip_get_Password_m1475188513 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_Password(System.String)
extern "C"  void FastZip_set_Password_m1588726890 (FastZip_t2142290519 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.FastZip::get_NameTransform()
extern "C"  Il2CppObject * FastZip_get_NameTransform_m2995764823 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_NameTransform(ICSharpCode.SharpZipLib.Core.INameTransform)
extern "C"  void FastZip_set_NameTransform_m1541748150 (FastZip_t2142290519 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory ICSharpCode.SharpZipLib.Zip.FastZip::get_EntryFactory()
extern "C"  Il2CppObject * FastZip_get_EntryFactory_m3863599025 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_EntryFactory(ICSharpCode.SharpZipLib.Zip.IEntryFactory)
extern "C"  void FastZip_set_EntryFactory_m2904420442 (FastZip_t2142290519 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.FastZip::get_UseZip64()
extern "C"  int32_t FastZip_get_UseZip64_m3753192890 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_UseZip64(ICSharpCode.SharpZipLib.Zip.UseZip64)
extern "C"  void FastZip_set_UseZip64_m3059639903 (FastZip_t2142290519 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::get_RestoreDateTimeOnExtract()
extern "C"  bool FastZip_get_RestoreDateTimeOnExtract_m2514157614 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_RestoreDateTimeOnExtract(System.Boolean)
extern "C"  void FastZip_set_RestoreDateTimeOnExtract_m195907021 (FastZip_t2142290519 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::get_RestoreAttributesOnExtract()
extern "C"  bool FastZip_get_RestoreAttributesOnExtract_m935970674 (FastZip_t2142290519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::set_RestoreAttributesOnExtract(System.Boolean)
extern "C"  void FastZip_set_RestoreAttributesOnExtract_m3445523473 (FastZip_t2142290519 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::CreateZip(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void FastZip_CreateZip_m1692253598 (FastZip_t2142290519 * __this, String_t* ___zipFileName0, String_t* ___sourceDirectory1, bool ___recurse2, String_t* ___fileFilter3, String_t* ___directoryFilter4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::CreateZip(System.String,System.String,System.Boolean,System.String)
extern "C"  void FastZip_CreateZip_m4264478946 (FastZip_t2142290519 * __this, String_t* ___zipFileName0, String_t* ___sourceDirectory1, bool ___recurse2, String_t* ___fileFilter3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::CreateZip(System.IO.Stream,System.String,System.Boolean,System.String,System.String)
extern "C"  void FastZip_CreateZip_m3701819643 (FastZip_t2142290519 * __this, Stream_t1561764144 * ___outputStream0, String_t* ___sourceDirectory1, bool ___recurse2, String_t* ___fileFilter3, String_t* ___directoryFilter4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ExtractZip(System.String,System.String,System.String)
extern "C"  void FastZip_ExtractZip_m3250456346 (FastZip_t2142290519 * __this, String_t* ___zipFileName0, String_t* ___targetDirectory1, String_t* ___fileFilter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ExtractZip(System.String,System.String,ICSharpCode.SharpZipLib.Zip.FastZip/Overwrite,ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate,System.String,System.String,System.Boolean)
extern "C"  void FastZip_ExtractZip_m4147436542 (FastZip_t2142290519 * __this, String_t* ___zipFileName0, String_t* ___targetDirectory1, int32_t ___overwrite2, ConfirmOverwriteDelegate_t2877285688 * ___confirmDelegate3, String_t* ___fileFilter4, String_t* ___directoryFilter5, bool ___restoreDateTime6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ExtractZip(System.IO.Stream,System.String,ICSharpCode.SharpZipLib.Zip.FastZip/Overwrite,ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate,System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void FastZip_ExtractZip_m833255754 (FastZip_t2142290519 * __this, Stream_t1561764144 * ___inputStream0, String_t* ___targetDirectory1, int32_t ___overwrite2, ConfirmOverwriteDelegate_t2877285688 * ___confirmDelegate3, String_t* ___fileFilter4, String_t* ___directoryFilter5, bool ___restoreDateTime6, bool ___isStreamOwner7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ProcessDirectory(System.Object,ICSharpCode.SharpZipLib.Core.DirectoryEventArgs)
extern "C"  void FastZip_ProcessDirectory_m2791190266 (FastZip_t2142290519 * __this, Il2CppObject * ___sender0, DirectoryEventArgs_t2626056408 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ProcessFile(System.Object,ICSharpCode.SharpZipLib.Core.ScanEventArgs)
extern "C"  void FastZip_ProcessFile_m1319765859 (FastZip_t2142290519 * __this, Il2CppObject * ___sender0, ScanEventArgs_t1070518092 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::AddFileContents(System.String,System.IO.Stream)
extern "C"  void FastZip_AddFileContents_m2676307124 (FastZip_t2142290519 * __this, String_t* ___name0, Stream_t1561764144 * ___stream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ExtractFileEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry,System.String)
extern "C"  void FastZip_ExtractFileEntry_m3662878594 (FastZip_t2142290519 * __this, ZipEntry_t3141689087 * ___entry0, String_t* ___targetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZip::ExtractEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void FastZip_ExtractEntry_m2262340578 (FastZip_t2142290519 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.FastZip::MakeExternalAttributes(System.IO.FileInfo)
extern "C"  int32_t FastZip_MakeExternalAttributes_m534093007 (Il2CppObject * __this /* static, unused */, FileInfo_t3233670074 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::NameIsValid(System.String)
extern "C"  bool FastZip_NameIsValid_m3994104293 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
