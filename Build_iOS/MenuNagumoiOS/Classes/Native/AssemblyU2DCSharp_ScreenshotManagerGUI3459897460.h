﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScreenshotManagerGUI
struct ScreenshotManagerGUI_t3459897460;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManagerGUI
struct  ScreenshotManagerGUI_t3459897460  : public MonoBehaviour_t667441552
{
public:
	// System.Action`1<System.String> ScreenshotManagerGUI::ScreenshotFinishedSaving
	Action_1_t403047693 * ___ScreenshotFinishedSaving_3;
	// System.Action`1<System.String> ScreenshotManagerGUI::ImageFinishedSaving
	Action_1_t403047693 * ___ImageFinishedSaving_4;

public:
	inline static int32_t get_offset_of_ScreenshotFinishedSaving_3() { return static_cast<int32_t>(offsetof(ScreenshotManagerGUI_t3459897460, ___ScreenshotFinishedSaving_3)); }
	inline Action_1_t403047693 * get_ScreenshotFinishedSaving_3() const { return ___ScreenshotFinishedSaving_3; }
	inline Action_1_t403047693 ** get_address_of_ScreenshotFinishedSaving_3() { return &___ScreenshotFinishedSaving_3; }
	inline void set_ScreenshotFinishedSaving_3(Action_1_t403047693 * value)
	{
		___ScreenshotFinishedSaving_3 = value;
		Il2CppCodeGenWriteBarrier(&___ScreenshotFinishedSaving_3, value);
	}

	inline static int32_t get_offset_of_ImageFinishedSaving_4() { return static_cast<int32_t>(offsetof(ScreenshotManagerGUI_t3459897460, ___ImageFinishedSaving_4)); }
	inline Action_1_t403047693 * get_ImageFinishedSaving_4() const { return ___ImageFinishedSaving_4; }
	inline Action_1_t403047693 ** get_address_of_ImageFinishedSaving_4() { return &___ImageFinishedSaving_4; }
	inline void set_ImageFinishedSaving_4(Action_1_t403047693 * value)
	{
		___ImageFinishedSaving_4 = value;
		Il2CppCodeGenWriteBarrier(&___ImageFinishedSaving_4, value);
	}
};

struct ScreenshotManagerGUI_t3459897460_StaticFields
{
public:
	// ScreenshotManagerGUI ScreenshotManagerGUI::instance
	ScreenshotManagerGUI_t3459897460 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ScreenshotManagerGUI_t3459897460_StaticFields, ___instance_2)); }
	inline ScreenshotManagerGUI_t3459897460 * get_instance_2() const { return ___instance_2; }
	inline ScreenshotManagerGUI_t3459897460 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ScreenshotManagerGUI_t3459897460 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
