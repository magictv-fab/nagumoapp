﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPointCallback
struct ResultPointCallback_t207829946;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Detector
struct  Detector_t967800930  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.Detector::image
	BitMatrix_t1058711404 * ___image_0;
	// ZXing.ResultPointCallback ZXing.QrCode.Internal.Detector::resultPointCallback
	ResultPointCallback_t207829946 * ___resultPointCallback_1;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(Detector_t967800930, ___image_0)); }
	inline BitMatrix_t1058711404 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t1058711404 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t1058711404 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_resultPointCallback_1() { return static_cast<int32_t>(offsetof(Detector_t967800930, ___resultPointCallback_1)); }
	inline ResultPointCallback_t207829946 * get_resultPointCallback_1() const { return ___resultPointCallback_1; }
	inline ResultPointCallback_t207829946 ** get_address_of_resultPointCallback_1() { return &___resultPointCallback_1; }
	inline void set_resultPointCallback_1(ResultPointCallback_t207829946 * value)
	{
		___resultPointCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___resultPointCallback_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
