﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct  OutputWindow_t617095569  : public Il2CppObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::window
	ByteU5BU5D_t4260760469* ___window_2;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowEnd
	int32_t ___windowEnd_3;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowFilled
	int32_t ___windowFilled_4;

public:
	inline static int32_t get_offset_of_window_2() { return static_cast<int32_t>(offsetof(OutputWindow_t617095569, ___window_2)); }
	inline ByteU5BU5D_t4260760469* get_window_2() const { return ___window_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_window_2() { return &___window_2; }
	inline void set_window_2(ByteU5BU5D_t4260760469* value)
	{
		___window_2 = value;
		Il2CppCodeGenWriteBarrier(&___window_2, value);
	}

	inline static int32_t get_offset_of_windowEnd_3() { return static_cast<int32_t>(offsetof(OutputWindow_t617095569, ___windowEnd_3)); }
	inline int32_t get_windowEnd_3() const { return ___windowEnd_3; }
	inline int32_t* get_address_of_windowEnd_3() { return &___windowEnd_3; }
	inline void set_windowEnd_3(int32_t value)
	{
		___windowEnd_3 = value;
	}

	inline static int32_t get_offset_of_windowFilled_4() { return static_cast<int32_t>(offsetof(OutputWindow_t617095569, ___windowFilled_4)); }
	inline int32_t get_windowFilled_4() const { return ___windowFilled_4; }
	inline int32_t* get_address_of_windowFilled_4() { return &___windowFilled_4; }
	inline void set_windowFilled_4(int32_t value)
	{
		___windowFilled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
