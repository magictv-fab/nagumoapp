﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveStateToggler
struct ActiveStateToggler_t829858835;

#include "codegen/il2cpp-codegen.h"

// System.Void ActiveStateToggler::.ctor()
extern "C"  void ActiveStateToggler__ctor_m425411048 (ActiveStateToggler_t829858835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveStateToggler::ToggleActive()
extern "C"  void ActiveStateToggler_ToggleActive_m1296786518 (ActiveStateToggler_t829858835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
