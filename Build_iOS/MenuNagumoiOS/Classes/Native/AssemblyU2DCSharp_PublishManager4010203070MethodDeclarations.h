﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager
struct PublishManager_t4010203070;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void PublishManager::.ctor()
extern "C"  void PublishManager__ctor_m4199729821 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::Start()
extern "C"  void PublishManager_Start_m3146867613 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::InstantiateNextItem()
extern "C"  void PublishManager_InstantiateNextItem_m270293995 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::SendStatus(System.Int32)
extern "C"  void PublishManager_SendStatus_m1157927282 (PublishManager_t4010203070 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::Favoritar(System.String)
extern "C"  void PublishManager_Favoritar_m1445022477 (PublishManager_t4010203070 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::DesFavoritar(System.String)
extern "C"  void PublishManager_DesFavoritar_m1038648947 (PublishManager_t4010203070 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::BtnAccept()
extern "C"  void PublishManager_BtnAccept_m2917309503 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::BtnRecuse()
extern "C"  void PublishManager_BtnRecuse_m4270172782 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishManager::IFavoritar(System.String)
extern "C"  Il2CppObject * PublishManager_IFavoritar_m496535298 (PublishManager_t4010203070 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishManager::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * PublishManager_ISendStatus_m177646251 (PublishManager_t4010203070 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishManager::ExcluirFavorito(System.String)
extern "C"  Il2CppObject * PublishManager_ExcluirFavorito_m1826383015 (PublishManager_t4010203070 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishManager::ILoadPublish()
extern "C"  Il2CppObject * PublishManager_ILoadPublish_m3935926895 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::LoadPublishUpdate()
extern "C"  void PublishManager_LoadPublishUpdate_m330588077 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishManager::ILoadPublishUpdate()
extern "C"  Il2CppObject * PublishManager_ILoadPublishUpdate_m445300376 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishManager::ILoadImgs()
extern "C"  Il2CppObject * PublishManager_ILoadImgs_m350719570 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::PopUp(System.String)
extern "C"  void PublishManager_PopUp_m2554001627 (PublishManager_t4010203070 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::Logout()
extern "C"  void PublishManager_Logout_m3775401105 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::OrderInverter()
extern "C"  void PublishManager_OrderInverter_m262916588 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager::OnApplicationQuit()
extern "C"  void PublishManager_OnApplicationQuit_m2782344347 (PublishManager_t4010203070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
