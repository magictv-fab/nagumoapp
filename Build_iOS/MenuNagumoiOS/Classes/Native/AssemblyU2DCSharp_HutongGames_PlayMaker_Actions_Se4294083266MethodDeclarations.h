﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimationWeight
struct SetAnimationWeight_t4294083266;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::.ctor()
extern "C"  void SetAnimationWeight__ctor_m4055348196 (SetAnimationWeight_t4294083266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::Reset()
extern "C"  void SetAnimationWeight_Reset_m1701781137 (SetAnimationWeight_t4294083266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::OnEnter()
extern "C"  void SetAnimationWeight_OnEnter_m1046007483 (SetAnimationWeight_t4294083266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::OnUpdate()
extern "C"  void SetAnimationWeight_OnUpdate_m1495020264 (SetAnimationWeight_t4294083266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::DoSetAnimationWeight(UnityEngine.GameObject)
extern "C"  void SetAnimationWeight_DoSetAnimationWeight_m2014377373 (SetAnimationWeight_t4294083266 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
