﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<ConfigurableListenerEventInterface>
struct List_1_t3353814812;
// System.Collections.Generic.List`1<OnOffListenerEventInterface>
struct List_1_t1033361267;
// System.Collections.Generic.Dictionary`2<System.String,ConfigurableListenerEventInterface>
struct Dictionary_2_t2806047630;
// System.Collections.Generic.Dictionary`2<System.String,OnOffListenerEventInterface>
struct Dictionary_2_t485594085;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugsConfigGui
struct  DebugsConfigGui_t3114428889  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean DebugsConfigGui::_active
	bool ____active_2;
	// System.String DebugsConfigGui::currentWindow
	String_t* ___currentWindow_3;
	// System.Collections.Generic.List`1<System.String> DebugsConfigGui::historyWindows
	List_1_t1375417109 * ___historyWindows_4;
	// System.Boolean DebugsConfigGui::_hasConfigurablesDebug
	bool ____hasConfigurablesDebug_5;
	// System.Boolean DebugsConfigGui::_hasOnOffsDebug
	bool ____hasOnOffsDebug_6;
	// System.Collections.Generic.List`1<ConfigurableListenerEventInterface> DebugsConfigGui::configurables
	List_1_t3353814812 * ___configurables_7;
	// System.Collections.Generic.List`1<OnOffListenerEventInterface> DebugsConfigGui::onOffs
	List_1_t1033361267 * ___onOffs_8;
	// System.Collections.Generic.Dictionary`2<System.String,ConfigurableListenerEventInterface> DebugsConfigGui::_configurableComponents
	Dictionary_2_t2806047630 * ____configurableComponents_9;
	// System.Collections.Generic.Dictionary`2<System.String,OnOffListenerEventInterface> DebugsConfigGui::_onOffs
	Dictionary_2_t485594085 * ____onOffs_10;
	// UnityEngine.GameObject[] DebugsConfigGui::gameObjectsToFind
	GameObjectU5BU5D_t2662109048* ___gameObjectsToFind_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DebugsConfigGui::DebugOnOffComponents
	List_1_t747900261 * ___DebugOnOffComponents_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DebugsConfigGui::DebugConfigurableComponents
	List_1_t747900261 * ___DebugConfigurableComponents_13;
	// System.String[] DebugsConfigGui::_buttonsOnsNames
	StringU5BU5D_t4054002952* ____buttonsOnsNames_14;
	// System.String[] DebugsConfigGui::_buttonsConfigNames
	StringU5BU5D_t4054002952* ____buttonsConfigNames_15;
	// System.Boolean DebugsConfigGui::_isRootWindow
	bool ____isRootWindow_16;

public:
	inline static int32_t get_offset_of__active_2() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____active_2)); }
	inline bool get__active_2() const { return ____active_2; }
	inline bool* get_address_of__active_2() { return &____active_2; }
	inline void set__active_2(bool value)
	{
		____active_2 = value;
	}

	inline static int32_t get_offset_of_currentWindow_3() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___currentWindow_3)); }
	inline String_t* get_currentWindow_3() const { return ___currentWindow_3; }
	inline String_t** get_address_of_currentWindow_3() { return &___currentWindow_3; }
	inline void set_currentWindow_3(String_t* value)
	{
		___currentWindow_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentWindow_3, value);
	}

	inline static int32_t get_offset_of_historyWindows_4() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___historyWindows_4)); }
	inline List_1_t1375417109 * get_historyWindows_4() const { return ___historyWindows_4; }
	inline List_1_t1375417109 ** get_address_of_historyWindows_4() { return &___historyWindows_4; }
	inline void set_historyWindows_4(List_1_t1375417109 * value)
	{
		___historyWindows_4 = value;
		Il2CppCodeGenWriteBarrier(&___historyWindows_4, value);
	}

	inline static int32_t get_offset_of__hasConfigurablesDebug_5() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____hasConfigurablesDebug_5)); }
	inline bool get__hasConfigurablesDebug_5() const { return ____hasConfigurablesDebug_5; }
	inline bool* get_address_of__hasConfigurablesDebug_5() { return &____hasConfigurablesDebug_5; }
	inline void set__hasConfigurablesDebug_5(bool value)
	{
		____hasConfigurablesDebug_5 = value;
	}

	inline static int32_t get_offset_of__hasOnOffsDebug_6() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____hasOnOffsDebug_6)); }
	inline bool get__hasOnOffsDebug_6() const { return ____hasOnOffsDebug_6; }
	inline bool* get_address_of__hasOnOffsDebug_6() { return &____hasOnOffsDebug_6; }
	inline void set__hasOnOffsDebug_6(bool value)
	{
		____hasOnOffsDebug_6 = value;
	}

	inline static int32_t get_offset_of_configurables_7() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___configurables_7)); }
	inline List_1_t3353814812 * get_configurables_7() const { return ___configurables_7; }
	inline List_1_t3353814812 ** get_address_of_configurables_7() { return &___configurables_7; }
	inline void set_configurables_7(List_1_t3353814812 * value)
	{
		___configurables_7 = value;
		Il2CppCodeGenWriteBarrier(&___configurables_7, value);
	}

	inline static int32_t get_offset_of_onOffs_8() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___onOffs_8)); }
	inline List_1_t1033361267 * get_onOffs_8() const { return ___onOffs_8; }
	inline List_1_t1033361267 ** get_address_of_onOffs_8() { return &___onOffs_8; }
	inline void set_onOffs_8(List_1_t1033361267 * value)
	{
		___onOffs_8 = value;
		Il2CppCodeGenWriteBarrier(&___onOffs_8, value);
	}

	inline static int32_t get_offset_of__configurableComponents_9() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____configurableComponents_9)); }
	inline Dictionary_2_t2806047630 * get__configurableComponents_9() const { return ____configurableComponents_9; }
	inline Dictionary_2_t2806047630 ** get_address_of__configurableComponents_9() { return &____configurableComponents_9; }
	inline void set__configurableComponents_9(Dictionary_2_t2806047630 * value)
	{
		____configurableComponents_9 = value;
		Il2CppCodeGenWriteBarrier(&____configurableComponents_9, value);
	}

	inline static int32_t get_offset_of__onOffs_10() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____onOffs_10)); }
	inline Dictionary_2_t485594085 * get__onOffs_10() const { return ____onOffs_10; }
	inline Dictionary_2_t485594085 ** get_address_of__onOffs_10() { return &____onOffs_10; }
	inline void set__onOffs_10(Dictionary_2_t485594085 * value)
	{
		____onOffs_10 = value;
		Il2CppCodeGenWriteBarrier(&____onOffs_10, value);
	}

	inline static int32_t get_offset_of_gameObjectsToFind_11() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___gameObjectsToFind_11)); }
	inline GameObjectU5BU5D_t2662109048* get_gameObjectsToFind_11() const { return ___gameObjectsToFind_11; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_gameObjectsToFind_11() { return &___gameObjectsToFind_11; }
	inline void set_gameObjectsToFind_11(GameObjectU5BU5D_t2662109048* value)
	{
		___gameObjectsToFind_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectsToFind_11, value);
	}

	inline static int32_t get_offset_of_DebugOnOffComponents_12() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___DebugOnOffComponents_12)); }
	inline List_1_t747900261 * get_DebugOnOffComponents_12() const { return ___DebugOnOffComponents_12; }
	inline List_1_t747900261 ** get_address_of_DebugOnOffComponents_12() { return &___DebugOnOffComponents_12; }
	inline void set_DebugOnOffComponents_12(List_1_t747900261 * value)
	{
		___DebugOnOffComponents_12 = value;
		Il2CppCodeGenWriteBarrier(&___DebugOnOffComponents_12, value);
	}

	inline static int32_t get_offset_of_DebugConfigurableComponents_13() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ___DebugConfigurableComponents_13)); }
	inline List_1_t747900261 * get_DebugConfigurableComponents_13() const { return ___DebugConfigurableComponents_13; }
	inline List_1_t747900261 ** get_address_of_DebugConfigurableComponents_13() { return &___DebugConfigurableComponents_13; }
	inline void set_DebugConfigurableComponents_13(List_1_t747900261 * value)
	{
		___DebugConfigurableComponents_13 = value;
		Il2CppCodeGenWriteBarrier(&___DebugConfigurableComponents_13, value);
	}

	inline static int32_t get_offset_of__buttonsOnsNames_14() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____buttonsOnsNames_14)); }
	inline StringU5BU5D_t4054002952* get__buttonsOnsNames_14() const { return ____buttonsOnsNames_14; }
	inline StringU5BU5D_t4054002952** get_address_of__buttonsOnsNames_14() { return &____buttonsOnsNames_14; }
	inline void set__buttonsOnsNames_14(StringU5BU5D_t4054002952* value)
	{
		____buttonsOnsNames_14 = value;
		Il2CppCodeGenWriteBarrier(&____buttonsOnsNames_14, value);
	}

	inline static int32_t get_offset_of__buttonsConfigNames_15() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____buttonsConfigNames_15)); }
	inline StringU5BU5D_t4054002952* get__buttonsConfigNames_15() const { return ____buttonsConfigNames_15; }
	inline StringU5BU5D_t4054002952** get_address_of__buttonsConfigNames_15() { return &____buttonsConfigNames_15; }
	inline void set__buttonsConfigNames_15(StringU5BU5D_t4054002952* value)
	{
		____buttonsConfigNames_15 = value;
		Il2CppCodeGenWriteBarrier(&____buttonsConfigNames_15, value);
	}

	inline static int32_t get_offset_of__isRootWindow_16() { return static_cast<int32_t>(offsetof(DebugsConfigGui_t3114428889, ____isRootWindow_16)); }
	inline bool get__isRootWindow_16() const { return ____isRootWindow_16; }
	inline bool* get_address_of__isRootWindow_16() { return &____isRootWindow_16; }
	inline void set__isRootWindow_16(bool value)
	{
		____isRootWindow_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
