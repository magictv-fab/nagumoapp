﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Action
struct Action_t3771233898;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.InAppAbstract
struct  InAppAbstract_t382673128  : public GeneralComponentsAbstract_t3900398046
{
public:
	// System.Int32 MagicTV.abstracts.InAppAbstract::_delayToRun
	int32_t ____delayToRun_4;
	// System.Boolean MagicTV.abstracts.InAppAbstract::_waitingToRun
	bool ____waitingToRun_5;
	// System.Boolean MagicTV.abstracts.InAppAbstract::_delayFinished
	bool ____delayFinished_6;
	// System.Int32 MagicTV.abstracts.InAppAbstract::_timerRunningId
	int32_t ____timerRunningId_7;
	// System.Boolean MagicTV.abstracts.InAppAbstract::_delayResetOnStop
	bool ____delayResetOnStop_8;
	// System.Int32 MagicTV.abstracts.InAppAbstract::_delayPaused
	int32_t ____delayPaused_9;
	// System.Collections.Generic.List`1<System.String> MagicTV.abstracts.InAppAbstract::DebugMetas
	List_1_t1375417109 * ___DebugMetas_10;
	// System.Action MagicTV.abstracts.InAppAbstract::onQuit
	Action_t3771233898 * ___onQuit_11;
	// MagicTV.vo.BundleVO MagicTV.abstracts.InAppAbstract::_vo
	BundleVO_t1984518073 * ____vo_12;
	// MagicTV.globals.InAppAnalytics MagicTV.abstracts.InAppAbstract::_analytics
	InAppAnalytics_t2316734034 * ____analytics_13;

public:
	inline static int32_t get_offset_of__delayToRun_4() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____delayToRun_4)); }
	inline int32_t get__delayToRun_4() const { return ____delayToRun_4; }
	inline int32_t* get_address_of__delayToRun_4() { return &____delayToRun_4; }
	inline void set__delayToRun_4(int32_t value)
	{
		____delayToRun_4 = value;
	}

	inline static int32_t get_offset_of__waitingToRun_5() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____waitingToRun_5)); }
	inline bool get__waitingToRun_5() const { return ____waitingToRun_5; }
	inline bool* get_address_of__waitingToRun_5() { return &____waitingToRun_5; }
	inline void set__waitingToRun_5(bool value)
	{
		____waitingToRun_5 = value;
	}

	inline static int32_t get_offset_of__delayFinished_6() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____delayFinished_6)); }
	inline bool get__delayFinished_6() const { return ____delayFinished_6; }
	inline bool* get_address_of__delayFinished_6() { return &____delayFinished_6; }
	inline void set__delayFinished_6(bool value)
	{
		____delayFinished_6 = value;
	}

	inline static int32_t get_offset_of__timerRunningId_7() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____timerRunningId_7)); }
	inline int32_t get__timerRunningId_7() const { return ____timerRunningId_7; }
	inline int32_t* get_address_of__timerRunningId_7() { return &____timerRunningId_7; }
	inline void set__timerRunningId_7(int32_t value)
	{
		____timerRunningId_7 = value;
	}

	inline static int32_t get_offset_of__delayResetOnStop_8() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____delayResetOnStop_8)); }
	inline bool get__delayResetOnStop_8() const { return ____delayResetOnStop_8; }
	inline bool* get_address_of__delayResetOnStop_8() { return &____delayResetOnStop_8; }
	inline void set__delayResetOnStop_8(bool value)
	{
		____delayResetOnStop_8 = value;
	}

	inline static int32_t get_offset_of__delayPaused_9() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____delayPaused_9)); }
	inline int32_t get__delayPaused_9() const { return ____delayPaused_9; }
	inline int32_t* get_address_of__delayPaused_9() { return &____delayPaused_9; }
	inline void set__delayPaused_9(int32_t value)
	{
		____delayPaused_9 = value;
	}

	inline static int32_t get_offset_of_DebugMetas_10() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ___DebugMetas_10)); }
	inline List_1_t1375417109 * get_DebugMetas_10() const { return ___DebugMetas_10; }
	inline List_1_t1375417109 ** get_address_of_DebugMetas_10() { return &___DebugMetas_10; }
	inline void set_DebugMetas_10(List_1_t1375417109 * value)
	{
		___DebugMetas_10 = value;
		Il2CppCodeGenWriteBarrier(&___DebugMetas_10, value);
	}

	inline static int32_t get_offset_of_onQuit_11() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ___onQuit_11)); }
	inline Action_t3771233898 * get_onQuit_11() const { return ___onQuit_11; }
	inline Action_t3771233898 ** get_address_of_onQuit_11() { return &___onQuit_11; }
	inline void set_onQuit_11(Action_t3771233898 * value)
	{
		___onQuit_11 = value;
		Il2CppCodeGenWriteBarrier(&___onQuit_11, value);
	}

	inline static int32_t get_offset_of__vo_12() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____vo_12)); }
	inline BundleVO_t1984518073 * get__vo_12() const { return ____vo_12; }
	inline BundleVO_t1984518073 ** get_address_of__vo_12() { return &____vo_12; }
	inline void set__vo_12(BundleVO_t1984518073 * value)
	{
		____vo_12 = value;
		Il2CppCodeGenWriteBarrier(&____vo_12, value);
	}

	inline static int32_t get_offset_of__analytics_13() { return static_cast<int32_t>(offsetof(InAppAbstract_t382673128, ____analytics_13)); }
	inline InAppAnalytics_t2316734034 * get__analytics_13() const { return ____analytics_13; }
	inline InAppAnalytics_t2316734034 ** get_address_of__analytics_13() { return &____analytics_13; }
	inline void set__analytics_13(InAppAnalytics_t2316734034 * value)
	{
		____analytics_13 = value;
		Il2CppCodeGenWriteBarrier(&____analytics_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
