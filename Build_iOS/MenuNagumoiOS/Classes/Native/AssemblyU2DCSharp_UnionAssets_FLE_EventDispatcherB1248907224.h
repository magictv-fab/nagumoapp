﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>>
struct Dictionary_2_t4037634331;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>
struct Dictionary_2_t1804416613;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnionAssets.FLE.EventDispatcherBase
struct  EventDispatcherBase_t1248907224  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>> UnionAssets.FLE.EventDispatcherBase::listners
	Dictionary_2_t4037634331 * ___listners_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>> UnionAssets.FLE.EventDispatcherBase::dataListners
	Dictionary_2_t1804416613 * ___dataListners_1;

public:
	inline static int32_t get_offset_of_listners_0() { return static_cast<int32_t>(offsetof(EventDispatcherBase_t1248907224, ___listners_0)); }
	inline Dictionary_2_t4037634331 * get_listners_0() const { return ___listners_0; }
	inline Dictionary_2_t4037634331 ** get_address_of_listners_0() { return &___listners_0; }
	inline void set_listners_0(Dictionary_2_t4037634331 * value)
	{
		___listners_0 = value;
		Il2CppCodeGenWriteBarrier(&___listners_0, value);
	}

	inline static int32_t get_offset_of_dataListners_1() { return static_cast<int32_t>(offsetof(EventDispatcherBase_t1248907224, ___dataListners_1)); }
	inline Dictionary_2_t1804416613 * get_dataListners_1() const { return ___dataListners_1; }
	inline Dictionary_2_t1804416613 ** get_address_of_dataListners_1() { return &___dataListners_1; }
	inline void set_dataListners_1(Dictionary_2_t1804416613 * value)
	{
		___dataListners_1 = value;
		Il2CppCodeGenWriteBarrier(&___dataListners_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
