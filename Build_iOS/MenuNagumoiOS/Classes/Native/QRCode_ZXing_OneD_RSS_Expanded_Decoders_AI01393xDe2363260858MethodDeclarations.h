﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder
struct AI01393xDecoder_t2363260858;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AI01393xDecoder__ctor_m167148854 (AI01393xDecoder_t2363260858 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::parseInformation()
extern "C"  String_t* AI01393xDecoder_parseInformation_m3652034185 (AI01393xDecoder_t2363260858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::.cctor()
extern "C"  void AI01393xDecoder__cctor_m1743200758 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
