﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t153068654;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t4024365272;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct  ZipAESTransform_t496812608  : public Il2CppObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_blockSize
	int32_t ____blockSize_3;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptor
	Il2CppObject * ____encryptor_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_counterNonce
	ByteU5BU5D_t4260760469* ____counterNonce_5;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptBuffer
	ByteU5BU5D_t4260760469* ____encryptBuffer_6;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encrPos
	int32_t ____encrPos_7;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_pwdVerifier
	ByteU5BU5D_t4260760469* ____pwdVerifier_8;
	// System.Security.Cryptography.HMACSHA1 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_hmacsha1
	HMACSHA1_t4024365272 * ____hmacsha1_9;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_finalised
	bool ____finalised_10;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_writeMode
	bool ____writeMode_11;

public:
	inline static int32_t get_offset_of__blockSize_3() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____blockSize_3)); }
	inline int32_t get__blockSize_3() const { return ____blockSize_3; }
	inline int32_t* get_address_of__blockSize_3() { return &____blockSize_3; }
	inline void set__blockSize_3(int32_t value)
	{
		____blockSize_3 = value;
	}

	inline static int32_t get_offset_of__encryptor_4() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____encryptor_4)); }
	inline Il2CppObject * get__encryptor_4() const { return ____encryptor_4; }
	inline Il2CppObject ** get_address_of__encryptor_4() { return &____encryptor_4; }
	inline void set__encryptor_4(Il2CppObject * value)
	{
		____encryptor_4 = value;
		Il2CppCodeGenWriteBarrier(&____encryptor_4, value);
	}

	inline static int32_t get_offset_of__counterNonce_5() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____counterNonce_5)); }
	inline ByteU5BU5D_t4260760469* get__counterNonce_5() const { return ____counterNonce_5; }
	inline ByteU5BU5D_t4260760469** get_address_of__counterNonce_5() { return &____counterNonce_5; }
	inline void set__counterNonce_5(ByteU5BU5D_t4260760469* value)
	{
		____counterNonce_5 = value;
		Il2CppCodeGenWriteBarrier(&____counterNonce_5, value);
	}

	inline static int32_t get_offset_of__encryptBuffer_6() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____encryptBuffer_6)); }
	inline ByteU5BU5D_t4260760469* get__encryptBuffer_6() const { return ____encryptBuffer_6; }
	inline ByteU5BU5D_t4260760469** get_address_of__encryptBuffer_6() { return &____encryptBuffer_6; }
	inline void set__encryptBuffer_6(ByteU5BU5D_t4260760469* value)
	{
		____encryptBuffer_6 = value;
		Il2CppCodeGenWriteBarrier(&____encryptBuffer_6, value);
	}

	inline static int32_t get_offset_of__encrPos_7() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____encrPos_7)); }
	inline int32_t get__encrPos_7() const { return ____encrPos_7; }
	inline int32_t* get_address_of__encrPos_7() { return &____encrPos_7; }
	inline void set__encrPos_7(int32_t value)
	{
		____encrPos_7 = value;
	}

	inline static int32_t get_offset_of__pwdVerifier_8() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____pwdVerifier_8)); }
	inline ByteU5BU5D_t4260760469* get__pwdVerifier_8() const { return ____pwdVerifier_8; }
	inline ByteU5BU5D_t4260760469** get_address_of__pwdVerifier_8() { return &____pwdVerifier_8; }
	inline void set__pwdVerifier_8(ByteU5BU5D_t4260760469* value)
	{
		____pwdVerifier_8 = value;
		Il2CppCodeGenWriteBarrier(&____pwdVerifier_8, value);
	}

	inline static int32_t get_offset_of__hmacsha1_9() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____hmacsha1_9)); }
	inline HMACSHA1_t4024365272 * get__hmacsha1_9() const { return ____hmacsha1_9; }
	inline HMACSHA1_t4024365272 ** get_address_of__hmacsha1_9() { return &____hmacsha1_9; }
	inline void set__hmacsha1_9(HMACSHA1_t4024365272 * value)
	{
		____hmacsha1_9 = value;
		Il2CppCodeGenWriteBarrier(&____hmacsha1_9, value);
	}

	inline static int32_t get_offset_of__finalised_10() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____finalised_10)); }
	inline bool get__finalised_10() const { return ____finalised_10; }
	inline bool* get_address_of__finalised_10() { return &____finalised_10; }
	inline void set__finalised_10(bool value)
	{
		____finalised_10 = value;
	}

	inline static int32_t get_offset_of__writeMode_11() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____writeMode_11)); }
	inline bool get__writeMode_11() const { return ____writeMode_11; }
	inline bool* get_address_of__writeMode_11() { return &____writeMode_11; }
	inline void set__writeMode_11(bool value)
	{
		____writeMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
