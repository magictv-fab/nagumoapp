﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolAnyTrue
struct BoolAnyTrue_t539162078;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::.ctor()
extern "C"  void BoolAnyTrue__ctor_m3459755800 (BoolAnyTrue_t539162078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::Reset()
extern "C"  void BoolAnyTrue_Reset_m1106188741 (BoolAnyTrue_t539162078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::OnEnter()
extern "C"  void BoolAnyTrue_OnEnter_m4207332591 (BoolAnyTrue_t539162078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::OnUpdate()
extern "C"  void BoolAnyTrue_OnUpdate_m711850804 (BoolAnyTrue_t539162078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::DoAnyTrue()
extern "C"  void BoolAnyTrue_DoAnyTrue_m2370568325 (BoolAnyTrue_t539162078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
