﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.ResultPointCallback
struct ResultPointCallback_t207829946;
// System.Object
struct Il2CppObject;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ZXing.ResultPointCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ResultPointCallback__ctor_m3045382437 (ResultPointCallback_t207829946 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.ResultPointCallback::Invoke(ZXing.ResultPoint)
extern "C"  void ResultPointCallback_Invoke_m4074197546 (ResultPointCallback_t207829946 * __this, ResultPoint_t1538592853 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ZXing.ResultPointCallback::BeginInvoke(ZXing.ResultPoint,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResultPointCallback_BeginInvoke_m110987337 (ResultPointCallback_t207829946 * __this, ResultPoint_t1538592853 * ___point0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.ResultPointCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ResultPointCallback_EndInvoke_m3465848245 (ResultPointCallback_t207829946 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
