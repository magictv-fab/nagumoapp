﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute2523058482.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonProperty
struct  JsonProperty_t2248022781  : public Attribute_t2523058482
{
public:
	// System.String JsonProperty::PropertyName
	String_t* ___PropertyName_0;

public:
	inline static int32_t get_offset_of_PropertyName_0() { return static_cast<int32_t>(offsetof(JsonProperty_t2248022781, ___PropertyName_0)); }
	inline String_t* get_PropertyName_0() const { return ___PropertyName_0; }
	inline String_t** get_address_of_PropertyName_0() { return &___PropertyName_0; }
	inline void set_PropertyName_0(String_t* value)
	{
		___PropertyName_0 = value;
		Il2CppCodeGenWriteBarrier(&___PropertyName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
