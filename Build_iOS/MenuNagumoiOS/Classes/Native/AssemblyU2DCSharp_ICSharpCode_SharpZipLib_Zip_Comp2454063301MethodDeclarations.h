﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct Deflater_t2454063301;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1587604368.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor()
extern "C"  void Deflater__ctor_m365211342 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32)
extern "C"  void Deflater__ctor_m2720833311 (Deflater_t2454063301 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern "C"  void Deflater__ctor_m889959038 (Deflater_t2454063301 * __this, int32_t ___level0, bool ___noZlibHeaderOrFooter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Reset()
extern "C"  void Deflater_Reset_m2306611579 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_Adler()
extern "C"  int32_t Deflater_get_Adler_m1674305739 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
extern "C"  int64_t Deflater_get_TotalIn_m2453635037 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_TotalOut()
extern "C"  int64_t Deflater_get_TotalOut_m3054064824 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Flush()
extern "C"  void Deflater_Flush_m449158640 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Finish()
extern "C"  void Deflater_Finish_m2462222153 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern "C"  bool Deflater_get_IsFinished_m1770549039 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern "C"  bool Deflater_get_IsNeedingInput_m1363416411 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[])
extern "C"  void Deflater_SetInput_m2633401291 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void Deflater_SetInput_m2290278763 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___input0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern "C"  void Deflater_SetLevel_m829415241 (Deflater_t2454063301 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::GetLevel()
extern "C"  int32_t Deflater_GetLevel_m4243126674 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetStrategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern "C"  void Deflater_SetStrategy_m719377081 (Deflater_t2454063301 * __this, int32_t ___strategy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[])
extern "C"  int32_t Deflater_Deflate_m1217314464 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___output0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t Deflater_Deflate_m1952025600 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetDictionary(System.Byte[])
extern "C"  void Deflater_SetDictionary_m1425894597 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetDictionary(System.Byte[],System.Int32,System.Int32)
extern "C"  void Deflater_SetDictionary_m358753765 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___dictionary0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
