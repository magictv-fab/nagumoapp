﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.AlignmentPattern
struct AlignmentPattern_t1786970569;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.AlignmentPattern::.ctor(System.Single,System.Single,System.Single)
extern "C"  void AlignmentPattern__ctor_m3786214722 (AlignmentPattern_t1786970569 * __this, float ___posX0, float ___posY1, float ___estimatedModuleSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.AlignmentPattern::aboutEquals(System.Single,System.Single,System.Single)
extern "C"  bool AlignmentPattern_aboutEquals_m1296811404 (AlignmentPattern_t1786970569 * __this, float ___moduleSize0, float ___i1, float ___j2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPattern::combineEstimate(System.Single,System.Single,System.Single)
extern "C"  AlignmentPattern_t1786970569 * AlignmentPattern_combineEstimate_m3748352242 (AlignmentPattern_t1786970569 * __this, float ___i0, float ___j1, float ___newModuleSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
