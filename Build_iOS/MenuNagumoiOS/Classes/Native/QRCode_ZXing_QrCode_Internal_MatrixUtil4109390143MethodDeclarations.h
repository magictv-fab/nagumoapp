﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_ByteMatrix2072255685.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"

// System.Void ZXing.QrCode.Internal.MatrixUtil::clearMatrix(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_clearMatrix_m1167357413 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::buildMatrix(ZXing.Common.BitArray,ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Version,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_buildMatrix_m3233338916 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___dataBits0, ErrorCorrectionLevel_t1225927610 * ___ecLevel1, Version_t1953509534 * ___version2, int32_t ___maskPattern3, ByteMatrix_t2072255685 * ___matrix4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedBasicPatterns(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedBasicPatterns_m4164843564 (Il2CppObject * __this /* static, unused */, Version_t1953509534 * ___version0, ByteMatrix_t2072255685 * ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedTypeInfo(ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedTypeInfo_m3579401230 (Il2CppObject * __this /* static, unused */, ErrorCorrectionLevel_t1225927610 * ___ecLevel0, int32_t ___maskPattern1, ByteMatrix_t2072255685 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::maybeEmbedVersionInfo(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_maybeEmbedVersionInfo_m3015810397 (Il2CppObject * __this /* static, unused */, Version_t1953509534 * ___version0, ByteMatrix_t2072255685 * ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedDataBits(ZXing.Common.BitArray,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedDataBits_m2252514292 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___dataBits0, int32_t ___maskPattern1, ByteMatrix_t2072255685 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.MatrixUtil::findMSBSet(System.Int32)
extern "C"  int32_t MatrixUtil_findMSBSet_m178289189 (Il2CppObject * __this /* static, unused */, int32_t ___value_Renamed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.MatrixUtil::calculateBCHCode(System.Int32,System.Int32)
extern "C"  int32_t MatrixUtil_calculateBCHCode_m771642947 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___poly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::makeTypeInfoBits(ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Int32,ZXing.Common.BitArray)
extern "C"  void MatrixUtil_makeTypeInfoBits_m2602443796 (Il2CppObject * __this /* static, unused */, ErrorCorrectionLevel_t1225927610 * ___ecLevel0, int32_t ___maskPattern1, BitArray_t4163851164 * ___bits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::makeVersionInfoBits(ZXing.QrCode.Internal.Version,ZXing.Common.BitArray)
extern "C"  void MatrixUtil_makeVersionInfoBits_m2751930627 (Il2CppObject * __this /* static, unused */, Version_t1953509534 * ___version0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.MatrixUtil::isEmpty(System.Int32)
extern "C"  bool MatrixUtil_isEmpty_m93146555 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedTimingPatterns(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedTimingPatterns_m337610237 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedDarkDotAtLeftBottomCorner(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedDarkDotAtLeftBottomCorner_m1529637847 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedHorizontalSeparationPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedHorizontalSeparationPattern_m3877227888 (Il2CppObject * __this /* static, unused */, int32_t ___xStart0, int32_t ___yStart1, ByteMatrix_t2072255685 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedVerticalSeparationPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedVerticalSeparationPattern_m2961761950 (Il2CppObject * __this /* static, unused */, int32_t ___xStart0, int32_t ___yStart1, ByteMatrix_t2072255685 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionAdjustmentPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedPositionAdjustmentPattern_m3796308036 (Il2CppObject * __this /* static, unused */, int32_t ___xStart0, int32_t ___yStart1, ByteMatrix_t2072255685 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionDetectionPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedPositionDetectionPattern_m1666620388 (Il2CppObject * __this /* static, unused */, int32_t ___xStart0, int32_t ___yStart1, ByteMatrix_t2072255685 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionDetectionPatternsAndSeparators(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_embedPositionDetectionPatternsAndSeparators_m3994476726 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::maybeEmbedPositionAdjustmentPatterns(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void MatrixUtil_maybeEmbedPositionAdjustmentPatterns_m4148737150 (Il2CppObject * __this /* static, unused */, Version_t1953509534 * ___version0, ByteMatrix_t2072255685 * ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.MatrixUtil::.cctor()
extern "C"  void MatrixUtil__cctor_m3471126448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
