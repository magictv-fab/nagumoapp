﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublishLink/<checkInternetConnection>c__Iterator73
struct  U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968  : public Il2CppObject
{
public:
	// UnityEngine.WWW PublishLink/<checkInternetConnection>c__Iterator73::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// System.Action`1<System.Boolean> PublishLink/<checkInternetConnection>c__Iterator73::action
	Action_1_t872614854 * ___action_1;
	// System.Int32 PublishLink/<checkInternetConnection>c__Iterator73::$PC
	int32_t ___U24PC_2;
	// System.Object PublishLink/<checkInternetConnection>c__Iterator73::$current
	Il2CppObject * ___U24current_3;
	// System.Action`1<System.Boolean> PublishLink/<checkInternetConnection>c__Iterator73::<$>action
	Action_1_t872614854 * ___U3CU24U3Eaction_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968, ___action_1)); }
	inline Action_1_t872614854 * get_action_1() const { return ___action_1; }
	inline Action_1_t872614854 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t872614854 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eaction_4() { return static_cast<int32_t>(offsetof(U3CcheckInternetConnectionU3Ec__Iterator73_t3649015968, ___U3CU24U3Eaction_4)); }
	inline Action_1_t872614854 * get_U3CU24U3Eaction_4() const { return ___U3CU24U3Eaction_4; }
	inline Action_1_t872614854 ** get_address_of_U3CU24U3Eaction_4() { return &___U3CU24U3Eaction_4; }
	inline void set_U3CU24U3Eaction_4(Action_1_t872614854 * value)
	{
		___U3CU24U3Eaction_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eaction_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
