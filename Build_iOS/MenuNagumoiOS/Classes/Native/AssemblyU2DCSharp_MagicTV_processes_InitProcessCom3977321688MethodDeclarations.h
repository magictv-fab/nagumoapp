﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.processes.InitProcessComponent
struct InitProcessComponent_t3977321688;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.processes.InitProcessComponent::.ctor()
extern "C"  void InitProcessComponent__ctor_m53047501 (InitProcessComponent_t3977321688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.InitProcessComponent::prepare()
extern "C"  void InitProcessComponent_prepare_m366021330 (InitProcessComponent_t3977321688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.InitProcessComponent::componentComplete()
extern "C"  void InitProcessComponent_componentComplete_m264595169 (InitProcessComponent_t3977321688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
