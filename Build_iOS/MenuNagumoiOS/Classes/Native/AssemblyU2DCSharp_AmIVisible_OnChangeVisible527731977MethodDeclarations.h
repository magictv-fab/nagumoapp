﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AmIVisible/OnChangeVisible
struct OnChangeVisible_t527731977;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void AmIVisible/OnChangeVisible::.ctor(System.Object,System.IntPtr)
extern "C"  void OnChangeVisible__ctor_m4079986480 (OnChangeVisible_t527731977 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmIVisible/OnChangeVisible::Invoke(System.Boolean)
extern "C"  void OnChangeVisible_Invoke_m4124224385 (OnChangeVisible_t527731977 * __this, bool ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AmIVisible/OnChangeVisible::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnChangeVisible_BeginInvoke_m1343680174 (OnChangeVisible_t527731977 * __this, bool ___m0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmIVisible/OnChangeVisible::EndInvoke(System.IAsyncResult)
extern "C"  void OnChangeVisible_EndInvoke_m643182912 (OnChangeVisible_t527731977 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
