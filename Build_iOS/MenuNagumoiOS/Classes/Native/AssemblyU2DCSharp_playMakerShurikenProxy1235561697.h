﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t4168492807;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// playMakerShurikenProxy
struct  playMakerShurikenProxy_t1235561697  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.ParticleCollisionEvent[] playMakerShurikenProxy::collisionEvents
	ParticleCollisionEventU5BU5D_t4168492807* ___collisionEvents_2;
	// PlayMakerFSM playMakerShurikenProxy::_fsm
	PlayMakerFSM_t3799847376 * ____fsm_3;

public:
	inline static int32_t get_offset_of_collisionEvents_2() { return static_cast<int32_t>(offsetof(playMakerShurikenProxy_t1235561697, ___collisionEvents_2)); }
	inline ParticleCollisionEventU5BU5D_t4168492807* get_collisionEvents_2() const { return ___collisionEvents_2; }
	inline ParticleCollisionEventU5BU5D_t4168492807** get_address_of_collisionEvents_2() { return &___collisionEvents_2; }
	inline void set_collisionEvents_2(ParticleCollisionEventU5BU5D_t4168492807* value)
	{
		___collisionEvents_2 = value;
		Il2CppCodeGenWriteBarrier(&___collisionEvents_2, value);
	}

	inline static int32_t get_offset_of__fsm_3() { return static_cast<int32_t>(offsetof(playMakerShurikenProxy_t1235561697, ____fsm_3)); }
	inline PlayMakerFSM_t3799847376 * get__fsm_3() const { return ____fsm_3; }
	inline PlayMakerFSM_t3799847376 ** get_address_of__fsm_3() { return &____fsm_3; }
	inline void set__fsm_3(PlayMakerFSM_t3799847376 * value)
	{
		____fsm_3 = value;
		Il2CppCodeGenWriteBarrier(&____fsm_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
