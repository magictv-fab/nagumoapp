﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.Sprite>>
struct Dictionary_2_t1092803867;
// DownloadImages/OnLoadCompleted
struct OnLoadCompleted_t2465843575;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.WWW>
struct List_1_t207839261;
// ChangeBackgroundImage
struct ChangeBackgroundImage_t1317572317;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadImages
struct  DownloadImages_t3644489024  : public MonoBehaviour_t667441552
{
public:
	// DownloadImages/OnLoadCompleted DownloadImages::onLoadCompleted
	OnLoadCompleted_t2465843575 * ___onLoadCompleted_3;
	// System.String DownloadImages::url
	String_t* ___url_4;
	// System.String DownloadImages::pathFiles
	String_t* ___pathFiles_5;
	// System.Collections.Generic.List`1<UnityEngine.WWW> DownloadImages::wwws
	List_1_t207839261 * ___wwws_6;
	// ChangeBackgroundImage DownloadImages::changeBackgroundImage
	ChangeBackgroundImage_t1317572317 * ___changeBackgroundImage_7;

public:
	inline static int32_t get_offset_of_onLoadCompleted_3() { return static_cast<int32_t>(offsetof(DownloadImages_t3644489024, ___onLoadCompleted_3)); }
	inline OnLoadCompleted_t2465843575 * get_onLoadCompleted_3() const { return ___onLoadCompleted_3; }
	inline OnLoadCompleted_t2465843575 ** get_address_of_onLoadCompleted_3() { return &___onLoadCompleted_3; }
	inline void set_onLoadCompleted_3(OnLoadCompleted_t2465843575 * value)
	{
		___onLoadCompleted_3 = value;
		Il2CppCodeGenWriteBarrier(&___onLoadCompleted_3, value);
	}

	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(DownloadImages_t3644489024, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier(&___url_4, value);
	}

	inline static int32_t get_offset_of_pathFiles_5() { return static_cast<int32_t>(offsetof(DownloadImages_t3644489024, ___pathFiles_5)); }
	inline String_t* get_pathFiles_5() const { return ___pathFiles_5; }
	inline String_t** get_address_of_pathFiles_5() { return &___pathFiles_5; }
	inline void set_pathFiles_5(String_t* value)
	{
		___pathFiles_5 = value;
		Il2CppCodeGenWriteBarrier(&___pathFiles_5, value);
	}

	inline static int32_t get_offset_of_wwws_6() { return static_cast<int32_t>(offsetof(DownloadImages_t3644489024, ___wwws_6)); }
	inline List_1_t207839261 * get_wwws_6() const { return ___wwws_6; }
	inline List_1_t207839261 ** get_address_of_wwws_6() { return &___wwws_6; }
	inline void set_wwws_6(List_1_t207839261 * value)
	{
		___wwws_6 = value;
		Il2CppCodeGenWriteBarrier(&___wwws_6, value);
	}

	inline static int32_t get_offset_of_changeBackgroundImage_7() { return static_cast<int32_t>(offsetof(DownloadImages_t3644489024, ___changeBackgroundImage_7)); }
	inline ChangeBackgroundImage_t1317572317 * get_changeBackgroundImage_7() const { return ___changeBackgroundImage_7; }
	inline ChangeBackgroundImage_t1317572317 ** get_address_of_changeBackgroundImage_7() { return &___changeBackgroundImage_7; }
	inline void set_changeBackgroundImage_7(ChangeBackgroundImage_t1317572317 * value)
	{
		___changeBackgroundImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___changeBackgroundImage_7, value);
	}
};

struct DownloadImages_t3644489024_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.Sprite>> DownloadImages::bdImages
	Dictionary_2_t1092803867 * ___bdImages_2;

public:
	inline static int32_t get_offset_of_bdImages_2() { return static_cast<int32_t>(offsetof(DownloadImages_t3644489024_StaticFields, ___bdImages_2)); }
	inline Dictionary_2_t1092803867 * get_bdImages_2() const { return ___bdImages_2; }
	inline Dictionary_2_t1092803867 ** get_address_of_bdImages_2() { return &___bdImages_2; }
	inline void set_bdImages_2(Dictionary_2_t1092803867 * value)
	{
		___bdImages_2 = value;
		Il2CppCodeGenWriteBarrier(&___bdImages_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
