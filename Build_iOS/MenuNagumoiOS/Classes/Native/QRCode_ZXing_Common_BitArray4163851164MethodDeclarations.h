﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 ZXing.Common.BitArray::get_Size()
extern "C"  int32_t BitArray_get_Size_m1739620026 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitArray::get_SizeInBytes()
extern "C"  int32_t BitArray_get_SizeInBytes_m1961603534 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.BitArray::get_Item(System.Int32)
extern "C"  bool BitArray_get_Item_m2499408311 (BitArray_t4163851164 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::set_Item(System.Int32,System.Boolean)
extern "C"  void BitArray_set_Item_m3107170558 (BitArray_t4163851164 * __this, int32_t ___i0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::.ctor()
extern "C"  void BitArray__ctor_m905328262 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::.ctor(System.Int32)
extern "C"  void BitArray__ctor_m2272809687 (BitArray_t4163851164 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::ensureCapacity(System.Int32)
extern "C"  void BitArray_ensureCapacity_m3902920455 (BitArray_t4163851164 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitArray::numberOfTrailingZeros(System.Int32)
extern "C"  int32_t BitArray_numberOfTrailingZeros_m2786685330 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitArray::getNextSet(System.Int32)
extern "C"  int32_t BitArray_getNextSet_m1534724058 (BitArray_t4163851164 * __this, int32_t ___from0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitArray::getNextUnset(System.Int32)
extern "C"  int32_t BitArray_getNextUnset_m2519264481 (BitArray_t4163851164 * __this, int32_t ___from0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::setBulk(System.Int32,System.Int32)
extern "C"  void BitArray_setBulk_m3953255310 (BitArray_t4163851164 * __this, int32_t ___i0, int32_t ___newBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::clear()
extern "C"  void BitArray_clear_m941775569 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.BitArray::isRange(System.Int32,System.Int32,System.Boolean)
extern "C"  bool BitArray_isRange_m1428414722 (BitArray_t4163851164 * __this, int32_t ___start0, int32_t ___end1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::appendBit(System.Boolean)
extern "C"  void BitArray_appendBit_m860453454 (BitArray_t4163851164 * __this, bool ___bit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.BitArray::get_Array()
extern "C"  Int32U5BU5D_t3230847821* BitArray_get_Array_m4263571968 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::appendBits(System.Int32,System.Int32)
extern "C"  void BitArray_appendBits_m454161800 (BitArray_t4163851164 * __this, int32_t ___value0, int32_t ___numBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::appendBitArray(ZXing.Common.BitArray)
extern "C"  void BitArray_appendBitArray_m3475402057 (BitArray_t4163851164 * __this, BitArray_t4163851164 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::xor(ZXing.Common.BitArray)
extern "C"  void BitArray_xor_m156336302 (BitArray_t4163851164 * __this, BitArray_t4163851164 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::toBytes(System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void BitArray_toBytes_m19593384 (BitArray_t4163851164 * __this, int32_t ___bitOffset0, ByteU5BU5D_t4260760469* ___array1, int32_t ___offset2, int32_t ___numBytes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::reverse()
extern "C"  void BitArray_reverse_m470909286 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.BitArray::makeArray(System.Int32)
extern "C"  Int32U5BU5D_t3230847821* BitArray_makeArray_m1615902988 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.BitArray::Equals(System.Object)
extern "C"  bool BitArray_Equals_m2042827755 (BitArray_t4163851164 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitArray::GetHashCode()
extern "C"  int32_t BitArray_GetHashCode_m3981119235 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.BitArray::ToString()
extern "C"  String_t* BitArray_ToString_m3867598119 (BitArray_t4163851164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitArray::.cctor()
extern "C"  void BitArray__cctor_m1813276135 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
