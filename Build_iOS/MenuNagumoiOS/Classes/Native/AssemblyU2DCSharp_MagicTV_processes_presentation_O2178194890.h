﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.screens.GeneralScreenAbstract
struct GeneralScreenAbstract_t2842388060;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<Vuforia.ObjectTracker>
struct List_1_t1824139763;
// Vuforia.TrackerManager
struct TrackerManager_t2355365335;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Vuforia.ImageTargetBehaviour[]
struct ImageTargetBehaviourU5BU5D_t645171330;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.processes.presentation.OpenProcessComponent
struct  OpenProcessComponent_t2178194890  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// MagicTV.abstracts.screens.GeneralScreenAbstract MagicTV.processes.presentation.OpenProcessComponent::inAppGui
	GeneralScreenAbstract_t2842388060 * ___inAppGui_8;
	// UnityEngine.GameObject MagicTV.processes.presentation.OpenProcessComponent::trackingContainer
	GameObject_t3674682005 * ___trackingContainer_9;
	// System.Collections.Generic.List`1<Vuforia.ObjectTracker> MagicTV.processes.presentation.OpenProcessComponent::_imageTarget
	List_1_t1824139763 * ____imageTarget_10;
	// Vuforia.TrackerManager MagicTV.processes.presentation.OpenProcessComponent::trackerManager
	TrackerManager_t2355365335 * ___trackerManager_11;
	// System.String[] MagicTV.processes.presentation.OpenProcessComponent::foldersTracks
	StringU5BU5D_t4054002952* ___foldersTracks_12;
	// System.String MagicTV.processes.presentation.OpenProcessComponent::_trackFolder
	String_t* ____trackFolder_13;
	// System.Int32 MagicTV.processes.presentation.OpenProcessComponent::_indexLoadDataset
	int32_t ____indexLoadDataset_14;
	// System.Boolean MagicTV.processes.presentation.OpenProcessComponent::_startLoadDataset
	bool ____startLoadDataset_15;
	// System.Collections.Generic.List`1<System.String> MagicTV.processes.presentation.OpenProcessComponent::dataXmlList
	List_1_t1375417109 * ___dataXmlList_16;
	// Vuforia.ImageTargetBehaviour[] MagicTV.processes.presentation.OpenProcessComponent::DebugImgTargetsObj
	ImageTargetBehaviourU5BU5D_t645171330* ___DebugImgTargetsObj_17;
	// UnityEngine.GameObject MagicTV.processes.presentation.OpenProcessComponent::DebugRoot
	GameObject_t3674682005 * ___DebugRoot_18;

public:
	inline static int32_t get_offset_of_inAppGui_8() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___inAppGui_8)); }
	inline GeneralScreenAbstract_t2842388060 * get_inAppGui_8() const { return ___inAppGui_8; }
	inline GeneralScreenAbstract_t2842388060 ** get_address_of_inAppGui_8() { return &___inAppGui_8; }
	inline void set_inAppGui_8(GeneralScreenAbstract_t2842388060 * value)
	{
		___inAppGui_8 = value;
		Il2CppCodeGenWriteBarrier(&___inAppGui_8, value);
	}

	inline static int32_t get_offset_of_trackingContainer_9() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___trackingContainer_9)); }
	inline GameObject_t3674682005 * get_trackingContainer_9() const { return ___trackingContainer_9; }
	inline GameObject_t3674682005 ** get_address_of_trackingContainer_9() { return &___trackingContainer_9; }
	inline void set_trackingContainer_9(GameObject_t3674682005 * value)
	{
		___trackingContainer_9 = value;
		Il2CppCodeGenWriteBarrier(&___trackingContainer_9, value);
	}

	inline static int32_t get_offset_of__imageTarget_10() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ____imageTarget_10)); }
	inline List_1_t1824139763 * get__imageTarget_10() const { return ____imageTarget_10; }
	inline List_1_t1824139763 ** get_address_of__imageTarget_10() { return &____imageTarget_10; }
	inline void set__imageTarget_10(List_1_t1824139763 * value)
	{
		____imageTarget_10 = value;
		Il2CppCodeGenWriteBarrier(&____imageTarget_10, value);
	}

	inline static int32_t get_offset_of_trackerManager_11() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___trackerManager_11)); }
	inline TrackerManager_t2355365335 * get_trackerManager_11() const { return ___trackerManager_11; }
	inline TrackerManager_t2355365335 ** get_address_of_trackerManager_11() { return &___trackerManager_11; }
	inline void set_trackerManager_11(TrackerManager_t2355365335 * value)
	{
		___trackerManager_11 = value;
		Il2CppCodeGenWriteBarrier(&___trackerManager_11, value);
	}

	inline static int32_t get_offset_of_foldersTracks_12() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___foldersTracks_12)); }
	inline StringU5BU5D_t4054002952* get_foldersTracks_12() const { return ___foldersTracks_12; }
	inline StringU5BU5D_t4054002952** get_address_of_foldersTracks_12() { return &___foldersTracks_12; }
	inline void set_foldersTracks_12(StringU5BU5D_t4054002952* value)
	{
		___foldersTracks_12 = value;
		Il2CppCodeGenWriteBarrier(&___foldersTracks_12, value);
	}

	inline static int32_t get_offset_of__trackFolder_13() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ____trackFolder_13)); }
	inline String_t* get__trackFolder_13() const { return ____trackFolder_13; }
	inline String_t** get_address_of__trackFolder_13() { return &____trackFolder_13; }
	inline void set__trackFolder_13(String_t* value)
	{
		____trackFolder_13 = value;
		Il2CppCodeGenWriteBarrier(&____trackFolder_13, value);
	}

	inline static int32_t get_offset_of__indexLoadDataset_14() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ____indexLoadDataset_14)); }
	inline int32_t get__indexLoadDataset_14() const { return ____indexLoadDataset_14; }
	inline int32_t* get_address_of__indexLoadDataset_14() { return &____indexLoadDataset_14; }
	inline void set__indexLoadDataset_14(int32_t value)
	{
		____indexLoadDataset_14 = value;
	}

	inline static int32_t get_offset_of__startLoadDataset_15() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ____startLoadDataset_15)); }
	inline bool get__startLoadDataset_15() const { return ____startLoadDataset_15; }
	inline bool* get_address_of__startLoadDataset_15() { return &____startLoadDataset_15; }
	inline void set__startLoadDataset_15(bool value)
	{
		____startLoadDataset_15 = value;
	}

	inline static int32_t get_offset_of_dataXmlList_16() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___dataXmlList_16)); }
	inline List_1_t1375417109 * get_dataXmlList_16() const { return ___dataXmlList_16; }
	inline List_1_t1375417109 ** get_address_of_dataXmlList_16() { return &___dataXmlList_16; }
	inline void set_dataXmlList_16(List_1_t1375417109 * value)
	{
		___dataXmlList_16 = value;
		Il2CppCodeGenWriteBarrier(&___dataXmlList_16, value);
	}

	inline static int32_t get_offset_of_DebugImgTargetsObj_17() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___DebugImgTargetsObj_17)); }
	inline ImageTargetBehaviourU5BU5D_t645171330* get_DebugImgTargetsObj_17() const { return ___DebugImgTargetsObj_17; }
	inline ImageTargetBehaviourU5BU5D_t645171330** get_address_of_DebugImgTargetsObj_17() { return &___DebugImgTargetsObj_17; }
	inline void set_DebugImgTargetsObj_17(ImageTargetBehaviourU5BU5D_t645171330* value)
	{
		___DebugImgTargetsObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___DebugImgTargetsObj_17, value);
	}

	inline static int32_t get_offset_of_DebugRoot_18() { return static_cast<int32_t>(offsetof(OpenProcessComponent_t2178194890, ___DebugRoot_18)); }
	inline GameObject_t3674682005 * get_DebugRoot_18() const { return ___DebugRoot_18; }
	inline GameObject_t3674682005 ** get_address_of_DebugRoot_18() { return &___DebugRoot_18; }
	inline void set_DebugRoot_18(GameObject_t3674682005 * value)
	{
		___DebugRoot_18 = value;
		Il2CppCodeGenWriteBarrier(&___DebugRoot_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
