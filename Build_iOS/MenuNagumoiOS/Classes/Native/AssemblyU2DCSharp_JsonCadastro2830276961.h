﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonCadastro
struct  JsonCadastro_t2830276961  : public Il2CppObject
{
public:
	// System.String JsonCadastro::acao
	String_t* ___acao_0;
	// System.String JsonCadastro::nome
	String_t* ___nome_1;
	// System.String JsonCadastro::sobrenome
	String_t* ___sobrenome_2;
	// System.String JsonCadastro::cpf
	String_t* ___cpf_3;
	// System.String JsonCadastro::email
	String_t* ___email_4;
	// System.String JsonCadastro::telefone
	String_t* ___telefone_5;
	// System.Int32 JsonCadastro::idade
	int32_t ___idade_6;
	// System.Int32 JsonCadastro::sexo
	int32_t ___sexo_7;
	// System.String JsonCadastro::senha
	String_t* ___senha_8;
	// System.String JsonCadastro::novaSenha
	String_t* ___novaSenha_9;
	// System.String JsonCadastro::device_id
	String_t* ___device_id_10;
	// System.String JsonCadastro::mac_address
	String_t* ___mac_address_11;
	// System.String JsonCadastro::dt_nasc
	String_t* ___dt_nasc_12;
	// System.String JsonCadastro::cep
	String_t* ___cep_13;
	// System.String JsonCadastro::rua
	String_t* ___rua_14;
	// System.String JsonCadastro::bairro
	String_t* ___bairro_15;
	// System.Int32 JsonCadastro::numero
	int32_t ___numero_16;
	// System.String JsonCadastro::complemento
	String_t* ___complemento_17;
	// System.String JsonCadastro::estado
	String_t* ___estado_18;
	// System.String JsonCadastro::cidade
	String_t* ___cidade_19;

public:
	inline static int32_t get_offset_of_acao_0() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___acao_0)); }
	inline String_t* get_acao_0() const { return ___acao_0; }
	inline String_t** get_address_of_acao_0() { return &___acao_0; }
	inline void set_acao_0(String_t* value)
	{
		___acao_0 = value;
		Il2CppCodeGenWriteBarrier(&___acao_0, value);
	}

	inline static int32_t get_offset_of_nome_1() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___nome_1)); }
	inline String_t* get_nome_1() const { return ___nome_1; }
	inline String_t** get_address_of_nome_1() { return &___nome_1; }
	inline void set_nome_1(String_t* value)
	{
		___nome_1 = value;
		Il2CppCodeGenWriteBarrier(&___nome_1, value);
	}

	inline static int32_t get_offset_of_sobrenome_2() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___sobrenome_2)); }
	inline String_t* get_sobrenome_2() const { return ___sobrenome_2; }
	inline String_t** get_address_of_sobrenome_2() { return &___sobrenome_2; }
	inline void set_sobrenome_2(String_t* value)
	{
		___sobrenome_2 = value;
		Il2CppCodeGenWriteBarrier(&___sobrenome_2, value);
	}

	inline static int32_t get_offset_of_cpf_3() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___cpf_3)); }
	inline String_t* get_cpf_3() const { return ___cpf_3; }
	inline String_t** get_address_of_cpf_3() { return &___cpf_3; }
	inline void set_cpf_3(String_t* value)
	{
		___cpf_3 = value;
		Il2CppCodeGenWriteBarrier(&___cpf_3, value);
	}

	inline static int32_t get_offset_of_email_4() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___email_4)); }
	inline String_t* get_email_4() const { return ___email_4; }
	inline String_t** get_address_of_email_4() { return &___email_4; }
	inline void set_email_4(String_t* value)
	{
		___email_4 = value;
		Il2CppCodeGenWriteBarrier(&___email_4, value);
	}

	inline static int32_t get_offset_of_telefone_5() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___telefone_5)); }
	inline String_t* get_telefone_5() const { return ___telefone_5; }
	inline String_t** get_address_of_telefone_5() { return &___telefone_5; }
	inline void set_telefone_5(String_t* value)
	{
		___telefone_5 = value;
		Il2CppCodeGenWriteBarrier(&___telefone_5, value);
	}

	inline static int32_t get_offset_of_idade_6() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___idade_6)); }
	inline int32_t get_idade_6() const { return ___idade_6; }
	inline int32_t* get_address_of_idade_6() { return &___idade_6; }
	inline void set_idade_6(int32_t value)
	{
		___idade_6 = value;
	}

	inline static int32_t get_offset_of_sexo_7() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___sexo_7)); }
	inline int32_t get_sexo_7() const { return ___sexo_7; }
	inline int32_t* get_address_of_sexo_7() { return &___sexo_7; }
	inline void set_sexo_7(int32_t value)
	{
		___sexo_7 = value;
	}

	inline static int32_t get_offset_of_senha_8() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___senha_8)); }
	inline String_t* get_senha_8() const { return ___senha_8; }
	inline String_t** get_address_of_senha_8() { return &___senha_8; }
	inline void set_senha_8(String_t* value)
	{
		___senha_8 = value;
		Il2CppCodeGenWriteBarrier(&___senha_8, value);
	}

	inline static int32_t get_offset_of_novaSenha_9() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___novaSenha_9)); }
	inline String_t* get_novaSenha_9() const { return ___novaSenha_9; }
	inline String_t** get_address_of_novaSenha_9() { return &___novaSenha_9; }
	inline void set_novaSenha_9(String_t* value)
	{
		___novaSenha_9 = value;
		Il2CppCodeGenWriteBarrier(&___novaSenha_9, value);
	}

	inline static int32_t get_offset_of_device_id_10() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___device_id_10)); }
	inline String_t* get_device_id_10() const { return ___device_id_10; }
	inline String_t** get_address_of_device_id_10() { return &___device_id_10; }
	inline void set_device_id_10(String_t* value)
	{
		___device_id_10 = value;
		Il2CppCodeGenWriteBarrier(&___device_id_10, value);
	}

	inline static int32_t get_offset_of_mac_address_11() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___mac_address_11)); }
	inline String_t* get_mac_address_11() const { return ___mac_address_11; }
	inline String_t** get_address_of_mac_address_11() { return &___mac_address_11; }
	inline void set_mac_address_11(String_t* value)
	{
		___mac_address_11 = value;
		Il2CppCodeGenWriteBarrier(&___mac_address_11, value);
	}

	inline static int32_t get_offset_of_dt_nasc_12() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___dt_nasc_12)); }
	inline String_t* get_dt_nasc_12() const { return ___dt_nasc_12; }
	inline String_t** get_address_of_dt_nasc_12() { return &___dt_nasc_12; }
	inline void set_dt_nasc_12(String_t* value)
	{
		___dt_nasc_12 = value;
		Il2CppCodeGenWriteBarrier(&___dt_nasc_12, value);
	}

	inline static int32_t get_offset_of_cep_13() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___cep_13)); }
	inline String_t* get_cep_13() const { return ___cep_13; }
	inline String_t** get_address_of_cep_13() { return &___cep_13; }
	inline void set_cep_13(String_t* value)
	{
		___cep_13 = value;
		Il2CppCodeGenWriteBarrier(&___cep_13, value);
	}

	inline static int32_t get_offset_of_rua_14() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___rua_14)); }
	inline String_t* get_rua_14() const { return ___rua_14; }
	inline String_t** get_address_of_rua_14() { return &___rua_14; }
	inline void set_rua_14(String_t* value)
	{
		___rua_14 = value;
		Il2CppCodeGenWriteBarrier(&___rua_14, value);
	}

	inline static int32_t get_offset_of_bairro_15() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___bairro_15)); }
	inline String_t* get_bairro_15() const { return ___bairro_15; }
	inline String_t** get_address_of_bairro_15() { return &___bairro_15; }
	inline void set_bairro_15(String_t* value)
	{
		___bairro_15 = value;
		Il2CppCodeGenWriteBarrier(&___bairro_15, value);
	}

	inline static int32_t get_offset_of_numero_16() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___numero_16)); }
	inline int32_t get_numero_16() const { return ___numero_16; }
	inline int32_t* get_address_of_numero_16() { return &___numero_16; }
	inline void set_numero_16(int32_t value)
	{
		___numero_16 = value;
	}

	inline static int32_t get_offset_of_complemento_17() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___complemento_17)); }
	inline String_t* get_complemento_17() const { return ___complemento_17; }
	inline String_t** get_address_of_complemento_17() { return &___complemento_17; }
	inline void set_complemento_17(String_t* value)
	{
		___complemento_17 = value;
		Il2CppCodeGenWriteBarrier(&___complemento_17, value);
	}

	inline static int32_t get_offset_of_estado_18() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___estado_18)); }
	inline String_t* get_estado_18() const { return ___estado_18; }
	inline String_t** get_address_of_estado_18() { return &___estado_18; }
	inline void set_estado_18(String_t* value)
	{
		___estado_18 = value;
		Il2CppCodeGenWriteBarrier(&___estado_18, value);
	}

	inline static int32_t get_offset_of_cidade_19() { return static_cast<int32_t>(offsetof(JsonCadastro_t2830276961, ___cidade_19)); }
	inline String_t* get_cidade_19() const { return ___cidade_19; }
	inline String_t** get_address_of_cidade_19() { return &___cidade_19; }
	inline void set_cidade_19(String_t* value)
	{
		___cidade_19 = value;
		Il2CppCodeGenWriteBarrier(&___cidade_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
