﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.GUILoaderAbstract
struct GUILoaderAbstract_t2603370330;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.GUILoaderAbstract::.ctor()
extern "C"  void GUILoaderAbstract__ctor_m4163605335 (GUILoaderAbstract_t2603370330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
