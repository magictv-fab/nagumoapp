﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatorRotationScript
struct AnimatorRotationScript_t2214770896;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimatorRotationScript::.ctor()
extern "C"  void AnimatorRotationScript__ctor_m3258213835 (AnimatorRotationScript_t2214770896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorRotationScript::Start()
extern "C"  void AnimatorRotationScript_Start_m2205351627 (AnimatorRotationScript_t2214770896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorRotationScript::Update()
extern "C"  void AnimatorRotationScript_Update_m3947243170 (AnimatorRotationScript_t2214770896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
