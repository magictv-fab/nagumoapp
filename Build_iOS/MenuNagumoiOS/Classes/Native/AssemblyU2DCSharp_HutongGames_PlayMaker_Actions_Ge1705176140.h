﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetKeyUp
struct  GetKeyUp_t1705176140  : public FsmStateAction_t2366529033
{
public:
	// UnityEngine.KeyCode HutongGames.PlayMaker.Actions.GetKeyUp::key
	int32_t ___key_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetKeyUp::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetKeyUp::storeResult
	FsmBool_t1075959796 * ___storeResult_11;

public:
	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(GetKeyUp_t1705176140, ___key_9)); }
	inline int32_t get_key_9() const { return ___key_9; }
	inline int32_t* get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(int32_t value)
	{
		___key_9 = value;
	}

	inline static int32_t get_offset_of_sendEvent_10() { return static_cast<int32_t>(offsetof(GetKeyUp_t1705176140, ___sendEvent_10)); }
	inline FsmEvent_t2133468028 * get_sendEvent_10() const { return ___sendEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_10() { return &___sendEvent_10; }
	inline void set_sendEvent_10(FsmEvent_t2133468028 * value)
	{
		___sendEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(GetKeyUp_t1705176140, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
