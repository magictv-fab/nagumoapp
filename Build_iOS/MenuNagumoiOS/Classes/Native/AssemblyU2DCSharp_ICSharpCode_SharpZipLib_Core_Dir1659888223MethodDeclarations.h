﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler
struct DirectoryFailureHandler_t1659888223;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs
struct ScanFailureEventArgs_t2731106168;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca2731106168.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DirectoryFailureHandler__ctor_m3498760630 (DirectoryFailureHandler_t1659888223 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs)
extern "C"  void DirectoryFailureHandler_Invoke_m3876501960 (DirectoryFailureHandler_t1659888223 * __this, Il2CppObject * ___sender0, ScanFailureEventArgs_t2731106168 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DirectoryFailureHandler_BeginInvoke_m962465313 (DirectoryFailureHandler_t1659888223 * __this, Il2CppObject * ___sender0, ScanFailureEventArgs_t2731106168 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DirectoryFailureHandler_EndInvoke_m260806342 (DirectoryFailureHandler_t1659888223 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
