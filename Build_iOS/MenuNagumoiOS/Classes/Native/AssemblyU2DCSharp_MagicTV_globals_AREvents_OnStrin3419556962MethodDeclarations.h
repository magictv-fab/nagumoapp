﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.AREvents/OnStringEvent
struct OnStringEvent_t3419556962;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.AREvents/OnStringEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void OnStringEvent__ctor_m1882739209 (OnStringEvent_t3419556962 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents/OnStringEvent::Invoke(System.String)
extern "C"  void OnStringEvent_Invoke_m2153211967 (OnStringEvent_t3419556962 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.AREvents/OnStringEvent::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnStringEvent_BeginInvoke_m3845196996 (OnStringEvent_t3419556962 * __this, String_t* ___s0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents/OnStringEvent::EndInvoke(System.IAsyncResult)
extern "C"  void OnStringEvent_EndInvoke_m3650105497 (OnStringEvent_t3419556962 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
