﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugMedianVector3
struct DebugMedianVector3_t3206408051;

#include "codegen/il2cpp-codegen.h"

// System.Void DebugMedianVector3::.ctor()
extern "C"  void DebugMedianVector3__ctor_m3417950856 (DebugMedianVector3_t3206408051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugMedianVector3::Start()
extern "C"  void DebugMedianVector3_Start_m2365088648 (DebugMedianVector3_t3206408051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugMedianVector3::LateUpdate()
extern "C"  void DebugMedianVector3_LateUpdate_m3015496907 (DebugMedianVector3_t3206408051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
