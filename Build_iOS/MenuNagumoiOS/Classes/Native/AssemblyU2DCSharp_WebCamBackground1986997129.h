﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamBackground
struct  WebCamBackground_t1986997129  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.WebCamTexture WebCamBackground::camTexture
	WebCamTexture_t1290350902 * ___camTexture_2;

public:
	inline static int32_t get_offset_of_camTexture_2() { return static_cast<int32_t>(offsetof(WebCamBackground_t1986997129, ___camTexture_2)); }
	inline WebCamTexture_t1290350902 * get_camTexture_2() const { return ___camTexture_2; }
	inline WebCamTexture_t1290350902 ** get_address_of_camTexture_2() { return &___camTexture_2; }
	inline void set_camTexture_2(WebCamTexture_t1290350902 * value)
	{
		___camTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___camTexture_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
