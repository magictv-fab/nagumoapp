﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.GenericParameterEventDispatcher
struct GenericParameterEventDispatcher_t3811317845;
// ARM.events.GenericParameterEventDispatcher/GenericEvent
struct GenericEvent_t3373847142;
// ARM.events.GenericParameterVO
struct GenericParameterVO_t1159220759;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterEvent3373847142.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterVO1159220759.h"

// System.Void ARM.events.GenericParameterEventDispatcher::.ctor()
extern "C"  void GenericParameterEventDispatcher__ctor_m1327786031 (GenericParameterEventDispatcher_t3811317845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.GenericParameterEventDispatcher::AddOnGenericEventHandler(ARM.events.GenericParameterEventDispatcher/GenericEvent)
extern "C"  void GenericParameterEventDispatcher_AddOnGenericEventHandler_m570501520 (GenericParameterEventDispatcher_t3811317845 * __this, GenericEvent_t3373847142 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.GenericParameterEventDispatcher::RemoveOnGenericEventHandler(ARM.events.GenericParameterEventDispatcher/GenericEvent)
extern "C"  void GenericParameterEventDispatcher_RemoveOnGenericEventHandler_m172491291 (GenericParameterEventDispatcher_t3811317845 * __this, GenericEvent_t3373847142 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.GenericParameterEventDispatcher::Raise(ARM.events.GenericParameterVO)
extern "C"  void GenericParameterEventDispatcher_Raise_m2097010219 (GenericParameterEventDispatcher_t3811317845 * __this, GenericParameterVO_t1159220759 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
