﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/OnLogoutEmailFailure
struct OnLogoutEmailFailure_t4146960440;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/OnLogoutEmailFailure::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLogoutEmailFailure__ctor_m1697403615 (OnLogoutEmailFailure_t4146960440 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnLogoutEmailFailure::Invoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void OnLogoutEmailFailure_Invoke_m1234067032 (OnLogoutEmailFailure_t4146960440 * __this, Dictionary_2_t696267445 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/OnLogoutEmailFailure::BeginInvoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLogoutEmailFailure_BeginInvoke_m1524834293 (OnLogoutEmailFailure_t4146960440 * __this, Dictionary_2_t696267445 * ___error0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnLogoutEmailFailure::EndInvoke(System.IAsyncResult)
extern "C"  void OnLogoutEmailFailure_EndInvoke_m3844213871 (OnLogoutEmailFailure_t4146960440 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
