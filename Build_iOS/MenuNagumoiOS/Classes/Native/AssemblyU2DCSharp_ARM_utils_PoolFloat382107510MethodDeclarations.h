﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.PoolFloat
struct PoolFloat_t382107510;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.PoolFloat::.ctor(System.UInt32)
extern "C"  void PoolFloat__ctor_m2010213248 (PoolFloat_t382107510 * __this, uint32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.PoolFloat::addItem(System.Single)
extern "C"  void PoolFloat_addItem_m2441489311 (PoolFloat_t382107510 * __this, float ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.PoolFloat::getCurrentSize()
extern "C"  int32_t PoolFloat_getCurrentSize_m1919522944 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] ARM.utils.PoolFloat::getItens()
extern "C"  SingleU5BU5D_t2316563989* PoolFloat_getItens_m1519938473 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.PoolFloat::getMedian()
extern "C"  float PoolFloat_getMedian_m52820548 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.PoolFloat::getMean()
extern "C"  float PoolFloat_getMean_m2274826367 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.PoolFloat::getMeanAngle()
extern "C"  float PoolFloat_getMeanAngle_m300390454 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.PoolFloat::reset()
extern "C"  void PoolFloat_reset_m3213112967 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.PoolFloat::isFull()
extern "C"  bool PoolFloat_isFull_m564757071 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.PoolFloat::getLastPosition()
extern "C"  float PoolFloat_getLastPosition_m1133868825 (PoolFloat_t382107510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] ARM.utils.PoolFloat::getLastsPosition(System.UInt32)
extern "C"  SingleU5BU5D_t2316563989* PoolFloat_getLastsPosition_m458359050 (PoolFloat_t382107510 * __this, uint32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
