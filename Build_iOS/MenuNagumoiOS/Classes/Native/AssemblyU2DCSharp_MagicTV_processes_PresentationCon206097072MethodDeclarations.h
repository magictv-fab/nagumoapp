﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.processes.PresentationController
struct PresentationController_t206097072;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.processes.PresentationController::.ctor()
extern "C"  void PresentationController__ctor_m1099771893 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::.cctor()
extern "C"  void PresentationController__cctor_m3546061400 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 MagicTV.processes.PresentationController::getTotalTime()
extern "C"  int64_t PresentationController_getTotalTime_m3494601689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::Start()
extern "C"  void PresentationController_Start_m46909685 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::OnTrackingFound(System.String)
extern "C"  void PresentationController_OnTrackingFound_m4285968995 (PresentationController_t206097072 * __this, String_t* ___trackingName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::OnTrackingLost(System.String)
extern "C"  void PresentationController_OnTrackingLost_m2384901369 (PresentationController_t206097072 * __this, String_t* ___trackingName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::prepare()
extern "C"  void PresentationController_prepare_m1245814778 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::ARChanged(System.Boolean)
extern "C"  void PresentationController_ARChanged_m725489581 (PresentationController_t206097072 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::Reset()
extern "C"  void PresentationController_Reset_m3041172130 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::Play()
extern "C"  void PresentationController_Play_m1709357283 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::step1()
extern "C"  void PresentationController_step1_m2680793816 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::StopLastPresentation()
extern "C"  void PresentationController_StopLastPresentation_m1962235681 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::Stop()
extern "C"  void PresentationController_Stop_m1803041329 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.PresentationController::Pause()
extern "C"  void PresentationController_Pause_m1153897865 (PresentationController_t206097072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
