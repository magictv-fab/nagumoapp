﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertMaterialToObject
struct ConvertMaterialToObject_t616279490;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::.ctor()
extern "C"  void ConvertMaterialToObject__ctor_m2208398260 (ConvertMaterialToObject_t616279490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::Reset()
extern "C"  void ConvertMaterialToObject_Reset_m4149798497 (ConvertMaterialToObject_t616279490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::OnEnter()
extern "C"  void ConvertMaterialToObject_OnEnter_m4243579531 (ConvertMaterialToObject_t616279490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::OnUpdate()
extern "C"  void ConvertMaterialToObject_OnUpdate_m1835505944 (ConvertMaterialToObject_t616279490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::DoConvertMaterialToObject()
extern "C"  void ConvertMaterialToObject_DoConvertMaterialToObject_m2413529787 (ConvertMaterialToObject_t616279490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
