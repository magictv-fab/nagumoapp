﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/PageFinishedDelegate
struct PageFinishedDelegate_t1325724172;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/PageFinishedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void PageFinishedDelegate__ctor_m3478238691 (PageFinishedDelegate_t1325724172 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/PageFinishedDelegate::Invoke(UniWebView,System.Int32,System.String)
extern "C"  void PageFinishedDelegate_Invoke_m2703381855 (PageFinishedDelegate_t1325724172 * __this, UniWebView_t424341801 * ___webView0, int32_t ___statusCode1, String_t* ___url2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/PageFinishedDelegate::BeginInvoke(UniWebView,System.Int32,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PageFinishedDelegate_BeginInvoke_m462809528 (PageFinishedDelegate_t1325724172 * __this, UniWebView_t424341801 * ___webView0, int32_t ___statusCode1, String_t* ___url2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/PageFinishedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void PageFinishedDelegate_EndInvoke_m1884918131 (PageFinishedDelegate_t1325724172 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
