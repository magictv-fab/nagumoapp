﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11
struct U3CILoadOfertasU3Ec__Iterator11_t1647394742;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11::.ctor()
extern "C"  void U3CILoadOfertasU3Ec__Iterator11__ctor_m2968813685 (U3CILoadOfertasU3Ec__Iterator11_t1647394742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2906615421 (U3CILoadOfertasU3Ec__Iterator11_t1647394742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3566210065 (U3CILoadOfertasU3Ec__Iterator11_t1647394742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11::MoveNext()
extern "C"  bool U3CILoadOfertasU3Ec__Iterator11_MoveNext_m1373663071 (U3CILoadOfertasU3Ec__Iterator11_t1647394742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11::Dispose()
extern "C"  void U3CILoadOfertasU3Ec__Iterator11_Dispose_m1069949810 (U3CILoadOfertasU3Ec__Iterator11_t1647394742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<ILoadOfertas>c__Iterator11::Reset()
extern "C"  void U3CILoadOfertasU3Ec__Iterator11_Reset_m615246626 (U3CILoadOfertasU3Ec__Iterator11_t1647394742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
