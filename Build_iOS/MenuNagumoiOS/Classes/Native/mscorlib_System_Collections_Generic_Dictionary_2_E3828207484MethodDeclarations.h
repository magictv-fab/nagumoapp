﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3828207484.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3444471362_gshared (Enumerator_t3828207484 * __this, Dictionary_2_t2510884092 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3444471362(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3828207484 *, Dictionary_2_t2510884092 *, const MethodInfo*))Enumerator__ctor_m3444471362_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2371533151_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2371533151(__this, method) ((  Il2CppObject * (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2371533151_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3450565939_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3450565939(__this, method) ((  void (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3450565939_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3609446076_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3609446076(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3609446076_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1907045115_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1907045115(__this, method) ((  Il2CppObject * (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1907045115_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4062723789_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4062723789(__this, method) ((  Il2CppObject * (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4062723789_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3044872223_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3044872223(__this, method) ((  bool (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_MoveNext_m3044872223_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2409664798  Enumerator_get_Current_m4212335665_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4212335665(__this, method) ((  KeyValuePair_2_t2409664798  (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_get_Current_m4212335665_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3803229484_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3803229484(__this, method) ((  int32_t (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_get_CurrentKey_m3803229484_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2354261840_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2354261840(__this, method) ((  Il2CppObject * (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_get_CurrentValue_m2354261840_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2959396244_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2959396244(__this, method) ((  void (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_Reset_m2959396244_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4194606749_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4194606749(__this, method) ((  void (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_VerifyState_m4194606749_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m787666629_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m787666629(__this, method) ((  void (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_VerifyCurrent_m787666629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3234869604_gshared (Enumerator_t3828207484 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3234869604(__this, method) ((  void (*) (Enumerator_t3828207484 *, const MethodInfo*))Enumerator_Dispose_m3234869604_gshared)(__this, method)
