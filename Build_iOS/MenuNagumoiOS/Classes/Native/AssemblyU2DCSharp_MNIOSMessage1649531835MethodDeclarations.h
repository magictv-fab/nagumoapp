﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNIOSMessage
struct MNIOSMessage_t1649531835;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNIOSMessage::.ctor()
extern "C"  void MNIOSMessage__ctor_m1979849024 (MNIOSMessage_t1649531835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSMessage MNIOSMessage::Create(System.String,System.String)
extern "C"  MNIOSMessage_t1649531835 * MNIOSMessage_Create_m1281873116 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSMessage MNIOSMessage::Create(System.String,System.String,System.String)
extern "C"  MNIOSMessage_t1649531835 * MNIOSMessage_Create_m153963544 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSMessage::init()
extern "C"  void MNIOSMessage_init_m2594103540 (MNIOSMessage_t1649531835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSMessage::onPopUpCallBack(System.String)
extern "C"  void MNIOSMessage_onPopUpCallBack_m3048287634 (MNIOSMessage_t1649531835 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
