﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugsConfigGui
struct DebugsConfigGui_t3114428889;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void DebugsConfigGui::.ctor()
extern "C"  void DebugsConfigGui__ctor_m7102898 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::Start()
extern "C"  void DebugsConfigGui_Start_m3249207986 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::trackIn(System.String)
extern "C"  void DebugsConfigGui_trackIn_m1182953474 (DebugsConfigGui_t3114428889 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::trackOut(System.String)
extern "C"  void DebugsConfigGui_trackOut_m73599789 (DebugsConfigGui_t3114428889 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::OnGUI()
extern "C"  void DebugsConfigGui_OnGUI_m3797468844 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::showRootWindow()
extern "C"  void DebugsConfigGui_showRootWindow_m302888577 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::showConfigurablesDebugButton()
extern "C"  void DebugsConfigGui_showConfigurablesDebugButton_m2351202042 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::showOnOffDebugButton()
extern "C"  void DebugsConfigGui_showOnOffDebugButton_m118451716 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DebugsConfigGui::getUniqueName()
extern "C"  String_t* DebugsConfigGui_getUniqueName_m2990369669 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::turnOn()
extern "C"  void DebugsConfigGui_turnOn_m693868846 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::turnOff()
extern "C"  void DebugsConfigGui_turnOff_m34919010 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::ResetOnOff()
extern "C"  void DebugsConfigGui_ResetOnOff_m4057113427 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject DebugsConfigGui::getGameObjectReference()
extern "C"  GameObject_t3674682005 * DebugsConfigGui_getGameObjectReference_m2882839585 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::saveHistory(System.String)
extern "C"  void DebugsConfigGui_saveHistory_m1929658395 (DebugsConfigGui_t3114428889 * __this, String_t* ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugsConfigGui::HistoryBack()
extern "C"  void DebugsConfigGui_HistoryBack_m3831064683 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DebugsConfigGui::isActive()
extern "C"  bool DebugsConfigGui_isActive_m95346254 (DebugsConfigGui_t3114428889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
