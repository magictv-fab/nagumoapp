﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBEasyWebCam.CallBack.EasyWebCamStartedDelegate
struct EasyWebCamStartedDelegate_t3886828347;
// TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate
struct EasyWebCamUpdateDelegate_t1731309161;
// TBEasyWebCam.CallBack.EasyWebCamStopedDelegate
struct EasyWebCamStopedDelegate_t2264447425;

#include "codegen/il2cpp-codegen.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStar3886828347.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamUpda1731309161.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStop2264447425.h"

// System.Void TBEasyWebCam.EasyWebCamBase::add_onEasyWebCamStart(TBEasyWebCam.CallBack.EasyWebCamStartedDelegate)
extern "C"  void EasyWebCamBase_add_onEasyWebCamStart_m3076024489 (Il2CppObject * __this /* static, unused */, EasyWebCamStartedDelegate_t3886828347 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCamBase::remove_onEasyWebCamStart(TBEasyWebCam.CallBack.EasyWebCamStartedDelegate)
extern "C"  void EasyWebCamBase_remove_onEasyWebCamStart_m1528401280 (Il2CppObject * __this /* static, unused */, EasyWebCamStartedDelegate_t3886828347 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCamBase::add_OnEasyWebCamUpdate(TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate)
extern "C"  void EasyWebCamBase_add_OnEasyWebCamUpdate_m3667554550 (Il2CppObject * __this /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCamBase::remove_OnEasyWebCamUpdate(TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate)
extern "C"  void EasyWebCamBase_remove_OnEasyWebCamUpdate_m2119931341 (Il2CppObject * __this /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCamBase::add_OnEasyWebCamStoped(TBEasyWebCam.CallBack.EasyWebCamStopedDelegate)
extern "C"  void EasyWebCamBase_add_OnEasyWebCamStoped_m2036573174 (Il2CppObject * __this /* static, unused */, EasyWebCamStopedDelegate_t2264447425 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCamBase::remove_OnEasyWebCamStoped(TBEasyWebCam.CallBack.EasyWebCamStopedDelegate)
extern "C"  void EasyWebCamBase_remove_OnEasyWebCamStoped_m488949965 (Il2CppObject * __this /* static, unused */, EasyWebCamStopedDelegate_t2264447425 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCamBase::.cctor()
extern "C"  void EasyWebCamBase__cctor_m691467341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
