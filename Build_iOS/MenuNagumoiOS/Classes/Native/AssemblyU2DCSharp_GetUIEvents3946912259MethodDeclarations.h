﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetUIEvents
struct GetUIEvents_t3946912259;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void GetUIEvents::.ctor()
extern "C"  void GetUIEvents__ctor_m3546543304 (GetUIEvents_t3946912259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetUIEvents::Start()
extern "C"  void GetUIEvents_Start_m2493681096 (GetUIEvents_t3946912259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetUIEvents::Update()
extern "C"  void GetUIEvents_Update_m554821 (GetUIEvents_t3946912259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetUIEvents::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void GetUIEvents_OnPointerClick_m2598452280 (GetUIEvents_t3946912259 * __this, PointerEventData_t1848751023 * ___tst0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
