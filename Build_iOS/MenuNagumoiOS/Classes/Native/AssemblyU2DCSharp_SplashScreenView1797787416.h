﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2376705138;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashScreenView
struct  SplashScreenView_t1797787416  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D SplashScreenView::mAndroidPotrait
	Texture2D_t3884108195 * ___mAndroidPotrait_0;
	// UnityEngine.Texture2D SplashScreenView::mWindowsPlayModeTexture
	Texture2D_t3884108195 * ___mWindowsPlayModeTexture_1;
	// UnityEngine.GUIStyle SplashScreenView::m_SplashStyle
	GUIStyle_t2990928826 * ___m_SplashStyle_2;
	// UnityEngine.Texture2D[] SplashScreenView::iPhoneImages
	Texture2DU5BU5D_t2376705138* ___iPhoneImages_3;

public:
	inline static int32_t get_offset_of_mAndroidPotrait_0() { return static_cast<int32_t>(offsetof(SplashScreenView_t1797787416, ___mAndroidPotrait_0)); }
	inline Texture2D_t3884108195 * get_mAndroidPotrait_0() const { return ___mAndroidPotrait_0; }
	inline Texture2D_t3884108195 ** get_address_of_mAndroidPotrait_0() { return &___mAndroidPotrait_0; }
	inline void set_mAndroidPotrait_0(Texture2D_t3884108195 * value)
	{
		___mAndroidPotrait_0 = value;
		Il2CppCodeGenWriteBarrier(&___mAndroidPotrait_0, value);
	}

	inline static int32_t get_offset_of_mWindowsPlayModeTexture_1() { return static_cast<int32_t>(offsetof(SplashScreenView_t1797787416, ___mWindowsPlayModeTexture_1)); }
	inline Texture2D_t3884108195 * get_mWindowsPlayModeTexture_1() const { return ___mWindowsPlayModeTexture_1; }
	inline Texture2D_t3884108195 ** get_address_of_mWindowsPlayModeTexture_1() { return &___mWindowsPlayModeTexture_1; }
	inline void set_mWindowsPlayModeTexture_1(Texture2D_t3884108195 * value)
	{
		___mWindowsPlayModeTexture_1 = value;
		Il2CppCodeGenWriteBarrier(&___mWindowsPlayModeTexture_1, value);
	}

	inline static int32_t get_offset_of_m_SplashStyle_2() { return static_cast<int32_t>(offsetof(SplashScreenView_t1797787416, ___m_SplashStyle_2)); }
	inline GUIStyle_t2990928826 * get_m_SplashStyle_2() const { return ___m_SplashStyle_2; }
	inline GUIStyle_t2990928826 ** get_address_of_m_SplashStyle_2() { return &___m_SplashStyle_2; }
	inline void set_m_SplashStyle_2(GUIStyle_t2990928826 * value)
	{
		___m_SplashStyle_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_SplashStyle_2, value);
	}

	inline static int32_t get_offset_of_iPhoneImages_3() { return static_cast<int32_t>(offsetof(SplashScreenView_t1797787416, ___iPhoneImages_3)); }
	inline Texture2DU5BU5D_t2376705138* get_iPhoneImages_3() const { return ___iPhoneImages_3; }
	inline Texture2DU5BU5D_t2376705138** get_address_of_iPhoneImages_3() { return &___iPhoneImages_3; }
	inline void set_iPhoneImages_3(Texture2DU5BU5D_t2376705138* value)
	{
		___iPhoneImages_3 = value;
		Il2CppCodeGenWriteBarrier(&___iPhoneImages_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
