﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenPick
struct  ScreenPick_t2360912917  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick::screenVector
	FsmVector3_t533912882 * ___screenVector_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::screenX
	FsmFloat_t2134102846 * ___screenX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::screenY
	FsmFloat_t2134102846 * ___screenY_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick::normalized
	FsmBool_t1075959796 * ___normalized_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::rayDistance
	FsmFloat_t2134102846 * ___rayDistance_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick::storeDidPickObject
	FsmBool_t1075959796 * ___storeDidPickObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ScreenPick::storeGameObject
	FsmGameObject_t1697147867 * ___storeGameObject_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick::storePoint
	FsmVector3_t533912882 * ___storePoint_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick::storeNormal
	FsmVector3_t533912882 * ___storeNormal_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::storeDistance
	FsmFloat_t2134102846 * ___storeDistance_18;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.ScreenPick::layerMask
	FsmIntU5BU5D_t1976821196* ___layerMask_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick::invertMask
	FsmBool_t1075959796 * ___invertMask_20;
	// System.Boolean HutongGames.PlayMaker.Actions.ScreenPick::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_screenVector_9() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___screenVector_9)); }
	inline FsmVector3_t533912882 * get_screenVector_9() const { return ___screenVector_9; }
	inline FsmVector3_t533912882 ** get_address_of_screenVector_9() { return &___screenVector_9; }
	inline void set_screenVector_9(FsmVector3_t533912882 * value)
	{
		___screenVector_9 = value;
		Il2CppCodeGenWriteBarrier(&___screenVector_9, value);
	}

	inline static int32_t get_offset_of_screenX_10() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___screenX_10)); }
	inline FsmFloat_t2134102846 * get_screenX_10() const { return ___screenX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_screenX_10() { return &___screenX_10; }
	inline void set_screenX_10(FsmFloat_t2134102846 * value)
	{
		___screenX_10 = value;
		Il2CppCodeGenWriteBarrier(&___screenX_10, value);
	}

	inline static int32_t get_offset_of_screenY_11() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___screenY_11)); }
	inline FsmFloat_t2134102846 * get_screenY_11() const { return ___screenY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_screenY_11() { return &___screenY_11; }
	inline void set_screenY_11(FsmFloat_t2134102846 * value)
	{
		___screenY_11 = value;
		Il2CppCodeGenWriteBarrier(&___screenY_11, value);
	}

	inline static int32_t get_offset_of_normalized_12() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___normalized_12)); }
	inline FsmBool_t1075959796 * get_normalized_12() const { return ___normalized_12; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_12() { return &___normalized_12; }
	inline void set_normalized_12(FsmBool_t1075959796 * value)
	{
		___normalized_12 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_12, value);
	}

	inline static int32_t get_offset_of_rayDistance_13() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___rayDistance_13)); }
	inline FsmFloat_t2134102846 * get_rayDistance_13() const { return ___rayDistance_13; }
	inline FsmFloat_t2134102846 ** get_address_of_rayDistance_13() { return &___rayDistance_13; }
	inline void set_rayDistance_13(FsmFloat_t2134102846 * value)
	{
		___rayDistance_13 = value;
		Il2CppCodeGenWriteBarrier(&___rayDistance_13, value);
	}

	inline static int32_t get_offset_of_storeDidPickObject_14() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___storeDidPickObject_14)); }
	inline FsmBool_t1075959796 * get_storeDidPickObject_14() const { return ___storeDidPickObject_14; }
	inline FsmBool_t1075959796 ** get_address_of_storeDidPickObject_14() { return &___storeDidPickObject_14; }
	inline void set_storeDidPickObject_14(FsmBool_t1075959796 * value)
	{
		___storeDidPickObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeDidPickObject_14, value);
	}

	inline static int32_t get_offset_of_storeGameObject_15() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___storeGameObject_15)); }
	inline FsmGameObject_t1697147867 * get_storeGameObject_15() const { return ___storeGameObject_15; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeGameObject_15() { return &___storeGameObject_15; }
	inline void set_storeGameObject_15(FsmGameObject_t1697147867 * value)
	{
		___storeGameObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeGameObject_15, value);
	}

	inline static int32_t get_offset_of_storePoint_16() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___storePoint_16)); }
	inline FsmVector3_t533912882 * get_storePoint_16() const { return ___storePoint_16; }
	inline FsmVector3_t533912882 ** get_address_of_storePoint_16() { return &___storePoint_16; }
	inline void set_storePoint_16(FsmVector3_t533912882 * value)
	{
		___storePoint_16 = value;
		Il2CppCodeGenWriteBarrier(&___storePoint_16, value);
	}

	inline static int32_t get_offset_of_storeNormal_17() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___storeNormal_17)); }
	inline FsmVector3_t533912882 * get_storeNormal_17() const { return ___storeNormal_17; }
	inline FsmVector3_t533912882 ** get_address_of_storeNormal_17() { return &___storeNormal_17; }
	inline void set_storeNormal_17(FsmVector3_t533912882 * value)
	{
		___storeNormal_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeNormal_17, value);
	}

	inline static int32_t get_offset_of_storeDistance_18() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___storeDistance_18)); }
	inline FsmFloat_t2134102846 * get_storeDistance_18() const { return ___storeDistance_18; }
	inline FsmFloat_t2134102846 ** get_address_of_storeDistance_18() { return &___storeDistance_18; }
	inline void set_storeDistance_18(FsmFloat_t2134102846 * value)
	{
		___storeDistance_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeDistance_18, value);
	}

	inline static int32_t get_offset_of_layerMask_19() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___layerMask_19)); }
	inline FsmIntU5BU5D_t1976821196* get_layerMask_19() const { return ___layerMask_19; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_layerMask_19() { return &___layerMask_19; }
	inline void set_layerMask_19(FsmIntU5BU5D_t1976821196* value)
	{
		___layerMask_19 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_19, value);
	}

	inline static int32_t get_offset_of_invertMask_20() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___invertMask_20)); }
	inline FsmBool_t1075959796 * get_invertMask_20() const { return ___invertMask_20; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_20() { return &___invertMask_20; }
	inline void set_invertMask_20(FsmBool_t1075959796 * value)
	{
		___invertMask_20 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_20, value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(ScreenPick_t2360912917, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
