﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.screens.LoadScreenAbstract
struct LoadScreenAbstract_t4059313920;
// MagicTV.abstracts.screens.InitialSplashScreen
struct InitialSplashScreen_t2241678589;
// MagicTV.abstracts.screens.MenuScreenAbstract
struct MenuScreenAbstract_t4225613753;
// ARM.components.GroupComponentsManager
struct GroupComponentsManager_t3788191382;

#include "AssemblyU2DCSharp_MagicTV_abstracts_GUILoaderAbstr2603370330.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.gui.GUILoader
struct  GUILoader_t3143521200  : public GUILoaderAbstract_t2603370330
{
public:
	// MagicTV.abstracts.screens.LoadScreenAbstract MagicTV.gui.GUILoader::_loadScreen
	LoadScreenAbstract_t4059313920 * ____loadScreen_4;
	// MagicTV.abstracts.screens.InitialSplashScreen MagicTV.gui.GUILoader::_initialSplashScreen
	InitialSplashScreen_t2241678589 * ____initialSplashScreen_5;
	// MagicTV.abstracts.screens.MenuScreenAbstract MagicTV.gui.GUILoader::_menuScreen
	MenuScreenAbstract_t4225613753 * ____menuScreen_6;
	// ARM.components.GroupComponentsManager MagicTV.gui.GUILoader::group
	GroupComponentsManager_t3788191382 * ___group_7;

public:
	inline static int32_t get_offset_of__loadScreen_4() { return static_cast<int32_t>(offsetof(GUILoader_t3143521200, ____loadScreen_4)); }
	inline LoadScreenAbstract_t4059313920 * get__loadScreen_4() const { return ____loadScreen_4; }
	inline LoadScreenAbstract_t4059313920 ** get_address_of__loadScreen_4() { return &____loadScreen_4; }
	inline void set__loadScreen_4(LoadScreenAbstract_t4059313920 * value)
	{
		____loadScreen_4 = value;
		Il2CppCodeGenWriteBarrier(&____loadScreen_4, value);
	}

	inline static int32_t get_offset_of__initialSplashScreen_5() { return static_cast<int32_t>(offsetof(GUILoader_t3143521200, ____initialSplashScreen_5)); }
	inline InitialSplashScreen_t2241678589 * get__initialSplashScreen_5() const { return ____initialSplashScreen_5; }
	inline InitialSplashScreen_t2241678589 ** get_address_of__initialSplashScreen_5() { return &____initialSplashScreen_5; }
	inline void set__initialSplashScreen_5(InitialSplashScreen_t2241678589 * value)
	{
		____initialSplashScreen_5 = value;
		Il2CppCodeGenWriteBarrier(&____initialSplashScreen_5, value);
	}

	inline static int32_t get_offset_of__menuScreen_6() { return static_cast<int32_t>(offsetof(GUILoader_t3143521200, ____menuScreen_6)); }
	inline MenuScreenAbstract_t4225613753 * get__menuScreen_6() const { return ____menuScreen_6; }
	inline MenuScreenAbstract_t4225613753 ** get_address_of__menuScreen_6() { return &____menuScreen_6; }
	inline void set__menuScreen_6(MenuScreenAbstract_t4225613753 * value)
	{
		____menuScreen_6 = value;
		Il2CppCodeGenWriteBarrier(&____menuScreen_6, value);
	}

	inline static int32_t get_offset_of_group_7() { return static_cast<int32_t>(offsetof(GUILoader_t3143521200, ___group_7)); }
	inline GroupComponentsManager_t3788191382 * get_group_7() const { return ___group_7; }
	inline GroupComponentsManager_t3788191382 ** get_address_of_group_7() { return &___group_7; }
	inline void set_group_7(GroupComponentsManager_t3788191382 * value)
	{
		___group_7 = value;
		Il2CppCodeGenWriteBarrier(&___group_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
