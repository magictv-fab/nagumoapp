﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.utils.BundleHelper
struct BundleHelper_t1394860594;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;
// System.String
struct String_t;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String[]
struct StringU5BU5D_t4054002952;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void MagicTV.utils.BundleHelper::.ctor()
extern "C"  void BundleHelper__ctor_m884922767 (BundleHelper_t1394860594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.utils.BundleHelper::getSingleURLInfo(MagicTV.vo.FileVO)
extern "C"  UrlInfoVO_t1761987528 * BundleHelper_getSingleURLInfo_m1015200438 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___fVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.BundleHelper::GetPersistentPath(System.String)
extern "C"  String_t* BundleHelper_GetPersistentPath_m1042280582 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.BundleHelper::GetFileDownloadPath(MagicTV.vo.BundleVO,MagicTV.vo.UrlInfoVO)
extern "C"  String_t* BundleHelper_GetFileDownloadPath_m4201164430 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___bundleVO0, UrlInfoVO_t1761987528 * ___urlInfoVO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.BundleHelper::GetPrefabNameByBundle(MagicTV.vo.BundleVO)
extern "C"  String_t* BundleHelper_GetPrefabNameByBundle_m1822628633 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.BundleHelper::GetBundleLocalPath(System.String,System.String)
extern "C"  String_t* BundleHelper_GetBundleLocalPath_m2346802990 (Il2CppObject * __this /* static, unused */, String_t* ___bundleContext0, String_t* ___bundleId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] MagicTV.utils.BundleHelper::GetMetadataValuesByKey(System.String,MagicTV.vo.MetadataVO[])
extern "C"  StringU5BU5D_t4054002952* BundleHelper_GetMetadataValuesByKey_m2457594359 (Il2CppObject * __this /* static, unused */, String_t* ___key0, MetadataVOU5BU5D_t4238035203* ___metadata1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.BundleHelper::GetMetadataSingleValueByKey(System.String,MagicTV.vo.MetadataVO[])
extern "C"  String_t* BundleHelper_GetMetadataSingleValueByKey_m2800989856 (Il2CppObject * __this /* static, unused */, String_t* ___key0, MetadataVOU5BU5D_t4238035203* ___metadata1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.BundleHelper::<GetMetadataValuesByKey>m__75(MagicTV.vo.MetadataVO)
extern "C"  String_t* BundleHelper_U3CGetMetadataValuesByKeyU3Em__75_m653766016 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___mt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
