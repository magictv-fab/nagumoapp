﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.AREvents/OnARToggleOnOff
struct OnARToggleOnOff_t2139692132;
// MagicTV.globals.AREvents/OnARWantToDoSomething
struct OnARWantToDoSomething_t4073481420;
// MagicTV.globals.AREvents/OnStringEvent
struct OnStringEvent_t3419556962;
// MagicTV.globals.AREvents
struct AREvents_t1242196082;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.AREvents
struct  AREvents_t1242196082  : public MonoBehaviour_t667441552
{
public:
	// MagicTV.globals.AREvents/OnARToggleOnOff MagicTV.globals.AREvents::onARToggle
	OnARToggleOnOff_t2139692132 * ___onARToggle_2;
	// MagicTV.globals.AREvents/OnARWantToDoSomething MagicTV.globals.AREvents::onWantToTurnOn
	OnARWantToDoSomething_t4073481420 * ___onWantToTurnOn_3;
	// MagicTV.globals.AREvents/OnARWantToDoSomething MagicTV.globals.AREvents::onWantToTurnOff
	OnARWantToDoSomething_t4073481420 * ___onWantToTurnOff_4;
	// MagicTV.globals.AREvents/OnStringEvent MagicTV.globals.AREvents::onTrackIn
	OnStringEvent_t3419556962 * ___onTrackIn_5;
	// MagicTV.globals.AREvents/OnStringEvent MagicTV.globals.AREvents::onTrackOut
	OnStringEvent_t3419556962 * ___onTrackOut_6;

public:
	inline static int32_t get_offset_of_onARToggle_2() { return static_cast<int32_t>(offsetof(AREvents_t1242196082, ___onARToggle_2)); }
	inline OnARToggleOnOff_t2139692132 * get_onARToggle_2() const { return ___onARToggle_2; }
	inline OnARToggleOnOff_t2139692132 ** get_address_of_onARToggle_2() { return &___onARToggle_2; }
	inline void set_onARToggle_2(OnARToggleOnOff_t2139692132 * value)
	{
		___onARToggle_2 = value;
		Il2CppCodeGenWriteBarrier(&___onARToggle_2, value);
	}

	inline static int32_t get_offset_of_onWantToTurnOn_3() { return static_cast<int32_t>(offsetof(AREvents_t1242196082, ___onWantToTurnOn_3)); }
	inline OnARWantToDoSomething_t4073481420 * get_onWantToTurnOn_3() const { return ___onWantToTurnOn_3; }
	inline OnARWantToDoSomething_t4073481420 ** get_address_of_onWantToTurnOn_3() { return &___onWantToTurnOn_3; }
	inline void set_onWantToTurnOn_3(OnARWantToDoSomething_t4073481420 * value)
	{
		___onWantToTurnOn_3 = value;
		Il2CppCodeGenWriteBarrier(&___onWantToTurnOn_3, value);
	}

	inline static int32_t get_offset_of_onWantToTurnOff_4() { return static_cast<int32_t>(offsetof(AREvents_t1242196082, ___onWantToTurnOff_4)); }
	inline OnARWantToDoSomething_t4073481420 * get_onWantToTurnOff_4() const { return ___onWantToTurnOff_4; }
	inline OnARWantToDoSomething_t4073481420 ** get_address_of_onWantToTurnOff_4() { return &___onWantToTurnOff_4; }
	inline void set_onWantToTurnOff_4(OnARWantToDoSomething_t4073481420 * value)
	{
		___onWantToTurnOff_4 = value;
		Il2CppCodeGenWriteBarrier(&___onWantToTurnOff_4, value);
	}

	inline static int32_t get_offset_of_onTrackIn_5() { return static_cast<int32_t>(offsetof(AREvents_t1242196082, ___onTrackIn_5)); }
	inline OnStringEvent_t3419556962 * get_onTrackIn_5() const { return ___onTrackIn_5; }
	inline OnStringEvent_t3419556962 ** get_address_of_onTrackIn_5() { return &___onTrackIn_5; }
	inline void set_onTrackIn_5(OnStringEvent_t3419556962 * value)
	{
		___onTrackIn_5 = value;
		Il2CppCodeGenWriteBarrier(&___onTrackIn_5, value);
	}

	inline static int32_t get_offset_of_onTrackOut_6() { return static_cast<int32_t>(offsetof(AREvents_t1242196082, ___onTrackOut_6)); }
	inline OnStringEvent_t3419556962 * get_onTrackOut_6() const { return ___onTrackOut_6; }
	inline OnStringEvent_t3419556962 ** get_address_of_onTrackOut_6() { return &___onTrackOut_6; }
	inline void set_onTrackOut_6(OnStringEvent_t3419556962 * value)
	{
		___onTrackOut_6 = value;
		Il2CppCodeGenWriteBarrier(&___onTrackOut_6, value);
	}
};

struct AREvents_t1242196082_StaticFields
{
public:
	// MagicTV.globals.AREvents MagicTV.globals.AREvents::_instance
	AREvents_t1242196082 * ____instance_7;

public:
	inline static int32_t get_offset_of__instance_7() { return static_cast<int32_t>(offsetof(AREvents_t1242196082_StaticFields, ____instance_7)); }
	inline AREvents_t1242196082 * get__instance_7() const { return ____instance_7; }
	inline AREvents_t1242196082 ** get_address_of__instance_7() { return &____instance_7; }
	inline void set__instance_7(AREvents_t1242196082 * value)
	{
		____instance_7 = value;
		Il2CppCodeGenWriteBarrier(&____instance_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
