﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowAboutControllerScript
struct  MagicTVWindowAboutControllerScript_t2586885333  : public GenericWindowControllerScript_t248075822
{
public:
	// System.Action MagicTVWindowAboutControllerScript::onClickFacebook
	Action_t3771233898 * ___onClickFacebook_4;
	// System.Action MagicTVWindowAboutControllerScript::onClickTwitter
	Action_t3771233898 * ___onClickTwitter_5;
	// System.Action MagicTVWindowAboutControllerScript::onClickSite
	Action_t3771233898 * ___onClickSite_6;
	// System.Action MagicTVWindowAboutControllerScript::onClickEmail
	Action_t3771233898 * ___onClickEmail_7;
	// System.Action MagicTVWindowAboutControllerScript::onClickYouTubeChannel
	Action_t3771233898 * ___onClickYouTubeChannel_8;
	// System.Action MagicTVWindowAboutControllerScript::onClickClearCache
	Action_t3771233898 * ___onClickClearCache_9;
	// UnityEngine.UI.Text MagicTVWindowAboutControllerScript::LabelPINLand
	Text_t9039225 * ___LabelPINLand_10;
	// UnityEngine.UI.Text MagicTVWindowAboutControllerScript::LabelPINPort
	Text_t9039225 * ___LabelPINPort_11;

public:
	inline static int32_t get_offset_of_onClickFacebook_4() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___onClickFacebook_4)); }
	inline Action_t3771233898 * get_onClickFacebook_4() const { return ___onClickFacebook_4; }
	inline Action_t3771233898 ** get_address_of_onClickFacebook_4() { return &___onClickFacebook_4; }
	inline void set_onClickFacebook_4(Action_t3771233898 * value)
	{
		___onClickFacebook_4 = value;
		Il2CppCodeGenWriteBarrier(&___onClickFacebook_4, value);
	}

	inline static int32_t get_offset_of_onClickTwitter_5() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___onClickTwitter_5)); }
	inline Action_t3771233898 * get_onClickTwitter_5() const { return ___onClickTwitter_5; }
	inline Action_t3771233898 ** get_address_of_onClickTwitter_5() { return &___onClickTwitter_5; }
	inline void set_onClickTwitter_5(Action_t3771233898 * value)
	{
		___onClickTwitter_5 = value;
		Il2CppCodeGenWriteBarrier(&___onClickTwitter_5, value);
	}

	inline static int32_t get_offset_of_onClickSite_6() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___onClickSite_6)); }
	inline Action_t3771233898 * get_onClickSite_6() const { return ___onClickSite_6; }
	inline Action_t3771233898 ** get_address_of_onClickSite_6() { return &___onClickSite_6; }
	inline void set_onClickSite_6(Action_t3771233898 * value)
	{
		___onClickSite_6 = value;
		Il2CppCodeGenWriteBarrier(&___onClickSite_6, value);
	}

	inline static int32_t get_offset_of_onClickEmail_7() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___onClickEmail_7)); }
	inline Action_t3771233898 * get_onClickEmail_7() const { return ___onClickEmail_7; }
	inline Action_t3771233898 ** get_address_of_onClickEmail_7() { return &___onClickEmail_7; }
	inline void set_onClickEmail_7(Action_t3771233898 * value)
	{
		___onClickEmail_7 = value;
		Il2CppCodeGenWriteBarrier(&___onClickEmail_7, value);
	}

	inline static int32_t get_offset_of_onClickYouTubeChannel_8() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___onClickYouTubeChannel_8)); }
	inline Action_t3771233898 * get_onClickYouTubeChannel_8() const { return ___onClickYouTubeChannel_8; }
	inline Action_t3771233898 ** get_address_of_onClickYouTubeChannel_8() { return &___onClickYouTubeChannel_8; }
	inline void set_onClickYouTubeChannel_8(Action_t3771233898 * value)
	{
		___onClickYouTubeChannel_8 = value;
		Il2CppCodeGenWriteBarrier(&___onClickYouTubeChannel_8, value);
	}

	inline static int32_t get_offset_of_onClickClearCache_9() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___onClickClearCache_9)); }
	inline Action_t3771233898 * get_onClickClearCache_9() const { return ___onClickClearCache_9; }
	inline Action_t3771233898 ** get_address_of_onClickClearCache_9() { return &___onClickClearCache_9; }
	inline void set_onClickClearCache_9(Action_t3771233898 * value)
	{
		___onClickClearCache_9 = value;
		Il2CppCodeGenWriteBarrier(&___onClickClearCache_9, value);
	}

	inline static int32_t get_offset_of_LabelPINLand_10() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___LabelPINLand_10)); }
	inline Text_t9039225 * get_LabelPINLand_10() const { return ___LabelPINLand_10; }
	inline Text_t9039225 ** get_address_of_LabelPINLand_10() { return &___LabelPINLand_10; }
	inline void set_LabelPINLand_10(Text_t9039225 * value)
	{
		___LabelPINLand_10 = value;
		Il2CppCodeGenWriteBarrier(&___LabelPINLand_10, value);
	}

	inline static int32_t get_offset_of_LabelPINPort_11() { return static_cast<int32_t>(offsetof(MagicTVWindowAboutControllerScript_t2586885333, ___LabelPINPort_11)); }
	inline Text_t9039225 * get_LabelPINPort_11() const { return ___LabelPINPort_11; }
	inline Text_t9039225 ** get_address_of_LabelPINPort_11() { return &___LabelPINPort_11; }
	inline void set_LabelPINPort_11(Text_t9039225 * value)
	{
		___LabelPINPort_11 = value;
		Il2CppCodeGenWriteBarrier(&___LabelPINPort_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
