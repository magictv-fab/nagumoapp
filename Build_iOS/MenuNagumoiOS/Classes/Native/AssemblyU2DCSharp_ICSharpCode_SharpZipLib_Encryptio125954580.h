﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase
struct  PkzipClassicCryptoBase_t125954580  : public Il2CppObject
{
public:
	// System.UInt32[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::keys
	UInt32U5BU5D_t3230734560* ___keys_0;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(PkzipClassicCryptoBase_t125954580, ___keys_0)); }
	inline UInt32U5BU5D_t3230734560* get_keys_0() const { return ___keys_0; }
	inline UInt32U5BU5D_t3230734560** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(UInt32U5BU5D_t3230734560* value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier(&___keys_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
