﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<MagicTV.globals.StateMachine>
struct EqualityComparer_1_t3770800047;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<MagicTV.globals.StateMachine>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1869575742_gshared (EqualityComparer_1_t3770800047 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1869575742(__this, method) ((  void (*) (EqualityComparer_1_t3770800047 *, const MethodInfo*))EqualityComparer_1__ctor_m1869575742_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<MagicTV.globals.StateMachine>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1640176943_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1640176943(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1640176943_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<MagicTV.globals.StateMachine>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1386900943_gshared (EqualityComparer_1_t3770800047 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1386900943(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3770800047 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1386900943_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<MagicTV.globals.StateMachine>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m629969551_gshared (EqualityComparer_1_t3770800047 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m629969551(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3770800047 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m629969551_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<MagicTV.globals.StateMachine>::get_Default()
extern "C"  EqualityComparer_1_t3770800047 * EqualityComparer_1_get_Default_m2373711872_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2373711872(__this /* static, unused */, method) ((  EqualityComparer_1_t3770800047 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2373711872_gshared)(__this /* static, unused */, method)
