﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenShotME
struct ScreenShotME_t2769423806;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenShotME::.ctor()
extern "C"  void ScreenShotME__ctor_m2151053981 (ScreenShotME_t2769423806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShotME::Start()
extern "C"  void ScreenShotME_Start_m1098191773 (ScreenShotME_t2769423806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShotME::Update()
extern "C"  void ScreenShotME_Update_m3985026064 (ScreenShotME_t2769423806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenShotME::takeScreenShot()
extern "C"  Il2CppObject * ScreenShotME_takeScreenShot_m1598705116 (ScreenShotME_t2769423806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShotME::Capture()
extern "C"  void ScreenShotME_Capture_m3684247297 (ScreenShotME_t2769423806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
