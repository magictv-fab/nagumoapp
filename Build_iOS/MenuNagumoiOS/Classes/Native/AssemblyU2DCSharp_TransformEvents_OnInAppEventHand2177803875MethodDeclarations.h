﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransformEvents/OnInAppEventHandler
struct OnInAppEventHandler_t2177803875;
// System.Object
struct Il2CppObject;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TransformEvents/OnInAppEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInAppEventHandler__ctor_m32276154 (OnInAppEventHandler_t2177803875 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents/OnInAppEventHandler::Invoke(MagicTV.abstracts.InAppAbstract)
extern "C"  void OnInAppEventHandler_Invoke_m4137879420 (OnInAppEventHandler_t2177803875 * __this, InAppAbstract_t382673128 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TransformEvents/OnInAppEventHandler::BeginInvoke(MagicTV.abstracts.InAppAbstract,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInAppEventHandler_BeginInvoke_m117469933 (OnInAppEventHandler_t2177803875 * __this, InAppAbstract_t382673128 * ___value0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents/OnInAppEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnInAppEventHandler_EndInvoke_m1584422346 (OnInAppEventHandler_t2177803875 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
