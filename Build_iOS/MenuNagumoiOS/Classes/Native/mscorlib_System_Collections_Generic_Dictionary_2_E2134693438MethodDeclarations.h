﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3484314264MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1712417724(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2134693438 *, Dictionary_2_t817370046 *, const MethodInfo*))Enumerator__ctor_m3455248874_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m10061413(__this, method) ((  Il2CppObject * (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3267178999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3328231289(__this, method) ((  void (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m692652171_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m70694466(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m948350932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2571215745(__this, method) ((  Il2CppObject * (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3926317459_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2380572115(__this, method) ((  Il2CppObject * (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3258228581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::MoveNext()
#define Enumerator_MoveNext_m1454099429(__this, method) ((  bool (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_MoveNext_m2518547447_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_Current()
#define Enumerator_get_Current_m2397044779(__this, method) ((  KeyValuePair_2_t716150752  (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_get_Current_m3624402649_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2060027890(__this, method) ((  String_t* (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_get_CurrentKey_m3221742212_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3911546902(__this, method) ((  float (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_get_CurrentValue_m3627513384_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Reset()
#define Enumerator_Reset_m1384262670(__this, method) ((  void (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_Reset_m2659337532_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyState()
#define Enumerator_VerifyState_m2447096215(__this, method) ((  void (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_VerifyState_m3674454085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m762256191(__this, method) ((  void (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_VerifyCurrent_m3432130157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Dispose()
#define Enumerator_Dispose_m1359993182(__this, method) ((  void (*) (Enumerator_t2134693438 *, const MethodInfo*))Enumerator_Dispose_m2641256204_gshared)(__this, method)
