﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawTexture
struct  DrawTexture_t1601360325  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.DrawTexture::texture
	FsmTexture_t3073272573 * ___texture_9;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.DrawTexture::screenRect
	FsmRect_t1076426478 * ___screenRect_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::left
	FsmFloat_t2134102846 * ___left_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::top
	FsmFloat_t2134102846 * ___top_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::width
	FsmFloat_t2134102846 * ___width_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::height
	FsmFloat_t2134102846 * ___height_14;
	// UnityEngine.ScaleMode HutongGames.PlayMaker.Actions.DrawTexture::scaleMode
	int32_t ___scaleMode_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DrawTexture::alphaBlend
	FsmBool_t1075959796 * ___alphaBlend_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::imageAspect
	FsmFloat_t2134102846 * ___imageAspect_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DrawTexture::normalized
	FsmBool_t1075959796 * ___normalized_18;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.DrawTexture::rect
	Rect_t4241904616  ___rect_19;

public:
	inline static int32_t get_offset_of_texture_9() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___texture_9)); }
	inline FsmTexture_t3073272573 * get_texture_9() const { return ___texture_9; }
	inline FsmTexture_t3073272573 ** get_address_of_texture_9() { return &___texture_9; }
	inline void set_texture_9(FsmTexture_t3073272573 * value)
	{
		___texture_9 = value;
		Il2CppCodeGenWriteBarrier(&___texture_9, value);
	}

	inline static int32_t get_offset_of_screenRect_10() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___screenRect_10)); }
	inline FsmRect_t1076426478 * get_screenRect_10() const { return ___screenRect_10; }
	inline FsmRect_t1076426478 ** get_address_of_screenRect_10() { return &___screenRect_10; }
	inline void set_screenRect_10(FsmRect_t1076426478 * value)
	{
		___screenRect_10 = value;
		Il2CppCodeGenWriteBarrier(&___screenRect_10, value);
	}

	inline static int32_t get_offset_of_left_11() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___left_11)); }
	inline FsmFloat_t2134102846 * get_left_11() const { return ___left_11; }
	inline FsmFloat_t2134102846 ** get_address_of_left_11() { return &___left_11; }
	inline void set_left_11(FsmFloat_t2134102846 * value)
	{
		___left_11 = value;
		Il2CppCodeGenWriteBarrier(&___left_11, value);
	}

	inline static int32_t get_offset_of_top_12() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___top_12)); }
	inline FsmFloat_t2134102846 * get_top_12() const { return ___top_12; }
	inline FsmFloat_t2134102846 ** get_address_of_top_12() { return &___top_12; }
	inline void set_top_12(FsmFloat_t2134102846 * value)
	{
		___top_12 = value;
		Il2CppCodeGenWriteBarrier(&___top_12, value);
	}

	inline static int32_t get_offset_of_width_13() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___width_13)); }
	inline FsmFloat_t2134102846 * get_width_13() const { return ___width_13; }
	inline FsmFloat_t2134102846 ** get_address_of_width_13() { return &___width_13; }
	inline void set_width_13(FsmFloat_t2134102846 * value)
	{
		___width_13 = value;
		Il2CppCodeGenWriteBarrier(&___width_13, value);
	}

	inline static int32_t get_offset_of_height_14() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___height_14)); }
	inline FsmFloat_t2134102846 * get_height_14() const { return ___height_14; }
	inline FsmFloat_t2134102846 ** get_address_of_height_14() { return &___height_14; }
	inline void set_height_14(FsmFloat_t2134102846 * value)
	{
		___height_14 = value;
		Il2CppCodeGenWriteBarrier(&___height_14, value);
	}

	inline static int32_t get_offset_of_scaleMode_15() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___scaleMode_15)); }
	inline int32_t get_scaleMode_15() const { return ___scaleMode_15; }
	inline int32_t* get_address_of_scaleMode_15() { return &___scaleMode_15; }
	inline void set_scaleMode_15(int32_t value)
	{
		___scaleMode_15 = value;
	}

	inline static int32_t get_offset_of_alphaBlend_16() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___alphaBlend_16)); }
	inline FsmBool_t1075959796 * get_alphaBlend_16() const { return ___alphaBlend_16; }
	inline FsmBool_t1075959796 ** get_address_of_alphaBlend_16() { return &___alphaBlend_16; }
	inline void set_alphaBlend_16(FsmBool_t1075959796 * value)
	{
		___alphaBlend_16 = value;
		Il2CppCodeGenWriteBarrier(&___alphaBlend_16, value);
	}

	inline static int32_t get_offset_of_imageAspect_17() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___imageAspect_17)); }
	inline FsmFloat_t2134102846 * get_imageAspect_17() const { return ___imageAspect_17; }
	inline FsmFloat_t2134102846 ** get_address_of_imageAspect_17() { return &___imageAspect_17; }
	inline void set_imageAspect_17(FsmFloat_t2134102846 * value)
	{
		___imageAspect_17 = value;
		Il2CppCodeGenWriteBarrier(&___imageAspect_17, value);
	}

	inline static int32_t get_offset_of_normalized_18() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___normalized_18)); }
	inline FsmBool_t1075959796 * get_normalized_18() const { return ___normalized_18; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_18() { return &___normalized_18; }
	inline void set_normalized_18(FsmBool_t1075959796 * value)
	{
		___normalized_18 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_18, value);
	}

	inline static int32_t get_offset_of_rect_19() { return static_cast<int32_t>(offsetof(DrawTexture_t1601360325, ___rect_19)); }
	inline Rect_t4241904616  get_rect_19() const { return ___rect_19; }
	inline Rect_t4241904616 * get_address_of_rect_19() { return &___rect_19; }
	inline void set_rect_19(Rect_t4241904616  value)
	{
		___rect_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
