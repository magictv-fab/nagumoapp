﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Object,System.EventArgs>
struct Action_2_t2663079113;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DecodingOptions
struct  DecodingOptions_t3608870897  : public Il2CppObject
{
public:
	// System.Action`2<System.Object,System.EventArgs> ZXing.Common.DecodingOptions::ValueChanged
	Action_2_t2663079113 * ___ValueChanged_0;
	// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.Common.DecodingOptions::<Hints>k__BackingField
	Il2CppObject* ___U3CHintsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_ValueChanged_0() { return static_cast<int32_t>(offsetof(DecodingOptions_t3608870897, ___ValueChanged_0)); }
	inline Action_2_t2663079113 * get_ValueChanged_0() const { return ___ValueChanged_0; }
	inline Action_2_t2663079113 ** get_address_of_ValueChanged_0() { return &___ValueChanged_0; }
	inline void set_ValueChanged_0(Action_2_t2663079113 * value)
	{
		___ValueChanged_0 = value;
		Il2CppCodeGenWriteBarrier(&___ValueChanged_0, value);
	}

	inline static int32_t get_offset_of_U3CHintsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DecodingOptions_t3608870897, ___U3CHintsU3Ek__BackingField_1)); }
	inline Il2CppObject* get_U3CHintsU3Ek__BackingField_1() const { return ___U3CHintsU3Ek__BackingField_1; }
	inline Il2CppObject** get_address_of_U3CHintsU3Ek__BackingField_1() { return &___U3CHintsU3Ek__BackingField_1; }
	inline void set_U3CHintsU3Ek__BackingField_1(Il2CppObject* value)
	{
		___U3CHintsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHintsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
