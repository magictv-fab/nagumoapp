﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.gui.screens.HowToScreen
struct HowToScreen_t4005946933;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.gui.screens.HowToScreen::.ctor()
extern "C"  void HowToScreen__ctor_m724421965 (HowToScreen_t4005946933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.HowToScreen::prepare()
extern "C"  void HowToScreen_prepare_m1311786834 (HowToScreen_t4005946933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.HowToScreen::checkSelected()
extern "C"  void HowToScreen_checkSelected_m3638312846 (HowToScreen_t4005946933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.HowToScreen::hide()
extern "C"  void HowToScreen_hide_m441985785 (HowToScreen_t4005946933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.screens.HowToScreen::show()
extern "C"  void HowToScreen_show_m756327924 (HowToScreen_t4005946933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
