﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,MagicTV.globals.InAppAnalytics>
struct Dictionary_2_t3137152404;
// System.String
struct String_t;
// Analytics
struct Analytics_t310950758;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.InAppAnalytics
struct  InAppAnalytics_t2316734034  : public Il2CppObject
{
public:
	// System.String MagicTV.globals.InAppAnalytics::_appName
	String_t* ____appName_1;
	// Analytics MagicTV.globals.InAppAnalytics::_Analytics
	Analytics_t310950758 * ____Analytics_2;
	// System.String MagicTV.globals.InAppAnalytics::_lastTrackNameAnalytics
	String_t* ____lastTrackNameAnalytics_3;

public:
	inline static int32_t get_offset_of__appName_1() { return static_cast<int32_t>(offsetof(InAppAnalytics_t2316734034, ____appName_1)); }
	inline String_t* get__appName_1() const { return ____appName_1; }
	inline String_t** get_address_of__appName_1() { return &____appName_1; }
	inline void set__appName_1(String_t* value)
	{
		____appName_1 = value;
		Il2CppCodeGenWriteBarrier(&____appName_1, value);
	}

	inline static int32_t get_offset_of__Analytics_2() { return static_cast<int32_t>(offsetof(InAppAnalytics_t2316734034, ____Analytics_2)); }
	inline Analytics_t310950758 * get__Analytics_2() const { return ____Analytics_2; }
	inline Analytics_t310950758 ** get_address_of__Analytics_2() { return &____Analytics_2; }
	inline void set__Analytics_2(Analytics_t310950758 * value)
	{
		____Analytics_2 = value;
		Il2CppCodeGenWriteBarrier(&____Analytics_2, value);
	}

	inline static int32_t get_offset_of__lastTrackNameAnalytics_3() { return static_cast<int32_t>(offsetof(InAppAnalytics_t2316734034, ____lastTrackNameAnalytics_3)); }
	inline String_t* get__lastTrackNameAnalytics_3() const { return ____lastTrackNameAnalytics_3; }
	inline String_t** get_address_of__lastTrackNameAnalytics_3() { return &____lastTrackNameAnalytics_3; }
	inline void set__lastTrackNameAnalytics_3(String_t* value)
	{
		____lastTrackNameAnalytics_3 = value;
		Il2CppCodeGenWriteBarrier(&____lastTrackNameAnalytics_3, value);
	}
};

struct InAppAnalytics_t2316734034_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,MagicTV.globals.InAppAnalytics> MagicTV.globals.InAppAnalytics::instances
	Dictionary_2_t3137152404 * ___instances_0;

public:
	inline static int32_t get_offset_of_instances_0() { return static_cast<int32_t>(offsetof(InAppAnalytics_t2316734034_StaticFields, ___instances_0)); }
	inline Dictionary_2_t3137152404 * get_instances_0() const { return ___instances_0; }
	inline Dictionary_2_t3137152404 ** get_address_of_instances_0() { return &___instances_0; }
	inline void set_instances_0(Dictionary_2_t3137152404 * value)
	{
		___instances_0 = value;
		Il2CppCodeGenWriteBarrier(&___instances_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
