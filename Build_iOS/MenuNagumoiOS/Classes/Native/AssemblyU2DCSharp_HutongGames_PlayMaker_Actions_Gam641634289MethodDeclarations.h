﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectChanged
struct GameObjectChanged_t641634289;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::.ctor()
extern "C"  void GameObjectChanged__ctor_m3955321957 (GameObjectChanged_t641634289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::Reset()
extern "C"  void GameObjectChanged_Reset_m1601754898 (GameObjectChanged_t641634289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnEnter()
extern "C"  void GameObjectChanged_OnEnter_m3705039612 (GameObjectChanged_t641634289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnUpdate()
extern "C"  void GameObjectChanged_OnUpdate_m2320637639 (GameObjectChanged_t641634289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
