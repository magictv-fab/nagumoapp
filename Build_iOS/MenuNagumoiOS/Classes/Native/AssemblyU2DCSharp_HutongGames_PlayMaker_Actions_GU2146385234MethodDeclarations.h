﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndCentered
struct GUILayoutEndCentered_t2146385234;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndCentered::.ctor()
extern "C"  void GUILayoutEndCentered__ctor_m3634672980 (GUILayoutEndCentered_t2146385234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndCentered::Reset()
extern "C"  void GUILayoutEndCentered_Reset_m1281105921 (GUILayoutEndCentered_t2146385234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndCentered::OnGUI()
extern "C"  void GUILayoutEndCentered_OnGUI_m3130071630 (GUILayoutEndCentered_t2146385234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
