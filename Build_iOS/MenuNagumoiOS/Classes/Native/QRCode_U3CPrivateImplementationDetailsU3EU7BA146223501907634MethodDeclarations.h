﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{A1462213-BC44-4626-98E1-2B98D236945C}/__StaticArrayInitTypeSize=40
struct __StaticArrayInitTypeSizeU3D40_t3501907634;
struct __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_pinvoke;
struct __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D40_t3501907634;
struct __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_pinvoke;

extern "C" void __StaticArrayInitTypeSizeU3D40_t3501907634_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D40_t3501907634& unmarshaled, __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_pinvoke& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D40_t3501907634_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D40_t3501907634& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D40_t3501907634_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D40_t3501907634;
struct __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_com;

extern "C" void __StaticArrayInitTypeSizeU3D40_t3501907634_marshal_com(const __StaticArrayInitTypeSizeU3D40_t3501907634& unmarshaled, __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_com& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D40_t3501907634_marshal_com_back(const __StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D40_t3501907634& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D40_t3501907634_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D40_t3501907634_marshaled_com& marshaled);
