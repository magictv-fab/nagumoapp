﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.ITFReader
struct ITFReader_t4072828816;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// ZXing.Result ZXing.OneD.ITFReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * ITFReader_decodeRow_m2829106942 (ITFReader_t4072828816 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.ITFReader::decodeMiddle(ZXing.Common.BitArray,System.Int32,System.Int32,System.Text.StringBuilder)
extern "C"  bool ITFReader_decodeMiddle_m255411545 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___payloadStart1, int32_t ___payloadEnd2, StringBuilder_t243639308 * ___resultString3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.ITFReader::decodeStart(ZXing.Common.BitArray)
extern "C"  Int32U5BU5D_t3230847821* ITFReader_decodeStart_m1866925796 (ITFReader_t4072828816 * __this, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.ITFReader::validateQuietZone(ZXing.Common.BitArray,System.Int32)
extern "C"  bool ITFReader_validateQuietZone_m3167157409 (ITFReader_t4072828816 * __this, BitArray_t4163851164 * ___row0, int32_t ___startPattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.ITFReader::skipWhiteSpace(ZXing.Common.BitArray)
extern "C"  int32_t ITFReader_skipWhiteSpace_m2508373890 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.ITFReader::decodeEnd(ZXing.Common.BitArray)
extern "C"  Int32U5BU5D_t3230847821* ITFReader_decodeEnd_m4116291787 (ITFReader_t4072828816 * __this, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.ITFReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* ITFReader_findGuardPattern_m2407740826 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___rowOffset1, Int32U5BU5D_t3230847821* ___pattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.ITFReader::decodeDigit(System.Int32[],System.Int32&)
extern "C"  bool ITFReader_decodeDigit_m3361960880 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, int32_t* ___bestMatch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.ITFReader::.ctor()
extern "C"  void ITFReader__ctor_m782191875 (ITFReader_t4072828816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.ITFReader::.cctor()
extern "C"  void ITFReader__cctor_m2291015434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
