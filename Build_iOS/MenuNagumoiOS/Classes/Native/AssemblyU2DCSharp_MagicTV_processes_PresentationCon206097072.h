﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// MagicTV.processes.PresentationController
struct PresentationController_t206097072;
// ARM.animation.GenericStartableComponentControllAbstract
struct GenericStartableComponentControllAbstract_t1794600275;
// MagicTV.abstracts.GUILoaderAbstract
struct GUILoaderAbstract_t2603370330;
// MagicTV.in_apps.InAppManager
struct InAppManager_t3917644817;
// ARM.components.GroupComponentsManager
struct GroupComponentsManager_t3788191382;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.processes.PresentationController
struct  PresentationController_t206097072  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// UnityEngine.GameObject MagicTV.processes.PresentationController::buttonGame
	GameObject_t3674682005 * ___buttonGame_8;
	// System.Boolean MagicTV.processes.PresentationController::lockRun
	bool ___lockRun_11;
	// System.Boolean MagicTV.processes.PresentationController::debugNow
	bool ___debugNow_14;
	// System.String MagicTV.processes.PresentationController::_lastTrackName
	String_t* ____lastTrackName_15;
	// ARM.animation.GenericStartableComponentControllAbstract MagicTV.processes.PresentationController::openProcessComponent
	GenericStartableComponentControllAbstract_t1794600275 * ___openProcessComponent_17;
	// MagicTV.abstracts.GUILoaderAbstract MagicTV.processes.PresentationController::guiLoader
	GUILoaderAbstract_t2603370330 * ___guiLoader_18;
	// MagicTV.in_apps.InAppManager MagicTV.processes.PresentationController::inAppManager
	InAppManager_t3917644817 * ___inAppManager_19;
	// ARM.components.GroupComponentsManager MagicTV.processes.PresentationController::groupComponent
	GroupComponentsManager_t3788191382 * ___groupComponent_20;

public:
	inline static int32_t get_offset_of_buttonGame_8() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___buttonGame_8)); }
	inline GameObject_t3674682005 * get_buttonGame_8() const { return ___buttonGame_8; }
	inline GameObject_t3674682005 ** get_address_of_buttonGame_8() { return &___buttonGame_8; }
	inline void set_buttonGame_8(GameObject_t3674682005 * value)
	{
		___buttonGame_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonGame_8, value);
	}

	inline static int32_t get_offset_of_lockRun_11() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___lockRun_11)); }
	inline bool get_lockRun_11() const { return ___lockRun_11; }
	inline bool* get_address_of_lockRun_11() { return &___lockRun_11; }
	inline void set_lockRun_11(bool value)
	{
		___lockRun_11 = value;
	}

	inline static int32_t get_offset_of_debugNow_14() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___debugNow_14)); }
	inline bool get_debugNow_14() const { return ___debugNow_14; }
	inline bool* get_address_of_debugNow_14() { return &___debugNow_14; }
	inline void set_debugNow_14(bool value)
	{
		___debugNow_14 = value;
	}

	inline static int32_t get_offset_of__lastTrackName_15() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ____lastTrackName_15)); }
	inline String_t* get__lastTrackName_15() const { return ____lastTrackName_15; }
	inline String_t** get_address_of__lastTrackName_15() { return &____lastTrackName_15; }
	inline void set__lastTrackName_15(String_t* value)
	{
		____lastTrackName_15 = value;
		Il2CppCodeGenWriteBarrier(&____lastTrackName_15, value);
	}

	inline static int32_t get_offset_of_openProcessComponent_17() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___openProcessComponent_17)); }
	inline GenericStartableComponentControllAbstract_t1794600275 * get_openProcessComponent_17() const { return ___openProcessComponent_17; }
	inline GenericStartableComponentControllAbstract_t1794600275 ** get_address_of_openProcessComponent_17() { return &___openProcessComponent_17; }
	inline void set_openProcessComponent_17(GenericStartableComponentControllAbstract_t1794600275 * value)
	{
		___openProcessComponent_17 = value;
		Il2CppCodeGenWriteBarrier(&___openProcessComponent_17, value);
	}

	inline static int32_t get_offset_of_guiLoader_18() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___guiLoader_18)); }
	inline GUILoaderAbstract_t2603370330 * get_guiLoader_18() const { return ___guiLoader_18; }
	inline GUILoaderAbstract_t2603370330 ** get_address_of_guiLoader_18() { return &___guiLoader_18; }
	inline void set_guiLoader_18(GUILoaderAbstract_t2603370330 * value)
	{
		___guiLoader_18 = value;
		Il2CppCodeGenWriteBarrier(&___guiLoader_18, value);
	}

	inline static int32_t get_offset_of_inAppManager_19() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___inAppManager_19)); }
	inline InAppManager_t3917644817 * get_inAppManager_19() const { return ___inAppManager_19; }
	inline InAppManager_t3917644817 ** get_address_of_inAppManager_19() { return &___inAppManager_19; }
	inline void set_inAppManager_19(InAppManager_t3917644817 * value)
	{
		___inAppManager_19 = value;
		Il2CppCodeGenWriteBarrier(&___inAppManager_19, value);
	}

	inline static int32_t get_offset_of_groupComponent_20() { return static_cast<int32_t>(offsetof(PresentationController_t206097072, ___groupComponent_20)); }
	inline GroupComponentsManager_t3788191382 * get_groupComponent_20() const { return ___groupComponent_20; }
	inline GroupComponentsManager_t3788191382 ** get_address_of_groupComponent_20() { return &___groupComponent_20; }
	inline void set_groupComponent_20(GroupComponentsManager_t3788191382 * value)
	{
		___groupComponent_20 = value;
		Il2CppCodeGenWriteBarrier(&___groupComponent_20, value);
	}
};

struct PresentationController_t206097072_StaticFields
{
public:
	// System.Boolean MagicTV.processes.PresentationController::tracking
	bool ___tracking_9;
	// System.Boolean MagicTV.processes.PresentationController::checkTracking
	bool ___checkTracking_10;
	// System.Int64 MagicTV.processes.PresentationController::startTime
	int64_t ___startTime_12;
	// System.Int64 MagicTV.processes.PresentationController::lastTotal
	int64_t ___lastTotal_13;
	// MagicTV.processes.PresentationController MagicTV.processes.PresentationController::Instance
	PresentationController_t206097072 * ___Instance_16;

public:
	inline static int32_t get_offset_of_tracking_9() { return static_cast<int32_t>(offsetof(PresentationController_t206097072_StaticFields, ___tracking_9)); }
	inline bool get_tracking_9() const { return ___tracking_9; }
	inline bool* get_address_of_tracking_9() { return &___tracking_9; }
	inline void set_tracking_9(bool value)
	{
		___tracking_9 = value;
	}

	inline static int32_t get_offset_of_checkTracking_10() { return static_cast<int32_t>(offsetof(PresentationController_t206097072_StaticFields, ___checkTracking_10)); }
	inline bool get_checkTracking_10() const { return ___checkTracking_10; }
	inline bool* get_address_of_checkTracking_10() { return &___checkTracking_10; }
	inline void set_checkTracking_10(bool value)
	{
		___checkTracking_10 = value;
	}

	inline static int32_t get_offset_of_startTime_12() { return static_cast<int32_t>(offsetof(PresentationController_t206097072_StaticFields, ___startTime_12)); }
	inline int64_t get_startTime_12() const { return ___startTime_12; }
	inline int64_t* get_address_of_startTime_12() { return &___startTime_12; }
	inline void set_startTime_12(int64_t value)
	{
		___startTime_12 = value;
	}

	inline static int32_t get_offset_of_lastTotal_13() { return static_cast<int32_t>(offsetof(PresentationController_t206097072_StaticFields, ___lastTotal_13)); }
	inline int64_t get_lastTotal_13() const { return ___lastTotal_13; }
	inline int64_t* get_address_of_lastTotal_13() { return &___lastTotal_13; }
	inline void set_lastTotal_13(int64_t value)
	{
		___lastTotal_13 = value;
	}

	inline static int32_t get_offset_of_Instance_16() { return static_cast<int32_t>(offsetof(PresentationController_t206097072_StaticFields, ___Instance_16)); }
	inline PresentationController_t206097072 * get_Instance_16() const { return ___Instance_16; }
	inline PresentationController_t206097072 ** get_address_of_Instance_16() { return &___Instance_16; }
	inline void set_Instance_16(PresentationController_t206097072 * value)
	{
		___Instance_16 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
