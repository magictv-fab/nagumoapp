﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_DeriveBytes381192106.h"
#include "mscorlib_System_Security_Cryptography_DES2466800440.h"
#include "mscorlib_System_Security_Cryptography_DESTransform1541249382.h"
#include "mscorlib_System_Security_Cryptography_DESCryptoSer3403271487.h"
#include "mscorlib_System_Security_Cryptography_DSA2466800856.h"
#include "mscorlib_System_Security_Cryptography_DSACryptoSer1680412575.h"
#include "mscorlib_System_Security_Cryptography_DSAParameter3032565794.h"
#include "mscorlib_System_Security_Cryptography_DSASignature3757876489.h"
#include "mscorlib_System_Security_Cryptography_DSASignature3693321770.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Tr3311781052.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Tra529135353.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithm532578791.h"
#include "mscorlib_System_Security_Cryptography_HMAC3976621235.h"
#include "mscorlib_System_Security_Cryptography_HMACMD5528676093.h"
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD163168893383.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA14024365272.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA2561408601978.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA3841408603030.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA5121408604733.h"
#include "mscorlib_System_Security_Cryptography_KeyedHashAlg1039941647.h"
#include "mscorlib_System_Security_Cryptography_KeySizes2106826975.h"
#include "mscorlib_System_Security_Cryptography_MACTripleDES3005896753.h"
#include "mscorlib_System_Security_Cryptography_MD52466809028.h"
#include "mscorlib_System_Security_Cryptography_MD5CryptoSer2567592755.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode2126978938.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber1757673517.h"
#include "mscorlib_System_Security_Cryptography_RC22466813799.h"
#include "mscorlib_System_Security_Cryptography_RC2CryptoSer2806996080.h"
#include "mscorlib_System_Security_Cryptography_RC2Transform542353943.h"
#include "mscorlib_System_Security_Cryptography_Rfc2898Deriv2593193852.h"
#include "mscorlib_System_Security_Cryptography_Rijndael271969675.h"
#include "mscorlib_System_Security_Cryptography_RijndaelManag804207654.h"
#include "mscorlib_System_Security_Cryptography_RijndaelTrans739966067.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana4117818424.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD1601924651086.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD160Mana500645891.h"
#include "mscorlib_System_Security_Cryptography_RNGCryptoSer3764994374.h"
#include "mscorlib_System_Security_Cryptography_RSA2466814310.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoServ742318033.h"
#include "mscorlib_System_Security_Cryptography_RSAParameter3219890992.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyE1037641210.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Sign3036443203.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Sign3813241316.h"
#include "mscorlib_System_Security_Cryptography_SHA13976944113.h"
#include "mscorlib_System_Security_Cryptography_SHA1Internal831343822.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3725620262.h"
#include "mscorlib_System_Security_Cryptography_SHA1Managed2350919936.h"
#include "mscorlib_System_Security_Cryptography_SHA2563081508435.h"
#include "mscorlib_System_Security_Cryptography_SHA256Manage2930188894.h"
#include "mscorlib_System_Security_Cryptography_SHA3843081509487.h"
#include "mscorlib_System_Security_Cryptography_SHA384Manage2415625922.h"
#include "mscorlib_System_Security_Cryptography_SHA5123081511190.h"
#include "mscorlib_System_Security_Cryptography_SHA512Manage2599224891.h"
#include "mscorlib_System_Security_Cryptography_SHAConstants148935663.h"
#include "mscorlib_System_Security_Cryptography_SignatureDes2571609872.h"
#include "mscorlib_System_Security_Cryptography_DSASignature2196243356.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA1S555239675.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg3097904804.h"
#include "mscorlib_System_Security_Cryptography_ToBase64Tran3870587720.h"
#include "mscorlib_System_Security_Cryptography_TripleDES3018929210.h"
#include "mscorlib_System_Security_Cryptography_TripleDESCrypt89723261.h"
#include "mscorlib_System_Security_Cryptography_TripleDESTra2648885924.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3472602339.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPer761473792.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPe1343087044.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer3272779043.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer2587096487.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss3512558049.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss2673978341.h"
#include "mscorlib_System_Security_Permissions_GacIdentityPe1916590740.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora1203478336.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora3238287339.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorag942005711.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP2656552033.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP2372748443.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPe326505049.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP1147157375.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPe860156468.h"
#include "mscorlib_System_Security_Permissions_PermissionStat240192512.h"
#include "mscorlib_System_Security_Permissions_PublisherIden2106095047.h"
#include "mscorlib_System_Security_Permissions_ReflectionPermi68145834.h"
#include "mscorlib_System_Security_Permissions_ReflectionPer1843672246.h"
#include "mscorlib_System_Security_Permissions_RegistryPermi2715331196.h"
#include "mscorlib_System_Security_Permissions_RegistryPermi2947339328.h"
#include "mscorlib_System_Security_Permissions_SecurityActio2614322854.h"
#include "mscorlib_System_Security_Permissions_SecurityPermiss72370623.h"
#include "mscorlib_System_Security_Permissions_SecurityPermi3694729547.h"
#include "mscorlib_System_Security_Permissions_SiteIdentityP1699840228.h"
#include "mscorlib_System_Security_Permissions_StrongNameIdent25715679.h"
#include "mscorlib_System_Security_Permissions_StrongNameIdent80533897.h"
#include "mscorlib_System_Security_Permissions_StrongNamePub4133101135.h"
#include "mscorlib_System_Security_Permissions_UIPermission3504199347.h"
#include "mscorlib_System_Security_Permissions_UIPermissionC1570897969.h"
#include "mscorlib_System_Security_Permissions_UIPermissionW3699106243.h"
#include "mscorlib_System_Security_Permissions_UrlIdentityPe3210146746.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize800 = { sizeof (DeriveBytes_t381192106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize801 = { sizeof (DES_t2466800440), -1, sizeof(DES_t2466800440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable801[2] = 
{
	DES_t2466800440_StaticFields::get_offset_of_weakKeys_10(),
	DES_t2466800440_StaticFields::get_offset_of_semiWeakKeys_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize802 = { sizeof (DESTransform_t1541249382), -1, sizeof(DESTransform_t1541249382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable802[13] = 
{
	DESTransform_t1541249382_StaticFields::get_offset_of_KEY_BIT_SIZE_12(),
	DESTransform_t1541249382_StaticFields::get_offset_of_KEY_BYTE_SIZE_13(),
	DESTransform_t1541249382_StaticFields::get_offset_of_BLOCK_BIT_SIZE_14(),
	DESTransform_t1541249382_StaticFields::get_offset_of_BLOCK_BYTE_SIZE_15(),
	DESTransform_t1541249382::get_offset_of_keySchedule_16(),
	DESTransform_t1541249382::get_offset_of_byteBuff_17(),
	DESTransform_t1541249382::get_offset_of_dwordBuff_18(),
	DESTransform_t1541249382_StaticFields::get_offset_of_spBoxes_19(),
	DESTransform_t1541249382_StaticFields::get_offset_of_PC1_20(),
	DESTransform_t1541249382_StaticFields::get_offset_of_leftRotTotal_21(),
	DESTransform_t1541249382_StaticFields::get_offset_of_PC2_22(),
	DESTransform_t1541249382_StaticFields::get_offset_of_ipTab_23(),
	DESTransform_t1541249382_StaticFields::get_offset_of_fpTab_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize803 = { sizeof (DESCryptoServiceProvider_t3403271487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize804 = { sizeof (DSA_t2466800856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize805 = { sizeof (DSACryptoServiceProvider_t1680412575), -1, sizeof(DSACryptoServiceProvider_t1680412575_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable805[7] = 
{
	DSACryptoServiceProvider_t1680412575::get_offset_of_store_2(),
	DSACryptoServiceProvider_t1680412575::get_offset_of_persistKey_3(),
	DSACryptoServiceProvider_t1680412575::get_offset_of_persisted_4(),
	DSACryptoServiceProvider_t1680412575::get_offset_of_privateKeyExportable_5(),
	DSACryptoServiceProvider_t1680412575::get_offset_of_m_disposed_6(),
	DSACryptoServiceProvider_t1680412575::get_offset_of_dsa_7(),
	DSACryptoServiceProvider_t1680412575_StaticFields::get_offset_of_useMachineKeyStore_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize806 = { sizeof (DSAParameters_t3032565794)+ sizeof (Il2CppObject), sizeof(DSAParameters_t3032565794_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable806[8] = 
{
	DSAParameters_t3032565794::get_offset_of_Counter_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_G_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_J_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_P_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_Q_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_Seed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_X_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t3032565794::get_offset_of_Y_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize807 = { sizeof (DSASignatureDeformatter_t3757876489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable807[1] = 
{
	DSASignatureDeformatter_t3757876489::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize808 = { sizeof (DSASignatureFormatter_t3693321770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable808[1] = 
{
	DSASignatureFormatter_t3693321770::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize809 = { sizeof (FromBase64TransformMode_t3311781052)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable809[3] = 
{
	FromBase64TransformMode_t3311781052::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize810 = { sizeof (FromBase64Transform_t529135353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable810[5] = 
{
	FromBase64Transform_t529135353::get_offset_of_mode_0(),
	FromBase64Transform_t529135353::get_offset_of_accumulator_1(),
	FromBase64Transform_t529135353::get_offset_of_accPtr_2(),
	FromBase64Transform_t529135353::get_offset_of_m_disposed_3(),
	FromBase64Transform_t529135353::get_offset_of_lookupTable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize811 = { sizeof (HashAlgorithm_t532578791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable811[4] = 
{
	HashAlgorithm_t532578791::get_offset_of_HashValue_0(),
	HashAlgorithm_t532578791::get_offset_of_HashSizeValue_1(),
	HashAlgorithm_t532578791::get_offset_of_State_2(),
	HashAlgorithm_t532578791::get_offset_of_disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize812 = { sizeof (HMAC_t3976621235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable812[5] = 
{
	HMAC_t3976621235::get_offset_of__disposed_5(),
	HMAC_t3976621235::get_offset_of__hashName_6(),
	HMAC_t3976621235::get_offset_of__algo_7(),
	HMAC_t3976621235::get_offset_of__block_8(),
	HMAC_t3976621235::get_offset_of__blockSizeValue_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize813 = { sizeof (HMACMD5_t528676093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize814 = { sizeof (HMACRIPEMD160_t3168893383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize815 = { sizeof (HMACSHA1_t4024365272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize816 = { sizeof (HMACSHA256_t1408601978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize817 = { sizeof (HMACSHA384_t1408603030), -1, sizeof(HMACSHA384_t1408603030_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable817[2] = 
{
	HMACSHA384_t1408603030_StaticFields::get_offset_of_legacy_mode_10(),
	HMACSHA384_t1408603030::get_offset_of_legacy_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize818 = { sizeof (HMACSHA512_t1408604733), -1, sizeof(HMACSHA512_t1408604733_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable818[2] = 
{
	HMACSHA512_t1408604733_StaticFields::get_offset_of_legacy_mode_10(),
	HMACSHA512_t1408604733::get_offset_of_legacy_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize819 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize821 = { sizeof (KeyedHashAlgorithm_t1039941647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable821[1] = 
{
	KeyedHashAlgorithm_t1039941647::get_offset_of_KeyValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize822 = { sizeof (KeySizes_t2106826975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable822[3] = 
{
	KeySizes_t2106826975::get_offset_of__maxSize_0(),
	KeySizes_t2106826975::get_offset_of__minSize_1(),
	KeySizes_t2106826975::get_offset_of__skipSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize823 = { sizeof (MACTripleDES_t3005896753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable823[3] = 
{
	MACTripleDES_t3005896753::get_offset_of_tdes_5(),
	MACTripleDES_t3005896753::get_offset_of_mac_6(),
	MACTripleDES_t3005896753::get_offset_of_m_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize824 = { sizeof (MD5_t2466809028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize825 = { sizeof (MD5CryptoServiceProvider_t2567592755), -1, sizeof(MD5CryptoServiceProvider_t2567592755_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable825[6] = 
{
	MD5CryptoServiceProvider_t2567592755::get_offset_of__H_4(),
	MD5CryptoServiceProvider_t2567592755::get_offset_of_buff_5(),
	MD5CryptoServiceProvider_t2567592755::get_offset_of_count_6(),
	MD5CryptoServiceProvider_t2567592755::get_offset_of__ProcessingBuffer_7(),
	MD5CryptoServiceProvider_t2567592755::get_offset_of__ProcessingBufferCount_8(),
	MD5CryptoServiceProvider_t2567592755_StaticFields::get_offset_of_K_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize826 = { sizeof (PaddingMode_t2126978938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable826[6] = 
{
	PaddingMode_t2126978938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize827 = { sizeof (RandomNumberGenerator_t1757673517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize828 = { sizeof (RC2_t2466813799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable828[1] = 
{
	RC2_t2466813799::get_offset_of_EffectiveKeySizeValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize829 = { sizeof (RC2CryptoServiceProvider_t2806996080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize830 = { sizeof (RC2Transform_t542353943), -1, sizeof(RC2Transform_t542353943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable830[7] = 
{
	RC2Transform_t542353943::get_offset_of_R0_12(),
	RC2Transform_t542353943::get_offset_of_R1_13(),
	RC2Transform_t542353943::get_offset_of_R2_14(),
	RC2Transform_t542353943::get_offset_of_R3_15(),
	RC2Transform_t542353943::get_offset_of_K_16(),
	RC2Transform_t542353943::get_offset_of_j_17(),
	RC2Transform_t542353943_StaticFields::get_offset_of_pitable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize831 = { sizeof (Rfc2898DeriveBytes_t2593193852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable831[6] = 
{
	Rfc2898DeriveBytes_t2593193852::get_offset_of__iteration_0(),
	Rfc2898DeriveBytes_t2593193852::get_offset_of__salt_1(),
	Rfc2898DeriveBytes_t2593193852::get_offset_of__hmac_2(),
	Rfc2898DeriveBytes_t2593193852::get_offset_of__buffer_3(),
	Rfc2898DeriveBytes_t2593193852::get_offset_of__pos_4(),
	Rfc2898DeriveBytes_t2593193852::get_offset_of__f_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize832 = { sizeof (Rijndael_t271969675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize833 = { sizeof (RijndaelManaged_t804207654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize834 = { sizeof (RijndaelTransform_t739966067), -1, sizeof(RijndaelTransform_t739966067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable834[15] = 
{
	RijndaelTransform_t739966067::get_offset_of_expandedKey_12(),
	RijndaelTransform_t739966067::get_offset_of_Nb_13(),
	RijndaelTransform_t739966067::get_offset_of_Nk_14(),
	RijndaelTransform_t739966067::get_offset_of_Nr_15(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_Rcon_16(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_SBox_17(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_iSBox_18(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_T0_19(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_T1_20(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_T2_21(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_T3_22(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_iT0_23(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_iT1_24(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_iT2_25(),
	RijndaelTransform_t739966067_StaticFields::get_offset_of_iT3_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize835 = { sizeof (RijndaelManagedTransform_t4117818424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable835[2] = 
{
	RijndaelManagedTransform_t4117818424::get_offset_of__st_0(),
	RijndaelManagedTransform_t4117818424::get_offset_of__bs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize836 = { sizeof (RIPEMD160_t1924651086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize837 = { sizeof (RIPEMD160Managed_t500645891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable837[5] = 
{
	RIPEMD160Managed_t500645891::get_offset_of__ProcessingBuffer_4(),
	RIPEMD160Managed_t500645891::get_offset_of__X_5(),
	RIPEMD160Managed_t500645891::get_offset_of__HashValue_6(),
	RIPEMD160Managed_t500645891::get_offset_of__Length_7(),
	RIPEMD160Managed_t500645891::get_offset_of__ProcessingBufferCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize838 = { sizeof (RNGCryptoServiceProvider_t3764994374), -1, sizeof(RNGCryptoServiceProvider_t3764994374_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable838[2] = 
{
	RNGCryptoServiceProvider_t3764994374_StaticFields::get_offset_of__lock_0(),
	RNGCryptoServiceProvider_t3764994374::get_offset_of__handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize839 = { sizeof (RSA_t2466814310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize840 = { sizeof (RSACryptoServiceProvider_t742318033), -1, sizeof(RSACryptoServiceProvider_t742318033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable840[8] = 
{
	RSACryptoServiceProvider_t742318033::get_offset_of_store_2(),
	RSACryptoServiceProvider_t742318033::get_offset_of_persistKey_3(),
	RSACryptoServiceProvider_t742318033::get_offset_of_persisted_4(),
	RSACryptoServiceProvider_t742318033::get_offset_of_privateKeyExportable_5(),
	RSACryptoServiceProvider_t742318033::get_offset_of_m_disposed_6(),
	RSACryptoServiceProvider_t742318033::get_offset_of_rsa_7(),
	RSACryptoServiceProvider_t742318033_StaticFields::get_offset_of_useMachineKeyStore_8(),
	RSACryptoServiceProvider_t742318033_StaticFields::get_offset_of_U3CU3Ef__switchU24map2D_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize841 = { sizeof (RSAParameters_t3219890992)+ sizeof (Il2CppObject), sizeof(RSAParameters_t3219890992_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable841[8] = 
{
	RSAParameters_t3219890992::get_offset_of_P_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_Q_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_D_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_DP_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_DQ_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_InverseQ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_Modulus_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t3219890992::get_offset_of_Exponent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize842 = { sizeof (RSAPKCS1KeyExchangeFormatter_t1037641210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable842[2] = 
{
	RSAPKCS1KeyExchangeFormatter_t1037641210::get_offset_of_rsa_0(),
	RSAPKCS1KeyExchangeFormatter_t1037641210::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize843 = { sizeof (RSAPKCS1SignatureDeformatter_t3036443203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable843[2] = 
{
	RSAPKCS1SignatureDeformatter_t3036443203::get_offset_of_rsa_0(),
	RSAPKCS1SignatureDeformatter_t3036443203::get_offset_of_hashName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize844 = { sizeof (RSAPKCS1SignatureFormatter_t3813241316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable844[2] = 
{
	RSAPKCS1SignatureFormatter_t3813241316::get_offset_of_rsa_0(),
	RSAPKCS1SignatureFormatter_t3813241316::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize845 = { sizeof (SHA1_t3976944113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize846 = { sizeof (SHA1Internal_t831343822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable846[5] = 
{
	SHA1Internal_t831343822::get_offset_of__H_0(),
	SHA1Internal_t831343822::get_offset_of_count_1(),
	SHA1Internal_t831343822::get_offset_of__ProcessingBuffer_2(),
	SHA1Internal_t831343822::get_offset_of__ProcessingBufferCount_3(),
	SHA1Internal_t831343822::get_offset_of_buff_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize847 = { sizeof (SHA1CryptoServiceProvider_t3725620262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable847[1] = 
{
	SHA1CryptoServiceProvider_t3725620262::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize848 = { sizeof (SHA1Managed_t2350919936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable848[1] = 
{
	SHA1Managed_t2350919936::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize849 = { sizeof (SHA256_t3081508435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize850 = { sizeof (SHA256Managed_t2930188894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable850[5] = 
{
	SHA256Managed_t2930188894::get_offset_of__H_4(),
	SHA256Managed_t2930188894::get_offset_of_count_5(),
	SHA256Managed_t2930188894::get_offset_of__ProcessingBuffer_6(),
	SHA256Managed_t2930188894::get_offset_of__ProcessingBufferCount_7(),
	SHA256Managed_t2930188894::get_offset_of_buff_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize851 = { sizeof (SHA384_t3081509487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize852 = { sizeof (SHA384Managed_t2415625922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable852[14] = 
{
	SHA384Managed_t2415625922::get_offset_of_xBuf_4(),
	SHA384Managed_t2415625922::get_offset_of_xBufOff_5(),
	SHA384Managed_t2415625922::get_offset_of_byteCount1_6(),
	SHA384Managed_t2415625922::get_offset_of_byteCount2_7(),
	SHA384Managed_t2415625922::get_offset_of_H1_8(),
	SHA384Managed_t2415625922::get_offset_of_H2_9(),
	SHA384Managed_t2415625922::get_offset_of_H3_10(),
	SHA384Managed_t2415625922::get_offset_of_H4_11(),
	SHA384Managed_t2415625922::get_offset_of_H5_12(),
	SHA384Managed_t2415625922::get_offset_of_H6_13(),
	SHA384Managed_t2415625922::get_offset_of_H7_14(),
	SHA384Managed_t2415625922::get_offset_of_H8_15(),
	SHA384Managed_t2415625922::get_offset_of_W_16(),
	SHA384Managed_t2415625922::get_offset_of_wOff_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize853 = { sizeof (SHA512_t3081511190), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize854 = { sizeof (SHA512Managed_t2599224891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable854[14] = 
{
	SHA512Managed_t2599224891::get_offset_of_xBuf_4(),
	SHA512Managed_t2599224891::get_offset_of_xBufOff_5(),
	SHA512Managed_t2599224891::get_offset_of_byteCount1_6(),
	SHA512Managed_t2599224891::get_offset_of_byteCount2_7(),
	SHA512Managed_t2599224891::get_offset_of_H1_8(),
	SHA512Managed_t2599224891::get_offset_of_H2_9(),
	SHA512Managed_t2599224891::get_offset_of_H3_10(),
	SHA512Managed_t2599224891::get_offset_of_H4_11(),
	SHA512Managed_t2599224891::get_offset_of_H5_12(),
	SHA512Managed_t2599224891::get_offset_of_H6_13(),
	SHA512Managed_t2599224891::get_offset_of_H7_14(),
	SHA512Managed_t2599224891::get_offset_of_H8_15(),
	SHA512Managed_t2599224891::get_offset_of_W_16(),
	SHA512Managed_t2599224891::get_offset_of_wOff_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize855 = { sizeof (SHAConstants_t148935663), -1, sizeof(SHAConstants_t148935663_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable855[2] = 
{
	SHAConstants_t148935663_StaticFields::get_offset_of_K1_0(),
	SHAConstants_t148935663_StaticFields::get_offset_of_K2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize856 = { sizeof (SignatureDescription_t2571609872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable856[4] = 
{
	SignatureDescription_t2571609872::get_offset_of__DeformatterAlgorithm_0(),
	SignatureDescription_t2571609872::get_offset_of__DigestAlgorithm_1(),
	SignatureDescription_t2571609872::get_offset_of__FormatterAlgorithm_2(),
	SignatureDescription_t2571609872::get_offset_of__KeyAlgorithm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize857 = { sizeof (DSASignatureDescription_t2196243356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize858 = { sizeof (RSAPKCS1SHA1SignatureDescription_t555239675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize859 = { sizeof (SymmetricAlgorithm_t3097904804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable859[10] = 
{
	SymmetricAlgorithm_t3097904804::get_offset_of_BlockSizeValue_0(),
	SymmetricAlgorithm_t3097904804::get_offset_of_IVValue_1(),
	SymmetricAlgorithm_t3097904804::get_offset_of_KeySizeValue_2(),
	SymmetricAlgorithm_t3097904804::get_offset_of_KeyValue_3(),
	SymmetricAlgorithm_t3097904804::get_offset_of_LegalBlockSizesValue_4(),
	SymmetricAlgorithm_t3097904804::get_offset_of_LegalKeySizesValue_5(),
	SymmetricAlgorithm_t3097904804::get_offset_of_FeedbackSizeValue_6(),
	SymmetricAlgorithm_t3097904804::get_offset_of_ModeValue_7(),
	SymmetricAlgorithm_t3097904804::get_offset_of_PaddingValue_8(),
	SymmetricAlgorithm_t3097904804::get_offset_of_m_disposed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize860 = { sizeof (ToBase64Transform_t3870587720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable860[1] = 
{
	ToBase64Transform_t3870587720::get_offset_of_m_disposed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize861 = { sizeof (TripleDES_t3018929210), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize862 = { sizeof (TripleDESCryptoServiceProvider_t89723261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize863 = { sizeof (TripleDESTransform_t2648885924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable863[6] = 
{
	TripleDESTransform_t2648885924::get_offset_of_E1_12(),
	TripleDESTransform_t2648885924::get_offset_of_D2_13(),
	TripleDESTransform_t2648885924::get_offset_of_E3_14(),
	TripleDESTransform_t2648885924::get_offset_of_D1_15(),
	TripleDESTransform_t2648885924::get_offset_of_E2_16(),
	TripleDESTransform_t2648885924::get_offset_of_D3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize864 = { sizeof (X509Certificate_t3076817455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable864[5] = 
{
	X509Certificate_t3076817455::get_offset_of_x509_0(),
	X509Certificate_t3076817455::get_offset_of_hideDates_1(),
	X509Certificate_t3076817455::get_offset_of_cachedCertificateHash_2(),
	X509Certificate_t3076817455::get_offset_of_issuer_name_3(),
	X509Certificate_t3076817455::get_offset_of_subject_name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize865 = { sizeof (X509KeyStorageFlags_t3472602339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable865[7] = 
{
	X509KeyStorageFlags_t3472602339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize866 = { sizeof (EnvironmentPermission_t761473792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable866[3] = 
{
	EnvironmentPermission_t761473792::get_offset_of__state_0(),
	EnvironmentPermission_t761473792::get_offset_of_readList_1(),
	EnvironmentPermission_t761473792::get_offset_of_writeList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize867 = { sizeof (EnvironmentPermissionAccess_t1343087044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable867[5] = 
{
	EnvironmentPermissionAccess_t1343087044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize868 = { sizeof (FileDialogPermission_t3272779043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable868[1] = 
{
	FileDialogPermission_t3272779043::get_offset_of__access_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize869 = { sizeof (FileDialogPermissionAccess_t2587096487)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable869[5] = 
{
	FileDialogPermissionAccess_t2587096487::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize870 = { sizeof (FileIOPermission_t3512558049), -1, sizeof(FileIOPermission_t3512558049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable870[9] = 
{
	FileIOPermission_t3512558049_StaticFields::get_offset_of_BadPathNameCharacters_0(),
	FileIOPermission_t3512558049_StaticFields::get_offset_of_BadFileNameCharacters_1(),
	FileIOPermission_t3512558049::get_offset_of_m_Unrestricted_2(),
	FileIOPermission_t3512558049::get_offset_of_m_AllFilesAccess_3(),
	FileIOPermission_t3512558049::get_offset_of_m_AllLocalFilesAccess_4(),
	FileIOPermission_t3512558049::get_offset_of_readList_5(),
	FileIOPermission_t3512558049::get_offset_of_writeList_6(),
	FileIOPermission_t3512558049::get_offset_of_appendList_7(),
	FileIOPermission_t3512558049::get_offset_of_pathList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize871 = { sizeof (FileIOPermissionAccess_t2673978341)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable871[7] = 
{
	FileIOPermissionAccess_t2673978341::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize872 = { sizeof (GacIdentityPermission_t1916590740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize873 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize874 = { sizeof (IsolatedStorageContainment_t1203478336)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable874[13] = 
{
	IsolatedStorageContainment_t1203478336::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize875 = { sizeof (IsolatedStorageFilePermission_t3238287339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize876 = { sizeof (IsolatedStoragePermission_t942005711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable876[5] = 
{
	IsolatedStoragePermission_t942005711::get_offset_of_m_userQuota_0(),
	IsolatedStoragePermission_t942005711::get_offset_of_m_machineQuota_1(),
	IsolatedStoragePermission_t942005711::get_offset_of_m_expirationDays_2(),
	IsolatedStoragePermission_t942005711::get_offset_of_m_permanentData_3(),
	IsolatedStoragePermission_t942005711::get_offset_of_m_allowed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize878 = { sizeof (KeyContainerPermission_t2656552033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable878[2] = 
{
	KeyContainerPermission_t2656552033::get_offset_of__accessEntries_0(),
	KeyContainerPermission_t2656552033::get_offset_of__flags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize879 = { sizeof (KeyContainerPermissionAccessEntry_t2372748443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable879[6] = 
{
	KeyContainerPermissionAccessEntry_t2372748443::get_offset_of__flags_0(),
	KeyContainerPermissionAccessEntry_t2372748443::get_offset_of__containerName_1(),
	KeyContainerPermissionAccessEntry_t2372748443::get_offset_of__spec_2(),
	KeyContainerPermissionAccessEntry_t2372748443::get_offset_of__store_3(),
	KeyContainerPermissionAccessEntry_t2372748443::get_offset_of__providerName_4(),
	KeyContainerPermissionAccessEntry_t2372748443::get_offset_of__type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize880 = { sizeof (KeyContainerPermissionAccessEntryCollection_t326505049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable880[1] = 
{
	KeyContainerPermissionAccessEntryCollection_t326505049::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize881 = { sizeof (KeyContainerPermissionAccessEntryEnumerator_t1147157375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable881[1] = 
{
	KeyContainerPermissionAccessEntryEnumerator_t1147157375::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize882 = { sizeof (KeyContainerPermissionFlags_t860156468)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable882[12] = 
{
	KeyContainerPermissionFlags_t860156468::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize883 = { sizeof (PermissionState_t240192512)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable883[3] = 
{
	PermissionState_t240192512::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize884 = { sizeof (PublisherIdentityPermission_t2106095047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable884[1] = 
{
	PublisherIdentityPermission_t2106095047::get_offset_of_x509_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize885 = { sizeof (ReflectionPermission_t68145834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable885[1] = 
{
	ReflectionPermission_t68145834::get_offset_of_flags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize886 = { sizeof (ReflectionPermissionFlag_t1843672246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable886[7] = 
{
	ReflectionPermissionFlag_t1843672246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize887 = { sizeof (RegistryPermission_t2715331196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable887[4] = 
{
	RegistryPermission_t2715331196::get_offset_of__state_0(),
	RegistryPermission_t2715331196::get_offset_of_createList_1(),
	RegistryPermission_t2715331196::get_offset_of_readList_2(),
	RegistryPermission_t2715331196::get_offset_of_writeList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize888 = { sizeof (RegistryPermissionAccess_t2947339328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable888[6] = 
{
	RegistryPermissionAccess_t2947339328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize889 = { sizeof (SecurityAction_t2614322854)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable889[10] = 
{
	SecurityAction_t2614322854::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize890 = { sizeof (SecurityPermission_t72370623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable890[1] = 
{
	SecurityPermission_t72370623::get_offset_of_flags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize891 = { sizeof (SecurityPermissionFlag_t3694729547)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable891[17] = 
{
	SecurityPermissionFlag_t3694729547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize892 = { sizeof (SiteIdentityPermission_t1699840228), -1, sizeof(SiteIdentityPermission_t1699840228_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable892[2] = 
{
	SiteIdentityPermission_t1699840228::get_offset_of__site_0(),
	SiteIdentityPermission_t1699840228_StaticFields::get_offset_of_valid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize893 = { sizeof (StrongNameIdentityPermission_t25715679), -1, sizeof(StrongNameIdentityPermission_t25715679_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable893[3] = 
{
	StrongNameIdentityPermission_t25715679_StaticFields::get_offset_of_defaultVersion_0(),
	StrongNameIdentityPermission_t25715679::get_offset_of__state_1(),
	StrongNameIdentityPermission_t25715679::get_offset_of__list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize894 = { sizeof (SNIP_t80533897)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable894[3] = 
{
	SNIP_t80533897::get_offset_of_PublicKey_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SNIP_t80533897::get_offset_of_Name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SNIP_t80533897::get_offset_of_AssemblyVersion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize895 = { sizeof (StrongNamePublicKeyBlob_t4133101135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable895[1] = 
{
	StrongNamePublicKeyBlob_t4133101135::get_offset_of_pubkey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize896 = { sizeof (UIPermission_t3504199347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable896[2] = 
{
	UIPermission_t3504199347::get_offset_of__window_0(),
	UIPermission_t3504199347::get_offset_of__clipboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize897 = { sizeof (UIPermissionClipboard_t1570897969)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable897[4] = 
{
	UIPermissionClipboard_t1570897969::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize898 = { sizeof (UIPermissionWindow_t3699106243)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable898[5] = 
{
	UIPermissionWindow_t3699106243::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize899 = { sizeof (UrlIdentityPermission_t3210146746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable899[1] = 
{
	UrlIdentityPermission_t3210146746::get_offset_of_url_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
