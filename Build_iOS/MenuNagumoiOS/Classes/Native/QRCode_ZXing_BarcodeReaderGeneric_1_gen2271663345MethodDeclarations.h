﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BarcodeReaderGeneric_1_gen3481712763MethodDeclarations.h"

// System.Void ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::.cctor()
#define BarcodeReaderGeneric_1__cctor_m2012611904(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))BarcodeReaderGeneric_1__cctor_m4288126543_gshared)(__this /* static, unused */, method)
// System.Void ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::.ctor(ZXing.Reader,System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
#define BarcodeReaderGeneric_1__ctor_m689372726(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, method) ((  void (*) (BarcodeReaderGeneric_1_t2271663345 *, Il2CppObject *, Func_4_t4289141939 *, Func_2_t2392815316 *, const MethodInfo*))BarcodeReaderGeneric_1__ctor_m3825891079_gshared)(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, method)
// System.Void ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::.ctor(ZXing.Reader,System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
#define BarcodeReaderGeneric_1__ctor_m1118518381(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, ___createRGBLuminanceSource3, method) ((  void (*) (BarcodeReaderGeneric_1_t2271663345 *, Il2CppObject *, Func_4_t4289141939 *, Func_2_t2392815316 *, Func_5_t533482363 *, const MethodInfo*))BarcodeReaderGeneric_1__ctor_m2970014716_gshared)(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, ___createRGBLuminanceSource3, method)
// ZXing.Result ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::Decode(ZXing.LuminanceSource)
#define BarcodeReaderGeneric_1_Decode_m3443407546(__this, ___luminanceSource0, method) ((  Result_t2610723219 * (*) (BarcodeReaderGeneric_1_t2271663345 *, LuminanceSource_t1231523093 *, const MethodInfo*))BarcodeReaderGeneric_1_Decode_m1780868875_gshared)(__this, ___luminanceSource0, method)
// ZXing.Result ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::Decode(T,System.Int32,System.Int32)
#define BarcodeReaderGeneric_1_Decode_m1831234464(__this, ___rawRGB0, ___width1, ___height2, method) ((  Result_t2610723219 * (*) (BarcodeReaderGeneric_1_t2271663345 *, Color32U5BU5D_t2960766953*, int32_t, int32_t, const MethodInfo*))BarcodeReaderGeneric_1_Decode_m693721600_gshared)(__this, ___rawRGB0, ___width1, ___height2, method)
// System.Void ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::OnResultFound(ZXing.Result)
#define BarcodeReaderGeneric_1_OnResultFound_m1030978314(__this, ___result0, method) ((  void (*) (BarcodeReaderGeneric_1_t2271663345 *, Result_t2610723219 *, const MethodInfo*))BarcodeReaderGeneric_1_OnResultFound_m1942802843_gshared)(__this, ___result0, method)
// System.Boolean ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::get_AutoRotate()
#define BarcodeReaderGeneric_1_get_AutoRotate_m2814172246(__this, method) ((  bool (*) (BarcodeReaderGeneric_1_t2271663345 *, const MethodInfo*))BarcodeReaderGeneric_1_get_AutoRotate_m3605742181_gshared)(__this, method)
// System.Void ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::set_AutoRotate(System.Boolean)
#define BarcodeReaderGeneric_1_set_AutoRotate_m4172882581(__this, ___value0, method) ((  void (*) (BarcodeReaderGeneric_1_t2271663345 *, bool, const MethodInfo*))BarcodeReaderGeneric_1_set_AutoRotate_m3182146308_gshared)(__this, ___value0, method)
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::get_CreateBinarizer()
#define BarcodeReaderGeneric_1_get_CreateBinarizer_m584064189(__this, method) ((  Func_2_t2392815316 * (*) (BarcodeReaderGeneric_1_t2271663345 *, const MethodInfo*))BarcodeReaderGeneric_1_get_CreateBinarizer_m3095086350_gshared)(__this, method)
// System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::get_CreateLuminanceSource()
#define BarcodeReaderGeneric_1_get_CreateLuminanceSource_m438192868(__this, method) ((  Func_4_t4289141939 * (*) (BarcodeReaderGeneric_1_t2271663345 *, const MethodInfo*))BarcodeReaderGeneric_1_get_CreateLuminanceSource_m3272449141_gshared)(__this, method)
// ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::get_Options()
#define BarcodeReaderGeneric_1_get_Options_m469766883(__this, method) ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t2271663345 *, const MethodInfo*))BarcodeReaderGeneric_1_get_Options_m4237311540_gshared)(__this, method)
// ZXing.Reader ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::get_Reader()
#define BarcodeReaderGeneric_1_get_Reader_m912764147(__this, method) ((  Il2CppObject * (*) (BarcodeReaderGeneric_1_t2271663345 *, const MethodInfo*))BarcodeReaderGeneric_1_get_Reader_m3072341634_gshared)(__this, method)
// System.Boolean ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::get_TryInverted()
#define BarcodeReaderGeneric_1_get_TryInverted_m2755532358(__this, method) ((  bool (*) (BarcodeReaderGeneric_1_t2271663345 *, const MethodInfo*))BarcodeReaderGeneric_1_get_TryInverted_m1524396567_gshared)(__this, method)
// System.Void ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::set_TryInverted(System.Boolean)
#define BarcodeReaderGeneric_1_set_TryInverted_m416310005(__this, ___value0, method) ((  void (*) (BarcodeReaderGeneric_1_t2271663345 *, bool, const MethodInfo*))BarcodeReaderGeneric_1_set_TryInverted_m4063223910_gshared)(__this, ___value0, method)
// ZXing.Binarizer ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::<.cctor>b__0(ZXing.LuminanceSource)
#define BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m3223669426(__this /* static, unused */, ___luminanceSource0, method) ((  Binarizer_t1492033400 * (*) (Il2CppObject * /* static, unused */, LuminanceSource_t1231523093 *, const MethodInfo*))BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963_gshared)(__this /* static, unused */, ___luminanceSource0, method)
// ZXing.LuminanceSource ZXing.BarcodeReaderGeneric`1<UnityEngine.Color32[]>::<.cctor>b__1(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
#define BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m737845115(__this /* static, unused */, ___rawBytes0, ___width1, ___height2, ___format3, method) ((  LuminanceSource_t1231523093 * (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, int32_t, int32_t, int32_t, const MethodInfo*))BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580_gshared)(__this /* static, unused */, ___rawBytes0, ___width1, ___height2, ___format3, method)
