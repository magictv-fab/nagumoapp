﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolQuaternion
struct PoolQuaternion_t2988316782;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugMedianVector3
struct  DebugMedianVector3_t3206408051  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector4 DebugMedianVector3::vec1
	Vector4_t4282066567  ___vec1_2;
	// UnityEngine.Vector4 DebugMedianVector3::vec2
	Vector4_t4282066567  ___vec2_3;
	// UnityEngine.Vector4 DebugMedianVector3::vec3
	Vector4_t4282066567  ___vec3_4;
	// UnityEngine.Vector4 DebugMedianVector3::vec4
	Vector4_t4282066567  ___vec4_5;
	// UnityEngine.Vector4 DebugMedianVector3::median
	Vector4_t4282066567  ___median_6;
	// UnityEngine.Vector4 DebugMedianVector3::mean
	Vector4_t4282066567  ___mean_7;
	// ARM.utils.PoolQuaternion DebugMedianVector3::pool
	PoolQuaternion_t2988316782 * ___pool_8;
	// System.Boolean DebugMedianVector3::_loaded
	bool ____loaded_9;

public:
	inline static int32_t get_offset_of_vec1_2() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___vec1_2)); }
	inline Vector4_t4282066567  get_vec1_2() const { return ___vec1_2; }
	inline Vector4_t4282066567 * get_address_of_vec1_2() { return &___vec1_2; }
	inline void set_vec1_2(Vector4_t4282066567  value)
	{
		___vec1_2 = value;
	}

	inline static int32_t get_offset_of_vec2_3() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___vec2_3)); }
	inline Vector4_t4282066567  get_vec2_3() const { return ___vec2_3; }
	inline Vector4_t4282066567 * get_address_of_vec2_3() { return &___vec2_3; }
	inline void set_vec2_3(Vector4_t4282066567  value)
	{
		___vec2_3 = value;
	}

	inline static int32_t get_offset_of_vec3_4() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___vec3_4)); }
	inline Vector4_t4282066567  get_vec3_4() const { return ___vec3_4; }
	inline Vector4_t4282066567 * get_address_of_vec3_4() { return &___vec3_4; }
	inline void set_vec3_4(Vector4_t4282066567  value)
	{
		___vec3_4 = value;
	}

	inline static int32_t get_offset_of_vec4_5() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___vec4_5)); }
	inline Vector4_t4282066567  get_vec4_5() const { return ___vec4_5; }
	inline Vector4_t4282066567 * get_address_of_vec4_5() { return &___vec4_5; }
	inline void set_vec4_5(Vector4_t4282066567  value)
	{
		___vec4_5 = value;
	}

	inline static int32_t get_offset_of_median_6() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___median_6)); }
	inline Vector4_t4282066567  get_median_6() const { return ___median_6; }
	inline Vector4_t4282066567 * get_address_of_median_6() { return &___median_6; }
	inline void set_median_6(Vector4_t4282066567  value)
	{
		___median_6 = value;
	}

	inline static int32_t get_offset_of_mean_7() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___mean_7)); }
	inline Vector4_t4282066567  get_mean_7() const { return ___mean_7; }
	inline Vector4_t4282066567 * get_address_of_mean_7() { return &___mean_7; }
	inline void set_mean_7(Vector4_t4282066567  value)
	{
		___mean_7 = value;
	}

	inline static int32_t get_offset_of_pool_8() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ___pool_8)); }
	inline PoolQuaternion_t2988316782 * get_pool_8() const { return ___pool_8; }
	inline PoolQuaternion_t2988316782 ** get_address_of_pool_8() { return &___pool_8; }
	inline void set_pool_8(PoolQuaternion_t2988316782 * value)
	{
		___pool_8 = value;
		Il2CppCodeGenWriteBarrier(&___pool_8, value);
	}

	inline static int32_t get_offset_of__loaded_9() { return static_cast<int32_t>(offsetof(DebugMedianVector3_t3206408051, ____loaded_9)); }
	inline bool get__loaded_9() const { return ____loaded_9; }
	inline bool* get_address_of__loaded_9() { return &____loaded_9; }
	inline void set__loaded_9(bool value)
	{
		____loaded_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
