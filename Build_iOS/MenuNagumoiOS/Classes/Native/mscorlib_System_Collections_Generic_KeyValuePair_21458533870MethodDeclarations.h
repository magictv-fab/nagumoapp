﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1061609505(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1458533870 *, String_t*, GroupInAppAbstractComponentsManager_t739334794 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>::get_Key()
#define KeyValuePair_2_get_Key_m1240380071(__this, method) ((  String_t* (*) (KeyValuePair_2_t1458533870 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1494428008(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1458533870 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>::get_Value()
#define KeyValuePair_2_get_Value_m3856211111(__this, method) ((  GroupInAppAbstractComponentsManager_t739334794 * (*) (KeyValuePair_2_t1458533870 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3547040104(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1458533870 *, GroupInAppAbstractComponentsManager_t739334794 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>::ToString()
#define KeyValuePair_2_ToString_m4227433018(__this, method) ((  String_t* (*) (KeyValuePair_2_t1458533870 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
