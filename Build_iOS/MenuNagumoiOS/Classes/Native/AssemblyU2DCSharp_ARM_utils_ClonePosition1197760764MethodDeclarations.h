﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.ClonePosition
struct ClonePosition_t1197760764;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.ClonePosition::.ctor()
extern "C"  void ClonePosition__ctor_m1574315252 (ClonePosition_t1197760764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.ClonePosition::Start()
extern "C"  void ClonePosition_Start_m521453044 (ClonePosition_t1197760764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.ClonePosition::Update()
extern "C"  void ClonePosition_Update_m3285994649 (ClonePosition_t1197760764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
