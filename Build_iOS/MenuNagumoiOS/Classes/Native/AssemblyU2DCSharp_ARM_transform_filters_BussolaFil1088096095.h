﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.BussolaFilter
struct  BussolaFilter_t1088096095  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// System.Boolean ARM.transform.filters.BussolaFilter::debugMode
	bool ___debugMode_15;
	// System.Single ARM.transform.filters.BussolaFilter::busolaDebugAngle
	float ___busolaDebugAngle_16;
	// System.Single ARM.transform.filters.BussolaFilter::outRotationAngle
	float ___outRotationAngle_17;
	// System.Single ARM.transform.filters.BussolaFilter::outInclinationAngle
	float ___outInclinationAngle_18;
	// System.Single ARM.transform.filters.BussolaFilter::calibration
	float ___calibration_19;
	// UnityEngine.Vector3 ARM.transform.filters.BussolaFilter::referencePoint
	Vector3_t4282066566  ___referencePoint_20;
	// System.Single ARM.transform.filters.BussolaFilter::_resetCalibration
	float ____resetCalibration_21;
	// System.Single ARM.transform.filters.BussolaFilter::roll
	float ___roll_22;
	// System.Single ARM.transform.filters.BussolaFilter::magneticHeading
	float ___magneticHeading_23;
	// UnityEngine.Vector3 ARM.transform.filters.BussolaFilter::DebugPositionNext
	Vector3_t4282066566  ___DebugPositionNext_24;

public:
	inline static int32_t get_offset_of_debugMode_15() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___debugMode_15)); }
	inline bool get_debugMode_15() const { return ___debugMode_15; }
	inline bool* get_address_of_debugMode_15() { return &___debugMode_15; }
	inline void set_debugMode_15(bool value)
	{
		___debugMode_15 = value;
	}

	inline static int32_t get_offset_of_busolaDebugAngle_16() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___busolaDebugAngle_16)); }
	inline float get_busolaDebugAngle_16() const { return ___busolaDebugAngle_16; }
	inline float* get_address_of_busolaDebugAngle_16() { return &___busolaDebugAngle_16; }
	inline void set_busolaDebugAngle_16(float value)
	{
		___busolaDebugAngle_16 = value;
	}

	inline static int32_t get_offset_of_outRotationAngle_17() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___outRotationAngle_17)); }
	inline float get_outRotationAngle_17() const { return ___outRotationAngle_17; }
	inline float* get_address_of_outRotationAngle_17() { return &___outRotationAngle_17; }
	inline void set_outRotationAngle_17(float value)
	{
		___outRotationAngle_17 = value;
	}

	inline static int32_t get_offset_of_outInclinationAngle_18() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___outInclinationAngle_18)); }
	inline float get_outInclinationAngle_18() const { return ___outInclinationAngle_18; }
	inline float* get_address_of_outInclinationAngle_18() { return &___outInclinationAngle_18; }
	inline void set_outInclinationAngle_18(float value)
	{
		___outInclinationAngle_18 = value;
	}

	inline static int32_t get_offset_of_calibration_19() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___calibration_19)); }
	inline float get_calibration_19() const { return ___calibration_19; }
	inline float* get_address_of_calibration_19() { return &___calibration_19; }
	inline void set_calibration_19(float value)
	{
		___calibration_19 = value;
	}

	inline static int32_t get_offset_of_referencePoint_20() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___referencePoint_20)); }
	inline Vector3_t4282066566  get_referencePoint_20() const { return ___referencePoint_20; }
	inline Vector3_t4282066566 * get_address_of_referencePoint_20() { return &___referencePoint_20; }
	inline void set_referencePoint_20(Vector3_t4282066566  value)
	{
		___referencePoint_20 = value;
	}

	inline static int32_t get_offset_of__resetCalibration_21() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ____resetCalibration_21)); }
	inline float get__resetCalibration_21() const { return ____resetCalibration_21; }
	inline float* get_address_of__resetCalibration_21() { return &____resetCalibration_21; }
	inline void set__resetCalibration_21(float value)
	{
		____resetCalibration_21 = value;
	}

	inline static int32_t get_offset_of_roll_22() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___roll_22)); }
	inline float get_roll_22() const { return ___roll_22; }
	inline float* get_address_of_roll_22() { return &___roll_22; }
	inline void set_roll_22(float value)
	{
		___roll_22 = value;
	}

	inline static int32_t get_offset_of_magneticHeading_23() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___magneticHeading_23)); }
	inline float get_magneticHeading_23() const { return ___magneticHeading_23; }
	inline float* get_address_of_magneticHeading_23() { return &___magneticHeading_23; }
	inline void set_magneticHeading_23(float value)
	{
		___magneticHeading_23 = value;
	}

	inline static int32_t get_offset_of_DebugPositionNext_24() { return static_cast<int32_t>(offsetof(BussolaFilter_t1088096095, ___DebugPositionNext_24)); }
	inline Vector3_t4282066566  get_DebugPositionNext_24() const { return ___DebugPositionNext_24; }
	inline Vector3_t4282066566 * get_address_of_DebugPositionNext_24() { return &___DebugPositionNext_24; }
	inline void set_DebugPositionNext_24(Vector3_t4282066566  value)
	{
		___DebugPositionNext_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
