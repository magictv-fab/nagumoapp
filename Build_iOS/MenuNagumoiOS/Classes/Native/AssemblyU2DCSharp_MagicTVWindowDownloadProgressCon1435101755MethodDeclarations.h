﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowDownloadProgressControllerScript
struct MagicTVWindowDownloadProgressControllerScript_t1435101755;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowDownloadProgressControllerScript::.ctor()
extern "C"  void MagicTVWindowDownloadProgressControllerScript__ctor_m1070016912 (MagicTVWindowDownloadProgressControllerScript_t1435101755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
