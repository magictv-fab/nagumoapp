﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 ZXing.OneD.RSS.FinderPattern::get_Value()
extern "C"  int32_t FinderPattern_get_Value_m3117908811 (FinderPattern_t3366792524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.FinderPattern::set_Value(System.Int32)
extern "C"  void FinderPattern_set_Value_m99375542 (FinderPattern_t3366792524 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.RSS.FinderPattern::get_StartEnd()
extern "C"  Int32U5BU5D_t3230847821* FinderPattern_get_StartEnd_m2863987327 (FinderPattern_t3366792524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.FinderPattern::set_StartEnd(System.Int32[])
extern "C"  void FinderPattern_set_StartEnd_m3422259510 (FinderPattern_t3366792524 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.OneD.RSS.FinderPattern::get_ResultPoints()
extern "C"  ResultPointU5BU5D_t1195164344* FinderPattern_get_ResultPoints_m1306167906 (FinderPattern_t3366792524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.FinderPattern::set_ResultPoints(ZXing.ResultPoint[])
extern "C"  void FinderPattern_set_ResultPoints_m3256295641 (FinderPattern_t3366792524 * __this, ResultPointU5BU5D_t1195164344* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.FinderPattern::.ctor(System.Int32,System.Int32[],System.Int32,System.Int32,System.Int32)
extern "C"  void FinderPattern__ctor_m1887177190 (FinderPattern_t3366792524 * __this, int32_t ___value0, Int32U5BU5D_t3230847821* ___startEnd1, int32_t ___start2, int32_t ___end3, int32_t ___rowNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.FinderPattern::Equals(System.Object)
extern "C"  bool FinderPattern_Equals_m2407170224 (FinderPattern_t3366792524 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.FinderPattern::GetHashCode()
extern "C"  int32_t FinderPattern_GetHashCode_m1670228692 (FinderPattern_t3366792524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
