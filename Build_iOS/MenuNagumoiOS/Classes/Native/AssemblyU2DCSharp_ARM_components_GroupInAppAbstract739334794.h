﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.abstracts.InAppAbstract[]
struct InAppAbstractU5BU5D_t412350073;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.components.GroupInAppAbstractComponentsManager
struct  GroupInAppAbstractComponentsManager_t739334794  : public InAppAbstract_t382673128
{
public:
	// MagicTV.vo.BundleVO ARM.components.GroupInAppAbstractComponentsManager::_bundleVO
	BundleVO_t1984518073 * ____bundleVO_14;
	// MagicTV.abstracts.InAppAbstract[] ARM.components.GroupInAppAbstractComponentsManager::_components
	InAppAbstractU5BU5D_t412350073* ____components_15;
	// System.Int32 ARM.components.GroupInAppAbstractComponentsManager::DebugTotalComponents
	int32_t ___DebugTotalComponents_16;
	// System.Boolean ARM.components.GroupInAppAbstractComponentsManager::keepGameObjectHierarchy
	bool ___keepGameObjectHierarchy_17;
	// UnityEngine.GameObject[] ARM.components.GroupInAppAbstractComponentsManager::_debugInsideInApps
	GameObjectU5BU5D_t2662109048* ____debugInsideInApps_18;
	// System.Boolean ARM.components.GroupInAppAbstractComponentsManager::_preparing
	bool ____preparing_19;

public:
	inline static int32_t get_offset_of__bundleVO_14() { return static_cast<int32_t>(offsetof(GroupInAppAbstractComponentsManager_t739334794, ____bundleVO_14)); }
	inline BundleVO_t1984518073 * get__bundleVO_14() const { return ____bundleVO_14; }
	inline BundleVO_t1984518073 ** get_address_of__bundleVO_14() { return &____bundleVO_14; }
	inline void set__bundleVO_14(BundleVO_t1984518073 * value)
	{
		____bundleVO_14 = value;
		Il2CppCodeGenWriteBarrier(&____bundleVO_14, value);
	}

	inline static int32_t get_offset_of__components_15() { return static_cast<int32_t>(offsetof(GroupInAppAbstractComponentsManager_t739334794, ____components_15)); }
	inline InAppAbstractU5BU5D_t412350073* get__components_15() const { return ____components_15; }
	inline InAppAbstractU5BU5D_t412350073** get_address_of__components_15() { return &____components_15; }
	inline void set__components_15(InAppAbstractU5BU5D_t412350073* value)
	{
		____components_15 = value;
		Il2CppCodeGenWriteBarrier(&____components_15, value);
	}

	inline static int32_t get_offset_of_DebugTotalComponents_16() { return static_cast<int32_t>(offsetof(GroupInAppAbstractComponentsManager_t739334794, ___DebugTotalComponents_16)); }
	inline int32_t get_DebugTotalComponents_16() const { return ___DebugTotalComponents_16; }
	inline int32_t* get_address_of_DebugTotalComponents_16() { return &___DebugTotalComponents_16; }
	inline void set_DebugTotalComponents_16(int32_t value)
	{
		___DebugTotalComponents_16 = value;
	}

	inline static int32_t get_offset_of_keepGameObjectHierarchy_17() { return static_cast<int32_t>(offsetof(GroupInAppAbstractComponentsManager_t739334794, ___keepGameObjectHierarchy_17)); }
	inline bool get_keepGameObjectHierarchy_17() const { return ___keepGameObjectHierarchy_17; }
	inline bool* get_address_of_keepGameObjectHierarchy_17() { return &___keepGameObjectHierarchy_17; }
	inline void set_keepGameObjectHierarchy_17(bool value)
	{
		___keepGameObjectHierarchy_17 = value;
	}

	inline static int32_t get_offset_of__debugInsideInApps_18() { return static_cast<int32_t>(offsetof(GroupInAppAbstractComponentsManager_t739334794, ____debugInsideInApps_18)); }
	inline GameObjectU5BU5D_t2662109048* get__debugInsideInApps_18() const { return ____debugInsideInApps_18; }
	inline GameObjectU5BU5D_t2662109048** get_address_of__debugInsideInApps_18() { return &____debugInsideInApps_18; }
	inline void set__debugInsideInApps_18(GameObjectU5BU5D_t2662109048* value)
	{
		____debugInsideInApps_18 = value;
		Il2CppCodeGenWriteBarrier(&____debugInsideInApps_18, value);
	}

	inline static int32_t get_offset_of__preparing_19() { return static_cast<int32_t>(offsetof(GroupInAppAbstractComponentsManager_t739334794, ____preparing_19)); }
	inline bool get__preparing_19() const { return ____preparing_19; }
	inline bool* get_address_of__preparing_19() { return &____preparing_19; }
	inline void set__preparing_19(bool value)
	{
		____preparing_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
