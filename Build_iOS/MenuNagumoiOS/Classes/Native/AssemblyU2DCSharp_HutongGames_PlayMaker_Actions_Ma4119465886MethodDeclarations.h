﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerRequestHostList
struct MasterServerRequestHostList_t4119465886;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::.ctor()
extern "C"  void MasterServerRequestHostList__ctor_m3999972696 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::Reset()
extern "C"  void MasterServerRequestHostList_Reset_m1646405637 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::OnEnter()
extern "C"  void MasterServerRequestHostList_OnEnter_m3664726831 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::OnUpdate()
extern "C"  void MasterServerRequestHostList_OnUpdate_m1070941428 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::DoMasterServerRequestHost()
extern "C"  void MasterServerRequestHostList_DoMasterServerRequestHost_m3691053693 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::WatchServerRequestHost()
extern "C"  void MasterServerRequestHostList_WatchServerRequestHost_m3633021809 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
