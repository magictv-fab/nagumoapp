﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler
struct OnFileDonwloadCompleteEventHandler_t3421606269;
// System.Object
struct Il2CppObject;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFileDonwloadCompleteEventHandler__ctor_m1171073956 (OnFileDonwloadCompleteEventHandler_t3421606269 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler::Invoke(MagicTV.vo.UrlInfoVO,UnityEngine.WWW)
extern "C"  void OnFileDonwloadCompleteEventHandler_Invoke_m527011274 (OnFileDonwloadCompleteEventHandler_t3421606269 * __this, UrlInfoVO_t1761987528 * ___vo0, WWW_t3134621005 * ___www1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler::BeginInvoke(MagicTV.vo.UrlInfoVO,UnityEngine.WWW,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFileDonwloadCompleteEventHandler_BeginInvoke_m1349995629 (OnFileDonwloadCompleteEventHandler_t3421606269 * __this, UrlInfoVO_t1761987528 * ___vo0, WWW_t3134621005 * ___www1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnFileDonwloadCompleteEventHandler_EndInvoke_m246419380 (OnFileDonwloadCompleteEventHandler_t3421606269 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
