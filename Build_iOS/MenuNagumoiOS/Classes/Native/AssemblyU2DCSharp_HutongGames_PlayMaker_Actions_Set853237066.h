﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetEventData
struct  SetEventData_t853237066  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetEventData::setGameObjectData
	FsmGameObject_t1697147867 * ___setGameObjectData_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetEventData::setIntData
	FsmInt_t1596138449 * ___setIntData_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetEventData::setFloatData
	FsmFloat_t2134102846 * ___setFloatData_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetEventData::setStringData
	FsmString_t952858651 * ___setStringData_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetEventData::setBoolData
	FsmBool_t1075959796 * ___setBoolData_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetEventData::setVector2Data
	FsmVector2_t533912881 * ___setVector2Data_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetEventData::setVector3Data
	FsmVector3_t533912882 * ___setVector3Data_15;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetEventData::setRectData
	FsmRect_t1076426478 * ___setRectData_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetEventData::setQuaternionData
	FsmQuaternion_t3871136040 * ___setQuaternionData_17;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetEventData::setColorData
	FsmColor_t2131419205 * ___setColorData_18;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetEventData::setMaterialData
	FsmMaterial_t924399665 * ___setMaterialData_19;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetEventData::setTextureData
	FsmTexture_t3073272573 * ___setTextureData_20;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetEventData::setObjectData
	FsmObject_t821476169 * ___setObjectData_21;

public:
	inline static int32_t get_offset_of_setGameObjectData_9() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setGameObjectData_9)); }
	inline FsmGameObject_t1697147867 * get_setGameObjectData_9() const { return ___setGameObjectData_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_setGameObjectData_9() { return &___setGameObjectData_9; }
	inline void set_setGameObjectData_9(FsmGameObject_t1697147867 * value)
	{
		___setGameObjectData_9 = value;
		Il2CppCodeGenWriteBarrier(&___setGameObjectData_9, value);
	}

	inline static int32_t get_offset_of_setIntData_10() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setIntData_10)); }
	inline FsmInt_t1596138449 * get_setIntData_10() const { return ___setIntData_10; }
	inline FsmInt_t1596138449 ** get_address_of_setIntData_10() { return &___setIntData_10; }
	inline void set_setIntData_10(FsmInt_t1596138449 * value)
	{
		___setIntData_10 = value;
		Il2CppCodeGenWriteBarrier(&___setIntData_10, value);
	}

	inline static int32_t get_offset_of_setFloatData_11() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setFloatData_11)); }
	inline FsmFloat_t2134102846 * get_setFloatData_11() const { return ___setFloatData_11; }
	inline FsmFloat_t2134102846 ** get_address_of_setFloatData_11() { return &___setFloatData_11; }
	inline void set_setFloatData_11(FsmFloat_t2134102846 * value)
	{
		___setFloatData_11 = value;
		Il2CppCodeGenWriteBarrier(&___setFloatData_11, value);
	}

	inline static int32_t get_offset_of_setStringData_12() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setStringData_12)); }
	inline FsmString_t952858651 * get_setStringData_12() const { return ___setStringData_12; }
	inline FsmString_t952858651 ** get_address_of_setStringData_12() { return &___setStringData_12; }
	inline void set_setStringData_12(FsmString_t952858651 * value)
	{
		___setStringData_12 = value;
		Il2CppCodeGenWriteBarrier(&___setStringData_12, value);
	}

	inline static int32_t get_offset_of_setBoolData_13() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setBoolData_13)); }
	inline FsmBool_t1075959796 * get_setBoolData_13() const { return ___setBoolData_13; }
	inline FsmBool_t1075959796 ** get_address_of_setBoolData_13() { return &___setBoolData_13; }
	inline void set_setBoolData_13(FsmBool_t1075959796 * value)
	{
		___setBoolData_13 = value;
		Il2CppCodeGenWriteBarrier(&___setBoolData_13, value);
	}

	inline static int32_t get_offset_of_setVector2Data_14() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setVector2Data_14)); }
	inline FsmVector2_t533912881 * get_setVector2Data_14() const { return ___setVector2Data_14; }
	inline FsmVector2_t533912881 ** get_address_of_setVector2Data_14() { return &___setVector2Data_14; }
	inline void set_setVector2Data_14(FsmVector2_t533912881 * value)
	{
		___setVector2Data_14 = value;
		Il2CppCodeGenWriteBarrier(&___setVector2Data_14, value);
	}

	inline static int32_t get_offset_of_setVector3Data_15() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setVector3Data_15)); }
	inline FsmVector3_t533912882 * get_setVector3Data_15() const { return ___setVector3Data_15; }
	inline FsmVector3_t533912882 ** get_address_of_setVector3Data_15() { return &___setVector3Data_15; }
	inline void set_setVector3Data_15(FsmVector3_t533912882 * value)
	{
		___setVector3Data_15 = value;
		Il2CppCodeGenWriteBarrier(&___setVector3Data_15, value);
	}

	inline static int32_t get_offset_of_setRectData_16() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setRectData_16)); }
	inline FsmRect_t1076426478 * get_setRectData_16() const { return ___setRectData_16; }
	inline FsmRect_t1076426478 ** get_address_of_setRectData_16() { return &___setRectData_16; }
	inline void set_setRectData_16(FsmRect_t1076426478 * value)
	{
		___setRectData_16 = value;
		Il2CppCodeGenWriteBarrier(&___setRectData_16, value);
	}

	inline static int32_t get_offset_of_setQuaternionData_17() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setQuaternionData_17)); }
	inline FsmQuaternion_t3871136040 * get_setQuaternionData_17() const { return ___setQuaternionData_17; }
	inline FsmQuaternion_t3871136040 ** get_address_of_setQuaternionData_17() { return &___setQuaternionData_17; }
	inline void set_setQuaternionData_17(FsmQuaternion_t3871136040 * value)
	{
		___setQuaternionData_17 = value;
		Il2CppCodeGenWriteBarrier(&___setQuaternionData_17, value);
	}

	inline static int32_t get_offset_of_setColorData_18() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setColorData_18)); }
	inline FsmColor_t2131419205 * get_setColorData_18() const { return ___setColorData_18; }
	inline FsmColor_t2131419205 ** get_address_of_setColorData_18() { return &___setColorData_18; }
	inline void set_setColorData_18(FsmColor_t2131419205 * value)
	{
		___setColorData_18 = value;
		Il2CppCodeGenWriteBarrier(&___setColorData_18, value);
	}

	inline static int32_t get_offset_of_setMaterialData_19() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setMaterialData_19)); }
	inline FsmMaterial_t924399665 * get_setMaterialData_19() const { return ___setMaterialData_19; }
	inline FsmMaterial_t924399665 ** get_address_of_setMaterialData_19() { return &___setMaterialData_19; }
	inline void set_setMaterialData_19(FsmMaterial_t924399665 * value)
	{
		___setMaterialData_19 = value;
		Il2CppCodeGenWriteBarrier(&___setMaterialData_19, value);
	}

	inline static int32_t get_offset_of_setTextureData_20() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setTextureData_20)); }
	inline FsmTexture_t3073272573 * get_setTextureData_20() const { return ___setTextureData_20; }
	inline FsmTexture_t3073272573 ** get_address_of_setTextureData_20() { return &___setTextureData_20; }
	inline void set_setTextureData_20(FsmTexture_t3073272573 * value)
	{
		___setTextureData_20 = value;
		Il2CppCodeGenWriteBarrier(&___setTextureData_20, value);
	}

	inline static int32_t get_offset_of_setObjectData_21() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setObjectData_21)); }
	inline FsmObject_t821476169 * get_setObjectData_21() const { return ___setObjectData_21; }
	inline FsmObject_t821476169 ** get_address_of_setObjectData_21() { return &___setObjectData_21; }
	inline void set_setObjectData_21(FsmObject_t821476169 * value)
	{
		___setObjectData_21 = value;
		Il2CppCodeGenWriteBarrier(&___setObjectData_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
