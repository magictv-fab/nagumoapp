﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.download.DownloaderAbstract
struct DownloaderAbstract_t3533812091;
// UnityEngine.WWW
struct WWW_t3134621005;
// ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler
struct OnDownloadCompleteEventHandler_t1994402001;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_ARM_utils_download_DownloaderAbs1994402001.h"

// System.Void ARM.utils.download.DownloaderAbstract::.ctor()
extern "C"  void DownloaderAbstract__ctor_m2032687709 (DownloaderAbstract_t3533812091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.DownloaderAbstract::RaiseDownloadComplete(UnityEngine.WWW)
extern "C"  void DownloaderAbstract_RaiseDownloadComplete_m637940554 (DownloaderAbstract_t3533812091 * __this, WWW_t3134621005 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.DownloaderAbstract::AddDownloadCompleteEventhandler(ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler)
extern "C"  void DownloaderAbstract_AddDownloadCompleteEventhandler_m1047325436 (DownloaderAbstract_t3533812091 * __this, OnDownloadCompleteEventHandler_t1994402001 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.DownloaderAbstract::RemoveDownloadCompleteEventhandler(ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler)
extern "C"  void DownloaderAbstract_RemoveDownloadCompleteEventhandler_m2560036331 (DownloaderAbstract_t3533812091 * __this, OnDownloadCompleteEventHandler_t1994402001 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
