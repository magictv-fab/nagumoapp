﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1304513828(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2116912562 *, String_t*, InAppEventsChannel_t1397713486 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>::get_Key()
#define KeyValuePair_2_get_Key_m3742664260(__this, method) ((  String_t* (*) (KeyValuePair_2_t2116912562 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3081987717(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2116912562 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>::get_Value()
#define KeyValuePair_2_get_Value_m341612804(__this, method) ((  InAppEventsChannel_t1397713486 * (*) (KeyValuePair_2_t2116912562 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1061023749(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2116912562 *, InAppEventsChannel_t1397713486 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,MagicTV.in_apps.InAppEventsChannel>::ToString()
#define KeyValuePair_2_ToString_m859946045(__this, method) ((  String_t* (*) (KeyValuePair_2_t2116912562 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
