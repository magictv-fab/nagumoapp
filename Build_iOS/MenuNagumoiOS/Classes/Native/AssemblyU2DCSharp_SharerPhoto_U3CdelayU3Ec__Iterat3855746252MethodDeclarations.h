﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharerPhoto/<delay>c__Iterator24
struct U3CdelayU3Ec__Iterator24_t3855746252;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SharerPhoto/<delay>c__Iterator24::.ctor()
extern "C"  void U3CdelayU3Ec__Iterator24__ctor_m2449381199 (U3CdelayU3Ec__Iterator24_t3855746252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SharerPhoto/<delay>c__Iterator24::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdelayU3Ec__Iterator24_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1640494573 (U3CdelayU3Ec__Iterator24_t3855746252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SharerPhoto/<delay>c__Iterator24::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdelayU3Ec__Iterator24_System_Collections_IEnumerator_get_Current_m1349874561 (U3CdelayU3Ec__Iterator24_t3855746252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SharerPhoto/<delay>c__Iterator24::MoveNext()
extern "C"  bool U3CdelayU3Ec__Iterator24_MoveNext_m1258838061 (U3CdelayU3Ec__Iterator24_t3855746252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto/<delay>c__Iterator24::Dispose()
extern "C"  void U3CdelayU3Ec__Iterator24_Dispose_m111537100 (U3CdelayU3Ec__Iterator24_t3855746252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto/<delay>c__Iterator24::Reset()
extern "C"  void U3CdelayU3Ec__Iterator24_Reset_m95814140 (U3CdelayU3Ec__Iterator24_t3855746252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
