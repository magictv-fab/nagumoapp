﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadMainScene/<FirstStart>c__Iterator3A
struct U3CFirstStartU3Ec__Iterator3A_t648989837;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadMainScene/<FirstStart>c__Iterator3A::.ctor()
extern "C"  void U3CFirstStartU3Ec__Iterator3A__ctor_m3107819902 (U3CFirstStartU3Ec__Iterator3A_t648989837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadMainScene/<FirstStart>c__Iterator3A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFirstStartU3Ec__Iterator3A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3552491924 (U3CFirstStartU3Ec__Iterator3A_t648989837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadMainScene/<FirstStart>c__Iterator3A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFirstStartU3Ec__Iterator3A_System_Collections_IEnumerator_get_Current_m2308859176 (U3CFirstStartU3Ec__Iterator3A_t648989837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadMainScene/<FirstStart>c__Iterator3A::MoveNext()
extern "C"  bool U3CFirstStartU3Ec__Iterator3A_MoveNext_m2637604790 (U3CFirstStartU3Ec__Iterator3A_t648989837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene/<FirstStart>c__Iterator3A::Dispose()
extern "C"  void U3CFirstStartU3Ec__Iterator3A_Dispose_m1510938171 (U3CFirstStartU3Ec__Iterator3A_t648989837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene/<FirstStart>c__Iterator3A::Reset()
extern "C"  void U3CFirstStartU3Ec__Iterator3A_Reset_m754252843 (U3CFirstStartU3Ec__Iterator3A_t648989837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
