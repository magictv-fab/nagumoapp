﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CuponsControllerNagumo
struct CuponsControllerNagumo_t4205714869;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CuponsControllerNagumo::.ctor()
extern "C"  void CuponsControllerNagumo__ctor_m3519787654 (CuponsControllerNagumo_t4205714869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControllerNagumo::Start()
extern "C"  void CuponsControllerNagumo_Start_m2466925446 (CuponsControllerNagumo_t4205714869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControllerNagumo::ClearAll()
extern "C"  void CuponsControllerNagumo_ClearAll_m1856086674 (CuponsControllerNagumo_t4205714869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControllerNagumo::Instantiation()
extern "C"  void CuponsControllerNagumo_Instantiation_m971256497 (CuponsControllerNagumo_t4205714869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControllerNagumo::Inst(System.Int32)
extern "C"  void CuponsControllerNagumo_Inst_m3476368917 (CuponsControllerNagumo_t4205714869 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControllerNagumo::UpdateTxt()
extern "C"  void CuponsControllerNagumo_UpdateTxt_m3229081931 (CuponsControllerNagumo_t4205714869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControllerNagumo::PopUp(System.String)
extern "C"  void CuponsControllerNagumo_PopUp_m1468920722 (CuponsControllerNagumo_t4205714869 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
