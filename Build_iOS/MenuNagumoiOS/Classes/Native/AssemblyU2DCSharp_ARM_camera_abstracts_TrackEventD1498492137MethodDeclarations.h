﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack
struct OnTrack_t1498492137;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTrack__ctor_m648288320 (OnTrack_t1498492137 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack::Invoke()
extern "C"  void OnTrack_Invoke_m3351942298 (OnTrack_t1498492137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnTrack_BeginInvoke_m3328825521 (OnTrack_t1498492137 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack::EndInvoke(System.IAsyncResult)
extern "C"  void OnTrack_EndInvoke_m55421520 (OnTrack_t1498492137 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
