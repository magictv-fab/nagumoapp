﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.Internal.BitMatrixParser
struct  BitMatrixParser_t4050453827  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Maxicode.Internal.BitMatrixParser::bitMatrix
	BitMatrix_t1058711404 * ___bitMatrix_1;

public:
	inline static int32_t get_offset_of_bitMatrix_1() { return static_cast<int32_t>(offsetof(BitMatrixParser_t4050453827, ___bitMatrix_1)); }
	inline BitMatrix_t1058711404 * get_bitMatrix_1() const { return ___bitMatrix_1; }
	inline BitMatrix_t1058711404 ** get_address_of_bitMatrix_1() { return &___bitMatrix_1; }
	inline void set_bitMatrix_1(BitMatrix_t1058711404 * value)
	{
		___bitMatrix_1 = value;
		Il2CppCodeGenWriteBarrier(&___bitMatrix_1, value);
	}
};

struct BitMatrixParser_t4050453827_StaticFields
{
public:
	// System.Int32[][] ZXing.Maxicode.Internal.BitMatrixParser::BITNR
	Int32U5BU5DU5BU5D_t1820556512* ___BITNR_0;

public:
	inline static int32_t get_offset_of_BITNR_0() { return static_cast<int32_t>(offsetof(BitMatrixParser_t4050453827_StaticFields, ___BITNR_0)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_BITNR_0() const { return ___BITNR_0; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_BITNR_0() { return &___BITNR_0; }
	inline void set_BITNR_0(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___BITNR_0 = value;
		Il2CppCodeGenWriteBarrier(&___BITNR_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
