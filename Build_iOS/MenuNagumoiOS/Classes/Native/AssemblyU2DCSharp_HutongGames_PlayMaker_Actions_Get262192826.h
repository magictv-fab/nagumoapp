﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetDeviceAcceleration
struct  GetDeviceAcceleration_t262192826  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeVector
	FsmVector3_t533912882 * ___storeVector_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeX
	FsmFloat_t2134102846 * ___storeX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeY
	FsmFloat_t2134102846 * ___storeY_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeZ
	FsmFloat_t2134102846 * ___storeZ_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::multiplier
	FsmFloat_t2134102846 * ___multiplier_13;
	// System.Boolean HutongGames.PlayMaker.Actions.GetDeviceAcceleration::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_storeVector_9() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t262192826, ___storeVector_9)); }
	inline FsmVector3_t533912882 * get_storeVector_9() const { return ___storeVector_9; }
	inline FsmVector3_t533912882 ** get_address_of_storeVector_9() { return &___storeVector_9; }
	inline void set_storeVector_9(FsmVector3_t533912882 * value)
	{
		___storeVector_9 = value;
		Il2CppCodeGenWriteBarrier(&___storeVector_9, value);
	}

	inline static int32_t get_offset_of_storeX_10() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t262192826, ___storeX_10)); }
	inline FsmFloat_t2134102846 * get_storeX_10() const { return ___storeX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeX_10() { return &___storeX_10; }
	inline void set_storeX_10(FsmFloat_t2134102846 * value)
	{
		___storeX_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeX_10, value);
	}

	inline static int32_t get_offset_of_storeY_11() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t262192826, ___storeY_11)); }
	inline FsmFloat_t2134102846 * get_storeY_11() const { return ___storeY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_storeY_11() { return &___storeY_11; }
	inline void set_storeY_11(FsmFloat_t2134102846 * value)
	{
		___storeY_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeY_11, value);
	}

	inline static int32_t get_offset_of_storeZ_12() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t262192826, ___storeZ_12)); }
	inline FsmFloat_t2134102846 * get_storeZ_12() const { return ___storeZ_12; }
	inline FsmFloat_t2134102846 ** get_address_of_storeZ_12() { return &___storeZ_12; }
	inline void set_storeZ_12(FsmFloat_t2134102846 * value)
	{
		___storeZ_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeZ_12, value);
	}

	inline static int32_t get_offset_of_multiplier_13() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t262192826, ___multiplier_13)); }
	inline FsmFloat_t2134102846 * get_multiplier_13() const { return ___multiplier_13; }
	inline FsmFloat_t2134102846 ** get_address_of_multiplier_13() { return &___multiplier_13; }
	inline void set_multiplier_13(FsmFloat_t2134102846 * value)
	{
		___multiplier_13 = value;
		Il2CppCodeGenWriteBarrier(&___multiplier_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t262192826, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
