﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2780069266(__this, ___l0, method) ((  void (*) (Enumerator_t2583022666 *, List_1_t2563349896 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2177033088(__this, method) ((  void (*) (Enumerator_t2583022666 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m392083564(__this, method) ((  Il2CppObject * (*) (Enumerator_t2583022666 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::Dispose()
#define Enumerator_Dispose_m2811498487(__this, method) ((  void (*) (Enumerator_t2583022666 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::VerifyState()
#define Enumerator_VerifyState_m130087856(__this, method) ((  void (*) (Enumerator_t2583022666 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::MoveNext()
#define Enumerator_MoveNext_m98448067(__this, method) ((  bool (*) (Enumerator_t2583022666 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.ResultPoint[]>::get_Current()
#define Enumerator_get_Current_m2952360173(__this, method) ((  ResultPointU5BU5D_t1195164344* (*) (Enumerator_t2583022666 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
