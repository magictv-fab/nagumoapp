﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2632148816.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkDestroy
struct  NetworkDestroy_t2935674068  : public ComponentAction_1_t2632148816
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.NetworkDestroy::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkDestroy::removeRPCs
	FsmBool_t1075959796 * ___removeRPCs_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(NetworkDestroy_t2935674068, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_removeRPCs_12() { return static_cast<int32_t>(offsetof(NetworkDestroy_t2935674068, ___removeRPCs_12)); }
	inline FsmBool_t1075959796 * get_removeRPCs_12() const { return ___removeRPCs_12; }
	inline FsmBool_t1075959796 ** get_address_of_removeRPCs_12() { return &___removeRPCs_12; }
	inline void set_removeRPCs_12(FsmBool_t1075959796 * value)
	{
		___removeRPCs_12 = value;
		Il2CppCodeGenWriteBarrier(&___removeRPCs_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
