﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName
struct GetAnimatorCurrentTransitionInfoIsUserName_t1580140583;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName__ctor_m3001526303 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_Reset_m647959244 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnEnter()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnUpdate_m3297939661 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::IsName()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
