﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35
struct U3CdoLoadU3Ec__Iterator35_t862749232;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35::.ctor()
extern "C"  void U3CdoLoadU3Ec__Iterator35__ctor_m447276347 (U3CdoLoadU3Ec__Iterator35_t862749232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdoLoadU3Ec__Iterator35_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1597165495 (U3CdoLoadU3Ec__Iterator35_t862749232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdoLoadU3Ec__Iterator35_System_Collections_IEnumerator_get_Current_m4073791819 (U3CdoLoadU3Ec__Iterator35_t862749232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35::MoveNext()
extern "C"  bool U3CdoLoadU3Ec__Iterator35_MoveNext_m737508825 (U3CdoLoadU3Ec__Iterator35_t862749232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35::Dispose()
extern "C"  void U3CdoLoadU3Ec__Iterator35_Dispose_m234122936 (U3CdoLoadU3Ec__Iterator35_t862749232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader/<doLoad>c__Iterator35::Reset()
extern "C"  void U3CdoLoadU3Ec__Iterator35_Reset_m2388676584 (U3CdoLoadU3Ec__Iterator35_t862749232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
