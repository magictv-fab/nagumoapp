﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs
struct KeysRequiredEventArgs_t1890758614;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::.ctor(System.String)
extern "C"  void KeysRequiredEventArgs__ctor_m566883261 (KeysRequiredEventArgs_t1890758614 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::.ctor(System.String,System.Byte[])
extern "C"  void KeysRequiredEventArgs__ctor_m11775488 (KeysRequiredEventArgs_t1890758614 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___keyValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::get_FileName()
extern "C"  String_t* KeysRequiredEventArgs_get_FileName_m2617243244 (KeysRequiredEventArgs_t1890758614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::get_Key()
extern "C"  ByteU5BU5D_t4260760469* KeysRequiredEventArgs_get_Key_m432406627 (KeysRequiredEventArgs_t1890758614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::set_Key(System.Byte[])
extern "C"  void KeysRequiredEventArgs_set_Key_m3919240036 (KeysRequiredEventArgs_t1890758614 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
