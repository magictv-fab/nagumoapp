﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertaData[]
struct OfertaDataU5BU5D_t382376346;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasData
struct  OfertasData_t2909397772  : public Il2CppObject
{
public:
	// OfertaData[] OfertasData::ofertas
	OfertaDataU5BU5D_t382376346* ___ofertas_0;

public:
	inline static int32_t get_offset_of_ofertas_0() { return static_cast<int32_t>(offsetof(OfertasData_t2909397772, ___ofertas_0)); }
	inline OfertaDataU5BU5D_t382376346* get_ofertas_0() const { return ___ofertas_0; }
	inline OfertaDataU5BU5D_t382376346** get_address_of_ofertas_0() { return &___ofertas_0; }
	inline void set_ofertas_0(OfertaDataU5BU5D_t382376346* value)
	{
		___ofertas_0 = value;
		Il2CppCodeGenWriteBarrier(&___ofertas_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
