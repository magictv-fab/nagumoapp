﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.download.BundleDownloadItem
struct BundleDownloadItem_t715938369;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.download.BundleDownloadItem::.ctor()
extern "C"  void BundleDownloadItem__ctor_m2893606887 (BundleDownloadItem_t715938369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
