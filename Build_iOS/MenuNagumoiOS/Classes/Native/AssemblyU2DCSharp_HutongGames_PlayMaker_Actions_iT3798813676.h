﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_iTweenFSMType470630072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenStop
struct  iTweenStop_t3798813676  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenStop::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenStop::id
	FsmString_t952858651 * ___id_10;
	// iTweenFSMType HutongGames.PlayMaker.Actions.iTweenStop::iTweenType
	int32_t ___iTweenType_11;
	// System.Boolean HutongGames.PlayMaker.Actions.iTweenStop::includeChildren
	bool ___includeChildren_12;
	// System.Boolean HutongGames.PlayMaker.Actions.iTweenStop::inScene
	bool ___inScene_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(iTweenStop_t3798813676, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_id_10() { return static_cast<int32_t>(offsetof(iTweenStop_t3798813676, ___id_10)); }
	inline FsmString_t952858651 * get_id_10() const { return ___id_10; }
	inline FsmString_t952858651 ** get_address_of_id_10() { return &___id_10; }
	inline void set_id_10(FsmString_t952858651 * value)
	{
		___id_10 = value;
		Il2CppCodeGenWriteBarrier(&___id_10, value);
	}

	inline static int32_t get_offset_of_iTweenType_11() { return static_cast<int32_t>(offsetof(iTweenStop_t3798813676, ___iTweenType_11)); }
	inline int32_t get_iTweenType_11() const { return ___iTweenType_11; }
	inline int32_t* get_address_of_iTweenType_11() { return &___iTweenType_11; }
	inline void set_iTweenType_11(int32_t value)
	{
		___iTweenType_11 = value;
	}

	inline static int32_t get_offset_of_includeChildren_12() { return static_cast<int32_t>(offsetof(iTweenStop_t3798813676, ___includeChildren_12)); }
	inline bool get_includeChildren_12() const { return ___includeChildren_12; }
	inline bool* get_address_of_includeChildren_12() { return &___includeChildren_12; }
	inline void set_includeChildren_12(bool value)
	{
		___includeChildren_12 = value;
	}

	inline static int32_t get_offset_of_inScene_13() { return static_cast<int32_t>(offsetof(iTweenStop_t3798813676, ___inScene_13)); }
	inline bool get_inScene_13() const { return ___inScene_13; }
	inline bool* get_address_of_inScene_13() { return &___inScene_13; }
	inline void set_inScene_13(bool value)
	{
		___inScene_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
