﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator
struct CenterComparator_t1145119813;
// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t4119758992;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPattern4119758992.h"

// System.Void ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::.ctor(System.Single)
extern "C"  void CenterComparator__ctor_m642516693 (CenterComparator_t1145119813 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern "C"  int32_t CenterComparator_Compare_m3233278485 (CenterComparator_t1145119813 * __this, FinderPattern_t4119758992 * ___x0, FinderPattern_t4119758992 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
