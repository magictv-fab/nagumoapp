﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutRepeatButton
struct  GUILayoutRepeatButton_t3980277856  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::storeButtonState
	FsmBool_t1075959796 * ___storeButtonState_12;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::image
	FsmTexture_t3073272573 * ___image_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::text
	FsmString_t952858651 * ___text_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::tooltip
	FsmString_t952858651 * ___tooltip_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::style
	FsmString_t952858651 * ___style_16;

public:
	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t3980277856, ___sendEvent_11)); }
	inline FsmEvent_t2133468028 * get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmEvent_t2133468028 * value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_storeButtonState_12() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t3980277856, ___storeButtonState_12)); }
	inline FsmBool_t1075959796 * get_storeButtonState_12() const { return ___storeButtonState_12; }
	inline FsmBool_t1075959796 ** get_address_of_storeButtonState_12() { return &___storeButtonState_12; }
	inline void set_storeButtonState_12(FsmBool_t1075959796 * value)
	{
		___storeButtonState_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeButtonState_12, value);
	}

	inline static int32_t get_offset_of_image_13() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t3980277856, ___image_13)); }
	inline FsmTexture_t3073272573 * get_image_13() const { return ___image_13; }
	inline FsmTexture_t3073272573 ** get_address_of_image_13() { return &___image_13; }
	inline void set_image_13(FsmTexture_t3073272573 * value)
	{
		___image_13 = value;
		Il2CppCodeGenWriteBarrier(&___image_13, value);
	}

	inline static int32_t get_offset_of_text_14() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t3980277856, ___text_14)); }
	inline FsmString_t952858651 * get_text_14() const { return ___text_14; }
	inline FsmString_t952858651 ** get_address_of_text_14() { return &___text_14; }
	inline void set_text_14(FsmString_t952858651 * value)
	{
		___text_14 = value;
		Il2CppCodeGenWriteBarrier(&___text_14, value);
	}

	inline static int32_t get_offset_of_tooltip_15() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t3980277856, ___tooltip_15)); }
	inline FsmString_t952858651 * get_tooltip_15() const { return ___tooltip_15; }
	inline FsmString_t952858651 ** get_address_of_tooltip_15() { return &___tooltip_15; }
	inline void set_tooltip_15(FsmString_t952858651 * value)
	{
		___tooltip_15 = value;
		Il2CppCodeGenWriteBarrier(&___tooltip_15, value);
	}

	inline static int32_t get_offset_of_style_16() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t3980277856, ___style_16)); }
	inline FsmString_t952858651 * get_style_16() const { return ___style_16; }
	inline FsmString_t952858651 ** get_address_of_style_16() { return &___style_16; }
	inline void set_style_16(FsmString_t952858651 * value)
	{
		___style_16 = value;
		Il2CppCodeGenWriteBarrier(&___style_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
