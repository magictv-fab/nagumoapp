﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVAbstractWindowPopUpControllerScript
struct MagicTVAbstractWindowPopUpControllerScript_t2050906514;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVAbstractWindowPopUpControllerScript::.ctor()
extern "C"  void MagicTVAbstractWindowPopUpControllerScript__ctor_m628982601 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::ClickOK()
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_ClickOK_m839019339 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::RaiseClickOk()
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_RaiseClickOk_m2935816403 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::SetTitleText(System.String)
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_SetTitleText_m967918660 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::SetMessageText(System.String)
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_SetMessageText_m2019866677 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::SetButtonOkText(System.String)
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_SetButtonOkText_m3763050334 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::Hide()
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_Hide_m769700253 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowPopUpControllerScript::Show()
extern "C"  void MagicTVAbstractWindowPopUpControllerScript_Show_m1084042392 (MagicTVAbstractWindowPopUpControllerScript_t2050906514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
