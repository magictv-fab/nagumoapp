﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutTextLabel
struct GUILayoutTextLabel_t3779575658;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextLabel::.ctor()
extern "C"  void GUILayoutTextLabel__ctor_m1186477116 (GUILayoutTextLabel_t3779575658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextLabel::Reset()
extern "C"  void GUILayoutTextLabel_Reset_m3127877353 (GUILayoutTextLabel_t3779575658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextLabel::OnGUI()
extern "C"  void GUILayoutTextLabel_OnGUI_m681875766 (GUILayoutTextLabel_t3779575658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
