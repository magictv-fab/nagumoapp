﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollRectEx
struct ScrollRectEx_t4192460072;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void UnityEngine.UI.Extensions.ScrollRectEx::.ctor()
extern "C"  void ScrollRectEx__ctor_m2350207872 (ScrollRectEx_t4192460072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectEx_OnInitializePotentialDrag_m1027860317 (ScrollRectEx_t4192460072 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectEx_OnDrag_m358296423 (ScrollRectEx_t4192460072 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectEx_OnBeginDrag_m2606001986 (ScrollRectEx_t4192460072 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectEx_OnEndDrag_m675865936 (ScrollRectEx_t4192460072 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollRectEx_OnScroll_m2464736430 (ScrollRectEx_t4192460072 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
