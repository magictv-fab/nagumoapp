﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnionAssets.FLE.EventDispatcher
struct EventDispatcher_t4168900583;
// System.String
struct String_t;
// UnionAssets.FLE.EventHandlerFunction
struct EventHandlerFunction_t2672185540;
// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t438967822;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>
struct List_1_t4040371092;
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>
struct List_1_t1807153374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventHandlerFunc2672185540.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_DataEventHandlerF438967822.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnionAssets.FLE.EventDispatcher::.ctor()
extern "C"  void EventDispatcher__ctor_m3521383413 (EventDispatcher_t4168900583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::addEventListener(System.String,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcher_addEventListener_m388947757 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::addEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcher_addEventListener_m1003515782 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::addEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction,System.String)
extern "C"  void EventDispatcher_addEventListener_m2352280898 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::addEventListener(System.String,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcher_addEventListener_m253036451 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::addEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcher_addEventListener_m3981755260 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::addEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction,System.String)
extern "C"  void EventDispatcher_addEventListener_m3271828152 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::removeEventListener(System.String,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcher_removeEventListener_m2215886542 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::removeEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcher_removeEventListener_m3002111941 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::removeEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction,System.String)
extern "C"  void EventDispatcher_removeEventListener_m1894662977 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::removeEventListener(System.String,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcher_removeEventListener_m3108974276 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::removeEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcher_removeEventListener_m194556987 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::removeEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction,System.String)
extern "C"  void EventDispatcher_removeEventListener_m3998767415 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatchEvent(System.String)
extern "C"  void EventDispatcher_dispatchEvent_m3220955247 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatchEvent(System.String,System.Object)
extern "C"  void EventDispatcher_dispatchEvent_m3180150013 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatchEvent(System.Int32)
extern "C"  void EventDispatcher_dispatchEvent_m1059000900 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatchEvent(System.Int32,System.Object)
extern "C"  void EventDispatcher_dispatchEvent_m1180046866 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatch(System.String)
extern "C"  void EventDispatcher_dispatch_m1536011289 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatch(System.String,System.Object)
extern "C"  void EventDispatcher_dispatch_m4032059175 (EventDispatcher_t4168900583 * __this, String_t* ___eventName0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatch(System.Int32)
extern "C"  void EventDispatcher_dispatch_m1974479194 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatch(System.Int32,System.Object)
extern "C"  void EventDispatcher_dispatch_m2593001128 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::dispatch(System.Int32,System.Object,System.String)
extern "C"  void EventDispatcher_dispatch_m307523812 (EventDispatcher_t4168900583 * __this, int32_t ___eventID0, Il2CppObject * ___data1, String_t* ___eventName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::clearEvents()
extern "C"  void EventDispatcher_clearEvents_m1293608953 (EventDispatcher_t4168900583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction> UnionAssets.FLE.EventDispatcher::cloenArray(System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>)
extern "C"  List_1_t4040371092 * EventDispatcher_cloenArray_m1480974002 (EventDispatcher_t4168900583 * __this, List_1_t4040371092 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction> UnionAssets.FLE.EventDispatcher::cloenArray(System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>)
extern "C"  List_1_t1807153374 * EventDispatcher_cloenArray_m4022239174 (EventDispatcher_t4168900583 * __this, List_1_t1807153374 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcher::OnDestroy()
extern "C"  void EventDispatcher_OnDestroy_m2016730734 (EventDispatcher_t4168900583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
