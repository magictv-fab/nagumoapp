﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomInt
struct RandomInt_t3577963386;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomInt::.ctor()
extern "C"  void RandomInt__ctor_m1697180412 (RandomInt_t3577963386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomInt::Reset()
extern "C"  void RandomInt_Reset_m3638580649 (RandomInt_t3577963386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomInt::OnEnter()
extern "C"  void RandomInt_OnEnter_m2589499347 (RandomInt_t3577963386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
