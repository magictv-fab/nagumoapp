﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickableBehaviour
struct  ClickableBehaviour_t143571995  : public MonoBehaviour_t667441552
{
public:
	// System.String ClickableBehaviour::_externalLink
	String_t* ____externalLink_2;
	// System.Single ClickableBehaviour::_unlockClickTimer
	float ____unlockClickTimer_3;
	// System.Boolean ClickableBehaviour::_hasClick
	bool ____hasClick_4;
	// System.Boolean ClickableBehaviour::_clickLocked
	bool ____clickLocked_5;

public:
	inline static int32_t get_offset_of__externalLink_2() { return static_cast<int32_t>(offsetof(ClickableBehaviour_t143571995, ____externalLink_2)); }
	inline String_t* get__externalLink_2() const { return ____externalLink_2; }
	inline String_t** get_address_of__externalLink_2() { return &____externalLink_2; }
	inline void set__externalLink_2(String_t* value)
	{
		____externalLink_2 = value;
		Il2CppCodeGenWriteBarrier(&____externalLink_2, value);
	}

	inline static int32_t get_offset_of__unlockClickTimer_3() { return static_cast<int32_t>(offsetof(ClickableBehaviour_t143571995, ____unlockClickTimer_3)); }
	inline float get__unlockClickTimer_3() const { return ____unlockClickTimer_3; }
	inline float* get_address_of__unlockClickTimer_3() { return &____unlockClickTimer_3; }
	inline void set__unlockClickTimer_3(float value)
	{
		____unlockClickTimer_3 = value;
	}

	inline static int32_t get_offset_of__hasClick_4() { return static_cast<int32_t>(offsetof(ClickableBehaviour_t143571995, ____hasClick_4)); }
	inline bool get__hasClick_4() const { return ____hasClick_4; }
	inline bool* get_address_of__hasClick_4() { return &____hasClick_4; }
	inline void set__hasClick_4(bool value)
	{
		____hasClick_4 = value;
	}

	inline static int32_t get_offset_of__clickLocked_5() { return static_cast<int32_t>(offsetof(ClickableBehaviour_t143571995, ____clickLocked_5)); }
	inline bool get__clickLocked_5() const { return ____clickLocked_5; }
	inline bool* get_address_of__clickLocked_5() { return &____clickLocked_5; }
	inline void set__clickLocked_5(bool value)
	{
		____clickLocked_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
