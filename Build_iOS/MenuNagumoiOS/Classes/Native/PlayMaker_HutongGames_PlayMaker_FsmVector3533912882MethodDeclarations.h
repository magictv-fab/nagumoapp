﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"

// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVector3::get_Value()
extern "C"  Vector3_t4282066566  FsmVector3_get_Value_m2779135117 (FsmVector3_t533912882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::set_Value(UnityEngine.Vector3)
extern "C"  void FsmVector3_set_Value_m716982822 (FsmVector3_t533912882 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::.ctor()
extern "C"  void FsmVector3__ctor_m1215698529 (FsmVector3_t533912882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::.ctor(System.String)
extern "C"  void FsmVector3__ctor_m570994241 (FsmVector3_t533912882 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::.ctor(HutongGames.PlayMaker.FsmVector3)
extern "C"  void FsmVector3__ctor_m1639627365 (FsmVector3_t533912882 * __this, FsmVector3_t533912882 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVector3::ToString()
extern "C"  String_t* FsmVector3_ToString_m3211298642 (FsmVector3_t533912882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmVector3::op_Implicit(UnityEngine.Vector3)
extern "C"  FsmVector3_t533912882 * FsmVector3_op_Implicit_m3836665052 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
