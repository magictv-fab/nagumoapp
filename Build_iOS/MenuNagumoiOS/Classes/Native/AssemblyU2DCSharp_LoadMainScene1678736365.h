﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Material
struct Material_t3870600107;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadMainScene
struct  LoadMainScene_t1678736365  : public MonoBehaviour_t667441552
{
public:
	// System.String LoadMainScene::LevelName
	String_t* ___LevelName_2;
	// UnityEngine.Material LoadMainScene::magicTVSkyBox
	Material_t3870600107 * ___magicTVSkyBox_4;

public:
	inline static int32_t get_offset_of_LevelName_2() { return static_cast<int32_t>(offsetof(LoadMainScene_t1678736365, ___LevelName_2)); }
	inline String_t* get_LevelName_2() const { return ___LevelName_2; }
	inline String_t** get_address_of_LevelName_2() { return &___LevelName_2; }
	inline void set_LevelName_2(String_t* value)
	{
		___LevelName_2 = value;
		Il2CppCodeGenWriteBarrier(&___LevelName_2, value);
	}

	inline static int32_t get_offset_of_magicTVSkyBox_4() { return static_cast<int32_t>(offsetof(LoadMainScene_t1678736365, ___magicTVSkyBox_4)); }
	inline Material_t3870600107 * get_magicTVSkyBox_4() const { return ___magicTVSkyBox_4; }
	inline Material_t3870600107 ** get_address_of_magicTVSkyBox_4() { return &___magicTVSkyBox_4; }
	inline void set_magicTVSkyBox_4(Material_t3870600107 * value)
	{
		___magicTVSkyBox_4 = value;
		Il2CppCodeGenWriteBarrier(&___magicTVSkyBox_4, value);
	}
};

struct LoadMainScene_t1678736365_StaticFields
{
public:
	// System.Boolean LoadMainScene::started
	bool ___started_3;

public:
	inline static int32_t get_offset_of_started_3() { return static_cast<int32_t>(offsetof(LoadMainScene_t1678736365_StaticFields, ___started_3)); }
	inline bool get_started_3() const { return ___started_3; }
	inline bool* get_address_of_started_3() { return &___started_3; }
	inline void set_started_3(bool value)
	{
		___started_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
