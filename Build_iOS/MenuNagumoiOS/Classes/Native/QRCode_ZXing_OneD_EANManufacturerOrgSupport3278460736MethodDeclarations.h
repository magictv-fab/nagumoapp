﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.EANManufacturerOrgSupport
struct EANManufacturerOrgSupport_t3278460736;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String ZXing.OneD.EANManufacturerOrgSupport::lookupCountryIdentifier(System.String)
extern "C"  String_t* EANManufacturerOrgSupport_lookupCountryIdentifier_m3473574543 (EANManufacturerOrgSupport_t3278460736 * __this, String_t* ___productCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.EANManufacturerOrgSupport::add(System.Int32[],System.String)
extern "C"  void EANManufacturerOrgSupport_add_m3197827101 (EANManufacturerOrgSupport_t3278460736 * __this, Int32U5BU5D_t3230847821* ___range0, String_t* ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.EANManufacturerOrgSupport::initIfNeeded()
extern "C"  void EANManufacturerOrgSupport_initIfNeeded_m3326500691 (EANManufacturerOrgSupport_t3278460736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.EANManufacturerOrgSupport::.ctor()
extern "C"  void EANManufacturerOrgSupport__ctor_m3407190867 (EANManufacturerOrgSupport_t3278460736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
