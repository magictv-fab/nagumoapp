﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DevGUIChromaPanel1432993864.h"
#include "AssemblyU2DCSharp_DevGUIInAppTransform3180776118.h"
#include "AssemblyU2DCSharp_DevGUITransformPanel2319713438.h"
#include "AssemblyU2DCSharp_IncrementalSliderController153649975.h"
#include "AssemblyU2DCSharp_IncrementalSliderController_OnTr3424591110.h"
#include "AssemblyU2DCSharp_TransformEvents2233642885.h"
#include "AssemblyU2DCSharp_TransformEvents_OnBundleIDEventH3610444662.h"
#include "AssemblyU2DCSharp_TransformEvents_OnInAppEventHand2177803875.h"
#include "AssemblyU2DCSharp_TransformEvents_OnTransformEvent2999119763.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst3695678548.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst1722022561.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2318258528.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstrac3311189389.h"
#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"
#include "AssemblyU2DCSharp_ARM_abstracts_components_General1944110717.h"
#include "AssemblyU2DCSharp_ARM_abstracts_internet_InternetIn823636095.h"
#include "AssemblyU2DCSharp_ARM_abstracts_internet_InternetIn558195666.h"
#include "AssemblyU2DCSharp_ARM_abstracts_AViewAbstract2756665534.h"
#include "AssemblyU2DCSharp_ARM_animation_AnimatorController975235909.h"
#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"
#include "AssemblyU2DCSharp_ARM_animation_GenericTimeControl1375523775.h"
#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2357615493.h"
#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2819726214.h"
#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb3844636377.h"
#include "AssemblyU2DCSharp_ARM_animation_utils_LookAtLockAx3125444558.h"
#include "AssemblyU2DCSharp_ARM_components_ComponentsStarter1779695339.h"
#include "AssemblyU2DCSharp_ARM_components_GroupComponentsMa3788191382.h"
#include "AssemblyU2DCSharp_ARM_components_GroupInAppAbstract739334794.h"
#include "AssemblyU2DCSharp_ARM_components_GroupProgressComp1430043881.h"
#include "AssemblyU2DCSharp_ARM_components_GroupStartableCom3626371340.h"
#include "AssemblyU2DCSharp_ARM_device_DeviceControll2606264131.h"
#include "AssemblyU2DCSharp_ARM_device_abstracts_DeviceAccel1009313746.h"
#include "AssemblyU2DCSharp_ARM_device_abstracts_DeviceAccel1904900265.h"
#include "AssemblyU2DCSharp_ARM_device_abstracts_DeviceAccele662891892.h"
#include "AssemblyU2DCSharp_ARM_device_AccelerometerEventDis1386858712.h"
#include "AssemblyU2DCSharp_ARM_device_accelerometer_Acceler1112181119.h"
#include "AssemblyU2DCSharp_ARM_device_AccelerometerInvertEv3204041497.h"
#include "AssemblyU2DCSharp_ARM_device_internet_InternetInfo59940211.h"
#include "AssemblyU2DCSharp_AMR_device_DeviceAppStateMachineE922942516.h"
#include "AssemblyU2DCSharp_AMR_device_DeviceAppStateMachine3609693858.h"
#include "AssemblyU2DCSharp_AMR_device_DeviceAppStateMachine1806304177.h"
#include "AssemblyU2DCSharp_AmIVisible1175015477.h"
#include "AssemblyU2DCSharp_AmIVisible_OnChangeVisible527731977.h"
#include "AssemblyU2DCSharp_ARM_display_ColorChange151994789.h"
#include "AssemblyU2DCSharp_ARM_display_RotateObject3491987816.h"
#include "AssemblyU2DCSharp_ARM_display_RotateObject_OnRotat2396770826.h"
#include "AssemblyU2DCSharp_ARM_display_SimpleView902624229.h"
#include "AssemblyU2DCSharp_ARM_events_BaseDelayToggleEventA1686333410.h"
#include "AssemblyU2DCSharp_ARM_events_ChangeEventAbstract369667058.h"
#include "AssemblyU2DCSharp_ARM_events_ChangeEventAbstract_O2629944389.h"
#include "AssemblyU2DCSharp_ARM_events_ChangeEventToggleOnOff892756926.h"
#include "AssemblyU2DCSharp_ARM_events_DebugToggleOnOff1258336469.h"
#include "AssemblyU2DCSharp_ARM_events_DelayToggleEvent90356527.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterEvent3811317845.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterEvent3373847142.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterVO1159220759.h"
#include "AssemblyU2DCSharp_ARM_events_InvertFilterActiveTog1330757774.h"
#include "AssemblyU2DCSharp_ARM_events_InvertToggleEvent625222166.h"
#include "AssemblyU2DCSharp_ARM_events_TrackInToggleOnOff288618104.h"
#include "AssemblyU2DCSharp_ARM_events_VoidEventDispatcher1760461203.h"
#include "AssemblyU2DCSharp_ARM_events_VoidEventDispatcher_V1756309665.h"
#include "AssemblyU2DCSharp_TouchControllerState2242910518.h"
#include "AssemblyU2DCSharp_ARMMZGuiToutchController3240755437.h"
#include "AssemblyU2DCSharp_CameraOrbitDebugController3226416010.h"
#include "AssemblyU2DCSharp_DetectTouchMovement706316043.h"
#include "AssemblyU2DCSharp_PinchToutchController1962414581.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_AngleMeanF3394023366.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_AngleMedia3039409739.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_BussolaFil1088096095.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_CameraZoom2989246420.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_CameraZoom1406660070.h"
#include "AssemblyU2DCSharp_CopyPositionAngle1779009429.h"
#include "AssemblyU2DCSharp_CopyPositionAngleFilter2952166253.h"
#include "AssemblyU2DCSharp_CopyReversingPositionAngleFilter2437400024.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_DeviceIncli832161023.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_FixedPositi556916690.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_HistoryDela914159810.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_HistoryDel3528974141.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_HistorySit3661546483.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_LockMoveFi1293691288.h"
#include "AssemblyU2DCSharp_MoveRotateFilter1484818244.h"
#include "AssemblyU2DCSharp_PastePositionAngleFilter1444485071.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_PositionMe3420889871.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_SlotFilter3610247053.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_SmoothLeve4050832104.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_SmoothPlug1435567013.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (DevGUIChromaPanel_t1432993864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[18] = 
{
	0,
	0,
	0,
	0,
	DevGUIChromaPanel_t1432993864::get_offset_of__smoothBtn_6(),
	DevGUIChromaPanel_t1432993864::get_offset_of__smoothSensBtn_7(),
	DevGUIChromaPanel_t1432993864::get_offset_of__recorteBtn_8(),
	DevGUIChromaPanel_t1432993864::get_offset_of__recorteSensBtn_9(),
	DevGUIChromaPanel_t1432993864::get_offset_of__shaderChoosed_10(),
	DevGUIChromaPanel_t1432993864::get_offset_of__recorteSens_11(),
	DevGUIChromaPanel_t1432993864::get_offset_of__recorte_12(),
	DevGUIChromaPanel_t1432993864::get_offset_of__smooth_13(),
	DevGUIChromaPanel_t1432993864::get_offset_of__smoothSens_14(),
	DevGUIChromaPanel_t1432993864::get_offset_of__selectedColor_15(),
	DevGUIChromaPanel_t1432993864::get_offset_of__defaultColor_16(),
	DevGUIChromaPanel_t1432993864::get_offset_of__slider_17(),
	DevGUIChromaPanel_t1432993864::get_offset_of__currentProperty_18(),
	DevGUIChromaPanel_t1432993864::get_offset_of__bundleID_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (DevGUIInAppTransform_t3180776118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DevGUIInAppTransform_t3180776118::get_offset_of__selectedColor_8(),
	DevGUIInAppTransform_t3180776118::get_offset_of__defaultColor_9(),
	DevGUIInAppTransform_t3180776118::get_offset_of__positionBtn_10(),
	DevGUIInAppTransform_t3180776118::get_offset_of__rotationBtn_11(),
	DevGUIInAppTransform_t3180776118::get_offset_of__scaleBtn_12(),
	DevGUIInAppTransform_t3180776118::get_offset_of__propertyXBtn_13(),
	DevGUIInAppTransform_t3180776118::get_offset_of__propertyYBtn_14(),
	DevGUIInAppTransform_t3180776118::get_offset_of__propertyZBtn_15(),
	DevGUIInAppTransform_t3180776118::get_offset_of__changeValueInput_16(),
	DevGUIInAppTransform_t3180776118::get_offset_of__changeValue_17(),
	DevGUIInAppTransform_t3180776118::get_offset_of__currentTransform_18(),
	DevGUIInAppTransform_t3180776118::get_offset_of__currentProperty_19(),
	DevGUIInAppTransform_t3180776118::get_offset_of_currentInApp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (DevGUITransformPanel_t2319713438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DevGUITransformPanel_t2319713438::get_offset_of__selectedColor_8(),
	DevGUITransformPanel_t2319713438::get_offset_of__defaultColor_9(),
	DevGUITransformPanel_t2319713438::get_offset_of__positionBtn_10(),
	DevGUITransformPanel_t2319713438::get_offset_of__rotationBtn_11(),
	DevGUITransformPanel_t2319713438::get_offset_of__scaleBtn_12(),
	DevGUITransformPanel_t2319713438::get_offset_of__propertyXBtn_13(),
	DevGUITransformPanel_t2319713438::get_offset_of__propertyYBtn_14(),
	DevGUITransformPanel_t2319713438::get_offset_of__propertyZBtn_15(),
	DevGUITransformPanel_t2319713438::get_offset_of__currentTransform_16(),
	DevGUITransformPanel_t2319713438::get_offset_of__currentProperty_17(),
	DevGUITransformPanel_t2319713438::get_offset_of_currentInApp_18(),
	DevGUITransformPanel_t2319713438::get_offset_of__slider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (IncrementalSliderController_t153649975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[4] = 
{
	IncrementalSliderController_t153649975::get_offset_of_onTransformAxis_2(),
	IncrementalSliderController_t153649975::get_offset_of__slider_3(),
	IncrementalSliderController_t153649975::get_offset_of__multiplyFactorText_4(),
	IncrementalSliderController_t153649975::get_offset_of__value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (OnTransformAxisEventHandler_t3424591110), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (TransformEvents_t2233642885), -1, sizeof(TransformEvents_t2233642885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3705[5] = 
{
	TransformEvents_t2233642885_StaticFields::get_offset_of_onBundleID_0(),
	TransformEvents_t2233642885_StaticFields::get_offset_of_onInApp_1(),
	TransformEvents_t2233642885_StaticFields::get_offset_of_onPosition_2(),
	TransformEvents_t2233642885_StaticFields::get_offset_of_onRotation_3(),
	TransformEvents_t2233642885_StaticFields::get_offset_of_onScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (OnBundleIDEventHandler_t3610444662), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (OnInAppEventHandler_t2177803875), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (OnTransformEventHandler_t2999119763), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (ProgressEventsAbstract_t2129719228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3710[6] = 
{
	ProgressEventsAbstract_t2129719228::get_offset_of_onProgress_8(),
	ProgressEventsAbstract_t2129719228::get_offset_of_onError_9(),
	ProgressEventsAbstract_t2129719228::get_offset_of__isComplete_10(),
	ProgressEventsAbstract_t2129719228::get_offset_of_onComplete_11(),
	ProgressEventsAbstract_t2129719228::get_offset_of_weight_12(),
	ProgressEventsAbstract_t2129719228::get_offset_of__currentProgress_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (OnProgressEventHandler_t3695678548), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (OnErrorEventHandler_t1722022561), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (OnCompleteEventHandler_t2318258528), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (ToggleOnOffAbstract_t832893812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[5] = 
{
	ToggleOnOffAbstract_t832893812::get_offset_of_onTurnOn_2(),
	ToggleOnOffAbstract_t832893812::get_offset_of_onTurnOff_3(),
	ToggleOnOffAbstract_t832893812::get_offset_of_activeByDefault_4(),
	ToggleOnOffAbstract_t832893812::get_offset_of__isActive_5(),
	ToggleOnOffAbstract_t832893812::get_offset_of_DebugIsOn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (TurnChange_t3311189389), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (GeneralComponentsAbstract_t3900398046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[2] = 
{
	GeneralComponentsAbstract_t3900398046::get_offset_of_onCompoentIsReady_2(),
	GeneralComponentsAbstract_t3900398046::get_offset_of__isComponentReady_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (GeneralComponentsObjectAbstract_t1944110717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[2] = 
{
	GeneralComponentsObjectAbstract_t1944110717::get_offset_of_onCompoentIsReady_0(),
	GeneralComponentsObjectAbstract_t1944110717::get_offset_of__isComponentReady_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (InternetInfoAbstract_t823636095), -1, sizeof(InternetInfoAbstract_t823636095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3718[3] = 
{
	InternetInfoAbstract_t823636095_StaticFields::get_offset_of_isWifi_4(),
	InternetInfoAbstract_t823636095::get_offset_of_onWifiInfo_5(),
	InternetInfoAbstract_t823636095::get_offset_of__hasWifi_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (OnWifiInfoEventHandler_t558195666), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (AViewAbstract_t2756665534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (AnimatorController_t975235909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[5] = 
{
	AnimatorController_t975235909::get_offset_of_anim_6(),
	AnimatorController_t975235909::get_offset_of_RestartOnHide_7(),
	AnimatorController_t975235909::get_offset_of__isReadyToPlay_8(),
	AnimatorController_t975235909::get_offset_of_autoPlay_9(),
	AnimatorController_t975235909::get_offset_of__playNow_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (GenericStartableComponentControllAbstract_t1794600275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[4] = 
{
	GenericStartableComponentControllAbstract_t1794600275::get_offset_of_onFinished_4(),
	GenericStartableComponentControllAbstract_t1794600275::get_offset_of__isFinished_5(),
	GenericStartableComponentControllAbstract_t1794600275::get_offset_of_onStart_6(),
	GenericStartableComponentControllAbstract_t1794600275::get_offset_of__isPlaying_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (GenericTimeControllAbstract_t1375523775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[3] = 
{
	GenericTimeControllAbstract_t1375523775::get_offset_of_onFinished_2(),
	GenericTimeControllAbstract_t1375523775::get_offset_of_onStart_3(),
	GenericTimeControllAbstract_t1375523775::get_offset_of__isPlaying_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (ViewTimeControllAbstract_t2357615493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[4] = 
{
	ViewTimeControllAbstract_t2357615493::get_offset_of__onReady_2(),
	ViewTimeControllAbstract_t2357615493::get_offset_of__isReady_3(),
	ViewTimeControllAbstract_t2357615493::get_offset_of_onFinished_4(),
	ViewTimeControllAbstract_t2357615493::get_offset_of__isPlaying_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (OnReadyEventHandlerandler_t2819726214), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (OnFinishedEvent_t3844636377), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (LookAtLockAxis_t3125444558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[11] = 
{
	LookAtLockAxis_t3125444558::get_offset_of_lockX_2(),
	LookAtLockAxis_t3125444558::get_offset_of_lockY_3(),
	LookAtLockAxis_t3125444558::get_offset_of_lockZ_4(),
	LookAtLockAxis_t3125444558::get_offset_of_calibrationX_5(),
	LookAtLockAxis_t3125444558::get_offset_of_calibrationY_6(),
	LookAtLockAxis_t3125444558::get_offset_of_calibrationZ_7(),
	LookAtLockAxis_t3125444558::get_offset_of_targetToLook_8(),
	LookAtLockAxis_t3125444558::get_offset_of_lookToCamera_9(),
	LookAtLockAxis_t3125444558::get_offset_of_debugRotation_10(),
	LookAtLockAxis_t3125444558::get_offset_of_debugRotationQuaternion_11(),
	LookAtLockAxis_t3125444558::get_offset_of__hasTarget_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (ComponentsStarter_t1779695339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[2] = 
{
	ComponentsStarter_t1779695339::get_offset_of_componentsToInit_4(),
	ComponentsStarter_t1779695339::get_offset_of_components_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (GroupComponentsManager_t3788191382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3731[1] = 
{
	GroupComponentsManager_t3788191382::get_offset_of__components_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (GroupInAppAbstractComponentsManager_t739334794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[6] = 
{
	GroupInAppAbstractComponentsManager_t739334794::get_offset_of__bundleVO_14(),
	GroupInAppAbstractComponentsManager_t739334794::get_offset_of__components_15(),
	GroupInAppAbstractComponentsManager_t739334794::get_offset_of_DebugTotalComponents_16(),
	GroupInAppAbstractComponentsManager_t739334794::get_offset_of_keepGameObjectHierarchy_17(),
	GroupInAppAbstractComponentsManager_t739334794::get_offset_of__debugInsideInApps_18(),
	GroupInAppAbstractComponentsManager_t739334794::get_offset_of__preparing_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (GroupProgressComponentsManager_t1430043881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3733[2] = 
{
	GroupProgressComponentsManager_t1430043881::get_offset_of_groupComponentManager_14(),
	GroupProgressComponentsManager_t1430043881::get_offset_of__components_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (GroupStartableComponentsManager_t3626371340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3734[2] = 
{
	GroupStartableComponentsManager_t3626371340::get_offset_of__components_8(),
	GroupStartableComponentsManager_t3626371340::get_offset_of__nextIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (DeviceControll_t2606264131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (DeviceAccelerometerEventDispetcherAbstract_t1009313746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[9] = 
{
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_toleranceChangePercent_3(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of__lastVectorValue_4(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of__lastVectorHasValue_5(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_DebugPercentChange_6(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_onShakeInit_7(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_onShakeEnd_8(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_onMoveInit_9(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_onMoveEnd_10(),
	DeviceAccelerometerEventDispetcherAbstract_t1009313746::get_offset_of_onMove_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (OnVoidEventHandler_t1904900265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (OnMoveHandler_t662891892), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (AccelerometerEventDispacher_t1386858712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[20] = 
{
	AccelerometerEventDispacher_t1386858712::get_offset_of_onGUIDebug_12(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_shakeTolerance_13(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_movementToleranceX_14(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_movementToleranceY_15(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_movementToleranceZ_16(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__historyRange_17(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__time_18(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__clockTimer_19(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__poolVector3_20(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__isShaking_21(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__isMoving_22(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__active_23(),
	AccelerometerEventDispacher_t1386858712::get_offset_of__inicialConfig_24(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_xDiff_25(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_yDiff_26(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_zDiff_27(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_inicialX_28(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_inicialY_29(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_larguraBotao_30(),
	AccelerometerEventDispacher_t1386858712::get_offset_of_alturaBotao_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (AccelerometerEventToggleOnOff_t1112181119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[1] = 
{
	AccelerometerEventToggleOnOff_t1112181119::get_offset_of_accelerometerEvent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (AccelerometerInvertEventToggleOnOff_t3204041497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3741[1] = 
{
	AccelerometerInvertEventToggleOnOff_t3204041497::get_offset_of_accelerometerEvent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (InternetInfo_t59940211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3742[2] = 
{
	InternetInfo_t59940211::get_offset_of_wifiReachability_7(),
	InternetInfo_t59940211::get_offset_of__wifiReachabilityDictionary_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (DeviceAppStateMachineEvents_t922942516), -1, sizeof(DeviceAppStateMachineEvents_t922942516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3743[9] = 
{
	0,
	DeviceAppStateMachineEvents_t922942516_StaticFields::get_offset_of__selfGameObject_3(),
	DeviceAppStateMachineEvents_t922942516_StaticFields::get_offset_of__selfInstance_4(),
	DeviceAppStateMachineEvents_t922942516::get_offset_of_onPause_5(),
	DeviceAppStateMachineEvents_t922942516::get_offset_of_onResume_6(),
	DeviceAppStateMachineEvents_t922942516::get_offset_of_onAwake_7(),
	DeviceAppStateMachineEvents_t922942516::get_offset_of__IOSPauseToleranceInMilliseconds_8(),
	DeviceAppStateMachineEvents_t922942516::get_offset_of__currentFocus_9(),
	DeviceAppStateMachineEvents_t922942516::get_offset_of__pauseTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (OnPauseEventHandler_t3609693858), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (OnAwakeEventHandler_t1806304177), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (AmIVisible_t1175015477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3746[2] = 
{
	AmIVisible_t1175015477::get_offset_of_ChangeVisible_2(),
	AmIVisible_t1175015477::get_offset_of_visible_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (OnChangeVisible_t527731977), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (ColorChange_t151994789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[6] = 
{
	ColorChange_t151994789::get_offset_of_materialToChangeColor_2(),
	ColorChange_t151994789::get_offset_of_objetctToRotate_3(),
	ColorChange_t151994789::get_offset_of__indexColor_4(),
	ColorChange_t151994789::get_offset_of_colors_5(),
	ColorChange_t151994789::get_offset_of__colorTochange_6(),
	ColorChange_t151994789::get_offset_of__lastColorChanged_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (RotateObject_t3491987816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[3] = 
{
	RotateObject_t3491987816::get_offset_of__onRotateComplete_2(),
	RotateObject_t3491987816::get_offset_of_speedy_3(),
	RotateObject_t3491987816::get_offset_of__currentRotation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (OnRotateComplete_t2396770826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (SimpleView_t902624229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[2] = 
{
	SimpleView_t902624229::get_offset_of__state_2(),
	SimpleView_t902624229::get_offset_of__onOff_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (BaseDelayToggleEventAbstract_t1686333410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[10] = 
{
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of_delayToOn_7(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of_delayToOff_8(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__active_9(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__totalTimeElapsedOn_10(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__wantToOn_11(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__timerEnabledOn_12(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__totalTimeElapsedOff_13(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__wantToOff_14(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__timerEnabledOff_15(),
	BaseDelayToggleEventAbstract_t1686333410::get_offset_of__inicialConfig_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (ChangeEventAbstract_t369667058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[1] = 
{
	ChangeEventAbstract_t369667058::get_offset_of_onChangeEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (OnChangeEventHandler_t2629944389), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (ChangeEventToggleOnOff_t892756926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[2] = 
{
	ChangeEventToggleOnOff_t892756926::get_offset_of_PluginChange_17(),
	ChangeEventToggleOnOff_t892756926::get_offset_of__hasPluginChange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (DebugToggleOnOff_t1258336469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[2] = 
{
	DebugToggleOnOff_t1258336469::get_offset_of_ligado_7(),
	DebugToggleOnOff_t1258336469::get_offset_of__lastValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (DelayToggleEvent_t90356527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[1] = 
{
	DelayToggleEvent_t90356527::get_offset_of_toggle_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (GenericParameterEventDispatcher_t3811317845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[1] = 
{
	GenericParameterEventDispatcher_t3811317845::get_offset_of__genericEvent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (GenericEvent_t3373847142), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (GenericParameterVO_t1159220759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[5] = 
{
	GenericParameterVO_t1159220759::get_offset_of_id_0(),
	GenericParameterVO_t1159220759::get_offset_of_type_1(),
	GenericParameterVO_t1159220759::get_offset_of_intValue_2(),
	GenericParameterVO_t1159220759::get_offset_of_stringValue_3(),
	GenericParameterVO_t1159220759::get_offset_of_floatValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (InvertFilterActiveToggleOnOff_t1330757774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[4] = 
{
	InvertFilterActiveToggleOnOff_t1330757774::get_offset_of_ligado_7(),
	InvertFilterActiveToggleOnOff_t1330757774::get_offset_of__lastValue_8(),
	InvertFilterActiveToggleOnOff_t1330757774::get_offset_of__hasFilter_9(),
	InvertFilterActiveToggleOnOff_t1330757774::get_offset_of_filterToInvertActive_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (InvertToggleEvent_t625222166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[1] = 
{
	InvertToggleEvent_t625222166::get_offset_of_toggle_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (TrackInToggleOnOff_t288618104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3763[1] = 
{
	TrackInToggleOnOff_t288618104::get_offset_of_ResetOnTrackin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (VoidEventDispatcher_t1760461203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3764[1] = 
{
	VoidEventDispatcher_t1760461203::get_offset_of__voidEvent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (VoidEvent_t1756309665), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (TouchControllerState_t2242910518)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3775[5] = 
{
	TouchControllerState_t2242910518::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (ARMMZGuiToutchController_t3240755437), -1, sizeof(ARMMZGuiToutchController_t3240755437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3776[15] = 
{
	ARMMZGuiToutchController_t3240755437::get_offset_of__currentState_2(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__startClick_3(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__start1Click_4(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__start2Click_5(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__startRotationClick_6(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__screenPoint_7(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__offset_8(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__scaleAcurracy_9(),
	ARMMZGuiToutchController_t3240755437::get_offset_of_RotationAngleTolerance_10(),
	ARMMZGuiToutchController_t3240755437::get_offset_of_ScaleMaximumTolerance_11(),
	ARMMZGuiToutchController_t3240755437::get_offset_of_ScaleMinimumTolerance_12(),
	ARMMZGuiToutchController_t3240755437::get_offset_of_TapToSelect_13(),
	ARMMZGuiToutchController_t3240755437_StaticFields::get_offset_of_SelectedObject_14(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__angle2Touches_15(),
	ARMMZGuiToutchController_t3240755437::get_offset_of__touchCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (CameraOrbitDebugController_t3226416010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3777[4] = 
{
	CameraOrbitDebugController_t3226416010::get_offset_of_orbitPivot_2(),
	CameraOrbitDebugController_t3226416010::get_offset_of_upAngle_3(),
	CameraOrbitDebugController_t3226416010::get_offset_of_leftAngle_4(),
	CameraOrbitDebugController_t3226416010::get_offset_of_backAngle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (DetectTouchMovement_t706316043), -1, sizeof(DetectTouchMovement_t706316043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3778[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DetectTouchMovement_t706316043_StaticFields::get_offset_of_turnAngleDelta_8(),
	DetectTouchMovement_t706316043_StaticFields::get_offset_of_turnAngle_9(),
	DetectTouchMovement_t706316043_StaticFields::get_offset_of_pinchDistanceDelta_10(),
	DetectTouchMovement_t706316043_StaticFields::get_offset_of_pinchDistance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (PinchToutchController_t1962414581), -1, sizeof(PinchToutchController_t1962414581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3779[50] = 
{
	PinchToutchController_t1962414581::get_offset_of_touchAngle_2(),
	PinchToutchController_t1962414581::get_offset_of_zoom_3(),
	PinchToutchController_t1962414581::get_offset_of_touchDistance_4(),
	PinchToutchController_t1962414581::get_offset_of_deltaAngle_5(),
	PinchToutchController_t1962414581::get_offset_of_deltaAngleTolerance_6(),
	PinchToutchController_t1962414581::get_offset_of_deltaAngleToleranceReset_7(),
	PinchToutchController_t1962414581::get_offset_of_deltaZoom_8(),
	PinchToutchController_t1962414581::get_offset_of_fixAxisVertical_9(),
	PinchToutchController_t1962414581::get_offset_of_fixAxisHorizontal_10(),
	PinchToutchController_t1962414581::get_offset_of_fixAxisDepth_11(),
	PinchToutchController_t1962414581::get_offset_of_canPan_12(),
	PinchToutchController_t1962414581::get_offset_of_canZoom_13(),
	PinchToutchController_t1962414581::get_offset_of_canRotate_14(),
	PinchToutchController_t1962414581::get_offset_of__startClick_15(),
	PinchToutchController_t1962414581::get_offset_of__startRotationClick_16(),
	PinchToutchController_t1962414581::get_offset_of__screenPoint_17(),
	PinchToutchController_t1962414581::get_offset_of__offset_18(),
	PinchToutchController_t1962414581::get_offset_of__scaleAcurracy_19(),
	PinchToutchController_t1962414581::get_offset_of_PanSpeedFactor_20(),
	PinchToutchController_t1962414581::get_offset_of_RotateSpeedFactor_21(),
	PinchToutchController_t1962414581::get_offset_of_RotateDeltaMinimum_22(),
	PinchToutchController_t1962414581::get_offset_of_ScaleDeltaMinimum_23(),
	PinchToutchController_t1962414581::get_offset_of_ScaleDeltaMaximum_24(),
	PinchToutchController_t1962414581::get_offset_of_ScaleSpeedFactor_25(),
	PinchToutchController_t1962414581::get_offset_of_ScaleMaximumTolerance_26(),
	PinchToutchController_t1962414581::get_offset_of_ScaleMinimumTolerance_27(),
	PinchToutchController_t1962414581::get_offset_of_tapToSelect_28(),
	PinchToutchController_t1962414581::get_offset_of_PanRelativeToWorld_29(),
	PinchToutchController_t1962414581_StaticFields::get_offset_of_SelectedObject_30(),
	PinchToutchController_t1962414581::get_offset_of_currentCamera_31(),
	PinchToutchController_t1962414581::get_offset_of_cameraToControl_32(),
	PinchToutchController_t1962414581::get_offset_of_cameraToReference_33(),
	PinchToutchController_t1962414581::get_offset_of_cameraToControlTargetPivot_34(),
	PinchToutchController_t1962414581::get_offset_of_cameraToReferenceTargetPivot_35(),
	PinchToutchController_t1962414581::get_offset_of_cameraToControlInitalTargetDistance_36(),
	PinchToutchController_t1962414581::get_offset_of_cameraToReferenceInitalTargetDistance_37(),
	PinchToutchController_t1962414581::get_offset_of_cameraToControlTargetDistance_38(),
	PinchToutchController_t1962414581::get_offset_of_cameraToReferenceTargetDistance_39(),
	PinchToutchController_t1962414581::get_offset_of_cameraToControlCurrentPercentage_40(),
	PinchToutchController_t1962414581::get_offset_of_cameraToReferenceCurrentPercentage_41(),
	PinchToutchController_t1962414581::get_offset_of_cameraCurrentZoom_42(),
	PinchToutchController_t1962414581::get_offset_of_cameraZoom_43(),
	PinchToutchController_t1962414581::get_offset_of_cameraZoomBonus_44(),
	PinchToutchController_t1962414581::get_offset_of_cameraRelativeRatio_45(),
	PinchToutchController_t1962414581::get_offset_of_camerasDistance_46(),
	PinchToutchController_t1962414581::get_offset_of_camerasMaxDistance_47(),
	PinchToutchController_t1962414581::get_offset_of_camerasMinDistance_48(),
	PinchToutchController_t1962414581::get_offset_of_currentState_49(),
	PinchToutchController_t1962414581::get_offset_of__angle2Touches_50(),
	PinchToutchController_t1962414581::get_offset_of__touchCount_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (AngleMeanFilter_t3394023366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3780[6] = 
{
	AngleMeanFilter_t3394023366::get_offset_of_maxSizeCache_15(),
	AngleMeanFilter_t3394023366::get_offset_of__history_16(),
	AngleMeanFilter_t3394023366::get_offset_of__hasItem_17(),
	AngleMeanFilter_t3394023366::get_offset_of__loaded_18(),
	AngleMeanFilter_t3394023366::get_offset_of_isFull_19(),
	AngleMeanFilter_t3394023366::get_offset_of__currentIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (AngleMedianFilter_t3039409739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[6] = 
{
	AngleMedianFilter_t3039409739::get_offset_of_maxSizeCache_15(),
	AngleMedianFilter_t3039409739::get_offset_of__intialMaxSizeCache_16(),
	AngleMedianFilter_t3039409739::get_offset_of__history_17(),
	AngleMedianFilter_t3039409739::get_offset_of__hasItem_18(),
	AngleMedianFilter_t3039409739::get_offset_of__loaded_19(),
	AngleMedianFilter_t3039409739::get_offset_of_isFull_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (BussolaFilter_t1088096095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[10] = 
{
	BussolaFilter_t1088096095::get_offset_of_debugMode_15(),
	BussolaFilter_t1088096095::get_offset_of_busolaDebugAngle_16(),
	BussolaFilter_t1088096095::get_offset_of_outRotationAngle_17(),
	BussolaFilter_t1088096095::get_offset_of_outInclinationAngle_18(),
	BussolaFilter_t1088096095::get_offset_of_calibration_19(),
	BussolaFilter_t1088096095::get_offset_of_referencePoint_20(),
	BussolaFilter_t1088096095::get_offset_of__resetCalibration_21(),
	BussolaFilter_t1088096095::get_offset_of_roll_22(),
	BussolaFilter_t1088096095::get_offset_of_magneticHeading_23(),
	BussolaFilter_t1088096095::get_offset_of_DebugPositionNext_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (CameraZoomFilter_t2989246420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3783[10] = 
{
	CameraZoomFilter_t2989246420::get_offset_of_positionToZoom_15(),
	CameraZoomFilter_t2989246420::get_offset_of_percentZoom_16(),
	CameraZoomFilter_t2989246420::get_offset_of_ScaleDeltaMinimum_17(),
	CameraZoomFilter_t2989246420::get_offset_of_ScaleDeltaMaximum_18(),
	CameraZoomFilter_t2989246420::get_offset_of_currentState_19(),
	CameraZoomFilter_t2989246420::get_offset_of_touchDistance_20(),
	CameraZoomFilter_t2989246420::get_offset_of_deltaZoom_21(),
	CameraZoomFilter_t2989246420::get_offset_of_deltaZoomRatio_22(),
	CameraZoomFilter_t2989246420::get_offset_of_zoomMax_23(),
	CameraZoomFilter_t2989246420::get_offset_of_zoomMin_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (CameraZoomFilterButton_t1406660070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3784[3] = 
{
	CameraZoomFilterButton_t1406660070::get_offset_of_positionToZoom_15(),
	CameraZoomFilterButton_t1406660070::get_offset_of_percentZoom_16(),
	CameraZoomFilterButton_t1406660070::get_offset_of_DebugResultVector_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (CopyPositionAngle_t1779009429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3785[5] = 
{
	CopyPositionAngle_t1779009429::get_offset_of_gameObjectToCopy_2(),
	CopyPositionAngle_t1779009429::get_offset_of_globalPosition_3(),
	CopyPositionAngle_t1779009429::get_offset_of_copyPosition_4(),
	CopyPositionAngle_t1779009429::get_offset_of_copyAngle_5(),
	CopyPositionAngle_t1779009429::get_offset_of__changeOnLateUpdate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (CopyPositionAngleFilter_t2952166253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3786[11] = 
{
	CopyPositionAngleFilter_t2952166253::get_offset_of_gameObjectToCopy_15(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_readGlobalPosition_16(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_writeGlobalPosition_17(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_copyPosition_18(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_copyAngle_19(),
	CopyPositionAngleFilter_t2952166253::get_offset_of__changeOnLateUpdate_20(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_TransformThisObjectToo_21(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_rotationToSend_22(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_positionToSend_23(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_debugCurrent_24(),
	CopyPositionAngleFilter_t2952166253::get_offset_of_debugNext_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (CopyReversingPositionAngleFilter_t2437400024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (DeviceInclinationCameraFilter_t832161023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[10] = 
{
	DeviceInclinationCameraFilter_t832161023::get_offset_of_sizeFilter_15(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of__filters_16(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of__filtersum_17(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_posFilter_18(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_countSamples_19(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_lastSample_20(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_calibration_21(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_DebugRotationLast_22(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_DebugRotationNext_23(),
	DeviceInclinationCameraFilter_t832161023::get_offset_of_DebugRotationOut_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (FixedPositionAngleFilter_t556916690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3789[5] = 
{
	FixedPositionAngleFilter_t556916690::get_offset_of_positionToLock_15(),
	FixedPositionAngleFilter_t556916690::get_offset_of_rotationToLock_16(),
	FixedPositionAngleFilter_t556916690::get_offset_of__lastHistoryRotation_17(),
	FixedPositionAngleFilter_t556916690::get_offset_of__lastHistoryPosition_18(),
	FixedPositionAngleFilter_t556916690::get_offset_of_keepHistory_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (HistoryDelaySituationCameraFilter_t914159810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[8] = 
{
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of__lastTargetAngle_15(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of__lastTargetPosition_16(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of_trackerEventPlugin_17(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of__isActive_18(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of_delayTime_19(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of__totalTimeElapsed_20(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of__timer_21(),
	HistoryDelaySituationCameraFilter_t914159810::get_offset_of__timerEnabled_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (HistoryDelaySituationFilter_t3528974141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3791[8] = 
{
	HistoryDelaySituationFilter_t3528974141::get_offset_of__lastTargetAngle_15(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of__lastTargetPosition_16(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of_trackerEventPlugin_17(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of__isActive_18(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of_delayTime_19(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of__totalTimeElapsed_20(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of__timer_21(),
	HistoryDelaySituationFilter_t3528974141::get_offset_of__timerEnabled_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (HistorySituationCamera_t3661546483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[5] = 
{
	HistorySituationCamera_t3661546483::get_offset_of_positionFilter_15(),
	HistorySituationCamera_t3661546483::get_offset_of_angleFilter_16(),
	HistorySituationCamera_t3661546483::get_offset_of__hasPositionFilter_17(),
	HistorySituationCamera_t3661546483::get_offset_of__hasAngleFilter_18(),
	HistorySituationCamera_t3661546483::get_offset_of__isActive_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (LockMoveFilter_t1293691288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3793[5] = 
{
	LockMoveFilter_t1293691288::get_offset_of__lastAngleHasValue_15(),
	LockMoveFilter_t1293691288::get_offset_of__lastAngleValue_16(),
	LockMoveFilter_t1293691288::get_offset_of__lastPositionHasValue_17(),
	LockMoveFilter_t1293691288::get_offset_of__lastPositionValue_18(),
	LockMoveFilter_t1293691288::get_offset_of_isWorking_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (MoveRotateFilter_t1484818244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3794[13] = 
{
	MoveRotateFilter_t1484818244::get_offset_of_gameObjectToMove_15(),
	MoveRotateFilter_t1484818244::get_offset_of_readGlobalPosition_16(),
	MoveRotateFilter_t1484818244::get_offset_of_writeGlobalPosition_17(),
	MoveRotateFilter_t1484818244::get_offset_of_movePosition_18(),
	MoveRotateFilter_t1484818244::get_offset_of_moveAngle_19(),
	MoveRotateFilter_t1484818244::get_offset_of__changeOnLateUpdate_20(),
	MoveRotateFilter_t1484818244::get_offset_of_forceToMovePosition_21(),
	MoveRotateFilter_t1484818244::get_offset_of_forceToRotate_22(),
	MoveRotateFilter_t1484818244::get_offset_of_TransformThisObjectToo_23(),
	MoveRotateFilter_t1484818244::get_offset_of_rotationToSend_24(),
	MoveRotateFilter_t1484818244::get_offset_of_positionToSend_25(),
	MoveRotateFilter_t1484818244::get_offset_of_debugCurrent_26(),
	MoveRotateFilter_t1484818244::get_offset_of_debugNext_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (PastePositionAngleFilter_t1444485071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3795[7] = 
{
	PastePositionAngleFilter_t1444485071::get_offset_of_objectToPaste_2(),
	PastePositionAngleFilter_t1444485071::get_offset_of_readGlobalPosition_3(),
	PastePositionAngleFilter_t1444485071::get_offset_of_writeGlobalPosition_4(),
	PastePositionAngleFilter_t1444485071::get_offset_of_copyPosition_5(),
	PastePositionAngleFilter_t1444485071::get_offset_of_copyAngle_6(),
	PastePositionAngleFilter_t1444485071::get_offset_of__changeOnLateUpdate_7(),
	PastePositionAngleFilter_t1444485071::get_offset_of__active_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (PositionMedianFilter_t3420889871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3796[9] = 
{
	PositionMedianFilter_t3420889871::get_offset_of_maxSizeCache_15(),
	PositionMedianFilter_t3420889871::get_offset_of__historyPosition_16(),
	PositionMedianFilter_t3420889871::get_offset_of__hasItem_17(),
	PositionMedianFilter_t3420889871::get_offset_of__loaded_18(),
	PositionMedianFilter_t3420889871::get_offset_of_isFull_19(),
	PositionMedianFilter_t3420889871::get_offset_of__calcInProgress_20(),
	PositionMedianFilter_t3420889871::get_offset_of__lastMeanValueHasValue_21(),
	PositionMedianFilter_t3420889871::get_offset_of___lastMeanValue_22(),
	PositionMedianFilter_t3420889871::get_offset_of__saving_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (SlotFilterPlugin_t3610247053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3797[14] = 
{
	SlotFilterPlugin_t3610247053::get_offset_of__lastPosition_15(),
	SlotFilterPlugin_t3610247053::get_offset_of__lastAngle_16(),
	SlotFilterPlugin_t3610247053::get_offset_of_applyInThisObject_17(),
	SlotFilterPlugin_t3610247053::get_offset_of_returnThisValues_18(),
	SlotFilterPlugin_t3610247053::get_offset_of__isLoaded_19(),
	SlotFilterPlugin_t3610247053::get_offset_of_position_20(),
	SlotFilterPlugin_t3610247053::get_offset_of_rotation_21(),
	SlotFilterPlugin_t3610247053::get_offset_of_filteredNextValuePosition_22(),
	SlotFilterPlugin_t3610247053::get_offset_of_filteredNextValueRotation_23(),
	SlotFilterPlugin_t3610247053::get_offset_of_DebugRotationLast_24(),
	SlotFilterPlugin_t3610247053::get_offset_of_DebugRotationNext_25(),
	SlotFilterPlugin_t3610247053::get_offset_of_DebugRotationOut_26(),
	SlotFilterPlugin_t3610247053::get_offset_of_DebugPositionLast_27(),
	SlotFilterPlugin_t3610247053::get_offset_of_DebugPositionNew_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (SmoothLevelController_t4050832104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3798[15] = 
{
	SmoothLevelController_t4050832104::get_offset_of_smoothFollowToControl_2(),
	SmoothLevelController_t4050832104::get_offset_of_accelerometerEventDispatcher_3(),
	SmoothLevelController_t4050832104::get_offset_of__hasAccelerometer_4(),
	SmoothLevelController_t4050832104::get_offset_of__hasSmooth_5(),
	SmoothLevelController_t4050832104::get_offset_of_angleRatio_6(),
	SmoothLevelController_t4050832104::get_offset_of_positionRatio_7(),
	SmoothLevelController_t4050832104::get_offset_of_angleMinSmooth_8(),
	SmoothLevelController_t4050832104::get_offset_of_angleMaxSmooth_9(),
	SmoothLevelController_t4050832104::get_offset_of_positionMinSmooth_10(),
	SmoothLevelController_t4050832104::get_offset_of_positionMaxSmooth_11(),
	SmoothLevelController_t4050832104::get_offset_of__difMinMaxPosition_12(),
	SmoothLevelController_t4050832104::get_offset_of__difMinMaxAngle_13(),
	SmoothLevelController_t4050832104::get_offset_of__defaultSpeedPosition_14(),
	SmoothLevelController_t4050832104::get_offset_of__defaultSpeedAngle_15(),
	SmoothLevelController_t4050832104::get_offset_of_debugMag_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (SmoothPlugin_t1435567013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3799[9] = 
{
	SmoothPlugin_t1435567013::get_offset_of_accelerationShakeEvent_15(),
	SmoothPlugin_t1435567013::get_offset_of_initialToleranceTime_16(),
	SmoothPlugin_t1435567013::get_offset_of__toleranceTimePassed_17(),
	SmoothPlugin_t1435567013::get_offset_of_tweenSpeedAngle_18(),
	SmoothPlugin_t1435567013::get_offset_of_tweenSpeedPosition_19(),
	SmoothPlugin_t1435567013::get_offset_of__resetTweenSpeedAngle_20(),
	SmoothPlugin_t1435567013::get_offset_of__resetTweenSpeedPosition_21(),
	SmoothPlugin_t1435567013::get_offset_of__resetInitialToleranceTime_22(),
	SmoothPlugin_t1435567013::get_offset_of__moveQuick_23(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
