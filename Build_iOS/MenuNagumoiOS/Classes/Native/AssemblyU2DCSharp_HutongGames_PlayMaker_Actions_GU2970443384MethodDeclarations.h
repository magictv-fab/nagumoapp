﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIBox
struct GUIBox_t2970443384;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIBox::.ctor()
extern "C"  void GUIBox__ctor_m2423523566 (GUIBox_t2970443384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIBox::OnGUI()
extern "C"  void GUIBox_OnGUI_m1918922216 (GUIBox_t2970443384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
