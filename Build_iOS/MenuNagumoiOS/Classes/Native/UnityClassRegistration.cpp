struct ClassRegistrationContext;
void InvokeRegisterStaticallyLinkedModuleClasses(ClassRegistrationContext& context)
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_AI();
	RegisterModule_AI();

	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

}

void RegisterAllClasses()
{
	//Total: 94 classes
	//0. AssetBundle
	void RegisterClass_AssetBundle();
	RegisterClass_AssetBundle();

	//1. NamedObject
	void RegisterClass_NamedObject();
	RegisterClass_NamedObject();

	//2. EditorExtension
	void RegisterClass_EditorExtension();
	RegisterClass_EditorExtension();

	//3. RenderSettings
	void RegisterClass_RenderSettings();
	RegisterClass_RenderSettings();

	//4. LevelGameManager
	void RegisterClass_LevelGameManager();
	RegisterClass_LevelGameManager();

	//5. GameManager
	void RegisterClass_GameManager();
	RegisterClass_GameManager();

	//6. QualitySettings
	void RegisterClass_QualitySettings();
	RegisterClass_QualitySettings();

	//7. GlobalGameManager
	void RegisterClass_GlobalGameManager();
	RegisterClass_GlobalGameManager();

	//8. MeshFilter
	void RegisterClass_MeshFilter();
	RegisterClass_MeshFilter();

	//9. Component
	void RegisterClass_Component();
	RegisterClass_Component();

	//10. Mesh
	void RegisterClass_Mesh();
	RegisterClass_Mesh();

	//11. Renderer
	void RegisterClass_Renderer();
	RegisterClass_Renderer();

	//12. GUIElement
	void RegisterClass_GUIElement();
	RegisterClass_GUIElement();

	//13. Behaviour
	void RegisterClass_Behaviour();
	RegisterClass_Behaviour();

	//14. GUITexture
	void RegisterClass_GUITexture();
	RegisterClass_GUITexture();

	//15. GUILayer
	void RegisterClass_GUILayer();
	RegisterClass_GUILayer();

	//16. Texture
	void RegisterClass_Texture();
	RegisterClass_Texture();

	//17. Texture2D
	void RegisterClass_Texture2D();
	RegisterClass_Texture2D();

	//18. RenderTexture
	void RegisterClass_RenderTexture();
	RegisterClass_RenderTexture();

	//19. NetworkView
	void RegisterClass_NetworkView();
	RegisterClass_NetworkView();

	//20. RectTransform
	void RegisterClass_RectTransform();
	RegisterClass_RectTransform();

	//21. Transform
	void RegisterClass_Transform();
	RegisterClass_Transform();

	//22. TextAsset
	void RegisterClass_TextAsset();
	RegisterClass_TextAsset();

	//23. Shader
	void RegisterClass_Shader();
	RegisterClass_Shader();

	//24. Material
	void RegisterClass_Material();
	RegisterClass_Material();

	//25. Sprite
	void RegisterClass_Sprite();
	RegisterClass_Sprite();

	//26. Camera
	void RegisterClass_Camera();
	RegisterClass_Camera();

	//27. MonoBehaviour
	void RegisterClass_MonoBehaviour();
	RegisterClass_MonoBehaviour();

	//28. Light
	void RegisterClass_Light();
	RegisterClass_Light();

	//29. GameObject
	void RegisterClass_GameObject();
	RegisterClass_GameObject();

	//30. Motion
	void RegisterClass_Motion();
	RegisterClass_Motion();

	//31. ParticleSystem
	void RegisterClass_ParticleSystem();
	RegisterClass_ParticleSystem();

	//32. Rigidbody
	void RegisterClass_Rigidbody();
	RegisterClass_Rigidbody();

	//33. Joint
	void RegisterClass_Joint();
	RegisterClass_Joint();

	//34. Collider
	void RegisterClass_Collider();
	RegisterClass_Collider();

	//35. SphereCollider
	void RegisterClass_SphereCollider();
	RegisterClass_SphereCollider();

	//36. MeshCollider
	void RegisterClass_MeshCollider();
	RegisterClass_MeshCollider();

	//37. CapsuleCollider
	void RegisterClass_CapsuleCollider();
	RegisterClass_CapsuleCollider();

	//38. CharacterController
	void RegisterClass_CharacterController();
	RegisterClass_CharacterController();

	//39. Cloth
	void RegisterClass_Cloth();
	RegisterClass_Cloth();

	//40. Collider2D
	void RegisterClass_Collider2D();
	RegisterClass_Collider2D();

	//41. NavMeshAgent
	void RegisterClass_NavMeshAgent();
	RegisterClass_NavMeshAgent();

	//42. AudioClip
	void RegisterClass_AudioClip();
	RegisterClass_AudioClip();

	//43. SampleClip
	void RegisterClass_SampleClip();
	RegisterClass_SampleClip();

	//44. AudioListener
	void RegisterClass_AudioListener();
	RegisterClass_AudioListener();

	//45. AudioBehaviour
	void RegisterClass_AudioBehaviour();
	RegisterClass_AudioBehaviour();

	//46. AudioSource
	void RegisterClass_AudioSource();
	RegisterClass_AudioSource();

	//47. WebCamTexture
	void RegisterClass_WebCamTexture();
	RegisterClass_WebCamTexture();

	//48. AnimationClip
	void RegisterClass_AnimationClip();
	RegisterClass_AnimationClip();

	//49. Animation
	void RegisterClass_Animation();
	RegisterClass_Animation();

	//50. Animator
	void RegisterClass_Animator();
	RegisterClass_Animator();

	//51. DirectorPlayer
	void RegisterClass_DirectorPlayer();
	RegisterClass_DirectorPlayer();

	//52. GUIText
	void RegisterClass_GUIText();
	RegisterClass_GUIText();

	//53. TextMesh
	void RegisterClass_TextMesh();
	RegisterClass_TextMesh();

	//54. Font
	void RegisterClass_Font();
	RegisterClass_Font();

	//55. Canvas
	void RegisterClass_Canvas();
	RegisterClass_Canvas();

	//56. CanvasGroup
	void RegisterClass_CanvasGroup();
	RegisterClass_CanvasGroup();

	//57. CanvasRenderer
	void RegisterClass_CanvasRenderer();
	RegisterClass_CanvasRenderer();

	//58. PhysicMaterial
	void RegisterClass_PhysicMaterial();
	RegisterClass_PhysicMaterial();

	//59. Flare
	void RegisterClass_Flare();
	RegisterClass_Flare();

	//60. MeshRenderer
	void RegisterClass_MeshRenderer();
	RegisterClass_MeshRenderer();

	//61. SpriteRenderer
	void RegisterClass_SpriteRenderer();
	RegisterClass_SpriteRenderer();

	//62. RuntimeAnimatorController
	void RegisterClass_RuntimeAnimatorController();
	RegisterClass_RuntimeAnimatorController();

	//63. BoxCollider
	void RegisterClass_BoxCollider();
	RegisterClass_BoxCollider();

	//64. PreloadData
	void RegisterClass_PreloadData();
	RegisterClass_PreloadData();

	//65. Cubemap
	void RegisterClass_Cubemap();
	RegisterClass_Cubemap();

	//66. Texture3D
	void RegisterClass_Texture3D();
	RegisterClass_Texture3D();

	//67. TimeManager
	void RegisterClass_TimeManager();
	RegisterClass_TimeManager();

	//68. AudioManager
	void RegisterClass_AudioManager();
	RegisterClass_AudioManager();

	//69. InputManager
	void RegisterClass_InputManager();
	RegisterClass_InputManager();

	//70. Physics2DSettings
	void RegisterClass_Physics2DSettings();
	RegisterClass_Physics2DSettings();

	//71. GraphicsSettings
	void RegisterClass_GraphicsSettings();
	RegisterClass_GraphicsSettings();

	//72. Rigidbody2D
	void RegisterClass_Rigidbody2D();
	RegisterClass_Rigidbody2D();

	//73. PhysicsManager
	void RegisterClass_PhysicsManager();
	RegisterClass_PhysicsManager();

	//74. BoxCollider2D
	void RegisterClass_BoxCollider2D();
	RegisterClass_BoxCollider2D();

	//75. TagManager
	void RegisterClass_TagManager();
	RegisterClass_TagManager();

	//76. Avatar
	void RegisterClass_Avatar();
	RegisterClass_Avatar();

	//77. AnimatorController
	void RegisterClass_AnimatorController();
	RegisterClass_AnimatorController();

	//78. ScriptMapper
	void RegisterClass_ScriptMapper();
	RegisterClass_ScriptMapper();

	//79. DelayedCallManager
	void RegisterClass_DelayedCallManager();
	RegisterClass_DelayedCallManager();

	//80. MonoScript
	void RegisterClass_MonoScript();
	RegisterClass_MonoScript();

	//81. MonoManager
	void RegisterClass_MonoManager();
	RegisterClass_MonoManager();

	//82. FlareLayer
	void RegisterClass_FlareLayer();
	RegisterClass_FlareLayer();

	//83. NavMeshAreas
	void RegisterClass_NavMeshAreas();
	RegisterClass_NavMeshAreas();

	//84. PlayerSettings
	void RegisterClass_PlayerSettings();
	RegisterClass_PlayerSettings();

	//85. SkinnedMeshRenderer
	void RegisterClass_SkinnedMeshRenderer();
	RegisterClass_SkinnedMeshRenderer();

	//86. BuildSettings
	void RegisterClass_BuildSettings();
	RegisterClass_BuildSettings();

	//87. ResourceManager
	void RegisterClass_ResourceManager();
	RegisterClass_ResourceManager();

	//88. NetworkManager
	void RegisterClass_NetworkManager();
	RegisterClass_NetworkManager();

	//89. MasterServerInterface
	void RegisterClass_MasterServerInterface();
	RegisterClass_MasterServerInterface();

	//90. LightmapSettings
	void RegisterClass_LightmapSettings();
	RegisterClass_LightmapSettings();

	//91. ParticleSystemRenderer
	void RegisterClass_ParticleSystemRenderer();
	RegisterClass_ParticleSystemRenderer();

	//92. LightProbes
	void RegisterClass_LightProbes();
	RegisterClass_LightProbes();

	//93. RuntimeInitializeOnLoadManager
	void RegisterClass_RuntimeInitializeOnLoadManager();
	RegisterClass_RuntimeInitializeOnLoadManager();

}
