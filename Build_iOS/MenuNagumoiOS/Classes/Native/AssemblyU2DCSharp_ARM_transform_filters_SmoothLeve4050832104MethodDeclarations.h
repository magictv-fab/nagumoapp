﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.SmoothLevelController
struct SmoothLevelController_t4050832104;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.transform.filters.SmoothLevelController::.ctor()
extern "C"  void SmoothLevelController__ctor_m2559725472 (SmoothLevelController_t4050832104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothLevelController::Start()
extern "C"  void SmoothLevelController_Start_m1506863264 (SmoothLevelController_t4050832104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothLevelController::ReturnDefaultEventHandler()
extern "C"  void SmoothLevelController_ReturnDefaultEventHandler_m1450338527 (SmoothLevelController_t4050832104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothLevelController::OnDeviceMoveEventHandler(UnityEngine.Vector3)
extern "C"  void SmoothLevelController_OnDeviceMoveEventHandler_m3694274559 (SmoothLevelController_t4050832104 * __this, Vector3_t4282066566  ___totalMove0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothLevelController::Update()
extern "C"  void SmoothLevelController_Update_m3768940397 (SmoothLevelController_t4050832104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
