﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorTarget
struct SetAnimatorTarget_t2430133416;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::.ctor()
extern "C"  void SetAnimatorTarget__ctor_m2282059150 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::Reset()
extern "C"  void SetAnimatorTarget_Reset_m4223459387 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::OnEnter()
extern "C"  void SetAnimatorTarget_OnEnter_m2017250789 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::OnAnimatorMoveEvent()
extern "C"  void SetAnimatorTarget_OnAnimatorMoveEvent_m2024060079 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::OnUpdate()
extern "C"  void SetAnimatorTarget_OnUpdate_m1538791678 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::SetTarget()
extern "C"  void SetAnimatorTarget_SetTarget_m3985308927 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::OnExit()
extern "C"  void SetAnimatorTarget_OnExit_m3537675539 (SetAnimatorTarget_t2430133416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
