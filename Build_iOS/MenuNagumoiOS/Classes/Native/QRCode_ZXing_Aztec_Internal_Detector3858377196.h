﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Detector
struct  Detector_t3858377196  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.Aztec.Internal.Detector::image
	BitMatrix_t1058711404 * ___image_0;
	// System.Boolean ZXing.Aztec.Internal.Detector::compact
	bool ___compact_1;
	// System.Int32 ZXing.Aztec.Internal.Detector::nbLayers
	int32_t ___nbLayers_2;
	// System.Int32 ZXing.Aztec.Internal.Detector::nbDataBlocks
	int32_t ___nbDataBlocks_3;
	// System.Int32 ZXing.Aztec.Internal.Detector::nbCenterLayers
	int32_t ___nbCenterLayers_4;
	// System.Int32 ZXing.Aztec.Internal.Detector::shift
	int32_t ___shift_5;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(Detector_t3858377196, ___image_0)); }
	inline BitMatrix_t1058711404 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t1058711404 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t1058711404 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_compact_1() { return static_cast<int32_t>(offsetof(Detector_t3858377196, ___compact_1)); }
	inline bool get_compact_1() const { return ___compact_1; }
	inline bool* get_address_of_compact_1() { return &___compact_1; }
	inline void set_compact_1(bool value)
	{
		___compact_1 = value;
	}

	inline static int32_t get_offset_of_nbLayers_2() { return static_cast<int32_t>(offsetof(Detector_t3858377196, ___nbLayers_2)); }
	inline int32_t get_nbLayers_2() const { return ___nbLayers_2; }
	inline int32_t* get_address_of_nbLayers_2() { return &___nbLayers_2; }
	inline void set_nbLayers_2(int32_t value)
	{
		___nbLayers_2 = value;
	}

	inline static int32_t get_offset_of_nbDataBlocks_3() { return static_cast<int32_t>(offsetof(Detector_t3858377196, ___nbDataBlocks_3)); }
	inline int32_t get_nbDataBlocks_3() const { return ___nbDataBlocks_3; }
	inline int32_t* get_address_of_nbDataBlocks_3() { return &___nbDataBlocks_3; }
	inline void set_nbDataBlocks_3(int32_t value)
	{
		___nbDataBlocks_3 = value;
	}

	inline static int32_t get_offset_of_nbCenterLayers_4() { return static_cast<int32_t>(offsetof(Detector_t3858377196, ___nbCenterLayers_4)); }
	inline int32_t get_nbCenterLayers_4() const { return ___nbCenterLayers_4; }
	inline int32_t* get_address_of_nbCenterLayers_4() { return &___nbCenterLayers_4; }
	inline void set_nbCenterLayers_4(int32_t value)
	{
		___nbCenterLayers_4 = value;
	}

	inline static int32_t get_offset_of_shift_5() { return static_cast<int32_t>(offsetof(Detector_t3858377196, ___shift_5)); }
	inline int32_t get_shift_5() const { return ___shift_5; }
	inline int32_t* get_address_of_shift_5() { return &___shift_5; }
	inline void set_shift_5(int32_t value)
	{
		___shift_5 = value;
	}
};

struct Detector_t3858377196_StaticFields
{
public:
	// System.Int32[] ZXing.Aztec.Internal.Detector::EXPECTED_CORNER_BITS
	Int32U5BU5D_t3230847821* ___EXPECTED_CORNER_BITS_6;

public:
	inline static int32_t get_offset_of_EXPECTED_CORNER_BITS_6() { return static_cast<int32_t>(offsetof(Detector_t3858377196_StaticFields, ___EXPECTED_CORNER_BITS_6)); }
	inline Int32U5BU5D_t3230847821* get_EXPECTED_CORNER_BITS_6() const { return ___EXPECTED_CORNER_BITS_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_EXPECTED_CORNER_BITS_6() { return &___EXPECTED_CORNER_BITS_6; }
	inline void set_EXPECTED_CORNER_BITS_6(Int32U5BU5D_t3230847821* value)
	{
		___EXPECTED_CORNER_BITS_6 = value;
		Il2CppCodeGenWriteBarrier(&___EXPECTED_CORNER_BITS_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
