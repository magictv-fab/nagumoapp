﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.EncodeHintType>
struct DefaultComparer_t1692339624;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.EncodeHintType>::.ctor()
extern "C"  void DefaultComparer__ctor_m392906029_gshared (DefaultComparer_t1692339624 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m392906029(__this, method) ((  void (*) (DefaultComparer_t1692339624 *, const MethodInfo*))DefaultComparer__ctor_m392906029_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.EncodeHintType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3641549606_gshared (DefaultComparer_t1692339624 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3641549606(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1692339624 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3641549606_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.EncodeHintType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1137492418_gshared (DefaultComparer_t1692339624 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1137492418(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1692339624 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1137492418_gshared)(__this, ___x0, ___y1, method)
