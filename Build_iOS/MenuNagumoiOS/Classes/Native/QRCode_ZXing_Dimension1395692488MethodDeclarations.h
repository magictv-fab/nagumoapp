﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Dimension
struct Dimension_t1395692488;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ZXing.Dimension::.ctor(System.Int32,System.Int32)
extern "C"  void Dimension__ctor_m4159131511 (Dimension_t1395692488 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Dimension::get_Width()
extern "C"  int32_t Dimension_get_Width_m628606040 (Dimension_t1395692488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Dimension::get_Height()
extern "C"  int32_t Dimension_get_Height_m2810732407 (Dimension_t1395692488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Dimension::Equals(System.Object)
extern "C"  bool Dimension_Equals_m3246248756 (Dimension_t1395692488 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Dimension::GetHashCode()
extern "C"  int32_t Dimension_GetHashCode_m1032876748 (Dimension_t1395692488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Dimension::ToString()
extern "C"  String_t* Dimension_ToString_m3902777918 (Dimension_t1395692488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
