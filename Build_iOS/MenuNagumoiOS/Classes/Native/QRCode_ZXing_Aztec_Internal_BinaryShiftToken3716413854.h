﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.BinaryShiftToken
struct  BinaryShiftToken_t3716413854  : public Token_t3066207355
{
public:
	// System.Int16 ZXing.Aztec.Internal.BinaryShiftToken::binaryShiftStart
	int16_t ___binaryShiftStart_2;
	// System.Int16 ZXing.Aztec.Internal.BinaryShiftToken::binaryShiftByteCount
	int16_t ___binaryShiftByteCount_3;

public:
	inline static int32_t get_offset_of_binaryShiftStart_2() { return static_cast<int32_t>(offsetof(BinaryShiftToken_t3716413854, ___binaryShiftStart_2)); }
	inline int16_t get_binaryShiftStart_2() const { return ___binaryShiftStart_2; }
	inline int16_t* get_address_of_binaryShiftStart_2() { return &___binaryShiftStart_2; }
	inline void set_binaryShiftStart_2(int16_t value)
	{
		___binaryShiftStart_2 = value;
	}

	inline static int32_t get_offset_of_binaryShiftByteCount_3() { return static_cast<int32_t>(offsetof(BinaryShiftToken_t3716413854, ___binaryShiftByteCount_3)); }
	inline int16_t get_binaryShiftByteCount_3() const { return ___binaryShiftByteCount_3; }
	inline int16_t* get_address_of_binaryShiftByteCount_3() { return &___binaryShiftByteCount_3; }
	inline void set_binaryShiftByteCount_3(int16_t value)
	{
		___binaryShiftByteCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
