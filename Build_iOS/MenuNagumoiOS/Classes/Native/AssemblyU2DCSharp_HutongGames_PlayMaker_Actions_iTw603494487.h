﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_iTweenFSMType470630072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenResume
struct  iTweenResume_t603494487  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenResume::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// iTweenFSMType HutongGames.PlayMaker.Actions.iTweenResume::iTweenType
	int32_t ___iTweenType_10;
	// System.Boolean HutongGames.PlayMaker.Actions.iTweenResume::includeChildren
	bool ___includeChildren_11;
	// System.Boolean HutongGames.PlayMaker.Actions.iTweenResume::inScene
	bool ___inScene_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(iTweenResume_t603494487, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_iTweenType_10() { return static_cast<int32_t>(offsetof(iTweenResume_t603494487, ___iTweenType_10)); }
	inline int32_t get_iTweenType_10() const { return ___iTweenType_10; }
	inline int32_t* get_address_of_iTweenType_10() { return &___iTweenType_10; }
	inline void set_iTweenType_10(int32_t value)
	{
		___iTweenType_10 = value;
	}

	inline static int32_t get_offset_of_includeChildren_11() { return static_cast<int32_t>(offsetof(iTweenResume_t603494487, ___includeChildren_11)); }
	inline bool get_includeChildren_11() const { return ___includeChildren_11; }
	inline bool* get_address_of_includeChildren_11() { return &___includeChildren_11; }
	inline void set_includeChildren_11(bool value)
	{
		___includeChildren_11 = value;
	}

	inline static int32_t get_offset_of_inScene_12() { return static_cast<int32_t>(offsetof(iTweenResume_t603494487, ___inScene_12)); }
	inline bool get_inScene_12() const { return ___inScene_12; }
	inline bool* get_address_of_inScene_12() { return &___inScene_12; }
	inline void set_inScene_12(bool value)
	{
		___inScene_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
