﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3146284570MethodDeclarations.h"

// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::.ctor()
#define ComponentAction_1__ctor_m2313799833(__this, method) ((  void (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1__ctor_m1981820550_gshared)(__this, method)
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_rigidbody()
#define ComponentAction_1_get_rigidbody_m3493983742(__this, method) ((  Rigidbody_t3346577219 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_rigidbody_m3654614315_gshared)(__this, method)
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_renderer()
#define ComponentAction_1_get_renderer_m4208417274(__this, method) ((  Renderer_t3076687687 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_renderer_m2707840429_gshared)(__this, method)
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_animation()
#define ComponentAction_1_get_animation_m3299398508(__this, method) ((  Animation_t1724966010 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_animation_m3244187609_gshared)(__this, method)
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_audio()
#define ComponentAction_1_get_audio_m485698059(__this, method) ((  AudioSource_t1740077639 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_audio_m1198830264_gshared)(__this, method)
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_camera()
#define ComponentAction_1_get_camera_m2792973114(__this, method) ((  Camera_t2727095145 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_camera_m2580411949_gshared)(__this, method)
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_guiText()
#define ComponentAction_1_get_guiText_m1496058036(__this, method) ((  GUIText_t3371372606 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_guiText_m995647329_gshared)(__this, method)
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_guiTexture()
#define ComponentAction_1_get_guiTexture_m2080551194(__this, method) ((  GUITexture_t4020448292 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_guiTexture_m3296952909_gshared)(__this, method)
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_light()
#define ComponentAction_1_get_light_m3760368656(__this, method) ((  Light_t4202674828 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_light_m3399183485_gshared)(__this, method)
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_networkView()
#define ComponentAction_1_get_networkView_m3678362186(__this, method) ((  NetworkView_t3656680617 * (*) (ComponentAction_1_t2346840805 *, const MethodInfo*))ComponentAction_1_get_networkView_m1393581751_gshared)(__this, method)
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::UpdateCache(UnityEngine.GameObject)
#define ComponentAction_1_UpdateCache_m4044781172(__this, ___go0, method) ((  bool (*) (ComponentAction_1_t2346840805 *, GameObject_t3674682005 *, const MethodInfo*))ComponentAction_1_UpdateCache_m1764863969_gshared)(__this, ___go0, method)
