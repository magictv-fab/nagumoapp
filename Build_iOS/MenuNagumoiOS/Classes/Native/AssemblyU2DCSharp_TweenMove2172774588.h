﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenMove
struct  TweenMove_t2172774588  : public MonoBehaviour_t667441552
{
public:
	// iTween/EaseType TweenMove::easetype
	int32_t ___easetype_2;
	// UnityEngine.Vector3 TweenMove::position
	Vector3_t4282066566  ___position_3;
	// System.Single TweenMove::time
	float ___time_4;
	// UnityEngine.Vector3 TweenMove::startPositon
	Vector3_t4282066566  ___startPositon_5;
	// System.Boolean TweenMove::flipflop
	bool ___flipflop_6;

public:
	inline static int32_t get_offset_of_easetype_2() { return static_cast<int32_t>(offsetof(TweenMove_t2172774588, ___easetype_2)); }
	inline int32_t get_easetype_2() const { return ___easetype_2; }
	inline int32_t* get_address_of_easetype_2() { return &___easetype_2; }
	inline void set_easetype_2(int32_t value)
	{
		___easetype_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(TweenMove_t2172774588, ___position_3)); }
	inline Vector3_t4282066566  get_position_3() const { return ___position_3; }
	inline Vector3_t4282066566 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector3_t4282066566  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(TweenMove_t2172774588, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_startPositon_5() { return static_cast<int32_t>(offsetof(TweenMove_t2172774588, ___startPositon_5)); }
	inline Vector3_t4282066566  get_startPositon_5() const { return ___startPositon_5; }
	inline Vector3_t4282066566 * get_address_of_startPositon_5() { return &___startPositon_5; }
	inline void set_startPositon_5(Vector3_t4282066566  value)
	{
		___startPositon_5 = value;
	}

	inline static int32_t get_offset_of_flipflop_6() { return static_cast<int32_t>(offsetof(TweenMove_t2172774588, ___flipflop_6)); }
	inline bool get_flipflop_6() const { return ___flipflop_6; }
	inline bool* get_address_of_flipflop_6() { return &___flipflop_6; }
	inline void set_flipflop_6(bool value)
	{
		___flipflop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
