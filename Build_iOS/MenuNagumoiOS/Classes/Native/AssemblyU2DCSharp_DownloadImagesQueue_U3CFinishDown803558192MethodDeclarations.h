﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImagesQueue/<FinishDownload>c__Iterator37
struct U3CFinishDownloadU3Ec__Iterator37_t803558192;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DownloadImagesQueue/<FinishDownload>c__Iterator37::.ctor()
extern "C"  void U3CFinishDownloadU3Ec__Iterator37__ctor_m2498973755 (U3CFinishDownloadU3Ec__Iterator37_t803558192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImagesQueue/<FinishDownload>c__Iterator37::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFinishDownloadU3Ec__Iterator37_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4236045623 (U3CFinishDownloadU3Ec__Iterator37_t803558192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImagesQueue/<FinishDownload>c__Iterator37::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFinishDownloadU3Ec__Iterator37_System_Collections_IEnumerator_get_Current_m117849803 (U3CFinishDownloadU3Ec__Iterator37_t803558192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DownloadImagesQueue/<FinishDownload>c__Iterator37::MoveNext()
extern "C"  bool U3CFinishDownloadU3Ec__Iterator37_MoveNext_m1563526361 (U3CFinishDownloadU3Ec__Iterator37_t803558192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue/<FinishDownload>c__Iterator37::Dispose()
extern "C"  void U3CFinishDownloadU3Ec__Iterator37_Dispose_m525343160 (U3CFinishDownloadU3Ec__Iterator37_t803558192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImagesQueue/<FinishDownload>c__Iterator37::Reset()
extern "C"  void U3CFinishDownloadU3Ec__Iterator37_Reset_m145406696 (U3CFinishDownloadU3Ec__Iterator37_t803558192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
