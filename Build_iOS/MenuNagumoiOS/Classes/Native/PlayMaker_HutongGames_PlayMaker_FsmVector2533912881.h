﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVector2
struct  FsmVector2_t533912881  : public NamedVariable_t3211770239
{
public:
	// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVector2::value
	Vector2_t4282066565  ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmVector2_t533912881, ___value_5)); }
	inline Vector2_t4282066565  get_value_5() const { return ___value_5; }
	inline Vector2_t4282066565 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Vector2_t4282066565  value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
