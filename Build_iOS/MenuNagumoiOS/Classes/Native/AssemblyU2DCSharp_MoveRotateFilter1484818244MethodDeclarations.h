﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveRotateFilter
struct MoveRotateFilter_t1484818244;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void MoveRotateFilter::.ctor()
extern "C"  void MoveRotateFilter__ctor_m3187575767 (MoveRotateFilter_t1484818244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateFilter::Start()
extern "C"  void MoveRotateFilter_Start_m2134713559 (MoveRotateFilter_t1484818244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MoveRotateFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  MoveRotateFilter__doFilter_m3917929015 (MoveRotateFilter_t1484818244 * __this, Quaternion_t1553702882  ___currentValue0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 MoveRotateFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  MoveRotateFilter__doFilter_m542272431 (MoveRotateFilter_t1484818244 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateFilter::Update()
extern "C"  void MoveRotateFilter_Update_m1757463062 (MoveRotateFilter_t1484818244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateFilter::LateUpdate()
extern "C"  void MoveRotateFilter_LateUpdate_m1779928284 (MoveRotateFilter_t1484818244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateFilter::doFollow()
extern "C"  void MoveRotateFilter_doFollow_m2729644425 (MoveRotateFilter_t1484818244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MoveRotateFilter::doRotate(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  MoveRotateFilter_doRotate_m2967783750 (MoveRotateFilter_t1484818244 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 MoveRotateFilter::doMovePosition(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  MoveRotateFilter_doMovePosition_m3400474349 (MoveRotateFilter_t1484818244 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
