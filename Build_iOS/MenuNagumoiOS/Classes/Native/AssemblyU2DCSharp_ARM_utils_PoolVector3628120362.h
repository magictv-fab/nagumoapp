﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolFloat
struct PoolFloat_t382107510;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.PoolVector3
struct  PoolVector3_t628120362  : public Il2CppObject
{
public:
	// ARM.utils.PoolFloat ARM.utils.PoolVector3::_x
	PoolFloat_t382107510 * ____x_0;
	// ARM.utils.PoolFloat ARM.utils.PoolVector3::_y
	PoolFloat_t382107510 * ____y_1;
	// ARM.utils.PoolFloat ARM.utils.PoolVector3::_z
	PoolFloat_t382107510 * ____z_2;
	// System.Int32 ARM.utils.PoolVector3::_currentIndex
	int32_t ____currentIndex_3;
	// System.UInt32 ARM.utils.PoolVector3::_maxSize
	uint32_t ____maxSize_4;
	// System.Int32 ARM.utils.PoolVector3::_currentSize
	int32_t ____currentSize_5;
	// UnityEngine.Vector3[] ARM.utils.PoolVector3::itens
	Vector3U5BU5D_t215400611* ___itens_6;

public:
	inline static int32_t get_offset_of__x_0() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ____x_0)); }
	inline PoolFloat_t382107510 * get__x_0() const { return ____x_0; }
	inline PoolFloat_t382107510 ** get_address_of__x_0() { return &____x_0; }
	inline void set__x_0(PoolFloat_t382107510 * value)
	{
		____x_0 = value;
		Il2CppCodeGenWriteBarrier(&____x_0, value);
	}

	inline static int32_t get_offset_of__y_1() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ____y_1)); }
	inline PoolFloat_t382107510 * get__y_1() const { return ____y_1; }
	inline PoolFloat_t382107510 ** get_address_of__y_1() { return &____y_1; }
	inline void set__y_1(PoolFloat_t382107510 * value)
	{
		____y_1 = value;
		Il2CppCodeGenWriteBarrier(&____y_1, value);
	}

	inline static int32_t get_offset_of__z_2() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ____z_2)); }
	inline PoolFloat_t382107510 * get__z_2() const { return ____z_2; }
	inline PoolFloat_t382107510 ** get_address_of__z_2() { return &____z_2; }
	inline void set__z_2(PoolFloat_t382107510 * value)
	{
		____z_2 = value;
		Il2CppCodeGenWriteBarrier(&____z_2, value);
	}

	inline static int32_t get_offset_of__currentIndex_3() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ____currentIndex_3)); }
	inline int32_t get__currentIndex_3() const { return ____currentIndex_3; }
	inline int32_t* get_address_of__currentIndex_3() { return &____currentIndex_3; }
	inline void set__currentIndex_3(int32_t value)
	{
		____currentIndex_3 = value;
	}

	inline static int32_t get_offset_of__maxSize_4() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ____maxSize_4)); }
	inline uint32_t get__maxSize_4() const { return ____maxSize_4; }
	inline uint32_t* get_address_of__maxSize_4() { return &____maxSize_4; }
	inline void set__maxSize_4(uint32_t value)
	{
		____maxSize_4 = value;
	}

	inline static int32_t get_offset_of__currentSize_5() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ____currentSize_5)); }
	inline int32_t get__currentSize_5() const { return ____currentSize_5; }
	inline int32_t* get_address_of__currentSize_5() { return &____currentSize_5; }
	inline void set__currentSize_5(int32_t value)
	{
		____currentSize_5 = value;
	}

	inline static int32_t get_offset_of_itens_6() { return static_cast<int32_t>(offsetof(PoolVector3_t628120362, ___itens_6)); }
	inline Vector3U5BU5D_t215400611* get_itens_6() const { return ___itens_6; }
	inline Vector3U5BU5D_t215400611** get_address_of_itens_6() { return &___itens_6; }
	inline void set_itens_6(Vector3U5BU5D_t215400611* value)
	{
		___itens_6 = value;
		Il2CppCodeGenWriteBarrier(&___itens_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
