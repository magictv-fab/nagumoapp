﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.GenericTimeControllAbstract
struct GenericTimeControllAbstract_t1375523775;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.animation.GenericTimeControllAbstract::.ctor()
extern "C"  void GenericTimeControllAbstract__ctor_m2400087538 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.animation.GenericTimeControllAbstract::isPlaying()
extern "C"  bool GenericTimeControllAbstract_isPlaying_m2811406856 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericTimeControllAbstract::Play()
extern "C"  void GenericTimeControllAbstract_Play_m2859681606 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericTimeControllAbstract::Stop()
extern "C"  void GenericTimeControllAbstract_Stop_m2953365652 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericTimeControllAbstract::Pause()
extern "C"  void GenericTimeControllAbstract_Pause_m2454213510 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericTimeControllAbstract::_onFinished()
extern "C"  void GenericTimeControllAbstract__onFinished_m815059200 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericTimeControllAbstract::_onStart()
extern "C"  void GenericTimeControllAbstract__onStart_m598383606 (GenericTimeControllAbstract_t1375523775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
