﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.InAppActorAbstract
struct InAppActorAbstract_t1901198337;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"

// System.Void InApp.InAppActorAbstract::.ctor()
extern "C"  void InAppActorAbstract__ctor_m3917500590 (InAppActorAbstract_t1901198337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppActorAbstract::Awake()
extern "C"  void InAppActorAbstract_Awake_m4155105809 (InAppActorAbstract_t1901198337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppActorAbstract::ActorRun()
extern "C"  void InAppActorAbstract_ActorRun_m4011079468 (InAppActorAbstract_t1901198337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppActorAbstract::ActorStop()
extern "C"  void InAppActorAbstract_ActorStop_m4112183843 (InAppActorAbstract_t1901198337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppActorAbstract::applyActorConfig(MagicTV.vo.BundleVO)
extern "C"  void InAppActorAbstract_applyActorConfig_m2381136034 (InAppActorAbstract_t1901198337 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppActorAbstract::DefautOrientation()
extern "C"  void InAppActorAbstract_DefautOrientation_m812951105 (InAppActorAbstract_t1901198337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
