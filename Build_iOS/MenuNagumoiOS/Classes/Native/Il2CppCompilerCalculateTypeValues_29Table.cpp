﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoI689169466.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderT3316332105.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg1650423632.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg1649924032.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg4096057777.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarge614449126.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev2518788125.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetIm1837478850.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTempl3973358313.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image2247677317.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXE354375056.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageImpl2172378181.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2771251825.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg1650084406.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Tracker3880226402.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac455954211.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrac243160803.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerImpl756848636.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTra4028259784.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTra1934199560.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTarg3488594068.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTex2229337627.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullWebCa4189520834.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeE1981907378.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeE1478921838.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumOb1788967734.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumOb3790484746.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1442390413.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa2162615373.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa2263627731.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan459380335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1781697161.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1781697192.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1826866697.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1548845516.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan725431450.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1047832367.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa3919795561.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1737958143.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan526813605.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1066652956.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1341527372.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa3728673716.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRen700547663.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2019914778.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2868837278.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe1534193604.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRen172958837.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe4078668580.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2217546767.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe1888443502.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn1802138261.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr3253421379.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceIm4225773975.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2057130681.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropImpl1612666277.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr2067565974.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerr1058218966.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrack3721656949.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrack2086340917.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrack3876737300.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TypeMappi2898721118.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexA139577819.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordImpl2634698188.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefa2200707570.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManag2702921325.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManag3815219501.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResul1079862857.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResul1642228489.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordList2634783818.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordListI2962471050.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNa3177738953.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNu1083833836.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNa4290029276.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaWr2202637939.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAlive2256617941.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Reconstru1860057025.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstr1293468098.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateMana3262709086.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (CloudRecoImageTargetImpl_t689169466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[1] = 
{
	CloudRecoImageTargetImpl_t689169466::get_offset_of_mSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (CylinderTargetImpl_t3316332105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[3] = 
{
	CylinderTargetImpl_t3316332105::get_offset_of_mSideLength_4(),
	CylinderTargetImpl_t3316332105::get_offset_of_mTopDiameter_5(),
	CylinderTargetImpl_t3316332105::get_offset_of_mBottomDiameter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (ImageTargetType_t1650423632)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2903[4] = 
{
	ImageTargetType_t1650423632::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (ImageTargetData_t1649924032)+ sizeof (Il2CppObject), sizeof(ImageTargetData_t1649924032_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2904[2] = 
{
	ImageTargetData_t1649924032::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageTargetData_t1649924032::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (ImageTargetBuilder_t4096057777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (FrameQuality_t614449126)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2906[5] = 
{
	FrameQuality_t614449126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (CameraDeviceImpl_t2518788125), -1, sizeof(CameraDeviceImpl_t2518788125_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2907[10] = 
{
	CameraDeviceImpl_t2518788125::get_offset_of_mCameraImages_1(),
	CameraDeviceImpl_t2518788125::get_offset_of_mForcedCameraFormats_2(),
	CameraDeviceImpl_t2518788125_StaticFields::get_offset_of_mWebCam_3(),
	CameraDeviceImpl_t2518788125::get_offset_of_mCameraReady_4(),
	CameraDeviceImpl_t2518788125::get_offset_of_mIsDirty_5(),
	CameraDeviceImpl_t2518788125::get_offset_of_mCameraDirection_6(),
	CameraDeviceImpl_t2518788125::get_offset_of_mCameraDeviceMode_7(),
	CameraDeviceImpl_t2518788125::get_offset_of_mVideoModeData_8(),
	CameraDeviceImpl_t2518788125::get_offset_of_mVideoModeDataNeedsUpdate_9(),
	CameraDeviceImpl_t2518788125::get_offset_of_mHasCameraDeviceModeBeenSet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (DataSetImpl_t1837478850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[4] = 
{
	DataSetImpl_t1837478850::get_offset_of_mDataSetPtr_0(),
	DataSetImpl_t1837478850::get_offset_of_mPath_1(),
	DataSetImpl_t1837478850::get_offset_of_mStorageType_2(),
	DataSetImpl_t1837478850::get_offset_of_mTrackablesDict_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (WordTemplateMode_t3973358313)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2915[3] = 
{
	WordTemplateMode_t3973358313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (Image_t2247677317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (PIXEL_FORMAT_t354375056)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2918[7] = 
{
	PIXEL_FORMAT_t354375056::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (ImageImpl_t2172378181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[10] = 
{
	ImageImpl_t2172378181::get_offset_of_mWidth_0(),
	ImageImpl_t2172378181::get_offset_of_mHeight_1(),
	ImageImpl_t2172378181::get_offset_of_mStride_2(),
	ImageImpl_t2172378181::get_offset_of_mBufferWidth_3(),
	ImageImpl_t2172378181::get_offset_of_mBufferHeight_4(),
	ImageImpl_t2172378181::get_offset_of_mPixelFormat_5(),
	ImageImpl_t2172378181::get_offset_of_mData_6(),
	ImageImpl_t2172378181::get_offset_of_mUnmanagedData_7(),
	ImageImpl_t2172378181::get_offset_of_mDataSet_8(),
	ImageImpl_t2172378181::get_offset_of_mPixel32_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (ImageTargetBuilderImpl_t2771251825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[1] = 
{
	ImageTargetBuilderImpl_t2771251825::get_offset_of_mTrackableSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (ImageTargetImpl_t1650084406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[2] = 
{
	ImageTargetImpl_t1650084406::get_offset_of_mImageTargetType_4(),
	ImageTargetImpl_t1650084406::get_offset_of_mVirtualButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (Tracker_t3880226402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[1] = 
{
	Tracker_t3880226402::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (ObjectTracker_t455954211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (ObjectTrackerImpl_t243160803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	ObjectTrackerImpl_t243160803::get_offset_of_mActiveDataSets_1(),
	ObjectTrackerImpl_t243160803::get_offset_of_mDataSets_2(),
	ObjectTrackerImpl_t243160803::get_offset_of_mImageTargetBuilder_3(),
	ObjectTrackerImpl_t243160803::get_offset_of_mTargetFinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (MarkerImpl_t756848636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[2] = 
{
	MarkerImpl_t756848636::get_offset_of_mSize_2(),
	MarkerImpl_t756848636::get_offset_of_U3CMarkerIDU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (MarkerTracker_t4028259784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (MarkerTrackerImpl_t1934199560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[1] = 
{
	MarkerTrackerImpl_t1934199560::get_offset_of_mMarkerDict_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (MultiTargetImpl_t3488594068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (WebCamTexAdaptor_t2229337627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (NullWebCamTexAdaptor_t4189520834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[5] = 
{
	0,
	NullWebCamTexAdaptor_t4189520834::get_offset_of_mTexture_1(),
	NullWebCamTexAdaptor_t4189520834::get_offset_of_mPseudoPlaying_2(),
	NullWebCamTexAdaptor_t4189520834::get_offset_of_mMsBetweenFrames_3(),
	NullWebCamTexAdaptor_t4189520834::get_offset_of_mLastFrame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (PlayModeEditorUtility_t1981907378), -1, sizeof(PlayModeEditorUtility_t1981907378_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2934[1] = 
{
	PlayModeEditorUtility_t1981907378_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (NullPlayModeEditorUtility_t1478921838), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (PremiumObjectFactory_t1788967734), -1, sizeof(PremiumObjectFactory_t1788967734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2937[1] = 
{
	PremiumObjectFactory_t1788967734_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (NullPremiumObjectFactory_t3790484746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (VuforiaManager_t1442390413), -1, sizeof(VuforiaManager_t1442390413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2939[1] = 
{
	VuforiaManager_t1442390413_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (VuforiaManagerImpl_t2162615373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[20] = 
{
	VuforiaManagerImpl_t2162615373::get_offset_of_mWorldCenterMode_1(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mWorldCenter_2(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mARCameraTransform_3(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mCentralAnchorPoint_4(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mParentAnchorPoint_5(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mTrackableResultDataArray_6(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mWordDataArray_7(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mWordResultDataArray_8(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mTrackableFoundQueue_9(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mImageHeaderData_10(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mNumImageHeaders_11(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mInjectedFrameIdx_12(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mLastProcessedFrameStatePtr_13(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mInitialized_14(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mPaused_15(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mFrameState_16(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mAutoRotationState_17(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mVideoBackgroundNeedsRedrawing_18(),
	VuforiaManagerImpl_t2162615373::get_offset_of_mDiscardStatesForRendering_19(),
	VuforiaManagerImpl_t2162615373::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (PoseData_t2263627731)+ sizeof (Il2CppObject), sizeof(PoseData_t2263627731_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2941[3] = 
{
	PoseData_t2263627731::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t2263627731::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t2263627731::get_offset_of_unused_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (TrackableResultData_t395876724)+ sizeof (Il2CppObject), sizeof(TrackableResultData_t395876724_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2942[3] = 
{
	TrackableResultData_t395876724::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t395876724::get_offset_of_status_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t395876724::get_offset_of_id_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (VirtualButtonData_t459380335)+ sizeof (Il2CppObject), sizeof(VirtualButtonData_t459380335_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2943[2] = 
{
	VirtualButtonData_t459380335::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VirtualButtonData_t459380335::get_offset_of_isPressed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (Obb2D_t1781697161)+ sizeof (Il2CppObject), sizeof(Obb2D_t1781697161_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2944[4] = 
{
	Obb2D_t1781697161::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t1781697161::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t1781697161::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t1781697161::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (Obb3D_t1781697192)+ sizeof (Il2CppObject), sizeof(Obb3D_t1781697192_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2945[4] = 
{
	Obb3D_t1781697192::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t1781697192::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t1781697192::get_offset_of_rotationZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t1781697192::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (WordResultData_t1826866697)+ sizeof (Il2CppObject), sizeof(WordResultData_t1826866697_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2946[4] = 
{
	WordResultData_t1826866697::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1826866697::get_offset_of_status_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1826866697::get_offset_of_id_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1826866697::get_offset_of_orientedBoundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (WordData_t1548845516)+ sizeof (Il2CppObject), sizeof(WordData_t1548845516_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2947[4] = 
{
	WordData_t1548845516::get_offset_of_stringValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1548845516::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1548845516::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1548845516::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (ImageHeaderData_t725431450)+ sizeof (Il2CppObject), sizeof(ImageHeaderData_t725431450_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2948[9] = 
{
	ImageHeaderData_t725431450::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_width_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_height_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_stride_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_bufferWidth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_bufferHeight_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_format_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_reallocate_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t725431450::get_offset_of_updated_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (MeshData_t1047832367)+ sizeof (Il2CppObject), sizeof(MeshData_t1047832367_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2949[7] = 
{
	MeshData_t1047832367::get_offset_of_positionsArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1047832367::get_offset_of_normalsArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1047832367::get_offset_of_triangleIdxArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1047832367::get_offset_of_numVertexValues_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1047832367::get_offset_of_hasNormals_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1047832367::get_offset_of_numTriangleIndices_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1047832367::get_offset_of_unused_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (SmartTerrainRevisionData_t3919795561)+ sizeof (Il2CppObject), sizeof(SmartTerrainRevisionData_t3919795561_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	SmartTerrainRevisionData_t3919795561::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmartTerrainRevisionData_t3919795561::get_offset_of_revision_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (SurfaceData_t1737958143)+ sizeof (Il2CppObject), sizeof(SurfaceData_t1737958143_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2951[9] = 
{
	SurfaceData_t1737958143::get_offset_of_meshBoundaryArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_meshData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_navMeshData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_localPose_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_parentID_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_numBoundaryIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1737958143::get_offset_of_revision_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (PropData_t526813605)+ sizeof (Il2CppObject), sizeof(PropData_t526813605_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2952[8] = 
{
	PropData_t526813605::get_offset_of_meshData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_parentID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_localPosition_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_localPose_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_revision_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t526813605::get_offset_of_unused_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (FrameState_t1066652956)+ sizeof (Il2CppObject), sizeof(FrameState_t1066652956_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2953[18] = 
{
	FrameState_t1066652956::get_offset_of_trackableDataArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_vbDataArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_wordResultArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_newWordDataArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_propTrackableDataArray_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_smartTerrainRevisionsArray_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_updatedSurfacesArray_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_updatedPropsArray_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numTrackableResults_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numVirtualButtonResults_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_frameIndex_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numWordResults_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numNewWords_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numPropTrackableResults_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numSmartTerrainRevisions_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numUpdatedSurfaces_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_numUpdatedProps_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t1066652956::get_offset_of_unused_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (AutoRotationState_t1341527372)+ sizeof (Il2CppObject), sizeof(AutoRotationState_t1341527372_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2954[5] = 
{
	AutoRotationState_t1341527372::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t1341527372::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t1341527372::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t1341527372::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t1341527372::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (U3CU3Ec__DisplayClass3_t3728673716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	U3CU3Ec__DisplayClass3_t3728673716::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (VuforiaRenderer_t700547663), -1, sizeof(VuforiaRenderer_t700547663_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2956[1] = 
{
	VuforiaRenderer_t700547663_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (FpsHint_t2019914778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2957[5] = 
{
	FpsHint_t2019914778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (VideoBackgroundReflection_t2868837278)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2958[4] = 
{
	VideoBackgroundReflection_t2868837278::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (VideoBGCfgData_t1534193604)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t1534193604_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2959[4] = 
{
	VideoBGCfgData_t1534193604::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t1534193604::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t1534193604::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t1534193604::get_offset_of_reflection_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (Vec2I_t172958837)+ sizeof (Il2CppObject), sizeof(Vec2I_t172958837_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2960[2] = 
{
	Vec2I_t172958837::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t172958837::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (VideoTextureInfo_t4078668580)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t4078668580_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2961[2] = 
{
	VideoTextureInfo_t4078668580::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t4078668580::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (VuforiaRendererImpl_t2217546767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[5] = 
{
	VuforiaRendererImpl_t2217546767::get_offset_of_mVideoBGConfig_1(),
	VuforiaRendererImpl_t2217546767::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRendererImpl_t2217546767::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRendererImpl_t2217546767::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRendererImpl_t2217546767::get_offset_of_mLastSetReflection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (RenderEvent_t1888443502)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2963[7] = 
{
	RenderEvent_t1888443502::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (VuforiaUnityImpl_t1802138261), -1, sizeof(VuforiaUnityImpl_t1802138261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2964[1] = 
{
	VuforiaUnityImpl_t1802138261_StaticFields::get_offset_of_mRendererDirty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (SmartTerrainTrackableImpl_t3253421379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[5] = 
{
	SmartTerrainTrackableImpl_t3253421379::get_offset_of_mChildren_2(),
	SmartTerrainTrackableImpl_t3253421379::get_offset_of_mMesh_3(),
	SmartTerrainTrackableImpl_t3253421379::get_offset_of_mMeshRevision_4(),
	SmartTerrainTrackableImpl_t3253421379::get_offset_of_mLocalPose_5(),
	SmartTerrainTrackableImpl_t3253421379::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (SurfaceImpl_t4225773975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[5] = 
{
	SurfaceImpl_t4225773975::get_offset_of_mNavMesh_7(),
	SurfaceImpl_t4225773975::get_offset_of_mMeshBoundaries_8(),
	SurfaceImpl_t4225773975::get_offset_of_mBoundingBox_9(),
	SurfaceImpl_t4225773975::get_offset_of_mSurfaceArea_10(),
	SurfaceImpl_t4225773975::get_offset_of_mAreaNeedsUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (SmartTerrainBuilderImpl_t2057130681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[2] = 
{
	SmartTerrainBuilderImpl_t2057130681::get_offset_of_mReconstructionBehaviours_0(),
	SmartTerrainBuilderImpl_t2057130681::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (PropImpl_t1612666277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	PropImpl_t1612666277::get_offset_of_mOrientedBoundingBox3D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (SmartTerrainTracker_t2067565974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (SmartTerrainTrackerImpl_t1058218966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[2] = 
{
	SmartTerrainTrackerImpl_t1058218966::get_offset_of_mScaleToMillimeter_1(),
	SmartTerrainTrackerImpl_t1058218966::get_offset_of_mSmartTerrainBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (TextTracker_t3721656949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (TextTrackerImpl_t2086340917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[1] = 
{
	TextTrackerImpl_t2086340917::get_offset_of_mWordList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (UpDirection_t3876737300)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2976[5] = 
{
	UpDirection_t3876737300::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (TypeMapping_t2898721118), -1, sizeof(TypeMapping_t2898721118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2977[1] = 
{
	TypeMapping_t2898721118_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (WebCamTexAdaptorImpl_t139577819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[1] = 
{
	WebCamTexAdaptorImpl_t139577819::get_offset_of_mWebCamTexture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (WordImpl_t2634698188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[5] = 
{
	WordImpl_t2634698188::get_offset_of_mText_2(),
	WordImpl_t2634698188::get_offset_of_mSize_3(),
	WordImpl_t2634698188::get_offset_of_mLetterMask_4(),
	WordImpl_t2634698188::get_offset_of_mLetterImageHeader_5(),
	WordImpl_t2634698188::get_offset_of_mLetterBoundingBoxes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (WordPrefabCreationMode_t2200707570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2981[3] = 
{
	WordPrefabCreationMode_t2200707570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (WordManager_t2702921325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (WordManagerImpl_t3815219501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[11] = 
{
	0,
	WordManagerImpl_t3815219501::get_offset_of_mTrackedWords_1(),
	WordManagerImpl_t3815219501::get_offset_of_mNewWords_2(),
	WordManagerImpl_t3815219501::get_offset_of_mLostWords_3(),
	WordManagerImpl_t3815219501::get_offset_of_mActiveWordBehaviours_4(),
	WordManagerImpl_t3815219501::get_offset_of_mWordBehavioursMarkedForDeletion_5(),
	WordManagerImpl_t3815219501::get_offset_of_mWaitingQueue_6(),
	WordManagerImpl_t3815219501::get_offset_of_mWordBehaviours_7(),
	WordManagerImpl_t3815219501::get_offset_of_mAutomaticTemplate_8(),
	WordManagerImpl_t3815219501::get_offset_of_mMaxInstances_9(),
	WordManagerImpl_t3815219501::get_offset_of_mWordPrefabCreationMode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (WordResult_t1079862857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (WordResultImpl_t1642228489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[5] = 
{
	WordResultImpl_t1642228489::get_offset_of_mObb_0(),
	WordResultImpl_t1642228489::get_offset_of_mPosition_1(),
	WordResultImpl_t1642228489::get_offset_of_mOrientation_2(),
	WordResultImpl_t1642228489::get_offset_of_mWord_3(),
	WordResultImpl_t1642228489::get_offset_of_mStatus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (WordList_t2634783818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (WordListImpl_t2962471050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (VuforiaNativeIosWrapper_t3177738953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (VuforiaNullWrapper_t1083833836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (VuforiaNativeWrapper_t4290029276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (VuforiaWrapper_t2202637939), -1, sizeof(VuforiaWrapper_t2202637939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2992[1] = 
{
	VuforiaWrapper_t2202637939_StaticFields::get_offset_of_sWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (KeepAliveAbstractBehaviour_t2256617941), -1, sizeof(KeepAliveAbstractBehaviour_t2256617941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2996[8] = 
{
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepARCameraAlive_2(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepTrackableBehavioursAlive_3(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepTextRecoBehaviourAlive_4(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepUDTBuildingBehaviourAlive_5(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepCloudRecoBehaviourAlive_6(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mKeepSmartTerrainAlive_7(),
	KeepAliveAbstractBehaviour_t2256617941_StaticFields::get_offset_of_sKeepAliveBehaviour_8(),
	KeepAliveAbstractBehaviour_t2256617941::get_offset_of_mHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (ReconstructionAbstractBehaviour_t1860057025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[22] = 
{
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t1860057025::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (PropAbstractBehaviour_t1293468098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[2] = 
{
	PropAbstractBehaviour_t1293468098::get_offset_of_mProp_13(),
	PropAbstractBehaviour_t1293468098::get_offset_of_mBoxColliderToUpdate_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (StateManager_t3262709086), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
