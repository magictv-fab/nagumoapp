﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CreateEmptyObject
struct CreateEmptyObject_t3131320478;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CreateEmptyObject::.ctor()
extern "C"  void CreateEmptyObject__ctor_m700991064 (CreateEmptyObject_t3131320478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateEmptyObject::Reset()
extern "C"  void CreateEmptyObject_Reset_m2642391301 (CreateEmptyObject_t3131320478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateEmptyObject::OnEnter()
extern "C"  void CreateEmptyObject_OnEnter_m3029242927 (CreateEmptyObject_t3131320478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
