﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.HighLevelEncoder
struct HighLevelEncoder_t3204486112;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>
struct ICollection_1_t3960013622;
// System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>
struct IEnumerable_1_t2071369296;
// ZXing.Aztec.Internal.State
struct State_t3065423635;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_State3065423635.h"

// System.Void ZXing.Aztec.Internal.HighLevelEncoder::.cctor()
extern "C"  void HighLevelEncoder__cctor_m712434030 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.HighLevelEncoder::.ctor(System.Byte[])
extern "C"  void HighLevelEncoder__ctor_m4273830922 (HighLevelEncoder_t3204486112 * __this, ByteU5BU5D_t4260760469* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Aztec.Internal.HighLevelEncoder::encode()
extern "C"  BitArray_t4163851164 * HighLevelEncoder_encode_m2727550573 (HighLevelEncoder_t3204486112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::updateStateListForChar(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>,System.Int32)
extern "C"  Il2CppObject* HighLevelEncoder_updateStateListForChar_m3621527168 (HighLevelEncoder_t3204486112 * __this, Il2CppObject* ___states0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.HighLevelEncoder::updateStateForChar(ZXing.Aztec.Internal.State,System.Int32,System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>)
extern "C"  void HighLevelEncoder_updateStateForChar_m2855680128 (HighLevelEncoder_t3204486112 * __this, State_t3065423635 * ___state0, int32_t ___index1, Il2CppObject* ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::updateStateListForPair(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>,System.Int32,System.Int32)
extern "C"  Il2CppObject* HighLevelEncoder_updateStateListForPair_m1792355803 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___states0, int32_t ___index1, int32_t ___pairCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.HighLevelEncoder::updateStateForPair(ZXing.Aztec.Internal.State,System.Int32,System.Int32,System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>)
extern "C"  void HighLevelEncoder_updateStateForPair_m2112177209 (Il2CppObject * __this /* static, unused */, State_t3065423635 * ___state0, int32_t ___index1, int32_t ___pairCode2, Il2CppObject* ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::simplifyStates(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>)
extern "C"  Il2CppObject* HighLevelEncoder_simplifyStates_m1843477193 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___states0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
