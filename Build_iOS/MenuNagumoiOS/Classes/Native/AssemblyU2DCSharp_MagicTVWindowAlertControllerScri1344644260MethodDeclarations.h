﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowAlertControllerScript
struct MagicTVWindowAlertControllerScript_t1344644260;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowAlertControllerScript::.ctor()
extern "C"  void MagicTVWindowAlertControllerScript__ctor_m3340903543 (MagicTVWindowAlertControllerScript_t1344644260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAlertControllerScript::Hide()
extern "C"  void MagicTVWindowAlertControllerScript_Hide_m2242654895 (MagicTVWindowAlertControllerScript_t1344644260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
