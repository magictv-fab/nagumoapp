﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CreateEmptyObject
struct  CreateEmptyObject_t3131320478  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateEmptyObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateEmptyObject::spawnPoint
	FsmGameObject_t1697147867 * ___spawnPoint_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateEmptyObject::position
	FsmVector3_t533912882 * ___position_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateEmptyObject::rotation
	FsmVector3_t533912882 * ___rotation_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateEmptyObject::storeObject
	FsmGameObject_t1697147867 * ___storeObject_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t3131320478, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_spawnPoint_10() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t3131320478, ___spawnPoint_10)); }
	inline FsmGameObject_t1697147867 * get_spawnPoint_10() const { return ___spawnPoint_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_spawnPoint_10() { return &___spawnPoint_10; }
	inline void set_spawnPoint_10(FsmGameObject_t1697147867 * value)
	{
		___spawnPoint_10 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPoint_10, value);
	}

	inline static int32_t get_offset_of_position_11() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t3131320478, ___position_11)); }
	inline FsmVector3_t533912882 * get_position_11() const { return ___position_11; }
	inline FsmVector3_t533912882 ** get_address_of_position_11() { return &___position_11; }
	inline void set_position_11(FsmVector3_t533912882 * value)
	{
		___position_11 = value;
		Il2CppCodeGenWriteBarrier(&___position_11, value);
	}

	inline static int32_t get_offset_of_rotation_12() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t3131320478, ___rotation_12)); }
	inline FsmVector3_t533912882 * get_rotation_12() const { return ___rotation_12; }
	inline FsmVector3_t533912882 ** get_address_of_rotation_12() { return &___rotation_12; }
	inline void set_rotation_12(FsmVector3_t533912882 * value)
	{
		___rotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_12, value);
	}

	inline static int32_t get_offset_of_storeObject_13() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t3131320478, ___storeObject_13)); }
	inline FsmGameObject_t1697147867 * get_storeObject_13() const { return ___storeObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeObject_13() { return &___storeObject_13; }
	inline void set_storeObject_13(FsmGameObject_t1697147867 * value)
	{
		___storeObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeObject_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
