﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISampleAppUIElement
struct ISampleAppUIElement_t2180050874;
// UnityEngine.Font
struct Font_t4241557075;

#include "codegen/il2cpp-codegen.h"

// System.Void ISampleAppUIElement::.ctor()
extern "C"  void ISampleAppUIElement__ctor_m1546007793 (ISampleAppUIElement_t2180050874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISampleAppUIElement::Draw()
extern "C"  void ISampleAppUIElement_Draw_m3048309367 (ISampleAppUIElement_t2180050874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font ISampleAppUIElement::get_font()
extern "C"  Font_t4241557075 * ISampleAppUIElement_get_font_m1519690068 (ISampleAppUIElement_t2180050874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ISampleAppUIElement::get_Height()
extern "C"  float ISampleAppUIElement_get_Height_m3367275415 (ISampleAppUIElement_t2180050874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISampleAppUIElement::set_Height(System.Single)
extern "C"  void ISampleAppUIElement_set_Height_m3751888820 (ISampleAppUIElement_t2180050874 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ISampleAppUIElement::get_Width()
extern "C"  float ISampleAppUIElement_get_Width_m369464376 (ISampleAppUIElement_t2180050874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ISampleAppUIElement::set_Width(System.Single)
extern "C"  void ISampleAppUIElement_set_Width_m1566145587 (ISampleAppUIElement_t2180050874 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
