﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNWP8RateUsPopUp
struct MNWP8RateUsPopUp_t4179652016;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNWP8RateUsPopUp::.ctor()
extern "C"  void MNWP8RateUsPopUp__ctor_m1792876267 (MNWP8RateUsPopUp_t4179652016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNWP8RateUsPopUp MNWP8RateUsPopUp::Create(System.String,System.String)
extern "C"  MNWP8RateUsPopUp_t4179652016 * MNWP8RateUsPopUp_Create_m246972348 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8RateUsPopUp::init()
extern "C"  void MNWP8RateUsPopUp_init_m2865166825 (MNWP8RateUsPopUp_t4179652016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8RateUsPopUp::OnOkDel()
extern "C"  void MNWP8RateUsPopUp_OnOkDel_m218815481 (MNWP8RateUsPopUp_t4179652016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8RateUsPopUp::OnCancelDel()
extern "C"  void MNWP8RateUsPopUp_OnCancelDel_m2502242715 (MNWP8RateUsPopUp_t4179652016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
