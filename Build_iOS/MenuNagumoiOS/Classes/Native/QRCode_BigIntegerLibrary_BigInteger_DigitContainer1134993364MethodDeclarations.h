﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BigIntegerLibrary.BigInteger/DigitContainer
struct DigitContainer_t1134993364;

#include "codegen/il2cpp-codegen.h"

// System.Void BigIntegerLibrary.BigInteger/DigitContainer::.ctor()
extern "C"  void DigitContainer__ctor_m225386871 (DigitContainer_t1134993364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 BigIntegerLibrary.BigInteger/DigitContainer::get_Item(System.Int32)
extern "C"  int64_t DigitContainer_get_Item_m559199467 (DigitContainer_t1134993364 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
extern "C"  void DigitContainer_set_Item_m265023882 (DigitContainer_t1134993364 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
