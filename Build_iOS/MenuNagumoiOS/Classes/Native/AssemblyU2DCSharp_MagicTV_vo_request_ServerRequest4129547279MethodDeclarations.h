﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerRequestParamsDeviceVO
struct ServerRequestParamsDeviceVO_t4129547279;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.request.ServerRequestParamsDeviceVO::.ctor()
extern "C"  void ServerRequestParamsDeviceVO__ctor_m2192972719 (ServerRequestParamsDeviceVO_t4129547279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.request.ServerRequestParamsDeviceVO::ToURLString()
extern "C"  String_t* ServerRequestParamsDeviceVO_ToURLString_m1277850991 (ServerRequestParamsDeviceVO_t4129547279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
