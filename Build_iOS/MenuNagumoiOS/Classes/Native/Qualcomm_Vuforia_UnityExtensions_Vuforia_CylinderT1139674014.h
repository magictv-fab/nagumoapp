﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.CylinderTarget
struct CylinderTarget_t1959645577;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTr3340678586.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetAbstractBehaviour
struct  CylinderTargetAbstractBehaviour_t1139674014  : public DataSetTrackableBehaviour_t3340678586
{
public:
	// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::mCylinderTarget
	Il2CppObject * ___mCylinderTarget_20;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_21;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_22;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mFrameIndex
	int32_t ___mFrameIndex_23;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mUpdateFrameIndex
	int32_t ___mUpdateFrameIndex_24;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mFutureScale
	float ___mFutureScale_25;

public:
	inline static int32_t get_offset_of_mCylinderTarget_20() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t1139674014, ___mCylinderTarget_20)); }
	inline Il2CppObject * get_mCylinderTarget_20() const { return ___mCylinderTarget_20; }
	inline Il2CppObject ** get_address_of_mCylinderTarget_20() { return &___mCylinderTarget_20; }
	inline void set_mCylinderTarget_20(Il2CppObject * value)
	{
		___mCylinderTarget_20 = value;
		Il2CppCodeGenWriteBarrier(&___mCylinderTarget_20, value);
	}

	inline static int32_t get_offset_of_mTopDiameterRatio_21() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t1139674014, ___mTopDiameterRatio_21)); }
	inline float get_mTopDiameterRatio_21() const { return ___mTopDiameterRatio_21; }
	inline float* get_address_of_mTopDiameterRatio_21() { return &___mTopDiameterRatio_21; }
	inline void set_mTopDiameterRatio_21(float value)
	{
		___mTopDiameterRatio_21 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_22() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t1139674014, ___mBottomDiameterRatio_22)); }
	inline float get_mBottomDiameterRatio_22() const { return ___mBottomDiameterRatio_22; }
	inline float* get_address_of_mBottomDiameterRatio_22() { return &___mBottomDiameterRatio_22; }
	inline void set_mBottomDiameterRatio_22(float value)
	{
		___mBottomDiameterRatio_22 = value;
	}

	inline static int32_t get_offset_of_mFrameIndex_23() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t1139674014, ___mFrameIndex_23)); }
	inline int32_t get_mFrameIndex_23() const { return ___mFrameIndex_23; }
	inline int32_t* get_address_of_mFrameIndex_23() { return &___mFrameIndex_23; }
	inline void set_mFrameIndex_23(int32_t value)
	{
		___mFrameIndex_23 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrameIndex_24() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t1139674014, ___mUpdateFrameIndex_24)); }
	inline int32_t get_mUpdateFrameIndex_24() const { return ___mUpdateFrameIndex_24; }
	inline int32_t* get_address_of_mUpdateFrameIndex_24() { return &___mUpdateFrameIndex_24; }
	inline void set_mUpdateFrameIndex_24(int32_t value)
	{
		___mUpdateFrameIndex_24 = value;
	}

	inline static int32_t get_offset_of_mFutureScale_25() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t1139674014, ___mFutureScale_25)); }
	inline float get_mFutureScale_25() const { return ___mFutureScale_25; }
	inline float* get_address_of_mFutureScale_25() { return &___mFutureScale_25; }
	inline void set_mFutureScale_25(float value)
	{
		___mFutureScale_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
