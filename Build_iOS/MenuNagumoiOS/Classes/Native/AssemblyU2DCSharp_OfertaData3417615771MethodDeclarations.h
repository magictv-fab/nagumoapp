﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertaData
struct OfertaData_t3417615771;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertaData::.ctor()
extern "C"  void OfertaData__ctor_m1149307232 (OfertaData_t3417615771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
