﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PedidoData>
struct List_1_t516228991;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AcougueAvaliacao
struct  AcougueAvaliacao_t3448807114  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text AcougueAvaliacao::codePedido
	Text_t9039225 * ___codePedido_3;
	// UnityEngine.UI.Text AcougueAvaliacao::infoPedido
	Text_t9039225 * ___infoPedido_4;
	// UnityEngine.GameObject AcougueAvaliacao::content
	GameObject_t3674682005 * ___content_5;
	// UnityEngine.GameObject AcougueAvaliacao::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_6;
	// UnityEngine.GameObject AcougueAvaliacao::loadingObj
	GameObject_t3674682005 * ___loadingObj_7;
	// UnityEngine.GameObject AcougueAvaliacao::popup
	GameObject_t3674682005 * ___popup_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> AcougueAvaliacao::estrelaLst
	List_1_t747900261 * ___estrelaLst_9;

public:
	inline static int32_t get_offset_of_codePedido_3() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___codePedido_3)); }
	inline Text_t9039225 * get_codePedido_3() const { return ___codePedido_3; }
	inline Text_t9039225 ** get_address_of_codePedido_3() { return &___codePedido_3; }
	inline void set_codePedido_3(Text_t9039225 * value)
	{
		___codePedido_3 = value;
		Il2CppCodeGenWriteBarrier(&___codePedido_3, value);
	}

	inline static int32_t get_offset_of_infoPedido_4() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___infoPedido_4)); }
	inline Text_t9039225 * get_infoPedido_4() const { return ___infoPedido_4; }
	inline Text_t9039225 ** get_address_of_infoPedido_4() { return &___infoPedido_4; }
	inline void set_infoPedido_4(Text_t9039225 * value)
	{
		___infoPedido_4 = value;
		Il2CppCodeGenWriteBarrier(&___infoPedido_4, value);
	}

	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___content_5)); }
	inline GameObject_t3674682005 * get_content_5() const { return ___content_5; }
	inline GameObject_t3674682005 ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(GameObject_t3674682005 * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier(&___content_5, value);
	}

	inline static int32_t get_offset_of_itemPrefab_6() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___itemPrefab_6)); }
	inline GameObject_t3674682005 * get_itemPrefab_6() const { return ___itemPrefab_6; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_6() { return &___itemPrefab_6; }
	inline void set_itemPrefab_6(GameObject_t3674682005 * value)
	{
		___itemPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_6, value);
	}

	inline static int32_t get_offset_of_loadingObj_7() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___loadingObj_7)); }
	inline GameObject_t3674682005 * get_loadingObj_7() const { return ___loadingObj_7; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_7() { return &___loadingObj_7; }
	inline void set_loadingObj_7(GameObject_t3674682005 * value)
	{
		___loadingObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_7, value);
	}

	inline static int32_t get_offset_of_popup_8() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___popup_8)); }
	inline GameObject_t3674682005 * get_popup_8() const { return ___popup_8; }
	inline GameObject_t3674682005 ** get_address_of_popup_8() { return &___popup_8; }
	inline void set_popup_8(GameObject_t3674682005 * value)
	{
		___popup_8 = value;
		Il2CppCodeGenWriteBarrier(&___popup_8, value);
	}

	inline static int32_t get_offset_of_estrelaLst_9() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114, ___estrelaLst_9)); }
	inline List_1_t747900261 * get_estrelaLst_9() const { return ___estrelaLst_9; }
	inline List_1_t747900261 ** get_address_of_estrelaLst_9() { return &___estrelaLst_9; }
	inline void set_estrelaLst_9(List_1_t747900261 * value)
	{
		___estrelaLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___estrelaLst_9, value);
	}
};

struct AcougueAvaliacao_t3448807114_StaticFields
{
public:
	// System.Collections.Generic.List`1<PedidoData> AcougueAvaliacao::pedidoDataLst
	List_1_t516228991 * ___pedidoDataLst_2;

public:
	inline static int32_t get_offset_of_pedidoDataLst_2() { return static_cast<int32_t>(offsetof(AcougueAvaliacao_t3448807114_StaticFields, ___pedidoDataLst_2)); }
	inline List_1_t516228991 * get_pedidoDataLst_2() const { return ___pedidoDataLst_2; }
	inline List_1_t516228991 ** get_address_of_pedidoDataLst_2() { return &___pedidoDataLst_2; }
	inline void set_pedidoDataLst_2(List_1_t516228991 * value)
	{
		___pedidoDataLst_2 = value;
		Il2CppCodeGenWriteBarrier(&___pedidoDataLst_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
