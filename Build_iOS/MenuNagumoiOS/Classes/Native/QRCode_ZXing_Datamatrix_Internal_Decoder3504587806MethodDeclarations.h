﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Decoder
struct Decoder_t3504587806;
// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Void ZXing.Datamatrix.Internal.Decoder::.ctor()
extern "C"  void Decoder__ctor_m3433818047 (Decoder_t3504587806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.Decoder::decode(ZXing.Common.BitMatrix)
extern "C"  DecoderResult_t3752650303 * Decoder_decode_m13780937 (Decoder_t3504587806 * __this, BitMatrix_t1058711404 * ___bits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.Decoder::correctErrors(System.Byte[],System.Int32)
extern "C"  bool Decoder_correctErrors_m809239380 (Decoder_t3504587806 * __this, ByteU5BU5D_t4260760469* ___codewordBytes0, int32_t ___numDataCodewords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
