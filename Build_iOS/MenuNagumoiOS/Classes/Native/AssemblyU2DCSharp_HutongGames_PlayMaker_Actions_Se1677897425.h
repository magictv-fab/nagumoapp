﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralBoolean
struct  SetProceduralBoolean_t1677897425  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralBoolean::substanceMaterial
	FsmMaterial_t924399665 * ___substanceMaterial_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralBoolean::boolProperty
	FsmString_t952858651 * ___boolProperty_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetProceduralBoolean::boolValue
	FsmBool_t1075959796 * ___boolValue_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralBoolean::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_substanceMaterial_9() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t1677897425, ___substanceMaterial_9)); }
	inline FsmMaterial_t924399665 * get_substanceMaterial_9() const { return ___substanceMaterial_9; }
	inline FsmMaterial_t924399665 ** get_address_of_substanceMaterial_9() { return &___substanceMaterial_9; }
	inline void set_substanceMaterial_9(FsmMaterial_t924399665 * value)
	{
		___substanceMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___substanceMaterial_9, value);
	}

	inline static int32_t get_offset_of_boolProperty_10() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t1677897425, ___boolProperty_10)); }
	inline FsmString_t952858651 * get_boolProperty_10() const { return ___boolProperty_10; }
	inline FsmString_t952858651 ** get_address_of_boolProperty_10() { return &___boolProperty_10; }
	inline void set_boolProperty_10(FsmString_t952858651 * value)
	{
		___boolProperty_10 = value;
		Il2CppCodeGenWriteBarrier(&___boolProperty_10, value);
	}

	inline static int32_t get_offset_of_boolValue_11() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t1677897425, ___boolValue_11)); }
	inline FsmBool_t1075959796 * get_boolValue_11() const { return ___boolValue_11; }
	inline FsmBool_t1075959796 ** get_address_of_boolValue_11() { return &___boolValue_11; }
	inline void set_boolValue_11(FsmBool_t1075959796 * value)
	{
		___boolValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___boolValue_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t1677897425, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
