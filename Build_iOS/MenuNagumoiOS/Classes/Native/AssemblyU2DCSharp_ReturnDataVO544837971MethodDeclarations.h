﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReturnDataVO
struct ReturnDataVO_t544837971;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ReturnDataVO::.ctor()
extern "C"  void ReturnDataVO__ctor_m2891001512 (ReturnDataVO_t544837971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReturnDataVO::ToString()
extern "C"  String_t* ReturnDataVO_ToString_m116386987 (ReturnDataVO_t544837971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
