﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t530285832;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomColor
struct  SelectRandomColor_t2148977170  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.Actions.SelectRandomColor::colors
	FsmColorU5BU5D_t530285832* ___colors_9;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomColor::weights
	FsmFloatU5BU5D_t2945380875* ___weights_10;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SelectRandomColor::storeColor
	FsmColor_t2131419205 * ___storeColor_11;

public:
	inline static int32_t get_offset_of_colors_9() { return static_cast<int32_t>(offsetof(SelectRandomColor_t2148977170, ___colors_9)); }
	inline FsmColorU5BU5D_t530285832* get_colors_9() const { return ___colors_9; }
	inline FsmColorU5BU5D_t530285832** get_address_of_colors_9() { return &___colors_9; }
	inline void set_colors_9(FsmColorU5BU5D_t530285832* value)
	{
		___colors_9 = value;
		Il2CppCodeGenWriteBarrier(&___colors_9, value);
	}

	inline static int32_t get_offset_of_weights_10() { return static_cast<int32_t>(offsetof(SelectRandomColor_t2148977170, ___weights_10)); }
	inline FsmFloatU5BU5D_t2945380875* get_weights_10() const { return ___weights_10; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_weights_10() { return &___weights_10; }
	inline void set_weights_10(FsmFloatU5BU5D_t2945380875* value)
	{
		___weights_10 = value;
		Il2CppCodeGenWriteBarrier(&___weights_10, value);
	}

	inline static int32_t get_offset_of_storeColor_11() { return static_cast<int32_t>(offsetof(SelectRandomColor_t2148977170, ___storeColor_11)); }
	inline FsmColor_t2131419205 * get_storeColor_11() const { return ___storeColor_11; }
	inline FsmColor_t2131419205 ** get_address_of_storeColor_11() { return &___storeColor_11; }
	inline void set_storeColor_11(FsmColor_t2131419205 * value)
	{
		___storeColor_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeColor_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
