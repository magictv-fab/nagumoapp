﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloneRigColliders
struct CloneRigColliders_t491736492;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void CloneRigColliders::.ctor()
extern "C"  void CloneRigColliders__ctor_m1002635583 (CloneRigColliders_t491736492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloneRigColliders::CloneCollider()
extern "C"  void CloneRigColliders_CloneCollider_m3312066190 (CloneRigColliders_t491736492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloneRigColliders::nodeChildrens(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void CloneRigColliders_nodeChildrens_m1052902647 (CloneRigColliders_t491736492 * __this, GameObject_t3674682005 * ___parent0, GameObject_t3674682005 * ___parentReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
