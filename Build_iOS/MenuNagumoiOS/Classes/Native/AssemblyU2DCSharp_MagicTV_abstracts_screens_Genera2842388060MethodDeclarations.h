﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.GeneralScreenAbstract
struct GeneralScreenAbstract_t2842388060;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.screens.GeneralScreenAbstract::.ctor()
extern "C"  void GeneralScreenAbstract__ctor_m1370482968 (GeneralScreenAbstract_t2842388060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.abstracts.screens.GeneralScreenAbstract::isVisible()
extern "C"  bool GeneralScreenAbstract_isVisible_m2653596274 (GeneralScreenAbstract_t2842388060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
