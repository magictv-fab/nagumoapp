﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code39Writer
struct  Code39Writer_t1501941636  : public OneDimensionalCodeWriter_t4068326409
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
