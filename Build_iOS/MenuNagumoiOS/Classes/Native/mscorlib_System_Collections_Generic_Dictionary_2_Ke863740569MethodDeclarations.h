﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1263323042MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2546729455(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t863740569 *, Dictionary_2_t3531948414 *, const MethodInfo*))KeyCollection__ctor_m3053849912_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m497917447(__this, ___item0, method) ((  void (*) (KeyCollection_t863740569 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4289325342_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3607206206(__this, method) ((  void (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1827913237_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m363727331(__this, ___item0, method) ((  bool (*) (KeyCollection_t863740569 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m870847788_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m984193032(__this, ___item0, method) ((  bool (*) (KeyCollection_t863740569 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1766842769_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m919485882(__this, method) ((  Il2CppObject* (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3755318161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m813797552(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t863740569 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m677915399_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2127934123(__this, method) ((  Il2CppObject * (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3944613058_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m235705476(__this, method) ((  bool (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2247160205_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m673296950(__this, method) ((  bool (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1031542399_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1191164962(__this, method) ((  Il2CppObject * (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4159261483_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1183114404(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t863740569 *, NetworkReachabilityU5BU5D_t2994299098*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4223030253_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::GetEnumerator()
#define KeyCollection_GetEnumerator_m190658503(__this, method) ((  Enumerator_t4146884468  (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_GetEnumerator_m202149008_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Action>::get_Count()
#define KeyCollection_get_Count_m2074855548(__this, method) ((  int32_t (*) (KeyCollection_t863740569 *, const MethodInfo*))KeyCollection_get_Count_m4040092101_gshared)(__this, method)
