﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Multiply
struct  Vector3Multiply_t1696882338  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Multiply::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Multiply::multiplyBy
	FsmFloat_t2134102846 * ___multiplyBy_10;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Multiply::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_vector3Variable_9() { return static_cast<int32_t>(offsetof(Vector3Multiply_t1696882338, ___vector3Variable_9)); }
	inline FsmVector3_t533912882 * get_vector3Variable_9() const { return ___vector3Variable_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_9() { return &___vector3Variable_9; }
	inline void set_vector3Variable_9(FsmVector3_t533912882 * value)
	{
		___vector3Variable_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_9, value);
	}

	inline static int32_t get_offset_of_multiplyBy_10() { return static_cast<int32_t>(offsetof(Vector3Multiply_t1696882338, ___multiplyBy_10)); }
	inline FsmFloat_t2134102846 * get_multiplyBy_10() const { return ___multiplyBy_10; }
	inline FsmFloat_t2134102846 ** get_address_of_multiplyBy_10() { return &___multiplyBy_10; }
	inline void set_multiplyBy_10(FsmFloat_t2134102846 * value)
	{
		___multiplyBy_10 = value;
		Il2CppCodeGenWriteBarrier(&___multiplyBy_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(Vector3Multiply_t1696882338, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
