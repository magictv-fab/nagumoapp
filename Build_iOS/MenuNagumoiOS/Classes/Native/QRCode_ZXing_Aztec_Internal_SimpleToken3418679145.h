﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.SimpleToken
struct  SimpleToken_t3418679145  : public Token_t3066207355
{
public:
	// System.Int16 ZXing.Aztec.Internal.SimpleToken::value
	int16_t ___value_2;
	// System.Int16 ZXing.Aztec.Internal.SimpleToken::bitCount
	int16_t ___bitCount_3;

public:
	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(SimpleToken_t3418679145, ___value_2)); }
	inline int16_t get_value_2() const { return ___value_2; }
	inline int16_t* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(int16_t value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_bitCount_3() { return static_cast<int32_t>(offsetof(SimpleToken_t3418679145, ___bitCount_3)); }
	inline int16_t get_bitCount_3() const { return ___bitCount_3; }
	inline int16_t* get_address_of_bitCount_3() { return &___bitCount_3; }
	inline void set_bitCount_3(int16_t value)
	{
		___bitCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
