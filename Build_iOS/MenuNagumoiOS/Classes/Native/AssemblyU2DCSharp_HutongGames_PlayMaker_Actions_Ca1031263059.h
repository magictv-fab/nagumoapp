﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip
struct  CapturePoseAsAnimationClip_t1031263059  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::position
	FsmBool_t1075959796 * ___position_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::rotation
	FsmBool_t1075959796 * ___rotation_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::scale
	FsmBool_t1075959796 * ___scale_12;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::storeAnimationClip
	FsmObject_t821476169 * ___storeAnimationClip_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1031263059, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_position_10() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1031263059, ___position_10)); }
	inline FsmBool_t1075959796 * get_position_10() const { return ___position_10; }
	inline FsmBool_t1075959796 ** get_address_of_position_10() { return &___position_10; }
	inline void set_position_10(FsmBool_t1075959796 * value)
	{
		___position_10 = value;
		Il2CppCodeGenWriteBarrier(&___position_10, value);
	}

	inline static int32_t get_offset_of_rotation_11() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1031263059, ___rotation_11)); }
	inline FsmBool_t1075959796 * get_rotation_11() const { return ___rotation_11; }
	inline FsmBool_t1075959796 ** get_address_of_rotation_11() { return &___rotation_11; }
	inline void set_rotation_11(FsmBool_t1075959796 * value)
	{
		___rotation_11 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_11, value);
	}

	inline static int32_t get_offset_of_scale_12() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1031263059, ___scale_12)); }
	inline FsmBool_t1075959796 * get_scale_12() const { return ___scale_12; }
	inline FsmBool_t1075959796 ** get_address_of_scale_12() { return &___scale_12; }
	inline void set_scale_12(FsmBool_t1075959796 * value)
	{
		___scale_12 = value;
		Il2CppCodeGenWriteBarrier(&___scale_12, value);
	}

	inline static int32_t get_offset_of_storeAnimationClip_13() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1031263059, ___storeAnimationClip_13)); }
	inline FsmObject_t821476169 * get_storeAnimationClip_13() const { return ___storeAnimationClip_13; }
	inline FsmObject_t821476169 ** get_address_of_storeAnimationClip_13() { return &___storeAnimationClip_13; }
	inline void set_storeAnimationClip_13(FsmObject_t821476169 * value)
	{
		___storeAnimationClip_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeAnimationClip_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
