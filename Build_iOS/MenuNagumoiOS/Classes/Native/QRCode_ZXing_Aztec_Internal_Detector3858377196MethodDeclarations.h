﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.Detector
struct Detector_t3858377196;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Aztec.Internal.AztecDetectorResult
struct AztecDetectorResult_t1662194462;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Aztec.Internal.Detector/Point
struct Point_t330760921;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_Aztec_Internal_Detector_Point330760921.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"

// System.Void ZXing.Aztec.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern "C"  void Detector__ctor_m3822833918 (Detector_t3858377196 * __this, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.AztecDetectorResult ZXing.Aztec.Internal.Detector::detect(System.Boolean)
extern "C"  AztecDetectorResult_t1662194462 * Detector_detect_m311552920 (Detector_t3858377196 * __this, bool ___isMirror0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Aztec.Internal.Detector::extractParameters(ZXing.ResultPoint[])
extern "C"  bool Detector_extractParameters_m3954107543 (Detector_t3858377196 * __this, ResultPointU5BU5D_t1195164344* ___bullsEyeCorners0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Detector::getRotation(System.Int32[],System.Int32)
extern "C"  int32_t Detector_getRotation_m3099582485 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___sides0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Detector::getCorrectedParameterData(System.Int64,System.Boolean)
extern "C"  int32_t Detector_getCorrectedParameterData_m2993584780 (Il2CppObject * __this /* static, unused */, int64_t ___parameterData0, bool ___compact1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::getBullsEyeCorners(ZXing.Aztec.Internal.Detector/Point)
extern "C"  ResultPointU5BU5D_t1195164344* Detector_getBullsEyeCorners_m4129417817 (Detector_t3858377196 * __this, Point_t330760921 * ___pCenter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.Detector/Point ZXing.Aztec.Internal.Detector::getMatrixCenter()
extern "C"  Point_t330760921 * Detector_getMatrixCenter_m2637492375 (Detector_t3858377196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::getMatrixCornerPoints(ZXing.ResultPoint[])
extern "C"  ResultPointU5BU5D_t1195164344* Detector_getMatrixCornerPoints_m2601032799 (Detector_t3858377196 * __this, ResultPointU5BU5D_t1195164344* ___bullsEyeCorners0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Aztec.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  BitMatrix_t1058711404 * Detector_sampleGrid_m3715944336 (Detector_t3858377196 * __this, BitMatrix_t1058711404 * ___image0, ResultPoint_t1538592853 * ___topLeft1, ResultPoint_t1538592853 * ___topRight2, ResultPoint_t1538592853 * ___bottomRight3, ResultPoint_t1538592853 * ___bottomLeft4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Detector::sampleLine(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern "C"  int32_t Detector_sampleLine_m3788340396 (Detector_t3858377196 * __this, ResultPoint_t1538592853 * ___p10, ResultPoint_t1538592853 * ___p21, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Aztec.Internal.Detector::isWhiteOrBlackRectangle(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern "C"  bool Detector_isWhiteOrBlackRectangle_m3902510267 (Detector_t3858377196 * __this, Point_t330760921 * ___p10, Point_t330760921 * ___p21, Point_t330760921 * ___p32, Point_t330760921 * ___p43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Detector::getColor(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern "C"  int32_t Detector_getColor_m728112540 (Detector_t3858377196 * __this, Point_t330760921 * ___p10, Point_t330760921 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.Detector/Point ZXing.Aztec.Internal.Detector::getFirstDifferent(ZXing.Aztec.Internal.Detector/Point,System.Boolean,System.Int32,System.Int32)
extern "C"  Point_t330760921 * Detector_getFirstDifferent_m2827045454 (Detector_t3858377196 * __this, Point_t330760921 * ___init0, bool ___color1, int32_t ___dx2, int32_t ___dy3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::expandSquare(ZXing.ResultPoint[],System.Single,System.Single)
extern "C"  ResultPointU5BU5D_t1195164344* Detector_expandSquare_m4119845681 (Il2CppObject * __this /* static, unused */, ResultPointU5BU5D_t1195164344* ___cornerPoints0, float ___oldSide1, float ___newSide2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Aztec.Internal.Detector::isValid(System.Int32,System.Int32)
extern "C"  bool Detector_isValid_m1281368015 (Detector_t3858377196 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Aztec.Internal.Detector::isValid(ZXing.ResultPoint)
extern "C"  bool Detector_isValid_m1054092210 (Detector_t3858377196 * __this, ResultPoint_t1538592853 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.Aztec.Internal.Detector::distance(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern "C"  float Detector_distance_m1243634130 (Il2CppObject * __this /* static, unused */, Point_t330760921 * ___a0, Point_t330760921 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.Aztec.Internal.Detector::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  float Detector_distance_m354505010 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___a0, ResultPoint_t1538592853 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Detector::getDimension()
extern "C"  int32_t Detector_getDimension_m2696159059 (Detector_t3858377196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Detector::.cctor()
extern "C"  void Detector__cctor_m3602223226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
