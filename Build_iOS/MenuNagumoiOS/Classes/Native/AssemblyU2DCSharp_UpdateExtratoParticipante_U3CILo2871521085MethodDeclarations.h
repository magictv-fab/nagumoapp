﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateExtratoParticipante/<ILogin>c__Iterator1
struct U3CILoginU3Ec__Iterator1_t2871521085;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateExtratoParticipante/<ILogin>c__Iterator1::.ctor()
extern "C"  void U3CILoginU3Ec__Iterator1__ctor_m3407106558 (U3CILoginU3Ec__Iterator1_t2871521085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UpdateExtratoParticipante/<ILogin>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoginU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m505870814 (U3CILoginU3Ec__Iterator1_t2871521085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UpdateExtratoParticipante/<ILogin>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoginU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3918821746 (U3CILoginU3Ec__Iterator1_t2871521085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UpdateExtratoParticipante/<ILogin>c__Iterator1::MoveNext()
extern "C"  bool U3CILoginU3Ec__Iterator1_MoveNext_m3208814622 (U3CILoginU3Ec__Iterator1_t2871521085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateExtratoParticipante/<ILogin>c__Iterator1::Dispose()
extern "C"  void U3CILoginU3Ec__Iterator1_Dispose_m1362605755 (U3CILoginU3Ec__Iterator1_t2871521085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateExtratoParticipante/<ILogin>c__Iterator1::Reset()
extern "C"  void U3CILoginU3Ec__Iterator1_Reset_m1053539499 (U3CILoginU3Ec__Iterator1_t2871521085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
