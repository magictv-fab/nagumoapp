﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Multiply
struct Vector3Multiply_t1696882338;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::.ctor()
extern "C"  void Vector3Multiply__ctor_m2985635540 (Vector3Multiply_t1696882338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::Reset()
extern "C"  void Vector3Multiply_Reset_m632068481 (Vector3Multiply_t1696882338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::OnEnter()
extern "C"  void Vector3Multiply_OnEnter_m3844296107 (Vector3Multiply_t1696882338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::OnUpdate()
extern "C"  void Vector3Multiply_OnUpdate_m2342621688 (Vector3Multiply_t1696882338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
