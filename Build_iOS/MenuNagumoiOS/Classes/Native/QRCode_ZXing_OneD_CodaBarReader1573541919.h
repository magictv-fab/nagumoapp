﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.CodaBarReader
struct  CodaBarReader_t1573541919  : public OneDReader_t3436042911
{
public:
	// System.Text.StringBuilder ZXing.OneD.CodaBarReader::decodeRowResult
	StringBuilder_t243639308 * ___decodeRowResult_7;
	// System.Int32[] ZXing.OneD.CodaBarReader::counters
	Int32U5BU5D_t3230847821* ___counters_8;
	// System.Int32 ZXing.OneD.CodaBarReader::counterLength
	int32_t ___counterLength_9;

public:
	inline static int32_t get_offset_of_decodeRowResult_7() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919, ___decodeRowResult_7)); }
	inline StringBuilder_t243639308 * get_decodeRowResult_7() const { return ___decodeRowResult_7; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowResult_7() { return &___decodeRowResult_7; }
	inline void set_decodeRowResult_7(StringBuilder_t243639308 * value)
	{
		___decodeRowResult_7 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowResult_7, value);
	}

	inline static int32_t get_offset_of_counters_8() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919, ___counters_8)); }
	inline Int32U5BU5D_t3230847821* get_counters_8() const { return ___counters_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_counters_8() { return &___counters_8; }
	inline void set_counters_8(Int32U5BU5D_t3230847821* value)
	{
		___counters_8 = value;
		Il2CppCodeGenWriteBarrier(&___counters_8, value);
	}

	inline static int32_t get_offset_of_counterLength_9() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919, ___counterLength_9)); }
	inline int32_t get_counterLength_9() const { return ___counterLength_9; }
	inline int32_t* get_address_of_counterLength_9() { return &___counterLength_9; }
	inline void set_counterLength_9(int32_t value)
	{
		___counterLength_9 = value;
	}
};

struct CodaBarReader_t1573541919_StaticFields
{
public:
	// System.Int32 ZXing.OneD.CodaBarReader::MAX_ACCEPTABLE
	int32_t ___MAX_ACCEPTABLE_2;
	// System.Int32 ZXing.OneD.CodaBarReader::PADDING
	int32_t ___PADDING_3;
	// System.Char[] ZXing.OneD.CodaBarReader::ALPHABET
	CharU5BU5D_t3324145743* ___ALPHABET_4;
	// System.Int32[] ZXing.OneD.CodaBarReader::CHARACTER_ENCODINGS
	Int32U5BU5D_t3230847821* ___CHARACTER_ENCODINGS_5;
	// System.Char[] ZXing.OneD.CodaBarReader::STARTEND_ENCODING
	CharU5BU5D_t3324145743* ___STARTEND_ENCODING_6;

public:
	inline static int32_t get_offset_of_MAX_ACCEPTABLE_2() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919_StaticFields, ___MAX_ACCEPTABLE_2)); }
	inline int32_t get_MAX_ACCEPTABLE_2() const { return ___MAX_ACCEPTABLE_2; }
	inline int32_t* get_address_of_MAX_ACCEPTABLE_2() { return &___MAX_ACCEPTABLE_2; }
	inline void set_MAX_ACCEPTABLE_2(int32_t value)
	{
		___MAX_ACCEPTABLE_2 = value;
	}

	inline static int32_t get_offset_of_PADDING_3() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919_StaticFields, ___PADDING_3)); }
	inline int32_t get_PADDING_3() const { return ___PADDING_3; }
	inline int32_t* get_address_of_PADDING_3() { return &___PADDING_3; }
	inline void set_PADDING_3(int32_t value)
	{
		___PADDING_3 = value;
	}

	inline static int32_t get_offset_of_ALPHABET_4() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919_StaticFields, ___ALPHABET_4)); }
	inline CharU5BU5D_t3324145743* get_ALPHABET_4() const { return ___ALPHABET_4; }
	inline CharU5BU5D_t3324145743** get_address_of_ALPHABET_4() { return &___ALPHABET_4; }
	inline void set_ALPHABET_4(CharU5BU5D_t3324145743* value)
	{
		___ALPHABET_4 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHABET_4, value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_5() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919_StaticFields, ___CHARACTER_ENCODINGS_5)); }
	inline Int32U5BU5D_t3230847821* get_CHARACTER_ENCODINGS_5() const { return ___CHARACTER_ENCODINGS_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_CHARACTER_ENCODINGS_5() { return &___CHARACTER_ENCODINGS_5; }
	inline void set_CHARACTER_ENCODINGS_5(Int32U5BU5D_t3230847821* value)
	{
		___CHARACTER_ENCODINGS_5 = value;
		Il2CppCodeGenWriteBarrier(&___CHARACTER_ENCODINGS_5, value);
	}

	inline static int32_t get_offset_of_STARTEND_ENCODING_6() { return static_cast<int32_t>(offsetof(CodaBarReader_t1573541919_StaticFields, ___STARTEND_ENCODING_6)); }
	inline CharU5BU5D_t3324145743* get_STARTEND_ENCODING_6() const { return ___STARTEND_ENCODING_6; }
	inline CharU5BU5D_t3324145743** get_address_of_STARTEND_ENCODING_6() { return &___STARTEND_ENCODING_6; }
	inline void set_STARTEND_ENCODING_6(CharU5BU5D_t3324145743* value)
	{
		___STARTEND_ENCODING_6 = value;
		Il2CppCodeGenWriteBarrier(&___STARTEND_ENCODING_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
