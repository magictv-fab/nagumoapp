﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_FsmContext3936467683.h"
#include "AssemblyU2DCSharp_LitJson_Lexer3664372066.h"
#include "AssemblyU2DCSharp_LitJson_Lexer_StateHandler3261942315.h"
#include "AssemblyU2DCSharp_LitJson_ParserToken608116400.h"
#include "AssemblyU2DCSharp_LoadMainScene1678736365.h"
#include "AssemblyU2DCSharp_LoadMainScene_U3CFirstStartU3Ec__648989837.h"
#include "AssemblyU2DCSharp_LoadMainScene_U3CStartAppU3Ec__I2033279771.h"
#include "AssemblyU2DCSharp_MagicTV_MagicTVProcess1089716.h"
#include "AssemblyU2DCSharp_MagicTV_processes_InitProcessCom3977321688.h"
#include "AssemblyU2DCSharp_MagicTV_processes_PresentationCon206097072.h"
#include "AssemblyU2DCSharp_MagicTV_processes_UpdateProcessCom50846353.h"
#include "AssemblyU2DCSharp_MagicTV_processes_presentation_O2178194890.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMBulkRequest248639535.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMBulkRequest1722164776.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestEven2525848346.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMSingleReques410726329.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMSingleReques726677771.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMSingleReque1908116762.h"
#include "AssemblyU2DCSharp_MagicTV_update_UpdateProcessContr827871604.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommun2900333373.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommun2032344769.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommun2711744668.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommun2597841398.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommuni145687397.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommun1531128233.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommuni518132223.h"
#include "AssemblyU2DCSharp_ARMDebug3644730743.h"
#include "AssemblyU2DCSharp_MagicTV_utils_BundleHelper1394860594.h"
#include "AssemblyU2DCSharp_MagicTV_utils_BundleHelper_U3CGetM16500566.h"
#include "AssemblyU2DCSharp_PipeToArrayOfVector377621353.h"
#include "AssemblyU2DCSharp_PipeToList234179879.h"
#include "AssemblyU2DCSharp_PipeToVector31581806119.h"
#include "AssemblyU2DCSharp_ToggleCamera809234361.h"
#include "AssemblyU2DCSharp_UpdateTransformMetadata2275182802.h"
#include "AssemblyU2DCSharp_MagicTV_utils_UrlHelper1208334183.h"
#include "AssemblyU2DCSharp_UserPreferencesHelper3600094235.h"
#include "AssemblyU2DCSharp_MagicTV_utils_chroma_generic_Gen3704250624.h"
#include "AssemblyU2DCSharp_MagicTV_utils_chroma_vuforia_Vuf3978586099.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_CurrentVersionRevisio3939651409.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyTypeValue654867638.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyValueVO1780100105.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RequestImagesItemVO2061191909.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RequestImagesResultVO1706607919.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ResultRevisionVO2445597391.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoPingVO4026113458.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoVO1983749152.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoConstants3004261904.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerMetadat2699110633.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequest3297042857.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequestM675100796.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequest4129547279.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequest3388834476.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequestPa47984066.h"
#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequest3510446940.h"
#include "AssemblyU2DCSharp_MagicTV_vo_result_ConfirmResultV1731700412.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ErrorResultVO356660816.h"
#include "AssemblyU2DCSharp_MagicTV_vo_result_ReturnResultVO972950402.h"
#include "AssemblyU2DCSharp_VuforiaARControll483402658.h"
#include "AssemblyU2DCSharp_CollisionCheck2934647062.h"
#include "AssemblyU2DCSharp_GameScene2993529754.h"
#include "AssemblyU2DCSharp_GameScene_U3CchangeScoreU3Ec__It2803473388.h"
#include "AssemblyU2DCSharp_GameScene_U3CchangeDifficultyU3E3054859492.h"
#include "AssemblyU2DCSharp_HeliControl91627683.h"
#include "AssemblyU2DCSharp_WallMovement2710429881.h"
#include "AssemblyU2DCSharp_WallSmall3452223805.h"
#include "AssemblyU2DCSharp_GameControllerExample2194515004.h"
#include "AssemblyU2DCSharp_OneSignalPush_MiniJSON_Json3641448984.h"
#include "AssemblyU2DCSharp_OneSignalPush_MiniJSON_Json_Pars2910106365.h"
#include "AssemblyU2DCSharp_OneSignalPush_MiniJSON_Json_Pars2015905863.h"
#include "AssemblyU2DCSharp_OneSignalPush_MiniJSON_Json_Seri3283242352.h"
#include "AssemblyU2DCSharp_OSNotificationPayload1451568543.h"
#include "AssemblyU2DCSharp_OSNotification892481775.h"
#include "AssemblyU2DCSharp_OSNotification_DisplayType1794322908.h"
#include "AssemblyU2DCSharp_OSNotificationAction173272069.h"
#include "AssemblyU2DCSharp_OSNotificationAction_ActionType2045309690.h"
#include "AssemblyU2DCSharp_OSNotificationOpenedResult158818421.h"
#include "AssemblyU2DCSharp_OSNotificationPermission431010846.h"
#include "AssemblyU2DCSharp_OSPermissionState1777959294.h"
#include "AssemblyU2DCSharp_OSPermissionStateChanges1134260069.h"
#include "AssemblyU2DCSharp_OSSubscriptionState1688362992.h"
#include "AssemblyU2DCSharp_OSSubscriptionStateChanges52612275.h"
#include "AssemblyU2DCSharp_OSEmailSubscriptionState2956802108.h"
#include "AssemblyU2DCSharp_OSEmailSubscriptionStateChanges721307367.h"
#include "AssemblyU2DCSharp_OSPermissionSubscriptionState4252985057.h"
#include "AssemblyU2DCSharp_OneSignal3595519118.h"
#include "AssemblyU2DCSharp_OneSignal_LOG_LEVEL343241288.h"
#include "AssemblyU2DCSharp_OneSignal_OSInFocusDisplayOption2441910921.h"
#include "AssemblyU2DCSharp_OneSignal_UnityBuilder2457889383.h"
#include "AssemblyU2DCSharp_OneSignal_NotificationReceived1402323693.h"
#include "AssemblyU2DCSharp_OneSignal_OnSetEmailSuccess2952831753.h"
#include "AssemblyU2DCSharp_OneSignal_OnSetEmailFailure3733427344.h"
#include "AssemblyU2DCSharp_OneSignal_OnLogoutEmailSuccess3366364849.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (FsmContext_t3936467683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4000[4] = 
{
	FsmContext_t3936467683::get_offset_of_Return_0(),
	FsmContext_t3936467683::get_offset_of_NextState_1(),
	FsmContext_t3936467683::get_offset_of_L_2(),
	FsmContext_t3936467683::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (Lexer_t3664372066), -1, sizeof(Lexer_t3664372066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4001[14] = 
{
	Lexer_t3664372066_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_t3664372066_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_t3664372066::get_offset_of_allow_comments_2(),
	Lexer_t3664372066::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_t3664372066::get_offset_of_end_of_input_4(),
	Lexer_t3664372066::get_offset_of_fsm_context_5(),
	Lexer_t3664372066::get_offset_of_input_buffer_6(),
	Lexer_t3664372066::get_offset_of_input_char_7(),
	Lexer_t3664372066::get_offset_of_reader_8(),
	Lexer_t3664372066::get_offset_of_state_9(),
	Lexer_t3664372066::get_offset_of_string_buffer_10(),
	Lexer_t3664372066::get_offset_of_string_value_11(),
	Lexer_t3664372066::get_offset_of_token_12(),
	Lexer_t3664372066::get_offset_of_unichar_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (StateHandler_t3261942315), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (ParserToken_t608116400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4003[20] = 
{
	ParserToken_t608116400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (LoadMainScene_t1678736365), -1, sizeof(LoadMainScene_t1678736365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4004[3] = 
{
	LoadMainScene_t1678736365::get_offset_of_LevelName_2(),
	LoadMainScene_t1678736365_StaticFields::get_offset_of_started_3(),
	LoadMainScene_t1678736365::get_offset_of_magicTVSkyBox_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (U3CFirstStartU3Ec__Iterator3A_t648989837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4005[3] = 
{
	U3CFirstStartU3Ec__Iterator3A_t648989837::get_offset_of_U3CcamU3E__0_0(),
	U3CFirstStartU3Ec__Iterator3A_t648989837::get_offset_of_U24PC_1(),
	U3CFirstStartU3Ec__Iterator3A_t648989837::get_offset_of_U24current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (U3CStartAppU3Ec__Iterator3B_t2033279771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4006[4] = 
{
	U3CStartAppU3Ec__Iterator3B_t2033279771::get_offset_of_U3CasyncU3E__0_0(),
	U3CStartAppU3Ec__Iterator3B_t2033279771::get_offset_of_U24PC_1(),
	U3CStartAppU3Ec__Iterator3B_t2033279771::get_offset_of_U24current_2(),
	U3CStartAppU3Ec__Iterator3B_t2033279771::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (MagicTVProcess_t1089716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4007[9] = 
{
	MagicTVProcess_t1089716::get_offset_of_initProcessComponent_8(),
	MagicTVProcess_t1089716::get_offset_of_updateProcessComponent_9(),
	MagicTVProcess_t1089716::get_offset_of_magicPresentationsController_10(),
	MagicTVProcess_t1089716::get_offset_of__analytics_11(),
	MagicTVProcess_t1089716::get_offset_of_autoPlayMode_12(),
	MagicTVProcess_t1089716::get_offset_of_debugMode_13(),
	MagicTVProcess_t1089716::get_offset_of__lastTrackFoundedEvent_14(),
	MagicTVProcess_t1089716::get_offset_of_noPromptOnEditor_15(),
	MagicTVProcess_t1089716::get_offset_of__resetClicked_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (InitProcessComponent_t3977321688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4008[1] = 
{
	InitProcessComponent_t3977321688::get_offset_of__componentStarter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (PresentationController_t206097072), -1, sizeof(PresentationController_t206097072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4009[13] = 
{
	PresentationController_t206097072::get_offset_of_buttonGame_8(),
	PresentationController_t206097072_StaticFields::get_offset_of_tracking_9(),
	PresentationController_t206097072_StaticFields::get_offset_of_checkTracking_10(),
	PresentationController_t206097072::get_offset_of_lockRun_11(),
	PresentationController_t206097072_StaticFields::get_offset_of_startTime_12(),
	PresentationController_t206097072_StaticFields::get_offset_of_lastTotal_13(),
	PresentationController_t206097072::get_offset_of_debugNow_14(),
	PresentationController_t206097072::get_offset_of__lastTrackName_15(),
	PresentationController_t206097072_StaticFields::get_offset_of_Instance_16(),
	PresentationController_t206097072::get_offset_of_openProcessComponent_17(),
	PresentationController_t206097072::get_offset_of_guiLoader_18(),
	PresentationController_t206097072::get_offset_of_inAppManager_19(),
	PresentationController_t206097072::get_offset_of_groupComponent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (UpdateProcessComponent_t50846353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[1] = 
{
	UpdateProcessComponent_t50846353::get_offset_of_updateProcess_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (OpenProcessComponent_t2178194890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4011[11] = 
{
	OpenProcessComponent_t2178194890::get_offset_of_inAppGui_8(),
	OpenProcessComponent_t2178194890::get_offset_of_trackingContainer_9(),
	OpenProcessComponent_t2178194890::get_offset_of__imageTarget_10(),
	OpenProcessComponent_t2178194890::get_offset_of_trackerManager_11(),
	OpenProcessComponent_t2178194890::get_offset_of_foldersTracks_12(),
	OpenProcessComponent_t2178194890::get_offset_of__trackFolder_13(),
	OpenProcessComponent_t2178194890::get_offset_of__indexLoadDataset_14(),
	OpenProcessComponent_t2178194890::get_offset_of__startLoadDataset_15(),
	OpenProcessComponent_t2178194890::get_offset_of_dataXmlList_16(),
	OpenProcessComponent_t2178194890::get_offset_of_DebugImgTargetsObj_17(),
	OpenProcessComponent_t2178194890::get_offset_of_DebugRoot_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (ARMBulkRequest_t248639535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (ARMBulkRequestEvents_t1722164776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (ARMRequestEvents_t2525848346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4015[6] = 
{
	ARMRequestEvents_t2525848346::get_offset_of_onRequestComplete_0(),
	ARMRequestEvents_t2525848346::get_offset_of_onRequestFail_1(),
	ARMRequestEvents_t2525848346::get_offset_of_onProgress_2(),
	ARMRequestEvents_t2525848346::get_offset_of__isComplete_3(),
	ARMRequestEvents_t2525848346::get_offset_of_weight_4(),
	ARMRequestEvents_t2525848346::get_offset_of__currentProgress_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (ARMRequestVO_t2431191322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4016[9] = 
{
	ARMRequestVO_t2431191322::get_offset_of_UseUnityCache_0(),
	ARMRequestVO_t2431191322::get_offset_of_UnityCacheVersion_1(),
	ARMRequestVO_t2431191322::get_offset_of_Url_2(),
	ARMRequestVO_t2431191322::get_offset_of_TotalAttempts_3(),
	ARMRequestVO_t2431191322::get_offset_of_PostData_4(),
	ARMRequestVO_t2431191322::get_offset_of_Weight_5(),
	ARMRequestVO_t2431191322::get_offset_of_MetaData_6(),
	ARMRequestVO_t2431191322::get_offset_of_CurrentAttempt_7(),
	ARMRequestVO_t2431191322::get_offset_of_Request_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (ARMSingleRequest_t410726329), -1, sizeof(ARMSingleRequest_t410726329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4017[9] = 
{
	ARMSingleRequest_t410726329::get_offset_of__isComplete_2(),
	ARMSingleRequest_t410726329::get_offset_of_Events_3(),
	ARMSingleRequest_t410726329::get_offset_of_log_url_4(),
	ARMSingleRequest_t410726329::get_offset_of_log_progress_5(),
	ARMSingleRequest_t410726329::get_offset_of__isPrepared_6(),
	ARMSingleRequest_t410726329::get_offset_of__started_7(),
	ARMSingleRequest_t410726329::get_offset_of_onStartError_8(),
	ARMSingleRequest_t410726329::get_offset_of_VO_9(),
	ARMSingleRequest_t410726329_StaticFields::get_offset_of__instanceID_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (OnStartErrorEventHandler_t726677771), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (U3CdoLoadU3Ec__Iterator3C_t1908116762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4019[4] = 
{
	U3CdoLoadU3Ec__Iterator3C_t1908116762::get_offset_of_U3CU24s_233U3E__0_0(),
	U3CdoLoadU3Ec__Iterator3C_t1908116762::get_offset_of_U24PC_1(),
	U3CdoLoadU3Ec__Iterator3C_t1908116762::get_offset_of_U24current_2(),
	U3CdoLoadU3Ec__Iterator3C_t1908116762::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (UpdateProcessController_t827871604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4021[15] = 
{
	UpdateProcessController_t827871604::get_offset_of_progressCacheWeight_14(),
	UpdateProcessController_t827871604::get_offset_of_progressUpdateWeight_15(),
	UpdateProcessController_t827871604::get_offset_of_progressDeviceInfoWeight_16(),
	UpdateProcessController_t827871604::get_offset_of_serverCommunication_17(),
	UpdateProcessController_t827871604::get_offset_of_deviceFileInfo_18(),
	UpdateProcessController_t827871604::get_offset_of_deviceInfo_19(),
	UpdateProcessController_t827871604::get_offset_of_cacheManager_20(),
	UpdateProcessController_t827871604::get_offset_of_loadScreen_21(),
	UpdateProcessController_t827871604::get_offset_of_internetInfo_22(),
	UpdateProcessController_t827871604::get_offset_of_groupComponent_23(),
	UpdateProcessController_t827871604::get_offset_of__initing_24(),
	UpdateProcessController_t827871604::get_offset_of__resultInfoToSave_25(),
	UpdateProcessController_t827871604::get_offset_of__serverCommRunning_26(),
	UpdateProcessController_t827871604::get_offset_of__tokenToSave_27(),
	UpdateProcessController_t827871604::get_offset_of_deleteImmediately_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (ServerCommunication_t2900333373), -1, sizeof(ServerCommunication_t2900333373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4022[7] = 
{
	ServerCommunication_t2900333373::get_offset_of_urlToGetJson_7(),
	ServerCommunication_t2900333373_StaticFields::get_offset_of_Instance_8(),
	ServerCommunication_t2900333373::get_offset_of_delayMilisecondsToDownloadImages_9(),
	ServerCommunication_t2900333373::get_offset_of_onUpdateMetadataRequestComplete_10(),
	ServerCommunication_t2900333373::get_offset_of_requestIsFinished_11(),
	ServerCommunication_t2900333373::get_offset_of_currentRoutine_12(),
	ServerCommunication_t2900333373::get_offset_of__timerTimeout_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (OnBooleanEventHandler_t2032344769), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4024[5] = 
{
	U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668::get_offset_of_www_0(),
	U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668::get_offset_of_U24PC_1(),
	U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668::get_offset_of_U24current_2(),
	U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668::get_offset_of_U3CU24U3Ewww_3(),
	U3CWaitForRequestDeviceRegisterU3Ec__Iterator3D_t2711744668::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4025[5] = 
{
	U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398::get_offset_of_www_0(),
	U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398::get_offset_of_U24PC_1(),
	U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398::get_offset_of_U24current_2(),
	U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398::get_offset_of_U3CU24U3Ewww_3(),
	U3CWaitForRequestPingU3Ec__Iterator3E_t2597841398::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (U3CWaitForRequestU3Ec__Iterator3F_t145687397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4026[5] = 
{
	U3CWaitForRequestU3Ec__Iterator3F_t145687397::get_offset_of_www_0(),
	U3CWaitForRequestU3Ec__Iterator3F_t145687397::get_offset_of_U24PC_1(),
	U3CWaitForRequestU3Ec__Iterator3F_t145687397::get_offset_of_U24current_2(),
	U3CWaitForRequestU3Ec__Iterator3F_t145687397::get_offset_of_U3CU24U3Ewww_3(),
	U3CWaitForRequestU3Ec__Iterator3F_t145687397::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4027[5] = 
{
	U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233::get_offset_of_www_0(),
	U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233::get_offset_of_U24PC_1(),
	U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233::get_offset_of_U24current_2(),
	U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233::get_offset_of_U3CU24U3Ewww_3(),
	U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4028[5] = 
{
	U3CWaitForRequestMetasU3Ec__Iterator41_t518132223::get_offset_of_www_0(),
	U3CWaitForRequestMetasU3Ec__Iterator41_t518132223::get_offset_of_U24PC_1(),
	U3CWaitForRequestMetasU3Ec__Iterator41_t518132223::get_offset_of_U24current_2(),
	U3CWaitForRequestMetasU3Ec__Iterator41_t518132223::get_offset_of_U3CU24U3Ewww_3(),
	U3CWaitForRequestMetasU3Ec__Iterator41_t518132223::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (ARMDebug_t3644730743), -1, sizeof(ARMDebug_t3644730743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4029[2] = 
{
	ARMDebug_t3644730743_StaticFields::get_offset_of_startTime_0(),
	ARMDebug_t3644730743_StaticFields::get_offset_of_lastTotal_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (BundleHelper_t1394860594), -1, sizeof(BundleHelper_t1394860594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4030[1] = 
{
	BundleHelper_t1394860594_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4_t16500566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[1] = 
{
	U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4_t16500566::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (PipeToArrayOfVector3_t77621353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (PipeToList_t234179879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (PipeToVector3_t1581806119), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (ToggleCamera_t809234361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (UpdateTransformMetadata_t2275182802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4036[5] = 
{
	UpdateTransformMetadata_t2275182802::get_offset_of_bundle_id_2(),
	UpdateTransformMetadata_t2275182802::get_offset_of_buttonSize_3(),
	UpdateTransformMetadata_t2275182802::get_offset_of_objetoToSendTransform_4(),
	UpdateTransformMetadata_t2275182802::get_offset_of_result_5(),
	UpdateTransformMetadata_t2275182802::get_offset_of__enviando_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (UrlHelper_t1208334183), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (UserPreferencesHelper_t3600094235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (GenericChromaKeyConfig_t3704250624), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (VuforiaChromaKeyConfig_t3978586099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (BundleVO_t1984518073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4041[8] = 
{
	BundleVO_t1984518073::get_offset_of_revision_id_1(),
	BundleVO_t1984518073::get_offset_of_name_2(),
	BundleVO_t1984518073::get_offset_of_context_3(),
	BundleVO_t1984518073::get_offset_of_files_4(),
	BundleVO_t1984518073::get_offset_of_metadata_5(),
	BundleVO_t1984518073::get_offset_of_published_at_6(),
	BundleVO_t1984518073::get_offset_of_expired_at_7(),
	BundleVO_t1984518073::get_offset_of_children_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (CurrentVersionRevisionVO_t3939651409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[1] = 
{
	CurrentVersionRevisionVO_t3939651409::get_offset_of_current_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (FileVO_t1935348659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4043[3] = 
{
	FileVO_t1935348659::get_offset_of_bundle_id_1(),
	FileVO_t1935348659::get_offset_of_type_2(),
	FileVO_t1935348659::get_offset_of_urls_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (KeyTypeValue_t654867638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4044[5] = 
{
	0,
	0,
	KeyTypeValue_t654867638::get_offset_of_type_2(),
	KeyTypeValue_t654867638::get_offset_of_key_3(),
	KeyTypeValue_t654867638::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (KeyValueVO_t1780100105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4045[3] = 
{
	KeyValueVO_t1780100105::get_offset_of_id_0(),
	KeyValueVO_t1780100105::get_offset_of_key_1(),
	KeyValueVO_t1780100105::get_offset_of_value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (MetadataVO_t2511256998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4046[4] = 
{
	MetadataVO_t2511256998::get_offset_of_key_1(),
	MetadataVO_t2511256998::get_offset_of_value_2(),
	MetadataVO_t2511256998::get_offset_of_tabela_3(),
	MetadataVO_t2511256998::get_offset_of_tabela_id_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (RequestImagesItemVO_t2061191909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4047[3] = 
{
	RequestImagesItemVO_t2061191909::get_offset_of_url_0(),
	RequestImagesItemVO_t2061191909::get_offset_of_size_1(),
	RequestImagesItemVO_t2061191909::get_offset_of_tempo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (RequestImagesResultVO_t1706607919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[2] = 
{
	RequestImagesResultVO_t1706607919::get_offset_of_hash_0(),
	RequestImagesResultVO_t1706607919::get_offset_of_imagens_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (ResultRevisionVO_t2445597391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4049[7] = 
{
	ResultRevisionVO_t2445597391::get_offset_of_revision_0(),
	ResultRevisionVO_t2445597391::get_offset_of_name_1(),
	ResultRevisionVO_t2445597391::get_offset_of_short_description_2(),
	ResultRevisionVO_t2445597391::get_offset_of_long_description_3(),
	ResultRevisionVO_t2445597391::get_offset_of_bundles_4(),
	ResultRevisionVO_t2445597391::get_offset_of_metadata_5(),
	ResultRevisionVO_t2445597391::get_offset_of_pin_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (RevisionInfoPingVO_t4026113458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4050[3] = 
{
	RevisionInfoPingVO_t4026113458::get_offset_of_jsonrpc_1(),
	RevisionInfoPingVO_t4026113458::get_offset_of_result_2(),
	RevisionInfoPingVO_t4026113458::get_offset_of_error_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (RevisionInfoVO_t1983749152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4051[3] = 
{
	RevisionInfoVO_t1983749152::get_offset_of_jsonrpc_1(),
	RevisionInfoVO_t1983749152::get_offset_of_result_2(),
	RevisionInfoVO_t1983749152::get_offset_of_error_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (UrlInfoConstants_t3004261904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4052[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (UrlInfoVO_t1761987528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4053[7] = 
{
	UrlInfoVO_t1761987528::get_offset_of_bundle_file_id_1(),
	UrlInfoVO_t1761987528::get_offset_of_type_2(),
	UrlInfoVO_t1761987528::get_offset_of_md5_3(),
	UrlInfoVO_t1761987528::get_offset_of_url_4(),
	UrlInfoVO_t1761987528::get_offset_of_size_5(),
	UrlInfoVO_t1761987528::get_offset_of_local_url_6(),
	UrlInfoVO_t1761987528::get_offset_of_status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (VOWithId_t2461972856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4054[1] = 
{
	VOWithId_t2461972856::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (ServerMetadataParamsVO_t2699110633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4055[1] = 
{
	ServerMetadataParamsVO_t2699110633::get_offset_of_metas_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (ServerRequestDeviceVO_t3297042857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4056[3] = 
{
	ServerRequestDeviceVO_t3297042857::get_offset_of_jsonrpc_0(),
	ServerRequestDeviceVO_t3297042857::get_offset_of_id_1(),
	ServerRequestDeviceVO_t3297042857::get_offset_of_result_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (ServerRequestMobile_t675100796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4057[3] = 
{
	ServerRequestMobile_t675100796::get_offset_of_jsonrpc_0(),
	ServerRequestMobile_t675100796::get_offset_of_result_1(),
	ServerRequestMobile_t675100796::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (ServerRequestParamsDeviceVO_t4129547279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4058[1] = 
{
	ServerRequestParamsDeviceVO_t4129547279::get_offset_of_token_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (ServerRequestParamsUpdateConstants_t3388834476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4059[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (ServerRequestParamsUpdateVO_t47984066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[6] = 
{
	ServerRequestParamsUpdateVO_t47984066::get_offset_of__revision_0(),
	ServerRequestParamsUpdateVO_t47984066::get_offset_of_device_id_1(),
	ServerRequestParamsUpdateVO_t47984066::get_offset_of_app_2(),
	ServerRequestParamsUpdateVO_t47984066::get_offset_of_api_3(),
	ServerRequestParamsUpdateVO_t47984066::get_offset_of_user_agent_4(),
	ServerRequestParamsUpdateVO_t47984066::get_offset_of_connection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (ServerRequestUpdateVO_t3510446940), -1, sizeof(ServerRequestUpdateVO_t3510446940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4061[5] = 
{
	ServerRequestUpdateVO_t3510446940::get_offset_of_jsonrpc_0(),
	ServerRequestUpdateVO_t3510446940::get_offset_of_method_1(),
	ServerRequestUpdateVO_t3510446940::get_offset_of_param_2(),
	ServerRequestUpdateVO_t3510446940::get_offset_of_id_3(),
	ServerRequestUpdateVO_t3510446940_StaticFields::get_offset_of__id_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (ConfirmResultVO_t1731700412), -1, sizeof(ConfirmResultVO_t1731700412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4062[4] = 
{
	ConfirmResultVO_t1731700412_StaticFields::get_offset_of__autoId_0(),
	ConfirmResultVO_t1731700412::get_offset_of_id_1(),
	ConfirmResultVO_t1731700412::get_offset_of_confirm_2(),
	ConfirmResultVO_t1731700412::get_offset_of_message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (ErrorResultVO_t356660816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[2] = 
{
	ErrorResultVO_t356660816::get_offset_of_code_0(),
	ErrorResultVO_t356660816::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (ReturnResultVO_t972950402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[2] = 
{
	ReturnResultVO_t972950402::get_offset_of_success_0(),
	ReturnResultVO_t972950402::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (VuforiaARControll_t483402658), -1, sizeof(VuforiaARControll_t483402658_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4065[3] = 
{
	VuforiaARControll_t483402658_StaticFields::get_offset_of__instance_0(),
	VuforiaARControll_t483402658::get_offset_of__vuforiaImageTracker_1(),
	VuforiaARControll_t483402658::get_offset_of__chekStartVuforia_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (CollisionCheck_t2934647062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (GameScene_t2993529754), -1, sizeof(GameScene_t2993529754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4067[25] = 
{
	GameScene_t2993529754::get_offset_of_copterL_2(),
	GameScene_t2993529754_StaticFields::get_offset_of_leftUp_3(),
	GameScene_t2993529754_StaticFields::get_offset_of_leftDown_4(),
	GameScene_t2993529754_StaticFields::get_offset_of_gameOver_5(),
	GameScene_t2993529754_StaticFields::get_offset_of_gameStart_6(),
	GameScene_t2993529754_StaticFields::get_offset_of_speedDown_7(),
	GameScene_t2993529754_StaticFields::get_offset_of_speedWall_8(),
	GameScene_t2993529754_StaticFields::get_offset_of_btnPlay_9(),
	GameScene_t2993529754::get_offset_of_WallSingle_10(),
	GameScene_t2993529754::get_offset_of_WallDouble_11(),
	GameScene_t2993529754::get_offset_of_lastWall_12(),
	GameScene_t2993529754::get_offset_of_scoreText_13(),
	GameScene_t2993529754::get_offset_of_bestText_14(),
	GameScene_t2993529754::get_offset_of_scoreNum_15(),
	GameScene_t2993529754::get_offset_of_bestNum_16(),
	GameScene_t2993529754_StaticFields::get_offset_of_makeNewWall_17(),
	GameScene_t2993529754_StaticFields::get_offset_of_initGame_18(),
	GameScene_t2993529754::get_offset_of_diffY1_19(),
	GameScene_t2993529754::get_offset_of_diffY2_20(),
	GameScene_t2993529754_StaticFields::get_offset_of_difficultyNum_21(),
	GameScene_t2993529754::get_offset_of_anim_22(),
	GameScene_t2993529754::get_offset_of_heliClip_23(),
	GameScene_t2993529754_StaticFields::get_offset_of_heliSource_24(),
	GameScene_t2993529754::get_offset_of_explodeClip_25(),
	GameScene_t2993529754_StaticFields::get_offset_of_explodeSource_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (U3CchangeScoreU3Ec__Iterator42_t2803473388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4068[3] = 
{
	U3CchangeScoreU3Ec__Iterator42_t2803473388::get_offset_of_U24PC_0(),
	U3CchangeScoreU3Ec__Iterator42_t2803473388::get_offset_of_U24current_1(),
	U3CchangeScoreU3Ec__Iterator42_t2803473388::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (U3CchangeDifficultyU3Ec__Iterator43_t3054859492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4069[3] = 
{
	U3CchangeDifficultyU3Ec__Iterator43_t3054859492::get_offset_of_U24PC_0(),
	U3CchangeDifficultyU3Ec__Iterator43_t3054859492::get_offset_of_U24current_1(),
	U3CchangeDifficultyU3Ec__Iterator43_t3054859492::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (HeliControl_t91627683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4070[1] = 
{
	HeliControl_t91627683::get_offset_of_down_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (WallMovement_t2710429881), -1, sizeof(WallMovement_t2710429881_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4071[5] = 
{
	WallMovement_t2710429881::get_offset_of_WallUpL_2(),
	WallMovement_t2710429881::get_offset_of_WallUpR_3(),
	WallMovement_t2710429881::get_offset_of_WallDownL_4(),
	WallMovement_t2710429881::get_offset_of_WallDownR_5(),
	WallMovement_t2710429881_StaticFields::get_offset_of_diffY_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (WallSmall_t3452223805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (GameControllerExample_t2194515004), -1, sizeof(GameControllerExample_t2194515004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4073[11] = 
{
	GameControllerExample_t2194515004_StaticFields::get_offset_of_extraMessage_2(),
	GameControllerExample_t2194515004::get_offset_of_email_3(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_requiresUserPrivacyConsent_4(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_6(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_7(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_8(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_9(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_10(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_11(),
	GameControllerExample_t2194515004_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (Json_t3641448984), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (Parser_t2910106365), -1, sizeof(Parser_t2910106365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4075[3] = 
{
	0,
	Parser_t2910106365::get_offset_of_json_1(),
	Parser_t2910106365_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (TOKEN_t2015905863)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4076[13] = 
{
	TOKEN_t2015905863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (Serializer_t3283242352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4077[1] = 
{
	Serializer_t3283242352::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (OSNotificationPayload_t1451568543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4078[19] = 
{
	OSNotificationPayload_t1451568543::get_offset_of_notificationID_0(),
	OSNotificationPayload_t1451568543::get_offset_of_sound_1(),
	OSNotificationPayload_t1451568543::get_offset_of_title_2(),
	OSNotificationPayload_t1451568543::get_offset_of_body_3(),
	OSNotificationPayload_t1451568543::get_offset_of_subtitle_4(),
	OSNotificationPayload_t1451568543::get_offset_of_launchURL_5(),
	OSNotificationPayload_t1451568543::get_offset_of_additionalData_6(),
	OSNotificationPayload_t1451568543::get_offset_of_actionButtons_7(),
	OSNotificationPayload_t1451568543::get_offset_of_contentAvailable_8(),
	OSNotificationPayload_t1451568543::get_offset_of_badge_9(),
	OSNotificationPayload_t1451568543::get_offset_of_smallIcon_10(),
	OSNotificationPayload_t1451568543::get_offset_of_largeIcon_11(),
	OSNotificationPayload_t1451568543::get_offset_of_bigPicture_12(),
	OSNotificationPayload_t1451568543::get_offset_of_smallIconAccentColor_13(),
	OSNotificationPayload_t1451568543::get_offset_of_ledColor_14(),
	OSNotificationPayload_t1451568543::get_offset_of_lockScreenVisibility_15(),
	OSNotificationPayload_t1451568543::get_offset_of_groupKey_16(),
	OSNotificationPayload_t1451568543::get_offset_of_groupMessage_17(),
	OSNotificationPayload_t1451568543::get_offset_of_fromProjectNumber_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (OSNotification_t892481775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4079[6] = 
{
	OSNotification_t892481775::get_offset_of_isAppInFocus_0(),
	OSNotification_t892481775::get_offset_of_shown_1(),
	OSNotification_t892481775::get_offset_of_silentNotification_2(),
	OSNotification_t892481775::get_offset_of_androidNotificationId_3(),
	OSNotification_t892481775::get_offset_of_displayType_4(),
	OSNotification_t892481775::get_offset_of_payload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (DisplayType_t1794322908)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4080[4] = 
{
	DisplayType_t1794322908::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (OSNotificationAction_t173272069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4081[2] = 
{
	OSNotificationAction_t173272069::get_offset_of_actionID_0(),
	OSNotificationAction_t173272069::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (ActionType_t2045309690)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4082[3] = 
{
	ActionType_t2045309690::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (OSNotificationOpenedResult_t158818421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4083[2] = 
{
	OSNotificationOpenedResult_t158818421::get_offset_of_action_0(),
	OSNotificationOpenedResult_t158818421::get_offset_of_notification_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (OSNotificationPermission_t431010846)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4084[4] = 
{
	OSNotificationPermission_t431010846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (OSPermissionState_t1777959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4085[2] = 
{
	OSPermissionState_t1777959294::get_offset_of_hasPrompted_0(),
	OSPermissionState_t1777959294::get_offset_of_status_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (OSPermissionStateChanges_t1134260069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4086[2] = 
{
	OSPermissionStateChanges_t1134260069::get_offset_of_to_0(),
	OSPermissionStateChanges_t1134260069::get_offset_of_from_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (OSSubscriptionState_t1688362992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4087[4] = 
{
	OSSubscriptionState_t1688362992::get_offset_of_userSubscriptionSetting_0(),
	OSSubscriptionState_t1688362992::get_offset_of_userId_1(),
	OSSubscriptionState_t1688362992::get_offset_of_pushToken_2(),
	OSSubscriptionState_t1688362992::get_offset_of_subscribed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (OSSubscriptionStateChanges_t52612275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4088[2] = 
{
	OSSubscriptionStateChanges_t52612275::get_offset_of_to_0(),
	OSSubscriptionStateChanges_t52612275::get_offset_of_from_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (OSEmailSubscriptionState_t2956802108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4089[3] = 
{
	OSEmailSubscriptionState_t2956802108::get_offset_of_emailUserId_0(),
	OSEmailSubscriptionState_t2956802108::get_offset_of_emailAddress_1(),
	OSEmailSubscriptionState_t2956802108::get_offset_of_subscribed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (OSEmailSubscriptionStateChanges_t721307367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4090[2] = 
{
	OSEmailSubscriptionStateChanges_t721307367::get_offset_of_to_0(),
	OSEmailSubscriptionStateChanges_t721307367::get_offset_of_from_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (OSPermissionSubscriptionState_t4252985057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4091[3] = 
{
	OSPermissionSubscriptionState_t4252985057::get_offset_of_permissionStatus_0(),
	OSPermissionSubscriptionState_t4252985057::get_offset_of_subscriptionStatus_1(),
	OSPermissionSubscriptionState_t4252985057::get_offset_of_emailSubscriptionStatus_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (OneSignal_t3595519118), -1, sizeof(OneSignal_t3595519118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4092[24] = 
{
	0,
	0,
	0,
	OneSignal_t3595519118_StaticFields::get_offset_of_idsAvailableDelegate_5(),
	OneSignal_t3595519118_StaticFields::get_offset_of_tagsReceivedDelegate_6(),
	OneSignal_t3595519118_StaticFields::get_offset_of_notificationUserResponseDelegate_7(),
	OneSignal_t3595519118_StaticFields::get_offset_of_internalPermissionObserver_8(),
	OneSignal_t3595519118_StaticFields::get_offset_of_addedPermissionObserver_9(),
	OneSignal_t3595519118_StaticFields::get_offset_of_internalSubscriptionObserver_10(),
	OneSignal_t3595519118_StaticFields::get_offset_of_addedSubscriptionObserver_11(),
	OneSignal_t3595519118_StaticFields::get_offset_of_internalEmailSubscriptionObserver_12(),
	OneSignal_t3595519118_StaticFields::get_offset_of_addedEmailSubscriptionObserver_13(),
	OneSignal_t3595519118_StaticFields::get_offset_of_builder_14(),
	OneSignal_t3595519118_StaticFields::get_offset_of_oneSignalPlatform_15(),
	OneSignal_t3595519118_StaticFields::get_offset_of_logLevel_16(),
	OneSignal_t3595519118_StaticFields::get_offset_of_visualLogLevel_17(),
	OneSignal_t3595519118_StaticFields::get_offset_of_requiresUserConsent_18(),
	OneSignal_t3595519118_StaticFields::get_offset_of_postNotificationSuccessDelegate_19(),
	OneSignal_t3595519118_StaticFields::get_offset_of_postNotificationFailureDelegate_20(),
	OneSignal_t3595519118_StaticFields::get_offset_of_setEmailSuccessDelegate_21(),
	OneSignal_t3595519118_StaticFields::get_offset_of_setEmailFailureDelegate_22(),
	OneSignal_t3595519118_StaticFields::get_offset_of_logoutEmailSuccessDelegate_23(),
	OneSignal_t3595519118_StaticFields::get_offset_of_logoutEmailFailureDelegate_24(),
	OneSignal_t3595519118_StaticFields::get_offset_of__inFocusDisplayType_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (LOG_LEVEL_t343241288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4093[8] = 
{
	LOG_LEVEL_t343241288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (OSInFocusDisplayOption_t2441910921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4094[4] = 
{
	OSInFocusDisplayOption_t2441910921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (UnityBuilder_t2457889383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4095[5] = 
{
	UnityBuilder_t2457889383::get_offset_of_appID_0(),
	UnityBuilder_t2457889383::get_offset_of_googleProjectNumber_1(),
	UnityBuilder_t2457889383::get_offset_of_iOSSettings_2(),
	UnityBuilder_t2457889383::get_offset_of_notificationReceivedDelegate_3(),
	UnityBuilder_t2457889383::get_offset_of_notificationOpenedDelegate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { sizeof (NotificationReceived_t1402323693), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { sizeof (OnSetEmailSuccess_t2952831753), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (OnSetEmailFailure_t3733427344), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (OnLogoutEmailSuccess_t3366364849), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
