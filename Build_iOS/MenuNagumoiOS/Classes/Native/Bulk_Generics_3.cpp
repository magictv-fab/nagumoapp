﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>
struct KeyCollection_t1050929640;
// System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>
struct Dictionary_2_t3719137485;
// System.Collections.Generic.IEnumerator`1<ZXing.EncodeHintType>
struct IEnumerator_1_t861460710;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.Dictionary`2/Transform`1<ZXing.EncodeHintType,System.Object,ZXing.EncodeHintType>
struct Transform_1_t2819433829;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.EncodeHintType[]
struct EncodeHintTypeU5BU5D_t2781905696;
// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>
struct KeyCollection_t466821149;
// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Collections.Generic.IEnumerator`1<ZXing.ResultMetadataType>
struct IEnumerator_1_t540264725;
// System.Collections.Generic.Dictionary`2/Transform`1<ZXing.ResultMetadataType,System.Object,ZXing.ResultMetadataType>
struct Transform_1_t1750793963;
// ZXing.ResultMetadataType[]
struct ResultMetadataTypeU5BU5D_t2197797205;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>
struct ShimEnumerator_t4003355771;
// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>
struct Dictionary_2_t4287577744;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>
struct ShimEnumerator_t2226662119;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>
struct ShimEnumerator_t1662330499;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>
struct Dictionary_2_t1946552472;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct ShimEnumerator_t813817701;
// System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct Dictionary_2_t1098039674;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t866879766;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t3883857637;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t4168079610;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>
struct ShimEnumerator_t1981506396;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Reflection.Emit.Label>
struct Dictionary_2_t2265728369;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ShimEnumerator_t108917990;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t393139963;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ShimEnumerator_t172421601;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t456643574;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>
struct ShimEnumerator_t1649192837;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ArrayMetadata>
struct Dictionary_2_t1933414810;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>
struct ShimEnumerator_t3895111721;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.ObjectMetadata>
struct Dictionary_2_t4179333694;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>
struct ShimEnumerator_t1657484543;
// System.Collections.Generic.Dictionary`2<System.Object,LitJson.PropertyMetadata>
struct Dictionary_2_t1941706516;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t2362615941;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2646837914;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t3039655723;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3323877696;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t1761666298;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2045888271;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>
struct ShimEnumerator_t1882768899;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2166990872;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>
struct ShimEnumerator_t1910485146;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t2194707119;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t1736811037;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2021033010;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t3847508013;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t4131729986;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>
struct ShimEnumerator_t3647308914;
// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ShimEnumerator_t1535079881;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t1819301854;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct ShimEnumerator_t2473866692;
// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>
struct ShimEnumerator_t1005475740;
// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>
struct ShimEnumerator_t897894528;
// System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>
struct Dictionary_2_t1182116501;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>
struct ShimEnumerator_t3434915512;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>
struct ShimEnumerator_t2850807021;
// System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,InApp.InAppEventDispatcher/EventNames>
struct Transform_1_t3966784111;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4103754879;
// System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>
struct Transform_1_t2243539419;
// System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Object>
struct Transform_1_t2227997340;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,MagicTV.globals.Perspective>
struct Transform_1_t3570006487;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t382092003;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>
struct Transform_1_t1040150187;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Object>
struct Transform_1_t2801301760;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>
struct Transform_1_t2607415263;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3991026007;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>>
struct Transform_1_t4084752571;
// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Object>
struct Transform_1_t2115268468;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Char>
struct Transform_1_t4258277537;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.DictionaryEntry>
struct Transform_1_t3147261613;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>>
struct Transform_1_t2392475379;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,ZXing.Aztec.Internal.Decoder/Table>
struct Transform_1_t2403133512;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3765932824;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct Transform_1_t3064208655;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>
struct Transform_1_t3168164710;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1643741485;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t3958995187;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>
struct Transform_1_t1045973371;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>
struct Transform_1_t4062951242;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>
struct Transform_1_t1319344378;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>
struct Transform_1_t1732246839;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>
struct Transform_1_t721576264;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>
struct Transform_1_t1836202894;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>
struct Transform_1_t1809018376;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
struct Transform_1_t349332431;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Int32>
struct Transform_1_t1211250262;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Transform_1_t453288486;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>
struct Transform_1_t1509469889;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct Transform_1_t113287555;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>
struct Transform_1_t911701775;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Transform_1_t217243610;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,LitJson.ArrayMetadata>
struct Transform_1_t4102664885;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.DictionaryEntry>
struct Transform_1_t1795928589;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>
struct Transform_1_t1876517491;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Object>
struct Transform_1_t4215138346;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,LitJson.ObjectMetadata>
struct Transform_1_t199362469;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.DictionaryEntry>
struct Transform_1_t4236641881;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>
struct Transform_1_t2268182371;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Object>
struct Transform_1_t2360884342;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,LitJson.PropertyMetadata>
struct Transform_1_t585079693;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.DictionaryEntry>
struct Transform_1_t2565018987;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>
struct Transform_1_t2653899595;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Object>
struct Transform_1_t689261448;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>
struct Transform_1_t1899867829;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t3174675725;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct Transform_1_t3968687731;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t1298918186;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t2949220623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Transform_1_t125305115;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>
struct Transform_1_t2351452509;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1073463084;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t827029284;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Transform_1_t1020091647;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t3246239041;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>
struct Transform_1_t2034936791;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>
struct Transform_1_t2349101755;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>
struct Transform_1_t159179252;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>
struct Transform_1_t280281853;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>
struct Transform_1_t520022276;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct Transform_1_t861903487;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>
struct Transform_1_t2939232033;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>
struct Transform_1_t3088050881;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>
struct Transform_1_t1958177301;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct Transform_1_t2126384403;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>
struct Transform_1_t82419762;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>
struct Transform_1_t57564501;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
struct Transform_1_t2588701125;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct Transform_1_t572637907;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1050929640.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1050929640MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3719137485.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3719137485MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key39106243.h"
#include "mscorlib_System_Int321153838500.h"
#include "QRCode_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2819433829.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2819433829MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key39106243MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke466821149.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke466821149MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3135028994.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3135028994MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3749965048.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1750793963.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1750793963MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3749965048MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S4003355771.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S4003355771MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4287577744.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4287577744MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1309933840.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1309933840MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450MethodDeclarations.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2226662119.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2226662119MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2510884092.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2510884092MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3828207484.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3828207484MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798MethodDeclarations.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1662330499.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1662330499MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1946552472.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1946552472MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3263875864.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3263875864MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178MethodDeclarations.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh813817701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh813817701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1098039674.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1098039674MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2415363066.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2415363066MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_996820380.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_996820380MethodDeclarations.h"
#include "mscorlib_System_Char2862622538.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh866879766.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh866879766MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1151101739.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1151101739MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2468425131.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2468425131MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3883857637.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3883857637MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4168079610.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4168079610MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1981506396.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1981506396MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2265728369.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2265728369MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3583051761.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3583051761MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22164509075.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22164509075MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_Label2268465130.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh108917990.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh108917990MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge393139963.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge393139963MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1710463355.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1710463355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_291920669.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_291920669MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan395876724.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh172421601.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh172421601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge456643574.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge456643574MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1773966966.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1773966966MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_355424280.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_355424280MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMan459380335.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1649192837.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1649192837MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1933414810.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1933414810MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3250738202.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3250738202MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21832195516.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21832195516MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata4058342910.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3895111721.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3895111721MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4179333694.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4179333694MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1201689790.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1201689790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24078114400.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24078114400MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata2009294498.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1657484543.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1657484543MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1941706516.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1941706516MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3259029908.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3259029908MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21840487222.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21840487222MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata4066634616.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2362615941.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2362615941MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2646837914.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2646837914MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3964161306.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3964161306MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3039655723.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3039655723MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3323877696.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3323877696MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En346233792.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En346233792MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1761666298.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1761666298MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1882768899.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1882768899MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2166990872.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2166990872MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3484314264.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3484314264MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1910485146.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1910485146MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2194707119.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2194707119MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3512030511.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3512030511MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1736811037.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1736811037MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2021033010.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2021033010MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3338356402.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3338356402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3847508013.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3847508013MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4131729986.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4131729986MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1154086082.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1154086082MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030510692.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030510692MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro1961690790.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3647308914.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3647308914MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3931530887.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3931530887MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En953886983.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En953886983MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1535079881.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1535079881MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1819301854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1819301854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3136625246.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3136625246MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21718082560.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21718082560MethodDeclarations.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXE354375056.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2473866692.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2473866692MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2758088665.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2758088665MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4075412057.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4075412057MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1005475740.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1005475740MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1289697713.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1289697713MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2607021105.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2607021105MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21188478419.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21188478419MethodDeclarations.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh897894528.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh897894528MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1182116501.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1182116501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2499439893.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2499439893MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207MethodDeclarations.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3434915512.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3434915512MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En741493581.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En741493581MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23617918191.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23617918191MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2850807021.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2850807021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En157385090.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En157385090MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23033809700.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23033809700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3966784111.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3966784111MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4103754879.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4103754879MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2243539419.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2243539419MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2227997340.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2227997340MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3570006487.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3570006487MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr382092003.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr382092003MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1040150187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1040150187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2801301760.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2801301760MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2607415263.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2607415263MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3991026007.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3991026007MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4084752571.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4084752571MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2115268468.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2115268468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4258277537.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4258277537MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3147261613.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3147261613MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2392475379.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2392475379MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2403133512.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2403133512MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3765932824.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3765932824MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3064208655.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3064208655MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3168164710.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3168164710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1643741485.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1643741485MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3958995187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3958995187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1045973371.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1045973371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4062951242.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4062951242MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1319344378.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1319344378MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1732246839.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1732246839MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr721576264.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr721576264MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1836202894.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1836202894MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1809018376.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1809018376MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr349332431.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr349332431MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1211250262.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1211250262MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr453288486.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr453288486MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1509469889.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1509469889MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr113287555.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr113287555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr911701775.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr911701775MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr217243610.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr217243610MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4102664885.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4102664885MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1795928589.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1795928589MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1876517491.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1876517491MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4215138346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4215138346MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr199362469.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr199362469MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4236641881.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4236641881MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2268182371.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2268182371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2360884342.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2360884342MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr585079693.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr585079693MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2565018987.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2565018987MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2653899595.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2653899595MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr689261448.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr689261448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1899867829.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1899867829MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3174675725.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3174675725MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3968687731.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3968687731MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1298918186.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1298918186MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2949220623.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2949220623MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr125305115.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr125305115MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2351452509.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2351452509MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1073463084.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1073463084MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr827029284.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr827029284MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1020091647.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1020091647MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3246239041.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3246239041MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2034936791.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2034936791MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2349101755.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2349101755MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr159179252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr159179252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr280281853.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr280281853MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr520022276.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr520022276MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr861903487.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr861903487MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2939232033.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2939232033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3088050881.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3088050881MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1958177301.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1958177301MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2126384403.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2126384403MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra82419762.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra82419762MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra57564501.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra57564501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2588701125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2588701125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr572637907.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr572637907MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Do_ICollectionCopyTo<ZXing.EncodeHintType>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisEncodeHintType_t3244562957_m1779277463_gshared (Dictionary_2_t3719137485 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2819433829 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisEncodeHintType_t3244562957_m1779277463(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3719137485 *, Il2CppArray *, int32_t, Transform_1_t2819433829 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisEncodeHintType_t3244562957_m1779277463_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>::Do_CopyTo<ZXing.EncodeHintType,ZXing.EncodeHintType>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisEncodeHintType_t3244562957_TisEncodeHintType_t3244562957_m2666990026_gshared (Dictionary_2_t3719137485 * __this, EncodeHintTypeU5BU5D_t2781905696* p0, int32_t p1, Transform_1_t2819433829 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisEncodeHintType_t3244562957_TisEncodeHintType_t3244562957_m2666990026(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3719137485 *, EncodeHintTypeU5BU5D_t2781905696*, int32_t, Transform_1_t2819433829 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisEncodeHintType_t3244562957_TisEncodeHintType_t3244562957_m2666990026_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Do_ICollectionCopyTo<ZXing.ResultMetadataType>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisResultMetadataType_t2923366972_m2714497589_gshared (Dictionary_2_t3135028994 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1750793963 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisResultMetadataType_t2923366972_m2714497589(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3135028994 *, Il2CppArray *, int32_t, Transform_1_t1750793963 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisResultMetadataType_t2923366972_m2714497589_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>::Do_CopyTo<ZXing.ResultMetadataType,ZXing.ResultMetadataType>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisResultMetadataType_t2923366972_TisResultMetadataType_t2923366972_m3783204635_gshared (Dictionary_2_t3135028994 * __this, ResultMetadataTypeU5BU5D_t2197797205* p0, int32_t p1, Transform_1_t1750793963 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisResultMetadataType_t2923366972_TisResultMetadataType_t2923366972_m3783204635(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3135028994 *, ResultMetadataTypeU5BU5D_t2197797205*, int32_t, Transform_1_t1750793963 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisResultMetadataType_t2923366972_TisResultMetadataType_t2923366972_m3783204635_gshared)(__this, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t KeyCollection__ctor_m529852539_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m529852539_gshared (KeyCollection_t1050929640 * __this, Dictionary_2_t3719137485 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m529852539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3719137485 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3719137485 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123164782;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1798950907_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1798950907_gshared (KeyCollection_t1050929640 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1798950907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2123164782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123164782;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2316941362_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2316941362_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2316941362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2123164782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2226256051_gshared (KeyCollection_t1050929640 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3719137485 * L_0 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3719137485 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3719137485 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3719137485 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123164782;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3631938264_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3631938264_gshared (KeyCollection_t1050929640 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3631938264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2123164782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4097076356_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1050929640 *)__this);
		Enumerator_t39106243  L_0 = ((  Enumerator_t39106243  (*) (KeyCollection_t1050929640 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1050929640 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t39106243  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2940133796_gshared (KeyCollection_t1050929640 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	EncodeHintTypeU5BU5D_t2781905696* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (EncodeHintTypeU5BU5D_t2781905696*)((EncodeHintTypeU5BU5D_t2781905696*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		EncodeHintTypeU5BU5D_t2781905696* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		EncodeHintTypeU5BU5D_t2781905696* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1050929640 *)__this);
		((  void (*) (KeyCollection_t1050929640 *, EncodeHintTypeU5BU5D_t2781905696*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1050929640 *)__this, (EncodeHintTypeU5BU5D_t2781905696*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3719137485 * L_4 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3719137485 *)L_4);
		((  void (*) (Dictionary_2_t3719137485 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3719137485 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3719137485 * L_7 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2819433829 * L_11 = (Transform_1_t2819433829 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2819433829 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3719137485 *)L_7);
		((  void (*) (Dictionary_2_t3719137485 *, Il2CppArray *, int32_t, Transform_1_t2819433829 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3719137485 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2819433829 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2667546419_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1050929640 *)__this);
		Enumerator_t39106243  L_0 = ((  Enumerator_t39106243  (*) (KeyCollection_t1050929640 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1050929640 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t39106243  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3419410260_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1277784326_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m4580792_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4580792_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m4580792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3719137485 * L_0 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2318072624_gshared (KeyCollection_t1050929640 * __this, EncodeHintTypeU5BU5D_t2781905696* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3719137485 * L_0 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		EncodeHintTypeU5BU5D_t2781905696* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3719137485 *)L_0);
		((  void (*) (Dictionary_2_t3719137485 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3719137485 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3719137485 * L_3 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		EncodeHintTypeU5BU5D_t2781905696* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2819433829 * L_7 = (Transform_1_t2819433829 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2819433829 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3719137485 *)L_3);
		((  void (*) (Dictionary_2_t3719137485 *, EncodeHintTypeU5BU5D_t2781905696*, int32_t, Transform_1_t2819433829 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3719137485 *)L_3, (EncodeHintTypeU5BU5D_t2781905696*)L_4, (int32_t)L_5, (Transform_1_t2819433829 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t39106243  KeyCollection_GetEnumerator_m1376463997_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3719137485 * L_0 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		Enumerator_t39106243  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3136035504(&L_1, (Dictionary_2_t3719137485 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m82071232_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3719137485 * L_0 = (Dictionary_2_t3719137485 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3719137485 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3719137485 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3719137485 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447049878;
extern const uint32_t KeyCollection__ctor_m1475468842_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1475468842_gshared (KeyCollection_t466821149 * __this, Dictionary_2_t3135028994 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1475468842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3135028994 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral447049878, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3135028994 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123164782;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3256338668_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3256338668_gshared (KeyCollection_t466821149 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3256338668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2123164782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123164782;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3377674595_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3377674595_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3377674595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2123164782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1890999650_gshared (KeyCollection_t466821149 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3135028994 * L_0 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3135028994 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3135028994 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3135028994 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123164782;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2925444935_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2925444935_gshared (KeyCollection_t466821149 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2925444935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral2123164782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1477277493_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t466821149 *)__this);
		Enumerator_t3749965048  L_0 = ((  Enumerator_t3749965048  (*) (KeyCollection_t466821149 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t466821149 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3749965048  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2387812693_gshared (KeyCollection_t466821149 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ResultMetadataTypeU5BU5D_t2197797205* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ResultMetadataTypeU5BU5D_t2197797205*)((ResultMetadataTypeU5BU5D_t2197797205*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ResultMetadataTypeU5BU5D_t2197797205* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ResultMetadataTypeU5BU5D_t2197797205* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t466821149 *)__this);
		((  void (*) (KeyCollection_t466821149 *, ResultMetadataTypeU5BU5D_t2197797205*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t466821149 *)__this, (ResultMetadataTypeU5BU5D_t2197797205*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3135028994 * L_4 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3135028994 *)L_4);
		((  void (*) (Dictionary_2_t3135028994 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3135028994 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3135028994 * L_7 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1750793963 * L_11 = (Transform_1_t1750793963 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1750793963 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3135028994 *)L_7);
		((  void (*) (Dictionary_2_t3135028994 *, Il2CppArray *, int32_t, Transform_1_t1750793963 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3135028994 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1750793963 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3378895908_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t466821149 *)__this);
		Enumerator_t3749965048  L_0 = ((  Enumerator_t3749965048  (*) (KeyCollection_t466821149 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t466821149 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3749965048  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3360556099_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3080254645_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3608160039_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3608160039_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3608160039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3135028994 * L_0 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3725749471_gshared (KeyCollection_t466821149 * __this, ResultMetadataTypeU5BU5D_t2197797205* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3135028994 * L_0 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		ResultMetadataTypeU5BU5D_t2197797205* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3135028994 *)L_0);
		((  void (*) (Dictionary_2_t3135028994 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3135028994 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3135028994 * L_3 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		ResultMetadataTypeU5BU5D_t2197797205* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1750793963 * L_7 = (Transform_1_t1750793963 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1750793963 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3135028994 *)L_3);
		((  void (*) (Dictionary_2_t3135028994 *, ResultMetadataTypeU5BU5D_t2197797205*, int32_t, Transform_1_t1750793963 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3135028994 *)L_3, (ResultMetadataTypeU5BU5D_t2197797205*)L_4, (int32_t)L_5, (Transform_1_t1750793963 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3749965048  KeyCollection_GetEnumerator_m1807819308_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3135028994 * L_0 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		Enumerator_t3749965048  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m860548575(&L_1, (Dictionary_2_t3135028994 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.ResultMetadataType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2184843375_gshared (KeyCollection_t466821149 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3135028994 * L_0 = (Dictionary_2_t3135028994 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3135028994 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3135028994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3135028994 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2694716770_gshared (ShimEnumerator_t4003355771 * __this, Dictionary_2_t4287577744 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4287577744 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4287577744 *)L_0);
		Enumerator_t1309933840  L_1 = ((  Enumerator_t1309933840  (*) (Dictionary_2_t4287577744 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4287577744 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m594395263_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1309933840 * L_0 = (Enumerator_t1309933840 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2347244280((Enumerator_t1309933840 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2009877557_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2009877557_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2009877557_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1309933840  L_0 = (Enumerator_t1309933840 )__this->get_host_enumerator_0();
		Enumerator_t1309933840  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2489925456_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4186358450  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1309933840 * L_0 = (Enumerator_t1309933840 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4186358450  L_1 = Enumerator_get_Current_m534300216((Enumerator_t1309933840 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4186358450 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m965051057((KeyValuePair_2_t4186358450 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1570015714_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4186358450  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1309933840 * L_0 = (Enumerator_t1309933840 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4186358450  L_1 = Enumerator_get_Current_m534300216((Enumerator_t1309933840 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4186358450 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3958869269((KeyValuePair_2_t4186358450 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2676271082_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2676271082_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2676271082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t4003355771 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t4003355771 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t4003355771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2317075636_gshared (ShimEnumerator_t4003355771 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1309933840 * L_0 = (Enumerator_t1309933840 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1067045851((Enumerator_t1309933840 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m316360667_gshared (ShimEnumerator_t2226662119 * __this, Dictionary_2_t2510884092 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2510884092 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2510884092 *)L_0);
		Enumerator_t3828207484  L_1 = ((  Enumerator_t3828207484  (*) (Dictionary_2_t2510884092 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2510884092 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3944981094_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3828207484 * L_0 = (Enumerator_t3828207484 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3044872223((Enumerator_t3828207484 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3853351278_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3853351278_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3853351278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3828207484  L_0 = (Enumerator_t3828207484 )__this->get_host_enumerator_0();
		Enumerator_t3828207484  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2477119561_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2409664798  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3828207484 * L_0 = (Enumerator_t3828207484 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2409664798  L_1 = Enumerator_get_Current_m4212335665((Enumerator_t3828207484 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2409664798 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1012642026((KeyValuePair_2_t2409664798 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2148452507_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2409664798  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3828207484 * L_0 = (Enumerator_t3828207484 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2409664798  L_1 = Enumerator_get_Current_m4212335665((Enumerator_t3828207484 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2409664798 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3824131982((KeyValuePair_2_t2409664798 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m208280675_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m208280675_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m208280675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2226662119 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2226662119 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1790724525_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3828207484 * L_0 = (Enumerator_t3828207484 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2959396244((Enumerator_t3828207484 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m903319451_gshared (ShimEnumerator_t1662330499 * __this, Dictionary_2_t1946552472 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1946552472 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1946552472 *)L_0);
		Enumerator_t3263875864  L_1 = ((  Enumerator_t3263875864  (*) (Dictionary_2_t1946552472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1946552472 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m990777834_gshared (ShimEnumerator_t1662330499 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3263875864 * L_0 = (Enumerator_t3263875864 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3152173905((Enumerator_t3263875864 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2989976192_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2989976192_gshared (ShimEnumerator_t1662330499 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2989976192_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3263875864  L_0 = (Enumerator_t3263875864 )__this->get_host_enumerator_0();
		Enumerator_t3263875864  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3545970879_gshared (ShimEnumerator_t1662330499 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1845333178  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3263875864 * L_0 = (Enumerator_t3263875864 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1845333178  L_1 = Enumerator_get_Current_m2674279371((Enumerator_t3263875864 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1845333178 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1096840254((KeyValuePair_2_t1845333178 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2817385361_gshared (ShimEnumerator_t1662330499 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1845333178  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3263875864 * L_0 = (Enumerator_t3263875864 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1845333178  L_1 = Enumerator_get_Current_m2674279371((Enumerator_t3263875864 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1845333178 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3223841534((KeyValuePair_2_t1845333178 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3102626265_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3102626265_gshared (ShimEnumerator_t1662330499 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3102626265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1662330499 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1662330499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1662330499 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.StateMachine,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1441329517_gshared (ShimEnumerator_t1662330499 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3263875864 * L_0 = (Enumerator_t3263875864 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3310414438((Enumerator_t3263875864 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m316912990_gshared (ShimEnumerator_t813817701 * __this, Dictionary_2_t1098039674 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1098039674 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1098039674 *)L_0);
		Enumerator_t2415363066  L_1 = ((  Enumerator_t2415363066  (*) (Dictionary_2_t1098039674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1098039674 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1593779399_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2415363066 * L_0 = (Enumerator_t2415363066 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1613914798((Enumerator_t2415363066 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m51187139_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m51187139_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m51187139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2415363066  L_0 = (Enumerator_t2415363066 )__this->get_host_enumerator_0();
		Enumerator_t2415363066  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1010456514_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t996820380  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2415363066 * L_0 = (Enumerator_t2415363066 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t996820380  L_1 = Enumerator_get_Current_m1941561422((Enumerator_t2415363066 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t996820380 )L_1;
		Il2CppChar L_2 = KeyValuePair_2_get_Key_m1713880001((KeyValuePair_2_t996820380 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppChar L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1434537428_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t996820380  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2415363066 * L_0 = (Enumerator_t2415363066 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t996820380  L_1 = Enumerator_get_Current_m1941561422((Enumerator_t2415363066 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t996820380 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m694146753((KeyValuePair_2_t996820380 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1330657116_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1330657116_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1330657116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t813817701 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t813817701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t813817701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Reset()
extern "C"  void ShimEnumerator_Reset_m1335829168_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2415363066 * L_0 = (Enumerator_t2415363066 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1744828201((Enumerator_t2415363066 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m710390134_gshared (ShimEnumerator_t866879766 * __this, Dictionary_2_t1151101739 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1151101739 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1151101739 *)L_0);
		Enumerator_t2468425131  L_1 = ((  Enumerator_t2468425131  (*) (Dictionary_2_t1151101739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1151101739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3381678955_gshared (ShimEnumerator_t866879766 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2468425131 * L_0 = (Enumerator_t2468425131 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2667991844((Enumerator_t2468425131 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2738492169_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2738492169_gshared (ShimEnumerator_t866879766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2738492169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2468425131  L_0 = (Enumerator_t2468425131 )__this->get_host_enumerator_0();
		Enumerator_t2468425131  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m702986404_gshared (ShimEnumerator_t866879766 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2468425131 * L_0 = (Enumerator_t2468425131 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1049882445  L_1 = Enumerator_get_Current_m760986380((Enumerator_t2468425131 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1049882445 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m3585751456((KeyValuePair_2_t1049882445 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2308505142_gshared (ShimEnumerator_t866879766 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2468425131 * L_0 = (Enumerator_t2468425131 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1049882445  L_1 = Enumerator_get_Current_m760986380((Enumerator_t2468425131 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1049882445 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m906758643((KeyValuePair_2_t1049882445 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3695007550_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3695007550_gshared (ShimEnumerator_t866879766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3695007550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t866879766 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t866879766 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t866879766 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m101612232_gshared (ShimEnumerator_t866879766 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2468425131 * L_0 = (Enumerator_t2468425131 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m619818159((Enumerator_t2468425131 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3534173527_gshared (ShimEnumerator_t3883857637 * __this, Dictionary_2_t4168079610 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4168079610 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4168079610 *)L_0);
		Enumerator_t1190435706  L_1 = ((  Enumerator_t1190435706  (*) (Dictionary_2_t4168079610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4168079610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1863458990_gshared (ShimEnumerator_t3883857637 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1190435706 * L_0 = (Enumerator_t1190435706 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1213995029((Enumerator_t1190435706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m4239870524_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m4239870524_gshared (ShimEnumerator_t3883857637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m4239870524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1190435706  L_0 = (Enumerator_t1190435706 )__this->get_host_enumerator_0();
		Enumerator_t1190435706  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3865492347_gshared (ShimEnumerator_t3883857637 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1190435706 * L_0 = (Enumerator_t1190435706 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4066860316  L_1 = Enumerator_get_Current_m1399860359((Enumerator_t1190435706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4066860316 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m494458106((KeyValuePair_2_t4066860316 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m639870797_gshared (ShimEnumerator_t3883857637 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1190435706 * L_0 = (Enumerator_t1190435706 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4066860316  L_1 = Enumerator_get_Current_m1399860359((Enumerator_t1190435706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4066860316 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1563175098((KeyValuePair_2_t4066860316 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2160203413_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2160203413_gshared (ShimEnumerator_t3883857637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2160203413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3883857637 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3883857637 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3883857637 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2195569961_gshared (ShimEnumerator_t3883857637 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1190435706 * L_0 = (Enumerator_t1190435706 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1080084514((Enumerator_t1190435706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m544537892_gshared (ShimEnumerator_t1981506396 * __this, Dictionary_2_t2265728369 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2265728369 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2265728369 *)L_0);
		Enumerator_t3583051761  L_1 = ((  Enumerator_t3583051761  (*) (Dictionary_2_t2265728369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2265728369 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1974061565_gshared (ShimEnumerator_t1981506396 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3583051761 * L_0 = (Enumerator_t3583051761 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1073952694((Enumerator_t3583051761 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2884388023_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2884388023_gshared (ShimEnumerator_t1981506396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2884388023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3583051761  L_0 = (Enumerator_t3583051761 )__this->get_host_enumerator_0();
		Enumerator_t3583051761  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m612426194_gshared (ShimEnumerator_t1981506396 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2164509075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3583051761 * L_0 = (Enumerator_t3583051761 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2164509075  L_1 = Enumerator_get_Current_m751583546((Enumerator_t3583051761 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2164509075 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m3442915955((KeyValuePair_2_t2164509075 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1179489252_gshared (ShimEnumerator_t1981506396 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2164509075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3583051761 * L_0 = (Enumerator_t3583051761 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2164509075  L_1 = Enumerator_get_Current_m751583546((Enumerator_t3583051761 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2164509075 )L_1;
		Label_t2268465130  L_2 = KeyValuePair_2_get_Value_m2855168727((KeyValuePair_2_t2164509075 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Label_t2268465130  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1042495852_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1042495852_gshared (ShimEnumerator_t1981506396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1042495852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1981506396 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1981506396 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1981506396 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::Reset()
extern "C"  void ShimEnumerator_Reset_m2964201846_gshared (ShimEnumerator_t1981506396 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3583051761 * L_0 = (Enumerator_t3583051761 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m4132873565((Enumerator_t3583051761 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3625952347_gshared (ShimEnumerator_t108917990 * __this, Dictionary_2_t393139963 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t393139963 * L_0 = ___host0;
		NullCheck((Dictionary_2_t393139963 *)L_0);
		Enumerator_t1710463355  L_1 = ((  Enumerator_t1710463355  (*) (Dictionary_2_t393139963 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t393139963 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1584882278_gshared (ShimEnumerator_t108917990 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1710463355 * L_0 = (Enumerator_t1710463355 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3494917087((Enumerator_t1710463355 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2985497134_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2985497134_gshared (ShimEnumerator_t108917990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2985497134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1710463355  L_0 = (Enumerator_t1710463355 )__this->get_host_enumerator_0();
		Enumerator_t1710463355  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2692702601_gshared (ShimEnumerator_t108917990 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t291920669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1710463355 * L_0 = (Enumerator_t1710463355 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t291920669  L_1 = Enumerator_get_Current_m2970416625((Enumerator_t1710463355 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t291920669 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m2157360106((KeyValuePair_2_t291920669 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3165323739_gshared (ShimEnumerator_t108917990 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t291920669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1710463355 * L_0 = (Enumerator_t1710463355 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t291920669  L_1 = Enumerator_get_Current_m2970416625((Enumerator_t1710463355 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t291920669 )L_1;
		TrackableResultData_t395876724  L_2 = KeyValuePair_2_get_Value_m2398953998((KeyValuePair_2_t291920669 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		TrackableResultData_t395876724  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2463958435_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2463958435_gshared (ShimEnumerator_t108917990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2463958435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t108917990 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t108917990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t108917990 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C"  void ShimEnumerator_Reset_m3170756141_gshared (ShimEnumerator_t108917990 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1710463355 * L_0 = (Enumerator_t1710463355 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2909396564((Enumerator_t1710463355 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3580527062_gshared (ShimEnumerator_t172421601 * __this, Dictionary_2_t456643574 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t456643574 * L_0 = ___host0;
		NullCheck((Dictionary_2_t456643574 *)L_0);
		Enumerator_t1773966966  L_1 = ((  Enumerator_t1773966966  (*) (Dictionary_2_t456643574 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t456643574 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m4139990795_gshared (ShimEnumerator_t172421601 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1773966966 * L_0 = (Enumerator_t1773966966 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2372147908((Enumerator_t1773966966 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3341961065_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3341961065_gshared (ShimEnumerator_t172421601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3341961065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1773966966  L_0 = (Enumerator_t1773966966 )__this->get_host_enumerator_0();
		Enumerator_t1773966966  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1282362628_gshared (ShimEnumerator_t172421601 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t355424280  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1773966966 * L_0 = (Enumerator_t1773966966 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t355424280  L_1 = Enumerator_get_Current_m2953865068((Enumerator_t1773966966 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t355424280 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m4182360997((KeyValuePair_2_t355424280 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m743307926_gshared (ShimEnumerator_t172421601 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t355424280  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1773966966 * L_0 = (Enumerator_t1773966966 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t355424280  L_1 = Enumerator_get_Current_m2953865068((Enumerator_t1773966966 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t355424280 )L_1;
		VirtualButtonData_t459380335  L_2 = KeyValuePair_2_get_Value_m1627425673((KeyValuePair_2_t355424280 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		VirtualButtonData_t459380335  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2779036574_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2779036574_gshared (ShimEnumerator_t172421601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2779036574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t172421601 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t172421601 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t172421601 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
extern "C"  void ShimEnumerator_Reset_m113163048_gshared (ShimEnumerator_t172421601 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1773966966 * L_0 = (Enumerator_t1773966966 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2991100175((Enumerator_t1773966966 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2646074293_gshared (ShimEnumerator_t1649192837 * __this, Dictionary_2_t1933414810 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1933414810 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1933414810 *)L_0);
		Enumerator_t3250738202  L_1 = ((  Enumerator_t3250738202  (*) (Dictionary_2_t1933414810 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1933414810 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1159482828_gshared (ShimEnumerator_t1649192837 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3250738202 * L_0 = (Enumerator_t3250738202 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m4087659077((Enumerator_t3250738202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2145784456_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2145784456_gshared (ShimEnumerator_t1649192837 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2145784456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3250738202  L_0 = (Enumerator_t3250738202 )__this->get_host_enumerator_0();
		Enumerator_t3250738202  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m897063523_gshared (ShimEnumerator_t1649192837 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1832195516  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3250738202 * L_0 = (Enumerator_t3250738202 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1832195516  L_1 = Enumerator_get_Current_m1568685003((Enumerator_t3250738202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1832195516 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m790708164((KeyValuePair_2_t1832195516 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4133022773_gshared (ShimEnumerator_t1649192837 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1832195516  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3250738202 * L_0 = (Enumerator_t3250738202 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1832195516  L_1 = Enumerator_get_Current_m1568685003((Enumerator_t3250738202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1832195516 )L_1;
		ArrayMetadata_t4058342910  L_2 = KeyValuePair_2_get_Value_m764851560((KeyValuePair_2_t1832195516 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		ArrayMetadata_t4058342910  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m414826877_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m414826877_gshared (ShimEnumerator_t1649192837 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m414826877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1649192837 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1649192837 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1649192837 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ArrayMetadata>::Reset()
extern "C"  void ShimEnumerator_Reset_m481279623_gshared (ShimEnumerator_t1649192837 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3250738202 * L_0 = (Enumerator_t3250738202 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1550252974((Enumerator_t3250738202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2666508385_gshared (ShimEnumerator_t3895111721 * __this, Dictionary_2_t4179333694 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4179333694 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4179333694 *)L_0);
		Enumerator_t1201689790  L_1 = ((  Enumerator_t1201689790  (*) (Dictionary_2_t4179333694 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4179333694 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m4053247460_gshared (ShimEnumerator_t3895111721 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1201689790 * L_0 = (Enumerator_t1201689790 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m337430667((Enumerator_t1201689790 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3141498374_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3141498374_gshared (ShimEnumerator_t3895111721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3141498374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1201689790  L_0 = (Enumerator_t1201689790 )__this->get_host_enumerator_0();
		Enumerator_t1201689790  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3861877573_gshared (ShimEnumerator_t3895111721 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4078114400  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1201689790 * L_0 = (Enumerator_t1201689790 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4078114400  L_1 = Enumerator_get_Current_m2439956689((Enumerator_t1201689790 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4078114400 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m564861444((KeyValuePair_2_t4078114400 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1461040279_gshared (ShimEnumerator_t3895111721 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4078114400  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1201689790 * L_0 = (Enumerator_t1201689790 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4078114400  L_1 = Enumerator_get_Current_m2439956689((Enumerator_t1201689790 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4078114400 )L_1;
		ObjectMetadata_t2009294498  L_2 = KeyValuePair_2_get_Value_m126947780((KeyValuePair_2_t4078114400 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		ObjectMetadata_t2009294498  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1030093151_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1030093151_gshared (ShimEnumerator_t3895111721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1030093151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3895111721 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3895111721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3895111721 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.ObjectMetadata>::Reset()
extern "C"  void ShimEnumerator_Reset_m2169063731_gshared (ShimEnumerator_t3895111721 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1201689790 * L_0 = (Enumerator_t1201689790 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m947499244((Enumerator_t1201689790 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m48464311_gshared (ShimEnumerator_t1657484543 * __this, Dictionary_2_t1941706516 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1941706516 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1941706516 *)L_0);
		Enumerator_t3259029908  L_1 = ((  Enumerator_t3259029908  (*) (Dictionary_2_t1941706516 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1941706516 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m109532494_gshared (ShimEnumerator_t1657484543 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3259029908 * L_0 = (Enumerator_t3259029908 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2622384693((Enumerator_t3259029908 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3723557148_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3723557148_gshared (ShimEnumerator_t1657484543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3723557148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3259029908  L_0 = (Enumerator_t3259029908 )__this->get_host_enumerator_0();
		Enumerator_t3259029908  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2311108443_gshared (ShimEnumerator_t1657484543 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1840487222  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3259029908 * L_0 = (Enumerator_t3259029908 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1840487222  L_1 = Enumerator_get_Current_m571333991((Enumerator_t3259029908 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1840487222 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3564472922((KeyValuePair_2_t1840487222 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1525558061_gshared (ShimEnumerator_t1657484543 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1840487222  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3259029908 * L_0 = (Enumerator_t3259029908 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1840487222  L_1 = Enumerator_get_Current_m571333991((Enumerator_t3259029908 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1840487222 )L_1;
		PropertyMetadata_t4066634616  L_2 = KeyValuePair_2_get_Value_m3657888026((KeyValuePair_2_t1840487222 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		PropertyMetadata_t4066634616  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2902139509_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2902139509_gshared (ShimEnumerator_t1657484543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2902139509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1657484543 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1657484543 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1657484543 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,LitJson.PropertyMetadata>::Reset()
extern "C"  void ShimEnumerator_Reset_m1144879497_gshared (ShimEnumerator_t1657484543 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3259029908 * L_0 = (Enumerator_t3259029908 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m4042446594((Enumerator_t3259029908 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3002184013_gshared (ShimEnumerator_t2362615941 * __this, Dictionary_2_t2646837914 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2646837914 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2646837914 *)L_0);
		Enumerator_t3964161306  L_1 = ((  Enumerator_t3964161306  (*) (Dictionary_2_t2646837914 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2646837914 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3121803640_gshared (ShimEnumerator_t2362615941 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3964161306 * L_0 = (Enumerator_t3964161306 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1757195039((Enumerator_t3964161306 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1318414322_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1318414322_gshared (ShimEnumerator_t2362615941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1318414322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3964161306  L_0 = (Enumerator_t3964161306 )__this->get_host_enumerator_0();
		Enumerator_t3964161306  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1859453489_gshared (ShimEnumerator_t2362615941 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3964161306 * L_0 = (Enumerator_t3964161306 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2545618620  L_1 = Enumerator_get_Current_m3861017533((Enumerator_t3964161306 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2545618620 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m700889072((KeyValuePair_2_t2545618620 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1276844163_gshared (ShimEnumerator_t2362615941 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3964161306 * L_0 = (Enumerator_t3964161306 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2545618620  L_1 = Enumerator_get_Current_m3861017533((Enumerator_t3964161306 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2545618620 )L_1;
		bool L_2 = KeyValuePair_2_get_Value_m3809014448((KeyValuePair_2_t2545618620 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m111284811_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m111284811_gshared (ShimEnumerator_t2362615941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m111284811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2362615941 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2362615941 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2362615941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C"  void ShimEnumerator_Reset_m3498223647_gshared (ShimEnumerator_t2362615941 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3964161306 * L_0 = (Enumerator_t3964161306 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m963565784((Enumerator_t3964161306 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1741374067_gshared (ShimEnumerator_t3039655723 * __this, Dictionary_2_t3323877696 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3323877696 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3323877696 *)L_0);
		Enumerator_t346233792  L_1 = ((  Enumerator_t346233792  (*) (Dictionary_2_t3323877696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3323877696 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3423852562_gshared (ShimEnumerator_t3039655723 * __this, const MethodInfo* method)
{
	{
		Enumerator_t346233792 * L_0 = (Enumerator_t346233792 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2774388601((Enumerator_t346233792 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1072463704_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1072463704_gshared (ShimEnumerator_t3039655723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1072463704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t346233792  L_0 = (Enumerator_t346233792 )__this->get_host_enumerator_0();
		Enumerator_t346233792  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3361638295_gshared (ShimEnumerator_t3039655723 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t346233792 * L_0 = (Enumerator_t346233792 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3222658402  L_1 = Enumerator_get_Current_m2653719203((Enumerator_t346233792 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3222658402 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4285571350((KeyValuePair_2_t3222658402 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1767431273_gshared (ShimEnumerator_t3039655723 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t346233792 * L_0 = (Enumerator_t346233792 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3222658402  L_1 = Enumerator_get_Current_m2653719203((Enumerator_t346233792 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3222658402 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m2690735574((KeyValuePair_2_t3222658402 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3414062257_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3414062257_gshared (ShimEnumerator_t3039655723 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3414062257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3039655723 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3039655723 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3039655723 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m827449413_gshared (ShimEnumerator_t3039655723 * __this, const MethodInfo* method)
{
	{
		Enumerator_t346233792 * L_0 = (Enumerator_t346233792 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m4006931262((Enumerator_t346233792 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1134937082_gshared (ShimEnumerator_t1761666298 * __this, Dictionary_2_t2045888271 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2045888271 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2045888271 *)L_0);
		Enumerator_t3363211663  L_1 = ((  Enumerator_t3363211663  (*) (Dictionary_2_t2045888271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2045888271 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3170840807_gshared (ShimEnumerator_t1761666298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3363211663 * L_0 = (Enumerator_t3363211663 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m217327200((Enumerator_t3363211663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m4132595661_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m4132595661_gshared (ShimEnumerator_t1761666298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m4132595661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3363211663  L_0 = (Enumerator_t3363211663 )__this->get_host_enumerator_0();
		Enumerator_t3363211663  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m384355048_gshared (ShimEnumerator_t1761666298 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3363211663 * L_0 = (Enumerator_t3363211663 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1944668977  L_1 = Enumerator_get_Current_m4240003024((Enumerator_t3363211663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1944668977 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3256475977((KeyValuePair_2_t1944668977 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1046450042_gshared (ShimEnumerator_t1761666298 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3363211663 * L_0 = (Enumerator_t3363211663 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1944668977  L_1 = Enumerator_get_Current_m4240003024((Enumerator_t3363211663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1944668977 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3899079597((KeyValuePair_2_t1944668977 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2040833922_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2040833922_gshared (ShimEnumerator_t1761666298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2040833922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1761666298 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1761666298 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1761666298 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3221686092_gshared (ShimEnumerator_t1761666298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3363211663 * L_0 = (Enumerator_t3363211663 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3001375603((Enumerator_t3363211663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m669354819_gshared (ShimEnumerator_t1882768899 * __this, Dictionary_2_t2166990872 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2166990872 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2166990872 *)L_0);
		Enumerator_t3484314264  L_1 = ((  Enumerator_t3484314264  (*) (Dictionary_2_t2166990872 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2166990872 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1177093758_gshared (ShimEnumerator_t1882768899 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3484314264 * L_0 = (Enumerator_t3484314264 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2518547447((Enumerator_t3484314264 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2455979286_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2455979286_gshared (ShimEnumerator_t1882768899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2455979286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3484314264  L_0 = (Enumerator_t3484314264 )__this->get_host_enumerator_0();
		Enumerator_t3484314264  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2398250609_gshared (ShimEnumerator_t1882768899 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3484314264 * L_0 = (Enumerator_t3484314264 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2065771578  L_1 = Enumerator_get_Current_m3624402649((Enumerator_t3484314264 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2065771578 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m975404242((KeyValuePair_2_t2065771578 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3664800963_gshared (ShimEnumerator_t1882768899 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3484314264 * L_0 = (Enumerator_t3484314264 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2065771578  L_1 = Enumerator_get_Current_m3624402649((Enumerator_t3484314264 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2065771578 )L_1;
		float L_2 = KeyValuePair_2_get_Value_m2222463222((KeyValuePair_2_t2065771578 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1425233547_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1425233547_gshared (ShimEnumerator_t1882768899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1425233547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1882768899 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1882768899 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1882768899 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Reset()
extern "C"  void ShimEnumerator_Reset_m2879648021_gshared (ShimEnumerator_t1882768899 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3484314264 * L_0 = (Enumerator_t3484314264 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2659337532((Enumerator_t3484314264 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m60159898_gshared (ShimEnumerator_t1910485146 * __this, Dictionary_2_t2194707119 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2194707119 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2194707119 *)L_0);
		Enumerator_t3512030511  L_1 = ((  Enumerator_t3512030511  (*) (Dictionary_2_t2194707119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2194707119 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2522892615_gshared (ShimEnumerator_t1910485146 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3512030511 * L_0 = (Enumerator_t3512030511 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3864346304((Enumerator_t3512030511 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1226070893_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1226070893_gshared (ShimEnumerator_t1910485146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1226070893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3512030511  L_0 = (Enumerator_t3512030511 )__this->get_host_enumerator_0();
		Enumerator_t3512030511  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3134400136_gshared (ShimEnumerator_t1910485146 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3512030511 * L_0 = (Enumerator_t3512030511 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2093487825  L_1 = Enumerator_get_Current_m2798443376((Enumerator_t3512030511 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2093487825 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1711553769((KeyValuePair_2_t2093487825 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2434892570_gshared (ShimEnumerator_t1910485146 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3512030511 * L_0 = (Enumerator_t3512030511 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2093487825  L_1 = Enumerator_get_Current_m2798443376((Enumerator_t3512030511 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2093487825 )L_1;
		uint16_t L_2 = KeyValuePair_2_get_Value_m992554829((KeyValuePair_2_t2093487825 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		uint16_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m599274274_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m599274274_gshared (ShimEnumerator_t1910485146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m599274274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1910485146 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1910485146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1910485146 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::Reset()
extern "C"  void ShimEnumerator_Reset_m873712364_gshared (ShimEnumerator_t1910485146 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3512030511 * L_0 = (Enumerator_t3512030511 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m653401875((Enumerator_t3512030511 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4026385586_gshared (ShimEnumerator_t1736811037 * __this, Dictionary_2_t2021033010 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2021033010 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2021033010 *)L_0);
		Enumerator_t3338356402  L_1 = ((  Enumerator_t3338356402  (*) (Dictionary_2_t2021033010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2021033010 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m354623023_gshared (ShimEnumerator_t1736811037 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3338356402 * L_0 = (Enumerator_t3338356402 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m978820392((Enumerator_t3338356402 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3193073413_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3193073413_gshared (ShimEnumerator_t1736811037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3193073413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3338356402  L_0 = (Enumerator_t3338356402 )__this->get_host_enumerator_0();
		Enumerator_t3338356402  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1143012640_gshared (ShimEnumerator_t1736811037 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3338356402 * L_0 = (Enumerator_t3338356402 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1919813716  L_1 = Enumerator_get_Current_m2002023176((Enumerator_t3338356402 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1919813716 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1474304257((KeyValuePair_2_t1919813716 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4266922930_gshared (ShimEnumerator_t1736811037 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3338356402 * L_0 = (Enumerator_t3338356402 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1919813716  L_1 = Enumerator_get_Current_m2002023176((Enumerator_t3338356402 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1919813716 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m2789648485((KeyValuePair_2_t1919813716 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m243858874_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m243858874_gshared (ShimEnumerator_t1736811037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m243858874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1736811037 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1736811037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1736811037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C"  void ShimEnumerator_Reset_m2655876100_gshared (ShimEnumerator_t1736811037 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3338356402 * L_0 = (Enumerator_t3338356402 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2449944235((Enumerator_t3338356402 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3727177410_gshared (ShimEnumerator_t3847508013 * __this, Dictionary_2_t4131729986 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4131729986 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4131729986 *)L_0);
		Enumerator_t1154086082  L_1 = ((  Enumerator_t1154086082  (*) (Dictionary_2_t4131729986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4131729986 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3539716639_gshared (ShimEnumerator_t3847508013 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1154086082 * L_0 = (Enumerator_t1154086082 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m4163914008((Enumerator_t1154086082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3146727701_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3146727701_gshared (ShimEnumerator_t3847508013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3146727701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1154086082  L_0 = (Enumerator_t1154086082 )__this->get_host_enumerator_0();
		Enumerator_t1154086082  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1938494256_gshared (ShimEnumerator_t3847508013 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1154086082 * L_0 = (Enumerator_t1154086082 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4030510692  L_1 = Enumerator_get_Current_m413466904((Enumerator_t1154086082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4030510692 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2269785873((KeyValuePair_2_t4030510692 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4220577218_gshared (ShimEnumerator_t3847508013 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1154086082 * L_0 = (Enumerator_t1154086082 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t4030510692  L_1 = Enumerator_get_Current_m413466904((Enumerator_t1154086082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t4030510692 )L_1;
		ProfileData_t1961690790  L_2 = KeyValuePair_2_get_Value_m2743302773((KeyValuePair_2_t4030510692 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		ProfileData_t1961690790  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2950269898_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2950269898_gshared (ShimEnumerator_t3847508013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2950269898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3847508013 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3847508013 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3847508013 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
extern "C"  void ShimEnumerator_Reset_m1919274516_gshared (ShimEnumerator_t3847508013 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1154086082 * L_0 = (Enumerator_t1154086082 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1713342651((Enumerator_t1154086082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4265492650_gshared (ShimEnumerator_t3647308914 * __this, Dictionary_2_t3931530887 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3931530887 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3931530887 *)L_0);
		Enumerator_t953886983  L_1 = ((  Enumerator_t953886983  (*) (Dictionary_2_t3931530887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3931530887 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3296381623_gshared (ShimEnumerator_t3647308914 * __this, const MethodInfo* method)
{
	{
		Enumerator_t953886983 * L_0 = (Enumerator_t953886983 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3297031152((Enumerator_t953886983 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m90288317_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m90288317_gshared (ShimEnumerator_t3647308914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m90288317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t953886983  L_0 = (Enumerator_t953886983 )__this->get_host_enumerator_0();
		Enumerator_t953886983  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1555902552_gshared (ShimEnumerator_t3647308914 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3830311593  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t953886983 * L_0 = (Enumerator_t953886983 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3830311593  L_1 = Enumerator_get_Current_m722290112((Enumerator_t953886983 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3830311593 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m2409877625((KeyValuePair_2_t3830311593 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1622169834_gshared (ShimEnumerator_t3647308914 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3830311593  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t953886983 * L_0 = (Enumerator_t953886983 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3830311593  L_1 = Enumerator_get_Current_m722290112((Enumerator_t953886983 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3830311593 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2845212253((KeyValuePair_2_t3830311593 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1256772850_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1256772850_gshared (ShimEnumerator_t3647308914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1256772850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3647308914 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3647308914 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3647308914 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.NetworkReachability,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m299334652_gshared (ShimEnumerator_t3647308914 * __this, const MethodInfo* method)
{
	{
		Enumerator_t953886983 * L_0 = (Enumerator_t953886983 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2252190819((Enumerator_t953886983 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2717421784_gshared (ShimEnumerator_t1535079881 * __this, Dictionary_2_t1819301854 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1819301854 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1819301854 *)L_0);
		Enumerator_t3136625246  L_1 = ((  Enumerator_t3136625246  (*) (Dictionary_2_t1819301854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1819301854 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2175557517_gshared (ShimEnumerator_t1535079881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3136625246 * L_0 = (Enumerator_t3136625246 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3254900404((Enumerator_t3136625246 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2740726781_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2740726781_gshared (ShimEnumerator_t1535079881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2740726781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3136625246  L_0 = (Enumerator_t3136625246 )__this->get_host_enumerator_0();
		Enumerator_t3136625246  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m697573884_gshared (ShimEnumerator_t1535079881 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3136625246 * L_0 = (Enumerator_t3136625246 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1718082560  L_1 = Enumerator_get_Current_m2234915592((Enumerator_t3136625246 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1718082560 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m2589995323((KeyValuePair_2_t1718082560 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1402040718_gshared (ShimEnumerator_t1535079881 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3136625246 * L_0 = (Enumerator_t3136625246 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1718082560  L_1 = Enumerator_get_Current_m2234915592((Enumerator_t3136625246 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1718082560 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1871736891((KeyValuePair_2_t1718082560 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m166089878_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m166089878_gshared (ShimEnumerator_t1535079881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m166089878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1535079881 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1535079881 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1535079881 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1754621738_gshared (ShimEnumerator_t1535079881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3136625246 * L_0 = (Enumerator_t3136625246 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3177794147((Enumerator_t3136625246 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m898693817_gshared (ShimEnumerator_t2473866692 * __this, Dictionary_2_t2758088665 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2758088665 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2758088665 *)L_0);
		Enumerator_t4075412057  L_1 = ((  Enumerator_t4075412057  (*) (Dictionary_2_t2758088665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2758088665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m276639116_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4075412057 * L_0 = (Enumerator_t4075412057 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2446888371((Enumerator_t4075412057 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m513135326_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m513135326_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m513135326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t4075412057  L_0 = (Enumerator_t4075412057 )__this->get_host_enumerator_0();
		Enumerator_t4075412057  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3163757341_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2656869371  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4075412057 * L_0 = (Enumerator_t4075412057 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2656869371  L_1 = Enumerator_get_Current_m1429539241((Enumerator_t4075412057 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2656869371 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m548895580((KeyValuePair_2_t2656869371 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m582395503_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2656869371  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4075412057 * L_0 = (Enumerator_t4075412057 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2656869371  L_1 = Enumerator_get_Current_m1429539241((Enumerator_t4075412057 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2656869371 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2031527964((KeyValuePair_2_t2656869371 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2761020727_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2761020727_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2761020727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2473866692 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2473866692 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3467939723_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4075412057 * L_0 = (Enumerator_t4075412057 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1379019204((Enumerator_t4075412057 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m772629001_gshared (ShimEnumerator_t1005475740 * __this, Dictionary_2_t1289697713 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1289697713 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1289697713 *)L_0);
		Enumerator_t2607021105  L_1 = ((  Enumerator_t2607021105  (*) (Dictionary_2_t1289697713 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1289697713 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3488666744_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2607021105 * L_0 = (Enumerator_t2607021105 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2959870769((Enumerator_t2607021105 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1581620892_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1581620892_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1581620892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2607021105  L_0 = (Enumerator_t2607021105 )__this->get_host_enumerator_0();
		Enumerator_t2607021105  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2760163063_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1188478419  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2607021105 * L_0 = (Enumerator_t2607021105 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1188478419  L_1 = Enumerator_get_Current_m1243001311((Enumerator_t2607021105 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1188478419 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m994691224((KeyValuePair_2_t1188478419 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3570318281_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1188478419  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2607021105 * L_0 = (Enumerator_t2607021105 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1188478419  L_1 = Enumerator_get_Current_m1243001311((Enumerator_t2607021105 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1188478419 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3611506108((KeyValuePair_2_t1188478419 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m821689361_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m821689361_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m821689361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1005475740 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1005475740 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1289944795_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2607021105 * L_0 = (Enumerator_t2607021105 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3150272962((Enumerator_t2607021105 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1459855393_gshared (ShimEnumerator_t897894528 * __this, Dictionary_2_t1182116501 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1182116501 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1182116501 *)L_0);
		Enumerator_t2499439893  L_1 = ((  Enumerator_t2499439893  (*) (Dictionary_2_t1182116501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1182116501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m337221284_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2499439893 * L_0 = (Enumerator_t2499439893 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1124415243((Enumerator_t2499439893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2931820550_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2931820550_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2931820550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2499439893  L_0 = (Enumerator_t2499439893 )__this->get_host_enumerator_0();
		Enumerator_t2499439893  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3472880069_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1080897207  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2499439893 * L_0 = (Enumerator_t2499439893 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1080897207  L_1 = Enumerator_get_Current_m2086476753((Enumerator_t2499439893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1080897207 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m4050111561((KeyValuePair_2_t1080897207 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1296593687_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1080897207  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2499439893 * L_0 = (Enumerator_t2499439893 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1080897207  L_1 = Enumerator_get_Current_m2086476753((Enumerator_t2499439893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1080897207 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1951026170((KeyValuePair_2_t1080897207 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1910708191_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1910708191_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1910708191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t897894528 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t897894528 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t897894528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3297876211_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2499439893 * L_0 = (Enumerator_t2499439893 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m838507244((Enumerator_t2499439893 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3225053257_gshared (ShimEnumerator_t3434915512 * __this, Dictionary_2_t3719137485 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3719137485 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3719137485 *)L_0);
		Enumerator_t741493581  L_1 = ((  Enumerator_t741493581  (*) (Dictionary_2_t3719137485 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3719137485 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2965846396_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method)
{
	{
		Enumerator_t741493581 * L_0 = (Enumerator_t741493581 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3753040355((Enumerator_t741493581 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2814820398_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2814820398_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2814820398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t741493581  L_0 = (Enumerator_t741493581 )__this->get_host_enumerator_0();
		Enumerator_t741493581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m925275117_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3617918191  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t741493581 * L_0 = (Enumerator_t741493581 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3617918191  L_1 = Enumerator_get_Current_m1318480377((Enumerator_t741493581 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3617918191 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m2030222956((KeyValuePair_2_t3617918191 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1179593535_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3617918191  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t741493581 * L_0 = (Enumerator_t741493581 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3617918191  L_1 = Enumerator_get_Current_m1318480377((Enumerator_t741493581 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3617918191 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2456416172((KeyValuePair_2_t3617918191 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1142711815_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1142711815_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1142711815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3434915512 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3434915512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3434915512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.EncodeHintType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3603604763_gshared (ShimEnumerator_t3434915512 * __this, const MethodInfo* method)
{
	{
		Enumerator_t741493581 * L_0 = (Enumerator_t741493581 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1144235796((Enumerator_t741493581 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m655804152_gshared (ShimEnumerator_t2850807021 * __this, Dictionary_2_t3135028994 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3135028994 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3135028994 *)L_0);
		Enumerator_t157385090  L_1 = ((  Enumerator_t157385090  (*) (Dictionary_2_t3135028994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3135028994 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2518629869_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method)
{
	{
		Enumerator_t157385090 * L_0 = (Enumerator_t157385090 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m736514772((Enumerator_t157385090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1096164445_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1096164445_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1096164445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t157385090  L_0 = (Enumerator_t157385090 )__this->get_host_enumerator_0();
		Enumerator_t157385090  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t1751606614  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m726253276_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3033809700  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t157385090 * L_0 = (Enumerator_t157385090 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3033809700  L_1 = Enumerator_get_Current_m1087716456((Enumerator_t157385090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3033809700 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m3380131680((KeyValuePair_2_t3033809700 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3193132654_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3033809700  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t157385090 * L_0 = (Enumerator_t157385090 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3033809700  L_1 = Enumerator_get_Current_m1087716456((Enumerator_t157385090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3033809700 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1831339473((KeyValuePair_2_t3033809700 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3418521974_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3418521974_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3418521974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2850807021 *)__this);
		DictionaryEntry_t1751606614  L_0 = ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2850807021 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2850807021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.ResultMetadataType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3887361354_gshared (ShimEnumerator_t2850807021 * __this, const MethodInfo* method)
{
	{
		Enumerator_t157385090 * L_0 = (Enumerator_t157385090 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2489961155((Enumerator_t157385090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,InApp.InAppEventDispatcher/EventNames>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3398306291_gshared (Transform_1_t3966784111 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,InApp.InAppEventDispatcher/EventNames>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2097913257_gshared (Transform_1_t3966784111 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2097913257((Transform_1_t3966784111 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,InApp.InAppEventDispatcher/EventNames>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* EventNames_t1614635846_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m848982280_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m848982280_gshared (Transform_1_t3966784111 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m848982280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EventNames_t1614635846_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,InApp.InAppEventDispatcher/EventNames>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2406551361_gshared (Transform_1_t3966784111 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2817164477_gshared (Transform_1_t4103754879 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m894862683_gshared (Transform_1_t4103754879 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m894862683((Transform_1_t4103754879 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* EventNames_t1614635846_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2516332806_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2516332806_gshared (Transform_1_t4103754879 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2516332806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EventNames_t1614635846_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1252312143_gshared (Transform_1_t4103754879 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1940021356_gshared (Transform_1_t2243539419 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4186358450  Transform_1_Invoke_m213811340_gshared (Transform_1_t2243539419 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m213811340((Transform_1_t2243539419 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4186358450  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4186358450  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* EventNames_t1614635846_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2445298231_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2445298231_gshared (Transform_1_t2243539419 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2445298231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EventNames_t1614635846_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4186358450  Transform_1_EndInvoke_m1124241534_gshared (Transform_1_t2243539419 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4186358450 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1441706075_gshared (Transform_1_t2227997340 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1233934913_gshared (Transform_1_t2227997340 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1233934913((Transform_1_t2227997340 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* EventNames_t1614635846_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2950988704_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2950988704_gshared (Transform_1_t2227997340 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2950988704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EventNames_t1614635846_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<InApp.InAppEventDispatcher/EventNames,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m4192011177_gshared (Transform_1_t2227997340 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,MagicTV.globals.Perspective>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m989996673_gshared (Transform_1_t3570006487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,MagicTV.globals.Perspective>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m138988507_gshared (Transform_1_t3570006487 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m138988507((Transform_1_t3570006487 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,MagicTV.globals.Perspective>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Perspective_t644553802_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2573354554_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2573354554_gshared (Transform_1_t3570006487 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2573354554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Perspective_t644553802_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,MagicTV.globals.Perspective>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m4168969679_gshared (Transform_1_t3570006487 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m770665846_gshared (Transform_1_t382092003 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2393864002_gshared (Transform_1_t382092003 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2393864002((Transform_1_t382092003 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Perspective_t644553802_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3209817965_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3209817965_gshared (Transform_1_t382092003 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3209817965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Perspective_t644553802_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1481075720_gshared (Transform_1_t382092003 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3266439198_gshared (Transform_1_t1040150187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2409664798  Transform_1_Invoke_m3308738074_gshared (Transform_1_t1040150187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3308738074((Transform_1_t1040150187 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2409664798  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2409664798  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Perspective_t644553802_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1966004421_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1966004421_gshared (Transform_1_t1040150187 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1966004421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Perspective_t644553802_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2409664798  Transform_1_EndInvoke_m581719600_gshared (Transform_1_t1040150187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2409664798 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1812047362_gshared (Transform_1_t2801301760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2956207610_gshared (Transform_1_t2801301760 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2956207610((Transform_1_t2801301760 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Perspective_t644553802_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4028150489_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4028150489_gshared (Transform_1_t2801301760 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4028150489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Perspective_t644553802_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.Perspective,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m576248400_gshared (Transform_1_t2801301760 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m742614827_gshared (Transform_1_t2607415263 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2115507185_gshared (Transform_1_t2607415263 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2115507185((Transform_1_t2607415263 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* StateMachine_t367995870_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4021086672_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4021086672_gshared (Transform_1_t2607415263 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4021086672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(StateMachine_t367995870_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3138739193_gshared (Transform_1_t2607415263 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2894542746_gshared (Transform_1_t3991026007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2722867682_gshared (Transform_1_t3991026007 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2722867682((Transform_1_t3991026007 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* StateMachine_t367995870_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m777303873_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m777303873_gshared (Transform_1_t3991026007 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m777303873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(StateMachine_t367995870_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2894026088_gshared (Transform_1_t3991026007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1921763186_gshared (Transform_1_t4084752571 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1845333178  Transform_1_Invoke_m475668934_gshared (Transform_1_t4084752571 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m475668934((Transform_1_t4084752571 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1845333178  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1845333178  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* StateMachine_t367995870_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2115473137_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2115473137_gshared (Transform_1_t4084752571 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2115473137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(StateMachine_t367995870_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1845333178  Transform_1_EndInvoke_m1808254724_gshared (Transform_1_t4084752571 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1845333178 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2572635486_gshared (Transform_1_t2115268468 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1802080090_gshared (Transform_1_t2115268468 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1802080090((Transform_1_t2115268468 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* StateMachine_t367995870_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1916415621_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1916415621_gshared (Transform_1_t2115268468 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1916415621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(StateMachine_t367995870_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2205537520_gshared (Transform_1_t2115268468 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2078929412_gshared (Transform_1_t4258277537 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Char>::Invoke(TKey,TValue)
extern "C"  Il2CppChar Transform_1_Invoke_m41696244_gshared (Transform_1_t4258277537 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m41696244((Transform_1_t4258277537 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppChar (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppChar (*FunctionPointerType) (void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Char>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Table_t1007478513_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3778387871_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3778387871_gshared (Transform_1_t4258277537 * __this, Il2CppChar ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3778387871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Char_t2862622538_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Table_t1007478513_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppChar Transform_1_EndInvoke_m689775638_gshared (Transform_1_t4258277537 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Il2CppChar*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3219188893_gshared (Transform_1_t3147261613 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1759190591_gshared (Transform_1_t3147261613 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1759190591((Transform_1_t3147261613 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Table_t1007478513_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m149405086_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m149405086_gshared (Transform_1_t3147261613 * __this, Il2CppChar ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m149405086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Char_t2862622538_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Table_t1007478513_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3506992363_gshared (Transform_1_t3147261613 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3048230354_gshared (Transform_1_t2392475379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t996820380  Transform_1_Invoke_m903350630_gshared (Transform_1_t2392475379 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m903350630((Transform_1_t2392475379 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t996820380  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t996820380  (*FunctionPointerType) (void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Table_t1007478513_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1119957137_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1119957137_gshared (Transform_1_t2392475379 * __this, Il2CppChar ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1119957137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Char_t2862622538_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Table_t1007478513_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t996820380  Transform_1_EndInvoke_m2372590436_gshared (Transform_1_t2392475379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t996820380 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2141786512_gshared (Transform_1_t2403133512 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,ZXing.Aztec.Internal.Decoder/Table>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m671748524_gshared (Transform_1_t2403133512 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m671748524((Transform_1_t2403133512 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,ZXing.Aztec.Internal.Decoder/Table>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Table_t1007478513_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1796866059_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1796866059_gshared (Transform_1_t2403133512 * __this, Il2CppChar ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1796866059_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Char_t2862622538_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Table_t1007478513_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Char,ZXing.Aztec.Internal.Decoder/Table,ZXing.Aztec.Internal.Decoder/Table>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1337396062_gshared (Transform_1_t2403133512 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2761551185_gshared (Transform_1_t3765932824 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1815730375_gshared (Transform_1_t3765932824 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1815730375((Transform_1_t3765932824 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2847273970_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2847273970_gshared (Transform_1_t3765932824 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2847273970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3480311139_gshared (Transform_1_t3765932824 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4243145684_gshared (Transform_1_t3064208655 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1049882445  Transform_1_Invoke_m3806805284_gshared (Transform_1_t3064208655 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3806805284((Transform_1_t3064208655 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1049882445  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1049882445  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2237662671_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2237662671_gshared (Transform_1_t3064208655 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2237662671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1049882445  Transform_1_EndInvoke_m2328265958_gshared (Transform_1_t3064208655 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1049882445 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3702420454_gshared (Transform_1_t3168164710 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3961126226_gshared (Transform_1_t3168164710 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3961126226((Transform_1_t3168164710 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2680818173_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2680818173_gshared (Transform_1_t3168164710 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2680818173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3427236856_gshared (Transform_1_t3168164710 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3355330134_gshared (Transform_1_t1643741485 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m4168400550_gshared (Transform_1_t1643741485 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4168400550((Transform_1_t1643741485 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1630268421_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1630268421_gshared (Transform_1_t1643741485 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1630268421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3617873444_gshared (Transform_1_t1643741485 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3753371890_gshared (Transform_1_t3958995187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4066860316  Transform_1_Invoke_m2319558726_gshared (Transform_1_t3958995187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2319558726((Transform_1_t3958995187 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4066860316  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4066860316  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m365112689_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m365112689_gshared (Transform_1_t3958995187 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m365112689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4066860316  Transform_1_EndInvoke_m3974312068_gshared (Transform_1_t3958995187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4066860316 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m80961195_gshared (Transform_1_t1045973371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m300848241_gshared (Transform_1_t1045973371 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m300848241((Transform_1_t1045973371 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1162957392_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1162957392_gshared (Transform_1_t1045973371 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1162957392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m822184825_gshared (Transform_1_t1045973371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m224461090_gshared (Transform_1_t4062951242 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m100698134_gshared (Transform_1_t4062951242 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m100698134((Transform_1_t4062951242 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3146712897_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3146712897_gshared (Transform_1_t4062951242 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3146712897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1305038516_gshared (Transform_1_t4062951242 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4158774143_gshared (Transform_1_t1319344378 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2594822233_gshared (Transform_1_t1319344378 * __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2594822233((Transform_1_t1319344378 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t2268465130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m469446020_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m469446020_gshared (Transform_1_t1319344378 * __this, int32_t ___key0, Label_t2268465130  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m469446020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t2268465130_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3970713233_gshared (Transform_1_t1319344378 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4259619376_gshared (Transform_1_t1732246839 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2164509075  Transform_1_Invoke_m731773512_gshared (Transform_1_t1732246839 * __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m731773512((Transform_1_t1732246839 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2164509075  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2164509075  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t2268465130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3723048691_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3723048691_gshared (Transform_1_t1732246839 * __this, int32_t ___key0, Label_t2268465130  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3723048691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t2268465130_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2164509075  Transform_1_EndInvoke_m274917698_gshared (Transform_1_t1732246839 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2164509075 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2649438356_gshared (Transform_1_t721576264 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1899886180_gshared (Transform_1_t721576264 * __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1899886180((Transform_1_t721576264 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t2268465130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1684716815_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1684716815_gshared (Transform_1_t721576264 * __this, int32_t ___key0, Label_t2268465130  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1684716815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t2268465130_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m396067750_gshared (Transform_1_t721576264 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2142264102_gshared (Transform_1_t1836202894 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::Invoke(TKey,TValue)
extern "C"  Label_t2268465130  Transform_1_Invoke_m1313211922_gshared (Transform_1_t1836202894 * __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1313211922((Transform_1_t1836202894 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Label_t2268465130  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Label_t2268465130  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t2268465130  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t2268465130_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m643021501_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m643021501_gshared (Transform_1_t1836202894 * __this, int32_t ___key0, Label_t2268465130  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m643021501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t2268465130_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::EndInvoke(System.IAsyncResult)
extern "C"  Label_t2268465130  Transform_1_EndInvoke_m3909253944_gshared (Transform_1_t1836202894 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Label_t2268465130 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m926571126_gshared (Transform_1_t1809018376 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m7599810_gshared (Transform_1_t1809018376 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m7599810((Transform_1_t1809018376 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* TrackableResultData_t395876724_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2967095661_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2967095661_gshared (Transform_1_t1809018376 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2967095661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(TrackableResultData_t395876724_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3722460296_gshared (Transform_1_t1809018376 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m653979806_gshared (Transform_1_t349332431 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t291920669  Transform_1_Invoke_m1611372186_gshared (Transform_1_t349332431 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1611372186((Transform_1_t349332431 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t291920669  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t291920669  (*FunctionPointerType) (void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* TrackableResultData_t395876724_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1644850757_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1644850757_gshared (Transform_1_t349332431 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1644850757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(TrackableResultData_t395876724_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t291920669  Transform_1_EndInvoke_m2105722288_gshared (Transform_1_t349332431 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t291920669 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3142929099_gshared (Transform_1_t1211250262 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2848377229_gshared (Transform_1_t1211250262 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2848377229((Transform_1_t1211250262 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* TrackableResultData_t395876724_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3453988280_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3453988280_gshared (Transform_1_t1211250262 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3453988280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(TrackableResultData_t395876724_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1624526045_gshared (Transform_1_t1211250262 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m558890822_gshared (Transform_1_t453288486 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(TKey,TValue)
extern "C"  TrackableResultData_t395876724  Transform_1_Invoke_m2064834290_gshared (Transform_1_t453288486 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2064834290((Transform_1_t453288486 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TrackableResultData_t395876724  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TrackableResultData_t395876724  (*FunctionPointerType) (void* __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* TrackableResultData_t395876724_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2739079325_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2739079325_gshared (Transform_1_t453288486 * __this, int32_t ___key0, TrackableResultData_t395876724  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2739079325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(TrackableResultData_t395876724_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C"  TrackableResultData_t395876724  Transform_1_EndInvoke_m2746177112_gshared (Transform_1_t453288486 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TrackableResultData_t395876724 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1897500081_gshared (Transform_1_t1509469889 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1169886311_gshared (Transform_1_t1509469889 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1169886311((Transform_1_t1509469889 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* VirtualButtonData_t459380335_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4266291090_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4266291090_gshared (Transform_1_t1509469889 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4266291090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t459380335_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3729366467_gshared (Transform_1_t1509469889 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1144553620_gshared (Transform_1_t113287555 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t355424280  Transform_1_Invoke_m3560777828_gshared (Transform_1_t113287555 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3560777828((Transform_1_t113287555 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t355424280  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t355424280  (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* VirtualButtonData_t459380335_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m213156111_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m213156111_gshared (Transform_1_t113287555 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m213156111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t459380335_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t355424280  Transform_1_EndInvoke_m3783856550_gshared (Transform_1_t113287555 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t355424280 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m94542918_gshared (Transform_1_t911701775 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3246237938_gshared (Transform_1_t911701775 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3246237938((Transform_1_t911701775 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* VirtualButtonData_t459380335_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1038548381_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1038548381_gshared (Transform_1_t911701775 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1038548381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t459380335_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1132612696_gshared (Transform_1_t911701775 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4008240102_gshared (Transform_1_t217243610 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C"  VirtualButtonData_t459380335  Transform_1_Invoke_m2967248210_gshared (Transform_1_t217243610 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2967248210((Transform_1_t217243610 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef VirtualButtonData_t459380335  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef VirtualButtonData_t459380335  (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* VirtualButtonData_t459380335_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1939307005_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1939307005_gshared (Transform_1_t217243610 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1939307005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t459380335_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::EndInvoke(System.IAsyncResult)
extern "C"  VirtualButtonData_t459380335  Transform_1_EndInvoke_m3785415672_gshared (Transform_1_t217243610 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(VirtualButtonData_t459380335 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,LitJson.ArrayMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m219810509_gshared (Transform_1_t4102664885 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,LitJson.ArrayMetadata>::Invoke(TKey,TValue)
extern "C"  ArrayMetadata_t4058342910  Transform_1_Invoke_m4214344719_gshared (Transform_1_t4102664885 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4214344719((Transform_1_t4102664885 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ArrayMetadata_t4058342910  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ArrayMetadata_t4058342910  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ArrayMetadata_t4058342910  (*FunctionPointerType) (void* __this, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,LitJson.ArrayMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t4058342910_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1715087726_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1715087726_gshared (Transform_1_t4102664885 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1715087726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t4058342910_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,LitJson.ArrayMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  ArrayMetadata_t4058342910  Transform_1_EndInvoke_m3658089755_gshared (Transform_1_t4102664885 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ArrayMetadata_t4058342910 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2715605840_gshared (Transform_1_t1795928589 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2685564712_gshared (Transform_1_t1795928589 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2685564712((Transform_1_t1795928589 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t4058342910_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2229198291_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2229198291_gshared (Transform_1_t1795928589 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2229198291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t4058342910_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3539574882_gshared (Transform_1_t1795928589 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m840461266_gshared (Transform_1_t1876517491 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1832195516  Transform_1_Invoke_m2559593958_gshared (Transform_1_t1876517491 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2559593958((Transform_1_t1876517491 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1832195516  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1832195516  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1832195516  (*FunctionPointerType) (void* __this, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t4058342910_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2558971281_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2558971281_gshared (Transform_1_t1876517491 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2558971281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t4058342910_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1832195516  Transform_1_EndInvoke_m3497182948_gshared (Transform_1_t1876517491 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1832195516 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m59114472_gshared (Transform_1_t4215138346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m4236132948_gshared (Transform_1_t4215138346 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4236132948((Transform_1_t4215138346 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ArrayMetadata_t4058342910  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ArrayMetadata_t4058342910_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4187017395_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4187017395_gshared (Transform_1_t4215138346 * __this, Il2CppObject * ___key0, ArrayMetadata_t4058342910  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4187017395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ArrayMetadata_t4058342910_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ArrayMetadata,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1180262838_gshared (Transform_1_t4215138346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,LitJson.ObjectMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m338076359_gshared (Transform_1_t199362469 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,LitJson.ObjectMetadata>::Invoke(TKey,TValue)
extern "C"  ObjectMetadata_t2009294498  Transform_1_Invoke_m4209723093_gshared (Transform_1_t199362469 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4209723093((Transform_1_t199362469 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ObjectMetadata_t2009294498  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ObjectMetadata_t2009294498  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ObjectMetadata_t2009294498  (*FunctionPointerType) (void* __this, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,LitJson.ObjectMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t2009294498_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2315571124_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2315571124_gshared (Transform_1_t199362469 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2315571124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t2009294498_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,LitJson.ObjectMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  ObjectMetadata_t2009294498  Transform_1_EndInvoke_m2565223061_gshared (Transform_1_t199362469 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ObjectMetadata_t2009294498 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4035874400_gshared (Transform_1_t4236641881 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2696677724_gshared (Transform_1_t4236641881 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2696677724((Transform_1_t4236641881 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t2009294498_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2204751931_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2204751931_gshared (Transform_1_t4236641881 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2204751931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t2009294498_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3742786990_gshared (Transform_1_t4236641881 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m506296114_gshared (Transform_1_t2268182371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4078114400  Transform_1_Invoke_m2629277446_gshared (Transform_1_t2268182371 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2629277446((Transform_1_t2268182371 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4078114400  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4078114400  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4078114400  (*FunctionPointerType) (void* __this, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t2009294498_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2935766321_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2935766321_gshared (Transform_1_t2268182371 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2935766321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t2009294498_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4078114400  Transform_1_EndInvoke_m474515908_gshared (Transform_1_t2268182371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4078114400 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3191375576_gshared (Transform_1_t2360884342 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3315808160_gshared (Transform_1_t2360884342 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3315808160((Transform_1_t2360884342 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectMetadata_t2009294498  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ObjectMetadata_t2009294498_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3668518219_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3668518219_gshared (Transform_1_t2360884342 * __this, Il2CppObject * ___key0, ObjectMetadata_t2009294498  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3668518219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ObjectMetadata_t2009294498_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.ObjectMetadata,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1790338282_gshared (Transform_1_t2360884342 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,LitJson.PropertyMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3989598919_gshared (Transform_1_t585079693 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,LitJson.PropertyMetadata>::Invoke(TKey,TValue)
extern "C"  PropertyMetadata_t4066634616  Transform_1_Invoke_m2937808341_gshared (Transform_1_t585079693 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2937808341((Transform_1_t585079693 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef PropertyMetadata_t4066634616  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef PropertyMetadata_t4066634616  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef PropertyMetadata_t4066634616  (*FunctionPointerType) (void* __this, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,LitJson.PropertyMetadata>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t4066634616_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1508417972_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1508417972_gshared (Transform_1_t585079693 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1508417972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,LitJson.PropertyMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  PropertyMetadata_t4066634616  Transform_1_EndInvoke_m3921458069_gshared (Transform_1_t585079693 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(PropertyMetadata_t4066634616 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3131117494_gshared (Transform_1_t2565018987 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1588227654_gshared (Transform_1_t2565018987 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1588227654((Transform_1_t2565018987 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t4066634616_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1726403749_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1726403749_gshared (Transform_1_t2565018987 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1726403749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m761440900_gshared (Transform_1_t2565018987 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3593258482_gshared (Transform_1_t2653899595 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1840487222  Transform_1_Invoke_m1570214214_gshared (Transform_1_t2653899595 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1570214214((Transform_1_t2653899595 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1840487222  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1840487222  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1840487222  (*FunctionPointerType) (void* __this, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t4066634616_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3109135473_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3109135473_gshared (Transform_1_t2653899595 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3109135473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1840487222  Transform_1_EndInvoke_m3522275204_gshared (Transform_1_t2653899595 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1840487222 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1728519106_gshared (Transform_1_t689261448 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m487085174_gshared (Transform_1_t689261448 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m487085174((Transform_1_t689261448 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, PropertyMetadata_t4066634616  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t4066634616_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3525311137_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3525311137_gshared (Transform_1_t689261448 * __this, Il2CppObject * ___key0, PropertyMetadata_t4066634616  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3525311137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,LitJson.PropertyMetadata,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3469215316_gshared (Transform_1_t689261448 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2503808327_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::Invoke(TKey,TValue)
extern "C"  bool Transform_1_Invoke_m159606869_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m159606869((Transform_1_t1899867829 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2945688884_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2945688884_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2945688884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Transform_1_EndInvoke_m3128041749_gshared (Transform_1_t1899867829 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m199821900_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3999618288_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3999618288((Transform_1_t3174675725 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m960240079_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m960240079_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m960240079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m468964762_gshared (Transform_1_t3174675725 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3170899378_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2545618620  Transform_1_Invoke_m2434466950_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2434466950((Transform_1_t3968687731 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2545618620  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2545618620  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2545618620  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m987788977_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m987788977_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m987788977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2545618620  Transform_1_EndInvoke_m406957124_gshared (Transform_1_t3968687731 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2545618620 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2344546156_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3218823052_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3218823052((Transform_1_t1298918186 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2152369975_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2152369975_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2152369975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m4165556606_gshared (Transform_1_t1298918186 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m641310834_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m3922456586_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3922456586((Transform_1_t2949220623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2253318505_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2253318505_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2253318505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m2079925824_gshared (Transform_1_t2949220623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1506220658_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3222658402  Transform_1_Invoke_m417703622_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m417703622((Transform_1_t125305115 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3222658402  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3222658402  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3222658402  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1076276209_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1076276209_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1076276209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3222658402  Transform_1_EndInvoke_m88547844_gshared (Transform_1_t125305115 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3222658402 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1991062983_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3088902357_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3088902357((Transform_1_t2351452509 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m573338292_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m573338292_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m573338292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3451605141_gshared (Transform_1_t2351452509 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3603041670_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m631029810_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m631029810((Transform_1_t1073463084 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2048389981_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2048389981_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2048389981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1212689688_gshared (Transform_1_t1073463084 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2052388693_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m757436355_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m757436355((Transform_1_t827029284 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m397518190_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3155601639_gshared (Transform_1_t827029284 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1310500508_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1944668977  Transform_1_Invoke_m1166627932_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1166627932((Transform_1_t1020091647 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1944668977  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1944668977  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1944668977  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3524588039_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1944668977  Transform_1_EndInvoke_m865876654_gshared (Transform_1_t1020091647 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1944668977 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m582405827_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3707150041_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3707150041((Transform_1_t3246239041 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m788143672_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3248123921_gshared (Transform_1_t3246239041 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2550069086_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1400282842_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1400282842((Transform_1_t2034936791 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3723202949_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3723202949_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3723202949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1120671088_gshared (Transform_1_t2034936791 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3757893742_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2065771578  Transform_1_Invoke_m4133218506_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4133218506((Transform_1_t2349101755 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2065771578  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2065771578  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2065771578  (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3465937013_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3465937013_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3465937013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2065771578  Transform_1_EndInvoke_m762983808_gshared (Transform_1_t2349101755 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2065771578 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3798645530_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m108556130_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m108556130((Transform_1_t159179252 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1747120577_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1747120577_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1747120577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3253215208_gshared (Transform_1_t159179252 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3911383985_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::Invoke(TKey,TValue)
extern "C"  float Transform_1_Invoke_m3990877099_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3990877099((Transform_1_t280281853 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, float ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1281538314_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1281538314_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___key0, float ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1281538314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Transform_1_EndInvoke_m3473818623_gshared (Transform_1_t280281853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2899387125_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m1200330787_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1200330787((Transform_1_t520022276 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m293588430_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m293588430_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m293588430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m3048842375_gshared (Transform_1_t520022276 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1124898268_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2093487825  Transform_1_Invoke_m3709914396_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3709914396((Transform_1_t861903487 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2093487825  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2093487825  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2093487825  (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1391766215_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1391766215_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1391766215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2093487825  Transform_1_EndInvoke_m3529886190_gshared (Transform_1_t861903487 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2093487825 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1659019043_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2893027961_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2893027961((Transform_1_t2939232033 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m708278744_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m708278744_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m708278744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3858591857_gshared (Transform_1_t2939232033 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2191038339_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::Invoke(TKey,TValue)
extern "C"  uint16_t Transform_1_Invoke_m1314156057_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1314156057((Transform_1_t3088050881 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3928468856_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3928468856_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3928468856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C"  uint16_t Transform_1_EndInvoke_m1430165713_gshared (Transform_1_t3088050881 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint16_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m380669709_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m2187326475_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2187326475((Transform_1_t1958177301 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3824501430_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3824501430_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3824501430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1294747551_gshared (Transform_1_t1958177301 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1812037516_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1919813716  Transform_1_Invoke_m514401132_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m514401132((Transform_1_t2126384403 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1919813716  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1919813716  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1919813716  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3477811991_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3477811991_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3477811991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1919813716  Transform_1_EndInvoke_m3773784478_gshared (Transform_1_t2126384403 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1919813716 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2623329291_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1148525969_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1148525969((Transform_1_t82419762 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2482881520_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2482881520_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2482881520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1450184281_gshared (Transform_1_t82419762 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m809054291_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2664867145_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2664867145((Transform_1_t57564501 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1557263016_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1557263016_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1557263016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,UnityEngine.TextEditor/TextEditOp>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2605908897_gshared (Transform_1_t57564501 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4265585949_gshared (Transform_1_t2588701125 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Transform_1_Invoke_m886732795_gshared (Transform_1_t2588701125 * __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m886732795((Transform_1_t2588701125 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t1751606614  (*FunctionPointerType) (void* __this, ProfileData_t1961690790  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ProfileData_t1961690790_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3768129190_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3768129190_gshared (Transform_1_t2588701125 * __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3768129190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProfileData_t1961690790_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t1751606614  Transform_1_EndInvoke_m1218244015_gshared (Transform_1_t2588701125 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t1751606614 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4165652396_gshared (Transform_1_t572637907 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t4030510692  Transform_1_Invoke_m3224213324_gshared (Transform_1_t572637907 * __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3224213324((Transform_1_t572637907 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4030510692  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t4030510692  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t4030510692  (*FunctionPointerType) (void* __this, ProfileData_t1961690790  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ProfileData_t1961690790_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2621851383_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2621851383_gshared (Transform_1_t572637907 * __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2621851383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProfileData_t1961690790_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t4030510692  Transform_1_EndInvoke_m1839579582_gshared (Transform_1_t572637907 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t4030510692 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
