﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417ErrorCorrection
struct  PDF417ErrorCorrection_t2269626348  : public Il2CppObject
{
public:

public:
};

struct PDF417ErrorCorrection_t2269626348_StaticFields
{
public:
	// System.Int32[][] ZXing.PDF417.Internal.PDF417ErrorCorrection::EC_COEFFICIENTS
	Int32U5BU5DU5BU5D_t1820556512* ___EC_COEFFICIENTS_0;

public:
	inline static int32_t get_offset_of_EC_COEFFICIENTS_0() { return static_cast<int32_t>(offsetof(PDF417ErrorCorrection_t2269626348_StaticFields, ___EC_COEFFICIENTS_0)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_EC_COEFFICIENTS_0() const { return ___EC_COEFFICIENTS_0; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_EC_COEFFICIENTS_0() { return &___EC_COEFFICIENTS_0; }
	inline void set_EC_COEFFICIENTS_0(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___EC_COEFFICIENTS_0 = value;
		Il2CppCodeGenWriteBarrier(&___EC_COEFFICIENTS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
