﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QRCodeEncodeController
struct QRCodeEncodeController_t2317858336;
// QRCodeEncodeController/QREncodeFinished
struct QREncodeFinished_t363463992;
// System.String
struct String_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_QRCodeEncodeController_QREncodeFi363463992.h"
#include "mscorlib_System_String7231557.h"

// System.Void QRCodeEncodeController::.ctor()
extern "C"  void QRCodeEncodeController__ctor_m1347379835 (QRCodeEncodeController_t2317858336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController::add_onQREncodeFinished(QRCodeEncodeController/QREncodeFinished)
extern "C"  void QRCodeEncodeController_add_onQREncodeFinished_m3326460459 (QRCodeEncodeController_t2317858336 * __this, QREncodeFinished_t363463992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController::remove_onQREncodeFinished(QRCodeEncodeController/QREncodeFinished)
extern "C"  void QRCodeEncodeController_remove_onQREncodeFinished_m2266968862 (QRCodeEncodeController_t2317858336 * __this, QREncodeFinished_t363463992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController::Start()
extern "C"  void QRCodeEncodeController_Start_m294517627 (QRCodeEncodeController_t2317858336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController::Update()
extern "C"  void QRCodeEncodeController_Update_m545964018 (QRCodeEncodeController_t2317858336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 QRCodeEncodeController::Encode(System.String)
extern "C"  int32_t QRCodeEncodeController_Encode_m4377621 (QRCodeEncodeController_t2317858336 * __this, String_t* ___valueStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] QRCodeEncodeController::RotateMatrixByClockwise(UnityEngine.Color32[],System.Int32)
extern "C"  Color32U5BU5D_t2960766953* QRCodeEncodeController_RotateMatrixByClockwise_m3582041828 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t2960766953* ___matrix0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] QRCodeEncodeController::RotateMatrixByAnticlockwise(UnityEngine.Color32[],System.Int32)
extern "C"  Color32U5BU5D_t2960766953* QRCodeEncodeController_RotateMatrixByAnticlockwise_m2167520066 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t2960766953* ___matrix0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean QRCodeEncodeController::isContainDigit(System.String)
extern "C"  bool QRCodeEncodeController_isContainDigit_m2250714306 (QRCodeEncodeController_t2317858336 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean QRCodeEncodeController::isContainChar(System.String)
extern "C"  bool QRCodeEncodeController_isContainChar_m1853546973 (QRCodeEncodeController_t2317858336 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean QRCodeEncodeController::bAllDigit(System.String)
extern "C"  bool QRCodeEncodeController_bAllDigit_m3943684687 (QRCodeEncodeController_t2317858336 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController::AddLogoToQRCode()
extern "C"  void QRCodeEncodeController_AddLogoToQRCode_m2031987790 (QRCodeEncodeController_t2317858336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
