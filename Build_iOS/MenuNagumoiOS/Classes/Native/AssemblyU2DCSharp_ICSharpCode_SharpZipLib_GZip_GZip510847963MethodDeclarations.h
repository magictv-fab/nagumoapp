﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.GZip.GZipConstants
struct GZipConstants_t510847963;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.GZip.GZipConstants::.ctor()
extern "C"  void GZipConstants__ctor_m1468082281 (GZipConstants_t510847963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
