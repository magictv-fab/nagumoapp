﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ServerControl
struct ServerControl_t2725829754;
// ServerControl/OnPlayDelegate
struct OnPlayDelegate_t3097072301;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ServerControl_OnPlayDelegate3097072301.h"
#include "mscorlib_System_String7231557.h"

// System.Void ServerControl::.ctor()
extern "C"  void ServerControl__ctor_m326692401 (ServerControl_t2725829754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::.cctor()
extern "C"  void ServerControl__cctor_m1055433628 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::add_OnPlay(ServerControl/OnPlayDelegate)
extern "C"  void ServerControl_add_OnPlay_m3734939319 (ServerControl_t2725829754 * __this, OnPlayDelegate_t3097072301 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::remove_OnPlay(ServerControl/OnPlayDelegate)
extern "C"  void ServerControl_remove_OnPlay_m1251324112 (ServerControl_t2725829754 * __this, OnPlayDelegate_t3097072301 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::Start()
extern "C"  void ServerControl_Start_m3568797489 (ServerControl_t2725829754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::Play()
extern "C"  void ServerControl_Play_m1822966567 (ServerControl_t2725829754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ServerControl::IPlay()
extern "C"  Il2CppObject * ServerControl_IPlay_m3410703252 (ServerControl_t2725829754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ServerControl::IPlayRoleta()
extern "C"  Il2CppObject * ServerControl_IPlayRoleta_m2582223383 (ServerControl_t2725829754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ServerControl::ConectandoLetreiro(System.String)
extern "C"  Il2CppObject * ServerControl_ConectandoLetreiro_m2716099507 (ServerControl_t2725829754 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::Update()
extern "C"  void ServerControl_Update_m3264391932 (ServerControl_t2725829754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl::PopUp(System.String)
extern "C"  void ServerControl_PopUp_m4087236039 (ServerControl_t2725829754 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ServerControl::Base64Encode(System.String)
extern "C"  String_t* ServerControl_Base64Encode_m2122611309 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
