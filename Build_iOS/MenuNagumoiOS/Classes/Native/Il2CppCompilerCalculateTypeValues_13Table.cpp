﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm4089761650.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFractionDigi2720582299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroup2984073803.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupBase1407981116.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupRef3207684034.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons3091667497.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaImport3119143443.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInclude2975206580.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo4089849692.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKey4125466091.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKeyref3169284162.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaLengthFacet4184726269.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxExclusive2184141193.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxInclusive3095936763.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxLengthFace167394267.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinExclusiveF204218871.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinInclusive1116014441.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinLengthFace824310317.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNotation4279727664.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNumericFacet927300696.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject3280570797.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectCollec3489199211.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectEnumerat14884241.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable1281427547.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_X982183004.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3890186420.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle_Emp3770983499.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaPatternFacet2872935861.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaRedefine1921142748.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet4125473774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSequence4050883055.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleContent982844211.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten3054769670.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten3536703251.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType3060492794.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1889388569.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi1048666168.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe2016622060.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUn3206834991.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaTotalDigitsF1636162491.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType4090188264.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUnique3463410239.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse4125476115.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity1280069984.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationEx2573697378.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpaceFac869890662.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath2998747273.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType944542020.h"
#include "System_Xml_System_Xml_Schema_ValidationHandler3187238407.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil4090213040.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaReader3368959793.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFlag87720602.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode3316180404.h"
#include "System_Xml_System_Xml_Serialization_CodeIdentifier182900840.h"
#include "System_Xml_System_Xml_Serialization_SchemaTypes1835162914.h"
#include "System_Xml_System_Xml_Serialization_TypeData1652947702.h"
#include "System_Xml_System_Xml_Serialization_TypeTranslator2984757990.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyAttribut1487319943.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA2740664679.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAt1769351841.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2146545409.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut927884470.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2315070405.h"
#include "System_Xml_System_Xml_Serialization_XmlNamespaceDe3537105033.h"
#include "System_Xml_System_Xml_Serialization_XmlRootAttribu2353874421.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerNa142497835.h"
#include "System_Xml_System_Xml_Serialization_XmlTextAttribu1483548170.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler3074502249.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle4231404781.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3379220348.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3988332413.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1676616792.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar435480436.h"
#include "Mono_Security_U3CModuleU3E86524790.h"
#include "Mono_Security_Locale2281372282.h"
#include "Mono_Security_Mono_Math_BigInteger3334373498.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign162809939.h"
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing3697743089.h"
#include "Mono_Security_Mono_Math_BigInteger_Kernel1609132371.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor3802548591.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTests194676186.h"
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGener1565729053.h"
#include "Mono_Security_Mono_Math_Prime_Generator_Sequential3892164356.h"
#include "Mono_Security_Mono_Security_ASN13752917377.h"
#include "Mono_Security_Mono_Security_ASN1Convert1533901092.h"
#include "Mono_Security_Mono_Security_BitConverterLE159646808.h"
#include "Mono_Security_Mono_Security_PKCS71481122354.h"
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo3081670593.h"
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData2709367816.h"
#include "Mono_Security_Mono_Security_Cryptography_ARC4Manag1354014707.h"
#include "Mono_Security_Mono_Security_Cryptography_CryptoCon3338259528.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilder373726640.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2227518833.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2Managed915988216.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4227518835.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4Managed106641590.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS13260516764.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS83260516771.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (XmlSchemaForm_t4089761650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1300[4] = 
{
	XmlSchemaForm_t4089761650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (XmlSchemaFractionDigitsFacet_t2720582299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (XmlSchemaGroup_t2984073803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1302[4] = 
{
	XmlSchemaGroup_t2984073803::get_offset_of_name_16(),
	XmlSchemaGroup_t2984073803::get_offset_of_particle_17(),
	XmlSchemaGroup_t2984073803::get_offset_of_qualifiedName_18(),
	XmlSchemaGroup_t2984073803::get_offset_of_isCircularDefinition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (XmlSchemaGroupBase_t1407981116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1303[1] = 
{
	XmlSchemaGroupBase_t1407981116::get_offset_of_compiledItems_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (XmlSchemaGroupRef_t3207684034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[4] = 
{
	XmlSchemaGroupRef_t3207684034::get_offset_of_schema_27(),
	XmlSchemaGroupRef_t3207684034::get_offset_of_refName_28(),
	XmlSchemaGroupRef_t3207684034::get_offset_of_referencedGroup_29(),
	XmlSchemaGroupRef_t3207684034::get_offset_of_busy_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (XmlSchemaIdentityConstraint_t3091667497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1305[5] = 
{
	XmlSchemaIdentityConstraint_t3091667497::get_offset_of_fields_16(),
	XmlSchemaIdentityConstraint_t3091667497::get_offset_of_name_17(),
	XmlSchemaIdentityConstraint_t3091667497::get_offset_of_qName_18(),
	XmlSchemaIdentityConstraint_t3091667497::get_offset_of_selector_19(),
	XmlSchemaIdentityConstraint_t3091667497::get_offset_of_compiledSelector_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (XmlSchemaImport_t3119143443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1306[2] = 
{
	XmlSchemaImport_t3119143443::get_offset_of_annotation_16(),
	XmlSchemaImport_t3119143443::get_offset_of_nameSpace_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (XmlSchemaInclude_t2975206580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[1] = 
{
	XmlSchemaInclude_t2975206580::get_offset_of_annotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (XmlSchemaInfo_t4089849692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[7] = 
{
	XmlSchemaInfo_t4089849692::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t4089849692::get_offset_of_isNil_1(),
	XmlSchemaInfo_t4089849692::get_offset_of_memberType_2(),
	XmlSchemaInfo_t4089849692::get_offset_of_attr_3(),
	XmlSchemaInfo_t4089849692::get_offset_of_elem_4(),
	XmlSchemaInfo_t4089849692::get_offset_of_type_5(),
	XmlSchemaInfo_t4089849692::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (XmlSchemaKey_t4125466091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (XmlSchemaKeyref_t3169284162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1310[2] = 
{
	XmlSchemaKeyref_t3169284162::get_offset_of_refer_21(),
	XmlSchemaKeyref_t3169284162::get_offset_of_target_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (XmlSchemaLengthFacet_t4184726269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (XmlSchemaMaxExclusiveFacet_t2184141193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (XmlSchemaMaxInclusiveFacet_t3095936763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (XmlSchemaMaxLengthFacet_t167394267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (XmlSchemaMinExclusiveFacet_t204218871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (XmlSchemaMinInclusiveFacet_t1116014441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (XmlSchemaMinLengthFacet_t824310317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (XmlSchemaNotation_t4279727664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1318[4] = 
{
	XmlSchemaNotation_t4279727664::get_offset_of_name_16(),
	XmlSchemaNotation_t4279727664::get_offset_of_pub_17(),
	XmlSchemaNotation_t4279727664::get_offset_of_system_18(),
	XmlSchemaNotation_t4279727664::get_offset_of_qualifiedName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (XmlSchemaNumericFacet_t927300696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (XmlSchemaObject_t3280570797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1320[13] = 
{
	XmlSchemaObject_t3280570797::get_offset_of_lineNumber_0(),
	XmlSchemaObject_t3280570797::get_offset_of_linePosition_1(),
	XmlSchemaObject_t3280570797::get_offset_of_sourceUri_2(),
	XmlSchemaObject_t3280570797::get_offset_of_namespaces_3(),
	XmlSchemaObject_t3280570797::get_offset_of_unhandledAttributeList_4(),
	XmlSchemaObject_t3280570797::get_offset_of_isCompiled_5(),
	XmlSchemaObject_t3280570797::get_offset_of_errorCount_6(),
	XmlSchemaObject_t3280570797::get_offset_of_CompilationId_7(),
	XmlSchemaObject_t3280570797::get_offset_of_ValidationId_8(),
	XmlSchemaObject_t3280570797::get_offset_of_isRedefineChild_9(),
	XmlSchemaObject_t3280570797::get_offset_of_isRedefinedComponent_10(),
	XmlSchemaObject_t3280570797::get_offset_of_redefinedObject_11(),
	XmlSchemaObject_t3280570797::get_offset_of_parent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (XmlSchemaObjectCollection_t3489199211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (XmlSchemaObjectEnumerator_t14884241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1322[1] = 
{
	XmlSchemaObjectEnumerator_t14884241::get_offset_of_ienum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (XmlSchemaObjectTable_t1281427547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1323[1] = 
{
	XmlSchemaObjectTable_t1281427547::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (XmlSchemaObjectTableEnumerator_t982183004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1324[2] = 
{
	XmlSchemaObjectTableEnumerator_t982183004::get_offset_of_xenum_0(),
	XmlSchemaObjectTableEnumerator_t982183004::get_offset_of_tmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (XmlSchemaParticle_t3890186420), -1, sizeof(XmlSchemaParticle_t3890186420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1325[11] = 
{
	XmlSchemaParticle_t3890186420::get_offset_of_minOccurs_16(),
	XmlSchemaParticle_t3890186420::get_offset_of_maxOccurs_17(),
	XmlSchemaParticle_t3890186420::get_offset_of_minstr_18(),
	XmlSchemaParticle_t3890186420::get_offset_of_maxstr_19(),
	XmlSchemaParticle_t3890186420_StaticFields::get_offset_of_empty_20(),
	XmlSchemaParticle_t3890186420::get_offset_of_validatedMinOccurs_21(),
	XmlSchemaParticle_t3890186420::get_offset_of_validatedMaxOccurs_22(),
	XmlSchemaParticle_t3890186420::get_offset_of_recursionDepth_23(),
	XmlSchemaParticle_t3890186420::get_offset_of_minEffectiveTotalRange_24(),
	XmlSchemaParticle_t3890186420::get_offset_of_parentIsGroupDefinition_25(),
	XmlSchemaParticle_t3890186420::get_offset_of_OptimizedParticle_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (EmptyParticle_t3770983499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (XmlSchemaPatternFacet_t2872935861), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (XmlSchemaRedefine_t1921142748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1328[4] = 
{
	XmlSchemaRedefine_t1921142748::get_offset_of_attributeGroups_16(),
	XmlSchemaRedefine_t1921142748::get_offset_of_groups_17(),
	XmlSchemaRedefine_t1921142748::get_offset_of_items_18(),
	XmlSchemaRedefine_t1921142748::get_offset_of_schemaTypes_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (XmlSchemaSet_t4125473774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1329[12] = 
{
	XmlSchemaSet_t4125473774::get_offset_of_nameTable_0(),
	XmlSchemaSet_t4125473774::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t4125473774::get_offset_of_schemas_2(),
	XmlSchemaSet_t4125473774::get_offset_of_attributes_3(),
	XmlSchemaSet_t4125473774::get_offset_of_elements_4(),
	XmlSchemaSet_t4125473774::get_offset_of_types_5(),
	XmlSchemaSet_t4125473774::get_offset_of_idCollection_6(),
	XmlSchemaSet_t4125473774::get_offset_of_namedIdentities_7(),
	XmlSchemaSet_t4125473774::get_offset_of_settings_8(),
	XmlSchemaSet_t4125473774::get_offset_of_isCompiled_9(),
	XmlSchemaSet_t4125473774::get_offset_of_CompilationId_10(),
	XmlSchemaSet_t4125473774::get_offset_of_ValidationEventHandler_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (XmlSchemaSequence_t4050883055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1330[1] = 
{
	XmlSchemaSequence_t4050883055::get_offset_of_items_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (XmlSchemaSimpleContent_t982844211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1331[1] = 
{
	XmlSchemaSimpleContent_t982844211::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (XmlSchemaSimpleContentExtension_t3054769670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1332[3] = 
{
	XmlSchemaSimpleContentExtension_t3054769670::get_offset_of_any_17(),
	XmlSchemaSimpleContentExtension_t3054769670::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentExtension_t3054769670::get_offset_of_baseTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (XmlSchemaSimpleContentRestriction_t3536703251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[5] = 
{
	XmlSchemaSimpleContentRestriction_t3536703251::get_offset_of_any_17(),
	XmlSchemaSimpleContentRestriction_t3536703251::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentRestriction_t3536703251::get_offset_of_baseType_19(),
	XmlSchemaSimpleContentRestriction_t3536703251::get_offset_of_baseTypeName_20(),
	XmlSchemaSimpleContentRestriction_t3536703251::get_offset_of_facets_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (XmlSchemaSimpleType_t3060492794), -1, sizeof(XmlSchemaSimpleType_t3060492794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1334[54] = 
{
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_schemaLocationType_28(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_content_29(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_islocal_30(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_recursed_31(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_variety_32(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsAnySimpleType_33(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsString_34(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsBoolean_35(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDecimal_36(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsFloat_37(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDouble_38(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDuration_39(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDateTime_40(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsTime_41(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDate_42(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGYearMonth_43(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGYear_44(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGMonthDay_45(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGDay_46(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGMonth_47(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsHexBinary_48(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsBase64Binary_49(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsAnyUri_50(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsQName_51(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNotation_52(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNormalizedString_53(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsToken_54(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsLanguage_55(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNMToken_56(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNMTokens_57(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsName_58(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNCName_59(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsID_60(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsIDRef_61(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsIDRefs_62(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsEntity_63(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsEntities_64(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsInteger_65(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNonPositiveInteger_66(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNegativeInteger_67(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsLong_68(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsInt_69(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsShort_70(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsByte_71(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNonNegativeInteger_72(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedLong_73(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedInt_74(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedShort_75(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedByte_76(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsPositiveInteger_77(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtUntypedAtomic_78(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtAnyAtomicType_79(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtYearMonthDuration_80(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtDayTimeDuration_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (XmlSchemaSimpleTypeContent_t1889388569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1335[1] = 
{
	XmlSchemaSimpleTypeContent_t1889388569::get_offset_of_OwnerType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (XmlSchemaSimpleTypeList_t1048666168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[4] = 
{
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_itemType_17(),
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_itemTypeName_18(),
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_validatedListItemType_19(),
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_validatedListItemSchemaType_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (XmlSchemaSimpleTypeRestriction_t2016622060), -1, sizeof(XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1337[18] = 
{
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_baseType_17(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_baseTypeName_18(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_facets_19(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_enumarationFacetValues_20(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_patternFacetValues_21(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_rexPatterns_22(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_lengthFacet_23(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_maxLengthFacet_24(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_minLengthFacet_25(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_fractionDigitsFacet_26(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_totalDigitsFacet_27(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_maxInclusiveFacet_28(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_maxExclusiveFacet_29(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_minInclusiveFacet_30(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_minExclusiveFacet_31(),
	XmlSchemaSimpleTypeRestriction_t2016622060::get_offset_of_fixedFacets_32(),
	XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields::get_offset_of_lengthStyle_33(),
	XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields::get_offset_of_listFacets_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (XmlSchemaSimpleTypeUnion_t3206834991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1338[4] = 
{
	XmlSchemaSimpleTypeUnion_t3206834991::get_offset_of_baseTypes_17(),
	XmlSchemaSimpleTypeUnion_t3206834991::get_offset_of_memberTypes_18(),
	XmlSchemaSimpleTypeUnion_t3206834991::get_offset_of_validatedTypes_19(),
	XmlSchemaSimpleTypeUnion_t3206834991::get_offset_of_validatedSchemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (XmlSchemaTotalDigitsFacet_t1636162491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (XmlSchemaType_t4090188264), -1, sizeof(XmlSchemaType_t4090188264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1340[12] = 
{
	XmlSchemaType_t4090188264::get_offset_of_final_16(),
	XmlSchemaType_t4090188264::get_offset_of_isMixed_17(),
	XmlSchemaType_t4090188264::get_offset_of_name_18(),
	XmlSchemaType_t4090188264::get_offset_of_recursed_19(),
	XmlSchemaType_t4090188264::get_offset_of_BaseSchemaTypeName_20(),
	XmlSchemaType_t4090188264::get_offset_of_BaseXmlSchemaTypeInternal_21(),
	XmlSchemaType_t4090188264::get_offset_of_DatatypeInternal_22(),
	XmlSchemaType_t4090188264::get_offset_of_resolvedDerivedBy_23(),
	XmlSchemaType_t4090188264::get_offset_of_finalResolved_24(),
	XmlSchemaType_t4090188264::get_offset_of_QNameInternal_25(),
	XmlSchemaType_t4090188264_StaticFields::get_offset_of_U3CU3Ef__switchU24map42_26(),
	XmlSchemaType_t4090188264_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (XmlSchemaUnique_t3463410239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (XmlSchemaUse_t4125476115)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1342[5] = 
{
	XmlSchemaUse_t4125476115::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (XmlSchemaValidity_t1280069984)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1343[4] = 
{
	XmlSchemaValidity_t1280069984::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (XmlSchemaValidationException_t2573697378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (XmlSchemaWhiteSpaceFacet_t869890662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (XmlSchemaXPath_t2998747273), -1, sizeof(XmlSchemaXPath_t2998747273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1346[6] = 
{
	XmlSchemaXPath_t2998747273::get_offset_of_xpath_16(),
	XmlSchemaXPath_t2998747273::get_offset_of_nsmgr_17(),
	XmlSchemaXPath_t2998747273::get_offset_of_isSelector_18(),
	XmlSchemaXPath_t2998747273::get_offset_of_compiledExpression_19(),
	XmlSchemaXPath_t2998747273::get_offset_of_currentPath_20(),
	XmlSchemaXPath_t2998747273_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (XmlSeverityType_t944542020)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1347[3] = 
{
	XmlSeverityType_t944542020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (ValidationHandler_t3187238407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (XmlSchemaUtil_t4090213040), -1, sizeof(XmlSchemaUtil_t4090213040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1349[10] = 
{
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_StrictMsCompliant_3(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_U3CU3Ef__switchU24map4B_4(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_U3CU3Ef__switchU24map4C_5(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_U3CU3Ef__switchU24map4D_6(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_U3CU3Ef__switchU24map4E_7(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_U3CU3Ef__switchU24map4F_8(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_U3CU3Ef__switchU24map50_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (XmlSchemaReader_t3368959793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1350[3] = 
{
	XmlSchemaReader_t3368959793::get_offset_of_reader_2(),
	XmlSchemaReader_t3368959793::get_offset_of_handler_3(),
	XmlSchemaReader_t3368959793::get_offset_of_hasLineInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (XmlSchemaValidationFlags_t87720602)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1351[7] = 
{
	XmlSchemaValidationFlags_t87720602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (XmlTypeCode_t3316180404)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1352[56] = 
{
	XmlTypeCode_t3316180404::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (CodeIdentifier_t182900840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (SchemaTypes_t1835162914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1355[9] = 
{
	SchemaTypes_t1835162914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (TypeData_t1652947702), -1, sizeof(TypeData_t1652947702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1356[12] = 
{
	TypeData_t1652947702::get_offset_of_type_0(),
	TypeData_t1652947702::get_offset_of_elementName_1(),
	TypeData_t1652947702::get_offset_of_sType_2(),
	TypeData_t1652947702::get_offset_of_listItemType_3(),
	TypeData_t1652947702::get_offset_of_typeName_4(),
	TypeData_t1652947702::get_offset_of_fullTypeName_5(),
	TypeData_t1652947702::get_offset_of_listItemTypeData_6(),
	TypeData_t1652947702::get_offset_of_mappedType_7(),
	TypeData_t1652947702::get_offset_of_facet_8(),
	TypeData_t1652947702::get_offset_of_hasPublicConstructor_9(),
	TypeData_t1652947702::get_offset_of_nullableOverride_10(),
	TypeData_t1652947702_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (TypeTranslator_t2984757990), -1, sizeof(TypeTranslator_t2984757990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1357[4] = 
{
	TypeTranslator_t2984757990_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t2984757990_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t2984757990_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t2984757990_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (XmlAnyAttributeAttribute_t1487319943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (XmlAnyElementAttribute_t2740664679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1359[1] = 
{
	XmlAnyElementAttribute_t2740664679::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (XmlAttributeAttribute_t1769351841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1360[2] = 
{
	XmlAttributeAttribute_t1769351841::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t1769351841::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (XmlElementAttribute_t2146545409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1361[3] = 
{
	XmlElementAttribute_t2146545409::get_offset_of_elementName_0(),
	XmlElementAttribute_t2146545409::get_offset_of_type_1(),
	XmlElementAttribute_t2146545409::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (XmlEnumAttribute_t927884470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1362[1] = 
{
	XmlEnumAttribute_t927884470::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (XmlIgnoreAttribute_t2315070405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (XmlNamespaceDeclarationsAttribute_t3537105033), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (XmlRootAttribute_t2353874421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1365[3] = 
{
	XmlRootAttribute_t2353874421::get_offset_of_elementName_0(),
	XmlRootAttribute_t2353874421::get_offset_of_isNullable_1(),
	XmlRootAttribute_t2353874421::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (XmlSerializerNamespaces_t142497835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1366[1] = 
{
	XmlSerializerNamespaces_t142497835::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (XmlTextAttribute_t1483548170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (XmlNodeChangedEventHandler_t3074502249), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (ValidationEventHandler_t4231404781), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238934), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1370[8] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D36_0(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D37_1(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D38_2(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D39_3(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D40_4(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D41_5(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D43_6(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D44_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (U24ArrayTypeU2412_t3379220349)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220349_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (U24ArrayTypeU248_t3988332414)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3988332414_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (U24ArrayTypeU24256_t1676616793)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616793_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (U24ArrayTypeU241280_t435480436)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t435480436_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (U3CModuleU3E_t86524792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (Locale_t2281372283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (BigInteger_t3334373499), -1, sizeof(BigInteger_t3334373499_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1377[4] = 
{
	BigInteger_t3334373499::get_offset_of_length_0(),
	BigInteger_t3334373499::get_offset_of_data_1(),
	BigInteger_t3334373499_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t3334373499_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (Sign_t162809940)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1378[4] = 
{
	Sign_t162809940::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (ModulusRing_t3697743090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1379[2] = 
{
	ModulusRing_t3697743090::get_offset_of_mod_0(),
	ModulusRing_t3697743090::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (Kernel_t1609132372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (ConfidenceFactor_t3802548592)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1381[7] = 
{
	ConfidenceFactor_t3802548592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (PrimalityTests_t194676187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (PrimeGeneratorBase_t1565729054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (SequentialSearchPrimeGeneratorBase_t3892164357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (ASN1_t3752917378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1385[3] = 
{
	ASN1_t3752917378::get_offset_of_m_nTag_0(),
	ASN1_t3752917378::get_offset_of_m_aValue_1(),
	ASN1_t3752917378::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (ASN1Convert_t1533901093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (BitConverterLE_t159646809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (PKCS7_t1481122355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (ContentInfo_t3081670594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1389[2] = 
{
	ContentInfo_t3081670594::get_offset_of_contentType_0(),
	ContentInfo_t3081670594::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (EncryptedData_t2709367817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1390[4] = 
{
	EncryptedData_t2709367817::get_offset_of__version_0(),
	EncryptedData_t2709367817::get_offset_of__content_1(),
	EncryptedData_t2709367817::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2709367817::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (ARC4Managed_t1354014707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1391[5] = 
{
	ARC4Managed_t1354014707::get_offset_of_key_12(),
	ARC4Managed_t1354014707::get_offset_of_state_13(),
	ARC4Managed_t1354014707::get_offset_of_x_14(),
	ARC4Managed_t1354014707::get_offset_of_y_15(),
	ARC4Managed_t1354014707::get_offset_of_m_disposed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (CryptoConvert_t3338259529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (KeyBuilder_t373726641), -1, sizeof(KeyBuilder_t373726641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1393[1] = 
{
	KeyBuilder_t373726641_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (MD2_t227518833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (MD2Managed_t915988216), -1, sizeof(MD2Managed_t915988216_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1395[6] = 
{
	MD2Managed_t915988216::get_offset_of_state_4(),
	MD2Managed_t915988216::get_offset_of_checksum_5(),
	MD2Managed_t915988216::get_offset_of_buffer_6(),
	MD2Managed_t915988216::get_offset_of_count_7(),
	MD2Managed_t915988216::get_offset_of_x_8(),
	MD2Managed_t915988216_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (MD4_t227518835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (MD4Managed_t106641590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1397[5] = 
{
	MD4Managed_t106641590::get_offset_of_state_4(),
	MD4Managed_t106641590::get_offset_of_buffer_5(),
	MD4Managed_t106641590::get_offset_of_count_6(),
	MD4Managed_t106641590::get_offset_of_x_7(),
	MD4Managed_t106641590::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (PKCS1_t3260516765), -1, sizeof(PKCS1_t3260516765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1398[4] = 
{
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (PKCS8_t3260516772), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
