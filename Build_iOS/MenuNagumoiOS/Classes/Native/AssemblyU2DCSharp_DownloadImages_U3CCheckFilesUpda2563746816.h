﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// DownloadImages
struct DownloadImages_t3644489024;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadImages/<CheckFilesUpdate>c__Iterator19
struct  U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816  : public Il2CppObject
{
public:
	// UnityEngine.WWW DownloadImages/<CheckFilesUpdate>c__Iterator19::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_0;
	// System.String[] DownloadImages/<CheckFilesUpdate>c__Iterator19::<paths>__1
	StringU5BU5D_t4054002952* ___U3CpathsU3E__1_1;
	// System.Collections.Generic.List`1<System.String> DownloadImages/<CheckFilesUpdate>c__Iterator19::<pathsList>__2
	List_1_t1375417109 * ___U3CpathsListU3E__2_2;
	// System.String[] DownloadImages/<CheckFilesUpdate>c__Iterator19::<$s_17>__3
	StringU5BU5D_t4054002952* ___U3CU24s_17U3E__3_3;
	// System.Int32 DownloadImages/<CheckFilesUpdate>c__Iterator19::<$s_18>__4
	int32_t ___U3CU24s_18U3E__4_4;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<path>__5
	String_t* ___U3CpathU3E__5_5;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<folder>__6
	String_t* ___U3CfolderU3E__6_6;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<fileName>__7
	String_t* ___U3CfileNameU3E__7_7;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<pathDirectory>__8
	String_t* ___U3CpathDirectoryU3E__8_8;
	// System.String[] DownloadImages/<CheckFilesUpdate>c__Iterator19::<$s_19>__9
	StringU5BU5D_t4054002952* ___U3CU24s_19U3E__9_9;
	// System.Int32 DownloadImages/<CheckFilesUpdate>c__Iterator19::<$s_20>__10
	int32_t ___U3CU24s_20U3E__10_10;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<localFile>__11
	String_t* ___U3ClocalFileU3E__11_11;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<lFile>__12
	String_t* ___U3ClFileU3E__12_12;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<lFileDir>__13
	String_t* ___U3ClFileDirU3E__13_13;
	// System.String DownloadImages/<CheckFilesUpdate>c__Iterator19::<lDirFile>__14
	String_t* ___U3ClDirFileU3E__14_14;
	// System.Int32 DownloadImages/<CheckFilesUpdate>c__Iterator19::$PC
	int32_t ___U24PC_15;
	// System.Object DownloadImages/<CheckFilesUpdate>c__Iterator19::$current
	Il2CppObject * ___U24current_16;
	// DownloadImages DownloadImages/<CheckFilesUpdate>c__Iterator19::<>f__this
	DownloadImages_t3644489024 * ___U3CU3Ef__this_17;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CwwwU3E__0_0)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CpathsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CpathsU3E__1_1)); }
	inline StringU5BU5D_t4054002952* get_U3CpathsU3E__1_1() const { return ___U3CpathsU3E__1_1; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CpathsU3E__1_1() { return &___U3CpathsU3E__1_1; }
	inline void set_U3CpathsU3E__1_1(StringU5BU5D_t4054002952* value)
	{
		___U3CpathsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathsU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpathsListU3E__2_2() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CpathsListU3E__2_2)); }
	inline List_1_t1375417109 * get_U3CpathsListU3E__2_2() const { return ___U3CpathsListU3E__2_2; }
	inline List_1_t1375417109 ** get_address_of_U3CpathsListU3E__2_2() { return &___U3CpathsListU3E__2_2; }
	inline void set_U3CpathsListU3E__2_2(List_1_t1375417109 * value)
	{
		___U3CpathsListU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathsListU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_17U3E__3_3() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CU24s_17U3E__3_3)); }
	inline StringU5BU5D_t4054002952* get_U3CU24s_17U3E__3_3() const { return ___U3CU24s_17U3E__3_3; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU24s_17U3E__3_3() { return &___U3CU24s_17U3E__3_3; }
	inline void set_U3CU24s_17U3E__3_3(StringU5BU5D_t4054002952* value)
	{
		___U3CU24s_17U3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_17U3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_18U3E__4_4() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CU24s_18U3E__4_4)); }
	inline int32_t get_U3CU24s_18U3E__4_4() const { return ___U3CU24s_18U3E__4_4; }
	inline int32_t* get_address_of_U3CU24s_18U3E__4_4() { return &___U3CU24s_18U3E__4_4; }
	inline void set_U3CU24s_18U3E__4_4(int32_t value)
	{
		___U3CU24s_18U3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CpathU3E__5_5() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CpathU3E__5_5)); }
	inline String_t* get_U3CpathU3E__5_5() const { return ___U3CpathU3E__5_5; }
	inline String_t** get_address_of_U3CpathU3E__5_5() { return &___U3CpathU3E__5_5; }
	inline void set_U3CpathU3E__5_5(String_t* value)
	{
		___U3CpathU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CfolderU3E__6_6() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CfolderU3E__6_6)); }
	inline String_t* get_U3CfolderU3E__6_6() const { return ___U3CfolderU3E__6_6; }
	inline String_t** get_address_of_U3CfolderU3E__6_6() { return &___U3CfolderU3E__6_6; }
	inline void set_U3CfolderU3E__6_6(String_t* value)
	{
		___U3CfolderU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfolderU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3E__7_7() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CfileNameU3E__7_7)); }
	inline String_t* get_U3CfileNameU3E__7_7() const { return ___U3CfileNameU3E__7_7; }
	inline String_t** get_address_of_U3CfileNameU3E__7_7() { return &___U3CfileNameU3E__7_7; }
	inline void set_U3CfileNameU3E__7_7(String_t* value)
	{
		___U3CfileNameU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileNameU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CpathDirectoryU3E__8_8() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CpathDirectoryU3E__8_8)); }
	inline String_t* get_U3CpathDirectoryU3E__8_8() const { return ___U3CpathDirectoryU3E__8_8; }
	inline String_t** get_address_of_U3CpathDirectoryU3E__8_8() { return &___U3CpathDirectoryU3E__8_8; }
	inline void set_U3CpathDirectoryU3E__8_8(String_t* value)
	{
		___U3CpathDirectoryU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathDirectoryU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3CU24s_19U3E__9_9() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CU24s_19U3E__9_9)); }
	inline StringU5BU5D_t4054002952* get_U3CU24s_19U3E__9_9() const { return ___U3CU24s_19U3E__9_9; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU24s_19U3E__9_9() { return &___U3CU24s_19U3E__9_9; }
	inline void set_U3CU24s_19U3E__9_9(StringU5BU5D_t4054002952* value)
	{
		___U3CU24s_19U3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_19U3E__9_9, value);
	}

	inline static int32_t get_offset_of_U3CU24s_20U3E__10_10() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CU24s_20U3E__10_10)); }
	inline int32_t get_U3CU24s_20U3E__10_10() const { return ___U3CU24s_20U3E__10_10; }
	inline int32_t* get_address_of_U3CU24s_20U3E__10_10() { return &___U3CU24s_20U3E__10_10; }
	inline void set_U3CU24s_20U3E__10_10(int32_t value)
	{
		___U3CU24s_20U3E__10_10 = value;
	}

	inline static int32_t get_offset_of_U3ClocalFileU3E__11_11() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3ClocalFileU3E__11_11)); }
	inline String_t* get_U3ClocalFileU3E__11_11() const { return ___U3ClocalFileU3E__11_11; }
	inline String_t** get_address_of_U3ClocalFileU3E__11_11() { return &___U3ClocalFileU3E__11_11; }
	inline void set_U3ClocalFileU3E__11_11(String_t* value)
	{
		___U3ClocalFileU3E__11_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalFileU3E__11_11, value);
	}

	inline static int32_t get_offset_of_U3ClFileU3E__12_12() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3ClFileU3E__12_12)); }
	inline String_t* get_U3ClFileU3E__12_12() const { return ___U3ClFileU3E__12_12; }
	inline String_t** get_address_of_U3ClFileU3E__12_12() { return &___U3ClFileU3E__12_12; }
	inline void set_U3ClFileU3E__12_12(String_t* value)
	{
		___U3ClFileU3E__12_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClFileU3E__12_12, value);
	}

	inline static int32_t get_offset_of_U3ClFileDirU3E__13_13() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3ClFileDirU3E__13_13)); }
	inline String_t* get_U3ClFileDirU3E__13_13() const { return ___U3ClFileDirU3E__13_13; }
	inline String_t** get_address_of_U3ClFileDirU3E__13_13() { return &___U3ClFileDirU3E__13_13; }
	inline void set_U3ClFileDirU3E__13_13(String_t* value)
	{
		___U3ClFileDirU3E__13_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClFileDirU3E__13_13, value);
	}

	inline static int32_t get_offset_of_U3ClDirFileU3E__14_14() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3ClDirFileU3E__14_14)); }
	inline String_t* get_U3ClDirFileU3E__14_14() const { return ___U3ClDirFileU3E__14_14; }
	inline String_t** get_address_of_U3ClDirFileU3E__14_14() { return &___U3ClDirFileU3E__14_14; }
	inline void set_U3ClDirFileU3E__14_14(String_t* value)
	{
		___U3ClDirFileU3E__14_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClDirFileU3E__14_14, value);
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}

	inline static int32_t get_offset_of_U24current_16() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U24current_16)); }
	inline Il2CppObject * get_U24current_16() const { return ___U24current_16; }
	inline Il2CppObject ** get_address_of_U24current_16() { return &___U24current_16; }
	inline void set_U24current_16(Il2CppObject * value)
	{
		___U24current_16 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_17() { return static_cast<int32_t>(offsetof(U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816, ___U3CU3Ef__this_17)); }
	inline DownloadImages_t3644489024 * get_U3CU3Ef__this_17() const { return ___U3CU3Ef__this_17; }
	inline DownloadImages_t3644489024 ** get_address_of_U3CU3Ef__this_17() { return &___U3CU3Ef__this_17; }
	inline void set_U3CU3Ef__this_17(DownloadImages_t3644489024 * value)
	{
		___U3CU3Ef__this_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
