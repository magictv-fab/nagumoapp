﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.DescriptorData
struct  DescriptorData_t3973589607  : public Il2CppObject
{
public:
	// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::size
	int64_t ___size_0;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::compressedSize
	int64_t ___compressedSize_1;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::crc
	int64_t ___crc_2;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(DescriptorData_t3973589607, ___size_0)); }
	inline int64_t get_size_0() const { return ___size_0; }
	inline int64_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int64_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_compressedSize_1() { return static_cast<int32_t>(offsetof(DescriptorData_t3973589607, ___compressedSize_1)); }
	inline int64_t get_compressedSize_1() const { return ___compressedSize_1; }
	inline int64_t* get_address_of_compressedSize_1() { return &___compressedSize_1; }
	inline void set_compressedSize_1(int64_t value)
	{
		___compressedSize_1 = value;
	}

	inline static int32_t get_offset_of_crc_2() { return static_cast<int32_t>(offsetof(DescriptorData_t3973589607, ___crc_2)); }
	inline int64_t get_crc_2() const { return ___crc_2; }
	inline int64_t* get_address_of_crc_2() { return &___crc_2; }
	inline void set_crc_2(int64_t value)
	{
		___crc_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
