﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t147246886;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t1860057025;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t41970945;

#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C"  ReconstructionAbstractBehaviour_t1860057025 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2163573237 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C"  Il2CppObject * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m947614179 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern "C"  void ReconstructionFromTargetAbstractBehaviour_Awake_m3706742978 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern "C"  void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m1720706264 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C"  void ReconstructionFromTargetAbstractBehaviour_Initialize_m896549525 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern "C"  void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2206025709 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C"  void ReconstructionFromTargetAbstractBehaviour__ctor_m3469137759 (ReconstructionFromTargetAbstractBehaviour_t147246886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
