﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.PoolQuaternion
struct PoolQuaternion_t2988316782;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2399324759;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.utils.PoolQuaternion::.ctor(System.UInt32)
extern "C"  void PoolQuaternion__ctor_m908438856 (PoolQuaternion_t2988316782 * __this, uint32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.PoolQuaternion::addItem(UnityEngine.Quaternion)
extern "C"  void PoolQuaternion_addItem_m1011664271 (PoolQuaternion_t2988316782 * __this, Quaternion_t1553702882  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.PoolQuaternion::getCurrentSize()
extern "C"  int32_t PoolQuaternion_getCurrentSize_m2715025444 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.PoolQuaternion::isFull()
extern "C"  bool PoolQuaternion_isFull_m4235457535 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.PoolQuaternion::reset()
extern "C"  void PoolQuaternion_reset_m394309567 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion[] ARM.utils.PoolQuaternion::getItens()
extern "C"  QuaternionU5BU5D_t2399324759* PoolQuaternion_getItens_m1807339073 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.utils.PoolQuaternion::getMedian()
extern "C"  Quaternion_t1553702882  PoolQuaternion_getMedian_m3707410600 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.utils.PoolQuaternion::getMean()
extern "C"  Quaternion_t1553702882  PoolQuaternion_getMean_m973602787 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.utils.PoolQuaternion::getLastAngle()
extern "C"  Quaternion_t1553702882  PoolQuaternion_getLastAngle_m3047166817 (PoolQuaternion_t2988316782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion[] ARM.utils.PoolQuaternion::getLastsAngles(System.UInt32)
extern "C"  QuaternionU5BU5D_t2399324759* PoolQuaternion_getLastsAngles_m286170171 (PoolQuaternion_t2988316782 * __this, uint32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
