﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.DebugToggleOnOff
struct  DebugToggleOnOff_t1258336469  : public ToggleOnOffAbstract_t832893812
{
public:
	// System.Boolean ARM.events.DebugToggleOnOff::ligado
	bool ___ligado_7;
	// System.Boolean ARM.events.DebugToggleOnOff::_lastValue
	bool ____lastValue_8;

public:
	inline static int32_t get_offset_of_ligado_7() { return static_cast<int32_t>(offsetof(DebugToggleOnOff_t1258336469, ___ligado_7)); }
	inline bool get_ligado_7() const { return ___ligado_7; }
	inline bool* get_address_of_ligado_7() { return &___ligado_7; }
	inline void set_ligado_7(bool value)
	{
		___ligado_7 = value;
	}

	inline static int32_t get_offset_of__lastValue_8() { return static_cast<int32_t>(offsetof(DebugToggleOnOff_t1258336469, ____lastValue_8)); }
	inline bool get__lastValue_8() const { return ____lastValue_8; }
	inline bool* get_address_of__lastValue_8() { return &____lastValue_8; }
	inline void set__lastValue_8(bool value)
	{
		____lastValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
