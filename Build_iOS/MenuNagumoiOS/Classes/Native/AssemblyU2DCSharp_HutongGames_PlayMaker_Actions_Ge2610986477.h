﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVector3XYZ
struct  GetVector3XYZ_t2610986477  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetVector3XYZ::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector3XYZ::storeX
	FsmFloat_t2134102846 * ___storeX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector3XYZ::storeY
	FsmFloat_t2134102846 * ___storeY_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector3XYZ::storeZ
	FsmFloat_t2134102846 * ___storeZ_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVector3XYZ::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_vector3Variable_9() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t2610986477, ___vector3Variable_9)); }
	inline FsmVector3_t533912882 * get_vector3Variable_9() const { return ___vector3Variable_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_9() { return &___vector3Variable_9; }
	inline void set_vector3Variable_9(FsmVector3_t533912882 * value)
	{
		___vector3Variable_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_9, value);
	}

	inline static int32_t get_offset_of_storeX_10() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t2610986477, ___storeX_10)); }
	inline FsmFloat_t2134102846 * get_storeX_10() const { return ___storeX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeX_10() { return &___storeX_10; }
	inline void set_storeX_10(FsmFloat_t2134102846 * value)
	{
		___storeX_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeX_10, value);
	}

	inline static int32_t get_offset_of_storeY_11() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t2610986477, ___storeY_11)); }
	inline FsmFloat_t2134102846 * get_storeY_11() const { return ___storeY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_storeY_11() { return &___storeY_11; }
	inline void set_storeY_11(FsmFloat_t2134102846 * value)
	{
		___storeY_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeY_11, value);
	}

	inline static int32_t get_offset_of_storeZ_12() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t2610986477, ___storeZ_12)); }
	inline FsmFloat_t2134102846 * get_storeZ_12() const { return ___storeZ_12; }
	inline FsmFloat_t2134102846 ** get_address_of_storeZ_12() { return &___storeZ_12; }
	inline void set_storeZ_12(FsmFloat_t2134102846 * value)
	{
		___storeZ_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeZ_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t2610986477, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
