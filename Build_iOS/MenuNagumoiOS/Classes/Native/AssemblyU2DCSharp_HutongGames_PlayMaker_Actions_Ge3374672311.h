﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3972655516.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetDeviceRoll
struct  GetDeviceRoll_t3374672311  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.Actions.GetDeviceRoll/BaseOrientation HutongGames.PlayMaker.Actions.GetDeviceRoll::baseOrientation
	int32_t ___baseOrientation_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceRoll::storeAngle
	FsmFloat_t2134102846 * ___storeAngle_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceRoll::limitAngle
	FsmFloat_t2134102846 * ___limitAngle_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceRoll::smoothing
	FsmFloat_t2134102846 * ___smoothing_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetDeviceRoll::everyFrame
	bool ___everyFrame_13;
	// System.Single HutongGames.PlayMaker.Actions.GetDeviceRoll::lastZAngle
	float ___lastZAngle_14;

public:
	inline static int32_t get_offset_of_baseOrientation_9() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t3374672311, ___baseOrientation_9)); }
	inline int32_t get_baseOrientation_9() const { return ___baseOrientation_9; }
	inline int32_t* get_address_of_baseOrientation_9() { return &___baseOrientation_9; }
	inline void set_baseOrientation_9(int32_t value)
	{
		___baseOrientation_9 = value;
	}

	inline static int32_t get_offset_of_storeAngle_10() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t3374672311, ___storeAngle_10)); }
	inline FsmFloat_t2134102846 * get_storeAngle_10() const { return ___storeAngle_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeAngle_10() { return &___storeAngle_10; }
	inline void set_storeAngle_10(FsmFloat_t2134102846 * value)
	{
		___storeAngle_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeAngle_10, value);
	}

	inline static int32_t get_offset_of_limitAngle_11() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t3374672311, ___limitAngle_11)); }
	inline FsmFloat_t2134102846 * get_limitAngle_11() const { return ___limitAngle_11; }
	inline FsmFloat_t2134102846 ** get_address_of_limitAngle_11() { return &___limitAngle_11; }
	inline void set_limitAngle_11(FsmFloat_t2134102846 * value)
	{
		___limitAngle_11 = value;
		Il2CppCodeGenWriteBarrier(&___limitAngle_11, value);
	}

	inline static int32_t get_offset_of_smoothing_12() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t3374672311, ___smoothing_12)); }
	inline FsmFloat_t2134102846 * get_smoothing_12() const { return ___smoothing_12; }
	inline FsmFloat_t2134102846 ** get_address_of_smoothing_12() { return &___smoothing_12; }
	inline void set_smoothing_12(FsmFloat_t2134102846 * value)
	{
		___smoothing_12 = value;
		Il2CppCodeGenWriteBarrier(&___smoothing_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t3374672311, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_lastZAngle_14() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t3374672311, ___lastZAngle_14)); }
	inline float get_lastZAngle_14() const { return ___lastZAngle_14; }
	inline float* get_address_of_lastZAngle_14() { return &___lastZAngle_14; }
	inline void set_lastZAngle_14(float value)
	{
		___lastZAngle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
