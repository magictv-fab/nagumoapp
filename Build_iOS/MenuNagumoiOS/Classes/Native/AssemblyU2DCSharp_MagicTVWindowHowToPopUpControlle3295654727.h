﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t110812896;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowHowToPopUpControllerScript
struct  MagicTVWindowHowToPopUpControllerScript_t3295654727  : public GenericWindowControllerScript_t248075822
{
public:
	// UnityEngine.UI.Toggle MagicTVWindowHowToPopUpControllerScript::DontShowAnymoreToggle
	Toggle_t110812896 * ___DontShowAnymoreToggle_4;

public:
	inline static int32_t get_offset_of_DontShowAnymoreToggle_4() { return static_cast<int32_t>(offsetof(MagicTVWindowHowToPopUpControllerScript_t3295654727, ___DontShowAnymoreToggle_4)); }
	inline Toggle_t110812896 * get_DontShowAnymoreToggle_4() const { return ___DontShowAnymoreToggle_4; }
	inline Toggle_t110812896 ** get_address_of_DontShowAnymoreToggle_4() { return &___DontShowAnymoreToggle_4; }
	inline void set_DontShowAnymoreToggle_4(Toggle_t110812896 * value)
	{
		___DontShowAnymoreToggle_4 = value;
		Il2CppCodeGenWriteBarrier(&___DontShowAnymoreToggle_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
