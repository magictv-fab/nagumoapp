﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ImageTargetImpl
struct ImageTargetImpl_t1650084406;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t2095838082;
// Vuforia.VirtualButton
struct VirtualButton_t704206407;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton>
struct IEnumerable_1_t4005119364;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg1650423632.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Rectangle2265684451.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBut704206407.h"

// System.Void Vuforia.ImageTargetImpl::.ctor(System.String,System.Int32,Vuforia.ImageTargetType,Vuforia.DataSet)
extern "C"  void ImageTargetImpl__ctor_m2986592022 (ImageTargetImpl_t1650084406 * __this, String_t* ___name0, int32_t ___id1, int32_t ___imageTargetType2, DataSet_t2095838082 * ___dataSet3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::get_ImageTargetType()
extern "C"  int32_t ImageTargetImpl_get_ImageTargetType_m102237635 (ImageTargetImpl_t1650084406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
extern "C"  VirtualButton_t704206407 * ImageTargetImpl_CreateVirtualButton_m2793306705 (ImageTargetImpl_t1650084406 * __this, String_t* ___name0, RectangleData_t2265684451  ___area1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::GetVirtualButtonByName(System.String)
extern "C"  VirtualButton_t704206407 * ImageTargetImpl_GetVirtualButtonByName_m2687414886 (ImageTargetImpl_t1650084406 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.ImageTargetImpl::GetVirtualButtons()
extern "C"  Il2CppObject* ImageTargetImpl_GetVirtualButtons_m568114070 (ImageTargetImpl_t1650084406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
extern "C"  bool ImageTargetImpl_DestroyVirtualButton_m3935437621 (ImageTargetImpl_t1650084406 * __this, VirtualButton_t704206407 * ___vb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateNewVirtualButtonInNative(System.String,Vuforia.RectangleData)
extern "C"  VirtualButton_t704206407 * ImageTargetImpl_CreateNewVirtualButtonInNative_m342129337 (ImageTargetImpl_t1650084406 * __this, String_t* ___name0, RectangleData_t2265684451  ___rectangleData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetImpl::UnregisterVirtualButtonInNative(Vuforia.VirtualButton)
extern "C"  bool ImageTargetImpl_UnregisterVirtualButtonInNative_m2271340555 (ImageTargetImpl_t1650084406 * __this, VirtualButton_t704206407 * ___vb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetImpl::CreateVirtualButtonsFromNative()
extern "C"  void ImageTargetImpl_CreateVirtualButtonsFromNative_m3078399080 (ImageTargetImpl_t1650084406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
