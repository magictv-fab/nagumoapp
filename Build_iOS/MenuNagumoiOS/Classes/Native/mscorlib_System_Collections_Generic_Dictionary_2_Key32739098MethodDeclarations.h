﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3573311923MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m470874251(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t32739098 *, Dictionary_2_t2700946943 *, const MethodInfo*))KeyCollection__ctor_m3356474829_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1851258347(__this, ___item0, method) ((  void (*) (KeyCollection_t32739098 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m404713321_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m627612194(__this, method) ((  void (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3978058528_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m122086911(__this, ___item0, method) ((  bool (*) (KeyCollection_t32739098 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2234264581_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2378353444(__this, ___item0, method) ((  bool (*) (KeyCollection_t32739098 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2049825450_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2358316254(__this, method) ((  Il2CppObject* (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1922668146_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2300205972(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t32739098 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1194233746_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m761618959(__this, method) ((  Il2CppObject * (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3274484385_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4242463136(__this, method) ((  bool (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2525672998_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1182165586(__this, method) ((  bool (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1714230872_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3566048638(__this, method) ((  Il2CppObject * (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3784199562_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1047219008(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t32739098 *, StateMachineU5BU5D_t1009320683*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3846150274_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2640098851(__this, method) ((  Enumerator_t3315882997  (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_GetEnumerator_m2404802511_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_Count()
#define KeyCollection_get_Count_m89322520(__this, method) ((  int32_t (*) (KeyCollection_t32739098 *, const MethodInfo*))KeyCollection_get_Count_m2843145234_gshared)(__this, method)
