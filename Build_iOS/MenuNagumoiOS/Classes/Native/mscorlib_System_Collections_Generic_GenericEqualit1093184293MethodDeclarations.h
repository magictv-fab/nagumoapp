﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Int64>
struct GenericEqualityComparer_1_t1093184293;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1631029438_gshared (GenericEqualityComparer_1_t1093184293 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1631029438(__this, method) ((  void (*) (GenericEqualityComparer_1_t1093184293 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1631029438_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3466651949_gshared (GenericEqualityComparer_1_t1093184293 * __this, int64_t ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m3466651949(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t1093184293 *, int64_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m3466651949_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m32283279_gshared (GenericEqualityComparer_1_t1093184293 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m32283279(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t1093184293 *, int64_t, int64_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m32283279_gshared)(__this, ___x0, ___y1, method)
