﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t3721690872;
// UnityEngine.UI.RawImage
struct RawImage_t821930207;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraFake
struct  cameraFake_t2282419226  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.WebCamTexture cameraFake::mCamera
	WebCamTexture_t1290350902 * ___mCamera_2;
	// System.Single cameraFake::bias
	float ___bias_3;
	// System.Int32 cameraFake::_cameraIndex
	int32_t ____cameraIndex_4;
	// System.Boolean cameraFake::back
	bool ___back_5;
	// UnityEngine.WebCamDevice[] cameraFake::devices
	WebCamDeviceU5BU5D_t3721690872* ___devices_6;
	// UnityEngine.UI.RawImage cameraFake::image
	RawImage_t821930207 * ___image_7;

public:
	inline static int32_t get_offset_of_mCamera_2() { return static_cast<int32_t>(offsetof(cameraFake_t2282419226, ___mCamera_2)); }
	inline WebCamTexture_t1290350902 * get_mCamera_2() const { return ___mCamera_2; }
	inline WebCamTexture_t1290350902 ** get_address_of_mCamera_2() { return &___mCamera_2; }
	inline void set_mCamera_2(WebCamTexture_t1290350902 * value)
	{
		___mCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___mCamera_2, value);
	}

	inline static int32_t get_offset_of_bias_3() { return static_cast<int32_t>(offsetof(cameraFake_t2282419226, ___bias_3)); }
	inline float get_bias_3() const { return ___bias_3; }
	inline float* get_address_of_bias_3() { return &___bias_3; }
	inline void set_bias_3(float value)
	{
		___bias_3 = value;
	}

	inline static int32_t get_offset_of__cameraIndex_4() { return static_cast<int32_t>(offsetof(cameraFake_t2282419226, ____cameraIndex_4)); }
	inline int32_t get__cameraIndex_4() const { return ____cameraIndex_4; }
	inline int32_t* get_address_of__cameraIndex_4() { return &____cameraIndex_4; }
	inline void set__cameraIndex_4(int32_t value)
	{
		____cameraIndex_4 = value;
	}

	inline static int32_t get_offset_of_back_5() { return static_cast<int32_t>(offsetof(cameraFake_t2282419226, ___back_5)); }
	inline bool get_back_5() const { return ___back_5; }
	inline bool* get_address_of_back_5() { return &___back_5; }
	inline void set_back_5(bool value)
	{
		___back_5 = value;
	}

	inline static int32_t get_offset_of_devices_6() { return static_cast<int32_t>(offsetof(cameraFake_t2282419226, ___devices_6)); }
	inline WebCamDeviceU5BU5D_t3721690872* get_devices_6() const { return ___devices_6; }
	inline WebCamDeviceU5BU5D_t3721690872** get_address_of_devices_6() { return &___devices_6; }
	inline void set_devices_6(WebCamDeviceU5BU5D_t3721690872* value)
	{
		___devices_6 = value;
		Il2CppCodeGenWriteBarrier(&___devices_6, value);
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(cameraFake_t2282419226, ___image_7)); }
	inline RawImage_t821930207 * get_image_7() const { return ___image_7; }
	inline RawImage_t821930207 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(RawImage_t821930207 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier(&___image_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
