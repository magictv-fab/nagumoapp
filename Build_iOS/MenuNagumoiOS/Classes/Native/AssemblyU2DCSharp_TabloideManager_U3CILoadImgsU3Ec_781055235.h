﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// TabloideManager
struct TabloideManager_t3674153819;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabloideManager/<ILoadImgs>c__Iterator81
struct  U3CILoadImgsU3Ec__Iterator81_t781055235  : public Il2CppObject
{
public:
	// System.Int32 TabloideManager/<ILoadImgs>c__Iterator81::<i>__0
	int32_t ___U3CiU3E__0_0;
	// UnityEngine.WWW TabloideManager/<ILoadImgs>c__Iterator81::<www>__1
	WWW_t3134621005 * ___U3CwwwU3E__1_1;
	// UnityEngine.Sprite TabloideManager/<ILoadImgs>c__Iterator81::<spt>__2
	Sprite_t3199167241 * ___U3CsptU3E__2_2;
	// System.Int32 TabloideManager/<ILoadImgs>c__Iterator81::$PC
	int32_t ___U24PC_3;
	// System.Object TabloideManager/<ILoadImgs>c__Iterator81::$current
	Il2CppObject * ___U24current_4;
	// TabloideManager TabloideManager/<ILoadImgs>c__Iterator81::<>f__this
	TabloideManager_t3674153819 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator81_t781055235, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator81_t781055235, ___U3CwwwU3E__1_1)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__1_1() const { return ___U3CwwwU3E__1_1; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__1_1() { return &___U3CwwwU3E__1_1; }
	inline void set_U3CwwwU3E__1_1(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator81_t781055235, ___U3CsptU3E__2_2)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__2_2() const { return ___U3CsptU3E__2_2; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__2_2() { return &___U3CsptU3E__2_2; }
	inline void set_U3CsptU3E__2_2(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator81_t781055235, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator81_t781055235, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator81_t781055235, ___U3CU3Ef__this_5)); }
	inline TabloideManager_t3674153819 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline TabloideManager_t3674153819 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(TabloideManager_t3674153819 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
