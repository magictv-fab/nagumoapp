﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Analytics
struct Analytics_t310950758;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"

// System.Void Analytics::.ctor()
extern "C"  void Analytics__ctor_m3195522245 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::.cctor()
extern "C"  void Analytics__cctor_m4089812872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Analytics Analytics::get_Instance()
extern "C"  Analytics_t310950758 * Analytics_get_Instance_m3599909552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Analytics::getPOSIXTime()
extern "C"  int32_t Analytics_getPOSIXTime_m2948734507 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::setPlayerPref_disableAnalyticsByUserOptOut(System.Boolean)
extern "C"  void Analytics_setPlayerPref_disableAnalyticsByUserOptOut_m3403905802 (Il2CppObject * __this /* static, unused */, bool ___analyticsDisabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::Init()
extern "C"  void Analytics_Init_m3656848623 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::OnLevelWasLoaded(System.Int32)
extern "C"  void Analytics_OnLevelWasLoaded_m69246681 (Analytics_t310950758 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::LevelChange(System.String)
extern "C"  void Analytics_LevelChange_m3264740523 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::OnEnable()
extern "C"  void Analytics_OnEnable_m1924663457 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::OnLevelFinishedLoading(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void Analytics_OnLevelFinishedLoading_m3599826481 (Analytics_t310950758 * __this, Scene_t1080795294  ___scene0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::OnDisable()
extern "C"  void Analytics_OnDisable_m4270929580 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::Start()
extern "C"  void Analytics_Start_m2142660037 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::changeScreen(System.String)
extern "C"  void Analytics_changeScreen_m7849959 (Analytics_t310950758 * __this, String_t* ___newScreenName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::sendEvent(System.String,System.String,System.String,System.Int32)
extern "C"  void Analytics_sendEvent_m2675680850 (Analytics_t310950758 * __this, String_t* ___category0, String_t* ___action1, String_t* ___label2, int32_t ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Analytics::debugSendEvent()
extern "C"  void Analytics_debugSendEvent_m1636816222 (Analytics_t310950758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
