﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.AnimatorController
struct AnimatorController_t975235909;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.animation.AnimatorController::.ctor()
extern "C"  void AnimatorController__ctor_m2128473692 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Start()
extern "C"  void AnimatorController_Start_m1075611484 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Update()
extern "C"  void AnimatorController_Update_m3285037105 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::prepareToPlay()
extern "C"  void AnimatorController_prepareToPlay_m3165326672 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Play()
extern "C"  void AnimatorController_Play_m1742541212 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Pause()
extern "C"  void AnimatorController_Pause_m2182599664 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Stop()
extern "C"  void AnimatorController_Stop_m1836225258 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Hide()
extern "C"  void AnimatorController_Hide_m1510807594 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.AnimatorController::Show()
extern "C"  void AnimatorController_Show_m1825149733 (AnimatorController_t975235909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
