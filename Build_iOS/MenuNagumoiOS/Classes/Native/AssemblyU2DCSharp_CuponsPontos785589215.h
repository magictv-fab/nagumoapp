﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.UI.RawImage
struct RawImage_t821930207;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// QRCodeEncodeController
struct QRCodeEncodeController_t2317858336;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CuponsPontos
struct  CuponsPontos_t785589215  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image CuponsPontos::container
	Image_t538875265 * ___container_2;
	// UnityEngine.Sprite CuponsPontos::cupom10
	Sprite_t3199167241 * ___cupom10_3;
	// UnityEngine.Sprite CuponsPontos::cupom50
	Sprite_t3199167241 * ___cupom50_4;
	// UnityEngine.Sprite CuponsPontos::cupom100
	Sprite_t3199167241 * ___cupom100_5;
	// UnityEngine.Sprite CuponsPontos::cupom200
	Sprite_t3199167241 * ___cupom200_6;
	// UnityEngine.Sprite CuponsPontos::cupom500
	Sprite_t3199167241 * ___cupom500_7;
	// UnityEngine.UI.RawImage CuponsPontos::qrCodeImage
	RawImage_t821930207 * ___qrCodeImage_8;
	// System.Int32 CuponsPontos::cupomValue
	int32_t ___cupomValue_9;
	// UnityEngine.UI.Text CuponsPontos::codeText
	Text_t9039225 * ___codeText_10;
	// UnityEngine.UI.Text CuponsPontos::dateText
	Text_t9039225 * ___dateText_11;
	// System.String CuponsPontos::code
	String_t* ___code_12;
	// UnityEngine.GameObject CuponsPontos::usedObj
	GameObject_t3674682005 * ___usedObj_13;
	// System.Boolean CuponsPontos::used
	bool ___used_14;
	// UnityEngine.UI.Image CuponsPontos::barcodeSprite
	Image_t538875265 * ___barcodeSprite_15;
	// QRCodeEncodeController CuponsPontos::e_qrController
	QRCodeEncodeController_t2317858336 * ___e_qrController_16;

public:
	inline static int32_t get_offset_of_container_2() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___container_2)); }
	inline Image_t538875265 * get_container_2() const { return ___container_2; }
	inline Image_t538875265 ** get_address_of_container_2() { return &___container_2; }
	inline void set_container_2(Image_t538875265 * value)
	{
		___container_2 = value;
		Il2CppCodeGenWriteBarrier(&___container_2, value);
	}

	inline static int32_t get_offset_of_cupom10_3() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___cupom10_3)); }
	inline Sprite_t3199167241 * get_cupom10_3() const { return ___cupom10_3; }
	inline Sprite_t3199167241 ** get_address_of_cupom10_3() { return &___cupom10_3; }
	inline void set_cupom10_3(Sprite_t3199167241 * value)
	{
		___cupom10_3 = value;
		Il2CppCodeGenWriteBarrier(&___cupom10_3, value);
	}

	inline static int32_t get_offset_of_cupom50_4() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___cupom50_4)); }
	inline Sprite_t3199167241 * get_cupom50_4() const { return ___cupom50_4; }
	inline Sprite_t3199167241 ** get_address_of_cupom50_4() { return &___cupom50_4; }
	inline void set_cupom50_4(Sprite_t3199167241 * value)
	{
		___cupom50_4 = value;
		Il2CppCodeGenWriteBarrier(&___cupom50_4, value);
	}

	inline static int32_t get_offset_of_cupom100_5() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___cupom100_5)); }
	inline Sprite_t3199167241 * get_cupom100_5() const { return ___cupom100_5; }
	inline Sprite_t3199167241 ** get_address_of_cupom100_5() { return &___cupom100_5; }
	inline void set_cupom100_5(Sprite_t3199167241 * value)
	{
		___cupom100_5 = value;
		Il2CppCodeGenWriteBarrier(&___cupom100_5, value);
	}

	inline static int32_t get_offset_of_cupom200_6() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___cupom200_6)); }
	inline Sprite_t3199167241 * get_cupom200_6() const { return ___cupom200_6; }
	inline Sprite_t3199167241 ** get_address_of_cupom200_6() { return &___cupom200_6; }
	inline void set_cupom200_6(Sprite_t3199167241 * value)
	{
		___cupom200_6 = value;
		Il2CppCodeGenWriteBarrier(&___cupom200_6, value);
	}

	inline static int32_t get_offset_of_cupom500_7() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___cupom500_7)); }
	inline Sprite_t3199167241 * get_cupom500_7() const { return ___cupom500_7; }
	inline Sprite_t3199167241 ** get_address_of_cupom500_7() { return &___cupom500_7; }
	inline void set_cupom500_7(Sprite_t3199167241 * value)
	{
		___cupom500_7 = value;
		Il2CppCodeGenWriteBarrier(&___cupom500_7, value);
	}

	inline static int32_t get_offset_of_qrCodeImage_8() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___qrCodeImage_8)); }
	inline RawImage_t821930207 * get_qrCodeImage_8() const { return ___qrCodeImage_8; }
	inline RawImage_t821930207 ** get_address_of_qrCodeImage_8() { return &___qrCodeImage_8; }
	inline void set_qrCodeImage_8(RawImage_t821930207 * value)
	{
		___qrCodeImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___qrCodeImage_8, value);
	}

	inline static int32_t get_offset_of_cupomValue_9() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___cupomValue_9)); }
	inline int32_t get_cupomValue_9() const { return ___cupomValue_9; }
	inline int32_t* get_address_of_cupomValue_9() { return &___cupomValue_9; }
	inline void set_cupomValue_9(int32_t value)
	{
		___cupomValue_9 = value;
	}

	inline static int32_t get_offset_of_codeText_10() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___codeText_10)); }
	inline Text_t9039225 * get_codeText_10() const { return ___codeText_10; }
	inline Text_t9039225 ** get_address_of_codeText_10() { return &___codeText_10; }
	inline void set_codeText_10(Text_t9039225 * value)
	{
		___codeText_10 = value;
		Il2CppCodeGenWriteBarrier(&___codeText_10, value);
	}

	inline static int32_t get_offset_of_dateText_11() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___dateText_11)); }
	inline Text_t9039225 * get_dateText_11() const { return ___dateText_11; }
	inline Text_t9039225 ** get_address_of_dateText_11() { return &___dateText_11; }
	inline void set_dateText_11(Text_t9039225 * value)
	{
		___dateText_11 = value;
		Il2CppCodeGenWriteBarrier(&___dateText_11, value);
	}

	inline static int32_t get_offset_of_code_12() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___code_12)); }
	inline String_t* get_code_12() const { return ___code_12; }
	inline String_t** get_address_of_code_12() { return &___code_12; }
	inline void set_code_12(String_t* value)
	{
		___code_12 = value;
		Il2CppCodeGenWriteBarrier(&___code_12, value);
	}

	inline static int32_t get_offset_of_usedObj_13() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___usedObj_13)); }
	inline GameObject_t3674682005 * get_usedObj_13() const { return ___usedObj_13; }
	inline GameObject_t3674682005 ** get_address_of_usedObj_13() { return &___usedObj_13; }
	inline void set_usedObj_13(GameObject_t3674682005 * value)
	{
		___usedObj_13 = value;
		Il2CppCodeGenWriteBarrier(&___usedObj_13, value);
	}

	inline static int32_t get_offset_of_used_14() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___used_14)); }
	inline bool get_used_14() const { return ___used_14; }
	inline bool* get_address_of_used_14() { return &___used_14; }
	inline void set_used_14(bool value)
	{
		___used_14 = value;
	}

	inline static int32_t get_offset_of_barcodeSprite_15() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___barcodeSprite_15)); }
	inline Image_t538875265 * get_barcodeSprite_15() const { return ___barcodeSprite_15; }
	inline Image_t538875265 ** get_address_of_barcodeSprite_15() { return &___barcodeSprite_15; }
	inline void set_barcodeSprite_15(Image_t538875265 * value)
	{
		___barcodeSprite_15 = value;
		Il2CppCodeGenWriteBarrier(&___barcodeSprite_15, value);
	}

	inline static int32_t get_offset_of_e_qrController_16() { return static_cast<int32_t>(offsetof(CuponsPontos_t785589215, ___e_qrController_16)); }
	inline QRCodeEncodeController_t2317858336 * get_e_qrController_16() const { return ___e_qrController_16; }
	inline QRCodeEncodeController_t2317858336 ** get_address_of_e_qrController_16() { return &___e_qrController_16; }
	inline void set_e_qrController_16(QRCodeEncodeController_t2317858336 * value)
	{
		___e_qrController_16 = value;
		Il2CppCodeGenWriteBarrier(&___e_qrController_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
