﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowSplashDownloadAvailableControllerScript
struct  MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250  : public GenericWindowControllerScript_t248075822
{
public:
	// System.Action MagicTVWindowSplashDownloadAvailableControllerScript::onClickConfirmDownload
	Action_t3771233898 * ___onClickConfirmDownload_4;
	// System.Action MagicTVWindowSplashDownloadAvailableControllerScript::onClickCancelDownload
	Action_t3771233898 * ___onClickCancelDownload_5;

public:
	inline static int32_t get_offset_of_onClickConfirmDownload_4() { return static_cast<int32_t>(offsetof(MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250, ___onClickConfirmDownload_4)); }
	inline Action_t3771233898 * get_onClickConfirmDownload_4() const { return ___onClickConfirmDownload_4; }
	inline Action_t3771233898 ** get_address_of_onClickConfirmDownload_4() { return &___onClickConfirmDownload_4; }
	inline void set_onClickConfirmDownload_4(Action_t3771233898 * value)
	{
		___onClickConfirmDownload_4 = value;
		Il2CppCodeGenWriteBarrier(&___onClickConfirmDownload_4, value);
	}

	inline static int32_t get_offset_of_onClickCancelDownload_5() { return static_cast<int32_t>(offsetof(MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250, ___onClickCancelDownload_5)); }
	inline Action_t3771233898 * get_onClickCancelDownload_5() const { return ___onClickCancelDownload_5; }
	inline Action_t3771233898 ** get_address_of_onClickCancelDownload_5() { return &___onClickCancelDownload_5; }
	inline void set_onClickCancelDownload_5(Action_t3771233898 * value)
	{
		___onClickCancelDownload_5 = value;
		Il2CppCodeGenWriteBarrier(&___onClickCancelDownload_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
