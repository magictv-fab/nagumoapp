﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.download.SingleGenericDownloader
struct SingleGenericDownloader_t168833466;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler
struct OnErrorEventHandler_t1722022561;
// ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler
struct OnDownloadCompleteEventHandler_t1994402001;
// ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler
struct OnProgressEventHandler_t3695678548;
// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler
struct OnCompleteEventHandler_t2318258528;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_utils_download_SingleGenericD168833466.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst1722022561.h"
#include "AssemblyU2DCSharp_ARM_utils_download_DownloaderAbs1994402001.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst3695678548.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2318258528.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.utils.download.SingleGenericDownloader::.ctor()
extern "C"  void SingleGenericDownloader__ctor_m2104082478 (SingleGenericDownloader_t168833466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARM.utils.download.SingleGenericDownloader::get_SelfGameObject()
extern "C"  GameObject_t3674682005 * SingleGenericDownloader_get_SelfGameObject_m3604292113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.download.SingleGenericDownloader ARM.utils.download.SingleGenericDownloader::get_SelfInstance()
extern "C"  SingleGenericDownloader_t168833466 * SingleGenericDownloader_get_SelfInstance_m3732851120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::set_SelfInstance(ARM.utils.download.SingleGenericDownloader)
extern "C"  void SingleGenericDownloader_set_SelfInstance_m908246503 (Il2CppObject * __this /* static, unused */, SingleGenericDownloader_t168833466 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::AddErrorEventhandlerStatic(ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler)
extern "C"  void SingleGenericDownloader_AddErrorEventhandlerStatic_m4226764826 (Il2CppObject * __this /* static, unused */, OnErrorEventHandler_t1722022561 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::AddDownloadCompleteEventHandler(ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler)
extern "C"  void SingleGenericDownloader_AddDownloadCompleteEventHandler_m298988781 (Il2CppObject * __this /* static, unused */, OnDownloadCompleteEventHandler_t1994402001 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::AddProgressEventhandlerStatic(ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler)
extern "C"  void SingleGenericDownloader_AddProgressEventhandlerStatic_m2333689670 (Il2CppObject * __this /* static, unused */, OnProgressEventHandler_t3695678548 * ___enventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::AddBusyEventHandler(ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler)
extern "C"  void SingleGenericDownloader_AddBusyEventHandler_m1884036252 (Il2CppObject * __this /* static, unused */, OnCompleteEventHandler_t2318258528 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::LoadStatic(System.String,System.Int32)
extern "C"  void SingleGenericDownloader_LoadStatic_m277111327 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___extraAttempts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::doLoad(System.String,System.Int32)
extern "C"  void SingleGenericDownloader_doLoad_m2585266204 (SingleGenericDownloader_t168833466 * __this, String_t* ___url0, int32_t ___extraAttempts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::RaiseBusy()
extern "C"  void SingleGenericDownloader_RaiseBusy_m4266537617 (SingleGenericDownloader_t168833466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::RaiseComplete()
extern "C"  void SingleGenericDownloader_RaiseComplete_m2529001041 (SingleGenericDownloader_t168833466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::Start()
extern "C"  void SingleGenericDownloader_Start_m1051220270 (SingleGenericDownloader_t168833466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.SingleGenericDownloader::Update()
extern "C"  void SingleGenericDownloader_Update_m2528909471 (SingleGenericDownloader_t168833466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
