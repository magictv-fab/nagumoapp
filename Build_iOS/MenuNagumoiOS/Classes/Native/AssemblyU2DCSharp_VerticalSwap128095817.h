﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VerticalSwap
struct  VerticalSwap_t128095817  : public MonoBehaviour_t667441552
{
public:
	// System.Single VerticalSwap::minAxisY
	float ___minAxisY_2;
	// System.Single VerticalSwap::maxAxisX
	float ___maxAxisX_3;
	// System.Single VerticalSwap::multiplier
	float ___multiplier_4;
	// UnityEngine.UI.ScrollRect VerticalSwap::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_5;
	// UnityEngine.Events.UnityEvent VerticalSwap::onScroll
	UnityEvent_t1266085011 * ___onScroll_6;
	// UnityEngine.Vector3 VerticalSwap::startPosition
	Vector3_t4282066566  ___startPosition_7;
	// System.Boolean VerticalSwap::onScrollFlag
	bool ___onScrollFlag_8;
	// System.Boolean VerticalSwap::endUpdate
	bool ___endUpdate_9;

public:
	inline static int32_t get_offset_of_minAxisY_2() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___minAxisY_2)); }
	inline float get_minAxisY_2() const { return ___minAxisY_2; }
	inline float* get_address_of_minAxisY_2() { return &___minAxisY_2; }
	inline void set_minAxisY_2(float value)
	{
		___minAxisY_2 = value;
	}

	inline static int32_t get_offset_of_maxAxisX_3() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___maxAxisX_3)); }
	inline float get_maxAxisX_3() const { return ___maxAxisX_3; }
	inline float* get_address_of_maxAxisX_3() { return &___maxAxisX_3; }
	inline void set_maxAxisX_3(float value)
	{
		___maxAxisX_3 = value;
	}

	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_scrollRect_5() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___scrollRect_5)); }
	inline ScrollRect_t3606982749 * get_scrollRect_5() const { return ___scrollRect_5; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_5() { return &___scrollRect_5; }
	inline void set_scrollRect_5(ScrollRect_t3606982749 * value)
	{
		___scrollRect_5 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_5, value);
	}

	inline static int32_t get_offset_of_onScroll_6() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___onScroll_6)); }
	inline UnityEvent_t1266085011 * get_onScroll_6() const { return ___onScroll_6; }
	inline UnityEvent_t1266085011 ** get_address_of_onScroll_6() { return &___onScroll_6; }
	inline void set_onScroll_6(UnityEvent_t1266085011 * value)
	{
		___onScroll_6 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_6, value);
	}

	inline static int32_t get_offset_of_startPosition_7() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___startPosition_7)); }
	inline Vector3_t4282066566  get_startPosition_7() const { return ___startPosition_7; }
	inline Vector3_t4282066566 * get_address_of_startPosition_7() { return &___startPosition_7; }
	inline void set_startPosition_7(Vector3_t4282066566  value)
	{
		___startPosition_7 = value;
	}

	inline static int32_t get_offset_of_onScrollFlag_8() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___onScrollFlag_8)); }
	inline bool get_onScrollFlag_8() const { return ___onScrollFlag_8; }
	inline bool* get_address_of_onScrollFlag_8() { return &___onScrollFlag_8; }
	inline void set_onScrollFlag_8(bool value)
	{
		___onScrollFlag_8 = value;
	}

	inline static int32_t get_offset_of_endUpdate_9() { return static_cast<int32_t>(offsetof(VerticalSwap_t128095817, ___endUpdate_9)); }
	inline bool get_endUpdate_9() const { return ___endUpdate_9; }
	inline bool* get_address_of_endUpdate_9() { return &___endUpdate_9; }
	inline void set_endUpdate_9(bool value)
	{
		___endUpdate_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
