﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Button
struct Button_t3896396478;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasValuesCRM
struct  OfertasValuesCRM_t2856949562  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text OfertasValuesCRM::title
	Text_t9039225 * ___title_2;
	// UnityEngine.UI.Text OfertasValuesCRM::info
	Text_t9039225 * ___info_3;
	// UnityEngine.UI.Text OfertasValuesCRM::value
	Text_t9039225 * ___value_4;
	// UnityEngine.UI.Text OfertasValuesCRM::DE
	Text_t9039225 * ___DE_5;
	// UnityEngine.UI.Text OfertasValuesCRM::POR
	Text_t9039225 * ___POR_6;
	// UnityEngine.UI.Text OfertasValuesCRM::validade
	Text_t9039225 * ___validade_7;
	// UnityEngine.UI.Text OfertasValuesCRM::unidades
	Text_t9039225 * ___unidades_8;
	// UnityEngine.UI.Text OfertasValuesCRM::aquisicao
	Text_t9039225 * ___aquisicao_9;
	// UnityEngine.UI.Image OfertasValuesCRM::img
	Image_t538875265 * ___img_10;
	// UnityEngine.UI.Image OfertasValuesCRM::favoritoImg
	Image_t538875265 * ___favoritoImg_11;
	// UnityEngine.UI.Button OfertasValuesCRM::btn
	Button_t3896396478 * ___btn_12;
	// System.String OfertasValuesCRM::id
	String_t* ___id_13;
	// System.String OfertasValuesCRM::id_crm
	String_t* ___id_crm_14;
	// UnityEngine.Texture2D OfertasValuesCRM::saveImageSource
	Texture2D_t3884108195 * ___saveImageSource_15;
	// System.Boolean OfertasValuesCRM::favoritou
	bool ___favoritou_16;
	// UnityEngine.Sprite OfertasValuesCRM::favoritoSprite
	Sprite_t3199167241 * ___favoritoSprite_17;
	// UnityEngine.Sprite OfertasValuesCRM::desfavoritouSprite
	Sprite_t3199167241 * ___desfavoritouSprite_18;
	// UnityEngine.GameObject OfertasValuesCRM::favoritouObj
	GameObject_t3674682005 * ___favoritouObj_19;
	// UnityEngine.GameObject OfertasValuesCRM::btnFavoritado
	GameObject_t3674682005 * ___btnFavoritado_20;
	// UnityEngine.GameObject OfertasValuesCRM::btnFavoritar
	GameObject_t3674682005 * ___btnFavoritar_21;
	// UnityEngine.GameObject OfertasValuesCRM::popupSemConexao
	GameObject_t3674682005 * ___popupSemConexao_22;
	// System.Boolean OfertasValuesCRM::showPopup
	bool ___showPopup_23;
	// System.String OfertasValuesCRM::link
	String_t* ___link_24;
	// System.String OfertasValuesCRM::imgLink
	String_t* ___imgLink_25;

public:
	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___title_2)); }
	inline Text_t9039225 * get_title_2() const { return ___title_2; }
	inline Text_t9039225 ** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(Text_t9039225 * value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_info_3() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___info_3)); }
	inline Text_t9039225 * get_info_3() const { return ___info_3; }
	inline Text_t9039225 ** get_address_of_info_3() { return &___info_3; }
	inline void set_info_3(Text_t9039225 * value)
	{
		___info_3 = value;
		Il2CppCodeGenWriteBarrier(&___info_3, value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___value_4)); }
	inline Text_t9039225 * get_value_4() const { return ___value_4; }
	inline Text_t9039225 ** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(Text_t9039225 * value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier(&___value_4, value);
	}

	inline static int32_t get_offset_of_DE_5() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___DE_5)); }
	inline Text_t9039225 * get_DE_5() const { return ___DE_5; }
	inline Text_t9039225 ** get_address_of_DE_5() { return &___DE_5; }
	inline void set_DE_5(Text_t9039225 * value)
	{
		___DE_5 = value;
		Il2CppCodeGenWriteBarrier(&___DE_5, value);
	}

	inline static int32_t get_offset_of_POR_6() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___POR_6)); }
	inline Text_t9039225 * get_POR_6() const { return ___POR_6; }
	inline Text_t9039225 ** get_address_of_POR_6() { return &___POR_6; }
	inline void set_POR_6(Text_t9039225 * value)
	{
		___POR_6 = value;
		Il2CppCodeGenWriteBarrier(&___POR_6, value);
	}

	inline static int32_t get_offset_of_validade_7() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___validade_7)); }
	inline Text_t9039225 * get_validade_7() const { return ___validade_7; }
	inline Text_t9039225 ** get_address_of_validade_7() { return &___validade_7; }
	inline void set_validade_7(Text_t9039225 * value)
	{
		___validade_7 = value;
		Il2CppCodeGenWriteBarrier(&___validade_7, value);
	}

	inline static int32_t get_offset_of_unidades_8() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___unidades_8)); }
	inline Text_t9039225 * get_unidades_8() const { return ___unidades_8; }
	inline Text_t9039225 ** get_address_of_unidades_8() { return &___unidades_8; }
	inline void set_unidades_8(Text_t9039225 * value)
	{
		___unidades_8 = value;
		Il2CppCodeGenWriteBarrier(&___unidades_8, value);
	}

	inline static int32_t get_offset_of_aquisicao_9() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___aquisicao_9)); }
	inline Text_t9039225 * get_aquisicao_9() const { return ___aquisicao_9; }
	inline Text_t9039225 ** get_address_of_aquisicao_9() { return &___aquisicao_9; }
	inline void set_aquisicao_9(Text_t9039225 * value)
	{
		___aquisicao_9 = value;
		Il2CppCodeGenWriteBarrier(&___aquisicao_9, value);
	}

	inline static int32_t get_offset_of_img_10() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___img_10)); }
	inline Image_t538875265 * get_img_10() const { return ___img_10; }
	inline Image_t538875265 ** get_address_of_img_10() { return &___img_10; }
	inline void set_img_10(Image_t538875265 * value)
	{
		___img_10 = value;
		Il2CppCodeGenWriteBarrier(&___img_10, value);
	}

	inline static int32_t get_offset_of_favoritoImg_11() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___favoritoImg_11)); }
	inline Image_t538875265 * get_favoritoImg_11() const { return ___favoritoImg_11; }
	inline Image_t538875265 ** get_address_of_favoritoImg_11() { return &___favoritoImg_11; }
	inline void set_favoritoImg_11(Image_t538875265 * value)
	{
		___favoritoImg_11 = value;
		Il2CppCodeGenWriteBarrier(&___favoritoImg_11, value);
	}

	inline static int32_t get_offset_of_btn_12() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___btn_12)); }
	inline Button_t3896396478 * get_btn_12() const { return ___btn_12; }
	inline Button_t3896396478 ** get_address_of_btn_12() { return &___btn_12; }
	inline void set_btn_12(Button_t3896396478 * value)
	{
		___btn_12 = value;
		Il2CppCodeGenWriteBarrier(&___btn_12, value);
	}

	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___id_13)); }
	inline String_t* get_id_13() const { return ___id_13; }
	inline String_t** get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(String_t* value)
	{
		___id_13 = value;
		Il2CppCodeGenWriteBarrier(&___id_13, value);
	}

	inline static int32_t get_offset_of_id_crm_14() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___id_crm_14)); }
	inline String_t* get_id_crm_14() const { return ___id_crm_14; }
	inline String_t** get_address_of_id_crm_14() { return &___id_crm_14; }
	inline void set_id_crm_14(String_t* value)
	{
		___id_crm_14 = value;
		Il2CppCodeGenWriteBarrier(&___id_crm_14, value);
	}

	inline static int32_t get_offset_of_saveImageSource_15() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___saveImageSource_15)); }
	inline Texture2D_t3884108195 * get_saveImageSource_15() const { return ___saveImageSource_15; }
	inline Texture2D_t3884108195 ** get_address_of_saveImageSource_15() { return &___saveImageSource_15; }
	inline void set_saveImageSource_15(Texture2D_t3884108195 * value)
	{
		___saveImageSource_15 = value;
		Il2CppCodeGenWriteBarrier(&___saveImageSource_15, value);
	}

	inline static int32_t get_offset_of_favoritou_16() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___favoritou_16)); }
	inline bool get_favoritou_16() const { return ___favoritou_16; }
	inline bool* get_address_of_favoritou_16() { return &___favoritou_16; }
	inline void set_favoritou_16(bool value)
	{
		___favoritou_16 = value;
	}

	inline static int32_t get_offset_of_favoritoSprite_17() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___favoritoSprite_17)); }
	inline Sprite_t3199167241 * get_favoritoSprite_17() const { return ___favoritoSprite_17; }
	inline Sprite_t3199167241 ** get_address_of_favoritoSprite_17() { return &___favoritoSprite_17; }
	inline void set_favoritoSprite_17(Sprite_t3199167241 * value)
	{
		___favoritoSprite_17 = value;
		Il2CppCodeGenWriteBarrier(&___favoritoSprite_17, value);
	}

	inline static int32_t get_offset_of_desfavoritouSprite_18() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___desfavoritouSprite_18)); }
	inline Sprite_t3199167241 * get_desfavoritouSprite_18() const { return ___desfavoritouSprite_18; }
	inline Sprite_t3199167241 ** get_address_of_desfavoritouSprite_18() { return &___desfavoritouSprite_18; }
	inline void set_desfavoritouSprite_18(Sprite_t3199167241 * value)
	{
		___desfavoritouSprite_18 = value;
		Il2CppCodeGenWriteBarrier(&___desfavoritouSprite_18, value);
	}

	inline static int32_t get_offset_of_favoritouObj_19() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___favoritouObj_19)); }
	inline GameObject_t3674682005 * get_favoritouObj_19() const { return ___favoritouObj_19; }
	inline GameObject_t3674682005 ** get_address_of_favoritouObj_19() { return &___favoritouObj_19; }
	inline void set_favoritouObj_19(GameObject_t3674682005 * value)
	{
		___favoritouObj_19 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouObj_19, value);
	}

	inline static int32_t get_offset_of_btnFavoritado_20() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___btnFavoritado_20)); }
	inline GameObject_t3674682005 * get_btnFavoritado_20() const { return ___btnFavoritado_20; }
	inline GameObject_t3674682005 ** get_address_of_btnFavoritado_20() { return &___btnFavoritado_20; }
	inline void set_btnFavoritado_20(GameObject_t3674682005 * value)
	{
		___btnFavoritado_20 = value;
		Il2CppCodeGenWriteBarrier(&___btnFavoritado_20, value);
	}

	inline static int32_t get_offset_of_btnFavoritar_21() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___btnFavoritar_21)); }
	inline GameObject_t3674682005 * get_btnFavoritar_21() const { return ___btnFavoritar_21; }
	inline GameObject_t3674682005 ** get_address_of_btnFavoritar_21() { return &___btnFavoritar_21; }
	inline void set_btnFavoritar_21(GameObject_t3674682005 * value)
	{
		___btnFavoritar_21 = value;
		Il2CppCodeGenWriteBarrier(&___btnFavoritar_21, value);
	}

	inline static int32_t get_offset_of_popupSemConexao_22() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___popupSemConexao_22)); }
	inline GameObject_t3674682005 * get_popupSemConexao_22() const { return ___popupSemConexao_22; }
	inline GameObject_t3674682005 ** get_address_of_popupSemConexao_22() { return &___popupSemConexao_22; }
	inline void set_popupSemConexao_22(GameObject_t3674682005 * value)
	{
		___popupSemConexao_22 = value;
		Il2CppCodeGenWriteBarrier(&___popupSemConexao_22, value);
	}

	inline static int32_t get_offset_of_showPopup_23() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___showPopup_23)); }
	inline bool get_showPopup_23() const { return ___showPopup_23; }
	inline bool* get_address_of_showPopup_23() { return &___showPopup_23; }
	inline void set_showPopup_23(bool value)
	{
		___showPopup_23 = value;
	}

	inline static int32_t get_offset_of_link_24() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___link_24)); }
	inline String_t* get_link_24() const { return ___link_24; }
	inline String_t** get_address_of_link_24() { return &___link_24; }
	inline void set_link_24(String_t* value)
	{
		___link_24 = value;
		Il2CppCodeGenWriteBarrier(&___link_24, value);
	}

	inline static int32_t get_offset_of_imgLink_25() { return static_cast<int32_t>(offsetof(OfertasValuesCRM_t2856949562, ___imgLink_25)); }
	inline String_t* get_imgLink_25() const { return ___imgLink_25; }
	inline String_t** get_address_of_imgLink_25() { return &___imgLink_25; }
	inline void set_imgLink_25(String_t* value)
	{
		___imgLink_25 = value;
		Il2CppCodeGenWriteBarrier(&___imgLink_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
