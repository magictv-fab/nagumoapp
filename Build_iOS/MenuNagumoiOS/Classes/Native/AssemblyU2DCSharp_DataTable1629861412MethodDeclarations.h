﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataTable
struct DataTable_t1629861412;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<DataRow>
struct List_1_t181054592;
// DataRow
struct DataRow_t3107836336;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void DataTable::.ctor()
extern "C"  void DataTable__ctor_m643172295 (DataTable_t1629861412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> DataTable::get_Columns()
extern "C"  List_1_t1375417109 * DataTable_get_Columns_m2206762954 (DataTable_t1629861412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataTable::set_Columns(System.Collections.Generic.List`1<System.String>)
extern "C"  void DataTable_set_Columns_m1988390169 (DataTable_t1629861412 * __this, List_1_t1375417109 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<DataRow> DataTable::get_Rows()
extern "C"  List_1_t181054592 * DataTable_get_Rows_m2829871022 (DataTable_t1629861412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataTable::set_Rows(System.Collections.Generic.List`1<DataRow>)
extern "C"  void DataTable_set_Rows_m4210183399 (DataTable_t1629861412 * __this, List_1_t181054592 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataRow DataTable::get_Item(System.Int32)
extern "C"  DataRow_t3107836336 * DataTable_get_Item_m1232130247 (DataTable_t1629861412 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataTable::AddRow(System.Object[])
extern "C"  void DataTable_AddRow_m2607862812 (DataTable_t1629861412 * __this, ObjectU5BU5D_t1108656482* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
