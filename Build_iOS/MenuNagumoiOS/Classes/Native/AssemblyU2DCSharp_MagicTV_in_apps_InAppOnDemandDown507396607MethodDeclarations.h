﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppOnDemandDownloadManager
struct InAppOnDemandDownloadManager_t507396607;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::.ctor(MagicTV.vo.BundleVO[])
extern "C"  void InAppOnDemandDownloadManager__ctor_m548728036 (InAppOnDemandDownloadManager_t507396607 * __this, BundleVOU5BU5D_t2162892100* ___bundles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.in_apps.InAppOnDemandDownloadManager::HasFileToDownload()
extern "C"  bool InAppOnDemandDownloadManager_HasFileToDownload_m342386878 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::Download()
extern "C"  void InAppOnDemandDownloadManager_Download_m1413150289 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::CreateConfirmWindow()
extern "C"  void InAppOnDemandDownloadManager_CreateConfirmWindow_m2905044845 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.in_apps.InAppOnDemandDownloadManager::GetDownloadSize()
extern "C"  float InAppOnDemandDownloadManager_GetDownloadSize_m2701853260 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::WindowConfirmSuccess()
extern "C"  void InAppOnDemandDownloadManager_WindowConfirmSuccess_m2968952892 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::WindowConfirmFailed()
extern "C"  void InAppOnDemandDownloadManager_WindowConfirmFailed_m3234549510 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::DoStartDownload()
extern "C"  void InAppOnDemandDownloadManager_DoStartDownload_m3633521080 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::VuforiaChangeState(System.Boolean)
extern "C"  void InAppOnDemandDownloadManager_VuforiaChangeState_m242357919 (InAppOnDemandDownloadManager_t507396607 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::DownloadProgress(System.Single)
extern "C"  void InAppOnDemandDownloadManager_DownloadProgress_m3497049357 (InAppOnDemandDownloadManager_t507396607 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::DownloadCanceled()
extern "C"  void InAppOnDemandDownloadManager_DownloadCanceled_m596133066 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::DownloadComplete()
extern "C"  void InAppOnDemandDownloadManager_DownloadComplete_m2460764522 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::turnAROn()
extern "C"  void InAppOnDemandDownloadManager_turnAROn_m1228132438 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppOnDemandDownloadManager::removeListeners()
extern "C"  void InAppOnDemandDownloadManager_removeListeners_m3056639156 (InAppOnDemandDownloadManager_t507396607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
