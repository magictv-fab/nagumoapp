﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImages/<Download>c__Iterator1A
struct U3CDownloadU3Ec__Iterator1A_t1709202840;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DownloadImages/<Download>c__Iterator1A::.ctor()
extern "C"  void U3CDownloadU3Ec__Iterator1A__ctor_m4259607043 (U3CDownloadU3Ec__Iterator1A_t1709202840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImages/<Download>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1000822777 (U3CDownloadU3Ec__Iterator1A_t1709202840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImages/<Download>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m932343181 (U3CDownloadU3Ec__Iterator1A_t1709202840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DownloadImages/<Download>c__Iterator1A::MoveNext()
extern "C"  bool U3CDownloadU3Ec__Iterator1A_MoveNext_m4182489593 (U3CDownloadU3Ec__Iterator1A_t1709202840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/<Download>c__Iterator1A::Dispose()
extern "C"  void U3CDownloadU3Ec__Iterator1A_Dispose_m276818304 (U3CDownloadU3Ec__Iterator1A_t1709202840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/<Download>c__Iterator1A::Reset()
extern "C"  void U3CDownloadU3Ec__Iterator1A_Reset_m1906039984 (U3CDownloadU3Ec__Iterator1A_t1709202840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
