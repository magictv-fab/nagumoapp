﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_CollisionType1355765302.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CollisionEvent
struct  CollisionEvent_t2725908848  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.CollisionType HutongGames.PlayMaker.Actions.CollisionEvent::collision
	int32_t ___collision_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CollisionEvent::collideTag
	FsmString_t952858651 * ___collideTag_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.CollisionEvent::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CollisionEvent::storeCollider
	FsmGameObject_t1697147867 * ___storeCollider_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CollisionEvent::storeForce
	FsmFloat_t2134102846 * ___storeForce_13;

public:
	inline static int32_t get_offset_of_collision_9() { return static_cast<int32_t>(offsetof(CollisionEvent_t2725908848, ___collision_9)); }
	inline int32_t get_collision_9() const { return ___collision_9; }
	inline int32_t* get_address_of_collision_9() { return &___collision_9; }
	inline void set_collision_9(int32_t value)
	{
		___collision_9 = value;
	}

	inline static int32_t get_offset_of_collideTag_10() { return static_cast<int32_t>(offsetof(CollisionEvent_t2725908848, ___collideTag_10)); }
	inline FsmString_t952858651 * get_collideTag_10() const { return ___collideTag_10; }
	inline FsmString_t952858651 ** get_address_of_collideTag_10() { return &___collideTag_10; }
	inline void set_collideTag_10(FsmString_t952858651 * value)
	{
		___collideTag_10 = value;
		Il2CppCodeGenWriteBarrier(&___collideTag_10, value);
	}

	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(CollisionEvent_t2725908848, ___sendEvent_11)); }
	inline FsmEvent_t2133468028 * get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmEvent_t2133468028 * value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_storeCollider_12() { return static_cast<int32_t>(offsetof(CollisionEvent_t2725908848, ___storeCollider_12)); }
	inline FsmGameObject_t1697147867 * get_storeCollider_12() const { return ___storeCollider_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeCollider_12() { return &___storeCollider_12; }
	inline void set_storeCollider_12(FsmGameObject_t1697147867 * value)
	{
		___storeCollider_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeCollider_12, value);
	}

	inline static int32_t get_offset_of_storeForce_13() { return static_cast<int32_t>(offsetof(CollisionEvent_t2725908848, ___storeForce_13)); }
	inline FsmFloat_t2134102846 * get_storeForce_13() const { return ___storeForce_13; }
	inline FsmFloat_t2134102846 ** get_address_of_storeForce_13() { return &___storeForce_13; }
	inline void set_storeForce_13(FsmFloat_t2134102846 * value)
	{
		___storeForce_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeForce_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
