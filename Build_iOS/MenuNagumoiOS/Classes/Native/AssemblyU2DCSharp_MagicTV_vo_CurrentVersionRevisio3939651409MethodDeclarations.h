﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.CurrentVersionRevisionVO
struct CurrentVersionRevisionVO_t3939651409;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.CurrentVersionRevisionVO::.ctor()
extern "C"  void CurrentVersionRevisionVO__ctor_m4294842914 (CurrentVersionRevisionVO_t3939651409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
