﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;

#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.CodaBarWriter
struct  CodaBarWriter_t1728947151  : public OneDimensionalCodeWriter_t4068326409
{
public:

public:
};

struct CodaBarWriter_t1728947151_StaticFields
{
public:
	// System.Char[] ZXing.OneD.CodaBarWriter::START_END_CHARS
	CharU5BU5D_t3324145743* ___START_END_CHARS_0;
	// System.Char[] ZXing.OneD.CodaBarWriter::ALT_START_END_CHARS
	CharU5BU5D_t3324145743* ___ALT_START_END_CHARS_1;
	// System.Char[] ZXing.OneD.CodaBarWriter::CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED
	CharU5BU5D_t3324145743* ___CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2;
	// System.Char ZXing.OneD.CodaBarWriter::DEFAULT_GUARD
	Il2CppChar ___DEFAULT_GUARD_3;

public:
	inline static int32_t get_offset_of_START_END_CHARS_0() { return static_cast<int32_t>(offsetof(CodaBarWriter_t1728947151_StaticFields, ___START_END_CHARS_0)); }
	inline CharU5BU5D_t3324145743* get_START_END_CHARS_0() const { return ___START_END_CHARS_0; }
	inline CharU5BU5D_t3324145743** get_address_of_START_END_CHARS_0() { return &___START_END_CHARS_0; }
	inline void set_START_END_CHARS_0(CharU5BU5D_t3324145743* value)
	{
		___START_END_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier(&___START_END_CHARS_0, value);
	}

	inline static int32_t get_offset_of_ALT_START_END_CHARS_1() { return static_cast<int32_t>(offsetof(CodaBarWriter_t1728947151_StaticFields, ___ALT_START_END_CHARS_1)); }
	inline CharU5BU5D_t3324145743* get_ALT_START_END_CHARS_1() const { return ___ALT_START_END_CHARS_1; }
	inline CharU5BU5D_t3324145743** get_address_of_ALT_START_END_CHARS_1() { return &___ALT_START_END_CHARS_1; }
	inline void set_ALT_START_END_CHARS_1(CharU5BU5D_t3324145743* value)
	{
		___ALT_START_END_CHARS_1 = value;
		Il2CppCodeGenWriteBarrier(&___ALT_START_END_CHARS_1, value);
	}

	inline static int32_t get_offset_of_CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2() { return static_cast<int32_t>(offsetof(CodaBarWriter_t1728947151_StaticFields, ___CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2)); }
	inline CharU5BU5D_t3324145743* get_CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2() const { return ___CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2; }
	inline CharU5BU5D_t3324145743** get_address_of_CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2() { return &___CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2; }
	inline void set_CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2(CharU5BU5D_t3324145743* value)
	{
		___CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2 = value;
		Il2CppCodeGenWriteBarrier(&___CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2, value);
	}

	inline static int32_t get_offset_of_DEFAULT_GUARD_3() { return static_cast<int32_t>(offsetof(CodaBarWriter_t1728947151_StaticFields, ___DEFAULT_GUARD_3)); }
	inline Il2CppChar get_DEFAULT_GUARD_3() const { return ___DEFAULT_GUARD_3; }
	inline Il2CppChar* get_address_of_DEFAULT_GUARD_3() { return &___DEFAULT_GUARD_3; }
	inline void set_DEFAULT_GUARD_3(Il2CppChar value)
	{
		___DEFAULT_GUARD_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
