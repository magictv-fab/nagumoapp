﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler
struct OnPauseEventHandler_t3609693858;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPauseEventHandler__ctor_m1355149385 (OnPauseEventHandler_t3609693858 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler::Invoke()
extern "C"  void OnPauseEventHandler_Invoke_m2856445411 (OnPauseEventHandler_t3609693858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPauseEventHandler_BeginInvoke_m1795576512 (OnPauseEventHandler_t3609693858 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnPauseEventHandler_EndInvoke_m2082926297 (OnPauseEventHandler_t3609693858 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
