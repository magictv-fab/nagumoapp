﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// ZXing.QrCode.Internal.ErrorCorrectionLevel[]
struct ErrorCorrectionLevelU5BU5D_t232463391;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct  ErrorCorrectionLevel_t1225927610  : public Il2CppObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::bits
	int32_t ___bits_5;
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal_Renamed_Field
	int32_t ___ordinal_Renamed_Field_6;
	// System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_bits_5() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610, ___bits_5)); }
	inline int32_t get_bits_5() const { return ___bits_5; }
	inline int32_t* get_address_of_bits_5() { return &___bits_5; }
	inline void set_bits_5(int32_t value)
	{
		___bits_5 = value;
	}

	inline static int32_t get_offset_of_ordinal_Renamed_Field_6() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610, ___ordinal_Renamed_Field_6)); }
	inline int32_t get_ordinal_Renamed_Field_6() const { return ___ordinal_Renamed_Field_6; }
	inline int32_t* get_address_of_ordinal_Renamed_Field_6() { return &___ordinal_Renamed_Field_6; }
	inline void set_ordinal_Renamed_Field_6(int32_t value)
	{
		___ordinal_Renamed_Field_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier(&___name_7, value);
	}
};

struct ErrorCorrectionLevel_t1225927610_StaticFields
{
public:
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::L
	ErrorCorrectionLevel_t1225927610 * ___L_0;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::M
	ErrorCorrectionLevel_t1225927610 * ___M_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::Q
	ErrorCorrectionLevel_t1225927610 * ___Q_2;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::H
	ErrorCorrectionLevel_t1225927610 * ___H_3;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel[] ZXing.QrCode.Internal.ErrorCorrectionLevel::FOR_BITS
	ErrorCorrectionLevelU5BU5D_t232463391* ___FOR_BITS_4;

public:
	inline static int32_t get_offset_of_L_0() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610_StaticFields, ___L_0)); }
	inline ErrorCorrectionLevel_t1225927610 * get_L_0() const { return ___L_0; }
	inline ErrorCorrectionLevel_t1225927610 ** get_address_of_L_0() { return &___L_0; }
	inline void set_L_0(ErrorCorrectionLevel_t1225927610 * value)
	{
		___L_0 = value;
		Il2CppCodeGenWriteBarrier(&___L_0, value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610_StaticFields, ___M_1)); }
	inline ErrorCorrectionLevel_t1225927610 * get_M_1() const { return ___M_1; }
	inline ErrorCorrectionLevel_t1225927610 ** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(ErrorCorrectionLevel_t1225927610 * value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier(&___M_1, value);
	}

	inline static int32_t get_offset_of_Q_2() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610_StaticFields, ___Q_2)); }
	inline ErrorCorrectionLevel_t1225927610 * get_Q_2() const { return ___Q_2; }
	inline ErrorCorrectionLevel_t1225927610 ** get_address_of_Q_2() { return &___Q_2; }
	inline void set_Q_2(ErrorCorrectionLevel_t1225927610 * value)
	{
		___Q_2 = value;
		Il2CppCodeGenWriteBarrier(&___Q_2, value);
	}

	inline static int32_t get_offset_of_H_3() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610_StaticFields, ___H_3)); }
	inline ErrorCorrectionLevel_t1225927610 * get_H_3() const { return ___H_3; }
	inline ErrorCorrectionLevel_t1225927610 ** get_address_of_H_3() { return &___H_3; }
	inline void set_H_3(ErrorCorrectionLevel_t1225927610 * value)
	{
		___H_3 = value;
		Il2CppCodeGenWriteBarrier(&___H_3, value);
	}

	inline static int32_t get_offset_of_FOR_BITS_4() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1225927610_StaticFields, ___FOR_BITS_4)); }
	inline ErrorCorrectionLevelU5BU5D_t232463391* get_FOR_BITS_4() const { return ___FOR_BITS_4; }
	inline ErrorCorrectionLevelU5BU5D_t232463391** get_address_of_FOR_BITS_4() { return &___FOR_BITS_4; }
	inline void set_FOR_BITS_4(ErrorCorrectionLevelU5BU5D_t232463391* value)
	{
		___FOR_BITS_4 = value;
		Il2CppCodeGenWriteBarrier(&___FOR_BITS_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
