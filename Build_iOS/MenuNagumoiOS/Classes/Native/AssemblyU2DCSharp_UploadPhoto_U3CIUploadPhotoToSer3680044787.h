﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// System.Object
struct Il2CppObject;
// UploadPhoto
struct UploadPhoto_t2718662801;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UploadPhoto/<IUploadPhotoToServer>c__Iterator84
struct  U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787  : public Il2CppObject
{
public:
	// System.String UploadPhoto/<IUploadPhotoToServer>c__Iterator84::localFileName
	String_t* ___localFileName_0;
	// System.String UploadPhoto/<IUploadPhotoToServer>c__Iterator84::uploadURL
	String_t* ___uploadURL_1;
	// UnityEngine.WWW UploadPhoto/<IUploadPhotoToServer>c__Iterator84::<localFile>__0
	WWW_t3134621005 * ___U3ClocalFileU3E__0_2;
	// UnityEngine.WWWForm UploadPhoto/<IUploadPhotoToServer>c__Iterator84::<postForm>__1
	WWWForm_t461342257 * ___U3CpostFormU3E__1_3;
	// UnityEngine.WWW UploadPhoto/<IUploadPhotoToServer>c__Iterator84::<upload>__2
	WWW_t3134621005 * ___U3CuploadU3E__2_4;
	// System.Int32 UploadPhoto/<IUploadPhotoToServer>c__Iterator84::$PC
	int32_t ___U24PC_5;
	// System.Object UploadPhoto/<IUploadPhotoToServer>c__Iterator84::$current
	Il2CppObject * ___U24current_6;
	// System.String UploadPhoto/<IUploadPhotoToServer>c__Iterator84::<$>localFileName
	String_t* ___U3CU24U3ElocalFileName_7;
	// System.String UploadPhoto/<IUploadPhotoToServer>c__Iterator84::<$>uploadURL
	String_t* ___U3CU24U3EuploadURL_8;
	// UploadPhoto UploadPhoto/<IUploadPhotoToServer>c__Iterator84::<>f__this
	UploadPhoto_t2718662801 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_localFileName_0() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___localFileName_0)); }
	inline String_t* get_localFileName_0() const { return ___localFileName_0; }
	inline String_t** get_address_of_localFileName_0() { return &___localFileName_0; }
	inline void set_localFileName_0(String_t* value)
	{
		___localFileName_0 = value;
		Il2CppCodeGenWriteBarrier(&___localFileName_0, value);
	}

	inline static int32_t get_offset_of_uploadURL_1() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___uploadURL_1)); }
	inline String_t* get_uploadURL_1() const { return ___uploadURL_1; }
	inline String_t** get_address_of_uploadURL_1() { return &___uploadURL_1; }
	inline void set_uploadURL_1(String_t* value)
	{
		___uploadURL_1 = value;
		Il2CppCodeGenWriteBarrier(&___uploadURL_1, value);
	}

	inline static int32_t get_offset_of_U3ClocalFileU3E__0_2() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U3ClocalFileU3E__0_2)); }
	inline WWW_t3134621005 * get_U3ClocalFileU3E__0_2() const { return ___U3ClocalFileU3E__0_2; }
	inline WWW_t3134621005 ** get_address_of_U3ClocalFileU3E__0_2() { return &___U3ClocalFileU3E__0_2; }
	inline void set_U3ClocalFileU3E__0_2(WWW_t3134621005 * value)
	{
		___U3ClocalFileU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalFileU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CpostFormU3E__1_3() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U3CpostFormU3E__1_3)); }
	inline WWWForm_t461342257 * get_U3CpostFormU3E__1_3() const { return ___U3CpostFormU3E__1_3; }
	inline WWWForm_t461342257 ** get_address_of_U3CpostFormU3E__1_3() { return &___U3CpostFormU3E__1_3; }
	inline void set_U3CpostFormU3E__1_3(WWWForm_t461342257 * value)
	{
		___U3CpostFormU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpostFormU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CuploadU3E__2_4() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U3CuploadU3E__2_4)); }
	inline WWW_t3134621005 * get_U3CuploadU3E__2_4() const { return ___U3CuploadU3E__2_4; }
	inline WWW_t3134621005 ** get_address_of_U3CuploadU3E__2_4() { return &___U3CuploadU3E__2_4; }
	inline void set_U3CuploadU3E__2_4(WWW_t3134621005 * value)
	{
		___U3CuploadU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuploadU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ElocalFileName_7() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U3CU24U3ElocalFileName_7)); }
	inline String_t* get_U3CU24U3ElocalFileName_7() const { return ___U3CU24U3ElocalFileName_7; }
	inline String_t** get_address_of_U3CU24U3ElocalFileName_7() { return &___U3CU24U3ElocalFileName_7; }
	inline void set_U3CU24U3ElocalFileName_7(String_t* value)
	{
		___U3CU24U3ElocalFileName_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ElocalFileName_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EuploadURL_8() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U3CU24U3EuploadURL_8)); }
	inline String_t* get_U3CU24U3EuploadURL_8() const { return ___U3CU24U3EuploadURL_8; }
	inline String_t** get_address_of_U3CU24U3EuploadURL_8() { return &___U3CU24U3EuploadURL_8; }
	inline void set_U3CU24U3EuploadURL_8(String_t* value)
	{
		___U3CU24U3EuploadURL_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EuploadURL_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787, ___U3CU3Ef__this_9)); }
	inline UploadPhoto_t2718662801 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline UploadPhoto_t2718662801 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(UploadPhoto_t2718662801 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
