﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WebCamProfile
struct WebCamProfile_t3136319864;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro1961690790.h"
#include "mscorlib_System_String7231557.h"

// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamProfile::get_Default()
extern "C"  ProfileData_t1961690790  WebCamProfile_get_Default_m3916349232 (WebCamProfile_t3136319864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamProfile::.ctor()
extern "C"  void WebCamProfile__ctor_m2951678797 (WebCamProfile_t3136319864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamProfile::GetProfile(System.String)
extern "C"  ProfileData_t1961690790  WebCamProfile_GetProfile_m2457954597 (WebCamProfile_t3136319864 * __this, String_t* ___webcamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamProfile::ProfileAvailable(System.String)
extern "C"  bool WebCamProfile_ProfileAvailable_m4003026711 (WebCamProfile_t3136319864 * __this, String_t* ___webcamName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
