﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code93Reader
struct  Code93Reader_t2183409032  : public OneDReader_t3436042911
{
public:
	// System.Text.StringBuilder ZXing.OneD.Code93Reader::decodeRowResult
	StringBuilder_t243639308 * ___decodeRowResult_5;
	// System.Int32[] ZXing.OneD.Code93Reader::counters
	Int32U5BU5D_t3230847821* ___counters_6;

public:
	inline static int32_t get_offset_of_decodeRowResult_5() { return static_cast<int32_t>(offsetof(Code93Reader_t2183409032, ___decodeRowResult_5)); }
	inline StringBuilder_t243639308 * get_decodeRowResult_5() const { return ___decodeRowResult_5; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowResult_5() { return &___decodeRowResult_5; }
	inline void set_decodeRowResult_5(StringBuilder_t243639308 * value)
	{
		___decodeRowResult_5 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowResult_5, value);
	}

	inline static int32_t get_offset_of_counters_6() { return static_cast<int32_t>(offsetof(Code93Reader_t2183409032, ___counters_6)); }
	inline Int32U5BU5D_t3230847821* get_counters_6() const { return ___counters_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_counters_6() { return &___counters_6; }
	inline void set_counters_6(Int32U5BU5D_t3230847821* value)
	{
		___counters_6 = value;
		Il2CppCodeGenWriteBarrier(&___counters_6, value);
	}
};

struct Code93Reader_t2183409032_StaticFields
{
public:
	// System.Char[] ZXing.OneD.Code93Reader::ALPHABET
	CharU5BU5D_t3324145743* ___ALPHABET_2;
	// System.Int32[] ZXing.OneD.Code93Reader::CHARACTER_ENCODINGS
	Int32U5BU5D_t3230847821* ___CHARACTER_ENCODINGS_3;
	// System.Int32 ZXing.OneD.Code93Reader::ASTERISK_ENCODING
	int32_t ___ASTERISK_ENCODING_4;

public:
	inline static int32_t get_offset_of_ALPHABET_2() { return static_cast<int32_t>(offsetof(Code93Reader_t2183409032_StaticFields, ___ALPHABET_2)); }
	inline CharU5BU5D_t3324145743* get_ALPHABET_2() const { return ___ALPHABET_2; }
	inline CharU5BU5D_t3324145743** get_address_of_ALPHABET_2() { return &___ALPHABET_2; }
	inline void set_ALPHABET_2(CharU5BU5D_t3324145743* value)
	{
		___ALPHABET_2 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHABET_2, value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_3() { return static_cast<int32_t>(offsetof(Code93Reader_t2183409032_StaticFields, ___CHARACTER_ENCODINGS_3)); }
	inline Int32U5BU5D_t3230847821* get_CHARACTER_ENCODINGS_3() const { return ___CHARACTER_ENCODINGS_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_CHARACTER_ENCODINGS_3() { return &___CHARACTER_ENCODINGS_3; }
	inline void set_CHARACTER_ENCODINGS_3(Int32U5BU5D_t3230847821* value)
	{
		___CHARACTER_ENCODINGS_3 = value;
		Il2CppCodeGenWriteBarrier(&___CHARACTER_ENCODINGS_3, value);
	}

	inline static int32_t get_offset_of_ASTERISK_ENCODING_4() { return static_cast<int32_t>(offsetof(Code93Reader_t2183409032_StaticFields, ___ASTERISK_ENCODING_4)); }
	inline int32_t get_ASTERISK_ENCODING_4() const { return ___ASTERISK_ENCODING_4; }
	inline int32_t* get_address_of_ASTERISK_ENCODING_4() { return &___ASTERISK_ENCODING_4; }
	inline void set_ASTERISK_ENCODING_4(int32_t value)
	{
		___ASTERISK_ENCODING_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
