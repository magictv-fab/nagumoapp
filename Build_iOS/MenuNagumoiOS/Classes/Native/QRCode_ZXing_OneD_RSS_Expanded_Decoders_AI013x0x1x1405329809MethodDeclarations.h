﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder
struct AI013x0x1xDecoder_t1405329809;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::.ctor(ZXing.Common.BitArray,System.String,System.String)
extern "C"  void AI013x0x1xDecoder__ctor_m161434821 (AI013x0x1xDecoder_t1405329809 * __this, BitArray_t4163851164 * ___information0, String_t* ___firstAIdigits1, String_t* ___dateCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::parseInformation()
extern "C"  String_t* AI013x0x1xDecoder_parseInformation_m4086652960 (AI013x0x1xDecoder_t1405329809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::encodeCompressedDate(System.Text.StringBuilder,System.Int32)
extern "C"  void AI013x0x1xDecoder_encodeCompressedDate_m3654910006 (AI013x0x1xDecoder_t1405329809 * __this, StringBuilder_t243639308 * ___buf0, int32_t ___currentPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern "C"  void AI013x0x1xDecoder_addWeightCode_m3675203473 (AI013x0x1xDecoder_t1405329809 * __this, StringBuilder_t243639308 * ___buf0, int32_t ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::checkWeight(System.Int32)
extern "C"  int32_t AI013x0x1xDecoder_checkWeight_m2350706561 (AI013x0x1xDecoder_t1405329809 * __this, int32_t ___weight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::.cctor()
extern "C"  void AI013x0x1xDecoder__cctor_m2760323341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
