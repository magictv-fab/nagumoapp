﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.DecodedObject
struct DecodedObject_t746289471;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::get_NewPosition()
extern "C"  int32_t DecodedObject_get_NewPosition_m1400206082 (DecodedObject_t746289471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::set_NewPosition(System.Int32)
extern "C"  void DecodedObject_set_NewPosition_m3648891885 (DecodedObject_t746289471 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::.ctor(System.Int32)
extern "C"  void DecodedObject__ctor_m973975811 (DecodedObject_t746289471 * __this, int32_t ___newPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
