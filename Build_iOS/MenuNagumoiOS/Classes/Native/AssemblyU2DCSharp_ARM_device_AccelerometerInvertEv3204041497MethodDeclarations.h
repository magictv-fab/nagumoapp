﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.AccelerometerInvertEventToggleOnOff
struct AccelerometerInvertEventToggleOnOff_t3204041497;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.device.AccelerometerInvertEventToggleOnOff::.ctor()
extern "C"  void AccelerometerInvertEventToggleOnOff__ctor_m2237881914 (AccelerometerInvertEventToggleOnOff_t3204041497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerInvertEventToggleOnOff::Start()
extern "C"  void AccelerometerInvertEventToggleOnOff_Start_m1185019706 (AccelerometerInvertEventToggleOnOff_t3204041497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerInvertEventToggleOnOff::_onMoveInit()
extern "C"  void AccelerometerInvertEventToggleOnOff__onMoveInit_m468625303 (AccelerometerInvertEventToggleOnOff_t3204041497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerInvertEventToggleOnOff::_onMoveEnd()
extern "C"  void AccelerometerInvertEventToggleOnOff__onMoveEnd_m2228173014 (AccelerometerInvertEventToggleOnOff_t3204041497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
