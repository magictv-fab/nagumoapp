﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARMMZGuiToutchController
struct ARMMZGuiToutchController_t3240755437;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_TouchControllerState2242910518.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMMZGuiToutchController
struct  ARMMZGuiToutchController_t3240755437  : public MonoBehaviour_t667441552
{
public:
	// TouchControllerState ARMMZGuiToutchController::_currentState
	int32_t ____currentState_2;
	// UnityEngine.Vector2 ARMMZGuiToutchController::_startClick
	Vector2_t4282066565  ____startClick_3;
	// UnityEngine.Vector2 ARMMZGuiToutchController::_start1Click
	Vector2_t4282066565  ____start1Click_4;
	// UnityEngine.Vector2 ARMMZGuiToutchController::_start2Click
	Vector2_t4282066565  ____start2Click_5;
	// UnityEngine.Vector2 ARMMZGuiToutchController::_startRotationClick
	Vector2_t4282066565  ____startRotationClick_6;
	// UnityEngine.Vector3 ARMMZGuiToutchController::_screenPoint
	Vector3_t4282066566  ____screenPoint_7;
	// UnityEngine.Vector3 ARMMZGuiToutchController::_offset
	Vector3_t4282066566  ____offset_8;
	// System.Single ARMMZGuiToutchController::_scaleAcurracy
	float ____scaleAcurracy_9;
	// System.Single ARMMZGuiToutchController::RotationAngleTolerance
	float ___RotationAngleTolerance_10;
	// System.Single ARMMZGuiToutchController::ScaleMaximumTolerance
	float ___ScaleMaximumTolerance_11;
	// System.Single ARMMZGuiToutchController::ScaleMinimumTolerance
	float ___ScaleMinimumTolerance_12;
	// System.Boolean ARMMZGuiToutchController::TapToSelect
	bool ___TapToSelect_13;
	// System.Single ARMMZGuiToutchController::_angle2Touches
	float ____angle2Touches_15;
	// System.Int32 ARMMZGuiToutchController::_touchCount
	int32_t ____touchCount_16;

public:
	inline static int32_t get_offset_of__currentState_2() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____currentState_2)); }
	inline int32_t get__currentState_2() const { return ____currentState_2; }
	inline int32_t* get_address_of__currentState_2() { return &____currentState_2; }
	inline void set__currentState_2(int32_t value)
	{
		____currentState_2 = value;
	}

	inline static int32_t get_offset_of__startClick_3() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____startClick_3)); }
	inline Vector2_t4282066565  get__startClick_3() const { return ____startClick_3; }
	inline Vector2_t4282066565 * get_address_of__startClick_3() { return &____startClick_3; }
	inline void set__startClick_3(Vector2_t4282066565  value)
	{
		____startClick_3 = value;
	}

	inline static int32_t get_offset_of__start1Click_4() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____start1Click_4)); }
	inline Vector2_t4282066565  get__start1Click_4() const { return ____start1Click_4; }
	inline Vector2_t4282066565 * get_address_of__start1Click_4() { return &____start1Click_4; }
	inline void set__start1Click_4(Vector2_t4282066565  value)
	{
		____start1Click_4 = value;
	}

	inline static int32_t get_offset_of__start2Click_5() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____start2Click_5)); }
	inline Vector2_t4282066565  get__start2Click_5() const { return ____start2Click_5; }
	inline Vector2_t4282066565 * get_address_of__start2Click_5() { return &____start2Click_5; }
	inline void set__start2Click_5(Vector2_t4282066565  value)
	{
		____start2Click_5 = value;
	}

	inline static int32_t get_offset_of__startRotationClick_6() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____startRotationClick_6)); }
	inline Vector2_t4282066565  get__startRotationClick_6() const { return ____startRotationClick_6; }
	inline Vector2_t4282066565 * get_address_of__startRotationClick_6() { return &____startRotationClick_6; }
	inline void set__startRotationClick_6(Vector2_t4282066565  value)
	{
		____startRotationClick_6 = value;
	}

	inline static int32_t get_offset_of__screenPoint_7() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____screenPoint_7)); }
	inline Vector3_t4282066566  get__screenPoint_7() const { return ____screenPoint_7; }
	inline Vector3_t4282066566 * get_address_of__screenPoint_7() { return &____screenPoint_7; }
	inline void set__screenPoint_7(Vector3_t4282066566  value)
	{
		____screenPoint_7 = value;
	}

	inline static int32_t get_offset_of__offset_8() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____offset_8)); }
	inline Vector3_t4282066566  get__offset_8() const { return ____offset_8; }
	inline Vector3_t4282066566 * get_address_of__offset_8() { return &____offset_8; }
	inline void set__offset_8(Vector3_t4282066566  value)
	{
		____offset_8 = value;
	}

	inline static int32_t get_offset_of__scaleAcurracy_9() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____scaleAcurracy_9)); }
	inline float get__scaleAcurracy_9() const { return ____scaleAcurracy_9; }
	inline float* get_address_of__scaleAcurracy_9() { return &____scaleAcurracy_9; }
	inline void set__scaleAcurracy_9(float value)
	{
		____scaleAcurracy_9 = value;
	}

	inline static int32_t get_offset_of_RotationAngleTolerance_10() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ___RotationAngleTolerance_10)); }
	inline float get_RotationAngleTolerance_10() const { return ___RotationAngleTolerance_10; }
	inline float* get_address_of_RotationAngleTolerance_10() { return &___RotationAngleTolerance_10; }
	inline void set_RotationAngleTolerance_10(float value)
	{
		___RotationAngleTolerance_10 = value;
	}

	inline static int32_t get_offset_of_ScaleMaximumTolerance_11() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ___ScaleMaximumTolerance_11)); }
	inline float get_ScaleMaximumTolerance_11() const { return ___ScaleMaximumTolerance_11; }
	inline float* get_address_of_ScaleMaximumTolerance_11() { return &___ScaleMaximumTolerance_11; }
	inline void set_ScaleMaximumTolerance_11(float value)
	{
		___ScaleMaximumTolerance_11 = value;
	}

	inline static int32_t get_offset_of_ScaleMinimumTolerance_12() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ___ScaleMinimumTolerance_12)); }
	inline float get_ScaleMinimumTolerance_12() const { return ___ScaleMinimumTolerance_12; }
	inline float* get_address_of_ScaleMinimumTolerance_12() { return &___ScaleMinimumTolerance_12; }
	inline void set_ScaleMinimumTolerance_12(float value)
	{
		___ScaleMinimumTolerance_12 = value;
	}

	inline static int32_t get_offset_of_TapToSelect_13() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ___TapToSelect_13)); }
	inline bool get_TapToSelect_13() const { return ___TapToSelect_13; }
	inline bool* get_address_of_TapToSelect_13() { return &___TapToSelect_13; }
	inline void set_TapToSelect_13(bool value)
	{
		___TapToSelect_13 = value;
	}

	inline static int32_t get_offset_of__angle2Touches_15() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____angle2Touches_15)); }
	inline float get__angle2Touches_15() const { return ____angle2Touches_15; }
	inline float* get_address_of__angle2Touches_15() { return &____angle2Touches_15; }
	inline void set__angle2Touches_15(float value)
	{
		____angle2Touches_15 = value;
	}

	inline static int32_t get_offset_of__touchCount_16() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437, ____touchCount_16)); }
	inline int32_t get__touchCount_16() const { return ____touchCount_16; }
	inline int32_t* get_address_of__touchCount_16() { return &____touchCount_16; }
	inline void set__touchCount_16(int32_t value)
	{
		____touchCount_16 = value;
	}
};

struct ARMMZGuiToutchController_t3240755437_StaticFields
{
public:
	// ARMMZGuiToutchController ARMMZGuiToutchController::SelectedObject
	ARMMZGuiToutchController_t3240755437 * ___SelectedObject_14;

public:
	inline static int32_t get_offset_of_SelectedObject_14() { return static_cast<int32_t>(offsetof(ARMMZGuiToutchController_t3240755437_StaticFields, ___SelectedObject_14)); }
	inline ARMMZGuiToutchController_t3240755437 * get_SelectedObject_14() const { return ___SelectedObject_14; }
	inline ARMMZGuiToutchController_t3240755437 ** get_address_of_SelectedObject_14() { return &___SelectedObject_14; }
	inline void set_SelectedObject_14(ARMMZGuiToutchController_t3240755437 * value)
	{
		___SelectedObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedObject_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
