﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.animation.GenericStartableComponentControllAbstract[]
struct GenericStartableComponentControllAbstractU5BU5D_t3957787650;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.components.GroupStartableComponentsManager
struct  GroupStartableComponentsManager_t3626371340  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// ARM.animation.GenericStartableComponentControllAbstract[] ARM.components.GroupStartableComponentsManager::_components
	GenericStartableComponentControllAbstractU5BU5D_t3957787650* ____components_8;
	// System.Int32 ARM.components.GroupStartableComponentsManager::_nextIndex
	int32_t ____nextIndex_9;

public:
	inline static int32_t get_offset_of__components_8() { return static_cast<int32_t>(offsetof(GroupStartableComponentsManager_t3626371340, ____components_8)); }
	inline GenericStartableComponentControllAbstractU5BU5D_t3957787650* get__components_8() const { return ____components_8; }
	inline GenericStartableComponentControllAbstractU5BU5D_t3957787650** get_address_of__components_8() { return &____components_8; }
	inline void set__components_8(GenericStartableComponentControllAbstractU5BU5D_t3957787650* value)
	{
		____components_8 = value;
		Il2CppCodeGenWriteBarrier(&____components_8, value);
	}

	inline static int32_t get_offset_of__nextIndex_9() { return static_cast<int32_t>(offsetof(GroupStartableComponentsManager_t3626371340, ____nextIndex_9)); }
	inline int32_t get__nextIndex_9() const { return ____nextIndex_9; }
	inline int32_t* get_address_of__nextIndex_9() { return &____nextIndex_9; }
	inline void set__nextIndex_9(int32_t value)
	{
		____nextIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
