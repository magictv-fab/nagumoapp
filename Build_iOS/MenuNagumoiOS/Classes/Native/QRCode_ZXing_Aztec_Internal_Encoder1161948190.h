﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Encoder
struct  Encoder_t1161948190  : public Il2CppObject
{
public:

public:
};

struct Encoder_t1161948190_StaticFields
{
public:
	// System.Int32[] ZXing.Aztec.Internal.Encoder::WORD_SIZE
	Int32U5BU5D_t3230847821* ___WORD_SIZE_0;

public:
	inline static int32_t get_offset_of_WORD_SIZE_0() { return static_cast<int32_t>(offsetof(Encoder_t1161948190_StaticFields, ___WORD_SIZE_0)); }
	inline Int32U5BU5D_t3230847821* get_WORD_SIZE_0() const { return ___WORD_SIZE_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_WORD_SIZE_0() { return &___WORD_SIZE_0; }
	inline void set_WORD_SIZE_0(Int32U5BU5D_t3230847821* value)
	{
		___WORD_SIZE_0 = value;
		Il2CppCodeGenWriteBarrier(&___WORD_SIZE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
