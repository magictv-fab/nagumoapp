﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenURL
struct OpenURL_t401448613;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OpenURL::.ctor()
extern "C"  void OpenURL__ctor_m4228069990 (OpenURL_t401448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenURL::Start()
extern "C"  void OpenURL_Start_m3175207782 (OpenURL_t401448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenURL::Update()
extern "C"  void OpenURL_Update_m3948012903 (OpenURL_t401448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenURL::GoURL()
extern "C"  void OpenURL_GoURL_m959878443 (OpenURL_t401448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenURL::GoURL(System.String)
extern "C"  void OpenURL_GoURL_m3414558647 (OpenURL_t401448613 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
