﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.Dimensions
struct Dimensions_t3285070981;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.PDF417.Internal.Dimensions::get_MinCols()
extern "C"  int32_t Dimensions_get_MinCols_m3393367621 (Dimensions_t3285070981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.Dimensions::get_MaxCols()
extern "C"  int32_t Dimensions_get_MaxCols_m2620889047 (Dimensions_t3285070981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.Dimensions::get_MinRows()
extern "C"  int32_t Dimensions_get_MinRows_m3823132587 (Dimensions_t3285070981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.Dimensions::get_MaxRows()
extern "C"  int32_t Dimensions_get_MaxRows_m3050654013 (Dimensions_t3285070981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
