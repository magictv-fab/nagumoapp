﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WallMovement
struct WallMovement_t2710429881;

#include "codegen/il2cpp-codegen.h"

// System.Void WallMovement::.ctor()
extern "C"  void WallMovement__ctor_m552129026 (WallMovement_t2710429881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallMovement::Start()
extern "C"  void WallMovement_Start_m3794234114 (WallMovement_t2710429881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WallMovement::Update()
extern "C"  void WallMovement_Update_m1662992715 (WallMovement_t2710429881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
