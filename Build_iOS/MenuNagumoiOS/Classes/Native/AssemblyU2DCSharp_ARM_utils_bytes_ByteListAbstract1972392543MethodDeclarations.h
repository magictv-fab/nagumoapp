﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler
struct OnReadyEventHandler_t1972392543;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnReadyEventHandler__ctor_m3582011270 (OnReadyEventHandler_t1972392543 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler::Invoke()
extern "C"  void OnReadyEventHandler_Invoke_m3445698400 (OnReadyEventHandler_t1972392543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnReadyEventHandler_BeginInvoke_m3574641571 (OnReadyEventHandler_t1972392543 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnReadyEventHandler_EndInvoke_m1290882198 (OnReadyEventHandler_t1972392543 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
