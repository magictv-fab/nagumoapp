﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Collections.Generic.List`1<ConfigurableListenerEventInterface>
struct List_1_t3353814812;
// System.Collections.Generic.Dictionary`2<System.String,ConfigurableListenerEventInterface>
struct Dictionary_2_t2806047630;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.GenericConfig
struct  GenericConfig_t3386406039  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// UnityEngine.GameObject[] InApp.SimpleEvents.GenericConfig::gameObjectsToFind
	GameObjectU5BU5D_t2662109048* ___gameObjectsToFind_2;
	// System.Collections.Generic.List`1<ConfigurableListenerEventInterface> InApp.SimpleEvents.GenericConfig::configurables
	List_1_t3353814812 * ___configurables_3;
	// System.Collections.Generic.Dictionary`2<System.String,ConfigurableListenerEventInterface> InApp.SimpleEvents.GenericConfig::_configurableComponents
	Dictionary_2_t2806047630 * ____configurableComponents_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> InApp.SimpleEvents.GenericConfig::DebugConfigurableComponents
	List_1_t747900261 * ___DebugConfigurableComponents_5;

public:
	inline static int32_t get_offset_of_gameObjectsToFind_2() { return static_cast<int32_t>(offsetof(GenericConfig_t3386406039, ___gameObjectsToFind_2)); }
	inline GameObjectU5BU5D_t2662109048* get_gameObjectsToFind_2() const { return ___gameObjectsToFind_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_gameObjectsToFind_2() { return &___gameObjectsToFind_2; }
	inline void set_gameObjectsToFind_2(GameObjectU5BU5D_t2662109048* value)
	{
		___gameObjectsToFind_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectsToFind_2, value);
	}

	inline static int32_t get_offset_of_configurables_3() { return static_cast<int32_t>(offsetof(GenericConfig_t3386406039, ___configurables_3)); }
	inline List_1_t3353814812 * get_configurables_3() const { return ___configurables_3; }
	inline List_1_t3353814812 ** get_address_of_configurables_3() { return &___configurables_3; }
	inline void set_configurables_3(List_1_t3353814812 * value)
	{
		___configurables_3 = value;
		Il2CppCodeGenWriteBarrier(&___configurables_3, value);
	}

	inline static int32_t get_offset_of__configurableComponents_4() { return static_cast<int32_t>(offsetof(GenericConfig_t3386406039, ____configurableComponents_4)); }
	inline Dictionary_2_t2806047630 * get__configurableComponents_4() const { return ____configurableComponents_4; }
	inline Dictionary_2_t2806047630 ** get_address_of__configurableComponents_4() { return &____configurableComponents_4; }
	inline void set__configurableComponents_4(Dictionary_2_t2806047630 * value)
	{
		____configurableComponents_4 = value;
		Il2CppCodeGenWriteBarrier(&____configurableComponents_4, value);
	}

	inline static int32_t get_offset_of_DebugConfigurableComponents_5() { return static_cast<int32_t>(offsetof(GenericConfig_t3386406039, ___DebugConfigurableComponents_5)); }
	inline List_1_t747900261 * get_DebugConfigurableComponents_5() const { return ___DebugConfigurableComponents_5; }
	inline List_1_t747900261 ** get_address_of_DebugConfigurableComponents_5() { return &___DebugConfigurableComponents_5; }
	inline void set_DebugConfigurableComponents_5(List_1_t747900261 * value)
	{
		___DebugConfigurableComponents_5 = value;
		Il2CppCodeGenWriteBarrier(&___DebugConfigurableComponents_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
