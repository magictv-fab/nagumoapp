﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMain/<corrigeVoltaRoleta>c__Iterator5A
struct U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameMain/<corrigeVoltaRoleta>c__Iterator5A::.ctor()
extern "C"  void U3CcorrigeVoltaRoletaU3Ec__Iterator5A__ctor_m4215454780 (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMain/<corrigeVoltaRoleta>c__Iterator5A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcorrigeVoltaRoletaU3Ec__Iterator5A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2883476704 (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMain/<corrigeVoltaRoleta>c__Iterator5A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcorrigeVoltaRoletaU3Ec__Iterator5A_System_Collections_IEnumerator_get_Current_m1210458228 (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMain/<corrigeVoltaRoleta>c__Iterator5A::MoveNext()
extern "C"  bool U3CcorrigeVoltaRoletaU3Ec__Iterator5A_MoveNext_m998186784 (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain/<corrigeVoltaRoleta>c__Iterator5A::Dispose()
extern "C"  void U3CcorrigeVoltaRoletaU3Ec__Iterator5A_Dispose_m796166521 (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain/<corrigeVoltaRoleta>c__Iterator5A::Reset()
extern "C"  void U3CcorrigeVoltaRoletaU3Ec__Iterator5A_Reset_m1861887721 (U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
