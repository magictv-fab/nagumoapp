﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.Decoder
struct Decoder_t16782150;
// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// ZXing.Aztec.Internal.AztecDetectorResult
struct AztecDetectorResult_t1662194462;
// System.String
struct String_t;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.String[]
struct StringU5BU5D_t4054002952;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_AztecDetectorResult1662194462.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Common.DecoderResult ZXing.Aztec.Internal.Decoder::decode(ZXing.Aztec.Internal.AztecDetectorResult)
extern "C"  DecoderResult_t3752650303 * Decoder_decode_m3961978452 (Decoder_t16782150 * __this, AztecDetectorResult_t1662194462 * ___detectorResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Aztec.Internal.Decoder::getEncodedData(System.Boolean[])
extern "C"  String_t* Decoder_getEncodedData_m716441461 (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3456302923* ___correctedBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.Decoder/Table ZXing.Aztec.Internal.Decoder::getTable(System.Char)
extern "C"  int32_t Decoder_getTable_m4196144478 (Il2CppObject * __this /* static, unused */, Il2CppChar ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Aztec.Internal.Decoder::getCharacter(System.String[],System.Int32)
extern "C"  String_t* Decoder_getCharacter_m1748663080 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___table0, int32_t ___code1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] ZXing.Aztec.Internal.Decoder::correctBits(System.Boolean[])
extern "C"  BooleanU5BU5D_t3456302923* Decoder_correctBits_m943412474 (Decoder_t16782150 * __this, BooleanU5BU5D_t3456302923* ___rawbits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] ZXing.Aztec.Internal.Decoder::extractBits(ZXing.Common.BitMatrix)
extern "C"  BooleanU5BU5D_t3456302923* Decoder_extractBits_m1964427687 (Decoder_t16782150 * __this, BitMatrix_t1058711404 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Decoder::readCode(System.Boolean[],System.Int32,System.Int32)
extern "C"  int32_t Decoder_readCode_m1829843265 (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3456302923* ___rawbits0, int32_t ___startIndex1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.Decoder::totalBitsInLayer(System.Int32,System.Boolean)
extern "C"  int32_t Decoder_totalBitsInLayer_m63243745 (Il2CppObject * __this /* static, unused */, int32_t ___layers0, bool ___compact1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Decoder::.ctor()
extern "C"  void Decoder__ctor_m227684329 (Decoder_t16782150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Decoder::.cctor()
extern "C"  void Decoder__cctor_m2281150692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
