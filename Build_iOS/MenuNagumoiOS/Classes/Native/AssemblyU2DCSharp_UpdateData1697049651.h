﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateData
struct  UpdateData_t1697049651  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject UpdateData::loadingObj
	GameObject_t3674682005 * ___loadingObj_3;

public:
	inline static int32_t get_offset_of_loadingObj_3() { return static_cast<int32_t>(offsetof(UpdateData_t1697049651, ___loadingObj_3)); }
	inline GameObject_t3674682005 * get_loadingObj_3() const { return ___loadingObj_3; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_3() { return &___loadingObj_3; }
	inline void set_loadingObj_3(GameObject_t3674682005 * value)
	{
		___loadingObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_3, value);
	}
};

struct UpdateData_t1697049651_StaticFields
{
public:
	// System.Boolean UpdateData::onUpdate
	bool ___onUpdate_2;

public:
	inline static int32_t get_offset_of_onUpdate_2() { return static_cast<int32_t>(offsetof(UpdateData_t1697049651_StaticFields, ___onUpdate_2)); }
	inline bool get_onUpdate_2() const { return ___onUpdate_2; }
	inline bool* get_address_of_onUpdate_2() { return &___onUpdate_2; }
	inline void set_onUpdate_2(bool value)
	{
		___onUpdate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
