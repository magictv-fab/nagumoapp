﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookData
struct  FacebookData_t4095115184  : public Il2CppObject
{
public:
	// System.String FacebookData::name
	String_t* ___name_0;
	// System.String FacebookData::id
	String_t* ___id_1;
	// System.String FacebookData::email
	String_t* ___email_2;
	// System.String FacebookData::birthday
	String_t* ___birthday_3;
	// System.String FacebookData::gender
	String_t* ___gender_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(FacebookData_t4095115184, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(FacebookData_t4095115184, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_email_2() { return static_cast<int32_t>(offsetof(FacebookData_t4095115184, ___email_2)); }
	inline String_t* get_email_2() const { return ___email_2; }
	inline String_t** get_address_of_email_2() { return &___email_2; }
	inline void set_email_2(String_t* value)
	{
		___email_2 = value;
		Il2CppCodeGenWriteBarrier(&___email_2, value);
	}

	inline static int32_t get_offset_of_birthday_3() { return static_cast<int32_t>(offsetof(FacebookData_t4095115184, ___birthday_3)); }
	inline String_t* get_birthday_3() const { return ___birthday_3; }
	inline String_t** get_address_of_birthday_3() { return &___birthday_3; }
	inline void set_birthday_3(String_t* value)
	{
		___birthday_3 = value;
		Il2CppCodeGenWriteBarrier(&___birthday_3, value);
	}

	inline static int32_t get_offset_of_gender_4() { return static_cast<int32_t>(offsetof(FacebookData_t4095115184, ___gender_4)); }
	inline String_t* get_gender_4() const { return ___gender_4; }
	inline String_t** get_address_of_gender_4() { return &___gender_4; }
	inline void set_gender_4(String_t* value)
	{
		___gender_4 = value;
		Il2CppCodeGenWriteBarrier(&___gender_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
