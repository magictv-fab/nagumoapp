﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IncrementalSliderController/OnTransformAxisEventHandler
struct OnTransformAxisEventHandler_t3424591110;
// UnityEngine.UI.Slider
struct Slider_t79469677;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IncrementalSliderController
struct  IncrementalSliderController_t153649975  : public MonoBehaviour_t667441552
{
public:
	// IncrementalSliderController/OnTransformAxisEventHandler IncrementalSliderController::onTransformAxis
	OnTransformAxisEventHandler_t3424591110 * ___onTransformAxis_2;
	// UnityEngine.UI.Slider IncrementalSliderController::_slider
	Slider_t79469677 * ____slider_3;
	// UnityEngine.UI.Text IncrementalSliderController::_multiplyFactorText
	Text_t9039225 * ____multiplyFactorText_4;
	// System.Single IncrementalSliderController::_value
	float ____value_5;

public:
	inline static int32_t get_offset_of_onTransformAxis_2() { return static_cast<int32_t>(offsetof(IncrementalSliderController_t153649975, ___onTransformAxis_2)); }
	inline OnTransformAxisEventHandler_t3424591110 * get_onTransformAxis_2() const { return ___onTransformAxis_2; }
	inline OnTransformAxisEventHandler_t3424591110 ** get_address_of_onTransformAxis_2() { return &___onTransformAxis_2; }
	inline void set_onTransformAxis_2(OnTransformAxisEventHandler_t3424591110 * value)
	{
		___onTransformAxis_2 = value;
		Il2CppCodeGenWriteBarrier(&___onTransformAxis_2, value);
	}

	inline static int32_t get_offset_of__slider_3() { return static_cast<int32_t>(offsetof(IncrementalSliderController_t153649975, ____slider_3)); }
	inline Slider_t79469677 * get__slider_3() const { return ____slider_3; }
	inline Slider_t79469677 ** get_address_of__slider_3() { return &____slider_3; }
	inline void set__slider_3(Slider_t79469677 * value)
	{
		____slider_3 = value;
		Il2CppCodeGenWriteBarrier(&____slider_3, value);
	}

	inline static int32_t get_offset_of__multiplyFactorText_4() { return static_cast<int32_t>(offsetof(IncrementalSliderController_t153649975, ____multiplyFactorText_4)); }
	inline Text_t9039225 * get__multiplyFactorText_4() const { return ____multiplyFactorText_4; }
	inline Text_t9039225 ** get_address_of__multiplyFactorText_4() { return &____multiplyFactorText_4; }
	inline void set__multiplyFactorText_4(Text_t9039225 * value)
	{
		____multiplyFactorText_4 = value;
		Il2CppCodeGenWriteBarrier(&____multiplyFactorText_4, value);
	}

	inline static int32_t get_offset_of__value_5() { return static_cast<int32_t>(offsetof(IncrementalSliderController_t153649975, ____value_5)); }
	inline float get__value_5() const { return ____value_5; }
	inline float* get_address_of__value_5() { return &____value_5; }
	inline void set__value_5(float value)
	{
		____value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
