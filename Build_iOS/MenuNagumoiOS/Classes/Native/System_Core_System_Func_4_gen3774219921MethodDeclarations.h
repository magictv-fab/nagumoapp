﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_4_gen2418545903MethodDeclarations.h"

// System.Void System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>::.ctor(System.Object,System.IntPtr)
#define Func_4__ctor_m3233632095(__this, ___object0, ___method1, method) ((  void (*) (Func_4_t3774219921 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_4__ctor_m1271633066_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>::Invoke(T1,T2,T3)
#define Func_4_Invoke_m2246812157(__this, ___arg10, ___arg21, ___arg32, method) ((  LuminanceSource_t1231523093 * (*) (Func_4_t3774219921 *, Il2CppObject *, int32_t, int32_t, const MethodInfo*))Func_4_Invoke_m1626437128_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Func_4_BeginInvoke_m2595321874(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Func_4_t3774219921 *, Il2CppObject *, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_4_BeginInvoke_m2187110173_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TResult System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>::EndInvoke(System.IAsyncResult)
#define Func_4_EndInvoke_m961397649(__this, ___result0, method) ((  LuminanceSource_t1231523093 * (*) (Func_4_t3774219921 *, Il2CppObject *, const MethodInfo*))Func_4_EndInvoke_m2013145180_gshared)(__this, ___result0, method)
