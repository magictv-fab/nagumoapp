﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider
struct  GUILayoutVerticalSlider_t1513124266  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::topValue
	FsmFloat_t2134102846 * ___topValue_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::bottomValue
	FsmFloat_t2134102846 * ___bottomValue_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_14;

public:
	inline static int32_t get_offset_of_floatVariable_11() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___floatVariable_11)); }
	inline FsmFloat_t2134102846 * get_floatVariable_11() const { return ___floatVariable_11; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_11() { return &___floatVariable_11; }
	inline void set_floatVariable_11(FsmFloat_t2134102846 * value)
	{
		___floatVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_11, value);
	}

	inline static int32_t get_offset_of_topValue_12() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___topValue_12)); }
	inline FsmFloat_t2134102846 * get_topValue_12() const { return ___topValue_12; }
	inline FsmFloat_t2134102846 ** get_address_of_topValue_12() { return &___topValue_12; }
	inline void set_topValue_12(FsmFloat_t2134102846 * value)
	{
		___topValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___topValue_12, value);
	}

	inline static int32_t get_offset_of_bottomValue_13() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___bottomValue_13)); }
	inline FsmFloat_t2134102846 * get_bottomValue_13() const { return ___bottomValue_13; }
	inline FsmFloat_t2134102846 ** get_address_of_bottomValue_13() { return &___bottomValue_13; }
	inline void set_bottomValue_13(FsmFloat_t2134102846 * value)
	{
		___bottomValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___bottomValue_13, value);
	}

	inline static int32_t get_offset_of_changedEvent_14() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___changedEvent_14)); }
	inline FsmEvent_t2133468028 * get_changedEvent_14() const { return ___changedEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_14() { return &___changedEvent_14; }
	inline void set_changedEvent_14(FsmEvent_t2133468028 * value)
	{
		___changedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
