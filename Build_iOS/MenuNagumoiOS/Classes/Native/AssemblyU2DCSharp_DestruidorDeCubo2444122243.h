﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestruidorDeCubo
struct  DestruidorDeCubo_t2444122243  : public MonoBehaviour_t667441552
{
public:
	// System.Single DestruidorDeCubo::TimeToDie
	float ___TimeToDie_2;
	// System.Single DestruidorDeCubo::timeLived
	float ___timeLived_3;

public:
	inline static int32_t get_offset_of_TimeToDie_2() { return static_cast<int32_t>(offsetof(DestruidorDeCubo_t2444122243, ___TimeToDie_2)); }
	inline float get_TimeToDie_2() const { return ___TimeToDie_2; }
	inline float* get_address_of_TimeToDie_2() { return &___TimeToDie_2; }
	inline void set_TimeToDie_2(float value)
	{
		___TimeToDie_2 = value;
	}

	inline static int32_t get_offset_of_timeLived_3() { return static_cast<int32_t>(offsetof(DestruidorDeCubo_t2444122243, ___timeLived_3)); }
	inline float get_timeLived_3() const { return ___timeLived_3; }
	inline float* get_address_of_timeLived_3() { return &___timeLived_3; }
	inline void set_timeLived_3(float value)
	{
		___timeLived_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
