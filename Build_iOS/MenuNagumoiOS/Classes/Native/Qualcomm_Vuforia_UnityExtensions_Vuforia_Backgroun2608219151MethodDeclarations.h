﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t2608219151;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
extern "C"  int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m4258794709 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
extern "C"  void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m3474274196 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
extern "C"  void BackgroundPlaneAbstractBehaviour_SetEditorValues_m3322524830 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, int32_t ___numDivisions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
extern "C"  bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m2122090200 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
extern "C"  void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2475563612 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, float ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
extern "C"  void BackgroundPlaneAbstractBehaviour_Start_m3981040190 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
extern "C"  void BackgroundPlaneAbstractBehaviour_Update_m3159013775 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
extern "C"  Quaternion_t1553702882  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m1758561873 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
extern "C"  void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m1621625277 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
extern "C"  void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2466457595 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
extern "C"  bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2427184698 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnBackgroundTextureChanged()
extern "C"  void BackgroundPlaneAbstractBehaviour_OnBackgroundTextureChanged_m1985951724 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C"  void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m2498978574 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C"  void BackgroundPlaneAbstractBehaviour__ctor_m738935102 (BackgroundPlaneAbstractBehaviour_t2608219151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
extern "C"  void BackgroundPlaneAbstractBehaviour__cctor_m950055471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
