﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.ImageTarget
struct ImageTarget_t3520455670;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t1188501543;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTr3340678586.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg1650423632.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetAbstractBehaviour
struct  ImageTargetAbstractBehaviour_t2347335889  : public DataSetTrackableBehaviour_t3340678586
{
public:
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mAspectRatio
	float ___mAspectRatio_20;
	// Vuforia.ImageTargetType Vuforia.ImageTargetAbstractBehaviour::mImageTargetType
	int32_t ___mImageTargetType_21;
	// Vuforia.ImageTarget Vuforia.ImageTargetAbstractBehaviour::mImageTarget
	Il2CppObject * ___mImageTarget_22;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour> Vuforia.ImageTargetAbstractBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t1188501543 * ___mVirtualButtonBehaviours_23;

public:
	inline static int32_t get_offset_of_mAspectRatio_20() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t2347335889, ___mAspectRatio_20)); }
	inline float get_mAspectRatio_20() const { return ___mAspectRatio_20; }
	inline float* get_address_of_mAspectRatio_20() { return &___mAspectRatio_20; }
	inline void set_mAspectRatio_20(float value)
	{
		___mAspectRatio_20 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_21() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t2347335889, ___mImageTargetType_21)); }
	inline int32_t get_mImageTargetType_21() const { return ___mImageTargetType_21; }
	inline int32_t* get_address_of_mImageTargetType_21() { return &___mImageTargetType_21; }
	inline void set_mImageTargetType_21(int32_t value)
	{
		___mImageTargetType_21 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_22() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t2347335889, ___mImageTarget_22)); }
	inline Il2CppObject * get_mImageTarget_22() const { return ___mImageTarget_22; }
	inline Il2CppObject ** get_address_of_mImageTarget_22() { return &___mImageTarget_22; }
	inline void set_mImageTarget_22(Il2CppObject * value)
	{
		___mImageTarget_22 = value;
		Il2CppCodeGenWriteBarrier(&___mImageTarget_22, value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_23() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t2347335889, ___mVirtualButtonBehaviours_23)); }
	inline Dictionary_2_t1188501543 * get_mVirtualButtonBehaviours_23() const { return ___mVirtualButtonBehaviours_23; }
	inline Dictionary_2_t1188501543 ** get_address_of_mVirtualButtonBehaviours_23() { return &___mVirtualButtonBehaviours_23; }
	inline void set_mVirtualButtonBehaviours_23(Dictionary_2_t1188501543 * value)
	{
		___mVirtualButtonBehaviours_23 = value;
		Il2CppCodeGenWriteBarrier(&___mVirtualButtonBehaviours_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
