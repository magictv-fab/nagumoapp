﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResizePanel
struct ResizePanel_t788198800;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void ResizePanel::.ctor()
extern "C"  void ResizePanel__ctor_m268683227 (ResizePanel_t788198800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResizePanel::Awake()
extern "C"  void ResizePanel_Awake_m506288446 (ResizePanel_t788198800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResizePanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ResizePanel_OnPointerDown_m1183833989 (ResizePanel_t788198800 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResizePanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ResizePanel_OnDrag_m4097506818 (ResizePanel_t788198800 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
