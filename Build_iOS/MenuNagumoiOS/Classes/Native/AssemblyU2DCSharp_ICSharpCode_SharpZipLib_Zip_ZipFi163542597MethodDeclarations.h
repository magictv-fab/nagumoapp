﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator
struct ZipEntryEnumerator_t163542597;
// ICSharpCode.SharpZipLib.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t3672810022;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntry[])
extern "C"  void ZipEntryEnumerator__ctor_m2225560773 (ZipEntryEnumerator_t163542597 * __this, ZipEntryU5BU5D_t3672810022* ___entries0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::get_Current()
extern "C"  Il2CppObject * ZipEntryEnumerator_get_Current_m3139658959 (ZipEntryEnumerator_t163542597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::Reset()
extern "C"  void ZipEntryEnumerator_Reset_m2085488291 (ZipEntryEnumerator_t163542597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::MoveNext()
extern "C"  bool ZipEntryEnumerator_MoveNext_m1007255334 (ZipEntryEnumerator_t163542597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
