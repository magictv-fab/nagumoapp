﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetControllerCollisionFlags
struct  GetControllerCollisionFlags_t722913077  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::isGrounded
	FsmBool_t1075959796 * ___isGrounded_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::none
	FsmBool_t1075959796 * ___none_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::sides
	FsmBool_t1075959796 * ___sides_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::above
	FsmBool_t1075959796 * ___above_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::below
	FsmBool_t1075959796 * ___below_14;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::previousGo
	GameObject_t3674682005 * ___previousGo_15;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::controller
	CharacterController_t1618060635 * ___controller_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_isGrounded_10() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___isGrounded_10)); }
	inline FsmBool_t1075959796 * get_isGrounded_10() const { return ___isGrounded_10; }
	inline FsmBool_t1075959796 ** get_address_of_isGrounded_10() { return &___isGrounded_10; }
	inline void set_isGrounded_10(FsmBool_t1075959796 * value)
	{
		___isGrounded_10 = value;
		Il2CppCodeGenWriteBarrier(&___isGrounded_10, value);
	}

	inline static int32_t get_offset_of_none_11() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___none_11)); }
	inline FsmBool_t1075959796 * get_none_11() const { return ___none_11; }
	inline FsmBool_t1075959796 ** get_address_of_none_11() { return &___none_11; }
	inline void set_none_11(FsmBool_t1075959796 * value)
	{
		___none_11 = value;
		Il2CppCodeGenWriteBarrier(&___none_11, value);
	}

	inline static int32_t get_offset_of_sides_12() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___sides_12)); }
	inline FsmBool_t1075959796 * get_sides_12() const { return ___sides_12; }
	inline FsmBool_t1075959796 ** get_address_of_sides_12() { return &___sides_12; }
	inline void set_sides_12(FsmBool_t1075959796 * value)
	{
		___sides_12 = value;
		Il2CppCodeGenWriteBarrier(&___sides_12, value);
	}

	inline static int32_t get_offset_of_above_13() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___above_13)); }
	inline FsmBool_t1075959796 * get_above_13() const { return ___above_13; }
	inline FsmBool_t1075959796 ** get_address_of_above_13() { return &___above_13; }
	inline void set_above_13(FsmBool_t1075959796 * value)
	{
		___above_13 = value;
		Il2CppCodeGenWriteBarrier(&___above_13, value);
	}

	inline static int32_t get_offset_of_below_14() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___below_14)); }
	inline FsmBool_t1075959796 * get_below_14() const { return ___below_14; }
	inline FsmBool_t1075959796 ** get_address_of_below_14() { return &___below_14; }
	inline void set_below_14(FsmBool_t1075959796 * value)
	{
		___below_14 = value;
		Il2CppCodeGenWriteBarrier(&___below_14, value);
	}

	inline static int32_t get_offset_of_previousGo_15() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___previousGo_15)); }
	inline GameObject_t3674682005 * get_previousGo_15() const { return ___previousGo_15; }
	inline GameObject_t3674682005 ** get_address_of_previousGo_15() { return &___previousGo_15; }
	inline void set_previousGo_15(GameObject_t3674682005 * value)
	{
		___previousGo_15 = value;
		Il2CppCodeGenWriteBarrier(&___previousGo_15, value);
	}

	inline static int32_t get_offset_of_controller_16() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t722913077, ___controller_16)); }
	inline CharacterController_t1618060635 * get_controller_16() const { return ___controller_16; }
	inline CharacterController_t1618060635 ** get_address_of_controller_16() { return &___controller_16; }
	inline void set_controller_16(CharacterController_t1618060635 * value)
	{
		___controller_16 = value;
		Il2CppCodeGenWriteBarrier(&___controller_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
