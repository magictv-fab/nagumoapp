﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UploadPhoto
struct  UploadPhoto_t2718662801  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image UploadPhoto::image
	Image_t538875265 * ___image_2;
	// System.String UploadPhoto::imgPath
	String_t* ___imgPath_3;
	// System.String UploadPhoto::uploadUrl
	String_t* ___uploadUrl_4;
	// UnityEngine.GameObject UploadPhoto::loadingObj
	GameObject_t3674682005 * ___loadingObj_5;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(UploadPhoto_t2718662801, ___image_2)); }
	inline Image_t538875265 * get_image_2() const { return ___image_2; }
	inline Image_t538875265 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(Image_t538875265 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier(&___image_2, value);
	}

	inline static int32_t get_offset_of_imgPath_3() { return static_cast<int32_t>(offsetof(UploadPhoto_t2718662801, ___imgPath_3)); }
	inline String_t* get_imgPath_3() const { return ___imgPath_3; }
	inline String_t** get_address_of_imgPath_3() { return &___imgPath_3; }
	inline void set_imgPath_3(String_t* value)
	{
		___imgPath_3 = value;
		Il2CppCodeGenWriteBarrier(&___imgPath_3, value);
	}

	inline static int32_t get_offset_of_uploadUrl_4() { return static_cast<int32_t>(offsetof(UploadPhoto_t2718662801, ___uploadUrl_4)); }
	inline String_t* get_uploadUrl_4() const { return ___uploadUrl_4; }
	inline String_t** get_address_of_uploadUrl_4() { return &___uploadUrl_4; }
	inline void set_uploadUrl_4(String_t* value)
	{
		___uploadUrl_4 = value;
		Il2CppCodeGenWriteBarrier(&___uploadUrl_4, value);
	}

	inline static int32_t get_offset_of_loadingObj_5() { return static_cast<int32_t>(offsetof(UploadPhoto_t2718662801, ___loadingObj_5)); }
	inline GameObject_t3674682005 * get_loadingObj_5() const { return ___loadingObj_5; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_5() { return &___loadingObj_5; }
	inline void set_loadingObj_5(GameObject_t3674682005 * value)
	{
		___loadingObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
