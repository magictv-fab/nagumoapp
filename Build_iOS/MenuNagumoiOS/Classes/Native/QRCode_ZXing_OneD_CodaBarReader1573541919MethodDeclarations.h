﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.CodaBarReader
struct CodaBarReader_t1573541919;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.CodaBarReader::.ctor()
extern "C"  void CodaBarReader__ctor_m3742398804 (CodaBarReader_t1573541919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.CodaBarReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * CodaBarReader_decodeRow_m3989411853 (CodaBarReader_t1573541919 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.CodaBarReader::validatePattern(System.Int32)
extern "C"  bool CodaBarReader_validatePattern_m3915307337 (CodaBarReader_t1573541919 * __this, int32_t ___start0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.CodaBarReader::setCounters(ZXing.Common.BitArray)
extern "C"  bool CodaBarReader_setCounters_m3844334646 (CodaBarReader_t1573541919 * __this, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.CodaBarReader::counterAppend(System.Int32)
extern "C"  void CodaBarReader_counterAppend_m324935065 (CodaBarReader_t1573541919 * __this, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.CodaBarReader::findStartPattern()
extern "C"  int32_t CodaBarReader_findStartPattern_m448547973 (CodaBarReader_t1573541919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.CodaBarReader::arrayContains(System.Char[],System.Char)
extern "C"  bool CodaBarReader_arrayContains_m2051090396 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___array0, Il2CppChar ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.CodaBarReader::toNarrowWidePattern(System.Int32)
extern "C"  int32_t CodaBarReader_toNarrowWidePattern_m10467196 (CodaBarReader_t1573541919 * __this, int32_t ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.CodaBarReader::.cctor()
extern "C"  void CodaBarReader__cctor_m3863117017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
