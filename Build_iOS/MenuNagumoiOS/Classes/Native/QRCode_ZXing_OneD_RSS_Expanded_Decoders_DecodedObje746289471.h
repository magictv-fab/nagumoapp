﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedObject
struct  DecodedObject_t746289471  : public Il2CppObject
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::<NewPosition>k__BackingField
	int32_t ___U3CNewPositionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNewPositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DecodedObject_t746289471, ___U3CNewPositionU3Ek__BackingField_0)); }
	inline int32_t get_U3CNewPositionU3Ek__BackingField_0() const { return ___U3CNewPositionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNewPositionU3Ek__BackingField_0() { return &___U3CNewPositionU3Ek__BackingField_0; }
	inline void set_U3CNewPositionU3Ek__BackingField_0(int32_t value)
	{
		___U3CNewPositionU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
