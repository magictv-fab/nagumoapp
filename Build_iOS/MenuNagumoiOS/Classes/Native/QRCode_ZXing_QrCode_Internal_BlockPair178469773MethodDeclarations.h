﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.BlockPair
struct BlockPair_t178469773;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.BlockPair::.ctor(System.Byte[],System.Byte[])
extern "C"  void BlockPair__ctor_m1789724941 (BlockPair_t178469773 * __this, ByteU5BU5D_t4260760469* ___data0, ByteU5BU5D_t4260760469* ___errorCorrection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.QrCode.Internal.BlockPair::get_DataBytes()
extern "C"  ByteU5BU5D_t4260760469* BlockPair_get_DataBytes_m52437983 (BlockPair_t178469773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.QrCode.Internal.BlockPair::get_ErrorCorrectionBytes()
extern "C"  ByteU5BU5D_t4260760469* BlockPair_get_ErrorCorrectionBytes_m1391941609 (BlockPair_t178469773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
