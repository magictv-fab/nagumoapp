﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream
struct  UncompressedStream_t600883402  : public Stream_t1561764144
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::baseStream_
	Stream_t1561764144 * ___baseStream__2;

public:
	inline static int32_t get_offset_of_baseStream__2() { return static_cast<int32_t>(offsetof(UncompressedStream_t600883402, ___baseStream__2)); }
	inline Stream_t1561764144 * get_baseStream__2() const { return ___baseStream__2; }
	inline Stream_t1561764144 ** get_address_of_baseStream__2() { return &___baseStream__2; }
	inline void set_baseStream__2(Stream_t1561764144 * value)
	{
		___baseStream__2 = value;
		Il2CppCodeGenWriteBarrier(&___baseStream__2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
