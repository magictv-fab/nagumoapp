﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3523361801;

#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr537202536.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_GZip_GZip278189375.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.GZip.GZipOutputStream
struct  GZipOutputStream_t2632250695  : public DeflaterOutputStream_t537202536
{
public:
	// ICSharpCode.SharpZipLib.Checksums.Crc32 ICSharpCode.SharpZipLib.GZip.GZipOutputStream::crc
	Crc32_t3523361801 * ___crc_11;
	// ICSharpCode.SharpZipLib.GZip.GZipOutputStream/OutputState ICSharpCode.SharpZipLib.GZip.GZipOutputStream::state_
	int32_t ___state__12;

public:
	inline static int32_t get_offset_of_crc_11() { return static_cast<int32_t>(offsetof(GZipOutputStream_t2632250695, ___crc_11)); }
	inline Crc32_t3523361801 * get_crc_11() const { return ___crc_11; }
	inline Crc32_t3523361801 ** get_address_of_crc_11() { return &___crc_11; }
	inline void set_crc_11(Crc32_t3523361801 * value)
	{
		___crc_11 = value;
		Il2CppCodeGenWriteBarrier(&___crc_11, value);
	}

	inline static int32_t get_offset_of_state__12() { return static_cast<int32_t>(offsetof(GZipOutputStream_t2632250695, ___state__12)); }
	inline int32_t get_state__12() const { return ___state__12; }
	inline int32_t* get_address_of_state__12() { return &___state__12; }
	inline void set_state__12(int32_t value)
	{
		___state__12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
