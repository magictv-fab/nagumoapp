﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowAboutControllerScript
struct MagicTVWindowAboutControllerScript_t2586885333;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVWindowAboutControllerScript::.ctor()
extern "C"  void MagicTVWindowAboutControllerScript__ctor_m1042330470 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::FacebookClick()
extern "C"  void MagicTVWindowAboutControllerScript_FacebookClick_m2392775526 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::TwitterClick()
extern "C"  void MagicTVWindowAboutControllerScript_TwitterClick_m2744115635 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::SiteClick()
extern "C"  void MagicTVWindowAboutControllerScript_SiteClick_m4288703813 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::EmailClick()
extern "C"  void MagicTVWindowAboutControllerScript_EmailClick_m998798250 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::YouTubeChannelClick()
extern "C"  void MagicTVWindowAboutControllerScript_YouTubeChannelClick_m768128620 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::ClearCacheClick()
extern "C"  void MagicTVWindowAboutControllerScript_ClearCacheClick_m3902874967 (MagicTVWindowAboutControllerScript_t2586885333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::SetPINText(System.String)
extern "C"  void MagicTVWindowAboutControllerScript_SetPINText_m1556400836 (MagicTVWindowAboutControllerScript_t2586885333 * __this, String_t* ___pin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAboutControllerScript::SetVersionText(System.String)
extern "C"  void MagicTVWindowAboutControllerScript_SetVersionText_m2875894817 (MagicTVWindowAboutControllerScript_t2586885333 * __this, String_t* ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
