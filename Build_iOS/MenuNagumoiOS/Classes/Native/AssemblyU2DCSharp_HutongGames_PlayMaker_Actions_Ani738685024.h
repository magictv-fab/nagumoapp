﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorFollow
struct  AnimatorFollow_t738685024  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorFollow::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.AnimatorFollow::target
	FsmGameObject_t1697147867 * ___target_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorFollow::minimumDistance
	FsmFloat_t2134102846 * ___minimumDistance_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorFollow::speedDampTime
	FsmFloat_t2134102846 * ___speedDampTime_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorFollow::directionDampTime
	FsmFloat_t2134102846 * ___directionDampTime_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.AnimatorFollow::_go
	GameObject_t3674682005 * ____go_14;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.AnimatorFollow::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorFollow::avatar
	Animator_t2776330603 * ___avatar_16;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.AnimatorFollow::controller
	CharacterController_t1618060635 * ___controller_17;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___target_10)); }
	inline FsmGameObject_t1697147867 * get_target_10() const { return ___target_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(FsmGameObject_t1697147867 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier(&___target_10, value);
	}

	inline static int32_t get_offset_of_minimumDistance_11() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___minimumDistance_11)); }
	inline FsmFloat_t2134102846 * get_minimumDistance_11() const { return ___minimumDistance_11; }
	inline FsmFloat_t2134102846 ** get_address_of_minimumDistance_11() { return &___minimumDistance_11; }
	inline void set_minimumDistance_11(FsmFloat_t2134102846 * value)
	{
		___minimumDistance_11 = value;
		Il2CppCodeGenWriteBarrier(&___minimumDistance_11, value);
	}

	inline static int32_t get_offset_of_speedDampTime_12() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___speedDampTime_12)); }
	inline FsmFloat_t2134102846 * get_speedDampTime_12() const { return ___speedDampTime_12; }
	inline FsmFloat_t2134102846 ** get_address_of_speedDampTime_12() { return &___speedDampTime_12; }
	inline void set_speedDampTime_12(FsmFloat_t2134102846 * value)
	{
		___speedDampTime_12 = value;
		Il2CppCodeGenWriteBarrier(&___speedDampTime_12, value);
	}

	inline static int32_t get_offset_of_directionDampTime_13() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___directionDampTime_13)); }
	inline FsmFloat_t2134102846 * get_directionDampTime_13() const { return ___directionDampTime_13; }
	inline FsmFloat_t2134102846 ** get_address_of_directionDampTime_13() { return &___directionDampTime_13; }
	inline void set_directionDampTime_13(FsmFloat_t2134102846 * value)
	{
		___directionDampTime_13 = value;
		Il2CppCodeGenWriteBarrier(&___directionDampTime_13, value);
	}

	inline static int32_t get_offset_of__go_14() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ____go_14)); }
	inline GameObject_t3674682005 * get__go_14() const { return ____go_14; }
	inline GameObject_t3674682005 ** get_address_of__go_14() { return &____go_14; }
	inline void set__go_14(GameObject_t3674682005 * value)
	{
		____go_14 = value;
		Il2CppCodeGenWriteBarrier(&____go_14, value);
	}

	inline static int32_t get_offset_of__animatorProxy_15() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ____animatorProxy_15)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_15() const { return ____animatorProxy_15; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_15() { return &____animatorProxy_15; }
	inline void set__animatorProxy_15(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_15 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_15, value);
	}

	inline static int32_t get_offset_of_avatar_16() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___avatar_16)); }
	inline Animator_t2776330603 * get_avatar_16() const { return ___avatar_16; }
	inline Animator_t2776330603 ** get_address_of_avatar_16() { return &___avatar_16; }
	inline void set_avatar_16(Animator_t2776330603 * value)
	{
		___avatar_16 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_16, value);
	}

	inline static int32_t get_offset_of_controller_17() { return static_cast<int32_t>(offsetof(AnimatorFollow_t738685024, ___controller_17)); }
	inline CharacterController_t1618060635 * get_controller_17() const { return ___controller_17; }
	inline CharacterController_t1618060635 ** get_address_of_controller_17() { return &___controller_17; }
	inline void set_controller_17(CharacterController_t1618060635 * value)
	{
		___controller_17 = value;
		Il2CppCodeGenWriteBarrier(&___controller_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
