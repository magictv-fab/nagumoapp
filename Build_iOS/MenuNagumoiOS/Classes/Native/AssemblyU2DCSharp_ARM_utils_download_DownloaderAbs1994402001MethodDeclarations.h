﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler
struct OnDownloadCompleteEventHandler_t1994402001;
// System.Object
struct Il2CppObject;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDownloadCompleteEventHandler__ctor_m2299485432 (OnDownloadCompleteEventHandler_t1994402001 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler::Invoke(UnityEngine.WWW)
extern "C"  void OnDownloadCompleteEventHandler_Invoke_m866212064 (OnDownloadCompleteEventHandler_t1994402001 * __this, WWW_t3134621005 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler::BeginInvoke(UnityEngine.WWW,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDownloadCompleteEventHandler_BeginInvoke_m2475296965 (OnDownloadCompleteEventHandler_t1994402001 * __this, WWW_t3134621005 * ___request0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.DownloaderAbstract/OnDownloadCompleteEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnDownloadCompleteEventHandler_EndInvoke_m3754429192 (OnDownloadCompleteEventHandler_t1994402001 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
