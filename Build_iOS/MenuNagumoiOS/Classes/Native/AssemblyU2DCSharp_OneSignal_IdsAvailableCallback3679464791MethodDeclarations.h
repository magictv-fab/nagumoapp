﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/IdsAvailableCallback
struct IdsAvailableCallback_t3679464791;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/IdsAvailableCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void IdsAvailableCallback__ctor_m1874527358 (IdsAvailableCallback_t3679464791 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/IdsAvailableCallback::Invoke(System.String,System.String)
extern "C"  void IdsAvailableCallback_Invoke_m3717026406 (IdsAvailableCallback_t3679464791 * __this, String_t* ___playerID0, String_t* ___pushToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/IdsAvailableCallback::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IdsAvailableCallback_BeginInvoke_m363373747 (IdsAvailableCallback_t3679464791 * __this, String_t* ___playerID0, String_t* ___pushToken1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/IdsAvailableCallback::EndInvoke(System.IAsyncResult)
extern "C"  void IdsAvailableCallback_EndInvoke_m2396022158 (IdsAvailableCallback_t3679464791 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
