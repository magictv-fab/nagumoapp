﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmLog
struct FsmLog_t1596141350;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry>
struct List_1_t3983052136;
// HutongGames.PlayMaker.FsmLogEntry
struct FsmLogEntry_t2614866584;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry2614866584.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType537852544.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean HutongGames.PlayMaker.FsmLog::get_LoggingEnabled()
extern "C"  bool FsmLog_get_LoggingEnabled_m1266640218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_LoggingEnabled(System.Boolean)
extern "C"  void FsmLog_set_LoggingEnabled_m1472154829 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_MirrorDebugLog()
extern "C"  bool FsmLog_get_MirrorDebugLog_m4102960072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_MirrorDebugLog(System.Boolean)
extern "C"  void FsmLog_set_MirrorDebugLog_m1008026299 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_EnableDebugFlow()
extern "C"  bool FsmLog_get_EnableDebugFlow_m811792968 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_EnableDebugFlow(System.Boolean)
extern "C"  void FsmLog_set_EnableDebugFlow_m2148166083 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmLog::get_Fsm()
extern "C"  Fsm_t1527112426 * FsmLog_get_Fsm_m3008425891 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_Fsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmLog_set_Fsm_m1654475664 (FsmLog_t1596141350 * __this, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry> HutongGames.PlayMaker.FsmLog::get_Entries()
extern "C"  List_1_t3983052136 * FsmLog_get_Entries_m3074759723 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_Resized()
extern "C"  bool FsmLog_get_Resized_m1171618074 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_Resized(System.Boolean)
extern "C"  void FsmLog_set_Resized_m2274350357 (FsmLog_t1596141350 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::.ctor(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmLog__ctor_m3952510513 (FsmLog_t1596141350 * __this, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.FsmLog::GetLog(HutongGames.PlayMaker.Fsm)
extern "C"  FsmLog_t1596141350 * FsmLog_GetLog_m1870011304 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::ClearLogs()
extern "C"  void FsmLog_ClearLogs_m2417888935 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::AddEntry(HutongGames.PlayMaker.FsmLogEntry)
extern "C"  void FsmLog_AddEntry_m1775641288 (FsmLog_t1596141350 * __this, FsmLogEntry_t2614866584 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogEvent(HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogEvent_m3634974178 (FsmLog_t1596141350 * __this, FsmEvent_t2133468028 * ___fsmEvent0, FsmState_t2146334067 * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogSendEvent(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void FsmLog_LogSendEvent_m4220404055 (FsmLog_t1596141350 * __this, FsmState_t2146334067 * ___state0, FsmEvent_t2133468028 * ___fsmEvent1, FsmEventTarget_t1823904941 * ___eventTarget2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogExitState(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogExitState_m2692285513 (FsmLog_t1596141350 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogEnterState(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogEnterState_m2643461739 (FsmLog_t1596141350 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogTransition(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmTransition)
extern "C"  void FsmLog_LogTransition_m1191398858 (FsmLog_t1596141350 * __this, FsmState_t2146334067 * ___fromState0, FsmTransition_t3771611999 * ___transition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogBreak()
extern "C"  void FsmLog_LogBreak_m1164218898 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogAction(HutongGames.PlayMaker.FsmLogType,System.String)
extern "C"  void FsmLog_LogAction_m3070094743 (FsmLog_t1596141350 * __this, int32_t ___logType0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::Log(HutongGames.PlayMaker.FsmLogType,System.String)
extern "C"  void FsmLog_Log_m3566188993 (FsmLog_t1596141350 * __this, int32_t ___logType0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogStart(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogStart_m3118737304 (FsmLog_t1596141350 * __this, FsmState_t2146334067 * ___startState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogStop()
extern "C"  void FsmLog_LogStop_m526408305 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::Log(System.String)
extern "C"  void FsmLog_Log_m961892755 (FsmLog_t1596141350 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogWarning(System.String)
extern "C"  void FsmLog_LogWarning_m1852297843 (FsmLog_t1596141350 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogError(System.String)
extern "C"  void FsmLog_LogError_m413642247 (FsmLog_t1596141350 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLog::FormatUnityLogString(System.String)
extern "C"  String_t* FsmLog_FormatUnityLogString_m2614613297 (FsmLog_t1596141350 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::Clear()
extern "C"  void FsmLog_Clear_m940208152 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::OnDestroy()
extern "C"  void FsmLog_OnDestroy_m1869728358 (FsmLog_t1596141350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::.cctor()
extern "C"  void FsmLog__cctor_m1700042080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
