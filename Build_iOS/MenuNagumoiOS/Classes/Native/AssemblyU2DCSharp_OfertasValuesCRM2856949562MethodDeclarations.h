﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasValuesCRM
struct OfertasValuesCRM_t2856949562;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertasValuesCRM::.ctor()
extern "C"  void OfertasValuesCRM__ctor_m1787367585 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::Start()
extern "C"  void OfertasValuesCRM_Start_m734505377 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::Update()
extern "C"  void OfertasValuesCRM_Update_m1300682380 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::GoLink()
extern "C"  void OfertasValuesCRM_GoLink_m1103753349 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::SaveImage()
extern "C"  void OfertasValuesCRM_SaveImage_m2663007517 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::RemoverOferta()
extern "C"  void OfertasValuesCRM_RemoverOferta_m3727240254 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::Favoritar()
extern "C"  void OfertasValuesCRM_Favoritar_m2432582169 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::BtnAtivarDesativar()
extern "C"  void OfertasValuesCRM_BtnAtivarDesativar_m863406873 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::DesFavoritar(System.String)
extern "C"  void OfertasValuesCRM_DesFavoritar_m2091098743 (OfertasValuesCRM_t2856949562 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasValuesCRM::ExcluirFavorito(System.String)
extern "C"  Il2CppObject * OfertasValuesCRM_ExcluirFavorito_m3645810723 (OfertasValuesCRM_t2856949562 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasValuesCRM::IFavoritar(System.String)
extern "C"  Il2CppObject * OfertasValuesCRM_IFavoritar_m913376774 (OfertasValuesCRM_t2856949562 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM::Compartilhar()
extern "C"  void OfertasValuesCRM_Compartilhar_m1470229541 (OfertasValuesCRM_t2856949562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
