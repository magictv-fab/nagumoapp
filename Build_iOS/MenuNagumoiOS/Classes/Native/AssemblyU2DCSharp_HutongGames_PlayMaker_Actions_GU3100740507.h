﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3188246076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIButton
struct  GUIButton_t3100740507  : public GUIContentAction_t3188246076
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUIButton::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIButton::storeButtonState
	FsmBool_t1075959796 * ___storeButtonState_22;

public:
	inline static int32_t get_offset_of_sendEvent_21() { return static_cast<int32_t>(offsetof(GUIButton_t3100740507, ___sendEvent_21)); }
	inline FsmEvent_t2133468028 * get_sendEvent_21() const { return ___sendEvent_21; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_21() { return &___sendEvent_21; }
	inline void set_sendEvent_21(FsmEvent_t2133468028 * value)
	{
		___sendEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_21, value);
	}

	inline static int32_t get_offset_of_storeButtonState_22() { return static_cast<int32_t>(offsetof(GUIButton_t3100740507, ___storeButtonState_22)); }
	inline FsmBool_t1075959796 * get_storeButtonState_22() const { return ___storeButtonState_22; }
	inline FsmBool_t1075959796 ** get_address_of_storeButtonState_22() { return &___storeButtonState_22; }
	inline void set_storeButtonState_22(FsmBool_t1075959796 * value)
	{
		___storeButtonState_22 = value;
		Il2CppCodeGenWriteBarrier(&___storeButtonState_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
