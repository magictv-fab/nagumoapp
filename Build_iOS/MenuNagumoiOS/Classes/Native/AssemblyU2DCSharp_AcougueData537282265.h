﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CarnesData[]
struct CarnesDataU5BU5D_t2965870023;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AcougueData
struct  AcougueData_t537282265  : public Il2CppObject
{
public:
	// CarnesData[] AcougueData::carnes
	CarnesDataU5BU5D_t2965870023* ___carnes_0;
	// System.Int32 AcougueData::posicao
	int32_t ___posicao_1;

public:
	inline static int32_t get_offset_of_carnes_0() { return static_cast<int32_t>(offsetof(AcougueData_t537282265, ___carnes_0)); }
	inline CarnesDataU5BU5D_t2965870023* get_carnes_0() const { return ___carnes_0; }
	inline CarnesDataU5BU5D_t2965870023** get_address_of_carnes_0() { return &___carnes_0; }
	inline void set_carnes_0(CarnesDataU5BU5D_t2965870023* value)
	{
		___carnes_0 = value;
		Il2CppCodeGenWriteBarrier(&___carnes_0, value);
	}

	inline static int32_t get_offset_of_posicao_1() { return static_cast<int32_t>(offsetof(AcougueData_t537282265, ___posicao_1)); }
	inline int32_t get_posicao_1() const { return ___posicao_1; }
	inline int32_t* get_address_of_posicao_1() { return &___posicao_1; }
	inline void set_posicao_1(int32_t value)
	{
		___posicao_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
