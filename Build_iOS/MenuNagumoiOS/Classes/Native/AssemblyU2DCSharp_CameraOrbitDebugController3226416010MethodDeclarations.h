﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraOrbitDebugController
struct CameraOrbitDebugController_t3226416010;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraOrbitDebugController::.ctor()
extern "C"  void CameraOrbitDebugController__ctor_m1632211025 (CameraOrbitDebugController_t3226416010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraOrbitDebugController::Start()
extern "C"  void CameraOrbitDebugController_Start_m579348817 (CameraOrbitDebugController_t3226416010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraOrbitDebugController::Update()
extern "C"  void CameraOrbitDebugController_Update_m785796316 (CameraOrbitDebugController_t3226416010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
