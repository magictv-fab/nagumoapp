﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPunchRotation
struct iTweenPunchRotation_t1677375000;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::.ctor()
extern "C"  void iTweenPunchRotation__ctor_m2887002142 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::Reset()
extern "C"  void iTweenPunchRotation_Reset_m533435083 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::OnEnter()
extern "C"  void iTweenPunchRotation_OnEnter_m3546881141 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::OnExit()
extern "C"  void iTweenPunchRotation_OnExit_m816071811 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::DoiTween()
extern "C"  void iTweenPunchRotation_DoiTween_m4030265203 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
