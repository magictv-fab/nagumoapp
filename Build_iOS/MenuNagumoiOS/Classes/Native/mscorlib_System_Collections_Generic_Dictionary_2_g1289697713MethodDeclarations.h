﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Collections.Generic.IEqualityComparer`1<ZXing.BarcodeFormat>
struct IEqualityComparer_1_t697872925;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<ZXing.BarcodeFormat>
struct ICollection_1_t801428508;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Object>[]
struct KeyValuePair_2U5BU5D_t4058114434;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Object>>
struct IEnumerator_1_t3100343468;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Object>
struct KeyCollection_t2916457164;
// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>
struct ValueCollection_t4285270722;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21188478419.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2607021105.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3570390478_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3570390478(__this, method) ((  void (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2__ctor_m3570390478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2876064069_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2876064069(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2876064069_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m515196447_gshared (Dictionary_2_t1289697713 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m515196447(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1289697713 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m515196447_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3929179663_gshared (Dictionary_2_t1289697713 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3929179663(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1289697713 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3929179663_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768771968_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768771968(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768771968_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2427604756_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2427604756(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2427604756_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3128483586_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3128483586(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3128483586_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070376659_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070376659(__this, method) ((  bool (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070376659_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m446763974_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m446763974(__this, method) ((  bool (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m446763974_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m297087802_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m297087802(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m297087802_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1534981663_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1534981663(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1534981663_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1059107026_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1059107026(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1059107026_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m208714340_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m208714340(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m208714340_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1984335453_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1984335453(__this, ___key0, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1984335453_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m291352432_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m291352432(__this, method) ((  bool (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m291352432_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1605888796_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1605888796(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1605888796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2424470452_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2424470452(__this, method) ((  bool (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2424470452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3964512371_gshared (Dictionary_2_t1289697713 * __this, KeyValuePair_2_t1188478419  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3964512371(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1289697713 *, KeyValuePair_2_t1188478419 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3964512371_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1373357103_gshared (Dictionary_2_t1289697713 * __this, KeyValuePair_2_t1188478419  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1373357103(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1289697713 *, KeyValuePair_2_t1188478419 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1373357103_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1406828951_gshared (Dictionary_2_t1289697713 * __this, KeyValuePair_2U5BU5D_t4058114434* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1406828951(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1289697713 *, KeyValuePair_2U5BU5D_t4058114434*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1406828951_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1419455252_gshared (Dictionary_2_t1289697713 * __this, KeyValuePair_2_t1188478419  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1419455252(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1289697713 *, KeyValuePair_2_t1188478419 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1419455252_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3563291446_gshared (Dictionary_2_t1289697713 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3563291446(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3563291446_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2211260977_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2211260977(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2211260977_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4037686574_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4037686574(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4037686574_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1927728713_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1927728713(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1927728713_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3281021750_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3281021750(__this, method) ((  int32_t (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_get_Count_m3281021750_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3507498997_gshared (Dictionary_2_t1289697713 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3507498997(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3507498997_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2088422158_gshared (Dictionary_2_t1289697713 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2088422158(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1289697713 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2088422158_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m535772486_gshared (Dictionary_2_t1289697713 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m535772486(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1289697713 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m535772486_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2551372593_gshared (Dictionary_2_t1289697713 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2551372593(__this, ___size0, method) ((  void (*) (Dictionary_2_t1289697713 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2551372593_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3271150957_gshared (Dictionary_2_t1289697713 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3271150957(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3271150957_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1188478419  Dictionary_2_make_pair_m2020831673_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2020831673(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1188478419  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2020831673_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3996357149_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3996357149(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3996357149_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3201555485_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3201555485(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3201555485_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1382217154_gshared (Dictionary_2_t1289697713 * __this, KeyValuePair_2U5BU5D_t4058114434* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1382217154(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1289697713 *, KeyValuePair_2U5BU5D_t4058114434*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1382217154_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3365719082_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3365719082(__this, method) ((  void (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_Resize_m3365719082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1045431975_gshared (Dictionary_2_t1289697713 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1045431975(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1289697713 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1045431975_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m976523769_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m976523769(__this, method) ((  void (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_Clear_m976523769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2727337247_gshared (Dictionary_2_t1289697713 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2727337247(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1289697713 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2727337247_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3815344543_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3815344543(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3815344543_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2593148524_gshared (Dictionary_2_t1289697713 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2593148524(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1289697713 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2593148524_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3441332088_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3441332088(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3441332088_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1557423217_gshared (Dictionary_2_t1289697713 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1557423217(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1289697713 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1557423217_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3141052024_gshared (Dictionary_2_t1289697713 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3141052024(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1289697713 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3141052024_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::get_Keys()
extern "C"  KeyCollection_t2916457164 * Dictionary_2_get_Keys_m969535879_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m969535879(__this, method) ((  KeyCollection_t2916457164 * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_get_Keys_m969535879_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::get_Values()
extern "C"  ValueCollection_t4285270722 * Dictionary_2_get_Values_m3522920135_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3522920135(__this, method) ((  ValueCollection_t4285270722 * (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_get_Values_m3522920135_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3446216056_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3446216056(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3446216056_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1513869624_gshared (Dictionary_2_t1289697713 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1513869624(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1289697713 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1513869624_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m458852948_gshared (Dictionary_2_t1289697713 * __this, KeyValuePair_2_t1188478419  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m458852948(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1289697713 *, KeyValuePair_2_t1188478419 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m458852948_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2607021105  Dictionary_2_GetEnumerator_m1603070483_gshared (Dictionary_2_t1289697713 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1603070483(__this, method) ((  Enumerator_t2607021105  (*) (Dictionary_2_t1289697713 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1603070483_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m979069152_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m979069152(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m979069152_gshared)(__this /* static, unused */, ___key0, ___value1, method)
