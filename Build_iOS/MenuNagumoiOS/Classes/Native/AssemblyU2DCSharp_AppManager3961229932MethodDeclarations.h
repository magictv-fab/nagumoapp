﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppManager
struct AppManager_t3961229932;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void AppManager::.ctor()
extern "C"  void AppManager__ctor_m261095343 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::InitManager()
extern "C"  void AppManager_InitManager_m3816071434 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::DeInitManager()
extern "C"  void AppManager_DeInitManager_m3771685641 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::UpdateManager()
extern "C"  void AppManager_UpdateManager_m1715169905 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::Draw()
extern "C"  void AppManager_Draw_m1621387257 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::OnSingleTapped()
extern "C"  void AppManager_OnSingleTapped_m1024895592 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::OnDoubleTapped()
extern "C"  void AppManager_OnDoubleTapped_m3851290865 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::OnTappedOnGoToAboutPage()
extern "C"  void AppManager_OnTappedOnGoToAboutPage_m1609633564 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::OnBackButtonTapped()
extern "C"  void AppManager_OnBackButtonTapped_m17597081 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::OnTappedOnCloseButton()
extern "C"  void AppManager_OnTappedOnCloseButton_m917452077 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::OnAboutStartButtonTapped()
extern "C"  void AppManager_OnAboutStartButtonTapped_m360047559 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AppManager::LoadAboutPageForFirstTime()
extern "C"  Il2CppObject * AppManager_LoadAboutPageForFirstTime_m169883759 (AppManager_t3961229932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
