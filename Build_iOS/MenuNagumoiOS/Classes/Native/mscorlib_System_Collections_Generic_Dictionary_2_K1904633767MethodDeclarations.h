﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1904633767.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2293778114_gshared (Enumerator_t1904633767 * __this, Dictionary_2_t1289697713 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2293778114(__this, ___host0, method) ((  void (*) (Enumerator_t1904633767 *, Dictionary_2_t1289697713 *, const MethodInfo*))Enumerator__ctor_m2293778114_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1706572959_gshared (Enumerator_t1904633767 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1706572959(__this, method) ((  Il2CppObject * (*) (Enumerator_t1904633767 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1706572959_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1831883955_gshared (Enumerator_t1904633767 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1831883955(__this, method) ((  void (*) (Enumerator_t1904633767 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1831883955_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1413947876_gshared (Enumerator_t1904633767 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1413947876(__this, method) ((  void (*) (Enumerator_t1904633767 *, const MethodInfo*))Enumerator_Dispose_m1413947876_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3330165023_gshared (Enumerator_t1904633767 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3330165023(__this, method) ((  bool (*) (Enumerator_t1904633767 *, const MethodInfo*))Enumerator_MoveNext_m3330165023_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m568930133_gshared (Enumerator_t1904633767 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m568930133(__this, method) ((  int32_t (*) (Enumerator_t1904633767 *, const MethodInfo*))Enumerator_get_Current_m568930133_gshared)(__this, method)
