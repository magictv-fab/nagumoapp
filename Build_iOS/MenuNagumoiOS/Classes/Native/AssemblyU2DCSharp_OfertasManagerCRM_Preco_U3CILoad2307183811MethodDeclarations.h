﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12
struct U3CILoadImgsU3Ec__Iterator12_t2307183811;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator12__ctor_m2201066808 (U3CILoadImgsU3Ec__Iterator12_t2307183811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2949891492 (U3CILoadImgsU3Ec__Iterator12_t2307183811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m2800587064 (U3CILoadImgsU3Ec__Iterator12_t2307183811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator12_MoveNext_m2981442980 (U3CILoadImgsU3Ec__Iterator12_t2307183811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator12_Dispose_m1999575925 (U3CILoadImgsU3Ec__Iterator12_t2307183811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<ILoadImgs>c__Iterator12::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator12_Reset_m4142467045 (U3CILoadImgsU3Ec__Iterator12_t2307183811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
