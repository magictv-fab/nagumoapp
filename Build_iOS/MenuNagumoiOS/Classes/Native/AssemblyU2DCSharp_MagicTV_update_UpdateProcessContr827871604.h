﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.comm.ServerCommunicationAbstract
struct ServerCommunicationAbstract_t3244961639;
// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;
// MagicTV.abstracts.device.DeviceInfoAbstract
struct DeviceInfoAbstract_t471529032;
// MagicTV.abstracts.device.DeviceCacheFileManagerAbstract
struct DeviceCacheFileManagerAbstract_t121423689;
// MagicTV.abstracts.screens.LoadScreenAbstract
struct LoadScreenAbstract_t4059313920;
// ARM.abstracts.internet.InternetInfoAbstract
struct InternetInfoAbstract_t823636095;
// ARM.components.GroupComponentsManager
struct GroupComponentsManager_t3788191382;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.update.UpdateProcessController
struct  UpdateProcessController_t827871604  : public ProgressEventsAbstract_t2129719228
{
public:
	// System.Single MagicTV.update.UpdateProcessController::progressCacheWeight
	float ___progressCacheWeight_14;
	// System.Single MagicTV.update.UpdateProcessController::progressUpdateWeight
	float ___progressUpdateWeight_15;
	// System.Single MagicTV.update.UpdateProcessController::progressDeviceInfoWeight
	float ___progressDeviceInfoWeight_16;
	// MagicTV.abstracts.comm.ServerCommunicationAbstract MagicTV.update.UpdateProcessController::serverCommunication
	ServerCommunicationAbstract_t3244961639 * ___serverCommunication_17;
	// MagicTV.abstracts.device.DeviceFileInfoAbstract MagicTV.update.UpdateProcessController::deviceFileInfo
	DeviceFileInfoAbstract_t3788299492 * ___deviceFileInfo_18;
	// MagicTV.abstracts.device.DeviceInfoAbstract MagicTV.update.UpdateProcessController::deviceInfo
	DeviceInfoAbstract_t471529032 * ___deviceInfo_19;
	// MagicTV.abstracts.device.DeviceCacheFileManagerAbstract MagicTV.update.UpdateProcessController::cacheManager
	DeviceCacheFileManagerAbstract_t121423689 * ___cacheManager_20;
	// MagicTV.abstracts.screens.LoadScreenAbstract MagicTV.update.UpdateProcessController::loadScreen
	LoadScreenAbstract_t4059313920 * ___loadScreen_21;
	// ARM.abstracts.internet.InternetInfoAbstract MagicTV.update.UpdateProcessController::internetInfo
	InternetInfoAbstract_t823636095 * ___internetInfo_22;
	// ARM.components.GroupComponentsManager MagicTV.update.UpdateProcessController::groupComponent
	GroupComponentsManager_t3788191382 * ___groupComponent_23;
	// System.Boolean MagicTV.update.UpdateProcessController::_initing
	bool ____initing_24;
	// MagicTV.vo.ResultRevisionVO MagicTV.update.UpdateProcessController::_resultInfoToSave
	ResultRevisionVO_t2445597391 * ____resultInfoToSave_25;
	// System.Boolean MagicTV.update.UpdateProcessController::_serverCommRunning
	bool ____serverCommRunning_26;
	// System.String MagicTV.update.UpdateProcessController::_tokenToSave
	String_t* ____tokenToSave_27;
	// System.Boolean MagicTV.update.UpdateProcessController::deleteImmediately
	bool ___deleteImmediately_28;

public:
	inline static int32_t get_offset_of_progressCacheWeight_14() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___progressCacheWeight_14)); }
	inline float get_progressCacheWeight_14() const { return ___progressCacheWeight_14; }
	inline float* get_address_of_progressCacheWeight_14() { return &___progressCacheWeight_14; }
	inline void set_progressCacheWeight_14(float value)
	{
		___progressCacheWeight_14 = value;
	}

	inline static int32_t get_offset_of_progressUpdateWeight_15() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___progressUpdateWeight_15)); }
	inline float get_progressUpdateWeight_15() const { return ___progressUpdateWeight_15; }
	inline float* get_address_of_progressUpdateWeight_15() { return &___progressUpdateWeight_15; }
	inline void set_progressUpdateWeight_15(float value)
	{
		___progressUpdateWeight_15 = value;
	}

	inline static int32_t get_offset_of_progressDeviceInfoWeight_16() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___progressDeviceInfoWeight_16)); }
	inline float get_progressDeviceInfoWeight_16() const { return ___progressDeviceInfoWeight_16; }
	inline float* get_address_of_progressDeviceInfoWeight_16() { return &___progressDeviceInfoWeight_16; }
	inline void set_progressDeviceInfoWeight_16(float value)
	{
		___progressDeviceInfoWeight_16 = value;
	}

	inline static int32_t get_offset_of_serverCommunication_17() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___serverCommunication_17)); }
	inline ServerCommunicationAbstract_t3244961639 * get_serverCommunication_17() const { return ___serverCommunication_17; }
	inline ServerCommunicationAbstract_t3244961639 ** get_address_of_serverCommunication_17() { return &___serverCommunication_17; }
	inline void set_serverCommunication_17(ServerCommunicationAbstract_t3244961639 * value)
	{
		___serverCommunication_17 = value;
		Il2CppCodeGenWriteBarrier(&___serverCommunication_17, value);
	}

	inline static int32_t get_offset_of_deviceFileInfo_18() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___deviceFileInfo_18)); }
	inline DeviceFileInfoAbstract_t3788299492 * get_deviceFileInfo_18() const { return ___deviceFileInfo_18; }
	inline DeviceFileInfoAbstract_t3788299492 ** get_address_of_deviceFileInfo_18() { return &___deviceFileInfo_18; }
	inline void set_deviceFileInfo_18(DeviceFileInfoAbstract_t3788299492 * value)
	{
		___deviceFileInfo_18 = value;
		Il2CppCodeGenWriteBarrier(&___deviceFileInfo_18, value);
	}

	inline static int32_t get_offset_of_deviceInfo_19() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___deviceInfo_19)); }
	inline DeviceInfoAbstract_t471529032 * get_deviceInfo_19() const { return ___deviceInfo_19; }
	inline DeviceInfoAbstract_t471529032 ** get_address_of_deviceInfo_19() { return &___deviceInfo_19; }
	inline void set_deviceInfo_19(DeviceInfoAbstract_t471529032 * value)
	{
		___deviceInfo_19 = value;
		Il2CppCodeGenWriteBarrier(&___deviceInfo_19, value);
	}

	inline static int32_t get_offset_of_cacheManager_20() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___cacheManager_20)); }
	inline DeviceCacheFileManagerAbstract_t121423689 * get_cacheManager_20() const { return ___cacheManager_20; }
	inline DeviceCacheFileManagerAbstract_t121423689 ** get_address_of_cacheManager_20() { return &___cacheManager_20; }
	inline void set_cacheManager_20(DeviceCacheFileManagerAbstract_t121423689 * value)
	{
		___cacheManager_20 = value;
		Il2CppCodeGenWriteBarrier(&___cacheManager_20, value);
	}

	inline static int32_t get_offset_of_loadScreen_21() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___loadScreen_21)); }
	inline LoadScreenAbstract_t4059313920 * get_loadScreen_21() const { return ___loadScreen_21; }
	inline LoadScreenAbstract_t4059313920 ** get_address_of_loadScreen_21() { return &___loadScreen_21; }
	inline void set_loadScreen_21(LoadScreenAbstract_t4059313920 * value)
	{
		___loadScreen_21 = value;
		Il2CppCodeGenWriteBarrier(&___loadScreen_21, value);
	}

	inline static int32_t get_offset_of_internetInfo_22() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___internetInfo_22)); }
	inline InternetInfoAbstract_t823636095 * get_internetInfo_22() const { return ___internetInfo_22; }
	inline InternetInfoAbstract_t823636095 ** get_address_of_internetInfo_22() { return &___internetInfo_22; }
	inline void set_internetInfo_22(InternetInfoAbstract_t823636095 * value)
	{
		___internetInfo_22 = value;
		Il2CppCodeGenWriteBarrier(&___internetInfo_22, value);
	}

	inline static int32_t get_offset_of_groupComponent_23() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___groupComponent_23)); }
	inline GroupComponentsManager_t3788191382 * get_groupComponent_23() const { return ___groupComponent_23; }
	inline GroupComponentsManager_t3788191382 ** get_address_of_groupComponent_23() { return &___groupComponent_23; }
	inline void set_groupComponent_23(GroupComponentsManager_t3788191382 * value)
	{
		___groupComponent_23 = value;
		Il2CppCodeGenWriteBarrier(&___groupComponent_23, value);
	}

	inline static int32_t get_offset_of__initing_24() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ____initing_24)); }
	inline bool get__initing_24() const { return ____initing_24; }
	inline bool* get_address_of__initing_24() { return &____initing_24; }
	inline void set__initing_24(bool value)
	{
		____initing_24 = value;
	}

	inline static int32_t get_offset_of__resultInfoToSave_25() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ____resultInfoToSave_25)); }
	inline ResultRevisionVO_t2445597391 * get__resultInfoToSave_25() const { return ____resultInfoToSave_25; }
	inline ResultRevisionVO_t2445597391 ** get_address_of__resultInfoToSave_25() { return &____resultInfoToSave_25; }
	inline void set__resultInfoToSave_25(ResultRevisionVO_t2445597391 * value)
	{
		____resultInfoToSave_25 = value;
		Il2CppCodeGenWriteBarrier(&____resultInfoToSave_25, value);
	}

	inline static int32_t get_offset_of__serverCommRunning_26() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ____serverCommRunning_26)); }
	inline bool get__serverCommRunning_26() const { return ____serverCommRunning_26; }
	inline bool* get_address_of__serverCommRunning_26() { return &____serverCommRunning_26; }
	inline void set__serverCommRunning_26(bool value)
	{
		____serverCommRunning_26 = value;
	}

	inline static int32_t get_offset_of__tokenToSave_27() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ____tokenToSave_27)); }
	inline String_t* get__tokenToSave_27() const { return ____tokenToSave_27; }
	inline String_t** get_address_of__tokenToSave_27() { return &____tokenToSave_27; }
	inline void set__tokenToSave_27(String_t* value)
	{
		____tokenToSave_27 = value;
		Il2CppCodeGenWriteBarrier(&____tokenToSave_27, value);
	}

	inline static int32_t get_offset_of_deleteImmediately_28() { return static_cast<int32_t>(offsetof(UpdateProcessController_t827871604, ___deleteImmediately_28)); }
	inline bool get_deleteImmediately_28() const { return ___deleteImmediately_28; }
	inline bool* get_address_of_deleteImmediately_28() { return &___deleteImmediately_28; }
	inline void set_deleteImmediately_28(bool value)
	{
		___deleteImmediately_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
