﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonPlay
struct JsonPlay_t2384567388;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonPlay::.ctor()
extern "C"  void JsonPlay__ctor_m1523796415 (JsonPlay_t2384567388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
