﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Enumerator__ctor_m1201932670(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t2423213364 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Enumerator__ctor_m2035556075_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m3302681678(__this, ___parent0, method) ((  void (*) (Enumerator_t2423213364 *, LinkedList_1_t940058804 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1790499155(__this, method) ((  Il2CppObject * (*) (Enumerator_t2423213364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2585558941(__this, method) ((  void (*) (Enumerator_t2423213364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m998662446(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t2423213364 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3585730209_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
#define Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m397022489(__this, ___sender0, method) ((  void (*) (Enumerator_t2423213364 *, Il2CppObject *, const MethodInfo*))Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m407024268_gshared)(__this, ___sender0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::get_Current()
#define Enumerator_get_Current_m3751437148(__this, method) ((  State_t3065423635 * (*) (Enumerator_t2423213364 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::MoveNext()
#define Enumerator_MoveNext_m808573568(__this, method) ((  bool (*) (Enumerator_t2423213364 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.State>::Dispose()
#define Enumerator_Dispose_m523628218(__this, method) ((  void (*) (Enumerator_t2423213364 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
