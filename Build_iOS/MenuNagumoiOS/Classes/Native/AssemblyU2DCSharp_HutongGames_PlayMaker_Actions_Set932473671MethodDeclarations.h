﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTagsOnChildren
struct SetTagsOnChildren_t932473671;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::.ctor()
extern "C"  void SetTagsOnChildren__ctor_m1853747663 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::Reset()
extern "C"  void SetTagsOnChildren_Reset_m3795147900 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::OnEnter()
extern "C"  void SetTagsOnChildren_OnEnter_m2726772198 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::SetTag(UnityEngine.GameObject)
extern "C"  void SetTagsOnChildren_SetTag_m639115813 (SetTagsOnChildren_t932473671 * __this, GameObject_t3674682005 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::UpdateComponentFilter()
extern "C"  void SetTagsOnChildren_UpdateComponentFilter_m2711655097 (SetTagsOnChildren_t932473671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
