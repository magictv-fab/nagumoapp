﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PedidoData[]
struct PedidoDataU5BU5D_t3795515318;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoEnvioData
struct  PedidoEnvioData_t1309077304  : public Il2CppObject
{
public:
	// System.String PedidoEnvioData::login
	String_t* ___login_0;
	// System.String PedidoEnvioData::senha
	String_t* ___senha_1;
	// System.Int32 PedidoEnvioData::loja
	int32_t ___loja_2;
	// System.Int32 PedidoEnvioData::nota
	int32_t ___nota_3;
	// System.Boolean PedidoEnvioData::passarPeso
	bool ___passarPeso_4;
	// PedidoData[] PedidoEnvioData::pedidos
	PedidoDataU5BU5D_t3795515318* ___pedidos_5;
	// System.String PedidoEnvioData::codePedido
	String_t* ___codePedido_6;

public:
	inline static int32_t get_offset_of_login_0() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___login_0)); }
	inline String_t* get_login_0() const { return ___login_0; }
	inline String_t** get_address_of_login_0() { return &___login_0; }
	inline void set_login_0(String_t* value)
	{
		___login_0 = value;
		Il2CppCodeGenWriteBarrier(&___login_0, value);
	}

	inline static int32_t get_offset_of_senha_1() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___senha_1)); }
	inline String_t* get_senha_1() const { return ___senha_1; }
	inline String_t** get_address_of_senha_1() { return &___senha_1; }
	inline void set_senha_1(String_t* value)
	{
		___senha_1 = value;
		Il2CppCodeGenWriteBarrier(&___senha_1, value);
	}

	inline static int32_t get_offset_of_loja_2() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___loja_2)); }
	inline int32_t get_loja_2() const { return ___loja_2; }
	inline int32_t* get_address_of_loja_2() { return &___loja_2; }
	inline void set_loja_2(int32_t value)
	{
		___loja_2 = value;
	}

	inline static int32_t get_offset_of_nota_3() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___nota_3)); }
	inline int32_t get_nota_3() const { return ___nota_3; }
	inline int32_t* get_address_of_nota_3() { return &___nota_3; }
	inline void set_nota_3(int32_t value)
	{
		___nota_3 = value;
	}

	inline static int32_t get_offset_of_passarPeso_4() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___passarPeso_4)); }
	inline bool get_passarPeso_4() const { return ___passarPeso_4; }
	inline bool* get_address_of_passarPeso_4() { return &___passarPeso_4; }
	inline void set_passarPeso_4(bool value)
	{
		___passarPeso_4 = value;
	}

	inline static int32_t get_offset_of_pedidos_5() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___pedidos_5)); }
	inline PedidoDataU5BU5D_t3795515318* get_pedidos_5() const { return ___pedidos_5; }
	inline PedidoDataU5BU5D_t3795515318** get_address_of_pedidos_5() { return &___pedidos_5; }
	inline void set_pedidos_5(PedidoDataU5BU5D_t3795515318* value)
	{
		___pedidos_5 = value;
		Il2CppCodeGenWriteBarrier(&___pedidos_5, value);
	}

	inline static int32_t get_offset_of_codePedido_6() { return static_cast<int32_t>(offsetof(PedidoEnvioData_t1309077304, ___codePedido_6)); }
	inline String_t* get_codePedido_6() const { return ___codePedido_6; }
	inline String_t** get_address_of_codePedido_6() { return &___codePedido_6; }
	inline void set_codePedido_6(String_t* value)
	{
		___codePedido_6 = value;
		Il2CppCodeGenWriteBarrier(&___codePedido_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
