﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CopyPositionAngleFilter
struct  CopyPositionAngleFilter_t2952166253  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.GameObject CopyPositionAngleFilter::gameObjectToCopy
	GameObject_t3674682005 * ___gameObjectToCopy_15;
	// System.Boolean CopyPositionAngleFilter::readGlobalPosition
	bool ___readGlobalPosition_16;
	// System.Boolean CopyPositionAngleFilter::writeGlobalPosition
	bool ___writeGlobalPosition_17;
	// System.Boolean CopyPositionAngleFilter::copyPosition
	bool ___copyPosition_18;
	// System.Boolean CopyPositionAngleFilter::copyAngle
	bool ___copyAngle_19;
	// System.Boolean CopyPositionAngleFilter::_changeOnLateUpdate
	bool ____changeOnLateUpdate_20;
	// System.Boolean CopyPositionAngleFilter::TransformThisObjectToo
	bool ___TransformThisObjectToo_21;
	// UnityEngine.Quaternion CopyPositionAngleFilter::rotationToSend
	Quaternion_t1553702882  ___rotationToSend_22;
	// UnityEngine.Vector3 CopyPositionAngleFilter::positionToSend
	Vector3_t4282066566  ___positionToSend_23;
	// UnityEngine.Vector3 CopyPositionAngleFilter::debugCurrent
	Vector3_t4282066566  ___debugCurrent_24;
	// UnityEngine.Vector3 CopyPositionAngleFilter::debugNext
	Vector3_t4282066566  ___debugNext_25;

public:
	inline static int32_t get_offset_of_gameObjectToCopy_15() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___gameObjectToCopy_15)); }
	inline GameObject_t3674682005 * get_gameObjectToCopy_15() const { return ___gameObjectToCopy_15; }
	inline GameObject_t3674682005 ** get_address_of_gameObjectToCopy_15() { return &___gameObjectToCopy_15; }
	inline void set_gameObjectToCopy_15(GameObject_t3674682005 * value)
	{
		___gameObjectToCopy_15 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToCopy_15, value);
	}

	inline static int32_t get_offset_of_readGlobalPosition_16() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___readGlobalPosition_16)); }
	inline bool get_readGlobalPosition_16() const { return ___readGlobalPosition_16; }
	inline bool* get_address_of_readGlobalPosition_16() { return &___readGlobalPosition_16; }
	inline void set_readGlobalPosition_16(bool value)
	{
		___readGlobalPosition_16 = value;
	}

	inline static int32_t get_offset_of_writeGlobalPosition_17() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___writeGlobalPosition_17)); }
	inline bool get_writeGlobalPosition_17() const { return ___writeGlobalPosition_17; }
	inline bool* get_address_of_writeGlobalPosition_17() { return &___writeGlobalPosition_17; }
	inline void set_writeGlobalPosition_17(bool value)
	{
		___writeGlobalPosition_17 = value;
	}

	inline static int32_t get_offset_of_copyPosition_18() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___copyPosition_18)); }
	inline bool get_copyPosition_18() const { return ___copyPosition_18; }
	inline bool* get_address_of_copyPosition_18() { return &___copyPosition_18; }
	inline void set_copyPosition_18(bool value)
	{
		___copyPosition_18 = value;
	}

	inline static int32_t get_offset_of_copyAngle_19() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___copyAngle_19)); }
	inline bool get_copyAngle_19() const { return ___copyAngle_19; }
	inline bool* get_address_of_copyAngle_19() { return &___copyAngle_19; }
	inline void set_copyAngle_19(bool value)
	{
		___copyAngle_19 = value;
	}

	inline static int32_t get_offset_of__changeOnLateUpdate_20() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ____changeOnLateUpdate_20)); }
	inline bool get__changeOnLateUpdate_20() const { return ____changeOnLateUpdate_20; }
	inline bool* get_address_of__changeOnLateUpdate_20() { return &____changeOnLateUpdate_20; }
	inline void set__changeOnLateUpdate_20(bool value)
	{
		____changeOnLateUpdate_20 = value;
	}

	inline static int32_t get_offset_of_TransformThisObjectToo_21() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___TransformThisObjectToo_21)); }
	inline bool get_TransformThisObjectToo_21() const { return ___TransformThisObjectToo_21; }
	inline bool* get_address_of_TransformThisObjectToo_21() { return &___TransformThisObjectToo_21; }
	inline void set_TransformThisObjectToo_21(bool value)
	{
		___TransformThisObjectToo_21 = value;
	}

	inline static int32_t get_offset_of_rotationToSend_22() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___rotationToSend_22)); }
	inline Quaternion_t1553702882  get_rotationToSend_22() const { return ___rotationToSend_22; }
	inline Quaternion_t1553702882 * get_address_of_rotationToSend_22() { return &___rotationToSend_22; }
	inline void set_rotationToSend_22(Quaternion_t1553702882  value)
	{
		___rotationToSend_22 = value;
	}

	inline static int32_t get_offset_of_positionToSend_23() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___positionToSend_23)); }
	inline Vector3_t4282066566  get_positionToSend_23() const { return ___positionToSend_23; }
	inline Vector3_t4282066566 * get_address_of_positionToSend_23() { return &___positionToSend_23; }
	inline void set_positionToSend_23(Vector3_t4282066566  value)
	{
		___positionToSend_23 = value;
	}

	inline static int32_t get_offset_of_debugCurrent_24() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___debugCurrent_24)); }
	inline Vector3_t4282066566  get_debugCurrent_24() const { return ___debugCurrent_24; }
	inline Vector3_t4282066566 * get_address_of_debugCurrent_24() { return &___debugCurrent_24; }
	inline void set_debugCurrent_24(Vector3_t4282066566  value)
	{
		___debugCurrent_24 = value;
	}

	inline static int32_t get_offset_of_debugNext_25() { return static_cast<int32_t>(offsetof(CopyPositionAngleFilter_t2952166253, ___debugNext_25)); }
	inline Vector3_t4282066566  get_debugNext_25() const { return ___debugNext_25; }
	inline Vector3_t4282066566 * get_address_of_debugNext_25() { return &___debugNext_25; }
	inline void set_debugNext_25(Vector3_t4282066566  value)
	{
		___debugNext_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
