﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3523361801;

#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr928059325.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.GZip.GZipInputStream
struct  GZipInputStream_t2489725366  : public InflaterInputStream_t928059325
{
public:
	// ICSharpCode.SharpZipLib.Checksums.Crc32 ICSharpCode.SharpZipLib.GZip.GZipInputStream::crc
	Crc32_t3523361801 * ___crc_8;
	// System.Boolean ICSharpCode.SharpZipLib.GZip.GZipInputStream::readGZIPHeader
	bool ___readGZIPHeader_9;

public:
	inline static int32_t get_offset_of_crc_8() { return static_cast<int32_t>(offsetof(GZipInputStream_t2489725366, ___crc_8)); }
	inline Crc32_t3523361801 * get_crc_8() const { return ___crc_8; }
	inline Crc32_t3523361801 ** get_address_of_crc_8() { return &___crc_8; }
	inline void set_crc_8(Crc32_t3523361801 * value)
	{
		___crc_8 = value;
		Il2CppCodeGenWriteBarrier(&___crc_8, value);
	}

	inline static int32_t get_offset_of_readGZIPHeader_9() { return static_cast<int32_t>(offsetof(GZipInputStream_t2489725366, ___readGZIPHeader_9)); }
	inline bool get_readGZIPHeader_9() const { return ___readGZIPHeader_9; }
	inline bool* get_address_of_readGZIPHeader_9() { return &___readGZIPHeader_9; }
	inline void set_readGZIPHeader_9(bool value)
	{
		___readGZIPHeader_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
