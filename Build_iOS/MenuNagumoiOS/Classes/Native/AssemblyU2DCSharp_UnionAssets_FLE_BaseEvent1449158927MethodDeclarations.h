﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnionAssets.FLE.BaseEvent
struct BaseEvent_t1449158927;

#include "codegen/il2cpp-codegen.h"

// System.Void UnionAssets.FLE.BaseEvent::.ctor()
extern "C"  void BaseEvent__ctor_m3146456525 (BaseEvent_t1449158927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
