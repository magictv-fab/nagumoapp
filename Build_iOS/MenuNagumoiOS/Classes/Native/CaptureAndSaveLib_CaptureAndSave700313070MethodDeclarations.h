﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureAndSave
struct CaptureAndSave_t700313070;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "CaptureAndSaveLib_ImageType1125820181.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "CaptureAndSaveLib_WatermarkAlignment1574127103.h"

// System.Void CaptureAndSave::.ctor()
extern "C"  void CaptureAndSave__ctor_m2839589910 (CaptureAndSave_t700313070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::SetAlbumPath(System.String)
extern "C"  void CaptureAndSave_SetAlbumPath_m1200194850 (CaptureAndSave_t700313070 * __this, String_t* ___albumPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,UnityEngine.Camera,ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m1006194421 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,UnityEngine.Camera,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m1564711100 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, Texture2D_t3884108195 * ___watermark4, int32_t ___align5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,UnityEngine.Camera,System.String,ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m2689293131 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, String_t* ___path3, int32_t ___imgType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,UnityEngine.Camera,System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m3081692454 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, String_t* ___path3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.String,ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m3439077541 (CaptureAndSave_t700313070 * __this, String_t* ___path0, int32_t ___imgType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m907896076 (CaptureAndSave_t700313070 * __this, String_t* ___path0, int32_t ___imgType1, Texture2D_t3884108195 * ___watermark2, int32_t ___align3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,System.Int32,System.Int32,System.String,ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m3109496421 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, String_t* ___path4, int32_t ___imgType5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,System.Int32,System.Int32,System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m141221708 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, String_t* ___path4, int32_t ___imgType5, Texture2D_t3884108195 * ___watermark6, int32_t ___align7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::SaveTextureAtPath(UnityEngine.Texture2D,System.String,ImageType)
extern "C"  void CaptureAndSave_SaveTextureAtPath_m808158949 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, String_t* ___path1, int32_t ___imgType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::SaveTextureAtPath(UnityEngine.Texture2D,System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_SaveTextureAtPath_m4137488076 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, String_t* ___path1, int32_t ___imgType2, Texture2D_t3884108195 * ___watermark3, int32_t ___align4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::SaveTextureToGallery(UnityEngine.Texture2D,ImageType)
extern "C"  void CaptureAndSave_SaveTextureToGallery_m1530839788 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, int32_t ___imgType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::SaveTextureToGallery(UnityEngine.Texture2D,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_SaveTextureToGallery_m153208485 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, int32_t ___imgType1, Texture2D_t3884108195 * ___watermark2, int32_t ___align3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m3265519171 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m2807830830 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, Texture2D_t3884108195 * ___watermark1, int32_t ___align2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,System.Int32,System.Int32,ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m390648579 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,System.Int32,System.Int32,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m3720445038 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::SaveTexture(UnityEngine.Texture2D,System.String,ImageType,System.Boolean,System.Boolean,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_SaveTexture_m1816178388 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, String_t* ___path1, int32_t ___imgType2, bool ___destroy3, bool ___saveToGallery4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CaptureAndSave::SaveToAlbum(System.Int32,System.Int32,System.Int32,System.Int32,System.String,ImageType,System.Boolean,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Il2CppObject * CaptureAndSave_SaveToAlbum_m1026814172 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, String_t* ___path4, int32_t ___imgType5, bool ___saveToGallery6, Texture2D_t3884108195 * ___watermark7, int32_t ___align8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CaptureAndSave::GetFileName(ImageType)
extern "C"  String_t* CaptureAndSave_GetFileName_m569272239 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CaptureAndSave::GetFullPath(System.String,ImageType)
extern "C"  String_t* CaptureAndSave_GetFullPath_m3173349982 (CaptureAndSave_t700313070 * __this, String_t* ___path0, int32_t ___imgType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,ImageType)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetPixels_m1882752963 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CaptureAndSave::GetScreenPixels(System.Int32,System.Int32,System.Int32,System.Int32,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Il2CppObject * CaptureAndSave_GetScreenPixels_m3796987421 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::GetScreenShot(System.Int32,System.Int32,UnityEngine.Camera,ImageType)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetScreenShot_m2602002446 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::GetScreenShot(System.Int32,System.Int32,UnityEngine.Camera,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetScreenShot_m3002585027 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, Texture2D_t3884108195 * ___watermark4, int32_t ___align5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::GetScreenShotFromCamera(System.Int32,System.Int32,UnityEngine.Camera,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetScreenShotFromCamera_m3302824084 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, Texture2D_t3884108195 * ___watermark4, int32_t ___align5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::GetScreenShot(System.Int32,System.Int32,System.Int32,System.Int32,ImageType)
extern "C"  void CaptureAndSave_GetScreenShot_m2957471437 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::GetScreenShot(System.Int32,System.Int32,System.Int32,System.Int32,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_GetScreenShot_m394865636 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::GetFullScreenShot(ImageType)
extern "C"  void CaptureAndSave_GetFullScreenShot_m2596668478 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::GetFullScreenShot(ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_GetFullScreenShot_m3194136979 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, Texture2D_t3884108195 * ___watermark1, int32_t ___align2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CopyImageToCameraRoll(System.String)
extern "C"  void CaptureAndSave_CopyImageToCameraRoll_m1468462539 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::CopyVideoToCameraRoll(System.String)
extern "C"  void CaptureAndSave_CopyVideoToCameraRoll_m99063467 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::MoveImageToCameraRoll(System.String)
extern "C"  void CaptureAndSave_MoveImageToCameraRoll_m2078478887 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::MoveVideoToCameraRoll(System.String)
extern "C"  void CaptureAndSave_MoveVideoToCameraRoll_m709079815 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::AddWatermark(UnityEngine.Texture2D,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_AddWatermark_m277847075 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___background0, Texture2D_t3884108195 * ___watermark1, int32_t ___align2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::AddWatermark(UnityEngine.Texture2D,UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_AddWatermark_m66560740 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___background0, Texture2D_t3884108195 * ___watermark1, int32_t ___startX2, int32_t ___startY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D CaptureAndSave::CombineTexture(UnityEngine.Texture2D,UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_CombineTexture_m3511564587 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___background0, Texture2D_t3884108195 * ___watermark1, int32_t ___startX2, int32_t ___startY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::AndroidGalleryPath(System.String)
extern "C"  void CaptureAndSave_AndroidGalleryPath_m2832616556 (CaptureAndSave_t700313070 * __this, String_t* ___androidPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::OnError(System.String)
extern "C"  void CaptureAndSave_OnError_m608607365 (CaptureAndSave_t700313070 * __this, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave::OnComplete(System.String)
extern "C"  void CaptureAndSave_OnComplete_m3721315804 (CaptureAndSave_t700313070 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
