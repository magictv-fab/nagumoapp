﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.Arrays`1<System.Object>
struct  Arrays_1_t792694677  : public Il2CppObject
{
public:

public:
};

struct Arrays_1_t792694677_StaticFields
{
public:
	// T[] HutongGames.Utility.Arrays`1::Empty
	ObjectU5BU5D_t1108656482* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Arrays_1_t792694677_StaticFields, ___Empty_0)); }
	inline ObjectU5BU5D_t1108656482* get_Empty_0() const { return ___Empty_0; }
	inline ObjectU5BU5D_t1108656482** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ObjectU5BU5D_t1108656482* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
