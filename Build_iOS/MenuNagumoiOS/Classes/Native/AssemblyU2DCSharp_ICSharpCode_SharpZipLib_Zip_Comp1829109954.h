﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3572745737.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct  DeflaterPending_t1829109954  : public PendingBuffer_t3572745737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
