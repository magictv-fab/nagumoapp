﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PublicationData[]
struct PublicationDataU5BU5D_t1155489811;
// PublicationData
struct PublicationData_t473548758;
// System.Object
struct Il2CppObject;
// FavoritosManager
struct FavoritosManager_t4195735424;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FavoritosManager/<ILoadPublish>c__Iterator53
struct  U3CILoadPublishU3Ec__Iterator53_t1296913502  : public Il2CppObject
{
public:
	// System.String FavoritosManager/<ILoadPublish>c__Iterator53::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> FavoritosManager/<ILoadPublish>c__Iterator53::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] FavoritosManager/<ILoadPublish>c__Iterator53::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW FavoritosManager/<ILoadPublish>c__Iterator53::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> FavoritosManager/<ILoadPublish>c__Iterator53::<$s_309>__4
	Enumerator_t767573031  ___U3CU24s_309U3E__4_4;
	// UnityEngine.GameObject FavoritosManager/<ILoadPublish>c__Iterator53::<obj>__5
	GameObject_t3674682005 * ___U3CobjU3E__5_5;
	// System.String FavoritosManager/<ILoadPublish>c__Iterator53::<message>__6
	String_t* ___U3CmessageU3E__6_6;
	// PublicationData[] FavoritosManager/<ILoadPublish>c__Iterator53::<$s_310>__7
	PublicationDataU5BU5D_t1155489811* ___U3CU24s_310U3E__7_7;
	// System.Int32 FavoritosManager/<ILoadPublish>c__Iterator53::<$s_311>__8
	int32_t ___U3CU24s_311U3E__8_8;
	// PublicationData FavoritosManager/<ILoadPublish>c__Iterator53::<publicationData>__9
	PublicationData_t473548758 * ___U3CpublicationDataU3E__9_9;
	// System.Int32 FavoritosManager/<ILoadPublish>c__Iterator53::$PC
	int32_t ___U24PC_10;
	// System.Object FavoritosManager/<ILoadPublish>c__Iterator53::$current
	Il2CppObject * ___U24current_11;
	// FavoritosManager FavoritosManager/<ILoadPublish>c__Iterator53::<>f__this
	FavoritosManager_t4195735424 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_309U3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CU24s_309U3E__4_4)); }
	inline Enumerator_t767573031  get_U3CU24s_309U3E__4_4() const { return ___U3CU24s_309U3E__4_4; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_309U3E__4_4() { return &___U3CU24s_309U3E__4_4; }
	inline void set_U3CU24s_309U3E__4_4(Enumerator_t767573031  value)
	{
		___U3CU24s_309U3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CobjU3E__5_5)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__5_5() const { return ___U3CobjU3E__5_5; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__5_5() { return &___U3CobjU3E__5_5; }
	inline void set_U3CobjU3E__5_5(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__6_6() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CmessageU3E__6_6)); }
	inline String_t* get_U3CmessageU3E__6_6() const { return ___U3CmessageU3E__6_6; }
	inline String_t** get_address_of_U3CmessageU3E__6_6() { return &___U3CmessageU3E__6_6; }
	inline void set_U3CmessageU3E__6_6(String_t* value)
	{
		___U3CmessageU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CU24s_310U3E__7_7() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CU24s_310U3E__7_7)); }
	inline PublicationDataU5BU5D_t1155489811* get_U3CU24s_310U3E__7_7() const { return ___U3CU24s_310U3E__7_7; }
	inline PublicationDataU5BU5D_t1155489811** get_address_of_U3CU24s_310U3E__7_7() { return &___U3CU24s_310U3E__7_7; }
	inline void set_U3CU24s_310U3E__7_7(PublicationDataU5BU5D_t1155489811* value)
	{
		___U3CU24s_310U3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_310U3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_311U3E__8_8() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CU24s_311U3E__8_8)); }
	inline int32_t get_U3CU24s_311U3E__8_8() const { return ___U3CU24s_311U3E__8_8; }
	inline int32_t* get_address_of_U3CU24s_311U3E__8_8() { return &___U3CU24s_311U3E__8_8; }
	inline void set_U3CU24s_311U3E__8_8(int32_t value)
	{
		___U3CU24s_311U3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CpublicationDataU3E__9_9() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CpublicationDataU3E__9_9)); }
	inline PublicationData_t473548758 * get_U3CpublicationDataU3E__9_9() const { return ___U3CpublicationDataU3E__9_9; }
	inline PublicationData_t473548758 ** get_address_of_U3CpublicationDataU3E__9_9() { return &___U3CpublicationDataU3E__9_9; }
	inline void set_U3CpublicationDataU3E__9_9(PublicationData_t473548758 * value)
	{
		___U3CpublicationDataU3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpublicationDataU3E__9_9, value);
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator53_t1296913502, ___U3CU3Ef__this_12)); }
	inline FavoritosManager_t4195735424 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline FavoritosManager_t4195735424 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(FavoritosManager_t4195735424 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
