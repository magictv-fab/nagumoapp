﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.LoadScreenFake
struct LoadScreenFake_t1630568627;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.abstracts.screens.LoadScreenFake::.ctor()
extern "C"  void LoadScreenFake__ctor_m2536355793 (LoadScreenFake_t1630568627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::prepare()
extern "C"  void LoadScreenFake_prepare_m3118440662 (LoadScreenFake_t1630568627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::OnGUI()
extern "C"  void LoadScreenFake_OnGUI_m2031754443 (LoadScreenFake_t1630568627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::setMessage(System.String)
extern "C"  void LoadScreenFake_setMessage_m476938314 (LoadScreenFake_t1630568627 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::setPercentLoaded(System.Single)
extern "C"  void LoadScreenFake_setPercentLoaded_m2951398384 (LoadScreenFake_t1630568627 * __this, float ___total0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::HideNow()
extern "C"  void LoadScreenFake_HideNow_m2676708003 (LoadScreenFake_t1630568627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::hide()
extern "C"  void LoadScreenFake_hide_m2301550581 (LoadScreenFake_t1630568627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenFake::show()
extern "C"  void LoadScreenFake_show_m2615892720 (LoadScreenFake_t1630568627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
