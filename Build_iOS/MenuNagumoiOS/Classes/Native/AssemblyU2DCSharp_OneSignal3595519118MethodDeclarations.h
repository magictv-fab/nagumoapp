﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal
struct OneSignal_t3595519118;
// OneSignal/PermissionObservable
struct PermissionObservable_t1360586611;
// OneSignal/SubscriptionObservable
struct SubscriptionObservable_t2046194945;
// OneSignal/EmailSubscriptionObservable
struct EmailSubscriptionObservable_t3601566427;
// OneSignal/UnityBuilder
struct UnityBuilder_t2457889383;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// OneSignal/TagsReceived
struct TagsReceived_t3348492571;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2701878760;
// OneSignal/PromptForPushNotificationsUserResponse
struct PromptForPushNotificationsUserResponse_t2193543734;
// OneSignal/IdsAvailableCallback
struct IdsAvailableCallback_t3679464791;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// OneSignal/OnSetEmailSuccess
struct OnSetEmailSuccess_t2952831753;
// OneSignal/OnSetEmailFailure
struct OnSetEmailFailure_t3733427344;
// OneSignal/OnLogoutEmailSuccess
struct OnLogoutEmailSuccess_t3366364849;
// OneSignal/OnLogoutEmailFailure
struct OnLogoutEmailFailure_t4146960440;
// OneSignal/OnPostNotificationSuccess
struct OnPostNotificationSuccess_t2053930136;
// OneSignal/OnPostNotificationFailure
struct OnPostNotificationFailure_t2834525727;
// OSPermissionSubscriptionState
struct OSPermissionSubscriptionState_t4252985057;
// OSNotification
struct OSNotification_t892481775;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OneSignal_PermissionObservable1360586611.h"
#include "AssemblyU2DCSharp_OneSignal_SubscriptionObservable2046194945.h"
#include "AssemblyU2DCSharp_OneSignal_EmailSubscriptionObser3601566427.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_OneSignal_OSInFocusDisplayOption2441910921.h"
#include "AssemblyU2DCSharp_OneSignal_LOG_LEVEL343241288.h"
#include "AssemblyU2DCSharp_OneSignal_TagsReceived3348492571.h"
#include "AssemblyU2DCSharp_OneSignal_PromptForPushNotificat2193543734.h"
#include "AssemblyU2DCSharp_OneSignal_IdsAvailableCallback3679464791.h"
#include "AssemblyU2DCSharp_OneSignal_OnSetEmailSuccess2952831753.h"
#include "AssemblyU2DCSharp_OneSignal_OnSetEmailFailure3733427344.h"
#include "AssemblyU2DCSharp_OneSignal_OnLogoutEmailSuccess3366364849.h"
#include "AssemblyU2DCSharp_OneSignal_OnLogoutEmailFailure4146960440.h"
#include "AssemblyU2DCSharp_OneSignal_OnPostNotificationSucc2053930136.h"
#include "AssemblyU2DCSharp_OneSignal_OnPostNotificationFail2834525727.h"

// System.Void OneSignal::.ctor()
extern "C"  void OneSignal__ctor_m1383403677 (OneSignal_t3595519118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::.cctor()
extern "C"  void OneSignal__cctor_m3748712112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::add_permissionObserver(OneSignal/PermissionObservable)
extern "C"  void OneSignal_add_permissionObserver_m741184503 (Il2CppObject * __this /* static, unused */, PermissionObservable_t1360586611 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::remove_permissionObserver(OneSignal/PermissionObservable)
extern "C"  void OneSignal_remove_permissionObserver_m2425931176 (Il2CppObject * __this /* static, unused */, PermissionObservable_t1360586611 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::add_subscriptionObserver(OneSignal/SubscriptionObservable)
extern "C"  void OneSignal_add_subscriptionObserver_m427676855 (Il2CppObject * __this /* static, unused */, SubscriptionObservable_t2046194945 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::remove_subscriptionObserver(OneSignal/SubscriptionObservable)
extern "C"  void OneSignal_remove_subscriptionObserver_m212256232 (Il2CppObject * __this /* static, unused */, SubscriptionObservable_t2046194945 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::add_emailSubscriptionObserver(OneSignal/EmailSubscriptionObservable)
extern "C"  void OneSignal_add_emailSubscriptionObserver_m1971805291 (Il2CppObject * __this /* static, unused */, EmailSubscriptionObservable_t3601566427 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::remove_emailSubscriptionObserver(OneSignal/EmailSubscriptionObservable)
extern "C"  void OneSignal_remove_emailSubscriptionObserver_m1656570972 (Il2CppObject * __this /* static, unused */, EmailSubscriptionObservable_t3601566427 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::addPermissionObserver()
extern "C"  void OneSignal_addPermissionObserver_m411294881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::addSubscriptionObserver()
extern "C"  void OneSignal_addSubscriptionObserver_m182832047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::addEmailSubscriptionObserver()
extern "C"  void OneSignal_addEmailSubscriptionObserver_m288698005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/UnityBuilder OneSignal::StartInit(System.String,System.String)
extern "C"  UnityBuilder_t2457889383 * OneSignal_StartInit_m71846683 (Il2CppObject * __this /* static, unused */, String_t* ___appID0, String_t* ___googleProjectNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::Init()
extern "C"  void OneSignal_Init_m1104541207 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::initOneSignalPlatform()
extern "C"  void OneSignal_initOneSignalPlatform_m1176709548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::initIOS()
extern "C"  void OneSignal_initIOS_m3965331704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::initUnityEditor()
extern "C"  void OneSignal_initUnityEditor_m1826654957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OneSignal/OSInFocusDisplayOption OneSignal::get_inFocusDisplayType()
extern "C"  int32_t OneSignal_get_inFocusDisplayType_m3867454469 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::set_inFocusDisplayType(OneSignal/OSInFocusDisplayOption)
extern "C"  void OneSignal_set_inFocusDisplayType_m1253763556 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetLogLevel(OneSignal/LOG_LEVEL,OneSignal/LOG_LEVEL)
extern "C"  void OneSignal_SetLogLevel_m3494354577 (Il2CppObject * __this /* static, unused */, int32_t ___inLogLevel0, int32_t ___inVisualLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetLocationShared(System.Boolean)
extern "C"  void OneSignal_SetLocationShared_m1746508398 (Il2CppObject * __this /* static, unused */, bool ___shared0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SendTag(System.String,System.String)
extern "C"  void OneSignal_SendTag_m3688829425 (Il2CppObject * __this /* static, unused */, String_t* ___tagName0, String_t* ___tagValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SendTags(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void OneSignal_SendTags_m4028967097 (Il2CppObject * __this /* static, unused */, Dictionary_2_t827649927 * ___tags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::GetTags(OneSignal/TagsReceived)
extern "C"  void OneSignal_GetTags_m2808317839 (Il2CppObject * __this /* static, unused */, TagsReceived_t3348492571 * ___inTagsReceivedDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::GetTags()
extern "C"  void OneSignal_GetTags_m2768233482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::DeleteTag(System.String)
extern "C"  void OneSignal_DeleteTag_m2965210200 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::DeleteTags(System.Collections.Generic.IList`1<System.String>)
extern "C"  void OneSignal_DeleteTags_m1091554022 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___keys0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::RegisterForPushNotifications()
extern "C"  void OneSignal_RegisterForPushNotifications_m1750993519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::PromptForPushNotificationsWithUserResponse(OneSignal/PromptForPushNotificationsUserResponse)
extern "C"  void OneSignal_PromptForPushNotificationsWithUserResponse_m2991701292 (Il2CppObject * __this /* static, unused */, PromptForPushNotificationsUserResponse_t2193543734 * ___inDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::IdsAvailable(OneSignal/IdsAvailableCallback)
extern "C"  void OneSignal_IdsAvailable_m4059493121 (Il2CppObject * __this /* static, unused */, IdsAvailableCallback_t3679464791 * ___inIdsAvailableDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::IdsAvailable()
extern "C"  void OneSignal_IdsAvailable_m2027641272 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::EnableVibrate(System.Boolean)
extern "C"  void OneSignal_EnableVibrate_m3359708254 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::EnableSound(System.Boolean)
extern "C"  void OneSignal_EnableSound_m3695626686 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::ClearOneSignalNotifications()
extern "C"  void OneSignal_ClearOneSignalNotifications_m3465129474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetSubscription(System.Boolean)
extern "C"  void OneSignal_SetSubscription_m2674692113 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::PostNotification(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void OneSignal_PostNotification_m809901265 (Il2CppObject * __this /* static, unused */, Dictionary_2_t696267445 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetEmail(System.String,OneSignal/OnSetEmailSuccess,OneSignal/OnSetEmailFailure)
extern "C"  void OneSignal_SetEmail_m2410488352 (Il2CppObject * __this /* static, unused */, String_t* ___email0, OnSetEmailSuccess_t2952831753 * ___successDelegate1, OnSetEmailFailure_t3733427344 * ___failureDelegate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetEmail(System.String,System.String,OneSignal/OnSetEmailSuccess,OneSignal/OnSetEmailFailure)
extern "C"  void OneSignal_SetEmail_m3272392860 (Il2CppObject * __this /* static, unused */, String_t* ___email0, String_t* ___emailAuthToken1, OnSetEmailSuccess_t2952831753 * ___successDelegate2, OnSetEmailFailure_t3733427344 * ___failureDelegate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::LogoutEmail(OneSignal/OnLogoutEmailSuccess,OneSignal/OnLogoutEmailFailure)
extern "C"  void OneSignal_LogoutEmail_m3444816050 (Il2CppObject * __this /* static, unused */, OnLogoutEmailSuccess_t3366364849 * ___successDelegate0, OnLogoutEmailFailure_t4146960440 * ___failureDelegate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetEmail(System.String)
extern "C"  void OneSignal_SetEmail_m3313743041 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetEmail(System.String,System.String)
extern "C"  void OneSignal_SetEmail_m2044524349 (Il2CppObject * __this /* static, unused */, String_t* ___email0, String_t* ___emailAuthToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::LogoutEmail()
extern "C"  void OneSignal_LogoutEmail_m2550819309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::PostNotification(System.Collections.Generic.Dictionary`2<System.String,System.Object>,OneSignal/OnPostNotificationSuccess,OneSignal/OnPostNotificationFailure)
extern "C"  void OneSignal_PostNotification_m4189461330 (Il2CppObject * __this /* static, unused */, Dictionary_2_t696267445 * ___data0, OnPostNotificationSuccess_t2053930136 * ___inOnPostNotificationSuccess1, OnPostNotificationFailure_t2834525727 * ___inOnPostNotificationFailure2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SyncHashedEmail(System.String)
extern "C"  void OneSignal_SyncHashedEmail_m2499339091 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::PromptLocation()
extern "C"  void OneSignal_PromptLocation_m3550352512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSPermissionSubscriptionState OneSignal::GetPermissionSubscriptionState()
extern "C"  OSPermissionSubscriptionState_t4252985057 * OneSignal_GetPermissionSubscriptionState_m1129573540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::UserDidProvideConsent(System.Boolean)
extern "C"  void OneSignal_UserDidProvideConsent_m3121653247 (Il2CppObject * __this /* static, unused */, bool ___consent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OneSignal::UserProvidedConsent()
extern "C"  bool OneSignal_UserProvidedConsent_m4153603419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::SetRequiresUserPrivacyConsent(System.Boolean)
extern "C"  void OneSignal_SetRequiresUserPrivacyConsent_m3911499359 (Il2CppObject * __this /* static, unused */, bool ___required0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSNotification OneSignal::DictionaryToNotification(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  OSNotification_t892481775 * OneSignal_DictionaryToNotification_m936891252 (OneSignal_t3595519118 * __this, Dictionary_2_t696267445 * ___jsonObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onPushNotificationReceived(System.String)
extern "C"  void OneSignal_onPushNotificationReceived_m588710966 (OneSignal_t3595519118 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onPushNotificationOpened(System.String)
extern "C"  void OneSignal_onPushNotificationOpened_m3896144430 (OneSignal_t3595519118 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onIdsAvailable(System.String)
extern "C"  void OneSignal_onIdsAvailable_m633503883 (OneSignal_t3595519118 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onTagsReceived(System.String)
extern "C"  void OneSignal_onTagsReceived_m1499009858 (OneSignal_t3595519118 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onPostNotificationSuccess(System.String)
extern "C"  void OneSignal_onPostNotificationSuccess_m2981425646 (OneSignal_t3595519118 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onSetEmailSuccess()
extern "C"  void OneSignal_onSetEmailSuccess_m2520504005 (OneSignal_t3595519118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onSetEmailFailure(System.String)
extern "C"  void OneSignal_onSetEmailFailure_m683843574 (OneSignal_t3595519118 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onLogoutEmailSuccess()
extern "C"  void OneSignal_onLogoutEmailSuccess_m2408485015 (OneSignal_t3595519118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onLogoutEmailFailure(System.String)
extern "C"  void OneSignal_onLogoutEmailFailure_m4082363236 (OneSignal_t3595519118 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onPostNotificationFailed(System.String)
extern "C"  void OneSignal_onPostNotificationFailed_m3051741972 (OneSignal_t3595519118 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onOSPermissionChanged(System.String)
extern "C"  void OneSignal_onOSPermissionChanged_m3622314149 (OneSignal_t3595519118 * __this, String_t* ___stateChangesJSONString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onOSSubscriptionChanged(System.String)
extern "C"  void OneSignal_onOSSubscriptionChanged_m345171763 (OneSignal_t3595519118 * __this, String_t* ___stateChangesJSONString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onOSEmailSubscriptionChanged(System.String)
extern "C"  void OneSignal_onOSEmailSubscriptionChanged_m2485233405 (OneSignal_t3595519118 * __this, String_t* ___stateChangesJSONString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal::onPromptForPushNotificationsWithUserResponse(System.String)
extern "C"  void OneSignal_onPromptForPushNotificationsWithUserResponse_m3350790177 (OneSignal_t3595519118 * __this, String_t* ___accepted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
