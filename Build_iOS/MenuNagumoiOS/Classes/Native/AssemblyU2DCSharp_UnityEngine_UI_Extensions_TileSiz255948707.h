﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t972643934;

#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou2511441271.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker4185719096.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TileSizeFitter
struct  TileSizeFitter_t255948707  : public UIBehaviour_t2511441271
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::m_Border
	Vector2_t4282066565  ___m_Border_2;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::m_TileSize
	Vector2_t4282066565  ___m_TileSize_3;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.TileSizeFitter::m_Rect
	RectTransform_t972643934 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Extensions.TileSizeFitter::m_Tracker
	DrivenRectTransformTracker_t4185719096  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_Border_2() { return static_cast<int32_t>(offsetof(TileSizeFitter_t255948707, ___m_Border_2)); }
	inline Vector2_t4282066565  get_m_Border_2() const { return ___m_Border_2; }
	inline Vector2_t4282066565 * get_address_of_m_Border_2() { return &___m_Border_2; }
	inline void set_m_Border_2(Vector2_t4282066565  value)
	{
		___m_Border_2 = value;
	}

	inline static int32_t get_offset_of_m_TileSize_3() { return static_cast<int32_t>(offsetof(TileSizeFitter_t255948707, ___m_TileSize_3)); }
	inline Vector2_t4282066565  get_m_TileSize_3() const { return ___m_TileSize_3; }
	inline Vector2_t4282066565 * get_address_of_m_TileSize_3() { return &___m_TileSize_3; }
	inline void set_m_TileSize_3(Vector2_t4282066565  value)
	{
		___m_TileSize_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(TileSizeFitter_t255948707, ___m_Rect_4)); }
	inline RectTransform_t972643934 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t972643934 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t972643934 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Rect_4, value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(TileSizeFitter_t255948707, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t4185719096  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t4185719096 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t4185719096  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
