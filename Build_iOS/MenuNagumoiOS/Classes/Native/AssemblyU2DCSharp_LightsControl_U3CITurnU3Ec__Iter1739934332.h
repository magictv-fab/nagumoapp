﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// LightsControl
struct LightsControl_t2444629088;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightsControl/<ITurn>c__Iterator5E
struct  U3CITurnU3Ec__Iterator5E_t1739934332  : public Il2CppObject
{
public:
	// System.Int32 LightsControl/<ITurn>c__Iterator5E::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> LightsControl/<ITurn>c__Iterator5E::<$s_315>__1
	Enumerator_t767573031  ___U3CU24s_315U3E__1_1;
	// UnityEngine.GameObject LightsControl/<ITurn>c__Iterator5E::<obj>__2
	GameObject_t3674682005 * ___U3CobjU3E__2_2;
	// System.Int32 LightsControl/<ITurn>c__Iterator5E::$PC
	int32_t ___U24PC_3;
	// System.Object LightsControl/<ITurn>c__Iterator5E::$current
	Il2CppObject * ___U24current_4;
	// LightsControl LightsControl/<ITurn>c__Iterator5E::<>f__this
	LightsControl_t2444629088 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CITurnU3Ec__Iterator5E_t1739934332, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_315U3E__1_1() { return static_cast<int32_t>(offsetof(U3CITurnU3Ec__Iterator5E_t1739934332, ___U3CU24s_315U3E__1_1)); }
	inline Enumerator_t767573031  get_U3CU24s_315U3E__1_1() const { return ___U3CU24s_315U3E__1_1; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_315U3E__1_1() { return &___U3CU24s_315U3E__1_1; }
	inline void set_U3CU24s_315U3E__1_1(Enumerator_t767573031  value)
	{
		___U3CU24s_315U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_2() { return static_cast<int32_t>(offsetof(U3CITurnU3Ec__Iterator5E_t1739934332, ___U3CobjU3E__2_2)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__2_2() const { return ___U3CobjU3E__2_2; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__2_2() { return &___U3CobjU3E__2_2; }
	inline void set_U3CobjU3E__2_2(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CITurnU3Ec__Iterator5E_t1739934332, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CITurnU3Ec__Iterator5E_t1739934332, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CITurnU3Ec__Iterator5E_t1739934332, ___U3CU3Ef__this_5)); }
	inline LightsControl_t2444629088 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline LightsControl_t2444629088 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(LightsControl_t2444629088 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
