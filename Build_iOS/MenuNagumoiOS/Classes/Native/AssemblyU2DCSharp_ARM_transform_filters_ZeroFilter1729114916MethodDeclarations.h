﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.ZeroFilter
struct ZeroFilter_t1729114916;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.transform.filters.ZeroFilter::.ctor()
extern "C"  void ZeroFilter__ctor_m664831508 (ZeroFilter_t1729114916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.ZeroFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  ZeroFilter__doFilter_m3598820762 (ZeroFilter_t1729114916 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.ZeroFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ZeroFilter__doFilter_m3750828370 (ZeroFilter_t1729114916 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.ZeroFilter::filterCurrent(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  ZeroFilter_filterCurrent_m3270018166 (ZeroFilter_t1729114916 * __this, Quaternion_t1553702882  ___cur0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.ZeroFilter::filterCurrent(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ZeroFilter_filterCurrent_m1441272960 (ZeroFilter_t1729114916 * __this, Vector3_t4282066566  ___cur0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.ZeroFilter::configByString(System.String)
extern "C"  void ZeroFilter_configByString_m3418367304 (ZeroFilter_t1729114916 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.ZeroFilter::Reset()
extern "C"  void ZeroFilter_Reset_m2606231745 (ZeroFilter_t1729114916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
