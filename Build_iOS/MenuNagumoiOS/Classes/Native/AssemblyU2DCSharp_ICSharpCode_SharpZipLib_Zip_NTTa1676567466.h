﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.NTTaggedData
struct  NTTaggedData_t1676567466  : public Il2CppObject
{
public:
	// System.DateTime ICSharpCode.SharpZipLib.Zip.NTTaggedData::_lastAccessTime
	DateTime_t4283661327  ____lastAccessTime_0;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.NTTaggedData::_lastModificationTime
	DateTime_t4283661327  ____lastModificationTime_1;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.NTTaggedData::_createTime
	DateTime_t4283661327  ____createTime_2;

public:
	inline static int32_t get_offset_of__lastAccessTime_0() { return static_cast<int32_t>(offsetof(NTTaggedData_t1676567466, ____lastAccessTime_0)); }
	inline DateTime_t4283661327  get__lastAccessTime_0() const { return ____lastAccessTime_0; }
	inline DateTime_t4283661327 * get_address_of__lastAccessTime_0() { return &____lastAccessTime_0; }
	inline void set__lastAccessTime_0(DateTime_t4283661327  value)
	{
		____lastAccessTime_0 = value;
	}

	inline static int32_t get_offset_of__lastModificationTime_1() { return static_cast<int32_t>(offsetof(NTTaggedData_t1676567466, ____lastModificationTime_1)); }
	inline DateTime_t4283661327  get__lastModificationTime_1() const { return ____lastModificationTime_1; }
	inline DateTime_t4283661327 * get_address_of__lastModificationTime_1() { return &____lastModificationTime_1; }
	inline void set__lastModificationTime_1(DateTime_t4283661327  value)
	{
		____lastModificationTime_1 = value;
	}

	inline static int32_t get_offset_of__createTime_2() { return static_cast<int32_t>(offsetof(NTTaggedData_t1676567466, ____createTime_2)); }
	inline DateTime_t4283661327  get__createTime_2() const { return ____createTime_2; }
	inline DateTime_t4283661327 * get_address_of__createTime_2() { return &____createTime_2; }
	inline void set__createTime_2(DateTime_t4283661327  value)
	{
		____createTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
