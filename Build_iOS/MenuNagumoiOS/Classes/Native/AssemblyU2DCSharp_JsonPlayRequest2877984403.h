﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonPlayRequest
struct  JsonPlayRequest_t2877984403  : public Il2CppObject
{
public:
	// System.Int32 JsonPlayRequest::id_participante
	int32_t ___id_participante_0;
	// System.String JsonPlayRequest::senha
	String_t* ___senha_1;
	// System.String JsonPlayRequest::cpf
	String_t* ___cpf_2;

public:
	inline static int32_t get_offset_of_id_participante_0() { return static_cast<int32_t>(offsetof(JsonPlayRequest_t2877984403, ___id_participante_0)); }
	inline int32_t get_id_participante_0() const { return ___id_participante_0; }
	inline int32_t* get_address_of_id_participante_0() { return &___id_participante_0; }
	inline void set_id_participante_0(int32_t value)
	{
		___id_participante_0 = value;
	}

	inline static int32_t get_offset_of_senha_1() { return static_cast<int32_t>(offsetof(JsonPlayRequest_t2877984403, ___senha_1)); }
	inline String_t* get_senha_1() const { return ___senha_1; }
	inline String_t** get_address_of_senha_1() { return &___senha_1; }
	inline void set_senha_1(String_t* value)
	{
		___senha_1 = value;
		Il2CppCodeGenWriteBarrier(&___senha_1, value);
	}

	inline static int32_t get_offset_of_cpf_2() { return static_cast<int32_t>(offsetof(JsonPlayRequest_t2877984403, ___cpf_2)); }
	inline String_t* get_cpf_2() const { return ___cpf_2; }
	inline String_t** get_address_of_cpf_2() { return &___cpf_2; }
	inline void set_cpf_2(String_t* value)
	{
		___cpf_2 = value;
		Il2CppCodeGenWriteBarrier(&___cpf_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
