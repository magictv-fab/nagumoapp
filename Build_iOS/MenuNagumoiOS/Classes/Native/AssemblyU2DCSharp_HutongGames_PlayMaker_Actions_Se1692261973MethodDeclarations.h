﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFogColor
struct SetFogColor_t1692261973;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFogColor::.ctor()
extern "C"  void SetFogColor__ctor_m1466260609 (SetFogColor_t1692261973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::Reset()
extern "C"  void SetFogColor_Reset_m3407660846 (SetFogColor_t1692261973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::OnEnter()
extern "C"  void SetFogColor_OnEnter_m4013868056 (SetFogColor_t1692261973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::OnUpdate()
extern "C"  void SetFogColor_OnUpdate_m3304384811 (SetFogColor_t1692261973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::DoSetFogColor()
extern "C"  void SetFogColor_DoSetFogColor_m1891050203 (SetFogColor_t1692261973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
