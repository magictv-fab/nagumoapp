﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.transform.filters.BussolaFilter
struct BussolaFilter_t1088096095;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BussolaSimuladaDebug
struct  BussolaSimuladaDebug_t1344610386  : public MonoBehaviour_t667441552
{
public:
	// ARM.transform.filters.BussolaFilter BussolaSimuladaDebug::bussola
	BussolaFilter_t1088096095 * ___bussola_2;
	// System.Boolean BussolaSimuladaDebug::byPass
	bool ___byPass_3;
	// UnityEngine.Vector2[] BussolaSimuladaDebug::sequenciaDeComportamento
	Vector2U5BU5D_t4024180168* ___sequenciaDeComportamento_4;
	// System.Int32 BussolaSimuladaDebug::currentIndex
	int32_t ___currentIndex_5;
	// UnityEngine.Vector2 BussolaSimuladaDebug::currentVector
	Vector2_t4282066565  ___currentVector_6;
	// System.Single BussolaSimuladaDebug::_timePassed
	float ____timePassed_7;

public:
	inline static int32_t get_offset_of_bussola_2() { return static_cast<int32_t>(offsetof(BussolaSimuladaDebug_t1344610386, ___bussola_2)); }
	inline BussolaFilter_t1088096095 * get_bussola_2() const { return ___bussola_2; }
	inline BussolaFilter_t1088096095 ** get_address_of_bussola_2() { return &___bussola_2; }
	inline void set_bussola_2(BussolaFilter_t1088096095 * value)
	{
		___bussola_2 = value;
		Il2CppCodeGenWriteBarrier(&___bussola_2, value);
	}

	inline static int32_t get_offset_of_byPass_3() { return static_cast<int32_t>(offsetof(BussolaSimuladaDebug_t1344610386, ___byPass_3)); }
	inline bool get_byPass_3() const { return ___byPass_3; }
	inline bool* get_address_of_byPass_3() { return &___byPass_3; }
	inline void set_byPass_3(bool value)
	{
		___byPass_3 = value;
	}

	inline static int32_t get_offset_of_sequenciaDeComportamento_4() { return static_cast<int32_t>(offsetof(BussolaSimuladaDebug_t1344610386, ___sequenciaDeComportamento_4)); }
	inline Vector2U5BU5D_t4024180168* get_sequenciaDeComportamento_4() const { return ___sequenciaDeComportamento_4; }
	inline Vector2U5BU5D_t4024180168** get_address_of_sequenciaDeComportamento_4() { return &___sequenciaDeComportamento_4; }
	inline void set_sequenciaDeComportamento_4(Vector2U5BU5D_t4024180168* value)
	{
		___sequenciaDeComportamento_4 = value;
		Il2CppCodeGenWriteBarrier(&___sequenciaDeComportamento_4, value);
	}

	inline static int32_t get_offset_of_currentIndex_5() { return static_cast<int32_t>(offsetof(BussolaSimuladaDebug_t1344610386, ___currentIndex_5)); }
	inline int32_t get_currentIndex_5() const { return ___currentIndex_5; }
	inline int32_t* get_address_of_currentIndex_5() { return &___currentIndex_5; }
	inline void set_currentIndex_5(int32_t value)
	{
		___currentIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentVector_6() { return static_cast<int32_t>(offsetof(BussolaSimuladaDebug_t1344610386, ___currentVector_6)); }
	inline Vector2_t4282066565  get_currentVector_6() const { return ___currentVector_6; }
	inline Vector2_t4282066565 * get_address_of_currentVector_6() { return &___currentVector_6; }
	inline void set_currentVector_6(Vector2_t4282066565  value)
	{
		___currentVector_6 = value;
	}

	inline static int32_t get_offset_of__timePassed_7() { return static_cast<int32_t>(offsetof(BussolaSimuladaDebug_t1344610386, ____timePassed_7)); }
	inline float get__timePassed_7() const { return ____timePassed_7; }
	inline float* get_address_of__timePassed_7() { return &____timePassed_7; }
	inline void set__timePassed_7(float value)
	{
		____timePassed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
