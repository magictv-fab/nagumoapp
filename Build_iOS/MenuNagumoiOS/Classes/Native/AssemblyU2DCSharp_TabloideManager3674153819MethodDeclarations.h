﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloideManager
struct TabloideManager_t3674153819;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"

// System.Void TabloideManager::.ctor()
extern "C"  void TabloideManager__ctor_m3332061808 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager::.cctor()
extern "C"  void TabloideManager__cctor_m4027572029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager::Start()
extern "C"  void TabloideManager_Start_m2279199600 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager::GetScrollPos(UnityEngine.Vector2)
extern "C"  void TabloideManager_GetScrollPos_m3370046581 (TabloideManager_t3674153819 * __this, Vector2_t4282066565  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager::InstantiateNextItem()
extern "C"  void TabloideManager_InstantiateNextItem_m4048494206 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TabloideManager::ILoadPublish()
extern "C"  Il2CppObject * TabloideManager_ILoadPublish_m3602878380 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TabloideManager::DecodeFromUtf8(System.String)
extern "C"  String_t* TabloideManager_DecodeFromUtf8_m2467054056 (TabloideManager_t3674153819 * __this, String_t* ___utf8String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TabloideManager::ILoadImgs()
extern "C"  Il2CppObject * TabloideManager_ILoadImgs_m3004877365 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager::LoadImgs()
extern "C"  void TabloideManager_LoadImgs_m1614712042 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TabloideManager::LoadLoadeds()
extern "C"  Il2CppObject * TabloideManager_LoadLoadeds_m3979236702 (TabloideManager_t3674153819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager::PopUp(System.String)
extern "C"  void TabloideManager_PopUp_m1428953064 (TabloideManager_t3674153819 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
