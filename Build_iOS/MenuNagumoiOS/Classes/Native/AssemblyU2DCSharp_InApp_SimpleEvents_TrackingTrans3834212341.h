﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InApp.SimpleEvents.TrackingTransformConfig
struct TrackingTransformConfig_t3834212341;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.TrackingTransformConfig
struct  TrackingTransformConfig_t3834212341  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::_initialPosition
	Vector3_t4282066566  ____initialPosition_3;
	// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::_initialRotation
	Vector3_t4282066566  ____initialRotation_4;
	// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::_initialScale
	Vector3_t4282066566  ____initialScale_5;
	// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::_position
	Vector3_t4282066566  ____position_6;
	// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::_rotation
	Vector3_t4282066566  ____rotation_7;
	// UnityEngine.Vector3 InApp.SimpleEvents.TrackingTransformConfig::_scale
	Vector3_t4282066566  ____scale_8;
	// UnityEngine.GameObject InApp.SimpleEvents.TrackingTransformConfig::trackingContainer
	GameObject_t3674682005 * ___trackingContainer_9;

public:
	inline static int32_t get_offset_of__initialPosition_3() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ____initialPosition_3)); }
	inline Vector3_t4282066566  get__initialPosition_3() const { return ____initialPosition_3; }
	inline Vector3_t4282066566 * get_address_of__initialPosition_3() { return &____initialPosition_3; }
	inline void set__initialPosition_3(Vector3_t4282066566  value)
	{
		____initialPosition_3 = value;
	}

	inline static int32_t get_offset_of__initialRotation_4() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ____initialRotation_4)); }
	inline Vector3_t4282066566  get__initialRotation_4() const { return ____initialRotation_4; }
	inline Vector3_t4282066566 * get_address_of__initialRotation_4() { return &____initialRotation_4; }
	inline void set__initialRotation_4(Vector3_t4282066566  value)
	{
		____initialRotation_4 = value;
	}

	inline static int32_t get_offset_of__initialScale_5() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ____initialScale_5)); }
	inline Vector3_t4282066566  get__initialScale_5() const { return ____initialScale_5; }
	inline Vector3_t4282066566 * get_address_of__initialScale_5() { return &____initialScale_5; }
	inline void set__initialScale_5(Vector3_t4282066566  value)
	{
		____initialScale_5 = value;
	}

	inline static int32_t get_offset_of__position_6() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ____position_6)); }
	inline Vector3_t4282066566  get__position_6() const { return ____position_6; }
	inline Vector3_t4282066566 * get_address_of__position_6() { return &____position_6; }
	inline void set__position_6(Vector3_t4282066566  value)
	{
		____position_6 = value;
	}

	inline static int32_t get_offset_of__rotation_7() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ____rotation_7)); }
	inline Vector3_t4282066566  get__rotation_7() const { return ____rotation_7; }
	inline Vector3_t4282066566 * get_address_of__rotation_7() { return &____rotation_7; }
	inline void set__rotation_7(Vector3_t4282066566  value)
	{
		____rotation_7 = value;
	}

	inline static int32_t get_offset_of__scale_8() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ____scale_8)); }
	inline Vector3_t4282066566  get__scale_8() const { return ____scale_8; }
	inline Vector3_t4282066566 * get_address_of__scale_8() { return &____scale_8; }
	inline void set__scale_8(Vector3_t4282066566  value)
	{
		____scale_8 = value;
	}

	inline static int32_t get_offset_of_trackingContainer_9() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341, ___trackingContainer_9)); }
	inline GameObject_t3674682005 * get_trackingContainer_9() const { return ___trackingContainer_9; }
	inline GameObject_t3674682005 ** get_address_of_trackingContainer_9() { return &___trackingContainer_9; }
	inline void set_trackingContainer_9(GameObject_t3674682005 * value)
	{
		___trackingContainer_9 = value;
		Il2CppCodeGenWriteBarrier(&___trackingContainer_9, value);
	}
};

struct TrackingTransformConfig_t3834212341_StaticFields
{
public:
	// InApp.SimpleEvents.TrackingTransformConfig InApp.SimpleEvents.TrackingTransformConfig::_instance
	TrackingTransformConfig_t3834212341 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(TrackingTransformConfig_t3834212341_StaticFields, ____instance_2)); }
	inline TrackingTransformConfig_t3834212341 * get__instance_2() const { return ____instance_2; }
	inline TrackingTransformConfig_t3834212341 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(TrackingTransformConfig_t3834212341 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
