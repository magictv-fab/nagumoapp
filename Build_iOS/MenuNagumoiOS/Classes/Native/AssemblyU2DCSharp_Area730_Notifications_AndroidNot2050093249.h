﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Area730.Notifications.DataHolder
struct DataHolder_t1724900414;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Area730.Notifications.AndroidNotifications
struct  AndroidNotifications_t2050093249  : public Il2CppObject
{
public:

public:
};

struct AndroidNotifications_t2050093249_StaticFields
{
public:
	// Area730.Notifications.DataHolder Area730.Notifications.AndroidNotifications::_dataHolder
	DataHolder_t1724900414 * ____dataHolder_0;

public:
	inline static int32_t get_offset_of__dataHolder_0() { return static_cast<int32_t>(offsetof(AndroidNotifications_t2050093249_StaticFields, ____dataHolder_0)); }
	inline DataHolder_t1724900414 * get__dataHolder_0() const { return ____dataHolder_0; }
	inline DataHolder_t1724900414 ** get_address_of__dataHolder_0() { return &____dataHolder_0; }
	inline void set__dataHolder_0(DataHolder_t1724900414 * value)
	{
		____dataHolder_0 = value;
		Il2CppCodeGenWriteBarrier(&____dataHolder_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
