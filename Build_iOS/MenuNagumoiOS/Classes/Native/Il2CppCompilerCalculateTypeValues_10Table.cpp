﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExce876339989.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTest2979531601.h"
#include "mscorlib_System_AppDomainInitializer691583313.h"
#include "mscorlib_System_AssemblyLoadEventHandler3394555856.h"
#include "mscorlib_System_ConsoleCancelEventHandler3348198711.h"
#include "mscorlib_System_EventHandler2463957060.h"
#include "mscorlib_System_ResolveEventHandler2622793906.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120.h"
#include "mscorlib_System_Reflection_MemberFilter1019468612.h"
#include "mscorlib_System_Reflection_ModuleResolveEventHandle626980826.h"
#include "mscorlib_System_Reflection_TypeFilter3531778276.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCont651537830.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHan53887223.h"
#include "mscorlib_System_Threading_ParameterizedThreadStart669514423.h"
#include "mscorlib_System_Threading_ThreadStart124146534.h"
#include "mscorlib_System_Threading_TimerCallback2757719768.h"
#include "mscorlib_System_Threading_WaitCallback777536786.h"
#include "mscorlib_System_Threading_WaitOrTimerCallback2612136768.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220472.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220381.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220352.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676615732.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra435538904.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220377.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220410.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220447.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220505.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220348.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra435484226.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra435509018.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra435482267.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676615769.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3988332413.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220567.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220534.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676615736.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220600.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra435508189.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3379220476.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676616792.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra435478332.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676620599.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676615856.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1676615740.h"
#include "mscorlib_System___Il2CppComObject3942315138.h"
#include "System_Xml_U3CModuleU3E86524790.h"
#include "System_Xml_System_MonoTODOAttribute2091695241.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentitySelector3090381356.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityField471607183.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityPath691771506.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityStep691878681.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryField372543514.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryFieldCollecti720268184.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntry526158530.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryCollection1006318656.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyTable539606558.h"
#include "System_Xml_Mono_Xml_Schema_XsdParticleStateManager1144338417.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationState2934101963.h"
#include "System_Xml_Mono_Xml_Schema_XsdElementValidationSta2329295339.h"
#include "System_Xml_Mono_Xml_Schema_XsdSequenceValidationSt1326242762.h"
#include "System_Xml_Mono_Xml_Schema_XsdChoiceValidationStat1509569130.h"
#include "System_Xml_Mono_Xml_Schema_XsdAllValidationState3644125094.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyValidationState4008767483.h"
#include "System_Xml_Mono_Xml_Schema_XsdAppendedValidationSt2779068242.h"
#include "System_Xml_Mono_Xml_Schema_XsdEmptyValidationState3672997626.h"
#include "System_Xml_Mono_Xml_Schema_XsdInvalidValidationStat795849104.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader1865486949.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationContext4179462825.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDManager1983908837.h"
#include "System_Xml_Mono_Xml_Schema_XsdWildcard3171165001.h"
#include "System_Xml_System_Xml_ConformanceLevel233510445.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory809616844.h"
#include "System_Xml_Mono_Xml_DTDAutomata415542374.h"
#include "System_Xml_Mono_Xml_DTDElementAutomata966321246.h"
#include "System_Xml_Mono_Xml_DTDChoiceAutomata1534320551.h"
#include "System_Xml_Mono_Xml_DTDSequenceAutomata1500816711.h"
#include "System_Xml_Mono_Xml_DTDOneOrMoreAutomata4059181504.h"
#include "System_Xml_Mono_Xml_DTDEmptyAutomata488285487.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1001[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1002[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (KeyNotFoundException_t876339989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1006[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1007[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1010[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1011[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1012[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1013[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (PrimalityTest_t2979531601), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (AppDomainInitializer_t691583313), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (AssemblyLoadEventHandler_t3394555856), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (ConsoleCancelEventHandler_t3348198711), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (EventHandler_t2463957060), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (ResolveEventHandler_t2622793906), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (UnhandledExceptionEventHandler_t2544755120), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (MemberFilter_t1019468612), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (ModuleResolveEventHandler_t626980826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (TypeFilter_t3531778276), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (CrossContextDelegate_t651537830), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (HeaderHandler_t53887223), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (ParameterizedThreadStart_t669514423), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (ThreadStart_t124146534), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (TimerCallback_t2757719768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (WaitCallback_t777536786), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (WaitOrTimerCallback_t2612136768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238933), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1037[59] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D15_7(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D16_8(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D17_9(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D18_10(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D19_11(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D20_12(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D24_16(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D25_17(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D26_18(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D27_19(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D28_20(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D29_21(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D30_22(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D31_23(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D32_24(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D34_25(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D35_26(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D36_27(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D37_28(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D38_29(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D39_30(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D40_31(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D41_32(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D42_33(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D43_34(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D44_35(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D45_36(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D46_37(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D47_38(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D48_39(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D49_40(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D50_41(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D51_42(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D52_43(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D53_44(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D54_45(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D55_46(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D56_47(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D57_48(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D58_49(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D59_50(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D60_51(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D61_52(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D62_53(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D63_54(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D64_55(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D65_56(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D67_57(),
	U3CPrivateImplementationDetailsU3E_t3053238933_StaticFields::get_offset_of_U24U24fieldU2D68_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (U24ArrayTypeU2452_t3379220472)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t3379220472_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { sizeof (U24ArrayTypeU2424_t3379220381)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2424_t3379220381_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (U24ArrayTypeU2416_t3379220352)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220352_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (U24ArrayTypeU24120_t1676615732)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1676615732_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { sizeof (U24ArrayTypeU243132_t435538904)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t435538904_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { sizeof (U24ArrayTypeU2420_t3379220377)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220377_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (U24ArrayTypeU2432_t3379220410)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3379220410_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (U24ArrayTypeU2448_t3379220447)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t3379220447_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (U24ArrayTypeU2464_t3379220505)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t3379220505_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (U24ArrayTypeU2412_t3379220348)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220348_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (U24ArrayTypeU241668_t435484226)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241668_t435484226_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (U24ArrayTypeU242100_t435509018)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242100_t435509018_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (U24ArrayTypeU241452_t435482267)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241452_t435482267_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (U24ArrayTypeU24136_t1676615769)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1676615769_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { sizeof (U24ArrayTypeU248_t3988332413)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3988332413_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (U24ArrayTypeU2484_t3379220567)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2484_t3379220567_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (U24ArrayTypeU2472_t3379220534)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t3379220534_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (U24ArrayTypeU24124_t1676615736)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24124_t1676615736_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (U24ArrayTypeU2496_t3379220600)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2496_t3379220600_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (U24ArrayTypeU242048_t435508189)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242048_t435508189_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (U24ArrayTypeU2456_t3379220476)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2456_t3379220476_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (U24ArrayTypeU24256_t1676616792)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616792_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (U24ArrayTypeU241024_t435478332)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t435478332_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (U24ArrayTypeU24640_t1676620599)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24640_t1676620599_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (U24ArrayTypeU24160_t1676615856)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24160_t1676615856_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { sizeof (U24ArrayTypeU24128_t1676615740)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t1676615740_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { sizeof (Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { sizeof (U3CModuleU3E_t86524791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (MonoTODOAttribute_t2091695242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (XsdIdentitySelector_t3090381356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1067[3] = 
{
	XsdIdentitySelector_t3090381356::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t3090381356::get_offset_of_fields_1(),
	XsdIdentitySelector_t3090381356::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (XsdIdentityField_t471607183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1068[2] = 
{
	XsdIdentityField_t471607183::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t471607183::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (XsdIdentityPath_t691771506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1069[2] = 
{
	XsdIdentityPath_t691771506::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t691771506::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (XsdIdentityStep_t691878681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1070[6] = 
{
	XsdIdentityStep_t691878681::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t691878681::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t691878681::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t691878681::get_offset_of_NsName_3(),
	XsdIdentityStep_t691878681::get_offset_of_Name_4(),
	XsdIdentityStep_t691878681::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (XsdKeyEntryField_t372543514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1071[13] = 
{
	XsdKeyEntryField_t372543514::get_offset_of_entry_0(),
	XsdKeyEntryField_t372543514::get_offset_of_field_1(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t372543514::get_offset_of_Identity_7(),
	XsdKeyEntryField_t372543514::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t372543514::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t372543514::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t372543514::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (XsdKeyEntryFieldCollection_t720268184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (XsdKeyEntry_t526158530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1073[8] = 
{
	XsdKeyEntry_t526158530::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t526158530::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t526158530::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t526158530::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t526158530::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t526158530::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t526158530::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t526158530::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (XsdKeyEntryCollection_t1006318656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (XsdKeyTable_t539606558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1075[9] = 
{
	XsdKeyTable_t539606558::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t539606558::get_offset_of_selector_1(),
	XsdKeyTable_t539606558::get_offset_of_source_2(),
	XsdKeyTable_t539606558::get_offset_of_qname_3(),
	XsdKeyTable_t539606558::get_offset_of_refKeyName_4(),
	XsdKeyTable_t539606558::get_offset_of_Entries_5(),
	XsdKeyTable_t539606558::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t539606558::get_offset_of_StartDepth_7(),
	XsdKeyTable_t539606558::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (XsdParticleStateManager_t1144338417), -1, sizeof(XsdParticleStateManager_t1144338417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1076[6] = 
{
	XsdParticleStateManager_t1144338417::get_offset_of_table_0(),
	XsdParticleStateManager_t1144338417::get_offset_of_processContents_1(),
	XsdParticleStateManager_t1144338417::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t1144338417::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t1144338417::get_offset_of_Context_4(),
	XsdParticleStateManager_t1144338417_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (XsdValidationState_t2934101963), -1, sizeof(XsdValidationState_t2934101963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1077[3] = 
{
	XsdValidationState_t2934101963_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t2934101963::get_offset_of_occured_1(),
	XsdValidationState_t2934101963::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (XsdElementValidationState_t2329295339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1078[1] = 
{
	XsdElementValidationState_t2329295339::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (XsdSequenceValidationState_t1326242762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1079[4] = 
{
	XsdSequenceValidationState_t1326242762::get_offset_of_seq_3(),
	XsdSequenceValidationState_t1326242762::get_offset_of_current_4(),
	XsdSequenceValidationState_t1326242762::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t1326242762::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (XsdChoiceValidationState_t1509569130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1080[3] = 
{
	XsdChoiceValidationState_t1509569130::get_offset_of_choice_3(),
	XsdChoiceValidationState_t1509569130::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t1509569130::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (XsdAllValidationState_t3644125094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1081[2] = 
{
	XsdAllValidationState_t3644125094::get_offset_of_all_3(),
	XsdAllValidationState_t3644125094::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (XsdAnyValidationState_t4008767483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1082[1] = 
{
	XsdAnyValidationState_t4008767483::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (XsdAppendedValidationState_t2779068242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1083[2] = 
{
	XsdAppendedValidationState_t2779068242::get_offset_of_head_3(),
	XsdAppendedValidationState_t2779068242::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (XsdEmptyValidationState_t3672997626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (XsdInvalidValidationState_t795849104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (XsdValidatingReader_t1865486949), -1, sizeof(XsdValidatingReader_t1865486949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1086[30] = 
{
	XsdValidatingReader_t1865486949_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t1865486949::get_offset_of_reader_3(),
	XsdValidatingReader_t1865486949::get_offset_of_resolver_4(),
	XsdValidatingReader_t1865486949::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t1865486949::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t1865486949::get_offset_of_validationType_7(),
	XsdValidatingReader_t1865486949::get_offset_of_schemas_8(),
	XsdValidatingReader_t1865486949::get_offset_of_namespaces_9(),
	XsdValidatingReader_t1865486949::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t1865486949::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t1865486949::get_offset_of_idManager_12(),
	XsdValidatingReader_t1865486949::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t1865486949::get_offset_of_keyTables_14(),
	XsdValidatingReader_t1865486949::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t1865486949::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t1865486949::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t1865486949::get_offset_of_state_18(),
	XsdValidatingReader_t1865486949::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t1865486949::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t1865486949::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t1865486949::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t1865486949::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t1865486949::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t1865486949::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t1865486949::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t1865486949::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t1865486949::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t1865486949_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t1865486949_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t1865486949_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (XsdValidationContext_t4179462825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1087[3] = 
{
	XsdValidationContext_t4179462825::get_offset_of_xsi_type_0(),
	XsdValidationContext_t4179462825::get_offset_of_State_1(),
	XsdValidationContext_t4179462825::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (XsdIDManager_t1983908837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1088[3] = 
{
	XsdIDManager_t1983908837::get_offset_of_idList_0(),
	XsdIDManager_t1983908837::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t1983908837::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (XsdWildcard_t3171165001), -1, sizeof(XsdWildcard_t3171165001_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1089[10] = 
{
	XsdWildcard_t3171165001::get_offset_of_xsobj_0(),
	XsdWildcard_t3171165001::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t3171165001::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t3171165001::get_offset_of_SkipCompile_3(),
	XsdWildcard_t3171165001::get_offset_of_HasValueAny_4(),
	XsdWildcard_t3171165001::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t3171165001::get_offset_of_HasValueOther_6(),
	XsdWildcard_t3171165001::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t3171165001::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t3171165001_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { sizeof (ConformanceLevel_t233510445)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1092[4] = 
{
	ConformanceLevel_t233510445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (DTDAutomataFactory_t809616844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1093[3] = 
{
	DTDAutomataFactory_t809616844::get_offset_of_root_0(),
	DTDAutomataFactory_t809616844::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t809616844::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (DTDAutomata_t415542374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1094[1] = 
{
	DTDAutomata_t415542374::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (DTDElementAutomata_t966321246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1095[1] = 
{
	DTDElementAutomata_t966321246::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (DTDChoiceAutomata_t1534320551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1096[4] = 
{
	DTDChoiceAutomata_t1534320551::get_offset_of_left_1(),
	DTDChoiceAutomata_t1534320551::get_offset_of_right_2(),
	DTDChoiceAutomata_t1534320551::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t1534320551::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (DTDSequenceAutomata_t1500816711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1097[4] = 
{
	DTDSequenceAutomata_t1500816711::get_offset_of_left_1(),
	DTDSequenceAutomata_t1500816711::get_offset_of_right_2(),
	DTDSequenceAutomata_t1500816711::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t1500816711::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (DTDOneOrMoreAutomata_t4059181504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1098[1] = 
{
	DTDOneOrMoreAutomata_t4059181504::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (DTDEmptyAutomata_t488285487), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
