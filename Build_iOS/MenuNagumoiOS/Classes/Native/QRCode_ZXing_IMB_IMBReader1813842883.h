﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Char[][]
struct CharU5BU5DU5BU5D_t2611876246;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t728975084;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.IMB.IMBReader
struct  IMBReader_t1813842883  : public OneDReader_t3436042911
{
public:
	// ZXing.BinaryBitmap ZXing.IMB.IMBReader::currentBitmap
	BinaryBitmap_t2444664454 * ___currentBitmap_26;

public:
	inline static int32_t get_offset_of_currentBitmap_26() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883, ___currentBitmap_26)); }
	inline BinaryBitmap_t2444664454 * get_currentBitmap_26() const { return ___currentBitmap_26; }
	inline BinaryBitmap_t2444664454 ** get_address_of_currentBitmap_26() { return &___currentBitmap_26; }
	inline void set_currentBitmap_26(BinaryBitmap_t2444664454 * value)
	{
		___currentBitmap_26 = value;
		Il2CppCodeGenWriteBarrier(&___currentBitmap_26, value);
	}
};

struct IMBReader_t1813842883_StaticFields
{
public:
	// System.Int32[] ZXing.IMB.IMBReader::barPosA
	Int32U5BU5D_t3230847821* ___barPosA_2;
	// System.Int32[] ZXing.IMB.IMBReader::barPosB
	Int32U5BU5D_t3230847821* ___barPosB_3;
	// System.Int32[] ZXing.IMB.IMBReader::barPosC
	Int32U5BU5D_t3230847821* ___barPosC_4;
	// System.Int32[] ZXing.IMB.IMBReader::barPosD
	Int32U5BU5D_t3230847821* ___barPosD_5;
	// System.Int32[] ZXing.IMB.IMBReader::barPosE
	Int32U5BU5D_t3230847821* ___barPosE_6;
	// System.Int32[] ZXing.IMB.IMBReader::barPosF
	Int32U5BU5D_t3230847821* ___barPosF_7;
	// System.Int32[] ZXing.IMB.IMBReader::barPosG
	Int32U5BU5D_t3230847821* ___barPosG_8;
	// System.Int32[] ZXing.IMB.IMBReader::barPosH
	Int32U5BU5D_t3230847821* ___barPosH_9;
	// System.Int32[] ZXing.IMB.IMBReader::barPosI
	Int32U5BU5D_t3230847821* ___barPosI_10;
	// System.Int32[] ZXing.IMB.IMBReader::barPosJ
	Int32U5BU5D_t3230847821* ___barPosJ_11;
	// System.Int32[][] ZXing.IMB.IMBReader::barPos
	Int32U5BU5DU5BU5D_t1820556512* ___barPos_12;
	// System.Char[] ZXing.IMB.IMBReader::barTypeA
	CharU5BU5D_t3324145743* ___barTypeA_13;
	// System.Char[] ZXing.IMB.IMBReader::barTypeB
	CharU5BU5D_t3324145743* ___barTypeB_14;
	// System.Char[] ZXing.IMB.IMBReader::barTypeC
	CharU5BU5D_t3324145743* ___barTypeC_15;
	// System.Char[] ZXing.IMB.IMBReader::barTypeD
	CharU5BU5D_t3324145743* ___barTypeD_16;
	// System.Char[] ZXing.IMB.IMBReader::barTypeE
	CharU5BU5D_t3324145743* ___barTypeE_17;
	// System.Char[] ZXing.IMB.IMBReader::barTypeF
	CharU5BU5D_t3324145743* ___barTypeF_18;
	// System.Char[] ZXing.IMB.IMBReader::barTypeG
	CharU5BU5D_t3324145743* ___barTypeG_19;
	// System.Char[] ZXing.IMB.IMBReader::barTypeH
	CharU5BU5D_t3324145743* ___barTypeH_20;
	// System.Char[] ZXing.IMB.IMBReader::barTypeI
	CharU5BU5D_t3324145743* ___barTypeI_21;
	// System.Char[] ZXing.IMB.IMBReader::barTypeJ
	CharU5BU5D_t3324145743* ___barTypeJ_22;
	// System.Char[][] ZXing.IMB.IMBReader::barType
	CharU5BU5DU5BU5D_t2611876246* ___barType_23;
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> ZXing.IMB.IMBReader::table1Check
	Il2CppObject* ___table1Check_24;
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> ZXing.IMB.IMBReader::table2Check
	Il2CppObject* ___table2Check_25;

public:
	inline static int32_t get_offset_of_barPosA_2() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosA_2)); }
	inline Int32U5BU5D_t3230847821* get_barPosA_2() const { return ___barPosA_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosA_2() { return &___barPosA_2; }
	inline void set_barPosA_2(Int32U5BU5D_t3230847821* value)
	{
		___barPosA_2 = value;
		Il2CppCodeGenWriteBarrier(&___barPosA_2, value);
	}

	inline static int32_t get_offset_of_barPosB_3() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosB_3)); }
	inline Int32U5BU5D_t3230847821* get_barPosB_3() const { return ___barPosB_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosB_3() { return &___barPosB_3; }
	inline void set_barPosB_3(Int32U5BU5D_t3230847821* value)
	{
		___barPosB_3 = value;
		Il2CppCodeGenWriteBarrier(&___barPosB_3, value);
	}

	inline static int32_t get_offset_of_barPosC_4() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosC_4)); }
	inline Int32U5BU5D_t3230847821* get_barPosC_4() const { return ___barPosC_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosC_4() { return &___barPosC_4; }
	inline void set_barPosC_4(Int32U5BU5D_t3230847821* value)
	{
		___barPosC_4 = value;
		Il2CppCodeGenWriteBarrier(&___barPosC_4, value);
	}

	inline static int32_t get_offset_of_barPosD_5() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosD_5)); }
	inline Int32U5BU5D_t3230847821* get_barPosD_5() const { return ___barPosD_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosD_5() { return &___barPosD_5; }
	inline void set_barPosD_5(Int32U5BU5D_t3230847821* value)
	{
		___barPosD_5 = value;
		Il2CppCodeGenWriteBarrier(&___barPosD_5, value);
	}

	inline static int32_t get_offset_of_barPosE_6() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosE_6)); }
	inline Int32U5BU5D_t3230847821* get_barPosE_6() const { return ___barPosE_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosE_6() { return &___barPosE_6; }
	inline void set_barPosE_6(Int32U5BU5D_t3230847821* value)
	{
		___barPosE_6 = value;
		Il2CppCodeGenWriteBarrier(&___barPosE_6, value);
	}

	inline static int32_t get_offset_of_barPosF_7() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosF_7)); }
	inline Int32U5BU5D_t3230847821* get_barPosF_7() const { return ___barPosF_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosF_7() { return &___barPosF_7; }
	inline void set_barPosF_7(Int32U5BU5D_t3230847821* value)
	{
		___barPosF_7 = value;
		Il2CppCodeGenWriteBarrier(&___barPosF_7, value);
	}

	inline static int32_t get_offset_of_barPosG_8() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosG_8)); }
	inline Int32U5BU5D_t3230847821* get_barPosG_8() const { return ___barPosG_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosG_8() { return &___barPosG_8; }
	inline void set_barPosG_8(Int32U5BU5D_t3230847821* value)
	{
		___barPosG_8 = value;
		Il2CppCodeGenWriteBarrier(&___barPosG_8, value);
	}

	inline static int32_t get_offset_of_barPosH_9() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosH_9)); }
	inline Int32U5BU5D_t3230847821* get_barPosH_9() const { return ___barPosH_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosH_9() { return &___barPosH_9; }
	inline void set_barPosH_9(Int32U5BU5D_t3230847821* value)
	{
		___barPosH_9 = value;
		Il2CppCodeGenWriteBarrier(&___barPosH_9, value);
	}

	inline static int32_t get_offset_of_barPosI_10() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosI_10)); }
	inline Int32U5BU5D_t3230847821* get_barPosI_10() const { return ___barPosI_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosI_10() { return &___barPosI_10; }
	inline void set_barPosI_10(Int32U5BU5D_t3230847821* value)
	{
		___barPosI_10 = value;
		Il2CppCodeGenWriteBarrier(&___barPosI_10, value);
	}

	inline static int32_t get_offset_of_barPosJ_11() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPosJ_11)); }
	inline Int32U5BU5D_t3230847821* get_barPosJ_11() const { return ___barPosJ_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_barPosJ_11() { return &___barPosJ_11; }
	inline void set_barPosJ_11(Int32U5BU5D_t3230847821* value)
	{
		___barPosJ_11 = value;
		Il2CppCodeGenWriteBarrier(&___barPosJ_11, value);
	}

	inline static int32_t get_offset_of_barPos_12() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barPos_12)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_barPos_12() const { return ___barPos_12; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_barPos_12() { return &___barPos_12; }
	inline void set_barPos_12(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___barPos_12 = value;
		Il2CppCodeGenWriteBarrier(&___barPos_12, value);
	}

	inline static int32_t get_offset_of_barTypeA_13() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeA_13)); }
	inline CharU5BU5D_t3324145743* get_barTypeA_13() const { return ___barTypeA_13; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeA_13() { return &___barTypeA_13; }
	inline void set_barTypeA_13(CharU5BU5D_t3324145743* value)
	{
		___barTypeA_13 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeA_13, value);
	}

	inline static int32_t get_offset_of_barTypeB_14() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeB_14)); }
	inline CharU5BU5D_t3324145743* get_barTypeB_14() const { return ___barTypeB_14; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeB_14() { return &___barTypeB_14; }
	inline void set_barTypeB_14(CharU5BU5D_t3324145743* value)
	{
		___barTypeB_14 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeB_14, value);
	}

	inline static int32_t get_offset_of_barTypeC_15() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeC_15)); }
	inline CharU5BU5D_t3324145743* get_barTypeC_15() const { return ___barTypeC_15; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeC_15() { return &___barTypeC_15; }
	inline void set_barTypeC_15(CharU5BU5D_t3324145743* value)
	{
		___barTypeC_15 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeC_15, value);
	}

	inline static int32_t get_offset_of_barTypeD_16() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeD_16)); }
	inline CharU5BU5D_t3324145743* get_barTypeD_16() const { return ___barTypeD_16; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeD_16() { return &___barTypeD_16; }
	inline void set_barTypeD_16(CharU5BU5D_t3324145743* value)
	{
		___barTypeD_16 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeD_16, value);
	}

	inline static int32_t get_offset_of_barTypeE_17() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeE_17)); }
	inline CharU5BU5D_t3324145743* get_barTypeE_17() const { return ___barTypeE_17; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeE_17() { return &___barTypeE_17; }
	inline void set_barTypeE_17(CharU5BU5D_t3324145743* value)
	{
		___barTypeE_17 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeE_17, value);
	}

	inline static int32_t get_offset_of_barTypeF_18() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeF_18)); }
	inline CharU5BU5D_t3324145743* get_barTypeF_18() const { return ___barTypeF_18; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeF_18() { return &___barTypeF_18; }
	inline void set_barTypeF_18(CharU5BU5D_t3324145743* value)
	{
		___barTypeF_18 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeF_18, value);
	}

	inline static int32_t get_offset_of_barTypeG_19() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeG_19)); }
	inline CharU5BU5D_t3324145743* get_barTypeG_19() const { return ___barTypeG_19; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeG_19() { return &___barTypeG_19; }
	inline void set_barTypeG_19(CharU5BU5D_t3324145743* value)
	{
		___barTypeG_19 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeG_19, value);
	}

	inline static int32_t get_offset_of_barTypeH_20() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeH_20)); }
	inline CharU5BU5D_t3324145743* get_barTypeH_20() const { return ___barTypeH_20; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeH_20() { return &___barTypeH_20; }
	inline void set_barTypeH_20(CharU5BU5D_t3324145743* value)
	{
		___barTypeH_20 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeH_20, value);
	}

	inline static int32_t get_offset_of_barTypeI_21() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeI_21)); }
	inline CharU5BU5D_t3324145743* get_barTypeI_21() const { return ___barTypeI_21; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeI_21() { return &___barTypeI_21; }
	inline void set_barTypeI_21(CharU5BU5D_t3324145743* value)
	{
		___barTypeI_21 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeI_21, value);
	}

	inline static int32_t get_offset_of_barTypeJ_22() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barTypeJ_22)); }
	inline CharU5BU5D_t3324145743* get_barTypeJ_22() const { return ___barTypeJ_22; }
	inline CharU5BU5D_t3324145743** get_address_of_barTypeJ_22() { return &___barTypeJ_22; }
	inline void set_barTypeJ_22(CharU5BU5D_t3324145743* value)
	{
		___barTypeJ_22 = value;
		Il2CppCodeGenWriteBarrier(&___barTypeJ_22, value);
	}

	inline static int32_t get_offset_of_barType_23() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___barType_23)); }
	inline CharU5BU5DU5BU5D_t2611876246* get_barType_23() const { return ___barType_23; }
	inline CharU5BU5DU5BU5D_t2611876246** get_address_of_barType_23() { return &___barType_23; }
	inline void set_barType_23(CharU5BU5DU5BU5D_t2611876246* value)
	{
		___barType_23 = value;
		Il2CppCodeGenWriteBarrier(&___barType_23, value);
	}

	inline static int32_t get_offset_of_table1Check_24() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___table1Check_24)); }
	inline Il2CppObject* get_table1Check_24() const { return ___table1Check_24; }
	inline Il2CppObject** get_address_of_table1Check_24() { return &___table1Check_24; }
	inline void set_table1Check_24(Il2CppObject* value)
	{
		___table1Check_24 = value;
		Il2CppCodeGenWriteBarrier(&___table1Check_24, value);
	}

	inline static int32_t get_offset_of_table2Check_25() { return static_cast<int32_t>(offsetof(IMBReader_t1813842883_StaticFields, ___table2Check_25)); }
	inline Il2CppObject* get_table2Check_25() const { return ___table2Check_25; }
	inline Il2CppObject** get_address_of_table2Check_25() { return &___table2Check_25; }
	inline void set_table2Check_25(Il2CppObject* value)
	{
		___table2Check_25 = value;
		Il2CppCodeGenWriteBarrier(&___table2Check_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
