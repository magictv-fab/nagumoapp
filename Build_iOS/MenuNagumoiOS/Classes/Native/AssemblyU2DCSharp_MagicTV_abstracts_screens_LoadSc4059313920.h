﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler
struct OnLoadAnswerEventHandler_t3624955211;

#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_Genera2842388060.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.screens.LoadScreenAbstract
struct  LoadScreenAbstract_t4059313920  : public GeneralScreenAbstract_t2842388060
{
public:
	// MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler MagicTV.abstracts.screens.LoadScreenAbstract::onLoadCanceled
	OnLoadAnswerEventHandler_t3624955211 * ___onLoadCanceled_5;
	// MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler MagicTV.abstracts.screens.LoadScreenAbstract::onLoadConfirmed
	OnLoadAnswerEventHandler_t3624955211 * ___onLoadConfirmed_6;
	// MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler MagicTV.abstracts.screens.LoadScreenAbstract::onLoadConfirm
	OnLoadAnswerEventHandler_t3624955211 * ___onLoadConfirm_7;

public:
	inline static int32_t get_offset_of_onLoadCanceled_5() { return static_cast<int32_t>(offsetof(LoadScreenAbstract_t4059313920, ___onLoadCanceled_5)); }
	inline OnLoadAnswerEventHandler_t3624955211 * get_onLoadCanceled_5() const { return ___onLoadCanceled_5; }
	inline OnLoadAnswerEventHandler_t3624955211 ** get_address_of_onLoadCanceled_5() { return &___onLoadCanceled_5; }
	inline void set_onLoadCanceled_5(OnLoadAnswerEventHandler_t3624955211 * value)
	{
		___onLoadCanceled_5 = value;
		Il2CppCodeGenWriteBarrier(&___onLoadCanceled_5, value);
	}

	inline static int32_t get_offset_of_onLoadConfirmed_6() { return static_cast<int32_t>(offsetof(LoadScreenAbstract_t4059313920, ___onLoadConfirmed_6)); }
	inline OnLoadAnswerEventHandler_t3624955211 * get_onLoadConfirmed_6() const { return ___onLoadConfirmed_6; }
	inline OnLoadAnswerEventHandler_t3624955211 ** get_address_of_onLoadConfirmed_6() { return &___onLoadConfirmed_6; }
	inline void set_onLoadConfirmed_6(OnLoadAnswerEventHandler_t3624955211 * value)
	{
		___onLoadConfirmed_6 = value;
		Il2CppCodeGenWriteBarrier(&___onLoadConfirmed_6, value);
	}

	inline static int32_t get_offset_of_onLoadConfirm_7() { return static_cast<int32_t>(offsetof(LoadScreenAbstract_t4059313920, ___onLoadConfirm_7)); }
	inline OnLoadAnswerEventHandler_t3624955211 * get_onLoadConfirm_7() const { return ___onLoadConfirm_7; }
	inline OnLoadAnswerEventHandler_t3624955211 ** get_address_of_onLoadConfirm_7() { return &___onLoadConfirm_7; }
	inline void set_onLoadConfirm_7(OnLoadAnswerEventHandler_t3624955211 * value)
	{
		___onLoadConfirm_7 = value;
		Il2CppCodeGenWriteBarrier(&___onLoadConfirm_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
