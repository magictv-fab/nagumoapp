﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler
struct OnMoveHandler_t662891892;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnMoveHandler__ctor_m2419045195 (OnMoveHandler_t662891892 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler::Invoke(UnityEngine.Vector3)
extern "C"  void OnMoveHandler_Invoke_m3437296724 (OnMoveHandler_t662891892 * __this, Vector3_t4282066566  ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnMoveHandler_BeginInvoke_m2066449683 (OnMoveHandler_t662891892 * __this, Vector3_t4282066566  ___factor0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnMoveHandler_EndInvoke_m157188827 (OnMoveHandler_t662891892 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
