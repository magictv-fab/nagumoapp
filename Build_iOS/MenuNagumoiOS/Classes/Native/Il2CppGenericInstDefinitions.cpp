﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t1153838500_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0 = { 1, GenInst_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType Char_t2862622538_0_0_0;
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Types[] = { &Char_t2862622538_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0 = { 1, GenInst_Char_t2862622538_0_0_0_Types };
extern const Il2CppType IConvertible_t2116191568_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2116191568_0_0_0_Types[] = { &IConvertible_t2116191568_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2116191568_0_0_0 = { 1, GenInst_IConvertible_t2116191568_0_0_0_Types };
extern const Il2CppType IComparable_t1391370361_0_0_0;
static const Il2CppType* GenInst_IComparable_t1391370361_0_0_0_Types[] = { &IComparable_t1391370361_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1391370361_0_0_0 = { 1, GenInst_IComparable_t1391370361_0_0_0_Types };
extern const Il2CppType IComparable_1_t2772552329_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2772552329_0_0_0_Types[] = { &IComparable_1_t2772552329_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2772552329_0_0_0 = { 1, GenInst_IComparable_1_t2772552329_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2411901105_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2411901105_0_0_0_Types[] = { &IEquatable_1_t2411901105_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2411901105_0_0_0 = { 1, GenInst_IEquatable_1_t2411901105_0_0_0_Types };
extern const Il2CppType ValueType_t1744280289_0_0_0;
static const Il2CppType* GenInst_ValueType_t1744280289_0_0_0_Types[] = { &ValueType_t1744280289_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1744280289_0_0_0 = { 1, GenInst_ValueType_t1744280289_0_0_0_Types };
extern const Il2CppType Int64_t1153838595_0_0_0;
static const Il2CppType* GenInst_Int64_t1153838595_0_0_0_Types[] = { &Int64_t1153838595_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1153838595_0_0_0 = { 1, GenInst_Int64_t1153838595_0_0_0_Types };
extern const Il2CppType UInt32_t24667981_0_0_0;
static const Il2CppType* GenInst_UInt32_t24667981_0_0_0_Types[] = { &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t24667981_0_0_0 = { 1, GenInst_UInt32_t24667981_0_0_0_Types };
extern const Il2CppType UInt64_t24668076_0_0_0;
static const Il2CppType* GenInst_UInt64_t24668076_0_0_0_Types[] = { &UInt64_t24668076_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t24668076_0_0_0 = { 1, GenInst_UInt64_t24668076_0_0_0_Types };
extern const Il2CppType Byte_t2862609660_0_0_0;
static const Il2CppType* GenInst_Byte_t2862609660_0_0_0_Types[] = { &Byte_t2862609660_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t2862609660_0_0_0 = { 1, GenInst_Byte_t2862609660_0_0_0_Types };
extern const Il2CppType SByte_t1161769777_0_0_0;
static const Il2CppType* GenInst_SByte_t1161769777_0_0_0_Types[] = { &SByte_t1161769777_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1161769777_0_0_0 = { 1, GenInst_SByte_t1161769777_0_0_0_Types };
extern const Il2CppType Int16_t1153838442_0_0_0;
static const Il2CppType* GenInst_Int16_t1153838442_0_0_0_Types[] = { &Int16_t1153838442_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1153838442_0_0_0 = { 1, GenInst_Int16_t1153838442_0_0_0_Types };
extern const Il2CppType UInt16_t24667923_0_0_0;
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_Types[] = { &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0 = { 1, GenInst_UInt16_t24667923_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t3464557803_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t3464557803_0_0_0_Types[] = { &IEnumerable_t3464557803_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t3464557803_0_0_0 = { 1, GenInst_IEnumerable_t3464557803_0_0_0_Types };
extern const Il2CppType ICloneable_t1025544834_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1025544834_0_0_0_Types[] = { &ICloneable_t1025544834_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1025544834_0_0_0 = { 1, GenInst_ICloneable_t1025544834_0_0_0_Types };
extern const Il2CppType IComparable_1_t4212128644_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4212128644_0_0_0_Types[] = { &IComparable_1_t4212128644_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4212128644_0_0_0 = { 1, GenInst_IComparable_1_t4212128644_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3851477420_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3851477420_0_0_0_Types[] = { &IEquatable_1_t3851477420_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3851477420_0_0_0 = { 1, GenInst_IEquatable_1_t3851477420_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t2853506214_0_0_0;
static const Il2CppType* GenInst_IReflect_t2853506214_0_0_0_Types[] = { &IReflect_t2853506214_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2853506214_0_0_0 = { 1, GenInst_IReflect_t2853506214_0_0_0_Types };
extern const Il2CppType _Type_t2149739635_0_0_0;
static const Il2CppType* GenInst__Type_t2149739635_0_0_0_Types[] = { &_Type_t2149739635_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2149739635_0_0_0 = { 1, GenInst__Type_t2149739635_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1425685797_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1425685797_0_0_0_Types[] = { &ICustomAttributeProvider_t1425685797_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1425685797_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1425685797_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3353101921_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3353101921_0_0_0_Types[] = { &_MemberInfo_t3353101921_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3353101921_0_0_0 = { 1, GenInst__MemberInfo_t3353101921_0_0_0_Types };
extern const Il2CppType IFormattable_t382002946_0_0_0;
static const Il2CppType* GenInst_IFormattable_t382002946_0_0_0_Types[] = { &IFormattable_t382002946_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t382002946_0_0_0 = { 1, GenInst_IFormattable_t382002946_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768291_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768291_0_0_0_Types[] = { &IComparable_1_t1063768291_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768291_0_0_0 = { 1, GenInst_IComparable_1_t1063768291_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117067_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117067_0_0_0_Types[] = { &IEquatable_1_t703117067_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117067_0_0_0 = { 1, GenInst_IEquatable_1_t703117067_0_0_0_Types };
extern const Il2CppType Double_t3868226565_0_0_0;
static const Il2CppType* GenInst_Double_t3868226565_0_0_0_Types[] = { &Double_t3868226565_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t3868226565_0_0_0 = { 1, GenInst_Double_t3868226565_0_0_0_Types };
extern const Il2CppType IComparable_1_t3778156356_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3778156356_0_0_0_Types[] = { &IComparable_1_t3778156356_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3778156356_0_0_0 = { 1, GenInst_IComparable_1_t3778156356_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3417505132_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3417505132_0_0_0_Types[] = { &IEquatable_1_t3417505132_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3417505132_0_0_0 = { 1, GenInst_IEquatable_1_t3417505132_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565068_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565068_0_0_0_Types[] = { &IComparable_1_t4229565068_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565068_0_0_0 = { 1, GenInst_IComparable_1_t4229565068_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913844_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913844_0_0_0_Types[] = { &IEquatable_1_t3868913844_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913844_0_0_0 = { 1, GenInst_IEquatable_1_t3868913844_0_0_0_Types };
extern const Il2CppType IComparable_1_t2772539451_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2772539451_0_0_0_Types[] = { &IComparable_1_t2772539451_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2772539451_0_0_0 = { 1, GenInst_IComparable_1_t2772539451_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2411888227_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2411888227_0_0_0_Types[] = { &IEquatable_1_t2411888227_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2411888227_0_0_0 = { 1, GenInst_IEquatable_1_t2411888227_0_0_0_Types };
extern const Il2CppType Single_t4291918972_0_0_0;
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Types[] = { &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0 = { 1, GenInst_Single_t4291918972_0_0_0_Types };
extern const Il2CppType IComparable_1_t4201848763_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4201848763_0_0_0_Types[] = { &IComparable_1_t4201848763_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4201848763_0_0_0 = { 1, GenInst_IComparable_1_t4201848763_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3841197539_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3841197539_0_0_0_Types[] = { &IEquatable_1_t3841197539_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3841197539_0_0_0 = { 1, GenInst_IEquatable_1_t3841197539_0_0_0_Types };
extern const Il2CppType Decimal_t1954350631_0_0_0;
static const Il2CppType* GenInst_Decimal_t1954350631_0_0_0_Types[] = { &Decimal_t1954350631_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1954350631_0_0_0 = { 1, GenInst_Decimal_t1954350631_0_0_0_Types };
extern const Il2CppType Boolean_t476798718_0_0_0;
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Types[] = { &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0 = { 1, GenInst_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Delegate_t3310234105_0_0_0;
static const Il2CppType* GenInst_Delegate_t3310234105_0_0_0_Types[] = { &Delegate_t3310234105_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3310234105_0_0_0 = { 1, GenInst_Delegate_t3310234105_0_0_0_Types };
extern const Il2CppType ISerializable_t867484142_0_0_0;
static const Il2CppType* GenInst_ISerializable_t867484142_0_0_0_Types[] = { &ISerializable_t867484142_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t867484142_0_0_0 = { 1, GenInst_ISerializable_t867484142_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2235474049_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2235474049_0_0_0_Types[] = { &ParameterInfo_t2235474049_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2235474049_0_0_0 = { 1, GenInst_ParameterInfo_t2235474049_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2787166306_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2787166306_0_0_0_Types[] = { &_ParameterInfo_t2787166306_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2787166306_0_0_0 = { 1, GenInst__ParameterInfo_t2787166306_0_0_0_Types };
extern const Il2CppType ParameterModifier_t741930026_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t741930026_0_0_0_Types[] = { &ParameterModifier_t741930026_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t741930026_0_0_0 = { 1, GenInst_ParameterModifier_t741930026_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565010_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565010_0_0_0_Types[] = { &IComparable_1_t4229565010_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565010_0_0_0 = { 1, GenInst_IComparable_1_t4229565010_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913786_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913786_0_0_0_Types[] = { &IEquatable_1_t3868913786_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913786_0_0_0 = { 1, GenInst_IEquatable_1_t3868913786_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565163_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565163_0_0_0_Types[] = { &IComparable_1_t4229565163_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565163_0_0_0 = { 1, GenInst_IComparable_1_t4229565163_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913939_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913939_0_0_0_Types[] = { &IEquatable_1_t3868913939_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913939_0_0_0 = { 1, GenInst_IEquatable_1_t3868913939_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768233_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768233_0_0_0_Types[] = { &IComparable_1_t1063768233_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768233_0_0_0 = { 1, GenInst_IComparable_1_t1063768233_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117009_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117009_0_0_0_Types[] = { &IEquatable_1_t703117009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117009_0_0_0 = { 1, GenInst_IEquatable_1_t703117009_0_0_0_Types };
extern const Il2CppType IComparable_1_t1071699568_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1071699568_0_0_0_Types[] = { &IComparable_1_t1071699568_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1071699568_0_0_0 = { 1, GenInst_IComparable_1_t1071699568_0_0_0_Types };
extern const Il2CppType IEquatable_1_t711048344_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t711048344_0_0_0_Types[] = { &IEquatable_1_t711048344_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t711048344_0_0_0 = { 1, GenInst_IEquatable_1_t711048344_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768386_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768386_0_0_0_Types[] = { &IComparable_1_t1063768386_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768386_0_0_0 = { 1, GenInst_IComparable_1_t1063768386_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117162_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117162_0_0_0_Types[] = { &IEquatable_1_t703117162_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117162_0_0_0 = { 1, GenInst_IEquatable_1_t703117162_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t209867187_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t209867187_0_0_0_Types[] = { &_FieldInfo_t209867187_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t209867187_0_0_0 = { 1, GenInst__FieldInfo_t209867187_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3971289384_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3971289384_0_0_0_Types[] = { &_MethodInfo_t3971289384_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3971289384_0_0_0 = { 1, GenInst__MethodInfo_t3971289384_0_0_0_Types };
extern const Il2CppType MethodBase_t318515428_0_0_0;
static const Il2CppType* GenInst_MethodBase_t318515428_0_0_0_Types[] = { &MethodBase_t318515428_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t318515428_0_0_0 = { 1, GenInst_MethodBase_t318515428_0_0_0_Types };
extern const Il2CppType _MethodBase_t3971068747_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3971068747_0_0_0_Types[] = { &_MethodBase_t3971068747_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3971068747_0_0_0 = { 1, GenInst__MethodBase_t3971068747_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t3711326812_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t3711326812_0_0_0_Types[] = { &_PropertyInfo_t3711326812_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3711326812_0_0_0 = { 1, GenInst__PropertyInfo_t3711326812_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t4136801618_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t4136801618_0_0_0_Types[] = { &ConstructorInfo_t4136801618_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t4136801618_0_0_0 = { 1, GenInst_ConstructorInfo_t4136801618_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3408715251_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3408715251_0_0_0_Types[] = { &_ConstructorInfo_t3408715251_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3408715251_0_0_0 = { 1, GenInst__ConstructorInfo_t3408715251_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t3372848153_0_0_0;
static const Il2CppType* GenInst_TableRange_t3372848153_0_0_0_Types[] = { &TableRange_t3372848153_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t3372848153_0_0_0 = { 1, GenInst_TableRange_t3372848153_0_0_0_Types };
extern const Il2CppType TailoringInfo_t3025807515_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t3025807515_0_0_0_Types[] = { &TailoringInfo_t3025807515_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t3025807515_0_0_0 = { 1, GenInst_TailoringInfo_t3025807515_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3222658402_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0 = { 1, GenInst_KeyValuePair_2_t3222658402_0_0_0_Types };
extern const Il2CppType Link_t2063667470_0_0_0;
static const Il2CppType* GenInst_Link_t2063667470_0_0_0_Types[] = { &Link_t2063667470_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2063667470_0_0_0 = { 1, GenInst_Link_t2063667470_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1751606614_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0 = { 1, GenInst_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1873037576_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1873037576_0_0_0_Types[] = { &KeyValuePair_2_t1873037576_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1873037576_0_0_0 = { 1, GenInst_KeyValuePair_2_t1873037576_0_0_0_Types };
extern const Il2CppType Contraction_t3998770676_0_0_0;
static const Il2CppType* GenInst_Contraction_t3998770676_0_0_0_Types[] = { &Contraction_t3998770676_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t3998770676_0_0_0 = { 1, GenInst_Contraction_t3998770676_0_0_0_Types };
extern const Il2CppType Level2Map_t3664214860_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3664214860_0_0_0_Types[] = { &Level2Map_t3664214860_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3664214860_0_0_0 = { 1, GenInst_Level2Map_t3664214860_0_0_0_Types };
extern const Il2CppType BigInteger_t3334373498_0_0_0;
static const Il2CppType* GenInst_BigInteger_t3334373498_0_0_0_Types[] = { &BigInteger_t3334373498_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t3334373498_0_0_0 = { 1, GenInst_BigInteger_t3334373498_0_0_0_Types };
extern const Il2CppType UriScheme_t3372318283_0_0_0;
static const Il2CppType* GenInst_UriScheme_t3372318283_0_0_0_Types[] = { &UriScheme_t3372318283_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t3372318283_0_0_0 = { 1, GenInst_UriScheme_t3372318283_0_0_0_Types };
extern const Il2CppType KeySizes_t2106826975_0_0_0;
static const Il2CppType* GenInst_KeySizes_t2106826975_0_0_0_Types[] = { &KeySizes_t2106826975_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t2106826975_0_0_0 = { 1, GenInst_KeySizes_t2106826975_0_0_0_Types };
extern const Il2CppType Assembly_t1418687608_0_0_0;
static const Il2CppType* GenInst_Assembly_t1418687608_0_0_0_Types[] = { &Assembly_t1418687608_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t1418687608_0_0_0 = { 1, GenInst_Assembly_t1418687608_0_0_0_Types };
extern const Il2CppType _Assembly_t3789461407_0_0_0;
static const Il2CppType* GenInst__Assembly_t3789461407_0_0_0_Types[] = { &_Assembly_t3789461407_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t3789461407_0_0_0 = { 1, GenInst__Assembly_t3789461407_0_0_0_Types };
extern const Il2CppType IEvidenceFactory_t3943896478_0_0_0;
static const Il2CppType* GenInst_IEvidenceFactory_t3943896478_0_0_0_Types[] = { &IEvidenceFactory_t3943896478_0_0_0 };
extern const Il2CppGenericInst GenInst_IEvidenceFactory_t3943896478_0_0_0 = { 1, GenInst_IEvidenceFactory_t3943896478_0_0_0_Types };
extern const Il2CppType DateTime_t4283661327_0_0_0;
static const Il2CppType* GenInst_DateTime_t4283661327_0_0_0_Types[] = { &DateTime_t4283661327_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t4283661327_0_0_0 = { 1, GenInst_DateTime_t4283661327_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t3884714306_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t3884714306_0_0_0_Types[] = { &DateTimeOffset_t3884714306_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t3884714306_0_0_0 = { 1, GenInst_DateTimeOffset_t3884714306_0_0_0_Types };
extern const Il2CppType TimeSpan_t413522987_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t413522987_0_0_0_Types[] = { &TimeSpan_t413522987_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t413522987_0_0_0 = { 1, GenInst_TimeSpan_t413522987_0_0_0_Types };
extern const Il2CppType Guid_t2862754429_0_0_0;
static const Il2CppType* GenInst_Guid_t2862754429_0_0_0_Types[] = { &Guid_t2862754429_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2862754429_0_0_0 = { 1, GenInst_Guid_t2862754429_0_0_0_Types };
extern const Il2CppType IComparable_1_t386728509_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t386728509_0_0_0_Types[] = { &IComparable_1_t386728509_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t386728509_0_0_0 = { 1, GenInst_IComparable_1_t386728509_0_0_0_Types };
extern const Il2CppType IEquatable_1_t26077285_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t26077285_0_0_0_Types[] = { &IEquatable_1_t26077285_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t26077285_0_0_0 = { 1, GenInst_IEquatable_1_t26077285_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t2955630591_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t2955630591_0_0_0_Types[] = { &CustomAttributeData_t2955630591_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2955630591_0_0_0 = { 1, GenInst_CustomAttributeData_t2955630591_0_0_0_Types };
extern const Il2CppType TermInfoStrings_t2002624446_0_0_0;
static const Il2CppType* GenInst_TermInfoStrings_t2002624446_0_0_0_Types[] = { &TermInfoStrings_t2002624446_0_0_0 };
extern const Il2CppGenericInst GenInst_TermInfoStrings_t2002624446_0_0_0 = { 1, GenInst_TermInfoStrings_t2002624446_0_0_0_Types };
extern const Il2CppType Enum_t2862688501_0_0_0;
static const Il2CppType* GenInst_Enum_t2862688501_0_0_0_Types[] = { &Enum_t2862688501_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2862688501_0_0_0 = { 1, GenInst_Enum_t2862688501_0_0_0_Types };
extern const Il2CppType Version_t763695022_0_0_0;
static const Il2CppType* GenInst_Version_t763695022_0_0_0_Types[] = { &Version_t763695022_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t763695022_0_0_0 = { 1, GenInst_Version_t763695022_0_0_0_Types };
extern const Il2CppType Slot_t2260530181_0_0_0;
static const Il2CppType* GenInst_Slot_t2260530181_0_0_0_Types[] = { &Slot_t2260530181_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2260530181_0_0_0 = { 1, GenInst_Slot_t2260530181_0_0_0_Types };
extern const Il2CppType Slot_t2072023290_0_0_0;
static const Il2CppType* GenInst_Slot_t2072023290_0_0_0_Types[] = { &Slot_t2072023290_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2072023290_0_0_0 = { 1, GenInst_Slot_t2072023290_0_0_0_Types };
extern const Il2CppType StackFrame_t1034942277_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1034942277_0_0_0_Types[] = { &StackFrame_t1034942277_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1034942277_0_0_0 = { 1, GenInst_StackFrame_t1034942277_0_0_0_Types };
extern const Il2CppType Calendar_t3558528576_0_0_0;
static const Il2CppType* GenInst_Calendar_t3558528576_0_0_0_Types[] = { &Calendar_t3558528576_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t3558528576_0_0_0 = { 1, GenInst_Calendar_t3558528576_0_0_0_Types };
extern const Il2CppType CultureInfo_t1065375142_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t1065375142_0_0_0_Types[] = { &CultureInfo_t1065375142_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t1065375142_0_0_0 = { 1, GenInst_CultureInfo_t1065375142_0_0_0_Types };
extern const Il2CppType IFormatProvider_t192740775_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t192740775_0_0_0_Types[] = { &IFormatProvider_t192740775_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t192740775_0_0_0 = { 1, GenInst_IFormatProvider_t192740775_0_0_0_Types };
extern const Il2CppType FileInfo_t3233670074_0_0_0;
static const Il2CppType* GenInst_FileInfo_t3233670074_0_0_0_Types[] = { &FileInfo_t3233670074_0_0_0 };
extern const Il2CppGenericInst GenInst_FileInfo_t3233670074_0_0_0 = { 1, GenInst_FileInfo_t3233670074_0_0_0_Types };
extern const Il2CppType FileSystemInfo_t2605906633_0_0_0;
static const Il2CppType* GenInst_FileSystemInfo_t2605906633_0_0_0_Types[] = { &FileSystemInfo_t2605906633_0_0_0 };
extern const Il2CppGenericInst GenInst_FileSystemInfo_t2605906633_0_0_0 = { 1, GenInst_FileSystemInfo_t2605906633_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1219038801_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1219038801_0_0_0_Types[] = { &MarshalByRefObject_t1219038801_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1219038801_0_0_0 = { 1, GenInst_MarshalByRefObject_t1219038801_0_0_0_Types };
extern const Il2CppType DirectoryInfo_t89154617_0_0_0;
static const Il2CppType* GenInst_DirectoryInfo_t89154617_0_0_0_Types[] = { &DirectoryInfo_t89154617_0_0_0 };
extern const Il2CppGenericInst GenInst_DirectoryInfo_t89154617_0_0_0 = { 1, GenInst_DirectoryInfo_t89154617_0_0_0_Types };
extern const Il2CppType Module_t1394482686_0_0_0;
static const Il2CppType* GenInst_Module_t1394482686_0_0_0_Types[] = { &Module_t1394482686_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1394482686_0_0_0 = { 1, GenInst_Module_t1394482686_0_0_0_Types };
extern const Il2CppType _Module_t2601912805_0_0_0;
static const Il2CppType* GenInst__Module_t2601912805_0_0_0_Types[] = { &_Module_t2601912805_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2601912805_0_0_0 = { 1, GenInst__Module_t2601912805_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t3301293422_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_Types[] = { &CustomAttributeTypedArgument_t3301293422_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t3059612989_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_Types[] = { &CustomAttributeNamedArgument_t3059612989_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t595214213_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t595214213_0_0_0_Types[] = { &ModuleBuilder_t595214213_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t595214213_0_0_0 = { 1, GenInst_ModuleBuilder_t595214213_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1764509690_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1764509690_0_0_0_Types[] = { &_ModuleBuilder_t1764509690_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1764509690_0_0_0 = { 1, GenInst__ModuleBuilder_t1764509690_0_0_0_Types };
extern const Il2CppType MonoResource_t1505432149_0_0_0;
static const Il2CppType* GenInst_MonoResource_t1505432149_0_0_0_Types[] = { &MonoResource_t1505432149_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t1505432149_0_0_0 = { 1, GenInst_MonoResource_t1505432149_0_0_0_Types };
extern const Il2CppType RefEmitPermissionSet_t3880501745_0_0_0;
static const Il2CppType* GenInst_RefEmitPermissionSet_t3880501745_0_0_0_Types[] = { &RefEmitPermissionSet_t3880501745_0_0_0 };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t3880501745_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t3880501745_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3159962230_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3159962230_0_0_0_Types[] = { &ParameterBuilder_t3159962230_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3159962230_0_0_0 = { 1, GenInst_ParameterBuilder_t3159962230_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t4122453611_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t4122453611_0_0_0_Types[] = { &_ParameterBuilder_t4122453611_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t4122453611_0_0_0 = { 1, GenInst__ParameterBuilder_t4122453611_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t3339007067_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t3339007067_0_0_0_Types[] = { &TypeU5BU5D_t3339007067_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t3339007067_0_0_0 = { 1, GenInst_TypeU5BU5D_t3339007067_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t2643922881_0_0_0;
static const Il2CppType* GenInst_ICollection_t2643922881_0_0_0_Types[] = { &ICollection_t2643922881_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t2643922881_0_0_0 = { 1, GenInst_ICollection_t2643922881_0_0_0_Types };
extern const Il2CppType IList_t1751339649_0_0_0;
static const Il2CppType* GenInst_IList_t1751339649_0_0_0_Types[] = { &IList_t1751339649_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1751339649_0_0_0 = { 1, GenInst_IList_t1751339649_0_0_0_Types };
extern const Il2CppType LocalBuilder_t194563060_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t194563060_0_0_0_Types[] = { &LocalBuilder_t194563060_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t194563060_0_0_0 = { 1, GenInst_LocalBuilder_t194563060_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t3375243241_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t3375243241_0_0_0_Types[] = { &_LocalBuilder_t3375243241_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t3375243241_0_0_0 = { 1, GenInst__LocalBuilder_t3375243241_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t962988767_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t962988767_0_0_0_Types[] = { &LocalVariableInfo_t962988767_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t962988767_0_0_0 = { 1, GenInst_LocalVariableInfo_t962988767_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1354080954_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1354080954_0_0_0_Types[] = { &ILTokenInfo_t1354080954_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1354080954_0_0_0 = { 1, GenInst_ILTokenInfo_t1354080954_0_0_0_Types };
extern const Il2CppType LabelData_t3207823784_0_0_0;
static const Il2CppType* GenInst_LabelData_t3207823784_0_0_0_Types[] = { &LabelData_t3207823784_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3207823784_0_0_0 = { 1, GenInst_LabelData_t3207823784_0_0_0_Types };
extern const Il2CppType LabelFixup_t660379442_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t660379442_0_0_0_Types[] = { &LabelFixup_t660379442_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t660379442_0_0_0 = { 1, GenInst_LabelFixup_t660379442_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t553556921_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t553556921_0_0_0_Types[] = { &GenericTypeParameterBuilder_t553556921_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t553556921_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t553556921_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1918497079_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1918497079_0_0_0_Types[] = { &TypeBuilder_t1918497079_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1918497079_0_0_0 = { 1, GenInst_TypeBuilder_t1918497079_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3501492652_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3501492652_0_0_0_Types[] = { &_TypeBuilder_t3501492652_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3501492652_0_0_0 = { 1, GenInst__TypeBuilder_t3501492652_0_0_0_Types };
extern const Il2CppType MethodBuilder_t302405488_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t302405488_0_0_0_Types[] = { &MethodBuilder_t302405488_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t302405488_0_0_0 = { 1, GenInst_MethodBuilder_t302405488_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t1471700965_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t1471700965_0_0_0_Types[] = { &_MethodBuilder_t1471700965_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1471700965_0_0_0 = { 1, GenInst__MethodBuilder_t1471700965_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t3217839941_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t3217839941_0_0_0_Types[] = { &ConstructorBuilder_t3217839941_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t3217839941_0_0_0 = { 1, GenInst_ConstructorBuilder_t3217839941_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t788093754_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t788093754_0_0_0_Types[] = { &_ConstructorBuilder_t788093754_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t788093754_0_0_0 = { 1, GenInst__ConstructorBuilder_t788093754_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t2012258748_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t2012258748_0_0_0_Types[] = { &PropertyBuilder_t2012258748_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t2012258748_0_0_0 = { 1, GenInst_PropertyBuilder_t2012258748_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t752753201_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t752753201_0_0_0_Types[] = { &_PropertyBuilder_t752753201_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t752753201_0_0_0 = { 1, GenInst__PropertyBuilder_t752753201_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1754069893_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1754069893_0_0_0_Types[] = { &FieldBuilder_t1754069893_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1754069893_0_0_0 = { 1, GenInst_FieldBuilder_t1754069893_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t639782778_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t639782778_0_0_0_Types[] = { &_FieldBuilder_t639782778_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t639782778_0_0_0 = { 1, GenInst__FieldBuilder_t639782778_0_0_0_Types };
extern const Il2CppType ResourceInfo_t4013605874_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t4013605874_0_0_0_Types[] = { &ResourceInfo_t4013605874_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t4013605874_0_0_0 = { 1, GenInst_ResourceInfo_t4013605874_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t2113902833_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t2113902833_0_0_0_Types[] = { &ResourceCacheItem_t2113902833_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t2113902833_0_0_0 = { 1, GenInst_ResourceCacheItem_t2113902833_0_0_0_Types };
extern const Il2CppType IContextAttribute_t3913746816_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t3913746816_0_0_0_Types[] = { &IContextAttribute_t3913746816_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t3913746816_0_0_0 = { 1, GenInst_IContextAttribute_t3913746816_0_0_0_Types };
extern const Il2CppType IContextProperty_t82913453_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t82913453_0_0_0_Types[] = { &IContextProperty_t82913453_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t82913453_0_0_0 = { 1, GenInst_IContextProperty_t82913453_0_0_0_Types };
extern const Il2CppType Header_t1689611527_0_0_0;
static const Il2CppType* GenInst_Header_t1689611527_0_0_0_Types[] = { &Header_t1689611527_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1689611527_0_0_0 = { 1, GenInst_Header_t1689611527_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2228500544_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2228500544_0_0_0_Types[] = { &ITrackingHandler_t2228500544_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2228500544_0_0_0 = { 1, GenInst_ITrackingHandler_t2228500544_0_0_0_Types };
extern const Il2CppType IComparable_1_t4193591118_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4193591118_0_0_0_Types[] = { &IComparable_1_t4193591118_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4193591118_0_0_0 = { 1, GenInst_IComparable_1_t4193591118_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3832939894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3832939894_0_0_0_Types[] = { &IEquatable_1_t3832939894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3832939894_0_0_0 = { 1, GenInst_IEquatable_1_t3832939894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1864280422_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1864280422_0_0_0_Types[] = { &IComparable_1_t1864280422_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1864280422_0_0_0 = { 1, GenInst_IComparable_1_t1864280422_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1503629198_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1503629198_0_0_0_Types[] = { &IEquatable_1_t1503629198_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1503629198_0_0_0 = { 1, GenInst_IEquatable_1_t1503629198_0_0_0_Types };
extern const Il2CppType IComparable_1_t323452778_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t323452778_0_0_0_Types[] = { &IComparable_1_t323452778_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t323452778_0_0_0 = { 1, GenInst_IComparable_1_t323452778_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4257768850_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4257768850_0_0_0_Types[] = { &IEquatable_1_t4257768850_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4257768850_0_0_0 = { 1, GenInst_IEquatable_1_t4257768850_0_0_0_Types };
extern const Il2CppType TypeTag_t2420703430_0_0_0;
static const Il2CppType* GenInst_TypeTag_t2420703430_0_0_0_Types[] = { &TypeTag_t2420703430_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t2420703430_0_0_0 = { 1, GenInst_TypeTag_t2420703430_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType KeyContainerPermissionAccessEntry_t2372748443_0_0_0;
static const Il2CppType* GenInst_KeyContainerPermissionAccessEntry_t2372748443_0_0_0_Types[] = { &KeyContainerPermissionAccessEntry_t2372748443_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyContainerPermissionAccessEntry_t2372748443_0_0_0 = { 1, GenInst_KeyContainerPermissionAccessEntry_t2372748443_0_0_0_Types };
extern const Il2CppType StrongName_t2878058698_0_0_0;
static const Il2CppType* GenInst_StrongName_t2878058698_0_0_0_Types[] = { &StrongName_t2878058698_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2878058698_0_0_0 = { 1, GenInst_StrongName_t2878058698_0_0_0_Types };
extern const Il2CppType CodeConnectAccess_t2328951823_0_0_0;
static const Il2CppType* GenInst_CodeConnectAccess_t2328951823_0_0_0_Types[] = { &CodeConnectAccess_t2328951823_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeConnectAccess_t2328951823_0_0_0 = { 1, GenInst_CodeConnectAccess_t2328951823_0_0_0_Types };
extern const Il2CppType EncodingInfo_t1898473639_0_0_0;
static const Il2CppType* GenInst_EncodingInfo_t1898473639_0_0_0_Types[] = { &EncodingInfo_t1898473639_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodingInfo_t1898473639_0_0_0 = { 1, GenInst_EncodingInfo_t1898473639_0_0_0_Types };
extern const Il2CppType WaitHandle_t1661568373_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t1661568373_0_0_0_Types[] = { &WaitHandle_t1661568373_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t1661568373_0_0_0 = { 1, GenInst_WaitHandle_t1661568373_0_0_0_Types };
extern const Il2CppType IDisposable_t1423340799_0_0_0;
static const Il2CppType* GenInst_IDisposable_t1423340799_0_0_0_Types[] = { &IDisposable_t1423340799_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t1423340799_0_0_0 = { 1, GenInst_IDisposable_t1423340799_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1944668977_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0 = { 1, GenInst_KeyValuePair_2_t1944668977_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
extern const Il2CppType XsdIdentityPath_t691771506_0_0_0;
static const Il2CppType* GenInst_XsdIdentityPath_t691771506_0_0_0_Types[] = { &XsdIdentityPath_t691771506_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityPath_t691771506_0_0_0 = { 1, GenInst_XsdIdentityPath_t691771506_0_0_0_Types };
extern const Il2CppType XsdIdentityField_t471607183_0_0_0;
static const Il2CppType* GenInst_XsdIdentityField_t471607183_0_0_0_Types[] = { &XsdIdentityField_t471607183_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityField_t471607183_0_0_0 = { 1, GenInst_XsdIdentityField_t471607183_0_0_0_Types };
extern const Il2CppType XsdIdentityStep_t691878681_0_0_0;
static const Il2CppType* GenInst_XsdIdentityStep_t691878681_0_0_0_Types[] = { &XsdIdentityStep_t691878681_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityStep_t691878681_0_0_0 = { 1, GenInst_XsdIdentityStep_t691878681_0_0_0_Types };
extern const Il2CppType XmlSchemaAttribute_t2904598248_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAttribute_t2904598248_0_0_0_Types[] = { &XmlSchemaAttribute_t2904598248_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t2904598248_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t2904598248_0_0_0_Types };
extern const Il2CppType XmlSchemaAnnotated_t4226823396_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAnnotated_t4226823396_0_0_0_Types[] = { &XmlSchemaAnnotated_t4226823396_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t4226823396_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t4226823396_0_0_0_Types };
extern const Il2CppType XmlSchemaObject_t3280570797_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObject_t3280570797_0_0_0_Types[] = { &XmlSchemaObject_t3280570797_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t3280570797_0_0_0 = { 1, GenInst_XmlSchemaObject_t3280570797_0_0_0_Types };
extern const Il2CppType XmlSchemaException_t78171291_0_0_0;
static const Il2CppType* GenInst_XmlSchemaException_t78171291_0_0_0_Types[] = { &XmlSchemaException_t78171291_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaException_t78171291_0_0_0 = { 1, GenInst_XmlSchemaException_t78171291_0_0_0_Types };
extern const Il2CppType SystemException_t4206535862_0_0_0;
static const Il2CppType* GenInst_SystemException_t4206535862_0_0_0_Types[] = { &SystemException_t4206535862_0_0_0 };
extern const Il2CppGenericInst GenInst_SystemException_t4206535862_0_0_0 = { 1, GenInst_SystemException_t4206535862_0_0_0_Types };
extern const Il2CppType Exception_t3991598821_0_0_0;
static const Il2CppType* GenInst_Exception_t3991598821_0_0_0_Types[] = { &Exception_t3991598821_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t3991598821_0_0_0 = { 1, GenInst_Exception_t3991598821_0_0_0_Types };
extern const Il2CppType _Exception_t426620218_0_0_0;
static const Il2CppType* GenInst__Exception_t426620218_0_0_0_Types[] = { &_Exception_t426620218_0_0_0 };
extern const Il2CppGenericInst GenInst__Exception_t426620218_0_0_0 = { 1, GenInst__Exception_t426620218_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2758969756_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2758969756_0_0_0_Types[] = { &KeyValuePair_2_t2758969756_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2758969756_0_0_0 = { 1, GenInst_KeyValuePair_2_t2758969756_0_0_0_Types };
extern const Il2CppType DTDNode_t2039770680_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t2039770680_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t2039770680_0_0_0_Types[] = { &DTDNode_t2039770680_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t2039770680_0_0_0 = { 1, GenInst_DTDNode_t2039770680_0_0_0_Types };
extern const Il2CppType AttributeSlot_t2157829939_0_0_0;
static const Il2CppType* GenInst_AttributeSlot_t2157829939_0_0_0_Types[] = { &AttributeSlot_t2157829939_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeSlot_t2157829939_0_0_0 = { 1, GenInst_AttributeSlot_t2157829939_0_0_0_Types };
extern const Il2CppType Entry_t2866414864_0_0_0;
static const Il2CppType* GenInst_Entry_t2866414864_0_0_0_Types[] = { &Entry_t2866414864_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2866414864_0_0_0 = { 1, GenInst_Entry_t2866414864_0_0_0_Types };
extern const Il2CppType XmlNode_t856910923_0_0_0;
static const Il2CppType* GenInst_XmlNode_t856910923_0_0_0_Types[] = { &XmlNode_t856910923_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t856910923_0_0_0 = { 1, GenInst_XmlNode_t856910923_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t153087709_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t153087709_0_0_0_Types[] = { &IXPathNavigable_t153087709_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t153087709_0_0_0 = { 1, GenInst_IXPathNavigable_t153087709_0_0_0_Types };
extern const Il2CppType NsDecl_t3658211563_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3658211563_0_0_0_Types[] = { &NsDecl_t3658211563_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3658211563_0_0_0 = { 1, GenInst_NsDecl_t3658211563_0_0_0_Types };
extern const Il2CppType NsScope_t1749213747_0_0_0;
static const Il2CppType* GenInst_NsScope_t1749213747_0_0_0_Types[] = { &NsScope_t1749213747_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t1749213747_0_0_0 = { 1, GenInst_NsScope_t1749213747_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t982414386_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t982414386_0_0_0_Types[] = { &XmlAttributeTokenInfo_t982414386_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t982414386_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t982414386_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t597808448_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t597808448_0_0_0_Types[] = { &XmlTokenInfo_t597808448_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t597808448_0_0_0 = { 1, GenInst_XmlTokenInfo_t597808448_0_0_0_Types };
extern const Il2CppType TagName_t2016006645_0_0_0;
static const Il2CppType* GenInst_TagName_t2016006645_0_0_0_Types[] = { &TagName_t2016006645_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2016006645_0_0_0 = { 1, GenInst_TagName_t2016006645_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t1808742809_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t1808742809_0_0_0_Types[] = { &XmlNodeInfo_t1808742809_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t1808742809_0_0_0 = { 1, GenInst_XmlNodeInfo_t1808742809_0_0_0_Types };
extern const Il2CppType XmlAttribute_t6647939_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t6647939_0_0_0_Types[] = { &XmlAttribute_t6647939_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t6647939_0_0_0 = { 1, GenInst_XmlAttribute_t6647939_0_0_0_Types };
extern const Il2CppType IHasXmlChildNode_t954813494_0_0_0;
static const Il2CppType* GenInst_IHasXmlChildNode_t954813494_0_0_0_Types[] = { &IHasXmlChildNode_t954813494_0_0_0 };
extern const Il2CppGenericInst GenInst_IHasXmlChildNode_t954813494_0_0_0 = { 1, GenInst_IHasXmlChildNode_t954813494_0_0_0_Types };
extern const Il2CppType XmlQualifiedName_t2133315502_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t2133315502_0_0_0_Types[] = { &XmlQualifiedName_t2133315502_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t2133315502_0_0_0 = { 1, GenInst_XmlQualifiedName_t2133315502_0_0_0_Types };
extern const Il2CppType Regex_t2161232213_0_0_0;
static const Il2CppType* GenInst_Regex_t2161232213_0_0_0_Types[] = { &Regex_t2161232213_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t2161232213_0_0_0 = { 1, GenInst_Regex_t2161232213_0_0_0_Types };
extern const Il2CppType XmlSchemaSimpleType_t3060492794_0_0_0;
static const Il2CppType* GenInst_XmlSchemaSimpleType_t3060492794_0_0_0_Types[] = { &XmlSchemaSimpleType_t3060492794_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t3060492794_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t3060492794_0_0_0_Types };
extern const Il2CppType XmlSchemaType_t4090188264_0_0_0;
static const Il2CppType* GenInst_XmlSchemaType_t4090188264_0_0_0_Types[] = { &XmlSchemaType_t4090188264_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t4090188264_0_0_0 = { 1, GenInst_XmlSchemaType_t4090188264_0_0_0_Types };
extern const Il2CppType BigInteger_t3334373499_0_0_0;
static const Il2CppType* GenInst_BigInteger_t3334373499_0_0_0_Types[] = { &BigInteger_t3334373499_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t3334373499_0_0_0 = { 1, GenInst_BigInteger_t3334373499_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t4260760469_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t4260760469_0_0_0_Types[] = { &ByteU5BU5D_t4260760469_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4260760469_0_0_0 = { 1, GenInst_ByteU5BU5D_t4260760469_0_0_0_Types };
extern const Il2CppType X509Certificate_t3076817455_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t3076817455_0_0_0_Types[] = { &X509Certificate_t3076817455_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t3076817455_0_0_0 = { 1, GenInst_X509Certificate_t3076817455_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t675596727_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t675596727_0_0_0_Types[] = { &IDeserializationCallback_t675596727_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t675596727_0_0_0 = { 1, GenInst_IDeserializationCallback_t675596727_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t3167042548_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t3167042548_0_0_0_Types[] = { &ClientCertificateType_t3167042548_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t3167042548_0_0_0 = { 1, GenInst_ClientCertificateType_t3167042548_0_0_0_Types };
extern const Il2CppType ConfigurationProperty_t3009015393_0_0_0;
static const Il2CppType* GenInst_ConfigurationProperty_t3009015393_0_0_0_Types[] = { &ConfigurationProperty_t3009015393_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationProperty_t3009015393_0_0_0 = { 1, GenInst_ConfigurationProperty_t3009015393_0_0_0_Types };
extern const Il2CppType Attribute_t2523058482_0_0_0;
static const Il2CppType* GenInst_Attribute_t2523058482_0_0_0_Types[] = { &Attribute_t2523058482_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t2523058482_0_0_0 = { 1, GenInst_Attribute_t2523058482_0_0_0_Types };
extern const Il2CppType _Attribute_t3253047175_0_0_0;
static const Il2CppType* GenInst__Attribute_t3253047175_0_0_0_Types[] = { &_Attribute_t3253047175_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t3253047175_0_0_0 = { 1, GenInst__Attribute_t3253047175_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t2073374448_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t2073374448_0_0_0_Types[] = { &PropertyDescriptor_t2073374448_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t2073374448_0_0_0 = { 1, GenInst_PropertyDescriptor_t2073374448_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t2617136693_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t2617136693_0_0_0_Types[] = { &MemberDescriptor_t2617136693_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t2617136693_0_0_0 = { 1, GenInst_MemberDescriptor_t2617136693_0_0_0_Types };
extern const Il2CppType AttributeU5BU5D_t4055800263_0_0_0;
static const Il2CppType* GenInst_AttributeU5BU5D_t4055800263_0_0_0_Types[] = { &AttributeU5BU5D_t4055800263_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeU5BU5D_t4055800263_0_0_0 = { 1, GenInst_AttributeU5BU5D_t4055800263_0_0_0_Types };
extern const Il2CppType LinkedList_1_t1417720186_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t1417720186_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t3543085017_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t3543085017_0_0_0_Types[] = { &TypeDescriptionProvider_t3543085017_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t3543085017_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t3543085017_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t1417720186_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1421923377_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1421923377_0_0_0_Types[] = { &KeyValuePair_2_t1421923377_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1421923377_0_0_0 = { 1, GenInst_KeyValuePair_2_t1421923377_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t1518976226_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_Types[] = { &WeakObjectWrapper_t1518976226_0_0_0, &LinkedList_1_t1417720186_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0 = { 2, GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t1518976226_0_0_0_Types[] = { &WeakObjectWrapper_t1518976226_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t1518976226_0_0_0 = { 1, GenInst_WeakObjectWrapper_t1518976226_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &WeakObjectWrapper_t1518976226_0_0_0, &LinkedList_1_t1417720186_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472936237_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472936237_0_0_0_Types[] = { &KeyValuePair_2_t1472936237_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472936237_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472936237_0_0_0_Types };
extern const Il2CppType Cookie_t2033273982_0_0_0;
static const Il2CppType* GenInst_Cookie_t2033273982_0_0_0_Types[] = { &Cookie_t2033273982_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t2033273982_0_0_0 = { 1, GenInst_Cookie_t2033273982_0_0_0_Types };
extern const Il2CppType IPAddress_t3525271463_0_0_0;
static const Il2CppType* GenInst_IPAddress_t3525271463_0_0_0_Types[] = { &IPAddress_t3525271463_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t3525271463_0_0_0 = { 1, GenInst_IPAddress_t3525271463_0_0_0_Types };
extern const Il2CppType NetworkInterface_t3597375525_0_0_0;
static const Il2CppType* GenInst_NetworkInterface_t3597375525_0_0_0_Types[] = { &NetworkInterface_t3597375525_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkInterface_t3597375525_0_0_0 = { 1, GenInst_NetworkInterface_t3597375525_0_0_0_Types };
extern const Il2CppType LinuxNetworkInterface_t908270265_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0_Types[] = { &String_t_0_0_0, &LinuxNetworkInterface_t908270265_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0 = { 2, GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &LinuxNetworkInterface_t908270265_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1627469341_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1627469341_0_0_0_Types[] = { &KeyValuePair_2_t1627469341_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1627469341_0_0_0 = { 1, GenInst_KeyValuePair_2_t1627469341_0_0_0_Types };
static const Il2CppType* GenInst_LinuxNetworkInterface_t908270265_0_0_0_Types[] = { &LinuxNetworkInterface_t908270265_0_0_0 };
extern const Il2CppGenericInst GenInst_LinuxNetworkInterface_t908270265_0_0_0 = { 1, GenInst_LinuxNetworkInterface_t908270265_0_0_0_Types };
extern const Il2CppType MacOsNetworkInterface_t1851933528_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0_Types[] = { &String_t_0_0_0, &MacOsNetworkInterface_t1851933528_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0 = { 2, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &MacOsNetworkInterface_t1851933528_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2571132604_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2571132604_0_0_0_Types[] = { &KeyValuePair_2_t2571132604_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2571132604_0_0_0 = { 1, GenInst_KeyValuePair_2_t2571132604_0_0_0_Types };
static const Il2CppType* GenInst_MacOsNetworkInterface_t1851933528_0_0_0_Types[] = { &MacOsNetworkInterface_t1851933528_0_0_0 };
extern const Il2CppGenericInst GenInst_MacOsNetworkInterface_t1851933528_0_0_0 = { 1, GenInst_MacOsNetworkInterface_t1851933528_0_0_0_Types };
extern const Il2CppType Win32_IP_ADAPTER_ADDRESSES_t3597816152_0_0_0;
static const Il2CppType* GenInst_Win32_IP_ADAPTER_ADDRESSES_t3597816152_0_0_0_Types[] = { &Win32_IP_ADAPTER_ADDRESSES_t3597816152_0_0_0 };
extern const Il2CppGenericInst GenInst_Win32_IP_ADAPTER_ADDRESSES_t3597816152_0_0_0 = { 1, GenInst_Win32_IP_ADAPTER_ADDRESSES_t3597816152_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t766901931_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t766901931_0_0_0_Types[] = { &X509ChainStatus_t766901931_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t766901931_0_0_0 = { 1, GenInst_X509ChainStatus_t766901931_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2188033608_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2188033608_0_0_0_Types[] = { &ArraySegment_1_t2188033608_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2188033608_0_0_0 = { 1, GenInst_ArraySegment_1_t2188033608_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2545618620_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0 = { 1, GenInst_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t476798718_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1195997794_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1195997794_0_0_0_Types[] = { &KeyValuePair_2_t1195997794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1195997794_0_0_0 = { 1, GenInst_KeyValuePair_2_t1195997794_0_0_0_Types };
extern const Il2CppType Capture_t754001812_0_0_0;
static const Il2CppType* GenInst_Capture_t754001812_0_0_0_Types[] = { &Capture_t754001812_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t754001812_0_0_0 = { 1, GenInst_Capture_t754001812_0_0_0_Types };
extern const Il2CppType DynamicMethod_t2315379190_0_0_0;
static const Il2CppType* GenInst_DynamicMethod_t2315379190_0_0_0_Types[] = { &DynamicMethod_t2315379190_0_0_0 };
extern const Il2CppGenericInst GenInst_DynamicMethod_t2315379190_0_0_0 = { 1, GenInst_DynamicMethod_t2315379190_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1049882445_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1049882445_0_0_0_Types[] = { &KeyValuePair_2_t1049882445_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1049882445_0_0_0 = { 1, GenInst_KeyValuePair_2_t1049882445_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &KeyValuePair_2_t1049882445_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types };
extern const Il2CppType Label_t2268465130_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Label_t2268465130_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2164509075_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2164509075_0_0_0_Types[] = { &KeyValuePair_2_t2164509075_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2164509075_0_0_0 = { 1, GenInst_KeyValuePair_2_t2164509075_0_0_0_Types };
static const Il2CppType* GenInst_Label_t2268465130_0_0_0_Types[] = { &Label_t2268465130_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t2268465130_0_0_0 = { 1, GenInst_Label_t2268465130_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Label_t2268465130_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Label_t2268465130_0_0_0, &Label_t2268465130_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Label_t2268465130_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_KeyValuePair_2_t2164509075_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Label_t2268465130_0_0_0, &KeyValuePair_2_t2164509075_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_KeyValuePair_2_t2164509075_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_KeyValuePair_2_t2164509075_0_0_0_Types };
extern const Il2CppType Group_t2151468941_0_0_0;
static const Il2CppType* GenInst_Group_t2151468941_0_0_0_Types[] = { &Group_t2151468941_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t2151468941_0_0_0 = { 1, GenInst_Group_t2151468941_0_0_0_Types };
extern const Il2CppType Mark_t3811539797_0_0_0;
static const Il2CppType* GenInst_Mark_t3811539797_0_0_0_Types[] = { &Mark_t3811539797_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3811539797_0_0_0 = { 1, GenInst_Mark_t3811539797_0_0_0_Types };
extern const Il2CppType UriScheme_t1290668975_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1290668975_0_0_0_Types[] = { &UriScheme_t1290668975_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1290668975_0_0_0 = { 1, GenInst_UriScheme_t1290668975_0_0_0_Types };
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Single_t4291918972_0_0_0, &Single_t4291918972_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0 = { 3, GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types };
extern const Il2CppType Link_t2122599155_0_0_0;
static const Il2CppType* GenInst_Link_t2122599155_0_0_0_Types[] = { &Link_t2122599155_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2122599155_0_0_0 = { 1, GenInst_Link_t2122599155_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3176762032_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0_Types[] = { &Il2CppObject_0_0_0, &IEnumerable_1_t3176762032_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 5, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Object_t3071478659_0_0_0;
static const Il2CppType* GenInst_Object_t3071478659_0_0_0_Types[] = { &Object_t3071478659_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t3071478659_0_0_0 = { 1, GenInst_Object_t3071478659_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4128901257_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4128901257_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t655461400_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t655461400_0_0_0_Types[] = { &IAchievementDescription_t655461400_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t655461400_0_0_0 = { 1, GenInst_IAchievementDescription_t655461400_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1953253797_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1953253797_0_0_0_Types[] = { &IAchievementU5BU5D_t1953253797_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1953253797_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1953253797_0_0_0_Types };
extern const Il2CppType IAchievement_t2957812780_0_0_0;
static const Il2CppType* GenInst_IAchievement_t2957812780_0_0_0_Types[] = { &IAchievement_t2957812780_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t2957812780_0_0_0 = { 1, GenInst_IAchievement_t2957812780_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t250104726_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t250104726_0_0_0_Types[] = { &IScoreU5BU5D_t250104726_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t250104726_0_0_0 = { 1, GenInst_IScoreU5BU5D_t250104726_0_0_0_Types };
extern const Il2CppType IScore_t4279057999_0_0_0;
static const Il2CppType* GenInst_IScore_t4279057999_0_0_0_Types[] = { &IScore_t4279057999_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t4279057999_0_0_0 = { 1, GenInst_IScore_t4279057999_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3419104218_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3419104218_0_0_0_Types[] = { &IUserProfileU5BU5D_t3419104218_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3419104218_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3419104218_0_0_0_Types };
extern const Il2CppType IUserProfile_t598900827_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t598900827_0_0_0_Types[] = { &IUserProfile_t598900827_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t598900827_0_0_0 = { 1, GenInst_IUserProfile_t598900827_0_0_0_Types };
extern const Il2CppType AchievementDescription_t2116066607_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t2116066607_0_0_0_Types[] = { &AchievementDescription_t2116066607_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t2116066607_0_0_0 = { 1, GenInst_AchievementDescription_t2116066607_0_0_0_Types };
extern const Il2CppType UserProfile_t2280656072_0_0_0;
static const Il2CppType* GenInst_UserProfile_t2280656072_0_0_0_Types[] = { &UserProfile_t2280656072_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t2280656072_0_0_0 = { 1, GenInst_UserProfile_t2280656072_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t1820874799_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t1820874799_0_0_0_Types[] = { &GcLeaderboard_t1820874799_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t1820874799_0_0_0 = { 1, GenInst_GcLeaderboard_t1820874799_0_0_0_Types };
extern const Il2CppType GcAchievementData_t3481375915_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t3481375915_0_0_0_Types[] = { &GcAchievementData_t3481375915_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t3481375915_0_0_0 = { 1, GenInst_GcAchievementData_t3481375915_0_0_0_Types };
extern const Il2CppType Achievement_t344600729_0_0_0;
static const Il2CppType* GenInst_Achievement_t344600729_0_0_0_Types[] = { &Achievement_t344600729_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t344600729_0_0_0 = { 1, GenInst_Achievement_t344600729_0_0_0_Types };
extern const Il2CppType GcScoreData_t2181296590_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t2181296590_0_0_0_Types[] = { &GcScoreData_t2181296590_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t2181296590_0_0_0 = { 1, GenInst_GcScoreData_t2181296590_0_0_0_Types };
extern const Il2CppType Score_t3396031228_0_0_0;
static const Il2CppType* GenInst_Score_t3396031228_0_0_0_Types[] = { &Score_t3396031228_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t3396031228_0_0_0 = { 1, GenInst_Score_t3396031228_0_0_0_Types };
extern const Il2CppType Vector3_t4282066566_0_0_0;
static const Il2CppType* GenInst_Vector3_t4282066566_0_0_0_Types[] = { &Vector3_t4282066566_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t4282066566_0_0_0 = { 1, GenInst_Vector3_t4282066566_0_0_0_Types };
extern const Il2CppType Vector4_t4282066567_0_0_0;
static const Il2CppType* GenInst_Vector4_t4282066567_0_0_0_Types[] = { &Vector4_t4282066567_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t4282066567_0_0_0 = { 1, GenInst_Vector4_t4282066567_0_0_0_Types };
extern const Il2CppType Vector2_t4282066565_0_0_0;
static const Il2CppType* GenInst_Vector2_t4282066565_0_0_0_Types[] = { &Vector2_t4282066565_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t4282066565_0_0_0 = { 1, GenInst_Vector2_t4282066565_0_0_0_Types };
extern const Il2CppType Color32_t598853688_0_0_0;
static const Il2CppType* GenInst_Color32_t598853688_0_0_0_Types[] = { &Color32_t598853688_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t598853688_0_0_0 = { 1, GenInst_Color32_t598853688_0_0_0_Types };
extern const Il2CppType Material_t3870600107_0_0_0;
static const Il2CppType* GenInst_Material_t3870600107_0_0_0_Types[] = { &Material_t3870600107_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t3870600107_0_0_0 = { 1, GenInst_Material_t3870600107_0_0_0_Types };
extern const Il2CppType Color_t4194546905_0_0_0;
static const Il2CppType* GenInst_Color_t4194546905_0_0_0_Types[] = { &Color_t4194546905_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t4194546905_0_0_0 = { 1, GenInst_Color_t4194546905_0_0_0_Types };
extern const Il2CppType Keyframe_t4079056114_0_0_0;
static const Il2CppType* GenInst_Keyframe_t4079056114_0_0_0_Types[] = { &Keyframe_t4079056114_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t4079056114_0_0_0 = { 1, GenInst_Keyframe_t4079056114_0_0_0_Types };
extern const Il2CppType NetworkPlayer_t3231273765_0_0_0;
static const Il2CppType* GenInst_NetworkPlayer_t3231273765_0_0_0_Types[] = { &NetworkPlayer_t3231273765_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkPlayer_t3231273765_0_0_0 = { 1, GenInst_NetworkPlayer_t3231273765_0_0_0_Types };
extern const Il2CppType HostData_t3270478838_0_0_0;
static const Il2CppType* GenInst_HostData_t3270478838_0_0_0_Types[] = { &HostData_t3270478838_0_0_0 };
extern const Il2CppGenericInst GenInst_HostData_t3270478838_0_0_0 = { 1, GenInst_HostData_t3270478838_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t726430633_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t726430633_0_0_0_Types[] = { &KeyValuePair_2_t726430633_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t726430633_0_0_0 = { 1, GenInst_KeyValuePair_2_t726430633_0_0_0_Types };
extern const Il2CppType Camera_t2727095145_0_0_0;
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_Types[] = { &Camera_t2727095145_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0 = { 1, GenInst_Camera_t2727095145_0_0_0_Types };
extern const Il2CppType Behaviour_t200106419_0_0_0;
static const Il2CppType* GenInst_Behaviour_t200106419_0_0_0_Types[] = { &Behaviour_t200106419_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t200106419_0_0_0 = { 1, GenInst_Behaviour_t200106419_0_0_0_Types };
extern const Il2CppType Component_t3501516275_0_0_0;
static const Il2CppType* GenInst_Component_t3501516275_0_0_0_Types[] = { &Component_t3501516275_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3501516275_0_0_0 = { 1, GenInst_Component_t3501516275_0_0_0_Types };
extern const Il2CppType Display_t1321072632_0_0_0;
static const Il2CppType* GenInst_Display_t1321072632_0_0_0_Types[] = { &Display_t1321072632_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t1321072632_0_0_0 = { 1, GenInst_Display_t1321072632_0_0_0_Types };
extern const Il2CppType Touch_t4210255029_0_0_0;
static const Il2CppType* GenInst_Touch_t4210255029_0_0_0_Types[] = { &Touch_t4210255029_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t4210255029_0_0_0 = { 1, GenInst_Touch_t4210255029_0_0_0_Types };
extern const Il2CppType GameObject_t3674682005_0_0_0;
static const Il2CppType* GenInst_GameObject_t3674682005_0_0_0_Types[] = { &GameObject_t3674682005_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t3674682005_0_0_0 = { 1, GenInst_GameObject_t3674682005_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_t2362096582_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_t2362096582_0_0_0_Types[] = { &AndroidJavaObject_t2362096582_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_t2362096582_0_0_0 = { 1, GenInst_AndroidJavaObject_t2362096582_0_0_0_Types };
extern const Il2CppType jvalue_t3862675307_0_0_0;
static const Il2CppType* GenInst_jvalue_t3862675307_0_0_0_Types[] = { &jvalue_t3862675307_0_0_0 };
extern const Il2CppGenericInst GenInst_jvalue_t3862675307_0_0_0 = { 1, GenInst_jvalue_t3862675307_0_0_0_Types };
extern const Il2CppType Playable_t70832698_0_0_0;
static const Il2CppType* GenInst_Playable_t70832698_0_0_0_Types[] = { &Playable_t70832698_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t70832698_0_0_0 = { 1, GenInst_Playable_t70832698_0_0_0_Types };
extern const Il2CppType ParticleCollisionEvent_t2700926194_0_0_0;
static const Il2CppType* GenInst_ParticleCollisionEvent_t2700926194_0_0_0_Types[] = { &ParticleCollisionEvent_t2700926194_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleCollisionEvent_t2700926194_0_0_0 = { 1, GenInst_ParticleCollisionEvent_t2700926194_0_0_0_Types };
extern const Il2CppType ContactPoint_t243083348_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t243083348_0_0_0_Types[] = { &ContactPoint_t243083348_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t243083348_0_0_0 = { 1, GenInst_ContactPoint_t243083348_0_0_0_Types };
extern const Il2CppType RaycastHit_t4003175726_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t4003175726_0_0_0_Types[] = { &RaycastHit_t4003175726_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t4003175726_0_0_0 = { 1, GenInst_RaycastHit_t4003175726_0_0_0_Types };
extern const Il2CppType Collider_t2939674232_0_0_0;
static const Il2CppType* GenInst_Collider_t2939674232_0_0_0_Types[] = { &Collider_t2939674232_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t2939674232_0_0_0 = { 1, GenInst_Collider_t2939674232_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t1743771669_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t1743771669_0_0_0_Types[] = { &Rigidbody2D_t1743771669_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1743771669_0_0_0 = { 1, GenInst_Rigidbody2D_t1743771669_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t1374744384_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t1374744384_0_0_0_Types[] = { &RaycastHit2D_t1374744384_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t1374744384_0_0_0 = { 1, GenInst_RaycastHit2D_t1374744384_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t4288432358_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t4288432358_0_0_0_Types[] = { &ContactPoint2D_t4288432358_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t4288432358_0_0_0 = { 1, GenInst_ContactPoint2D_t4288432358_0_0_0_Types };
extern const Il2CppType WebCamDevice_t3274004757_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t3274004757_0_0_0_Types[] = { &WebCamDevice_t3274004757_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t3274004757_0_0_0 = { 1, GenInst_WebCamDevice_t3274004757_0_0_0_Types };
extern const Il2CppType UIVertex_t4244065212_0_0_0;
static const Il2CppType* GenInst_UIVertex_t4244065212_0_0_0_Types[] = { &UIVertex_t4244065212_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4244065212_0_0_0 = { 1, GenInst_UIVertex_t4244065212_0_0_0_Types };
extern const Il2CppType UICharInfo_t65807484_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t65807484_0_0_0_Types[] = { &UICharInfo_t65807484_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t65807484_0_0_0 = { 1, GenInst_UICharInfo_t65807484_0_0_0_Types };
extern const Il2CppType UILineInfo_t4113875482_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t4113875482_0_0_0_Types[] = { &UILineInfo_t4113875482_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4113875482_0_0_0 = { 1, GenInst_UILineInfo_t4113875482_0_0_0_Types };
extern const Il2CppType Font_t4241557075_0_0_0;
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_Types[] = { &Font_t4241557075_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0 = { 1, GenInst_Font_t4241557075_0_0_0_Types };
extern const Il2CppType GUIContent_t2094828418_0_0_0;
static const Il2CppType* GenInst_GUIContent_t2094828418_0_0_0_Types[] = { &GUIContent_t2094828418_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIContent_t2094828418_0_0_0 = { 1, GenInst_GUIContent_t2094828418_0_0_0_Types };
extern const Il2CppType Rect_t4241904616_0_0_0;
static const Il2CppType* GenInst_Rect_t4241904616_0_0_0_Types[] = { &Rect_t4241904616_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t4241904616_0_0_0 = { 1, GenInst_Rect_t4241904616_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t331591504_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t331591504_0_0_0_Types[] = { &GUILayoutOption_t331591504_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t331591504_0_0_0 = { 1, GenInst_GUILayoutOption_t331591504_0_0_0_Types };
extern const Il2CppType LayoutCache_t879908455_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &LayoutCache_t879908455_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4066860316_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0 = { 1, GenInst_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &LayoutCache_t879908455_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t775952400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t775952400_0_0_0_Types[] = { &KeyValuePair_2_t775952400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t775952400_0_0_0 = { 1, GenInst_KeyValuePair_2_t775952400_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t1336615025_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t1336615025_0_0_0_Types[] = { &GUILayoutEntry_t1336615025_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1336615025_0_0_0 = { 1, GenInst_GUILayoutEntry_t1336615025_0_0_0_Types };
extern const Il2CppType GUIStyle_t2990928826_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t2990928826_0_0_0_Types[] = { &GUIStyle_t2990928826_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t2990928826_0_0_0 = { 1, GenInst_GUIStyle_t2990928826_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t2990928826_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t2990928826_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3710127902_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3710127902_0_0_0_Types[] = { &KeyValuePair_2_t3710127902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3710127902_0_0_0 = { 1, GenInst_KeyValuePair_2_t3710127902_0_0_0_Types };
extern const Il2CppType Event_t4196595728_0_0_0;
extern const Il2CppType TextEditOp_t4145961110_0_0_0;
static const Il2CppType* GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &Event_t4196595728_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0 = { 2, GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1919813716_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1919813716_0_0_0_Types[] = { &KeyValuePair_2_t1919813716_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1919813716_0_0_0 = { 1, GenInst_KeyValuePair_2_t1919813716_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t4145961110_0_0_0_Types[] = { &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t4145961110_0_0_0 = { 1, GenInst_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &KeyValuePair_2_t1919813716_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types };
static const Il2CppType* GenInst_Event_t4196595728_0_0_0_Types[] = { &Event_t4196595728_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t4196595728_0_0_0 = { 1, GenInst_Event_t4196595728_0_0_0_Types };
static const Il2CppType* GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Event_t4196595728_0_0_0, &TextEditOp_t4145961110_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1289591971_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1289591971_0_0_0_Types[] = { &KeyValuePair_2_t1289591971_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1289591971_0_0_0 = { 1, GenInst_KeyValuePair_2_t1289591971_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t62111112_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t62111112_0_0_0_Types[] = { &DisallowMultipleComponent_t62111112_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t62111112_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t62111112_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3132250205_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3132250205_0_0_0_Types[] = { &ExecuteInEditMode_t3132250205_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3132250205_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3132250205_0_0_0_Types };
extern const Il2CppType RequireComponent_t1687166108_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t1687166108_0_0_0_Types[] = { &RequireComponent_t1687166108_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t1687166108_0_0_0 = { 1, GenInst_RequireComponent_t1687166108_0_0_0_Types };
extern const Il2CppType HitInfo_t3209134097_0_0_0;
static const Il2CppType* GenInst_HitInfo_t3209134097_0_0_0_Types[] = { &HitInfo_t3209134097_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t3209134097_0_0_0 = { 1, GenInst_HitInfo_t3209134097_0_0_0_Types };
extern const Il2CppType PersistentCall_t2972625667_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t2972625667_0_0_0_Types[] = { &PersistentCall_t2972625667_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t2972625667_0_0_0 = { 1, GenInst_PersistentCall_t2972625667_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t1559630662_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t1559630662_0_0_0_Types[] = { &BaseInvokableCall_t1559630662_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1559630662_0_0_0 = { 1, GenInst_BaseInvokableCall_t1559630662_0_0_0_Types };
extern const Il2CppType PlayMakerFSM_t3799847376_0_0_0;
static const Il2CppType* GenInst_PlayMakerFSM_t3799847376_0_0_0_Types[] = { &PlayMakerFSM_t3799847376_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerFSM_t3799847376_0_0_0 = { 1, GenInst_PlayMakerFSM_t3799847376_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t667441552_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t667441552_0_0_0_Types[] = { &MonoBehaviour_t667441552_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t667441552_0_0_0 = { 1, GenInst_MonoBehaviour_t667441552_0_0_0_Types };
extern const Il2CppType FsmFloat_t2134102846_0_0_0;
static const Il2CppType* GenInst_FsmFloat_t2134102846_0_0_0_Types[] = { &FsmFloat_t2134102846_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmFloat_t2134102846_0_0_0 = { 1, GenInst_FsmFloat_t2134102846_0_0_0_Types };
extern const Il2CppType NamedVariable_t3211770239_0_0_0;
static const Il2CppType* GenInst_NamedVariable_t3211770239_0_0_0_Types[] = { &NamedVariable_t3211770239_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedVariable_t3211770239_0_0_0 = { 1, GenInst_NamedVariable_t3211770239_0_0_0_Types };
extern const Il2CppType INameable_t2730839192_0_0_0;
static const Il2CppType* GenInst_INameable_t2730839192_0_0_0_Types[] = { &INameable_t2730839192_0_0_0 };
extern const Il2CppGenericInst GenInst_INameable_t2730839192_0_0_0 = { 1, GenInst_INameable_t2730839192_0_0_0_Types };
extern const Il2CppType INamedVariable_t1024128046_0_0_0;
static const Il2CppType* GenInst_INamedVariable_t1024128046_0_0_0_Types[] = { &INamedVariable_t1024128046_0_0_0 };
extern const Il2CppGenericInst GenInst_INamedVariable_t1024128046_0_0_0 = { 1, GenInst_INamedVariable_t1024128046_0_0_0_Types };
extern const Il2CppType FsmInt_t1596138449_0_0_0;
static const Il2CppType* GenInst_FsmInt_t1596138449_0_0_0_Types[] = { &FsmInt_t1596138449_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmInt_t1596138449_0_0_0 = { 1, GenInst_FsmInt_t1596138449_0_0_0_Types };
extern const Il2CppType FsmTransition_t3771611999_0_0_0;
static const Il2CppType* GenInst_FsmTransition_t3771611999_0_0_0_Types[] = { &FsmTransition_t3771611999_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmTransition_t3771611999_0_0_0 = { 1, GenInst_FsmTransition_t3771611999_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3320890566_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3320890566_0_0_0_Types[] = { &IEquatable_1_t3320890566_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3320890566_0_0_0 = { 1, GenInst_IEquatable_1_t3320890566_0_0_0_Types };
extern const Il2CppType ActionReport_t662142796_0_0_0;
static const Il2CppType* GenInst_ActionReport_t662142796_0_0_0_Types[] = { &ActionReport_t662142796_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionReport_t662142796_0_0_0 = { 1, GenInst_ActionReport_t662142796_0_0_0_Types };
extern const Il2CppType FsmVarOverride_t3235106805_0_0_0;
static const Il2CppType* GenInst_FsmVarOverride_t3235106805_0_0_0_Types[] = { &FsmVarOverride_t3235106805_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVarOverride_t3235106805_0_0_0 = { 1, GenInst_FsmVarOverride_t3235106805_0_0_0_Types };
extern const Il2CppType Fsm_t1527112426_0_0_0;
static const Il2CppType* GenInst_Fsm_t1527112426_0_0_0_Types[] = { &Fsm_t1527112426_0_0_0 };
extern const Il2CppGenericInst GenInst_Fsm_t1527112426_0_0_0 = { 1, GenInst_Fsm_t1527112426_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3582344850_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3582344850_0_0_0_Types[] = { &KeyValuePair_2_t3582344850_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3582344850_0_0_0 = { 1, GenInst_KeyValuePair_2_t3582344850_0_0_0_Types };
extern const Il2CppType FieldInfoU5BU5D_t2567562023_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_Types[] = { &Type_t_0_0_0, &FieldInfoU5BU5D_t2567562023_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0 = { 2, GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &FieldInfoU5BU5D_t2567562023_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2571765214_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2571765214_0_0_0_Types[] = { &KeyValuePair_2_t2571765214_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2571765214_0_0_0 = { 1, GenInst_KeyValuePair_2_t2571765214_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Type_t_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1158041691_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1158041691_0_0_0_Types[] = { &KeyValuePair_2_t1158041691_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1158041691_0_0_0 = { 1, GenInst_KeyValuePair_2_t1158041691_0_0_0_Types };
extern const Il2CppType FsmGameObject_t1697147867_0_0_0;
static const Il2CppType* GenInst_FsmGameObject_t1697147867_0_0_0_Types[] = { &FsmGameObject_t1697147867_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmGameObject_t1697147867_0_0_0 = { 1, GenInst_FsmGameObject_t1697147867_0_0_0_Types };
extern const Il2CppType FsmOwnerDefault_t251897112_0_0_0;
static const Il2CppType* GenInst_FsmOwnerDefault_t251897112_0_0_0_Types[] = { &FsmOwnerDefault_t251897112_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmOwnerDefault_t251897112_0_0_0 = { 1, GenInst_FsmOwnerDefault_t251897112_0_0_0_Types };
extern const Il2CppType FsmAnimationCurve_t2685995989_0_0_0;
static const Il2CppType* GenInst_FsmAnimationCurve_t2685995989_0_0_0_Types[] = { &FsmAnimationCurve_t2685995989_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmAnimationCurve_t2685995989_0_0_0 = { 1, GenInst_FsmAnimationCurve_t2685995989_0_0_0_Types };
extern const Il2CppType FunctionCall_t3279845016_0_0_0;
static const Il2CppType* GenInst_FunctionCall_t3279845016_0_0_0_Types[] = { &FunctionCall_t3279845016_0_0_0 };
extern const Il2CppGenericInst GenInst_FunctionCall_t3279845016_0_0_0 = { 1, GenInst_FunctionCall_t3279845016_0_0_0_Types };
extern const Il2CppType FsmTemplateControl_t2786508133_0_0_0;
static const Il2CppType* GenInst_FsmTemplateControl_t2786508133_0_0_0_Types[] = { &FsmTemplateControl_t2786508133_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmTemplateControl_t2786508133_0_0_0 = { 1, GenInst_FsmTemplateControl_t2786508133_0_0_0_Types };
extern const Il2CppType FsmEventTarget_t1823904941_0_0_0;
static const Il2CppType* GenInst_FsmEventTarget_t1823904941_0_0_0_Types[] = { &FsmEventTarget_t1823904941_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmEventTarget_t1823904941_0_0_0 = { 1, GenInst_FsmEventTarget_t1823904941_0_0_0_Types };
extern const Il2CppType FsmProperty_t3927159007_0_0_0;
static const Il2CppType* GenInst_FsmProperty_t3927159007_0_0_0_Types[] = { &FsmProperty_t3927159007_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmProperty_t3927159007_0_0_0 = { 1, GenInst_FsmProperty_t3927159007_0_0_0_Types };
extern const Il2CppType LayoutOption_t964995201_0_0_0;
static const Il2CppType* GenInst_LayoutOption_t964995201_0_0_0_Types[] = { &LayoutOption_t964995201_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutOption_t964995201_0_0_0 = { 1, GenInst_LayoutOption_t964995201_0_0_0_Types };
extern const Il2CppType FsmString_t952858651_0_0_0;
static const Il2CppType* GenInst_FsmString_t952858651_0_0_0_Types[] = { &FsmString_t952858651_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmString_t952858651_0_0_0 = { 1, GenInst_FsmString_t952858651_0_0_0_Types };
extern const Il2CppType FsmObject_t821476169_0_0_0;
static const Il2CppType* GenInst_FsmObject_t821476169_0_0_0_Types[] = { &FsmObject_t821476169_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmObject_t821476169_0_0_0 = { 1, GenInst_FsmObject_t821476169_0_0_0_Types };
extern const Il2CppType FsmVar_t1596150537_0_0_0;
static const Il2CppType* GenInst_FsmVar_t1596150537_0_0_0_Types[] = { &FsmVar_t1596150537_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVar_t1596150537_0_0_0 = { 1, GenInst_FsmVar_t1596150537_0_0_0_Types };
extern const Il2CppType ParamDataType_t2672665179_0_0_0;
static const Il2CppType* GenInst_ParamDataType_t2672665179_0_0_0_Types[] = { &ParamDataType_t2672665179_0_0_0 };
extern const Il2CppGenericInst GenInst_ParamDataType_t2672665179_0_0_0 = { 1, GenInst_ParamDataType_t2672665179_0_0_0_Types };
extern const Il2CppType FsmStateAction_t2366529033_0_0_0;
static const Il2CppType* GenInst_FsmStateAction_t2366529033_0_0_0_Types[] = { &FsmStateAction_t2366529033_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmStateAction_t2366529033_0_0_0 = { 1, GenInst_FsmStateAction_t2366529033_0_0_0_Types };
extern const Il2CppType IFsmStateAction_t3269097786_0_0_0;
static const Il2CppType* GenInst_IFsmStateAction_t3269097786_0_0_0_Types[] = { &IFsmStateAction_t3269097786_0_0_0 };
extern const Il2CppGenericInst GenInst_IFsmStateAction_t3269097786_0_0_0 = { 1, GenInst_IFsmStateAction_t3269097786_0_0_0_Types };
extern const Il2CppType DelayedEvent_t1938906778_0_0_0;
static const Il2CppType* GenInst_DelayedEvent_t1938906778_0_0_0_Types[] = { &DelayedEvent_t1938906778_0_0_0 };
extern const Il2CppGenericInst GenInst_DelayedEvent_t1938906778_0_0_0 = { 1, GenInst_DelayedEvent_t1938906778_0_0_0_Types };
extern const Il2CppType FsmState_t2146334067_0_0_0;
static const Il2CppType* GenInst_FsmState_t2146334067_0_0_0_Types[] = { &FsmState_t2146334067_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmState_t2146334067_0_0_0 = { 1, GenInst_FsmState_t2146334067_0_0_0_Types };
extern const Il2CppType FsmEvent_t2133468028_0_0_0;
static const Il2CppType* GenInst_FsmEvent_t2133468028_0_0_0_Types[] = { &FsmEvent_t2133468028_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmEvent_t2133468028_0_0_0 = { 1, GenInst_FsmEvent_t2133468028_0_0_0_Types };
extern const Il2CppType FsmLogEntry_t2614866584_0_0_0;
static const Il2CppType* GenInst_FsmLogEntry_t2614866584_0_0_0_Types[] = { &FsmLogEntry_t2614866584_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmLogEntry_t2614866584_0_0_0 = { 1, GenInst_FsmLogEntry_t2614866584_0_0_0_Types };
extern const Il2CppType FsmLog_t1596141350_0_0_0;
static const Il2CppType* GenInst_FsmLog_t1596141350_0_0_0_Types[] = { &FsmLog_t1596141350_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmLog_t1596141350_0_0_0 = { 1, GenInst_FsmLog_t1596141350_0_0_0_Types };
extern const Il2CppType FsmBool_t1075959796_0_0_0;
static const Il2CppType* GenInst_FsmBool_t1075959796_0_0_0_Types[] = { &FsmBool_t1075959796_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmBool_t1075959796_0_0_0 = { 1, GenInst_FsmBool_t1075959796_0_0_0_Types };
extern const Il2CppType FsmVector2_t533912881_0_0_0;
static const Il2CppType* GenInst_FsmVector2_t533912881_0_0_0_Types[] = { &FsmVector2_t533912881_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVector2_t533912881_0_0_0 = { 1, GenInst_FsmVector2_t533912881_0_0_0_Types };
extern const Il2CppType FsmVector3_t533912882_0_0_0;
static const Il2CppType* GenInst_FsmVector3_t533912882_0_0_0_Types[] = { &FsmVector3_t533912882_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVector3_t533912882_0_0_0 = { 1, GenInst_FsmVector3_t533912882_0_0_0_Types };
extern const Il2CppType FsmRect_t1076426478_0_0_0;
static const Il2CppType* GenInst_FsmRect_t1076426478_0_0_0_Types[] = { &FsmRect_t1076426478_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmRect_t1076426478_0_0_0 = { 1, GenInst_FsmRect_t1076426478_0_0_0_Types };
extern const Il2CppType FsmQuaternion_t3871136040_0_0_0;
static const Il2CppType* GenInst_FsmQuaternion_t3871136040_0_0_0_Types[] = { &FsmQuaternion_t3871136040_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmQuaternion_t3871136040_0_0_0 = { 1, GenInst_FsmQuaternion_t3871136040_0_0_0_Types };
extern const Il2CppType FsmColor_t2131419205_0_0_0;
static const Il2CppType* GenInst_FsmColor_t2131419205_0_0_0_Types[] = { &FsmColor_t2131419205_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmColor_t2131419205_0_0_0 = { 1, GenInst_FsmColor_t2131419205_0_0_0_Types };
extern const Il2CppType FsmMaterial_t924399665_0_0_0;
static const Il2CppType* GenInst_FsmMaterial_t924399665_0_0_0_Types[] = { &FsmMaterial_t924399665_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmMaterial_t924399665_0_0_0 = { 1, GenInst_FsmMaterial_t924399665_0_0_0_Types };
extern const Il2CppType FsmTexture_t3073272573_0_0_0;
static const Il2CppType* GenInst_FsmTexture_t3073272573_0_0_0_Types[] = { &FsmTexture_t3073272573_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmTexture_t3073272573_0_0_0 = { 1, GenInst_FsmTexture_t3073272573_0_0_0_Types };
extern const Il2CppType LuminanceSource_t1231523093_0_0_0;
extern const Il2CppType Binarizer_t1492033400_0_0_0;
static const Il2CppType* GenInst_LuminanceSource_t1231523093_0_0_0_Binarizer_t1492033400_0_0_0_Types[] = { &LuminanceSource_t1231523093_0_0_0, &Binarizer_t1492033400_0_0_0 };
extern const Il2CppGenericInst GenInst_LuminanceSource_t1231523093_0_0_0_Binarizer_t1492033400_0_0_0 = { 2, GenInst_LuminanceSource_t1231523093_0_0_0_Binarizer_t1492033400_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &LuminanceSource_t1231523093_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType BitmapFormat_t3665922245_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t4260760469_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_LuminanceSource_t1231523093_0_0_0_Types[] = { &ByteU5BU5D_t4260760469_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &BitmapFormat_t3665922245_0_0_0, &LuminanceSource_t1231523093_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4260760469_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_LuminanceSource_t1231523093_0_0_0 = { 5, GenInst_ByteU5BU5D_t4260760469_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_LuminanceSource_t1231523093_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &BitmapFormat_t3665922245_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_Il2CppObject_0_0_0 = { 5, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ResultPoint_t1538592853_0_0_0;
static const Il2CppType* GenInst_ResultPoint_t1538592853_0_0_0_Types[] = { &ResultPoint_t1538592853_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultPoint_t1538592853_0_0_0 = { 1, GenInst_ResultPoint_t1538592853_0_0_0_Types };
extern const Il2CppType Result_t2610723219_0_0_0;
static const Il2CppType* GenInst_Result_t2610723219_0_0_0_Types[] = { &Result_t2610723219_0_0_0 };
extern const Il2CppGenericInst GenInst_Result_t2610723219_0_0_0 = { 1, GenInst_Result_t2610723219_0_0_0_Types };
extern const Il2CppType EventArgs_t2540831021_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_EventArgs_t2540831021_0_0_0_Types[] = { &Il2CppObject_0_0_0, &EventArgs_t2540831021_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_EventArgs_t2540831021_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_EventArgs_t2540831021_0_0_0_Types };
extern const Il2CppType DecodeHintType_t2095781349_0_0_0;
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1080897207_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1080897207_0_0_0_Types[] = { &KeyValuePair_2_t1080897207_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1080897207_0_0_0 = { 1, GenInst_KeyValuePair_2_t1080897207_0_0_0_Types };
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0 = { 1, GenInst_DecodeHintType_t2095781349_0_0_0_Types };
extern const Il2CppType ResultMetadataType_t2923366972_0_0_0;
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3033809700_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3033809700_0_0_0_Types[] = { &KeyValuePair_2_t3033809700_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3033809700_0_0_0 = { 1, GenInst_KeyValuePair_2_t3033809700_0_0_0_Types };
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0 = { 1, GenInst_ResultMetadataType_t2923366972_0_0_0_Types };
extern const Il2CppType Color32U5BU5D_t2960766953_0_0_0;
static const Il2CppType* GenInst_Color32U5BU5D_t2960766953_0_0_0_Types[] = { &Color32U5BU5D_t2960766953_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32U5BU5D_t2960766953_0_0_0 = { 1, GenInst_Color32U5BU5D_t2960766953_0_0_0_Types };
static const Il2CppType* GenInst_Color32U5BU5D_t2960766953_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0_Types[] = { &Color32U5BU5D_t2960766953_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &LuminanceSource_t1231523093_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32U5BU5D_t2960766953_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0 = { 4, GenInst_Color32U5BU5D_t2960766953_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0_Types };
extern const Il2CppType BarcodeFormat_t4201805817_0_0_0;
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0 = { 1, GenInst_BarcodeFormat_t4201805817_0_0_0_Types };
extern const Il2CppType Reader_t2610170425_0_0_0;
static const Il2CppType* GenInst_Reader_t2610170425_0_0_0_Types[] = { &Reader_t2610170425_0_0_0 };
extern const Il2CppGenericInst GenInst_Reader_t2610170425_0_0_0 = { 1, GenInst_Reader_t2610170425_0_0_0_Types };
extern const Il2CppType EncodeHintType_t3244562957_0_0_0;
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3617918191_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3617918191_0_0_0_Types[] = { &KeyValuePair_2_t3617918191_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3617918191_0_0_0 = { 1, GenInst_KeyValuePair_2_t3617918191_0_0_0_Types };
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0 = { 1, GenInst_EncodeHintType_t3244562957_0_0_0_Types };
extern const Il2CppType Func_1_t3890737231_0_0_0;
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Func_1_t3890737231_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0 = { 2, GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0_Types };
extern const Il2CppType Writer_t2765575657_0_0_0;
static const Il2CppType* GenInst_Writer_t2765575657_0_0_0_Types[] = { &Writer_t2765575657_0_0_0 };
extern const Il2CppGenericInst GenInst_Writer_t2765575657_0_0_0 = { 1, GenInst_Writer_t2765575657_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1188478419_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1188478419_0_0_0_Types[] = { &KeyValuePair_2_t1188478419_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1188478419_0_0_0 = { 1, GenInst_KeyValuePair_2_t1188478419_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_BarcodeFormat_t4201805817_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Il2CppObject_0_0_0, &BarcodeFormat_t4201805817_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_BarcodeFormat_t4201805817_0_0_0 = { 3, GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_BarcodeFormat_t4201805817_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1188478419_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1188478419_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1188478419_0_0_0 = { 3, GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1188478419_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &Func_1_t3890737231_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t908399279_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t908399279_0_0_0_Types[] = { &KeyValuePair_2_t908399279_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t908399279_0_0_0 = { 1, GenInst_KeyValuePair_2_t908399279_0_0_0_Types };
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_ResultMetadataType_t2923366972_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0, &Il2CppObject_0_0_0, &ResultMetadataType_t2923366972_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_ResultMetadataType_t2923366972_0_0_0 = { 3, GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_ResultMetadataType_t2923366972_0_0_0_Types };
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3033809700_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3033809700_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3033809700_0_0_0 = { 3, GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3033809700_0_0_0_Types };
extern const Il2CppType Table_t1007478513_0_0_0;
extern const Il2CppType StringU5BU5D_t4054002952_0_0_0;
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &StringU5BU5D_t4054002952_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0 = { 2, GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2656869371_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2656869371_0_0_0_Types[] = { &KeyValuePair_2_t2656869371_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2656869371_0_0_0 = { 1, GenInst_KeyValuePair_2_t2656869371_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Types[] = { &Table_t1007478513_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0 = { 1, GenInst_Table_t1007478513_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Table_t1007478513_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0 = { 2, GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t996820380_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t996820380_0_0_0_Types[] = { &KeyValuePair_2_t996820380_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t996820380_0_0_0 = { 1, GenInst_KeyValuePair_2_t996820380_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Table_t1007478513_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &Il2CppObject_0_0_0, &Table_t1007478513_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Table_t1007478513_0_0_0 = { 3, GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Table_t1007478513_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2656869371_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2656869371_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2656869371_0_0_0 = { 3, GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2656869371_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &StringU5BU5D_t4054002952_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2540055952_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2540055952_0_0_0_Types[] = { &KeyValuePair_2_t2540055952_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2540055952_0_0_0 = { 1, GenInst_KeyValuePair_2_t2540055952_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Char_t2862622538_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Table_t1007478513_0_0_0, &Char_t2862622538_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Char_t2862622538_0_0_0 = { 3, GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Char_t2862622538_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Table_t1007478513_0_0_0, &Table_t1007478513_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0 = { 3, GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Table_t1007478513_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_KeyValuePair_2_t996820380_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Table_t1007478513_0_0_0, &KeyValuePair_2_t996820380_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_KeyValuePair_2_t996820380_0_0_0 = { 3, GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_KeyValuePair_2_t996820380_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t3230847821_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t3230847821_0_0_0_Types[] = { &Int32U5BU5D_t3230847821_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t3230847821_0_0_0 = { 1, GenInst_Int32U5BU5D_t3230847821_0_0_0_Types };
extern const Il2CppType State_t3065423635_0_0_0;
static const Il2CppType* GenInst_State_t3065423635_0_0_0_Types[] = { &State_t3065423635_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t3065423635_0_0_0 = { 1, GenInst_State_t3065423635_0_0_0_Types };
extern const Il2CppType Token_t3066207355_0_0_0;
static const Il2CppType* GenInst_Token_t3066207355_0_0_0_Types[] = { &Token_t3066207355_0_0_0 };
extern const Il2CppGenericInst GenInst_Token_t3066207355_0_0_0 = { 1, GenInst_Token_t3066207355_0_0_0_Types };
extern const Il2CppType CharacterSetECI_t2542265168_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &CharacterSetECI_t2542265168_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0_Types[] = { &String_t_0_0_0, &CharacterSetECI_t2542265168_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0 = { 2, GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &CharacterSetECI_t2542265168_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2438309113_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2438309113_0_0_0_Types[] = { &KeyValuePair_2_t2438309113_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2438309113_0_0_0 = { 1, GenInst_KeyValuePair_2_t2438309113_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &CharacterSetECI_t2542265168_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3261464244_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3261464244_0_0_0_Types[] = { &KeyValuePair_2_t3261464244_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3261464244_0_0_0 = { 1, GenInst_KeyValuePair_2_t3261464244_0_0_0_Types };
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DecodeHintType_t2095781349_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0, &Il2CppObject_0_0_0, &DecodeHintType_t2095781349_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DecodeHintType_t2095781349_0_0_0 = { 3, GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DecodeHintType_t2095781349_0_0_0_Types };
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1080897207_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1080897207_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1080897207_0_0_0 = { 3, GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1080897207_0_0_0_Types };
extern const Il2CppType Int64U5BU5D_t2174042770_0_0_0;
static const Il2CppType* GenInst_Int64U5BU5D_t2174042770_0_0_0_Types[] = { &Int64U5BU5D_t2174042770_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64U5BU5D_t2174042770_0_0_0 = { 1, GenInst_Int64U5BU5D_t2174042770_0_0_0_Types };
extern const Il2CppType BigInteger_t416298622_0_0_0;
static const Il2CppType* GenInst_BigInteger_t416298622_0_0_0_Types[] = { &BigInteger_t416298622_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t416298622_0_0_0 = { 1, GenInst_BigInteger_t416298622_0_0_0_Types };
extern const Il2CppType GenericGFPoly_t755870220_0_0_0;
static const Il2CppType* GenInst_GenericGFPoly_t755870220_0_0_0_Types[] = { &GenericGFPoly_t755870220_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGFPoly_t755870220_0_0_0 = { 1, GenInst_GenericGFPoly_t755870220_0_0_0_Types };
extern const Il2CppType SymbolShapeHint_t2380532246_0_0_0;
static const Il2CppType* GenInst_SymbolShapeHint_t2380532246_0_0_0_Types[] = { &SymbolShapeHint_t2380532246_0_0_0 };
extern const Il2CppGenericInst GenInst_SymbolShapeHint_t2380532246_0_0_0 = { 1, GenInst_SymbolShapeHint_t2380532246_0_0_0_Types };
extern const Il2CppType DataBlock_t594199773_0_0_0;
static const Il2CppType* GenInst_DataBlock_t594199773_0_0_0_Types[] = { &DataBlock_t594199773_0_0_0 };
extern const Il2CppGenericInst GenInst_DataBlock_t594199773_0_0_0 = { 1, GenInst_DataBlock_t594199773_0_0_0_Types };
extern const Il2CppType ECB_t2948426741_0_0_0;
static const Il2CppType* GenInst_ECB_t2948426741_0_0_0_Types[] = { &ECB_t2948426741_0_0_0 };
extern const Il2CppGenericInst GenInst_ECB_t2948426741_0_0_0 = { 1, GenInst_ECB_t2948426741_0_0_0_Types };
extern const Il2CppType Version_t2313761970_0_0_0;
static const Il2CppType* GenInst_Version_t2313761970_0_0_0_Types[] = { &Version_t2313761970_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t2313761970_0_0_0 = { 1, GenInst_Version_t2313761970_0_0_0_Types };
extern const Il2CppType ResultPointsAndTransitions_t1206419768_0_0_0;
static const Il2CppType* GenInst_ResultPointsAndTransitions_t1206419768_0_0_0_Types[] = { &ResultPointsAndTransitions_t1206419768_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultPointsAndTransitions_t1206419768_0_0_0 = { 1, GenInst_ResultPointsAndTransitions_t1206419768_0_0_0_Types };
static const Il2CppType* GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &ResultPoint_t1538592853_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ResultPoint_t1538592853_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3309166264_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3309166264_0_0_0_Types[] = { &KeyValuePair_2_t3309166264_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3309166264_0_0_0 = { 1, GenInst_KeyValuePair_2_t3309166264_0_0_0_Types };
extern const Il2CppType SymbolInfo_t553159586_0_0_0;
static const Il2CppType* GenInst_SymbolInfo_t553159586_0_0_0_Types[] = { &SymbolInfo_t553159586_0_0_0 };
extern const Il2CppGenericInst GenInst_SymbolInfo_t553159586_0_0_0 = { 1, GenInst_SymbolInfo_t553159586_0_0_0_Types };
extern const Il2CppType Encoder_t1508201762_0_0_0;
static const Il2CppType* GenInst_Encoder_t1508201762_0_0_0_Types[] = { &Encoder_t1508201762_0_0_0 };
extern const Il2CppGenericInst GenInst_Encoder_t1508201762_0_0_0 = { 1, GenInst_Encoder_t1508201762_0_0_0_Types };
extern const Il2CppType CharU5BU5D_t3324145743_0_0_0;
static const Il2CppType* GenInst_CharU5BU5D_t3324145743_0_0_0_Types[] = { &CharU5BU5D_t3324145743_0_0_0 };
extern const Il2CppGenericInst GenInst_CharU5BU5D_t3324145743_0_0_0 = { 1, GenInst_CharU5BU5D_t3324145743_0_0_0_Types };
extern const Il2CppType StringBuilder_t243639308_0_0_0;
static const Il2CppType* GenInst_StringBuilder_t243639308_0_0_0_Types[] = { &StringBuilder_t243639308_0_0_0 };
extern const Il2CppGenericInst GenInst_StringBuilder_t243639308_0_0_0 = { 1, GenInst_StringBuilder_t243639308_0_0_0_Types };
extern const Il2CppType FinderPattern_t4119758992_0_0_0;
static const Il2CppType* GenInst_FinderPattern_t4119758992_0_0_0_Types[] = { &FinderPattern_t4119758992_0_0_0 };
extern const Il2CppGenericInst GenInst_FinderPattern_t4119758992_0_0_0 = { 1, GenInst_FinderPattern_t4119758992_0_0_0_Types };
extern const Il2CppType OneDReader_t3436042911_0_0_0;
static const Il2CppType* GenInst_OneDReader_t3436042911_0_0_0_Types[] = { &OneDReader_t3436042911_0_0_0 };
extern const Il2CppGenericInst GenInst_OneDReader_t3436042911_0_0_0 = { 1, GenInst_OneDReader_t3436042911_0_0_0_Types };
extern const Il2CppType UPCEANReader_t3527170699_0_0_0;
static const Il2CppType* GenInst_UPCEANReader_t3527170699_0_0_0_Types[] = { &UPCEANReader_t3527170699_0_0_0 };
extern const Il2CppGenericInst GenInst_UPCEANReader_t3527170699_0_0_0 = { 1, GenInst_UPCEANReader_t3527170699_0_0_0_Types };
extern const Il2CppType Pair_t1045409632_0_0_0;
static const Il2CppType* GenInst_Pair_t1045409632_0_0_0_Types[] = { &Pair_t1045409632_0_0_0 };
extern const Il2CppGenericInst GenInst_Pair_t1045409632_0_0_0 = { 1, GenInst_Pair_t1045409632_0_0_0_Types };
extern const Il2CppType ExpandedPair_t3413366687_0_0_0;
static const Il2CppType* GenInst_ExpandedPair_t3413366687_0_0_0_Types[] = { &ExpandedPair_t3413366687_0_0_0 };
extern const Il2CppGenericInst GenInst_ExpandedPair_t3413366687_0_0_0 = { 1, GenInst_ExpandedPair_t3413366687_0_0_0_Types };
extern const Il2CppType ExpandedRow_t1562831751_0_0_0;
static const Il2CppType* GenInst_ExpandedRow_t1562831751_0_0_0_Types[] = { &ExpandedRow_t1562831751_0_0_0 };
extern const Il2CppGenericInst GenInst_ExpandedRow_t1562831751_0_0_0 = { 1, GenInst_ExpandedRow_t1562831751_0_0_0_Types };
extern const Il2CppType ObjectU5BU5D_t1108656482_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0_Types[] = { &String_t_0_0_0, &ObjectU5BU5D_t1108656482_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0 = { 2, GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &ObjectU5BU5D_t1108656482_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1827855558_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1827855558_0_0_0_Types[] = { &KeyValuePair_2_t1827855558_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1827855558_0_0_0 = { 1, GenInst_KeyValuePair_2_t1827855558_0_0_0_Types };
extern const Il2CppType ResultPointU5BU5D_t1195164344_0_0_0;
static const Il2CppType* GenInst_ResultPointU5BU5D_t1195164344_0_0_0_Types[] = { &ResultPointU5BU5D_t1195164344_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultPointU5BU5D_t1195164344_0_0_0 = { 1, GenInst_ResultPointU5BU5D_t1195164344_0_0_0_Types };
extern const Il2CppType SByteU5BU5D_t2505034988_0_0_0;
static const Il2CppType* GenInst_SByteU5BU5D_t2505034988_0_0_0_Types[] = { &SByteU5BU5D_t2505034988_0_0_0 };
extern const Il2CppGenericInst GenInst_SByteU5BU5D_t2505034988_0_0_0 = { 1, GenInst_SByteU5BU5D_t2505034988_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4260544485_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4260544485_0_0_0_Types[] = { &IEquatable_1_t4260544485_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4260544485_0_0_0 = { 1, GenInst_IEquatable_1_t4260544485_0_0_0_Types };
extern const Il2CppType IComparable_1_t326228413_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t326228413_0_0_0_Types[] = { &IComparable_1_t326228413_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t326228413_0_0_0 = { 1, GenInst_IComparable_1_t326228413_0_0_0_Types };
extern const Il2CppType DetectionResultColumn_t3195057574_0_0_0;
static const Il2CppType* GenInst_DetectionResultColumn_t3195057574_0_0_0_Types[] = { &DetectionResultColumn_t3195057574_0_0_0 };
extern const Il2CppGenericInst GenInst_DetectionResultColumn_t3195057574_0_0_0 = { 1, GenInst_DetectionResultColumn_t3195057574_0_0_0_Types };
extern const Il2CppType Codeword_t1124156527_0_0_0;
static const Il2CppType* GenInst_Codeword_t1124156527_0_0_0_Types[] = { &Codeword_t1124156527_0_0_0 };
extern const Il2CppGenericInst GenInst_Codeword_t1124156527_0_0_0 = { 1, GenInst_Codeword_t1124156527_0_0_0_Types };
extern const Il2CppType SingleU5BU5D_t2316563989_0_0_0;
static const Il2CppType* GenInst_SingleU5BU5D_t2316563989_0_0_0_Types[] = { &SingleU5BU5D_t2316563989_0_0_0 };
extern const Il2CppGenericInst GenInst_SingleU5BU5D_t2316563989_0_0_0 = { 1, GenInst_SingleU5BU5D_t2316563989_0_0_0_Types };
extern const Il2CppType BarcodeValueU5BU5D_t3614862452_0_0_0;
static const Il2CppType* GenInst_BarcodeValueU5BU5D_t3614862452_0_0_0_Types[] = { &BarcodeValueU5BU5D_t3614862452_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeValueU5BU5D_t3614862452_0_0_0 = { 1, GenInst_BarcodeValueU5BU5D_t3614862452_0_0_0_Types };
extern const Il2CppType BarcodeValue_t1597289545_0_0_0;
static const Il2CppType* GenInst_BarcodeValue_t1597289545_0_0_0_Types[] = { &BarcodeValue_t1597289545_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeValue_t1597289545_0_0_0 = { 1, GenInst_BarcodeValue_t1597289545_0_0_0_Types };
extern const Il2CppType ModulusPoly_t3133325097_0_0_0;
static const Il2CppType* GenInst_ModulusPoly_t3133325097_0_0_0_Types[] = { &ModulusPoly_t3133325097_0_0_0 };
extern const Il2CppGenericInst GenInst_ModulusPoly_t3133325097_0_0_0 = { 1, GenInst_ModulusPoly_t3133325097_0_0_0_Types };
extern const Il2CppType BarcodeRow_t3616570866_0_0_0;
static const Il2CppType* GenInst_BarcodeRow_t3616570866_0_0_0_Types[] = { &BarcodeRow_t3616570866_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeRow_t3616570866_0_0_0 = { 1, GenInst_BarcodeRow_t3616570866_0_0_0_Types };
extern const Il2CppType DataBlock_t3210726793_0_0_0;
static const Il2CppType* GenInst_DataBlock_t3210726793_0_0_0_Types[] = { &DataBlock_t3210726793_0_0_0 };
extern const Il2CppGenericInst GenInst_DataBlock_t3210726793_0_0_0 = { 1, GenInst_DataBlock_t3210726793_0_0_0_Types };
extern const Il2CppType ECB_t2022291218_0_0_0;
static const Il2CppType* GenInst_ECB_t2022291218_0_0_0_Types[] = { &ECB_t2022291218_0_0_0 };
extern const Il2CppGenericInst GenInst_ECB_t2022291218_0_0_0 = { 1, GenInst_ECB_t2022291218_0_0_0_Types };
extern const Il2CppType DataMask_t1708385874_0_0_0;
static const Il2CppType* GenInst_DataMask_t1708385874_0_0_0_Types[] = { &DataMask_t1708385874_0_0_0 };
extern const Il2CppGenericInst GenInst_DataMask_t1708385874_0_0_0 = { 1, GenInst_DataMask_t1708385874_0_0_0_Types };
extern const Il2CppType ErrorCorrectionLevel_t1225927610_0_0_0;
static const Il2CppType* GenInst_ErrorCorrectionLevel_t1225927610_0_0_0_Types[] = { &ErrorCorrectionLevel_t1225927610_0_0_0 };
extern const Il2CppGenericInst GenInst_ErrorCorrectionLevel_t1225927610_0_0_0 = { 1, GenInst_ErrorCorrectionLevel_t1225927610_0_0_0_Types };
extern const Il2CppType Version_t1953509534_0_0_0;
static const Il2CppType* GenInst_Version_t1953509534_0_0_0_Types[] = { &Version_t1953509534_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1953509534_0_0_0 = { 1, GenInst_Version_t1953509534_0_0_0_Types };
extern const Il2CppType ECBlocks_t3771581654_0_0_0;
static const Il2CppType* GenInst_ECBlocks_t3771581654_0_0_0_Types[] = { &ECBlocks_t3771581654_0_0_0 };
extern const Il2CppGenericInst GenInst_ECBlocks_t3771581654_0_0_0 = { 1, GenInst_ECBlocks_t3771581654_0_0_0_Types };
extern const Il2CppType AlignmentPattern_t1786970569_0_0_0;
static const Il2CppType* GenInst_AlignmentPattern_t1786970569_0_0_0_Types[] = { &AlignmentPattern_t1786970569_0_0_0 };
extern const Il2CppGenericInst GenInst_AlignmentPattern_t1786970569_0_0_0 = { 1, GenInst_AlignmentPattern_t1786970569_0_0_0_Types };
extern const Il2CppType BlockPair_t178469773_0_0_0;
static const Il2CppType* GenInst_BlockPair_t178469773_0_0_0_Types[] = { &BlockPair_t178469773_0_0_0 };
extern const Il2CppGenericInst GenInst_BlockPair_t178469773_0_0_0 = { 1, GenInst_BlockPair_t178469773_0_0_0_Types };
extern const Il2CppType EyewearCalibrationReading_t3010462567_0_0_0;
static const Il2CppType* GenInst_EyewearCalibrationReading_t3010462567_0_0_0_Types[] = { &EyewearCalibrationReading_t3010462567_0_0_0 };
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t3010462567_0_0_0 = { 1, GenInst_EyewearCalibrationReading_t3010462567_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t2475465524_0_0_0;
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types[] = { &Camera_t2727095145_0_0_0, &VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 = { 2, GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Camera_t2727095145_0_0_0, &VideoBackgroundAbstractBehaviour_t2475465524_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1857232484_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1857232484_0_0_0_Types[] = { &KeyValuePair_2_t1857232484_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1857232484_0_0_0 = { 1, GenInst_KeyValuePair_2_t1857232484_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_Types };
extern const Il2CppType Renderer_t3076687687_0_0_0;
static const Il2CppType* GenInst_Renderer_t3076687687_0_0_0_Types[] = { &Renderer_t3076687687_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t3076687687_0_0_0 = { 1, GenInst_Renderer_t3076687687_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t4179556250_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t4179556250_0_0_0_Types[] = { &TrackableBehaviour_t4179556250_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t4179556250_0_0_0 = { 1, GenInst_TrackableBehaviour_t4179556250_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t1613618350_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t1613618350_0_0_0_Types[] = { &ITrackableEventHandler_t1613618350_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t1613618350_0_0_0 = { 1, GenInst_ITrackableEventHandler_t1613618350_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t1860057025_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t1860057025_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t796991165_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t796991165_0_0_0_Types[] = { &ICloudRecoEventHandler_t796991165_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t796991165_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t796991165_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t3609410114_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t3609410114_0_0_0_Types[] = { &TargetSearchResult_t3609410114_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t3609410114_0_0_0 = { 1, GenInst_TargetSearchResult_t3609410114_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t465046465_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t465046465_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0_Types };
extern const Il2CppType Trackable_t3781061455_0_0_0;
static const Il2CppType* GenInst_Trackable_t3781061455_0_0_0_Types[] = { &Trackable_t3781061455_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t3781061455_0_0_0 = { 1, GenInst_Trackable_t3781061455_0_0_0_Types };
extern const Il2CppType VirtualButton_t704206407_0_0_0;
static const Il2CppType* GenInst_VirtualButton_t704206407_0_0_0_Types[] = { &VirtualButton_t704206407_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t704206407_0_0_0 = { 1, GenInst_VirtualButton_t704206407_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t354375056_0_0_0;
extern const Il2CppType Image_t2247677317_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Image_t2247677317_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1718082560_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1718082560_0_0_0_Types[] = { &KeyValuePair_2_t1718082560_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1718082560_0_0_0 = { 1, GenInst_KeyValuePair_2_t1718082560_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &PIXEL_FORMAT_t354375056_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1718082560_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &Image_t2247677317_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4089910802_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4089910802_0_0_0_Types[] = { &KeyValuePair_2_t4089910802_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4089910802_0_0_0 = { 1, GenInst_KeyValuePair_2_t4089910802_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Trackable_t3781061455_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Trackable_t3781061455_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3677105400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3677105400_0_0_0_Types[] = { &KeyValuePair_2_t3677105400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3677105400_0_0_0 = { 1, GenInst_KeyValuePair_2_t3677105400_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButton_t704206407_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButton_t704206407_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t600250352_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t600250352_0_0_0_Types[] = { &KeyValuePair_2_t600250352_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t600250352_0_0_0 = { 1, GenInst_KeyValuePair_2_t600250352_0_0_0_Types };
extern const Il2CppType DataSet_t2095838082_0_0_0;
static const Il2CppType* GenInst_DataSet_t2095838082_0_0_0_Types[] = { &DataSet_t2095838082_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t2095838082_0_0_0 = { 1, GenInst_DataSet_t2095838082_0_0_0_Types };
extern const Il2CppType DataSetImpl_t1837478850_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t1837478850_0_0_0_Types[] = { &DataSetImpl_t1837478850_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t1837478850_0_0_0 = { 1, GenInst_DataSetImpl_t1837478850_0_0_0_Types };
extern const Il2CppType Marker_t616694972_0_0_0;
static const Il2CppType* GenInst_Marker_t616694972_0_0_0_Types[] = { &Marker_t616694972_0_0_0 };
extern const Il2CppGenericInst GenInst_Marker_t616694972_0_0_0 = { 1, GenInst_Marker_t616694972_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Marker_t616694972_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Marker_t616694972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t512738917_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t512738917_0_0_0_Types[] = { &KeyValuePair_2_t512738917_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t512738917_0_0_0 = { 1, GenInst_KeyValuePair_2_t512738917_0_0_0_Types };
extern const Il2CppType TrackableResultData_t395876724_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t395876724_0_0_0_Types[] = { &TrackableResultData_t395876724_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t395876724_0_0_0 = { 1, GenInst_TrackableResultData_t395876724_0_0_0_Types };
extern const Il2CppType WordData_t1548845516_0_0_0;
static const Il2CppType* GenInst_WordData_t1548845516_0_0_0_Types[] = { &WordData_t1548845516_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t1548845516_0_0_0 = { 1, GenInst_WordData_t1548845516_0_0_0_Types };
extern const Il2CppType WordResultData_t1826866697_0_0_0;
static const Il2CppType* GenInst_WordResultData_t1826866697_0_0_0_Types[] = { &WordResultData_t1826866697_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t1826866697_0_0_0 = { 1, GenInst_WordResultData_t1826866697_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t3919795561_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t3919795561_0_0_0_Types[] = { &SmartTerrainRevisionData_t3919795561_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t3919795561_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t3919795561_0_0_0_Types };
extern const Il2CppType SurfaceData_t1737958143_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t1737958143_0_0_0_Types[] = { &SurfaceData_t1737958143_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t1737958143_0_0_0 = { 1, GenInst_SurfaceData_t1737958143_0_0_0_Types };
extern const Il2CppType PropData_t526813605_0_0_0;
static const Il2CppType* GenInst_PropData_t526813605_0_0_0_Types[] = { &PropData_t526813605_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t526813605_0_0_0 = { 1, GenInst_PropData_t526813605_0_0_0_Types };
extern const Il2CppType IEditorTrackableBehaviour_t1840126232_0_0_0;
static const Il2CppType* GenInst_IEditorTrackableBehaviour_t1840126232_0_0_0_Types[] = { &IEditorTrackableBehaviour_t1840126232_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t1840126232_0_0_0 = { 1, GenInst_IEditorTrackableBehaviour_t1840126232_0_0_0_Types };
static const Il2CppType* GenInst_Image_t2247677317_0_0_0_Types[] = { &Image_t2247677317_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2247677317_0_0_0 = { 1, GenInst_Image_t2247677317_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t1276145027_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t1276145027_0_0_0_Types[] = { &SmartTerrainTrackable_t1276145027_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t1276145027_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t1276145027_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t2826579942_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t2826579942_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2093487825_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2093487825_0_0_0_Types[] = { &KeyValuePair_2_t2093487825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487825_0_0_0 = { 1, GenInst_KeyValuePair_2_t2093487825_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t24667923_0_0_0, &KeyValuePair_2_t2093487825_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t24667923_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t28871114_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t28871114_0_0_0_Types[] = { &KeyValuePair_2_t28871114_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t28871114_0_0_0 = { 1, GenInst_KeyValuePair_2_t28871114_0_0_0_Types };
extern const Il2CppType RectangleData_t2265684451_0_0_0;
static const Il2CppType* GenInst_RectangleData_t2265684451_0_0_0_Types[] = { &RectangleData_t2265684451_0_0_0 };
extern const Il2CppGenericInst GenInst_RectangleData_t2265684451_0_0_0 = { 1, GenInst_RectangleData_t2265684451_0_0_0_Types };
extern const Il2CppType WordResult_t1079862857_0_0_0;
static const Il2CppType* GenInst_WordResult_t1079862857_0_0_0_Types[] = { &WordResult_t1079862857_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t1079862857_0_0_0 = { 1, GenInst_WordResult_t1079862857_0_0_0_Types };
extern const Il2CppType Word_t2165514892_0_0_0;
static const Il2CppType* GenInst_Word_t2165514892_0_0_0_Types[] = { &Word_t2165514892_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t2165514892_0_0_0 = { 1, GenInst_Word_t2165514892_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t545947899_0_0_0;
static const Il2CppType* GenInst_WordAbstractBehaviour_t545947899_0_0_0_Types[] = { &WordAbstractBehaviour_t545947899_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t545947899_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t545947899_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordResult_t1079862857_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordResult_t1079862857_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t975906802_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t975906802_0_0_0_Types[] = { &KeyValuePair_2_t975906802_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t975906802_0_0_0 = { 1, GenInst_KeyValuePair_2_t975906802_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordAbstractBehaviour_t545947899_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &WordAbstractBehaviour_t545947899_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t441991844_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t441991844_0_0_0_Types[] = { &KeyValuePair_2_t441991844_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t441991844_0_0_0 = { 1, GenInst_KeyValuePair_2_t441991844_0_0_0_Types };
extern const Il2CppType List_1_t1914133451_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1914133451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1914133451_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2633332527_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2633332527_0_0_0_Types[] = { &KeyValuePair_2_t2633332527_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2633332527_0_0_0 = { 1, GenInst_KeyValuePair_2_t2633332527_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1914133451_0_0_0_Types[] = { &List_1_t1914133451_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1914133451_0_0_0 = { 1, GenInst_List_1_t1914133451_0_0_0_Types };
extern const Il2CppType ILoadLevelEventHandler_t678501447_0_0_0;
static const Il2CppType* GenInst_ILoadLevelEventHandler_t678501447_0_0_0_Types[] = { &ILoadLevelEventHandler_t678501447_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t678501447_0_0_0 = { 1, GenInst_ILoadLevelEventHandler_t678501447_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t2222092879_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0_Types[] = { &ISmartTerrainEventHandler_t2222092879_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t587327548_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t587327548_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0_Types };
extern const Il2CppType Prop_t2165309157_0_0_0;
static const Il2CppType* GenInst_Prop_t2165309157_0_0_0_Types[] = { &Prop_t2165309157_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t2165309157_0_0_0 = { 1, GenInst_Prop_t2165309157_0_0_0_Types };
extern const Il2CppType Surface_t3094389719_0_0_0;
static const Il2CppType* GenInst_Surface_t3094389719_0_0_0_Types[] = { &Surface_t3094389719_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t3094389719_0_0_0 = { 1, GenInst_Surface_t3094389719_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Surface_t3094389719_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Surface_t3094389719_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2990433664_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2990433664_0_0_0_Types[] = { &KeyValuePair_2_t2990433664_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2990433664_0_0_0 = { 1, GenInst_KeyValuePair_2_t2990433664_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t142368528_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &SurfaceAbstractBehaviour_t142368528_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &SurfaceAbstractBehaviour_t142368528_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38412473_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38412473_0_0_0_Types[] = { &KeyValuePair_2_t38412473_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38412473_0_0_0 = { 1, GenInst_KeyValuePair_2_t38412473_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Prop_t2165309157_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Prop_t2165309157_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2061353102_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2061353102_0_0_0_Types[] = { &KeyValuePair_2_t2061353102_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2061353102_0_0_0 = { 1, GenInst_KeyValuePair_2_t2061353102_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t1293468098_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PropAbstractBehaviour_t1293468098_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PropAbstractBehaviour_t1293468098_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1189512043_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1189512043_0_0_0_Types[] = { &KeyValuePair_2_t1189512043_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1189512043_0_0_0 = { 1, GenInst_KeyValuePair_2_t1189512043_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t1293468098_0_0_0_Types[] = { &PropAbstractBehaviour_t1293468098_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t1293468098_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t1293468098_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t142368528_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0_Types };
extern const Il2CppType MeshFilter_t3839065225_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3839065225_0_0_0_Types[] = { &MeshFilter_t3839065225_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3839065225_0_0_0 = { 1, GenInst_MeshFilter_t3839065225_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableBehaviour_t4179556250_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableBehaviour_t4179556250_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4075600195_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4075600195_0_0_0_Types[] = { &KeyValuePair_2_t4075600195_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4075600195_0_0_0 = { 1, GenInst_KeyValuePair_2_t4075600195_0_0_0_Types };
extern const Il2CppType MarkerAbstractBehaviour_t865486027_0_0_0;
static const Il2CppType* GenInst_MarkerAbstractBehaviour_t865486027_0_0_0_Types[] = { &MarkerAbstractBehaviour_t865486027_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t865486027_0_0_0 = { 1, GenInst_MarkerAbstractBehaviour_t865486027_0_0_0_Types };
extern const Il2CppType IEditorMarkerBehaviour_t1845962767_0_0_0;
static const Il2CppType* GenInst_IEditorMarkerBehaviour_t1845962767_0_0_0_Types[] = { &IEditorMarkerBehaviour_t1845962767_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t1845962767_0_0_0 = { 1, GenInst_IEditorMarkerBehaviour_t1845962767_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t3541475369_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t3541475369_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t3340678586_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0_Types[] = { &DataSetTrackableBehaviour_t3340678586_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0_Types };
extern const Il2CppType IEditorDataSetTrackableBehaviour_t189565564_0_0_0;
static const Il2CppType* GenInst_IEditorDataSetTrackableBehaviour_t189565564_0_0_0_Types[] = { &IEditorDataSetTrackableBehaviour_t189565564_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t189565564_0_0_0 = { 1, GenInst_IEditorDataSetTrackableBehaviour_t189565564_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t1191238304_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t1191238304_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types };
extern const Il2CppType IEditorVirtualButtonBehaviour_t4082570784_0_0_0;
static const Il2CppType* GenInst_IEditorVirtualButtonBehaviour_t4082570784_0_0_0_Types[] = { &IEditorVirtualButtonBehaviour_t4082570784_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t4082570784_0_0_0 = { 1, GenInst_IEditorVirtualButtonBehaviour_t4082570784_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableResultData_t395876724_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t291920669_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t291920669_0_0_0_Types[] = { &KeyValuePair_2_t291920669_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t291920669_0_0_0 = { 1, GenInst_KeyValuePair_2_t291920669_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableResultData_t395876724_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableResultData_t395876724_0_0_0, &TrackableResultData_t395876724_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableResultData_t395876724_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_KeyValuePair_2_t291920669_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &TrackableResultData_t395876724_0_0_0, &KeyValuePair_2_t291920669_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_KeyValuePair_2_t291920669_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_KeyValuePair_2_t291920669_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t459380335_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t355424280_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t355424280_0_0_0_Types[] = { &KeyValuePair_2_t355424280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t355424280_0_0_0 = { 1, GenInst_KeyValuePair_2_t355424280_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t459380335_0_0_0_Types[] = { &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t459380335_0_0_0 = { 1, GenInst_VirtualButtonData_t459380335_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonData_t459380335_0_0_0, &KeyValuePair_2_t355424280_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types };
extern const Il2CppType ImageTarget_t3520455670_0_0_0;
static const Il2CppType* GenInst_ImageTarget_t3520455670_0_0_0_Types[] = { &ImageTarget_t3520455670_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t3520455670_0_0_0 = { 1, GenInst_ImageTarget_t3520455670_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &ImageTarget_t3520455670_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &ImageTarget_t3520455670_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3416499615_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3416499615_0_0_0_Types[] = { &KeyValuePair_2_t3416499615_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3416499615_0_0_0 = { 1, GenInst_KeyValuePair_2_t3416499615_0_0_0_Types };
extern const Il2CppType ProfileData_t1961690790_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4030510692_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4030510692_0_0_0_Types[] = { &KeyValuePair_2_t4030510692_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030510692_0_0_0 = { 1, GenInst_KeyValuePair_2_t4030510692_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1961690790_0_0_0_Types[] = { &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1961690790_0_0_0 = { 1, GenInst_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1961690790_0_0_0, &KeyValuePair_2_t4030510692_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1961690790_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2680889866_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2680889866_0_0_0_Types[] = { &KeyValuePair_2_t2680889866_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2680889866_0_0_0 = { 1, GenInst_KeyValuePair_2_t2680889866_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonAbstractBehaviour_t1191238304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &VirtualButtonAbstractBehaviour_t1191238304_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1087282249_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1087282249_0_0_0_Types[] = { &KeyValuePair_2_t1087282249_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1087282249_0_0_0 = { 1, GenInst_KeyValuePair_2_t1087282249_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t3566668545_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t3566668545_0_0_0_Types[] = { &ITrackerEventHandler_t3566668545_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t3566668545_0_0_0 = { 1, GenInst_ITrackerEventHandler_t3566668545_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t4010260306_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t4010260306_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0_Types };
extern const Il2CppType InitError_t432217960_0_0_0;
static const Il2CppType* GenInst_InitError_t432217960_0_0_0_Types[] = { &InitError_t432217960_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t432217960_0_0_0 = { 1, GenInst_InitError_t432217960_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t155716687_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t155716687_0_0_0_Types[] = { &ITextRecoEventHandler_t155716687_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t155716687_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t155716687_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t2660219000_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t2660219000_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0_Types };
extern const Il2CppType MeshRenderer_t2804666580_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t2804666580_0_0_0_Types[] = { &MeshRenderer_t2804666580_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t2804666580_0_0_0 = { 1, GenInst_MeshRenderer_t2804666580_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t2952053798_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0_Types[] = { &IVirtualButtonEventHandler_t2952053798_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0_Types };
extern const Il2CppType BaseInputModule_t15847059_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t15847059_0_0_0_Types[] = { &BaseInputModule_t15847059_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t15847059_0_0_0 = { 1, GenInst_BaseInputModule_t15847059_0_0_0_Types };
extern const Il2CppType RaycastResult_t3762661364_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t3762661364_0_0_0_Types[] = { &RaycastResult_t3762661364_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3762661364_0_0_0 = { 1, GenInst_RaycastResult_t3762661364_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t4282929196_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t4282929196_0_0_0_Types[] = { &IDeselectHandler_t4282929196_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t4282929196_0_0_0 = { 1, GenInst_IDeselectHandler_t4282929196_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t1130525624_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t1130525624_0_0_0_Types[] = { &IEventSystemHandler_t1130525624_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t1130525624_0_0_0 = { 1, GenInst_IEventSystemHandler_t1130525624_0_0_0_Types };
extern const Il2CppType List_1_t2498711176_0_0_0;
static const Il2CppType* GenInst_List_1_t2498711176_0_0_0_Types[] = { &List_1_t2498711176_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2498711176_0_0_0 = { 1, GenInst_List_1_t2498711176_0_0_0_Types };
extern const Il2CppType List_1_t1244034627_0_0_0;
static const Il2CppType* GenInst_List_1_t1244034627_0_0_0_Types[] = { &List_1_t1244034627_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1244034627_0_0_0 = { 1, GenInst_List_1_t1244034627_0_0_0_Types };
extern const Il2CppType List_1_t574734531_0_0_0;
static const Il2CppType* GenInst_List_1_t574734531_0_0_0_Types[] = { &List_1_t574734531_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t574734531_0_0_0 = { 1, GenInst_List_1_t574734531_0_0_0_Types };
extern const Il2CppType ISelectHandler_t3580292109_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t3580292109_0_0_0_Types[] = { &ISelectHandler_t3580292109_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t3580292109_0_0_0 = { 1, GenInst_ISelectHandler_t3580292109_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2327671059_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2327671059_0_0_0_Types[] = { &BaseRaycaster_t2327671059_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2327671059_0_0_0 = { 1, GenInst_BaseRaycaster_t2327671059_0_0_0_Types };
extern const Il2CppType Entry_t849715470_0_0_0;
static const Il2CppType* GenInst_Entry_t849715470_0_0_0_Types[] = { &Entry_t849715470_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t849715470_0_0_0 = { 1, GenInst_Entry_t849715470_0_0_0_Types };
extern const Il2CppType BaseEventData_t2054899105_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2054899105_0_0_0_Types[] = { &BaseEventData_t2054899105_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2054899105_0_0_0 = { 1, GenInst_BaseEventData_t2054899105_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t1772318350_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t1772318350_0_0_0_Types[] = { &IPointerEnterHandler_t1772318350_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t1772318350_0_0_0 = { 1, GenInst_IPointerEnterHandler_t1772318350_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t3471580134_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t3471580134_0_0_0_Types[] = { &IPointerExitHandler_t3471580134_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t3471580134_0_0_0 = { 1, GenInst_IPointerExitHandler_t3471580134_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t1340699618_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t1340699618_0_0_0_Types[] = { &IPointerDownHandler_t1340699618_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t1340699618_0_0_0 = { 1, GenInst_IPointerDownHandler_t1340699618_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t3049278345_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t3049278345_0_0_0_Types[] = { &IPointerUpHandler_t3049278345_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t3049278345_0_0_0 = { 1, GenInst_IPointerUpHandler_t3049278345_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t2979908062_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t2979908062_0_0_0_Types[] = { &IPointerClickHandler_t2979908062_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t2979908062_0_0_0 = { 1, GenInst_IPointerClickHandler_t2979908062_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t220416063_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t220416063_0_0_0_Types[] = { &IInitializePotentialDragHandler_t220416063_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t220416063_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t220416063_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t1569653796_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t1569653796_0_0_0_Types[] = { &IBeginDragHandler_t1569653796_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t1569653796_0_0_0 = { 1, GenInst_IBeginDragHandler_t1569653796_0_0_0_Types };
extern const Il2CppType IDragHandler_t880586197_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t880586197_0_0_0_Types[] = { &IDragHandler_t880586197_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t880586197_0_0_0 = { 1, GenInst_IDragHandler_t880586197_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t2789914546_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t2789914546_0_0_0_Types[] = { &IEndDragHandler_t2789914546_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t2789914546_0_0_0 = { 1, GenInst_IEndDragHandler_t2789914546_0_0_0_Types };
extern const Il2CppType IDropHandler_t4146418618_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t4146418618_0_0_0_Types[] = { &IDropHandler_t4146418618_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t4146418618_0_0_0 = { 1, GenInst_IDropHandler_t4146418618_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3315383580_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3315383580_0_0_0_Types[] = { &IScrollHandler_t3315383580_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3315383580_0_0_0 = { 1, GenInst_IScrollHandler_t3315383580_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3805412741_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3805412741_0_0_0_Types[] = { &IUpdateSelectedHandler_t3805412741_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3805412741_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3805412741_0_0_0_Types };
extern const Il2CppType IMoveHandler_t3984942232_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t3984942232_0_0_0_Types[] = { &IMoveHandler_t3984942232_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t3984942232_0_0_0 = { 1, GenInst_IMoveHandler_t3984942232_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t2608359793_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t2608359793_0_0_0_Types[] = { &ISubmitHandler_t2608359793_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t2608359793_0_0_0 = { 1, GenInst_ISubmitHandler_t2608359793_0_0_0_Types };
extern const Il2CppType ICancelHandler_t4180011215_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t4180011215_0_0_0_Types[] = { &ICancelHandler_t4180011215_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t4180011215_0_0_0 = { 1, GenInst_ICancelHandler_t4180011215_0_0_0_Types };
extern const Il2CppType Transform_t1659122786_0_0_0;
static const Il2CppType* GenInst_Transform_t1659122786_0_0_0_Types[] = { &Transform_t1659122786_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t1659122786_0_0_0 = { 1, GenInst_Transform_t1659122786_0_0_0_Types };
extern const Il2CppType PointerEventData_t1848751023_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PointerEventData_t1848751023_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PointerEventData_t1848751023_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744794968_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744794968_0_0_0_Types[] = { &KeyValuePair_2_t1744794968_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744794968_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744794968_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1848751023_0_0_0_Types[] = { &PointerEventData_t1848751023_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1848751023_0_0_0 = { 1, GenInst_PointerEventData_t1848751023_0_0_0_Types };
extern const Il2CppType ButtonState_t2039702646_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2039702646_0_0_0_Types[] = { &ButtonState_t2039702646_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2039702646_0_0_0 = { 1, GenInst_ButtonState_t2039702646_0_0_0_Types };
extern const Il2CppType ICanvasElement_t1249239335_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t1249239335_0_0_0_Types[] = { &ICanvasElement_t1249239335_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1249239335_0_0_0 = { 1, GenInst_ICanvasElement_t1249239335_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &ICanvasElement_t1249239335_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ICanvasElement_t1249239335_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType ColorBlock_t508458230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t508458230_0_0_0_Types[] = { &ColorBlock_t508458230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t508458230_0_0_0 = { 1, GenInst_ColorBlock_t508458230_0_0_0_Types };
extern const Il2CppType OptionData_t185687546_0_0_0;
static const Il2CppType* GenInst_OptionData_t185687546_0_0_0_Types[] = { &OptionData_t185687546_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t185687546_0_0_0 = { 1, GenInst_OptionData_t185687546_0_0_0_Types };
extern const Il2CppType DropdownItem_t3215807551_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t3215807551_0_0_0_Types[] = { &DropdownItem_t3215807551_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t3215807551_0_0_0 = { 1, GenInst_DropdownItem_t3215807551_0_0_0_Types };
extern const Il2CppType FloatTween_t2711705593_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2711705593_0_0_0_Types[] = { &FloatTween_t2711705593_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2711705593_0_0_0 = { 1, GenInst_FloatTween_t2711705593_0_0_0_Types };
extern const Il2CppType Sprite_t3199167241_0_0_0;
static const Il2CppType* GenInst_Sprite_t3199167241_0_0_0_Types[] = { &Sprite_t3199167241_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t3199167241_0_0_0 = { 1, GenInst_Sprite_t3199167241_0_0_0_Types };
extern const Il2CppType Canvas_t2727140764_0_0_0;
static const Il2CppType* GenInst_Canvas_t2727140764_0_0_0_Types[] = { &Canvas_t2727140764_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t2727140764_0_0_0 = { 1, GenInst_Canvas_t2727140764_0_0_0_Types };
extern const Il2CppType List_1_t4095326316_0_0_0;
static const Il2CppType* GenInst_List_1_t4095326316_0_0_0_Types[] = { &List_1_t4095326316_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4095326316_0_0_0 = { 1, GenInst_List_1_t4095326316_0_0_0_Types };
extern const Il2CppType List_1_t1377224777_0_0_0;
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_Types[] = { &Font_t4241557075_0_0_0, &List_1_t1377224777_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0 = { 2, GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_Types };
extern const Il2CppType Text_t9039225_0_0_0;
static const Il2CppType* GenInst_Text_t9039225_0_0_0_Types[] = { &Text_t9039225_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t9039225_0_0_0 = { 1, GenInst_Text_t9039225_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Font_t4241557075_0_0_0, &List_1_t1377224777_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1496360359_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1496360359_0_0_0_Types[] = { &KeyValuePair_2_t1496360359_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1496360359_0_0_0 = { 1, GenInst_KeyValuePair_2_t1496360359_0_0_0_Types };
extern const Il2CppType ColorTween_t723277650_0_0_0;
static const Il2CppType* GenInst_ColorTween_t723277650_0_0_0_Types[] = { &ColorTween_t723277650_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t723277650_0_0_0 = { 1, GenInst_ColorTween_t723277650_0_0_0_Types };
extern const Il2CppType Graphic_t836799438_0_0_0;
static const Il2CppType* GenInst_Graphic_t836799438_0_0_0_Types[] = { &Graphic_t836799438_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t836799438_0_0_0 = { 1, GenInst_Graphic_t836799438_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3656898711_0_0_0;
static const Il2CppType* GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_Types[] = { &Canvas_t2727140764_0_0_0, &IndexedSet_1_t3656898711_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0 = { 2, GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Graphic_t836799438_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Graphic_t836799438_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Canvas_t2727140764_0_0_0, &IndexedSet_1_t3656898711_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3226014568_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3226014568_0_0_0_Types[] = { &KeyValuePair_2_t3226014568_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3226014568_0_0_0 = { 1, GenInst_KeyValuePair_2_t3226014568_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1354567995_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1354567995_0_0_0_Types[] = { &KeyValuePair_2_t1354567995_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1354567995_0_0_0 = { 1, GenInst_KeyValuePair_2_t1354567995_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3983107678_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3983107678_0_0_0_Types[] = { &KeyValuePair_2_t3983107678_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3983107678_0_0_0 = { 1, GenInst_KeyValuePair_2_t3983107678_0_0_0_Types };
extern const Il2CppType ContentType_t2662964855_0_0_0;
static const Il2CppType* GenInst_ContentType_t2662964855_0_0_0_Types[] = { &ContentType_t2662964855_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t2662964855_0_0_0 = { 1, GenInst_ContentType_t2662964855_0_0_0_Types };
extern const Il2CppType Mask_t8826680_0_0_0;
static const Il2CppType* GenInst_Mask_t8826680_0_0_0_Types[] = { &Mask_t8826680_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t8826680_0_0_0 = { 1, GenInst_Mask_t8826680_0_0_0_Types };
extern const Il2CppType List_1_t1377012232_0_0_0;
static const Il2CppType* GenInst_List_1_t1377012232_0_0_0_Types[] = { &List_1_t1377012232_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1377012232_0_0_0 = { 1, GenInst_List_1_t1377012232_0_0_0_Types };
extern const Il2CppType RectMask2D_t3357079374_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t3357079374_0_0_0_Types[] = { &RectMask2D_t3357079374_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t3357079374_0_0_0 = { 1, GenInst_RectMask2D_t3357079374_0_0_0_Types };
extern const Il2CppType List_1_t430297630_0_0_0;
static const Il2CppType* GenInst_List_1_t430297630_0_0_0_Types[] = { &List_1_t430297630_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t430297630_0_0_0 = { 1, GenInst_List_1_t430297630_0_0_0_Types };
extern const Il2CppType Navigation_t1108456480_0_0_0;
static const Il2CppType* GenInst_Navigation_t1108456480_0_0_0_Types[] = { &Navigation_t1108456480_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1108456480_0_0_0 = { 1, GenInst_Navigation_t1108456480_0_0_0_Types };
extern const Il2CppType IClippable_t502791197_0_0_0;
static const Il2CppType* GenInst_IClippable_t502791197_0_0_0_Types[] = { &IClippable_t502791197_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t502791197_0_0_0 = { 1, GenInst_IClippable_t502791197_0_0_0_Types };
extern const Il2CppType Selectable_t1885181538_0_0_0;
static const Il2CppType* GenInst_Selectable_t1885181538_0_0_0_Types[] = { &Selectable_t1885181538_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1885181538_0_0_0 = { 1, GenInst_Selectable_t1885181538_0_0_0_Types };
extern const Il2CppType SpriteState_t2895308594_0_0_0;
static const Il2CppType* GenInst_SpriteState_t2895308594_0_0_0_Types[] = { &SpriteState_t2895308594_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t2895308594_0_0_0 = { 1, GenInst_SpriteState_t2895308594_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3702418109_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3702418109_0_0_0_Types[] = { &CanvasGroup_t3702418109_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3702418109_0_0_0 = { 1, GenInst_CanvasGroup_t3702418109_0_0_0_Types };
extern const Il2CppType MatEntry_t1574154081_0_0_0;
static const Il2CppType* GenInst_MatEntry_t1574154081_0_0_0_Types[] = { &MatEntry_t1574154081_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t1574154081_0_0_0 = { 1, GenInst_MatEntry_t1574154081_0_0_0_Types };
extern const Il2CppType Toggle_t110812896_0_0_0;
static const Il2CppType* GenInst_Toggle_t110812896_0_0_0_Types[] = { &Toggle_t110812896_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t110812896_0_0_0 = { 1, GenInst_Toggle_t110812896_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Toggle_t110812896_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType IClipper_t1175935472_0_0_0;
static const Il2CppType* GenInst_IClipper_t1175935472_0_0_0_Types[] = { &IClipper_t1175935472_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1175935472_0_0_0 = { 1, GenInst_IClipper_t1175935472_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &IClipper_t1175935472_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &IClipper_t1175935472_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1329917009_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1329917009_0_0_0_Types[] = { &KeyValuePair_2_t1329917009_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1329917009_0_0_0 = { 1, GenInst_KeyValuePair_2_t1329917009_0_0_0_Types };
extern const Il2CppType RectTransform_t972643934_0_0_0;
static const Il2CppType* GenInst_RectTransform_t972643934_0_0_0_Types[] = { &RectTransform_t972643934_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t972643934_0_0_0 = { 1, GenInst_RectTransform_t972643934_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t1942933988_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t1942933988_0_0_0_Types[] = { &LayoutRebuilder_t1942933988_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t1942933988_0_0_0 = { 1, GenInst_LayoutRebuilder_t1942933988_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1646037781_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0_Types[] = { &ILayoutElement_t1646037781_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Types };
extern const Il2CppType List_1_t1355284822_0_0_0;
static const Il2CppType* GenInst_List_1_t1355284822_0_0_0_Types[] = { &List_1_t1355284822_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1355284822_0_0_0 = { 1, GenInst_List_1_t1355284822_0_0_0_Types };
extern const Il2CppType List_1_t1967039240_0_0_0;
static const Il2CppType* GenInst_List_1_t1967039240_0_0_0_Types[] = { &List_1_t1967039240_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1967039240_0_0_0 = { 1, GenInst_List_1_t1967039240_0_0_0_Types };
extern const Il2CppType List_1_t1355284821_0_0_0;
static const Il2CppType* GenInst_List_1_t1355284821_0_0_0_Types[] = { &List_1_t1355284821_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1355284821_0_0_0 = { 1, GenInst_List_1_t1355284821_0_0_0_Types };
extern const Il2CppType List_1_t1355284823_0_0_0;
static const Il2CppType* GenInst_List_1_t1355284823_0_0_0_Types[] = { &List_1_t1355284823_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1355284823_0_0_0 = { 1, GenInst_List_1_t1355284823_0_0_0_Types };
extern const Il2CppType List_1_t2522024052_0_0_0;
static const Il2CppType* GenInst_List_1_t2522024052_0_0_0_Types[] = { &List_1_t2522024052_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2522024052_0_0_0 = { 1, GenInst_List_1_t2522024052_0_0_0_Types };
extern const Il2CppType List_1_t1317283468_0_0_0;
static const Il2CppType* GenInst_List_1_t1317283468_0_0_0_Types[] = { &List_1_t1317283468_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1317283468_0_0_0 = { 1, GenInst_List_1_t1317283468_0_0_0_Types };
extern const Il2CppType OfertasValuesCRM_t2856949562_0_0_0;
static const Il2CppType* GenInst_OfertasValuesCRM_t2856949562_0_0_0_Types[] = { &OfertasValuesCRM_t2856949562_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertasValuesCRM_t2856949562_0_0_0 = { 1, GenInst_OfertasValuesCRM_t2856949562_0_0_0_Types };
extern const Il2CppType Texture2D_t3884108195_0_0_0;
static const Il2CppType* GenInst_Texture2D_t3884108195_0_0_0_Types[] = { &Texture2D_t3884108195_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3884108195_0_0_0 = { 1, GenInst_Texture2D_t3884108195_0_0_0_Types };
extern const Il2CppType OfertaCRMData_t637956887_0_0_0;
static const Il2CppType* GenInst_OfertaCRMData_t637956887_0_0_0_Types[] = { &OfertaCRMData_t637956887_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertaCRMData_t637956887_0_0_0 = { 1, GenInst_OfertaCRMData_t637956887_0_0_0_Types };
extern const Il2CppType OfertasCRMDataGroup_t273423897_0_0_0;
static const Il2CppType* GenInst_OfertasCRMDataGroup_t273423897_0_0_0_Types[] = { &OfertasCRMDataGroup_t273423897_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertasCRMDataGroup_t273423897_0_0_0 = { 1, GenInst_OfertasCRMDataGroup_t273423897_0_0_0_Types };
extern const Il2CppType CortesData_t642513606_0_0_0;
static const Il2CppType* GenInst_CortesData_t642513606_0_0_0_Types[] = { &CortesData_t642513606_0_0_0 };
extern const Il2CppGenericInst GenInst_CortesData_t642513606_0_0_0 = { 1, GenInst_CortesData_t642513606_0_0_0_Types };
extern const Il2CppType List_1_t272385497_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t272385497_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t272385497_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t272385497_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t272385497_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t272385497_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t272385497_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t272385497_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t272385497_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t991584573_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t991584573_0_0_0_Types[] = { &KeyValuePair_2_t991584573_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t991584573_0_0_0 = { 1, GenInst_KeyValuePair_2_t991584573_0_0_0_Types };
extern const Il2CppType WWW_t3134621005_0_0_0;
static const Il2CppType* GenInst_WWW_t3134621005_0_0_0_Types[] = { &WWW_t3134621005_0_0_0 };
extern const Il2CppGenericInst GenInst_WWW_t3134621005_0_0_0 = { 1, GenInst_WWW_t3134621005_0_0_0_Types };
extern const Il2CppType OfertaData_t3417615771_0_0_0;
static const Il2CppType* GenInst_OfertaData_t3417615771_0_0_0_Types[] = { &OfertaData_t3417615771_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertaData_t3417615771_0_0_0 = { 1, GenInst_OfertaData_t3417615771_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t595048151_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t595048151_0_0_0_Types[] = { &KeyValuePair_2_t595048151_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t595048151_0_0_0 = { 1, GenInst_KeyValuePair_2_t595048151_0_0_0_Types };
extern const Il2CppType NotificationInstance_t3873938600_0_0_0;
static const Il2CppType* GenInst_NotificationInstance_t3873938600_0_0_0_Types[] = { &NotificationInstance_t3873938600_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationInstance_t3873938600_0_0_0 = { 1, GenInst_NotificationInstance_t3873938600_0_0_0_Types };
extern const Il2CppType ISampleAppUIElement_t2180050874_0_0_0;
static const Il2CppType* GenInst_ISampleAppUIElement_t2180050874_0_0_0_Types[] = { &ISampleAppUIElement_t2180050874_0_0_0 };
extern const Il2CppGenericInst GenInst_ISampleAppUIElement_t2180050874_0_0_0 = { 1, GenInst_ISampleAppUIElement_t2180050874_0_0_0_Types };
extern const Il2CppType SampleAppUIRect_t2784570703_0_0_0;
static const Il2CppType* GenInst_SampleAppUIRect_t2784570703_0_0_0_Types[] = { &SampleAppUIRect_t2784570703_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleAppUIRect_t2784570703_0_0_0 = { 1, GenInst_SampleAppUIRect_t2784570703_0_0_0_Types };
extern const Il2CppType List_1_t4040371092_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t4040371092_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0_Types };
extern const Il2CppType EventHandlerFunction_t2672185540_0_0_0;
static const Il2CppType* GenInst_EventHandlerFunction_t2672185540_0_0_0_Types[] = { &EventHandlerFunction_t2672185540_0_0_0 };
extern const Il2CppGenericInst GenInst_EventHandlerFunction_t2672185540_0_0_0 = { 1, GenInst_EventHandlerFunction_t2672185540_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t4040371092_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3936415037_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3936415037_0_0_0_Types[] = { &KeyValuePair_2_t3936415037_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3936415037_0_0_0 = { 1, GenInst_KeyValuePair_2_t3936415037_0_0_0_Types };
extern const Il2CppType List_1_t1807153374_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t1807153374_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0_Types };
extern const Il2CppType DataEventHandlerFunction_t438967822_0_0_0;
static const Il2CppType* GenInst_DataEventHandlerFunction_t438967822_0_0_0_Types[] = { &DataEventHandlerFunction_t438967822_0_0_0 };
extern const Il2CppGenericInst GenInst_DataEventHandlerFunction_t438967822_0_0_0 = { 1, GenInst_DataEventHandlerFunction_t438967822_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t1807153374_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1703197319_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1703197319_0_0_0_Types[] = { &KeyValuePair_2_t1703197319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1703197319_0_0_0 = { 1, GenInst_KeyValuePair_2_t1703197319_0_0_0_Types };
extern const Il2CppType MNDialogResult_t3125317574_0_0_0;
static const Il2CppType* GenInst_MNDialogResult_t3125317574_0_0_0_Types[] = { &MNDialogResult_t3125317574_0_0_0 };
extern const Il2CppGenericInst GenInst_MNDialogResult_t3125317574_0_0_0 = { 1, GenInst_MNDialogResult_t3125317574_0_0_0_Types };
extern const Il2CppType IAccessTokenRefreshResult_t760062154_0_0_0;
static const Il2CppType* GenInst_IAccessTokenRefreshResult_t760062154_0_0_0_Types[] = { &IAccessTokenRefreshResult_t760062154_0_0_0 };
extern const Il2CppGenericInst GenInst_IAccessTokenRefreshResult_t760062154_0_0_0 = { 1, GenInst_IAccessTokenRefreshResult_t760062154_0_0_0_Types };
extern const Il2CppType IAppInviteResult_t1487399198_0_0_0;
static const Il2CppType* GenInst_IAppInviteResult_t1487399198_0_0_0_Types[] = { &IAppInviteResult_t1487399198_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppInviteResult_t1487399198_0_0_0 = { 1, GenInst_IAppInviteResult_t1487399198_0_0_0_Types };
extern const Il2CppType IAppLinkResult_t3864497039_0_0_0;
static const Il2CppType* GenInst_IAppLinkResult_t3864497039_0_0_0_Types[] = { &IAppLinkResult_t3864497039_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppLinkResult_t3864497039_0_0_0 = { 1, GenInst_IAppLinkResult_t3864497039_0_0_0_Types };
extern const Il2CppType OGActionType_t3473630824_0_0_0;
static const Il2CppType* GenInst_OGActionType_t3473630824_0_0_0_Types[] = { &OGActionType_t3473630824_0_0_0 };
extern const Il2CppGenericInst GenInst_OGActionType_t3473630824_0_0_0 = { 1, GenInst_OGActionType_t3473630824_0_0_0_Types };
extern const Il2CppType IAppRequestResult_t2931577298_0_0_0;
static const Il2CppType* GenInst_IAppRequestResult_t2931577298_0_0_0_Types[] = { &IAppRequestResult_t2931577298_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppRequestResult_t2931577298_0_0_0 = { 1, GenInst_IAppRequestResult_t2931577298_0_0_0_Types };
extern const Il2CppType IShareResult_t3573065907_0_0_0;
static const Il2CppType* GenInst_IShareResult_t3573065907_0_0_0_Types[] = { &IShareResult_t3573065907_0_0_0 };
extern const Il2CppGenericInst GenInst_IShareResult_t3573065907_0_0_0 = { 1, GenInst_IShareResult_t3573065907_0_0_0_Types };
extern const Il2CppType IGroupCreateResult_t1004608047_0_0_0;
static const Il2CppType* GenInst_IGroupCreateResult_t1004608047_0_0_0_Types[] = { &IGroupCreateResult_t1004608047_0_0_0 };
extern const Il2CppGenericInst GenInst_IGroupCreateResult_t1004608047_0_0_0 = { 1, GenInst_IGroupCreateResult_t1004608047_0_0_0_Types };
extern const Il2CppType IGraphResult_t873401058_0_0_0;
static const Il2CppType* GenInst_IGraphResult_t873401058_0_0_0_Types[] = { &IGraphResult_t873401058_0_0_0 };
extern const Il2CppGenericInst GenInst_IGraphResult_t873401058_0_0_0 = { 1, GenInst_IGraphResult_t873401058_0_0_0_Types };
extern const Il2CppType IGroupJoinResult_t2814186909_0_0_0;
static const Il2CppType* GenInst_IGroupJoinResult_t2814186909_0_0_0_Types[] = { &IGroupJoinResult_t2814186909_0_0_0 };
extern const Il2CppGenericInst GenInst_IGroupJoinResult_t2814186909_0_0_0 = { 1, GenInst_IGroupJoinResult_t2814186909_0_0_0_Types };
extern const Il2CppType ILoginResult_t528611517_0_0_0;
static const Il2CppType* GenInst_ILoginResult_t528611517_0_0_0_Types[] = { &ILoginResult_t528611517_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoginResult_t528611517_0_0_0 = { 1, GenInst_ILoginResult_t528611517_0_0_0_Types };
extern const Il2CppType IPayResult_t2622336188_0_0_0;
static const Il2CppType* GenInst_IPayResult_t2622336188_0_0_0_Types[] = { &IPayResult_t2622336188_0_0_0 };
extern const Il2CppGenericInst GenInst_IPayResult_t2622336188_0_0_0 = { 1, GenInst_IPayResult_t2622336188_0_0_0_Types };
extern const Il2CppType FacebookUnityPlatform_t1192368832_0_0_0;
static const Il2CppType* GenInst_FacebookUnityPlatform_t1192368832_0_0_0_Types[] = { &FacebookUnityPlatform_t1192368832_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookUnityPlatform_t1192368832_0_0_0 = { 1, GenInst_FacebookUnityPlatform_t1192368832_0_0_0_Types };
extern const Il2CppType UrlSchemes_t3482710948_0_0_0;
static const Il2CppType* GenInst_UrlSchemes_t3482710948_0_0_0_Types[] = { &UrlSchemes_t3482710948_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlSchemes_t3482710948_0_0_0 = { 1, GenInst_UrlSchemes_t3482710948_0_0_0_Types };
extern const Il2CppType IResult_t2984644868_0_0_0;
static const Il2CppType* GenInst_IResult_t2984644868_0_0_0_Types[] = { &IResult_t2984644868_0_0_0 };
extern const Il2CppGenericInst GenInst_IResult_t2984644868_0_0_0 = { 1, GenInst_IResult_t2984644868_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_String_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType Slider_t79469677_0_0_0;
static const Il2CppType* GenInst_Slider_t79469677_0_0_0_Types[] = { &Slider_t79469677_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t79469677_0_0_0 = { 1, GenInst_Slider_t79469677_0_0_0_Types };
static const Il2CppType* GenInst_DirectoryInfo_t89154617_0_0_0_String_t_0_0_0_Types[] = { &DirectoryInfo_t89154617_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_DirectoryInfo_t89154617_0_0_0_String_t_0_0_0 = { 2, GenInst_DirectoryInfo_t89154617_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType StackElement_t2425423099_0_0_0;
static const Il2CppType* GenInst_StackElement_t2425423099_0_0_0_Types[] = { &StackElement_t2425423099_0_0_0 };
extern const Il2CppGenericInst GenInst_StackElement_t2425423099_0_0_0 = { 1, GenInst_StackElement_t2425423099_0_0_0_Types };
extern const Il2CppType TarEntry_t762185187_0_0_0;
static const Il2CppType* GenInst_TarEntry_t762185187_0_0_0_Types[] = { &TarEntry_t762185187_0_0_0 };
extern const Il2CppGenericInst GenInst_TarEntry_t762185187_0_0_0 = { 1, GenInst_TarEntry_t762185187_0_0_0_Types };
extern const Il2CppType ZipEntry_t3141689087_0_0_0;
static const Il2CppType* GenInst_ZipEntry_t3141689087_0_0_0_Types[] = { &ZipEntry_t3141689087_0_0_0 };
extern const Il2CppGenericInst GenInst_ZipEntry_t3141689087_0_0_0 = { 1, GenInst_ZipEntry_t3141689087_0_0_0_Types };
extern const Il2CppType GeneralComponentsAbstract_t3900398046_0_0_0;
static const Il2CppType* GenInst_GeneralComponentsAbstract_t3900398046_0_0_0_Types[] = { &GeneralComponentsAbstract_t3900398046_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneralComponentsAbstract_t3900398046_0_0_0 = { 1, GenInst_GeneralComponentsAbstract_t3900398046_0_0_0_Types };
extern const Il2CppType GeneralComponentInterface_t2984273286_0_0_0;
static const Il2CppType* GenInst_GeneralComponentInterface_t2984273286_0_0_0_Types[] = { &GeneralComponentInterface_t2984273286_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneralComponentInterface_t2984273286_0_0_0 = { 1, GenInst_GeneralComponentInterface_t2984273286_0_0_0_Types };
extern const Il2CppType InAppAbstract_t382673128_0_0_0;
static const Il2CppType* GenInst_InAppAbstract_t382673128_0_0_0_Types[] = { &InAppAbstract_t382673128_0_0_0 };
extern const Il2CppGenericInst GenInst_InAppAbstract_t382673128_0_0_0 = { 1, GenInst_InAppAbstract_t382673128_0_0_0_Types };
extern const Il2CppType ProgressEventsAbstract_t2129719228_0_0_0;
static const Il2CppType* GenInst_ProgressEventsAbstract_t2129719228_0_0_0_Types[] = { &ProgressEventsAbstract_t2129719228_0_0_0 };
extern const Il2CppGenericInst GenInst_ProgressEventsAbstract_t2129719228_0_0_0 = { 1, GenInst_ProgressEventsAbstract_t2129719228_0_0_0_Types };
extern const Il2CppType GenericStartableComponentControllAbstract_t1794600275_0_0_0;
static const Il2CppType* GenInst_GenericStartableComponentControllAbstract_t1794600275_0_0_0_Types[] = { &GenericStartableComponentControllAbstract_t1794600275_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericStartableComponentControllAbstract_t1794600275_0_0_0 = { 1, GenInst_GenericStartableComponentControllAbstract_t1794600275_0_0_0_Types };
extern const Il2CppType GenericTimeControllInterface_t4025522376_0_0_0;
static const Il2CppType* GenInst_GenericTimeControllInterface_t4025522376_0_0_0_Types[] = { &GenericTimeControllInterface_t4025522376_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTimeControllInterface_t4025522376_0_0_0 = { 1, GenInst_GenericTimeControllInterface_t4025522376_0_0_0_Types };
extern const Il2CppType NetworkReachability_t612403035_0_0_0;
extern const Il2CppType Action_t3771233898_0_0_0;
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Action_t3771233898_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0 = { 2, GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3830311593_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3830311593_0_0_0_Types[] = { &KeyValuePair_2_t3830311593_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3830311593_0_0_0 = { 1, GenInst_KeyValuePair_2_t3830311593_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0 = { 1, GenInst_NetworkReachability_t612403035_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_NetworkReachability_t612403035_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Il2CppObject_0_0_0, &NetworkReachability_t612403035_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_NetworkReachability_t612403035_0_0_0 = { 3, GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_NetworkReachability_t612403035_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3830311593_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3830311593_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3830311593_0_0_0 = { 3, GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3830311593_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &Action_t3771233898_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3430729120_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3430729120_0_0_0_Types[] = { &KeyValuePair_2_t3430729120_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3430729120_0_0_0 = { 1, GenInst_KeyValuePair_2_t3430729120_0_0_0_Types };
extern const Il2CppType Quaternion_t1553702882_0_0_0;
static const Il2CppType* GenInst_Quaternion_t1553702882_0_0_0_Types[] = { &Quaternion_t1553702882_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t1553702882_0_0_0 = { 1, GenInst_Quaternion_t1553702882_0_0_0_Types };
extern const Il2CppType TransformPositionAngleFilterAbstract_t4158746314_0_0_0;
static const Il2CppType* GenInst_TransformPositionAngleFilterAbstract_t4158746314_0_0_0_Types[] = { &TransformPositionAngleFilterAbstract_t4158746314_0_0_0 };
extern const Il2CppGenericInst GenInst_TransformPositionAngleFilterAbstract_t4158746314_0_0_0 = { 1, GenInst_TransformPositionAngleFilterAbstract_t4158746314_0_0_0_Types };
extern const Il2CppType TransformPositionAngleFilterInterface_t2619784369_0_0_0;
static const Il2CppType* GenInst_TransformPositionAngleFilterInterface_t2619784369_0_0_0_Types[] = { &TransformPositionAngleFilterInterface_t2619784369_0_0_0 };
extern const Il2CppGenericInst GenInst_TransformPositionAngleFilterInterface_t2619784369_0_0_0 = { 1, GenInst_TransformPositionAngleFilterInterface_t2619784369_0_0_0_Types };
extern const Il2CppType OnOffListenerEventInterface_t3960143011_0_0_0;
static const Il2CppType* GenInst_OnOffListenerEventInterface_t3960143011_0_0_0_Types[] = { &OnOffListenerEventInterface_t3960143011_0_0_0 };
extern const Il2CppGenericInst GenInst_OnOffListenerEventInterface_t3960143011_0_0_0 = { 1, GenInst_OnOffListenerEventInterface_t3960143011_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &GameObject_t3674682005_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &GameObject_t3674682005_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3570725950_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3570725950_0_0_0_Types[] = { &KeyValuePair_2_t3570725950_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3570725950_0_0_0 = { 1, GenInst_KeyValuePair_2_t3570725950_0_0_0_Types };
extern const Il2CppType DataRow_t3107836336_0_0_0;
static const Il2CppType* GenInst_DataRow_t3107836336_0_0_0_Types[] = { &DataRow_t3107836336_0_0_0 };
extern const Il2CppGenericInst GenInst_DataRow_t3107836336_0_0_0 = { 1, GenInst_DataRow_t3107836336_0_0_0_Types };
extern const Il2CppType BundleVO_t1984518073_0_0_0;
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_String_t_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_String_t_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType MetadataVO_t2511256998_0_0_0;
static const Il2CppType* GenInst_MetadataVO_t2511256998_0_0_0_String_t_0_0_0_Types[] = { &MetadataVO_t2511256998_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataVO_t2511256998_0_0_0_String_t_0_0_0 = { 2, GenInst_MetadataVO_t2511256998_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3308144514_0_0_0;
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t3308144514_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &IEnumerable_1_t3308144514_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t3308144514_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t3308144514_0_0_0_Types };
extern const Il2CppType FileVO_t1935348659_0_0_0;
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_String_t_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0_String_t_0_0_0 = { 2, GenInst_FileVO_t1935348659_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t3308144514_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0, &IEnumerable_1_t3308144514_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t3308144514_0_0_0 = { 2, GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t3308144514_0_0_0_Types };
extern const Il2CppType UrlInfoVO_t1761987528_0_0_0;
static const Il2CppType* GenInst_UrlInfoVO_t1761987528_0_0_0_String_t_0_0_0_Types[] = { &UrlInfoVO_t1761987528_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlInfoVO_t1761987528_0_0_0_String_t_0_0_0 = { 2, GenInst_UrlInfoVO_t1761987528_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0 = { 1, GenInst_BundleVO_t1984518073_0_0_0_Types };
extern const Il2CppType VOWithId_t2461972856_0_0_0;
static const Il2CppType* GenInst_VOWithId_t2461972856_0_0_0_Types[] = { &VOWithId_t2461972856_0_0_0 };
extern const Il2CppGenericInst GenInst_VOWithId_t2461972856_0_0_0 = { 1, GenInst_VOWithId_t2461972856_0_0_0_Types };
static const Il2CppType* GenInst_MetadataVO_t2511256998_0_0_0_Types[] = { &MetadataVO_t2511256998_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataVO_t2511256998_0_0_0 = { 1, GenInst_MetadataVO_t2511256998_0_0_0_Types };
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0 = { 1, GenInst_FileVO_t1935348659_0_0_0_Types };
static const Il2CppType* GenInst_UrlInfoVO_t1761987528_0_0_0_Types[] = { &UrlInfoVO_t1761987528_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlInfoVO_t1761987528_0_0_0 = { 1, GenInst_UrlInfoVO_t1761987528_0_0_0_Types };
extern const Il2CppType KeyValueVO_t1780100105_0_0_0;
static const Il2CppType* GenInst_KeyValueVO_t1780100105_0_0_0_Types[] = { &KeyValueVO_t1780100105_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValueVO_t1780100105_0_0_0 = { 1, GenInst_KeyValueVO_t1780100105_0_0_0_Types };
extern const Il2CppType ConfigurableListenerEventInterface_t1985629260_0_0_0;
static const Il2CppType* GenInst_ConfigurableListenerEventInterface_t1985629260_0_0_0_Types[] = { &ConfigurableListenerEventInterface_t1985629260_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurableListenerEventInterface_t1985629260_0_0_0 = { 1, GenInst_ConfigurableListenerEventInterface_t1985629260_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0_Types[] = { &String_t_0_0_0, &ConfigurableListenerEventInterface_t1985629260_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0 = { 2, GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &ConfigurableListenerEventInterface_t1985629260_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2704828336_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2704828336_0_0_0_Types[] = { &KeyValuePair_2_t2704828336_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2704828336_0_0_0 = { 1, GenInst_KeyValuePair_2_t2704828336_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0_Types[] = { &String_t_0_0_0, &OnOffListenerEventInterface_t3960143011_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0 = { 2, GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &OnOffListenerEventInterface_t3960143011_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t384374791_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t384374791_0_0_0_Types[] = { &KeyValuePair_2_t384374791_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t384374791_0_0_0 = { 1, GenInst_KeyValuePair_2_t384374791_0_0_0_Types };
static const Il2CppType* GenInst_FileInfo_t3233670074_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &FileInfo_t3233670074_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_FileInfo_t3233670074_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_FileInfo_t3233670074_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType VideoPlaybackBehaviour_t2848361927_0_0_0;
static const Il2CppType* GenInst_VideoPlaybackBehaviour_t2848361927_0_0_0_Types[] = { &VideoPlaybackBehaviour_t2848361927_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoPlaybackBehaviour_t2848361927_0_0_0 = { 1, GenInst_VideoPlaybackBehaviour_t2848361927_0_0_0_Types };
extern const Il2CppType BundleDownloadItem_t715938369_0_0_0;
static const Il2CppType* GenInst_BundleDownloadItem_t715938369_0_0_0_Types[] = { &BundleDownloadItem_t715938369_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleDownloadItem_t715938369_0_0_0 = { 1, GenInst_BundleDownloadItem_t715938369_0_0_0_Types };
extern const Il2CppType RequestImagesItemVO_t2061191909_0_0_0;
static const Il2CppType* GenInst_RequestImagesItemVO_t2061191909_0_0_0_Types[] = { &RequestImagesItemVO_t2061191909_0_0_0 };
extern const Il2CppGenericInst GenInst_RequestImagesItemVO_t2061191909_0_0_0 = { 1, GenInst_RequestImagesItemVO_t2061191909_0_0_0_Types };
extern const Il2CppType List_1_t3352703625_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3352703625_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3352703625_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4071902701_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4071902701_0_0_0_Types[] = { &KeyValuePair_2_t4071902701_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4071902701_0_0_0 = { 1, GenInst_KeyValuePair_2_t4071902701_0_0_0_Types };
extern const Il2CppType BundleVOU5BU5D_t2162892100_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0_Types[] = { &String_t_0_0_0, &BundleVOU5BU5D_t2162892100_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0 = { 2, GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &BundleVOU5BU5D_t2162892100_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2882091176_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2882091176_0_0_0_Types[] = { &KeyValuePair_2_t2882091176_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2882091176_0_0_0 = { 1, GenInst_KeyValuePair_2_t2882091176_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1517202659_0_0_0;
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t1517202659_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &IEnumerable_1_t1517202659_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t1517202659_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t1517202659_0_0_0_Types };
static const Il2CppType* GenInst_MetadataVO_t2511256998_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &MetadataVO_t2511256998_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataVO_t2511256998_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_MetadataVO_t2511256998_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_BundleVO_t1984518073_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &BundleVO_t1984518073_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_BundleVO_t1984518073_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_BundleVO_t1984518073_0_0_0_Types };
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_FileVO_t1935348659_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_FileVO_t1935348659_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0, &FileVO_t1935348659_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0_FileVO_t1935348659_0_0_0 = { 2, GenInst_FileVO_t1935348659_0_0_0_FileVO_t1935348659_0_0_0_Types };
static const Il2CppType* GenInst_UrlInfoVO_t1761987528_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &UrlInfoVO_t1761987528_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlInfoVO_t1761987528_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_UrlInfoVO_t1761987528_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_UrlInfoVO_t1761987528_0_0_0_UrlInfoVO_t1761987528_0_0_0_Types[] = { &UrlInfoVO_t1761987528_0_0_0, &UrlInfoVO_t1761987528_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlInfoVO_t1761987528_0_0_0_UrlInfoVO_t1761987528_0_0_0 = { 2, GenInst_UrlInfoVO_t1761987528_0_0_0_UrlInfoVO_t1761987528_0_0_0_Types };
static const Il2CppType* GenInst_MetadataVO_t2511256998_0_0_0_MetadataVO_t2511256998_0_0_0_Types[] = { &MetadataVO_t2511256998_0_0_0, &MetadataVO_t2511256998_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataVO_t2511256998_0_0_0_MetadataVO_t2511256998_0_0_0 = { 2, GenInst_MetadataVO_t2511256998_0_0_0_MetadataVO_t2511256998_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t767933189_0_0_0;
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t767933189_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &IEnumerable_1_t767933189_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t767933189_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t767933189_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t941294320_0_0_0;
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t941294320_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &IEnumerable_1_t941294320_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t941294320_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t941294320_0_0_0_Types };
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t767933189_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0, &IEnumerable_1_t767933189_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t767933189_0_0_0 = { 2, GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t767933189_0_0_0_Types };
extern const Il2CppType InAppAnalytics_t2316734034_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0_Types[] = { &String_t_0_0_0, &InAppAnalytics_t2316734034_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0 = { 2, GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &InAppAnalytics_t2316734034_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3035933110_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3035933110_0_0_0_Types[] = { &KeyValuePair_2_t3035933110_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3035933110_0_0_0 = { 1, GenInst_KeyValuePair_2_t3035933110_0_0_0_Types };
extern const Il2CppType InAppEventsChannel_t1397713486_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0_Types[] = { &String_t_0_0_0, &InAppEventsChannel_t1397713486_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0 = { 2, GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &InAppEventsChannel_t1397713486_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2116912562_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2116912562_0_0_0_Types[] = { &KeyValuePair_2_t2116912562_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2116912562_0_0_0 = { 1, GenInst_KeyValuePair_2_t2116912562_0_0_0_Types };
extern const Il2CppType Perspective_t644553802_0_0_0;
extern const Il2CppType OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0;
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0 = { 2, GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2409664798_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2409664798_0_0_0_Types[] = { &KeyValuePair_2_t2409664798_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2409664798_0_0_0 = { 1, GenInst_KeyValuePair_2_t2409664798_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Types[] = { &Perspective_t644553802_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0 = { 1, GenInst_Perspective_t644553802_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Perspective_t644553802_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &Il2CppObject_0_0_0, &Perspective_t644553802_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Perspective_t644553802_0_0_0 = { 3, GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Perspective_t644553802_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2409664798_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2409664798_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2409664798_0_0_0 = { 3, GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2409664798_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t941084846_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t941084846_0_0_0_Types[] = { &KeyValuePair_2_t941084846_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t941084846_0_0_0 = { 1, GenInst_KeyValuePair_2_t941084846_0_0_0_Types };
extern const Il2CppType StateMachine_t367995870_0_0_0;
extern const Il2CppType OnChangeToAnStateEventHandler_t630243546_0_0_0;
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &OnChangeToAnStateEventHandler_t630243546_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0 = { 2, GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1845333178_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1845333178_0_0_0_Types[] = { &KeyValuePair_2_t1845333178_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1845333178_0_0_0 = { 1, GenInst_KeyValuePair_2_t1845333178_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0 = { 1, GenInst_StateMachine_t367995870_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_StateMachine_t367995870_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &Il2CppObject_0_0_0, &StateMachine_t367995870_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_StateMachine_t367995870_0_0_0 = { 3, GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_StateMachine_t367995870_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1845333178_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1845333178_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1845333178_0_0_0 = { 3, GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1845333178_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &OnChangeToAnStateEventHandler_t630243546_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2599727649_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2599727649_0_0_0_Types[] = { &KeyValuePair_2_t2599727649_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2599727649_0_0_0 = { 1, GenInst_KeyValuePair_2_t2599727649_0_0_0_Types };
extern const Il2CppType GarbageItemVO_t1335202303_0_0_0;
static const Il2CppType* GenInst_GarbageItemVO_t1335202303_0_0_0_Types[] = { &GarbageItemVO_t1335202303_0_0_0 };
extern const Il2CppGenericInst GenInst_GarbageItemVO_t1335202303_0_0_0 = { 1, GenInst_GarbageItemVO_t1335202303_0_0_0_Types };
extern const Il2CppType AssetBundle_t2070959688_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0_Types[] = { &String_t_0_0_0, &AssetBundle_t2070959688_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0 = { 2, GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &AssetBundle_t2070959688_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2790158764_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2790158764_0_0_0_Types[] = { &KeyValuePair_2_t2790158764_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2790158764_0_0_0 = { 1, GenInst_KeyValuePair_2_t2790158764_0_0_0_Types };
extern const Il2CppType EventNames_t1614635846_0_0_0;
extern const Il2CppType InAppEventDispatcherOnOff_t3474551315_0_0_0;
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &InAppEventDispatcherOnOff_t3474551315_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0 = { 2, GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4186358450_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4186358450_0_0_0_Types[] = { &KeyValuePair_2_t4186358450_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4186358450_0_0_0 = { 1, GenInst_KeyValuePair_2_t4186358450_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0 = { 1, GenInst_EventNames_t1614635846_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_EventNames_t1614635846_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &Il2CppObject_0_0_0, &EventNames_t1614635846_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_EventNames_t1614635846_0_0_0 = { 3, GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_EventNames_t1614635846_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4186358450_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4186358450_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4186358450_0_0_0 = { 3, GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4186358450_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &InAppEventDispatcherOnOff_t3474551315_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3490093394_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3490093394_0_0_0_Types[] = { &KeyValuePair_2_t3490093394_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3490093394_0_0_0 = { 1, GenInst_KeyValuePair_2_t3490093394_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_Types[] = { &String_t_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2065771578_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2065771578_0_0_0_Types[] = { &KeyValuePair_2_t2065771578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2065771578_0_0_0 = { 1, GenInst_KeyValuePair_2_t2065771578_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &KeyValuePair_2_t2065771578_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Single_t4291918972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t716150752_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t716150752_0_0_0_Types[] = { &KeyValuePair_2_t716150752_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t716150752_0_0_0 = { 1, GenInst_KeyValuePair_2_t716150752_0_0_0_Types };
extern const Il2CppType VoidEventDispatcher_t1760461203_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0_Types[] = { &String_t_0_0_0, &VoidEventDispatcher_t1760461203_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0 = { 2, GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &VoidEventDispatcher_t1760461203_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2479660279_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2479660279_0_0_0_Types[] = { &KeyValuePair_2_t2479660279_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2479660279_0_0_0 = { 1, GenInst_KeyValuePair_2_t2479660279_0_0_0_Types };
extern const Il2CppType GenericParameterEventDispatcher_t3811317845_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0_Types[] = { &String_t_0_0_0, &GenericParameterEventDispatcher_t3811317845_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0 = { 2, GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &GenericParameterEventDispatcher_t3811317845_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t235549625_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t235549625_0_0_0_Types[] = { &KeyValuePair_2_t235549625_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t235549625_0_0_0 = { 1, GenInst_KeyValuePair_2_t235549625_0_0_0_Types };
extern const Il2CppType GroupInAppAbstractComponentsManager_t739334794_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0_Types[] = { &String_t_0_0_0, &GroupInAppAbstractComponentsManager_t739334794_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0 = { 2, GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &GroupInAppAbstractComponentsManager_t739334794_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1458533870_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1458533870_0_0_0_Types[] = { &KeyValuePair_2_t1458533870_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1458533870_0_0_0 = { 1, GenInst_KeyValuePair_2_t1458533870_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0_Types[] = { &String_t_0_0_0, &InAppAbstract_t382673128_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0 = { 2, GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &InAppAbstract_t382673128_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1101872204_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1101872204_0_0_0_Types[] = { &KeyValuePair_2_t1101872204_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1101872204_0_0_0 = { 1, GenInst_KeyValuePair_2_t1101872204_0_0_0_Types };
extern const Il2CppType OnInAppCommonEventHandler_t1653412278_0_0_0;
extern const Il2CppType OnInAppProgressEventHandler_t3139301368_0_0_0;
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_OnInAppCommonEventHandler_t1653412278_0_0_0_OnInAppProgressEventHandler_t3139301368_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &OnInAppCommonEventHandler_t1653412278_0_0_0, &OnInAppProgressEventHandler_t3139301368_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_OnInAppCommonEventHandler_t1653412278_0_0_0_OnInAppProgressEventHandler_t3139301368_0_0_0 = { 3, GenInst_BundleVO_t1984518073_0_0_0_OnInAppCommonEventHandler_t1653412278_0_0_0_OnInAppProgressEventHandler_t3139301368_0_0_0_Types };
extern const Il2CppType JsonData_t1715015430_0_0_0;
static const Il2CppType* GenInst_JsonData_t1715015430_0_0_0_Types[] = { &JsonData_t1715015430_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonData_t1715015430_0_0_0 = { 1, GenInst_JsonData_t1715015430_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t1715015430_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2434214506_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2434214506_0_0_0_Types[] = { &KeyValuePair_2_t2434214506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2434214506_0_0_0 = { 1, GenInst_KeyValuePair_2_t2434214506_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t1715015430_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType PropertyMetadata_t4066634616_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t4066634616_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0 = { 2, GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t4066634616_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1840487222_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1840487222_0_0_0_Types[] = { &KeyValuePair_2_t1840487222_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1840487222_0_0_0 = { 1, GenInst_KeyValuePair_2_t1840487222_0_0_0_Types };
extern const Il2CppType ExporterFunc_t3330360473_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t3330360473_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0 = { 2, GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1821615648_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t1821615648_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0_Types };
extern const Il2CppType ImporterFunc_t2138319818_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2138319818_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0 = { 2, GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0_Types };
extern const Il2CppType ArrayMetadata_t4058342910_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t4058342910_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0 = { 2, GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t4058342910_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1832195516_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1832195516_0_0_0_Types[] = { &KeyValuePair_2_t1832195516_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1832195516_0_0_0 = { 1, GenInst_KeyValuePair_2_t1832195516_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2031895_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2031895_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types };
extern const Il2CppType ObjectMetadata_t2009294498_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t2009294498_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0 = { 2, GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t2009294498_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4078114400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4078114400_0_0_0_Types[] = { &KeyValuePair_2_t4078114400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4078114400_0_0_0 = { 1, GenInst_KeyValuePair_2_t4078114400_0_0_0_Types };
extern const Il2CppType IList_1_t2466314523_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t2466314523_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0 = { 2, GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t4066634616_0_0_0_Types[] = { &PropertyMetadata_t4066634616_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t4066634616_0_0_0 = { 1, GenInst_PropertyMetadata_t4066634616_0_0_0_Types };
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &PropertyInfo_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &FieldInfo_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t4058342910_0_0_0_Types[] = { &ArrayMetadata_t4058342910_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t4058342910_0_0_0 = { 1, GenInst_ArrayMetadata_t4058342910_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t4058342910_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t4058342910_0_0_0, &ArrayMetadata_t4058342910_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t4058342910_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_KeyValuePair_2_t1832195516_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t4058342910_0_0_0, &KeyValuePair_2_t1832195516_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_KeyValuePair_2_t1832195516_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_KeyValuePair_2_t1832195516_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t4058342910_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4062546101_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4062546101_0_0_0_Types[] = { &KeyValuePair_2_t4062546101_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4062546101_0_0_0 = { 1, GenInst_KeyValuePair_2_t4062546101_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2031895_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t6235086_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t6235086_0_0_0_Types[] = { &KeyValuePair_2_t6235086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t6235086_0_0_0 = { 1, GenInst_KeyValuePair_2_t6235086_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t2009294498_0_0_0_Types[] = { &ObjectMetadata_t2009294498_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t2009294498_0_0_0 = { 1, GenInst_ObjectMetadata_t2009294498_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t2009294498_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t2009294498_0_0_0, &ObjectMetadata_t2009294498_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t2009294498_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_KeyValuePair_2_t4078114400_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t2009294498_0_0_0, &KeyValuePair_2_t4078114400_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_KeyValuePair_2_t4078114400_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_KeyValuePair_2_t4078114400_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t2009294498_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2013497689_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2013497689_0_0_0_Types[] = { &KeyValuePair_2_t2013497689_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2013497689_0_0_0 = { 1, GenInst_KeyValuePair_2_t2013497689_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t2466314523_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2470517714_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2470517714_0_0_0_Types[] = { &KeyValuePair_2_t2470517714_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2470517714_0_0_0 = { 1, GenInst_KeyValuePair_2_t2470517714_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t3330360473_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3334563664_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3334563664_0_0_0_Types[] = { &KeyValuePair_2_t3334563664_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3334563664_0_0_0 = { 1, GenInst_KeyValuePair_2_t3334563664_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t1821615648_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1825818839_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1825818839_0_0_0_Types[] = { &KeyValuePair_2_t1825818839_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1825818839_0_0_0 = { 1, GenInst_KeyValuePair_2_t1825818839_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t4066634616_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t4066634616_0_0_0, &PropertyMetadata_t4066634616_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t4066634616_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_KeyValuePair_2_t1840487222_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t4066634616_0_0_0, &KeyValuePair_2_t1840487222_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_KeyValuePair_2_t1840487222_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_KeyValuePair_2_t1840487222_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t4066634616_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t490866396_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t490866396_0_0_0_Types[] = { &KeyValuePair_2_t490866396_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t490866396_0_0_0 = { 1, GenInst_KeyValuePair_2_t490866396_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t322939256_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t322939256_0_0_0_Types[] = { &KeyValuePair_2_t322939256_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322939256_0_0_0 = { 1, GenInst_KeyValuePair_2_t322939256_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2138319818_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2142523009_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2142523009_0_0_0_Types[] = { &KeyValuePair_2_t2142523009_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2142523009_0_0_0 = { 1, GenInst_KeyValuePair_2_t2142523009_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2805984405_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &IDictionary_2_t2805984405_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32U5BU5D_t3230847821_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &IDictionary_2_t2805984405_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2702028350_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2702028350_0_0_0_Types[] = { &KeyValuePair_2_t2702028350_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2702028350_0_0_0 = { 1, GenInst_KeyValuePair_2_t2702028350_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32U5BU5D_t3230847821_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3126891766_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3126891766_0_0_0_Types[] = { &KeyValuePair_2_t3126891766_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3126891766_0_0_0 = { 1, GenInst_KeyValuePair_2_t3126891766_0_0_0_Types };
extern const Il2CppType WriterContext_t3060158226_0_0_0;
static const Il2CppType* GenInst_WriterContext_t3060158226_0_0_0_Types[] = { &WriterContext_t3060158226_0_0_0 };
extern const Il2CppGenericInst GenInst_WriterContext_t3060158226_0_0_0 = { 1, GenInst_WriterContext_t3060158226_0_0_0_Types };
extern const Il2CppType StateHandler_t3261942315_0_0_0;
static const Il2CppType* GenInst_StateHandler_t3261942315_0_0_0_Types[] = { &StateHandler_t3261942315_0_0_0 };
extern const Il2CppGenericInst GenInst_StateHandler_t3261942315_0_0_0 = { 1, GenInst_StateHandler_t3261942315_0_0_0_Types };
extern const Il2CppType MulticastDelegate_t3389745971_0_0_0;
static const Il2CppType* GenInst_MulticastDelegate_t3389745971_0_0_0_Types[] = { &MulticastDelegate_t3389745971_0_0_0 };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t3389745971_0_0_0 = { 1, GenInst_MulticastDelegate_t3389745971_0_0_0_Types };
extern const Il2CppType ObjectTracker_t455954211_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t455954211_0_0_0_Types[] = { &ObjectTracker_t455954211_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t455954211_0_0_0 = { 1, GenInst_ObjectTracker_t455954211_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t1735871187_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t1735871187_0_0_0_Types[] = { &ImageTargetBehaviour_t1735871187_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t1735871187_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t1735871187_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t2347335889_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t2347335889_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0_Types };
extern const Il2CppType IEditorImageTargetBehaviour_t4106486993_0_0_0;
static const Il2CppType* GenInst_IEditorImageTargetBehaviour_t4106486993_0_0_0_Types[] = { &IEditorImageTargetBehaviour_t4106486993_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorImageTargetBehaviour_t4106486993_0_0_0 = { 1, GenInst_IEditorImageTargetBehaviour_t4106486993_0_0_0_Types };
extern const Il2CppType ARMRequestVO_t2431191322_0_0_0;
extern const Il2CppType OnRequestEventHandler_t153308946_0_0_0;
extern const Il2CppType OnProgressEventHandler_t1956336554_0_0_0;
static const Il2CppType* GenInst_ARMRequestVO_t2431191322_0_0_0_OnRequestEventHandler_t153308946_0_0_0_OnProgressEventHandler_t1956336554_0_0_0_Types[] = { &ARMRequestVO_t2431191322_0_0_0, &OnRequestEventHandler_t153308946_0_0_0, &OnProgressEventHandler_t1956336554_0_0_0 };
extern const Il2CppGenericInst GenInst_ARMRequestVO_t2431191322_0_0_0_OnRequestEventHandler_t153308946_0_0_0_OnProgressEventHandler_t1956336554_0_0_0 = { 3, GenInst_ARMRequestVO_t2431191322_0_0_0_OnRequestEventHandler_t153308946_0_0_0_OnProgressEventHandler_t1956336554_0_0_0_Types };
extern const Il2CppType KeyTypeValue_t654867638_0_0_0;
static const Il2CppType* GenInst_KeyTypeValue_t654867638_0_0_0_Types[] = { &KeyTypeValue_t654867638_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyTypeValue_t654867638_0_0_0 = { 1, GenInst_KeyTypeValue_t654867638_0_0_0_Types };
extern const Il2CppType ParticleSystem_t381473177_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t381473177_0_0_0_Types[] = { &ParticleSystem_t381473177_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t381473177_0_0_0 = { 1, GenInst_ParticleSystem_t381473177_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &FsmState_t2146334067_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &FsmState_t2146334067_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2042378012_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2042378012_0_0_0_Types[] = { &KeyValuePair_2_t2042378012_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2042378012_0_0_0 = { 1, GenInst_KeyValuePair_2_t2042378012_0_0_0_Types };
extern const Il2CppType Rigidbody_t3346577219_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t3346577219_0_0_0_Types[] = { &Rigidbody_t3346577219_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t3346577219_0_0_0 = { 1, GenInst_Rigidbody_t3346577219_0_0_0_Types };
extern const Il2CppType AnimationCurve_t3667593487_0_0_0;
static const Il2CppType* GenInst_AnimationCurve_t3667593487_0_0_0_Types[] = { &AnimationCurve_t3667593487_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationCurve_t3667593487_0_0_0 = { 1, GenInst_AnimationCurve_t3667593487_0_0_0_Types };
extern const Il2CppType Calculation_t2191327052_0_0_0;
static const Il2CppType* GenInst_Calculation_t2191327052_0_0_0_Types[] = { &Calculation_t2191327052_0_0_0 };
extern const Il2CppGenericInst GenInst_Calculation_t2191327052_0_0_0 = { 1, GenInst_Calculation_t2191327052_0_0_0_Types };
extern const Il2CppType Calculation_t2771812670_0_0_0;
static const Il2CppType* GenInst_Calculation_t2771812670_0_0_0_Types[] = { &Calculation_t2771812670_0_0_0 };
extern const Il2CppGenericInst GenInst_Calculation_t2771812670_0_0_0 = { 1, GenInst_Calculation_t2771812670_0_0_0_Types };
extern const Il2CppType NetworkView_t3656680617_0_0_0;
static const Il2CppType* GenInst_NetworkView_t3656680617_0_0_0_Types[] = { &NetworkView_t3656680617_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkView_t3656680617_0_0_0 = { 1, GenInst_NetworkView_t3656680617_0_0_0_Types };
extern const Il2CppType Animation_t1724966010_0_0_0;
static const Il2CppType* GenInst_Animation_t1724966010_0_0_0_Types[] = { &Animation_t1724966010_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t1724966010_0_0_0 = { 1, GenInst_Animation_t1724966010_0_0_0_Types };
extern const Il2CppType AudioClip_t794140988_0_0_0;
static const Il2CppType* GenInst_AudioClip_t794140988_0_0_0_Types[] = { &AudioClip_t794140988_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t794140988_0_0_0 = { 1, GenInst_AudioClip_t794140988_0_0_0_Types };
extern const Il2CppType AudioSource_t1740077639_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1740077639_0_0_0_Types[] = { &AudioSource_t1740077639_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1740077639_0_0_0 = { 1, GenInst_AudioSource_t1740077639_0_0_0_Types };
extern const Il2CppType GUIText_t3371372606_0_0_0;
static const Il2CppType* GenInst_GUIText_t3371372606_0_0_0_Types[] = { &GUIText_t3371372606_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t3371372606_0_0_0 = { 1, GenInst_GUIText_t3371372606_0_0_0_Types };
extern const Il2CppType GUITexture_t4020448292_0_0_0;
static const Il2CppType* GenInst_GUITexture_t4020448292_0_0_0_Types[] = { &GUITexture_t4020448292_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t4020448292_0_0_0 = { 1, GenInst_GUITexture_t4020448292_0_0_0_Types };
extern const Il2CppType Light_t4202674828_0_0_0;
static const Il2CppType* GenInst_Light_t4202674828_0_0_0_Types[] = { &Light_t4202674828_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t4202674828_0_0_0 = { 1, GenInst_Light_t4202674828_0_0_0_Types };
static const Il2CppType* GenInst_Action_t3771233898_0_0_0_Types[] = { &Action_t3771233898_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3771233898_0_0_0 = { 1, GenInst_Action_t3771233898_0_0_0_Types };
extern const Il2CppType DelayedQueueItem_t3219765808_0_0_0;
static const Il2CppType* GenInst_DelayedQueueItem_t3219765808_0_0_0_Types[] = { &DelayedQueueItem_t3219765808_0_0_0 };
extern const Il2CppGenericInst GenInst_DelayedQueueItem_t3219765808_0_0_0 = { 1, GenInst_DelayedQueueItem_t3219765808_0_0_0_Types };
static const Il2CppType* GenInst_DelayedQueueItem_t3219765808_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &DelayedQueueItem_t3219765808_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_DelayedQueueItem_t3219765808_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_DelayedQueueItem_t3219765808_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_EncodeHintType_t3244562957_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0, &Il2CppObject_0_0_0, &EncodeHintType_t3244562957_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_EncodeHintType_t3244562957_0_0_0 = { 3, GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_EncodeHintType_t3244562957_0_0_0_Types };
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3617918191_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3617918191_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3617918191_0_0_0 = { 3, GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3617918191_0_0_0_Types };
extern const Il2CppType PedidoData_t3443010735_0_0_0;
static const Il2CppType* GenInst_PedidoData_t3443010735_0_0_0_Types[] = { &PedidoData_t3443010735_0_0_0 };
extern const Il2CppGenericInst GenInst_PedidoData_t3443010735_0_0_0 = { 1, GenInst_PedidoData_t3443010735_0_0_0_Types };
extern const Il2CppType CarnesData_t3441984818_0_0_0;
static const Il2CppType* GenInst_CarnesData_t3441984818_0_0_0_Types[] = { &CarnesData_t3441984818_0_0_0 };
extern const Il2CppGenericInst GenInst_CarnesData_t3441984818_0_0_0 = { 1, GenInst_CarnesData_t3441984818_0_0_0_Types };
static const Il2CppType* GenInst_OptionData_t185687546_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &OptionData_t185687546_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t185687546_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_OptionData_t185687546_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Char_t2862622538_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t3674682005_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0 = { 2, GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t3674682005_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t98913785_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t98913785_0_0_0_Types[] = { &KeyValuePair_2_t98913785_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t98913785_0_0_0 = { 1, GenInst_KeyValuePair_2_t98913785_0_0_0_Types };
extern const Il2CppType CuponData_t807724487_0_0_0;
static const Il2CppType* GenInst_CuponData_t807724487_0_0_0_Types[] = { &CuponData_t807724487_0_0_0 };
extern const Il2CppGenericInst GenInst_CuponData_t807724487_0_0_0 = { 1, GenInst_CuponData_t807724487_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &RectTransform_t972643934_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &RectTransform_t972643934_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t868687879_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t868687879_0_0_0_Types[] = { &KeyValuePair_2_t868687879_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t868687879_0_0_0 = { 1, GenInst_KeyValuePair_2_t868687879_0_0_0_Types };
extern const Il2CppType PublicationData_t473548758_0_0_0;
static const Il2CppType* GenInst_PublicationData_t473548758_0_0_0_Types[] = { &PublicationData_t473548758_0_0_0 };
extern const Il2CppGenericInst GenInst_PublicationData_t473548758_0_0_0 = { 1, GenInst_PublicationData_t473548758_0_0_0_Types };
extern const Il2CppType PedidoEnvioData_t1309077304_0_0_0;
static const Il2CppType* GenInst_PedidoEnvioData_t1309077304_0_0_0_Types[] = { &PedidoEnvioData_t1309077304_0_0_0 };
extern const Il2CppGenericInst GenInst_PedidoEnvioData_t1309077304_0_0_0 = { 1, GenInst_PedidoEnvioData_t1309077304_0_0_0_Types };
extern const Il2CppType OfertaValues_t3488850643_0_0_0;
static const Il2CppType* GenInst_OfertaValues_t3488850643_0_0_0_Types[] = { &OfertaValues_t3488850643_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertaValues_t3488850643_0_0_0 = { 1, GenInst_OfertaValues_t3488850643_0_0_0_Types };
extern const Il2CppType UIBehaviour_t2511441271_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t2511441271_0_0_0_Types[] = { &UIBehaviour_t2511441271_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t2511441271_0_0_0 = { 1, GenInst_UIBehaviour_t2511441271_0_0_0_Types };
extern const Il2CppType CortesDataU5BU5D_t1695339363_0_0_0;
static const Il2CppType* GenInst_CortesDataU5BU5D_t1695339363_0_0_0_Types[] = { &CortesDataU5BU5D_t1695339363_0_0_0 };
extern const Il2CppGenericInst GenInst_CortesDataU5BU5D_t1695339363_0_0_0 = { 1, GenInst_CortesDataU5BU5D_t1695339363_0_0_0_Types };
extern const Il2CppType TabloideData_t1867721404_0_0_0;
static const Il2CppType* GenInst_TabloideData_t1867721404_0_0_0_Types[] = { &TabloideData_t1867721404_0_0_0 };
extern const Il2CppGenericInst GenInst_TabloideData_t1867721404_0_0_0 = { 1, GenInst_TabloideData_t1867721404_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Action_t3771233898_0_0_0_Types[] = { &String_t_0_0_0, &Action_t3771233898_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Action_t3771233898_0_0_0 = { 2, GenInst_String_t_0_0_0_Action_t3771233898_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Action_t3771233898_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t195465678_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t195465678_0_0_0_Types[] = { &KeyValuePair_2_t195465678_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t195465678_0_0_0 = { 1, GenInst_KeyValuePair_2_t195465678_0_0_0_Types };
extern const Il2CppType Action_1_t1392508089_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0_Types[] = { &String_t_0_0_0, &Action_1_t1392508089_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0 = { 2, GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0_Types };
extern const Il2CppType UniWebViewNativeResultPayload_t996691953_0_0_0;
static const Il2CppType* GenInst_UniWebViewNativeResultPayload_t996691953_0_0_0_Types[] = { &UniWebViewNativeResultPayload_t996691953_0_0_0 };
extern const Il2CppGenericInst GenInst_UniWebViewNativeResultPayload_t996691953_0_0_0 = { 1, GenInst_UniWebViewNativeResultPayload_t996691953_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Action_1_t1392508089_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2111707165_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2111707165_0_0_0_Types[] = { &KeyValuePair_2_t2111707165_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2111707165_0_0_0 = { 1, GenInst_KeyValuePair_2_t2111707165_0_0_0_Types };
extern const Il2CppType UniWebViewNativeListener_t795571060_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0_Types[] = { &String_t_0_0_0, &UniWebViewNativeListener_t795571060_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0 = { 2, GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &UniWebViewNativeListener_t795571060_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1514770136_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1514770136_0_0_0_Types[] = { &KeyValuePair_2_t1514770136_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1514770136_0_0_0 = { 1, GenInst_KeyValuePair_2_t1514770136_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t433318935_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t433318935_0_0_0_Types[] = { &WireframeBehaviour_t433318935_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t433318935_0_0_0 = { 1, GenInst_WireframeBehaviour_t433318935_0_0_0_Types };
extern const Il2CppType Hashtable_t1407064410_0_0_0;
static const Il2CppType* GenInst_Hashtable_t1407064410_0_0_0_Types[] = { &Hashtable_t1407064410_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t1407064410_0_0_0 = { 1, GenInst_Hashtable_t1407064410_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t309087608_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t309087608_gp_0_0_0_0_Types[] = { &IEnumerable_1_t309087608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t309087608_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t309087608_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1181603442_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types[] = { &Array_Sort_m1181603442_gp_0_0_0_0, &Array_Sort_m1181603442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3908760906_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3908760906_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types[] = { &Array_Sort_m3908760906_gp_0_0_0_0, &Array_Sort_m3908760906_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m646233104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0, &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0, &Array_Sort_m2404937677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m377069906_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types[] = { &Array_Sort_m377069906_gp_0_0_0_0, &Array_Sort_m377069906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1327718954_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1327718954_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types[] = { &Array_Sort_m1327718954_gp_0_0_0_0, &Array_Sort_m1327718954_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m4084526832_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0, &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0, &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m3566161319_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types[] = { &Array_Sort_m3566161319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3566161319_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1767877396_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types[] = { &Array_Sort_m1767877396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1767877396_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0, &Array_qsort_m785378185_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m3718693973_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types[] = { &Array_compare_m3718693973_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m3718693973_gp_0_0_0_0 = { 1, GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m3270161954_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types[] = { &Array_qsort_m3270161954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m3270161954_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m2347367271_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types[] = { &Array_Resize_m2347367271_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m2347367271_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m123384663_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m123384663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1326446122_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types[] = { &Array_ForEach_m1326446122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1326446122_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1697145810_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1697145810_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1697145810_gp_0_0_0_0, &Array_ConvertAll_m1697145810_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m614217902_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m614217902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m653822311_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m653822311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1650781518_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1650781518_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m308210168_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types[] = { &Array_FindIndex_m308210168_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m308210168_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m3338256477_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types[] = { &Array_FindIndex_m3338256477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1605056024_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1605056024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3507857443_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3507857443_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1578426497_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1578426497_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3933814211_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3933814211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1779658273_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1779658273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3823835921_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3823835921_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1695958374_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1695958374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3280162097_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3280162097_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m921616327_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m921616327_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m4157241456_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m4157241456_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m925096551_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m925096551_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1181152586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types[] = { &Array_FindAll_m1181152586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1181152586_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m2312185345_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types[] = { &Array_Exists_m2312185345_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m2312185345_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m4233614634_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m4233614634_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m3878993575_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types[] = { &Array_Find_m3878993575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m3878993575_gp_0_0_0_0 = { 1, GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3671273393_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types[] = { &Array_FindLast_m3671273393_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3671273393_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t813279015_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t813279015_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2706020430_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2706020430_gp_0_0_0_0_Types[] = { &IList_1_t2706020430_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2706020430_gp_0_0_0_0 = { 1, GenInst_IList_1_t2706020430_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1952910030_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1952910030_gp_0_0_0_0_Types[] = { &ICollection_1_t1952910030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1952910030_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1952910030_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1122404262_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1122404262_gp_0_0_0_0_Types[] = { &Nullable_1_t1122404262_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1122404262_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1122404262_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t898371836_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t898371836_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4022413346_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022413346_0_0_0 = { 1, GenInst_KeyValuePair_2_t4022413346_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3290534805_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3290534805_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3290534805_gp_0_0_0_0, &ShimEnumerator_t3290534805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1176480892_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1176480892_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0_Types[] = { &Enumerator_t1176480892_gp_0_0_0_0, &Enumerator_t1176480892_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912719906_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912719906_0_0_0_Types[] = { &KeyValuePair_2_t1912719906_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912719906_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912719906_0_0_0_Types };
extern const Il2CppType KeyCollection_t2018332325_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t2018332325_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4049866126_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4049866126_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0_Types[] = { &Enumerator_t4049866126_gp_0_0_0_0, &Enumerator_t4049866126_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4049866126_gp_0_0_0_0_Types[] = { &Enumerator_t4049866126_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4049866126_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4049866126_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_1_0_0_0, &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t3097895223_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3097895223_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_0_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3097895223_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2802076604_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2802076604_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0_Types[] = { &Enumerator_t2802076604_gp_0_0_0_0, &Enumerator_t2802076604_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2802076604_gp_1_0_0_0_Types[] = { &Enumerator_t2802076604_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2802076604_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2802076604_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_0_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_1_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 2, GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &KeyValuePair_2_t4022413346_0_0_0, &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0 = { 2, GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_1_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t898371836_gp_1_0_0_0_Types };
extern const Il2CppType IDictionary_2_t435039943_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t435039943_gp_0_0_0_0_Types[] = { &IDictionary_2_t435039943_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t435039943_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t435039943_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2536192150_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2536192150_0_0_0_Types[] = { &KeyValuePair_2_t2536192150_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2536192150_0_0_0 = { 1, GenInst_KeyValuePair_2_t2536192150_0_0_0_Types };
extern const Il2CppType IDictionary_2_t435039943_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0_Types[] = { &IDictionary_2_t435039943_gp_0_0_0_0, &IDictionary_2_t435039943_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t911337330_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t911337330_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t911337330_gp_0_0_0_0, &KeyValuePair_2_t911337330_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2934485036_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2934485036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4247873286_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4247873286_gp_0_0_0_0_Types[] = { &DefaultComparer_t4247873286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4247873286_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4247873286_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t961487589_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t961487589_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t951551555_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t951551555_gp_0_0_0_0_Types[] = { &List_1_t951551555_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t951551555_gp_0_0_0_0 = { 1, GenInst_List_1_t951551555_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2095969493_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2095969493_gp_0_0_0_0_Types[] = { &Enumerator_t2095969493_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2095969493_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2095969493_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t3473121937_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t3473121937_gp_0_0_0_0_Types[] = { &Collection_1_t3473121937_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t3473121937_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3473121937_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t4004629011_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t4004629011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2757088543_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0_Types[] = { &ArraySegment_1_t2757088543_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1701613202_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1701613202_gp_0_0_0_0_Types[] = { &Comparer_1_t1701613202_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1701613202_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1701613202_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3219634540_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3219634540_gp_0_0_0_0_Types[] = { &DefaultComparer_t3219634540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3219634540_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3219634540_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2982791755_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2982791755_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2631682684_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t2631682684_gp_0_0_0_0_Types[] = { &LinkedList_1_t2631682684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2631682684_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t2631682684_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2473886460_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2473886460_gp_0_0_0_0_Types[] = { &Enumerator_t2473886460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2473886460_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2473886460_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t603318558_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t603318558_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4128415215_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4128415215_gp_0_0_0_0_Types[] = { &Stack_1_t4128415215_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4128415215_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4128415215_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2313787817_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2313787817_gp_0_0_0_0_Types[] = { &Enumerator_t2313787817_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2313787817_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2313787817_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m1224968048_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types[] = { &Enumerable_Any_m1224968048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m692115499_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m692115499_gp_0_0_0_0_Types[] = { &Enumerable_Any_m692115499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m692115499_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m692115499_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Any_m692115499_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_Any_m692115499_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m692115499_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_Any_m692115499_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m3421076499_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m3421076499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2837618484_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2837618484_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2710245799_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2710245799_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m3995152903_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0_Types[] = { &Enumerable_Count_m3995152903_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m1147197826_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0_Types[] = { &Enumerable_Count_m1147197826_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_Count_m1147197826_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_DefaultIfEmpty_m622648125_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_DefaultIfEmpty_m622648125_gp_0_0_0_0_Types[] = { &Enumerable_DefaultIfEmpty_m622648125_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_DefaultIfEmpty_m622648125_gp_0_0_0_0 = { 1, GenInst_Enumerable_DefaultIfEmpty_m622648125_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateDefaultIfEmptyIterator_m2450906323_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateDefaultIfEmptyIterator_m2450906323_gp_0_0_0_0_Types[] = { &Enumerable_CreateDefaultIfEmptyIterator_m2450906323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateDefaultIfEmptyIterator_m2450906323_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateDefaultIfEmptyIterator_m2450906323_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m683227859_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m683227859_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m683227859_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m683227859_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m683227859_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m1602634822_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m1602634822_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m1602634822_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m1602634822_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m1602634822_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateDistinctIterator_m116324828_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateDistinctIterator_m116324828_gp_0_0_0_0_Types[] = { &Enumerable_CreateDistinctIterator_m116324828_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateDistinctIterator_m116324828_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateDistinctIterator_m116324828_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m179991593_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m179991593_gp_0_0_0_0_Types[] = { &Enumerable_First_m179991593_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m179991593_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m179991593_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m179991593_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_First_m179991593_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m179991593_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_First_m179991593_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1340241468_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types[] = { &Enumerable_First_m1340241468_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1340241468_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_Last_m3437589648_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Last_m3437589648_gp_0_0_0_0_Types[] = { &Enumerable_Last_m3437589648_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3437589648_gp_0_0_0_0 = { 1, GenInst_Enumerable_Last_m3437589648_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Iterate_m3481514816_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0_Types[] = { &Enumerable_Iterate_m3481514816_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0 = { 1, GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Iterate_m3481514816_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0_Types[] = { &Enumerable_Iterate_m3481514816_gp_0_0_0_0, &Enumerable_Iterate_m3481514816_gp_1_0_0_0, &Enumerable_Iterate_m3481514816_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0 = { 3, GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OfType_m3233395589_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OfType_m3233395589_gp_0_0_0_0_Types[] = { &Enumerable_OfType_m3233395589_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OfType_m3233395589_gp_0_0_0_0 = { 1, GenInst_Enumerable_OfType_m3233395589_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateOfTypeIterator_m1358460463_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateOfTypeIterator_m1358460463_gp_0_0_0_0_Types[] = { &Enumerable_CreateOfTypeIterator_m1358460463_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateOfTypeIterator_m1358460463_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateOfTypeIterator_m1358460463_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2198038980_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2198038980_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2198038980_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2198038980_gp_0_0_0_0, &Enumerable_Select_m2198038980_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2198038980_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m3064021096_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2457202127_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_0_0_0_0, &IEnumerable_1_t2457202127_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m3064021096_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_0_0_0_0, &Enumerable_SelectMany_m3064021096_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t402820473_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0, &IEnumerable_1_t402820473_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0, &Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m2162276958_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0_Types[] = { &Enumerable_Single_m2162276958_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m600216364_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m600216364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m3865828353_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m3865828353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m4110359_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Types[] = { &Enumerable_Where_m4110359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m4110359_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_Where_m4110359_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t1600972147_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t1600972147_gp_0_0_0_0_Types[] = { &U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t1600972147_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t1600972147_gp_0_0_0_0 = { 1, GenInst_U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t1600972147_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3100449863_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3100449863_gp_0_0_0_0_Types[] = { &U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3100449863_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3100449863_gp_0_0_0_0 = { 1, GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3100449863_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t2905255426_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t2905255426_gp_0_0_0_0_Types[] = { &U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t2905255426_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t2905255426_gp_0_0_0_0 = { 1, GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t2905255426_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4053127014_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0, &IEnumerable_1_t4053127014_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0, &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType HashSet_1_t3763949723_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t3763949723_gp_0_0_0_0_Types[] = { &HashSet_1_t3763949723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t3763949723_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3763949723_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1211810685_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1211810685_gp_0_0_0_0_Types[] = { &Enumerator_t1211810685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1211810685_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1211810685_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3144359540_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3144359540_gp_0_0_0_0_Types[] = { &PrimeHelper_t3144359540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3144359540_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3144359540_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m2068134811_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m2548367508_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m885533373_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m290105122_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2190484233_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2983504498_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m228373778_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m228373778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2204190206_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2204190206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m482375811_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types[] = { &Component_GetComponents_m482375811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m482375811_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m311092670_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m311092670_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_GetStatic_m2919332632_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_GetStatic_m2919332632_gp_0_0_0_0_Types[] = { &AndroidJavaObject_GetStatic_m2919332632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_GetStatic_m2919332632_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_GetStatic_m2919332632_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_Call_m1663154586_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_Call_m1663154586_gp_0_0_0_0_Types[] = { &AndroidJavaObject_Call_m1663154586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_Call_m1663154586_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_Call_m1663154586_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject_CallStatic_m870649740_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject_CallStatic_m870649740_gp_0_0_0_0_Types[] = { &AndroidJavaObject_CallStatic_m870649740_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject_CallStatic_m870649740_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject_CallStatic_m870649740_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__Call_m2159746725_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__Call_m2159746725_gp_0_0_0_0_Types[] = { &AndroidJavaObject__Call_m2159746725_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__Call_m2159746725_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__Call_m2159746725_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__CallStatic_m369311831_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__CallStatic_m369311831_gp_0_0_0_0_Types[] = { &AndroidJavaObject__CallStatic_m369311831_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__CallStatic_m369311831_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__CallStatic_m369311831_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJavaObject__GetStatic_m1083753313_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJavaObject__GetStatic_m1083753313_gp_0_0_0_0_Types[] = { &AndroidJavaObject__GetStatic_m1083753313_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJavaObject__GetStatic_m1083753313_gp_0_0_0_0 = { 1, GenInst_AndroidJavaObject__GetStatic_m1083753313_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_ConvertFromJNIArray_m1627213724_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_ConvertFromJNIArray_m1627213724_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_ConvertFromJNIArray_m1627213724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_ConvertFromJNIArray_m1627213724_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_ConvertFromJNIArray_m1627213724_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_GetMethodID_m2032786278_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_GetMethodID_m2032786278_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_GetMethodID_m2032786278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_GetMethodID_m2032786278_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_GetMethodID_m2032786278_gp_0_0_0_0_Types };
extern const Il2CppType AndroidJNIHelper_GetFieldID_m1741341436_gp_0_0_0_0;
static const Il2CppType* GenInst_AndroidJNIHelper_GetFieldID_m1741341436_gp_0_0_0_0_Types[] = { &AndroidJNIHelper_GetFieldID_m1741341436_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidJNIHelper_GetFieldID_m1741341436_gp_0_0_0_0 = { 1, GenInst_AndroidJNIHelper_GetFieldID_m1741341436_gp_0_0_0_0_Types };
extern const Il2CppType _AndroidJNIHelper_GetMethodID_m4173184413_gp_0_0_0_0;
static const Il2CppType* GenInst__AndroidJNIHelper_GetMethodID_m4173184413_gp_0_0_0_0_Types[] = { &_AndroidJNIHelper_GetMethodID_m4173184413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst__AndroidJNIHelper_GetMethodID_m4173184413_gp_0_0_0_0 = { 1, GenInst__AndroidJNIHelper_GetMethodID_m4173184413_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3431730600_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3431730600_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3431730601_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3431730601_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3431730601_gp_0_0_0_0, &InvokableCall_2_t3431730601_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3431730601_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3431730601_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3431730602_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3431730602_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3431730602_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_0_0_0_0, &InvokableCall_3_t3431730602_gp_1_0_0_0, &InvokableCall_3_t3431730602_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3431730603_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3431730603_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3431730603_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3431730603_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_0_0_0_0, &InvokableCall_4_t3431730603_gp_1_0_0_0, &InvokableCall_4_t3431730603_gp_2_0_0_0, &InvokableCall_4_t3431730603_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t1222316710_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t1222316710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t1176538020_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0_Types[] = { &UnityEvent_1_t1176538020_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t1176538021_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1176538021_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0_Types[] = { &UnityEvent_2_t1176538021_gp_0_0_0_0, &UnityEvent_2_t1176538021_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t1176538022_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1176538022_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1176538022_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0_Types[] = { &UnityEvent_3_t1176538022_gp_0_0_0_0, &UnityEvent_3_t1176538022_gp_1_0_0_0, &UnityEvent_3_t1176538022_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t1176538023_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1176538023_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1176538023_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1176538023_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0_Types[] = { &UnityEvent_4_t1176538023_gp_0_0_0_0, &UnityEvent_4_t1176538023_gp_1_0_0_0, &UnityEvent_4_t1176538023_gp_2_0_0_0, &UnityEvent_4_t1176538023_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0_Types };
extern const Il2CppType Arrays_1_t2739302885_gp_0_0_0_0;
static const Il2CppType* GenInst_Arrays_1_t2739302885_gp_0_0_0_0_Types[] = { &Arrays_1_t2739302885_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Arrays_1_t2739302885_gp_0_0_0_0 = { 1, GenInst_Arrays_1_t2739302885_gp_0_0_0_0_Types };
extern const Il2CppType Lists_1_t4025286752_gp_0_0_0_0;
static const Il2CppType* GenInst_Lists_1_t4025286752_gp_0_0_0_0_Types[] = { &Lists_1_t4025286752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Lists_1_t4025286752_gp_0_0_0_0 = { 1, GenInst_Lists_1_t4025286752_gp_0_0_0_0_Types };
extern const Il2CppType BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0;
static const Il2CppType* GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0_Types[] = { &BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &LuminanceSource_t1231523093_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0 = { 4, GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0_Types[] = { &BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0 = { 1, GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0_Types };
extern const Il2CppType ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0;
static const Il2CppType* GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0_Types[] = { &ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0 = { 1, GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0_Types };
extern const Il2CppType ChangeNotifyDictionary_2_t868889846_gp_1_0_0_0;
static const Il2CppType* GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0_ChangeNotifyDictionary_2_t868889846_gp_1_0_0_0_Types[] = { &ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0, &ChangeNotifyDictionary_2_t868889846_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0_ChangeNotifyDictionary_2_t868889846_gp_1_0_0_0 = { 2, GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0_ChangeNotifyDictionary_2_t868889846_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t974145274_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t974145274_0_0_0_Types[] = { &KeyValuePair_2_t974145274_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t974145274_0_0_0 = { 1, GenInst_KeyValuePair_2_t974145274_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m330735833_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m330735833_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m330735833_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m330735833_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m330735833_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m825525191_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m825525191_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2054808038_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2054808038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t1486860772_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Types[] = { &IndexedSet_1_t1486860772_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &IndexedSet_1_t1486860772_gp_0_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType ListPool_1_t3815171383_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t3815171383_gp_0_0_0_0_Types[] = { &ListPool_1_t3815171383_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t3815171383_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t3815171383_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1536022088_0_0_0;
static const Il2CppType* GenInst_List_1_t1536022088_0_0_0_Types[] = { &List_1_t1536022088_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1536022088_0_0_0 = { 1, GenInst_List_1_t1536022088_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t3981551192_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0_Types[] = { &ObjectPool_1_t3981551192_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0_Types };
extern const Il2CppType ScrollRectEx_DoForParents_m145041647_gp_0_0_0_0;
static const Il2CppType* GenInst_ScrollRectEx_DoForParents_m145041647_gp_0_0_0_0_Types[] = { &ScrollRectEx_DoForParents_m145041647_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRectEx_DoForParents_m145041647_gp_0_0_0_0 = { 1, GenInst_ScrollRectEx_DoForParents_m145041647_gp_0_0_0_0_Types };
extern const Il2CppType CallbackManager_AddFacebookDelegate_m4253588562_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackManager_AddFacebookDelegate_m4253588562_gp_0_0_0_0_Types[] = { &CallbackManager_AddFacebookDelegate_m4253588562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackManager_AddFacebookDelegate_m4253588562_gp_0_0_0_0 = { 1, GenInst_CallbackManager_AddFacebookDelegate_m4253588562_gp_0_0_0_0_Types };
extern const Il2CppType CallbackManager_TryCallCallback_m948328713_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackManager_TryCallCallback_m948328713_gp_0_0_0_0_Types[] = { &CallbackManager_TryCallCallback_m948328713_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackManager_TryCallCallback_m948328713_gp_0_0_0_0 = { 1, GenInst_CallbackManager_TryCallCallback_m948328713_gp_0_0_0_0_Types };
extern const Il2CppType CanvasUIMethodCall_1_t2411194616_gp_0_0_0_0;
static const Il2CppType* GenInst_CanvasUIMethodCall_1_t2411194616_gp_0_0_0_0_Types[] = { &CanvasUIMethodCall_1_t2411194616_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasUIMethodCall_1_t2411194616_gp_0_0_0_0 = { 1, GenInst_CanvasUIMethodCall_1_t2411194616_gp_0_0_0_0_Types };
extern const Il2CppType ComponentFactory_GetComponent_m1706671437_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentFactory_GetComponent_m1706671437_gp_0_0_0_0_Types[] = { &ComponentFactory_GetComponent_m1706671437_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactory_GetComponent_m1706671437_gp_0_0_0_0 = { 1, GenInst_ComponentFactory_GetComponent_m1706671437_gp_0_0_0_0_Types };
extern const Il2CppType ComponentFactory_AddComponent_m3464507492_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentFactory_AddComponent_m3464507492_gp_0_0_0_0_Types[] = { &ComponentFactory_AddComponent_m3464507492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactory_AddComponent_m3464507492_gp_0_0_0_0 = { 1, GenInst_ComponentFactory_AddComponent_m3464507492_gp_0_0_0_0_Types };
extern const Il2CppType MethodArguments_AddNullablePrimitive_m3778017809_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodArguments_AddNullablePrimitive_m3778017809_gp_0_0_0_0_Types[] = { &MethodArguments_AddNullablePrimitive_m3778017809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodArguments_AddNullablePrimitive_m3778017809_gp_0_0_0_0 = { 1, GenInst_MethodArguments_AddNullablePrimitive_m3778017809_gp_0_0_0_0_Types };
extern const Il2CppType MethodArguments_AddList_m2038064151_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodArguments_AddList_m2038064151_gp_0_0_0_0_Types[] = { &MethodArguments_AddList_m2038064151_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodArguments_AddList_m2038064151_gp_0_0_0_0 = { 1, GenInst_MethodArguments_AddList_m2038064151_gp_0_0_0_0_Types };
extern const Il2CppType MethodCall_1_t1724252912_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodCall_1_t1724252912_gp_0_0_0_0_Types[] = { &MethodCall_1_t1724252912_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodCall_1_t1724252912_gp_0_0_0_0 = { 1, GenInst_MethodCall_1_t1724252912_gp_0_0_0_0_Types };
extern const Il2CppType JavaMethodCall_1_t2135904438_gp_0_0_0_0;
static const Il2CppType* GenInst_JavaMethodCall_1_t2135904438_gp_0_0_0_0_Types[] = { &JavaMethodCall_1_t2135904438_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JavaMethodCall_1_t2135904438_gp_0_0_0_0 = { 1, GenInst_JavaMethodCall_1_t2135904438_gp_0_0_0_0_Types };
extern const Il2CppType FBJavaClass_CallStatic_m1002577452_gp_0_0_0_0;
static const Il2CppType* GenInst_FBJavaClass_CallStatic_m1002577452_gp_0_0_0_0_Types[] = { &FBJavaClass_CallStatic_m1002577452_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_FBJavaClass_CallStatic_m1002577452_gp_0_0_0_0 = { 1, GenInst_FBJavaClass_CallStatic_m1002577452_gp_0_0_0_0_Types };
extern const Il2CppType IOSFacebook_AddCallback_m3533341517_gp_0_0_0_0;
static const Il2CppType* GenInst_IOSFacebook_AddCallback_m3533341517_gp_0_0_0_0_Types[] = { &IOSFacebook_AddCallback_m3533341517_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebook_AddCallback_m3533341517_gp_0_0_0_0 = { 1, GenInst_IOSFacebook_AddCallback_m3533341517_gp_0_0_0_0_Types };
extern const Il2CppType EditorFacebook_ShowEmptyMockDialog_m3588839153_gp_0_0_0_0;
static const Il2CppType* GenInst_EditorFacebook_ShowEmptyMockDialog_m3588839153_gp_0_0_0_0_Types[] = { &EditorFacebook_ShowEmptyMockDialog_m3588839153_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebook_ShowEmptyMockDialog_m3588839153_gp_0_0_0_0 = { 1, GenInst_EditorFacebook_ShowEmptyMockDialog_m3588839153_gp_0_0_0_0_Types };
extern const Il2CppType Utilities_GetValueOrDefault_m3842879737_gp_0_0_0_0;
static const Il2CppType* GenInst_Utilities_GetValueOrDefault_m3842879737_gp_0_0_0_0_Types[] = { &Utilities_GetValueOrDefault_m3842879737_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_GetValueOrDefault_m3842879737_gp_0_0_0_0 = { 1, GenInst_Utilities_GetValueOrDefault_m3842879737_gp_0_0_0_0_Types };
extern const Il2CppType Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0;
extern const Il2CppType Utilities_AddAllKVPFrom_m505241225_gp_1_0_0_0;
static const Il2CppType* GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0_Utilities_AddAllKVPFrom_m505241225_gp_1_0_0_0_Types[] = { &Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0, &Utilities_AddAllKVPFrom_m505241225_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0_Utilities_AddAllKVPFrom_m505241225_gp_1_0_0_0 = { 2, GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0_Utilities_AddAllKVPFrom_m505241225_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0_Types[] = { &Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0 = { 1, GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0_Types };
extern const Il2CppType JsonMapper_RegisterExporter_m1048406668_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonMapper_RegisterExporter_m1048406668_gp_0_0_0_0_Types[] = { &JsonMapper_RegisterExporter_m1048406668_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonMapper_RegisterExporter_m1048406668_gp_0_0_0_0 = { 1, GenInst_JsonMapper_RegisterExporter_m1048406668_gp_0_0_0_0_Types };
extern const Il2CppType JsonMapper_RegisterImporter_m3836829258_gp_0_0_0_0;
extern const Il2CppType JsonMapper_RegisterImporter_m3836829258_gp_1_0_0_0;
static const Il2CppType* GenInst_JsonMapper_RegisterImporter_m3836829258_gp_0_0_0_0_JsonMapper_RegisterImporter_m3836829258_gp_1_0_0_0_Types[] = { &JsonMapper_RegisterImporter_m3836829258_gp_0_0_0_0, &JsonMapper_RegisterImporter_m3836829258_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonMapper_RegisterImporter_m3836829258_gp_0_0_0_0_JsonMapper_RegisterImporter_m3836829258_gp_1_0_0_0 = { 2, GenInst_JsonMapper_RegisterImporter_m3836829258_gp_0_0_0_0_JsonMapper_RegisterImporter_m3836829258_gp_1_0_0_0_Types };
extern const Il2CppType U3CRegisterExporterU3Ec__AnonStoreyA2_1_t3613248724_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRegisterExporterU3Ec__AnonStoreyA2_1_t3613248724_gp_0_0_0_0_Types[] = { &U3CRegisterExporterU3Ec__AnonStoreyA2_1_t3613248724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterExporterU3Ec__AnonStoreyA2_1_t3613248724_gp_0_0_0_0 = { 1, GenInst_U3CRegisterExporterU3Ec__AnonStoreyA2_1_t3613248724_gp_0_0_0_0_Types };
extern const Il2CppType U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_0_0_0_0;
extern const Il2CppType U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_1_0_0_0_Types[] = { &U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_0_0_0_0, &U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_1_0_0_0 = { 2, GenInst_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_1_0_0_0_Types };
extern const Il2CppType ComponentAction_1_t2706192626_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0_Types[] = { &ComponentAction_1_t2706192626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0 = { 1, GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0_Types };
extern const Il2CppType DragMe_FindInParents_m3518302513_gp_0_0_0_0;
static const Il2CppType* GenInst_DragMe_FindInParents_m3518302513_gp_0_0_0_0_Types[] = { &DragMe_FindInParents_m3518302513_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DragMe_FindInParents_m3518302513_gp_0_0_0_0 = { 1, GenInst_DragMe_FindInParents_m3518302513_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Single_t4291918972_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types };
extern const Il2CppType GUILayer_t2983897946_0_0_0;
static const Il2CppType* GenInst_GUILayer_t2983897946_0_0_0_Types[] = { &GUILayer_t2983897946_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t2983897946_0_0_0 = { 1, GenInst_GUILayer_t2983897946_0_0_0_Types };
extern const Il2CppType RenderListenerUtility_t2682722690_0_0_0;
static const Il2CppType* GenInst_RenderListenerUtility_t2682722690_0_0_0_Types[] = { &RenderListenerUtility_t2682722690_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderListenerUtility_t2682722690_0_0_0 = { 1, GenInst_RenderListenerUtility_t2682722690_0_0_0_Types };
extern const Il2CppType PlayMakerMouseEvents_t316289710_0_0_0;
static const Il2CppType* GenInst_PlayMakerMouseEvents_t316289710_0_0_0_Types[] = { &PlayMakerMouseEvents_t316289710_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerMouseEvents_t316289710_0_0_0 = { 1, GenInst_PlayMakerMouseEvents_t316289710_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionEnter_t4046453814_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0_Types[] = { &PlayMakerCollisionEnter_t4046453814_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0 = { 1, GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionExit_t3871318016_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionExit_t3871318016_0_0_0_Types[] = { &PlayMakerCollisionExit_t3871318016_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionExit_t3871318016_0_0_0 = { 1, GenInst_PlayMakerCollisionExit_t3871318016_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionStay_t3871731003_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionStay_t3871731003_0_0_0_Types[] = { &PlayMakerCollisionStay_t3871731003_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionStay_t3871731003_0_0_0 = { 1, GenInst_PlayMakerCollisionStay_t3871731003_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerEnter_t977448304_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerEnter_t977448304_0_0_0_Types[] = { &PlayMakerTriggerEnter_t977448304_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerEnter_t977448304_0_0_0 = { 1, GenInst_PlayMakerTriggerEnter_t977448304_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerExit_t3495223174_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerExit_t3495223174_0_0_0_Types[] = { &PlayMakerTriggerExit_t3495223174_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerExit_t3495223174_0_0_0 = { 1, GenInst_PlayMakerTriggerExit_t3495223174_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerStay_t3495636161_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerStay_t3495636161_0_0_0_Types[] = { &PlayMakerTriggerStay_t3495636161_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerStay_t3495636161_0_0_0 = { 1, GenInst_PlayMakerTriggerStay_t3495636161_0_0_0_Types };
extern const Il2CppType PlayMakerFixedUpdate_t997170669_0_0_0;
static const Il2CppType* GenInst_PlayMakerFixedUpdate_t997170669_0_0_0_Types[] = { &PlayMakerFixedUpdate_t997170669_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerFixedUpdate_t997170669_0_0_0 = { 1, GenInst_PlayMakerFixedUpdate_t997170669_0_0_0_Types };
extern const Il2CppType PlayMakerOnGUI_t940239724_0_0_0;
static const Il2CppType* GenInst_PlayMakerOnGUI_t940239724_0_0_0_Types[] = { &PlayMakerOnGUI_t940239724_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerOnGUI_t940239724_0_0_0 = { 1, GenInst_PlayMakerOnGUI_t940239724_0_0_0_Types };
extern const Il2CppType PlayMakerApplicationEvents_t1615127321_0_0_0;
static const Il2CppType* GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0_Types[] = { &PlayMakerApplicationEvents_t1615127321_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0 = { 1, GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0_Types };
extern const Il2CppType PlayMakerGlobals_t3097244096_0_0_0;
static const Il2CppType* GenInst_PlayMakerGlobals_t3097244096_0_0_0_Types[] = { &PlayMakerGlobals_t3097244096_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerGlobals_t3097244096_0_0_0 = { 1, GenInst_PlayMakerGlobals_t3097244096_0_0_0_Types };
extern const Il2CppType PlayMakerGUI_t3799848395_0_0_0;
static const Il2CppType* GenInst_PlayMakerGUI_t3799848395_0_0_0_Types[] = { &PlayMakerGUI_t3799848395_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerGUI_t3799848395_0_0_0 = { 1, GenInst_PlayMakerGUI_t3799848395_0_0_0_Types };
extern const Il2CppType ResultPointCallback_t207829946_0_0_0;
static const Il2CppType* GenInst_ResultPointCallback_t207829946_0_0_0_Types[] = { &ResultPointCallback_t207829946_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultPointCallback_t207829946_0_0_0 = { 1, GenInst_ResultPointCallback_t207829946_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t2067565974_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t2067565974_0_0_0_Types[] = { &SmartTerrainTracker_t2067565974_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t2067565974_0_0_0 = { 1, GenInst_SmartTerrainTracker_t2067565974_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t41970945_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t41970945_0_0_0_Types[] = { &ReconstructionFromTarget_t41970945_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t41970945_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t41970945_0_0_0_Types };
extern const Il2CppType MarkerTracker_t4028259784_0_0_0;
static const Il2CppType* GenInst_MarkerTracker_t4028259784_0_0_0_Types[] = { &MarkerTracker_t4028259784_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerTracker_t4028259784_0_0_0 = { 1, GenInst_MarkerTracker_t4028259784_0_0_0_Types };
extern const Il2CppType TextTracker_t3721656949_0_0_0;
static const Il2CppType* GenInst_TextTracker_t3721656949_0_0_0_Types[] = { &TextTracker_t3721656949_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t3721656949_0_0_0 = { 1, GenInst_TextTracker_t3721656949_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t1091759131_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t1091759131_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0_Types };
extern const Il2CppType WebCamAbstractBehaviour_t725705546_0_0_0;
static const Il2CppType* GenInst_WebCamAbstractBehaviour_t725705546_0_0_0_Types[] = { &WebCamAbstractBehaviour_t725705546_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamAbstractBehaviour_t725705546_0_0_0 = { 1, GenInst_WebCamAbstractBehaviour_t725705546_0_0_0_Types };
extern const Il2CppType EventSystem_t2276120119_0_0_0;
static const Il2CppType* GenInst_EventSystem_t2276120119_0_0_0_Types[] = { &EventSystem_t2276120119_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t2276120119_0_0_0 = { 1, GenInst_EventSystem_t2276120119_0_0_0_Types };
extern const Il2CppType AxisEventData_t3355659985_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t3355659985_0_0_0_Types[] = { &AxisEventData_t3355659985_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t3355659985_0_0_0 = { 1, GenInst_AxisEventData_t3355659985_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t2548470764_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t2548470764_0_0_0_Types[] = { &SpriteRenderer_t2548470764_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t2548470764_0_0_0 = { 1, GenInst_SpriteRenderer_t2548470764_0_0_0_Types };
extern const Il2CppType AspectMode_t2149445162_0_0_0;
static const Il2CppType* GenInst_AspectMode_t2149445162_0_0_0_Types[] = { &AspectMode_t2149445162_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t2149445162_0_0_0 = { 1, GenInst_AspectMode_t2149445162_0_0_0_Types };
extern const Il2CppType FitMode_t909765868_0_0_0;
static const Il2CppType* GenInst_FitMode_t909765868_0_0_0_Types[] = { &FitMode_t909765868_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t909765868_0_0_0 = { 1, GenInst_FitMode_t909765868_0_0_0_Types };
extern const Il2CppType Image_t538875265_0_0_0;
static const Il2CppType* GenInst_Image_t538875265_0_0_0_Types[] = { &Image_t538875265_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t538875265_0_0_0 = { 1, GenInst_Image_t538875265_0_0_0_Types };
extern const Il2CppType Button_t3896396478_0_0_0;
static const Il2CppType* GenInst_Button_t3896396478_0_0_0_Types[] = { &Button_t3896396478_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t3896396478_0_0_0 = { 1, GenInst_Button_t3896396478_0_0_0_Types };
extern const Il2CppType RawImage_t821930207_0_0_0;
static const Il2CppType* GenInst_RawImage_t821930207_0_0_0_Types[] = { &RawImage_t821930207_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t821930207_0_0_0 = { 1, GenInst_RawImage_t821930207_0_0_0_Types };
extern const Il2CppType Scrollbar_t2601556940_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t2601556940_0_0_0_Types[] = { &Scrollbar_t2601556940_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t2601556940_0_0_0 = { 1, GenInst_Scrollbar_t2601556940_0_0_0_Types };
extern const Il2CppType InputField_t609046876_0_0_0;
static const Il2CppType* GenInst_InputField_t609046876_0_0_0_Types[] = { &InputField_t609046876_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t609046876_0_0_0 = { 1, GenInst_InputField_t609046876_0_0_0_Types };
extern const Il2CppType ScrollRect_t3606982749_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t3606982749_0_0_0_Types[] = { &ScrollRect_t3606982749_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t3606982749_0_0_0 = { 1, GenInst_ScrollRect_t3606982749_0_0_0_Types };
extern const Il2CppType Dropdown_t4201779933_0_0_0;
static const Il2CppType* GenInst_Dropdown_t4201779933_0_0_0_Types[] = { &Dropdown_t4201779933_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t4201779933_0_0_0 = { 1, GenInst_Dropdown_t4201779933_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t911782554_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t911782554_0_0_0_Types[] = { &GraphicRaycaster_t911782554_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t911782554_0_0_0 = { 1, GenInst_GraphicRaycaster_t911782554_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t3950887807_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t3950887807_0_0_0_Types[] = { &CanvasRenderer_t3950887807_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t3950887807_0_0_0 = { 1, GenInst_CanvasRenderer_t3950887807_0_0_0_Types };
extern const Il2CppType Corner_t284493240_0_0_0;
static const Il2CppType* GenInst_Corner_t284493240_0_0_0_Types[] = { &Corner_t284493240_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t284493240_0_0_0 = { 1, GenInst_Corner_t284493240_0_0_0_Types };
extern const Il2CppType Axis_t1399125956_0_0_0;
static const Il2CppType* GenInst_Axis_t1399125956_0_0_0_Types[] = { &Axis_t1399125956_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1399125956_0_0_0 = { 1, GenInst_Axis_t1399125956_0_0_0_Types };
extern const Il2CppType Constraint_t1640775616_0_0_0;
static const Il2CppType* GenInst_Constraint_t1640775616_0_0_0_Types[] = { &Constraint_t1640775616_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t1640775616_0_0_0 = { 1, GenInst_Constraint_t1640775616_0_0_0_Types };
extern const Il2CppType Type_t3063828369_0_0_0;
static const Il2CppType* GenInst_Type_t3063828369_0_0_0_Types[] = { &Type_t3063828369_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3063828369_0_0_0 = { 1, GenInst_Type_t3063828369_0_0_0_Types };
extern const Il2CppType FillMethod_t2255824731_0_0_0;
static const Il2CppType* GenInst_FillMethod_t2255824731_0_0_0_Types[] = { &FillMethod_t2255824731_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t2255824731_0_0_0 = { 1, GenInst_FillMethod_t2255824731_0_0_0_Types };
extern const Il2CppType SubmitEvent_t3081690246_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t3081690246_0_0_0_Types[] = { &SubmitEvent_t3081690246_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t3081690246_0_0_0 = { 1, GenInst_SubmitEvent_t3081690246_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2697516943_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2697516943_0_0_0_Types[] = { &OnChangeEvent_t2697516943_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2697516943_0_0_0 = { 1, GenInst_OnChangeEvent_t2697516943_0_0_0_Types };
extern const Il2CppType OnValidateInput_t3952708057_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t3952708057_0_0_0_Types[] = { &OnValidateInput_t3952708057_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t3952708057_0_0_0 = { 1, GenInst_OnValidateInput_t3952708057_0_0_0_Types };
extern const Il2CppType LineType_t2016592042_0_0_0;
static const Il2CppType* GenInst_LineType_t2016592042_0_0_0_Types[] = { &LineType_t2016592042_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2016592042_0_0_0 = { 1, GenInst_LineType_t2016592042_0_0_0_Types };
extern const Il2CppType InputType_t1602890312_0_0_0;
static const Il2CppType* GenInst_InputType_t1602890312_0_0_0_Types[] = { &InputType_t1602890312_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1602890312_0_0_0 = { 1, GenInst_InputType_t1602890312_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t2604324130_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t2604324130_0_0_0_Types[] = { &TouchScreenKeyboardType_t2604324130_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t2604324130_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t2604324130_0_0_0_Types };
extern const Il2CppType CharacterValidation_t737650598_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t737650598_0_0_0_Types[] = { &CharacterValidation_t737650598_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t737650598_0_0_0 = { 1, GenInst_CharacterValidation_t737650598_0_0_0_Types };
extern const Il2CppType LayoutElement_t1596995480_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t1596995480_0_0_0_Types[] = { &LayoutElement_t1596995480_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t1596995480_0_0_0 = { 1, GenInst_LayoutElement_t1596995480_0_0_0_Types };
extern const Il2CppType RectOffset_t3056157787_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3056157787_0_0_0_Types[] = { &RectOffset_t3056157787_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3056157787_0_0_0 = { 1, GenInst_RectOffset_t3056157787_0_0_0_Types };
extern const Il2CppType TextAnchor_t213922566_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t213922566_0_0_0_Types[] = { &TextAnchor_t213922566_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t213922566_0_0_0 = { 1, GenInst_TextAnchor_t213922566_0_0_0_Types };
extern const Il2CppType Direction_t522766867_0_0_0;
static const Il2CppType* GenInst_Direction_t522766867_0_0_0_Types[] = { &Direction_t522766867_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t522766867_0_0_0 = { 1, GenInst_Direction_t522766867_0_0_0_Types };
extern const Il2CppType Transition_t1922345195_0_0_0;
static const Il2CppType* GenInst_Transition_t1922345195_0_0_0_Types[] = { &Transition_t1922345195_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t1922345195_0_0_0 = { 1, GenInst_Transition_t1922345195_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t115197445_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t115197445_0_0_0_Types[] = { &AnimationTriggers_t115197445_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t115197445_0_0_0 = { 1, GenInst_AnimationTriggers_t115197445_0_0_0_Types };
extern const Il2CppType Animator_t2776330603_0_0_0;
static const Il2CppType* GenInst_Animator_t2776330603_0_0_0_Types[] = { &Animator_t2776330603_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t2776330603_0_0_0 = { 1, GenInst_Animator_t2776330603_0_0_0_Types };
extern const Il2CppType Direction_t94975348_0_0_0;
static const Il2CppType* GenInst_Direction_t94975348_0_0_0_Types[] = { &Direction_t94975348_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t94975348_0_0_0 = { 1, GenInst_Direction_t94975348_0_0_0_Types };
extern const Il2CppType PedidoValues_t2123607271_0_0_0;
static const Il2CppType* GenInst_PedidoValues_t2123607271_0_0_0_Types[] = { &PedidoValues_t2123607271_0_0_0 };
extern const Il2CppGenericInst GenInst_PedidoValues_t2123607271_0_0_0 = { 1, GenInst_PedidoValues_t2123607271_0_0_0_Types };
extern const Il2CppType DeviceAppStateMachineEvents_t922942516_0_0_0;
static const Il2CppType* GenInst_DeviceAppStateMachineEvents_t922942516_0_0_0_Types[] = { &DeviceAppStateMachineEvents_t922942516_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceAppStateMachineEvents_t922942516_0_0_0 = { 1, GenInst_DeviceAppStateMachineEvents_t922942516_0_0_0_Types };
extern const Il2CppType Analytics_t310950758_0_0_0;
static const Il2CppType* GenInst_Analytics_t310950758_0_0_0_Types[] = { &Analytics_t310950758_0_0_0 };
extern const Il2CppGenericInst GenInst_Analytics_t310950758_0_0_0 = { 1, GenInst_Analytics_t310950758_0_0_0_Types };
extern const Il2CppType CronWatcher_t1902937828_0_0_0;
static const Il2CppType* GenInst_CronWatcher_t1902937828_0_0_0_Types[] = { &CronWatcher_t1902937828_0_0_0 };
extern const Il2CppGenericInst GenInst_CronWatcher_t1902937828_0_0_0 = { 1, GenInst_CronWatcher_t1902937828_0_0_0_Types };
extern const Il2CppType TimeoutItem_t109981810_0_0_0;
static const Il2CppType* GenInst_TimeoutItem_t109981810_0_0_0_Types[] = { &TimeoutItem_t109981810_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeoutItem_t109981810_0_0_0 = { 1, GenInst_TimeoutItem_t109981810_0_0_0_Types };
extern const Il2CppType IntervalItem_t889137048_0_0_0;
static const Il2CppType* GenInst_IntervalItem_t889137048_0_0_0_Types[] = { &IntervalItem_t889137048_0_0_0 };
extern const Il2CppGenericInst GenInst_IntervalItem_t889137048_0_0_0 = { 1, GenInst_IntervalItem_t889137048_0_0_0_Types };
extern const Il2CppType GameObjectDownloader_t2978402602_0_0_0;
static const Il2CppType* GenInst_GameObjectDownloader_t2978402602_0_0_0_Types[] = { &GameObjectDownloader_t2978402602_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectDownloader_t2978402602_0_0_0 = { 1, GenInst_GameObjectDownloader_t2978402602_0_0_0_Types };
extern const Il2CppType SingleGenericDownloader_t168833466_0_0_0;
static const Il2CppType* GenInst_SingleGenericDownloader_t168833466_0_0_0_Types[] = { &SingleGenericDownloader_t168833466_0_0_0 };
extern const Il2CppGenericInst GenInst_SingleGenericDownloader_t168833466_0_0_0 = { 1, GenInst_SingleGenericDownloader_t168833466_0_0_0_Types };
extern const Il2CppType ARMSingleRequest_t410726329_0_0_0;
static const Il2CppType* GenInst_ARMSingleRequest_t410726329_0_0_0_Types[] = { &ARMSingleRequest_t410726329_0_0_0 };
extern const Il2CppGenericInst GenInst_ARMSingleRequest_t410726329_0_0_0 = { 1, GenInst_ARMSingleRequest_t410726329_0_0_0_Types };
extern const Il2CppType CEP_t66606_0_0_0;
static const Il2CppType* GenInst_CEP_t66606_0_0_0_Types[] = { &CEP_t66606_0_0_0 };
extern const Il2CppGenericInst GenInst_CEP_t66606_0_0_0 = { 1, GenInst_CEP_t66606_0_0_0_Types };
extern const Il2CppType CaptureAndSave_t700313070_0_0_0;
static const Il2CppType* GenInst_CaptureAndSave_t700313070_0_0_0_Types[] = { &CaptureAndSave_t700313070_0_0_0 };
extern const Il2CppGenericInst GenInst_CaptureAndSave_t700313070_0_0_0 = { 1, GenInst_CaptureAndSave_t700313070_0_0_0_Types };
extern const Il2CppType DownloadImages_t3644489024_0_0_0;
static const Il2CppType* GenInst_DownloadImages_t3644489024_0_0_0_Types[] = { &DownloadImages_t3644489024_0_0_0 };
extern const Il2CppGenericInst GenInst_DownloadImages_t3644489024_0_0_0 = { 1, GenInst_DownloadImages_t3644489024_0_0_0_Types };
extern const Il2CppType SphereCollider_t111527973_0_0_0;
static const Il2CppType* GenInst_SphereCollider_t111527973_0_0_0_Types[] = { &SphereCollider_t111527973_0_0_0 };
extern const Il2CppGenericInst GenInst_SphereCollider_t111527973_0_0_0 = { 1, GenInst_SphereCollider_t111527973_0_0_0_Types };
extern const Il2CppType CapsuleCollider_t318617463_0_0_0;
static const Il2CppType* GenInst_CapsuleCollider_t318617463_0_0_0_Types[] = { &CapsuleCollider_t318617463_0_0_0 };
extern const Il2CppGenericInst GenInst_CapsuleCollider_t318617463_0_0_0 = { 1, GenInst_CapsuleCollider_t318617463_0_0_0_Types };
extern const Il2CppType InfiniteHorizontalScroll_t3122519653_0_0_0;
static const Il2CppType* GenInst_InfiniteHorizontalScroll_t3122519653_0_0_0_Types[] = { &InfiniteHorizontalScroll_t3122519653_0_0_0 };
extern const Il2CppGenericInst GenInst_InfiniteHorizontalScroll_t3122519653_0_0_0 = { 1, GenInst_InfiniteHorizontalScroll_t3122519653_0_0_0_Types };
extern const Il2CppType CarneValues_t2819179885_0_0_0;
static const Il2CppType* GenInst_CarneValues_t2819179885_0_0_0_Types[] = { &CarneValues_t2819179885_0_0_0 };
extern const Il2CppGenericInst GenInst_CarneValues_t2819179885_0_0_0 = { 1, GenInst_CarneValues_t2819179885_0_0_0_Types };
extern const Il2CppType QRCodeEncodeController_t2317858336_0_0_0;
static const Il2CppType* GenInst_QRCodeEncodeController_t2317858336_0_0_0_Types[] = { &QRCodeEncodeController_t2317858336_0_0_0 };
extern const Il2CppGenericInst GenInst_QRCodeEncodeController_t2317858336_0_0_0 = { 1, GenInst_QRCodeEncodeController_t2317858336_0_0_0_Types };
extern const Il2CppType Cupons_t2029651862_0_0_0;
static const Il2CppType* GenInst_Cupons_t2029651862_0_0_0_Types[] = { &Cupons_t2029651862_0_0_0 };
extern const Il2CppGenericInst GenInst_Cupons_t2029651862_0_0_0 = { 1, GenInst_Cupons_t2029651862_0_0_0_Types };
extern const Il2CppType UserData_t4092646965_0_0_0;
static const Il2CppType* GenInst_UserData_t4092646965_0_0_0_Types[] = { &UserData_t4092646965_0_0_0 };
extern const Il2CppGenericInst GenInst_UserData_t4092646965_0_0_0 = { 1, GenInst_UserData_t4092646965_0_0_0_Types };
extern const Il2CppType ParticleAutoDestruction_t2314186749_0_0_0;
static const Il2CppType* GenInst_ParticleAutoDestruction_t2314186749_0_0_0_Types[] = { &ParticleAutoDestruction_t2314186749_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleAutoDestruction_t2314186749_0_0_0 = { 1, GenInst_ParticleAutoDestruction_t2314186749_0_0_0_Types };
extern const Il2CppType IncrementalSliderController_t153649975_0_0_0;
static const Il2CppType* GenInst_IncrementalSliderController_t153649975_0_0_0_Types[] = { &IncrementalSliderController_t153649975_0_0_0 };
extern const Il2CppGenericInst GenInst_IncrementalSliderController_t153649975_0_0_0 = { 1, GenInst_IncrementalSliderController_t153649975_0_0_0_Types };
extern const Il2CppType CameraPlaneController_t4203072787_0_0_0;
static const Il2CppType* GenInst_CameraPlaneController_t4203072787_0_0_0_Types[] = { &CameraPlaneController_t4203072787_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraPlaneController_t4203072787_0_0_0 = { 1, GenInst_CameraPlaneController_t4203072787_0_0_0_Types };
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_MetadataVO_t2511256998_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &MetadataVO_t2511256998_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_MetadataVO_t2511256998_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_MetadataVO_t2511256998_0_0_0_Types };
extern const Il2CppType DragMe_t2055054860_0_0_0;
static const Il2CppType* GenInst_DragMe_t2055054860_0_0_0_Types[] = { &DragMe_t2055054860_0_0_0 };
extern const Il2CppGenericInst GenInst_DragMe_t2055054860_0_0_0 = { 1, GenInst_DragMe_t2055054860_0_0_0_Types };
extern const Il2CppType IDictionary_2_t274140790_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t274140790_0_0_0_Types[] = { &IDictionary_2_t274140790_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t274140790_0_0_0 = { 1, GenInst_IDictionary_2_t274140790_0_0_0_Types };
static const Il2CppType* GenInst_IEnumerable_1_t3176762032_0_0_0_Types[] = { &IEnumerable_1_t3176762032_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3176762032_0_0_0 = { 1, GenInst_IEnumerable_1_t3176762032_0_0_0_Types };
extern const Il2CppType AsyncRequestString_t2872237476_0_0_0;
static const Il2CppType* GenInst_AsyncRequestString_t2872237476_0_0_0_Types[] = { &AsyncRequestString_t2872237476_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncRequestString_t2872237476_0_0_0 = { 1, GenInst_AsyncRequestString_t2872237476_0_0_0_Types };
extern const Il2CppType JsBridge_t2292005674_0_0_0;
static const Il2CppType* GenInst_JsBridge_t2292005674_0_0_0_Types[] = { &JsBridge_t2292005674_0_0_0 };
extern const Il2CppGenericInst GenInst_JsBridge_t2292005674_0_0_0 = { 1, GenInst_JsBridge_t2292005674_0_0_0_Types };
extern const Il2CppType CanvasFacebookGameObject_t2832912295_0_0_0;
static const Il2CppType* GenInst_CanvasFacebookGameObject_t2832912295_0_0_0_Types[] = { &CanvasFacebookGameObject_t2832912295_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasFacebookGameObject_t2832912295_0_0_0 = { 1, GenInst_CanvasFacebookGameObject_t2832912295_0_0_0_Types };
extern const Il2CppType EditorFacebookGameObject_t3558123006_0_0_0;
static const Il2CppType* GenInst_EditorFacebookGameObject_t3558123006_0_0_0_Types[] = { &EditorFacebookGameObject_t3558123006_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebookGameObject_t3558123006_0_0_0 = { 1, GenInst_EditorFacebookGameObject_t3558123006_0_0_0_Types };
extern const Il2CppType MockLoginDialog_t760013837_0_0_0;
static const Il2CppType* GenInst_MockLoginDialog_t760013837_0_0_0_Types[] = { &MockLoginDialog_t760013837_0_0_0 };
extern const Il2CppGenericInst GenInst_MockLoginDialog_t760013837_0_0_0 = { 1, GenInst_MockLoginDialog_t760013837_0_0_0_Types };
extern const Il2CppType MockShareDialog_t3804468227_0_0_0;
static const Il2CppType* GenInst_MockShareDialog_t3804468227_0_0_0_Types[] = { &MockShareDialog_t3804468227_0_0_0 };
extern const Il2CppGenericInst GenInst_MockShareDialog_t3804468227_0_0_0 = { 1, GenInst_MockShareDialog_t3804468227_0_0_0_Types };
extern const Il2CppType FacebookSettings_t3725355145_0_0_0;
static const Il2CppType* GenInst_FacebookSettings_t3725355145_0_0_0_Types[] = { &FacebookSettings_t3725355145_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookSettings_t3725355145_0_0_0 = { 1, GenInst_FacebookSettings_t3725355145_0_0_0_Types };
extern const Il2CppType EditorFacebookLoader_t2301290944_0_0_0;
static const Il2CppType* GenInst_EditorFacebookLoader_t2301290944_0_0_0_Types[] = { &EditorFacebookLoader_t2301290944_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebookLoader_t2301290944_0_0_0 = { 1, GenInst_EditorFacebookLoader_t2301290944_0_0_0_Types };
extern const Il2CppType CanvasFacebookLoader_t3297174377_0_0_0;
static const Il2CppType* GenInst_CanvasFacebookLoader_t3297174377_0_0_0_Types[] = { &CanvasFacebookLoader_t3297174377_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasFacebookLoader_t3297174377_0_0_0 = { 1, GenInst_CanvasFacebookLoader_t3297174377_0_0_0_Types };
extern const Il2CppType IOSFacebookLoader_t562470544_0_0_0;
static const Il2CppType* GenInst_IOSFacebookLoader_t562470544_0_0_0_Types[] = { &IOSFacebookLoader_t562470544_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebookLoader_t562470544_0_0_0 = { 1, GenInst_IOSFacebookLoader_t562470544_0_0_0_Types };
extern const Il2CppType AndroidFacebookLoader_t148490282_0_0_0;
static const Il2CppType* GenInst_AndroidFacebookLoader_t148490282_0_0_0_Types[] = { &AndroidFacebookLoader_t148490282_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidFacebookLoader_t148490282_0_0_0 = { 1, GenInst_AndroidFacebookLoader_t148490282_0_0_0_Types };
extern const Il2CppType AndroidFacebookGameObject_t2212214888_0_0_0;
static const Il2CppType* GenInst_AndroidFacebookGameObject_t2212214888_0_0_0_Types[] = { &AndroidFacebookGameObject_t2212214888_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidFacebookGameObject_t2212214888_0_0_0 = { 1, GenInst_AndroidFacebookGameObject_t2212214888_0_0_0_Types };
extern const Il2CppType IOSFacebookGameObject_t4031893710_0_0_0;
static const Il2CppType* GenInst_IOSFacebookGameObject_t4031893710_0_0_0_Types[] = { &IOSFacebookGameObject_t4031893710_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebookGameObject_t4031893710_0_0_0 = { 1, GenInst_IOSFacebookGameObject_t4031893710_0_0_0_Types };
extern const Il2CppType FacebookData_t4095115184_0_0_0;
static const Il2CppType* GenInst_FacebookData_t4095115184_0_0_0_Types[] = { &FacebookData_t4095115184_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookData_t4095115184_0_0_0 = { 1, GenInst_FacebookData_t4095115184_0_0_0_Types };
extern const Il2CppType ItemMoveDrag_t687865080_0_0_0;
static const Il2CppType* GenInst_ItemMoveDrag_t687865080_0_0_0_Types[] = { &ItemMoveDrag_t687865080_0_0_0 };
extern const Il2CppGenericInst GenInst_ItemMoveDrag_t687865080_0_0_0 = { 1, GenInst_ItemMoveDrag_t687865080_0_0_0_Types };
extern const Il2CppType PublicationsData_t1837633585_0_0_0;
static const Il2CppType* GenInst_PublicationsData_t1837633585_0_0_0_Types[] = { &PublicationsData_t1837633585_0_0_0 };
extern const Il2CppGenericInst GenInst_PublicationsData_t1837633585_0_0_0 = { 1, GenInst_PublicationsData_t1837633585_0_0_0_Types };
extern const Il2CppType LightsControl_t2444629088_0_0_0;
static const Il2CppType* GenInst_LightsControl_t2444629088_0_0_0_Types[] = { &LightsControl_t2444629088_0_0_0 };
extern const Il2CppGenericInst GenInst_LightsControl_t2444629088_0_0_0 = { 1, GenInst_LightsControl_t2444629088_0_0_0_Types };
extern const Il2CppType ServerControl_t2725829754_0_0_0;
static const Il2CppType* GenInst_ServerControl_t2725829754_0_0_0_Types[] = { &ServerControl_t2725829754_0_0_0 };
extern const Il2CppGenericInst GenInst_ServerControl_t2725829754_0_0_0 = { 1, GenInst_ServerControl_t2725829754_0_0_0_Types };
extern const Il2CppType PlayMakerAnimatorMoveProxy_t4175490694_0_0_0;
static const Il2CppType* GenInst_PlayMakerAnimatorMoveProxy_t4175490694_0_0_0_Types[] = { &PlayMakerAnimatorMoveProxy_t4175490694_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerAnimatorMoveProxy_t4175490694_0_0_0 = { 1, GenInst_PlayMakerAnimatorMoveProxy_t4175490694_0_0_0_Types };
extern const Il2CppType CharacterController_t1618060635_0_0_0;
static const Il2CppType* GenInst_CharacterController_t1618060635_0_0_0_Types[] = { &CharacterController_t1618060635_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterController_t1618060635_0_0_0 = { 1, GenInst_CharacterController_t1618060635_0_0_0_Types };
extern const Il2CppType playMakerShurikenProxy_t1235561697_0_0_0;
static const Il2CppType* GenInst_playMakerShurikenProxy_t1235561697_0_0_0_Types[] = { &playMakerShurikenProxy_t1235561697_0_0_0 };
extern const Il2CppGenericInst GenInst_playMakerShurikenProxy_t1235561697_0_0_0 = { 1, GenInst_playMakerShurikenProxy_t1235561697_0_0_0_Types };
extern const Il2CppType iTweenFSMEvents_t871409943_0_0_0;
static const Il2CppType* GenInst_iTweenFSMEvents_t871409943_0_0_0_Types[] = { &iTweenFSMEvents_t871409943_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenFSMEvents_t871409943_0_0_0 = { 1, GenInst_iTweenFSMEvents_t871409943_0_0_0_Types };
extern const Il2CppType NavMeshAgent_t588466745_0_0_0;
static const Il2CppType* GenInst_NavMeshAgent_t588466745_0_0_0_Types[] = { &NavMeshAgent_t588466745_0_0_0 };
extern const Il2CppGenericInst GenInst_NavMeshAgent_t588466745_0_0_0 = { 1, GenInst_NavMeshAgent_t588466745_0_0_0_Types };
extern const Il2CppType PlayMakerAnimatorIKProxy_t1024752181_0_0_0;
static const Il2CppType* GenInst_PlayMakerAnimatorIKProxy_t1024752181_0_0_0_Types[] = { &PlayMakerAnimatorIKProxy_t1024752181_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerAnimatorIKProxy_t1024752181_0_0_0 = { 1, GenInst_PlayMakerAnimatorIKProxy_t1024752181_0_0_0_Types };
extern const Il2CppType Joint_t4201008640_0_0_0;
static const Il2CppType* GenInst_Joint_t4201008640_0_0_0_Types[] = { &Joint_t4201008640_0_0_0 };
extern const Il2CppGenericInst GenInst_Joint_t4201008640_0_0_0 = { 1, GenInst_Joint_t4201008640_0_0_0_Types };
extern const Il2CppType HorizontalLayoutGroup_t1336501463_0_0_0;
static const Il2CppType* GenInst_HorizontalLayoutGroup_t1336501463_0_0_0_Types[] = { &HorizontalLayoutGroup_t1336501463_0_0_0 };
extern const Il2CppGenericInst GenInst_HorizontalLayoutGroup_t1336501463_0_0_0 = { 1, GenInst_HorizontalLayoutGroup_t1336501463_0_0_0_Types };
extern const Il2CppType GridLayoutGroup_t169317941_0_0_0;
static const Il2CppType* GenInst_GridLayoutGroup_t169317941_0_0_0_Types[] = { &GridLayoutGroup_t169317941_0_0_0 };
extern const Il2CppGenericInst GenInst_GridLayoutGroup_t169317941_0_0_0 = { 1, GenInst_GridLayoutGroup_t169317941_0_0_0_Types };
extern const Il2CppType iTween_t3087282050_0_0_0;
static const Il2CppType* GenInst_iTween_t3087282050_0_0_0_Types[] = { &iTween_t3087282050_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t3087282050_0_0_0 = { 1, GenInst_iTween_t3087282050_0_0_0_Types };
extern const Il2CppType Loom_t2374337_0_0_0;
static const Il2CppType* GenInst_Loom_t2374337_0_0_0_Types[] = { &Loom_t2374337_0_0_0 };
extern const Il2CppGenericInst GenInst_Loom_t2374337_0_0_0 = { 1, GenInst_Loom_t2374337_0_0_0_Types };
extern const Il2CppType BulkBundleDownloader_t2985631149_0_0_0;
static const Il2CppType* GenInst_BulkBundleDownloader_t2985631149_0_0_0_Types[] = { &BulkBundleDownloader_t2985631149_0_0_0 };
extern const Il2CppGenericInst GenInst_BulkBundleDownloader_t2985631149_0_0_0 = { 1, GenInst_BulkBundleDownloader_t2985631149_0_0_0_Types };
extern const Il2CppType GenericDownloadManager_t3298305714_0_0_0;
static const Il2CppType* GenInst_GenericDownloadManager_t3298305714_0_0_0_Types[] = { &GenericDownloadManager_t3298305714_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericDownloadManager_t3298305714_0_0_0 = { 1, GenInst_GenericDownloadManager_t3298305714_0_0_0_Types };
extern const Il2CppType ResultRevisionVO_t2445597391_0_0_0;
static const Il2CppType* GenInst_ResultRevisionVO_t2445597391_0_0_0_Types[] = { &ResultRevisionVO_t2445597391_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultRevisionVO_t2445597391_0_0_0 = { 1, GenInst_ResultRevisionVO_t2445597391_0_0_0_Types };
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_UrlInfoVO_t1761987528_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &UrlInfoVO_t1761987528_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_UrlInfoVO_t1761987528_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_UrlInfoVO_t1761987528_0_0_0_Types };
static const Il2CppType* GenInst_BundleVO_t1984518073_0_0_0_FileVO_t1935348659_0_0_0_Types[] = { &BundleVO_t1984518073_0_0_0, &FileVO_t1935348659_0_0_0 };
extern const Il2CppGenericInst GenInst_BundleVO_t1984518073_0_0_0_FileVO_t1935348659_0_0_0 = { 2, GenInst_BundleVO_t1984518073_0_0_0_FileVO_t1935348659_0_0_0_Types };
static const Il2CppType* GenInst_FileVO_t1935348659_0_0_0_UrlInfoVO_t1761987528_0_0_0_Types[] = { &FileVO_t1935348659_0_0_0, &UrlInfoVO_t1761987528_0_0_0 };
extern const Il2CppGenericInst GenInst_FileVO_t1935348659_0_0_0_UrlInfoVO_t1761987528_0_0_0 = { 2, GenInst_FileVO_t1935348659_0_0_0_UrlInfoVO_t1761987528_0_0_0_Types };
extern const Il2CppType AmIVisible_t1175015477_0_0_0;
static const Il2CppType* GenInst_AmIVisible_t1175015477_0_0_0_Types[] = { &AmIVisible_t1175015477_0_0_0 };
extern const Il2CppGenericInst GenInst_AmIVisible_t1175015477_0_0_0 = { 1, GenInst_AmIVisible_t1175015477_0_0_0_Types };
static const Il2CppType* GenInst_GroupInAppAbstractComponentsManager_t739334794_0_0_0_Types[] = { &GroupInAppAbstractComponentsManager_t739334794_0_0_0 };
extern const Il2CppGenericInst GenInst_GroupInAppAbstractComponentsManager_t739334794_0_0_0 = { 1, GenInst_GroupInAppAbstractComponentsManager_t739334794_0_0_0_Types };
extern const Il2CppType LoadLevelName_t284939721_0_0_0;
static const Il2CppType* GenInst_LoadLevelName_t284939721_0_0_0_Types[] = { &LoadLevelName_t284939721_0_0_0 };
extern const Il2CppGenericInst GenInst_LoadLevelName_t284939721_0_0_0 = { 1, GenInst_LoadLevelName_t284939721_0_0_0_Types };
extern const Il2CppType ARMVideoPlaybackBehaviour_t1131738883_0_0_0;
static const Il2CppType* GenInst_ARMVideoPlaybackBehaviour_t1131738883_0_0_0_Types[] = { &ARMVideoPlaybackBehaviour_t1131738883_0_0_0 };
extern const Il2CppGenericInst GenInst_ARMVideoPlaybackBehaviour_t1131738883_0_0_0 = { 1, GenInst_ARMVideoPlaybackBehaviour_t1131738883_0_0_0_Types };
extern const Il2CppType ServerRequestMobile_t675100796_0_0_0;
static const Il2CppType* GenInst_ServerRequestMobile_t675100796_0_0_0_Types[] = { &ServerRequestMobile_t675100796_0_0_0 };
extern const Il2CppGenericInst GenInst_ServerRequestMobile_t675100796_0_0_0 = { 1, GenInst_ServerRequestMobile_t675100796_0_0_0_Types };
extern const Il2CppType RevisionInfoPingVO_t4026113458_0_0_0;
static const Il2CppType* GenInst_RevisionInfoPingVO_t4026113458_0_0_0_Types[] = { &RevisionInfoPingVO_t4026113458_0_0_0 };
extern const Il2CppGenericInst GenInst_RevisionInfoPingVO_t4026113458_0_0_0 = { 1, GenInst_RevisionInfoPingVO_t4026113458_0_0_0_Types };
extern const Il2CppType RevisionInfoVO_t1983749152_0_0_0;
static const Il2CppType* GenInst_RevisionInfoVO_t1983749152_0_0_0_Types[] = { &RevisionInfoVO_t1983749152_0_0_0 };
extern const Il2CppGenericInst GenInst_RevisionInfoVO_t1983749152_0_0_0 = { 1, GenInst_RevisionInfoVO_t1983749152_0_0_0_Types };
extern const Il2CppType KeyTypeValueU5BU5D_t1961483187_0_0_0;
static const Il2CppType* GenInst_KeyTypeValueU5BU5D_t1961483187_0_0_0_Types[] = { &KeyTypeValueU5BU5D_t1961483187_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyTypeValueU5BU5D_t1961483187_0_0_0 = { 1, GenInst_KeyTypeValueU5BU5D_t1961483187_0_0_0_Types };
extern const Il2CppType ButtonAnimationSoundScript_t4202863368_0_0_0;
static const Il2CppType* GenInst_ButtonAnimationSoundScript_t4202863368_0_0_0_Types[] = { &ButtonAnimationSoundScript_t4202863368_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonAnimationSoundScript_t4202863368_0_0_0 = { 1, GenInst_ButtonAnimationSoundScript_t4202863368_0_0_0_Types };
extern const Il2CppType MeuPedidoData_t2688356908_0_0_0;
static const Il2CppType* GenInst_MeuPedidoData_t2688356908_0_0_0_Types[] = { &MeuPedidoData_t2688356908_0_0_0 };
extern const Il2CppGenericInst GenInst_MeuPedidoData_t2688356908_0_0_0 = { 1, GenInst_MeuPedidoData_t2688356908_0_0_0_Types };
extern const Il2CppType OfertasData_t2909397772_0_0_0;
static const Il2CppType* GenInst_OfertasData_t2909397772_0_0_0_Types[] = { &OfertasData_t2909397772_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertasData_t2909397772_0_0_0 = { 1, GenInst_OfertasData_t2909397772_0_0_0_Types };
extern const Il2CppType MNAndroidDialog_t2972443766_0_0_0;
static const Il2CppType* GenInst_MNAndroidDialog_t2972443766_0_0_0_Types[] = { &MNAndroidDialog_t2972443766_0_0_0 };
extern const Il2CppGenericInst GenInst_MNAndroidDialog_t2972443766_0_0_0 = { 1, GenInst_MNAndroidDialog_t2972443766_0_0_0_Types };
extern const Il2CppType MNAndroidMessage_t1251344025_0_0_0;
static const Il2CppType* GenInst_MNAndroidMessage_t1251344025_0_0_0_Types[] = { &MNAndroidMessage_t1251344025_0_0_0 };
extern const Il2CppGenericInst GenInst_MNAndroidMessage_t1251344025_0_0_0 = { 1, GenInst_MNAndroidMessage_t1251344025_0_0_0_Types };
extern const Il2CppType MNAndroidRateUsPopUp_t2446611808_0_0_0;
static const Il2CppType* GenInst_MNAndroidRateUsPopUp_t2446611808_0_0_0_Types[] = { &MNAndroidRateUsPopUp_t2446611808_0_0_0 };
extern const Il2CppGenericInst GenInst_MNAndroidRateUsPopUp_t2446611808_0_0_0 = { 1, GenInst_MNAndroidRateUsPopUp_t2446611808_0_0_0_Types };
extern const Il2CppType MNIOSDialog_t768531220_0_0_0;
static const Il2CppType* GenInst_MNIOSDialog_t768531220_0_0_0_Types[] = { &MNIOSDialog_t768531220_0_0_0 };
extern const Il2CppGenericInst GenInst_MNIOSDialog_t768531220_0_0_0 = { 1, GenInst_MNIOSDialog_t768531220_0_0_0_Types };
extern const Il2CppType MNIOSMessage_t1649531835_0_0_0;
static const Il2CppType* GenInst_MNIOSMessage_t1649531835_0_0_0_Types[] = { &MNIOSMessage_t1649531835_0_0_0 };
extern const Il2CppGenericInst GenInst_MNIOSMessage_t1649531835_0_0_0 = { 1, GenInst_MNIOSMessage_t1649531835_0_0_0_Types };
extern const Il2CppType MNIOSRateUsPopUp_t2151207298_0_0_0;
static const Il2CppType* GenInst_MNIOSRateUsPopUp_t2151207298_0_0_0_Types[] = { &MNIOSRateUsPopUp_t2151207298_0_0_0 };
extern const Il2CppGenericInst GenInst_MNIOSRateUsPopUp_t2151207298_0_0_0 = { 1, GenInst_MNIOSRateUsPopUp_t2151207298_0_0_0_Types };
extern const Il2CppType MNWP8Dialog_t489019942_0_0_0;
static const Il2CppType* GenInst_MNWP8Dialog_t489019942_0_0_0_Types[] = { &MNWP8Dialog_t489019942_0_0_0 };
extern const Il2CppGenericInst GenInst_MNWP8Dialog_t489019942_0_0_0 = { 1, GenInst_MNWP8Dialog_t489019942_0_0_0_Types };
extern const Il2CppType MNWP8Message_t1574616809_0_0_0;
static const Il2CppType* GenInst_MNWP8Message_t1574616809_0_0_0_Types[] = { &MNWP8Message_t1574616809_0_0_0 };
extern const Il2CppGenericInst GenInst_MNWP8Message_t1574616809_0_0_0 = { 1, GenInst_MNWP8Message_t1574616809_0_0_0_Types };
extern const Il2CppType MNWP8RateUsPopUp_t4179652016_0_0_0;
static const Il2CppType* GenInst_MNWP8RateUsPopUp_t4179652016_0_0_0_Types[] = { &MNWP8RateUsPopUp_t4179652016_0_0_0 };
extern const Il2CppGenericInst GenInst_MNWP8RateUsPopUp_t4179652016_0_0_0 = { 1, GenInst_MNWP8RateUsPopUp_t4179652016_0_0_0_Types };
extern const Il2CppType NativeImagePickerBehaviour_t3828351720_0_0_0;
static const Il2CppType* GenInst_NativeImagePickerBehaviour_t3828351720_0_0_0_Types[] = { &NativeImagePickerBehaviour_t3828351720_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeImagePickerBehaviour_t3828351720_0_0_0 = { 1, GenInst_NativeImagePickerBehaviour_t3828351720_0_0_0_Types };
extern const Il2CppType NotificationObj_t1964455916_0_0_0;
static const Il2CppType* GenInst_NotificationObj_t1964455916_0_0_0_Types[] = { &NotificationObj_t1964455916_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationObj_t1964455916_0_0_0 = { 1, GenInst_NotificationObj_t1964455916_0_0_0_Types };
extern const Il2CppType OfertasCRMDataServer_t217840361_0_0_0;
static const Il2CppType* GenInst_OfertasCRMDataServer_t217840361_0_0_0_Types[] = { &OfertasCRMDataServer_t217840361_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertasCRMDataServer_t217840361_0_0_0 = { 1, GenInst_OfertasCRMDataServer_t217840361_0_0_0_Types };
extern const Il2CppType OfertasCRMDataExclusivas_t2158992831_0_0_0;
static const Il2CppType* GenInst_OfertasCRMDataExclusivas_t2158992831_0_0_0_Types[] = { &OfertasCRMDataExclusivas_t2158992831_0_0_0 };
extern const Il2CppGenericInst GenInst_OfertasCRMDataExclusivas_t2158992831_0_0_0 = { 1, GenInst_OfertasCRMDataExclusivas_t2158992831_0_0_0_Types };
extern const Il2CppType OneSignal_t3595519118_0_0_0;
static const Il2CppType* GenInst_OneSignal_t3595519118_0_0_0_Types[] = { &OneSignal_t3595519118_0_0_0 };
extern const Il2CppGenericInst GenInst_OneSignal_t3595519118_0_0_0 = { 1, GenInst_OneSignal_t3595519118_0_0_0_Types };
extern const Il2CppType AcougueData_t537282265_0_0_0;
static const Il2CppType* GenInst_AcougueData_t537282265_0_0_0_Types[] = { &AcougueData_t537282265_0_0_0 };
extern const Il2CppGenericInst GenInst_AcougueData_t537282265_0_0_0 = { 1, GenInst_AcougueData_t537282265_0_0_0_Types };
extern const Il2CppType DeviceCameraController_t2043460407_0_0_0;
static const Il2CppType* GenInst_DeviceCameraController_t2043460407_0_0_0_Types[] = { &DeviceCameraController_t2043460407_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceCameraController_t2043460407_0_0_0 = { 1, GenInst_DeviceCameraController_t2043460407_0_0_0_Types };
extern const Il2CppType SampleInitErrorHandler_t2792208188_0_0_0;
static const Il2CppType* GenInst_SampleInitErrorHandler_t2792208188_0_0_0_Types[] = { &SampleInitErrorHandler_t2792208188_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleInitErrorHandler_t2792208188_0_0_0 = { 1, GenInst_SampleInitErrorHandler_t2792208188_0_0_0_Types };
extern const Il2CppType Screenshot_t1577017734_0_0_0;
static const Il2CppType* GenInst_Screenshot_t1577017734_0_0_0_Types[] = { &Screenshot_t1577017734_0_0_0 };
extern const Il2CppGenericInst GenInst_Screenshot_t1577017734_0_0_0 = { 1, GenInst_Screenshot_t1577017734_0_0_0_Types };
extern const Il2CppType ScreenshotManager_t4227900231_0_0_0;
static const Il2CppType* GenInst_ScreenshotManager_t4227900231_0_0_0_Types[] = { &ScreenshotManager_t4227900231_0_0_0 };
extern const Il2CppGenericInst GenInst_ScreenshotManager_t4227900231_0_0_0 = { 1, GenInst_ScreenshotManager_t4227900231_0_0_0_Types };
extern const Il2CppType GameMain_t2590236907_0_0_0;
static const Il2CppType* GenInst_GameMain_t2590236907_0_0_0_Types[] = { &GameMain_t2590236907_0_0_0 };
extern const Il2CppGenericInst GenInst_GameMain_t2590236907_0_0_0 = { 1, GenInst_GameMain_t2590236907_0_0_0_Types };
extern const Il2CppType JsonPlay_t2384567388_0_0_0;
static const Il2CppType* GenInst_JsonPlay_t2384567388_0_0_0_Types[] = { &JsonPlay_t2384567388_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonPlay_t2384567388_0_0_0 = { 1, GenInst_JsonPlay_t2384567388_0_0_0_Types };
extern const Il2CppType TabloideValues_t99048372_0_0_0;
static const Il2CppType* GenInst_TabloideValues_t99048372_0_0_0_Types[] = { &TabloideValues_t99048372_0_0_0 };
extern const Il2CppGenericInst GenInst_TabloideValues_t99048372_0_0_0 = { 1, GenInst_TabloideValues_t99048372_0_0_0_Types };
extern const Il2CppType TabloidesData_t2107312651_0_0_0;
static const Il2CppType* GenInst_TabloidesData_t2107312651_0_0_0_Types[] = { &TabloidesData_t2107312651_0_0_0 };
extern const Il2CppGenericInst GenInst_TabloidesData_t2107312651_0_0_0 = { 1, GenInst_TabloidesData_t2107312651_0_0_0_Types };
extern const Il2CppType Corner_t4192955334_0_0_0;
static const Il2CppType* GenInst_Corner_t4192955334_0_0_0_Types[] = { &Corner_t4192955334_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t4192955334_0_0_0 = { 1, GenInst_Corner_t4192955334_0_0_0_Types };
extern const Il2CppType Direction_t361841815_0_0_0;
static const Il2CppType* GenInst_Direction_t361841815_0_0_0_Types[] = { &Direction_t361841815_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t361841815_0_0_0 = { 1, GenInst_Direction_t361841815_0_0_0_Types };
static const Il2CppType* GenInst_UniWebViewNativeListener_t795571060_0_0_0_Types[] = { &UniWebViewNativeListener_t795571060_0_0_0 };
extern const Il2CppGenericInst GenInst_UniWebViewNativeListener_t795571060_0_0_0 = { 1, GenInst_UniWebViewNativeListener_t795571060_0_0_0_Types };
extern const Il2CppType barGraphController_t2598333175_0_0_0;
static const Il2CppType* GenInst_barGraphController_t2598333175_0_0_0_Types[] = { &barGraphController_t2598333175_0_0_0 };
extern const Il2CppGenericInst GenInst_barGraphController_t2598333175_0_0_0 = { 1, GenInst_barGraphController_t2598333175_0_0_0_Types };
extern const Il2CppType Extrato_t1617908080_0_0_0;
static const Il2CppType* GenInst_Extrato_t1617908080_0_0_0_Types[] = { &Extrato_t1617908080_0_0_0 };
extern const Il2CppGenericInst GenInst_Extrato_t1617908080_0_0_0 = { 1, GenInst_Extrato_t1617908080_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t3695833539_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t3695833539_0_0_0_Types[] = { &ReconstructionBehaviour_t3695833539_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t3695833539_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t3695833539_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t2489055709_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t2489055709_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t1845338141_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t1845338141_0_0_0_Types[] = { &VuforiaBehaviour_t1845338141_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t1845338141_0_0_0 = { 1, GenInst_VuforiaBehaviour_t1845338141_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t1204933533_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t1204933533_0_0_0_Types[] = { &MaskOutBehaviour_t1204933533_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t1204933533_0_0_0 = { 1, GenInst_MaskOutBehaviour_t1204933533_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t2253448098_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t2253448098_0_0_0_Types[] = { &VirtualButtonBehaviour_t2253448098_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t2253448098_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t2253448098_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t1278464685_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t1278464685_0_0_0_Types[] = { &TurnOffBehaviour_t1278464685_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t1278464685_0_0_0 = { 1, GenInst_TurnOffBehaviour_t1278464685_0_0_0_Types };
extern const Il2CppType MarkerBehaviour_t3170674893_0_0_0;
static const Il2CppType* GenInst_MarkerBehaviour_t3170674893_0_0_0_Types[] = { &MarkerBehaviour_t3170674893_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t3170674893_0_0_0 = { 1, GenInst_MarkerBehaviour_t3170674893_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t2222860085_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t2222860085_0_0_0_Types[] = { &MultiTargetBehaviour_t2222860085_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t2222860085_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t2222860085_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t1850077856_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t1850077856_0_0_0_Types[] = { &CylinderTargetBehaviour_t1850077856_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t1850077856_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t1850077856_0_0_0_Types };
extern const Il2CppType WordBehaviour_t2034767101_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t2034767101_0_0_0_Types[] = { &WordBehaviour_t2034767101_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t2034767101_0_0_0 = { 1, GenInst_WordBehaviour_t2034767101_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t892056475_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t892056475_0_0_0_Types[] = { &TextRecoBehaviour_t892056475_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t892056475_0_0_0 = { 1, GenInst_TextRecoBehaviour_t892056475_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t2508606743_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t2508606743_0_0_0_Types[] = { &ObjectTargetBehaviour_t2508606743_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t2508606743_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t2508606743_0_0_0_Types };
extern const Il2CppType MagicTVWindowInAppGUIControllerScript_t4145218373_0_0_0;
static const Il2CppType* GenInst_MagicTVWindowInAppGUIControllerScript_t4145218373_0_0_0_Types[] = { &MagicTVWindowInAppGUIControllerScript_t4145218373_0_0_0 };
extern const Il2CppGenericInst GenInst_MagicTVWindowInAppGUIControllerScript_t4145218373_0_0_0 = { 1, GenInst_MagicTVWindowInAppGUIControllerScript_t4145218373_0_0_0_Types };
extern const Il2CppType MagicTVAbstractWindowPopUpControllerScript_t2050906514_0_0_0;
static const Il2CppType* GenInst_MagicTVAbstractWindowPopUpControllerScript_t2050906514_0_0_0_Types[] = { &MagicTVAbstractWindowPopUpControllerScript_t2050906514_0_0_0 };
extern const Il2CppGenericInst GenInst_MagicTVAbstractWindowPopUpControllerScript_t2050906514_0_0_0 = { 1, GenInst_MagicTVAbstractWindowPopUpControllerScript_t2050906514_0_0_0_Types };
extern const Il2CppType MagicTVWindowAlertControllerScript_t1344644260_0_0_0;
static const Il2CppType* GenInst_MagicTVWindowAlertControllerScript_t1344644260_0_0_0_Types[] = { &MagicTVWindowAlertControllerScript_t1344644260_0_0_0 };
extern const Il2CppGenericInst GenInst_MagicTVWindowAlertControllerScript_t1344644260_0_0_0 = { 1, GenInst_MagicTVWindowAlertControllerScript_t1344644260_0_0_0_Types };
extern const Il2CppType MagicTVWindowConfirmControllerScript_t165107144_0_0_0;
static const Il2CppType* GenInst_MagicTVWindowConfirmControllerScript_t165107144_0_0_0_Types[] = { &MagicTVWindowConfirmControllerScript_t165107144_0_0_0 };
extern const Il2CppGenericInst GenInst_MagicTVWindowConfirmControllerScript_t165107144_0_0_0 = { 1, GenInst_MagicTVWindowConfirmControllerScript_t165107144_0_0_0_Types };
extern const Il2CppType MagicTVWindowPromptControllerScript_t1472770442_0_0_0;
static const Il2CppType* GenInst_MagicTVWindowPromptControllerScript_t1472770442_0_0_0_Types[] = { &MagicTVWindowPromptControllerScript_t1472770442_0_0_0 };
extern const Il2CppGenericInst GenInst_MagicTVWindowPromptControllerScript_t1472770442_0_0_0 = { 1, GenInst_MagicTVWindowPromptControllerScript_t1472770442_0_0_0_Types };
extern const Il2CppType EmptyMockDialog_t1261978245_0_0_0;
static const Il2CppType* GenInst_EmptyMockDialog_t1261978245_0_0_0_Types[] = { &EmptyMockDialog_t1261978245_0_0_0 };
extern const Il2CppGenericInst GenInst_EmptyMockDialog_t1261978245_0_0_0 = { 1, GenInst_EmptyMockDialog_t1261978245_0_0_0_Types };
static const Il2CppType* GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0_Types[] = { &ParamDataType_t2672665179_0_0_0, &ParamDataType_t2672665179_0_0_0 };
extern const Il2CppGenericInst GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0 = { 2, GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types[] = { &PropertyMetadata_t4066634616_0_0_0, &PropertyMetadata_t4066634616_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0 = { 2, GenInst_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0_Types[] = { &Byte_t2862609660_0_0_0, &Byte_t2862609660_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0 = { 2, GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 2, GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
static const Il2CppType* GenInst_Double_t3868226565_0_0_0_Double_t3868226565_0_0_0_Types[] = { &Double_t3868226565_0_0_0, &Double_t3868226565_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t3868226565_0_0_0_Double_t3868226565_0_0_0 = { 2, GenInst_Double_t3868226565_0_0_0_Double_t3868226565_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t1153838595_0_0_0_Int64_t1153838595_0_0_0_Types[] = { &Int64_t1153838595_0_0_0, &Int64_t1153838595_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1153838595_0_0_0_Int64_t1153838595_0_0_0 = { 2, GenInst_Int64_t1153838595_0_0_0_Int64_t1153838595_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0_Types[] = { &CustomAttributeNamedArgument_t3059612989_0_0_0, &CustomAttributeNamedArgument_t3059612989_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0_Types[] = { &CustomAttributeTypedArgument_t3301293422_0_0_0, &CustomAttributeTypedArgument_t3301293422_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0_Types };
static const Il2CppType* GenInst_Color_t4194546905_0_0_0_Color_t4194546905_0_0_0_Types[] = { &Color_t4194546905_0_0_0, &Color_t4194546905_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t4194546905_0_0_0_Color_t4194546905_0_0_0 = { 2, GenInst_Color_t4194546905_0_0_0_Color_t4194546905_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0_Types[] = { &Color32_t598853688_0_0_0, &Color32_t598853688_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0 = { 2, GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0_Types[] = { &RaycastResult_t3762661364_0_0_0, &RaycastResult_t3762661364_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0 = { 2, GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0_Types[] = { &UICharInfo_t65807484_0_0_0, &UICharInfo_t65807484_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0 = { 2, GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0_Types[] = { &UILineInfo_t4113875482_0_0_0, &UILineInfo_t4113875482_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0 = { 2, GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0_Types[] = { &UIVertex_t4244065212_0_0_0, &UIVertex_t4244065212_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0 = { 2, GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0_Types[] = { &Vector2_t4282066565_0_0_0, &Vector2_t4282066565_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0 = { 2, GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0_Types[] = { &Vector3_t4282066566_0_0_0, &Vector3_t4282066566_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0 = { 2, GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0_Types[] = { &Vector4_t4282066567_0_0_0, &Vector4_t4282066567_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0 = { 2, GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types[] = { &PIXEL_FORMAT_t354375056_0_0_0, &PIXEL_FORMAT_t354375056_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0_Types };
static const Il2CppType* GenInst_TargetSearchResult_t3609410114_0_0_0_TargetSearchResult_t3609410114_0_0_0_Types[] = { &TargetSearchResult_t3609410114_0_0_0, &TargetSearchResult_t3609410114_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t3609410114_0_0_0_TargetSearchResult_t3609410114_0_0_0 = { 2, GenInst_TargetSearchResult_t3609410114_0_0_0_TargetSearchResult_t3609410114_0_0_0_Types };
static const Il2CppType* GenInst_EventNames_t1614635846_0_0_0_EventNames_t1614635846_0_0_0_Types[] = { &EventNames_t1614635846_0_0_0, &EventNames_t1614635846_0_0_0 };
extern const Il2CppGenericInst GenInst_EventNames_t1614635846_0_0_0_EventNames_t1614635846_0_0_0 = { 2, GenInst_EventNames_t1614635846_0_0_0_EventNames_t1614635846_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4186358450_0_0_0_KeyValuePair_2_t4186358450_0_0_0_Types[] = { &KeyValuePair_2_t4186358450_0_0_0, &KeyValuePair_2_t4186358450_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4186358450_0_0_0_KeyValuePair_2_t4186358450_0_0_0 = { 2, GenInst_KeyValuePair_2_t4186358450_0_0_0_KeyValuePair_2_t4186358450_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4186358450_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4186358450_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4186358450_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4186358450_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Perspective_t644553802_0_0_0_Perspective_t644553802_0_0_0_Types[] = { &Perspective_t644553802_0_0_0, &Perspective_t644553802_0_0_0 };
extern const Il2CppGenericInst GenInst_Perspective_t644553802_0_0_0_Perspective_t644553802_0_0_0 = { 2, GenInst_Perspective_t644553802_0_0_0_Perspective_t644553802_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2409664798_0_0_0_KeyValuePair_2_t2409664798_0_0_0_Types[] = { &KeyValuePair_2_t2409664798_0_0_0, &KeyValuePair_2_t2409664798_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2409664798_0_0_0_KeyValuePair_2_t2409664798_0_0_0 = { 2, GenInst_KeyValuePair_2_t2409664798_0_0_0_KeyValuePair_2_t2409664798_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2409664798_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2409664798_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2409664798_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2409664798_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_StateMachine_t367995870_0_0_0_StateMachine_t367995870_0_0_0_Types[] = { &StateMachine_t367995870_0_0_0, &StateMachine_t367995870_0_0_0 };
extern const Il2CppGenericInst GenInst_StateMachine_t367995870_0_0_0_StateMachine_t367995870_0_0_0 = { 2, GenInst_StateMachine_t367995870_0_0_0_StateMachine_t367995870_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1845333178_0_0_0_KeyValuePair_2_t1845333178_0_0_0_Types[] = { &KeyValuePair_2_t1845333178_0_0_0, &KeyValuePair_2_t1845333178_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1845333178_0_0_0_KeyValuePair_2_t1845333178_0_0_0 = { 2, GenInst_KeyValuePair_2_t1845333178_0_0_0_KeyValuePair_2_t1845333178_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1845333178_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1845333178_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1845333178_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1845333178_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Char_t2862622538_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0 = { 2, GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Char_t2862622538_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t996820380_0_0_0_KeyValuePair_2_t996820380_0_0_0_Types[] = { &KeyValuePair_2_t996820380_0_0_0, &KeyValuePair_2_t996820380_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t996820380_0_0_0_KeyValuePair_2_t996820380_0_0_0 = { 2, GenInst_KeyValuePair_2_t996820380_0_0_0_KeyValuePair_2_t996820380_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t996820380_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t996820380_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t996820380_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t996820380_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0_Types[] = { &Table_t1007478513_0_0_0, &Table_t1007478513_0_0_0 };
extern const Il2CppGenericInst GenInst_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0 = { 2, GenInst_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types[] = { &KeyValuePair_2_t1049882445_0_0_0, &KeyValuePair_2_t1049882445_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0 = { 2, GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1049882445_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0, &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0 = { 2, GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2164509075_0_0_0_KeyValuePair_2_t2164509075_0_0_0_Types[] = { &KeyValuePair_2_t2164509075_0_0_0, &KeyValuePair_2_t2164509075_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2164509075_0_0_0_KeyValuePair_2_t2164509075_0_0_0 = { 2, GenInst_KeyValuePair_2_t2164509075_0_0_0_KeyValuePair_2_t2164509075_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2164509075_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2164509075_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2164509075_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2164509075_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t2268465130_0_0_0_Il2CppObject_0_0_0_Types[] = { &Label_t2268465130_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t2268465130_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Label_t2268465130_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0_Types[] = { &Label_t2268465130_0_0_0, &Label_t2268465130_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0 = { 2, GenInst_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t291920669_0_0_0_KeyValuePair_2_t291920669_0_0_0_Types[] = { &KeyValuePair_2_t291920669_0_0_0, &KeyValuePair_2_t291920669_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t291920669_0_0_0_KeyValuePair_2_t291920669_0_0_0 = { 2, GenInst_KeyValuePair_2_t291920669_0_0_0_KeyValuePair_2_t291920669_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t291920669_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t291920669_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t291920669_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t291920669_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t395876724_0_0_0_Il2CppObject_0_0_0_Types[] = { &TrackableResultData_t395876724_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t395876724_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TrackableResultData_t395876724_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0_Types[] = { &TrackableResultData_t395876724_0_0_0, &TrackableResultData_t395876724_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0 = { 2, GenInst_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types[] = { &KeyValuePair_2_t355424280_0_0_0, &KeyValuePair_2_t355424280_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0 = { 2, GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t355424280_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0_Types[] = { &VirtualButtonData_t459380335_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types[] = { &VirtualButtonData_t459380335_0_0_0, &VirtualButtonData_t459380335_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0 = { 2, GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types[] = { &ArrayMetadata_t4058342910_0_0_0, &ArrayMetadata_t4058342910_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0 = { 2, GenInst_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0_Types[] = { &ArrayMetadata_t4058342910_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1832195516_0_0_0_KeyValuePair_2_t1832195516_0_0_0_Types[] = { &KeyValuePair_2_t1832195516_0_0_0, &KeyValuePair_2_t1832195516_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1832195516_0_0_0_KeyValuePair_2_t1832195516_0_0_0 = { 2, GenInst_KeyValuePair_2_t1832195516_0_0_0_KeyValuePair_2_t1832195516_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1832195516_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1832195516_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1832195516_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1832195516_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types[] = { &ObjectMetadata_t2009294498_0_0_0, &ObjectMetadata_t2009294498_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0 = { 2, GenInst_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0_Types[] = { &ObjectMetadata_t2009294498_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4078114400_0_0_0_KeyValuePair_2_t4078114400_0_0_0_Types[] = { &KeyValuePair_2_t4078114400_0_0_0, &KeyValuePair_2_t4078114400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4078114400_0_0_0_KeyValuePair_2_t4078114400_0_0_0 = { 2, GenInst_KeyValuePair_2_t4078114400_0_0_0_KeyValuePair_2_t4078114400_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4078114400_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4078114400_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4078114400_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4078114400_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0_Types[] = { &PropertyMetadata_t4066634616_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1840487222_0_0_0_KeyValuePair_2_t1840487222_0_0_0_Types[] = { &KeyValuePair_2_t1840487222_0_0_0, &KeyValuePair_2_t1840487222_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1840487222_0_0_0_KeyValuePair_2_t1840487222_0_0_0 = { 2, GenInst_KeyValuePair_2_t1840487222_0_0_0_KeyValuePair_2_t1840487222_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1840487222_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1840487222_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1840487222_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1840487222_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0, &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0 = { 2, GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0, &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0 = { 2, GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types[] = { &KeyValuePair_2_t2065771578_0_0_0, &KeyValuePair_2_t2065771578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0 = { 2, GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2065771578_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types[] = { &Single_t4291918972_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types[] = { &KeyValuePair_2_t2093487825_0_0_0, &KeyValuePair_2_t2093487825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0 = { 2, GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2093487825_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt16_t24667923_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types[] = { &UInt16_t24667923_0_0_0, &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0 = { 2, GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types[] = { &KeyValuePair_2_t1919813716_0_0_0, &KeyValuePair_2_t1919813716_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0 = { 2, GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1919813716_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t4145961110_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &TextEditOp_t4145961110_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0 = { 2, GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types[] = { &KeyValuePair_2_t4030510692_0_0_0, &KeyValuePair_2_t4030510692_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0 = { 2, GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4030510692_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types[] = { &ProfileData_t1961690790_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types[] = { &ProfileData_t1961690790_0_0_0, &ProfileData_t1961690790_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0 = { 2, GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3830311593_0_0_0_KeyValuePair_2_t3830311593_0_0_0_Types[] = { &KeyValuePair_2_t3830311593_0_0_0, &KeyValuePair_2_t3830311593_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3830311593_0_0_0_KeyValuePair_2_t3830311593_0_0_0 = { 2, GenInst_KeyValuePair_2_t3830311593_0_0_0_KeyValuePair_2_t3830311593_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3830311593_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3830311593_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3830311593_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3830311593_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_NetworkReachability_t612403035_0_0_0_NetworkReachability_t612403035_0_0_0_Types[] = { &NetworkReachability_t612403035_0_0_0, &NetworkReachability_t612403035_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkReachability_t612403035_0_0_0_NetworkReachability_t612403035_0_0_0 = { 2, GenInst_NetworkReachability_t612403035_0_0_0_NetworkReachability_t612403035_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types[] = { &KeyValuePair_2_t1718082560_0_0_0, &KeyValuePair_2_t1718082560_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0 = { 2, GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1718082560_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2656869371_0_0_0_KeyValuePair_2_t2656869371_0_0_0_Types[] = { &KeyValuePair_2_t2656869371_0_0_0, &KeyValuePair_2_t2656869371_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2656869371_0_0_0_KeyValuePair_2_t2656869371_0_0_0 = { 2, GenInst_KeyValuePair_2_t2656869371_0_0_0_KeyValuePair_2_t2656869371_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2656869371_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2656869371_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2656869371_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2656869371_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1188478419_0_0_0_KeyValuePair_2_t1188478419_0_0_0_Types[] = { &KeyValuePair_2_t1188478419_0_0_0, &KeyValuePair_2_t1188478419_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1188478419_0_0_0_KeyValuePair_2_t1188478419_0_0_0 = { 2, GenInst_KeyValuePair_2_t1188478419_0_0_0_KeyValuePair_2_t1188478419_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1188478419_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1188478419_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1188478419_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1188478419_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_BarcodeFormat_t4201805817_0_0_0_BarcodeFormat_t4201805817_0_0_0_Types[] = { &BarcodeFormat_t4201805817_0_0_0, &BarcodeFormat_t4201805817_0_0_0 };
extern const Il2CppGenericInst GenInst_BarcodeFormat_t4201805817_0_0_0_BarcodeFormat_t4201805817_0_0_0 = { 2, GenInst_BarcodeFormat_t4201805817_0_0_0_BarcodeFormat_t4201805817_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1080897207_0_0_0_KeyValuePair_2_t1080897207_0_0_0_Types[] = { &KeyValuePair_2_t1080897207_0_0_0, &KeyValuePair_2_t1080897207_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1080897207_0_0_0_KeyValuePair_2_t1080897207_0_0_0 = { 2, GenInst_KeyValuePair_2_t1080897207_0_0_0_KeyValuePair_2_t1080897207_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1080897207_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1080897207_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1080897207_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1080897207_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_DecodeHintType_t2095781349_0_0_0_DecodeHintType_t2095781349_0_0_0_Types[] = { &DecodeHintType_t2095781349_0_0_0, &DecodeHintType_t2095781349_0_0_0 };
extern const Il2CppGenericInst GenInst_DecodeHintType_t2095781349_0_0_0_DecodeHintType_t2095781349_0_0_0 = { 2, GenInst_DecodeHintType_t2095781349_0_0_0_DecodeHintType_t2095781349_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3617918191_0_0_0_KeyValuePair_2_t3617918191_0_0_0_Types[] = { &KeyValuePair_2_t3617918191_0_0_0, &KeyValuePair_2_t3617918191_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3617918191_0_0_0_KeyValuePair_2_t3617918191_0_0_0 = { 2, GenInst_KeyValuePair_2_t3617918191_0_0_0_KeyValuePair_2_t3617918191_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3617918191_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3617918191_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3617918191_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3617918191_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_EncodeHintType_t3244562957_0_0_0_EncodeHintType_t3244562957_0_0_0_Types[] = { &EncodeHintType_t3244562957_0_0_0, &EncodeHintType_t3244562957_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodeHintType_t3244562957_0_0_0_EncodeHintType_t3244562957_0_0_0 = { 2, GenInst_EncodeHintType_t3244562957_0_0_0_EncodeHintType_t3244562957_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3033809700_0_0_0_KeyValuePair_2_t3033809700_0_0_0_Types[] = { &KeyValuePair_2_t3033809700_0_0_0, &KeyValuePair_2_t3033809700_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3033809700_0_0_0_KeyValuePair_2_t3033809700_0_0_0 = { 2, GenInst_KeyValuePair_2_t3033809700_0_0_0_KeyValuePair_2_t3033809700_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3033809700_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3033809700_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3033809700_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3033809700_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ResultMetadataType_t2923366972_0_0_0_ResultMetadataType_t2923366972_0_0_0_Types[] = { &ResultMetadataType_t2923366972_0_0_0, &ResultMetadataType_t2923366972_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultMetadataType_t2923366972_0_0_0_ResultMetadataType_t2923366972_0_0_0 = { 2, GenInst_ResultMetadataType_t2923366972_0_0_0_ResultMetadataType_t2923366972_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1529] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0,
	&GenInst_Char_t2862622538_0_0_0,
	&GenInst_IConvertible_t2116191568_0_0_0,
	&GenInst_IComparable_t1391370361_0_0_0,
	&GenInst_IComparable_1_t2772552329_0_0_0,
	&GenInst_IEquatable_1_t2411901105_0_0_0,
	&GenInst_ValueType_t1744280289_0_0_0,
	&GenInst_Int64_t1153838595_0_0_0,
	&GenInst_UInt32_t24667981_0_0_0,
	&GenInst_UInt64_t24668076_0_0_0,
	&GenInst_Byte_t2862609660_0_0_0,
	&GenInst_SByte_t1161769777_0_0_0,
	&GenInst_Int16_t1153838442_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t3464557803_0_0_0,
	&GenInst_ICloneable_t1025544834_0_0_0,
	&GenInst_IComparable_1_t4212128644_0_0_0,
	&GenInst_IEquatable_1_t3851477420_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t2853506214_0_0_0,
	&GenInst__Type_t2149739635_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1425685797_0_0_0,
	&GenInst__MemberInfo_t3353101921_0_0_0,
	&GenInst_IFormattable_t382002946_0_0_0,
	&GenInst_IComparable_1_t1063768291_0_0_0,
	&GenInst_IEquatable_1_t703117067_0_0_0,
	&GenInst_Double_t3868226565_0_0_0,
	&GenInst_IComparable_1_t3778156356_0_0_0,
	&GenInst_IEquatable_1_t3417505132_0_0_0,
	&GenInst_IComparable_1_t4229565068_0_0_0,
	&GenInst_IEquatable_1_t3868913844_0_0_0,
	&GenInst_IComparable_1_t2772539451_0_0_0,
	&GenInst_IEquatable_1_t2411888227_0_0_0,
	&GenInst_Single_t4291918972_0_0_0,
	&GenInst_IComparable_1_t4201848763_0_0_0,
	&GenInst_IEquatable_1_t3841197539_0_0_0,
	&GenInst_Decimal_t1954350631_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0,
	&GenInst_Delegate_t3310234105_0_0_0,
	&GenInst_ISerializable_t867484142_0_0_0,
	&GenInst_ParameterInfo_t2235474049_0_0_0,
	&GenInst__ParameterInfo_t2787166306_0_0_0,
	&GenInst_ParameterModifier_t741930026_0_0_0,
	&GenInst_IComparable_1_t4229565010_0_0_0,
	&GenInst_IEquatable_1_t3868913786_0_0_0,
	&GenInst_IComparable_1_t4229565163_0_0_0,
	&GenInst_IEquatable_1_t3868913939_0_0_0,
	&GenInst_IComparable_1_t1063768233_0_0_0,
	&GenInst_IEquatable_1_t703117009_0_0_0,
	&GenInst_IComparable_1_t1071699568_0_0_0,
	&GenInst_IEquatable_1_t711048344_0_0_0,
	&GenInst_IComparable_1_t1063768386_0_0_0,
	&GenInst_IEquatable_1_t703117162_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t209867187_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3971289384_0_0_0,
	&GenInst_MethodBase_t318515428_0_0_0,
	&GenInst__MethodBase_t3971068747_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3711326812_0_0_0,
	&GenInst_ConstructorInfo_t4136801618_0_0_0,
	&GenInst__ConstructorInfo_t3408715251_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t3372848153_0_0_0,
	&GenInst_TailoringInfo_t3025807515_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_Link_t2063667470_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1873037576_0_0_0,
	&GenInst_Contraction_t3998770676_0_0_0,
	&GenInst_Level2Map_t3664214860_0_0_0,
	&GenInst_BigInteger_t3334373498_0_0_0,
	&GenInst_UriScheme_t3372318283_0_0_0,
	&GenInst_KeySizes_t2106826975_0_0_0,
	&GenInst_Assembly_t1418687608_0_0_0,
	&GenInst__Assembly_t3789461407_0_0_0,
	&GenInst_IEvidenceFactory_t3943896478_0_0_0,
	&GenInst_DateTime_t4283661327_0_0_0,
	&GenInst_DateTimeOffset_t3884714306_0_0_0,
	&GenInst_TimeSpan_t413522987_0_0_0,
	&GenInst_Guid_t2862754429_0_0_0,
	&GenInst_IComparable_1_t386728509_0_0_0,
	&GenInst_IEquatable_1_t26077285_0_0_0,
	&GenInst_CustomAttributeData_t2955630591_0_0_0,
	&GenInst_TermInfoStrings_t2002624446_0_0_0,
	&GenInst_Enum_t2862688501_0_0_0,
	&GenInst_Version_t763695022_0_0_0,
	&GenInst_Slot_t2260530181_0_0_0,
	&GenInst_Slot_t2072023290_0_0_0,
	&GenInst_StackFrame_t1034942277_0_0_0,
	&GenInst_Calendar_t3558528576_0_0_0,
	&GenInst_CultureInfo_t1065375142_0_0_0,
	&GenInst_IFormatProvider_t192740775_0_0_0,
	&GenInst_FileInfo_t3233670074_0_0_0,
	&GenInst_FileSystemInfo_t2605906633_0_0_0,
	&GenInst_MarshalByRefObject_t1219038801_0_0_0,
	&GenInst_DirectoryInfo_t89154617_0_0_0,
	&GenInst_Module_t1394482686_0_0_0,
	&GenInst__Module_t2601912805_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0,
	&GenInst_ModuleBuilder_t595214213_0_0_0,
	&GenInst__ModuleBuilder_t1764509690_0_0_0,
	&GenInst_MonoResource_t1505432149_0_0_0,
	&GenInst_RefEmitPermissionSet_t3880501745_0_0_0,
	&GenInst_ParameterBuilder_t3159962230_0_0_0,
	&GenInst__ParameterBuilder_t4122453611_0_0_0,
	&GenInst_TypeU5BU5D_t3339007067_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t2643922881_0_0_0,
	&GenInst_IList_t1751339649_0_0_0,
	&GenInst_LocalBuilder_t194563060_0_0_0,
	&GenInst__LocalBuilder_t3375243241_0_0_0,
	&GenInst_LocalVariableInfo_t962988767_0_0_0,
	&GenInst_ILTokenInfo_t1354080954_0_0_0,
	&GenInst_LabelData_t3207823784_0_0_0,
	&GenInst_LabelFixup_t660379442_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t553556921_0_0_0,
	&GenInst_TypeBuilder_t1918497079_0_0_0,
	&GenInst__TypeBuilder_t3501492652_0_0_0,
	&GenInst_MethodBuilder_t302405488_0_0_0,
	&GenInst__MethodBuilder_t1471700965_0_0_0,
	&GenInst_ConstructorBuilder_t3217839941_0_0_0,
	&GenInst__ConstructorBuilder_t788093754_0_0_0,
	&GenInst_PropertyBuilder_t2012258748_0_0_0,
	&GenInst__PropertyBuilder_t752753201_0_0_0,
	&GenInst_FieldBuilder_t1754069893_0_0_0,
	&GenInst__FieldBuilder_t639782778_0_0_0,
	&GenInst_ResourceInfo_t4013605874_0_0_0,
	&GenInst_ResourceCacheItem_t2113902833_0_0_0,
	&GenInst_IContextAttribute_t3913746816_0_0_0,
	&GenInst_IContextProperty_t82913453_0_0_0,
	&GenInst_Header_t1689611527_0_0_0,
	&GenInst_ITrackingHandler_t2228500544_0_0_0,
	&GenInst_IComparable_1_t4193591118_0_0_0,
	&GenInst_IEquatable_1_t3832939894_0_0_0,
	&GenInst_IComparable_1_t1864280422_0_0_0,
	&GenInst_IEquatable_1_t1503629198_0_0_0,
	&GenInst_IComparable_1_t323452778_0_0_0,
	&GenInst_IEquatable_1_t4257768850_0_0_0,
	&GenInst_TypeTag_t2420703430_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_KeyContainerPermissionAccessEntry_t2372748443_0_0_0,
	&GenInst_StrongName_t2878058698_0_0_0,
	&GenInst_CodeConnectAccess_t2328951823_0_0_0,
	&GenInst_EncodingInfo_t1898473639_0_0_0,
	&GenInst_WaitHandle_t1661568373_0_0_0,
	&GenInst_IDisposable_t1423340799_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_XsdIdentityPath_t691771506_0_0_0,
	&GenInst_XsdIdentityField_t471607183_0_0_0,
	&GenInst_XsdIdentityStep_t691878681_0_0_0,
	&GenInst_XmlSchemaAttribute_t2904598248_0_0_0,
	&GenInst_XmlSchemaAnnotated_t4226823396_0_0_0,
	&GenInst_XmlSchemaObject_t3280570797_0_0_0,
	&GenInst_XmlSchemaException_t78171291_0_0_0,
	&GenInst_SystemException_t4206535862_0_0_0,
	&GenInst_Exception_t3991598821_0_0_0,
	&GenInst__Exception_t426620218_0_0_0,
	&GenInst_KeyValuePair_2_t2758969756_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0,
	&GenInst_DTDNode_t2039770680_0_0_0,
	&GenInst_AttributeSlot_t2157829939_0_0_0,
	&GenInst_Entry_t2866414864_0_0_0,
	&GenInst_XmlNode_t856910923_0_0_0,
	&GenInst_IXPathNavigable_t153087709_0_0_0,
	&GenInst_NsDecl_t3658211563_0_0_0,
	&GenInst_NsScope_t1749213747_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t982414386_0_0_0,
	&GenInst_XmlTokenInfo_t597808448_0_0_0,
	&GenInst_TagName_t2016006645_0_0_0,
	&GenInst_XmlNodeInfo_t1808742809_0_0_0,
	&GenInst_XmlAttribute_t6647939_0_0_0,
	&GenInst_IHasXmlChildNode_t954813494_0_0_0,
	&GenInst_XmlQualifiedName_t2133315502_0_0_0,
	&GenInst_Regex_t2161232213_0_0_0,
	&GenInst_XmlSchemaSimpleType_t3060492794_0_0_0,
	&GenInst_XmlSchemaType_t4090188264_0_0_0,
	&GenInst_BigInteger_t3334373499_0_0_0,
	&GenInst_ByteU5BU5D_t4260760469_0_0_0,
	&GenInst_X509Certificate_t3076817455_0_0_0,
	&GenInst_IDeserializationCallback_t675596727_0_0_0,
	&GenInst_ClientCertificateType_t3167042548_0_0_0,
	&GenInst_ConfigurationProperty_t3009015393_0_0_0,
	&GenInst_Attribute_t2523058482_0_0_0,
	&GenInst__Attribute_t3253047175_0_0_0,
	&GenInst_PropertyDescriptor_t2073374448_0_0_0,
	&GenInst_MemberDescriptor_t2617136693_0_0_0,
	&GenInst_AttributeU5BU5D_t4055800263_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0,
	&GenInst_TypeDescriptionProvider_t3543085017_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1421923377_0_0_0,
	&GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0,
	&GenInst_WeakObjectWrapper_t1518976226_0_0_0,
	&GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1472936237_0_0_0,
	&GenInst_Cookie_t2033273982_0_0_0,
	&GenInst_IPAddress_t3525271463_0_0_0,
	&GenInst_NetworkInterface_t3597375525_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t908270265_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1627469341_0_0_0,
	&GenInst_LinuxNetworkInterface_t908270265_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1851933528_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2571132604_0_0_0,
	&GenInst_MacOsNetworkInterface_t1851933528_0_0_0,
	&GenInst_Win32_IP_ADAPTER_ADDRESSES_t3597816152_0_0_0,
	&GenInst_X509ChainStatus_t766901931_0_0_0,
	&GenInst_ArraySegment_1_t2188033608_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1195997794_0_0_0,
	&GenInst_Capture_t754001812_0_0_0,
	&GenInst_DynamicMethod_t2315379190_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_KeyValuePair_2_t1049882445_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0,
	&GenInst_KeyValuePair_2_t2164509075_0_0_0,
	&GenInst_Label_t2268465130_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Label_t2268465130_0_0_0_KeyValuePair_2_t2164509075_0_0_0,
	&GenInst_Group_t2151468941_0_0_0,
	&GenInst_Mark_t3811539797_0_0_0,
	&GenInst_UriScheme_t1290668975_0_0_0,
	&GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_Link_t2122599155_0_0_0,
	&GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Object_t3071478659_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0,
	&GenInst_IAchievementDescription_t655461400_0_0_0,
	&GenInst_IAchievementU5BU5D_t1953253797_0_0_0,
	&GenInst_IAchievement_t2957812780_0_0_0,
	&GenInst_IScoreU5BU5D_t250104726_0_0_0,
	&GenInst_IScore_t4279057999_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3419104218_0_0_0,
	&GenInst_IUserProfile_t598900827_0_0_0,
	&GenInst_AchievementDescription_t2116066607_0_0_0,
	&GenInst_UserProfile_t2280656072_0_0_0,
	&GenInst_GcLeaderboard_t1820874799_0_0_0,
	&GenInst_GcAchievementData_t3481375915_0_0_0,
	&GenInst_Achievement_t344600729_0_0_0,
	&GenInst_GcScoreData_t2181296590_0_0_0,
	&GenInst_Score_t3396031228_0_0_0,
	&GenInst_Vector3_t4282066566_0_0_0,
	&GenInst_Vector4_t4282066567_0_0_0,
	&GenInst_Vector2_t4282066565_0_0_0,
	&GenInst_Color32_t598853688_0_0_0,
	&GenInst_Material_t3870600107_0_0_0,
	&GenInst_Color_t4194546905_0_0_0,
	&GenInst_Keyframe_t4079056114_0_0_0,
	&GenInst_NetworkPlayer_t3231273765_0_0_0,
	&GenInst_HostData_t3270478838_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t726430633_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0,
	&GenInst_Behaviour_t200106419_0_0_0,
	&GenInst_Component_t3501516275_0_0_0,
	&GenInst_Display_t1321072632_0_0_0,
	&GenInst_Touch_t4210255029_0_0_0,
	&GenInst_GameObject_t3674682005_0_0_0,
	&GenInst_AndroidJavaObject_t2362096582_0_0_0,
	&GenInst_jvalue_t3862675307_0_0_0,
	&GenInst_Playable_t70832698_0_0_0,
	&GenInst_ParticleCollisionEvent_t2700926194_0_0_0,
	&GenInst_ContactPoint_t243083348_0_0_0,
	&GenInst_RaycastHit_t4003175726_0_0_0,
	&GenInst_Collider_t2939674232_0_0_0,
	&GenInst_Rigidbody2D_t1743771669_0_0_0,
	&GenInst_RaycastHit2D_t1374744384_0_0_0,
	&GenInst_ContactPoint2D_t4288432358_0_0_0,
	&GenInst_WebCamDevice_t3274004757_0_0_0,
	&GenInst_UIVertex_t4244065212_0_0_0,
	&GenInst_UICharInfo_t65807484_0_0_0,
	&GenInst_UILineInfo_t4113875482_0_0_0,
	&GenInst_Font_t4241557075_0_0_0,
	&GenInst_GUIContent_t2094828418_0_0_0,
	&GenInst_Rect_t4241904616_0_0_0,
	&GenInst_GUILayoutOption_t331591504_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t775952400_0_0_0,
	&GenInst_GUILayoutEntry_t1336615025_0_0_0,
	&GenInst_GUIStyle_t2990928826_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3710127902_0_0_0,
	&GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_KeyValuePair_2_t1919813716_0_0_0,
	&GenInst_TextEditOp_t4145961110_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0,
	&GenInst_Event_t4196595728_0_0_0,
	&GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1289591971_0_0_0,
	&GenInst_DisallowMultipleComponent_t62111112_0_0_0,
	&GenInst_ExecuteInEditMode_t3132250205_0_0_0,
	&GenInst_RequireComponent_t1687166108_0_0_0,
	&GenInst_HitInfo_t3209134097_0_0_0,
	&GenInst_PersistentCall_t2972625667_0_0_0,
	&GenInst_BaseInvokableCall_t1559630662_0_0_0,
	&GenInst_PlayMakerFSM_t3799847376_0_0_0,
	&GenInst_MonoBehaviour_t667441552_0_0_0,
	&GenInst_FsmFloat_t2134102846_0_0_0,
	&GenInst_NamedVariable_t3211770239_0_0_0,
	&GenInst_INameable_t2730839192_0_0_0,
	&GenInst_INamedVariable_t1024128046_0_0_0,
	&GenInst_FsmInt_t1596138449_0_0_0,
	&GenInst_FsmTransition_t3771611999_0_0_0,
	&GenInst_IEquatable_1_t3320890566_0_0_0,
	&GenInst_ActionReport_t662142796_0_0_0,
	&GenInst_FsmVarOverride_t3235106805_0_0_0,
	&GenInst_Fsm_t1527112426_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3582344850_0_0_0,
	&GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0,
	&GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2571765214_0_0_0,
	&GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1158041691_0_0_0,
	&GenInst_FsmGameObject_t1697147867_0_0_0,
	&GenInst_FsmOwnerDefault_t251897112_0_0_0,
	&GenInst_FsmAnimationCurve_t2685995989_0_0_0,
	&GenInst_FunctionCall_t3279845016_0_0_0,
	&GenInst_FsmTemplateControl_t2786508133_0_0_0,
	&GenInst_FsmEventTarget_t1823904941_0_0_0,
	&GenInst_FsmProperty_t3927159007_0_0_0,
	&GenInst_LayoutOption_t964995201_0_0_0,
	&GenInst_FsmString_t952858651_0_0_0,
	&GenInst_FsmObject_t821476169_0_0_0,
	&GenInst_FsmVar_t1596150537_0_0_0,
	&GenInst_ParamDataType_t2672665179_0_0_0,
	&GenInst_FsmStateAction_t2366529033_0_0_0,
	&GenInst_IFsmStateAction_t3269097786_0_0_0,
	&GenInst_DelayedEvent_t1938906778_0_0_0,
	&GenInst_FsmState_t2146334067_0_0_0,
	&GenInst_FsmEvent_t2133468028_0_0_0,
	&GenInst_FsmLogEntry_t2614866584_0_0_0,
	&GenInst_FsmLog_t1596141350_0_0_0,
	&GenInst_FsmBool_t1075959796_0_0_0,
	&GenInst_FsmVector2_t533912881_0_0_0,
	&GenInst_FsmVector3_t533912882_0_0_0,
	&GenInst_FsmRect_t1076426478_0_0_0,
	&GenInst_FsmQuaternion_t3871136040_0_0_0,
	&GenInst_FsmColor_t2131419205_0_0_0,
	&GenInst_FsmMaterial_t924399665_0_0_0,
	&GenInst_FsmTexture_t3073272573_0_0_0,
	&GenInst_LuminanceSource_t1231523093_0_0_0_Binarizer_t1492033400_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ByteU5BU5D_t4260760469_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_LuminanceSource_t1231523093_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_BitmapFormat_t3665922245_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ResultPoint_t1538592853_0_0_0,
	&GenInst_Result_t2610723219_0_0_0,
	&GenInst_Il2CppObject_0_0_0_EventArgs_t2540831021_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1080897207_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3033809700_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0,
	&GenInst_Color32U5BU5D_t2960766953_0_0_0,
	&GenInst_Color32U5BU5D_t2960766953_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0,
	&GenInst_Reader_t2610170425_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3617918191_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0,
	&GenInst_Writer_t2765575657_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1188478419_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_BarcodeFormat_t4201805817_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1188478419_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_Func_1_t3890737231_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t908399279_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_ResultMetadataType_t2923366972_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3033809700_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2656869371_0_0_0,
	&GenInst_Table_t1007478513_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0,
	&GenInst_KeyValuePair_2_t996820380_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Table_t1007478513_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2656869371_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_StringU5BU5D_t4054002952_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2540055952_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Char_t2862622538_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Table_t1007478513_0_0_0_KeyValuePair_2_t996820380_0_0_0,
	&GenInst_Int32U5BU5D_t3230847821_0_0_0,
	&GenInst_State_t3065423635_0_0_0,
	&GenInst_Token_t3066207355_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0,
	&GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2438309113_0_0_0,
	&GenInst_String_t_0_0_0_CharacterSetECI_t2542265168_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3261464244_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DecodeHintType_t2095781349_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1080897207_0_0_0,
	&GenInst_Int64U5BU5D_t2174042770_0_0_0,
	&GenInst_BigInteger_t416298622_0_0_0,
	&GenInst_GenericGFPoly_t755870220_0_0_0,
	&GenInst_SymbolShapeHint_t2380532246_0_0_0,
	&GenInst_DataBlock_t594199773_0_0_0,
	&GenInst_ECB_t2948426741_0_0_0,
	&GenInst_Version_t2313761970_0_0_0,
	&GenInst_ResultPointsAndTransitions_t1206419768_0_0_0,
	&GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_ResultPoint_t1538592853_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3309166264_0_0_0,
	&GenInst_SymbolInfo_t553159586_0_0_0,
	&GenInst_Encoder_t1508201762_0_0_0,
	&GenInst_CharU5BU5D_t3324145743_0_0_0,
	&GenInst_StringBuilder_t243639308_0_0_0,
	&GenInst_FinderPattern_t4119758992_0_0_0,
	&GenInst_OneDReader_t3436042911_0_0_0,
	&GenInst_UPCEANReader_t3527170699_0_0_0,
	&GenInst_Pair_t1045409632_0_0_0,
	&GenInst_ExpandedPair_t3413366687_0_0_0,
	&GenInst_ExpandedRow_t1562831751_0_0_0,
	&GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0,
	&GenInst_String_t_0_0_0_ObjectU5BU5D_t1108656482_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1827855558_0_0_0,
	&GenInst_ResultPointU5BU5D_t1195164344_0_0_0,
	&GenInst_SByteU5BU5D_t2505034988_0_0_0,
	&GenInst_IEquatable_1_t4260544485_0_0_0,
	&GenInst_IComparable_1_t326228413_0_0_0,
	&GenInst_DetectionResultColumn_t3195057574_0_0_0,
	&GenInst_Codeword_t1124156527_0_0_0,
	&GenInst_SingleU5BU5D_t2316563989_0_0_0,
	&GenInst_BarcodeValueU5BU5D_t3614862452_0_0_0,
	&GenInst_BarcodeValue_t1597289545_0_0_0,
	&GenInst_ModulusPoly_t3133325097_0_0_0,
	&GenInst_BarcodeRow_t3616570866_0_0_0,
	&GenInst_DataBlock_t3210726793_0_0_0,
	&GenInst_ECB_t2022291218_0_0_0,
	&GenInst_DataMask_t1708385874_0_0_0,
	&GenInst_ErrorCorrectionLevel_t1225927610_0_0_0,
	&GenInst_Version_t1953509534_0_0_0,
	&GenInst_ECBlocks_t3771581654_0_0_0,
	&GenInst_AlignmentPattern_t1786970569_0_0_0,
	&GenInst_BlockPair_t178469773_0_0_0,
	&GenInst_EyewearCalibrationReading_t3010462567_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1857232484_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t2475465524_0_0_0,
	&GenInst_Renderer_t3076687687_0_0_0,
	&GenInst_TrackableBehaviour_t4179556250_0_0_0,
	&GenInst_ITrackableEventHandler_t1613618350_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t1860057025_0_0_0,
	&GenInst_ICloudRecoEventHandler_t796991165_0_0_0,
	&GenInst_TargetSearchResult_t3609410114_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t465046465_0_0_0,
	&GenInst_Trackable_t3781061455_0_0_0,
	&GenInst_VirtualButton_t704206407_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1718082560_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t354375056_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1718082560_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_Image_t2247677317_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4089910802_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Trackable_t3781061455_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3677105400_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButton_t704206407_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t600250352_0_0_0,
	&GenInst_DataSet_t2095838082_0_0_0,
	&GenInst_DataSetImpl_t1837478850_0_0_0,
	&GenInst_Marker_t616694972_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Marker_t616694972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t512738917_0_0_0,
	&GenInst_TrackableResultData_t395876724_0_0_0,
	&GenInst_WordData_t1548845516_0_0_0,
	&GenInst_WordResultData_t1826866697_0_0_0,
	&GenInst_SmartTerrainRevisionData_t3919795561_0_0_0,
	&GenInst_SurfaceData_t1737958143_0_0_0,
	&GenInst_PropData_t526813605_0_0_0,
	&GenInst_IEditorTrackableBehaviour_t1840126232_0_0_0,
	&GenInst_Image_t2247677317_0_0_0,
	&GenInst_SmartTerrainTrackable_t1276145027_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t2826579942_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_KeyValuePair_2_t2093487825_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t24667923_0_0_0_KeyValuePair_2_t2093487825_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t24667923_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t28871114_0_0_0,
	&GenInst_RectangleData_t2265684451_0_0_0,
	&GenInst_WordResult_t1079862857_0_0_0,
	&GenInst_Word_t2165514892_0_0_0,
	&GenInst_WordAbstractBehaviour_t545947899_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordResult_t1079862857_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t975906802_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_WordAbstractBehaviour_t545947899_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t441991844_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1914133451_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2633332527_0_0_0,
	&GenInst_List_1_t1914133451_0_0_0,
	&GenInst_ILoadLevelEventHandler_t678501447_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t2222092879_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t587327548_0_0_0,
	&GenInst_Prop_t2165309157_0_0_0,
	&GenInst_Surface_t3094389719_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Surface_t3094389719_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2990433664_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_SurfaceAbstractBehaviour_t142368528_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t38412473_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Prop_t2165309157_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2061353102_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PropAbstractBehaviour_t1293468098_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1189512043_0_0_0,
	&GenInst_PropAbstractBehaviour_t1293468098_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t142368528_0_0_0,
	&GenInst_MeshFilter_t3839065225_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableBehaviour_t4179556250_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4075600195_0_0_0,
	&GenInst_MarkerAbstractBehaviour_t865486027_0_0_0,
	&GenInst_IEditorMarkerBehaviour_t1845962767_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t3541475369_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t3340678586_0_0_0,
	&GenInst_IEditorDataSetTrackableBehaviour_t189565564_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t1191238304_0_0_0,
	&GenInst_IEditorVirtualButtonBehaviour_t4082570784_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0,
	&GenInst_KeyValuePair_2_t291920669_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_TrackableResultData_t395876724_0_0_0_KeyValuePair_2_t291920669_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0,
	&GenInst_KeyValuePair_2_t355424280_0_0_0,
	&GenInst_VirtualButtonData_t459380335_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonData_t459380335_0_0_0_KeyValuePair_2_t355424280_0_0_0,
	&GenInst_ImageTarget_t3520455670_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_ImageTarget_t3520455670_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3416499615_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_KeyValuePair_2_t4030510692_0_0_0,
	&GenInst_ProfileData_t1961690790_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1961690790_0_0_0_KeyValuePair_2_t4030510692_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1961690790_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2680889866_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_VirtualButtonAbstractBehaviour_t1191238304_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1087282249_0_0_0,
	&GenInst_ITrackerEventHandler_t3566668545_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t4010260306_0_0_0,
	&GenInst_InitError_t432217960_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t2608219151_0_0_0,
	&GenInst_ITextRecoEventHandler_t155716687_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t2660219000_0_0_0,
	&GenInst_MeshRenderer_t2804666580_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t2952053798_0_0_0,
	&GenInst_BaseInputModule_t15847059_0_0_0,
	&GenInst_RaycastResult_t3762661364_0_0_0,
	&GenInst_IDeselectHandler_t4282929196_0_0_0,
	&GenInst_IEventSystemHandler_t1130525624_0_0_0,
	&GenInst_List_1_t2498711176_0_0_0,
	&GenInst_List_1_t1244034627_0_0_0,
	&GenInst_List_1_t574734531_0_0_0,
	&GenInst_ISelectHandler_t3580292109_0_0_0,
	&GenInst_BaseRaycaster_t2327671059_0_0_0,
	&GenInst_Entry_t849715470_0_0_0,
	&GenInst_BaseEventData_t2054899105_0_0_0,
	&GenInst_IPointerEnterHandler_t1772318350_0_0_0,
	&GenInst_IPointerExitHandler_t3471580134_0_0_0,
	&GenInst_IPointerDownHandler_t1340699618_0_0_0,
	&GenInst_IPointerUpHandler_t3049278345_0_0_0,
	&GenInst_IPointerClickHandler_t2979908062_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t220416063_0_0_0,
	&GenInst_IBeginDragHandler_t1569653796_0_0_0,
	&GenInst_IDragHandler_t880586197_0_0_0,
	&GenInst_IEndDragHandler_t2789914546_0_0_0,
	&GenInst_IDropHandler_t4146418618_0_0_0,
	&GenInst_IScrollHandler_t3315383580_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3805412741_0_0_0,
	&GenInst_IMoveHandler_t3984942232_0_0_0,
	&GenInst_ISubmitHandler_t2608359793_0_0_0,
	&GenInst_ICancelHandler_t4180011215_0_0_0,
	&GenInst_Transform_t1659122786_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1744794968_0_0_0,
	&GenInst_PointerEventData_t1848751023_0_0_0,
	&GenInst_ButtonState_t2039702646_0_0_0,
	&GenInst_ICanvasElement_t1249239335_0_0_0,
	&GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ColorBlock_t508458230_0_0_0,
	&GenInst_OptionData_t185687546_0_0_0,
	&GenInst_DropdownItem_t3215807551_0_0_0,
	&GenInst_FloatTween_t2711705593_0_0_0,
	&GenInst_Sprite_t3199167241_0_0_0,
	&GenInst_Canvas_t2727140764_0_0_0,
	&GenInst_List_1_t4095326316_0_0_0,
	&GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0,
	&GenInst_Text_t9039225_0_0_0,
	&GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1496360359_0_0_0,
	&GenInst_ColorTween_t723277650_0_0_0,
	&GenInst_Graphic_t836799438_0_0_0,
	&GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0,
	&GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3226014568_0_0_0,
	&GenInst_KeyValuePair_2_t1354567995_0_0_0,
	&GenInst_KeyValuePair_2_t3983107678_0_0_0,
	&GenInst_ContentType_t2662964855_0_0_0,
	&GenInst_Mask_t8826680_0_0_0,
	&GenInst_List_1_t1377012232_0_0_0,
	&GenInst_RectMask2D_t3357079374_0_0_0,
	&GenInst_List_1_t430297630_0_0_0,
	&GenInst_Navigation_t1108456480_0_0_0,
	&GenInst_IClippable_t502791197_0_0_0,
	&GenInst_Selectable_t1885181538_0_0_0,
	&GenInst_SpriteState_t2895308594_0_0_0,
	&GenInst_CanvasGroup_t3702418109_0_0_0,
	&GenInst_MatEntry_t1574154081_0_0_0,
	&GenInst_Toggle_t110812896_0_0_0,
	&GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_IClipper_t1175935472_0_0_0,
	&GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1329917009_0_0_0,
	&GenInst_RectTransform_t972643934_0_0_0,
	&GenInst_LayoutRebuilder_t1942933988_0_0_0,
	&GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_List_1_t1355284822_0_0_0,
	&GenInst_List_1_t1967039240_0_0_0,
	&GenInst_List_1_t1355284821_0_0_0,
	&GenInst_List_1_t1355284823_0_0_0,
	&GenInst_List_1_t2522024052_0_0_0,
	&GenInst_List_1_t1317283468_0_0_0,
	&GenInst_OfertasValuesCRM_t2856949562_0_0_0,
	&GenInst_Texture2D_t3884108195_0_0_0,
	&GenInst_OfertaCRMData_t637956887_0_0_0,
	&GenInst_OfertasCRMDataGroup_t273423897_0_0_0,
	&GenInst_CortesData_t642513606_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t272385497_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t272385497_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t991584573_0_0_0,
	&GenInst_WWW_t3134621005_0_0_0,
	&GenInst_OfertaData_t3417615771_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t595048151_0_0_0,
	&GenInst_NotificationInstance_t3873938600_0_0_0,
	&GenInst_ISampleAppUIElement_t2180050874_0_0_0,
	&GenInst_SampleAppUIRect_t2784570703_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0,
	&GenInst_EventHandlerFunction_t2672185540_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t4040371092_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3936415037_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0,
	&GenInst_DataEventHandlerFunction_t438967822_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t1807153374_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1703197319_0_0_0,
	&GenInst_MNDialogResult_t3125317574_0_0_0,
	&GenInst_IAccessTokenRefreshResult_t760062154_0_0_0,
	&GenInst_IAppInviteResult_t1487399198_0_0_0,
	&GenInst_IAppLinkResult_t3864497039_0_0_0,
	&GenInst_OGActionType_t3473630824_0_0_0,
	&GenInst_IAppRequestResult_t2931577298_0_0_0,
	&GenInst_IShareResult_t3573065907_0_0_0,
	&GenInst_IGroupCreateResult_t1004608047_0_0_0,
	&GenInst_IGraphResult_t873401058_0_0_0,
	&GenInst_IGroupJoinResult_t2814186909_0_0_0,
	&GenInst_ILoginResult_t528611517_0_0_0,
	&GenInst_IPayResult_t2622336188_0_0_0,
	&GenInst_FacebookUnityPlatform_t1192368832_0_0_0,
	&GenInst_UrlSchemes_t3482710948_0_0_0,
	&GenInst_IResult_t2984644868_0_0_0,
	&GenInst_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_Slider_t79469677_0_0_0,
	&GenInst_DirectoryInfo_t89154617_0_0_0_String_t_0_0_0,
	&GenInst_StackElement_t2425423099_0_0_0,
	&GenInst_TarEntry_t762185187_0_0_0,
	&GenInst_ZipEntry_t3141689087_0_0_0,
	&GenInst_GeneralComponentsAbstract_t3900398046_0_0_0,
	&GenInst_GeneralComponentInterface_t2984273286_0_0_0,
	&GenInst_InAppAbstract_t382673128_0_0_0,
	&GenInst_ProgressEventsAbstract_t2129719228_0_0_0,
	&GenInst_GenericStartableComponentControllAbstract_t1794600275_0_0_0,
	&GenInst_GenericTimeControllInterface_t4025522376_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3830311593_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_NetworkReachability_t612403035_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3830311593_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3430729120_0_0_0,
	&GenInst_Quaternion_t1553702882_0_0_0,
	&GenInst_TransformPositionAngleFilterAbstract_t4158746314_0_0_0,
	&GenInst_TransformPositionAngleFilterInterface_t2619784369_0_0_0,
	&GenInst_OnOffListenerEventInterface_t3960143011_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3570725950_0_0_0,
	&GenInst_DataRow_t3107836336_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_String_t_0_0_0,
	&GenInst_MetadataVO_t2511256998_0_0_0_String_t_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t3308144514_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0_String_t_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t3308144514_0_0_0,
	&GenInst_UrlInfoVO_t1761987528_0_0_0_String_t_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0,
	&GenInst_VOWithId_t2461972856_0_0_0,
	&GenInst_MetadataVO_t2511256998_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0,
	&GenInst_UrlInfoVO_t1761987528_0_0_0,
	&GenInst_KeyValueVO_t1780100105_0_0_0,
	&GenInst_ConfigurableListenerEventInterface_t1985629260_0_0_0,
	&GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0,
	&GenInst_String_t_0_0_0_ConfigurableListenerEventInterface_t1985629260_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2704828336_0_0_0,
	&GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0,
	&GenInst_String_t_0_0_0_OnOffListenerEventInterface_t3960143011_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t384374791_0_0_0,
	&GenInst_FileInfo_t3233670074_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_VideoPlaybackBehaviour_t2848361927_0_0_0,
	&GenInst_BundleDownloadItem_t715938369_0_0_0,
	&GenInst_RequestImagesItemVO_t2061191909_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3352703625_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4071902701_0_0_0,
	&GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0,
	&GenInst_String_t_0_0_0_BundleVOU5BU5D_t2162892100_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2882091176_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t1517202659_0_0_0,
	&GenInst_MetadataVO_t2511256998_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_BundleVO_t1984518073_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0_FileVO_t1935348659_0_0_0,
	&GenInst_UrlInfoVO_t1761987528_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_UrlInfoVO_t1761987528_0_0_0_UrlInfoVO_t1761987528_0_0_0,
	&GenInst_MetadataVO_t2511256998_0_0_0_MetadataVO_t2511256998_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t767933189_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_IEnumerable_1_t941294320_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0_IEnumerable_1_t767933189_0_0_0,
	&GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0,
	&GenInst_String_t_0_0_0_InAppAnalytics_t2316734034_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3035933110_0_0_0,
	&GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0,
	&GenInst_String_t_0_0_0_InAppEventsChannel_t1397713486_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2116912562_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2409664798_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Perspective_t644553802_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2409664798_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_OnChangeAnToPerspectiveEventHandler_t2702236419_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t941084846_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1845333178_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_StateMachine_t367995870_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1845333178_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_OnChangeToAnStateEventHandler_t630243546_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2599727649_0_0_0,
	&GenInst_GarbageItemVO_t1335202303_0_0_0,
	&GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0,
	&GenInst_String_t_0_0_0_AssetBundle_t2070959688_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2790158764_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4186358450_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_EventNames_t1614635846_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4186358450_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_InAppEventDispatcherOnOff_t3474551315_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3490093394_0_0_0,
	&GenInst_String_t_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_KeyValuePair_2_t2065771578_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0,
	&GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t716150752_0_0_0,
	&GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0,
	&GenInst_String_t_0_0_0_VoidEventDispatcher_t1760461203_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2479660279_0_0_0,
	&GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0,
	&GenInst_String_t_0_0_0_GenericParameterEventDispatcher_t3811317845_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t235549625_0_0_0,
	&GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0,
	&GenInst_String_t_0_0_0_GroupInAppAbstractComponentsManager_t739334794_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1458533870_0_0_0,
	&GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0,
	&GenInst_String_t_0_0_0_InAppAbstract_t382673128_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1101872204_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_OnInAppCommonEventHandler_t1653412278_0_0_0_OnInAppProgressEventHandler_t3139301368_0_0_0,
	&GenInst_JsonData_t1715015430_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0,
	&GenInst_KeyValuePair_2_t2434214506_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t1715015430_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0,
	&GenInst_KeyValuePair_2_t1840487222_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0,
	&GenInst_KeyValuePair_2_t1832195516_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0,
	&GenInst_KeyValuePair_2_t4078114400_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0,
	&GenInst_PropertyMetadata_t4066634616_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_ArrayMetadata_t4058342910_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t4058342910_0_0_0_KeyValuePair_2_t1832195516_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t4058342910_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4062546101_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2031895_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t6235086_0_0_0,
	&GenInst_ObjectMetadata_t2009294498_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t2009294498_0_0_0_KeyValuePair_2_t4078114400_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t2009294498_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2013497689_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t2466314523_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2470517714_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t3330360473_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3334563664_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t1821615648_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1825818839_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t4066634616_0_0_0_KeyValuePair_2_t1840487222_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t4066634616_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t490866396_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t322939256_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2138319818_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2142523009_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_IDictionary_2_t2805984405_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2702028350_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32U5BU5D_t3230847821_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3126891766_0_0_0,
	&GenInst_WriterContext_t3060158226_0_0_0,
	&GenInst_StateHandler_t3261942315_0_0_0,
	&GenInst_MulticastDelegate_t3389745971_0_0_0,
	&GenInst_ObjectTracker_t455954211_0_0_0,
	&GenInst_ImageTargetBehaviour_t1735871187_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t2347335889_0_0_0,
	&GenInst_IEditorImageTargetBehaviour_t4106486993_0_0_0,
	&GenInst_ARMRequestVO_t2431191322_0_0_0_OnRequestEventHandler_t153308946_0_0_0_OnProgressEventHandler_t1956336554_0_0_0,
	&GenInst_KeyTypeValue_t654867638_0_0_0,
	&GenInst_ParticleSystem_t381473177_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_FsmState_t2146334067_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2042378012_0_0_0,
	&GenInst_Rigidbody_t3346577219_0_0_0,
	&GenInst_AnimationCurve_t3667593487_0_0_0,
	&GenInst_Calculation_t2191327052_0_0_0,
	&GenInst_Calculation_t2771812670_0_0_0,
	&GenInst_NetworkView_t3656680617_0_0_0,
	&GenInst_Animation_t1724966010_0_0_0,
	&GenInst_AudioClip_t794140988_0_0_0,
	&GenInst_AudioSource_t1740077639_0_0_0,
	&GenInst_GUIText_t3371372606_0_0_0,
	&GenInst_GUITexture_t4020448292_0_0_0,
	&GenInst_Light_t4202674828_0_0_0,
	&GenInst_Action_t3771233898_0_0_0,
	&GenInst_DelayedQueueItem_t3219765808_0_0_0,
	&GenInst_DelayedQueueItem_t3219765808_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_EncodeHintType_t3244562957_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3617918191_0_0_0,
	&GenInst_PedidoData_t3443010735_0_0_0,
	&GenInst_CarnesData_t3441984818_0_0_0,
	&GenInst_OptionData_t185687546_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t3674682005_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t98913785_0_0_0,
	&GenInst_CuponData_t807724487_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_RectTransform_t972643934_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t868687879_0_0_0,
	&GenInst_PublicationData_t473548758_0_0_0,
	&GenInst_PedidoEnvioData_t1309077304_0_0_0,
	&GenInst_OfertaValues_t3488850643_0_0_0,
	&GenInst_UIBehaviour_t2511441271_0_0_0,
	&GenInst_CortesDataU5BU5D_t1695339363_0_0_0,
	&GenInst_TabloideData_t1867721404_0_0_0,
	&GenInst_String_t_0_0_0_Action_t3771233898_0_0_0,
	&GenInst_String_t_0_0_0_Action_t3771233898_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t195465678_0_0_0,
	&GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0,
	&GenInst_UniWebViewNativeResultPayload_t996691953_0_0_0,
	&GenInst_String_t_0_0_0_Action_1_t1392508089_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2111707165_0_0_0,
	&GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0,
	&GenInst_String_t_0_0_0_UniWebViewNativeListener_t795571060_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1514770136_0_0_0,
	&GenInst_WireframeBehaviour_t433318935_0_0_0,
	&GenInst_Hashtable_t1407064410_0_0_0,
	&GenInst_IEnumerable_1_t309087608_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0,
	&GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0,
	&GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0,
	&GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0,
	&GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3566161319_gp_0_0_0_0,
	&GenInst_Array_Sort_m1767877396_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0,
	&GenInst_Array_compare_m3718693973_gp_0_0_0_0,
	&GenInst_Array_qsort_m3270161954_gp_0_0_0_0,
	&GenInst_Array_Resize_m2347367271_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1326446122_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m308210168_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1181152586_gp_0_0_0_0,
	&GenInst_Array_Exists_m2312185345_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0,
	&GenInst_Array_Find_m3878993575_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3671273393_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0,
	&GenInst_IList_1_t2706020430_gp_0_0_0_0,
	&GenInst_ICollection_1_t1952910030_gp_0_0_0_0,
	&GenInst_Nullable_1_t1122404262_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0,
	&GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1912719906_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0,
	&GenInst_Enumerator_t4049866126_gp_0_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0,
	&GenInst_Enumerator_t2802076604_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_1_0_0_0,
	&GenInst_IDictionary_2_t435039943_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2536192150_0_0_0,
	&GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4247873286_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0,
	&GenInst_List_1_t951551555_gp_0_0_0_0,
	&GenInst_Enumerator_t2095969493_gp_0_0_0_0,
	&GenInst_Collection_1_t3473121937_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0,
	&GenInst_Comparer_1_t1701613202_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3219634540_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0,
	&GenInst_LinkedList_1_t2631682684_gp_0_0_0_0,
	&GenInst_Enumerator_t2473886460_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0,
	&GenInst_Stack_1_t4128415215_gp_0_0_0_0,
	&GenInst_Enumerator_t2313787817_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m692115499_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m692115499_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1147197826_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_DefaultIfEmpty_m622648125_gp_0_0_0_0,
	&GenInst_Enumerable_CreateDefaultIfEmptyIterator_m2450906323_gp_0_0_0_0,
	&GenInst_Enumerable_Distinct_m683227859_gp_0_0_0_0,
	&GenInst_Enumerable_Distinct_m1602634822_gp_0_0_0_0,
	&GenInst_Enumerable_CreateDistinctIterator_m116324828_gp_0_0_0_0,
	&GenInst_Enumerable_First_m179991593_gp_0_0_0_0,
	&GenInst_Enumerable_First_m179991593_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_First_m1340241468_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1650429075_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_Last_m3437589648_gp_0_0_0_0,
	&GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0,
	&GenInst_Enumerable_Iterate_m3481514816_gp_0_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0_Enumerable_Iterate_m3481514816_gp_1_0_0_0,
	&GenInst_Enumerable_OfType_m3233395589_gp_0_0_0_0,
	&GenInst_Enumerable_CreateOfTypeIterator_m1358460463_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0,
	&GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m4110359_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0,
	&GenInst_U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t1600972147_gp_0_0_0_0,
	&GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3100449863_gp_0_0_0_0,
	&GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t2905255426_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_HashSet_1_t3763949723_gp_0_0_0_0,
	&GenInst_Enumerator_t1211810685_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3144359540_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m482375811_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_GetStatic_m2919332632_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_Call_m1663154586_gp_0_0_0_0,
	&GenInst_AndroidJavaObject_CallStatic_m870649740_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__Call_m2159746725_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__CallStatic_m369311831_gp_0_0_0_0,
	&GenInst_AndroidJavaObject__GetStatic_m1083753313_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_ConvertFromJNIArray_m1627213724_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_GetMethodID_m2032786278_gp_0_0_0_0,
	&GenInst_AndroidJNIHelper_GetFieldID_m1741341436_gp_0_0_0_0,
	&GenInst__AndroidJNIHelper_GetMethodID_m4173184413_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0,
	&GenInst_Arrays_1_t2739302885_gp_0_0_0_0,
	&GenInst_Lists_1_t4025286752_gp_0_0_0_0,
	&GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_LuminanceSource_t1231523093_0_0_0,
	&GenInst_BarcodeReaderGeneric_1_t3439348539_gp_0_0_0_0,
	&GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0,
	&GenInst_ChangeNotifyDictionary_2_t868889846_gp_0_0_0_0_ChangeNotifyDictionary_2_t868889846_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t974145274_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m330735833_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_ListPool_1_t3815171383_gp_0_0_0_0,
	&GenInst_List_1_t1536022088_0_0_0,
	&GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0,
	&GenInst_ScrollRectEx_DoForParents_m145041647_gp_0_0_0_0,
	&GenInst_CallbackManager_AddFacebookDelegate_m4253588562_gp_0_0_0_0,
	&GenInst_CallbackManager_TryCallCallback_m948328713_gp_0_0_0_0,
	&GenInst_CanvasUIMethodCall_1_t2411194616_gp_0_0_0_0,
	&GenInst_ComponentFactory_GetComponent_m1706671437_gp_0_0_0_0,
	&GenInst_ComponentFactory_AddComponent_m3464507492_gp_0_0_0_0,
	&GenInst_MethodArguments_AddNullablePrimitive_m3778017809_gp_0_0_0_0,
	&GenInst_MethodArguments_AddList_m2038064151_gp_0_0_0_0,
	&GenInst_MethodCall_1_t1724252912_gp_0_0_0_0,
	&GenInst_JavaMethodCall_1_t2135904438_gp_0_0_0_0,
	&GenInst_FBJavaClass_CallStatic_m1002577452_gp_0_0_0_0,
	&GenInst_IOSFacebook_AddCallback_m3533341517_gp_0_0_0_0,
	&GenInst_EditorFacebook_ShowEmptyMockDialog_m3588839153_gp_0_0_0_0,
	&GenInst_Utilities_GetValueOrDefault_m3842879737_gp_0_0_0_0,
	&GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0_Utilities_AddAllKVPFrom_m505241225_gp_1_0_0_0,
	&GenInst_Utilities_AddAllKVPFrom_m505241225_gp_0_0_0_0,
	&GenInst_JsonMapper_RegisterExporter_m1048406668_gp_0_0_0_0,
	&GenInst_JsonMapper_RegisterImporter_m3836829258_gp_0_0_0_0_JsonMapper_RegisterImporter_m3836829258_gp_1_0_0_0,
	&GenInst_U3CRegisterExporterU3Ec__AnonStoreyA2_1_t3613248724_gp_0_0_0_0,
	&GenInst_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStoreyA3_2_t816827911_gp_1_0_0_0,
	&GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0,
	&GenInst_DragMe_FindInParents_m3518302513_gp_0_0_0_0,
	&GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_GUILayer_t2983897946_0_0_0,
	&GenInst_RenderListenerUtility_t2682722690_0_0_0,
	&GenInst_PlayMakerMouseEvents_t316289710_0_0_0,
	&GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0,
	&GenInst_PlayMakerCollisionExit_t3871318016_0_0_0,
	&GenInst_PlayMakerCollisionStay_t3871731003_0_0_0,
	&GenInst_PlayMakerTriggerEnter_t977448304_0_0_0,
	&GenInst_PlayMakerTriggerExit_t3495223174_0_0_0,
	&GenInst_PlayMakerTriggerStay_t3495636161_0_0_0,
	&GenInst_PlayMakerFixedUpdate_t997170669_0_0_0,
	&GenInst_PlayMakerOnGUI_t940239724_0_0_0,
	&GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0,
	&GenInst_PlayMakerGlobals_t3097244096_0_0_0,
	&GenInst_PlayMakerGUI_t3799848395_0_0_0,
	&GenInst_ResultPointCallback_t207829946_0_0_0,
	&GenInst_SmartTerrainTracker_t2067565974_0_0_0,
	&GenInst_ReconstructionFromTarget_t41970945_0_0_0,
	&GenInst_MarkerTracker_t4028259784_0_0_0,
	&GenInst_TextTracker_t3721656949_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t1091759131_0_0_0,
	&GenInst_WebCamAbstractBehaviour_t725705546_0_0_0,
	&GenInst_EventSystem_t2276120119_0_0_0,
	&GenInst_AxisEventData_t3355659985_0_0_0,
	&GenInst_SpriteRenderer_t2548470764_0_0_0,
	&GenInst_AspectMode_t2149445162_0_0_0,
	&GenInst_FitMode_t909765868_0_0_0,
	&GenInst_Image_t538875265_0_0_0,
	&GenInst_Button_t3896396478_0_0_0,
	&GenInst_RawImage_t821930207_0_0_0,
	&GenInst_Scrollbar_t2601556940_0_0_0,
	&GenInst_InputField_t609046876_0_0_0,
	&GenInst_ScrollRect_t3606982749_0_0_0,
	&GenInst_Dropdown_t4201779933_0_0_0,
	&GenInst_GraphicRaycaster_t911782554_0_0_0,
	&GenInst_CanvasRenderer_t3950887807_0_0_0,
	&GenInst_Corner_t284493240_0_0_0,
	&GenInst_Axis_t1399125956_0_0_0,
	&GenInst_Constraint_t1640775616_0_0_0,
	&GenInst_Type_t3063828369_0_0_0,
	&GenInst_FillMethod_t2255824731_0_0_0,
	&GenInst_SubmitEvent_t3081690246_0_0_0,
	&GenInst_OnChangeEvent_t2697516943_0_0_0,
	&GenInst_OnValidateInput_t3952708057_0_0_0,
	&GenInst_LineType_t2016592042_0_0_0,
	&GenInst_InputType_t1602890312_0_0_0,
	&GenInst_TouchScreenKeyboardType_t2604324130_0_0_0,
	&GenInst_CharacterValidation_t737650598_0_0_0,
	&GenInst_LayoutElement_t1596995480_0_0_0,
	&GenInst_RectOffset_t3056157787_0_0_0,
	&GenInst_TextAnchor_t213922566_0_0_0,
	&GenInst_Direction_t522766867_0_0_0,
	&GenInst_Transition_t1922345195_0_0_0,
	&GenInst_AnimationTriggers_t115197445_0_0_0,
	&GenInst_Animator_t2776330603_0_0_0,
	&GenInst_Direction_t94975348_0_0_0,
	&GenInst_PedidoValues_t2123607271_0_0_0,
	&GenInst_DeviceAppStateMachineEvents_t922942516_0_0_0,
	&GenInst_Analytics_t310950758_0_0_0,
	&GenInst_CronWatcher_t1902937828_0_0_0,
	&GenInst_TimeoutItem_t109981810_0_0_0,
	&GenInst_IntervalItem_t889137048_0_0_0,
	&GenInst_GameObjectDownloader_t2978402602_0_0_0,
	&GenInst_SingleGenericDownloader_t168833466_0_0_0,
	&GenInst_ARMSingleRequest_t410726329_0_0_0,
	&GenInst_CEP_t66606_0_0_0,
	&GenInst_CaptureAndSave_t700313070_0_0_0,
	&GenInst_DownloadImages_t3644489024_0_0_0,
	&GenInst_SphereCollider_t111527973_0_0_0,
	&GenInst_CapsuleCollider_t318617463_0_0_0,
	&GenInst_InfiniteHorizontalScroll_t3122519653_0_0_0,
	&GenInst_CarneValues_t2819179885_0_0_0,
	&GenInst_QRCodeEncodeController_t2317858336_0_0_0,
	&GenInst_Cupons_t2029651862_0_0_0,
	&GenInst_UserData_t4092646965_0_0_0,
	&GenInst_ParticleAutoDestruction_t2314186749_0_0_0,
	&GenInst_IncrementalSliderController_t153649975_0_0_0,
	&GenInst_CameraPlaneController_t4203072787_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_MetadataVO_t2511256998_0_0_0,
	&GenInst_DragMe_t2055054860_0_0_0,
	&GenInst_IDictionary_2_t274140790_0_0_0,
	&GenInst_IEnumerable_1_t3176762032_0_0_0,
	&GenInst_AsyncRequestString_t2872237476_0_0_0,
	&GenInst_JsBridge_t2292005674_0_0_0,
	&GenInst_CanvasFacebookGameObject_t2832912295_0_0_0,
	&GenInst_EditorFacebookGameObject_t3558123006_0_0_0,
	&GenInst_MockLoginDialog_t760013837_0_0_0,
	&GenInst_MockShareDialog_t3804468227_0_0_0,
	&GenInst_FacebookSettings_t3725355145_0_0_0,
	&GenInst_EditorFacebookLoader_t2301290944_0_0_0,
	&GenInst_CanvasFacebookLoader_t3297174377_0_0_0,
	&GenInst_IOSFacebookLoader_t562470544_0_0_0,
	&GenInst_AndroidFacebookLoader_t148490282_0_0_0,
	&GenInst_AndroidFacebookGameObject_t2212214888_0_0_0,
	&GenInst_IOSFacebookGameObject_t4031893710_0_0_0,
	&GenInst_FacebookData_t4095115184_0_0_0,
	&GenInst_ItemMoveDrag_t687865080_0_0_0,
	&GenInst_PublicationsData_t1837633585_0_0_0,
	&GenInst_LightsControl_t2444629088_0_0_0,
	&GenInst_ServerControl_t2725829754_0_0_0,
	&GenInst_PlayMakerAnimatorMoveProxy_t4175490694_0_0_0,
	&GenInst_CharacterController_t1618060635_0_0_0,
	&GenInst_playMakerShurikenProxy_t1235561697_0_0_0,
	&GenInst_iTweenFSMEvents_t871409943_0_0_0,
	&GenInst_NavMeshAgent_t588466745_0_0_0,
	&GenInst_PlayMakerAnimatorIKProxy_t1024752181_0_0_0,
	&GenInst_Joint_t4201008640_0_0_0,
	&GenInst_HorizontalLayoutGroup_t1336501463_0_0_0,
	&GenInst_GridLayoutGroup_t169317941_0_0_0,
	&GenInst_iTween_t3087282050_0_0_0,
	&GenInst_Loom_t2374337_0_0_0,
	&GenInst_BulkBundleDownloader_t2985631149_0_0_0,
	&GenInst_GenericDownloadManager_t3298305714_0_0_0,
	&GenInst_ResultRevisionVO_t2445597391_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_UrlInfoVO_t1761987528_0_0_0,
	&GenInst_BundleVO_t1984518073_0_0_0_FileVO_t1935348659_0_0_0,
	&GenInst_FileVO_t1935348659_0_0_0_UrlInfoVO_t1761987528_0_0_0,
	&GenInst_AmIVisible_t1175015477_0_0_0,
	&GenInst_GroupInAppAbstractComponentsManager_t739334794_0_0_0,
	&GenInst_LoadLevelName_t284939721_0_0_0,
	&GenInst_ARMVideoPlaybackBehaviour_t1131738883_0_0_0,
	&GenInst_ServerRequestMobile_t675100796_0_0_0,
	&GenInst_RevisionInfoPingVO_t4026113458_0_0_0,
	&GenInst_RevisionInfoVO_t1983749152_0_0_0,
	&GenInst_KeyTypeValueU5BU5D_t1961483187_0_0_0,
	&GenInst_ButtonAnimationSoundScript_t4202863368_0_0_0,
	&GenInst_MeuPedidoData_t2688356908_0_0_0,
	&GenInst_OfertasData_t2909397772_0_0_0,
	&GenInst_MNAndroidDialog_t2972443766_0_0_0,
	&GenInst_MNAndroidMessage_t1251344025_0_0_0,
	&GenInst_MNAndroidRateUsPopUp_t2446611808_0_0_0,
	&GenInst_MNIOSDialog_t768531220_0_0_0,
	&GenInst_MNIOSMessage_t1649531835_0_0_0,
	&GenInst_MNIOSRateUsPopUp_t2151207298_0_0_0,
	&GenInst_MNWP8Dialog_t489019942_0_0_0,
	&GenInst_MNWP8Message_t1574616809_0_0_0,
	&GenInst_MNWP8RateUsPopUp_t4179652016_0_0_0,
	&GenInst_NativeImagePickerBehaviour_t3828351720_0_0_0,
	&GenInst_NotificationObj_t1964455916_0_0_0,
	&GenInst_OfertasCRMDataServer_t217840361_0_0_0,
	&GenInst_OfertasCRMDataExclusivas_t2158992831_0_0_0,
	&GenInst_OneSignal_t3595519118_0_0_0,
	&GenInst_AcougueData_t537282265_0_0_0,
	&GenInst_DeviceCameraController_t2043460407_0_0_0,
	&GenInst_SampleInitErrorHandler_t2792208188_0_0_0,
	&GenInst_Screenshot_t1577017734_0_0_0,
	&GenInst_ScreenshotManager_t4227900231_0_0_0,
	&GenInst_GameMain_t2590236907_0_0_0,
	&GenInst_JsonPlay_t2384567388_0_0_0,
	&GenInst_TabloideValues_t99048372_0_0_0,
	&GenInst_TabloidesData_t2107312651_0_0_0,
	&GenInst_Corner_t4192955334_0_0_0,
	&GenInst_Direction_t361841815_0_0_0,
	&GenInst_UniWebViewNativeListener_t795571060_0_0_0,
	&GenInst_barGraphController_t2598333175_0_0_0,
	&GenInst_Extrato_t1617908080_0_0_0,
	&GenInst_ReconstructionBehaviour_t3695833539_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t2489055709_0_0_0,
	&GenInst_VuforiaBehaviour_t1845338141_0_0_0,
	&GenInst_MaskOutBehaviour_t1204933533_0_0_0,
	&GenInst_VirtualButtonBehaviour_t2253448098_0_0_0,
	&GenInst_TurnOffBehaviour_t1278464685_0_0_0,
	&GenInst_MarkerBehaviour_t3170674893_0_0_0,
	&GenInst_MultiTargetBehaviour_t2222860085_0_0_0,
	&GenInst_CylinderTargetBehaviour_t1850077856_0_0_0,
	&GenInst_WordBehaviour_t2034767101_0_0_0,
	&GenInst_TextRecoBehaviour_t892056475_0_0_0,
	&GenInst_ObjectTargetBehaviour_t2508606743_0_0_0,
	&GenInst_MagicTVWindowInAppGUIControllerScript_t4145218373_0_0_0,
	&GenInst_MagicTVAbstractWindowPopUpControllerScript_t2050906514_0_0_0,
	&GenInst_MagicTVWindowAlertControllerScript_t1344644260_0_0_0,
	&GenInst_MagicTVWindowConfirmControllerScript_t165107144_0_0_0,
	&GenInst_MagicTVWindowPromptControllerScript_t1472770442_0_0_0,
	&GenInst_EmptyMockDialog_t1261978245_0_0_0,
	&GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0,
	&GenInst_PropertyMetadata_t4066634616_0_0_0_PropertyMetadata_t4066634616_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_Double_t3868226565_0_0_0_Double_t3868226565_0_0_0,
	&GenInst_Int64_t1153838595_0_0_0_Int64_t1153838595_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0,
	&GenInst_Color_t4194546905_0_0_0_Color_t4194546905_0_0_0,
	&GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0,
	&GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0,
	&GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0,
	&GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0,
	&GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0,
	&GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0,
	&GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0,
	&GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0,
	&GenInst_PIXEL_FORMAT_t354375056_0_0_0_PIXEL_FORMAT_t354375056_0_0_0,
	&GenInst_TargetSearchResult_t3609410114_0_0_0_TargetSearchResult_t3609410114_0_0_0,
	&GenInst_EventNames_t1614635846_0_0_0_EventNames_t1614635846_0_0_0,
	&GenInst_KeyValuePair_2_t4186358450_0_0_0_KeyValuePair_2_t4186358450_0_0_0,
	&GenInst_KeyValuePair_2_t4186358450_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Perspective_t644553802_0_0_0_Perspective_t644553802_0_0_0,
	&GenInst_KeyValuePair_2_t2409664798_0_0_0_KeyValuePair_2_t2409664798_0_0_0,
	&GenInst_KeyValuePair_2_t2409664798_0_0_0_Il2CppObject_0_0_0,
	&GenInst_StateMachine_t367995870_0_0_0_StateMachine_t367995870_0_0_0,
	&GenInst_KeyValuePair_2_t1845333178_0_0_0_KeyValuePair_2_t1845333178_0_0_0,
	&GenInst_KeyValuePair_2_t1845333178_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t996820380_0_0_0_KeyValuePair_2_t996820380_0_0_0,
	&GenInst_KeyValuePair_2_t996820380_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Table_t1007478513_0_0_0_Table_t1007478513_0_0_0,
	&GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0,
	&GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2164509075_0_0_0_KeyValuePair_2_t2164509075_0_0_0,
	&GenInst_KeyValuePair_2_t2164509075_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t2268465130_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t2268465130_0_0_0_Label_t2268465130_0_0_0,
	&GenInst_KeyValuePair_2_t291920669_0_0_0_KeyValuePair_2_t291920669_0_0_0,
	&GenInst_KeyValuePair_2_t291920669_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TrackableResultData_t395876724_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TrackableResultData_t395876724_0_0_0_TrackableResultData_t395876724_0_0_0,
	&GenInst_KeyValuePair_2_t355424280_0_0_0_KeyValuePair_2_t355424280_0_0_0,
	&GenInst_KeyValuePair_2_t355424280_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t459380335_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t459380335_0_0_0_VirtualButtonData_t459380335_0_0_0,
	&GenInst_ArrayMetadata_t4058342910_0_0_0_ArrayMetadata_t4058342910_0_0_0,
	&GenInst_ArrayMetadata_t4058342910_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1832195516_0_0_0_KeyValuePair_2_t1832195516_0_0_0,
	&GenInst_KeyValuePair_2_t1832195516_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ObjectMetadata_t2009294498_0_0_0_ObjectMetadata_t2009294498_0_0_0,
	&GenInst_ObjectMetadata_t2009294498_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4078114400_0_0_0_KeyValuePair_2_t4078114400_0_0_0,
	&GenInst_KeyValuePair_2_t4078114400_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PropertyMetadata_t4066634616_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1840487222_0_0_0_KeyValuePair_2_t1840487222_0_0_0,
	&GenInst_KeyValuePair_2_t1840487222_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0,
	&GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2093487825_0_0_0_KeyValuePair_2_t2093487825_0_0_0,
	&GenInst_KeyValuePair_2_t2093487825_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0_UInt16_t24667923_0_0_0,
	&GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0,
	&GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_KeyValuePair_2_t4030510692_0_0_0_KeyValuePair_2_t4030510692_0_0_0,
	&GenInst_KeyValuePair_2_t4030510692_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t1961690790_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t1961690790_0_0_0_ProfileData_t1961690790_0_0_0,
	&GenInst_KeyValuePair_2_t3830311593_0_0_0_KeyValuePair_2_t3830311593_0_0_0,
	&GenInst_KeyValuePair_2_t3830311593_0_0_0_Il2CppObject_0_0_0,
	&GenInst_NetworkReachability_t612403035_0_0_0_NetworkReachability_t612403035_0_0_0,
	&GenInst_KeyValuePair_2_t1718082560_0_0_0_KeyValuePair_2_t1718082560_0_0_0,
	&GenInst_KeyValuePair_2_t1718082560_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2656869371_0_0_0_KeyValuePair_2_t2656869371_0_0_0,
	&GenInst_KeyValuePair_2_t2656869371_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1188478419_0_0_0_KeyValuePair_2_t1188478419_0_0_0,
	&GenInst_KeyValuePair_2_t1188478419_0_0_0_Il2CppObject_0_0_0,
	&GenInst_BarcodeFormat_t4201805817_0_0_0_BarcodeFormat_t4201805817_0_0_0,
	&GenInst_KeyValuePair_2_t1080897207_0_0_0_KeyValuePair_2_t1080897207_0_0_0,
	&GenInst_KeyValuePair_2_t1080897207_0_0_0_Il2CppObject_0_0_0,
	&GenInst_DecodeHintType_t2095781349_0_0_0_DecodeHintType_t2095781349_0_0_0,
	&GenInst_KeyValuePair_2_t3617918191_0_0_0_KeyValuePair_2_t3617918191_0_0_0,
	&GenInst_KeyValuePair_2_t3617918191_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EncodeHintType_t3244562957_0_0_0_EncodeHintType_t3244562957_0_0_0,
	&GenInst_KeyValuePair_2_t3033809700_0_0_0_KeyValuePair_2_t3033809700_0_0_0,
	&GenInst_KeyValuePair_2_t3033809700_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ResultMetadataType_t2923366972_0_0_0_ResultMetadataType_t2923366972_0_0_0,
};
