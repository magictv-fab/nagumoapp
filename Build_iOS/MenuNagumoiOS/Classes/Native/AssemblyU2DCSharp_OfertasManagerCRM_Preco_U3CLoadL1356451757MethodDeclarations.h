﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13
struct U3CLoadLoadedsU3Ec__Iterator13_t1356451757;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator13__ctor_m601891726 (U3CLoadLoadedsU3Ec__Iterator13_t1356451757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1194942414 (U3CLoadLoadedsU3Ec__Iterator13_t1356451757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m579911522 (U3CLoadLoadedsU3Ec__Iterator13_t1356451757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator13_MoveNext_m1460185742 (U3CLoadLoadedsU3Ec__Iterator13_t1356451757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator13_Dispose_m2790614091 (U3CLoadLoadedsU3Ec__Iterator13_t1356451757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco/<LoadLoadeds>c__Iterator13::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator13_Reset_m2543291963 (U3CLoadLoadedsU3Ec__Iterator13_t1356451757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
