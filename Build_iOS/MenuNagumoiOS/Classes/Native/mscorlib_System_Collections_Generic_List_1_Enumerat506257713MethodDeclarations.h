﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m722250425(__this, ___l0, method) ((  void (*) (Enumerator_t506257713 *, List_1_t486584943 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1978587641(__this, method) ((  void (*) (Enumerator_t506257713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m465353135(__this, method) ((  Il2CppObject * (*) (Enumerator_t506257713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::Dispose()
#define Enumerator_Dispose_m1845540574(__this, method) ((  void (*) (Enumerator_t506257713 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::VerifyState()
#define Enumerator_VerifyState_m3894531863(__this, method) ((  void (*) (Enumerator_t506257713 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::MoveNext()
#define Enumerator_MoveNext_m4087144754(__this, method) ((  bool (*) (Enumerator_t506257713 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.OneD.RSS.Expanded.ExpandedPair>::get_Current()
#define Enumerator_get_Current_m2150610026(__this, method) ((  ExpandedPair_t3413366687 * (*) (Enumerator_t506257713 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
