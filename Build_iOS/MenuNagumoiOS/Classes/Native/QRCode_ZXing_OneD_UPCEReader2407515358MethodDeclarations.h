﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCEReader
struct UPCEReader_t2407515358;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void ZXing.OneD.UPCEReader::.ctor()
extern "C"  void UPCEReader__ctor_m2284345605 (UPCEReader_t2407515358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.UPCEReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern "C"  int32_t UPCEReader_decodeMiddle_m1563616672 (UPCEReader_t2407515358 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___startRange1, StringBuilder_t243639308 * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.UPCEReader::decodeEnd(ZXing.Common.BitArray,System.Int32)
extern "C"  Int32U5BU5D_t3230847821* UPCEReader_decodeEnd_m3382519622 (UPCEReader_t2407515358 * __this, BitArray_t4163851164 * ___row0, int32_t ___endStart1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.UPCEReader::checkChecksum(System.String)
extern "C"  bool UPCEReader_checkChecksum_m3827154528 (UPCEReader_t2407515358 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.UPCEReader::determineNumSysAndCheckDigit(System.Text.StringBuilder,System.Int32)
extern "C"  bool UPCEReader_determineNumSysAndCheckDigit_m3620050362 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___resultString0, int32_t ___lgPatternFound1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.BarcodeFormat ZXing.OneD.UPCEReader::get_BarcodeFormat()
extern "C"  int32_t UPCEReader_get_BarcodeFormat_m442395095 (UPCEReader_t2407515358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.UPCEReader::convertUPCEtoUPCA(System.String)
extern "C"  String_t* UPCEReader_convertUPCEtoUPCA_m235382008 (Il2CppObject * __this /* static, unused */, String_t* ___upce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCEReader::.cctor()
extern "C"  void UPCEReader__cctor_m1613140808 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
