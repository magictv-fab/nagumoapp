﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.InvokeMethod
struct  InvokeMethod_t2919758241  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.InvokeMethod::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.InvokeMethod::behaviour
	FsmString_t952858651 * ___behaviour_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.InvokeMethod::methodName
	FsmString_t952858651 * ___methodName_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.InvokeMethod::delay
	FsmFloat_t2134102846 * ___delay_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.InvokeMethod::repeating
	FsmBool_t1075959796 * ___repeating_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.InvokeMethod::repeatDelay
	FsmFloat_t2134102846 * ___repeatDelay_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.InvokeMethod::cancelOnExit
	FsmBool_t1075959796 * ___cancelOnExit_15;
	// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Actions.InvokeMethod::component
	MonoBehaviour_t667441552 * ___component_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_behaviour_10() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___behaviour_10)); }
	inline FsmString_t952858651 * get_behaviour_10() const { return ___behaviour_10; }
	inline FsmString_t952858651 ** get_address_of_behaviour_10() { return &___behaviour_10; }
	inline void set_behaviour_10(FsmString_t952858651 * value)
	{
		___behaviour_10 = value;
		Il2CppCodeGenWriteBarrier(&___behaviour_10, value);
	}

	inline static int32_t get_offset_of_methodName_11() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___methodName_11)); }
	inline FsmString_t952858651 * get_methodName_11() const { return ___methodName_11; }
	inline FsmString_t952858651 ** get_address_of_methodName_11() { return &___methodName_11; }
	inline void set_methodName_11(FsmString_t952858651 * value)
	{
		___methodName_11 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_11, value);
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___delay_12)); }
	inline FsmFloat_t2134102846 * get_delay_12() const { return ___delay_12; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(FsmFloat_t2134102846 * value)
	{
		___delay_12 = value;
		Il2CppCodeGenWriteBarrier(&___delay_12, value);
	}

	inline static int32_t get_offset_of_repeating_13() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___repeating_13)); }
	inline FsmBool_t1075959796 * get_repeating_13() const { return ___repeating_13; }
	inline FsmBool_t1075959796 ** get_address_of_repeating_13() { return &___repeating_13; }
	inline void set_repeating_13(FsmBool_t1075959796 * value)
	{
		___repeating_13 = value;
		Il2CppCodeGenWriteBarrier(&___repeating_13, value);
	}

	inline static int32_t get_offset_of_repeatDelay_14() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___repeatDelay_14)); }
	inline FsmFloat_t2134102846 * get_repeatDelay_14() const { return ___repeatDelay_14; }
	inline FsmFloat_t2134102846 ** get_address_of_repeatDelay_14() { return &___repeatDelay_14; }
	inline void set_repeatDelay_14(FsmFloat_t2134102846 * value)
	{
		___repeatDelay_14 = value;
		Il2CppCodeGenWriteBarrier(&___repeatDelay_14, value);
	}

	inline static int32_t get_offset_of_cancelOnExit_15() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___cancelOnExit_15)); }
	inline FsmBool_t1075959796 * get_cancelOnExit_15() const { return ___cancelOnExit_15; }
	inline FsmBool_t1075959796 ** get_address_of_cancelOnExit_15() { return &___cancelOnExit_15; }
	inline void set_cancelOnExit_15(FsmBool_t1075959796 * value)
	{
		___cancelOnExit_15 = value;
		Il2CppCodeGenWriteBarrier(&___cancelOnExit_15, value);
	}

	inline static int32_t get_offset_of_component_16() { return static_cast<int32_t>(offsetof(InvokeMethod_t2919758241, ___component_16)); }
	inline MonoBehaviour_t667441552 * get_component_16() const { return ___component_16; }
	inline MonoBehaviour_t667441552 ** get_address_of_component_16() { return &___component_16; }
	inline void set_component_16(MonoBehaviour_t667441552 * value)
	{
		___component_16 = value;
		Il2CppCodeGenWriteBarrier(&___component_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
