﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTagCount
struct  GetTagCount_t870241689  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetTagCount::tag
	FsmString_t952858651 * ___tag_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTagCount::storeResult
	FsmInt_t1596138449 * ___storeResult_10;

public:
	inline static int32_t get_offset_of_tag_9() { return static_cast<int32_t>(offsetof(GetTagCount_t870241689, ___tag_9)); }
	inline FsmString_t952858651 * get_tag_9() const { return ___tag_9; }
	inline FsmString_t952858651 ** get_address_of_tag_9() { return &___tag_9; }
	inline void set_tag_9(FsmString_t952858651 * value)
	{
		___tag_9 = value;
		Il2CppCodeGenWriteBarrier(&___tag_9, value);
	}

	inline static int32_t get_offset_of_storeResult_10() { return static_cast<int32_t>(offsetof(GetTagCount_t870241689, ___storeResult_10)); }
	inline FsmInt_t1596138449 * get_storeResult_10() const { return ___storeResult_10; }
	inline FsmInt_t1596138449 ** get_address_of_storeResult_10() { return &___storeResult_10; }
	inline void set_storeResult_10(FsmInt_t1596138449 * value)
	{
		___storeResult_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
