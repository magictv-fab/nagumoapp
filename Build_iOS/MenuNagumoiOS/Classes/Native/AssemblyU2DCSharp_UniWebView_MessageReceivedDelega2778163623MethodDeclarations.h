﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/MessageReceivedDelegate
struct MessageReceivedDelegate_t2778163623;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "AssemblyU2DCSharp_UniWebViewMessage4192712350.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/MessageReceivedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MessageReceivedDelegate__ctor_m1172153550 (MessageReceivedDelegate_t2778163623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/MessageReceivedDelegate::Invoke(UniWebView,UniWebViewMessage)
extern "C"  void MessageReceivedDelegate_Invoke_m2260320429 (MessageReceivedDelegate_t2778163623 * __this, UniWebView_t424341801 * ___webView0, UniWebViewMessage_t4192712350  ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/MessageReceivedDelegate::BeginInvoke(UniWebView,UniWebViewMessage,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MessageReceivedDelegate_BeginInvoke_m1621211710 (MessageReceivedDelegate_t2778163623 * __this, UniWebView_t424341801 * ___webView0, UniWebViewMessage_t4192712350  ___message1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/MessageReceivedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void MessageReceivedDelegate_EndInvoke_m3513757150 (MessageReceivedDelegate_t2778163623 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
