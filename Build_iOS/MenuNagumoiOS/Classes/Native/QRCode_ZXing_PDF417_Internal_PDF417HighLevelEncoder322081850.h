﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.SByte[]
struct SByteU5BU5D_t2505034988;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417HighLevelEncoder
struct  PDF417HighLevelEncoder_t322081850  : public Il2CppObject
{
public:

public:
};

struct PDF417HighLevelEncoder_t322081850_StaticFields
{
public:
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::TEXT_MIXED_RAW
	SByteU5BU5D_t2505034988* ___TEXT_MIXED_RAW_0;
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::TEXT_PUNCTUATION_RAW
	SByteU5BU5D_t2505034988* ___TEXT_PUNCTUATION_RAW_1;
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::MIXED
	SByteU5BU5D_t2505034988* ___MIXED_2;
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::PUNCTUATION
	SByteU5BU5D_t2505034988* ___PUNCTUATION_3;
	// System.String ZXing.PDF417.Internal.PDF417HighLevelEncoder::DEFAULT_ENCODING_NAME
	String_t* ___DEFAULT_ENCODING_NAME_4;

public:
	inline static int32_t get_offset_of_TEXT_MIXED_RAW_0() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_t322081850_StaticFields, ___TEXT_MIXED_RAW_0)); }
	inline SByteU5BU5D_t2505034988* get_TEXT_MIXED_RAW_0() const { return ___TEXT_MIXED_RAW_0; }
	inline SByteU5BU5D_t2505034988** get_address_of_TEXT_MIXED_RAW_0() { return &___TEXT_MIXED_RAW_0; }
	inline void set_TEXT_MIXED_RAW_0(SByteU5BU5D_t2505034988* value)
	{
		___TEXT_MIXED_RAW_0 = value;
		Il2CppCodeGenWriteBarrier(&___TEXT_MIXED_RAW_0, value);
	}

	inline static int32_t get_offset_of_TEXT_PUNCTUATION_RAW_1() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_t322081850_StaticFields, ___TEXT_PUNCTUATION_RAW_1)); }
	inline SByteU5BU5D_t2505034988* get_TEXT_PUNCTUATION_RAW_1() const { return ___TEXT_PUNCTUATION_RAW_1; }
	inline SByteU5BU5D_t2505034988** get_address_of_TEXT_PUNCTUATION_RAW_1() { return &___TEXT_PUNCTUATION_RAW_1; }
	inline void set_TEXT_PUNCTUATION_RAW_1(SByteU5BU5D_t2505034988* value)
	{
		___TEXT_PUNCTUATION_RAW_1 = value;
		Il2CppCodeGenWriteBarrier(&___TEXT_PUNCTUATION_RAW_1, value);
	}

	inline static int32_t get_offset_of_MIXED_2() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_t322081850_StaticFields, ___MIXED_2)); }
	inline SByteU5BU5D_t2505034988* get_MIXED_2() const { return ___MIXED_2; }
	inline SByteU5BU5D_t2505034988** get_address_of_MIXED_2() { return &___MIXED_2; }
	inline void set_MIXED_2(SByteU5BU5D_t2505034988* value)
	{
		___MIXED_2 = value;
		Il2CppCodeGenWriteBarrier(&___MIXED_2, value);
	}

	inline static int32_t get_offset_of_PUNCTUATION_3() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_t322081850_StaticFields, ___PUNCTUATION_3)); }
	inline SByteU5BU5D_t2505034988* get_PUNCTUATION_3() const { return ___PUNCTUATION_3; }
	inline SByteU5BU5D_t2505034988** get_address_of_PUNCTUATION_3() { return &___PUNCTUATION_3; }
	inline void set_PUNCTUATION_3(SByteU5BU5D_t2505034988* value)
	{
		___PUNCTUATION_3 = value;
		Il2CppCodeGenWriteBarrier(&___PUNCTUATION_3, value);
	}

	inline static int32_t get_offset_of_DEFAULT_ENCODING_NAME_4() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_t322081850_StaticFields, ___DEFAULT_ENCODING_NAME_4)); }
	inline String_t* get_DEFAULT_ENCODING_NAME_4() const { return ___DEFAULT_ENCODING_NAME_4; }
	inline String_t** get_address_of_DEFAULT_ENCODING_NAME_4() { return &___DEFAULT_ENCODING_NAME_4; }
	inline void set_DEFAULT_ENCODING_NAME_4(String_t* value)
	{
		___DEFAULT_ENCODING_NAME_4 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_ENCODING_NAME_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
