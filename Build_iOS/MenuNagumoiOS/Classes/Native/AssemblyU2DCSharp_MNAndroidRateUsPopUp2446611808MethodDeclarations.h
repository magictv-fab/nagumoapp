﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNAndroidRateUsPopUp
struct MNAndroidRateUsPopUp_t2446611808;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNAndroidRateUsPopUp::.ctor()
extern "C"  void MNAndroidRateUsPopUp__ctor_m1374556475 (MNAndroidRateUsPopUp_t2446611808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidRateUsPopUp MNAndroidRateUsPopUp::Create(System.String,System.String,System.String)
extern "C"  MNAndroidRateUsPopUp_t2446611808 * MNAndroidRateUsPopUp_Create_m1699294712 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___url2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidRateUsPopUp MNAndroidRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  MNAndroidRateUsPopUp_t2446611808 * MNAndroidRateUsPopUp_Create_m1048847788 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___url2, String_t* ___yes3, String_t* ___later4, String_t* ___no5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidRateUsPopUp::init()
extern "C"  void MNAndroidRateUsPopUp_init_m1604746649 (MNAndroidRateUsPopUp_t2446611808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidRateUsPopUp::onPopUpCallBack(System.String)
extern "C"  void MNAndroidRateUsPopUp_onPopUpCallBack_m935750455 (MNAndroidRateUsPopUp_t2446611808 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
