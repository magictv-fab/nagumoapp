﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextureScale
struct TextureScale_t2944241935;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void TextureScale::.ctor()
extern "C"  void TextureScale__ctor_m3729465964 (TextureScale_t2944241935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::Point(UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  void TextureScale_Point_m1798251554 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___tex0, int32_t ___newWidth1, int32_t ___newHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::Bilinear(UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  void TextureScale_Bilinear_m4233902840 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___tex0, int32_t ___newWidth1, int32_t ___newHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::ThreadedScale(UnityEngine.Texture2D,System.Int32,System.Int32,System.Boolean)
extern "C"  void TextureScale_ThreadedScale_m3873504044 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___tex0, int32_t ___newWidth1, int32_t ___newHeight2, bool ___useBilinear3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::BilinearScale(System.Object)
extern "C"  void TextureScale_BilinearScale_m394702092 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::PointScale(System.Object)
extern "C"  void TextureScale_PointScale_m1143083938 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TextureScale::ColorLerpUnclamped(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t4194546905  TextureScale_ColorLerpUnclamped_m3069520581 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___c10, Color_t4194546905  ___c21, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
