﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchObjectEvent
struct  TouchObjectEvent_t2600801668  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TouchObjectEvent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.TouchObjectEvent::pickDistance
	FsmFloat_t2134102846 * ___pickDistance_10;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchObjectEvent::fingerId
	FsmInt_t1596138449 * ___fingerId_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchBegan
	FsmEvent_t2133468028 * ___touchBegan_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchMoved
	FsmEvent_t2133468028 * ___touchMoved_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchStationary
	FsmEvent_t2133468028 * ___touchStationary_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchEnded
	FsmEvent_t2133468028 * ___touchEnded_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchCanceled
	FsmEvent_t2133468028 * ___touchCanceled_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchObjectEvent::storeFingerId
	FsmInt_t1596138449 * ___storeFingerId_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchObjectEvent::storeHitPoint
	FsmVector3_t533912882 * ___storeHitPoint_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchObjectEvent::storeHitNormal
	FsmVector3_t533912882 * ___storeHitNormal_19;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_pickDistance_10() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___pickDistance_10)); }
	inline FsmFloat_t2134102846 * get_pickDistance_10() const { return ___pickDistance_10; }
	inline FsmFloat_t2134102846 ** get_address_of_pickDistance_10() { return &___pickDistance_10; }
	inline void set_pickDistance_10(FsmFloat_t2134102846 * value)
	{
		___pickDistance_10 = value;
		Il2CppCodeGenWriteBarrier(&___pickDistance_10, value);
	}

	inline static int32_t get_offset_of_fingerId_11() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___fingerId_11)); }
	inline FsmInt_t1596138449 * get_fingerId_11() const { return ___fingerId_11; }
	inline FsmInt_t1596138449 ** get_address_of_fingerId_11() { return &___fingerId_11; }
	inline void set_fingerId_11(FsmInt_t1596138449 * value)
	{
		___fingerId_11 = value;
		Il2CppCodeGenWriteBarrier(&___fingerId_11, value);
	}

	inline static int32_t get_offset_of_touchBegan_12() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___touchBegan_12)); }
	inline FsmEvent_t2133468028 * get_touchBegan_12() const { return ___touchBegan_12; }
	inline FsmEvent_t2133468028 ** get_address_of_touchBegan_12() { return &___touchBegan_12; }
	inline void set_touchBegan_12(FsmEvent_t2133468028 * value)
	{
		___touchBegan_12 = value;
		Il2CppCodeGenWriteBarrier(&___touchBegan_12, value);
	}

	inline static int32_t get_offset_of_touchMoved_13() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___touchMoved_13)); }
	inline FsmEvent_t2133468028 * get_touchMoved_13() const { return ___touchMoved_13; }
	inline FsmEvent_t2133468028 ** get_address_of_touchMoved_13() { return &___touchMoved_13; }
	inline void set_touchMoved_13(FsmEvent_t2133468028 * value)
	{
		___touchMoved_13 = value;
		Il2CppCodeGenWriteBarrier(&___touchMoved_13, value);
	}

	inline static int32_t get_offset_of_touchStationary_14() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___touchStationary_14)); }
	inline FsmEvent_t2133468028 * get_touchStationary_14() const { return ___touchStationary_14; }
	inline FsmEvent_t2133468028 ** get_address_of_touchStationary_14() { return &___touchStationary_14; }
	inline void set_touchStationary_14(FsmEvent_t2133468028 * value)
	{
		___touchStationary_14 = value;
		Il2CppCodeGenWriteBarrier(&___touchStationary_14, value);
	}

	inline static int32_t get_offset_of_touchEnded_15() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___touchEnded_15)); }
	inline FsmEvent_t2133468028 * get_touchEnded_15() const { return ___touchEnded_15; }
	inline FsmEvent_t2133468028 ** get_address_of_touchEnded_15() { return &___touchEnded_15; }
	inline void set_touchEnded_15(FsmEvent_t2133468028 * value)
	{
		___touchEnded_15 = value;
		Il2CppCodeGenWriteBarrier(&___touchEnded_15, value);
	}

	inline static int32_t get_offset_of_touchCanceled_16() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___touchCanceled_16)); }
	inline FsmEvent_t2133468028 * get_touchCanceled_16() const { return ___touchCanceled_16; }
	inline FsmEvent_t2133468028 ** get_address_of_touchCanceled_16() { return &___touchCanceled_16; }
	inline void set_touchCanceled_16(FsmEvent_t2133468028 * value)
	{
		___touchCanceled_16 = value;
		Il2CppCodeGenWriteBarrier(&___touchCanceled_16, value);
	}

	inline static int32_t get_offset_of_storeFingerId_17() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___storeFingerId_17)); }
	inline FsmInt_t1596138449 * get_storeFingerId_17() const { return ___storeFingerId_17; }
	inline FsmInt_t1596138449 ** get_address_of_storeFingerId_17() { return &___storeFingerId_17; }
	inline void set_storeFingerId_17(FsmInt_t1596138449 * value)
	{
		___storeFingerId_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeFingerId_17, value);
	}

	inline static int32_t get_offset_of_storeHitPoint_18() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___storeHitPoint_18)); }
	inline FsmVector3_t533912882 * get_storeHitPoint_18() const { return ___storeHitPoint_18; }
	inline FsmVector3_t533912882 ** get_address_of_storeHitPoint_18() { return &___storeHitPoint_18; }
	inline void set_storeHitPoint_18(FsmVector3_t533912882 * value)
	{
		___storeHitPoint_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitPoint_18, value);
	}

	inline static int32_t get_offset_of_storeHitNormal_19() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t2600801668, ___storeHitNormal_19)); }
	inline FsmVector3_t533912882 * get_storeHitNormal_19() const { return ___storeHitNormal_19; }
	inline FsmVector3_t533912882 ** get_address_of_storeHitNormal_19() { return &___storeHitNormal_19; }
	inline void set_storeHitNormal_19(FsmVector3_t533912882 * value)
	{
		___storeHitNormal_19 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitNormal_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
