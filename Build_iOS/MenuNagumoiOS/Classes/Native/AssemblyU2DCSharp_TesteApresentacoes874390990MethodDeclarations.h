﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TesteApresentacoes
struct TesteApresentacoes_t874390990;

#include "codegen/il2cpp-codegen.h"

// System.Void TesteApresentacoes::.ctor()
extern "C"  void TesteApresentacoes__ctor_m2520416909 (TesteApresentacoes_t874390990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteApresentacoes::Start()
extern "C"  void TesteApresentacoes_Start_m1467554701 (TesteApresentacoes_t874390990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteApresentacoes::OnGUI()
extern "C"  void TesteApresentacoes_OnGUI_m2015815559 (TesteApresentacoes_t874390990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteApresentacoes::showBackButton()
extern "C"  void TesteApresentacoes_showBackButton_m66227661 (TesteApresentacoes_t874390990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TesteApresentacoes::showAllButtonsTracks()
extern "C"  void TesteApresentacoes_showAllButtonsTracks_m3807457308 (TesteApresentacoes_t874390990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
