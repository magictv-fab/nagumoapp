﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplashScreenView
struct SplashScreenView_t1797787416;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2376705138;

#include "codegen/il2cpp-codegen.h"

// System.Void SplashScreenView::.ctor()
extern "C"  void SplashScreenView__ctor_m3199242883 (SplashScreenView_t1797787416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScreenView::LoadView()
extern "C"  void SplashScreenView_LoadView_m839426124 (SplashScreenView_t1797787416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScreenView::UnLoadView()
extern "C"  void SplashScreenView_UnLoadView_m731056069 (SplashScreenView_t1797787416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplashScreenView::UpdateUI(System.Boolean)
extern "C"  void SplashScreenView_UpdateUI_m2427085109 (SplashScreenView_t1797787416 * __this, bool ___tf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D SplashScreenView::PickImageWithBestAspect(UnityEngine.Texture2D[])
extern "C"  Texture2D_t3884108195 * SplashScreenView_PickImageWithBestAspect_m3248680922 (SplashScreenView_t1797787416 * __this, Texture2DU5BU5D_t2376705138* ___splashImages0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
