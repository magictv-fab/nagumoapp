﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.IDictionary`2<System.String,System.Object[]>
struct IDictionary_2_t1506948197;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.FieldParser
struct  FieldParser_t761510115  : public Il2CppObject
{
public:

public:
};

struct FieldParser_t761510115_StaticFields
{
public:
	// System.Object ZXing.OneD.RSS.Expanded.Decoders.FieldParser::VARIABLE_LENGTH
	Il2CppObject * ___VARIABLE_LENGTH_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::TWO_DIGIT_DATA_LENGTH
	Il2CppObject* ___TWO_DIGIT_DATA_LENGTH_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::THREE_DIGIT_DATA_LENGTH
	Il2CppObject* ___THREE_DIGIT_DATA_LENGTH_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH
	Il2CppObject* ___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object[]> ZXing.OneD.RSS.Expanded.Decoders.FieldParser::FOUR_DIGIT_DATA_LENGTH
	Il2CppObject* ___FOUR_DIGIT_DATA_LENGTH_4;

public:
	inline static int32_t get_offset_of_VARIABLE_LENGTH_0() { return static_cast<int32_t>(offsetof(FieldParser_t761510115_StaticFields, ___VARIABLE_LENGTH_0)); }
	inline Il2CppObject * get_VARIABLE_LENGTH_0() const { return ___VARIABLE_LENGTH_0; }
	inline Il2CppObject ** get_address_of_VARIABLE_LENGTH_0() { return &___VARIABLE_LENGTH_0; }
	inline void set_VARIABLE_LENGTH_0(Il2CppObject * value)
	{
		___VARIABLE_LENGTH_0 = value;
		Il2CppCodeGenWriteBarrier(&___VARIABLE_LENGTH_0, value);
	}

	inline static int32_t get_offset_of_TWO_DIGIT_DATA_LENGTH_1() { return static_cast<int32_t>(offsetof(FieldParser_t761510115_StaticFields, ___TWO_DIGIT_DATA_LENGTH_1)); }
	inline Il2CppObject* get_TWO_DIGIT_DATA_LENGTH_1() const { return ___TWO_DIGIT_DATA_LENGTH_1; }
	inline Il2CppObject** get_address_of_TWO_DIGIT_DATA_LENGTH_1() { return &___TWO_DIGIT_DATA_LENGTH_1; }
	inline void set_TWO_DIGIT_DATA_LENGTH_1(Il2CppObject* value)
	{
		___TWO_DIGIT_DATA_LENGTH_1 = value;
		Il2CppCodeGenWriteBarrier(&___TWO_DIGIT_DATA_LENGTH_1, value);
	}

	inline static int32_t get_offset_of_THREE_DIGIT_DATA_LENGTH_2() { return static_cast<int32_t>(offsetof(FieldParser_t761510115_StaticFields, ___THREE_DIGIT_DATA_LENGTH_2)); }
	inline Il2CppObject* get_THREE_DIGIT_DATA_LENGTH_2() const { return ___THREE_DIGIT_DATA_LENGTH_2; }
	inline Il2CppObject** get_address_of_THREE_DIGIT_DATA_LENGTH_2() { return &___THREE_DIGIT_DATA_LENGTH_2; }
	inline void set_THREE_DIGIT_DATA_LENGTH_2(Il2CppObject* value)
	{
		___THREE_DIGIT_DATA_LENGTH_2 = value;
		Il2CppCodeGenWriteBarrier(&___THREE_DIGIT_DATA_LENGTH_2, value);
	}

	inline static int32_t get_offset_of_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3() { return static_cast<int32_t>(offsetof(FieldParser_t761510115_StaticFields, ___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3)); }
	inline Il2CppObject* get_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3() const { return ___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3; }
	inline Il2CppObject** get_address_of_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3() { return &___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3; }
	inline void set_THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3(Il2CppObject* value)
	{
		___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3 = value;
		Il2CppCodeGenWriteBarrier(&___THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH_3, value);
	}

	inline static int32_t get_offset_of_FOUR_DIGIT_DATA_LENGTH_4() { return static_cast<int32_t>(offsetof(FieldParser_t761510115_StaticFields, ___FOUR_DIGIT_DATA_LENGTH_4)); }
	inline Il2CppObject* get_FOUR_DIGIT_DATA_LENGTH_4() const { return ___FOUR_DIGIT_DATA_LENGTH_4; }
	inline Il2CppObject** get_address_of_FOUR_DIGIT_DATA_LENGTH_4() { return &___FOUR_DIGIT_DATA_LENGTH_4; }
	inline void set_FOUR_DIGIT_DATA_LENGTH_4(Il2CppObject* value)
	{
		___FOUR_DIGIT_DATA_LENGTH_4 = value;
		Il2CppCodeGenWriteBarrier(&___FOUR_DIGIT_DATA_LENGTH_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
