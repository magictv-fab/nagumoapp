﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUICheckButton
struct SampleAppUICheckButton_t4241727631;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"

// System.Void SampleAppUICheckButton::.ctor(System.Single,System.String,System.Boolean,UnityEngine.GUIStyle)
extern "C"  void SampleAppUICheckButton__ctor_m1788666775 (SampleAppUICheckButton_t4241727631 * __this, float ___index0, String_t* ___title1, bool ___selected2, GUIStyle_t2990928826 * ___style3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUICheckButton::add_TappedOn(System.Action`1<System.Boolean>)
extern "C"  void SampleAppUICheckButton_add_TappedOn_m3420874898 (SampleAppUICheckButton_t4241727631 * __this, Action_1_t872614854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUICheckButton::remove_TappedOn(System.Action`1<System.Boolean>)
extern "C"  void SampleAppUICheckButton_remove_TappedOn_m1101892419 (SampleAppUICheckButton_t4241727631 * __this, Action_1_t872614854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SampleAppUICheckButton::get_Title()
extern "C"  String_t* SampleAppUICheckButton_get_Title_m3700593910 (SampleAppUICheckButton_t4241727631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUICheckButton::set_Title(System.String)
extern "C"  void SampleAppUICheckButton_set_Title_m1296577565 (SampleAppUICheckButton_t4241727631 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleAppUICheckButton::get_Width()
extern "C"  float SampleAppUICheckButton_get_Width_m2534871451 (SampleAppUICheckButton_t4241727631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleAppUICheckButton::get_Height()
extern "C"  float SampleAppUICheckButton_get_Height_m1775418004 (SampleAppUICheckButton_t4241727631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUICheckButton::Enable(System.Boolean)
extern "C"  void SampleAppUICheckButton_Enable_m3637062674 (SampleAppUICheckButton_t4241727631 * __this, bool ___tf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SampleAppUICheckButton::get_IsEnabled()
extern "C"  bool SampleAppUICheckButton_get_IsEnabled_m3115732324 (SampleAppUICheckButton_t4241727631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUICheckButton::Draw()
extern "C"  void SampleAppUICheckButton_Draw_m51742492 (SampleAppUICheckButton_t4241727631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
