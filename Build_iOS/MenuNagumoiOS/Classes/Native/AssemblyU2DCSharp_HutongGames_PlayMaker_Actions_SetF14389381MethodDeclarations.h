﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmObject
struct SetFsmObject_t14389381;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::.ctor()
extern "C"  void SetFsmObject__ctor_m583852929 (SetFsmObject_t14389381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::Reset()
extern "C"  void SetFsmObject_Reset_m2525253166 (SetFsmObject_t14389381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::OnEnter()
extern "C"  void SetFsmObject_OnEnter_m2128644888 (SetFsmObject_t14389381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::DoSetFsmBool()
extern "C"  void SetFsmObject_DoSetFsmBool_m2012864342 (SetFsmObject_t14389381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::OnUpdate()
extern "C"  void SetFsmObject_OnUpdate_m697041451 (SetFsmObject_t14389381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
