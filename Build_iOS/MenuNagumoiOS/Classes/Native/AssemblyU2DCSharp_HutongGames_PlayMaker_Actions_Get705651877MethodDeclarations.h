﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive
struct GetAnimatorFeetPivotActive_t705651877;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::.ctor()
extern "C"  void GetAnimatorFeetPivotActive__ctor_m502507873 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::Reset()
extern "C"  void GetAnimatorFeetPivotActive_Reset_m2443908110 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::OnEnter()
extern "C"  void GetAnimatorFeetPivotActive_OnEnter_m1265457400 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::DoGetFeetPivotActive()
extern "C"  void GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
