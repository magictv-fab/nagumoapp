﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.EyewearImpl
struct EyewearImpl_t3622106266;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_E1319805025.h"

// System.Boolean Vuforia.EyewearImpl::IsDeviceDetected()
extern "C"  bool EyewearImpl_IsDeviceDetected_m660937999 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::setHeadsetPresent(System.String)
extern "C"  bool EyewearImpl_setHeadsetPresent_m1640423954 (EyewearImpl_t3622106266 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::setHeadsetNotPresent()
extern "C"  bool EyewearImpl_setHeadsetNotPresent_m1685778677 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::IsSeeThru()
extern "C"  bool EyewearImpl_IsSeeThru_m2618800853 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.EyewearImpl::GetScreenOrientation()
extern "C"  int32_t EyewearImpl_GetScreenOrientation_m3928890945 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::IsStereoCapable()
extern "C"  bool EyewearImpl_IsStereoCapable_m1131853631 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::IsStereoEnabled()
extern "C"  bool EyewearImpl_IsStereoEnabled_m2603611412 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::IsStereoGLOnly()
extern "C"  bool EyewearImpl_IsStereoGLOnly_m184082304 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::SetStereo(System.Boolean)
extern "C"  bool EyewearImpl_SetStereo_m499444838 (EyewearImpl_t3622106266 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::IsPredictiveTrackingEnabled()
extern "C"  bool EyewearImpl_IsPredictiveTrackingEnabled_m3452888726 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.EyewearImpl::SetPredictiveTracking(System.Boolean)
extern "C"  bool EyewearImpl_SetPredictiveTracking_m2870408356 (EyewearImpl_t3622106266 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearImpl::setProjectionClippingPlanes(System.Single,System.Single)
extern "C"  void EyewearImpl_setProjectionClippingPlanes_m1248339869 (EyewearImpl_t3622106266 * __this, float ___nearPlane0, float ___farPlane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.EyewearImpl::GetProjectionMatrix(Vuforia.Eyewear/EyeID,UnityEngine.ScreenOrientation)
extern "C"  Matrix4x4_t1651859333  EyewearImpl_GetProjectionMatrix_m4063620422 (EyewearImpl_t3622106266 * __this, int32_t ___eyeID0, int32_t ___screenOrientation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.EyewearImpl::GetProjectionMatrix(Vuforia.Eyewear/EyeID,System.Int32,UnityEngine.ScreenOrientation)
extern "C"  Matrix4x4_t1651859333  EyewearImpl_GetProjectionMatrix_m2774927087 (EyewearImpl_t3622106266 * __this, int32_t ___eyeID0, int32_t ___profileID1, int32_t ___screenOrientation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearImpl::.ctor()
extern "C"  void EyewearImpl__ctor_m238303595 (EyewearImpl_t3622106266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
