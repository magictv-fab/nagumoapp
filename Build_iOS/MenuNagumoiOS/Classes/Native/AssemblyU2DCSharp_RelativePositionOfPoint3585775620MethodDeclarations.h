﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RelativePositionOfPoint
struct RelativePositionOfPoint_t3585775620;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void RelativePositionOfPoint::.ctor()
extern "C"  void RelativePositionOfPoint__ctor_m501517287 (RelativePositionOfPoint_t3585775620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RelativePositionOfPoint RelativePositionOfPoint::GetInstance()
extern "C"  RelativePositionOfPoint_t3585775620 * RelativePositionOfPoint_GetInstance_m984803743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RelativePositionOfPoint::Update()
extern "C"  void RelativePositionOfPoint_Update_m94028806 (RelativePositionOfPoint_t3585775620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RelativePositionOfPoint::Start()
extern "C"  void RelativePositionOfPoint_Start_m3743622375 (RelativePositionOfPoint_t3585775620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RelativePositionOfPoint::GetRelativePoint(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  RelativePositionOfPoint_GetRelativePoint_m2725843309 (RelativePositionOfPoint_t3585775620 * __this, Vector3_t4282066566  ___myPoint0, float ___percentDistance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RelativePositionOfPoint::setDistanceOfReferencePoint(System.Single)
extern "C"  void RelativePositionOfPoint_setDistanceOfReferencePoint_m1148427251 (RelativePositionOfPoint_t3585775620 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RelativePositionOfPoint::GetRelativePointOfPivot(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  RelativePositionOfPoint_GetRelativePointOfPivot_m3494025269 (RelativePositionOfPoint_t3585775620 * __this, Vector3_t4282066566  ___pivotPoint0, Vector3_t4282066566  ___myPoint1, float ___percentDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RelativePositionOfPoint::getUniqueName()
extern "C"  String_t* RelativePositionOfPoint_getUniqueName_m3307453882 (RelativePositionOfPoint_t3585775620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RelativePositionOfPoint::configByString(System.String)
extern "C"  void RelativePositionOfPoint_configByString_m367246043 (RelativePositionOfPoint_t3585775620 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RelativePositionOfPoint::Reset()
extern "C"  void RelativePositionOfPoint_Reset_m2442917524 (RelativePositionOfPoint_t3585775620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject RelativePositionOfPoint::getGameObjectReference()
extern "C"  GameObject_t3674682005 * RelativePositionOfPoint_getGameObjectReference_m973565836 (RelativePositionOfPoint_t3585775620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
