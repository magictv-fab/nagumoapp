﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ServerControl/OnPlayDelegate
struct OnPlayDelegate_t3097072301;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_PointsAngle2793040272.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ServerControl/OnPlayDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPlayDelegate__ctor_m2282295508 (OnPlayDelegate_t3097072301 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/OnPlayDelegate::Invoke(PointsAngle)
extern "C"  void OnPlayDelegate_Invoke_m2976982388 (OnPlayDelegate_t3097072301 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ServerControl/OnPlayDelegate::BeginInvoke(PointsAngle,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPlayDelegate_BeginInvoke_m1840144121 (OnPlayDelegate_t3097072301 * __this, int32_t ___status0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/OnPlayDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnPlayDelegate_EndInvoke_m395185380 (OnPlayDelegate_t3097072301 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
