﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t153068654;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct Deflater_t2454063301;
// System.IO.Stream
struct Stream_t1561764144;
// System.Security.Cryptography.RNGCryptoServiceProvider
struct RNGCryptoServiceProvider_t3764994374;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream
struct  DeflaterOutputStream_t537202536  : public Stream_t1561764144
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::password
	String_t* ___password_2;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::cryptoTransform_
	Il2CppObject * ___cryptoTransform__3;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::AESAuthCode
	ByteU5BU5D_t4260760469* ___AESAuthCode_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::buffer_
	ByteU5BU5D_t4260760469* ___buffer__5;
	// ICSharpCode.SharpZipLib.Zip.Compression.Deflater ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::deflater_
	Deflater_t2454063301 * ___deflater__6;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::baseOutputStream_
	Stream_t1561764144 * ___baseOutputStream__7;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::isClosed_
	bool ___isClosed__8;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::isStreamOwner_
	bool ___isStreamOwner__9;

public:
	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___password_2)); }
	inline String_t* get_password_2() const { return ___password_2; }
	inline String_t** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(String_t* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier(&___password_2, value);
	}

	inline static int32_t get_offset_of_cryptoTransform__3() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___cryptoTransform__3)); }
	inline Il2CppObject * get_cryptoTransform__3() const { return ___cryptoTransform__3; }
	inline Il2CppObject ** get_address_of_cryptoTransform__3() { return &___cryptoTransform__3; }
	inline void set_cryptoTransform__3(Il2CppObject * value)
	{
		___cryptoTransform__3 = value;
		Il2CppCodeGenWriteBarrier(&___cryptoTransform__3, value);
	}

	inline static int32_t get_offset_of_AESAuthCode_4() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___AESAuthCode_4)); }
	inline ByteU5BU5D_t4260760469* get_AESAuthCode_4() const { return ___AESAuthCode_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_AESAuthCode_4() { return &___AESAuthCode_4; }
	inline void set_AESAuthCode_4(ByteU5BU5D_t4260760469* value)
	{
		___AESAuthCode_4 = value;
		Il2CppCodeGenWriteBarrier(&___AESAuthCode_4, value);
	}

	inline static int32_t get_offset_of_buffer__5() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___buffer__5)); }
	inline ByteU5BU5D_t4260760469* get_buffer__5() const { return ___buffer__5; }
	inline ByteU5BU5D_t4260760469** get_address_of_buffer__5() { return &___buffer__5; }
	inline void set_buffer__5(ByteU5BU5D_t4260760469* value)
	{
		___buffer__5 = value;
		Il2CppCodeGenWriteBarrier(&___buffer__5, value);
	}

	inline static int32_t get_offset_of_deflater__6() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___deflater__6)); }
	inline Deflater_t2454063301 * get_deflater__6() const { return ___deflater__6; }
	inline Deflater_t2454063301 ** get_address_of_deflater__6() { return &___deflater__6; }
	inline void set_deflater__6(Deflater_t2454063301 * value)
	{
		___deflater__6 = value;
		Il2CppCodeGenWriteBarrier(&___deflater__6, value);
	}

	inline static int32_t get_offset_of_baseOutputStream__7() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___baseOutputStream__7)); }
	inline Stream_t1561764144 * get_baseOutputStream__7() const { return ___baseOutputStream__7; }
	inline Stream_t1561764144 ** get_address_of_baseOutputStream__7() { return &___baseOutputStream__7; }
	inline void set_baseOutputStream__7(Stream_t1561764144 * value)
	{
		___baseOutputStream__7 = value;
		Il2CppCodeGenWriteBarrier(&___baseOutputStream__7, value);
	}

	inline static int32_t get_offset_of_isClosed__8() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___isClosed__8)); }
	inline bool get_isClosed__8() const { return ___isClosed__8; }
	inline bool* get_address_of_isClosed__8() { return &___isClosed__8; }
	inline void set_isClosed__8(bool value)
	{
		___isClosed__8 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner__9() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___isStreamOwner__9)); }
	inline bool get_isStreamOwner__9() const { return ___isStreamOwner__9; }
	inline bool* get_address_of_isStreamOwner__9() { return &___isStreamOwner__9; }
	inline void set_isStreamOwner__9(bool value)
	{
		___isStreamOwner__9 = value;
	}
};

struct DeflaterOutputStream_t537202536_StaticFields
{
public:
	// System.Security.Cryptography.RNGCryptoServiceProvider ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::_aesRnd
	RNGCryptoServiceProvider_t3764994374 * ____aesRnd_10;

public:
	inline static int32_t get_offset_of__aesRnd_10() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536_StaticFields, ____aesRnd_10)); }
	inline RNGCryptoServiceProvider_t3764994374 * get__aesRnd_10() const { return ____aesRnd_10; }
	inline RNGCryptoServiceProvider_t3764994374 ** get_address_of__aesRnd_10() { return &____aesRnd_10; }
	inline void set__aesRnd_10(RNGCryptoServiceProvider_t3764994374 * value)
	{
		____aesRnd_10 = value;
		Il2CppCodeGenWriteBarrier(&____aesRnd_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
