﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutToggle
struct  GUILayoutToggle_t3170064647  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutToggle::storeButtonState
	FsmBool_t1075959796 * ___storeButtonState_11;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutToggle::image
	FsmTexture_t3073272573 * ___image_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToggle::text
	FsmString_t952858651 * ___text_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToggle::tooltip
	FsmString_t952858651 * ___tooltip_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToggle::style
	FsmString_t952858651 * ___style_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutToggle::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_16;

public:
	inline static int32_t get_offset_of_storeButtonState_11() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t3170064647, ___storeButtonState_11)); }
	inline FsmBool_t1075959796 * get_storeButtonState_11() const { return ___storeButtonState_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeButtonState_11() { return &___storeButtonState_11; }
	inline void set_storeButtonState_11(FsmBool_t1075959796 * value)
	{
		___storeButtonState_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeButtonState_11, value);
	}

	inline static int32_t get_offset_of_image_12() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t3170064647, ___image_12)); }
	inline FsmTexture_t3073272573 * get_image_12() const { return ___image_12; }
	inline FsmTexture_t3073272573 ** get_address_of_image_12() { return &___image_12; }
	inline void set_image_12(FsmTexture_t3073272573 * value)
	{
		___image_12 = value;
		Il2CppCodeGenWriteBarrier(&___image_12, value);
	}

	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t3170064647, ___text_13)); }
	inline FsmString_t952858651 * get_text_13() const { return ___text_13; }
	inline FsmString_t952858651 ** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(FsmString_t952858651 * value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier(&___text_13, value);
	}

	inline static int32_t get_offset_of_tooltip_14() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t3170064647, ___tooltip_14)); }
	inline FsmString_t952858651 * get_tooltip_14() const { return ___tooltip_14; }
	inline FsmString_t952858651 ** get_address_of_tooltip_14() { return &___tooltip_14; }
	inline void set_tooltip_14(FsmString_t952858651 * value)
	{
		___tooltip_14 = value;
		Il2CppCodeGenWriteBarrier(&___tooltip_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t3170064647, ___style_15)); }
	inline FsmString_t952858651 * get_style_15() const { return ___style_15; }
	inline FsmString_t952858651 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(FsmString_t952858651 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_changedEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t3170064647, ___changedEvent_16)); }
	inline FsmEvent_t2133468028 * get_changedEvent_16() const { return ___changedEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_16() { return &___changedEvent_16; }
	inline void set_changedEvent_16(FsmEvent_t2133468028 * value)
	{
		___changedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
