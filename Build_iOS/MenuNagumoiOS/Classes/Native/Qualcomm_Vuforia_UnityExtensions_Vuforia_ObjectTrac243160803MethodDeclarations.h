﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ObjectTrackerImpl
struct ObjectTrackerImpl_t243160803;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t4096057777;
// Vuforia.TargetFinder
struct TargetFinder_t1331156121;
// Vuforia.DataSet
struct DataSet_t2095838082;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>
struct IEnumerable_1_t1101783743;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet2095838082.h"

// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::get_ImageTargetBuilder()
extern "C"  ImageTargetBuilder_t4096057777 * ObjectTrackerImpl_get_ImageTargetBuilder_m692110144 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::get_TargetFinder()
extern "C"  TargetFinder_t1331156121 * ObjectTrackerImpl_get_TargetFinder_m2089854800 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::.ctor()
extern "C"  void ObjectTrackerImpl__ctor_m3044980802 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::Start()
extern "C"  bool ObjectTrackerImpl_Start_m1840265326 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::Stop()
extern "C"  void ObjectTrackerImpl_Stop_m2419979332 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSet Vuforia.ObjectTrackerImpl::CreateDataSet()
extern "C"  DataSet_t2095838082 * ObjectTrackerImpl_CreateDataSet_m447789443 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::DestroyDataSet(Vuforia.DataSet,System.Boolean)
extern "C"  bool ObjectTrackerImpl_DestroyDataSet_m947596939 (ObjectTrackerImpl_t243160803 * __this, DataSet_t2095838082 * ___dataSet0, bool ___destroyTrackables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::ActivateDataSet(Vuforia.DataSet)
extern "C"  bool ObjectTrackerImpl_ActivateDataSet_m3146086645 (ObjectTrackerImpl_t243160803 * __this, DataSet_t2095838082 * ___dataSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::DeactivateDataSet(Vuforia.DataSet)
extern "C"  bool ObjectTrackerImpl_DeactivateDataSet_m1075175478 (ObjectTrackerImpl_t243160803 * __this, DataSet_t2095838082 * ___dataSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetActiveDataSets()
extern "C"  Il2CppObject* ObjectTrackerImpl_GetActiveDataSets_m1229244943 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetDataSets()
extern "C"  Il2CppObject* ObjectTrackerImpl_GetDataSets_m2159152169 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::DestroyAllDataSets(System.Boolean)
extern "C"  void ObjectTrackerImpl_DestroyAllDataSets_m84535419 (ObjectTrackerImpl_t243160803 * __this, bool ___destroyTrackables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::PersistExtendedTracking(System.Boolean)
extern "C"  bool ObjectTrackerImpl_PersistExtendedTracking_m2793958727 (ObjectTrackerImpl_t243160803 * __this, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::ResetExtendedTracking()
extern "C"  bool ObjectTrackerImpl_ResetExtendedTracking_m3435230539 (ObjectTrackerImpl_t243160803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
