﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"

// HutongGames.PlayMaker.OwnerDefaultOption HutongGames.PlayMaker.FsmOwnerDefault::get_OwnerOption()
extern "C"  int32_t FsmOwnerDefault_get_OwnerOption_m3357910390 (FsmOwnerDefault_t251897112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::set_OwnerOption(HutongGames.PlayMaker.OwnerDefaultOption)
extern "C"  void FsmOwnerDefault_set_OwnerOption_m185008897 (FsmOwnerDefault_t251897112 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmOwnerDefault::get_GameObject()
extern "C"  FsmGameObject_t1697147867 * FsmOwnerDefault_get_GameObject_m3249227945 (FsmOwnerDefault_t251897112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::set_GameObject(HutongGames.PlayMaker.FsmGameObject)
extern "C"  void FsmOwnerDefault_set_GameObject_m3964195234 (FsmOwnerDefault_t251897112 * __this, FsmGameObject_t1697147867 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::.ctor()
extern "C"  void FsmOwnerDefault__ctor_m3170160071 (FsmOwnerDefault_t251897112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::.ctor(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  void FsmOwnerDefault__ctor_m2596779561 (FsmOwnerDefault_t251897112 * __this, FsmOwnerDefault_t251897112 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
