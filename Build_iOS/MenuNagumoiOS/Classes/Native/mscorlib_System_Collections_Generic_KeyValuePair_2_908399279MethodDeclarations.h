﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21188478419MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2122652373(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t908399279 *, int32_t, Func_1_t3890737231 *, const MethodInfo*))KeyValuePair_2__ctor_m3781279952_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Key()
#define KeyValuePair_2_get_Key_m893949171(__this, method) ((  int32_t (*) (KeyValuePair_2_t908399279 *, const MethodInfo*))KeyValuePair_2_get_Key_m994691224_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4133490228(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t908399279 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4226570073_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Value()
#define KeyValuePair_2_get_Value_m3414159319(__this, method) ((  Func_1_t3890737231 * (*) (KeyValuePair_2_t908399279 *, const MethodInfo*))KeyValuePair_2_get_Value_m3611506108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2195793972(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t908399279 *, Func_1_t3890737231 *, const MethodInfo*))KeyValuePair_2_set_Value_m3911866073_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::ToString()
#define KeyValuePair_2_ToString_m2815515092(__this, method) ((  String_t* (*) (KeyValuePair_2_t908399279 *, const MethodInfo*))KeyValuePair_2_ToString_m2022142991_gshared)(__this, method)
