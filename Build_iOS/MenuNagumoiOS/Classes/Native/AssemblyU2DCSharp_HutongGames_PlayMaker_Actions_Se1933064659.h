﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRectValue
struct  SetRectValue_t1933064659  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetRectValue::rectVariable
	FsmRect_t1076426478 * ___rectVariable_9;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetRectValue::rectValue
	FsmRect_t1076426478 * ___rectValue_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRectValue::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_rectVariable_9() { return static_cast<int32_t>(offsetof(SetRectValue_t1933064659, ___rectVariable_9)); }
	inline FsmRect_t1076426478 * get_rectVariable_9() const { return ___rectVariable_9; }
	inline FsmRect_t1076426478 ** get_address_of_rectVariable_9() { return &___rectVariable_9; }
	inline void set_rectVariable_9(FsmRect_t1076426478 * value)
	{
		___rectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariable_9, value);
	}

	inline static int32_t get_offset_of_rectValue_10() { return static_cast<int32_t>(offsetof(SetRectValue_t1933064659, ___rectValue_10)); }
	inline FsmRect_t1076426478 * get_rectValue_10() const { return ___rectValue_10; }
	inline FsmRect_t1076426478 ** get_address_of_rectValue_10() { return &___rectValue_10; }
	inline void set_rectValue_10(FsmRect_t1076426478 * value)
	{
		___rectValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___rectValue_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetRectValue_t1933064659, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
