﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.AREvents
struct AREvents_t1242196082;
// MagicTV.globals.AREvents/OnARToggleOnOff
struct OnARToggleOnOff_t2139692132;
// System.String
struct String_t;
// MagicTV.globals.AREvents/OnARWantToDoSomething
struct OnARWantToDoSomething_t4073481420;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AREvents_OnARTog2139692132.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AREvents_OnARWan4073481420.h"

// System.Void MagicTV.globals.AREvents::.ctor()
extern "C"  void AREvents__ctor_m1179727764 (AREvents_t1242196082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::AddChangeARToggleHandler(MagicTV.globals.AREvents/OnARToggleOnOff)
extern "C"  void AREvents_AddChangeARToggleHandler_m641381024 (AREvents_t1242196082 * __this, OnARToggleOnOff_t2139692132 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RemoveChangeARToggleHandler(MagicTV.globals.AREvents/OnARToggleOnOff)
extern "C"  void AREvents_RemoveChangeARToggleHandler_m2989745311 (AREvents_t1242196082 * __this, OnARToggleOnOff_t2139692132 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RaiseARToggle(System.Boolean)
extern "C"  void AREvents_RaiseARToggle_m135688826 (AREvents_t1242196082 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RaiseTrackIn(System.String)
extern "C"  void AREvents_RaiseTrackIn_m3375777294 (AREvents_t1242196082 * __this, String_t* ___trackName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RaiseTrackOut(System.String)
extern "C"  void AREvents_RaiseTrackOut_m3626628769 (AREvents_t1242196082 * __this, String_t* ___trackName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::AddOnWantToTurnOnEventHandler(MagicTV.globals.AREvents/OnARWantToDoSomething)
extern "C"  void AREvents_AddOnWantToTurnOnEventHandler_m3721794525 (AREvents_t1242196082 * __this, OnARWantToDoSomething_t4073481420 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RemoveOnWantToTurnOnEventHandler(MagicTV.globals.AREvents/OnARWantToDoSomething)
extern "C"  void AREvents_RemoveOnWantToTurnOnEventHandler_m4141350014 (AREvents_t1242196082 * __this, OnARWantToDoSomething_t4073481420 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RaiseWantToTurnOn()
extern "C"  void AREvents_RaiseWantToTurnOn_m680764485 (AREvents_t1242196082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::AddOnWantToTurnOffEventHandler(MagicTV.globals.AREvents/OnARWantToDoSomething)
extern "C"  void AREvents_AddOnWantToTurnOffEventHandler_m3221346267 (AREvents_t1242196082 * __this, OnARWantToDoSomething_t4073481420 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RemoveOnWantToTurnOffEventHandler(MagicTV.globals.AREvents/OnARWantToDoSomething)
extern "C"  void AREvents_RemoveOnWantToTurnOffEventHandler_m3342664538 (AREvents_t1242196082 * __this, OnARWantToDoSomething_t4073481420 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AREvents::RaiseWantToTurnOff()
extern "C"  void AREvents_RaiseWantToTurnOff_m3923651115 (AREvents_t1242196082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.AREvents MagicTV.globals.AREvents::GetInstance()
extern "C"  AREvents_t1242196082 * AREvents_GetInstance_m1304811975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
