﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AcougueData
struct AcougueData_t537282265;
// PedidoAcougue
struct PedidoAcougue_t9638794;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<CortesData[]>
struct List_1_t3063524915;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t957326451;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.Dropdown
struct Dropdown_t4201779933;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// PedidoData
struct PedidoData_t3443010735;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoAcougue
struct  PedidoAcougue_t9638794  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject PedidoAcougue::loadingObj
	GameObject_t3674682005 * ___loadingObj_11;
	// UnityEngine.GameObject PedidoAcougue::popup
	GameObject_t3674682005 * ___popup_12;
	// UnityEngine.GameObject PedidoAcougue::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_13;
	// UnityEngine.GameObject PedidoAcougue::containerItens
	GameObject_t3674682005 * ___containerItens_14;
	// UnityEngine.UI.Dropdown PedidoAcougue::dropdownTipoDeCorte
	Dropdown_t4201779933 * ___dropdownTipoDeCorte_16;
	// UnityEngine.UI.Dropdown PedidoAcougue::dropdownPeso
	Dropdown_t4201779933 * ___dropdownPeso_17;
	// UnityEngine.UI.Text PedidoAcougue::positionText
	Text_t9039225 * ___positionText_18;
	// UnityEngine.UI.InputField PedidoAcougue::quantidade
	InputField_t609046876 * ___quantidade_19;
	// UnityEngine.UI.InputField PedidoAcougue::observacoes
	InputField_t609046876 * ___observacoes_20;
	// UnityEngine.UI.Text PedidoAcougue::textPeso
	Text_t9039225 * ___textPeso_21;
	// PedidoData PedidoAcougue::pedidoData
	PedidoData_t3443010735 * ___pedidoData_22;
	// UnityEngine.UI.ScrollRect PedidoAcougue::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_23;
	// System.Int32 PedidoAcougue::carnesCount
	int32_t ___carnesCount_24;
	// UnityEngine.GameObject PedidoAcougue::selectedItem
	GameObject_t3674682005 * ___selectedItem_26;
	// UnityEngine.UI.Text PedidoAcougue::quantidadeOfertasText
	Text_t9039225 * ___quantidadeOfertasText_27;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap PedidoAcougue::horizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnap_28;
	// UnityEngine.UI.Text PedidoAcougue::carneTitle
	Text_t9039225 * ___carneTitle_29;
	// System.Int32 PedidoAcougue::categoria_carne
	int32_t ___categoria_carne_30;

public:
	inline static int32_t get_offset_of_loadingObj_11() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___loadingObj_11)); }
	inline GameObject_t3674682005 * get_loadingObj_11() const { return ___loadingObj_11; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_11() { return &___loadingObj_11; }
	inline void set_loadingObj_11(GameObject_t3674682005 * value)
	{
		___loadingObj_11 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_11, value);
	}

	inline static int32_t get_offset_of_popup_12() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___popup_12)); }
	inline GameObject_t3674682005 * get_popup_12() const { return ___popup_12; }
	inline GameObject_t3674682005 ** get_address_of_popup_12() { return &___popup_12; }
	inline void set_popup_12(GameObject_t3674682005 * value)
	{
		___popup_12 = value;
		Il2CppCodeGenWriteBarrier(&___popup_12, value);
	}

	inline static int32_t get_offset_of_itemPrefab_13() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___itemPrefab_13)); }
	inline GameObject_t3674682005 * get_itemPrefab_13() const { return ___itemPrefab_13; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_13() { return &___itemPrefab_13; }
	inline void set_itemPrefab_13(GameObject_t3674682005 * value)
	{
		___itemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_13, value);
	}

	inline static int32_t get_offset_of_containerItens_14() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___containerItens_14)); }
	inline GameObject_t3674682005 * get_containerItens_14() const { return ___containerItens_14; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_14() { return &___containerItens_14; }
	inline void set_containerItens_14(GameObject_t3674682005 * value)
	{
		___containerItens_14 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_14, value);
	}

	inline static int32_t get_offset_of_dropdownTipoDeCorte_16() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___dropdownTipoDeCorte_16)); }
	inline Dropdown_t4201779933 * get_dropdownTipoDeCorte_16() const { return ___dropdownTipoDeCorte_16; }
	inline Dropdown_t4201779933 ** get_address_of_dropdownTipoDeCorte_16() { return &___dropdownTipoDeCorte_16; }
	inline void set_dropdownTipoDeCorte_16(Dropdown_t4201779933 * value)
	{
		___dropdownTipoDeCorte_16 = value;
		Il2CppCodeGenWriteBarrier(&___dropdownTipoDeCorte_16, value);
	}

	inline static int32_t get_offset_of_dropdownPeso_17() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___dropdownPeso_17)); }
	inline Dropdown_t4201779933 * get_dropdownPeso_17() const { return ___dropdownPeso_17; }
	inline Dropdown_t4201779933 ** get_address_of_dropdownPeso_17() { return &___dropdownPeso_17; }
	inline void set_dropdownPeso_17(Dropdown_t4201779933 * value)
	{
		___dropdownPeso_17 = value;
		Il2CppCodeGenWriteBarrier(&___dropdownPeso_17, value);
	}

	inline static int32_t get_offset_of_positionText_18() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___positionText_18)); }
	inline Text_t9039225 * get_positionText_18() const { return ___positionText_18; }
	inline Text_t9039225 ** get_address_of_positionText_18() { return &___positionText_18; }
	inline void set_positionText_18(Text_t9039225 * value)
	{
		___positionText_18 = value;
		Il2CppCodeGenWriteBarrier(&___positionText_18, value);
	}

	inline static int32_t get_offset_of_quantidade_19() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___quantidade_19)); }
	inline InputField_t609046876 * get_quantidade_19() const { return ___quantidade_19; }
	inline InputField_t609046876 ** get_address_of_quantidade_19() { return &___quantidade_19; }
	inline void set_quantidade_19(InputField_t609046876 * value)
	{
		___quantidade_19 = value;
		Il2CppCodeGenWriteBarrier(&___quantidade_19, value);
	}

	inline static int32_t get_offset_of_observacoes_20() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___observacoes_20)); }
	inline InputField_t609046876 * get_observacoes_20() const { return ___observacoes_20; }
	inline InputField_t609046876 ** get_address_of_observacoes_20() { return &___observacoes_20; }
	inline void set_observacoes_20(InputField_t609046876 * value)
	{
		___observacoes_20 = value;
		Il2CppCodeGenWriteBarrier(&___observacoes_20, value);
	}

	inline static int32_t get_offset_of_textPeso_21() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___textPeso_21)); }
	inline Text_t9039225 * get_textPeso_21() const { return ___textPeso_21; }
	inline Text_t9039225 ** get_address_of_textPeso_21() { return &___textPeso_21; }
	inline void set_textPeso_21(Text_t9039225 * value)
	{
		___textPeso_21 = value;
		Il2CppCodeGenWriteBarrier(&___textPeso_21, value);
	}

	inline static int32_t get_offset_of_pedidoData_22() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___pedidoData_22)); }
	inline PedidoData_t3443010735 * get_pedidoData_22() const { return ___pedidoData_22; }
	inline PedidoData_t3443010735 ** get_address_of_pedidoData_22() { return &___pedidoData_22; }
	inline void set_pedidoData_22(PedidoData_t3443010735 * value)
	{
		___pedidoData_22 = value;
		Il2CppCodeGenWriteBarrier(&___pedidoData_22, value);
	}

	inline static int32_t get_offset_of_scrollRect_23() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___scrollRect_23)); }
	inline ScrollRect_t3606982749 * get_scrollRect_23() const { return ___scrollRect_23; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_23() { return &___scrollRect_23; }
	inline void set_scrollRect_23(ScrollRect_t3606982749 * value)
	{
		___scrollRect_23 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_23, value);
	}

	inline static int32_t get_offset_of_carnesCount_24() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___carnesCount_24)); }
	inline int32_t get_carnesCount_24() const { return ___carnesCount_24; }
	inline int32_t* get_address_of_carnesCount_24() { return &___carnesCount_24; }
	inline void set_carnesCount_24(int32_t value)
	{
		___carnesCount_24 = value;
	}

	inline static int32_t get_offset_of_selectedItem_26() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___selectedItem_26)); }
	inline GameObject_t3674682005 * get_selectedItem_26() const { return ___selectedItem_26; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_26() { return &___selectedItem_26; }
	inline void set_selectedItem_26(GameObject_t3674682005 * value)
	{
		___selectedItem_26 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_26, value);
	}

	inline static int32_t get_offset_of_quantidadeOfertasText_27() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___quantidadeOfertasText_27)); }
	inline Text_t9039225 * get_quantidadeOfertasText_27() const { return ___quantidadeOfertasText_27; }
	inline Text_t9039225 ** get_address_of_quantidadeOfertasText_27() { return &___quantidadeOfertasText_27; }
	inline void set_quantidadeOfertasText_27(Text_t9039225 * value)
	{
		___quantidadeOfertasText_27 = value;
		Il2CppCodeGenWriteBarrier(&___quantidadeOfertasText_27, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnap_28() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___horizontalScrollSnap_28)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnap_28() const { return ___horizontalScrollSnap_28; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnap_28() { return &___horizontalScrollSnap_28; }
	inline void set_horizontalScrollSnap_28(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnap_28 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnap_28, value);
	}

	inline static int32_t get_offset_of_carneTitle_29() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___carneTitle_29)); }
	inline Text_t9039225 * get_carneTitle_29() const { return ___carneTitle_29; }
	inline Text_t9039225 ** get_address_of_carneTitle_29() { return &___carneTitle_29; }
	inline void set_carneTitle_29(Text_t9039225 * value)
	{
		___carneTitle_29 = value;
		Il2CppCodeGenWriteBarrier(&___carneTitle_29, value);
	}

	inline static int32_t get_offset_of_categoria_carne_30() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794, ___categoria_carne_30)); }
	inline int32_t get_categoria_carne_30() const { return ___categoria_carne_30; }
	inline int32_t* get_address_of_categoria_carne_30() { return &___categoria_carne_30; }
	inline void set_categoria_carne_30(int32_t value)
	{
		___categoria_carne_30 = value;
	}
};

struct PedidoAcougue_t9638794_StaticFields
{
public:
	// AcougueData PedidoAcougue::acougueData
	AcougueData_t537282265 * ___acougueData_2;
	// PedidoAcougue PedidoAcougue::instance
	PedidoAcougue_t9638794 * ___instance_3;
	// System.Int32 PedidoAcougue::posicao
	int32_t ___posicao_4;
	// System.Collections.Generic.List`1<System.String> PedidoAcougue::titleLst
	List_1_t1375417109 * ___titleLst_5;
	// System.Collections.Generic.List`1<System.Int32> PedidoAcougue::idLst
	List_1_t2522024052 * ___idLst_6;
	// System.Collections.Generic.List`1<CortesData[]> PedidoAcougue::cortesLst
	List_1_t3063524915 * ___cortesLst_7;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> PedidoAcougue::textureLst
	List_1_t957326451 * ___textureLst_8;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> PedidoAcougue::imgLst
	List_1_t272385497 * ___imgLst_9;
	// System.Collections.Generic.List`1<System.Boolean> PedidoAcougue::favoritouLst
	List_1_t1844984270 * ___favoritouLst_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PedidoAcougue::itensList
	List_1_t747900261 * ___itensList_15;
	// UnityEngine.GameObject PedidoAcougue::currentItem
	GameObject_t3674682005 * ___currentItem_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PedidoAcougue::corteIdDict
	Dictionary_2_t1974256870 * ___corteIdDict_31;
	// System.String PedidoAcougue::currentJsonTxt
	String_t* ___currentJsonTxt_32;

public:
	inline static int32_t get_offset_of_acougueData_2() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___acougueData_2)); }
	inline AcougueData_t537282265 * get_acougueData_2() const { return ___acougueData_2; }
	inline AcougueData_t537282265 ** get_address_of_acougueData_2() { return &___acougueData_2; }
	inline void set_acougueData_2(AcougueData_t537282265 * value)
	{
		___acougueData_2 = value;
		Il2CppCodeGenWriteBarrier(&___acougueData_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___instance_3)); }
	inline PedidoAcougue_t9638794 * get_instance_3() const { return ___instance_3; }
	inline PedidoAcougue_t9638794 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(PedidoAcougue_t9638794 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of_posicao_4() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___posicao_4)); }
	inline int32_t get_posicao_4() const { return ___posicao_4; }
	inline int32_t* get_address_of_posicao_4() { return &___posicao_4; }
	inline void set_posicao_4(int32_t value)
	{
		___posicao_4 = value;
	}

	inline static int32_t get_offset_of_titleLst_5() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___titleLst_5)); }
	inline List_1_t1375417109 * get_titleLst_5() const { return ___titleLst_5; }
	inline List_1_t1375417109 ** get_address_of_titleLst_5() { return &___titleLst_5; }
	inline void set_titleLst_5(List_1_t1375417109 * value)
	{
		___titleLst_5 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_5, value);
	}

	inline static int32_t get_offset_of_idLst_6() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___idLst_6)); }
	inline List_1_t2522024052 * get_idLst_6() const { return ___idLst_6; }
	inline List_1_t2522024052 ** get_address_of_idLst_6() { return &___idLst_6; }
	inline void set_idLst_6(List_1_t2522024052 * value)
	{
		___idLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_6, value);
	}

	inline static int32_t get_offset_of_cortesLst_7() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___cortesLst_7)); }
	inline List_1_t3063524915 * get_cortesLst_7() const { return ___cortesLst_7; }
	inline List_1_t3063524915 ** get_address_of_cortesLst_7() { return &___cortesLst_7; }
	inline void set_cortesLst_7(List_1_t3063524915 * value)
	{
		___cortesLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___cortesLst_7, value);
	}

	inline static int32_t get_offset_of_textureLst_8() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___textureLst_8)); }
	inline List_1_t957326451 * get_textureLst_8() const { return ___textureLst_8; }
	inline List_1_t957326451 ** get_address_of_textureLst_8() { return &___textureLst_8; }
	inline void set_textureLst_8(List_1_t957326451 * value)
	{
		___textureLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___textureLst_8, value);
	}

	inline static int32_t get_offset_of_imgLst_9() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___imgLst_9)); }
	inline List_1_t272385497 * get_imgLst_9() const { return ___imgLst_9; }
	inline List_1_t272385497 ** get_address_of_imgLst_9() { return &___imgLst_9; }
	inline void set_imgLst_9(List_1_t272385497 * value)
	{
		___imgLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_9, value);
	}

	inline static int32_t get_offset_of_favoritouLst_10() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___favoritouLst_10)); }
	inline List_1_t1844984270 * get_favoritouLst_10() const { return ___favoritouLst_10; }
	inline List_1_t1844984270 ** get_address_of_favoritouLst_10() { return &___favoritouLst_10; }
	inline void set_favoritouLst_10(List_1_t1844984270 * value)
	{
		___favoritouLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouLst_10, value);
	}

	inline static int32_t get_offset_of_itensList_15() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___itensList_15)); }
	inline List_1_t747900261 * get_itensList_15() const { return ___itensList_15; }
	inline List_1_t747900261 ** get_address_of_itensList_15() { return &___itensList_15; }
	inline void set_itensList_15(List_1_t747900261 * value)
	{
		___itensList_15 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_15, value);
	}

	inline static int32_t get_offset_of_currentItem_25() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___currentItem_25)); }
	inline GameObject_t3674682005 * get_currentItem_25() const { return ___currentItem_25; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_25() { return &___currentItem_25; }
	inline void set_currentItem_25(GameObject_t3674682005 * value)
	{
		___currentItem_25 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_25, value);
	}

	inline static int32_t get_offset_of_corteIdDict_31() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___corteIdDict_31)); }
	inline Dictionary_2_t1974256870 * get_corteIdDict_31() const { return ___corteIdDict_31; }
	inline Dictionary_2_t1974256870 ** get_address_of_corteIdDict_31() { return &___corteIdDict_31; }
	inline void set_corteIdDict_31(Dictionary_2_t1974256870 * value)
	{
		___corteIdDict_31 = value;
		Il2CppCodeGenWriteBarrier(&___corteIdDict_31, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_32() { return static_cast<int32_t>(offsetof(PedidoAcougue_t9638794_StaticFields, ___currentJsonTxt_32)); }
	inline String_t* get_currentJsonTxt_32() const { return ___currentJsonTxt_32; }
	inline String_t** get_address_of_currentJsonTxt_32() { return &___currentJsonTxt_32; }
	inline void set_currentJsonTxt_32(String_t* value)
	{
		___currentJsonTxt_32 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
