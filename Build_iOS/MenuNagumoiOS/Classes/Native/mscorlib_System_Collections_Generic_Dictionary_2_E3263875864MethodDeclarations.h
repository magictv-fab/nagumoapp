﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>
struct Dictionary_2_t1946552472;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3263875864.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3385470484_gshared (Enumerator_t3263875864 * __this, Dictionary_2_t1946552472 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3385470484(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3263875864 *, Dictionary_2_t1946552472 *, const MethodInfo*))Enumerator__ctor_m3385470484_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1738102871_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1738102871(__this, method) ((  Il2CppObject * (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1738102871_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m433077793_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m433077793(__this, method) ((  void (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m433077793_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m62650648_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m62650648(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m62650648_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2097461235_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2097461235(__this, method) ((  Il2CppObject * (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2097461235_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2369021381_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2369021381(__this, method) ((  Il2CppObject * (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2369021381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3152173905_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3152173905(__this, method) ((  bool (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_MoveNext_m3152173905_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1845333178  Enumerator_get_Current_m2674279371_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2674279371(__this, method) ((  KeyValuePair_2_t1845333178  (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_get_Current_m2674279371_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2085094682_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2085094682(__this, method) ((  int32_t (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_get_CurrentKey_m2085094682_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m128293082_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m128293082(__this, method) ((  Il2CppObject * (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_get_CurrentValue_m128293082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3310414438_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3310414438(__this, method) ((  void (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_Reset_m3310414438_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1095259631_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1095259631(__this, method) ((  void (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_VerifyState_m1095259631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3022389655_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3022389655(__this, method) ((  void (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_VerifyCurrent_m3022389655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1260937654_gshared (Enumerator_t3263875864 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1260937654(__this, method) ((  void (*) (Enumerator_t3263875864 *, const MethodInfo*))Enumerator_Dispose_m1260937654_gshared)(__this, method)
