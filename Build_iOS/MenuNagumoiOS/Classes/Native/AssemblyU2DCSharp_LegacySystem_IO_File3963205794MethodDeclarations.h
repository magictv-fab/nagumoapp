﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LegacySystem.IO.File
struct File_t3963205794;
// System.String
struct String_t;
// System.IO.StreamWriter
struct StreamWriter_t2705123075;
// System.IO.StreamReader
struct StreamReader_t2549717843;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void LegacySystem.IO.File::.ctor()
extern "C"  void File__ctor_m641180013 (File_t3963205794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LegacySystem.IO.File::Delete(System.String)
extern "C"  void File_Delete_m2377783968 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter LegacySystem.IO.File::AppendText(System.String)
extern "C"  StreamWriter_t2705123075 * File_AppendText_m1057834275 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamReader LegacySystem.IO.File::OpenText(System.String)
extern "C"  StreamReader_t2549717843 * File_OpenText_m2016396931 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
