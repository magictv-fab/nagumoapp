﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.Version/ECBlocks
struct ECBlocks_t3771581654;
// ZXing.QrCode.Internal.Version/ECB[]
struct ECBU5BU5D_t54329447;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version/ECB[])
extern "C"  void ECBlocks__ctor_m133575470 (ECBlocks_t3771581654 * __this, int32_t ___ecCodewordsPerBlock0, ECBU5BU5D_t54329447* ___ecBlocks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_ECCodewordsPerBlock()
extern "C"  int32_t ECBlocks_get_ECCodewordsPerBlock_m3587771930 (ECBlocks_t3771581654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_NumBlocks()
extern "C"  int32_t ECBlocks_get_NumBlocks_m892271480 (ECBlocks_t3771581654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_TotalECCodewords()
extern "C"  int32_t ECBlocks_get_TotalECCodewords_m3200051056 (ECBlocks_t3771581654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::getECBlocks()
extern "C"  ECBU5BU5D_t54329447* ECBlocks_getECBlocks_m2968873406 (ECBlocks_t3771581654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
