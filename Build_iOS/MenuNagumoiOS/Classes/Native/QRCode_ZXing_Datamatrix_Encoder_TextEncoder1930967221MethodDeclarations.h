﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.TextEncoder
struct TextEncoder_t1930967221;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Int32 ZXing.Datamatrix.Encoder.TextEncoder::get_EncodingMode()
extern "C"  int32_t TextEncoder_get_EncodingMode_m3060302874 (TextEncoder_t1930967221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.TextEncoder::encodeChar(System.Char,System.Text.StringBuilder)
extern "C"  int32_t TextEncoder_encodeChar_m3644557370 (TextEncoder_t1930967221 * __this, Il2CppChar ___c0, StringBuilder_t243639308 * ___sb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.TextEncoder::.ctor()
extern "C"  void TextEncoder__ctor_m3285235127 (TextEncoder_t1930967221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
