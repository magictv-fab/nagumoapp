﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.FBJavaClass
struct FBJavaClass_t2461275164;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Facebook.Unity.Mobile.Android.FBJavaClass::.ctor()
extern "C"  void FBJavaClass__ctor_m3508152411 (FBJavaClass_t2461275164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.Android.FBJavaClass::CallStatic(System.String,System.Object[])
extern "C"  void FBJavaClass_CallStatic_m2178408089 (FBJavaClass_t2461275164 * __this, String_t* ___methodName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
