﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.QrCode.Internal.Version/ECBlocks[]
struct ECBlocksU5BU5D_t663932691;
// ZXing.QrCode.Internal.Version/ECBlocks
struct ECBlocks_t3771581654;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// ZXing.QrCode.Internal.Version[]
struct VersionU5BU5D_t1560908587;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"

// System.Void ZXing.QrCode.Internal.Version::.ctor(System.Int32,System.Int32[],ZXing.QrCode.Internal.Version/ECBlocks[])
extern "C"  void Version__ctor_m2189734200 (Version_t1953509534 * __this, int32_t ___versionNumber0, Int32U5BU5D_t3230847821* ___alignmentPatternCenters1, ECBlocksU5BU5D_t663932691* ___ecBlocks2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Version::get_VersionNumber()
extern "C"  int32_t Version_get_VersionNumber_m1987148850 (Version_t1953509534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.QrCode.Internal.Version::get_AlignmentPatternCenters()
extern "C"  Int32U5BU5D_t3230847821* Version_get_AlignmentPatternCenters_m2467869216 (Version_t1953509534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Version::get_TotalCodewords()
extern "C"  int32_t Version_get_TotalCodewords_m4050597321 (Version_t1953509534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Version::get_DimensionForVersion()
extern "C"  int32_t Version_get_DimensionForVersion_m326665926 (Version_t1953509534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version/ECBlocks ZXing.QrCode.Internal.Version::getECBlocksForLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern "C"  ECBlocks_t3771581654 * Version_getECBlocksForLevel_m767953167 (Version_t1953509534 * __this, ErrorCorrectionLevel_t1225927610 * ___ecLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getProvisionalVersionForDimension(System.Int32)
extern "C"  Version_t1953509534 * Version_getProvisionalVersionForDimension_m4075161438 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getVersionForNumber(System.Int32)
extern "C"  Version_t1953509534 * Version_getVersionForNumber_m2313302691 (Il2CppObject * __this /* static, unused */, int32_t ___versionNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::decodeVersionInformation(System.Int32)
extern "C"  Version_t1953509534 * Version_decodeVersionInformation_m3314811603 (Il2CppObject * __this /* static, unused */, int32_t ___versionBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.QrCode.Internal.Version::buildFunctionPattern()
extern "C"  BitMatrix_t1058711404 * Version_buildFunctionPattern_m825497432 (Version_t1953509534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.QrCode.Internal.Version::ToString()
extern "C"  String_t* Version_ToString_m886641919 (Version_t1953509534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::buildVersions()
extern "C"  VersionU5BU5D_t1560908587* Version_buildVersions_m1139911657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Version::.cctor()
extern "C"  void Version__cctor_m3548479679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
