﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSystemDateTime
struct  GetSystemDateTime_t1295868974  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSystemDateTime::storeString
	FsmString_t952858651 * ___storeString_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSystemDateTime::format
	FsmString_t952858651 * ___format_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSystemDateTime::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_storeString_9() { return static_cast<int32_t>(offsetof(GetSystemDateTime_t1295868974, ___storeString_9)); }
	inline FsmString_t952858651 * get_storeString_9() const { return ___storeString_9; }
	inline FsmString_t952858651 ** get_address_of_storeString_9() { return &___storeString_9; }
	inline void set_storeString_9(FsmString_t952858651 * value)
	{
		___storeString_9 = value;
		Il2CppCodeGenWriteBarrier(&___storeString_9, value);
	}

	inline static int32_t get_offset_of_format_10() { return static_cast<int32_t>(offsetof(GetSystemDateTime_t1295868974, ___format_10)); }
	inline FsmString_t952858651 * get_format_10() const { return ___format_10; }
	inline FsmString_t952858651 ** get_address_of_format_10() { return &___format_10; }
	inline void set_format_10(FsmString_t952858651 * value)
	{
		___format_10 = value;
		Il2CppCodeGenWriteBarrier(&___format_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetSystemDateTime_t1295868974, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
