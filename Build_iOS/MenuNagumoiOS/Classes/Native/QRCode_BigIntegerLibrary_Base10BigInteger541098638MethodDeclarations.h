﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BigIntegerLibrary.Base10BigInteger
struct Base10BigInteger_t541098638;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_BigIntegerLibrary_Sign1848424669.h"
#include "QRCode_BigIntegerLibrary_Base10BigInteger541098638.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BigIntegerLibrary.Base10BigInteger::set_NumberSign(BigIntegerLibrary.Sign)
extern "C"  void Base10BigInteger_set_NumberSign_m810298045 (Base10BigInteger_t541098638 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.Base10BigInteger::.ctor()
extern "C"  void Base10BigInteger__ctor_m201545692 (Base10BigInteger_t541098638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.Base10BigInteger::.ctor(System.Int64)
extern "C"  void Base10BigInteger__ctor_m3051368110 (Base10BigInteger_t541098638 * __this, int64_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.Base10BigInteger::.ctor(BigIntegerLibrary.Base10BigInteger)
extern "C"  void Base10BigInteger__ctor_m1817872061 (Base10BigInteger_t541098638 * __this, Base10BigInteger_t541098638 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_Equals_m2462924092 (Base10BigInteger_t541098638 * __this, Base10BigInteger_t541098638 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(System.Object)
extern "C"  bool Base10BigInteger_Equals_m741042009 (Base10BigInteger_t541098638 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BigIntegerLibrary.Base10BigInteger::GetHashCode()
extern "C"  int32_t Base10BigInteger_GetHashCode_m316663293 (Base10BigInteger_t541098638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BigIntegerLibrary.Base10BigInteger::ToString()
extern "C"  String_t* Base10BigInteger_ToString_m2874122935 (Base10BigInteger_t541098638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Opposite(BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Opposite_m2003300110 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::Greater(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_Greater_m2266239654 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::GreaterOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_GreaterOrEqual_m545301555 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::SmallerOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_SmallerOrEqual_m3078514349 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Abs(BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Abs_m919424495 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Addition_m3791199588 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiplication(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Multiplication_m2953888090 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Implicit(System.Int64)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_op_Implicit_m1394294001 (Il2CppObject * __this /* static, unused */, int64_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::op_Equality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_op_Equality_m4220154248 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::op_Inequality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_op_Inequality_m2205696643 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_op_GreaterThanOrEqual_m3320049900 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.Base10BigInteger::op_LessThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  bool Base10BigInteger_op_LessThanOrEqual_m3031094513 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_UnaryNegation(BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_op_UnaryNegation_m3264802621 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_op_Addition_m1955139376 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_op_Multiply_m363464456 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Add(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Add_m2413882761 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Subtract(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Subtract_m1604235852 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern "C"  Base10BigInteger_t541098638 * Base10BigInteger_Multiply_m2199524668 (Il2CppObject * __this /* static, unused */, Base10BigInteger_t541098638 * ___a0, Base10BigInteger_t541098638 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.Base10BigInteger::.cctor()
extern "C"  void Base10BigInteger__cctor_m1470852945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
