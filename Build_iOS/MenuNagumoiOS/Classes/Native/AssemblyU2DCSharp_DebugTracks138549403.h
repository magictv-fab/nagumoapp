﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DebugTracks
struct DebugTracks_t138549403;
// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugTracks
struct  DebugTracks_t138549403  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean DebugTracks::active
	bool ___active_3;
	// System.Boolean DebugTracks::showMenu
	bool ___showMenu_4;
	// MagicTV.abstracts.device.DeviceFileInfoAbstract DebugTracks::fileInfo
	DeviceFileInfoAbstract_t3788299492 * ___fileInfo_5;
	// System.Int32 DebugTracks::currentPage
	int32_t ___currentPage_6;
	// System.Int32 DebugTracks::totalButtonsPerPage
	int32_t ___totalButtonsPerPage_7;

public:
	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(DebugTracks_t138549403, ___active_3)); }
	inline bool get_active_3() const { return ___active_3; }
	inline bool* get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(bool value)
	{
		___active_3 = value;
	}

	inline static int32_t get_offset_of_showMenu_4() { return static_cast<int32_t>(offsetof(DebugTracks_t138549403, ___showMenu_4)); }
	inline bool get_showMenu_4() const { return ___showMenu_4; }
	inline bool* get_address_of_showMenu_4() { return &___showMenu_4; }
	inline void set_showMenu_4(bool value)
	{
		___showMenu_4 = value;
	}

	inline static int32_t get_offset_of_fileInfo_5() { return static_cast<int32_t>(offsetof(DebugTracks_t138549403, ___fileInfo_5)); }
	inline DeviceFileInfoAbstract_t3788299492 * get_fileInfo_5() const { return ___fileInfo_5; }
	inline DeviceFileInfoAbstract_t3788299492 ** get_address_of_fileInfo_5() { return &___fileInfo_5; }
	inline void set_fileInfo_5(DeviceFileInfoAbstract_t3788299492 * value)
	{
		___fileInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___fileInfo_5, value);
	}

	inline static int32_t get_offset_of_currentPage_6() { return static_cast<int32_t>(offsetof(DebugTracks_t138549403, ___currentPage_6)); }
	inline int32_t get_currentPage_6() const { return ___currentPage_6; }
	inline int32_t* get_address_of_currentPage_6() { return &___currentPage_6; }
	inline void set_currentPage_6(int32_t value)
	{
		___currentPage_6 = value;
	}

	inline static int32_t get_offset_of_totalButtonsPerPage_7() { return static_cast<int32_t>(offsetof(DebugTracks_t138549403, ___totalButtonsPerPage_7)); }
	inline int32_t get_totalButtonsPerPage_7() const { return ___totalButtonsPerPage_7; }
	inline int32_t* get_address_of_totalButtonsPerPage_7() { return &___totalButtonsPerPage_7; }
	inline void set_totalButtonsPerPage_7(int32_t value)
	{
		___totalButtonsPerPage_7 = value;
	}
};

struct DebugTracks_t138549403_StaticFields
{
public:
	// DebugTracks DebugTracks::Instance
	DebugTracks_t138549403 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(DebugTracks_t138549403_StaticFields, ___Instance_2)); }
	inline DebugTracks_t138549403 * get_Instance_2() const { return ___Instance_2; }
	inline DebugTracks_t138549403 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(DebugTracks_t138549403 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
