﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;
// System.String
struct String_t;
// MagicTVWindowPromptControllerScript/ClickOkPromptDelegate
struct ClickOkPromptDelegate_t3728636616;

#include "AssemblyU2DCSharp_MagicTVAbstractWindowConfirmContr227123142.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowPromptControllerScript
struct  MagicTVWindowPromptControllerScript_t1472770442  : public MagicTVAbstractWindowConfirmControllerScript_t227123142
{
public:
	// UnityEngine.UI.InputField MagicTVWindowPromptControllerScript::LabelInputPrompt
	InputField_t609046876 * ___LabelInputPrompt_11;
	// System.String MagicTVWindowPromptControllerScript::_defaultText
	String_t* ____defaultText_12;
	// MagicTVWindowPromptControllerScript/ClickOkPromptDelegate MagicTVWindowPromptControllerScript::OnClickOk
	ClickOkPromptDelegate_t3728636616 * ___OnClickOk_13;

public:
	inline static int32_t get_offset_of_LabelInputPrompt_11() { return static_cast<int32_t>(offsetof(MagicTVWindowPromptControllerScript_t1472770442, ___LabelInputPrompt_11)); }
	inline InputField_t609046876 * get_LabelInputPrompt_11() const { return ___LabelInputPrompt_11; }
	inline InputField_t609046876 ** get_address_of_LabelInputPrompt_11() { return &___LabelInputPrompt_11; }
	inline void set_LabelInputPrompt_11(InputField_t609046876 * value)
	{
		___LabelInputPrompt_11 = value;
		Il2CppCodeGenWriteBarrier(&___LabelInputPrompt_11, value);
	}

	inline static int32_t get_offset_of__defaultText_12() { return static_cast<int32_t>(offsetof(MagicTVWindowPromptControllerScript_t1472770442, ____defaultText_12)); }
	inline String_t* get__defaultText_12() const { return ____defaultText_12; }
	inline String_t** get_address_of__defaultText_12() { return &____defaultText_12; }
	inline void set__defaultText_12(String_t* value)
	{
		____defaultText_12 = value;
		Il2CppCodeGenWriteBarrier(&____defaultText_12, value);
	}

	inline static int32_t get_offset_of_OnClickOk_13() { return static_cast<int32_t>(offsetof(MagicTVWindowPromptControllerScript_t1472770442, ___OnClickOk_13)); }
	inline ClickOkPromptDelegate_t3728636616 * get_OnClickOk_13() const { return ___OnClickOk_13; }
	inline ClickOkPromptDelegate_t3728636616 ** get_address_of_OnClickOk_13() { return &___OnClickOk_13; }
	inline void set_OnClickOk_13(ClickOkPromptDelegate_t3728636616 * value)
	{
		___OnClickOk_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnClickOk_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
