﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int64[][]
struct Int64U5BU5DU5BU5D_t1231312615;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.Base10BigInteger/DigitContainer
struct  DigitContainer_t3576869316  : public Il2CppObject
{
public:
	// System.Int64[][] BigIntegerLibrary.Base10BigInteger/DigitContainer::digits
	Int64U5BU5DU5BU5D_t1231312615* ___digits_0;

public:
	inline static int32_t get_offset_of_digits_0() { return static_cast<int32_t>(offsetof(DigitContainer_t3576869316, ___digits_0)); }
	inline Int64U5BU5DU5BU5D_t1231312615* get_digits_0() const { return ___digits_0; }
	inline Int64U5BU5DU5BU5D_t1231312615** get_address_of_digits_0() { return &___digits_0; }
	inline void set_digits_0(Int64U5BU5DU5BU5D_t1231312615* value)
	{
		___digits_0 = value;
		Il2CppCodeGenWriteBarrier(&___digits_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
