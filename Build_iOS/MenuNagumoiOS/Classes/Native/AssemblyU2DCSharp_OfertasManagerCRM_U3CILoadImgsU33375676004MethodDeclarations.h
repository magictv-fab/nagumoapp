﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM/<ILoadImgs>c__Iterator6
struct U3CILoadImgsU3Ec__Iterator6_t3375676004;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM/<ILoadImgs>c__Iterator6::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator6__ctor_m3717868423 (U3CILoadImgsU3Ec__Iterator6_t3375676004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<ILoadImgs>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1921344619 (U3CILoadImgsU3Ec__Iterator6_t3375676004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<ILoadImgs>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m4213939711 (U3CILoadImgsU3Ec__Iterator6_t3375676004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM/<ILoadImgs>c__Iterator6::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator6_MoveNext_m4040942093 (U3CILoadImgsU3Ec__Iterator6_t3375676004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<ILoadImgs>c__Iterator6::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator6_Dispose_m3652014596 (U3CILoadImgsU3Ec__Iterator6_t3375676004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<ILoadImgs>c__Iterator6::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator6_Reset_m1364301364 (U3CILoadImgsU3Ec__Iterator6_t3375676004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
