﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo>
struct List_1_t1068734154;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t4286713048;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"

// System.Type HutongGames.PlayMaker.ReflectionUtils::GetGlobalType(System.String)
extern "C"  Type_t * ReflectionUtils_GetGlobalType_m3454401768 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ReflectionUtils::GetPropertyType(System.Type,System.String)
extern "C"  Type_t * ReflectionUtils_GetPropertyType_m1270033257 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] HutongGames.PlayMaker.ReflectionUtils::GetMemberInfo(System.Type,System.String)
extern "C"  MemberInfoU5BU5D_t674955999* ReflectionUtils_GetMemberInfo_m3169803543 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ReflectionUtils::CanReadMemberValue(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_CanReadMemberValue_m3245208610 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ReflectionUtils::CanSetMemberValue(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_CanSetMemberValue_m1108096574 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ReflectionUtils::CanGetMemberValue(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_CanGetMemberValue_m519521738 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ReflectionUtils::GetMemberUnderlyingType(System.Reflection.MemberInfo)
extern "C"  Type_t * ReflectionUtils_GetMemberUnderlyingType_m1589287586 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo[],System.Object)
extern "C"  Il2CppObject * ReflectionUtils_GetMemberValue_m3067547489 (Il2CppObject * __this /* static, unused */, MemberInfoU5BU5D_t674955999* ___memberInfo0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern "C"  Il2CppObject * ReflectionUtils_GetMemberValue_m3589017091 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern "C"  void ReflectionUtils_SetMemberValue_m2730875866 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo[],System.Object,System.Object)
extern "C"  void ReflectionUtils_SetMemberValue_m1024209528 (Il2CppObject * __this /* static, unused */, MemberInfoU5BU5D_t674955999* ___memberInfo0, Il2CppObject * ___target1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::SetBoxedMemberValue(System.Object,System.Reflection.MemberInfo,System.Object,System.Reflection.MemberInfo,System.Object)
extern "C"  void ReflectionUtils_SetBoxedMemberValue_m4249179992 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___parent0, MemberInfo_t * ___targetInfo1, Il2CppObject * ___target2, MemberInfo_t * ___propertyInfo3, Il2CppObject * ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> HutongGames.PlayMaker.ReflectionUtils::GetFieldsAndProperties(System.Type,System.Reflection.BindingFlags)
extern "C"  List_1_t1068734154 * ReflectionUtils_GetFieldsAndProperties_m4148368440 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] HutongGames.PlayMaker.ReflectionUtils::GetPublicFields(System.Type)
extern "C"  FieldInfoU5BU5D_t2567562023* ReflectionUtils_GetPublicFields_m3455307399 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] HutongGames.PlayMaker.ReflectionUtils::GetPublicProperties(System.Type)
extern "C"  PropertyInfoU5BU5D_t4286713048* ReflectionUtils_GetPublicProperties_m932201218 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::.cctor()
extern "C"  void ReflectionUtils__cctor_m898286238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
