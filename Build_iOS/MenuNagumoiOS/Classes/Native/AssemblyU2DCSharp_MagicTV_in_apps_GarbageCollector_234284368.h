﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.GarbageCollector/<getGarbageItem>c__AnonStoreyA0
struct  U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368  : public Il2CppObject
{
public:
	// System.String MagicTV.in_apps.GarbageCollector/<getGarbageItem>c__AnonStoreyA0::trackingName
	String_t* ___trackingName_0;

public:
	inline static int32_t get_offset_of_trackingName_0() { return static_cast<int32_t>(offsetof(U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368, ___trackingName_0)); }
	inline String_t* get_trackingName_0() const { return ___trackingName_0; }
	inline String_t** get_address_of_trackingName_0() { return &___trackingName_0; }
	inline void set_trackingName_0(String_t* value)
	{
		___trackingName_0 = value;
		Il2CppCodeGenWriteBarrier(&___trackingName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
