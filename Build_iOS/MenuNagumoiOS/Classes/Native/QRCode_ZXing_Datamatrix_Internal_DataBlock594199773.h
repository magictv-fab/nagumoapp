﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.DataBlock
struct  DataBlock_t594199773  : public Il2CppObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.DataBlock::numDataCodewords
	int32_t ___numDataCodewords_0;
	// System.Byte[] ZXing.Datamatrix.Internal.DataBlock::codewords
	ByteU5BU5D_t4260760469* ___codewords_1;

public:
	inline static int32_t get_offset_of_numDataCodewords_0() { return static_cast<int32_t>(offsetof(DataBlock_t594199773, ___numDataCodewords_0)); }
	inline int32_t get_numDataCodewords_0() const { return ___numDataCodewords_0; }
	inline int32_t* get_address_of_numDataCodewords_0() { return &___numDataCodewords_0; }
	inline void set_numDataCodewords_0(int32_t value)
	{
		___numDataCodewords_0 = value;
	}

	inline static int32_t get_offset_of_codewords_1() { return static_cast<int32_t>(offsetof(DataBlock_t594199773, ___codewords_1)); }
	inline ByteU5BU5D_t4260760469* get_codewords_1() const { return ___codewords_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_codewords_1() { return &___codewords_1; }
	inline void set_codewords_1(ByteU5BU5D_t4260760469* value)
	{
		___codewords_1 = value;
		Il2CppCodeGenWriteBarrier(&___codewords_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
