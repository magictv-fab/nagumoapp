﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.ExpandedRow
struct ExpandedRow_t1562831751;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>
struct List_1_t486584943;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::.ctor(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32,System.Boolean)
extern "C"  void ExpandedRow__ctor_m2658048897 (ExpandedRow_t1562831751 * __this, List_1_t486584943 * ___pairs0, int32_t ___rowNumber1, bool ___wasReversed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.ExpandedRow::get_Pairs()
extern "C"  List_1_t486584943 * ExpandedRow_get_Pairs_m1735854660 (ExpandedRow_t1562831751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_Pairs(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern "C"  void ExpandedRow_set_Pairs_m2017905441 (ExpandedRow_t1562831751 * __this, List_1_t486584943 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::get_RowNumber()
extern "C"  int32_t ExpandedRow_get_RowNumber_m899744771 (ExpandedRow_t1562831751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_RowNumber(System.Int32)
extern "C"  void ExpandedRow_set_RowNumber_m145786514 (ExpandedRow_t1562831751 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::get_IsReversed()
extern "C"  bool ExpandedRow_get_IsReversed_m2082712584 (ExpandedRow_t1562831751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_IsReversed(System.Boolean)
extern "C"  void ExpandedRow_set_IsReversed_m748861287 (ExpandedRow_t1562831751 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::IsEquivalent(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern "C"  bool ExpandedRow_IsEquivalent_m2451239313 (ExpandedRow_t1562831751 * __this, List_1_t486584943 * ___otherPairs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.ExpandedRow::ToString()
extern "C"  String_t* ExpandedRow_ToString_m1704400528 (ExpandedRow_t1562831751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::Equals(System.Object)
extern "C"  bool ExpandedRow_Equals_m1386851618 (ExpandedRow_t1562831751 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::GetHashCode()
extern "C"  int32_t ExpandedRow_GetHashCode_m3496651194 (ExpandedRow_t1562831751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
