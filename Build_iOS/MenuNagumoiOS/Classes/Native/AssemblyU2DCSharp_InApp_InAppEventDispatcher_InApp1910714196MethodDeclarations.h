﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.InAppEventDispatcher/InAppEventDispatcherWithString
struct InAppEventDispatcherWithString_t1910714196;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void InApp.InAppEventDispatcher/InAppEventDispatcherWithString::.ctor(System.Object,System.IntPtr)
extern "C"  void InAppEventDispatcherWithString__ctor_m1694662955 (InAppEventDispatcherWithString_t1910714196 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher/InAppEventDispatcherWithString::Invoke(System.String)
extern "C"  void InAppEventDispatcherWithString_Invoke_m1506101725 (InAppEventDispatcherWithString_t1910714196 * __this, String_t* ___metadado0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult InApp.InAppEventDispatcher/InAppEventDispatcherWithString::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InAppEventDispatcherWithString_BeginInvoke_m4099889386 (InAppEventDispatcherWithString_t1910714196 * __this, String_t* ___metadado0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher/InAppEventDispatcherWithString::EndInvoke(System.IAsyncResult)
extern "C"  void InAppEventDispatcherWithString_EndInvoke_m2625847483 (InAppEventDispatcherWithString_t1910714196 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
