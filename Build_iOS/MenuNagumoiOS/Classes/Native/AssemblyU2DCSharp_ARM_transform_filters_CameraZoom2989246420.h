﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TouchControllerState2242910518.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.CameraZoomFilter
struct  CameraZoomFilter_t2989246420  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilter::positionToZoom
	Vector3_t4282066566  ___positionToZoom_15;
	// System.Single ARM.transform.filters.CameraZoomFilter::percentZoom
	float ___percentZoom_16;
	// System.Single ARM.transform.filters.CameraZoomFilter::ScaleDeltaMinimum
	float ___ScaleDeltaMinimum_17;
	// System.Single ARM.transform.filters.CameraZoomFilter::ScaleDeltaMaximum
	float ___ScaleDeltaMaximum_18;
	// TouchControllerState ARM.transform.filters.CameraZoomFilter::currentState
	int32_t ___currentState_19;
	// System.Single ARM.transform.filters.CameraZoomFilter::touchDistance
	float ___touchDistance_20;
	// System.Single ARM.transform.filters.CameraZoomFilter::deltaZoom
	float ___deltaZoom_21;
	// System.Single ARM.transform.filters.CameraZoomFilter::deltaZoomRatio
	float ___deltaZoomRatio_22;
	// System.Single ARM.transform.filters.CameraZoomFilter::zoomMax
	float ___zoomMax_23;
	// System.Single ARM.transform.filters.CameraZoomFilter::zoomMin
	float ___zoomMin_24;

public:
	inline static int32_t get_offset_of_positionToZoom_15() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___positionToZoom_15)); }
	inline Vector3_t4282066566  get_positionToZoom_15() const { return ___positionToZoom_15; }
	inline Vector3_t4282066566 * get_address_of_positionToZoom_15() { return &___positionToZoom_15; }
	inline void set_positionToZoom_15(Vector3_t4282066566  value)
	{
		___positionToZoom_15 = value;
	}

	inline static int32_t get_offset_of_percentZoom_16() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___percentZoom_16)); }
	inline float get_percentZoom_16() const { return ___percentZoom_16; }
	inline float* get_address_of_percentZoom_16() { return &___percentZoom_16; }
	inline void set_percentZoom_16(float value)
	{
		___percentZoom_16 = value;
	}

	inline static int32_t get_offset_of_ScaleDeltaMinimum_17() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___ScaleDeltaMinimum_17)); }
	inline float get_ScaleDeltaMinimum_17() const { return ___ScaleDeltaMinimum_17; }
	inline float* get_address_of_ScaleDeltaMinimum_17() { return &___ScaleDeltaMinimum_17; }
	inline void set_ScaleDeltaMinimum_17(float value)
	{
		___ScaleDeltaMinimum_17 = value;
	}

	inline static int32_t get_offset_of_ScaleDeltaMaximum_18() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___ScaleDeltaMaximum_18)); }
	inline float get_ScaleDeltaMaximum_18() const { return ___ScaleDeltaMaximum_18; }
	inline float* get_address_of_ScaleDeltaMaximum_18() { return &___ScaleDeltaMaximum_18; }
	inline void set_ScaleDeltaMaximum_18(float value)
	{
		___ScaleDeltaMaximum_18 = value;
	}

	inline static int32_t get_offset_of_currentState_19() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___currentState_19)); }
	inline int32_t get_currentState_19() const { return ___currentState_19; }
	inline int32_t* get_address_of_currentState_19() { return &___currentState_19; }
	inline void set_currentState_19(int32_t value)
	{
		___currentState_19 = value;
	}

	inline static int32_t get_offset_of_touchDistance_20() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___touchDistance_20)); }
	inline float get_touchDistance_20() const { return ___touchDistance_20; }
	inline float* get_address_of_touchDistance_20() { return &___touchDistance_20; }
	inline void set_touchDistance_20(float value)
	{
		___touchDistance_20 = value;
	}

	inline static int32_t get_offset_of_deltaZoom_21() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___deltaZoom_21)); }
	inline float get_deltaZoom_21() const { return ___deltaZoom_21; }
	inline float* get_address_of_deltaZoom_21() { return &___deltaZoom_21; }
	inline void set_deltaZoom_21(float value)
	{
		___deltaZoom_21 = value;
	}

	inline static int32_t get_offset_of_deltaZoomRatio_22() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___deltaZoomRatio_22)); }
	inline float get_deltaZoomRatio_22() const { return ___deltaZoomRatio_22; }
	inline float* get_address_of_deltaZoomRatio_22() { return &___deltaZoomRatio_22; }
	inline void set_deltaZoomRatio_22(float value)
	{
		___deltaZoomRatio_22 = value;
	}

	inline static int32_t get_offset_of_zoomMax_23() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___zoomMax_23)); }
	inline float get_zoomMax_23() const { return ___zoomMax_23; }
	inline float* get_address_of_zoomMax_23() { return &___zoomMax_23; }
	inline void set_zoomMax_23(float value)
	{
		___zoomMax_23 = value;
	}

	inline static int32_t get_offset_of_zoomMin_24() { return static_cast<int32_t>(offsetof(CameraZoomFilter_t2989246420, ___zoomMin_24)); }
	inline float get_zoomMin_24() const { return ___zoomMin_24; }
	inline float* get_address_of_zoomMin_24() { return &___zoomMin_24; }
	inline void set_zoomMin_24(float value)
	{
		___zoomMin_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
