﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MeuPedidoAcougue
struct MeuPedidoAcougue_t2271273197;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MeuPedidoAcougue::.ctor()
extern "C"  void MeuPedidoAcougue__ctor_m401473614 (MeuPedidoAcougue_t2271273197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue::.cctor()
extern "C"  void MeuPedidoAcougue__cctor_m3373651231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue::Start()
extern "C"  void MeuPedidoAcougue_Start_m3643578702 (MeuPedidoAcougue_t2271273197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MeuPedidoAcougue::ICheckPedido(System.Boolean)
extern "C"  Il2CppObject * MeuPedidoAcougue_ICheckPedido_m3746642873 (MeuPedidoAcougue_t2271273197 * __this, bool ___isUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MeuPedidoAcougue::IConfirmarRetirada()
extern "C"  Il2CppObject * MeuPedidoAcougue_IConfirmarRetirada_m1002773306 (MeuPedidoAcougue_t2271273197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue::ButtonAvaliar()
extern "C"  void MeuPedidoAcougue_ButtonAvaliar_m588625940 (MeuPedidoAcougue_t2271273197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue::InstantiateObj(System.String,System.String)
extern "C"  void MeuPedidoAcougue_InstantiateObj_m3654078075 (MeuPedidoAcougue_t2271273197 * __this, String_t* ___code0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue::PopUp(System.String)
extern "C"  void MeuPedidoAcougue_PopUp_m3281406666 (MeuPedidoAcougue_t2271273197 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
