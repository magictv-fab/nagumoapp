﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightType
struct SetLightType_t1507991894;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightType::.ctor()
extern "C"  void SetLightType__ctor_m1344122320 (SetLightType_t1507991894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightType::Reset()
extern "C"  void SetLightType_Reset_m3285522557 (SetLightType_t1507991894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightType::OnEnter()
extern "C"  void SetLightType_OnEnter_m2603089319 (SetLightType_t1507991894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightType::DoSetLightType()
extern "C"  void SetLightType_DoSetLightType_m1127265997 (SetLightType_t1507991894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
