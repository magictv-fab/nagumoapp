﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4206632704(__this, ___l0, method) ((  void (*) (Enumerator_t3998028747 *, List_1_t3978355977 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m735810898(__this, method) ((  void (*) (Enumerator_t3998028747 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1940888904(__this, method) ((  Il2CppObject * (*) (Enumerator_t3998028747 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::Dispose()
#define Enumerator_Dispose_m3778047205(__this, method) ((  void (*) (Enumerator_t3998028747 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::VerifyState()
#define Enumerator_VerifyState_m820588958(__this, method) ((  void (*) (Enumerator_t3998028747 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::MoveNext()
#define Enumerator_MoveNext_m985714434(__this, method) ((  bool (*) (Enumerator_t3998028747 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.Reader>::get_Current()
#define Enumerator_get_Current_m1150803383(__this, method) ((  Il2CppObject * (*) (Enumerator_t3998028747 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
