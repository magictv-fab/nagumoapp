﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetInfoMenu/<ILoadImg>c__Iterator5D
struct U3CILoadImgU3Ec__Iterator5D_t529063718;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GetInfoMenu/<ILoadImg>c__Iterator5D::.ctor()
extern "C"  void U3CILoadImgU3Ec__Iterator5D__ctor_m2644774661 (U3CILoadImgU3Ec__Iterator5D_t529063718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GetInfoMenu/<ILoadImg>c__Iterator5D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgU3Ec__Iterator5D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1128206317 (U3CILoadImgU3Ec__Iterator5D_t529063718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GetInfoMenu/<ILoadImg>c__Iterator5D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgU3Ec__Iterator5D_System_Collections_IEnumerator_get_Current_m660639105 (U3CILoadImgU3Ec__Iterator5D_t529063718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GetInfoMenu/<ILoadImg>c__Iterator5D::MoveNext()
extern "C"  bool U3CILoadImgU3Ec__Iterator5D_MoveNext_m3905334991 (U3CILoadImgU3Ec__Iterator5D_t529063718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetInfoMenu/<ILoadImg>c__Iterator5D::Dispose()
extern "C"  void U3CILoadImgU3Ec__Iterator5D_Dispose_m3201060354 (U3CILoadImgU3Ec__Iterator5D_t529063718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetInfoMenu/<ILoadImg>c__Iterator5D::Reset()
extern "C"  void U3CILoadImgU3Ec__Iterator5D_Reset_m291207602 (U3CILoadImgU3Ec__Iterator5D_t529063718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
