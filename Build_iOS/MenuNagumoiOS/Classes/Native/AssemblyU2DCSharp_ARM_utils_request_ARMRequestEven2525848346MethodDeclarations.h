﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.ARMRequestEvents
struct ARMRequestEvents_t2525848346;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// ARM.utils.request.OnRequestEventHandler
struct OnRequestEventHandler_t153308946;
// ARM.utils.request.OnProgressEventHandler
struct OnProgressEventHandler_t1956336554;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"
#include "AssemblyU2DCSharp_ARM_utils_request_OnRequestEventH153308946.h"
#include "AssemblyU2DCSharp_ARM_utils_request_OnProgressEven1956336554.h"

// System.Void ARM.utils.request.ARMRequestEvents::.ctor()
extern "C"  void ARMRequestEvents__ctor_m3897335841 (ARMRequestEvents_t2525848346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::RaiseComplete(ARM.utils.request.ARMRequestVO)
extern "C"  void ARMRequestEvents_RaiseComplete_m1049651594 (ARMRequestEvents_t2525848346 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::AddCompleteEventhandler(ARM.utils.request.OnRequestEventHandler)
extern "C"  void ARMRequestEvents_AddCompleteEventhandler_m3921522711 (ARMRequestEvents_t2525848346 * __this, OnRequestEventHandler_t153308946 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::RemoveCompleteEventhandler(ARM.utils.request.OnRequestEventHandler)
extern "C"  void ARMRequestEvents_RemoveCompleteEventhandler_m992137424 (ARMRequestEvents_t2525848346 * __this, OnRequestEventHandler_t153308946 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.request.ARMRequestEvents::isComplete()
extern "C"  bool ARMRequestEvents_isComplete_m3928498650 (ARMRequestEvents_t2525848346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::RaiseError(ARM.utils.request.ARMRequestVO)
extern "C"  void ARMRequestEvents_RaiseError_m1560301317 (ARMRequestEvents_t2525848346 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::AddErrorEventhandler(ARM.utils.request.OnRequestEventHandler)
extern "C"  void ARMRequestEvents_AddErrorEventhandler_m4029361542 (ARMRequestEvents_t2525848346 * __this, OnRequestEventHandler_t153308946 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::RemoveErrorEventhandler(ARM.utils.request.OnRequestEventHandler)
extern "C"  void ARMRequestEvents_RemoveErrorEventhandler_m4072370029 (ARMRequestEvents_t2525848346 * __this, OnRequestEventHandler_t153308946 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.request.ARMRequestEvents::GetCurrentProgress()
extern "C"  float ARMRequestEvents_GetCurrentProgress_m999320287 (ARMRequestEvents_t2525848346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.request.ARMRequestEvents::GetCurrentProgressWithWeight()
extern "C"  float ARMRequestEvents_GetCurrentProgressWithWeight_m3792631005 (ARMRequestEvents_t2525848346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::RaiseProgress(System.Single)
extern "C"  void ARMRequestEvents_RaiseProgress_m1358142835 (ARMRequestEvents_t2525848346 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::AddProgressEventhandler(ARM.utils.request.OnProgressEventHandler)
extern "C"  void ARMRequestEvents_AddProgressEventhandler_m4096077235 (ARMRequestEvents_t2525848346 * __this, OnProgressEventHandler_t1956336554 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestEvents::RemoveProgressEventhandler(ARM.utils.request.OnProgressEventHandler)
extern "C"  void ARMRequestEvents_RemoveProgressEventhandler_m3479446554 (ARMRequestEvents_t2525848346 * __this, OnProgressEventHandler_t1956336554 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
