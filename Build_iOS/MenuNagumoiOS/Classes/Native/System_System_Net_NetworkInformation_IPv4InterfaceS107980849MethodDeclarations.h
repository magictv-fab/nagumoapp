﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.IPv4InterfaceStatistics
struct IPv4InterfaceStatistics_t107980849;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.NetworkInformation.IPv4InterfaceStatistics::.ctor()
extern "C"  void IPv4InterfaceStatistics__ctor_m488248819 (IPv4InterfaceStatistics_t107980849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
