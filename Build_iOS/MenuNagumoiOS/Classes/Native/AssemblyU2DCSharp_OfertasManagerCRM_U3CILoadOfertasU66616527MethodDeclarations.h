﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM/<ILoadOfertas>c__Iterator5
struct U3CILoadOfertasU3Ec__Iterator5_t66616527;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM/<ILoadOfertas>c__Iterator5::.ctor()
extern "C"  void U3CILoadOfertasU3Ec__Iterator5__ctor_m17764012 (U3CILoadOfertasU3Ec__Iterator5_t66616527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<ILoadOfertas>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m424711984 (U3CILoadOfertasU3Ec__Iterator5_t66616527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<ILoadOfertas>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2037840580 (U3CILoadOfertasU3Ec__Iterator5_t66616527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM/<ILoadOfertas>c__Iterator5::MoveNext()
extern "C"  bool U3CILoadOfertasU3Ec__Iterator5_MoveNext_m357783984 (U3CILoadOfertasU3Ec__Iterator5_t66616527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<ILoadOfertas>c__Iterator5::Dispose()
extern "C"  void U3CILoadOfertasU3Ec__Iterator5_Dispose_m4084596713 (U3CILoadOfertasU3Ec__Iterator5_t66616527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<ILoadOfertas>c__Iterator5::Reset()
extern "C"  void U3CILoadOfertasU3Ec__Iterator5_Reset_m1959164249 (U3CILoadOfertasU3Ec__Iterator5_t66616527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
