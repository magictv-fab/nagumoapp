﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.InverseTransformPoint
struct InverseTransformPoint_t1066006434;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::.ctor()
extern "C"  void InverseTransformPoint__ctor_m233269204 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::Reset()
extern "C"  void InverseTransformPoint_Reset_m2174669441 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::OnEnter()
extern "C"  void InverseTransformPoint_OnEnter_m225134251 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::OnUpdate()
extern "C"  void InverseTransformPoint_OnUpdate_m1817753848 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::DoInverseTransformPoint()
extern "C"  void InverseTransformPoint_DoInverseTransformPoint_m2473106811 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
