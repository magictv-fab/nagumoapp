﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StartLocationServiceUpdates
struct  StartLocationServiceUpdates_t407515898  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::maxWait
	FsmFloat_t2134102846 * ___maxWait_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::desiredAccuracy
	FsmFloat_t2134102846 * ___desiredAccuracy_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::updateDistance
	FsmFloat_t2134102846 * ___updateDistance_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::successEvent
	FsmEvent_t2133468028 * ___successEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::failedEvent
	FsmEvent_t2133468028 * ___failedEvent_13;
	// System.Single HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::startTime
	float ___startTime_14;

public:
	inline static int32_t get_offset_of_maxWait_9() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t407515898, ___maxWait_9)); }
	inline FsmFloat_t2134102846 * get_maxWait_9() const { return ___maxWait_9; }
	inline FsmFloat_t2134102846 ** get_address_of_maxWait_9() { return &___maxWait_9; }
	inline void set_maxWait_9(FsmFloat_t2134102846 * value)
	{
		___maxWait_9 = value;
		Il2CppCodeGenWriteBarrier(&___maxWait_9, value);
	}

	inline static int32_t get_offset_of_desiredAccuracy_10() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t407515898, ___desiredAccuracy_10)); }
	inline FsmFloat_t2134102846 * get_desiredAccuracy_10() const { return ___desiredAccuracy_10; }
	inline FsmFloat_t2134102846 ** get_address_of_desiredAccuracy_10() { return &___desiredAccuracy_10; }
	inline void set_desiredAccuracy_10(FsmFloat_t2134102846 * value)
	{
		___desiredAccuracy_10 = value;
		Il2CppCodeGenWriteBarrier(&___desiredAccuracy_10, value);
	}

	inline static int32_t get_offset_of_updateDistance_11() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t407515898, ___updateDistance_11)); }
	inline FsmFloat_t2134102846 * get_updateDistance_11() const { return ___updateDistance_11; }
	inline FsmFloat_t2134102846 ** get_address_of_updateDistance_11() { return &___updateDistance_11; }
	inline void set_updateDistance_11(FsmFloat_t2134102846 * value)
	{
		___updateDistance_11 = value;
		Il2CppCodeGenWriteBarrier(&___updateDistance_11, value);
	}

	inline static int32_t get_offset_of_successEvent_12() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t407515898, ___successEvent_12)); }
	inline FsmEvent_t2133468028 * get_successEvent_12() const { return ___successEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_successEvent_12() { return &___successEvent_12; }
	inline void set_successEvent_12(FsmEvent_t2133468028 * value)
	{
		___successEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_12, value);
	}

	inline static int32_t get_offset_of_failedEvent_13() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t407515898, ___failedEvent_13)); }
	inline FsmEvent_t2133468028 * get_failedEvent_13() const { return ___failedEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_failedEvent_13() { return &___failedEvent_13; }
	inline void set_failedEvent_13(FsmEvent_t2133468028 * value)
	{
		___failedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___failedEvent_13, value);
	}

	inline static int32_t get_offset_of_startTime_14() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t407515898, ___startTime_14)); }
	inline float get_startTime_14() const { return ___startTime_14; }
	inline float* get_address_of_startTime_14() { return &___startTime_14; }
	inline void set_startTime_14(float value)
	{
		___startTime_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
