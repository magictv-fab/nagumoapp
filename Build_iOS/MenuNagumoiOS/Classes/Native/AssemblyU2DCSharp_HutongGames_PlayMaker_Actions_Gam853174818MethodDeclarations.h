﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectCompare
struct GameObjectCompare_t853174818;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::.ctor()
extern "C"  void GameObjectCompare__ctor_m703166804 (GameObjectCompare_t853174818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::Reset()
extern "C"  void GameObjectCompare_Reset_m2644567041 (GameObjectCompare_t853174818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnEnter()
extern "C"  void GameObjectCompare_OnEnter_m825161771 (GameObjectCompare_t853174818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnUpdate()
extern "C"  void GameObjectCompare_OnUpdate_m3238737784 (GameObjectCompare_t853174818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::DoGameObjectCompare()
extern "C"  void GameObjectCompare_DoGameObjectCompare_m2271525627 (GameObjectCompare_t853174818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
