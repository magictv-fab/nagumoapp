﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName
struct  GetAnimatorCurrentTransitionInfoIsName_t3939956924  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::name
	FsmString_t952858651 * ___name_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::everyFrame
	bool ___everyFrame_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::nameMatch
	FsmBool_t1075959796 * ___nameMatch_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::nameMatchEvent
	FsmEvent_t2133468028 * ___nameMatchEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::nameDoNotMatchEvent
	FsmEvent_t2133468028 * ___nameDoNotMatchEvent_15;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::_animator
	Animator_t2776330603 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_name_11() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___name_11)); }
	inline FsmString_t952858651 * get_name_11() const { return ___name_11; }
	inline FsmString_t952858651 ** get_address_of_name_11() { return &___name_11; }
	inline void set_name_11(FsmString_t952858651 * value)
	{
		___name_11 = value;
		Il2CppCodeGenWriteBarrier(&___name_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of_nameMatch_13() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___nameMatch_13)); }
	inline FsmBool_t1075959796 * get_nameMatch_13() const { return ___nameMatch_13; }
	inline FsmBool_t1075959796 ** get_address_of_nameMatch_13() { return &___nameMatch_13; }
	inline void set_nameMatch_13(FsmBool_t1075959796 * value)
	{
		___nameMatch_13 = value;
		Il2CppCodeGenWriteBarrier(&___nameMatch_13, value);
	}

	inline static int32_t get_offset_of_nameMatchEvent_14() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___nameMatchEvent_14)); }
	inline FsmEvent_t2133468028 * get_nameMatchEvent_14() const { return ___nameMatchEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_nameMatchEvent_14() { return &___nameMatchEvent_14; }
	inline void set_nameMatchEvent_14(FsmEvent_t2133468028 * value)
	{
		___nameMatchEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___nameMatchEvent_14, value);
	}

	inline static int32_t get_offset_of_nameDoNotMatchEvent_15() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ___nameDoNotMatchEvent_15)); }
	inline FsmEvent_t2133468028 * get_nameDoNotMatchEvent_15() const { return ___nameDoNotMatchEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_nameDoNotMatchEvent_15() { return &___nameDoNotMatchEvent_15; }
	inline void set_nameDoNotMatchEvent_15(FsmEvent_t2133468028 * value)
	{
		___nameDoNotMatchEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___nameDoNotMatchEvent_15, value);
	}

	inline static int32_t get_offset_of__animatorProxy_16() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ____animatorProxy_16)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_16() const { return ____animatorProxy_16; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_16() { return &____animatorProxy_16; }
	inline void set__animatorProxy_16(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_16 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_16, value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t3939956924, ____animator_17)); }
	inline Animator_t2776330603 * get__animator_17() const { return ____animator_17; }
	inline Animator_t2776330603 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t2776330603 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier(&____animator_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
