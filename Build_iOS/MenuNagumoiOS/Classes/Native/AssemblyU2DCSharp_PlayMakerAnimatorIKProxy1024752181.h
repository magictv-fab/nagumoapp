﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t2776330603;
// System.Action`1<System.Int32>
struct Action_1_t1549654636;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerAnimatorIKProxy
struct  PlayMakerAnimatorIKProxy_t1024752181  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Animator PlayMakerAnimatorIKProxy::_animator
	Animator_t2776330603 * ____animator_2;
	// System.Action`1<System.Int32> PlayMakerAnimatorIKProxy::OnAnimatorIKEvent
	Action_1_t1549654636 * ___OnAnimatorIKEvent_3;

public:
	inline static int32_t get_offset_of__animator_2() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorIKProxy_t1024752181, ____animator_2)); }
	inline Animator_t2776330603 * get__animator_2() const { return ____animator_2; }
	inline Animator_t2776330603 ** get_address_of__animator_2() { return &____animator_2; }
	inline void set__animator_2(Animator_t2776330603 * value)
	{
		____animator_2 = value;
		Il2CppCodeGenWriteBarrier(&____animator_2, value);
	}

	inline static int32_t get_offset_of_OnAnimatorIKEvent_3() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorIKProxy_t1024752181, ___OnAnimatorIKEvent_3)); }
	inline Action_1_t1549654636 * get_OnAnimatorIKEvent_3() const { return ___OnAnimatorIKEvent_3; }
	inline Action_1_t1549654636 ** get_address_of_OnAnimatorIKEvent_3() { return &___OnAnimatorIKEvent_3; }
	inline void set_OnAnimatorIKEvent_3(Action_1_t1549654636 * value)
	{
		___OnAnimatorIKEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnAnimatorIKEvent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
