﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.QrCode.Internal.Version[]
struct VersionU5BU5D_t1560908587;
// ZXing.QrCode.Internal.Version/ECBlocks[]
struct ECBlocksU5BU5D_t663932691;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Version
struct  Version_t1953509534  : public Il2CppObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version::versionNumber
	int32_t ___versionNumber_2;
	// System.Int32[] ZXing.QrCode.Internal.Version::alignmentPatternCenters
	Int32U5BU5D_t3230847821* ___alignmentPatternCenters_3;
	// ZXing.QrCode.Internal.Version/ECBlocks[] ZXing.QrCode.Internal.Version::ecBlocks
	ECBlocksU5BU5D_t663932691* ___ecBlocks_4;
	// System.Int32 ZXing.QrCode.Internal.Version::totalCodewords
	int32_t ___totalCodewords_5;

public:
	inline static int32_t get_offset_of_versionNumber_2() { return static_cast<int32_t>(offsetof(Version_t1953509534, ___versionNumber_2)); }
	inline int32_t get_versionNumber_2() const { return ___versionNumber_2; }
	inline int32_t* get_address_of_versionNumber_2() { return &___versionNumber_2; }
	inline void set_versionNumber_2(int32_t value)
	{
		___versionNumber_2 = value;
	}

	inline static int32_t get_offset_of_alignmentPatternCenters_3() { return static_cast<int32_t>(offsetof(Version_t1953509534, ___alignmentPatternCenters_3)); }
	inline Int32U5BU5D_t3230847821* get_alignmentPatternCenters_3() const { return ___alignmentPatternCenters_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_alignmentPatternCenters_3() { return &___alignmentPatternCenters_3; }
	inline void set_alignmentPatternCenters_3(Int32U5BU5D_t3230847821* value)
	{
		___alignmentPatternCenters_3 = value;
		Il2CppCodeGenWriteBarrier(&___alignmentPatternCenters_3, value);
	}

	inline static int32_t get_offset_of_ecBlocks_4() { return static_cast<int32_t>(offsetof(Version_t1953509534, ___ecBlocks_4)); }
	inline ECBlocksU5BU5D_t663932691* get_ecBlocks_4() const { return ___ecBlocks_4; }
	inline ECBlocksU5BU5D_t663932691** get_address_of_ecBlocks_4() { return &___ecBlocks_4; }
	inline void set_ecBlocks_4(ECBlocksU5BU5D_t663932691* value)
	{
		___ecBlocks_4 = value;
		Il2CppCodeGenWriteBarrier(&___ecBlocks_4, value);
	}

	inline static int32_t get_offset_of_totalCodewords_5() { return static_cast<int32_t>(offsetof(Version_t1953509534, ___totalCodewords_5)); }
	inline int32_t get_totalCodewords_5() const { return ___totalCodewords_5; }
	inline int32_t* get_address_of_totalCodewords_5() { return &___totalCodewords_5; }
	inline void set_totalCodewords_5(int32_t value)
	{
		___totalCodewords_5 = value;
	}
};

struct Version_t1953509534_StaticFields
{
public:
	// System.Int32[] ZXing.QrCode.Internal.Version::VERSION_DECODE_INFO
	Int32U5BU5D_t3230847821* ___VERSION_DECODE_INFO_0;
	// ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::VERSIONS
	VersionU5BU5D_t1560908587* ___VERSIONS_1;

public:
	inline static int32_t get_offset_of_VERSION_DECODE_INFO_0() { return static_cast<int32_t>(offsetof(Version_t1953509534_StaticFields, ___VERSION_DECODE_INFO_0)); }
	inline Int32U5BU5D_t3230847821* get_VERSION_DECODE_INFO_0() const { return ___VERSION_DECODE_INFO_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_VERSION_DECODE_INFO_0() { return &___VERSION_DECODE_INFO_0; }
	inline void set_VERSION_DECODE_INFO_0(Int32U5BU5D_t3230847821* value)
	{
		___VERSION_DECODE_INFO_0 = value;
		Il2CppCodeGenWriteBarrier(&___VERSION_DECODE_INFO_0, value);
	}

	inline static int32_t get_offset_of_VERSIONS_1() { return static_cast<int32_t>(offsetof(Version_t1953509534_StaticFields, ___VERSIONS_1)); }
	inline VersionU5BU5D_t1560908587* get_VERSIONS_1() const { return ___VERSIONS_1; }
	inline VersionU5BU5D_t1560908587** get_address_of_VERSIONS_1() { return &___VERSIONS_1; }
	inline void set_VERSIONS_1(VersionU5BU5D_t1560908587* value)
	{
		___VERSIONS_1 = value;
		Il2CppCodeGenWriteBarrier(&___VERSIONS_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
