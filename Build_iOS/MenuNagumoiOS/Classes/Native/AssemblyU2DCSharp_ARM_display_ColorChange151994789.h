﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// ARM.display.RotateObject
struct RotateObject_t3491987816;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.display.ColorChange
struct  ColorChange_t151994789  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Material ARM.display.ColorChange::materialToChangeColor
	Material_t3870600107 * ___materialToChangeColor_2;
	// ARM.display.RotateObject ARM.display.ColorChange::objetctToRotate
	RotateObject_t3491987816 * ___objetctToRotate_3;
	// System.Int32 ARM.display.ColorChange::_indexColor
	int32_t ____indexColor_4;
	// UnityEngine.Vector3[] ARM.display.ColorChange::colors
	Vector3U5BU5D_t215400611* ___colors_5;
	// UnityEngine.Vector3 ARM.display.ColorChange::_colorTochange
	Vector3_t4282066566  ____colorTochange_6;
	// UnityEngine.Vector3 ARM.display.ColorChange::_lastColorChanged
	Vector3_t4282066566  ____lastColorChanged_7;

public:
	inline static int32_t get_offset_of_materialToChangeColor_2() { return static_cast<int32_t>(offsetof(ColorChange_t151994789, ___materialToChangeColor_2)); }
	inline Material_t3870600107 * get_materialToChangeColor_2() const { return ___materialToChangeColor_2; }
	inline Material_t3870600107 ** get_address_of_materialToChangeColor_2() { return &___materialToChangeColor_2; }
	inline void set_materialToChangeColor_2(Material_t3870600107 * value)
	{
		___materialToChangeColor_2 = value;
		Il2CppCodeGenWriteBarrier(&___materialToChangeColor_2, value);
	}

	inline static int32_t get_offset_of_objetctToRotate_3() { return static_cast<int32_t>(offsetof(ColorChange_t151994789, ___objetctToRotate_3)); }
	inline RotateObject_t3491987816 * get_objetctToRotate_3() const { return ___objetctToRotate_3; }
	inline RotateObject_t3491987816 ** get_address_of_objetctToRotate_3() { return &___objetctToRotate_3; }
	inline void set_objetctToRotate_3(RotateObject_t3491987816 * value)
	{
		___objetctToRotate_3 = value;
		Il2CppCodeGenWriteBarrier(&___objetctToRotate_3, value);
	}

	inline static int32_t get_offset_of__indexColor_4() { return static_cast<int32_t>(offsetof(ColorChange_t151994789, ____indexColor_4)); }
	inline int32_t get__indexColor_4() const { return ____indexColor_4; }
	inline int32_t* get_address_of__indexColor_4() { return &____indexColor_4; }
	inline void set__indexColor_4(int32_t value)
	{
		____indexColor_4 = value;
	}

	inline static int32_t get_offset_of_colors_5() { return static_cast<int32_t>(offsetof(ColorChange_t151994789, ___colors_5)); }
	inline Vector3U5BU5D_t215400611* get_colors_5() const { return ___colors_5; }
	inline Vector3U5BU5D_t215400611** get_address_of_colors_5() { return &___colors_5; }
	inline void set_colors_5(Vector3U5BU5D_t215400611* value)
	{
		___colors_5 = value;
		Il2CppCodeGenWriteBarrier(&___colors_5, value);
	}

	inline static int32_t get_offset_of__colorTochange_6() { return static_cast<int32_t>(offsetof(ColorChange_t151994789, ____colorTochange_6)); }
	inline Vector3_t4282066566  get__colorTochange_6() const { return ____colorTochange_6; }
	inline Vector3_t4282066566 * get_address_of__colorTochange_6() { return &____colorTochange_6; }
	inline void set__colorTochange_6(Vector3_t4282066566  value)
	{
		____colorTochange_6 = value;
	}

	inline static int32_t get_offset_of__lastColorChanged_7() { return static_cast<int32_t>(offsetof(ColorChange_t151994789, ____lastColorChanged_7)); }
	inline Vector3_t4282066566  get__lastColorChanged_7() const { return ____lastColorChanged_7; }
	inline Vector3_t4282066566 * get_address_of__lastColorChanged_7() { return &____lastColorChanged_7; }
	inline void set__lastColorChanged_7(Vector3_t4282066566  value)
	{
		____lastColorChanged_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
