﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.PDF417Common
struct  PDF417Common_t2260765717  : public Il2CppObject
{
public:

public:
};

struct PDF417Common_t2260765717_StaticFields
{
public:
	// System.Int32 ZXing.PDF417.PDF417Common::INVALID_CODEWORD
	int32_t ___INVALID_CODEWORD_0;
	// System.Int32 ZXing.PDF417.PDF417Common::NUMBER_OF_CODEWORDS
	int32_t ___NUMBER_OF_CODEWORDS_1;
	// System.Int32 ZXing.PDF417.PDF417Common::MAX_CODEWORDS_IN_BARCODE
	int32_t ___MAX_CODEWORDS_IN_BARCODE_2;
	// System.Int32 ZXing.PDF417.PDF417Common::MIN_ROWS_IN_BARCODE
	int32_t ___MIN_ROWS_IN_BARCODE_3;
	// System.Int32 ZXing.PDF417.PDF417Common::MAX_ROWS_IN_BARCODE
	int32_t ___MAX_ROWS_IN_BARCODE_4;
	// System.Int32 ZXing.PDF417.PDF417Common::MODULES_IN_CODEWORD
	int32_t ___MODULES_IN_CODEWORD_5;
	// System.Int32 ZXing.PDF417.PDF417Common::MODULES_IN_STOP_PATTERN
	int32_t ___MODULES_IN_STOP_PATTERN_6;
	// System.Int32 ZXing.PDF417.PDF417Common::BARS_IN_MODULE
	int32_t ___BARS_IN_MODULE_7;
	// System.Int32[] ZXing.PDF417.PDF417Common::EMPTY_INT_ARRAY
	Int32U5BU5D_t3230847821* ___EMPTY_INT_ARRAY_8;
	// System.Int32[] ZXing.PDF417.PDF417Common::SYMBOL_TABLE
	Int32U5BU5D_t3230847821* ___SYMBOL_TABLE_9;
	// System.Int32[] ZXing.PDF417.PDF417Common::CODEWORD_TABLE
	Int32U5BU5D_t3230847821* ___CODEWORD_TABLE_10;

public:
	inline static int32_t get_offset_of_INVALID_CODEWORD_0() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___INVALID_CODEWORD_0)); }
	inline int32_t get_INVALID_CODEWORD_0() const { return ___INVALID_CODEWORD_0; }
	inline int32_t* get_address_of_INVALID_CODEWORD_0() { return &___INVALID_CODEWORD_0; }
	inline void set_INVALID_CODEWORD_0(int32_t value)
	{
		___INVALID_CODEWORD_0 = value;
	}

	inline static int32_t get_offset_of_NUMBER_OF_CODEWORDS_1() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___NUMBER_OF_CODEWORDS_1)); }
	inline int32_t get_NUMBER_OF_CODEWORDS_1() const { return ___NUMBER_OF_CODEWORDS_1; }
	inline int32_t* get_address_of_NUMBER_OF_CODEWORDS_1() { return &___NUMBER_OF_CODEWORDS_1; }
	inline void set_NUMBER_OF_CODEWORDS_1(int32_t value)
	{
		___NUMBER_OF_CODEWORDS_1 = value;
	}

	inline static int32_t get_offset_of_MAX_CODEWORDS_IN_BARCODE_2() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___MAX_CODEWORDS_IN_BARCODE_2)); }
	inline int32_t get_MAX_CODEWORDS_IN_BARCODE_2() const { return ___MAX_CODEWORDS_IN_BARCODE_2; }
	inline int32_t* get_address_of_MAX_CODEWORDS_IN_BARCODE_2() { return &___MAX_CODEWORDS_IN_BARCODE_2; }
	inline void set_MAX_CODEWORDS_IN_BARCODE_2(int32_t value)
	{
		___MAX_CODEWORDS_IN_BARCODE_2 = value;
	}

	inline static int32_t get_offset_of_MIN_ROWS_IN_BARCODE_3() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___MIN_ROWS_IN_BARCODE_3)); }
	inline int32_t get_MIN_ROWS_IN_BARCODE_3() const { return ___MIN_ROWS_IN_BARCODE_3; }
	inline int32_t* get_address_of_MIN_ROWS_IN_BARCODE_3() { return &___MIN_ROWS_IN_BARCODE_3; }
	inline void set_MIN_ROWS_IN_BARCODE_3(int32_t value)
	{
		___MIN_ROWS_IN_BARCODE_3 = value;
	}

	inline static int32_t get_offset_of_MAX_ROWS_IN_BARCODE_4() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___MAX_ROWS_IN_BARCODE_4)); }
	inline int32_t get_MAX_ROWS_IN_BARCODE_4() const { return ___MAX_ROWS_IN_BARCODE_4; }
	inline int32_t* get_address_of_MAX_ROWS_IN_BARCODE_4() { return &___MAX_ROWS_IN_BARCODE_4; }
	inline void set_MAX_ROWS_IN_BARCODE_4(int32_t value)
	{
		___MAX_ROWS_IN_BARCODE_4 = value;
	}

	inline static int32_t get_offset_of_MODULES_IN_CODEWORD_5() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___MODULES_IN_CODEWORD_5)); }
	inline int32_t get_MODULES_IN_CODEWORD_5() const { return ___MODULES_IN_CODEWORD_5; }
	inline int32_t* get_address_of_MODULES_IN_CODEWORD_5() { return &___MODULES_IN_CODEWORD_5; }
	inline void set_MODULES_IN_CODEWORD_5(int32_t value)
	{
		___MODULES_IN_CODEWORD_5 = value;
	}

	inline static int32_t get_offset_of_MODULES_IN_STOP_PATTERN_6() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___MODULES_IN_STOP_PATTERN_6)); }
	inline int32_t get_MODULES_IN_STOP_PATTERN_6() const { return ___MODULES_IN_STOP_PATTERN_6; }
	inline int32_t* get_address_of_MODULES_IN_STOP_PATTERN_6() { return &___MODULES_IN_STOP_PATTERN_6; }
	inline void set_MODULES_IN_STOP_PATTERN_6(int32_t value)
	{
		___MODULES_IN_STOP_PATTERN_6 = value;
	}

	inline static int32_t get_offset_of_BARS_IN_MODULE_7() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___BARS_IN_MODULE_7)); }
	inline int32_t get_BARS_IN_MODULE_7() const { return ___BARS_IN_MODULE_7; }
	inline int32_t* get_address_of_BARS_IN_MODULE_7() { return &___BARS_IN_MODULE_7; }
	inline void set_BARS_IN_MODULE_7(int32_t value)
	{
		___BARS_IN_MODULE_7 = value;
	}

	inline static int32_t get_offset_of_EMPTY_INT_ARRAY_8() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___EMPTY_INT_ARRAY_8)); }
	inline Int32U5BU5D_t3230847821* get_EMPTY_INT_ARRAY_8() const { return ___EMPTY_INT_ARRAY_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_EMPTY_INT_ARRAY_8() { return &___EMPTY_INT_ARRAY_8; }
	inline void set_EMPTY_INT_ARRAY_8(Int32U5BU5D_t3230847821* value)
	{
		___EMPTY_INT_ARRAY_8 = value;
		Il2CppCodeGenWriteBarrier(&___EMPTY_INT_ARRAY_8, value);
	}

	inline static int32_t get_offset_of_SYMBOL_TABLE_9() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___SYMBOL_TABLE_9)); }
	inline Int32U5BU5D_t3230847821* get_SYMBOL_TABLE_9() const { return ___SYMBOL_TABLE_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_SYMBOL_TABLE_9() { return &___SYMBOL_TABLE_9; }
	inline void set_SYMBOL_TABLE_9(Int32U5BU5D_t3230847821* value)
	{
		___SYMBOL_TABLE_9 = value;
		Il2CppCodeGenWriteBarrier(&___SYMBOL_TABLE_9, value);
	}

	inline static int32_t get_offset_of_CODEWORD_TABLE_10() { return static_cast<int32_t>(offsetof(PDF417Common_t2260765717_StaticFields, ___CODEWORD_TABLE_10)); }
	inline Int32U5BU5D_t3230847821* get_CODEWORD_TABLE_10() const { return ___CODEWORD_TABLE_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_CODEWORD_TABLE_10() { return &___CODEWORD_TABLE_10; }
	inline void set_CODEWORD_TABLE_10(Int32U5BU5D_t3230847821* value)
	{
		___CODEWORD_TABLE_10 = value;
		Il2CppCodeGenWriteBarrier(&___CODEWORD_TABLE_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
