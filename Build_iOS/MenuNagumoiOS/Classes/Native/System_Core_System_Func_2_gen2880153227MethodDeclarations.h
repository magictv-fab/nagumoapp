﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m4093353726(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2880153227 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>>::Invoke(T)
#define Func_2_Invoke_m854055444(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t2880153227 *, BundleVO_t1984518073 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3240778819(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2880153227 *, BundleVO_t1984518073 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m363544128(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t2880153227 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
