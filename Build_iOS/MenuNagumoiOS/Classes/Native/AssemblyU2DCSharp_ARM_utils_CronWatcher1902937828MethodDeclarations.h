﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.CronWatcher
struct CronWatcher_t1902937828;
// ARM.utils.CronWatcher/OnCronEventHandler
struct OnCronEventHandler_t343176941;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_utils_CronWatcher_OnCronEvent343176941.h"

// System.Void ARM.utils.CronWatcher::.ctor()
extern "C"  void CronWatcher__ctor_m2316221964 (CronWatcher_t1902937828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::add_onUpdate(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void CronWatcher_add_onUpdate_m2762892337 (Il2CppObject * __this /* static, unused */, OnCronEventHandler_t343176941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::remove_onUpdate(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void CronWatcher_remove_onUpdate_m4015589088 (Il2CppObject * __this /* static, unused */, OnCronEventHandler_t343176941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::add_onLateUpdate(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void CronWatcher_add_onLateUpdate_m2776104183 (Il2CppObject * __this /* static, unused */, OnCronEventHandler_t343176941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::remove_onLateUpdate(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void CronWatcher_remove_onLateUpdate_m2141433894 (Il2CppObject * __this /* static, unused */, OnCronEventHandler_t343176941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::addUpdateListener(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void CronWatcher_addUpdateListener_m4015264155 (CronWatcher_t1902937828 * __this, OnCronEventHandler_t343176941 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::addLateUpdateListener(ARM.utils.CronWatcher/OnCronEventHandler)
extern "C"  void CronWatcher_addLateUpdateListener_m1834502497 (CronWatcher_t1902937828 * __this, OnCronEventHandler_t343176941 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::Update()
extern "C"  void CronWatcher_Update_m515298945 (CronWatcher_t1902937828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher::LateUpdate()
extern "C"  void CronWatcher_LateUpdate_m1422357447 (CronWatcher_t1902937828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
