﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
struct GetAnimatorCurrentTransitionInfo_t114989767;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfo__ctor_m3945219967 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfo_Reset_m1591652908 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnEnter()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnUpdate_m2019964269 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnAnimatorMoveEvent_m122932448 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::GetTransitionInfo()
extern "C"  void GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnExit()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnExit_m3556053314 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
