﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoPreencherDropDown
struct AutoPreencherDropDown_t4173649056;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoPreencherDropDown::.ctor()
extern "C"  void AutoPreencherDropDown__ctor_m1723291851 (AutoPreencherDropDown_t4173649056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoPreencherDropDown::Start()
extern "C"  void AutoPreencherDropDown_Start_m670429643 (AutoPreencherDropDown_t4173649056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoPreencherDropDown::Update()
extern "C"  void AutoPreencherDropDown_Update_m3609301922 (AutoPreencherDropDown_t4173649056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
