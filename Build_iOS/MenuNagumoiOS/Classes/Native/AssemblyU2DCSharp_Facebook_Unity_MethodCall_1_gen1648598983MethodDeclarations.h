﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen2887838056MethodDeclarations.h"

// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::.ctor(Facebook.Unity.FacebookBase,System.String)
#define MethodCall_1__ctor_m2439945355(__this, ___facebookImpl0, ___methodName1, method) ((  void (*) (MethodCall_1_t1648598983 *, FacebookBase_t850267831 *, String_t*, const MethodInfo*))MethodCall_1__ctor_m1960000006_gshared)(__this, ___facebookImpl0, ___methodName1, method)
// System.String Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::get_MethodName()
#define MethodCall_1_get_MethodName_m4059049227(__this, method) ((  String_t* (*) (MethodCall_1_t1648598983 *, const MethodInfo*))MethodCall_1_get_MethodName_m759377580_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::set_MethodName(System.String)
#define MethodCall_1_set_MethodName_m2603651648(__this, ___value0, method) ((  void (*) (MethodCall_1_t1648598983 *, String_t*, const MethodInfo*))MethodCall_1_set_MethodName_m817744037_gshared)(__this, ___value0, method)
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::get_Callback()
#define MethodCall_1_get_Callback_m3901544083(__this, method) ((  FacebookDelegate_1_t1018651059 * (*) (MethodCall_1_t1648598983 *, const MethodInfo*))MethodCall_1_get_Callback_m708582070_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
#define MethodCall_1_set_Callback_m2599528114(__this, ___value0, method) ((  void (*) (MethodCall_1_t1648598983 *, FacebookDelegate_1_t1018651059 *, const MethodInfo*))MethodCall_1_set_Callback_m933548397_gshared)(__this, ___value0, method)
// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::get_FacebookImpl()
#define MethodCall_1_get_FacebookImpl_m361823085(__this, method) ((  FacebookBase_t850267831 * (*) (MethodCall_1_t1648598983 *, const MethodInfo*))MethodCall_1_get_FacebookImpl_m1272944382_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::set_FacebookImpl(Facebook.Unity.FacebookBase)
#define MethodCall_1_set_FacebookImpl_m666965854(__this, ___value0, method) ((  void (*) (MethodCall_1_t1648598983 *, FacebookBase_t850267831 *, const MethodInfo*))MethodCall_1_set_FacebookImpl_m2871452611_gshared)(__this, ___value0, method)
// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::get_Parameters()
#define MethodCall_1_get_Parameters_m3372994563(__this, method) ((  MethodArguments_t3236074899 * (*) (MethodCall_1_t1648598983 *, const MethodInfo*))MethodCall_1_get_Parameters_m2809693360_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<Facebook.Unity.IAppRequestResult>::set_Parameters(Facebook.Unity.MethodArguments)
#define MethodCall_1_set_Parameters_m2414008300(__this, ___value0, method) ((  void (*) (MethodCall_1_t1648598983 *, MethodArguments_t3236074899 *, const MethodInfo*))MethodCall_1_set_Parameters_m2033621031_gshared)(__this, ___value0, method)
