﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ICSharpCode.SharpZipLib.Checksums.IChecksum
struct IChecksum_t3967152162;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int16[]
struct Int16U5BU5D_t801762735;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream
struct  BZip2OutputStream_t147045086  : public Stream_t1561764144
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::increments
	Int32U5BU5D_t3230847821* ___increments_9;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::isStreamOwner
	bool ___isStreamOwner_10;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::last
	int32_t ___last_11;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::origPtr
	int32_t ___origPtr_12;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::blockSize100k
	int32_t ___blockSize100k_13;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::blockRandomised
	bool ___blockRandomised_14;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::bytesOut
	int32_t ___bytesOut_15;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::bsBuff
	int32_t ___bsBuff_16;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::bsLive
	int32_t ___bsLive_17;
	// ICSharpCode.SharpZipLib.Checksums.IChecksum ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::mCrc
	Il2CppObject * ___mCrc_18;
	// System.Boolean[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::inUse
	BooleanU5BU5D_t3456302923* ___inUse_19;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::nInUse
	int32_t ___nInUse_20;
	// System.Char[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::seqToUnseq
	CharU5BU5D_t3324145743* ___seqToUnseq_21;
	// System.Char[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::unseqToSeq
	CharU5BU5D_t3324145743* ___unseqToSeq_22;
	// System.Char[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::selector
	CharU5BU5D_t3324145743* ___selector_23;
	// System.Char[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::selectorMtf
	CharU5BU5D_t3324145743* ___selectorMtf_24;
	// System.Byte[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::block
	ByteU5BU5D_t4260760469* ___block_25;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::quadrant
	Int32U5BU5D_t3230847821* ___quadrant_26;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::zptr
	Int32U5BU5D_t3230847821* ___zptr_27;
	// System.Int16[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::szptr
	Int16U5BU5D_t801762735* ___szptr_28;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::ftab
	Int32U5BU5D_t3230847821* ___ftab_29;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::nMTF
	int32_t ___nMTF_30;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::mtfFreq
	Int32U5BU5D_t3230847821* ___mtfFreq_31;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::workFactor
	int32_t ___workFactor_32;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::workDone
	int32_t ___workDone_33;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::workLimit
	int32_t ___workLimit_34;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::firstAttempt
	bool ___firstAttempt_35;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::nBlocksRandomised
	int32_t ___nBlocksRandomised_36;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::currentChar
	int32_t ___currentChar_37;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::runLength
	int32_t ___runLength_38;
	// System.UInt32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::blockCRC
	uint32_t ___blockCRC_39;
	// System.UInt32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::combinedCRC
	uint32_t ___combinedCRC_40;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::allowableBlockSize
	int32_t ___allowableBlockSize_41;
	// System.IO.Stream ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::baseStream
	Stream_t1561764144 * ___baseStream_42;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream::disposed_
	bool ___disposed__43;

public:
	inline static int32_t get_offset_of_increments_9() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___increments_9)); }
	inline Int32U5BU5D_t3230847821* get_increments_9() const { return ___increments_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_increments_9() { return &___increments_9; }
	inline void set_increments_9(Int32U5BU5D_t3230847821* value)
	{
		___increments_9 = value;
		Il2CppCodeGenWriteBarrier(&___increments_9, value);
	}

	inline static int32_t get_offset_of_isStreamOwner_10() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___isStreamOwner_10)); }
	inline bool get_isStreamOwner_10() const { return ___isStreamOwner_10; }
	inline bool* get_address_of_isStreamOwner_10() { return &___isStreamOwner_10; }
	inline void set_isStreamOwner_10(bool value)
	{
		___isStreamOwner_10 = value;
	}

	inline static int32_t get_offset_of_last_11() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___last_11)); }
	inline int32_t get_last_11() const { return ___last_11; }
	inline int32_t* get_address_of_last_11() { return &___last_11; }
	inline void set_last_11(int32_t value)
	{
		___last_11 = value;
	}

	inline static int32_t get_offset_of_origPtr_12() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___origPtr_12)); }
	inline int32_t get_origPtr_12() const { return ___origPtr_12; }
	inline int32_t* get_address_of_origPtr_12() { return &___origPtr_12; }
	inline void set_origPtr_12(int32_t value)
	{
		___origPtr_12 = value;
	}

	inline static int32_t get_offset_of_blockSize100k_13() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___blockSize100k_13)); }
	inline int32_t get_blockSize100k_13() const { return ___blockSize100k_13; }
	inline int32_t* get_address_of_blockSize100k_13() { return &___blockSize100k_13; }
	inline void set_blockSize100k_13(int32_t value)
	{
		___blockSize100k_13 = value;
	}

	inline static int32_t get_offset_of_blockRandomised_14() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___blockRandomised_14)); }
	inline bool get_blockRandomised_14() const { return ___blockRandomised_14; }
	inline bool* get_address_of_blockRandomised_14() { return &___blockRandomised_14; }
	inline void set_blockRandomised_14(bool value)
	{
		___blockRandomised_14 = value;
	}

	inline static int32_t get_offset_of_bytesOut_15() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___bytesOut_15)); }
	inline int32_t get_bytesOut_15() const { return ___bytesOut_15; }
	inline int32_t* get_address_of_bytesOut_15() { return &___bytesOut_15; }
	inline void set_bytesOut_15(int32_t value)
	{
		___bytesOut_15 = value;
	}

	inline static int32_t get_offset_of_bsBuff_16() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___bsBuff_16)); }
	inline int32_t get_bsBuff_16() const { return ___bsBuff_16; }
	inline int32_t* get_address_of_bsBuff_16() { return &___bsBuff_16; }
	inline void set_bsBuff_16(int32_t value)
	{
		___bsBuff_16 = value;
	}

	inline static int32_t get_offset_of_bsLive_17() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___bsLive_17)); }
	inline int32_t get_bsLive_17() const { return ___bsLive_17; }
	inline int32_t* get_address_of_bsLive_17() { return &___bsLive_17; }
	inline void set_bsLive_17(int32_t value)
	{
		___bsLive_17 = value;
	}

	inline static int32_t get_offset_of_mCrc_18() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___mCrc_18)); }
	inline Il2CppObject * get_mCrc_18() const { return ___mCrc_18; }
	inline Il2CppObject ** get_address_of_mCrc_18() { return &___mCrc_18; }
	inline void set_mCrc_18(Il2CppObject * value)
	{
		___mCrc_18 = value;
		Il2CppCodeGenWriteBarrier(&___mCrc_18, value);
	}

	inline static int32_t get_offset_of_inUse_19() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___inUse_19)); }
	inline BooleanU5BU5D_t3456302923* get_inUse_19() const { return ___inUse_19; }
	inline BooleanU5BU5D_t3456302923** get_address_of_inUse_19() { return &___inUse_19; }
	inline void set_inUse_19(BooleanU5BU5D_t3456302923* value)
	{
		___inUse_19 = value;
		Il2CppCodeGenWriteBarrier(&___inUse_19, value);
	}

	inline static int32_t get_offset_of_nInUse_20() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___nInUse_20)); }
	inline int32_t get_nInUse_20() const { return ___nInUse_20; }
	inline int32_t* get_address_of_nInUse_20() { return &___nInUse_20; }
	inline void set_nInUse_20(int32_t value)
	{
		___nInUse_20 = value;
	}

	inline static int32_t get_offset_of_seqToUnseq_21() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___seqToUnseq_21)); }
	inline CharU5BU5D_t3324145743* get_seqToUnseq_21() const { return ___seqToUnseq_21; }
	inline CharU5BU5D_t3324145743** get_address_of_seqToUnseq_21() { return &___seqToUnseq_21; }
	inline void set_seqToUnseq_21(CharU5BU5D_t3324145743* value)
	{
		___seqToUnseq_21 = value;
		Il2CppCodeGenWriteBarrier(&___seqToUnseq_21, value);
	}

	inline static int32_t get_offset_of_unseqToSeq_22() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___unseqToSeq_22)); }
	inline CharU5BU5D_t3324145743* get_unseqToSeq_22() const { return ___unseqToSeq_22; }
	inline CharU5BU5D_t3324145743** get_address_of_unseqToSeq_22() { return &___unseqToSeq_22; }
	inline void set_unseqToSeq_22(CharU5BU5D_t3324145743* value)
	{
		___unseqToSeq_22 = value;
		Il2CppCodeGenWriteBarrier(&___unseqToSeq_22, value);
	}

	inline static int32_t get_offset_of_selector_23() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___selector_23)); }
	inline CharU5BU5D_t3324145743* get_selector_23() const { return ___selector_23; }
	inline CharU5BU5D_t3324145743** get_address_of_selector_23() { return &___selector_23; }
	inline void set_selector_23(CharU5BU5D_t3324145743* value)
	{
		___selector_23 = value;
		Il2CppCodeGenWriteBarrier(&___selector_23, value);
	}

	inline static int32_t get_offset_of_selectorMtf_24() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___selectorMtf_24)); }
	inline CharU5BU5D_t3324145743* get_selectorMtf_24() const { return ___selectorMtf_24; }
	inline CharU5BU5D_t3324145743** get_address_of_selectorMtf_24() { return &___selectorMtf_24; }
	inline void set_selectorMtf_24(CharU5BU5D_t3324145743* value)
	{
		___selectorMtf_24 = value;
		Il2CppCodeGenWriteBarrier(&___selectorMtf_24, value);
	}

	inline static int32_t get_offset_of_block_25() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___block_25)); }
	inline ByteU5BU5D_t4260760469* get_block_25() const { return ___block_25; }
	inline ByteU5BU5D_t4260760469** get_address_of_block_25() { return &___block_25; }
	inline void set_block_25(ByteU5BU5D_t4260760469* value)
	{
		___block_25 = value;
		Il2CppCodeGenWriteBarrier(&___block_25, value);
	}

	inline static int32_t get_offset_of_quadrant_26() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___quadrant_26)); }
	inline Int32U5BU5D_t3230847821* get_quadrant_26() const { return ___quadrant_26; }
	inline Int32U5BU5D_t3230847821** get_address_of_quadrant_26() { return &___quadrant_26; }
	inline void set_quadrant_26(Int32U5BU5D_t3230847821* value)
	{
		___quadrant_26 = value;
		Il2CppCodeGenWriteBarrier(&___quadrant_26, value);
	}

	inline static int32_t get_offset_of_zptr_27() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___zptr_27)); }
	inline Int32U5BU5D_t3230847821* get_zptr_27() const { return ___zptr_27; }
	inline Int32U5BU5D_t3230847821** get_address_of_zptr_27() { return &___zptr_27; }
	inline void set_zptr_27(Int32U5BU5D_t3230847821* value)
	{
		___zptr_27 = value;
		Il2CppCodeGenWriteBarrier(&___zptr_27, value);
	}

	inline static int32_t get_offset_of_szptr_28() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___szptr_28)); }
	inline Int16U5BU5D_t801762735* get_szptr_28() const { return ___szptr_28; }
	inline Int16U5BU5D_t801762735** get_address_of_szptr_28() { return &___szptr_28; }
	inline void set_szptr_28(Int16U5BU5D_t801762735* value)
	{
		___szptr_28 = value;
		Il2CppCodeGenWriteBarrier(&___szptr_28, value);
	}

	inline static int32_t get_offset_of_ftab_29() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___ftab_29)); }
	inline Int32U5BU5D_t3230847821* get_ftab_29() const { return ___ftab_29; }
	inline Int32U5BU5D_t3230847821** get_address_of_ftab_29() { return &___ftab_29; }
	inline void set_ftab_29(Int32U5BU5D_t3230847821* value)
	{
		___ftab_29 = value;
		Il2CppCodeGenWriteBarrier(&___ftab_29, value);
	}

	inline static int32_t get_offset_of_nMTF_30() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___nMTF_30)); }
	inline int32_t get_nMTF_30() const { return ___nMTF_30; }
	inline int32_t* get_address_of_nMTF_30() { return &___nMTF_30; }
	inline void set_nMTF_30(int32_t value)
	{
		___nMTF_30 = value;
	}

	inline static int32_t get_offset_of_mtfFreq_31() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___mtfFreq_31)); }
	inline Int32U5BU5D_t3230847821* get_mtfFreq_31() const { return ___mtfFreq_31; }
	inline Int32U5BU5D_t3230847821** get_address_of_mtfFreq_31() { return &___mtfFreq_31; }
	inline void set_mtfFreq_31(Int32U5BU5D_t3230847821* value)
	{
		___mtfFreq_31 = value;
		Il2CppCodeGenWriteBarrier(&___mtfFreq_31, value);
	}

	inline static int32_t get_offset_of_workFactor_32() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___workFactor_32)); }
	inline int32_t get_workFactor_32() const { return ___workFactor_32; }
	inline int32_t* get_address_of_workFactor_32() { return &___workFactor_32; }
	inline void set_workFactor_32(int32_t value)
	{
		___workFactor_32 = value;
	}

	inline static int32_t get_offset_of_workDone_33() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___workDone_33)); }
	inline int32_t get_workDone_33() const { return ___workDone_33; }
	inline int32_t* get_address_of_workDone_33() { return &___workDone_33; }
	inline void set_workDone_33(int32_t value)
	{
		___workDone_33 = value;
	}

	inline static int32_t get_offset_of_workLimit_34() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___workLimit_34)); }
	inline int32_t get_workLimit_34() const { return ___workLimit_34; }
	inline int32_t* get_address_of_workLimit_34() { return &___workLimit_34; }
	inline void set_workLimit_34(int32_t value)
	{
		___workLimit_34 = value;
	}

	inline static int32_t get_offset_of_firstAttempt_35() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___firstAttempt_35)); }
	inline bool get_firstAttempt_35() const { return ___firstAttempt_35; }
	inline bool* get_address_of_firstAttempt_35() { return &___firstAttempt_35; }
	inline void set_firstAttempt_35(bool value)
	{
		___firstAttempt_35 = value;
	}

	inline static int32_t get_offset_of_nBlocksRandomised_36() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___nBlocksRandomised_36)); }
	inline int32_t get_nBlocksRandomised_36() const { return ___nBlocksRandomised_36; }
	inline int32_t* get_address_of_nBlocksRandomised_36() { return &___nBlocksRandomised_36; }
	inline void set_nBlocksRandomised_36(int32_t value)
	{
		___nBlocksRandomised_36 = value;
	}

	inline static int32_t get_offset_of_currentChar_37() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___currentChar_37)); }
	inline int32_t get_currentChar_37() const { return ___currentChar_37; }
	inline int32_t* get_address_of_currentChar_37() { return &___currentChar_37; }
	inline void set_currentChar_37(int32_t value)
	{
		___currentChar_37 = value;
	}

	inline static int32_t get_offset_of_runLength_38() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___runLength_38)); }
	inline int32_t get_runLength_38() const { return ___runLength_38; }
	inline int32_t* get_address_of_runLength_38() { return &___runLength_38; }
	inline void set_runLength_38(int32_t value)
	{
		___runLength_38 = value;
	}

	inline static int32_t get_offset_of_blockCRC_39() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___blockCRC_39)); }
	inline uint32_t get_blockCRC_39() const { return ___blockCRC_39; }
	inline uint32_t* get_address_of_blockCRC_39() { return &___blockCRC_39; }
	inline void set_blockCRC_39(uint32_t value)
	{
		___blockCRC_39 = value;
	}

	inline static int32_t get_offset_of_combinedCRC_40() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___combinedCRC_40)); }
	inline uint32_t get_combinedCRC_40() const { return ___combinedCRC_40; }
	inline uint32_t* get_address_of_combinedCRC_40() { return &___combinedCRC_40; }
	inline void set_combinedCRC_40(uint32_t value)
	{
		___combinedCRC_40 = value;
	}

	inline static int32_t get_offset_of_allowableBlockSize_41() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___allowableBlockSize_41)); }
	inline int32_t get_allowableBlockSize_41() const { return ___allowableBlockSize_41; }
	inline int32_t* get_address_of_allowableBlockSize_41() { return &___allowableBlockSize_41; }
	inline void set_allowableBlockSize_41(int32_t value)
	{
		___allowableBlockSize_41 = value;
	}

	inline static int32_t get_offset_of_baseStream_42() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___baseStream_42)); }
	inline Stream_t1561764144 * get_baseStream_42() const { return ___baseStream_42; }
	inline Stream_t1561764144 ** get_address_of_baseStream_42() { return &___baseStream_42; }
	inline void set_baseStream_42(Stream_t1561764144 * value)
	{
		___baseStream_42 = value;
		Il2CppCodeGenWriteBarrier(&___baseStream_42, value);
	}

	inline static int32_t get_offset_of_disposed__43() { return static_cast<int32_t>(offsetof(BZip2OutputStream_t147045086, ___disposed__43)); }
	inline bool get_disposed__43() const { return ___disposed__43; }
	inline bool* get_address_of_disposed__43() { return &___disposed__43; }
	inline void set_disposed__43(bool value)
	{
		___disposed__43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
