﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.gui.CenterExitButton
struct CenterExitButton_t719705803;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.gui.CenterExitButton::.ctor()
extern "C"  void CenterExitButton__ctor_m3589206052 (CenterExitButton_t719705803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.CenterExitButton::Start()
extern "C"  void CenterExitButton_Start_m2536343844 (CenterExitButton_t719705803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.CenterExitButton::LateUpdate()
extern "C"  void CenterExitButton_LateUpdate_m4029337775 (CenterExitButton_t719705803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.gui.CenterExitButton::Update()
extern "C"  void CenterExitButton_Update_m1323100009 (CenterExitButton_t719705803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.gui.CenterExitButton::Delay()
extern "C"  Il2CppObject * CenterExitButton_Delay_m913377469 (CenterExitButton_t719705803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
