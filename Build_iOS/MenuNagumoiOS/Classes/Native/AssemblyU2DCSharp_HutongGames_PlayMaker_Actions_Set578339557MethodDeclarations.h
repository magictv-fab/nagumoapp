﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProperty
struct SetProperty_t578339557;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProperty::.ctor()
extern "C"  void SetProperty__ctor_m421173745 (SetProperty_t578339557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProperty::Reset()
extern "C"  void SetProperty_Reset_m2362573982 (SetProperty_t578339557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProperty::OnEnter()
extern "C"  void SetProperty_OnEnter_m412771720 (SetProperty_t578339557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProperty::OnUpdate()
extern "C"  void SetProperty_OnUpdate_m3339548091 (SetProperty_t578339557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
