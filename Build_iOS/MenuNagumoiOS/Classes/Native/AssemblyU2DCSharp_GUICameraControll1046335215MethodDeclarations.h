﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUICameraControll
struct GUICameraControll_t1046335215;

#include "codegen/il2cpp-codegen.h"

// System.Void GUICameraControll::.ctor()
extern "C"  void GUICameraControll__ctor_m1538145116 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::Start()
extern "C"  void GUICameraControll_Start_m485282908 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::Update()
extern "C"  void GUICameraControll_Update_m2164720433 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::ResetAll()
extern "C"  void GUICameraControll_ResetAll_m125111354 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::Stop()
extern "C"  void GUICameraControll_Stop_m3202655722 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::RotateLeft()
extern "C"  void GUICameraControll_RotateLeft_m2886303914 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::RotateRight()
extern "C"  void GUICameraControll_RotateRight_m426318587 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::ZoomIN()
extern "C"  void GUICameraControll_ZoomIN_m1726033632 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::ZoomOUT()
extern "C"  void GUICameraControll_ZoomOUT_m1973226997 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::MoveUP()
extern "C"  void GUICameraControll_MoveUP_m737711060 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::MoveDown()
extern "C"  void GUICameraControll_MoveDown_m4110046779 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::MoveLeft()
extern "C"  void GUICameraControll_MoveLeft_m34376800 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUICameraControll::MoveRight()
extern "C"  void GUICameraControll_MoveRight_m2210891269 (GUICameraControll_t1046335215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
