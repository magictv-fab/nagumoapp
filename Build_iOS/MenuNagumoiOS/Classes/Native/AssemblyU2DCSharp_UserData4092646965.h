﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CuponData[]
struct CuponDataU5BU5D_t2974711614;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserData
struct  UserData_t4092646965  : public Il2CppObject
{
public:
	// System.String UserData::nome
	String_t* ___nome_0;
	// System.String UserData::sobrenome
	String_t* ___sobrenome_1;
	// System.String UserData::cpf
	String_t* ___cpf_2;
	// System.String UserData::telefone
	String_t* ___telefone_3;
	// System.String UserData::email
	String_t* ___email_4;
	// System.String UserData::senha
	String_t* ___senha_5;
	// System.String UserData::foto
	String_t* ___foto_6;
	// System.String UserData::cep
	String_t* ___cep_7;
	// System.String UserData::rua
	String_t* ___rua_8;
	// System.String UserData::bairro
	String_t* ___bairro_9;
	// System.Int32 UserData::numero
	int32_t ___numero_10;
	// System.String UserData::complemento
	String_t* ___complemento_11;
	// System.String UserData::estado
	String_t* ___estado_12;
	// System.String UserData::cidade
	String_t* ___cidade_13;
	// CuponData[] UserData::cupons
	CuponDataU5BU5D_t2974711614* ___cupons_14;
	// System.Int32 UserData::id_participante
	int32_t ___id_participante_15;
	// System.Int32 UserData::id
	int32_t ___id_16;
	// System.Int32 UserData::spins
	int32_t ___spins_17;
	// System.Int32 UserData::idade
	int32_t ___idade_18;
	// System.Int32 UserData::sexo
	int32_t ___sexo_19;
	// System.Int32 UserData::pontos
	int32_t ___pontos_20;
	// System.Int32 UserData::pontos_disponiveis
	int32_t ___pontos_disponiveis_21;
	// System.Int32 UserData::spins_disponiveis
	int32_t ___spins_disponiveis_22;
	// System.Int32 UserData::cupons_disponiveis
	int32_t ___cupons_disponiveis_23;
	// System.Single UserData::saldo_acumulado
	float ___saldo_acumulado_24;
	// System.String UserData::dt_nasc
	String_t* ___dt_nasc_25;

public:
	inline static int32_t get_offset_of_nome_0() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___nome_0)); }
	inline String_t* get_nome_0() const { return ___nome_0; }
	inline String_t** get_address_of_nome_0() { return &___nome_0; }
	inline void set_nome_0(String_t* value)
	{
		___nome_0 = value;
		Il2CppCodeGenWriteBarrier(&___nome_0, value);
	}

	inline static int32_t get_offset_of_sobrenome_1() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___sobrenome_1)); }
	inline String_t* get_sobrenome_1() const { return ___sobrenome_1; }
	inline String_t** get_address_of_sobrenome_1() { return &___sobrenome_1; }
	inline void set_sobrenome_1(String_t* value)
	{
		___sobrenome_1 = value;
		Il2CppCodeGenWriteBarrier(&___sobrenome_1, value);
	}

	inline static int32_t get_offset_of_cpf_2() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___cpf_2)); }
	inline String_t* get_cpf_2() const { return ___cpf_2; }
	inline String_t** get_address_of_cpf_2() { return &___cpf_2; }
	inline void set_cpf_2(String_t* value)
	{
		___cpf_2 = value;
		Il2CppCodeGenWriteBarrier(&___cpf_2, value);
	}

	inline static int32_t get_offset_of_telefone_3() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___telefone_3)); }
	inline String_t* get_telefone_3() const { return ___telefone_3; }
	inline String_t** get_address_of_telefone_3() { return &___telefone_3; }
	inline void set_telefone_3(String_t* value)
	{
		___telefone_3 = value;
		Il2CppCodeGenWriteBarrier(&___telefone_3, value);
	}

	inline static int32_t get_offset_of_email_4() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___email_4)); }
	inline String_t* get_email_4() const { return ___email_4; }
	inline String_t** get_address_of_email_4() { return &___email_4; }
	inline void set_email_4(String_t* value)
	{
		___email_4 = value;
		Il2CppCodeGenWriteBarrier(&___email_4, value);
	}

	inline static int32_t get_offset_of_senha_5() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___senha_5)); }
	inline String_t* get_senha_5() const { return ___senha_5; }
	inline String_t** get_address_of_senha_5() { return &___senha_5; }
	inline void set_senha_5(String_t* value)
	{
		___senha_5 = value;
		Il2CppCodeGenWriteBarrier(&___senha_5, value);
	}

	inline static int32_t get_offset_of_foto_6() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___foto_6)); }
	inline String_t* get_foto_6() const { return ___foto_6; }
	inline String_t** get_address_of_foto_6() { return &___foto_6; }
	inline void set_foto_6(String_t* value)
	{
		___foto_6 = value;
		Il2CppCodeGenWriteBarrier(&___foto_6, value);
	}

	inline static int32_t get_offset_of_cep_7() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___cep_7)); }
	inline String_t* get_cep_7() const { return ___cep_7; }
	inline String_t** get_address_of_cep_7() { return &___cep_7; }
	inline void set_cep_7(String_t* value)
	{
		___cep_7 = value;
		Il2CppCodeGenWriteBarrier(&___cep_7, value);
	}

	inline static int32_t get_offset_of_rua_8() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___rua_8)); }
	inline String_t* get_rua_8() const { return ___rua_8; }
	inline String_t** get_address_of_rua_8() { return &___rua_8; }
	inline void set_rua_8(String_t* value)
	{
		___rua_8 = value;
		Il2CppCodeGenWriteBarrier(&___rua_8, value);
	}

	inline static int32_t get_offset_of_bairro_9() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___bairro_9)); }
	inline String_t* get_bairro_9() const { return ___bairro_9; }
	inline String_t** get_address_of_bairro_9() { return &___bairro_9; }
	inline void set_bairro_9(String_t* value)
	{
		___bairro_9 = value;
		Il2CppCodeGenWriteBarrier(&___bairro_9, value);
	}

	inline static int32_t get_offset_of_numero_10() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___numero_10)); }
	inline int32_t get_numero_10() const { return ___numero_10; }
	inline int32_t* get_address_of_numero_10() { return &___numero_10; }
	inline void set_numero_10(int32_t value)
	{
		___numero_10 = value;
	}

	inline static int32_t get_offset_of_complemento_11() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___complemento_11)); }
	inline String_t* get_complemento_11() const { return ___complemento_11; }
	inline String_t** get_address_of_complemento_11() { return &___complemento_11; }
	inline void set_complemento_11(String_t* value)
	{
		___complemento_11 = value;
		Il2CppCodeGenWriteBarrier(&___complemento_11, value);
	}

	inline static int32_t get_offset_of_estado_12() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___estado_12)); }
	inline String_t* get_estado_12() const { return ___estado_12; }
	inline String_t** get_address_of_estado_12() { return &___estado_12; }
	inline void set_estado_12(String_t* value)
	{
		___estado_12 = value;
		Il2CppCodeGenWriteBarrier(&___estado_12, value);
	}

	inline static int32_t get_offset_of_cidade_13() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___cidade_13)); }
	inline String_t* get_cidade_13() const { return ___cidade_13; }
	inline String_t** get_address_of_cidade_13() { return &___cidade_13; }
	inline void set_cidade_13(String_t* value)
	{
		___cidade_13 = value;
		Il2CppCodeGenWriteBarrier(&___cidade_13, value);
	}

	inline static int32_t get_offset_of_cupons_14() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___cupons_14)); }
	inline CuponDataU5BU5D_t2974711614* get_cupons_14() const { return ___cupons_14; }
	inline CuponDataU5BU5D_t2974711614** get_address_of_cupons_14() { return &___cupons_14; }
	inline void set_cupons_14(CuponDataU5BU5D_t2974711614* value)
	{
		___cupons_14 = value;
		Il2CppCodeGenWriteBarrier(&___cupons_14, value);
	}

	inline static int32_t get_offset_of_id_participante_15() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___id_participante_15)); }
	inline int32_t get_id_participante_15() const { return ___id_participante_15; }
	inline int32_t* get_address_of_id_participante_15() { return &___id_participante_15; }
	inline void set_id_participante_15(int32_t value)
	{
		___id_participante_15 = value;
	}

	inline static int32_t get_offset_of_id_16() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___id_16)); }
	inline int32_t get_id_16() const { return ___id_16; }
	inline int32_t* get_address_of_id_16() { return &___id_16; }
	inline void set_id_16(int32_t value)
	{
		___id_16 = value;
	}

	inline static int32_t get_offset_of_spins_17() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___spins_17)); }
	inline int32_t get_spins_17() const { return ___spins_17; }
	inline int32_t* get_address_of_spins_17() { return &___spins_17; }
	inline void set_spins_17(int32_t value)
	{
		___spins_17 = value;
	}

	inline static int32_t get_offset_of_idade_18() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___idade_18)); }
	inline int32_t get_idade_18() const { return ___idade_18; }
	inline int32_t* get_address_of_idade_18() { return &___idade_18; }
	inline void set_idade_18(int32_t value)
	{
		___idade_18 = value;
	}

	inline static int32_t get_offset_of_sexo_19() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___sexo_19)); }
	inline int32_t get_sexo_19() const { return ___sexo_19; }
	inline int32_t* get_address_of_sexo_19() { return &___sexo_19; }
	inline void set_sexo_19(int32_t value)
	{
		___sexo_19 = value;
	}

	inline static int32_t get_offset_of_pontos_20() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___pontos_20)); }
	inline int32_t get_pontos_20() const { return ___pontos_20; }
	inline int32_t* get_address_of_pontos_20() { return &___pontos_20; }
	inline void set_pontos_20(int32_t value)
	{
		___pontos_20 = value;
	}

	inline static int32_t get_offset_of_pontos_disponiveis_21() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___pontos_disponiveis_21)); }
	inline int32_t get_pontos_disponiveis_21() const { return ___pontos_disponiveis_21; }
	inline int32_t* get_address_of_pontos_disponiveis_21() { return &___pontos_disponiveis_21; }
	inline void set_pontos_disponiveis_21(int32_t value)
	{
		___pontos_disponiveis_21 = value;
	}

	inline static int32_t get_offset_of_spins_disponiveis_22() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___spins_disponiveis_22)); }
	inline int32_t get_spins_disponiveis_22() const { return ___spins_disponiveis_22; }
	inline int32_t* get_address_of_spins_disponiveis_22() { return &___spins_disponiveis_22; }
	inline void set_spins_disponiveis_22(int32_t value)
	{
		___spins_disponiveis_22 = value;
	}

	inline static int32_t get_offset_of_cupons_disponiveis_23() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___cupons_disponiveis_23)); }
	inline int32_t get_cupons_disponiveis_23() const { return ___cupons_disponiveis_23; }
	inline int32_t* get_address_of_cupons_disponiveis_23() { return &___cupons_disponiveis_23; }
	inline void set_cupons_disponiveis_23(int32_t value)
	{
		___cupons_disponiveis_23 = value;
	}

	inline static int32_t get_offset_of_saldo_acumulado_24() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___saldo_acumulado_24)); }
	inline float get_saldo_acumulado_24() const { return ___saldo_acumulado_24; }
	inline float* get_address_of_saldo_acumulado_24() { return &___saldo_acumulado_24; }
	inline void set_saldo_acumulado_24(float value)
	{
		___saldo_acumulado_24 = value;
	}

	inline static int32_t get_offset_of_dt_nasc_25() { return static_cast<int32_t>(offsetof(UserData_t4092646965, ___dt_nasc_25)); }
	inline String_t* get_dt_nasc_25() const { return ___dt_nasc_25; }
	inline String_t** get_address_of_dt_nasc_25() { return &___dt_nasc_25; }
	inline void set_dt_nasc_25(String_t* value)
	{
		___dt_nasc_25 = value;
		Il2CppCodeGenWriteBarrier(&___dt_nasc_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
