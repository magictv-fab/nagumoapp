﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_PDF417_Internal_DetectionResultColumn3195057574.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn
struct  DetectionResultRowIndicatorColumn_t1625390043  : public DetectionResultColumn_t3195057574
{
public:
	// System.Boolean ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::<IsLeft>k__BackingField
	bool ___U3CIsLeftU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsLeftU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DetectionResultRowIndicatorColumn_t1625390043, ___U3CIsLeftU3Ek__BackingField_2)); }
	inline bool get_U3CIsLeftU3Ek__BackingField_2() const { return ___U3CIsLeftU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsLeftU3Ek__BackingField_2() { return &___U3CIsLeftU3Ek__BackingField_2; }
	inline void set_U3CIsLeftU3Ek__BackingField_2(bool value)
	{
		___U3CIsLeftU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
