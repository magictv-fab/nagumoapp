﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.ProgressEventsAbstract
struct ProgressEventsAbstract_t2129719228;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.processes.UpdateProcessComponent
struct  UpdateProcessComponent_t50846353  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// ARM.abstracts.ProgressEventsAbstract MagicTV.processes.UpdateProcessComponent::updateProcess
	ProgressEventsAbstract_t2129719228 * ___updateProcess_8;

public:
	inline static int32_t get_offset_of_updateProcess_8() { return static_cast<int32_t>(offsetof(UpdateProcessComponent_t50846353, ___updateProcess_8)); }
	inline ProgressEventsAbstract_t2129719228 * get_updateProcess_8() const { return ___updateProcess_8; }
	inline ProgressEventsAbstract_t2129719228 ** get_address_of_updateProcess_8() { return &___updateProcess_8; }
	inline void set_updateProcess_8(ProgressEventsAbstract_t2129719228 * value)
	{
		___updateProcess_8 = value;
		Il2CppCodeGenWriteBarrier(&___updateProcess_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
