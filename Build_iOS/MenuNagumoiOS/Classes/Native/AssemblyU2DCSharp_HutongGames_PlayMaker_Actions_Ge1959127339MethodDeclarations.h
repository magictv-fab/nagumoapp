﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMaterial
struct GetMaterial_t1959127339;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMaterial::.ctor()
extern "C"  void GetMaterial__ctor_m3226280555 (GetMaterial_t1959127339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::Reset()
extern "C"  void GetMaterial_Reset_m872713496 (GetMaterial_t1959127339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::OnEnter()
extern "C"  void GetMaterial_OnEnter_m3175921538 (GetMaterial_t1959127339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::DoGetMaterial()
extern "C"  void GetMaterial_DoGetMaterial_m3903327259 (GetMaterial_t1959127339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
