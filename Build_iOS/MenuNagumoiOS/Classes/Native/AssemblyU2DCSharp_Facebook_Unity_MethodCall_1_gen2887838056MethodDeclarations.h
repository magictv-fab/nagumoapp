﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.MethodCall`1<System.Object>
struct MethodCall_1_t2887838056;
// Facebook.Unity.FacebookBase
struct FacebookBase_t850267831;
// System.String
struct String_t;
// Facebook.Unity.FacebookDelegate`1<System.Object>
struct FacebookDelegate_1_t2257890132;
// Facebook.Unity.MethodArguments
struct MethodArguments_t3236074899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase850267831.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodArguments3236074899.h"

// System.Void Facebook.Unity.MethodCall`1<System.Object>::.ctor(Facebook.Unity.FacebookBase,System.String)
extern "C"  void MethodCall_1__ctor_m1960000006_gshared (MethodCall_1_t2887838056 * __this, FacebookBase_t850267831 * ___facebookImpl0, String_t* ___methodName1, const MethodInfo* method);
#define MethodCall_1__ctor_m1960000006(__this, ___facebookImpl0, ___methodName1, method) ((  void (*) (MethodCall_1_t2887838056 *, FacebookBase_t850267831 *, String_t*, const MethodInfo*))MethodCall_1__ctor_m1960000006_gshared)(__this, ___facebookImpl0, ___methodName1, method)
// System.String Facebook.Unity.MethodCall`1<System.Object>::get_MethodName()
extern "C"  String_t* MethodCall_1_get_MethodName_m759377580_gshared (MethodCall_1_t2887838056 * __this, const MethodInfo* method);
#define MethodCall_1_get_MethodName_m759377580(__this, method) ((  String_t* (*) (MethodCall_1_t2887838056 *, const MethodInfo*))MethodCall_1_get_MethodName_m759377580_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_MethodName(System.String)
extern "C"  void MethodCall_1_set_MethodName_m817744037_gshared (MethodCall_1_t2887838056 * __this, String_t* ___value0, const MethodInfo* method);
#define MethodCall_1_set_MethodName_m817744037(__this, ___value0, method) ((  void (*) (MethodCall_1_t2887838056 *, String_t*, const MethodInfo*))MethodCall_1_set_MethodName_m817744037_gshared)(__this, ___value0, method)
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<System.Object>::get_Callback()
extern "C"  FacebookDelegate_1_t2257890132 * MethodCall_1_get_Callback_m708582070_gshared (MethodCall_1_t2887838056 * __this, const MethodInfo* method);
#define MethodCall_1_get_Callback_m708582070(__this, method) ((  FacebookDelegate_1_t2257890132 * (*) (MethodCall_1_t2887838056 *, const MethodInfo*))MethodCall_1_get_Callback_m708582070_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
extern "C"  void MethodCall_1_set_Callback_m933548397_gshared (MethodCall_1_t2887838056 * __this, FacebookDelegate_1_t2257890132 * ___value0, const MethodInfo* method);
#define MethodCall_1_set_Callback_m933548397(__this, ___value0, method) ((  void (*) (MethodCall_1_t2887838056 *, FacebookDelegate_1_t2257890132 *, const MethodInfo*))MethodCall_1_set_Callback_m933548397_gshared)(__this, ___value0, method)
// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<System.Object>::get_FacebookImpl()
extern "C"  FacebookBase_t850267831 * MethodCall_1_get_FacebookImpl_m1272944382_gshared (MethodCall_1_t2887838056 * __this, const MethodInfo* method);
#define MethodCall_1_get_FacebookImpl_m1272944382(__this, method) ((  FacebookBase_t850267831 * (*) (MethodCall_1_t2887838056 *, const MethodInfo*))MethodCall_1_get_FacebookImpl_m1272944382_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_FacebookImpl(Facebook.Unity.FacebookBase)
extern "C"  void MethodCall_1_set_FacebookImpl_m2871452611_gshared (MethodCall_1_t2887838056 * __this, FacebookBase_t850267831 * ___value0, const MethodInfo* method);
#define MethodCall_1_set_FacebookImpl_m2871452611(__this, ___value0, method) ((  void (*) (MethodCall_1_t2887838056 *, FacebookBase_t850267831 *, const MethodInfo*))MethodCall_1_set_FacebookImpl_m2871452611_gshared)(__this, ___value0, method)
// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<System.Object>::get_Parameters()
extern "C"  MethodArguments_t3236074899 * MethodCall_1_get_Parameters_m2809693360_gshared (MethodCall_1_t2887838056 * __this, const MethodInfo* method);
#define MethodCall_1_get_Parameters_m2809693360(__this, method) ((  MethodArguments_t3236074899 * (*) (MethodCall_1_t2887838056 *, const MethodInfo*))MethodCall_1_get_Parameters_m2809693360_gshared)(__this, method)
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Parameters(Facebook.Unity.MethodArguments)
extern "C"  void MethodCall_1_set_Parameters_m2033621031_gshared (MethodCall_1_t2887838056 * __this, MethodArguments_t3236074899 * ___value0, const MethodInfo* method);
#define MethodCall_1_set_Parameters_m2033621031(__this, ___value0, method) ((  void (*) (MethodCall_1_t2887838056 *, MethodArguments_t3236074899 *, const MethodInfo*))MethodCall_1_set_Parameters_m2033621031_gshared)(__this, ___value0, method)
