﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// System.Object
struct Il2CppObject;
// Cadastro
struct Cadastro_t3948724313;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cadastro/<ISendJson>c__Iterator4A
struct  U3CISendJsonU3Ec__Iterator4A_t2077337939  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Cadastro/<ISendJson>c__Iterator4A::<headers>__0
	Dictionary_2_t827649927 * ___U3CheadersU3E__0_0;
	// System.String Cadastro/<ISendJson>c__Iterator4A::json
	String_t* ___json_1;
	// System.Byte[] Cadastro/<ISendJson>c__Iterator4A::<pData>__1
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__1_2;
	// UnityEngine.WWW Cadastro/<ISendJson>c__Iterator4A::<www>__2
	WWW_t3134621005 * ___U3CwwwU3E__2_3;
	// UnityEngine.Texture2D Cadastro/<ISendJson>c__Iterator4A::<localFile>__3
	Texture2D_t3884108195 * ___U3ClocalFileU3E__3_4;
	// UnityEngine.WWWForm Cadastro/<ISendJson>c__Iterator4A::<postForm>__4
	WWWForm_t461342257 * ___U3CpostFormU3E__4_5;
	// System.String Cadastro/<ISendJson>c__Iterator4A::<uploadURL>__5
	String_t* ___U3CuploadURLU3E__5_6;
	// UnityEngine.WWW Cadastro/<ISendJson>c__Iterator4A::<upload>__6
	WWW_t3134621005 * ___U3CuploadU3E__6_7;
	// System.Int32 Cadastro/<ISendJson>c__Iterator4A::$PC
	int32_t ___U24PC_8;
	// System.Object Cadastro/<ISendJson>c__Iterator4A::$current
	Il2CppObject * ___U24current_9;
	// System.String Cadastro/<ISendJson>c__Iterator4A::<$>json
	String_t* ___U3CU24U3Ejson_10;
	// Cadastro Cadastro/<ISendJson>c__Iterator4A::<>f__this
	Cadastro_t3948724313 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CheadersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CheadersU3E__0_0)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__0_0() const { return ___U3CheadersU3E__0_0; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__0_0() { return &___U3CheadersU3E__0_0; }
	inline void set_U3CheadersU3E__0_0(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__0_0, value);
	}

	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___json_1)); }
	inline String_t* get_json_1() const { return ___json_1; }
	inline String_t** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(String_t* value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier(&___json_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__1_2() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CpDataU3E__1_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__1_2() const { return ___U3CpDataU3E__1_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__1_2() { return &___U3CpDataU3E__1_2; }
	inline void set_U3CpDataU3E__1_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_3() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CwwwU3E__2_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__2_3() const { return ___U3CwwwU3E__2_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__2_3() { return &___U3CwwwU3E__2_3; }
	inline void set_U3CwwwU3E__2_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3ClocalFileU3E__3_4() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3ClocalFileU3E__3_4)); }
	inline Texture2D_t3884108195 * get_U3ClocalFileU3E__3_4() const { return ___U3ClocalFileU3E__3_4; }
	inline Texture2D_t3884108195 ** get_address_of_U3ClocalFileU3E__3_4() { return &___U3ClocalFileU3E__3_4; }
	inline void set_U3ClocalFileU3E__3_4(Texture2D_t3884108195 * value)
	{
		___U3ClocalFileU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalFileU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CpostFormU3E__4_5() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CpostFormU3E__4_5)); }
	inline WWWForm_t461342257 * get_U3CpostFormU3E__4_5() const { return ___U3CpostFormU3E__4_5; }
	inline WWWForm_t461342257 ** get_address_of_U3CpostFormU3E__4_5() { return &___U3CpostFormU3E__4_5; }
	inline void set_U3CpostFormU3E__4_5(WWWForm_t461342257 * value)
	{
		___U3CpostFormU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpostFormU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U3CuploadURLU3E__5_6() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CuploadURLU3E__5_6)); }
	inline String_t* get_U3CuploadURLU3E__5_6() const { return ___U3CuploadURLU3E__5_6; }
	inline String_t** get_address_of_U3CuploadURLU3E__5_6() { return &___U3CuploadURLU3E__5_6; }
	inline void set_U3CuploadURLU3E__5_6(String_t* value)
	{
		___U3CuploadURLU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuploadURLU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CuploadU3E__6_7() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CuploadU3E__6_7)); }
	inline WWW_t3134621005 * get_U3CuploadU3E__6_7() const { return ___U3CuploadU3E__6_7; }
	inline WWW_t3134621005 ** get_address_of_U3CuploadU3E__6_7() { return &___U3CuploadU3E__6_7; }
	inline void set_U3CuploadU3E__6_7(WWW_t3134621005 * value)
	{
		___U3CuploadU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuploadU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ejson_10() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CU24U3Ejson_10)); }
	inline String_t* get_U3CU24U3Ejson_10() const { return ___U3CU24U3Ejson_10; }
	inline String_t** get_address_of_U3CU24U3Ejson_10() { return &___U3CU24U3Ejson_10; }
	inline void set_U3CU24U3Ejson_10(String_t* value)
	{
		___U3CU24U3Ejson_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ejson_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CISendJsonU3Ec__Iterator4A_t2077337939, ___U3CU3Ef__this_11)); }
	inline Cadastro_t3948724313 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline Cadastro_t3948724313 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(Cadastro_t3948724313 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
