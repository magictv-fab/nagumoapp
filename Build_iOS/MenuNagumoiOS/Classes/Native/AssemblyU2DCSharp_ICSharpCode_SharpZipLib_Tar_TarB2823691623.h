﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarBuffer
struct  TarBuffer_t2823691623  : public Il2CppObject
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarBuffer::inputStream
	Stream_t1561764144 * ___inputStream_3;
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarBuffer::outputStream
	Stream_t1561764144 * ___outputStream_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarBuffer::recordBuffer
	ByteU5BU5D_t4260760469* ___recordBuffer_5;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::currentBlockIndex
	int32_t ___currentBlockIndex_6;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::currentRecordIndex
	int32_t ___currentRecordIndex_7;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::recordSize
	int32_t ___recordSize_8;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarBuffer::blockFactor
	int32_t ___blockFactor_9;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarBuffer::isStreamOwner_
	bool ___isStreamOwner__10;

public:
	inline static int32_t get_offset_of_inputStream_3() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___inputStream_3)); }
	inline Stream_t1561764144 * get_inputStream_3() const { return ___inputStream_3; }
	inline Stream_t1561764144 ** get_address_of_inputStream_3() { return &___inputStream_3; }
	inline void set_inputStream_3(Stream_t1561764144 * value)
	{
		___inputStream_3 = value;
		Il2CppCodeGenWriteBarrier(&___inputStream_3, value);
	}

	inline static int32_t get_offset_of_outputStream_4() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___outputStream_4)); }
	inline Stream_t1561764144 * get_outputStream_4() const { return ___outputStream_4; }
	inline Stream_t1561764144 ** get_address_of_outputStream_4() { return &___outputStream_4; }
	inline void set_outputStream_4(Stream_t1561764144 * value)
	{
		___outputStream_4 = value;
		Il2CppCodeGenWriteBarrier(&___outputStream_4, value);
	}

	inline static int32_t get_offset_of_recordBuffer_5() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___recordBuffer_5)); }
	inline ByteU5BU5D_t4260760469* get_recordBuffer_5() const { return ___recordBuffer_5; }
	inline ByteU5BU5D_t4260760469** get_address_of_recordBuffer_5() { return &___recordBuffer_5; }
	inline void set_recordBuffer_5(ByteU5BU5D_t4260760469* value)
	{
		___recordBuffer_5 = value;
		Il2CppCodeGenWriteBarrier(&___recordBuffer_5, value);
	}

	inline static int32_t get_offset_of_currentBlockIndex_6() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___currentBlockIndex_6)); }
	inline int32_t get_currentBlockIndex_6() const { return ___currentBlockIndex_6; }
	inline int32_t* get_address_of_currentBlockIndex_6() { return &___currentBlockIndex_6; }
	inline void set_currentBlockIndex_6(int32_t value)
	{
		___currentBlockIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentRecordIndex_7() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___currentRecordIndex_7)); }
	inline int32_t get_currentRecordIndex_7() const { return ___currentRecordIndex_7; }
	inline int32_t* get_address_of_currentRecordIndex_7() { return &___currentRecordIndex_7; }
	inline void set_currentRecordIndex_7(int32_t value)
	{
		___currentRecordIndex_7 = value;
	}

	inline static int32_t get_offset_of_recordSize_8() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___recordSize_8)); }
	inline int32_t get_recordSize_8() const { return ___recordSize_8; }
	inline int32_t* get_address_of_recordSize_8() { return &___recordSize_8; }
	inline void set_recordSize_8(int32_t value)
	{
		___recordSize_8 = value;
	}

	inline static int32_t get_offset_of_blockFactor_9() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___blockFactor_9)); }
	inline int32_t get_blockFactor_9() const { return ___blockFactor_9; }
	inline int32_t* get_address_of_blockFactor_9() { return &___blockFactor_9; }
	inline void set_blockFactor_9(int32_t value)
	{
		___blockFactor_9 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner__10() { return static_cast<int32_t>(offsetof(TarBuffer_t2823691623, ___isStreamOwner__10)); }
	inline bool get_isStreamOwner__10() const { return ___isStreamOwner__10; }
	inline bool* get_address_of_isStreamOwner__10() { return &___isStreamOwner__10; }
	inline void set_isStreamOwner__10(bool value)
	{
		___isStreamOwner__10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
