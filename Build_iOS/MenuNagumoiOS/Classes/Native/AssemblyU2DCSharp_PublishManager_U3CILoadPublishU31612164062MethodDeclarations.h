﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager/<ILoadPublish>c__Iterator77
struct U3CILoadPublishU3Ec__Iterator77_t1612164062;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishManager/<ILoadPublish>c__Iterator77::.ctor()
extern "C"  void U3CILoadPublishU3Ec__Iterator77__ctor_m3486385789 (U3CILoadPublishU3Ec__Iterator77_t1612164062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ILoadPublish>c__Iterator77::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishU3Ec__Iterator77_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2882042175 (U3CILoadPublishU3Ec__Iterator77_t1612164062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ILoadPublish>c__Iterator77::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishU3Ec__Iterator77_System_Collections_IEnumerator_get_Current_m2512050899 (U3CILoadPublishU3Ec__Iterator77_t1612164062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishManager/<ILoadPublish>c__Iterator77::MoveNext()
extern "C"  bool U3CILoadPublishU3Ec__Iterator77_MoveNext_m953491775 (U3CILoadPublishU3Ec__Iterator77_t1612164062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ILoadPublish>c__Iterator77::Dispose()
extern "C"  void U3CILoadPublishU3Ec__Iterator77_Dispose_m240535418 (U3CILoadPublishU3Ec__Iterator77_t1612164062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ILoadPublish>c__Iterator77::Reset()
extern "C"  void U3CILoadPublishU3Ec__Iterator77_Reset_m1132818730 (U3CILoadPublishU3Ec__Iterator77_t1612164062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
