﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImagemScroll
struct ImagemScroll_t3817704447;

#include "codegen/il2cpp-codegen.h"

// System.Void ImagemScroll::.ctor()
extern "C"  void ImagemScroll__ctor_m2288940412 (ImagemScroll_t3817704447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImagemScroll::Start()
extern "C"  void ImagemScroll_Start_m1236078204 (ImagemScroll_t3817704447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImagemScroll::Update()
extern "C"  void ImagemScroll_Update_m3964538129 (ImagemScroll_t3817704447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImagemScroll::OnSelect(System.Int32)
extern "C"  void ImagemScroll_OnSelect_m3152268948 (ImagemScroll_t3817704447 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImagemScroll::HideAll()
extern "C"  void ImagemScroll_HideAll_m1121622425 (ImagemScroll_t3817704447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
