﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetChild
struct GetChild_t1697862670;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.Actions.GetChild::.ctor()
extern "C"  void GetChild__ctor_m1259984408 (GetChild_t1697862670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChild::Reset()
extern "C"  void GetChild_Reset_m3201384645 (GetChild_t1697862670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChild::OnEnter()
extern "C"  void GetChild_OnEnter_m3350934511 (GetChild_t1697862670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetChild::DoGetChildByName(UnityEngine.GameObject,System.String,System.String)
extern "C"  GameObject_t3674682005 * GetChild_DoGetChildByName_m4062192452 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___root0, String_t* ___name1, String_t* ___tag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.GetChild::ErrorCheck()
extern "C"  String_t* GetChild_ErrorCheck_m4013168239 (GetChild_t1697862670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
