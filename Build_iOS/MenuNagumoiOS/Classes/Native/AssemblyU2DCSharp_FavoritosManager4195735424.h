﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PublicationsData
struct PublicationsData_t1837633585;
// FavoritosManager
struct FavoritosManager_t4195735424;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FavoritosManager
struct  FavoritosManager_t4195735424  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject FavoritosManager::loadingObj
	GameObject_t3674682005 * ___loadingObj_9;
	// UnityEngine.GameObject FavoritosManager::popup
	GameObject_t3674682005 * ___popup_10;
	// UnityEngine.GameObject FavoritosManager::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_11;
	// UnityEngine.GameObject FavoritosManager::containerItens
	GameObject_t3674682005 * ___containerItens_12;
	// UnityEngine.UI.ScrollRect FavoritosManager::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_14;
	// System.Int32 FavoritosManager::ofertaCounter
	int32_t ___ofertaCounter_15;
	// UnityEngine.GameObject FavoritosManager::currentItem
	GameObject_t3674682005 * ___currentItem_16;
	// UnityEngine.GameObject FavoritosManager::selectedItem
	GameObject_t3674682005 * ___selectedItem_17;

public:
	inline static int32_t get_offset_of_loadingObj_9() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___loadingObj_9)); }
	inline GameObject_t3674682005 * get_loadingObj_9() const { return ___loadingObj_9; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_9() { return &___loadingObj_9; }
	inline void set_loadingObj_9(GameObject_t3674682005 * value)
	{
		___loadingObj_9 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_9, value);
	}

	inline static int32_t get_offset_of_popup_10() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___popup_10)); }
	inline GameObject_t3674682005 * get_popup_10() const { return ___popup_10; }
	inline GameObject_t3674682005 ** get_address_of_popup_10() { return &___popup_10; }
	inline void set_popup_10(GameObject_t3674682005 * value)
	{
		___popup_10 = value;
		Il2CppCodeGenWriteBarrier(&___popup_10, value);
	}

	inline static int32_t get_offset_of_itemPrefab_11() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___itemPrefab_11)); }
	inline GameObject_t3674682005 * get_itemPrefab_11() const { return ___itemPrefab_11; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_11() { return &___itemPrefab_11; }
	inline void set_itemPrefab_11(GameObject_t3674682005 * value)
	{
		___itemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_11, value);
	}

	inline static int32_t get_offset_of_containerItens_12() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___containerItens_12)); }
	inline GameObject_t3674682005 * get_containerItens_12() const { return ___containerItens_12; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_12() { return &___containerItens_12; }
	inline void set_containerItens_12(GameObject_t3674682005 * value)
	{
		___containerItens_12 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_12, value);
	}

	inline static int32_t get_offset_of_scrollRect_14() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___scrollRect_14)); }
	inline ScrollRect_t3606982749 * get_scrollRect_14() const { return ___scrollRect_14; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_14() { return &___scrollRect_14; }
	inline void set_scrollRect_14(ScrollRect_t3606982749 * value)
	{
		___scrollRect_14 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_14, value);
	}

	inline static int32_t get_offset_of_ofertaCounter_15() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___ofertaCounter_15)); }
	inline int32_t get_ofertaCounter_15() const { return ___ofertaCounter_15; }
	inline int32_t* get_address_of_ofertaCounter_15() { return &___ofertaCounter_15; }
	inline void set_ofertaCounter_15(int32_t value)
	{
		___ofertaCounter_15 = value;
	}

	inline static int32_t get_offset_of_currentItem_16() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___currentItem_16)); }
	inline GameObject_t3674682005 * get_currentItem_16() const { return ___currentItem_16; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_16() { return &___currentItem_16; }
	inline void set_currentItem_16(GameObject_t3674682005 * value)
	{
		___currentItem_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_16, value);
	}

	inline static int32_t get_offset_of_selectedItem_17() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424, ___selectedItem_17)); }
	inline GameObject_t3674682005 * get_selectedItem_17() const { return ___selectedItem_17; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_17() { return &___selectedItem_17; }
	inline void set_selectedItem_17(GameObject_t3674682005 * value)
	{
		___selectedItem_17 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_17, value);
	}
};

struct FavoritosManager_t4195735424_StaticFields
{
public:
	// PublicationsData FavoritosManager::publicationsData
	PublicationsData_t1837633585 * ___publicationsData_2;
	// FavoritosManager FavoritosManager::instance
	FavoritosManager_t4195735424 * ___instance_3;
	// System.Collections.Generic.List`1<System.String> FavoritosManager::titleLst
	List_1_t1375417109 * ___titleLst_4;
	// System.Collections.Generic.List`1<System.String> FavoritosManager::valueLst
	List_1_t1375417109 * ___valueLst_5;
	// System.Collections.Generic.List`1<System.String> FavoritosManager::infoLst
	List_1_t1375417109 * ___infoLst_6;
	// System.Collections.Generic.List`1<System.String> FavoritosManager::idLst
	List_1_t1375417109 * ___idLst_7;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> FavoritosManager::imgLst
	List_1_t272385497 * ___imgLst_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> FavoritosManager::itensList
	List_1_t747900261 * ___itensList_13;
	// System.String FavoritosManager::currentJsonTxt
	String_t* ___currentJsonTxt_18;

public:
	inline static int32_t get_offset_of_publicationsData_2() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___publicationsData_2)); }
	inline PublicationsData_t1837633585 * get_publicationsData_2() const { return ___publicationsData_2; }
	inline PublicationsData_t1837633585 ** get_address_of_publicationsData_2() { return &___publicationsData_2; }
	inline void set_publicationsData_2(PublicationsData_t1837633585 * value)
	{
		___publicationsData_2 = value;
		Il2CppCodeGenWriteBarrier(&___publicationsData_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___instance_3)); }
	inline FavoritosManager_t4195735424 * get_instance_3() const { return ___instance_3; }
	inline FavoritosManager_t4195735424 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(FavoritosManager_t4195735424 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of_titleLst_4() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___titleLst_4)); }
	inline List_1_t1375417109 * get_titleLst_4() const { return ___titleLst_4; }
	inline List_1_t1375417109 ** get_address_of_titleLst_4() { return &___titleLst_4; }
	inline void set_titleLst_4(List_1_t1375417109 * value)
	{
		___titleLst_4 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_4, value);
	}

	inline static int32_t get_offset_of_valueLst_5() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___valueLst_5)); }
	inline List_1_t1375417109 * get_valueLst_5() const { return ___valueLst_5; }
	inline List_1_t1375417109 ** get_address_of_valueLst_5() { return &___valueLst_5; }
	inline void set_valueLst_5(List_1_t1375417109 * value)
	{
		___valueLst_5 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_5, value);
	}

	inline static int32_t get_offset_of_infoLst_6() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___infoLst_6)); }
	inline List_1_t1375417109 * get_infoLst_6() const { return ___infoLst_6; }
	inline List_1_t1375417109 ** get_address_of_infoLst_6() { return &___infoLst_6; }
	inline void set_infoLst_6(List_1_t1375417109 * value)
	{
		___infoLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_6, value);
	}

	inline static int32_t get_offset_of_idLst_7() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___idLst_7)); }
	inline List_1_t1375417109 * get_idLst_7() const { return ___idLst_7; }
	inline List_1_t1375417109 ** get_address_of_idLst_7() { return &___idLst_7; }
	inline void set_idLst_7(List_1_t1375417109 * value)
	{
		___idLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_7, value);
	}

	inline static int32_t get_offset_of_imgLst_8() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___imgLst_8)); }
	inline List_1_t272385497 * get_imgLst_8() const { return ___imgLst_8; }
	inline List_1_t272385497 ** get_address_of_imgLst_8() { return &___imgLst_8; }
	inline void set_imgLst_8(List_1_t272385497 * value)
	{
		___imgLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_8, value);
	}

	inline static int32_t get_offset_of_itensList_13() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___itensList_13)); }
	inline List_1_t747900261 * get_itensList_13() const { return ___itensList_13; }
	inline List_1_t747900261 ** get_address_of_itensList_13() { return &___itensList_13; }
	inline void set_itensList_13(List_1_t747900261 * value)
	{
		___itensList_13 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_13, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_18() { return static_cast<int32_t>(offsetof(FavoritosManager_t4195735424_StaticFields, ___currentJsonTxt_18)); }
	inline String_t* get_currentJsonTxt_18() const { return ___currentJsonTxt_18; }
	inline String_t** get_address_of_currentJsonTxt_18() { return &___currentJsonTxt_18; }
	inline void set_currentJsonTxt_18(String_t* value)
	{
		___currentJsonTxt_18 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
