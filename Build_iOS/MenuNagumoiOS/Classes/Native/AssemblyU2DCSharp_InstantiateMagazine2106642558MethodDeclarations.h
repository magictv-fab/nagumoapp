﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InstantiateMagazine
struct InstantiateMagazine_t2106642558;

#include "codegen/il2cpp-codegen.h"

// System.Void InstantiateMagazine::.ctor()
extern "C"  void InstantiateMagazine__ctor_m1318908077 (InstantiateMagazine_t2106642558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstantiateMagazine::OnDisable()
extern "C"  void InstantiateMagazine_OnDisable_m3996264084 (InstantiateMagazine_t2106642558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstantiateMagazine::OnEnable()
extern "C"  void InstantiateMagazine_OnEnable_m3301276601 (InstantiateMagazine_t2106642558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstantiateMagazine::Update()
extern "C"  void InstantiateMagazine_Update_m3958306816 (InstantiateMagazine_t2106642558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstantiateMagazine::Back()
extern "C"  void InstantiateMagazine_Back_m58579486 (InstantiateMagazine_t2106642558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
