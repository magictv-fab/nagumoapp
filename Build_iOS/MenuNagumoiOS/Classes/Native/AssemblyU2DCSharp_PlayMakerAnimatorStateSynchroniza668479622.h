﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.Animator
struct Animator_t2776330603;
// System.Collections.Generic.Dictionary`2<System.Int32,HutongGames.PlayMaker.FsmState>
struct Dictionary_2_t2143597306;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerAnimatorStateSynchronization
struct  PlayMakerAnimatorStateSynchronization_t668479622  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 PlayMakerAnimatorStateSynchronization::LayerIndex
	int32_t ___LayerIndex_2;
	// PlayMakerFSM PlayMakerAnimatorStateSynchronization::Fsm
	PlayMakerFSM_t3799847376 * ___Fsm_3;
	// System.Boolean PlayMakerAnimatorStateSynchronization::EveryFrame
	bool ___EveryFrame_4;
	// System.Boolean PlayMakerAnimatorStateSynchronization::debug
	bool ___debug_5;
	// UnityEngine.Animator PlayMakerAnimatorStateSynchronization::animator
	Animator_t2776330603 * ___animator_6;
	// System.Int32 PlayMakerAnimatorStateSynchronization::lastState
	int32_t ___lastState_7;
	// System.Int32 PlayMakerAnimatorStateSynchronization::lastTransition
	int32_t ___lastTransition_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,HutongGames.PlayMaker.FsmState> PlayMakerAnimatorStateSynchronization::fsmStateLUT
	Dictionary_2_t2143597306 * ___fsmStateLUT_9;

public:
	inline static int32_t get_offset_of_LayerIndex_2() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___LayerIndex_2)); }
	inline int32_t get_LayerIndex_2() const { return ___LayerIndex_2; }
	inline int32_t* get_address_of_LayerIndex_2() { return &___LayerIndex_2; }
	inline void set_LayerIndex_2(int32_t value)
	{
		___LayerIndex_2 = value;
	}

	inline static int32_t get_offset_of_Fsm_3() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___Fsm_3)); }
	inline PlayMakerFSM_t3799847376 * get_Fsm_3() const { return ___Fsm_3; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_Fsm_3() { return &___Fsm_3; }
	inline void set_Fsm_3(PlayMakerFSM_t3799847376 * value)
	{
		___Fsm_3 = value;
		Il2CppCodeGenWriteBarrier(&___Fsm_3, value);
	}

	inline static int32_t get_offset_of_EveryFrame_4() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___EveryFrame_4)); }
	inline bool get_EveryFrame_4() const { return ___EveryFrame_4; }
	inline bool* get_address_of_EveryFrame_4() { return &___EveryFrame_4; }
	inline void set_EveryFrame_4(bool value)
	{
		___EveryFrame_4 = value;
	}

	inline static int32_t get_offset_of_debug_5() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___debug_5)); }
	inline bool get_debug_5() const { return ___debug_5; }
	inline bool* get_address_of_debug_5() { return &___debug_5; }
	inline void set_debug_5(bool value)
	{
		___debug_5 = value;
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___animator_6)); }
	inline Animator_t2776330603 * get_animator_6() const { return ___animator_6; }
	inline Animator_t2776330603 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t2776330603 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier(&___animator_6, value);
	}

	inline static int32_t get_offset_of_lastState_7() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___lastState_7)); }
	inline int32_t get_lastState_7() const { return ___lastState_7; }
	inline int32_t* get_address_of_lastState_7() { return &___lastState_7; }
	inline void set_lastState_7(int32_t value)
	{
		___lastState_7 = value;
	}

	inline static int32_t get_offset_of_lastTransition_8() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___lastTransition_8)); }
	inline int32_t get_lastTransition_8() const { return ___lastTransition_8; }
	inline int32_t* get_address_of_lastTransition_8() { return &___lastTransition_8; }
	inline void set_lastTransition_8(int32_t value)
	{
		___lastTransition_8 = value;
	}

	inline static int32_t get_offset_of_fsmStateLUT_9() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorStateSynchronization_t668479622, ___fsmStateLUT_9)); }
	inline Dictionary_2_t2143597306 * get_fsmStateLUT_9() const { return ___fsmStateLUT_9; }
	inline Dictionary_2_t2143597306 ** get_address_of_fsmStateLUT_9() { return &___fsmStateLUT_9; }
	inline void set_fsmStateLUT_9(Dictionary_2_t2143597306 * value)
	{
		___fsmStateLUT_9 = value;
		Il2CppCodeGenWriteBarrier(&___fsmStateLUT_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
