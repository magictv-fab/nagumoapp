﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.OneD.RSS.RSSUtils::getRSSvalue(System.Int32[],System.Int32,System.Boolean)
extern "C"  int32_t RSSUtils_getRSSvalue_m2227331968 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___widths0, int32_t ___maxWidth1, bool ___noNarrow2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.RSSUtils::combins(System.Int32,System.Int32)
extern "C"  int32_t RSSUtils_combins_m3439803075 (Il2CppObject * __this /* static, unused */, int32_t ___n0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
