﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t4252399424;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat3810185450.h"

// System.Void System.Collections.Generic.Stack`1<System.Int32>::.ctor()
extern "C"  void Stack_1__ctor_m200529406_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1__ctor_m200529406(__this, method) ((  void (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1__ctor_m200529406_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Int32>::.ctor(System.Int32)
extern "C"  void Stack_1__ctor_m2250790608_gshared (Stack_1_t4252399424 * __this, int32_t ___count0, const MethodInfo* method);
#define Stack_1__ctor_m2250790608(__this, ___count0, method) ((  void (*) (Stack_1_t4252399424 *, int32_t, const MethodInfo*))Stack_1__ctor_m2250790608_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m1543008397_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m1543008397(__this, method) ((  bool (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1543008397_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m2338971085_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m2338971085(__this, method) ((  Il2CppObject * (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2338971085_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m542334501_gshared (Stack_1_t4252399424 * __this, Il2CppArray * ___dest0, int32_t ___idx1, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m542334501(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t4252399424 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m542334501_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2124675985_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2124675985(__this, method) ((  Il2CppObject* (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2124675985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Stack_1_System_Collections_IEnumerable_GetEnumerator_m802547700_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m802547700(__this, method) ((  Il2CppObject * (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m802547700_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Clear()
extern "C"  void Stack_1_Clear_m1901629993_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_Clear_m1901629993(__this, method) ((  void (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_Clear_m1901629993_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Int32>::Contains(T)
extern "C"  bool Stack_1_Contains_m1927584580_gshared (Stack_1_t4252399424 * __this, int32_t ___t0, const MethodInfo* method);
#define Stack_1_Contains_m1927584580(__this, ___t0, method) ((  bool (*) (Stack_1_t4252399424 *, int32_t, const MethodInfo*))Stack_1_Contains_m1927584580_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void Stack_1_CopyTo_m3885816976_gshared (Stack_1_t4252399424 * __this, Int32U5BU5D_t3230847821* ___dest0, int32_t ___idx1, const MethodInfo* method);
#define Stack_1_CopyTo_m3885816976(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t4252399424 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))Stack_1_CopyTo_m3885816976_gshared)(__this, ___dest0, ___idx1, method)
// T System.Collections.Generic.Stack`1<System.Int32>::Peek()
extern "C"  int32_t Stack_1_Peek_m4260936271_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_Peek_m4260936271(__this, method) ((  int32_t (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_Peek_m4260936271_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Int32>::Pop()
extern "C"  int32_t Stack_1_Pop_m2493060607_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_Pop_m2493060607(__this, method) ((  int32_t (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_Pop_m2493060607_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Push(T)
extern "C"  void Stack_1_Push_m4100313105_gshared (Stack_1_t4252399424 * __this, int32_t ___t0, const MethodInfo* method);
#define Stack_1_Push_m4100313105(__this, ___t0, method) ((  void (*) (Stack_1_t4252399424 *, int32_t, const MethodInfo*))Stack_1_Push_m4100313105_gshared)(__this, ___t0, method)
// T[] System.Collections.Generic.Stack`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t3230847821* Stack_1_ToArray_m1322992510_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_ToArray_m1322992510(__this, method) ((  Int32U5BU5D_t3230847821* (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_ToArray_m1322992510_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Int32>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1860890195_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m1860890195(__this, method) ((  int32_t (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_get_Count_m1860890195_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3810185450  Stack_1_GetEnumerator_m135367413_gshared (Stack_1_t4252399424 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m135367413(__this, method) ((  Enumerator_t3810185450  (*) (Stack_1_t4252399424 *, const MethodInfo*))Stack_1_GetEnumerator_m135367413_gshared)(__this, method)
