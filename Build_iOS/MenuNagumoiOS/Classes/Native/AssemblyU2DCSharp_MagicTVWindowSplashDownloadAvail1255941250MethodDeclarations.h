﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowSplashDownloadAvailableControllerScript
struct MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowSplashDownloadAvailableControllerScript::.ctor()
extern "C"  void MagicTVWindowSplashDownloadAvailableControllerScript__ctor_m896649305 (MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadAvailableControllerScript::ConfirmDownloadClick()
extern "C"  void MagicTVWindowSplashDownloadAvailableControllerScript_ConfirmDownloadClick_m3573073099 (MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadAvailableControllerScript::CancelDownLoadClick()
extern "C"  void MagicTVWindowSplashDownloadAvailableControllerScript_CancelDownLoadClick_m356351709 (MagicTVWindowSplashDownloadAvailableControllerScript_t1255941250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
