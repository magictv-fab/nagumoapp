﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowInAppListControllerScriptWaiting
struct  MagicTVWindowInAppListControllerScriptWaiting_t3050884011  : public GenericWindowControllerScript_t248075822
{
public:
	// UnityEngine.UI.Image MagicTVWindowInAppListControllerScriptWaiting::splashImage
	Image_t538875265 * ___splashImage_4;
	// System.Action MagicTVWindowInAppListControllerScriptWaiting::onClickStart
	Action_t3771233898 * ___onClickStart_5;

public:
	inline static int32_t get_offset_of_splashImage_4() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppListControllerScriptWaiting_t3050884011, ___splashImage_4)); }
	inline Image_t538875265 * get_splashImage_4() const { return ___splashImage_4; }
	inline Image_t538875265 ** get_address_of_splashImage_4() { return &___splashImage_4; }
	inline void set_splashImage_4(Image_t538875265 * value)
	{
		___splashImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___splashImage_4, value);
	}

	inline static int32_t get_offset_of_onClickStart_5() { return static_cast<int32_t>(offsetof(MagicTVWindowInAppListControllerScriptWaiting_t3050884011, ___onClickStart_5)); }
	inline Action_t3771233898 * get_onClickStart_5() const { return ___onClickStart_5; }
	inline Action_t3771233898 ** get_address_of_onClickStart_5() { return &___onClickStart_5; }
	inline void set_onClickStart_5(Action_t3771233898 * value)
	{
		___onClickStart_5 = value;
		Il2CppCodeGenWriteBarrier(&___onClickStart_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
