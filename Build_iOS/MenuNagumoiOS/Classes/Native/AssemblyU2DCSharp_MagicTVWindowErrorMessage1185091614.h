﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowErrorMessage
struct  MagicTVWindowErrorMessage_t1185091614  : public GenericWindowControllerScript_t248075822
{
public:
	// UnityEngine.UI.Text MagicTVWindowErrorMessage::_ButtonLabel
	Text_t9039225 * ____ButtonLabel_4;
	// UnityEngine.UI.Text MagicTVWindowErrorMessage::_ErrorMessagePort
	Text_t9039225 * ____ErrorMessagePort_5;
	// UnityEngine.UI.Text MagicTVWindowErrorMessage::_ErrorMessageLand
	Text_t9039225 * ____ErrorMessageLand_6;
	// System.Action MagicTVWindowErrorMessage::onClickConfig
	Action_t3771233898 * ___onClickConfig_7;

public:
	inline static int32_t get_offset_of__ButtonLabel_4() { return static_cast<int32_t>(offsetof(MagicTVWindowErrorMessage_t1185091614, ____ButtonLabel_4)); }
	inline Text_t9039225 * get__ButtonLabel_4() const { return ____ButtonLabel_4; }
	inline Text_t9039225 ** get_address_of__ButtonLabel_4() { return &____ButtonLabel_4; }
	inline void set__ButtonLabel_4(Text_t9039225 * value)
	{
		____ButtonLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&____ButtonLabel_4, value);
	}

	inline static int32_t get_offset_of__ErrorMessagePort_5() { return static_cast<int32_t>(offsetof(MagicTVWindowErrorMessage_t1185091614, ____ErrorMessagePort_5)); }
	inline Text_t9039225 * get__ErrorMessagePort_5() const { return ____ErrorMessagePort_5; }
	inline Text_t9039225 ** get_address_of__ErrorMessagePort_5() { return &____ErrorMessagePort_5; }
	inline void set__ErrorMessagePort_5(Text_t9039225 * value)
	{
		____ErrorMessagePort_5 = value;
		Il2CppCodeGenWriteBarrier(&____ErrorMessagePort_5, value);
	}

	inline static int32_t get_offset_of__ErrorMessageLand_6() { return static_cast<int32_t>(offsetof(MagicTVWindowErrorMessage_t1185091614, ____ErrorMessageLand_6)); }
	inline Text_t9039225 * get__ErrorMessageLand_6() const { return ____ErrorMessageLand_6; }
	inline Text_t9039225 ** get_address_of__ErrorMessageLand_6() { return &____ErrorMessageLand_6; }
	inline void set__ErrorMessageLand_6(Text_t9039225 * value)
	{
		____ErrorMessageLand_6 = value;
		Il2CppCodeGenWriteBarrier(&____ErrorMessageLand_6, value);
	}

	inline static int32_t get_offset_of_onClickConfig_7() { return static_cast<int32_t>(offsetof(MagicTVWindowErrorMessage_t1185091614, ___onClickConfig_7)); }
	inline Action_t3771233898 * get_onClickConfig_7() const { return ___onClickConfig_7; }
	inline Action_t3771233898 ** get_address_of_onClickConfig_7() { return &___onClickConfig_7; }
	inline void set_onClickConfig_7(Action_t3771233898 * value)
	{
		___onClickConfig_7 = value;
		Il2CppCodeGenWriteBarrier(&___onClickConfig_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
