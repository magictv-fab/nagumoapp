﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GerarCupom
struct GerarCupom_t1397758263;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void GerarCupom::.ctor()
extern "C"  void GerarCupom__ctor_m2857299268 (GerarCupom_t1397758263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GerarCupom::Start()
extern "C"  void GerarCupom_Start_m1804437060 (GerarCupom_t1397758263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GerarCupom::Update()
extern "C"  void GerarCupom_Update_m108826185 (GerarCupom_t1397758263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GerarCupom::ButtonGerar()
extern "C"  void GerarCupom_ButtonGerar_m3968328533 (GerarCupom_t1397758263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GerarCupom::IGerar()
extern "C"  Il2CppObject * GerarCupom_IGerar_m1435596452 (GerarCupom_t1397758263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
