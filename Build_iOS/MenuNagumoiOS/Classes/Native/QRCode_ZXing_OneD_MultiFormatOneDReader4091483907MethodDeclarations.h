﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.MultiFormatOneDReader
struct MultiFormatOneDReader_t4091483907;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.MultiFormatOneDReader::.ctor(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  void MultiFormatOneDReader__ctor_m3660296119 (MultiFormatOneDReader_t4091483907 * __this, Il2CppObject* ___hints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.MultiFormatOneDReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * MultiFormatOneDReader_decodeRow_m3631098609 (MultiFormatOneDReader_t4091483907 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.MultiFormatOneDReader::reset()
extern "C"  void MultiFormatOneDReader_reset_m2545866173 (MultiFormatOneDReader_t4091483907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
