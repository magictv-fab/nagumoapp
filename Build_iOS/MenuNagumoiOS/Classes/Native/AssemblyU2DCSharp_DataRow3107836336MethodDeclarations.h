﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataRow
struct DataRow_t3107836336;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void DataRow::.ctor()
extern "C"  void DataRow__ctor_m4116988859 (DataRow_t3107836336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DataRow::get_Item(System.String)
extern "C"  Il2CppObject * DataRow_get_Item_m1742201426 (DataRow_t3107836336 * __this, String_t* ___column0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataRow::set_Item(System.String,System.Object)
extern "C"  void DataRow_set_Item_m829536183 (DataRow_t3107836336 * __this, String_t* ___column0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
