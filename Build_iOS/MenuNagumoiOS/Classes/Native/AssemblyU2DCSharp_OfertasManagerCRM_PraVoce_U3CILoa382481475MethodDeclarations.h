﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD
struct U3CILoadOfertasU3Ec__IteratorD_t382481475;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::.ctor()
extern "C"  void U3CILoadOfertasU3Ec__IteratorD__ctor_m1350623672 (U3CILoadOfertasU3Ec__IteratorD_t382481475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944459428 (U3CILoadOfertasU3Ec__IteratorD_t382481475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3113687608 (U3CILoadOfertasU3Ec__IteratorD_t382481475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::MoveNext()
extern "C"  bool U3CILoadOfertasU3Ec__IteratorD_MoveNext_m2196645668 (U3CILoadOfertasU3Ec__IteratorD_t382481475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::Dispose()
extern "C"  void U3CILoadOfertasU3Ec__IteratorD_Dispose_m767508469 (U3CILoadOfertasU3Ec__IteratorD_t382481475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::Reset()
extern "C"  void U3CILoadOfertasU3Ec__IteratorD_Reset_m3292023909 (U3CILoadOfertasU3Ec__IteratorD_t382481475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
