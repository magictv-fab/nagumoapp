﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler
struct KeysRequiredEventHandler_t3775663731;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t3672810022;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.ArrayList
struct ArrayList_t3948406897;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// ICSharpCode.SharpZipLib.Zip.IArchiveStorage
struct IArchiveStorage_t345670484;
// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource
struct IDynamicDataSource_t1093195849;
// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString
struct ZipString_t3473946306;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory
struct IEntryFactory_t131367667;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile
struct  ZipFile_t2937401711  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler ICSharpCode.SharpZipLib.Zip.ZipFile::KeysRequired
	KeysRequiredEventHandler_t3775663731 * ___KeysRequired_1;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isDisposed_
	bool ___isDisposed__2;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::name_
	String_t* ___name__3;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::comment_
	String_t* ___comment__4;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::rawPassword_
	String_t* ___rawPassword__5;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::baseStream_
	Stream_t1561764144 * ___baseStream__6;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isStreamOwner
	bool ___isStreamOwner_7;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::offsetOfFirstEntry
	int64_t ___offsetOfFirstEntry_8;
	// ICSharpCode.SharpZipLib.Zip.ZipEntry[] ICSharpCode.SharpZipLib.Zip.ZipFile::entries_
	ZipEntryU5BU5D_t3672810022* ___entries__9;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile::key
	ByteU5BU5D_t4260760469* ___key_10;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isNewArchive_
	bool ___isNewArchive__11;
	// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.ZipFile::useZip64_
	int32_t ___useZip64__12;
	// System.Collections.ArrayList ICSharpCode.SharpZipLib.Zip.ZipFile::updates_
	ArrayList_t3948406897 * ___updates__13;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::updateCount_
	int64_t ___updateCount__14;
	// System.Collections.Hashtable ICSharpCode.SharpZipLib.Zip.ZipFile::updateIndex_
	Hashtable_t1407064410 * ___updateIndex__15;
	// ICSharpCode.SharpZipLib.Zip.IArchiveStorage ICSharpCode.SharpZipLib.Zip.ZipFile::archiveStorage_
	Il2CppObject * ___archiveStorage__16;
	// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource ICSharpCode.SharpZipLib.Zip.ZipFile::updateDataSource_
	Il2CppObject * ___updateDataSource__17;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::contentsEdited_
	bool ___contentsEdited__18;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::bufferSize_
	int32_t ___bufferSize__19;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile::copyBuffer_
	ByteU5BU5D_t4260760469* ___copyBuffer__20;
	// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString ICSharpCode.SharpZipLib.Zip.ZipFile::newComment_
	ZipString_t3473946306 * ___newComment__21;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::commentEdited_
	bool ___commentEdited__22;
	// ICSharpCode.SharpZipLib.Zip.IEntryFactory ICSharpCode.SharpZipLib.Zip.ZipFile::updateEntryFactory_
	Il2CppObject * ___updateEntryFactory__23;

public:
	inline static int32_t get_offset_of_KeysRequired_1() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___KeysRequired_1)); }
	inline KeysRequiredEventHandler_t3775663731 * get_KeysRequired_1() const { return ___KeysRequired_1; }
	inline KeysRequiredEventHandler_t3775663731 ** get_address_of_KeysRequired_1() { return &___KeysRequired_1; }
	inline void set_KeysRequired_1(KeysRequiredEventHandler_t3775663731 * value)
	{
		___KeysRequired_1 = value;
		Il2CppCodeGenWriteBarrier(&___KeysRequired_1, value);
	}

	inline static int32_t get_offset_of_isDisposed__2() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___isDisposed__2)); }
	inline bool get_isDisposed__2() const { return ___isDisposed__2; }
	inline bool* get_address_of_isDisposed__2() { return &___isDisposed__2; }
	inline void set_isDisposed__2(bool value)
	{
		___isDisposed__2 = value;
	}

	inline static int32_t get_offset_of_name__3() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___name__3)); }
	inline String_t* get_name__3() const { return ___name__3; }
	inline String_t** get_address_of_name__3() { return &___name__3; }
	inline void set_name__3(String_t* value)
	{
		___name__3 = value;
		Il2CppCodeGenWriteBarrier(&___name__3, value);
	}

	inline static int32_t get_offset_of_comment__4() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___comment__4)); }
	inline String_t* get_comment__4() const { return ___comment__4; }
	inline String_t** get_address_of_comment__4() { return &___comment__4; }
	inline void set_comment__4(String_t* value)
	{
		___comment__4 = value;
		Il2CppCodeGenWriteBarrier(&___comment__4, value);
	}

	inline static int32_t get_offset_of_rawPassword__5() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___rawPassword__5)); }
	inline String_t* get_rawPassword__5() const { return ___rawPassword__5; }
	inline String_t** get_address_of_rawPassword__5() { return &___rawPassword__5; }
	inline void set_rawPassword__5(String_t* value)
	{
		___rawPassword__5 = value;
		Il2CppCodeGenWriteBarrier(&___rawPassword__5, value);
	}

	inline static int32_t get_offset_of_baseStream__6() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___baseStream__6)); }
	inline Stream_t1561764144 * get_baseStream__6() const { return ___baseStream__6; }
	inline Stream_t1561764144 ** get_address_of_baseStream__6() { return &___baseStream__6; }
	inline void set_baseStream__6(Stream_t1561764144 * value)
	{
		___baseStream__6 = value;
		Il2CppCodeGenWriteBarrier(&___baseStream__6, value);
	}

	inline static int32_t get_offset_of_isStreamOwner_7() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___isStreamOwner_7)); }
	inline bool get_isStreamOwner_7() const { return ___isStreamOwner_7; }
	inline bool* get_address_of_isStreamOwner_7() { return &___isStreamOwner_7; }
	inline void set_isStreamOwner_7(bool value)
	{
		___isStreamOwner_7 = value;
	}

	inline static int32_t get_offset_of_offsetOfFirstEntry_8() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___offsetOfFirstEntry_8)); }
	inline int64_t get_offsetOfFirstEntry_8() const { return ___offsetOfFirstEntry_8; }
	inline int64_t* get_address_of_offsetOfFirstEntry_8() { return &___offsetOfFirstEntry_8; }
	inline void set_offsetOfFirstEntry_8(int64_t value)
	{
		___offsetOfFirstEntry_8 = value;
	}

	inline static int32_t get_offset_of_entries__9() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___entries__9)); }
	inline ZipEntryU5BU5D_t3672810022* get_entries__9() const { return ___entries__9; }
	inline ZipEntryU5BU5D_t3672810022** get_address_of_entries__9() { return &___entries__9; }
	inline void set_entries__9(ZipEntryU5BU5D_t3672810022* value)
	{
		___entries__9 = value;
		Il2CppCodeGenWriteBarrier(&___entries__9, value);
	}

	inline static int32_t get_offset_of_key_10() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___key_10)); }
	inline ByteU5BU5D_t4260760469* get_key_10() const { return ___key_10; }
	inline ByteU5BU5D_t4260760469** get_address_of_key_10() { return &___key_10; }
	inline void set_key_10(ByteU5BU5D_t4260760469* value)
	{
		___key_10 = value;
		Il2CppCodeGenWriteBarrier(&___key_10, value);
	}

	inline static int32_t get_offset_of_isNewArchive__11() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___isNewArchive__11)); }
	inline bool get_isNewArchive__11() const { return ___isNewArchive__11; }
	inline bool* get_address_of_isNewArchive__11() { return &___isNewArchive__11; }
	inline void set_isNewArchive__11(bool value)
	{
		___isNewArchive__11 = value;
	}

	inline static int32_t get_offset_of_useZip64__12() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___useZip64__12)); }
	inline int32_t get_useZip64__12() const { return ___useZip64__12; }
	inline int32_t* get_address_of_useZip64__12() { return &___useZip64__12; }
	inline void set_useZip64__12(int32_t value)
	{
		___useZip64__12 = value;
	}

	inline static int32_t get_offset_of_updates__13() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___updates__13)); }
	inline ArrayList_t3948406897 * get_updates__13() const { return ___updates__13; }
	inline ArrayList_t3948406897 ** get_address_of_updates__13() { return &___updates__13; }
	inline void set_updates__13(ArrayList_t3948406897 * value)
	{
		___updates__13 = value;
		Il2CppCodeGenWriteBarrier(&___updates__13, value);
	}

	inline static int32_t get_offset_of_updateCount__14() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___updateCount__14)); }
	inline int64_t get_updateCount__14() const { return ___updateCount__14; }
	inline int64_t* get_address_of_updateCount__14() { return &___updateCount__14; }
	inline void set_updateCount__14(int64_t value)
	{
		___updateCount__14 = value;
	}

	inline static int32_t get_offset_of_updateIndex__15() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___updateIndex__15)); }
	inline Hashtable_t1407064410 * get_updateIndex__15() const { return ___updateIndex__15; }
	inline Hashtable_t1407064410 ** get_address_of_updateIndex__15() { return &___updateIndex__15; }
	inline void set_updateIndex__15(Hashtable_t1407064410 * value)
	{
		___updateIndex__15 = value;
		Il2CppCodeGenWriteBarrier(&___updateIndex__15, value);
	}

	inline static int32_t get_offset_of_archiveStorage__16() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___archiveStorage__16)); }
	inline Il2CppObject * get_archiveStorage__16() const { return ___archiveStorage__16; }
	inline Il2CppObject ** get_address_of_archiveStorage__16() { return &___archiveStorage__16; }
	inline void set_archiveStorage__16(Il2CppObject * value)
	{
		___archiveStorage__16 = value;
		Il2CppCodeGenWriteBarrier(&___archiveStorage__16, value);
	}

	inline static int32_t get_offset_of_updateDataSource__17() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___updateDataSource__17)); }
	inline Il2CppObject * get_updateDataSource__17() const { return ___updateDataSource__17; }
	inline Il2CppObject ** get_address_of_updateDataSource__17() { return &___updateDataSource__17; }
	inline void set_updateDataSource__17(Il2CppObject * value)
	{
		___updateDataSource__17 = value;
		Il2CppCodeGenWriteBarrier(&___updateDataSource__17, value);
	}

	inline static int32_t get_offset_of_contentsEdited__18() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___contentsEdited__18)); }
	inline bool get_contentsEdited__18() const { return ___contentsEdited__18; }
	inline bool* get_address_of_contentsEdited__18() { return &___contentsEdited__18; }
	inline void set_contentsEdited__18(bool value)
	{
		___contentsEdited__18 = value;
	}

	inline static int32_t get_offset_of_bufferSize__19() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___bufferSize__19)); }
	inline int32_t get_bufferSize__19() const { return ___bufferSize__19; }
	inline int32_t* get_address_of_bufferSize__19() { return &___bufferSize__19; }
	inline void set_bufferSize__19(int32_t value)
	{
		___bufferSize__19 = value;
	}

	inline static int32_t get_offset_of_copyBuffer__20() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___copyBuffer__20)); }
	inline ByteU5BU5D_t4260760469* get_copyBuffer__20() const { return ___copyBuffer__20; }
	inline ByteU5BU5D_t4260760469** get_address_of_copyBuffer__20() { return &___copyBuffer__20; }
	inline void set_copyBuffer__20(ByteU5BU5D_t4260760469* value)
	{
		___copyBuffer__20 = value;
		Il2CppCodeGenWriteBarrier(&___copyBuffer__20, value);
	}

	inline static int32_t get_offset_of_newComment__21() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___newComment__21)); }
	inline ZipString_t3473946306 * get_newComment__21() const { return ___newComment__21; }
	inline ZipString_t3473946306 ** get_address_of_newComment__21() { return &___newComment__21; }
	inline void set_newComment__21(ZipString_t3473946306 * value)
	{
		___newComment__21 = value;
		Il2CppCodeGenWriteBarrier(&___newComment__21, value);
	}

	inline static int32_t get_offset_of_commentEdited__22() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___commentEdited__22)); }
	inline bool get_commentEdited__22() const { return ___commentEdited__22; }
	inline bool* get_address_of_commentEdited__22() { return &___commentEdited__22; }
	inline void set_commentEdited__22(bool value)
	{
		___commentEdited__22 = value;
	}

	inline static int32_t get_offset_of_updateEntryFactory__23() { return static_cast<int32_t>(offsetof(ZipFile_t2937401711, ___updateEntryFactory__23)); }
	inline Il2CppObject * get_updateEntryFactory__23() const { return ___updateEntryFactory__23; }
	inline Il2CppObject ** get_address_of_updateEntryFactory__23() { return &___updateEntryFactory__23; }
	inline void set_updateEntryFactory__23(Il2CppObject * value)
	{
		___updateEntryFactory__23 = value;
		Il2CppCodeGenWriteBarrier(&___updateEntryFactory__23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
