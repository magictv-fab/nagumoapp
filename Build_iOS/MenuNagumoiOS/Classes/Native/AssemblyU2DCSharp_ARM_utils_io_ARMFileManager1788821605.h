﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.IO.DirectoryInfo,System.String>
struct Func_2_t1469842285;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.io.ARMFileManager
struct  ARMFileManager_t1788821605  : public MonoBehaviour_t667441552
{
public:

public:
};

struct ARMFileManager_t1788821605_StaticFields
{
public:
	// System.Func`2<System.IO.DirectoryInfo,System.String> ARM.utils.io.ARMFileManager::<>f__am$cache0
	Func_2_t1469842285 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(ARMFileManager_t1788821605_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t1469842285 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t1469842285 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t1469842285 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
