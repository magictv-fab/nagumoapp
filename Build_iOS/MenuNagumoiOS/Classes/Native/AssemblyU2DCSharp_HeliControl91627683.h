﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeliControl
struct  HeliControl_t91627683  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean HeliControl::down
	bool ___down_2;

public:
	inline static int32_t get_offset_of_down_2() { return static_cast<int32_t>(offsetof(HeliControl_t91627683, ___down_2)); }
	inline bool get_down_2() const { return ___down_2; }
	inline bool* get_address_of_down_2() { return &___down_2; }
	inline void set_down_2(bool value)
	{
		___down_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
