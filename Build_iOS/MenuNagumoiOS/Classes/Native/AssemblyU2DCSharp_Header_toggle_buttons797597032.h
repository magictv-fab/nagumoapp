﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Header_toggle_buttons
struct  Header_toggle_buttons_t797597032  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] Header_toggle_buttons::toggles
	GameObjectU5BU5D_t2662109048* ___toggles_2;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap Header_toggle_buttons::horizontalScrollSnapHEADER
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnapHEADER_3;

public:
	inline static int32_t get_offset_of_toggles_2() { return static_cast<int32_t>(offsetof(Header_toggle_buttons_t797597032, ___toggles_2)); }
	inline GameObjectU5BU5D_t2662109048* get_toggles_2() const { return ___toggles_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_toggles_2() { return &___toggles_2; }
	inline void set_toggles_2(GameObjectU5BU5D_t2662109048* value)
	{
		___toggles_2 = value;
		Il2CppCodeGenWriteBarrier(&___toggles_2, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnapHEADER_3() { return static_cast<int32_t>(offsetof(Header_toggle_buttons_t797597032, ___horizontalScrollSnapHEADER_3)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnapHEADER_3() const { return ___horizontalScrollSnapHEADER_3; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnapHEADER_3() { return &___horizontalScrollSnapHEADER_3; }
	inline void set_horizontalScrollSnapHEADER_3(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnapHEADER_3 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnapHEADER_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
