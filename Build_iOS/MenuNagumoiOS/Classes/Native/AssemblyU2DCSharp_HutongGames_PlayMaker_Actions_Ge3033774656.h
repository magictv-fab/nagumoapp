﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIKGoal
struct  GetAnimatorIKGoal_t3033774656  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// UnityEngine.AvatarIKGoal HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::iKGoal
	int32_t ___iKGoal_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::goal
	FsmGameObject_t1697147867 * ___goal_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::position
	FsmVector3_t533912882 * ___position_13;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::rotation
	FsmQuaternion_t3871136040 * ___rotation_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::positionWeight
	FsmFloat_t2134102846 * ___positionWeight_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::rotationWeight
	FsmFloat_t2134102846 * ___rotationWeight_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::_animator
	Animator_t2776330603 * ____animator_17;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::_transform
	Transform_t1659122786 * ____transform_18;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_iKGoal_10() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___iKGoal_10)); }
	inline int32_t get_iKGoal_10() const { return ___iKGoal_10; }
	inline int32_t* get_address_of_iKGoal_10() { return &___iKGoal_10; }
	inline void set_iKGoal_10(int32_t value)
	{
		___iKGoal_10 = value;
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_goal_12() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___goal_12)); }
	inline FsmGameObject_t1697147867 * get_goal_12() const { return ___goal_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_goal_12() { return &___goal_12; }
	inline void set_goal_12(FsmGameObject_t1697147867 * value)
	{
		___goal_12 = value;
		Il2CppCodeGenWriteBarrier(&___goal_12, value);
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___position_13)); }
	inline FsmVector3_t533912882 * get_position_13() const { return ___position_13; }
	inline FsmVector3_t533912882 ** get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(FsmVector3_t533912882 * value)
	{
		___position_13 = value;
		Il2CppCodeGenWriteBarrier(&___position_13, value);
	}

	inline static int32_t get_offset_of_rotation_14() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___rotation_14)); }
	inline FsmQuaternion_t3871136040 * get_rotation_14() const { return ___rotation_14; }
	inline FsmQuaternion_t3871136040 ** get_address_of_rotation_14() { return &___rotation_14; }
	inline void set_rotation_14(FsmQuaternion_t3871136040 * value)
	{
		___rotation_14 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_14, value);
	}

	inline static int32_t get_offset_of_positionWeight_15() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___positionWeight_15)); }
	inline FsmFloat_t2134102846 * get_positionWeight_15() const { return ___positionWeight_15; }
	inline FsmFloat_t2134102846 ** get_address_of_positionWeight_15() { return &___positionWeight_15; }
	inline void set_positionWeight_15(FsmFloat_t2134102846 * value)
	{
		___positionWeight_15 = value;
		Il2CppCodeGenWriteBarrier(&___positionWeight_15, value);
	}

	inline static int32_t get_offset_of_rotationWeight_16() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ___rotationWeight_16)); }
	inline FsmFloat_t2134102846 * get_rotationWeight_16() const { return ___rotationWeight_16; }
	inline FsmFloat_t2134102846 ** get_address_of_rotationWeight_16() { return &___rotationWeight_16; }
	inline void set_rotationWeight_16(FsmFloat_t2134102846 * value)
	{
		___rotationWeight_16 = value;
		Il2CppCodeGenWriteBarrier(&___rotationWeight_16, value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ____animator_17)); }
	inline Animator_t2776330603 * get__animator_17() const { return ____animator_17; }
	inline Animator_t2776330603 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t2776330603 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier(&____animator_17, value);
	}

	inline static int32_t get_offset_of__transform_18() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t3033774656, ____transform_18)); }
	inline Transform_t1659122786 * get__transform_18() const { return ____transform_18; }
	inline Transform_t1659122786 ** get_address_of__transform_18() { return &____transform_18; }
	inline void set__transform_18(Transform_t1659122786 * value)
	{
		____transform_18 = value;
		Il2CppCodeGenWriteBarrier(&____transform_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
