﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.InvokeMethod
struct InvokeMethod_t2919758241;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::.ctor()
extern "C"  void InvokeMethod__ctor_m1586428389 (InvokeMethod_t2919758241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::Reset()
extern "C"  void InvokeMethod_Reset_m3527828626 (InvokeMethod_t2919758241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::OnEnter()
extern "C"  void InvokeMethod_OnEnter_m3530987644 (InvokeMethod_t2919758241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::DoInvokeMethod(UnityEngine.GameObject)
extern "C"  void InvokeMethod_DoInvokeMethod_m3833673499 (InvokeMethod_t2919758241 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::OnExit()
extern "C"  void InvokeMethod_OnExit_m3447958428 (InvokeMethod_t2919758241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
