﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BuildString
struct  BuildString_t1836011181  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.BuildString::stringParts
	FsmStringU5BU5D_t2523845914* ___stringParts_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BuildString::separator
	FsmString_t952858651 * ___separator_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BuildString::storeResult
	FsmString_t952858651 * ___storeResult_11;
	// System.Boolean HutongGames.PlayMaker.Actions.BuildString::everyFrame
	bool ___everyFrame_12;
	// System.String HutongGames.PlayMaker.Actions.BuildString::result
	String_t* ___result_13;

public:
	inline static int32_t get_offset_of_stringParts_9() { return static_cast<int32_t>(offsetof(BuildString_t1836011181, ___stringParts_9)); }
	inline FsmStringU5BU5D_t2523845914* get_stringParts_9() const { return ___stringParts_9; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_stringParts_9() { return &___stringParts_9; }
	inline void set_stringParts_9(FsmStringU5BU5D_t2523845914* value)
	{
		___stringParts_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringParts_9, value);
	}

	inline static int32_t get_offset_of_separator_10() { return static_cast<int32_t>(offsetof(BuildString_t1836011181, ___separator_10)); }
	inline FsmString_t952858651 * get_separator_10() const { return ___separator_10; }
	inline FsmString_t952858651 ** get_address_of_separator_10() { return &___separator_10; }
	inline void set_separator_10(FsmString_t952858651 * value)
	{
		___separator_10 = value;
		Il2CppCodeGenWriteBarrier(&___separator_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(BuildString_t1836011181, ___storeResult_11)); }
	inline FsmString_t952858651 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmString_t952858651 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmString_t952858651 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(BuildString_t1836011181, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of_result_13() { return static_cast<int32_t>(offsetof(BuildString_t1836011181, ___result_13)); }
	inline String_t* get_result_13() const { return ___result_13; }
	inline String_t** get_address_of_result_13() { return &___result_13; }
	inline void set_result_13(String_t* value)
	{
		___result_13 = value;
		Il2CppCodeGenWriteBarrier(&___result_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
