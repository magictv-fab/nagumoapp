﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolChanged
struct  BoolChanged_t2120993528  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolChanged::boolVariable
	FsmBool_t1075959796 * ___boolVariable_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolChanged::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolChanged::storeResult
	FsmBool_t1075959796 * ___storeResult_11;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolChanged::previousValue
	bool ___previousValue_12;

public:
	inline static int32_t get_offset_of_boolVariable_9() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___boolVariable_9)); }
	inline FsmBool_t1075959796 * get_boolVariable_9() const { return ___boolVariable_9; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_9() { return &___boolVariable_9; }
	inline void set_boolVariable_9(FsmBool_t1075959796 * value)
	{
		___boolVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_9, value);
	}

	inline static int32_t get_offset_of_changedEvent_10() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___changedEvent_10)); }
	inline FsmEvent_t2133468028 * get_changedEvent_10() const { return ___changedEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_10() { return &___changedEvent_10; }
	inline void set_changedEvent_10(FsmEvent_t2133468028 * value)
	{
		___changedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_previousValue_12() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___previousValue_12)); }
	inline bool get_previousValue_12() const { return ___previousValue_12; }
	inline bool* get_address_of_previousValue_12() { return &___previousValue_12; }
	inline void set_previousValue_12(bool value)
	{
		___previousValue_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
