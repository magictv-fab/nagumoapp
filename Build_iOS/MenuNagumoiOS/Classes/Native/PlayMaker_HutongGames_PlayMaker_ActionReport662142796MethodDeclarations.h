﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionReport
struct ActionReport_t662142796;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport662142796.h"

// System.Void HutongGames.PlayMaker.ActionReport::Start()
extern "C"  void ActionReport_Start_m4164926471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionReport HutongGames.PlayMaker.ActionReport::Log(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String,System.String,System.Boolean)
extern "C"  ActionReport_t662142796 * ActionReport_Log_m1050010470 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fsm0, FsmState_t2146334067 * ___state1, FsmStateAction_t2366529033 * ___action2, int32_t ___actionIndex3, String_t* ___parameter4, String_t* ___logLine5, bool ___isError6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionReport::ActionReportContains(HutongGames.PlayMaker.ActionReport)
extern "C"  bool ActionReport_ActionReportContains_m2447204936 (Il2CppObject * __this /* static, unused */, ActionReport_t662142796 * ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionReport::SameAs(HutongGames.PlayMaker.ActionReport)
extern "C"  bool ActionReport_SameAs_m1531728311 (ActionReport_t662142796 * __this, ActionReport_t662142796 * ___actionReport0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::LogError(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String,System.String)
extern "C"  void ActionReport_LogError_m1595246836 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fsm0, FsmState_t2146334067 * ___state1, FsmStateAction_t2366529033 * ___action2, int32_t ___actionIndex3, String_t* ___parameter4, String_t* ___logLine5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::LogError(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String)
extern "C"  void ActionReport_LogError_m2721284792 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fsm0, FsmState_t2146334067 * ___state1, FsmStateAction_t2366529033 * ___action2, int32_t ___actionIndex3, String_t* ___logLine4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::Clear()
extern "C"  void ActionReport_Clear_m2623921970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionReport::GetCount()
extern "C"  int32_t ActionReport_GetCount_m1664555720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::.ctor()
extern "C"  void ActionReport__ctor_m922821383 (ActionReport_t662142796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::.cctor()
extern "C"  void ActionReport__cctor_m2355562886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
