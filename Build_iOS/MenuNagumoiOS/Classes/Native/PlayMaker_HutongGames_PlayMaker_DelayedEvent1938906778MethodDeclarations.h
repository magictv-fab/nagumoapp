﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1938906778.h"

// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.DelayedEvent::get_FsmEvent()
extern "C"  FsmEvent_t2133468028 * DelayedEvent_get_FsmEvent_m2799248489 (DelayedEvent_t1938906778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.DelayedEvent::get_Timer()
extern "C"  float DelayedEvent_get_Timer_m2425215207 (DelayedEvent_t1938906778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  void DelayedEvent__ctor_m4054091168 (DelayedEvent_t1938906778 * __this, Fsm_t1527112426 * ___fsm0, FsmEvent_t2133468028 * ___fsmEvent1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,System.String,System.Single)
extern "C"  void DelayedEvent__ctor_m3358954694 (DelayedEvent_t1938906778 * __this, Fsm_t1527112426 * ___fsm0, String_t* ___fsmEventName1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmEventTarget,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  void DelayedEvent__ctor_m3669636347 (DelayedEvent_t1938906778 * __this, Fsm_t1527112426 * ___fsm0, FsmEventTarget_t1823904941 * ___eventTarget1, FsmEvent_t2133468028 * ___fsmEvent2, float ___delay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmEventTarget,System.String,System.Single)
extern "C"  void DelayedEvent__ctor_m3895212043 (DelayedEvent_t1938906778 * __this, Fsm_t1527112426 * ___fsm0, FsmEventTarget_t1823904941 * ___eventTarget1, String_t* ___fsmEvent2, float ___delay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(PlayMakerFSM,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  void DelayedEvent__ctor_m2811663516 (DelayedEvent_t1938906778 * __this, PlayMakerFSM_t3799847376 * ___fsm0, FsmEvent_t2133468028 * ___fsmEvent1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(PlayMakerFSM,System.String,System.Single)
extern "C"  void DelayedEvent__ctor_m3680714314 (DelayedEvent_t1938906778 * __this, PlayMakerFSM_t3799847376 * ___fsm0, String_t* ___fsmEventName1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::Update()
extern "C"  void DelayedEvent_Update_m1716430516 (DelayedEvent_t1938906778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.DelayedEvent::WasSent(HutongGames.PlayMaker.DelayedEvent)
extern "C"  bool DelayedEvent_WasSent_m283661276 (Il2CppObject * __this /* static, unused */, DelayedEvent_t1938906778 * ___delayedEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.DelayedEvent::get_Finished()
extern "C"  bool DelayedEvent_get_Finished_m4200943902 (DelayedEvent_t1938906778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
