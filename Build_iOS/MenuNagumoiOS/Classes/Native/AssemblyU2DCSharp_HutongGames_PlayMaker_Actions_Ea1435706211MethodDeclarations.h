﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseColor
struct EaseColor_t1435706211;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseColor::.ctor()
extern "C"  void EaseColor__ctor_m4026007859 (EaseColor_t1435706211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::Reset()
extern "C"  void EaseColor_Reset_m1672440800 (EaseColor_t1435706211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnEnter()
extern "C"  void EaseColor_OnEnter_m2914714698 (EaseColor_t1435706211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnExit()
extern "C"  void EaseColor_OnExit_m1765510670 (EaseColor_t1435706211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnUpdate()
extern "C"  void EaseColor_OnUpdate_m3590369081 (EaseColor_t1435706211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
