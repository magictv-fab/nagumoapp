﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1816152376.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23033809700.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3453056018_gshared (InternalEnumerator_1_t1816152376 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3453056018(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1816152376 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3453056018_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781252174_gshared (InternalEnumerator_1_t1816152376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781252174(__this, method) ((  void (*) (InternalEnumerator_1_t1816152376 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781252174_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4101604100_gshared (InternalEnumerator_1_t1816152376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4101604100(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1816152376 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4101604100_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1096518761_gshared (InternalEnumerator_1_t1816152376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1096518761(__this, method) ((  void (*) (InternalEnumerator_1_t1816152376 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1096518761_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2410371454_gshared (InternalEnumerator_1_t1816152376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2410371454(__this, method) ((  bool (*) (InternalEnumerator_1_t1816152376 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2410371454_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.ResultMetadataType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3033809700  InternalEnumerator_1_get_Current_m2742550523_gshared (InternalEnumerator_1_t1816152376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2742550523(__this, method) ((  KeyValuePair_2_t3033809700  (*) (InternalEnumerator_1_t1816152376 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2742550523_gshared)(__this, method)
