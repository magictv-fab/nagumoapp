﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.InverseTransformDirection
struct InverseTransformDirection_t4174732817;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::.ctor()
extern "C"  void InverseTransformDirection__ctor_m665262661 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::Reset()
extern "C"  void InverseTransformDirection_Reset_m2606662898 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::OnEnter()
extern "C"  void InverseTransformDirection_OnEnter_m3053986012 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::OnUpdate()
extern "C"  void InverseTransformDirection_OnUpdate_m3612812519 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::DoInverseTransformDirection()
extern "C"  void InverseTransformDirection_DoInverseTransformDirection_m347529819 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
