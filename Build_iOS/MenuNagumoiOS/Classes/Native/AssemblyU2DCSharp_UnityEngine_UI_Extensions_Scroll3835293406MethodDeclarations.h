﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollRectEx/<OnEndDrag>c__AnonStorey8E
struct U3COnEndDragU3Ec__AnonStorey8E_t3835293406;
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t2789914546;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnEndDrag>c__AnonStorey8E::.ctor()
extern "C"  void U3COnEndDragU3Ec__AnonStorey8E__ctor_m110176333 (U3COnEndDragU3Ec__AnonStorey8E_t3835293406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnEndDrag>c__AnonStorey8E::<>m__3(UnityEngine.EventSystems.IEndDragHandler)
extern "C"  void U3COnEndDragU3Ec__AnonStorey8E_U3CU3Em__3_m2919541928 (U3COnEndDragU3Ec__AnonStorey8E_t3835293406 * __this, Il2CppObject * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
