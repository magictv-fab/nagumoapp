﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Tar.TarBuffer
struct TarBuffer_t2823691623;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarOutputStream
struct  TarOutputStream_t1551862984  : public Stream_t1561764144
{
public:
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::currBytes
	int64_t ___currBytes_2;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarOutputStream::assemblyBufferLength
	int32_t ___assemblyBufferLength_3;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::isClosed
	bool ___isClosed_4;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::currSize
	int64_t ___currSize_5;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarOutputStream::blockBuffer
	ByteU5BU5D_t4260760469* ___blockBuffer_6;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarOutputStream::assemblyBuffer
	ByteU5BU5D_t4260760469* ___assemblyBuffer_7;
	// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarOutputStream::buffer
	TarBuffer_t2823691623 * ___buffer_8;
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarOutputStream::outputStream
	Stream_t1561764144 * ___outputStream_9;

public:
	inline static int32_t get_offset_of_currBytes_2() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___currBytes_2)); }
	inline int64_t get_currBytes_2() const { return ___currBytes_2; }
	inline int64_t* get_address_of_currBytes_2() { return &___currBytes_2; }
	inline void set_currBytes_2(int64_t value)
	{
		___currBytes_2 = value;
	}

	inline static int32_t get_offset_of_assemblyBufferLength_3() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___assemblyBufferLength_3)); }
	inline int32_t get_assemblyBufferLength_3() const { return ___assemblyBufferLength_3; }
	inline int32_t* get_address_of_assemblyBufferLength_3() { return &___assemblyBufferLength_3; }
	inline void set_assemblyBufferLength_3(int32_t value)
	{
		___assemblyBufferLength_3 = value;
	}

	inline static int32_t get_offset_of_isClosed_4() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___isClosed_4)); }
	inline bool get_isClosed_4() const { return ___isClosed_4; }
	inline bool* get_address_of_isClosed_4() { return &___isClosed_4; }
	inline void set_isClosed_4(bool value)
	{
		___isClosed_4 = value;
	}

	inline static int32_t get_offset_of_currSize_5() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___currSize_5)); }
	inline int64_t get_currSize_5() const { return ___currSize_5; }
	inline int64_t* get_address_of_currSize_5() { return &___currSize_5; }
	inline void set_currSize_5(int64_t value)
	{
		___currSize_5 = value;
	}

	inline static int32_t get_offset_of_blockBuffer_6() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___blockBuffer_6)); }
	inline ByteU5BU5D_t4260760469* get_blockBuffer_6() const { return ___blockBuffer_6; }
	inline ByteU5BU5D_t4260760469** get_address_of_blockBuffer_6() { return &___blockBuffer_6; }
	inline void set_blockBuffer_6(ByteU5BU5D_t4260760469* value)
	{
		___blockBuffer_6 = value;
		Il2CppCodeGenWriteBarrier(&___blockBuffer_6, value);
	}

	inline static int32_t get_offset_of_assemblyBuffer_7() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___assemblyBuffer_7)); }
	inline ByteU5BU5D_t4260760469* get_assemblyBuffer_7() const { return ___assemblyBuffer_7; }
	inline ByteU5BU5D_t4260760469** get_address_of_assemblyBuffer_7() { return &___assemblyBuffer_7; }
	inline void set_assemblyBuffer_7(ByteU5BU5D_t4260760469* value)
	{
		___assemblyBuffer_7 = value;
		Il2CppCodeGenWriteBarrier(&___assemblyBuffer_7, value);
	}

	inline static int32_t get_offset_of_buffer_8() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___buffer_8)); }
	inline TarBuffer_t2823691623 * get_buffer_8() const { return ___buffer_8; }
	inline TarBuffer_t2823691623 ** get_address_of_buffer_8() { return &___buffer_8; }
	inline void set_buffer_8(TarBuffer_t2823691623 * value)
	{
		___buffer_8 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_8, value);
	}

	inline static int32_t get_offset_of_outputStream_9() { return static_cast<int32_t>(offsetof(TarOutputStream_t1551862984, ___outputStream_9)); }
	inline Stream_t1561764144 * get_outputStream_9() const { return ___outputStream_9; }
	inline Stream_t1561764144 ** get_address_of_outputStream_9() { return &___outputStream_9; }
	inline void set_outputStream_9(Stream_t1561764144 * value)
	{
		___outputStream_9 = value;
		Il2CppCodeGenWriteBarrier(&___outputStream_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
