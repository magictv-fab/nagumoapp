﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnEvent
struct OnEvent_t314892251;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.animation.GenericStartableComponentControllAbstract
struct  GenericStartableComponentControllAbstract_t1794600275  : public GeneralComponentsAbstract_t3900398046
{
public:
	// OnEvent ARM.animation.GenericStartableComponentControllAbstract::onFinished
	OnEvent_t314892251 * ___onFinished_4;
	// System.Boolean ARM.animation.GenericStartableComponentControllAbstract::_isFinished
	bool ____isFinished_5;
	// OnEvent ARM.animation.GenericStartableComponentControllAbstract::onStart
	OnEvent_t314892251 * ___onStart_6;
	// System.Boolean ARM.animation.GenericStartableComponentControllAbstract::_isPlaying
	bool ____isPlaying_7;

public:
	inline static int32_t get_offset_of_onFinished_4() { return static_cast<int32_t>(offsetof(GenericStartableComponentControllAbstract_t1794600275, ___onFinished_4)); }
	inline OnEvent_t314892251 * get_onFinished_4() const { return ___onFinished_4; }
	inline OnEvent_t314892251 ** get_address_of_onFinished_4() { return &___onFinished_4; }
	inline void set_onFinished_4(OnEvent_t314892251 * value)
	{
		___onFinished_4 = value;
		Il2CppCodeGenWriteBarrier(&___onFinished_4, value);
	}

	inline static int32_t get_offset_of__isFinished_5() { return static_cast<int32_t>(offsetof(GenericStartableComponentControllAbstract_t1794600275, ____isFinished_5)); }
	inline bool get__isFinished_5() const { return ____isFinished_5; }
	inline bool* get_address_of__isFinished_5() { return &____isFinished_5; }
	inline void set__isFinished_5(bool value)
	{
		____isFinished_5 = value;
	}

	inline static int32_t get_offset_of_onStart_6() { return static_cast<int32_t>(offsetof(GenericStartableComponentControllAbstract_t1794600275, ___onStart_6)); }
	inline OnEvent_t314892251 * get_onStart_6() const { return ___onStart_6; }
	inline OnEvent_t314892251 ** get_address_of_onStart_6() { return &___onStart_6; }
	inline void set_onStart_6(OnEvent_t314892251 * value)
	{
		___onStart_6 = value;
		Il2CppCodeGenWriteBarrier(&___onStart_6, value);
	}

	inline static int32_t get_offset_of__isPlaying_7() { return static_cast<int32_t>(offsetof(GenericStartableComponentControllAbstract_t1794600275, ____isPlaying_7)); }
	inline bool get__isPlaying_7() const { return ____isPlaying_7; }
	inline bool* get_address_of__isPlaying_7() { return &____isPlaying_7; }
	inline void set__isPlaying_7(bool value)
	{
		____isPlaying_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
