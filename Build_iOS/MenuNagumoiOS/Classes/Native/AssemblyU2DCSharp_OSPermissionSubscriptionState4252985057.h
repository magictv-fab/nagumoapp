﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OSPermissionState
struct OSPermissionState_t1777959294;
// OSSubscriptionState
struct OSSubscriptionState_t1688362992;
// OSEmailSubscriptionState
struct OSEmailSubscriptionState_t2956802108;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSPermissionSubscriptionState
struct  OSPermissionSubscriptionState_t4252985057  : public Il2CppObject
{
public:
	// OSPermissionState OSPermissionSubscriptionState::permissionStatus
	OSPermissionState_t1777959294 * ___permissionStatus_0;
	// OSSubscriptionState OSPermissionSubscriptionState::subscriptionStatus
	OSSubscriptionState_t1688362992 * ___subscriptionStatus_1;
	// OSEmailSubscriptionState OSPermissionSubscriptionState::emailSubscriptionStatus
	OSEmailSubscriptionState_t2956802108 * ___emailSubscriptionStatus_2;

public:
	inline static int32_t get_offset_of_permissionStatus_0() { return static_cast<int32_t>(offsetof(OSPermissionSubscriptionState_t4252985057, ___permissionStatus_0)); }
	inline OSPermissionState_t1777959294 * get_permissionStatus_0() const { return ___permissionStatus_0; }
	inline OSPermissionState_t1777959294 ** get_address_of_permissionStatus_0() { return &___permissionStatus_0; }
	inline void set_permissionStatus_0(OSPermissionState_t1777959294 * value)
	{
		___permissionStatus_0 = value;
		Il2CppCodeGenWriteBarrier(&___permissionStatus_0, value);
	}

	inline static int32_t get_offset_of_subscriptionStatus_1() { return static_cast<int32_t>(offsetof(OSPermissionSubscriptionState_t4252985057, ___subscriptionStatus_1)); }
	inline OSSubscriptionState_t1688362992 * get_subscriptionStatus_1() const { return ___subscriptionStatus_1; }
	inline OSSubscriptionState_t1688362992 ** get_address_of_subscriptionStatus_1() { return &___subscriptionStatus_1; }
	inline void set_subscriptionStatus_1(OSSubscriptionState_t1688362992 * value)
	{
		___subscriptionStatus_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscriptionStatus_1, value);
	}

	inline static int32_t get_offset_of_emailSubscriptionStatus_2() { return static_cast<int32_t>(offsetof(OSPermissionSubscriptionState_t4252985057, ___emailSubscriptionStatus_2)); }
	inline OSEmailSubscriptionState_t2956802108 * get_emailSubscriptionStatus_2() const { return ___emailSubscriptionStatus_2; }
	inline OSEmailSubscriptionState_t2956802108 ** get_address_of_emailSubscriptionStatus_2() { return &___emailSubscriptionStatus_2; }
	inline void set_emailSubscriptionStatus_2(OSEmailSubscriptionState_t2956802108 * value)
	{
		___emailSubscriptionStatus_2 = value;
		Il2CppCodeGenWriteBarrier(&___emailSubscriptionStatus_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
