﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetStringRight
struct  GetStringRight_t919028285  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringRight::stringVariable
	FsmString_t952858651 * ___stringVariable_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetStringRight::charCount
	FsmInt_t1596138449 * ___charCount_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringRight::storeResult
	FsmString_t952858651 * ___storeResult_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetStringRight::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_stringVariable_9() { return static_cast<int32_t>(offsetof(GetStringRight_t919028285, ___stringVariable_9)); }
	inline FsmString_t952858651 * get_stringVariable_9() const { return ___stringVariable_9; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_9() { return &___stringVariable_9; }
	inline void set_stringVariable_9(FsmString_t952858651 * value)
	{
		___stringVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_9, value);
	}

	inline static int32_t get_offset_of_charCount_10() { return static_cast<int32_t>(offsetof(GetStringRight_t919028285, ___charCount_10)); }
	inline FsmInt_t1596138449 * get_charCount_10() const { return ___charCount_10; }
	inline FsmInt_t1596138449 ** get_address_of_charCount_10() { return &___charCount_10; }
	inline void set_charCount_10(FsmInt_t1596138449 * value)
	{
		___charCount_10 = value;
		Il2CppCodeGenWriteBarrier(&___charCount_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(GetStringRight_t919028285, ___storeResult_11)); }
	inline FsmString_t952858651 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmString_t952858651 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmString_t952858651 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetStringRight_t919028285, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
