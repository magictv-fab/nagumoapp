﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GoogleUniversalAnalytics
struct GoogleUniversalAnalytics_t2200773556;
// Analytics
struct Analytics_t310950758;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Analytics
struct  Analytics_t310950758  : public MonoBehaviour_t667441552
{
public:
	// System.String Analytics::trackingID
	String_t* ___trackingID_3;
	// System.String Analytics::appName
	String_t* ___appName_4;
	// System.String Analytics::appVersion
	String_t* ___appVersion_5;
	// System.String Analytics::newLevelAnalyticsEventPrefix
	String_t* ___newLevelAnalyticsEventPrefix_6;
	// System.Boolean Analytics::useHTTPS
	bool ___useHTTPS_7;
	// System.Boolean Analytics::useOfflineCache
	bool ___useOfflineCache_8;
	// System.String Analytics::sceneName
	String_t* ___sceneName_11;
	// System.String Analytics::offlineCacheFileName
	String_t* ___offlineCacheFileName_12;

public:
	inline static int32_t get_offset_of_trackingID_3() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___trackingID_3)); }
	inline String_t* get_trackingID_3() const { return ___trackingID_3; }
	inline String_t** get_address_of_trackingID_3() { return &___trackingID_3; }
	inline void set_trackingID_3(String_t* value)
	{
		___trackingID_3 = value;
		Il2CppCodeGenWriteBarrier(&___trackingID_3, value);
	}

	inline static int32_t get_offset_of_appName_4() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___appName_4)); }
	inline String_t* get_appName_4() const { return ___appName_4; }
	inline String_t** get_address_of_appName_4() { return &___appName_4; }
	inline void set_appName_4(String_t* value)
	{
		___appName_4 = value;
		Il2CppCodeGenWriteBarrier(&___appName_4, value);
	}

	inline static int32_t get_offset_of_appVersion_5() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___appVersion_5)); }
	inline String_t* get_appVersion_5() const { return ___appVersion_5; }
	inline String_t** get_address_of_appVersion_5() { return &___appVersion_5; }
	inline void set_appVersion_5(String_t* value)
	{
		___appVersion_5 = value;
		Il2CppCodeGenWriteBarrier(&___appVersion_5, value);
	}

	inline static int32_t get_offset_of_newLevelAnalyticsEventPrefix_6() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___newLevelAnalyticsEventPrefix_6)); }
	inline String_t* get_newLevelAnalyticsEventPrefix_6() const { return ___newLevelAnalyticsEventPrefix_6; }
	inline String_t** get_address_of_newLevelAnalyticsEventPrefix_6() { return &___newLevelAnalyticsEventPrefix_6; }
	inline void set_newLevelAnalyticsEventPrefix_6(String_t* value)
	{
		___newLevelAnalyticsEventPrefix_6 = value;
		Il2CppCodeGenWriteBarrier(&___newLevelAnalyticsEventPrefix_6, value);
	}

	inline static int32_t get_offset_of_useHTTPS_7() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___useHTTPS_7)); }
	inline bool get_useHTTPS_7() const { return ___useHTTPS_7; }
	inline bool* get_address_of_useHTTPS_7() { return &___useHTTPS_7; }
	inline void set_useHTTPS_7(bool value)
	{
		___useHTTPS_7 = value;
	}

	inline static int32_t get_offset_of_useOfflineCache_8() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___useOfflineCache_8)); }
	inline bool get_useOfflineCache_8() const { return ___useOfflineCache_8; }
	inline bool* get_address_of_useOfflineCache_8() { return &___useOfflineCache_8; }
	inline void set_useOfflineCache_8(bool value)
	{
		___useOfflineCache_8 = value;
	}

	inline static int32_t get_offset_of_sceneName_11() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___sceneName_11)); }
	inline String_t* get_sceneName_11() const { return ___sceneName_11; }
	inline String_t** get_address_of_sceneName_11() { return &___sceneName_11; }
	inline void set_sceneName_11(String_t* value)
	{
		___sceneName_11 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_11, value);
	}

	inline static int32_t get_offset_of_offlineCacheFileName_12() { return static_cast<int32_t>(offsetof(Analytics_t310950758, ___offlineCacheFileName_12)); }
	inline String_t* get_offlineCacheFileName_12() const { return ___offlineCacheFileName_12; }
	inline String_t** get_address_of_offlineCacheFileName_12() { return &___offlineCacheFileName_12; }
	inline void set_offlineCacheFileName_12(String_t* value)
	{
		___offlineCacheFileName_12 = value;
		Il2CppCodeGenWriteBarrier(&___offlineCacheFileName_12, value);
	}
};

struct Analytics_t310950758_StaticFields
{
public:
	// GoogleUniversalAnalytics Analytics::gua
	GoogleUniversalAnalytics_t2200773556 * ___gua_9;
	// Analytics Analytics::instance
	Analytics_t310950758 * ___instance_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Analytics::<>f__switch$map1
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1_13;

public:
	inline static int32_t get_offset_of_gua_9() { return static_cast<int32_t>(offsetof(Analytics_t310950758_StaticFields, ___gua_9)); }
	inline GoogleUniversalAnalytics_t2200773556 * get_gua_9() const { return ___gua_9; }
	inline GoogleUniversalAnalytics_t2200773556 ** get_address_of_gua_9() { return &___gua_9; }
	inline void set_gua_9(GoogleUniversalAnalytics_t2200773556 * value)
	{
		___gua_9 = value;
		Il2CppCodeGenWriteBarrier(&___gua_9, value);
	}

	inline static int32_t get_offset_of_instance_10() { return static_cast<int32_t>(offsetof(Analytics_t310950758_StaticFields, ___instance_10)); }
	inline Analytics_t310950758 * get_instance_10() const { return ___instance_10; }
	inline Analytics_t310950758 ** get_address_of_instance_10() { return &___instance_10; }
	inline void set_instance_10(Analytics_t310950758 * value)
	{
		___instance_10 = value;
		Il2CppCodeGenWriteBarrier(&___instance_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_13() { return static_cast<int32_t>(offsetof(Analytics_t310950758_StaticFields, ___U3CU3Ef__switchU24map1_13)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1_13() const { return ___U3CU3Ef__switchU24map1_13; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1_13() { return &___U3CU3Ef__switchU24map1_13; }
	inline void set_U3CU3Ef__switchU24map1_13(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
