﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.HybridBinarizer
struct HybridBinarizer_t3517076856;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// ZXing.Binarizer
struct Binarizer_t1492033400;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Common.BitMatrix ZXing.Common.HybridBinarizer::get_BlackMatrix()
extern "C"  BitMatrix_t1058711404 * HybridBinarizer_get_BlackMatrix_m3895431495 (HybridBinarizer_t3517076856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.HybridBinarizer::.ctor(ZXing.LuminanceSource)
extern "C"  void HybridBinarizer__ctor_m2380500591 (HybridBinarizer_t3517076856 * __this, LuminanceSource_t1231523093 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Binarizer ZXing.Common.HybridBinarizer::createBinarizer(ZXing.LuminanceSource)
extern "C"  Binarizer_t1492033400 * HybridBinarizer_createBinarizer_m2211026078 (HybridBinarizer_t3517076856 * __this, LuminanceSource_t1231523093 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.HybridBinarizer::binarizeEntireImage()
extern "C"  void HybridBinarizer_binarizeEntireImage_m4234732518 (HybridBinarizer_t3517076856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.HybridBinarizer::calculateThresholdForBlock(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[][],ZXing.Common.BitMatrix)
extern "C"  void HybridBinarizer_calculateThresholdForBlock_m1669476948 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___luminances0, int32_t ___subWidth1, int32_t ___subHeight2, int32_t ___width3, int32_t ___height4, Int32U5BU5DU5BU5D_t1820556512* ___blackPoints5, BitMatrix_t1058711404 * ___matrix6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.HybridBinarizer::cap(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t HybridBinarizer_cap_m1697731917 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___min1, int32_t ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.HybridBinarizer::thresholdBlock(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,ZXing.Common.BitMatrix)
extern "C"  void HybridBinarizer_thresholdBlock_m2342235124 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___luminances0, int32_t ___xoffset1, int32_t ___yoffset2, int32_t ___threshold3, int32_t ___stride4, BitMatrix_t1058711404 * ___matrix5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[][] ZXing.Common.HybridBinarizer::calculateBlackPoints(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Int32U5BU5DU5BU5D_t1820556512* HybridBinarizer_calculateBlackPoints_m1800238009 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___luminances0, int32_t ___subWidth1, int32_t ___subHeight2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
