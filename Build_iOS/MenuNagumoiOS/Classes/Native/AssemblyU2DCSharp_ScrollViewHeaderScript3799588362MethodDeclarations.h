﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollViewHeaderScript
struct ScrollViewHeaderScript_t3799588362;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollViewHeaderScript::.ctor()
extern "C"  void ScrollViewHeaderScript__ctor_m2596487633 (ScrollViewHeaderScript_t3799588362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewHeaderScript::Start()
extern "C"  void ScrollViewHeaderScript_Start_m1543625425 (ScrollViewHeaderScript_t3799588362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollViewHeaderScript::Update()
extern "C"  void ScrollViewHeaderScript_Update_m613600092 (ScrollViewHeaderScript_t3799588362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
