﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetButton
struct GetButton_t428887414;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetButton::.ctor()
extern "C"  void GetButton__ctor_m2258467968 (GetButton_t428887414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::Reset()
extern "C"  void GetButton_Reset_m4199868205 (GetButton_t428887414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::OnEnter()
extern "C"  void GetButton_OnEnter_m820961367 (GetButton_t428887414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::OnUpdate()
extern "C"  void GetButton_OnUpdate_m3108525260 (GetButton_t428887414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::DoGetButton()
extern "C"  void GetButton_DoGetButton_m2944171643 (GetButton_t428887414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
