﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.CurrentVersionRevisionVO
struct  CurrentVersionRevisionVO_t3939651409  : public Il2CppObject
{
public:
	// System.Int32 MagicTV.vo.CurrentVersionRevisionVO::current
	int32_t ___current_0;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(CurrentVersionRevisionVO_t3939651409, ___current_0)); }
	inline int32_t get_current_0() const { return ___current_0; }
	inline int32_t* get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(int32_t value)
	{
		___current_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
