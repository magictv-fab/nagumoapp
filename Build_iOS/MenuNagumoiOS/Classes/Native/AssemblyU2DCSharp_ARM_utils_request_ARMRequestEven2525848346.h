﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.request.OnRequestEventHandler
struct OnRequestEventHandler_t153308946;
// ARM.utils.request.OnProgressEventHandler
struct OnProgressEventHandler_t1956336554;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.request.ARMRequestEvents
struct  ARMRequestEvents_t2525848346  : public Il2CppObject
{
public:
	// ARM.utils.request.OnRequestEventHandler ARM.utils.request.ARMRequestEvents::onRequestComplete
	OnRequestEventHandler_t153308946 * ___onRequestComplete_0;
	// ARM.utils.request.OnRequestEventHandler ARM.utils.request.ARMRequestEvents::onRequestFail
	OnRequestEventHandler_t153308946 * ___onRequestFail_1;
	// ARM.utils.request.OnProgressEventHandler ARM.utils.request.ARMRequestEvents::onProgress
	OnProgressEventHandler_t1956336554 * ___onProgress_2;
	// System.Boolean ARM.utils.request.ARMRequestEvents::_isComplete
	bool ____isComplete_3;
	// System.Single ARM.utils.request.ARMRequestEvents::weight
	float ___weight_4;
	// System.Single ARM.utils.request.ARMRequestEvents::_currentProgress
	float ____currentProgress_5;

public:
	inline static int32_t get_offset_of_onRequestComplete_0() { return static_cast<int32_t>(offsetof(ARMRequestEvents_t2525848346, ___onRequestComplete_0)); }
	inline OnRequestEventHandler_t153308946 * get_onRequestComplete_0() const { return ___onRequestComplete_0; }
	inline OnRequestEventHandler_t153308946 ** get_address_of_onRequestComplete_0() { return &___onRequestComplete_0; }
	inline void set_onRequestComplete_0(OnRequestEventHandler_t153308946 * value)
	{
		___onRequestComplete_0 = value;
		Il2CppCodeGenWriteBarrier(&___onRequestComplete_0, value);
	}

	inline static int32_t get_offset_of_onRequestFail_1() { return static_cast<int32_t>(offsetof(ARMRequestEvents_t2525848346, ___onRequestFail_1)); }
	inline OnRequestEventHandler_t153308946 * get_onRequestFail_1() const { return ___onRequestFail_1; }
	inline OnRequestEventHandler_t153308946 ** get_address_of_onRequestFail_1() { return &___onRequestFail_1; }
	inline void set_onRequestFail_1(OnRequestEventHandler_t153308946 * value)
	{
		___onRequestFail_1 = value;
		Il2CppCodeGenWriteBarrier(&___onRequestFail_1, value);
	}

	inline static int32_t get_offset_of_onProgress_2() { return static_cast<int32_t>(offsetof(ARMRequestEvents_t2525848346, ___onProgress_2)); }
	inline OnProgressEventHandler_t1956336554 * get_onProgress_2() const { return ___onProgress_2; }
	inline OnProgressEventHandler_t1956336554 ** get_address_of_onProgress_2() { return &___onProgress_2; }
	inline void set_onProgress_2(OnProgressEventHandler_t1956336554 * value)
	{
		___onProgress_2 = value;
		Il2CppCodeGenWriteBarrier(&___onProgress_2, value);
	}

	inline static int32_t get_offset_of__isComplete_3() { return static_cast<int32_t>(offsetof(ARMRequestEvents_t2525848346, ____isComplete_3)); }
	inline bool get__isComplete_3() const { return ____isComplete_3; }
	inline bool* get_address_of__isComplete_3() { return &____isComplete_3; }
	inline void set__isComplete_3(bool value)
	{
		____isComplete_3 = value;
	}

	inline static int32_t get_offset_of_weight_4() { return static_cast<int32_t>(offsetof(ARMRequestEvents_t2525848346, ___weight_4)); }
	inline float get_weight_4() const { return ___weight_4; }
	inline float* get_address_of_weight_4() { return &___weight_4; }
	inline void set_weight_4(float value)
	{
		___weight_4 = value;
	}

	inline static int32_t get_offset_of__currentProgress_5() { return static_cast<int32_t>(offsetof(ARMRequestEvents_t2525848346, ____currentProgress_5)); }
	inline float get__currentProgress_5() const { return ____currentProgress_5; }
	inline float* get_address_of__currentProgress_5() { return &____currentProgress_5; }
	inline void set__currentProgress_5(float value)
	{
		____currentProgress_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
