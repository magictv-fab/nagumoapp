﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.internet.InternetInfoAbstract
struct InternetInfoAbstract_t823636095;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;
// MagicTV.vo.result.ConfirmResultVO
struct ConfirmResultVO_t1731700412;
// MagicTV.download.DeviceCacheFileManager
struct DeviceCacheFileManager_t2756182409;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Func`2<MagicTV.vo.UrlInfoVO,System.String>
struct Func_2_t888805570;

#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceCa121423689.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.download.DeviceCacheFileManager
struct  DeviceCacheFileManager_t2756182409  : public DeviceCacheFileManagerAbstract_t121423689
{
public:
	// ARM.abstracts.internet.InternetInfoAbstract MagicTV.download.DeviceCacheFileManager::internetInfo
	InternetInfoAbstract_t823636095 * ___internetInfo_14;
	// MagicTV.vo.ResultRevisionVO MagicTV.download.DeviceCacheFileManager::_revisionToCache
	ResultRevisionVO_t2445597391 * ____revisionToCache_15;
	// MagicTV.abstracts.device.DeviceFileInfoAbstract MagicTV.download.DeviceCacheFileManager::_deviceFileInfo
	DeviceFileInfoAbstract_t3788299492 * ____deviceFileInfo_16;
	// MagicTV.vo.result.ConfirmResultVO MagicTV.download.DeviceCacheFileManager::_lastMessageVO
	ConfirmResultVO_t1731700412 * ____lastMessageVO_17;
	// System.Collections.Generic.List`1<System.String> MagicTV.download.DeviceCacheFileManager::DebugUrlCachedToDelete
	List_1_t1375417109 * ___DebugUrlCachedToDelete_19;

public:
	inline static int32_t get_offset_of_internetInfo_14() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409, ___internetInfo_14)); }
	inline InternetInfoAbstract_t823636095 * get_internetInfo_14() const { return ___internetInfo_14; }
	inline InternetInfoAbstract_t823636095 ** get_address_of_internetInfo_14() { return &___internetInfo_14; }
	inline void set_internetInfo_14(InternetInfoAbstract_t823636095 * value)
	{
		___internetInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&___internetInfo_14, value);
	}

	inline static int32_t get_offset_of__revisionToCache_15() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409, ____revisionToCache_15)); }
	inline ResultRevisionVO_t2445597391 * get__revisionToCache_15() const { return ____revisionToCache_15; }
	inline ResultRevisionVO_t2445597391 ** get_address_of__revisionToCache_15() { return &____revisionToCache_15; }
	inline void set__revisionToCache_15(ResultRevisionVO_t2445597391 * value)
	{
		____revisionToCache_15 = value;
		Il2CppCodeGenWriteBarrier(&____revisionToCache_15, value);
	}

	inline static int32_t get_offset_of__deviceFileInfo_16() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409, ____deviceFileInfo_16)); }
	inline DeviceFileInfoAbstract_t3788299492 * get__deviceFileInfo_16() const { return ____deviceFileInfo_16; }
	inline DeviceFileInfoAbstract_t3788299492 ** get_address_of__deviceFileInfo_16() { return &____deviceFileInfo_16; }
	inline void set__deviceFileInfo_16(DeviceFileInfoAbstract_t3788299492 * value)
	{
		____deviceFileInfo_16 = value;
		Il2CppCodeGenWriteBarrier(&____deviceFileInfo_16, value);
	}

	inline static int32_t get_offset_of__lastMessageVO_17() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409, ____lastMessageVO_17)); }
	inline ConfirmResultVO_t1731700412 * get__lastMessageVO_17() const { return ____lastMessageVO_17; }
	inline ConfirmResultVO_t1731700412 ** get_address_of__lastMessageVO_17() { return &____lastMessageVO_17; }
	inline void set__lastMessageVO_17(ConfirmResultVO_t1731700412 * value)
	{
		____lastMessageVO_17 = value;
		Il2CppCodeGenWriteBarrier(&____lastMessageVO_17, value);
	}

	inline static int32_t get_offset_of_DebugUrlCachedToDelete_19() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409, ___DebugUrlCachedToDelete_19)); }
	inline List_1_t1375417109 * get_DebugUrlCachedToDelete_19() const { return ___DebugUrlCachedToDelete_19; }
	inline List_1_t1375417109 ** get_address_of_DebugUrlCachedToDelete_19() { return &___DebugUrlCachedToDelete_19; }
	inline void set_DebugUrlCachedToDelete_19(List_1_t1375417109 * value)
	{
		___DebugUrlCachedToDelete_19 = value;
		Il2CppCodeGenWriteBarrier(&___DebugUrlCachedToDelete_19, value);
	}
};

struct DeviceCacheFileManager_t2756182409_StaticFields
{
public:
	// MagicTV.download.DeviceCacheFileManager MagicTV.download.DeviceCacheFileManager::Instance
	DeviceCacheFileManager_t2756182409 * ___Instance_18;
	// System.Func`2<MagicTV.vo.UrlInfoVO,System.String> MagicTV.download.DeviceCacheFileManager::<>f__am$cache6
	Func_2_t888805570 * ___U3CU3Ef__amU24cache6_20;

public:
	inline static int32_t get_offset_of_Instance_18() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409_StaticFields, ___Instance_18)); }
	inline DeviceCacheFileManager_t2756182409 * get_Instance_18() const { return ___Instance_18; }
	inline DeviceCacheFileManager_t2756182409 ** get_address_of_Instance_18() { return &___Instance_18; }
	inline void set_Instance_18(DeviceCacheFileManager_t2756182409 * value)
	{
		___Instance_18 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_20() { return static_cast<int32_t>(offsetof(DeviceCacheFileManager_t2756182409_StaticFields, ___U3CU3Ef__amU24cache6_20)); }
	inline Func_2_t888805570 * get_U3CU3Ef__amU24cache6_20() const { return ___U3CU3Ef__amU24cache6_20; }
	inline Func_2_t888805570 ** get_address_of_U3CU3Ef__amU24cache6_20() { return &___U3CU3Ef__amU24cache6_20; }
	inline void set_U3CU3Ef__amU24cache6_20(Func_2_t888805570 * value)
	{
		___U3CU3Ef__amU24cache6_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
