﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3011917040(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2111707165 *, String_t*, Action_1_t1392508089 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>::get_Key()
#define KeyValuePair_2_get_Key_m2139293944(__this, method) ((  String_t* (*) (KeyValuePair_2_t2111707165 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3415723833(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2111707165 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>::get_Value()
#define KeyValuePair_2_get_Value_m2807622072(__this, method) ((  Action_1_t1392508089 * (*) (KeyValuePair_2_t2111707165 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2224479929(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2111707165 *, Action_1_t1392508089 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>::ToString()
#define KeyValuePair_2_ToString_m3903978633(__this, method) ((  String_t* (*) (KeyValuePair_2_t2111707165 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
