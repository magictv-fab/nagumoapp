﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.OneD.UPCEANReader
struct UPCEANReader_t3527170699;

#include "QRCode_ZXing_OneD_UPCEANReader3527170699.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCAReader
struct  UPCAReader_t3152467930  : public UPCEANReader_t3527170699
{
public:
	// ZXing.OneD.UPCEANReader ZXing.OneD.UPCAReader::ean13Reader
	UPCEANReader_t3527170699 * ___ean13Reader_11;

public:
	inline static int32_t get_offset_of_ean13Reader_11() { return static_cast<int32_t>(offsetof(UPCAReader_t3152467930, ___ean13Reader_11)); }
	inline UPCEANReader_t3527170699 * get_ean13Reader_11() const { return ___ean13Reader_11; }
	inline UPCEANReader_t3527170699 ** get_address_of_ean13Reader_11() { return &___ean13Reader_11; }
	inline void set_ean13Reader_11(UPCEANReader_t3527170699 * value)
	{
		___ean13Reader_11 = value;
		Il2CppCodeGenWriteBarrier(&___ean13Reader_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
