﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1267765161;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightsControl
struct  LightsControl_t2444629088  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> LightsControl::lights
	List_1_t747900261 * ___lights_2;
	// System.Single LightsControl::velocity
	float ___velocity_3;
	// UnityEngine.Color LightsControl::lightOnColor
	Color_t4194546905  ___lightOnColor_4;
	// UnityEngine.Color LightsControl::emissionColor
	Color_t4194546905  ___emissionColor_5;
	// System.Collections.Generic.List`1<UnityEngine.Color> LightsControl::emissionColorLst
	List_1_t1267765161 * ___emissionColorLst_6;
	// UnityEngine.Color LightsControl::startMaterialColor
	Color_t4194546905  ___startMaterialColor_7;
	// UnityEngine.Color LightsControl::startEmissionColor
	Color_t4194546905  ___startEmissionColor_8;

public:
	inline static int32_t get_offset_of_lights_2() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___lights_2)); }
	inline List_1_t747900261 * get_lights_2() const { return ___lights_2; }
	inline List_1_t747900261 ** get_address_of_lights_2() { return &___lights_2; }
	inline void set_lights_2(List_1_t747900261 * value)
	{
		___lights_2 = value;
		Il2CppCodeGenWriteBarrier(&___lights_2, value);
	}

	inline static int32_t get_offset_of_velocity_3() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___velocity_3)); }
	inline float get_velocity_3() const { return ___velocity_3; }
	inline float* get_address_of_velocity_3() { return &___velocity_3; }
	inline void set_velocity_3(float value)
	{
		___velocity_3 = value;
	}

	inline static int32_t get_offset_of_lightOnColor_4() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___lightOnColor_4)); }
	inline Color_t4194546905  get_lightOnColor_4() const { return ___lightOnColor_4; }
	inline Color_t4194546905 * get_address_of_lightOnColor_4() { return &___lightOnColor_4; }
	inline void set_lightOnColor_4(Color_t4194546905  value)
	{
		___lightOnColor_4 = value;
	}

	inline static int32_t get_offset_of_emissionColor_5() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___emissionColor_5)); }
	inline Color_t4194546905  get_emissionColor_5() const { return ___emissionColor_5; }
	inline Color_t4194546905 * get_address_of_emissionColor_5() { return &___emissionColor_5; }
	inline void set_emissionColor_5(Color_t4194546905  value)
	{
		___emissionColor_5 = value;
	}

	inline static int32_t get_offset_of_emissionColorLst_6() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___emissionColorLst_6)); }
	inline List_1_t1267765161 * get_emissionColorLst_6() const { return ___emissionColorLst_6; }
	inline List_1_t1267765161 ** get_address_of_emissionColorLst_6() { return &___emissionColorLst_6; }
	inline void set_emissionColorLst_6(List_1_t1267765161 * value)
	{
		___emissionColorLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___emissionColorLst_6, value);
	}

	inline static int32_t get_offset_of_startMaterialColor_7() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___startMaterialColor_7)); }
	inline Color_t4194546905  get_startMaterialColor_7() const { return ___startMaterialColor_7; }
	inline Color_t4194546905 * get_address_of_startMaterialColor_7() { return &___startMaterialColor_7; }
	inline void set_startMaterialColor_7(Color_t4194546905  value)
	{
		___startMaterialColor_7 = value;
	}

	inline static int32_t get_offset_of_startEmissionColor_8() { return static_cast<int32_t>(offsetof(LightsControl_t2444629088, ___startEmissionColor_8)); }
	inline Color_t4194546905  get_startEmissionColor_8() const { return ___startEmissionColor_8; }
	inline Color_t4194546905 * get_address_of_startEmissionColor_8() { return &___startEmissionColor_8; }
	inline void set_startEmissionColor_8(Color_t4194546905  value)
	{
		___startEmissionColor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
