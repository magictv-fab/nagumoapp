﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.ResultRevisionVO::.ctor()
extern "C"  void ResultRevisionVO__ctor_m521864292 (ResultRevisionVO_t2445597391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.ResultRevisionVO::ToString()
extern "C"  String_t* ResultRevisionVO_ToString_m1777475785 (ResultRevisionVO_t2445597391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
