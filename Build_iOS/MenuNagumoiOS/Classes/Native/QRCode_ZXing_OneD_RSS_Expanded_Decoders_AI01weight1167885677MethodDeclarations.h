﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder
struct AI01weightDecoder_t1167885677;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AI01weightDecoder__ctor_m1479333289 (AI01weightDecoder_t1167885677 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::encodeCompressedWeight(System.Text.StringBuilder,System.Int32,System.Int32)
extern "C"  void AI01weightDecoder_encodeCompressedWeight_m2587782139 (AI01weightDecoder_t1167885677 * __this, StringBuilder_t243639308 * ___buf0, int32_t ___currentPos1, int32_t ___weightSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
