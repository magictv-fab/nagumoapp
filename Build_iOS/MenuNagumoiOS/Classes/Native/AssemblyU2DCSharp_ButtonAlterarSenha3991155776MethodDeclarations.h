﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonAlterarSenha
struct ButtonAlterarSenha_t3991155776;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonAlterarSenha::.ctor()
extern "C"  void ButtonAlterarSenha__ctor_m1209570395 (ButtonAlterarSenha_t3991155776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAlterarSenha::Start()
extern "C"  void ButtonAlterarSenha_Start_m156708187 (ButtonAlterarSenha_t3991155776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAlterarSenha::Update()
extern "C"  void ButtonAlterarSenha_Update_m568838674 (ButtonAlterarSenha_t3991155776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAlterarSenha::Action()
extern "C"  void ButtonAlterarSenha_Action_m1885038655 (ButtonAlterarSenha_t3991155776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
