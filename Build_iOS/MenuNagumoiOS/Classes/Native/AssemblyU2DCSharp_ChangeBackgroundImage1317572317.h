﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// DownloadImages
struct DownloadImages_t3644489024;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeBackgroundImage
struct  ChangeBackgroundImage_t1317572317  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image ChangeBackgroundImage::backgroundImage
	Image_t538875265 * ___backgroundImage_2;
	// UnityEngine.Sprite ChangeBackgroundImage::spriteLoaded
	Sprite_t3199167241 * ___spriteLoaded_3;
	// UnityEngine.UI.Image ChangeBackgroundImage::loadBar
	Image_t538875265 * ___loadBar_4;
	// UnityEngine.GameObject ChangeBackgroundImage::scrollGroup
	GameObject_t3674682005 * ___scrollGroup_5;
	// UnityEngine.GameObject ChangeBackgroundImage::barGroup
	GameObject_t3674682005 * ___barGroup_6;
	// DownloadImages ChangeBackgroundImage::downloadImages
	DownloadImages_t3644489024 * ___downloadImages_7;
	// System.Boolean ChangeBackgroundImage::startLoad
	bool ___startLoad_8;
	// System.String ChangeBackgroundImage::focusCover
	String_t* ___focusCover_9;

public:
	inline static int32_t get_offset_of_backgroundImage_2() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___backgroundImage_2)); }
	inline Image_t538875265 * get_backgroundImage_2() const { return ___backgroundImage_2; }
	inline Image_t538875265 ** get_address_of_backgroundImage_2() { return &___backgroundImage_2; }
	inline void set_backgroundImage_2(Image_t538875265 * value)
	{
		___backgroundImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImage_2, value);
	}

	inline static int32_t get_offset_of_spriteLoaded_3() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___spriteLoaded_3)); }
	inline Sprite_t3199167241 * get_spriteLoaded_3() const { return ___spriteLoaded_3; }
	inline Sprite_t3199167241 ** get_address_of_spriteLoaded_3() { return &___spriteLoaded_3; }
	inline void set_spriteLoaded_3(Sprite_t3199167241 * value)
	{
		___spriteLoaded_3 = value;
		Il2CppCodeGenWriteBarrier(&___spriteLoaded_3, value);
	}

	inline static int32_t get_offset_of_loadBar_4() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___loadBar_4)); }
	inline Image_t538875265 * get_loadBar_4() const { return ___loadBar_4; }
	inline Image_t538875265 ** get_address_of_loadBar_4() { return &___loadBar_4; }
	inline void set_loadBar_4(Image_t538875265 * value)
	{
		___loadBar_4 = value;
		Il2CppCodeGenWriteBarrier(&___loadBar_4, value);
	}

	inline static int32_t get_offset_of_scrollGroup_5() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___scrollGroup_5)); }
	inline GameObject_t3674682005 * get_scrollGroup_5() const { return ___scrollGroup_5; }
	inline GameObject_t3674682005 ** get_address_of_scrollGroup_5() { return &___scrollGroup_5; }
	inline void set_scrollGroup_5(GameObject_t3674682005 * value)
	{
		___scrollGroup_5 = value;
		Il2CppCodeGenWriteBarrier(&___scrollGroup_5, value);
	}

	inline static int32_t get_offset_of_barGroup_6() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___barGroup_6)); }
	inline GameObject_t3674682005 * get_barGroup_6() const { return ___barGroup_6; }
	inline GameObject_t3674682005 ** get_address_of_barGroup_6() { return &___barGroup_6; }
	inline void set_barGroup_6(GameObject_t3674682005 * value)
	{
		___barGroup_6 = value;
		Il2CppCodeGenWriteBarrier(&___barGroup_6, value);
	}

	inline static int32_t get_offset_of_downloadImages_7() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___downloadImages_7)); }
	inline DownloadImages_t3644489024 * get_downloadImages_7() const { return ___downloadImages_7; }
	inline DownloadImages_t3644489024 ** get_address_of_downloadImages_7() { return &___downloadImages_7; }
	inline void set_downloadImages_7(DownloadImages_t3644489024 * value)
	{
		___downloadImages_7 = value;
		Il2CppCodeGenWriteBarrier(&___downloadImages_7, value);
	}

	inline static int32_t get_offset_of_startLoad_8() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___startLoad_8)); }
	inline bool get_startLoad_8() const { return ___startLoad_8; }
	inline bool* get_address_of_startLoad_8() { return &___startLoad_8; }
	inline void set_startLoad_8(bool value)
	{
		___startLoad_8 = value;
	}

	inline static int32_t get_offset_of_focusCover_9() { return static_cast<int32_t>(offsetof(ChangeBackgroundImage_t1317572317, ___focusCover_9)); }
	inline String_t* get_focusCover_9() const { return ___focusCover_9; }
	inline String_t** get_address_of_focusCover_9() { return &___focusCover_9; }
	inline void set_focusCover_9(String_t* value)
	{
		___focusCover_9 = value;
		Il2CppCodeGenWriteBarrier(&___focusCover_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
