﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3RotateTowards
struct Vector3RotateTowards_t705544633;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3RotateTowards::.ctor()
extern "C"  void Vector3RotateTowards__ctor_m3626448077 (Vector3RotateTowards_t705544633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3RotateTowards::Reset()
extern "C"  void Vector3RotateTowards_Reset_m1272881018 (Vector3RotateTowards_t705544633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3RotateTowards::OnUpdate()
extern "C"  void Vector3RotateTowards_OnUpdate_m1659280735 (Vector3RotateTowards_t705544633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
