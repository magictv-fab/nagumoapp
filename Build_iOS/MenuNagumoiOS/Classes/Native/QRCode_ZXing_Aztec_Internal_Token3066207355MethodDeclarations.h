﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.Token
struct Token_t3066207355;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Token3066207355.h"

// System.Void ZXing.Aztec.Internal.Token::.ctor(ZXing.Aztec.Internal.Token)
extern "C"  void Token__ctor_m2997228717 (Token_t3066207355 * __this, Token_t3066207355 * ___previous0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::get_Previous()
extern "C"  Token_t3066207355 * Token_get_Previous_m1517757574 (Token_t3066207355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::add(System.Int32,System.Int32)
extern "C"  Token_t3066207355 * Token_add_m3211655081 (Token_t3066207355 * __this, int32_t ___value0, int32_t ___bitCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::addBinaryShift(System.Int32,System.Int32)
extern "C"  Token_t3066207355 * Token_addBinaryShift_m872050400 (Token_t3066207355 * __this, int32_t ___start0, int32_t ___byteCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.Token::.cctor()
extern "C"  void Token__cctor_m1247892953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
