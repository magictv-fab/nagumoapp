﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleJSON561564090.h"
#include "AssemblyU2DCSharp_Sombra2482075392.h"
#include "AssemblyU2DCSharp_Sombra_U3CStartU3Ec__Iterator7F1445136119.h"
#include "AssemblyU2DCSharp_TabloideManager3674153819.h"
#include "AssemblyU2DCSharp_TabloideManager_U3CILoadPublishU3858799123.h"
#include "AssemblyU2DCSharp_TabloideManager_U3CILoadImgsU3Ec_781055235.h"
#include "AssemblyU2DCSharp_TabloideManager_U3CLoadLoadedsU33625498093.h"
#include "AssemblyU2DCSharp_TabloideValues99048372.h"
#include "AssemblyU2DCSharp_TiltWindow1575826733.h"
#include "AssemblyU2DCSharp_UpdateData1697049651.h"
#include "AssemblyU2DCSharp_UpdateData_U3CILoginU3Ec__Iterat2470511182.h"
#include "AssemblyU2DCSharp_UploadPhoto2718662801.h"
#include "AssemblyU2DCSharp_UploadPhoto_U3CIUploadPhotoToSer3680044787.h"
#include "AssemblyU2DCSharp_UploadPhoto_U3CloadImageByWWWU3Ec364354809.h"
#include "AssemblyU2DCSharp_UserData4092646965.h"
#include "AssemblyU2DCSharp_CuponData807724487.h"
#include "AssemblyU2DCSharp_OfertasData2909397772.h"
#include "AssemblyU2DCSharp_OfertaData3417615771.h"
#include "AssemblyU2DCSharp_OfertasCRMDataServer217840361.h"
#include "AssemblyU2DCSharp_OfertasCRMDataGroup273423897.h"
#include "AssemblyU2DCSharp_OfertasCRMDataExclusivas2158992831.h"
#include "AssemblyU2DCSharp_OfertaCRMData637956887.h"
#include "AssemblyU2DCSharp_PublicationsData1837633585.h"
#include "AssemblyU2DCSharp_PublicationData473548758.h"
#include "AssemblyU2DCSharp_TabloidesData2107312651.h"
#include "AssemblyU2DCSharp_TabloideData1867721404.h"
#include "AssemblyU2DCSharp_AcougueData537282265.h"
#include "AssemblyU2DCSharp_CarnesData3441984818.h"
#include "AssemblyU2DCSharp_CortesData642513606.h"
#include "AssemblyU2DCSharp_PedidoData3443010735.h"
#include "AssemblyU2DCSharp_PedidoEnvioData1309077304.h"
#include "AssemblyU2DCSharp_MeuPedidoData2688356908.h"
#include "AssemblyU2DCSharp_VerticalSwap128095817.h"
#include "AssemblyU2DCSharp_WebCamBackground1986997129.h"
#include "AssemblyU2DCSharp_updateCupomTabloid933712416.h"
#include "AssemblyU2DCSharp_UniWebViewAndroidStaticListener2819366888.h"
#include "AssemblyU2DCSharp_UniWebViewInterface2459884048.h"
#include "AssemblyU2DCSharp_UniWebViewInterface_UnitySendMes2158631822.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "AssemblyU2DCSharp_UniWebView_PageStartedDelegate2373082257.h"
#include "AssemblyU2DCSharp_UniWebView_PageFinishedDelegate1325724172.h"
#include "AssemblyU2DCSharp_UniWebView_PageErrorReceivedDele4149620697.h"
#include "AssemblyU2DCSharp_UniWebView_MessageReceivedDelega2778163623.h"
#include "AssemblyU2DCSharp_UniWebView_ShouldCloseDelegate2209923748.h"
#include "AssemblyU2DCSharp_UniWebView_KeyCodeReceivedDelega3758979212.h"
#include "AssemblyU2DCSharp_UniWebView_OreintationChangedDel3987443383.h"
#include "AssemblyU2DCSharp_UniWebView_OnWebContentProcessTe1791934381.h"
#include "AssemblyU2DCSharp_UniWebView_U3CGetHTMLContentU3Ec1968010125.h"
#include "AssemblyU2DCSharp_UniWebViewHelper823175735.h"
#include "AssemblyU2DCSharp_UniWebViewLogger946769945.h"
#include "AssemblyU2DCSharp_UniWebViewLogger_Level2924530158.h"
#include "AssemblyU2DCSharp_UniWebViewMessage4192712350.h"
#include "AssemblyU2DCSharp_UniWebViewNativeListener795571060.h"
#include "AssemblyU2DCSharp_UniWebViewNativeResultPayload996691953.h"
#include "AssemblyU2DCSharp_UniWebViewToolbarPosition920849755.h"
#include "AssemblyU2DCSharp_UniWebViewTransitionEdge1338482235.h"
#include "AssemblyU2DCSharp_AttachedAudio711869554.h"
#include "AssemblyU2DCSharp_UVT_PlayDirection3288864031.h"
#include "AssemblyU2DCSharp_UVT_PlayMode874588163.h"
#include "AssemblyU2DCSharp_UVT_PlayState1348117041.h"
#include "AssemblyU2DCSharp_DigitsLocation3189585627.h"
#include "AssemblyU2DCSharp_LowMemoryMode1214027928.h"
#include "AssemblyU2DCSharp_TextureType3281615573.h"
#include "AssemblyU2DCSharp_VideoTexture_FullScreen1562162522.h"
#include "AssemblyU2DCSharp_VideoTexture_Material3546220134.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour2848361927.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour_U3CResetTo338551677.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper638710250.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaState2586048626.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaType3131497273.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour1927425041.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3993405419.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour1850077856.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour2989373670.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErr3746290221.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH543367463.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan4108278998.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler4275497481.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour439862723.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour1735871187.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer1535493961.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB2489055709.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer2749231339.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponent900168586.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1171410903.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour3170674893.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour1204933533.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour2222860085.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour2508606743.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour3213561028.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour3695833539.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetB582925864.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehavi442856755.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour3457144850.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour892056475.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour1278464685.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour3289283011.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin3016919996.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3391125558.h"
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer1585639557.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (SimpleJSON_t561564090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (Sombra_t2482075392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4901[3] = 
{
	Sombra_t2482075392::get_offset_of_target_2(),
	Sombra_t2482075392::get_offset_of_bias_3(),
	Sombra_t2482075392::get_offset_of_ok_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (U3CStartU3Ec__Iterator7F_t1445136119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4902[3] = 
{
	U3CStartU3Ec__Iterator7F_t1445136119::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator7F_t1445136119::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator7F_t1445136119::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (TabloideManager_t3674153819), -1, sizeof(TabloideManager_t3674153819_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4903[15] = 
{
	TabloideManager_t3674153819::get_offset_of_scrollRect_2(),
	TabloideManager_t3674153819_StaticFields::get_offset_of_tabloidesData_3(),
	TabloideManager_t3674153819_StaticFields::get_offset_of_textoLst_4(),
	TabloideManager_t3674153819_StaticFields::get_offset_of_linkLst_5(),
	TabloideManager_t3674153819_StaticFields::get_offset_of_imgLst_6(),
	TabloideManager_t3674153819::get_offset_of_loadingObj_7(),
	TabloideManager_t3674153819::get_offset_of_popup_8(),
	TabloideManager_t3674153819::get_offset_of_itemPrefab_9(),
	TabloideManager_t3674153819::get_offset_of_containerItens_10(),
	TabloideManager_t3674153819::get_offset_of_currentItem_11(),
	TabloideManager_t3674153819::get_offset_of_loadUtil_12(),
	TabloideManager_t3674153819::get_offset_of_precoCupom_13(),
	TabloideManager_t3674153819::get_offset_of_objCounter_14(),
	TabloideManager_t3674153819_StaticFields::get_offset_of_currentJsonTxt_15(),
	TabloideManager_t3674153819::get_offset_of_imgLoadedCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (U3CILoadPublishU3Ec__Iterator80_t3858799123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4904[11] = 
{
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CmessageU3E__4_4(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CU24s_347U3E__5_5(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CU24s_348U3E__6_6(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CtabloideDataU3E__7_7(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U24PC_8(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U24current_9(),
	U3CILoadPublishU3Ec__Iterator80_t3858799123::get_offset_of_U3CU3Ef__this_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (U3CILoadImgsU3Ec__Iterator81_t781055235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4905[6] = 
{
	U3CILoadImgsU3Ec__Iterator81_t781055235::get_offset_of_U3CiU3E__0_0(),
	U3CILoadImgsU3Ec__Iterator81_t781055235::get_offset_of_U3CwwwU3E__1_1(),
	U3CILoadImgsU3Ec__Iterator81_t781055235::get_offset_of_U3CsptU3E__2_2(),
	U3CILoadImgsU3Ec__Iterator81_t781055235::get_offset_of_U24PC_3(),
	U3CILoadImgsU3Ec__Iterator81_t781055235::get_offset_of_U24current_4(),
	U3CILoadImgsU3Ec__Iterator81_t781055235::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (U3CLoadLoadedsU3Ec__Iterator82_t3625498093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4906[4] = 
{
	U3CLoadLoadedsU3Ec__Iterator82_t3625498093::get_offset_of_U3CiU3E__0_0(),
	U3CLoadLoadedsU3Ec__Iterator82_t3625498093::get_offset_of_U24PC_1(),
	U3CLoadLoadedsU3Ec__Iterator82_t3625498093::get_offset_of_U24current_2(),
	U3CLoadLoadedsU3Ec__Iterator82_t3625498093::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { sizeof (TabloideValues_t99048372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4907[6] = 
{
	TabloideValues_t99048372::get_offset_of_id_2(),
	TabloideValues_t99048372::get_offset_of_txt_3(),
	TabloideValues_t99048372::get_offset_of_link_4(),
	TabloideValues_t99048372::get_offset_of_spt_5(),
	TabloideValues_t99048372::get_offset_of_txtTxt_6(),
	TabloideValues_t99048372::get_offset_of_imgImg_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { sizeof (TiltWindow_t1575826733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4908[4] = 
{
	TiltWindow_t1575826733::get_offset_of_range_2(),
	TiltWindow_t1575826733::get_offset_of_mTrans_3(),
	TiltWindow_t1575826733::get_offset_of_mStart_4(),
	TiltWindow_t1575826733::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { sizeof (UpdateData_t1697049651), -1, sizeof(UpdateData_t1697049651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4909[2] = 
{
	UpdateData_t1697049651_StaticFields::get_offset_of_onUpdate_2(),
	UpdateData_t1697049651::get_offset_of_loadingObj_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { sizeof (U3CILoginU3Ec__Iterator83_t2470511182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4910[10] = 
{
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CcodeU3E__4_4(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CmessageU3E__5_5(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CuserDataU3E__6_6(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U24PC_7(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U24current_8(),
	U3CILoginU3Ec__Iterator83_t2470511182::get_offset_of_U3CU3Ef__this_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { sizeof (UploadPhoto_t2718662801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4911[4] = 
{
	UploadPhoto_t2718662801::get_offset_of_image_2(),
	UploadPhoto_t2718662801::get_offset_of_imgPath_3(),
	UploadPhoto_t2718662801::get_offset_of_uploadUrl_4(),
	UploadPhoto_t2718662801::get_offset_of_loadingObj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { sizeof (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4912[10] = 
{
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_localFileName_0(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_uploadURL_1(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U3ClocalFileU3E__0_2(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U3CpostFormU3E__1_3(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U3CuploadU3E__2_4(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U24PC_5(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U24current_6(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U3CU24U3ElocalFileName_7(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U3CU24U3EuploadURL_8(),
	U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787::get_offset_of_U3CU3Ef__this_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { sizeof (U3CloadImageByWWWU3Ec__Iterator85_t364354809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4913[6] = 
{
	U3CloadImageByWWWU3Ec__Iterator85_t364354809::get_offset_of_path_0(),
	U3CloadImageByWWWU3Ec__Iterator85_t364354809::get_offset_of_U3ClocalFileU3E__0_1(),
	U3CloadImageByWWWU3Ec__Iterator85_t364354809::get_offset_of_U24PC_2(),
	U3CloadImageByWWWU3Ec__Iterator85_t364354809::get_offset_of_U24current_3(),
	U3CloadImageByWWWU3Ec__Iterator85_t364354809::get_offset_of_U3CU24U3Epath_4(),
	U3CloadImageByWWWU3Ec__Iterator85_t364354809::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { sizeof (UserData_t4092646965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4914[26] = 
{
	UserData_t4092646965::get_offset_of_nome_0(),
	UserData_t4092646965::get_offset_of_sobrenome_1(),
	UserData_t4092646965::get_offset_of_cpf_2(),
	UserData_t4092646965::get_offset_of_telefone_3(),
	UserData_t4092646965::get_offset_of_email_4(),
	UserData_t4092646965::get_offset_of_senha_5(),
	UserData_t4092646965::get_offset_of_foto_6(),
	UserData_t4092646965::get_offset_of_cep_7(),
	UserData_t4092646965::get_offset_of_rua_8(),
	UserData_t4092646965::get_offset_of_bairro_9(),
	UserData_t4092646965::get_offset_of_numero_10(),
	UserData_t4092646965::get_offset_of_complemento_11(),
	UserData_t4092646965::get_offset_of_estado_12(),
	UserData_t4092646965::get_offset_of_cidade_13(),
	UserData_t4092646965::get_offset_of_cupons_14(),
	UserData_t4092646965::get_offset_of_id_participante_15(),
	UserData_t4092646965::get_offset_of_id_16(),
	UserData_t4092646965::get_offset_of_spins_17(),
	UserData_t4092646965::get_offset_of_idade_18(),
	UserData_t4092646965::get_offset_of_sexo_19(),
	UserData_t4092646965::get_offset_of_pontos_20(),
	UserData_t4092646965::get_offset_of_pontos_disponiveis_21(),
	UserData_t4092646965::get_offset_of_spins_disponiveis_22(),
	UserData_t4092646965::get_offset_of_cupons_disponiveis_23(),
	UserData_t4092646965::get_offset_of_saldo_acumulado_24(),
	UserData_t4092646965::get_offset_of_dt_nasc_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { sizeof (CuponData_t807724487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4915[6] = 
{
	CuponData_t807724487::get_offset_of_valor_0(),
	CuponData_t807724487::get_offset_of_pts_1(),
	CuponData_t807724487::get_offset_of_promocao_2(),
	CuponData_t807724487::get_offset_of_id_3(),
	CuponData_t807724487::get_offset_of_dataExpira_4(),
	CuponData_t807724487::get_offset_of_used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { sizeof (OfertasData_t2909397772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4916[1] = 
{
	OfertasData_t2909397772::get_offset_of_ofertas_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { sizeof (OfertaData_t3417615771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4917[15] = 
{
	OfertaData_t3417615771::get_offset_of_id_oferta_0(),
	OfertaData_t3417615771::get_offset_of_desconto_1(),
	OfertaData_t3417615771::get_offset_of_pague_2(),
	OfertaData_t3417615771::get_offset_of_leve_3(),
	OfertaData_t3417615771::get_offset_of_unidade_4(),
	OfertaData_t3417615771::get_offset_of_peso_5(),
	OfertaData_t3417615771::get_offset_of_imagem_6(),
	OfertaData_t3417615771::get_offset_of_titulo_7(),
	OfertaData_t3417615771::get_offset_of_ean_8(),
	OfertaData_t3417615771::get_offset_of_texto_9(),
	OfertaData_t3417615771::get_offset_of_datainicial_10(),
	OfertaData_t3417615771::get_offset_of_datafinal_11(),
	OfertaData_t3417615771::get_offset_of_link_12(),
	OfertaData_t3417615771::get_offset_of_dataAquisicao_13(),
	OfertaData_t3417615771::get_offset_of_aderiu_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { sizeof (OfertasCRMDataServer_t217840361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4918[1] = 
{
	OfertasCRMDataServer_t217840361::get_offset_of_ofertas_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { sizeof (OfertasCRMDataGroup_t273423897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4919[2] = 
{
	OfertasCRMDataGroup_t273423897::get_offset_of_preco_0(),
	OfertasCRMDataGroup_t273423897::get_offset_of_depor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { sizeof (OfertasCRMDataExclusivas_t2158992831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4920[1] = 
{
	OfertasCRMDataExclusivas_t2158992831::get_offset_of_ofertas_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { sizeof (OfertaCRMData_t637956887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4921[20] = 
{
	OfertaCRMData_t637956887::get_offset_of_id_oferta_0(),
	OfertaCRMData_t637956887::get_offset_of_tipo_1(),
	OfertaCRMData_t637956887::get_offset_of_unidade_2(),
	OfertaCRMData_t637956887::get_offset_of_desconto_3(),
	OfertaCRMData_t637956887::get_offset_of_pague_4(),
	OfertaCRMData_t637956887::get_offset_of_leve_5(),
	OfertaCRMData_t637956887::get_offset_of_id_crm_6(),
	OfertaCRMData_t637956887::get_offset_of_peso_7(),
	OfertaCRMData_t637956887::get_offset_of_preco_8(),
	OfertaCRMData_t637956887::get_offset_of_de_9(),
	OfertaCRMData_t637956887::get_offset_of_por_10(),
	OfertaCRMData_t637956887::get_offset_of_imagem_11(),
	OfertaCRMData_t637956887::get_offset_of_titulo_12(),
	OfertaCRMData_t637956887::get_offset_of_texto_13(),
	OfertaCRMData_t637956887::get_offset_of_datainicial_14(),
	OfertaCRMData_t637956887::get_offset_of_datafinal_15(),
	OfertaCRMData_t637956887::get_offset_of_link_16(),
	OfertaCRMData_t637956887::get_offset_of_ean_17(),
	OfertaCRMData_t637956887::get_offset_of_dataAquisicao_18(),
	OfertaCRMData_t637956887::get_offset_of_aderiu_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { sizeof (PublicationsData_t1837633585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4922[1] = 
{
	PublicationsData_t1837633585::get_offset_of_publicacoes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { sizeof (PublicationData_t473548758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4923[7] = 
{
	PublicationData_t473548758::get_offset_of_id_publicacao_0(),
	PublicationData_t473548758::get_offset_of_favoritou_1(),
	PublicationData_t473548758::get_offset_of_imagem_2(),
	PublicationData_t473548758::get_offset_of_titulo_3(),
	PublicationData_t473548758::get_offset_of_texto_4(),
	PublicationData_t473548758::get_offset_of_datainicial_5(),
	PublicationData_t473548758::get_offset_of_link_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { sizeof (TabloidesData_t2107312651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4924[1] = 
{
	TabloidesData_t2107312651::get_offset_of_tabloides_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { sizeof (TabloideData_t1867721404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4925[5] = 
{
	TabloideData_t1867721404::get_offset_of_valor_0(),
	TabloideData_t1867721404::get_offset_of_imagem_1(),
	TabloideData_t1867721404::get_offset_of_texto_2(),
	TabloideData_t1867721404::get_offset_of_link_3(),
	TabloideData_t1867721404::get_offset_of_id_tabloide_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (AcougueData_t537282265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4926[2] = 
{
	AcougueData_t537282265::get_offset_of_carnes_0(),
	AcougueData_t537282265::get_offset_of_posicao_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (CarnesData_t3441984818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4927[7] = 
{
	CarnesData_t3441984818::get_offset_of_nome_produto_0(),
	CarnesData_t3441984818::get_offset_of_corte_1(),
	CarnesData_t3441984818::get_offset_of_observacao_2(),
	CarnesData_t3441984818::get_offset_of_peso_3(),
	CarnesData_t3441984818::get_offset_of_imagem_4(),
	CarnesData_t3441984818::get_offset_of_id_carne_5(),
	CarnesData_t3441984818::get_offset_of_cortes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (CortesData_t642513606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4928[2] = 
{
	CortesData_t642513606::get_offset_of_corte_0(),
	CortesData_t642513606::get_offset_of_id_corte_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (PedidoData_t3443010735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4929[15] = 
{
	PedidoData_t3443010735::get_offset_of_nome_0(),
	PedidoData_t3443010735::get_offset_of_timeStamp_1(),
	PedidoData_t3443010735::get_offset_of_peso_2(),
	PedidoData_t3443010735::get_offset_of_observacoes_3(),
	PedidoData_t3443010735::get_offset_of_nome_loja_4(),
	PedidoData_t3443010735::get_offset_of_idCarne_5(),
	PedidoData_t3443010735::get_offset_of_idCorte_6(),
	PedidoData_t3443010735::get_offset_of_quantidade_7(),
	PedidoData_t3443010735::get_offset_of_status_pedido_8(),
	PedidoData_t3443010735::get_offset_of_avaliou_9(),
	PedidoData_t3443010735::get_offset_of_id_pedido_10(),
	PedidoData_t3443010735::get_offset_of_idPedido_11(),
	PedidoData_t3443010735::get_offset_of_carnes_12(),
	PedidoData_t3443010735::get_offset_of_info_13(),
	PedidoData_t3443010735::get_offset_of_sprite_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (PedidoEnvioData_t1309077304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4930[7] = 
{
	PedidoEnvioData_t1309077304::get_offset_of_login_0(),
	PedidoEnvioData_t1309077304::get_offset_of_senha_1(),
	PedidoEnvioData_t1309077304::get_offset_of_loja_2(),
	PedidoEnvioData_t1309077304::get_offset_of_nota_3(),
	PedidoEnvioData_t1309077304::get_offset_of_passarPeso_4(),
	PedidoEnvioData_t1309077304::get_offset_of_pedidos_5(),
	PedidoEnvioData_t1309077304::get_offset_of_codePedido_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (MeuPedidoData_t2688356908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4931[2] = 
{
	MeuPedidoData_t2688356908::get_offset_of_posicao_0(),
	MeuPedidoData_t2688356908::get_offset_of_pedidos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (VerticalSwap_t128095817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4932[8] = 
{
	VerticalSwap_t128095817::get_offset_of_minAxisY_2(),
	VerticalSwap_t128095817::get_offset_of_maxAxisX_3(),
	VerticalSwap_t128095817::get_offset_of_multiplier_4(),
	VerticalSwap_t128095817::get_offset_of_scrollRect_5(),
	VerticalSwap_t128095817::get_offset_of_onScroll_6(),
	VerticalSwap_t128095817::get_offset_of_startPosition_7(),
	VerticalSwap_t128095817::get_offset_of_onScrollFlag_8(),
	VerticalSwap_t128095817::get_offset_of_endUpdate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { sizeof (WebCamBackground_t1986997129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4933[1] = 
{
	WebCamBackground_t1986997129::get_offset_of_camTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { sizeof (updateCupomTabloid_t933712416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4934[7] = 
{
	updateCupomTabloid_t933712416::get_offset_of_container_2(),
	updateCupomTabloid_t933712416::get_offset_of_cupom10_3(),
	updateCupomTabloid_t933712416::get_offset_of_cupom50_4(),
	updateCupomTabloid_t933712416::get_offset_of_cupom100_5(),
	updateCupomTabloid_t933712416::get_offset_of_cupom200_6(),
	updateCupomTabloid_t933712416::get_offset_of_cupom500_7(),
	updateCupomTabloid_t933712416::get_offset_of_cupomValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (UniWebViewAndroidStaticListener_t2819366888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (UniWebViewInterface_t2459884048), -1, sizeof(UniWebViewInterface_t2459884048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4936[2] = 
{
	0,
	UniWebViewInterface_t2459884048_StaticFields::get_offset_of_correctPlatform_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (UnitySendMessageDelegate_t2158631822), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (UniWebView_t424341801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4938[24] = 
{
	UniWebView_t424341801::get_offset_of_id_2(),
	UniWebView_t424341801::get_offset_of_listener_3(),
	UniWebView_t424341801::get_offset_of_isPortrait_4(),
	UniWebView_t424341801::get_offset_of_urlOnStart_5(),
	UniWebView_t424341801::get_offset_of_showOnStart_6(),
	UniWebView_t424341801::get_offset_of_isBackButton_7(),
	UniWebView_t424341801::get_offset_of_isZoom_8(),
	UniWebView_t424341801::get_offset_of_actions_9(),
	UniWebView_t424341801::get_offset_of_payloadActions_10(),
	UniWebView_t424341801::get_offset_of_fullScreen_11(),
	UniWebView_t424341801::get_offset_of_frame_12(),
	UniWebView_t424341801::get_offset_of_referenceRectTransform_13(),
	UniWebView_t424341801::get_offset_of_useToolbar_14(),
	UniWebView_t424341801::get_offset_of_toolbarPosition_15(),
	UniWebView_t424341801::get_offset_of_started_16(),
	UniWebView_t424341801::get_offset_of_backgroundColor_17(),
	UniWebView_t424341801::get_offset_of_OnPageStarted_18(),
	UniWebView_t424341801::get_offset_of_OnPageFinished_19(),
	UniWebView_t424341801::get_offset_of_OnPageErrorReceived_20(),
	UniWebView_t424341801::get_offset_of_OnMessageReceived_21(),
	UniWebView_t424341801::get_offset_of_OnShouldClose_22(),
	UniWebView_t424341801::get_offset_of_OnKeyCodeReceived_23(),
	UniWebView_t424341801::get_offset_of_OnOreintationChanged_24(),
	UniWebView_t424341801::get_offset_of_OnWebContentProcessTerminated_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (PageStartedDelegate_t2373082257), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { sizeof (PageFinishedDelegate_t1325724172), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (PageErrorReceivedDelegate_t4149620697), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (MessageReceivedDelegate_t2778163623), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { sizeof (ShouldCloseDelegate_t2209923748), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (KeyCodeReceivedDelegate_t3758979212), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (OreintationChangedDelegate_t3987443383), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (OnWebContentProcessTerminatedDelegate_t1791934381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4947[1] = 
{
	U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125::get_offset_of_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (UniWebViewHelper_t823175735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (UniWebViewLogger_t946769945), -1, sizeof(UniWebViewLogger_t946769945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4949[2] = 
{
	UniWebViewLogger_t946769945_StaticFields::get_offset_of_instance_0(),
	UniWebViewLogger_t946769945::get_offset_of_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { sizeof (Level_t2924530158)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4950[6] = 
{
	Level_t2924530158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (UniWebViewMessage_t4192712350)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4951[4] = 
{
	UniWebViewMessage_t4192712350::get_offset_of_U3CRawMessageU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UniWebViewMessage_t4192712350::get_offset_of_U3CSchemeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UniWebViewMessage_t4192712350::get_offset_of_U3CPathU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UniWebViewMessage_t4192712350::get_offset_of_U3CArgsU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (UniWebViewNativeListener_t795571060), -1, sizeof(UniWebViewNativeListener_t795571060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4952[2] = 
{
	UniWebViewNativeListener_t795571060_StaticFields::get_offset_of_listeners_2(),
	UniWebViewNativeListener_t795571060::get_offset_of_webView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { sizeof (UniWebViewNativeResultPayload_t996691953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4953[3] = 
{
	UniWebViewNativeResultPayload_t996691953::get_offset_of_identifier_0(),
	UniWebViewNativeResultPayload_t996691953::get_offset_of_resultCode_1(),
	UniWebViewNativeResultPayload_t996691953::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (UniWebViewToolbarPosition_t920849755)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4954[3] = 
{
	UniWebViewToolbarPosition_t920849755::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (UniWebViewTransitionEdge_t1338482235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4955[6] = 
{
	UniWebViewTransitionEdge_t1338482235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (AttachedAudio_t711869554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4956[4] = 
{
	AttachedAudio_t711869554::get_offset_of_attachedAudioSource_0(),
	AttachedAudio_t711869554::get_offset_of_frameIndex_1(),
	AttachedAudio_t711869554::get_offset_of_fps_2(),
	AttachedAudio_t711869554::get_offset_of_togglePlay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (UVT_PlayDirection_t3288864031)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4957[3] = 
{
	UVT_PlayDirection_t3288864031::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (UVT_PlayMode_t874588163)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4958[5] = 
{
	UVT_PlayMode_t874588163::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (UVT_PlayState_t1348117041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4959[3] = 
{
	UVT_PlayState_t1348117041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { sizeof (DigitsLocation_t3189585627)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4960[3] = 
{
	DigitsLocation_t3189585627::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { sizeof (LowMemoryMode_t1214027928)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4961[4] = 
{
	LowMemoryMode_t1214027928::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { sizeof (TextureType_t3281615573)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4962[6] = 
{
	TextureType_t3281615573::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { sizeof (VideoTexture_FullScreen_t1562162522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4963[46] = 
{
	VideoTexture_FullScreen_t1562162522::get_offset_of_FPS_2(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_firstFrame_3(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_lastFrame_4(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_FileName_5(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_digitsFormat_6(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_digitsLocation_7(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_aspectRatio_8(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_playMode_9(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_pingpongStartsWithReverse_10(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_numberOfLoops_11(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_lowMemoryMode_12(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_ctiTexture_13(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_backgroundTexture_14(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_scrollBarTexture_15(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_scrollBarLength_16(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_scrollBarHeight_17(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_scrollBarOffset_18(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_timecodeSize_19(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_hideBackground_20(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_showScrollBar_21(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_showTimecode_22(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_enableTogglePlay_23(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_enableAudio_24(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_forceAudioSync_25(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_autoPlay_26(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_autoHideWhenDone_27(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_autoLoadLevelWhenDone_28(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_LevelToLoad_29(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_playState_30(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_playDirection_31(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_enableControls_32(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_scrubbing_33(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_audioAttached_34(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_loopNumber_35(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_indexStr_36(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_newTex_37(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_lastTex_38(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_playFactor_39(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_currentPosition_40(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_index_41(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_intIndex_42(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_lastIndex_43(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_myAudio_44(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_CTI_45(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_style_46(),
	VideoTexture_FullScreen_t1562162522::get_offset_of_enableGUI_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (VideoTexture_Material_t3546220134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4964[40] = 
{
	VideoTexture_Material_t3546220134::get_offset_of_FPS_2(),
	VideoTexture_Material_t3546220134::get_offset_of_firstFrame_3(),
	VideoTexture_Material_t3546220134::get_offset_of_lastFrame_4(),
	VideoTexture_Material_t3546220134::get_offset_of_FileName_5(),
	VideoTexture_Material_t3546220134::get_offset_of_digitsFormat_6(),
	VideoTexture_Material_t3546220134::get_offset_of_digitsLocation_7(),
	VideoTexture_Material_t3546220134::get_offset_of_playMode_8(),
	VideoTexture_Material_t3546220134::get_offset_of_pingpongStartsWithReverse_9(),
	VideoTexture_Material_t3546220134::get_offset_of_numberOfLoops_10(),
	VideoTexture_Material_t3546220134::get_offset_of_textureType_11(),
	VideoTexture_Material_t3546220134::get_offset_of_lowMemoryMode_12(),
	VideoTexture_Material_t3546220134::get_offset_of_scrollBar_13(),
	VideoTexture_Material_t3546220134::get_offset_of_CTI_14(),
	VideoTexture_Material_t3546220134::get_offset_of_timeCode_15(),
	VideoTexture_Material_t3546220134::get_offset_of_controlLayer_16(),
	VideoTexture_Material_t3546220134::get_offset_of_playState_17(),
	VideoTexture_Material_t3546220134::get_offset_of_playDirection_18(),
	VideoTexture_Material_t3546220134::get_offset_of_sharedMaterial_19(),
	VideoTexture_Material_t3546220134::get_offset_of_enableAudio_20(),
	VideoTexture_Material_t3546220134::get_offset_of_forceAudioSync_21(),
	VideoTexture_Material_t3546220134::get_offset_of_enableControls_22(),
	VideoTexture_Material_t3546220134::get_offset_of_autoPlay_23(),
	VideoTexture_Material_t3546220134::get_offset_of_autoDestruct_24(),
	VideoTexture_Material_t3546220134::get_offset_of_autoLoadLevelWhenDone_25(),
	VideoTexture_Material_t3546220134::get_offset_of_LevelToLoad_26(),
	VideoTexture_Material_t3546220134::get_offset_of_currentPosition_27(),
	VideoTexture_Material_t3546220134::get_offset_of_ctiPosition_28(),
	VideoTexture_Material_t3546220134::get_offset_of_currentLoop_29(),
	VideoTexture_Material_t3546220134::get_offset_of_scrollBarLength_30(),
	VideoTexture_Material_t3546220134::get_offset_of_scrubbing_31(),
	VideoTexture_Material_t3546220134::get_offset_of_audioAttached_32(),
	VideoTexture_Material_t3546220134::get_offset_of_myAudio_33(),
	VideoTexture_Material_t3546220134::get_offset_of_texType_34(),
	VideoTexture_Material_t3546220134::get_offset_of_indexStr_35(),
	VideoTexture_Material_t3546220134::get_offset_of_newTex_36(),
	VideoTexture_Material_t3546220134::get_offset_of_lastTex_37(),
	VideoTexture_Material_t3546220134::get_offset_of_playFactor_38(),
	VideoTexture_Material_t3546220134::get_offset_of_index_39(),
	VideoTexture_Material_t3546220134::get_offset_of_intIndex_40(),
	VideoTexture_Material_t3546220134::get_offset_of_lastIndex_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (VideoPlaybackBehaviour_t2848361927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4965[17] = 
{
	VideoPlaybackBehaviour_t2848361927::get_offset_of_m_path_2(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_m_playTexture_3(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_m_busyTexture_4(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_m_errorTexture_5(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_m_autoPlay_6(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mVideoPlayer_7(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mIsInited_8(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mIsPrepared_9(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mAppPaused_10(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mVideoTexture_11(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mKeyframeTexture_12(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mMediaType_13(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mCurrentState_14(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mSeekPosition_15(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_isPlayableOnTexture_16(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mIconPlane_17(),
	VideoPlaybackBehaviour_t2848361927::get_offset_of_mIconPlaneActive_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4966[2] = 
{
	U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677::get_offset_of_U24PC_0(),
	U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (VideoPlayerHelper_t638710250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4967[3] = 
{
	VideoPlayerHelper_t638710250::get_offset_of_mFilename_0(),
	VideoPlayerHelper_t638710250::get_offset_of_mFullScreenFilename_1(),
	VideoPlayerHelper_t638710250::get_offset_of_mVideoPlayerPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (MediaState_t2586048626)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4968[9] = 
{
	MediaState_t2586048626::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (MediaType_t3131497273)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4969[4] = 
{
	MediaType_t3131497273::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (BackgroundPlaneBehaviour_t1927425041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { sizeof (CloudRecoBehaviour_t3993405419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { sizeof (CylinderTargetBehaviour_t1850077856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (DatabaseLoadBehaviour_t2989373670), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { sizeof (DefaultInitializationErrorHandler_t3746290221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4974[3] = 
{
	0,
	DefaultInitializationErrorHandler_t3746290221::get_offset_of_mErrorText_3(),
	DefaultInitializationErrorHandler_t3746290221::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { sizeof (DefaultSmartTerrainEventHandler_t543367463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4975[3] = 
{
	DefaultSmartTerrainEventHandler_t543367463::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t543367463::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t543367463::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { sizeof (DefaultTrackableEventHandler_t4108278998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4976[1] = 
{
	DefaultTrackableEventHandler_t4108278998::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { sizeof (GLErrorHandler_t4275497481), -1, sizeof(GLErrorHandler_t4275497481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4977[3] = 
{
	0,
	GLErrorHandler_t4275497481_StaticFields::get_offset_of_mErrorText_3(),
	GLErrorHandler_t4275497481_StaticFields::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (HideExcessAreaBehaviour_t439862723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (ImageTargetBehaviour_t1735871187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4979[2] = 
{
	ImageTargetBehaviour_t1735871187::get_offset_of_mTrackableBehaviour_24(),
	ImageTargetBehaviour_t1735871187::get_offset_of__registred_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (AndroidUnityPlayer_t1535493961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4980[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t1535493961::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t1535493961::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t1535493961::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t1535493961::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { sizeof (ComponentFactoryStarterBehaviour_t2489055709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { sizeof (IOSUnityPlayer_t2749231339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4982[1] = 
{
	IOSUnityPlayer_t2749231339::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (VuforiaBehaviourComponentFactory_t900168586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (KeepAliveBehaviour_t1171410903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (MarkerBehaviour_t3170674893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (MaskOutBehaviour_t1204933533), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { sizeof (MultiTargetBehaviour_t2222860085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { sizeof (ObjectTargetBehaviour_t2508606743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (PropBehaviour_t3213561028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (ReconstructionBehaviour_t3695833539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (ReconstructionFromTargetBehaviour_t582925864), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (SmartTerrainTrackerBehaviour_t442856755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (SurfaceBehaviour_t3457144850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (TextRecoBehaviour_t892056475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (TurnOffBehaviour_t1278464685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (TurnOffWordBehaviour_t3289283011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (UserDefinedTargetBuildingBehaviour_t3016919996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (VideoBackgroundBehaviour_t3391125558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (VideoTextureRenderer_t1585639557), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
