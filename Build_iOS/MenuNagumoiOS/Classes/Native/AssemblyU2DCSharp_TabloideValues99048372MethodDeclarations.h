﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloideValues
struct TabloideValues_t99048372;

#include "codegen/il2cpp-codegen.h"

// System.Void TabloideValues::.ctor()
extern "C"  void TabloideValues__ctor_m4075314023 (TabloideValues_t99048372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideValues::Start()
extern "C"  void TabloideValues_Start_m3022451815 (TabloideValues_t99048372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideValues::Update()
extern "C"  void TabloideValues_Update_m3507545222 (TabloideValues_t99048372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideValues::GoLink()
extern "C"  void TabloideValues_GoLink_m3310616191 (TabloideValues_t99048372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
