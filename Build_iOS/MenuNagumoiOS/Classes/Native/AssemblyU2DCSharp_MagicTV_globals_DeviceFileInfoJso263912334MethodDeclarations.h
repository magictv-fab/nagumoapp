﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B
struct U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B::.ctor()
extern "C"  void U3CpopulateIndexedBundlesU3Ec__AnonStorey9B__ctor_m3647667405 (U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B::<>m__39(MagicTV.vo.MetadataVO)
extern "C"  void U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_U3CU3Em__39_m1945560824 (U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334 * __this, MetadataVO_t2511256998 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
