﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.BitArray
struct  BitArray_t4163851164  : public Il2CppObject
{
public:
	// System.Int32[] ZXing.Common.BitArray::bits
	Int32U5BU5D_t3230847821* ___bits_0;
	// System.Int32 ZXing.Common.BitArray::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_bits_0() { return static_cast<int32_t>(offsetof(BitArray_t4163851164, ___bits_0)); }
	inline Int32U5BU5D_t3230847821* get_bits_0() const { return ___bits_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_bits_0() { return &___bits_0; }
	inline void set_bits_0(Int32U5BU5D_t3230847821* value)
	{
		___bits_0 = value;
		Il2CppCodeGenWriteBarrier(&___bits_0, value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(BitArray_t4163851164, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

struct BitArray_t4163851164_StaticFields
{
public:
	// System.Int32[] ZXing.Common.BitArray::_lookup
	Int32U5BU5D_t3230847821* ____lookup_2;

public:
	inline static int32_t get_offset_of__lookup_2() { return static_cast<int32_t>(offsetof(BitArray_t4163851164_StaticFields, ____lookup_2)); }
	inline Int32U5BU5D_t3230847821* get__lookup_2() const { return ____lookup_2; }
	inline Int32U5BU5D_t3230847821** get_address_of__lookup_2() { return &____lookup_2; }
	inline void set__lookup_2(Int32U5BU5D_t3230847821* value)
	{
		____lookup_2 = value;
		Il2CppCodeGenWriteBarrier(&____lookup_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
