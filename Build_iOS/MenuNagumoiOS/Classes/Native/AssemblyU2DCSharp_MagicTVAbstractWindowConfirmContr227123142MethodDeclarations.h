﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVAbstractWindowConfirmControllerScript
struct MagicTVAbstractWindowConfirmControllerScript_t227123142;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVAbstractWindowConfirmControllerScript::.ctor()
extern "C"  void MagicTVAbstractWindowConfirmControllerScript__ctor_m2659209621 (MagicTVAbstractWindowConfirmControllerScript_t227123142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowConfirmControllerScript::SetButtonCancelText(System.String)
extern "C"  void MagicTVAbstractWindowConfirmControllerScript_SetButtonCancelText_m77395252 (MagicTVAbstractWindowConfirmControllerScript_t227123142 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowConfirmControllerScript::RaiseCancel()
extern "C"  void MagicTVAbstractWindowConfirmControllerScript_RaiseCancel_m144081209 (MagicTVAbstractWindowConfirmControllerScript_t227123142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowConfirmControllerScript::ClickCancel()
extern "C"  void MagicTVAbstractWindowConfirmControllerScript_ClickCancel_m2559492821 (MagicTVAbstractWindowConfirmControllerScript_t227123142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVAbstractWindowConfirmControllerScript::Hide()
extern "C"  void MagicTVAbstractWindowConfirmControllerScript_Hide_m2774854097 (MagicTVAbstractWindowConfirmControllerScript_t227123142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
