﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.FBScrollbar
struct FBScrollbar_t2094246736;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.FBScrollbar/ScrollEvent
struct ScrollEvent_t3510971109;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t3355659985;
// UnityEngine.UI.Selectable
struct Selectable_t1885181538;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_FBScrollbar_Direct361841815.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_FBScrollbar_Scrol3510971109.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2847075725.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_FBScrollbar_Axis973134729.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"

// System.Void UnityEngine.UI.FBScrollbar::.ctor()
extern "C"  void FBScrollbar__ctor_m420766516 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.FBScrollbar::get_handleRect()
extern "C"  RectTransform_t972643934 * FBScrollbar_get_handleRect_m1632872839 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::set_handleRect(UnityEngine.RectTransform)
extern "C"  void FBScrollbar_set_handleRect_m2653475784 (FBScrollbar_t2094246736 * __this, RectTransform_t972643934 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.FBScrollbar/Direction UnityEngine.UI.FBScrollbar::get_direction()
extern "C"  int32_t FBScrollbar_get_direction_m2837202562 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::set_direction(UnityEngine.UI.FBScrollbar/Direction)
extern "C"  void FBScrollbar_set_direction_m698777501 (FBScrollbar_t2094246736 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.FBScrollbar::get_value()
extern "C"  float FBScrollbar_get_value_m3467960046 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::set_value(System.Single)
extern "C"  void FBScrollbar_set_value_m1925478693 (FBScrollbar_t2094246736 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.FBScrollbar::get_size()
extern "C"  float FBScrollbar_get_size_m1557790854 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::set_size(System.Single)
extern "C"  void FBScrollbar_set_size_m1273844285 (FBScrollbar_t2094246736 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.FBScrollbar::get_numberOfSteps()
extern "C"  int32_t FBScrollbar_get_numberOfSteps_m2140382850 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::set_numberOfSteps(System.Int32)
extern "C"  void FBScrollbar_set_numberOfSteps_m167136941 (FBScrollbar_t2094246736 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.FBScrollbar/ScrollEvent UnityEngine.UI.FBScrollbar::get_onValueChanged()
extern "C"  ScrollEvent_t3510971109 * FBScrollbar_get_onValueChanged_m1849394611 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::set_onValueChanged(UnityEngine.UI.FBScrollbar/ScrollEvent)
extern "C"  void FBScrollbar_set_onValueChanged_m398165930 (FBScrollbar_t2094246736 * __this, ScrollEvent_t3510971109 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.FBScrollbar::get_stepSize()
extern "C"  float FBScrollbar_get_stepSize_m3551930482 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::Rebuild(UnityEngine.UI.CanvasUpdate)
extern "C"  void FBScrollbar_Rebuild_m3751887201 (FBScrollbar_t2094246736 * __this, int32_t ___executing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::LayoutComplete()
extern "C"  void FBScrollbar_LayoutComplete_m2308087443 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::GraphicUpdateComplete()
extern "C"  void FBScrollbar_GraphicUpdateComplete_m625107452 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnEnable()
extern "C"  void FBScrollbar_OnEnable_m117319634 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnDisable()
extern "C"  void FBScrollbar_OnDisable_m4077845915 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::UpdateCachedReferences()
extern "C"  void FBScrollbar_UpdateCachedReferences_m1638268227 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::Set(System.Single)
extern "C"  void FBScrollbar_Set_m1866222007 (FBScrollbar_t2094246736 * __this, float ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::Set(System.Single,System.Boolean)
extern "C"  void FBScrollbar_Set_m2483979494 (FBScrollbar_t2094246736 * __this, float ___input0, bool ___sendCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnRectTransformDimensionsChange()
extern "C"  void FBScrollbar_OnRectTransformDimensionsChange_m189453784 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.FBScrollbar/Axis UnityEngine.UI.FBScrollbar::get_axis()
extern "C"  int32_t FBScrollbar_get_axis_m3249717892 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.FBScrollbar::get_reverseValue()
extern "C"  bool FBScrollbar_get_reverseValue_m3642978460 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::UpdateVisuals()
extern "C"  void FBScrollbar_UpdateVisuals_m3388637436 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::UpdateDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FBScrollbar_UpdateDrag_m2836952433 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.FBScrollbar::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  bool FBScrollbar_MayDrag_m2803746695 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FBScrollbar_OnBeginDrag_m1315655950 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FBScrollbar_OnDrag_m4212742683 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FBScrollbar_OnPointerDown_m2064606540 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.FBScrollbar::ClickRepeat(UnityEngine.EventSystems.PointerEventData)
extern "C"  Il2CppObject * FBScrollbar_ClickRepeat_m4156248401 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FBScrollbar_OnPointerUp_m1861978611 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnMove(UnityEngine.EventSystems.AxisEventData)
extern "C"  void FBScrollbar_OnMove_m514653514 (FBScrollbar_t2094246736 * __this, AxisEventData_t3355659985 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.FBScrollbar::FindSelectableOnLeft()
extern "C"  Selectable_t1885181538 * FBScrollbar_FindSelectableOnLeft_m4234213183 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.FBScrollbar::FindSelectableOnRight()
extern "C"  Selectable_t1885181538 * FBScrollbar_FindSelectableOnRight_m3556800262 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.FBScrollbar::FindSelectableOnUp()
extern "C"  Selectable_t1885181538 * FBScrollbar_FindSelectableOnUp_m2681774739 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.FBScrollbar::FindSelectableOnDown()
extern "C"  Selectable_t1885181538 * FBScrollbar_FindSelectableOnDown_m4014915866 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void FBScrollbar_OnInitializePotentialDrag_m1159050281 (FBScrollbar_t2094246736 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FBScrollbar::SetDirection(UnityEngine.UI.FBScrollbar/Direction,System.Boolean)
extern "C"  void FBScrollbar_SetDirection_m3836030439 (FBScrollbar_t2094246736 * __this, int32_t ___direction0, bool ___includeRectLayouts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.UI.FBScrollbar::UnityEngine.UI.ICanvasElement.get_transform()
extern "C"  Transform_t1659122786 * FBScrollbar_UnityEngine_UI_ICanvasElement_get_transform_m1998802759 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.FBScrollbar::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern "C"  bool FBScrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m467760157 (FBScrollbar_t2094246736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
