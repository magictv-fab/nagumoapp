﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.PDF417DetectorResult
struct PDF417DetectorResult_t1810156355;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Collections.Generic.List`1<ZXing.ResultPoint[]>
struct List_1_t2563349896;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.PDF417.Internal.PDF417DetectorResult ZXing.PDF417.Internal.Detector::detect(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Boolean)
extern "C"  PDF417DetectorResult_t1810156355 * Detector_detect_m536552826 (Il2CppObject * __this /* static, unused */, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, bool ___multiple2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.Detector::detect(System.Boolean,ZXing.Common.BitMatrix)
extern "C"  List_1_t2563349896 * Detector_detect_m727726119 (Il2CppObject * __this /* static, unused */, bool ___multiple0, BitMatrix_t1058711404 * ___bitMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.PDF417.Internal.Detector::findVertices(ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern "C"  ResultPointU5BU5D_t1195164344* Detector_findVertices_m4023633961 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___matrix0, int32_t ___startRow1, int32_t ___startColumn2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.Detector::copyToResult(ZXing.ResultPoint[],ZXing.ResultPoint[],System.Int32[])
extern "C"  void Detector_copyToResult_m241742398 (Il2CppObject * __this /* static, unused */, ResultPointU5BU5D_t1195164344* ___result0, ResultPointU5BU5D_t1195164344* ___tmpResult1, Int32U5BU5D_t3230847821* ___destinationIndexes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.PDF417.Internal.Detector::findRowsWithPattern(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  ResultPointU5BU5D_t1195164344* Detector_findRowsWithPattern_m2952263484 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___matrix0, int32_t ___height1, int32_t ___width2, int32_t ___startRow3, int32_t ___startColumn4, Int32U5BU5D_t3230847821* ___pattern5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.Detector::findGuardPattern(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32[],System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* Detector_findGuardPattern_m137108965 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___matrix0, int32_t ___column1, int32_t ___row2, int32_t ___width3, bool ___whiteFirst4, Int32U5BU5D_t3230847821* ___pattern5, Int32U5BU5D_t3230847821* ___counters6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.Detector::patternMatchVariance(System.Int32[],System.Int32[],System.Int32)
extern "C"  int32_t Detector_patternMatchVariance_m984343657 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, Int32U5BU5D_t3230847821* ___pattern1, int32_t ___maxIndividualVariance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.Detector::.cctor()
extern "C"  void Detector__cctor_m1304839565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
