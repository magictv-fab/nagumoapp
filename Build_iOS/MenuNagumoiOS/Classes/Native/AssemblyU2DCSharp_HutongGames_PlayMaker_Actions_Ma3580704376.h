﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerRegisterHost
struct  MasterServerRegisterHost_t3580704376  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRegisterHost::gameTypeName
	FsmString_t952858651 * ___gameTypeName_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRegisterHost::gameName
	FsmString_t952858651 * ___gameName_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRegisterHost::comment
	FsmString_t952858651 * ___comment_11;

public:
	inline static int32_t get_offset_of_gameTypeName_9() { return static_cast<int32_t>(offsetof(MasterServerRegisterHost_t3580704376, ___gameTypeName_9)); }
	inline FsmString_t952858651 * get_gameTypeName_9() const { return ___gameTypeName_9; }
	inline FsmString_t952858651 ** get_address_of_gameTypeName_9() { return &___gameTypeName_9; }
	inline void set_gameTypeName_9(FsmString_t952858651 * value)
	{
		___gameTypeName_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameTypeName_9, value);
	}

	inline static int32_t get_offset_of_gameName_10() { return static_cast<int32_t>(offsetof(MasterServerRegisterHost_t3580704376, ___gameName_10)); }
	inline FsmString_t952858651 * get_gameName_10() const { return ___gameName_10; }
	inline FsmString_t952858651 ** get_address_of_gameName_10() { return &___gameName_10; }
	inline void set_gameName_10(FsmString_t952858651 * value)
	{
		___gameName_10 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_10, value);
	}

	inline static int32_t get_offset_of_comment_11() { return static_cast<int32_t>(offsetof(MasterServerRegisterHost_t3580704376, ___comment_11)); }
	inline FsmString_t952858651 * get_comment_11() const { return ___comment_11; }
	inline FsmString_t952858651 ** get_address_of_comment_11() { return &___comment_11; }
	inline void set_comment_11(FsmString_t952858651 * value)
	{
		___comment_11 = value;
		Il2CppCodeGenWriteBarrier(&___comment_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
