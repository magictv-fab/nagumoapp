﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// ZXing.QrCode.Internal.FormatInformation
struct FormatInformation_t3156512315;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.BitMatrixParser
struct  BitMatrixParser_t3480170419  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.BitMatrixParser::bitMatrix
	BitMatrix_t1058711404 * ___bitMatrix_0;
	// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.BitMatrixParser::parsedVersion
	Version_t1953509534 * ___parsedVersion_1;
	// ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.BitMatrixParser::parsedFormatInfo
	FormatInformation_t3156512315 * ___parsedFormatInfo_2;
	// System.Boolean ZXing.QrCode.Internal.BitMatrixParser::mirrored
	bool ___mirrored_3;

public:
	inline static int32_t get_offset_of_bitMatrix_0() { return static_cast<int32_t>(offsetof(BitMatrixParser_t3480170419, ___bitMatrix_0)); }
	inline BitMatrix_t1058711404 * get_bitMatrix_0() const { return ___bitMatrix_0; }
	inline BitMatrix_t1058711404 ** get_address_of_bitMatrix_0() { return &___bitMatrix_0; }
	inline void set_bitMatrix_0(BitMatrix_t1058711404 * value)
	{
		___bitMatrix_0 = value;
		Il2CppCodeGenWriteBarrier(&___bitMatrix_0, value);
	}

	inline static int32_t get_offset_of_parsedVersion_1() { return static_cast<int32_t>(offsetof(BitMatrixParser_t3480170419, ___parsedVersion_1)); }
	inline Version_t1953509534 * get_parsedVersion_1() const { return ___parsedVersion_1; }
	inline Version_t1953509534 ** get_address_of_parsedVersion_1() { return &___parsedVersion_1; }
	inline void set_parsedVersion_1(Version_t1953509534 * value)
	{
		___parsedVersion_1 = value;
		Il2CppCodeGenWriteBarrier(&___parsedVersion_1, value);
	}

	inline static int32_t get_offset_of_parsedFormatInfo_2() { return static_cast<int32_t>(offsetof(BitMatrixParser_t3480170419, ___parsedFormatInfo_2)); }
	inline FormatInformation_t3156512315 * get_parsedFormatInfo_2() const { return ___parsedFormatInfo_2; }
	inline FormatInformation_t3156512315 ** get_address_of_parsedFormatInfo_2() { return &___parsedFormatInfo_2; }
	inline void set_parsedFormatInfo_2(FormatInformation_t3156512315 * value)
	{
		___parsedFormatInfo_2 = value;
		Il2CppCodeGenWriteBarrier(&___parsedFormatInfo_2, value);
	}

	inline static int32_t get_offset_of_mirrored_3() { return static_cast<int32_t>(offsetof(BitMatrixParser_t3480170419, ___mirrored_3)); }
	inline bool get_mirrored_3() const { return ___mirrored_3; }
	inline bool* get_address_of_mirrored_3() { return &___mirrored_3; }
	inline void set_mirrored_3(bool value)
	{
		___mirrored_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
