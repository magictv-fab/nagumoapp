﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2
struct U3CU3Ec__DisplayClass2_t1823032379;
// HutongGames.PlayMaker.FsmVarOverride
struct FsmVarOverride_t3235106805;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride3235106805.h"

// System.Void HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2::.ctor()
extern "C"  void U3CU3Ec__DisplayClass2__ctor_m1744875662 (U3CU3Ec__DisplayClass2_t1823032379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2::<UpdateOverrides>b__0(HutongGames.PlayMaker.FsmVarOverride)
extern "C"  bool U3CU3Ec__DisplayClass2_U3CUpdateOverridesU3Eb__0_m378674703 (U3CU3Ec__DisplayClass2_t1823032379 * __this, FsmVarOverride_t3235106805 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
