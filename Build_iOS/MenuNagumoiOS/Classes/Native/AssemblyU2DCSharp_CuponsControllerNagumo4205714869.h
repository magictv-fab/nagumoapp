﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CuponsControllerNagumo
struct CuponsControllerNagumo_t4205714869;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CuponsControllerNagumo
struct  CuponsControllerNagumo_t4205714869  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CuponsControllerNagumo::cuponsLst
	List_1_t747900261 * ___cuponsLst_3;
	// UnityEngine.UI.Text CuponsControllerNagumo::saldoTxt
	Text_t9039225 * ___saldoTxt_4;
	// UnityEngine.UI.Text CuponsControllerNagumo::infoTxt
	Text_t9039225 * ___infoTxt_5;
	// System.Int32 CuponsControllerNagumo::pontos
	int32_t ___pontos_6;
	// UnityEngine.GameObject CuponsControllerNagumo::content
	GameObject_t3674682005 * ___content_7;
	// UnityEngine.GameObject CuponsControllerNagumo::loadingObj
	GameObject_t3674682005 * ___loadingObj_8;
	// UnityEngine.GameObject CuponsControllerNagumo::popup
	GameObject_t3674682005 * ___popup_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CuponsControllerNagumo::cuponsInstantiatedsLst
	List_1_t747900261 * ___cuponsInstantiatedsLst_10;

public:
	inline static int32_t get_offset_of_cuponsLst_3() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___cuponsLst_3)); }
	inline List_1_t747900261 * get_cuponsLst_3() const { return ___cuponsLst_3; }
	inline List_1_t747900261 ** get_address_of_cuponsLst_3() { return &___cuponsLst_3; }
	inline void set_cuponsLst_3(List_1_t747900261 * value)
	{
		___cuponsLst_3 = value;
		Il2CppCodeGenWriteBarrier(&___cuponsLst_3, value);
	}

	inline static int32_t get_offset_of_saldoTxt_4() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___saldoTxt_4)); }
	inline Text_t9039225 * get_saldoTxt_4() const { return ___saldoTxt_4; }
	inline Text_t9039225 ** get_address_of_saldoTxt_4() { return &___saldoTxt_4; }
	inline void set_saldoTxt_4(Text_t9039225 * value)
	{
		___saldoTxt_4 = value;
		Il2CppCodeGenWriteBarrier(&___saldoTxt_4, value);
	}

	inline static int32_t get_offset_of_infoTxt_5() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___infoTxt_5)); }
	inline Text_t9039225 * get_infoTxt_5() const { return ___infoTxt_5; }
	inline Text_t9039225 ** get_address_of_infoTxt_5() { return &___infoTxt_5; }
	inline void set_infoTxt_5(Text_t9039225 * value)
	{
		___infoTxt_5 = value;
		Il2CppCodeGenWriteBarrier(&___infoTxt_5, value);
	}

	inline static int32_t get_offset_of_pontos_6() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___pontos_6)); }
	inline int32_t get_pontos_6() const { return ___pontos_6; }
	inline int32_t* get_address_of_pontos_6() { return &___pontos_6; }
	inline void set_pontos_6(int32_t value)
	{
		___pontos_6 = value;
	}

	inline static int32_t get_offset_of_content_7() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___content_7)); }
	inline GameObject_t3674682005 * get_content_7() const { return ___content_7; }
	inline GameObject_t3674682005 ** get_address_of_content_7() { return &___content_7; }
	inline void set_content_7(GameObject_t3674682005 * value)
	{
		___content_7 = value;
		Il2CppCodeGenWriteBarrier(&___content_7, value);
	}

	inline static int32_t get_offset_of_loadingObj_8() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___loadingObj_8)); }
	inline GameObject_t3674682005 * get_loadingObj_8() const { return ___loadingObj_8; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_8() { return &___loadingObj_8; }
	inline void set_loadingObj_8(GameObject_t3674682005 * value)
	{
		___loadingObj_8 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_8, value);
	}

	inline static int32_t get_offset_of_popup_9() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___popup_9)); }
	inline GameObject_t3674682005 * get_popup_9() const { return ___popup_9; }
	inline GameObject_t3674682005 ** get_address_of_popup_9() { return &___popup_9; }
	inline void set_popup_9(GameObject_t3674682005 * value)
	{
		___popup_9 = value;
		Il2CppCodeGenWriteBarrier(&___popup_9, value);
	}

	inline static int32_t get_offset_of_cuponsInstantiatedsLst_10() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869, ___cuponsInstantiatedsLst_10)); }
	inline List_1_t747900261 * get_cuponsInstantiatedsLst_10() const { return ___cuponsInstantiatedsLst_10; }
	inline List_1_t747900261 ** get_address_of_cuponsInstantiatedsLst_10() { return &___cuponsInstantiatedsLst_10; }
	inline void set_cuponsInstantiatedsLst_10(List_1_t747900261 * value)
	{
		___cuponsInstantiatedsLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___cuponsInstantiatedsLst_10, value);
	}
};

struct CuponsControllerNagumo_t4205714869_StaticFields
{
public:
	// CuponsControllerNagumo CuponsControllerNagumo::instance
	CuponsControllerNagumo_t4205714869 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CuponsControllerNagumo_t4205714869_StaticFields, ___instance_2)); }
	inline CuponsControllerNagumo_t4205714869 * get_instance_2() const { return ___instance_2; }
	inline CuponsControllerNagumo_t4205714869 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CuponsControllerNagumo_t4205714869 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
