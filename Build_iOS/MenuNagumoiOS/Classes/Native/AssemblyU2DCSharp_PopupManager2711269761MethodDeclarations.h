﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupManager
struct PopupManager_t2711269761;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupManager::.ctor()
extern "C"  void PopupManager__ctor_m4087767610 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::Awake()
extern "C"  void PopupManager_Awake_m30405533 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::OnEnable()
extern "C"  void PopupManager_OnEnable_m1253737228 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::OnComplete()
extern "C"  void PopupManager_OnComplete_m4112533314 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::Disable()
extern "C"  void PopupManager_Disable_m2201196160 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
