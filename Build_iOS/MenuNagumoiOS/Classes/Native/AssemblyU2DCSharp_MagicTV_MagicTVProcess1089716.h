﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.animation.GenericStartableComponentControllAbstract
struct GenericStartableComponentControllAbstract_t1794600275;
// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.MagicTVProcess
struct  MagicTVProcess_t1089716  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// ARM.animation.GenericStartableComponentControllAbstract MagicTV.MagicTVProcess::initProcessComponent
	GenericStartableComponentControllAbstract_t1794600275 * ___initProcessComponent_8;
	// ARM.animation.GenericStartableComponentControllAbstract MagicTV.MagicTVProcess::updateProcessComponent
	GenericStartableComponentControllAbstract_t1794600275 * ___updateProcessComponent_9;
	// ARM.animation.GenericStartableComponentControllAbstract MagicTV.MagicTVProcess::magicPresentationsController
	GenericStartableComponentControllAbstract_t1794600275 * ___magicPresentationsController_10;
	// MagicTV.globals.InAppAnalytics MagicTV.MagicTVProcess::_analytics
	InAppAnalytics_t2316734034 * ____analytics_11;
	// System.Boolean MagicTV.MagicTVProcess::autoPlayMode
	bool ___autoPlayMode_12;
	// System.Boolean MagicTV.MagicTVProcess::debugMode
	bool ___debugMode_13;
	// System.String MagicTV.MagicTVProcess::_lastTrackFoundedEvent
	String_t* ____lastTrackFoundedEvent_14;
	// System.Boolean MagicTV.MagicTVProcess::noPromptOnEditor
	bool ___noPromptOnEditor_15;
	// System.Boolean MagicTV.MagicTVProcess::_resetClicked
	bool ____resetClicked_16;

public:
	inline static int32_t get_offset_of_initProcessComponent_8() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ___initProcessComponent_8)); }
	inline GenericStartableComponentControllAbstract_t1794600275 * get_initProcessComponent_8() const { return ___initProcessComponent_8; }
	inline GenericStartableComponentControllAbstract_t1794600275 ** get_address_of_initProcessComponent_8() { return &___initProcessComponent_8; }
	inline void set_initProcessComponent_8(GenericStartableComponentControllAbstract_t1794600275 * value)
	{
		___initProcessComponent_8 = value;
		Il2CppCodeGenWriteBarrier(&___initProcessComponent_8, value);
	}

	inline static int32_t get_offset_of_updateProcessComponent_9() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ___updateProcessComponent_9)); }
	inline GenericStartableComponentControllAbstract_t1794600275 * get_updateProcessComponent_9() const { return ___updateProcessComponent_9; }
	inline GenericStartableComponentControllAbstract_t1794600275 ** get_address_of_updateProcessComponent_9() { return &___updateProcessComponent_9; }
	inline void set_updateProcessComponent_9(GenericStartableComponentControllAbstract_t1794600275 * value)
	{
		___updateProcessComponent_9 = value;
		Il2CppCodeGenWriteBarrier(&___updateProcessComponent_9, value);
	}

	inline static int32_t get_offset_of_magicPresentationsController_10() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ___magicPresentationsController_10)); }
	inline GenericStartableComponentControllAbstract_t1794600275 * get_magicPresentationsController_10() const { return ___magicPresentationsController_10; }
	inline GenericStartableComponentControllAbstract_t1794600275 ** get_address_of_magicPresentationsController_10() { return &___magicPresentationsController_10; }
	inline void set_magicPresentationsController_10(GenericStartableComponentControllAbstract_t1794600275 * value)
	{
		___magicPresentationsController_10 = value;
		Il2CppCodeGenWriteBarrier(&___magicPresentationsController_10, value);
	}

	inline static int32_t get_offset_of__analytics_11() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ____analytics_11)); }
	inline InAppAnalytics_t2316734034 * get__analytics_11() const { return ____analytics_11; }
	inline InAppAnalytics_t2316734034 ** get_address_of__analytics_11() { return &____analytics_11; }
	inline void set__analytics_11(InAppAnalytics_t2316734034 * value)
	{
		____analytics_11 = value;
		Il2CppCodeGenWriteBarrier(&____analytics_11, value);
	}

	inline static int32_t get_offset_of_autoPlayMode_12() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ___autoPlayMode_12)); }
	inline bool get_autoPlayMode_12() const { return ___autoPlayMode_12; }
	inline bool* get_address_of_autoPlayMode_12() { return &___autoPlayMode_12; }
	inline void set_autoPlayMode_12(bool value)
	{
		___autoPlayMode_12 = value;
	}

	inline static int32_t get_offset_of_debugMode_13() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ___debugMode_13)); }
	inline bool get_debugMode_13() const { return ___debugMode_13; }
	inline bool* get_address_of_debugMode_13() { return &___debugMode_13; }
	inline void set_debugMode_13(bool value)
	{
		___debugMode_13 = value;
	}

	inline static int32_t get_offset_of__lastTrackFoundedEvent_14() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ____lastTrackFoundedEvent_14)); }
	inline String_t* get__lastTrackFoundedEvent_14() const { return ____lastTrackFoundedEvent_14; }
	inline String_t** get_address_of__lastTrackFoundedEvent_14() { return &____lastTrackFoundedEvent_14; }
	inline void set__lastTrackFoundedEvent_14(String_t* value)
	{
		____lastTrackFoundedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&____lastTrackFoundedEvent_14, value);
	}

	inline static int32_t get_offset_of_noPromptOnEditor_15() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ___noPromptOnEditor_15)); }
	inline bool get_noPromptOnEditor_15() const { return ___noPromptOnEditor_15; }
	inline bool* get_address_of_noPromptOnEditor_15() { return &___noPromptOnEditor_15; }
	inline void set_noPromptOnEditor_15(bool value)
	{
		___noPromptOnEditor_15 = value;
	}

	inline static int32_t get_offset_of__resetClicked_16() { return static_cast<int32_t>(offsetof(MagicTVProcess_t1089716, ____resetClicked_16)); }
	inline bool get__resetClicked_16() const { return ____resetClicked_16; }
	inline bool* get_address_of__resetClicked_16() { return &____resetClicked_16; }
	inline void set__resetClicked_16(bool value)
	{
		____resetClicked_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
