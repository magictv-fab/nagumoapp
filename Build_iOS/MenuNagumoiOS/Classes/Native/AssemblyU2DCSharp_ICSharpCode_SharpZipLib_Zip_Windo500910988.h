﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.WindowsNameTransform
struct  WindowsNameTransform_t500910988  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::_baseDirectory
	String_t* ____baseDirectory_1;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::_trimIncomingPaths
	bool ____trimIncomingPaths_2;
	// System.Char ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::_replacementChar
	Il2CppChar ____replacementChar_3;

public:
	inline static int32_t get_offset_of__baseDirectory_1() { return static_cast<int32_t>(offsetof(WindowsNameTransform_t500910988, ____baseDirectory_1)); }
	inline String_t* get__baseDirectory_1() const { return ____baseDirectory_1; }
	inline String_t** get_address_of__baseDirectory_1() { return &____baseDirectory_1; }
	inline void set__baseDirectory_1(String_t* value)
	{
		____baseDirectory_1 = value;
		Il2CppCodeGenWriteBarrier(&____baseDirectory_1, value);
	}

	inline static int32_t get_offset_of__trimIncomingPaths_2() { return static_cast<int32_t>(offsetof(WindowsNameTransform_t500910988, ____trimIncomingPaths_2)); }
	inline bool get__trimIncomingPaths_2() const { return ____trimIncomingPaths_2; }
	inline bool* get_address_of__trimIncomingPaths_2() { return &____trimIncomingPaths_2; }
	inline void set__trimIncomingPaths_2(bool value)
	{
		____trimIncomingPaths_2 = value;
	}

	inline static int32_t get_offset_of__replacementChar_3() { return static_cast<int32_t>(offsetof(WindowsNameTransform_t500910988, ____replacementChar_3)); }
	inline Il2CppChar get__replacementChar_3() const { return ____replacementChar_3; }
	inline Il2CppChar* get_address_of__replacementChar_3() { return &____replacementChar_3; }
	inline void set__replacementChar_3(Il2CppChar value)
	{
		____replacementChar_3 = value;
	}
};

struct WindowsNameTransform_t500910988_StaticFields
{
public:
	// System.Char[] ICSharpCode.SharpZipLib.Zip.WindowsNameTransform::InvalidEntryChars
	CharU5BU5D_t3324145743* ___InvalidEntryChars_4;

public:
	inline static int32_t get_offset_of_InvalidEntryChars_4() { return static_cast<int32_t>(offsetof(WindowsNameTransform_t500910988_StaticFields, ___InvalidEntryChars_4)); }
	inline CharU5BU5D_t3324145743* get_InvalidEntryChars_4() const { return ___InvalidEntryChars_4; }
	inline CharU5BU5D_t3324145743** get_address_of_InvalidEntryChars_4() { return &___InvalidEntryChars_4; }
	inline void set_InvalidEntryChars_4(CharU5BU5D_t3324145743* value)
	{
		___InvalidEntryChars_4 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidEntryChars_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
