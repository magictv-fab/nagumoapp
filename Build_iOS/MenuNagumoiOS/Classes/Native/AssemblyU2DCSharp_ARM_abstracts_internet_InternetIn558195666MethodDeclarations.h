﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler
struct OnWifiInfoEventHandler_t558195666;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnWifiInfoEventHandler__ctor_m2225702265 (OnWifiInfoEventHandler_t558195666 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler::Invoke(System.Boolean)
extern "C"  void OnWifiInfoEventHandler_Invoke_m548990794 (OnWifiInfoEventHandler_t558195666 * __this, bool ___has0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnWifiInfoEventHandler_BeginInvoke_m1495015799 (OnWifiInfoEventHandler_t558195666 * __this, bool ___has0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnWifiInfoEventHandler_EndInvoke_m1577360393 (OnWifiInfoEventHandler_t558195666 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
