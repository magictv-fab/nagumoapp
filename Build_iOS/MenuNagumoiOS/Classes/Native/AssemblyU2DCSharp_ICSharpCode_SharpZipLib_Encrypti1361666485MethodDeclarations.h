﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged
struct PkzipClassicManaged_t1361666485;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1457372358;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t153068654;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::.ctor()
extern "C"  void PkzipClassicManaged__ctor_m689229986 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_BlockSize()
extern "C"  int32_t PkzipClassicManaged_get_BlockSize_m3071298615 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_BlockSize(System.Int32)
extern "C"  void PkzipClassicManaged_set_BlockSize_m4091701474 (PkzipClassicManaged_t1361666485 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.KeySizes[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_LegalKeySizes()
extern "C"  KeySizesU5BU5D_t1457372358* PkzipClassicManaged_get_LegalKeySizes_m127422264 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateIV()
extern "C"  void PkzipClassicManaged_GenerateIV_m445397124 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.KeySizes[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_LegalBlockSizes()
extern "C"  KeySizesU5BU5D_t1457372358* PkzipClassicManaged_get_LegalBlockSizes_m3509463114 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_Key()
extern "C"  ByteU5BU5D_t4260760469* PkzipClassicManaged_get_Key_m1856217292 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_Key(System.Byte[])
extern "C"  void PkzipClassicManaged_set_Key_m636772487 (PkzipClassicManaged_t1361666485 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateKey()
extern "C"  void PkzipClassicManaged_GenerateKey_m924780714 (PkzipClassicManaged_t1361666485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * PkzipClassicManaged_CreateEncryptor_m406369630 (PkzipClassicManaged_t1361666485 * __this, ByteU5BU5D_t4260760469* ___rgbKey0, ByteU5BU5D_t4260760469* ___rgbIV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern "C"  Il2CppObject * PkzipClassicManaged_CreateDecryptor_m100641078 (PkzipClassicManaged_t1361666485 * __this, ByteU5BU5D_t4260760469* ___rgbKey0, ByteU5BU5D_t4260760469* ___rgbIV1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
