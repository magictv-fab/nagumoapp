﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSNotificationOpenedResult
struct OSNotificationOpenedResult_t158818421;

#include "codegen/il2cpp-codegen.h"

// System.Void OSNotificationOpenedResult::.ctor()
extern "C"  void OSNotificationOpenedResult__ctor_m3288185798 (OSNotificationOpenedResult_t158818421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
