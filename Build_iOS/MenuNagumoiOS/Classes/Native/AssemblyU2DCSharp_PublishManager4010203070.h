﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PublicationsData
struct PublicationsData_t1837633585;
// PublishManager
struct PublishManager_t4010203070;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublishManager
struct  PublishManager_t4010203070  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PublishManager::toggleList
	List_1_t747900261 * ___toggleList_4;
	// System.Collections.Generic.List`1<System.String> PublishManager::titleLst
	List_1_t1375417109 * ___titleLst_5;
	// System.Collections.Generic.List`1<System.String> PublishManager::valueLst
	List_1_t1375417109 * ___valueLst_6;
	// System.Collections.Generic.List`1<System.String> PublishManager::infoLst
	List_1_t1375417109 * ___infoLst_7;
	// System.Collections.Generic.List`1<System.String> PublishManager::idLst
	List_1_t1375417109 * ___idLst_8;
	// System.Collections.Generic.List`1<System.String> PublishManager::linkLst
	List_1_t1375417109 * ___linkLst_9;
	// System.Collections.Generic.List`1<System.String> PublishManager::urlImgLst
	List_1_t1375417109 * ___urlImgLst_10;
	// System.Collections.Generic.List`1<System.Boolean> PublishManager::favoritouLst
	List_1_t1844984270 * ___favoritouLst_11;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> PublishManager::imgLst
	List_1_t272385497 * ___imgLst_12;
	// UnityEngine.GameObject PublishManager::loadingObj
	GameObject_t3674682005 * ___loadingObj_13;
	// UnityEngine.GameObject PublishManager::popup
	GameObject_t3674682005 * ___popup_14;
	// UnityEngine.GameObject PublishManager::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_15;
	// UnityEngine.GameObject PublishManager::containerItens
	GameObject_t3674682005 * ___containerItens_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PublishManager::itensList
	List_1_t747900261 * ___itensList_17;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap PublishManager::horizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnap_18;
	// UnityEngine.UI.ScrollRect PublishManager::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_19;
	// System.Int32 PublishManager::ofertaCounter
	int32_t ___ofertaCounter_20;
	// UnityEngine.GameObject PublishManager::currentItem
	GameObject_t3674682005 * ___currentItem_21;
	// UnityEngine.GameObject PublishManager::selectedItem
	GameObject_t3674682005 * ___selectedItem_22;

public:
	inline static int32_t get_offset_of_toggleList_4() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___toggleList_4)); }
	inline List_1_t747900261 * get_toggleList_4() const { return ___toggleList_4; }
	inline List_1_t747900261 ** get_address_of_toggleList_4() { return &___toggleList_4; }
	inline void set_toggleList_4(List_1_t747900261 * value)
	{
		___toggleList_4 = value;
		Il2CppCodeGenWriteBarrier(&___toggleList_4, value);
	}

	inline static int32_t get_offset_of_titleLst_5() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___titleLst_5)); }
	inline List_1_t1375417109 * get_titleLst_5() const { return ___titleLst_5; }
	inline List_1_t1375417109 ** get_address_of_titleLst_5() { return &___titleLst_5; }
	inline void set_titleLst_5(List_1_t1375417109 * value)
	{
		___titleLst_5 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_5, value);
	}

	inline static int32_t get_offset_of_valueLst_6() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___valueLst_6)); }
	inline List_1_t1375417109 * get_valueLst_6() const { return ___valueLst_6; }
	inline List_1_t1375417109 ** get_address_of_valueLst_6() { return &___valueLst_6; }
	inline void set_valueLst_6(List_1_t1375417109 * value)
	{
		___valueLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_6, value);
	}

	inline static int32_t get_offset_of_infoLst_7() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___infoLst_7)); }
	inline List_1_t1375417109 * get_infoLst_7() const { return ___infoLst_7; }
	inline List_1_t1375417109 ** get_address_of_infoLst_7() { return &___infoLst_7; }
	inline void set_infoLst_7(List_1_t1375417109 * value)
	{
		___infoLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_7, value);
	}

	inline static int32_t get_offset_of_idLst_8() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___idLst_8)); }
	inline List_1_t1375417109 * get_idLst_8() const { return ___idLst_8; }
	inline List_1_t1375417109 ** get_address_of_idLst_8() { return &___idLst_8; }
	inline void set_idLst_8(List_1_t1375417109 * value)
	{
		___idLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_8, value);
	}

	inline static int32_t get_offset_of_linkLst_9() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___linkLst_9)); }
	inline List_1_t1375417109 * get_linkLst_9() const { return ___linkLst_9; }
	inline List_1_t1375417109 ** get_address_of_linkLst_9() { return &___linkLst_9; }
	inline void set_linkLst_9(List_1_t1375417109 * value)
	{
		___linkLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___linkLst_9, value);
	}

	inline static int32_t get_offset_of_urlImgLst_10() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___urlImgLst_10)); }
	inline List_1_t1375417109 * get_urlImgLst_10() const { return ___urlImgLst_10; }
	inline List_1_t1375417109 ** get_address_of_urlImgLst_10() { return &___urlImgLst_10; }
	inline void set_urlImgLst_10(List_1_t1375417109 * value)
	{
		___urlImgLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___urlImgLst_10, value);
	}

	inline static int32_t get_offset_of_favoritouLst_11() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___favoritouLst_11)); }
	inline List_1_t1844984270 * get_favoritouLst_11() const { return ___favoritouLst_11; }
	inline List_1_t1844984270 ** get_address_of_favoritouLst_11() { return &___favoritouLst_11; }
	inline void set_favoritouLst_11(List_1_t1844984270 * value)
	{
		___favoritouLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouLst_11, value);
	}

	inline static int32_t get_offset_of_imgLst_12() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___imgLst_12)); }
	inline List_1_t272385497 * get_imgLst_12() const { return ___imgLst_12; }
	inline List_1_t272385497 ** get_address_of_imgLst_12() { return &___imgLst_12; }
	inline void set_imgLst_12(List_1_t272385497 * value)
	{
		___imgLst_12 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_12, value);
	}

	inline static int32_t get_offset_of_loadingObj_13() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___loadingObj_13)); }
	inline GameObject_t3674682005 * get_loadingObj_13() const { return ___loadingObj_13; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_13() { return &___loadingObj_13; }
	inline void set_loadingObj_13(GameObject_t3674682005 * value)
	{
		___loadingObj_13 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_13, value);
	}

	inline static int32_t get_offset_of_popup_14() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___popup_14)); }
	inline GameObject_t3674682005 * get_popup_14() const { return ___popup_14; }
	inline GameObject_t3674682005 ** get_address_of_popup_14() { return &___popup_14; }
	inline void set_popup_14(GameObject_t3674682005 * value)
	{
		___popup_14 = value;
		Il2CppCodeGenWriteBarrier(&___popup_14, value);
	}

	inline static int32_t get_offset_of_itemPrefab_15() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___itemPrefab_15)); }
	inline GameObject_t3674682005 * get_itemPrefab_15() const { return ___itemPrefab_15; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_15() { return &___itemPrefab_15; }
	inline void set_itemPrefab_15(GameObject_t3674682005 * value)
	{
		___itemPrefab_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_15, value);
	}

	inline static int32_t get_offset_of_containerItens_16() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___containerItens_16)); }
	inline GameObject_t3674682005 * get_containerItens_16() const { return ___containerItens_16; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_16() { return &___containerItens_16; }
	inline void set_containerItens_16(GameObject_t3674682005 * value)
	{
		___containerItens_16 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_16, value);
	}

	inline static int32_t get_offset_of_itensList_17() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___itensList_17)); }
	inline List_1_t747900261 * get_itensList_17() const { return ___itensList_17; }
	inline List_1_t747900261 ** get_address_of_itensList_17() { return &___itensList_17; }
	inline void set_itensList_17(List_1_t747900261 * value)
	{
		___itensList_17 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_17, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnap_18() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___horizontalScrollSnap_18)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnap_18() const { return ___horizontalScrollSnap_18; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnap_18() { return &___horizontalScrollSnap_18; }
	inline void set_horizontalScrollSnap_18(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnap_18 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnap_18, value);
	}

	inline static int32_t get_offset_of_scrollRect_19() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___scrollRect_19)); }
	inline ScrollRect_t3606982749 * get_scrollRect_19() const { return ___scrollRect_19; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_19() { return &___scrollRect_19; }
	inline void set_scrollRect_19(ScrollRect_t3606982749 * value)
	{
		___scrollRect_19 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_19, value);
	}

	inline static int32_t get_offset_of_ofertaCounter_20() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___ofertaCounter_20)); }
	inline int32_t get_ofertaCounter_20() const { return ___ofertaCounter_20; }
	inline int32_t* get_address_of_ofertaCounter_20() { return &___ofertaCounter_20; }
	inline void set_ofertaCounter_20(int32_t value)
	{
		___ofertaCounter_20 = value;
	}

	inline static int32_t get_offset_of_currentItem_21() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___currentItem_21)); }
	inline GameObject_t3674682005 * get_currentItem_21() const { return ___currentItem_21; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_21() { return &___currentItem_21; }
	inline void set_currentItem_21(GameObject_t3674682005 * value)
	{
		___currentItem_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_21, value);
	}

	inline static int32_t get_offset_of_selectedItem_22() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070, ___selectedItem_22)); }
	inline GameObject_t3674682005 * get_selectedItem_22() const { return ___selectedItem_22; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_22() { return &___selectedItem_22; }
	inline void set_selectedItem_22(GameObject_t3674682005 * value)
	{
		___selectedItem_22 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_22, value);
	}
};

struct PublishManager_t4010203070_StaticFields
{
public:
	// PublicationsData PublishManager::publicationsData
	PublicationsData_t1837633585 * ___publicationsData_2;
	// PublishManager PublishManager::instance
	PublishManager_t4010203070 * ___instance_3;
	// System.String PublishManager::currentJsonTxt
	String_t* ___currentJsonTxt_23;

public:
	inline static int32_t get_offset_of_publicationsData_2() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070_StaticFields, ___publicationsData_2)); }
	inline PublicationsData_t1837633585 * get_publicationsData_2() const { return ___publicationsData_2; }
	inline PublicationsData_t1837633585 ** get_address_of_publicationsData_2() { return &___publicationsData_2; }
	inline void set_publicationsData_2(PublicationsData_t1837633585 * value)
	{
		___publicationsData_2 = value;
		Il2CppCodeGenWriteBarrier(&___publicationsData_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070_StaticFields, ___instance_3)); }
	inline PublishManager_t4010203070 * get_instance_3() const { return ___instance_3; }
	inline PublishManager_t4010203070 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(PublishManager_t4010203070 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_23() { return static_cast<int32_t>(offsetof(PublishManager_t4010203070_StaticFields, ___currentJsonTxt_23)); }
	inline String_t* get_currentJsonTxt_23() const { return ___currentJsonTxt_23; }
	inline String_t** get_address_of_currentJsonTxt_23() { return &___currentJsonTxt_23; }
	inline void set_currentJsonTxt_23(String_t* value)
	{
		___currentJsonTxt_23 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
