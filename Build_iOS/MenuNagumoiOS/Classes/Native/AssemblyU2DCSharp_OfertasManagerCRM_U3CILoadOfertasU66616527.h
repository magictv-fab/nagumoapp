﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// OfertaCRMData[]
struct OfertaCRMDataU5BU5D_t2228159150;
// OfertaCRMData
struct OfertaCRMData_t637956887;
// System.Object
struct Il2CppObject;
// OfertasManagerCRM
struct OfertasManagerCRM_t3100875987;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM/<ILoadOfertas>c__Iterator5
struct  U3CILoadOfertasU3Ec__Iterator5_t66616527  : public Il2CppObject
{
public:
	// System.String OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<$s_1>__4
	Enumerator_t767573031  ___U3CU24s_1U3E__4_4;
	// UnityEngine.GameObject OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<obj>__5
	GameObject_t3674682005 * ___U3CobjU3E__5_5;
	// System.String OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<message>__6
	String_t* ___U3CmessageU3E__6_6;
	// System.Int32 OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<count>__7
	int32_t ___U3CcountU3E__7_7;
	// OfertaCRMData[] OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<$s_2>__8
	OfertaCRMDataU5BU5D_t2228159150* ___U3CU24s_2U3E__8_8;
	// System.Int32 OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<$s_3>__9
	int32_t ___U3CU24s_3U3E__9_9;
	// OfertaCRMData OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<ofertaCRMDataPreco>__10
	OfertaCRMData_t637956887 * ___U3CofertaCRMDataPrecoU3E__10_10;
	// OfertaCRMData[] OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<$s_4>__11
	OfertaCRMDataU5BU5D_t2228159150* ___U3CU24s_4U3E__11_11;
	// System.Int32 OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<$s_5>__12
	int32_t ___U3CU24s_5U3E__12_12;
	// OfertaCRMData OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<ofertaCRMDataDePor>__13
	OfertaCRMData_t637956887 * ___U3CofertaCRMDataDePorU3E__13_13;
	// System.Int32 OfertasManagerCRM/<ILoadOfertas>c__Iterator5::$PC
	int32_t ___U24PC_14;
	// System.Object OfertasManagerCRM/<ILoadOfertas>c__Iterator5::$current
	Il2CppObject * ___U24current_15;
	// OfertasManagerCRM OfertasManagerCRM/<ILoadOfertas>c__Iterator5::<>f__this
	OfertasManagerCRM_t3100875987 * ___U3CU3Ef__this_16;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_1U3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CU24s_1U3E__4_4)); }
	inline Enumerator_t767573031  get_U3CU24s_1U3E__4_4() const { return ___U3CU24s_1U3E__4_4; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_1U3E__4_4() { return &___U3CU24s_1U3E__4_4; }
	inline void set_U3CU24s_1U3E__4_4(Enumerator_t767573031  value)
	{
		___U3CU24s_1U3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CobjU3E__5_5)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__5_5() const { return ___U3CobjU3E__5_5; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__5_5() { return &___U3CobjU3E__5_5; }
	inline void set_U3CobjU3E__5_5(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__6_6() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CmessageU3E__6_6)); }
	inline String_t* get_U3CmessageU3E__6_6() const { return ___U3CmessageU3E__6_6; }
	inline String_t** get_address_of_U3CmessageU3E__6_6() { return &___U3CmessageU3E__6_6; }
	inline void set_U3CmessageU3E__6_6(String_t* value)
	{
		___U3CmessageU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CcountU3E__7_7() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CcountU3E__7_7)); }
	inline int32_t get_U3CcountU3E__7_7() const { return ___U3CcountU3E__7_7; }
	inline int32_t* get_address_of_U3CcountU3E__7_7() { return &___U3CcountU3E__7_7; }
	inline void set_U3CcountU3E__7_7(int32_t value)
	{
		___U3CcountU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_2U3E__8_8() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CU24s_2U3E__8_8)); }
	inline OfertaCRMDataU5BU5D_t2228159150* get_U3CU24s_2U3E__8_8() const { return ___U3CU24s_2U3E__8_8; }
	inline OfertaCRMDataU5BU5D_t2228159150** get_address_of_U3CU24s_2U3E__8_8() { return &___U3CU24s_2U3E__8_8; }
	inline void set_U3CU24s_2U3E__8_8(OfertaCRMDataU5BU5D_t2228159150* value)
	{
		___U3CU24s_2U3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_2U3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3CU24s_3U3E__9_9() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CU24s_3U3E__9_9)); }
	inline int32_t get_U3CU24s_3U3E__9_9() const { return ___U3CU24s_3U3E__9_9; }
	inline int32_t* get_address_of_U3CU24s_3U3E__9_9() { return &___U3CU24s_3U3E__9_9; }
	inline void set_U3CU24s_3U3E__9_9(int32_t value)
	{
		___U3CU24s_3U3E__9_9 = value;
	}

	inline static int32_t get_offset_of_U3CofertaCRMDataPrecoU3E__10_10() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CofertaCRMDataPrecoU3E__10_10)); }
	inline OfertaCRMData_t637956887 * get_U3CofertaCRMDataPrecoU3E__10_10() const { return ___U3CofertaCRMDataPrecoU3E__10_10; }
	inline OfertaCRMData_t637956887 ** get_address_of_U3CofertaCRMDataPrecoU3E__10_10() { return &___U3CofertaCRMDataPrecoU3E__10_10; }
	inline void set_U3CofertaCRMDataPrecoU3E__10_10(OfertaCRMData_t637956887 * value)
	{
		___U3CofertaCRMDataPrecoU3E__10_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CofertaCRMDataPrecoU3E__10_10, value);
	}

	inline static int32_t get_offset_of_U3CU24s_4U3E__11_11() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CU24s_4U3E__11_11)); }
	inline OfertaCRMDataU5BU5D_t2228159150* get_U3CU24s_4U3E__11_11() const { return ___U3CU24s_4U3E__11_11; }
	inline OfertaCRMDataU5BU5D_t2228159150** get_address_of_U3CU24s_4U3E__11_11() { return &___U3CU24s_4U3E__11_11; }
	inline void set_U3CU24s_4U3E__11_11(OfertaCRMDataU5BU5D_t2228159150* value)
	{
		___U3CU24s_4U3E__11_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_4U3E__11_11, value);
	}

	inline static int32_t get_offset_of_U3CU24s_5U3E__12_12() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CU24s_5U3E__12_12)); }
	inline int32_t get_U3CU24s_5U3E__12_12() const { return ___U3CU24s_5U3E__12_12; }
	inline int32_t* get_address_of_U3CU24s_5U3E__12_12() { return &___U3CU24s_5U3E__12_12; }
	inline void set_U3CU24s_5U3E__12_12(int32_t value)
	{
		___U3CU24s_5U3E__12_12 = value;
	}

	inline static int32_t get_offset_of_U3CofertaCRMDataDePorU3E__13_13() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CofertaCRMDataDePorU3E__13_13)); }
	inline OfertaCRMData_t637956887 * get_U3CofertaCRMDataDePorU3E__13_13() const { return ___U3CofertaCRMDataDePorU3E__13_13; }
	inline OfertaCRMData_t637956887 ** get_address_of_U3CofertaCRMDataDePorU3E__13_13() { return &___U3CofertaCRMDataDePorU3E__13_13; }
	inline void set_U3CofertaCRMDataDePorU3E__13_13(OfertaCRMData_t637956887 * value)
	{
		___U3CofertaCRMDataDePorU3E__13_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CofertaCRMDataDePorU3E__13_13, value);
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U24current_15)); }
	inline Il2CppObject * get_U24current_15() const { return ___U24current_15; }
	inline Il2CppObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(Il2CppObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_16() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__Iterator5_t66616527, ___U3CU3Ef__this_16)); }
	inline OfertasManagerCRM_t3100875987 * get_U3CU3Ef__this_16() const { return ___U3CU3Ef__this_16; }
	inline OfertasManagerCRM_t3100875987 ** get_address_of_U3CU3Ef__this_16() { return &___U3CU3Ef__this_16; }
	inline void set_U3CU3Ef__this_16(OfertasManagerCRM_t3100875987 * value)
	{
		___U3CU3Ef__this_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
