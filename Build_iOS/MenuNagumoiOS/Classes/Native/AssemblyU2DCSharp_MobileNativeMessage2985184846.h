﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcherB1248907224.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileNativeMessage
struct  MobileNativeMessage_t2985184846  : public EventDispatcherBase_t1248907224
{
public:
	// System.Action MobileNativeMessage::OnComplete
	Action_t3771233898 * ___OnComplete_2;

public:
	inline static int32_t get_offset_of_OnComplete_2() { return static_cast<int32_t>(offsetof(MobileNativeMessage_t2985184846, ___OnComplete_2)); }
	inline Action_t3771233898 * get_OnComplete_2() const { return ___OnComplete_2; }
	inline Action_t3771233898 ** get_address_of_OnComplete_2() { return &___OnComplete_2; }
	inline void set_OnComplete_2(Action_t3771233898 * value)
	{
		___OnComplete_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnComplete_2, value);
	}
};

struct MobileNativeMessage_t2985184846_StaticFields
{
public:
	// System.Action MobileNativeMessage::<>f__am$cache1
	Action_t3771233898 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(MobileNativeMessage_t2985184846_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
