﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.ToggleOnOffAbstract
struct ToggleOnOffAbstract_t832893812;
// ARM.abstracts.ToggleOnOffAbstract/TurnChange
struct TurnChange_t3311189389;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstrac3311189389.h"

// System.Void ARM.abstracts.ToggleOnOffAbstract::.ctor()
extern "C"  void ToggleOnOffAbstract__ctor_m3749259036 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::Awake()
extern "C"  void ToggleOnOffAbstract_Awake_m3986864255 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::addTurnOnChange(ARM.abstracts.ToggleOnOffAbstract/TurnChange)
extern "C"  void ToggleOnOffAbstract_addTurnOnChange_m2166427194 (ToggleOnOffAbstract_t832893812 * __this, TurnChange_t3311189389 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::removeTurnOnChange(ARM.abstracts.ToggleOnOffAbstract/TurnChange)
extern "C"  void ToggleOnOffAbstract_removeTurnOnChange_m562431115 (ToggleOnOffAbstract_t832893812 * __this, TurnChange_t3311189389 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::onTurnOnEvent()
extern "C"  void ToggleOnOffAbstract_onTurnOnEvent_m1611694329 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::addTurnOffChange(ARM.abstracts.ToggleOnOffAbstract/TurnChange)
extern "C"  void ToggleOnOffAbstract_addTurnOffChange_m2185638588 (ToggleOnOffAbstract_t832893812 * __this, TurnChange_t3311189389 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::removeTurnOffChange(ARM.abstracts.ToggleOnOffAbstract/TurnChange)
extern "C"  void ToggleOnOffAbstract_removeTurnOffChange_m4001367691 (ToggleOnOffAbstract_t832893812 * __this, TurnChange_t3311189389 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::onTurnOffEvent()
extern "C"  void ToggleOnOffAbstract_onTurnOffEvent_m1616087439 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.abstracts.ToggleOnOffAbstract::getUniqueName()
extern "C"  String_t* ToggleOnOffAbstract_getUniqueName_m1146553391 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::turnOn()
extern "C"  void ToggleOnOffAbstract_turnOn_m736592132 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::turnOff()
extern "C"  void ToggleOnOffAbstract_turnOff_m1359340876 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ToggleOnOffAbstract::ResetOnOff()
extern "C"  void ToggleOnOffAbstract_ResetOnOff_m2044375081 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.abstracts.ToggleOnOffAbstract::isActive()
extern "C"  bool ToggleOnOffAbstract_isActive_m218500516 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARM.abstracts.ToggleOnOffAbstract::getGameObjectReference()
extern "C"  GameObject_t3674682005 * ToggleOnOffAbstract_getGameObjectReference_m863296311 (ToggleOnOffAbstract_t832893812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
