﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ReturnConfigTest
struct ReturnConfigTest_t510487652;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeValuesTest
struct  ChangeValuesTest_t1677872324  : public MonoBehaviour_t667441552
{
public:
	// ReturnConfigTest ChangeValuesTest::obj
	ReturnConfigTest_t510487652 * ___obj_2;
	// System.Boolean ChangeValuesTest::soma
	bool ___soma_3;
	// System.Boolean ChangeValuesTest::diminui
	bool ___diminui_4;
	// System.Single ChangeValuesTest::f
	float ___f_5;
	// UnityEngine.Vector3 ChangeValuesTest::v
	Vector3_t4282066566  ___v_6;

public:
	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(ChangeValuesTest_t1677872324, ___obj_2)); }
	inline ReturnConfigTest_t510487652 * get_obj_2() const { return ___obj_2; }
	inline ReturnConfigTest_t510487652 ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(ReturnConfigTest_t510487652 * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier(&___obj_2, value);
	}

	inline static int32_t get_offset_of_soma_3() { return static_cast<int32_t>(offsetof(ChangeValuesTest_t1677872324, ___soma_3)); }
	inline bool get_soma_3() const { return ___soma_3; }
	inline bool* get_address_of_soma_3() { return &___soma_3; }
	inline void set_soma_3(bool value)
	{
		___soma_3 = value;
	}

	inline static int32_t get_offset_of_diminui_4() { return static_cast<int32_t>(offsetof(ChangeValuesTest_t1677872324, ___diminui_4)); }
	inline bool get_diminui_4() const { return ___diminui_4; }
	inline bool* get_address_of_diminui_4() { return &___diminui_4; }
	inline void set_diminui_4(bool value)
	{
		___diminui_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(ChangeValuesTest_t1677872324, ___f_5)); }
	inline float get_f_5() const { return ___f_5; }
	inline float* get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(float value)
	{
		___f_5 = value;
	}

	inline static int32_t get_offset_of_v_6() { return static_cast<int32_t>(offsetof(ChangeValuesTest_t1677872324, ___v_6)); }
	inline Vector3_t4282066566  get_v_6() const { return ___v_6; }
	inline Vector3_t4282066566 * get_address_of_v_6() { return &___v_6; }
	inline void set_v_6(Vector3_t4282066566  value)
	{
		___v_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
