﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DevGUIChromaPanel
struct DevGUIChromaPanel_t1432993864;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void DevGUIChromaPanel::.ctor()
extern "C"  void DevGUIChromaPanel__ctor_m302628387 (DevGUIChromaPanel_t1432993864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::Start()
extern "C"  void DevGUIChromaPanel_Start_m3544733475 (DevGUIChromaPanel_t1432993864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::SetBundleID(System.Int32)
extern "C"  void DevGUIChromaPanel_SetBundleID_m1779420273 (DevGUIChromaPanel_t1432993864 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::changedShder(System.String)
extern "C"  void DevGUIChromaPanel_changedShder_m2515143097 (DevGUIChromaPanel_t1432993864 * __this, String_t* ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::changed1(System.Single)
extern "C"  void DevGUIChromaPanel_changed1_m3735002893 (DevGUIChromaPanel_t1432993864 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::changed2(System.Single)
extern "C"  void DevGUIChromaPanel_changed2_m3224468716 (DevGUIChromaPanel_t1432993864 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::changed3(System.Single)
extern "C"  void DevGUIChromaPanel_changed3_m2713934539 (DevGUIChromaPanel_t1432993864 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::changed4(System.Single)
extern "C"  void DevGUIChromaPanel_changed4_m2203400362 (DevGUIChromaPanel_t1432993864 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::OnTransform(System.Single)
extern "C"  void DevGUIChromaPanel_OnTransform_m3738874909 (DevGUIChromaPanel_t1432993864 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::SetProperty(System.String)
extern "C"  void DevGUIChromaPanel_SetProperty_m4059146282 (DevGUIChromaPanel_t1432993864 * __this, String_t* ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::doneToSaveChromakey(System.Boolean)
extern "C"  void DevGUIChromaPanel_doneToSaveChromakey_m25700775 (DevGUIChromaPanel_t1432993864 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIChromaPanel::SaveChromaInfo()
extern "C"  void DevGUIChromaPanel_SaveChromaInfo_m4096471074 (DevGUIChromaPanel_t1432993864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
