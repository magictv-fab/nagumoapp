﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2948932215(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2520414890 *, Dictionary_2_t1203091498 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3997867082(__this, method) ((  Il2CppObject * (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3526226398(__this, method) ((  void (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1715292071(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4076992934(__this, method) ((  Il2CppObject * (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2028471992(__this, method) ((  Il2CppObject * (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::MoveNext()
#define Enumerator_MoveNext_m1655626826(__this, method) ((  bool (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::get_Current()
#define Enumerator_get_Current_m4143646502(__this, method) ((  KeyValuePair_2_t1101872204  (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2415926295(__this, method) ((  String_t* (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3485267579(__this, method) ((  InAppAbstract_t382673128 * (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::Reset()
#define Enumerator_Reset_m1113193033(__this, method) ((  void (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::VerifyState()
#define Enumerator_VerifyState_m2033233810(__this, method) ((  void (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2472443514(__this, method) ((  void (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.abstracts.InAppAbstract>::Dispose()
#define Enumerator_Dispose_m2855077081(__this, method) ((  void (*) (Enumerator_t2520414890 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
