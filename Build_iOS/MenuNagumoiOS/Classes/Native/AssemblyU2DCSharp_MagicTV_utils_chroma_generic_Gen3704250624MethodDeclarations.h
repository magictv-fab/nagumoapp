﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.utils.chroma.generic.GenericChromaKeyConfig
struct GenericChromaKeyConfig_t3704250624;
// UnityEngine.Material
struct Material_t3870600107;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;
// System.String
struct String_t;
// MagicTV.vo.KeyTypeValue[]
struct KeyTypeValueU5BU5D_t1961483187;
// MagicTV.vo.KeyTypeValue
struct KeyTypeValue_t654867638;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyTypeValue654867638.h"

// System.Void MagicTV.utils.chroma.generic.GenericChromaKeyConfig::.ctor()
extern "C"  void GenericChromaKeyConfig__ctor_m1984125700 (GenericChromaKeyConfig_t3704250624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.generic.GenericChromaKeyConfig::configChroma(UnityEngine.Material,MagicTV.vo.MetadataVO[],System.String)
extern "C"  void GenericChromaKeyConfig_configChroma_m3582269730 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, MetadataVOU5BU5D_t4238035203* ___metas1, String_t* ___json2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.generic.GenericChromaKeyConfig::setChromaConfigJson(System.String,UnityEngine.Material)
extern "C"  void GenericChromaKeyConfig_setChromaConfigJson_m2828961418 (Il2CppObject * __this /* static, unused */, String_t* ___chromaConfigJson0, Material_t3870600107 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.generic.GenericChromaKeyConfig::setChromaKeyValues(MagicTV.vo.KeyTypeValue[],UnityEngine.Material)
extern "C"  void GenericChromaKeyConfig_setChromaKeyValues_m1740986191 (Il2CppObject * __this /* static, unused */, KeyTypeValueU5BU5D_t1961483187* ___keyValues0, Material_t3870600107 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.generic.GenericChromaKeyConfig::setChromaKeyValue(MagicTV.vo.KeyTypeValue,UnityEngine.Material)
extern "C"  void GenericChromaKeyConfig_setChromaKeyValue_m699311536 (Il2CppObject * __this /* static, unused */, KeyTypeValue_t654867638 * ___k0, Material_t3870600107 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
