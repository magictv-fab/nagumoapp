﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerMetadataParamsVO
struct ServerMetadataParamsVO_t2699110633;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.request.ServerMetadataParamsVO::.ctor()
extern "C"  void ServerMetadataParamsVO__ctor_m3562910949 (ServerMetadataParamsVO_t2699110633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.request.ServerMetadataParamsVO::ToURLString()
extern "C"  String_t* ServerMetadataParamsVO_ToURLString_m3074095755 (ServerMetadataParamsVO_t2699110633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
