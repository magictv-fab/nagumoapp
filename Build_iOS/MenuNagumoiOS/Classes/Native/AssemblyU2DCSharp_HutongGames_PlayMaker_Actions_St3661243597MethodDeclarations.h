﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringSwitch
struct StringSwitch_t3661243597;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringSwitch::.ctor()
extern "C"  void StringSwitch__ctor_m490512441 (StringSwitch_t3661243597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::Reset()
extern "C"  void StringSwitch_Reset_m2431912678 (StringSwitch_t3661243597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::OnEnter()
extern "C"  void StringSwitch_OnEnter_m2622749136 (StringSwitch_t3661243597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::OnUpdate()
extern "C"  void StringSwitch_OnUpdate_m3129371251 (StringSwitch_t3661243597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::DoStringSwitch()
extern "C"  void StringSwitch_DoStringSwitch_m3242617915 (StringSwitch_t3661243597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
