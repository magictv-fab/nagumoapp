﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TransformInputToWorldSpace
struct TransformInputToWorldSpace_t3646322613;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::.ctor()
extern "C"  void TransformInputToWorldSpace__ctor_m1720256593 (TransformInputToWorldSpace_t3646322613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::Reset()
extern "C"  void TransformInputToWorldSpace_Reset_m3661656830 (TransformInputToWorldSpace_t3646322613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::OnUpdate()
extern "C"  void TransformInputToWorldSpace_OnUpdate_m2366368603 (TransformInputToWorldSpace_t3646322613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
