﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler
struct OnUpdateRequestErrorEventHandler_t1521989041;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnUpdateRequestErrorEventHandler__ctor_m2886397704 (OnUpdateRequestErrorEventHandler_t1521989041 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler::Invoke(System.String)
extern "C"  void OnUpdateRequestErrorEventHandler_Invoke_m472815840 (OnUpdateRequestErrorEventHandler_t1521989041 * __this, String_t* ___descriptionError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnUpdateRequestErrorEventHandler_BeginInvoke_m1260981229 (OnUpdateRequestErrorEventHandler_t1521989041 * __this, String_t* ___descriptionError0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnUpdateRequestErrorEventHandler_EndInvoke_m117384472 (OnUpdateRequestErrorEventHandler_t1521989041 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
