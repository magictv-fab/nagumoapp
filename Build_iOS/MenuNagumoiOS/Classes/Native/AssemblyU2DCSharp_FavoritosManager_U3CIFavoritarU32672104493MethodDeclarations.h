﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager/<IFavoritar>c__Iterator51
struct U3CIFavoritarU3Ec__Iterator51_t2672104493;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FavoritosManager/<IFavoritar>c__Iterator51::.ctor()
extern "C"  void U3CIFavoritarU3Ec__Iterator51__ctor_m2346682126 (U3CIFavoritarU3Ec__Iterator51_t2672104493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<IFavoritar>c__Iterator51::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator51_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1631170894 (U3CIFavoritarU3Ec__Iterator51_t2672104493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<IFavoritar>c__Iterator51::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator51_System_Collections_IEnumerator_get_Current_m778309858 (U3CIFavoritarU3Ec__Iterator51_t2672104493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FavoritosManager/<IFavoritar>c__Iterator51::MoveNext()
extern "C"  bool U3CIFavoritarU3Ec__Iterator51_MoveNext_m4053109518 (U3CIFavoritarU3Ec__Iterator51_t2672104493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<IFavoritar>c__Iterator51::Dispose()
extern "C"  void U3CIFavoritarU3Ec__Iterator51_Dispose_m201975755 (U3CIFavoritarU3Ec__Iterator51_t2672104493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<IFavoritar>c__Iterator51::Reset()
extern "C"  void U3CIFavoritarU3Ec__Iterator51_Reset_m4288082363 (U3CIFavoritarU3Ec__Iterator51_t2672104493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
