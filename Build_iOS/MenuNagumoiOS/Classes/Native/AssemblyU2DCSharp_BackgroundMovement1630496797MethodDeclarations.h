﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackgroundMovement
struct BackgroundMovement_t1630496797;

#include "codegen/il2cpp-codegen.h"

// System.Void BackgroundMovement::.ctor()
extern "C"  void BackgroundMovement__ctor_m3726469406 (BackgroundMovement_t1630496797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundMovement::Start()
extern "C"  void BackgroundMovement_Start_m2673607198 (BackgroundMovement_t1630496797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundMovement::Update()
extern "C"  void BackgroundMovement_Update_m1283296687 (BackgroundMovement_t1630496797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
