﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollRectToolInfinity/SelectChildren
struct SelectChildren_t2120299545;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ScrollRectToolInfinity/SelectChildren::.ctor(System.Object,System.IntPtr)
extern "C"  void SelectChildren__ctor_m3467658096 (SelectChildren_t2120299545 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectToolInfinity/SelectChildren::Invoke(System.String)
extern "C"  void SelectChildren_Invoke_m544226168 (SelectChildren_t2120299545 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ScrollRectToolInfinity/SelectChildren::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SelectChildren_BeginInvoke_m3659611525 (SelectChildren_t2120299545 * __this, String_t* ___name0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectToolInfinity/SelectChildren::EndInvoke(System.IAsyncResult)
extern "C"  void SelectChildren_EndInvoke_m3238980992 (SelectChildren_t2120299545 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
