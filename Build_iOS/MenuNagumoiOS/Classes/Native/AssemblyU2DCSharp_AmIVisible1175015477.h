﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AmIVisible/OnChangeVisible
struct OnChangeVisible_t527731977;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AmIVisible
struct  AmIVisible_t1175015477  : public MonoBehaviour_t667441552
{
public:
	// AmIVisible/OnChangeVisible AmIVisible::ChangeVisible
	OnChangeVisible_t527731977 * ___ChangeVisible_2;
	// System.Boolean AmIVisible::visible
	bool ___visible_3;

public:
	inline static int32_t get_offset_of_ChangeVisible_2() { return static_cast<int32_t>(offsetof(AmIVisible_t1175015477, ___ChangeVisible_2)); }
	inline OnChangeVisible_t527731977 * get_ChangeVisible_2() const { return ___ChangeVisible_2; }
	inline OnChangeVisible_t527731977 ** get_address_of_ChangeVisible_2() { return &___ChangeVisible_2; }
	inline void set_ChangeVisible_2(OnChangeVisible_t527731977 * value)
	{
		___ChangeVisible_2 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeVisible_2, value);
	}

	inline static int32_t get_offset_of_visible_3() { return static_cast<int32_t>(offsetof(AmIVisible_t1175015477, ___visible_3)); }
	inline bool get_visible_3() const { return ___visible_3; }
	inline bool* get_address_of_visible_3() { return &___visible_3; }
	inline void set_visible_3(bool value)
	{
		___visible_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
