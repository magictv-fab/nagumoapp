﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.VoidEventDispatcher
struct VoidEventDispatcher_t1760461203;
// ARM.events.VoidEventDispatcher/VoidEvent
struct VoidEvent_t1756309665;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_events_VoidEventDispatcher_V1756309665.h"

// System.Void ARM.events.VoidEventDispatcher::.ctor()
extern "C"  void VoidEventDispatcher__ctor_m2886133937 (VoidEventDispatcher_t1760461203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.VoidEventDispatcher::AddOnVoidEventHandler(ARM.events.VoidEventDispatcher/VoidEvent)
extern "C"  void VoidEventDispatcher_AddOnVoidEventHandler_m3230605266 (VoidEventDispatcher_t1760461203 * __this, VoidEvent_t1756309665 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.VoidEventDispatcher::RemoveOnVoidEventHandler(ARM.events.VoidEventDispatcher/VoidEvent)
extern "C"  void VoidEventDispatcher_RemoveOnVoidEventHandler_m3756673241 (VoidEventDispatcher_t1760461203 * __this, VoidEvent_t1756309665 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.VoidEventDispatcher::Raise()
extern "C"  void VoidEventDispatcher_Raise_m409217723 (VoidEventDispatcher_t1760461203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
