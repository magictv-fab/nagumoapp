﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct ValueCollection_t1458694378;
// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va689922073.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2455720537_gshared (ValueCollection_t1458694378 * __this, Dictionary_2_t2758088665 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2455720537(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1458694378 *, Dictionary_2_t2758088665 *, const MethodInfo*))ValueCollection__ctor_m2455720537_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4105091769_gshared (ValueCollection_t1458694378 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4105091769(__this, ___item0, method) ((  void (*) (ValueCollection_t1458694378 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4105091769_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3852004354_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3852004354(__this, method) ((  void (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3852004354_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3827655505_gshared (ValueCollection_t1458694378 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3827655505(__this, ___item0, method) ((  bool (*) (ValueCollection_t1458694378 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3827655505_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2498829366_gshared (ValueCollection_t1458694378 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2498829366(__this, ___item0, method) ((  bool (*) (ValueCollection_t1458694378 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2498829366_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2256307202_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2256307202(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2256307202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m308408966_gshared (ValueCollection_t1458694378 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m308408966(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1458694378 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m308408966_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1137105045_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1137105045(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1137105045_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2102831364_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2102831364(__this, method) ((  bool (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2102831364_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1945064676_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1945064676(__this, method) ((  bool (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1945064676_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2646229526_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2646229526(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2646229526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2089912416_gshared (ValueCollection_t1458694378 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2089912416(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1458694378 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2089912416_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::GetEnumerator()
extern "C"  Enumerator_t689922073  ValueCollection_GetEnumerator_m1286678665_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1286678665(__this, method) ((  Enumerator_t689922073  (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_GetEnumerator_m1286678665_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3029300382_gshared (ValueCollection_t1458694378 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3029300382(__this, method) ((  int32_t (*) (ValueCollection_t1458694378 *, const MethodInfo*))ValueCollection_get_Count_m3029300382_gshared)(__this, method)
