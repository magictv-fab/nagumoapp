﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.PDF417Reader
struct PDF417Reader_t2680601325;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Result[]
struct ResultU5BU5D_t4049143490;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"

// ZXing.Result ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * PDF417Reader_decode_m2869694447 (PDF417Reader_t2680601325 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result[] ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Boolean)
extern "C"  ResultU5BU5D_t4049143490* PDF417Reader_decode_m3280041488 (Il2CppObject * __this /* static, unused */, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, bool ___multiple2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.PDF417Reader::getMaxWidth(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  int32_t PDF417Reader_getMaxWidth_m1805770852 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___p10, ResultPoint_t1538592853 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.PDF417Reader::getMinWidth(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  int32_t PDF417Reader_getMinWidth_m2390153170 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___p10, ResultPoint_t1538592853 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.PDF417Reader::getMaxCodewordWidth(ZXing.ResultPoint[])
extern "C"  int32_t PDF417Reader_getMaxCodewordWidth_m2241016854 (Il2CppObject * __this /* static, unused */, ResultPointU5BU5D_t1195164344* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.PDF417Reader::getMinCodewordWidth(ZXing.ResultPoint[])
extern "C"  int32_t PDF417Reader_getMinCodewordWidth_m588609924 (Il2CppObject * __this /* static, unused */, ResultPointU5BU5D_t1195164344* ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417Reader::reset()
extern "C"  void PDF417Reader_reset_m1412458865 (PDF417Reader_t2680601325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417Reader::.ctor()
extern "C"  void PDF417Reader__ctor_m1135711908 (PDF417Reader_t2680601325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
