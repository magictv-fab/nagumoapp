﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.interfaces.GeneralComponentInterface[]
struct GeneralComponentInterfaceU5BU5D_t4161906083;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General1944110717.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.components.GroupComponentsManager
struct  GroupComponentsManager_t3788191382  : public GeneralComponentsObjectAbstract_t1944110717
{
public:
	// ARM.interfaces.GeneralComponentInterface[] ARM.components.GroupComponentsManager::_components
	GeneralComponentInterfaceU5BU5D_t4161906083* ____components_2;

public:
	inline static int32_t get_offset_of__components_2() { return static_cast<int32_t>(offsetof(GroupComponentsManager_t3788191382, ____components_2)); }
	inline GeneralComponentInterfaceU5BU5D_t4161906083* get__components_2() const { return ____components_2; }
	inline GeneralComponentInterfaceU5BU5D_t4161906083** get_address_of__components_2() { return &____components_2; }
	inline void set__components_2(GeneralComponentInterfaceU5BU5D_t4161906083* value)
	{
		____components_2 = value;
		Il2CppCodeGenWriteBarrier(&____components_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
