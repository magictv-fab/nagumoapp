﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.AppRoot
struct AppRoot_t1948717617;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.globals.AppRoot::.ctor()
extern "C"  void AppRoot__ctor_m2935678341 (AppRoot_t1948717617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AppRoot::.cctor()
extern "C"  void AppRoot__cctor_m329619144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.StateMachine MagicTV.globals.AppRoot::get_stateMachine()
extern "C"  int32_t AppRoot_get_stateMachine_m4156174452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AppRoot::set_stateMachine(MagicTV.globals.StateMachine)
extern "C"  void AppRoot_set_stateMachine_m3227282687 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.globals.AppRoot::get_device_id()
extern "C"  String_t* AppRoot_get_device_id_m2617662241 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.globals.AppRoot::getDeviceId()
extern "C"  String_t* AppRoot_getDeviceId_m4236264173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.AppRoot::setDeviceId(System.String)
extern "C"  void AppRoot_setDeviceId_m1133447596 (Il2CppObject * __this /* static, unused */, String_t* ___device0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
