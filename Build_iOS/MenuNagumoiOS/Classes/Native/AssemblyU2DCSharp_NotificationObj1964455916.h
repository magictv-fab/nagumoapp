﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationObj
struct  NotificationObj_t1964455916  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text NotificationObj::titleText
	Text_t9039225 * ___titleText_2;
	// UnityEngine.UI.Text NotificationObj::textText
	Text_t9039225 * ___textText_3;
	// UnityEngine.UI.Text NotificationObj::dateText
	Text_t9039225 * ___dateText_4;
	// System.String NotificationObj::title
	String_t* ___title_5;
	// System.String NotificationObj::text
	String_t* ___text_6;
	// System.String NotificationObj::dateTime
	String_t* ___dateTime_7;

public:
	inline static int32_t get_offset_of_titleText_2() { return static_cast<int32_t>(offsetof(NotificationObj_t1964455916, ___titleText_2)); }
	inline Text_t9039225 * get_titleText_2() const { return ___titleText_2; }
	inline Text_t9039225 ** get_address_of_titleText_2() { return &___titleText_2; }
	inline void set_titleText_2(Text_t9039225 * value)
	{
		___titleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_2, value);
	}

	inline static int32_t get_offset_of_textText_3() { return static_cast<int32_t>(offsetof(NotificationObj_t1964455916, ___textText_3)); }
	inline Text_t9039225 * get_textText_3() const { return ___textText_3; }
	inline Text_t9039225 ** get_address_of_textText_3() { return &___textText_3; }
	inline void set_textText_3(Text_t9039225 * value)
	{
		___textText_3 = value;
		Il2CppCodeGenWriteBarrier(&___textText_3, value);
	}

	inline static int32_t get_offset_of_dateText_4() { return static_cast<int32_t>(offsetof(NotificationObj_t1964455916, ___dateText_4)); }
	inline Text_t9039225 * get_dateText_4() const { return ___dateText_4; }
	inline Text_t9039225 ** get_address_of_dateText_4() { return &___dateText_4; }
	inline void set_dateText_4(Text_t9039225 * value)
	{
		___dateText_4 = value;
		Il2CppCodeGenWriteBarrier(&___dateText_4, value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(NotificationObj_t1964455916, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier(&___title_5, value);
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(NotificationObj_t1964455916, ___text_6)); }
	inline String_t* get_text_6() const { return ___text_6; }
	inline String_t** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(String_t* value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier(&___text_6, value);
	}

	inline static int32_t get_offset_of_dateTime_7() { return static_cast<int32_t>(offsetof(NotificationObj_t1964455916, ___dateTime_7)); }
	inline String_t* get_dateTime_7() const { return ___dateTime_7; }
	inline String_t** get_address_of_dateTime_7() { return &___dateTime_7; }
	inline void set_dateTime_7(String_t* value)
	{
		___dateTime_7 = value;
		Il2CppCodeGenWriteBarrier(&___dateTime_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
