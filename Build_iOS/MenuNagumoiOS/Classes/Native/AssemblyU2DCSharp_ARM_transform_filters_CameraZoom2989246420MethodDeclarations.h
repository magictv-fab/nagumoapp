﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.CameraZoomFilter
struct CameraZoomFilter_t2989246420;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.transform.filters.CameraZoomFilter::.ctor()
extern "C"  void CameraZoomFilter__ctor_m505660772 (CameraZoomFilter_t2989246420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.CameraZoomFilter::Update()
extern "C"  void CameraZoomFilter_Update_m222476841 (CameraZoomFilter_t2989246420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.CameraZoomFilter::Start()
extern "C"  void CameraZoomFilter_Start_m3747765860 (CameraZoomFilter_t2989246420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CameraZoomFilter__doFilter_m787129474 (CameraZoomFilter_t2989246420 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilter::filtroComPlugin(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CameraZoomFilter_filtroComPlugin_m1422760261 (CameraZoomFilter_t2989246420 * __this, Vector3_t4282066566  ___nextValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.CameraZoomFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  CameraZoomFilter__doFilter_m855394890 (CameraZoomFilter_t2989246420 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
