﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.AlignmentPatternFinder
struct AlignmentPatternFinder_t4197406735;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPointCallback
struct ResultPointCallback_t207829946;
// ZXing.QrCode.Internal.AlignmentPattern
struct AlignmentPattern_t1786970569;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_ResultPointCallback207829946.h"
#include "mscorlib_System_Nullable_1_gen81078199.h"

// System.Void ZXing.QrCode.Internal.AlignmentPatternFinder::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32,System.Single,ZXing.ResultPointCallback)
extern "C"  void AlignmentPatternFinder__ctor_m1246855323 (AlignmentPatternFinder_t4197406735 * __this, BitMatrix_t1058711404 * ___image0, int32_t ___startX1, int32_t ___startY2, int32_t ___width3, int32_t ___height4, float ___moduleSize5, ResultPointCallback_t207829946 * ___resultPointCallback6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPatternFinder::find()
extern "C"  AlignmentPattern_t1786970569 * AlignmentPatternFinder_find_m3627414021 (AlignmentPatternFinder_t4197406735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> ZXing.QrCode.Internal.AlignmentPatternFinder::centerFromEnd(System.Int32[],System.Int32)
extern "C"  Nullable_1_t81078199  AlignmentPatternFinder_centerFromEnd_m1311044060 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___stateCount0, int32_t ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.AlignmentPatternFinder::foundPatternCross(System.Int32[])
extern "C"  bool AlignmentPatternFinder_foundPatternCross_m699222712 (AlignmentPatternFinder_t4197406735 * __this, Int32U5BU5D_t3230847821* ___stateCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> ZXing.QrCode.Internal.AlignmentPatternFinder::crossCheckVertical(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Nullable_1_t81078199  AlignmentPatternFinder_crossCheckVertical_m3012346582 (AlignmentPatternFinder_t4197406735 * __this, int32_t ___startI0, int32_t ___centerJ1, int32_t ___maxCount2, int32_t ___originalStateCountTotal3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32)
extern "C"  AlignmentPattern_t1786970569 * AlignmentPatternFinder_handlePossibleCenter_m3778747721 (AlignmentPatternFinder_t4197406735 * __this, Int32U5BU5D_t3230847821* ___stateCount0, int32_t ___i1, int32_t ___j2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
