﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t200133079;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CuponsControl
struct  CuponsControl_t4225786087  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean CuponsControl::showActiveCupons
	bool ___showActiveCupons_2;
	// UnityEngine.GameObject CuponsControl::container
	GameObject_t3674682005 * ___container_3;
	// UnityEngine.GameObject CuponsControl::cuponPrefab
	GameObject_t3674682005 * ___cuponPrefab_4;
	// UnityEngine.GameObject CuponsControl::loadingObj
	GameObject_t3674682005 * ___loadingObj_5;
	// UnityEngine.GameObject CuponsControl::cuponDoceNovembroPrefab
	GameObject_t3674682005 * ___cuponDoceNovembroPrefab_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CuponsControl::cuponsLst
	List_1_t747900261 * ___cuponsLst_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> CuponsControl::codeObjDict
	Dictionary_2_t200133079 * ___codeObjDict_8;
	// UnityEngine.GameObject CuponsControl::semCuponsImg
	GameObject_t3674682005 * ___semCuponsImg_9;
	// UnityEngine.GameObject CuponsControl::semCuponsResgatados
	GameObject_t3674682005 * ___semCuponsResgatados_10;
	// System.Single CuponsControl::timeOfUpdate
	float ___timeOfUpdate_11;
	// UnityEngine.GameObject CuponsControl::obj
	GameObject_t3674682005 * ___obj_12;

public:
	inline static int32_t get_offset_of_showActiveCupons_2() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___showActiveCupons_2)); }
	inline bool get_showActiveCupons_2() const { return ___showActiveCupons_2; }
	inline bool* get_address_of_showActiveCupons_2() { return &___showActiveCupons_2; }
	inline void set_showActiveCupons_2(bool value)
	{
		___showActiveCupons_2 = value;
	}

	inline static int32_t get_offset_of_container_3() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___container_3)); }
	inline GameObject_t3674682005 * get_container_3() const { return ___container_3; }
	inline GameObject_t3674682005 ** get_address_of_container_3() { return &___container_3; }
	inline void set_container_3(GameObject_t3674682005 * value)
	{
		___container_3 = value;
		Il2CppCodeGenWriteBarrier(&___container_3, value);
	}

	inline static int32_t get_offset_of_cuponPrefab_4() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___cuponPrefab_4)); }
	inline GameObject_t3674682005 * get_cuponPrefab_4() const { return ___cuponPrefab_4; }
	inline GameObject_t3674682005 ** get_address_of_cuponPrefab_4() { return &___cuponPrefab_4; }
	inline void set_cuponPrefab_4(GameObject_t3674682005 * value)
	{
		___cuponPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___cuponPrefab_4, value);
	}

	inline static int32_t get_offset_of_loadingObj_5() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___loadingObj_5)); }
	inline GameObject_t3674682005 * get_loadingObj_5() const { return ___loadingObj_5; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_5() { return &___loadingObj_5; }
	inline void set_loadingObj_5(GameObject_t3674682005 * value)
	{
		___loadingObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_5, value);
	}

	inline static int32_t get_offset_of_cuponDoceNovembroPrefab_6() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___cuponDoceNovembroPrefab_6)); }
	inline GameObject_t3674682005 * get_cuponDoceNovembroPrefab_6() const { return ___cuponDoceNovembroPrefab_6; }
	inline GameObject_t3674682005 ** get_address_of_cuponDoceNovembroPrefab_6() { return &___cuponDoceNovembroPrefab_6; }
	inline void set_cuponDoceNovembroPrefab_6(GameObject_t3674682005 * value)
	{
		___cuponDoceNovembroPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___cuponDoceNovembroPrefab_6, value);
	}

	inline static int32_t get_offset_of_cuponsLst_7() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___cuponsLst_7)); }
	inline List_1_t747900261 * get_cuponsLst_7() const { return ___cuponsLst_7; }
	inline List_1_t747900261 ** get_address_of_cuponsLst_7() { return &___cuponsLst_7; }
	inline void set_cuponsLst_7(List_1_t747900261 * value)
	{
		___cuponsLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___cuponsLst_7, value);
	}

	inline static int32_t get_offset_of_codeObjDict_8() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___codeObjDict_8)); }
	inline Dictionary_2_t200133079 * get_codeObjDict_8() const { return ___codeObjDict_8; }
	inline Dictionary_2_t200133079 ** get_address_of_codeObjDict_8() { return &___codeObjDict_8; }
	inline void set_codeObjDict_8(Dictionary_2_t200133079 * value)
	{
		___codeObjDict_8 = value;
		Il2CppCodeGenWriteBarrier(&___codeObjDict_8, value);
	}

	inline static int32_t get_offset_of_semCuponsImg_9() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___semCuponsImg_9)); }
	inline GameObject_t3674682005 * get_semCuponsImg_9() const { return ___semCuponsImg_9; }
	inline GameObject_t3674682005 ** get_address_of_semCuponsImg_9() { return &___semCuponsImg_9; }
	inline void set_semCuponsImg_9(GameObject_t3674682005 * value)
	{
		___semCuponsImg_9 = value;
		Il2CppCodeGenWriteBarrier(&___semCuponsImg_9, value);
	}

	inline static int32_t get_offset_of_semCuponsResgatados_10() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___semCuponsResgatados_10)); }
	inline GameObject_t3674682005 * get_semCuponsResgatados_10() const { return ___semCuponsResgatados_10; }
	inline GameObject_t3674682005 ** get_address_of_semCuponsResgatados_10() { return &___semCuponsResgatados_10; }
	inline void set_semCuponsResgatados_10(GameObject_t3674682005 * value)
	{
		___semCuponsResgatados_10 = value;
		Il2CppCodeGenWriteBarrier(&___semCuponsResgatados_10, value);
	}

	inline static int32_t get_offset_of_timeOfUpdate_11() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___timeOfUpdate_11)); }
	inline float get_timeOfUpdate_11() const { return ___timeOfUpdate_11; }
	inline float* get_address_of_timeOfUpdate_11() { return &___timeOfUpdate_11; }
	inline void set_timeOfUpdate_11(float value)
	{
		___timeOfUpdate_11 = value;
	}

	inline static int32_t get_offset_of_obj_12() { return static_cast<int32_t>(offsetof(CuponsControl_t4225786087, ___obj_12)); }
	inline GameObject_t3674682005 * get_obj_12() const { return ___obj_12; }
	inline GameObject_t3674682005 ** get_address_of_obj_12() { return &___obj_12; }
	inline void set_obj_12(GameObject_t3674682005 * value)
	{
		___obj_12 = value;
		Il2CppCodeGenWriteBarrier(&___obj_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
