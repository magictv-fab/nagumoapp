﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::.ctor()
#define LinkedList_1__ctor_m3387057913(__this, method) ((  void (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m3456946489(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t940842524 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1729898887(__this, ___value0, method) ((  void (*) (LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m2631462988(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t940842524 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3687724546(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2312516935(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1855661611(__this, method) ((  bool (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m362699890(__this, method) ((  bool (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m2779557968(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m1778188595(__this, ___node0, method) ((  void (*) (LinkedList_1_t940842524 *, LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::AddFirst(T)
#define LinkedList_1_AddFirst_m1014226436(__this, ___value0, method) ((  LinkedListNode_1_t2682942062 * (*) (LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedList_1_AddFirst_m2530092880_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::AddLast(T)
#define LinkedList_1_AddLast_m1638726591(__this, ___value0, method) ((  LinkedListNode_1_t2682942062 * (*) (LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::Clear()
#define LinkedList_1_Clear_m3222974499(__this, method) ((  void (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::Contains(T)
#define LinkedList_1_Contains_m2898792489(__this, ___value0, method) ((  bool (*) (LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m3202671031(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t940842524 *, TokenU5BU5D_t3282023226*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::Find(T)
#define LinkedList_1_Find_m3164051787(__this, ___value0, method) ((  LinkedListNode_1_t2682942062 * (*) (LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m3394258495(__this, method) ((  Enumerator_t2423997084  (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m1680461718(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t940842524 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m1532875918(__this, ___sender0, method) ((  void (*) (LinkedList_1_t940842524 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::Remove(T)
#define LinkedList_1_Remove_m1910818404(__this, ___value0, method) ((  bool (*) (LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m3677701123(__this, ___node0, method) ((  void (*) (LinkedList_1_t940842524 *, LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::RemoveLast()
#define LinkedList_1_RemoveLast_m1806019782(__this, method) ((  void (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::get_Count()
#define LinkedList_1_get_Count_m4001331244(__this, method) ((  int32_t (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.Token>::get_First()
#define LinkedList_1_get_First_m920216943(__this, method) ((  LinkedListNode_1_t2682942062 * (*) (LinkedList_1_t940842524 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
