﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.MultiFormatUPCEANReader
struct MultiFormatUPCEANReader_t2045775343;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.MultiFormatUPCEANReader::.ctor(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  void MultiFormatUPCEANReader__ctor_m2903316043 (MultiFormatUPCEANReader_t2045775343 * __this, Il2CppObject* ___hints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.MultiFormatUPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * MultiFormatUPCEANReader_decodeRow_m1638738013 (MultiFormatUPCEANReader_t2045775343 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.MultiFormatUPCEANReader::reset()
extern "C"  void MultiFormatUPCEANReader_reset_m1923422801 (MultiFormatUPCEANReader_t2045775343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
