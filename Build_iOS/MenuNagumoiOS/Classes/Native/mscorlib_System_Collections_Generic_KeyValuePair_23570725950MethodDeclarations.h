﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m585642602(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3570725950 *, int32_t, GameObject_t3674682005 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>::get_Key()
#define KeyValuePair_2_get_Key_m2633454014(__this, method) ((  int32_t (*) (KeyValuePair_2_t3570725950 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3858222463(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3570725950 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>::get_Value()
#define KeyValuePair_2_get_Value_m2117087458(__this, method) ((  GameObject_t3674682005 * (*) (KeyValuePair_2_t3570725950 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1453476351(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3570725950 *, GameObject_t3674682005 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GameObject>::ToString()
#define KeyValuePair_2_ToString_m1669264105(__this, method) ((  String_t* (*) (KeyValuePair_2_t3570725950 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
