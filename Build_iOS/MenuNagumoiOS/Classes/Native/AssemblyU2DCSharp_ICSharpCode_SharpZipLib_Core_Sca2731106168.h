﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "mscorlib_System_EventArgs2540831021.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs
struct  ScanFailureEventArgs_t2731106168  : public EventArgs_t2540831021
{
public:
	// System.String ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::name_
	String_t* ___name__1;
	// System.Exception ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::exception_
	Exception_t3991598821 * ___exception__2;
	// System.Boolean ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::continueRunning_
	bool ___continueRunning__3;

public:
	inline static int32_t get_offset_of_name__1() { return static_cast<int32_t>(offsetof(ScanFailureEventArgs_t2731106168, ___name__1)); }
	inline String_t* get_name__1() const { return ___name__1; }
	inline String_t** get_address_of_name__1() { return &___name__1; }
	inline void set_name__1(String_t* value)
	{
		___name__1 = value;
		Il2CppCodeGenWriteBarrier(&___name__1, value);
	}

	inline static int32_t get_offset_of_exception__2() { return static_cast<int32_t>(offsetof(ScanFailureEventArgs_t2731106168, ___exception__2)); }
	inline Exception_t3991598821 * get_exception__2() const { return ___exception__2; }
	inline Exception_t3991598821 ** get_address_of_exception__2() { return &___exception__2; }
	inline void set_exception__2(Exception_t3991598821 * value)
	{
		___exception__2 = value;
		Il2CppCodeGenWriteBarrier(&___exception__2, value);
	}

	inline static int32_t get_offset_of_continueRunning__3() { return static_cast<int32_t>(offsetof(ScanFailureEventArgs_t2731106168, ___continueRunning__3)); }
	inline bool get_continueRunning__3() const { return ___continueRunning__3; }
	inline bool* get_address_of_continueRunning__3() { return &___continueRunning__3; }
	inline void set_continueRunning__3(bool value)
	{
		___continueRunning__3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
