﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CopyPositionAngle
struct CopyPositionAngle_t1779009429;

#include "codegen/il2cpp-codegen.h"

// System.Void CopyPositionAngle::.ctor()
extern "C"  void CopyPositionAngle__ctor_m1364254070 (CopyPositionAngle_t1779009429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngle::Start()
extern "C"  void CopyPositionAngle_Start_m311391862 (CopyPositionAngle_t1779009429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngle::Update()
extern "C"  void CopyPositionAngle_Update_m1069065303 (CopyPositionAngle_t1779009429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngle::LateUpdate()
extern "C"  void CopyPositionAngle_LateUpdate_m1642227357 (CopyPositionAngle_t1779009429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngle::doFollow()
extern "C"  void CopyPositionAngle_doFollow_m2604361610 (CopyPositionAngle_t1779009429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
