﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// UnityEngine.Camera
struct Camera_t2727095145;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.GUIElement
struct GUIElement_t3775428101;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIElementHitTest
struct  GUIElementHitTest_t4187190930  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GUIElementHitTest::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.GUIElementHitTest::camera
	Camera_t2727095145 * ___camera_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GUIElementHitTest::screenPoint
	FsmVector3_t533912882 * ___screenPoint_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIElementHitTest::screenX
	FsmFloat_t2134102846 * ___screenX_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIElementHitTest::screenY
	FsmFloat_t2134102846 * ___screenY_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIElementHitTest::normalized
	FsmBool_t1075959796 * ___normalized_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUIElementHitTest::hitEvent
	FsmEvent_t2133468028 * ___hitEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIElementHitTest::storeResult
	FsmBool_t1075959796 * ___storeResult_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIElementHitTest::everyFrame
	FsmBool_t1075959796 * ___everyFrame_17;
	// UnityEngine.GUIElement HutongGames.PlayMaker.Actions.GUIElementHitTest::guiElement
	GUIElement_t3775428101 * ___guiElement_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GUIElementHitTest::gameObjectCached
	GameObject_t3674682005 * ___gameObjectCached_19;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_camera_10() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___camera_10)); }
	inline Camera_t2727095145 * get_camera_10() const { return ___camera_10; }
	inline Camera_t2727095145 ** get_address_of_camera_10() { return &___camera_10; }
	inline void set_camera_10(Camera_t2727095145 * value)
	{
		___camera_10 = value;
		Il2CppCodeGenWriteBarrier(&___camera_10, value);
	}

	inline static int32_t get_offset_of_screenPoint_11() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___screenPoint_11)); }
	inline FsmVector3_t533912882 * get_screenPoint_11() const { return ___screenPoint_11; }
	inline FsmVector3_t533912882 ** get_address_of_screenPoint_11() { return &___screenPoint_11; }
	inline void set_screenPoint_11(FsmVector3_t533912882 * value)
	{
		___screenPoint_11 = value;
		Il2CppCodeGenWriteBarrier(&___screenPoint_11, value);
	}

	inline static int32_t get_offset_of_screenX_12() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___screenX_12)); }
	inline FsmFloat_t2134102846 * get_screenX_12() const { return ___screenX_12; }
	inline FsmFloat_t2134102846 ** get_address_of_screenX_12() { return &___screenX_12; }
	inline void set_screenX_12(FsmFloat_t2134102846 * value)
	{
		___screenX_12 = value;
		Il2CppCodeGenWriteBarrier(&___screenX_12, value);
	}

	inline static int32_t get_offset_of_screenY_13() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___screenY_13)); }
	inline FsmFloat_t2134102846 * get_screenY_13() const { return ___screenY_13; }
	inline FsmFloat_t2134102846 ** get_address_of_screenY_13() { return &___screenY_13; }
	inline void set_screenY_13(FsmFloat_t2134102846 * value)
	{
		___screenY_13 = value;
		Il2CppCodeGenWriteBarrier(&___screenY_13, value);
	}

	inline static int32_t get_offset_of_normalized_14() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___normalized_14)); }
	inline FsmBool_t1075959796 * get_normalized_14() const { return ___normalized_14; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_14() { return &___normalized_14; }
	inline void set_normalized_14(FsmBool_t1075959796 * value)
	{
		___normalized_14 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_14, value);
	}

	inline static int32_t get_offset_of_hitEvent_15() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___hitEvent_15)); }
	inline FsmEvent_t2133468028 * get_hitEvent_15() const { return ___hitEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_hitEvent_15() { return &___hitEvent_15; }
	inline void set_hitEvent_15(FsmEvent_t2133468028 * value)
	{
		___hitEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___hitEvent_15, value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___storeResult_16)); }
	inline FsmBool_t1075959796 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t1075959796 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___everyFrame_17)); }
	inline FsmBool_t1075959796 * get_everyFrame_17() const { return ___everyFrame_17; }
	inline FsmBool_t1075959796 ** get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(FsmBool_t1075959796 * value)
	{
		___everyFrame_17 = value;
		Il2CppCodeGenWriteBarrier(&___everyFrame_17, value);
	}

	inline static int32_t get_offset_of_guiElement_18() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___guiElement_18)); }
	inline GUIElement_t3775428101 * get_guiElement_18() const { return ___guiElement_18; }
	inline GUIElement_t3775428101 ** get_address_of_guiElement_18() { return &___guiElement_18; }
	inline void set_guiElement_18(GUIElement_t3775428101 * value)
	{
		___guiElement_18 = value;
		Il2CppCodeGenWriteBarrier(&___guiElement_18, value);
	}

	inline static int32_t get_offset_of_gameObjectCached_19() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t4187190930, ___gameObjectCached_19)); }
	inline GameObject_t3674682005 * get_gameObjectCached_19() const { return ___gameObjectCached_19; }
	inline GameObject_t3674682005 ** get_address_of_gameObjectCached_19() { return &___gameObjectCached_19; }
	inline void set_gameObjectCached_19(GameObject_t3674682005 * value)
	{
		___gameObjectCached_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectCached_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
