﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144
struct DataMatrixSymbolInfo144_t3368906246;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::.ctor()
extern "C"  void DataMatrixSymbolInfo144__ctor_m2566887302 (DataMatrixSymbolInfo144_t3368906246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::getInterleavedBlockCount()
extern "C"  int32_t DataMatrixSymbolInfo144_getInterleavedBlockCount_m2646060417 (DataMatrixSymbolInfo144_t3368906246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::getDataLengthForInterleavedBlock(System.Int32)
extern "C"  int32_t DataMatrixSymbolInfo144_getDataLengthForInterleavedBlock_m434627140 (DataMatrixSymbolInfo144_t3368906246 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
