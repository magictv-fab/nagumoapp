﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.NetworkReachability>
struct IEqualityComparer_1_t1403437439;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<UnityEngine.NetworkReachability>
struct ICollection_1_t1506993022;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>[]
struct KeyValuePair_2U5BU5D_t1110487188;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>
struct IEnumerator_1_t1447209346;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.NetworkReachability,System.Object>
struct KeyCollection_t1263323042;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>
struct ValueCollection_t2632136600;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En953886983.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2588861167_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2588861167(__this, method) ((  void (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2__ctor_m2588861167_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m788574182_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m788574182(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m788574182_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m65841600_gshared (Dictionary_2_t3931530887 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m65841600(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3931530887 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m65841600_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1643839408_gshared (Dictionary_2_t3931530887 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1643839408(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3931530887 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1643839408_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3491327679_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3491327679(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3491327679_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m339619317_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m339619317(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m339619317_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2324203939_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2324203939(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2324203939_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m271220178_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m271220178(__this, method) ((  bool (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m271220178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m185663207_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m185663207(__this, method) ((  bool (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m185663207_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1710111865_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1710111865(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1710111865_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1578527902_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1578527902(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1578527902_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3626624755_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3626624755(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3626624755_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2841196579_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2841196579(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2841196579_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m230810332_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m230810332(__this, ___key0, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m230810332_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m598811153_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m598811153(__this, method) ((  bool (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m598811153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4143361789_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4143361789(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4143361789_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4237898517_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4237898517(__this, method) ((  bool (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4237898517_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m732524914_gshared (Dictionary_2_t3931530887 * __this, KeyValuePair_2_t3830311593  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m732524914(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3931530887 *, KeyValuePair_2_t3830311593 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m732524914_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m707515792_gshared (Dictionary_2_t3931530887 * __this, KeyValuePair_2_t3830311593  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m707515792(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3931530887 *, KeyValuePair_2_t3830311593 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m707515792_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2341185750_gshared (Dictionary_2_t3931530887 * __this, KeyValuePair_2U5BU5D_t1110487188* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2341185750(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3931530887 *, KeyValuePair_2U5BU5D_t1110487188*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2341185750_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3738312885_gshared (Dictionary_2_t3931530887 * __this, KeyValuePair_2_t3830311593  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3738312885(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3931530887 *, KeyValuePair_2_t3830311593 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3738312885_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1551962421_gshared (Dictionary_2_t3931530887 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1551962421(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1551962421_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1511168240_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1511168240(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1511168240_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2195823405_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2195823405(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2195823405_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3564084744_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3564084744(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3564084744_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m357206487_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m357206487(__this, method) ((  int32_t (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_get_Count_m357206487_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m4022064820_gshared (Dictionary_2_t3931530887 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m4022064820(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m4022064820_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4050111087_gshared (Dictionary_2_t3931530887 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4050111087(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3931530887 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m4050111087_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m741655143_gshared (Dictionary_2_t3931530887 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m741655143(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3931530887 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m741655143_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1606259760_gshared (Dictionary_2_t3931530887 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1606259760(__this, ___size0, method) ((  void (*) (Dictionary_2_t3931530887 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1606259760_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m252795948_gshared (Dictionary_2_t3931530887 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m252795948(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m252795948_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3830311593  Dictionary_2_make_pair_m757748856_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m757748856(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3830311593  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m757748856_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2305487230_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2305487230(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2305487230_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3534642238_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3534642238(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3534642238_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3311296035_gshared (Dictionary_2_t3931530887 * __this, KeyValuePair_2U5BU5D_t1110487188* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3311296035(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3931530887 *, KeyValuePair_2U5BU5D_t1110487188*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3311296035_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3003081513_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3003081513(__this, method) ((  void (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_Resize_m3003081513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1877421734_gshared (Dictionary_2_t3931530887 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1877421734(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3931530887 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1877421734_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m4289961754_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m4289961754(__this, method) ((  void (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_Clear_m4289961754_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1010649088_gshared (Dictionary_2_t3931530887 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1010649088(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3931530887 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1010649088_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3233061888_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3233061888(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3233061888_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1641649933_gshared (Dictionary_2_t3931530887 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1641649933(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3931530887 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1641649933_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m716953975_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m716953975(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m716953975_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2935353584_gshared (Dictionary_2_t3931530887 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2935353584(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3931530887 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2935353584_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4261519449_gshared (Dictionary_2_t3931530887 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4261519449(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3931530887 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m4261519449_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::get_Keys()
extern "C"  KeyCollection_t1263323042 * Dictionary_2_get_Keys_m812050118_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m812050118(__this, method) ((  KeyCollection_t1263323042 * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_get_Keys_m812050118_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::get_Values()
extern "C"  ValueCollection_t2632136600 * Dictionary_2_get_Values_m1679922502_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1679922502(__this, method) ((  ValueCollection_t2632136600 * (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_get_Values_m1679922502_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1755346137_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1755346137(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1755346137_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1846956377_gshared (Dictionary_2_t3931530887 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1846956377(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3931530887 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1846956377_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m756522707_gshared (Dictionary_2_t3931530887 * __this, KeyValuePair_2_t3830311593  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m756522707(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3931530887 *, KeyValuePair_2_t3830311593 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m756522707_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::GetEnumerator()
extern "C"  Enumerator_t953886983  Dictionary_2_GetEnumerator_m871254708_gshared (Dictionary_2_t3931530887 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m871254708(__this, method) ((  Enumerator_t953886983  (*) (Dictionary_2_t3931530887 *, const MethodInfo*))Dictionary_2_GetEnumerator_m871254708_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m3767734849_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m3767734849(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3767734849_gshared)(__this /* static, unused */, ___key0, ___value1, method)
