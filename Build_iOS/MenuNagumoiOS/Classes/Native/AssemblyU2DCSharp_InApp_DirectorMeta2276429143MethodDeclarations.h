﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.DirectorMeta
struct DirectorMeta_t2276429143;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.DirectorMeta::.ctor()
extern "C"  void DirectorMeta__ctor_m2892386840 (DirectorMeta_t2276429143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.DirectorMeta InApp.DirectorMeta::Parse(System.String)
extern "C"  DirectorMeta_t2276429143 * DirectorMeta_Parse_m3192422259 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
