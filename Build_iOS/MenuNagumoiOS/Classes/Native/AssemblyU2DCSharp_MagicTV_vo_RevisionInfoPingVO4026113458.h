﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.CurrentVersionRevisionVO
struct CurrentVersionRevisionVO_t3939651409;
// MagicTV.vo.ErrorResultVO
struct ErrorResultVO_t356660816;

#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.RevisionInfoPingVO
struct  RevisionInfoPingVO_t4026113458  : public VOWithId_t2461972856
{
public:
	// System.String MagicTV.vo.RevisionInfoPingVO::jsonrpc
	String_t* ___jsonrpc_1;
	// MagicTV.vo.CurrentVersionRevisionVO MagicTV.vo.RevisionInfoPingVO::result
	CurrentVersionRevisionVO_t3939651409 * ___result_2;
	// MagicTV.vo.ErrorResultVO MagicTV.vo.RevisionInfoPingVO::error
	ErrorResultVO_t356660816 * ___error_3;

public:
	inline static int32_t get_offset_of_jsonrpc_1() { return static_cast<int32_t>(offsetof(RevisionInfoPingVO_t4026113458, ___jsonrpc_1)); }
	inline String_t* get_jsonrpc_1() const { return ___jsonrpc_1; }
	inline String_t** get_address_of_jsonrpc_1() { return &___jsonrpc_1; }
	inline void set_jsonrpc_1(String_t* value)
	{
		___jsonrpc_1 = value;
		Il2CppCodeGenWriteBarrier(&___jsonrpc_1, value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(RevisionInfoPingVO_t4026113458, ___result_2)); }
	inline CurrentVersionRevisionVO_t3939651409 * get_result_2() const { return ___result_2; }
	inline CurrentVersionRevisionVO_t3939651409 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(CurrentVersionRevisionVO_t3939651409 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier(&___result_2, value);
	}

	inline static int32_t get_offset_of_error_3() { return static_cast<int32_t>(offsetof(RevisionInfoPingVO_t4026113458, ___error_3)); }
	inline ErrorResultVO_t356660816 * get_error_3() const { return ___error_3; }
	inline ErrorResultVO_t356660816 ** get_address_of_error_3() { return &___error_3; }
	inline void set_error_3(ErrorResultVO_t356660816 * value)
	{
		___error_3 = value;
		Il2CppCodeGenWriteBarrier(&___error_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
