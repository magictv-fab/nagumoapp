﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PublicationData[]
struct PublicationDataU5BU5D_t1155489811;
// PublicationData
struct PublicationData_t473548758;
// System.Exception
struct Exception_t3991598821;
// System.Object
struct Il2CppObject;
// PublishManager
struct PublishManager_t4010203070;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublishManager/<ILoadPublishUpdate>c__Iterator78
struct  U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936  : public Il2CppObject
{
public:
	// System.String PublishManager/<ILoadPublishUpdate>c__Iterator78::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PublishManager/<ILoadPublishUpdate>c__Iterator78::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] PublishManager/<ILoadPublishUpdate>c__Iterator78::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW PublishManager/<ILoadPublishUpdate>c__Iterator78::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> PublishManager/<ILoadPublishUpdate>c__Iterator78::<$s_342>__4
	Enumerator_t767573031  ___U3CU24s_342U3E__4_4;
	// UnityEngine.GameObject PublishManager/<ILoadPublishUpdate>c__Iterator78::<obj>__5
	GameObject_t3674682005 * ___U3CobjU3E__5_5;
	// System.String PublishManager/<ILoadPublishUpdate>c__Iterator78::<message>__6
	String_t* ___U3CmessageU3E__6_6;
	// PublicationData[] PublishManager/<ILoadPublishUpdate>c__Iterator78::<$s_343>__7
	PublicationDataU5BU5D_t1155489811* ___U3CU24s_343U3E__7_7;
	// System.Int32 PublishManager/<ILoadPublishUpdate>c__Iterator78::<$s_344>__8
	int32_t ___U3CU24s_344U3E__8_8;
	// PublicationData PublishManager/<ILoadPublishUpdate>c__Iterator78::<publicationData>__9
	PublicationData_t473548758 * ___U3CpublicationDataU3E__9_9;
	// System.Exception PublishManager/<ILoadPublishUpdate>c__Iterator78::<ex>__10
	Exception_t3991598821 * ___U3CexU3E__10_10;
	// System.Int32 PublishManager/<ILoadPublishUpdate>c__Iterator78::$PC
	int32_t ___U24PC_11;
	// System.Object PublishManager/<ILoadPublishUpdate>c__Iterator78::$current
	Il2CppObject * ___U24current_12;
	// PublishManager PublishManager/<ILoadPublishUpdate>c__Iterator78::<>f__this
	PublishManager_t4010203070 * ___U3CU3Ef__this_13;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_342U3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CU24s_342U3E__4_4)); }
	inline Enumerator_t767573031  get_U3CU24s_342U3E__4_4() const { return ___U3CU24s_342U3E__4_4; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_342U3E__4_4() { return &___U3CU24s_342U3E__4_4; }
	inline void set_U3CU24s_342U3E__4_4(Enumerator_t767573031  value)
	{
		___U3CU24s_342U3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CobjU3E__5_5)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__5_5() const { return ___U3CobjU3E__5_5; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__5_5() { return &___U3CobjU3E__5_5; }
	inline void set_U3CobjU3E__5_5(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__6_6() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CmessageU3E__6_6)); }
	inline String_t* get_U3CmessageU3E__6_6() const { return ___U3CmessageU3E__6_6; }
	inline String_t** get_address_of_U3CmessageU3E__6_6() { return &___U3CmessageU3E__6_6; }
	inline void set_U3CmessageU3E__6_6(String_t* value)
	{
		___U3CmessageU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CU24s_343U3E__7_7() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CU24s_343U3E__7_7)); }
	inline PublicationDataU5BU5D_t1155489811* get_U3CU24s_343U3E__7_7() const { return ___U3CU24s_343U3E__7_7; }
	inline PublicationDataU5BU5D_t1155489811** get_address_of_U3CU24s_343U3E__7_7() { return &___U3CU24s_343U3E__7_7; }
	inline void set_U3CU24s_343U3E__7_7(PublicationDataU5BU5D_t1155489811* value)
	{
		___U3CU24s_343U3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_343U3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_344U3E__8_8() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CU24s_344U3E__8_8)); }
	inline int32_t get_U3CU24s_344U3E__8_8() const { return ___U3CU24s_344U3E__8_8; }
	inline int32_t* get_address_of_U3CU24s_344U3E__8_8() { return &___U3CU24s_344U3E__8_8; }
	inline void set_U3CU24s_344U3E__8_8(int32_t value)
	{
		___U3CU24s_344U3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CpublicationDataU3E__9_9() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CpublicationDataU3E__9_9)); }
	inline PublicationData_t473548758 * get_U3CpublicationDataU3E__9_9() const { return ___U3CpublicationDataU3E__9_9; }
	inline PublicationData_t473548758 ** get_address_of_U3CpublicationDataU3E__9_9() { return &___U3CpublicationDataU3E__9_9; }
	inline void set_U3CpublicationDataU3E__9_9(PublicationData_t473548758 * value)
	{
		___U3CpublicationDataU3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpublicationDataU3E__9_9, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__10_10() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CexU3E__10_10)); }
	inline Exception_t3991598821 * get_U3CexU3E__10_10() const { return ___U3CexU3E__10_10; }
	inline Exception_t3991598821 ** get_address_of_U3CexU3E__10_10() { return &___U3CexU3E__10_10; }
	inline void set_U3CexU3E__10_10(Exception_t3991598821 * value)
	{
		___U3CexU3E__10_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__10_10, value);
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_13() { return static_cast<int32_t>(offsetof(U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936, ___U3CU3Ef__this_13)); }
	inline PublishManager_t4010203070 * get_U3CU3Ef__this_13() const { return ___U3CU3Ef__this_13; }
	inline PublishManager_t4010203070 ** get_address_of_U3CU3Ef__this_13() { return &___U3CU3Ef__this_13; }
	inline void set_U3CU3Ef__this_13(PublishManager_t4010203070 * value)
	{
		___U3CU3Ef__this_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
