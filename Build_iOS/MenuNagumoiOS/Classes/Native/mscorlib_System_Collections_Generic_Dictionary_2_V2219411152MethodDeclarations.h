﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>
struct Dictionary_2_t4287577744;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2219411152.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m575669321_gshared (Enumerator_t2219411152 * __this, Dictionary_2_t4287577744 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m575669321(__this, ___host0, method) ((  void (*) (Enumerator_t2219411152 *, Dictionary_2_t4287577744 *, const MethodInfo*))Enumerator__ctor_m575669321_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2161917560_gshared (Enumerator_t2219411152 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2161917560(__this, method) ((  Il2CppObject * (*) (Enumerator_t2219411152 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2161917560_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4259139788_gshared (Enumerator_t2219411152 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4259139788(__this, method) ((  void (*) (Enumerator_t2219411152 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4259139788_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m638770987_gshared (Enumerator_t2219411152 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m638770987(__this, method) ((  void (*) (Enumerator_t2219411152 *, const MethodInfo*))Enumerator_Dispose_m638770987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m266234296_gshared (Enumerator_t2219411152 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m266234296(__this, method) ((  bool (*) (Enumerator_t2219411152 *, const MethodInfo*))Enumerator_MoveNext_m266234296_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m711999946_gshared (Enumerator_t2219411152 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m711999946(__this, method) ((  Il2CppObject * (*) (Enumerator_t2219411152 *, const MethodInfo*))Enumerator_get_Current_m711999946_gshared)(__this, method)
