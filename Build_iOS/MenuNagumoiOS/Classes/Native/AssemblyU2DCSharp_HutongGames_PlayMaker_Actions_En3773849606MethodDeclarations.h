﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableGUI
struct EnableGUI_t3773849606;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnableGUI::.ctor()
extern "C"  void EnableGUI__ctor_m2659273712 (EnableGUI_t3773849606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::Reset()
extern "C"  void EnableGUI_Reset_m305706653 (EnableGUI_t3773849606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::OnEnter()
extern "C"  void EnableGUI_OnEnter_m3743192007 (EnableGUI_t3773849606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
