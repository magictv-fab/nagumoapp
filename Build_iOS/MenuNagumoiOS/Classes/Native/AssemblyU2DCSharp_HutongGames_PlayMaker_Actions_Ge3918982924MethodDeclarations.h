﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetIPhoneSettings
struct GetIPhoneSettings_t3918982924;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetIPhoneSettings::.ctor()
extern "C"  void GetIPhoneSettings__ctor_m1881332650 (GetIPhoneSettings_t3918982924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetIPhoneSettings::Reset()
extern "C"  void GetIPhoneSettings_Reset_m3822732887 (GetIPhoneSettings_t3918982924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetIPhoneSettings::OnEnter()
extern "C"  void GetIPhoneSettings_OnEnter_m3466140929 (GetIPhoneSettings_t3918982924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
