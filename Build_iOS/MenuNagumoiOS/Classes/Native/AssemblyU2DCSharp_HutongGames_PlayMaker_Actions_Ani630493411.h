﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorPlay
struct  AnimatorPlay_t630493411  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorPlay::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimatorPlay::stateName
	FsmString_t952858651 * ___stateName_10;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimatorPlay::layer
	FsmInt_t1596138449 * ___layer_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorPlay::normalizedTime
	FsmFloat_t2134102846 * ___normalizedTime_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorPlay::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimatorPlay_t630493411, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_stateName_10() { return static_cast<int32_t>(offsetof(AnimatorPlay_t630493411, ___stateName_10)); }
	inline FsmString_t952858651 * get_stateName_10() const { return ___stateName_10; }
	inline FsmString_t952858651 ** get_address_of_stateName_10() { return &___stateName_10; }
	inline void set_stateName_10(FsmString_t952858651 * value)
	{
		___stateName_10 = value;
		Il2CppCodeGenWriteBarrier(&___stateName_10, value);
	}

	inline static int32_t get_offset_of_layer_11() { return static_cast<int32_t>(offsetof(AnimatorPlay_t630493411, ___layer_11)); }
	inline FsmInt_t1596138449 * get_layer_11() const { return ___layer_11; }
	inline FsmInt_t1596138449 ** get_address_of_layer_11() { return &___layer_11; }
	inline void set_layer_11(FsmInt_t1596138449 * value)
	{
		___layer_11 = value;
		Il2CppCodeGenWriteBarrier(&___layer_11, value);
	}

	inline static int32_t get_offset_of_normalizedTime_12() { return static_cast<int32_t>(offsetof(AnimatorPlay_t630493411, ___normalizedTime_12)); }
	inline FsmFloat_t2134102846 * get_normalizedTime_12() const { return ___normalizedTime_12; }
	inline FsmFloat_t2134102846 ** get_address_of_normalizedTime_12() { return &___normalizedTime_12; }
	inline void set_normalizedTime_12(FsmFloat_t2134102846 * value)
	{
		___normalizedTime_12 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedTime_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(AnimatorPlay_t630493411, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
