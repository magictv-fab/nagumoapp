﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImages/OnLoadCompleted
struct OnLoadCompleted_t2465843575;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void DownloadImages/OnLoadCompleted::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLoadCompleted__ctor_m12652702 (OnLoadCompleted_t2465843575 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/OnLoadCompleted::Invoke()
extern "C"  void OnLoadCompleted_Invoke_m1959151224 (OnLoadCompleted_t2465843575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult DownloadImages/OnLoadCompleted::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLoadCompleted_BeginInvoke_m750171787 (OnLoadCompleted_t2465843575 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/OnLoadCompleted::EndInvoke(System.IAsyncResult)
extern "C"  void OnLoadCompleted_EndInvoke_m2483126702 (OnLoadCompleted_t2465843575 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
