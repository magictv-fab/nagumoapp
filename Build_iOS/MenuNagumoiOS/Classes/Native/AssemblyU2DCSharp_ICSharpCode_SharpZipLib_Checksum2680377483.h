﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Checksums.Adler32
struct  Adler32_t2680377483  : public Il2CppObject
{
public:
	// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Adler32::checksum
	uint32_t ___checksum_1;

public:
	inline static int32_t get_offset_of_checksum_1() { return static_cast<int32_t>(offsetof(Adler32_t2680377483, ___checksum_1)); }
	inline uint32_t get_checksum_1() const { return ___checksum_1; }
	inline uint32_t* get_address_of_checksum_1() { return &___checksum_1; }
	inline void set_checksum_1(uint32_t value)
	{
		___checksum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
