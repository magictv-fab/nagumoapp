﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate
struct ZipUpdate_t3527085946;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// ICSharpCode.SharpZipLib.Zip.IStaticDataSource
struct IStaticDataSource_t3265679022;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3174865049.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2599129714.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(System.String,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipUpdate__ctor_m922064678 (ZipUpdate_t3527085946 * __this, String_t* ___fileName0, ZipEntry_t3141689087 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(System.String,System.String,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  void ZipUpdate__ctor_m608164272 (ZipUpdate_t3527085946 * __this, String_t* ___fileName0, String_t* ___entryName1, int32_t ___compressionMethod2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(System.String,System.String)
extern "C"  void ZipUpdate__ctor_m2138728941 (ZipUpdate_t3527085946 * __this, String_t* ___fileName0, String_t* ___entryName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(ICSharpCode.SharpZipLib.Zip.IStaticDataSource,System.String,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  void ZipUpdate__ctor_m3594131842 (ZipUpdate_t3527085946 * __this, Il2CppObject * ___dataSource0, String_t* ___entryName1, int32_t ___compressionMethod2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(ICSharpCode.SharpZipLib.Zip.IStaticDataSource,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipUpdate__ctor_m774781844 (ZipUpdate_t3527085946 * __this, Il2CppObject * ___dataSource0, ZipEntry_t3141689087 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipUpdate__ctor_m667134997 (ZipUpdate_t3527085946 * __this, ZipEntry_t3141689087 * ___original0, ZipEntry_t3141689087 * ___updated1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile/UpdateCommand,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipUpdate__ctor_m3333961576 (ZipUpdate_t3527085946 * __this, int32_t ___command0, ZipEntry_t3141689087 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipUpdate__ctor_m4249088290 (ZipUpdate_t3527085946 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_Entry()
extern "C"  ZipEntry_t3141689087 * ZipUpdate_get_Entry_m239432966 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_OutEntry()
extern "C"  ZipEntry_t3141689087 * ZipUpdate_get_OutEntry_m43963602 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipFile/UpdateCommand ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_Command()
extern "C"  int32_t ZipUpdate_get_Command_m4203477074 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_Filename()
extern "C"  String_t* ZipUpdate_get_Filename_m3834982272 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_SizePatchOffset()
extern "C"  int64_t ZipUpdate_get_SizePatchOffset_m1492432623 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::set_SizePatchOffset(System.Int64)
extern "C"  void ZipUpdate_set_SizePatchOffset_m1084826046 (ZipUpdate_t3527085946 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_CrcPatchOffset()
extern "C"  int64_t ZipUpdate_get_CrcPatchOffset_m3999296788 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::set_CrcPatchOffset(System.Int64)
extern "C"  void ZipUpdate_set_CrcPatchOffset_m2914456521 (ZipUpdate_t3527085946 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::get_OffsetBasedSize()
extern "C"  int64_t ZipUpdate_get_OffsetBasedSize_m2916343798 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::set_OffsetBasedSize(System.Int64)
extern "C"  void ZipUpdate_set_OffsetBasedSize_m3232233541 (ZipUpdate_t3527085946 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::GetSource()
extern "C"  Stream_t1561764144 * ZipUpdate_GetSource_m3395612532 (ZipUpdate_t3527085946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
