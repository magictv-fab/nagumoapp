﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// ZXing.Binarizer
struct Binarizer_t1492033400;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Binarizer1492033400.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.BinaryBitmap::.ctor(ZXing.Binarizer)
extern "C"  void BinaryBitmap__ctor_m1263795045 (BinaryBitmap_t2444664454 * __this, Binarizer_t1492033400 * ___binarizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.BinaryBitmap::get_Width()
extern "C"  int32_t BinaryBitmap_get_Width_m213552494 (BinaryBitmap_t2444664454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.BinaryBitmap::get_Height()
extern "C"  int32_t BinaryBitmap_get_Height_m2828974369 (BinaryBitmap_t2444664454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.BinaryBitmap::getBlackRow(System.Int32,ZXing.Common.BitArray)
extern "C"  BitArray_t4163851164 * BinaryBitmap_getBlackRow_m2895517498 (BinaryBitmap_t2444664454 * __this, int32_t ___y0, BitArray_t4163851164 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.BinaryBitmap::get_BlackMatrix()
extern "C"  BitMatrix_t1058711404 * BinaryBitmap_get_BlackMatrix_m1804430318 (BinaryBitmap_t2444664454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.BinaryBitmap::get_RotateSupported()
extern "C"  bool BinaryBitmap_get_RotateSupported_m3015677781 (BinaryBitmap_t2444664454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.BinaryBitmap ZXing.BinaryBitmap::rotateCounterClockwise()
extern "C"  BinaryBitmap_t2444664454 * BinaryBitmap_rotateCounterClockwise_m1005881047 (BinaryBitmap_t2444664454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.BinaryBitmap::ToString()
extern "C"  String_t* BinaryBitmap_ToString_m1350657650 (BinaryBitmap_t2444664454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
