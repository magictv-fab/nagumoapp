﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.VOWithId
struct VOWithId_t2461972856;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.VOWithId::.ctor()
extern "C"  void VOWithId__ctor_m1006369755 (VOWithId_t2461972856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
