﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatAddMutiple
struct FloatAddMutiple_t949141333;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::.ctor()
extern "C"  void FloatAddMutiple__ctor_m873260929 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::Reset()
extern "C"  void FloatAddMutiple_Reset_m2814661166 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::OnEnter()
extern "C"  void FloatAddMutiple_OnEnter_m1076858648 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::OnUpdate()
extern "C"  void FloatAddMutiple_OnUpdate_m2451406379 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::DoFloatAdd()
extern "C"  void FloatAddMutiple_DoFloatAdd_m2489819763 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
