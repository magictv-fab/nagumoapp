﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.download.BulkBundleDownloader
struct BulkBundleDownloader_t2985631149;
// MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler
struct OnFileDonwloadCompleteEventHandler_t3421606269;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.BundleVO>
struct IEnumerable_1_t990463734;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.Collections.Generic.List`1<MagicTV.download.BundleDownloadItem>
struct List_1_t2084123921;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ARM.utils.download.GenericDownloadManager
struct GenericDownloadManager_t3298305714;
// ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler
struct OnErrorEventHandler_t1722022561;
// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler
struct OnCompleteEventHandler_t2318258528;
// ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler
struct OnProgressEventHandler_t3695678548;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_download_BulkBundleDownl3421606269.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_MagicTV_download_BulkBundleDownl2985631149.h"
#include "AssemblyU2DCSharp_ARM_utils_download_GenericDownlo3298305714.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst1722022561.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2318258528.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst3695678548.h"

// System.Void MagicTV.download.BulkBundleDownloader::.ctor()
extern "C"  void BulkBundleDownloader__ctor_m2232691707 (BulkBundleDownloader_t2985631149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::.cctor()
extern "C"  void BulkBundleDownloader__cctor_m11869970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::AddFileDownloadCompleteEventHandler(MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler)
extern "C"  void BulkBundleDownloader_AddFileDownloadCompleteEventHandler_m471613418 (Il2CppObject * __this /* static, unused */, OnFileDonwloadCompleteEventHandler_t3421606269 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::addBundle(System.Collections.Generic.IEnumerable`1<MagicTV.vo.BundleVO>)
extern "C"  void BulkBundleDownloader_addBundle_m3648264512 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___bundleList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::addBundle(MagicTV.vo.BundleVO)
extern "C"  void BulkBundleDownloader_addBundle_m2006536901 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<MagicTV.download.BundleDownloadItem> MagicTV.download.BulkBundleDownloader::filterBundleToFileList(System.Collections.Generic.IEnumerable`1<MagicTV.vo.BundleVO>)
extern "C"  List_1_t2084123921 * BulkBundleDownloader_filterBundleToFileList_m1070455209 (BulkBundleDownloader_t2985631149 * __this, Il2CppObject* ___bundleList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::startQueue()
extern "C"  void BulkBundleDownloader_startQueue_m4121071064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::doStartQueue()
extern "C"  void BulkBundleDownloader_doStartQueue_m1650451459 (BulkBundleDownloader_t2985631149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::downloadItem()
extern "C"  void BulkBundleDownloader_downloadItem_m2842727108 (BulkBundleDownloader_t2985631149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::Cancel()
extern "C"  void BulkBundleDownloader_Cancel_m775058403 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::Reset()
extern "C"  void BulkBundleDownloader_Reset_m4174091944 (BulkBundleDownloader_t2985631149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::prepare()
extern "C"  void BulkBundleDownloader_prepare_m3355030144 (BulkBundleDownloader_t2985631149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::onErrorEventHandler(System.String)
extern "C"  void BulkBundleDownloader_onErrorEventHandler_m2301251056 (BulkBundleDownloader_t2985631149 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::onDonwloadComplete(UnityEngine.WWW)
extern "C"  void BulkBundleDownloader_onDonwloadComplete_m2067566583 (BulkBundleDownloader_t2985631149 * __this, WWW_t3134621005 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::dispatchItemComplete(UnityEngine.WWW)
extern "C"  void BulkBundleDownloader_dispatchItemComplete_m431267651 (BulkBundleDownloader_t2985631149 * __this, WWW_t3134621005 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::onProgressEventHandler(System.Single)
extern "C"  void BulkBundleDownloader_onProgressEventHandler_m1164413542 (BulkBundleDownloader_t2985631149 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject MagicTV.download.BulkBundleDownloader::get_BulkBundleDownloaderGO()
extern "C"  GameObject_t3674682005 * BulkBundleDownloader_get_BulkBundleDownloaderGO_m3843671758 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.download.BulkBundleDownloader MagicTV.download.BulkBundleDownloader::get_InstanceBulkBundleDownloader()
extern "C"  BulkBundleDownloader_t2985631149 * BulkBundleDownloader_get_InstanceBulkBundleDownloader_m694906925 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::set_InstanceBulkBundleDownloader(MagicTV.download.BulkBundleDownloader)
extern "C"  void BulkBundleDownloader_set_InstanceBulkBundleDownloader_m3740944158 (Il2CppObject * __this /* static, unused */, BulkBundleDownloader_t2985631149 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject MagicTV.download.BulkBundleDownloader::get_GenericDownloadManagerGO()
extern "C"  GameObject_t3674682005 * BulkBundleDownloader_get_GenericDownloadManagerGO_m3631147507 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.utils.download.GenericDownloadManager MagicTV.download.BulkBundleDownloader::get_InstanceGenericDownload()
extern "C"  GenericDownloadManager_t3298305714 * BulkBundleDownloader_get_InstanceGenericDownload_m2421883656 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::set_InstanceGenericDownload(ARM.utils.download.GenericDownloadManager)
extern "C"  void BulkBundleDownloader_set_InstanceGenericDownload_m3404121191 (Il2CppObject * __this /* static, unused */, GenericDownloadManager_t3298305714 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::AddErrorEventhandlerStatic(ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler)
extern "C"  void BulkBundleDownloader_AddErrorEventhandlerStatic_m464593261 (Il2CppObject * __this /* static, unused */, OnErrorEventHandler_t1722022561 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::AddCompleteEventhandlerStatic(ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler)
extern "C"  void BulkBundleDownloader_AddCompleteEventhandlerStatic_m3164957249 (Il2CppObject * __this /* static, unused */, OnCompleteEventHandler_t2318258528 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.BulkBundleDownloader::AddProgressEventhandlerStatic(ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler)
extern "C"  void BulkBundleDownloader_AddProgressEventhandlerStatic_m857706073 (Il2CppObject * __this /* static, unused */, OnProgressEventHandler_t3695678548 * ___enventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
