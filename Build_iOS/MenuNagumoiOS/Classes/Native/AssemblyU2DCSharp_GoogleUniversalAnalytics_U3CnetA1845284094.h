﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// GoogleUniversalAnalytics
struct GoogleUniversalAnalytics_t2200773556;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleUniversalAnalytics/<netActivity>c__Iterator33
struct  U3CnetActivityU3Ec__Iterator33_t1845284094  : public Il2CppObject
{
public:
	// UnityEngine.NetworkReachability GoogleUniversalAnalytics/<netActivity>c__Iterator33::<prevReachability>__0
	int32_t ___U3CprevReachabilityU3E__0_0;
	// UnityEngine.NetworkReachability GoogleUniversalAnalytics/<netActivity>c__Iterator33::<networkReachability>__1
	int32_t ___U3CnetworkReachabilityU3E__1_1;
	// UnityEngine.WWW GoogleUniversalAnalytics/<netActivity>c__Iterator33::<www>__2
	WWW_t3134621005 * ___U3CwwwU3E__2_2;
	// System.Byte[] GoogleUniversalAnalytics/<netActivity>c__Iterator33::<result>__3
	ByteU5BU5D_t4260760469* ___U3CresultU3E__3_3;
	// System.Int32 GoogleUniversalAnalytics/<netActivity>c__Iterator33::$PC
	int32_t ___U24PC_4;
	// System.Object GoogleUniversalAnalytics/<netActivity>c__Iterator33::$current
	Il2CppObject * ___U24current_5;
	// GoogleUniversalAnalytics GoogleUniversalAnalytics/<netActivity>c__Iterator33::<>f__this
	GoogleUniversalAnalytics_t2200773556 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CprevReachabilityU3E__0_0() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U3CprevReachabilityU3E__0_0)); }
	inline int32_t get_U3CprevReachabilityU3E__0_0() const { return ___U3CprevReachabilityU3E__0_0; }
	inline int32_t* get_address_of_U3CprevReachabilityU3E__0_0() { return &___U3CprevReachabilityU3E__0_0; }
	inline void set_U3CprevReachabilityU3E__0_0(int32_t value)
	{
		___U3CprevReachabilityU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnetworkReachabilityU3E__1_1() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U3CnetworkReachabilityU3E__1_1)); }
	inline int32_t get_U3CnetworkReachabilityU3E__1_1() const { return ___U3CnetworkReachabilityU3E__1_1; }
	inline int32_t* get_address_of_U3CnetworkReachabilityU3E__1_1() { return &___U3CnetworkReachabilityU3E__1_1; }
	inline void set_U3CnetworkReachabilityU3E__1_1(int32_t value)
	{
		___U3CnetworkReachabilityU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_2() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U3CwwwU3E__2_2)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__2_2() const { return ___U3CwwwU3E__2_2; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__2_2() { return &___U3CwwwU3E__2_2; }
	inline void set_U3CwwwU3E__2_2(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CresultU3E__3_3() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U3CresultU3E__3_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CresultU3E__3_3() const { return ___U3CresultU3E__3_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CresultU3E__3_3() { return &___U3CresultU3E__3_3; }
	inline void set_U3CresultU3E__3_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CresultU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresultU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CnetActivityU3Ec__Iterator33_t1845284094, ___U3CU3Ef__this_6)); }
	inline GoogleUniversalAnalytics_t2200773556 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline GoogleUniversalAnalytics_t2200773556 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(GoogleUniversalAnalytics_t2200773556 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
