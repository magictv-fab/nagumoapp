﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HandAnim
struct HandAnim_t65440576;

#include "codegen/il2cpp-codegen.h"

// System.Void HandAnim::.ctor()
extern "C"  void HandAnim__ctor_m4196588379 (HandAnim_t65440576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandAnim::Start()
extern "C"  void HandAnim_Start_m3143726171 (HandAnim_t65440576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandAnim::Update()
extern "C"  void HandAnim_Update_m2972082962 (HandAnim_t65440576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
