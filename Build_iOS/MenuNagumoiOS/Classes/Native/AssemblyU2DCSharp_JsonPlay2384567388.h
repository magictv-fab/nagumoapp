﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonPlay
struct  JsonPlay_t2384567388  : public Il2CppObject
{
public:
	// System.Int32 JsonPlay::pts
	int32_t ___pts_0;
	// System.Int32 JsonPlay::jogadas
	int32_t ___jogadas_1;
	// System.String JsonPlay::cupom
	String_t* ___cupom_2;
	// System.Int32 JsonPlay::valor
	int32_t ___valor_3;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(JsonPlay_t2384567388, ___pts_0)); }
	inline int32_t get_pts_0() const { return ___pts_0; }
	inline int32_t* get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(int32_t value)
	{
		___pts_0 = value;
	}

	inline static int32_t get_offset_of_jogadas_1() { return static_cast<int32_t>(offsetof(JsonPlay_t2384567388, ___jogadas_1)); }
	inline int32_t get_jogadas_1() const { return ___jogadas_1; }
	inline int32_t* get_address_of_jogadas_1() { return &___jogadas_1; }
	inline void set_jogadas_1(int32_t value)
	{
		___jogadas_1 = value;
	}

	inline static int32_t get_offset_of_cupom_2() { return static_cast<int32_t>(offsetof(JsonPlay_t2384567388, ___cupom_2)); }
	inline String_t* get_cupom_2() const { return ___cupom_2; }
	inline String_t** get_address_of_cupom_2() { return &___cupom_2; }
	inline void set_cupom_2(String_t* value)
	{
		___cupom_2 = value;
		Il2CppCodeGenWriteBarrier(&___cupom_2, value);
	}

	inline static int32_t get_offset_of_valor_3() { return static_cast<int32_t>(offsetof(JsonPlay_t2384567388, ___valor_3)); }
	inline int32_t get_valor_3() const { return ___valor_3; }
	inline int32_t* get_address_of_valor_3() { return &___valor_3; }
	inline void set_valor_3(int32_t value)
	{
		___valor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
