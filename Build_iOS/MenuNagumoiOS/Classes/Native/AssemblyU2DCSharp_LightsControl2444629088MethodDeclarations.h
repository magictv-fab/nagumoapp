﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LightsControl
struct LightsControl_t2444629088;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void LightsControl::.ctor()
extern "C"  void LightsControl__ctor_m975517963 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::Awake()
extern "C"  void LightsControl_Awake_m1213123182 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::Start()
extern "C"  void LightsControl_Start_m4217623051 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::Turn()
extern "C"  void LightsControl_Turn_m3214146582 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::BlinkAll(System.Int32)
extern "C"  void LightsControl_BlinkAll_m4221792911 (LightsControl_t2444629088 * __this, int32_t ___repeat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LightsControl::ITurn()
extern "C"  Il2CppObject * LightsControl_ITurn_m4182852983 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::Stop()
extern "C"  void LightsControl_Stop_m3184506459 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::LoadLights(System.Single)
extern "C"  void LightsControl_LoadLights_m484716655 (LightsControl_t2444629088 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LightsControl::IBlinkAll(System.Int32)
extern "C"  Il2CppObject * LightsControl_IBlinkAll_m470538736 (LightsControl_t2444629088 * __this, int32_t ___repeat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::Reset()
extern "C"  void LightsControl_Reset_m2916918200 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl::Update()
extern "C"  void LightsControl_Update_m1903147874 (LightsControl_t2444629088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
