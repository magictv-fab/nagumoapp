﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.OnInAppCommonEventHandler
struct OnInAppCommonEventHandler_t1653412278;
// System.Object
struct Il2CppObject;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.in_apps.OnInAppCommonEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInAppCommonEventHandler__ctor_m2690733198 (OnInAppCommonEventHandler_t1653412278 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.OnInAppCommonEventHandler::Invoke(MagicTV.vo.BundleVO)
extern "C"  void OnInAppCommonEventHandler_Invoke_m1889725913 (OnInAppCommonEventHandler_t1653412278 * __this, BundleVO_t1984518073 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.in_apps.OnInAppCommonEventHandler::BeginInvoke(MagicTV.vo.BundleVO,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInAppCommonEventHandler_BeginInvoke_m677843912 (OnInAppCommonEventHandler_t1653412278 * __this, BundleVO_t1984518073 * ___vo0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.OnInAppCommonEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnInAppCommonEventHandler_EndInvoke_m1388395934 (OnInAppCommonEventHandler_t1653412278 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
