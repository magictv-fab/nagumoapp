﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// OfertaCRMData[]
struct OfertaCRMDataU5BU5D_t2228159150;
// OfertaCRMData
struct OfertaCRMData_t637956887;
// System.Object
struct Il2CppObject;
// OfertasManagerCRM_PraVoce
struct OfertasManagerCRM_PraVoce_t4201212046;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD
struct  U3CILoadOfertasU3Ec__IteratorD_t382481475  : public Il2CppObject
{
public:
	// System.String OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<acao>__0
	String_t* ___U3CacaoU3E__0_0;
	// System.String OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<json>__1
	String_t* ___U3CjsonU3E__1_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<headers>__2
	Dictionary_2_t827649927 * ___U3CheadersU3E__2_2;
	// System.Byte[] OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<pData>__3
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__3_3;
	// UnityEngine.WWW OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_4;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<$s_9>__5
	Enumerator_t767573031  ___U3CU24s_9U3E__5_5;
	// UnityEngine.GameObject OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<obj>__6
	GameObject_t3674682005 * ___U3CobjU3E__6_6;
	// System.String OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<message>__7
	String_t* ___U3CmessageU3E__7_7;
	// System.Int32 OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<count>__8
	int32_t ___U3CcountU3E__8_8;
	// OfertaCRMData[] OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<$s_10>__9
	OfertaCRMDataU5BU5D_t2228159150* ___U3CU24s_10U3E__9_9;
	// System.Int32 OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<$s_11>__10
	int32_t ___U3CU24s_11U3E__10_10;
	// OfertaCRMData OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<ofertaCRMDataDePor>__11
	OfertaCRMData_t637956887 * ___U3CofertaCRMDataDePorU3E__11_11;
	// System.Int32 OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::$PC
	int32_t ___U24PC_12;
	// System.Object OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::$current
	Il2CppObject * ___U24current_13;
	// OfertasManagerCRM_PraVoce OfertasManagerCRM_PraVoce/<ILoadOfertas>c__IteratorD::<>f__this
	OfertasManagerCRM_PraVoce_t4201212046 * ___U3CU3Ef__this_14;

public:
	inline static int32_t get_offset_of_U3CacaoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CacaoU3E__0_0)); }
	inline String_t* get_U3CacaoU3E__0_0() const { return ___U3CacaoU3E__0_0; }
	inline String_t** get_address_of_U3CacaoU3E__0_0() { return &___U3CacaoU3E__0_0; }
	inline void set_U3CacaoU3E__0_0(String_t* value)
	{
		___U3CacaoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CacaoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CjsonU3E__1_1)); }
	inline String_t* get_U3CjsonU3E__1_1() const { return ___U3CjsonU3E__1_1; }
	inline String_t** get_address_of_U3CjsonU3E__1_1() { return &___U3CjsonU3E__1_1; }
	inline void set_U3CjsonU3E__1_1(String_t* value)
	{
		___U3CjsonU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CheadersU3E__2_2)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__2_2() const { return ___U3CheadersU3E__2_2; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__2_2() { return &___U3CheadersU3E__2_2; }
	inline void set_U3CheadersU3E__2_2(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CpDataU3E__3_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__3_3() const { return ___U3CpDataU3E__3_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__3_3() { return &___U3CpDataU3E__3_3; }
	inline void set_U3CpDataU3E__3_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CwwwU3E__4_4)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_4() const { return ___U3CwwwU3E__4_4; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_4() { return &___U3CwwwU3E__4_4; }
	inline void set_U3CwwwU3E__4_4(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_9U3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CU24s_9U3E__5_5)); }
	inline Enumerator_t767573031  get_U3CU24s_9U3E__5_5() const { return ___U3CU24s_9U3E__5_5; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_9U3E__5_5() { return &___U3CU24s_9U3E__5_5; }
	inline void set_U3CU24s_9U3E__5_5(Enumerator_t767573031  value)
	{
		___U3CU24s_9U3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__6_6() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CobjU3E__6_6)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__6_6() const { return ___U3CobjU3E__6_6; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__6_6() { return &___U3CobjU3E__6_6; }
	inline void set_U3CobjU3E__6_6(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__7_7() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CmessageU3E__7_7)); }
	inline String_t* get_U3CmessageU3E__7_7() const { return ___U3CmessageU3E__7_7; }
	inline String_t** get_address_of_U3CmessageU3E__7_7() { return &___U3CmessageU3E__7_7; }
	inline void set_U3CmessageU3E__7_7(String_t* value)
	{
		___U3CmessageU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CcountU3E__8_8() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CcountU3E__8_8)); }
	inline int32_t get_U3CcountU3E__8_8() const { return ___U3CcountU3E__8_8; }
	inline int32_t* get_address_of_U3CcountU3E__8_8() { return &___U3CcountU3E__8_8; }
	inline void set_U3CcountU3E__8_8(int32_t value)
	{
		___U3CcountU3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_10U3E__9_9() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CU24s_10U3E__9_9)); }
	inline OfertaCRMDataU5BU5D_t2228159150* get_U3CU24s_10U3E__9_9() const { return ___U3CU24s_10U3E__9_9; }
	inline OfertaCRMDataU5BU5D_t2228159150** get_address_of_U3CU24s_10U3E__9_9() { return &___U3CU24s_10U3E__9_9; }
	inline void set_U3CU24s_10U3E__9_9(OfertaCRMDataU5BU5D_t2228159150* value)
	{
		___U3CU24s_10U3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_10U3E__9_9, value);
	}

	inline static int32_t get_offset_of_U3CU24s_11U3E__10_10() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CU24s_11U3E__10_10)); }
	inline int32_t get_U3CU24s_11U3E__10_10() const { return ___U3CU24s_11U3E__10_10; }
	inline int32_t* get_address_of_U3CU24s_11U3E__10_10() { return &___U3CU24s_11U3E__10_10; }
	inline void set_U3CU24s_11U3E__10_10(int32_t value)
	{
		___U3CU24s_11U3E__10_10 = value;
	}

	inline static int32_t get_offset_of_U3CofertaCRMDataDePorU3E__11_11() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CofertaCRMDataDePorU3E__11_11)); }
	inline OfertaCRMData_t637956887 * get_U3CofertaCRMDataDePorU3E__11_11() const { return ___U3CofertaCRMDataDePorU3E__11_11; }
	inline OfertaCRMData_t637956887 ** get_address_of_U3CofertaCRMDataDePorU3E__11_11() { return &___U3CofertaCRMDataDePorU3E__11_11; }
	inline void set_U3CofertaCRMDataDePorU3E__11_11(OfertaCRMData_t637956887 * value)
	{
		___U3CofertaCRMDataDePorU3E__11_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CofertaCRMDataDePorU3E__11_11, value);
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_14() { return static_cast<int32_t>(offsetof(U3CILoadOfertasU3Ec__IteratorD_t382481475, ___U3CU3Ef__this_14)); }
	inline OfertasManagerCRM_PraVoce_t4201212046 * get_U3CU3Ef__this_14() const { return ___U3CU3Ef__this_14; }
	inline OfertasManagerCRM_PraVoce_t4201212046 ** get_address_of_U3CU3Ef__this_14() { return &___U3CU3Ef__this_14; }
	inline void set_U3CU3Ef__this_14(OfertasManagerCRM_PraVoce_t4201212046 * value)
	{
		___U3CU3Ef__this_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
