﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler
struct OnBooleanEventHandler_t2032344769;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnBooleanEventHandler__ctor_m203764248 (OnBooleanEventHandler_t2032344769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler::Invoke(System.Boolean)
extern "C"  void OnBooleanEventHandler_Invoke_m4057158249 (OnBooleanEventHandler_t2032344769 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnBooleanEventHandler_BeginInvoke_m3642178062 (OnBooleanEventHandler_t2032344769 * __this, bool ___success0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnBooleanEventHandler_EndInvoke_m3763695656 (OnBooleanEventHandler_t2032344769 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
