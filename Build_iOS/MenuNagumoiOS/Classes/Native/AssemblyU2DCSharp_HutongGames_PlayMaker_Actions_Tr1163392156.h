﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Translate
struct  Translate_t1163392156  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Translate::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Translate::vector
	FsmVector3_t533912882 * ___vector_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::x
	FsmFloat_t2134102846 * ___x_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::y
	FsmFloat_t2134102846 * ___y_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::z
	FsmFloat_t2134102846 * ___z_13;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.Translate::space
	int32_t ___space_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::perSecond
	bool ___perSecond_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::everyFrame
	bool ___everyFrame_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::lateUpdate
	bool ___lateUpdate_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::fixedUpdate
	bool ___fixedUpdate_18;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_vector_10() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___vector_10)); }
	inline FsmVector3_t533912882 * get_vector_10() const { return ___vector_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector_10() { return &___vector_10; }
	inline void set_vector_10(FsmVector3_t533912882 * value)
	{
		___vector_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector_10, value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___x_11)); }
	inline FsmFloat_t2134102846 * get_x_11() const { return ___x_11; }
	inline FsmFloat_t2134102846 ** get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(FsmFloat_t2134102846 * value)
	{
		___x_11 = value;
		Il2CppCodeGenWriteBarrier(&___x_11, value);
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___y_12)); }
	inline FsmFloat_t2134102846 * get_y_12() const { return ___y_12; }
	inline FsmFloat_t2134102846 ** get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(FsmFloat_t2134102846 * value)
	{
		___y_12 = value;
		Il2CppCodeGenWriteBarrier(&___y_12, value);
	}

	inline static int32_t get_offset_of_z_13() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___z_13)); }
	inline FsmFloat_t2134102846 * get_z_13() const { return ___z_13; }
	inline FsmFloat_t2134102846 ** get_address_of_z_13() { return &___z_13; }
	inline void set_z_13(FsmFloat_t2134102846 * value)
	{
		___z_13 = value;
		Il2CppCodeGenWriteBarrier(&___z_13, value);
	}

	inline static int32_t get_offset_of_space_14() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___space_14)); }
	inline int32_t get_space_14() const { return ___space_14; }
	inline int32_t* get_address_of_space_14() { return &___space_14; }
	inline void set_space_14(int32_t value)
	{
		___space_14 = value;
	}

	inline static int32_t get_offset_of_perSecond_15() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___perSecond_15)); }
	inline bool get_perSecond_15() const { return ___perSecond_15; }
	inline bool* get_address_of_perSecond_15() { return &___perSecond_15; }
	inline void set_perSecond_15(bool value)
	{
		___perSecond_15 = value;
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_17() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___lateUpdate_17)); }
	inline bool get_lateUpdate_17() const { return ___lateUpdate_17; }
	inline bool* get_address_of_lateUpdate_17() { return &___lateUpdate_17; }
	inline void set_lateUpdate_17(bool value)
	{
		___lateUpdate_17 = value;
	}

	inline static int32_t get_offset_of_fixedUpdate_18() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___fixedUpdate_18)); }
	inline bool get_fixedUpdate_18() const { return ___fixedUpdate_18; }
	inline bool* get_address_of_fixedUpdate_18() { return &___fixedUpdate_18; }
	inline void set_fixedUpdate_18(bool value)
	{
		___fixedUpdate_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
