﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.PDF417
struct PDF417_t3545372256;
// ZXing.PDF417.Internal.BarcodeMatrix
struct BarcodeMatrix_t425107471;
// ZXing.PDF417.Internal.BarcodeRow
struct BarcodeRow_t3616570866;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_BarcodeRow3616570866.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_PDF417_Internal_BarcodeMatrix425107471.h"
#include "QRCode_ZXing_PDF417_Internal_Compaction2012764861.h"

// System.Void ZXing.PDF417.Internal.PDF417::.ctor()
extern "C"  void PDF417__ctor_m2093858238 (PDF417_t3545372256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::.ctor(System.Boolean)
extern "C"  void PDF417__ctor_m1119784629 (PDF417_t3545372256 * __this, bool ___compact0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.BarcodeMatrix ZXing.PDF417.Internal.PDF417::get_BarcodeMatrix()
extern "C"  BarcodeMatrix_t425107471 * PDF417_get_BarcodeMatrix_m1911285293 (PDF417_t3545372256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417::calculateNumberOfRows(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t PDF417_calculateNumberOfRows_m771190750 (Il2CppObject * __this /* static, unused */, int32_t ___m0, int32_t ___k1, int32_t ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.PDF417::getNumberOfPadCodewords(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t PDF417_getNumberOfPadCodewords_m4294831961 (Il2CppObject * __this /* static, unused */, int32_t ___m0, int32_t ___k1, int32_t ___c2, int32_t ___r3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::encodeChar(System.Int32,System.Int32,ZXing.PDF417.Internal.BarcodeRow)
extern "C"  void PDF417_encodeChar_m781493059 (Il2CppObject * __this /* static, unused */, int32_t ___pattern0, int32_t ___len1, BarcodeRow_t3616570866 * ___logic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::encodeLowLevel(System.String,System.Int32,System.Int32,System.Int32,ZXing.PDF417.Internal.BarcodeMatrix)
extern "C"  void PDF417_encodeLowLevel_m2558469313 (PDF417_t3545372256 * __this, String_t* ___fullCodewords0, int32_t ___c1, int32_t ___r2, int32_t ___errorCorrectionLevel3, BarcodeMatrix_t425107471 * ___logic4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::generateBarcodeLogic(System.String,System.Int32)
extern "C"  void PDF417_generateBarcodeLogic_m3225818510 (PDF417_t3545372256 * __this, String_t* ___msg0, int32_t ___errorCorrectionLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.Internal.PDF417::determineDimensions(System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t3230847821* PDF417_determineDimensions_m3033711382 (PDF417_t3545372256 * __this, int32_t ___sourceCodeWords0, int32_t ___errorCorrectionCodeWords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::setDimensions(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void PDF417_setDimensions_m705911067 (PDF417_t3545372256 * __this, int32_t ___maxCols0, int32_t ___minCols1, int32_t ___maxRows2, int32_t ___minRows3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::setCompaction(ZXing.PDF417.Internal.Compaction)
extern "C"  void PDF417_setCompaction_m2520424201 (PDF417_t3545372256 * __this, int32_t ___compaction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::setCompact(System.Boolean)
extern "C"  void PDF417_setCompact_m246731070 (PDF417_t3545372256 * __this, bool ___compact0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::setEncoding(System.String)
extern "C"  void PDF417_setEncoding_m15760721 (PDF417_t3545372256 * __this, String_t* ___encodingname0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::setDisableEci(System.Boolean)
extern "C"  void PDF417_setDisableEci_m1664216056 (PDF417_t3545372256 * __this, bool ___disabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417::.cctor()
extern "C"  void PDF417__cctor_m2999727 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
