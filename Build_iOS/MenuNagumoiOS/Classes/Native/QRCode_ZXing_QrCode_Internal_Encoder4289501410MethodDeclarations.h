﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;
// ZXing.QrCode.Internal.QRCode
struct QRCode_t3167984362;
// System.String
struct String_t;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// ZXing.QrCode.Internal.Mode
struct Mode_t2660577215;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.Common.CharacterSetECI
struct CharacterSetECI_t2542265168;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_ByteMatrix2072255685.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"
#include "QRCode_ZXing_QrCode_Internal_Mode2660577215.h"
#include "QRCode_ZXing_Common_CharacterSetECI2542265168.h"

// System.Int32 ZXing.QrCode.Internal.Encoder::calculateMaskPenalty(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  int32_t Encoder_calculateMaskPenalty_m3535629567 (Il2CppObject * __this /* static, unused */, ByteMatrix_t2072255685 * ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.QRCode ZXing.QrCode.Internal.Encoder::encode(System.String,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  QRCode_t3167984362 * Encoder_encode_m3702168910 (Il2CppObject * __this /* static, unused */, String_t* ___content0, ErrorCorrectionLevel_t1225927610 * ___ecLevel1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Encoder::getAlphanumericCode(System.Int32)
extern "C"  int32_t Encoder_getAlphanumericCode_m1169742073 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Encoder::chooseMode(System.String,System.String)
extern "C"  Mode_t2660577215 * Encoder_chooseMode_m1881447561 (Il2CppObject * __this /* static, unused */, String_t* ___content0, String_t* ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.Encoder::isOnlyDoubleByteKanji(System.String)
extern "C"  bool Encoder_isOnlyDoubleByteKanji_m1426108702 (Il2CppObject * __this /* static, unused */, String_t* ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Encoder::chooseMaskPattern(ZXing.Common.BitArray,ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern "C"  int32_t Encoder_chooseMaskPattern_m1988989232 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___bits0, ErrorCorrectionLevel_t1225927610 * ___ecLevel1, Version_t1953509534 * ___version2, ByteMatrix_t2072255685 * ___matrix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Encoder::chooseVersion(System.Int32,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern "C"  Version_t1953509534 * Encoder_chooseVersion_m2593566173 (Il2CppObject * __this /* static, unused */, int32_t ___numInputBits0, ErrorCorrectionLevel_t1225927610 * ___ecLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::terminateBits(System.Int32,ZXing.Common.BitArray)
extern "C"  void Encoder_terminateBits_m794673127 (Il2CppObject * __this /* static, unused */, int32_t ___numDataBytes0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::getNumDataBytesAndNumECBytesForBlockID(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern "C"  void Encoder_getNumDataBytesAndNumECBytesForBlockID_m631044928 (Il2CppObject * __this /* static, unused */, int32_t ___numTotalBytes0, int32_t ___numDataBytes1, int32_t ___numRSBlocks2, int32_t ___blockID3, Int32U5BU5D_t3230847821* ___numDataBytesInBlock4, Int32U5BU5D_t3230847821* ___numECBytesInBlock5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.QrCode.Internal.Encoder::interleaveWithECBytes(ZXing.Common.BitArray,System.Int32,System.Int32,System.Int32)
extern "C"  BitArray_t4163851164 * Encoder_interleaveWithECBytes_m1271479340 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___bits0, int32_t ___numTotalBytes1, int32_t ___numDataBytes2, int32_t ___numRSBlocks3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.QrCode.Internal.Encoder::generateECBytes(System.Byte[],System.Int32)
extern "C"  ByteU5BU5D_t4260760469* Encoder_generateECBytes_m3771715800 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___dataBytes0, int32_t ___numEcBytesInBlock1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendModeInfo(ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray)
extern "C"  void Encoder_appendModeInfo_m3604201838 (Il2CppObject * __this /* static, unused */, Mode_t2660577215 * ___mode0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendLengthInfo(System.Int32,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray)
extern "C"  void Encoder_appendLengthInfo_m4136286471 (Il2CppObject * __this /* static, unused */, int32_t ___numLetters0, Version_t1953509534 * ___version1, Mode_t2660577215 * ___mode2, BitArray_t4163851164 * ___bits3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendBytes(System.String,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray,System.String)
extern "C"  void Encoder_appendBytes_m3610295330 (Il2CppObject * __this /* static, unused */, String_t* ___content0, Mode_t2660577215 * ___mode1, BitArray_t4163851164 * ___bits2, String_t* ___encoding3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendNumericBytes(System.String,ZXing.Common.BitArray)
extern "C"  void Encoder_appendNumericBytes_m462125591 (Il2CppObject * __this /* static, unused */, String_t* ___content0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendAlphanumericBytes(System.String,ZXing.Common.BitArray)
extern "C"  void Encoder_appendAlphanumericBytes_m4088198719 (Il2CppObject * __this /* static, unused */, String_t* ___content0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::append8BitBytes(System.String,ZXing.Common.BitArray,System.String)
extern "C"  void Encoder_append8BitBytes_m404963041 (Il2CppObject * __this /* static, unused */, String_t* ___content0, BitArray_t4163851164 * ___bits1, String_t* ___encoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendKanjiBytes(System.String,ZXing.Common.BitArray)
extern "C"  void Encoder_appendKanjiBytes_m3631984353 (Il2CppObject * __this /* static, unused */, String_t* ___content0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::appendECI(ZXing.Common.CharacterSetECI,ZXing.Common.BitArray)
extern "C"  void Encoder_appendECI_m957218983 (Il2CppObject * __this /* static, unused */, CharacterSetECI_t2542265168 * ___eci0, BitArray_t4163851164 * ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Encoder::.cctor()
extern "C"  void Encoder__cctor_m4179321859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
