﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRectFields
struct  SetRectFields_t2990573965  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetRectFields::rectVariable
	FsmRect_t1076426478 * ___rectVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::x
	FsmFloat_t2134102846 * ___x_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::y
	FsmFloat_t2134102846 * ___y_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::width
	FsmFloat_t2134102846 * ___width_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::height
	FsmFloat_t2134102846 * ___height_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRectFields::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_rectVariable_9() { return static_cast<int32_t>(offsetof(SetRectFields_t2990573965, ___rectVariable_9)); }
	inline FsmRect_t1076426478 * get_rectVariable_9() const { return ___rectVariable_9; }
	inline FsmRect_t1076426478 ** get_address_of_rectVariable_9() { return &___rectVariable_9; }
	inline void set_rectVariable_9(FsmRect_t1076426478 * value)
	{
		___rectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariable_9, value);
	}

	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SetRectFields_t2990573965, ___x_10)); }
	inline FsmFloat_t2134102846 * get_x_10() const { return ___x_10; }
	inline FsmFloat_t2134102846 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(FsmFloat_t2134102846 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier(&___x_10, value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SetRectFields_t2990573965, ___y_11)); }
	inline FsmFloat_t2134102846 * get_y_11() const { return ___y_11; }
	inline FsmFloat_t2134102846 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(FsmFloat_t2134102846 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier(&___y_11, value);
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(SetRectFields_t2990573965, ___width_12)); }
	inline FsmFloat_t2134102846 * get_width_12() const { return ___width_12; }
	inline FsmFloat_t2134102846 ** get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(FsmFloat_t2134102846 * value)
	{
		___width_12 = value;
		Il2CppCodeGenWriteBarrier(&___width_12, value);
	}

	inline static int32_t get_offset_of_height_13() { return static_cast<int32_t>(offsetof(SetRectFields_t2990573965, ___height_13)); }
	inline FsmFloat_t2134102846 * get_height_13() const { return ___height_13; }
	inline FsmFloat_t2134102846 ** get_address_of_height_13() { return &___height_13; }
	inline void set_height_13(FsmFloat_t2134102846 * value)
	{
		___height_13 = value;
		Il2CppCodeGenWriteBarrier(&___height_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetRectFields_t2990573965, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
