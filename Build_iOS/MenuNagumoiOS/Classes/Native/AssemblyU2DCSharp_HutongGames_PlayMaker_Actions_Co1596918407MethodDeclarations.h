﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerSimpleMove
struct ControllerSimpleMove_t1596918407;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerSimpleMove::.ctor()
extern "C"  void ControllerSimpleMove__ctor_m319812543 (ControllerSimpleMove_t1596918407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSimpleMove::Reset()
extern "C"  void ControllerSimpleMove_Reset_m2261212780 (ControllerSimpleMove_t1596918407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSimpleMove::OnUpdate()
extern "C"  void ControllerSimpleMove_OnUpdate_m3049988397 (ControllerSimpleMove_t1596918407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
