﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloideManager/<ILoadPublish>c__Iterator80
struct U3CILoadPublishU3Ec__Iterator80_t3858799123;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TabloideManager/<ILoadPublish>c__Iterator80::.ctor()
extern "C"  void U3CILoadPublishU3Ec__Iterator80__ctor_m2011555512 (U3CILoadPublishU3Ec__Iterator80_t3858799123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TabloideManager/<ILoadPublish>c__Iterator80::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishU3Ec__Iterator80_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1891370778 (U3CILoadPublishU3Ec__Iterator80_t3858799123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TabloideManager/<ILoadPublish>c__Iterator80::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishU3Ec__Iterator80_System_Collections_IEnumerator_get_Current_m3238231726 (U3CILoadPublishU3Ec__Iterator80_t3858799123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TabloideManager/<ILoadPublish>c__Iterator80::MoveNext()
extern "C"  bool U3CILoadPublishU3Ec__Iterator80_MoveNext_m1949911612 (U3CILoadPublishU3Ec__Iterator80_t3858799123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager/<ILoadPublish>c__Iterator80::Dispose()
extern "C"  void U3CILoadPublishU3Ec__Iterator80_Dispose_m267846901 (U3CILoadPublishU3Ec__Iterator80_t3858799123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager/<ILoadPublish>c__Iterator80::Reset()
extern "C"  void U3CILoadPublishU3Ec__Iterator80_Reset_m3952955749 (U3CILoadPublishU3Ec__Iterator80_t3858799123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
