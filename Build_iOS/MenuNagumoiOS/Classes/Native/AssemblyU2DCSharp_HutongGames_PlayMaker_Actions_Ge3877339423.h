﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1115130358.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTimeInfo
struct  GetTimeInfo_t3877339423  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.Actions.GetTimeInfo/TimeInfo HutongGames.PlayMaker.Actions.GetTimeInfo::getInfo
	int32_t ___getInfo_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTimeInfo::storeValue
	FsmFloat_t2134102846 * ___storeValue_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTimeInfo::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_getInfo_9() { return static_cast<int32_t>(offsetof(GetTimeInfo_t3877339423, ___getInfo_9)); }
	inline int32_t get_getInfo_9() const { return ___getInfo_9; }
	inline int32_t* get_address_of_getInfo_9() { return &___getInfo_9; }
	inline void set_getInfo_9(int32_t value)
	{
		___getInfo_9 = value;
	}

	inline static int32_t get_offset_of_storeValue_10() { return static_cast<int32_t>(offsetof(GetTimeInfo_t3877339423, ___storeValue_10)); }
	inline FsmFloat_t2134102846 * get_storeValue_10() const { return ___storeValue_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeValue_10() { return &___storeValue_10; }
	inline void set_storeValue_10(FsmFloat_t2134102846 * value)
	{
		___storeValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeValue_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetTimeInfo_t3877339423, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
