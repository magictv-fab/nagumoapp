﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t725705546;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t1072092957;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern "C"  bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m1193912938 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern "C"  void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m3331084097 (WebCamAbstractBehaviour_t725705546 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern "C"  String_t* WebCamAbstractBehaviour_get_DeviceName_m1289048688 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern "C"  void WebCamAbstractBehaviour_set_DeviceName_m1909398203 (WebCamAbstractBehaviour_t725705546 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern "C"  bool WebCamAbstractBehaviour_get_FlipHorizontally_m149488964 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern "C"  void WebCamAbstractBehaviour_set_FlipHorizontally_m3210243803 (WebCamAbstractBehaviour_t725705546 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern "C"  bool WebCamAbstractBehaviour_get_TurnOffWebCam_m3163367433 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern "C"  void WebCamAbstractBehaviour_set_TurnOffWebCam_m2700489376 (WebCamAbstractBehaviour_t725705546 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern "C"  bool WebCamAbstractBehaviour_get_IsPlaying_m2739671360 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
extern "C"  bool WebCamAbstractBehaviour_IsWebCamUsed_m12312063 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern "C"  WebCamImpl_t1072092957 * WebCamAbstractBehaviour_get_ImplementationClass_m1289375332 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern "C"  void WebCamAbstractBehaviour_InitCamera_m1378657342 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
extern "C"  bool WebCamAbstractBehaviour_CheckNativePluginSupport_m2130301530 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern "C"  void WebCamAbstractBehaviour_OnLevelWasLoaded_m2545565138 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern "C"  void WebCamAbstractBehaviour_OnDestroy_m3179073076 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern "C"  void WebCamAbstractBehaviour_Update_m3729841074 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
extern "C"  int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m3140624067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern "C"  void WebCamAbstractBehaviour__ctor_m2004274875 (WebCamAbstractBehaviour_t725705546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
