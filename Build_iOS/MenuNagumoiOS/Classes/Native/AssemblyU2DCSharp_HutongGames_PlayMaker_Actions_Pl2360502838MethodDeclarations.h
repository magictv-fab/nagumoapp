﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsGetInt
struct PlayerPrefsGetInt_t2360502838;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::.ctor()
extern "C"  void PlayerPrefsGetInt__ctor_m2026786752 (PlayerPrefsGetInt_t2360502838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::Reset()
extern "C"  void PlayerPrefsGetInt_Reset_m3968186989 (PlayerPrefsGetInt_t2360502838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::OnEnter()
extern "C"  void PlayerPrefsGetInt_OnEnter_m1513612183 (PlayerPrefsGetInt_t2360502838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
