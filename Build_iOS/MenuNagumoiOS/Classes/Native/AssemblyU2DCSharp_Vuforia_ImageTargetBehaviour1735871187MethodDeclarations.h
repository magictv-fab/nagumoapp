﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t1735871187;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"

// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C"  void ImageTargetBehaviour__ctor_m3469834622 (ImageTargetBehaviour_t1735871187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBehaviour::Start()
extern "C"  void ImageTargetBehaviour_Start_m2416972414 (ImageTargetBehaviour_t1735871187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBehaviour::tryToRegisterEvents()
extern "C"  void ImageTargetBehaviour_tryToRegisterEvents_m2234450894 (ImageTargetBehaviour_t1735871187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBehaviour::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void ImageTargetBehaviour_OnTrackableStateChanged_m1078389983 (ImageTargetBehaviour_t1735871187 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBehaviour::Update()
extern "C"  void ImageTargetBehaviour_Update_m1917552975 (ImageTargetBehaviour_t1735871187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
