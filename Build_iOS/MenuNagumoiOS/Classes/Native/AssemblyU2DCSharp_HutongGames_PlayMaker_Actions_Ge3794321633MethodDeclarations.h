﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVelocity
struct GetVelocity_t3794321633;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVelocity::.ctor()
extern "C"  void GetVelocity__ctor_m2606929781 (GetVelocity_t3794321633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::Reset()
extern "C"  void GetVelocity_Reset_m253362722 (GetVelocity_t3794321633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::OnEnter()
extern "C"  void GetVelocity_OnEnter_m685314572 (GetVelocity_t3794321633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::OnUpdate()
extern "C"  void GetVelocity_OnUpdate_m3198441911 (GetVelocity_t3794321633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::DoGetVelocity()
extern "C"  void GetVelocity_DoGetVelocity_m2419104091 (GetVelocity_t3794321633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
