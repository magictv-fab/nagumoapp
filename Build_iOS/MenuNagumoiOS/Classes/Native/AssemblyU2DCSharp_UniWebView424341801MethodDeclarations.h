﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView
struct UniWebView_t424341801;
// UniWebView/PageStartedDelegate
struct PageStartedDelegate_t2373082257;
// UniWebView/PageFinishedDelegate
struct PageFinishedDelegate_t1325724172;
// UniWebView/PageErrorReceivedDelegate
struct PageErrorReceivedDelegate_t4149620697;
// UniWebView/MessageReceivedDelegate
struct MessageReceivedDelegate_t2778163623;
// UniWebView/ShouldCloseDelegate
struct ShouldCloseDelegate_t2209923748;
// UniWebView/KeyCodeReceivedDelegate
struct KeyCodeReceivedDelegate_t3758979212;
// UniWebView/OreintationChangedDelegate
struct OreintationChangedDelegate_t3987443383;
// UniWebView/OnWebContentProcessTerminatedDelegate
struct OnWebContentProcessTerminatedDelegate_t1791934381;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;
// System.Action`1<UniWebViewNativeResultPayload>
struct Action_1_t1392508089;
// System.Action`1<System.String>
struct Action_1_t403047693;
// UniWebViewNativeResultPayload
struct UniWebViewNativeResultPayload_t996691953;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UniWebView_PageStartedDelegate2373082257.h"
#include "AssemblyU2DCSharp_UniWebView_PageFinishedDelegate1325724172.h"
#include "AssemblyU2DCSharp_UniWebView_PageErrorReceivedDele4149620697.h"
#include "AssemblyU2DCSharp_UniWebView_MessageReceivedDelega2778163623.h"
#include "AssemblyU2DCSharp_UniWebView_ShouldCloseDelegate2209923748.h"
#include "AssemblyU2DCSharp_UniWebView_KeyCodeReceivedDelega3758979212.h"
#include "AssemblyU2DCSharp_UniWebView_OreintationChangedDel3987443383.h"
#include "AssemblyU2DCSharp_UniWebView_OnWebContentProcessTe1791934381.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UniWebViewTransitionEdge1338482235.h"
#include "System_Core_System_Action3771233898.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UniWebViewNativeResultPayload996691953.h"

// System.Void UniWebView::.ctor()
extern "C"  void UniWebView__ctor_m2567103890 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnPageStarted(UniWebView/PageStartedDelegate)
extern "C"  void UniWebView_add_OnPageStarted_m1260434420 (UniWebView_t424341801 * __this, PageStartedDelegate_t2373082257 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnPageStarted(UniWebView/PageStartedDelegate)
extern "C"  void UniWebView_remove_OnPageStarted_m2579028505 (UniWebView_t424341801 * __this, PageStartedDelegate_t2373082257 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnPageFinished(UniWebView/PageFinishedDelegate)
extern "C"  void UniWebView_add_OnPageFinished_m3687423926 (UniWebView_t424341801 * __this, PageFinishedDelegate_t1325724172 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnPageFinished(UniWebView/PageFinishedDelegate)
extern "C"  void UniWebView_remove_OnPageFinished_m3840987291 (UniWebView_t424341801 * __this, PageFinishedDelegate_t1325724172 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnPageErrorReceived(UniWebView/PageErrorReceivedDelegate)
extern "C"  void UniWebView_add_OnPageErrorReceived_m1115543540 (UniWebView_t424341801 * __this, PageErrorReceivedDelegate_t4149620697 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnPageErrorReceived(UniWebView/PageErrorReceivedDelegate)
extern "C"  void UniWebView_remove_OnPageErrorReceived_m1492971161 (UniWebView_t424341801 * __this, PageErrorReceivedDelegate_t4149620697 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnMessageReceived(UniWebView/MessageReceivedDelegate)
extern "C"  void UniWebView_add_OnMessageReceived_m2927361972 (UniWebView_t424341801 * __this, MessageReceivedDelegate_t2778163623 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnMessageReceived(UniWebView/MessageReceivedDelegate)
extern "C"  void UniWebView_remove_OnMessageReceived_m2071081177 (UniWebView_t424341801 * __this, MessageReceivedDelegate_t2778163623 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnShouldClose(UniWebView/ShouldCloseDelegate)
extern "C"  void UniWebView_add_OnShouldClose_m808529236 (UniWebView_t424341801 * __this, ShouldCloseDelegate_t2209923748 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnShouldClose(UniWebView/ShouldCloseDelegate)
extern "C"  void UniWebView_remove_OnShouldClose_m2127123321 (UniWebView_t424341801 * __this, ShouldCloseDelegate_t2209923748 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnKeyCodeReceived(UniWebView/KeyCodeReceivedDelegate)
extern "C"  void UniWebView_add_OnKeyCodeReceived_m2738111188 (UniWebView_t424341801 * __this, KeyCodeReceivedDelegate_t3758979212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnKeyCodeReceived(UniWebView/KeyCodeReceivedDelegate)
extern "C"  void UniWebView_remove_OnKeyCodeReceived_m1881830393 (UniWebView_t424341801 * __this, KeyCodeReceivedDelegate_t3758979212 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnOreintationChanged(UniWebView/OreintationChangedDelegate)
extern "C"  void UniWebView_add_OnOreintationChanged_m1174123936 (UniWebView_t424341801 * __this, OreintationChangedDelegate_t3987443383 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnOreintationChanged(UniWebView/OreintationChangedDelegate)
extern "C"  void UniWebView_remove_OnOreintationChanged_m3104814853 (UniWebView_t424341801 * __this, OreintationChangedDelegate_t3987443383 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::add_OnWebContentProcessTerminated(UniWebView/OnWebContentProcessTerminatedDelegate)
extern "C"  void UniWebView_add_OnWebContentProcessTerminated_m2371749779 (UniWebView_t424341801 * __this, OnWebContentProcessTerminatedDelegate_t1791934381 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::remove_OnWebContentProcessTerminated(UniWebView/OnWebContentProcessTerminatedDelegate)
extern "C"  void UniWebView_remove_OnWebContentProcessTerminated_m1069573240 (UniWebView_t424341801 * __this, OnWebContentProcessTerminatedDelegate_t1791934381 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UniWebView::get_Frame()
extern "C"  Rect_t4241904616  UniWebView_get_Frame_m1582702514 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::set_Frame(UnityEngine.Rect)
extern "C"  void UniWebView_set_Frame_m3151897669 (UniWebView_t424341801 * __this, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UniWebView::get_ReferenceRectTransform()
extern "C"  RectTransform_t972643934 * UniWebView_get_ReferenceRectTransform_m3428459258 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::set_ReferenceRectTransform(UnityEngine.RectTransform)
extern "C"  void UniWebView_set_ReferenceRectTransform_m4207018165 (UniWebView_t424341801 * __this, RectTransform_t972643934 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebView::get_Url()
extern "C"  String_t* UniWebView_get_Url_m1564890163 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::UpdateFrame()
extern "C"  void UniWebView_UpdateFrame_m752134292 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UniWebView::NextFrameRect()
extern "C"  Rect_t4241904616  UniWebView_NextFrameRect_m3797157548 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Awake()
extern "C"  void UniWebView_Awake_m2804709109 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Start()
extern "C"  void UniWebView_Start_m1514241682 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Update()
extern "C"  void UniWebView_Update_m3997671355 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::OnEnable()
extern "C"  void UniWebView_OnEnable_m2475892916 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::OnDisable()
extern "C"  void UniWebView_OnDisable_m4179173625 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Load(System.String,System.Boolean)
extern "C"  void UniWebView_Load_m2137607443 (UniWebView_t424341801 * __this, String_t* ___url0, bool ___skipEncoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::LoadHTMLString(System.String,System.String,System.Boolean)
extern "C"  void UniWebView_LoadHTMLString_m2711591859 (UniWebView_t424341801 * __this, String_t* ___htmlString0, String_t* ___baseUrl1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Reload()
extern "C"  void UniWebView_Reload_m2132997259 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Stop()
extern "C"  void UniWebView_Stop_m2681658612 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView::get_CanGoBack()
extern "C"  bool UniWebView_get_CanGoBack_m395271986 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView::get_CanGoForward()
extern "C"  bool UniWebView_get_CanGoForward_m3858150204 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::GoBack()
extern "C"  void UniWebView_GoBack_m3506734945 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::GoForward()
extern "C"  void UniWebView_GoForward_m3466979501 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetOpenLinksInExternalBrowser(System.Boolean)
extern "C"  void UniWebView_SetOpenLinksInExternalBrowser_m3828581458 (UniWebView_t424341801 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView::Show(System.Boolean,UniWebViewTransitionEdge,System.Single,System.Action)
extern "C"  bool UniWebView_Show_m1996812845 (UniWebView_t424341801 * __this, bool ___fade0, int32_t ___edge1, float ___duration2, Action_t3771233898 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView::Hide(System.Boolean,UniWebViewTransitionEdge,System.Single,System.Action)
extern "C"  bool UniWebView_Hide_m2837789448 (UniWebView_t424341801 * __this, bool ___fade0, int32_t ___edge1, float ___duration2, Action_t3771233898 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView::AnimateTo(UnityEngine.Rect,System.Single,System.Single,System.Action)
extern "C"  bool UniWebView_AnimateTo_m2992462846 (UniWebView_t424341801 * __this, Rect_t4241904616  ___frame0, float ___duration1, float ___delay2, Action_t3771233898 * ___completionHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::AddJavaScript(System.String,System.Action`1<UniWebViewNativeResultPayload>)
extern "C"  void UniWebView_AddJavaScript_m3175687071 (UniWebView_t424341801 * __this, String_t* ___jsString0, Action_1_t1392508089 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::EvaluateJavaScript(System.String,System.Action`1<UniWebViewNativeResultPayload>)
extern "C"  void UniWebView_EvaluateJavaScript_m1736304793 (UniWebView_t424341801 * __this, String_t* ___jsString0, Action_1_t1392508089 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::AddUrlScheme(System.String)
extern "C"  void UniWebView_AddUrlScheme_m1301538589 (UniWebView_t424341801 * __this, String_t* ___scheme0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::RemoveUrlScheme(System.String)
extern "C"  void UniWebView_RemoveUrlScheme_m558881602 (UniWebView_t424341801 * __this, String_t* ___scheme0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::AddSslExceptionDomain(System.String)
extern "C"  void UniWebView_AddSslExceptionDomain_m3675905674 (UniWebView_t424341801 * __this, String_t* ___domain0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::RemoveSslExceptionDomain(System.String)
extern "C"  void UniWebView_RemoveSslExceptionDomain_m1479394309 (UniWebView_t424341801 * __this, String_t* ___domain0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetHeaderField(System.String,System.String)
extern "C"  void UniWebView_SetHeaderField_m2008621953 (UniWebView_t424341801 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetUserAgent(System.String)
extern "C"  void UniWebView_SetUserAgent_m3024188696 (UniWebView_t424341801 * __this, String_t* ___agent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebView::GetUserAgent()
extern "C"  String_t* UniWebView_GetUserAgent_m3261140761 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetAllowAutoPlay(System.Boolean)
extern "C"  void UniWebView_SetAllowAutoPlay_m1322400915 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetAllowInlinePlay(System.Boolean)
extern "C"  void UniWebView_SetAllowInlinePlay_m3940921981 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetJavaScriptEnabled(System.Boolean)
extern "C"  void UniWebView_SetJavaScriptEnabled_m2785998363 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetAllowJavaScriptOpenWindow(System.Boolean)
extern "C"  void UniWebView_SetAllowJavaScriptOpenWindow_m2244175703 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::CleanCache()
extern "C"  void UniWebView_CleanCache_m1118812459 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::ClearCookies()
extern "C"  void UniWebView_ClearCookies_m1432016756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetCookie(System.String,System.String,System.Boolean)
extern "C"  void UniWebView_SetCookie_m3943662485 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___cookie1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebView::GetCookie(System.String,System.String,System.Boolean)
extern "C"  String_t* UniWebView_GetCookie_m2323665862 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___key1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::ClearHttpAuthUsernamePassword(System.String,System.String)
extern "C"  void UniWebView_ClearHttpAuthUsernamePassword_m1820233568 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___realm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UniWebView::get_BackgroundColor()
extern "C"  Color_t4194546905  UniWebView_get_BackgroundColor_m3154958837 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::set_BackgroundColor(UnityEngine.Color)
extern "C"  void UniWebView_set_BackgroundColor_m1436156862 (UniWebView_t424341801 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UniWebView::get_Alpha()
extern "C"  float UniWebView_get_Alpha_m4038482649 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::set_Alpha(System.Single)
extern "C"  void UniWebView_set_Alpha_m2754337562 (UniWebView_t424341801 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetShowSpinnerWhileLoading(System.Boolean)
extern "C"  void UniWebView_SetShowSpinnerWhileLoading_m3387638574 (UniWebView_t424341801 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetSpinnerText(System.String)
extern "C"  void UniWebView_SetSpinnerText_m1542286956 (UniWebView_t424341801 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetHorizontalScrollBarEnabled(System.Boolean)
extern "C"  void UniWebView_SetHorizontalScrollBarEnabled_m1056192968 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetVerticalScrollBarEnabled(System.Boolean)
extern "C"  void UniWebView_SetVerticalScrollBarEnabled_m3816994138 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetBouncesEnabled(System.Boolean)
extern "C"  void UniWebView_SetBouncesEnabled_m23174271 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetZoomEnabled(System.Boolean)
extern "C"  void UniWebView_SetZoomEnabled_m2256382037 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::AddPermissionTrustDomain(System.String)
extern "C"  void UniWebView_AddPermissionTrustDomain_m212659716 (UniWebView_t424341801 * __this, String_t* ___domain0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::RemovePermissionTrustDomain(System.String)
extern "C"  void UniWebView_RemovePermissionTrustDomain_m2064306857 (UniWebView_t424341801 * __this, String_t* ___domain0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetBackButtonEnabled(System.Boolean)
extern "C"  void UniWebView_SetBackButtonEnabled_m2079085359 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetUseWideViewPort(System.Boolean)
extern "C"  void UniWebView_SetUseWideViewPort_m2853797063 (UniWebView_t424341801 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetLoadWithOverviewMode(System.Boolean)
extern "C"  void UniWebView_SetLoadWithOverviewMode_m169821585 (UniWebView_t424341801 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetImmersiveModeEnabled(System.Boolean)
extern "C"  void UniWebView_SetImmersiveModeEnabled_m2242156108 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetShowToolbar(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void UniWebView_SetShowToolbar_m1575041816 (UniWebView_t424341801 * __this, bool ___show0, bool ___animated1, bool ___onTop2, bool ___adjustInset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetToolbarDoneButtonText(System.String)
extern "C"  void UniWebView_SetToolbarDoneButtonText_m377961558 (UniWebView_t424341801 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetWebContentsDebuggingEnabled(System.Boolean)
extern "C"  void UniWebView_SetWebContentsDebuggingEnabled_m3549881224 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::SetWindowUserResizeEnabled(System.Boolean)
extern "C"  void UniWebView_SetWindowUserResizeEnabled_m2322993593 (UniWebView_t424341801 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::GetHTMLContent(System.Action`1<System.String>)
extern "C"  void UniWebView_GetHTMLContent_m1568118150 (UniWebView_t424341801 * __this, Action_1_t403047693 * ___handler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::Print()
extern "C"  void UniWebView_Print_m3096708637 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::OnDestroy()
extern "C"  void UniWebView_OnDestroy_m2061738379 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::OnApplicationPause(System.Boolean)
extern "C"  void UniWebView_OnApplicationPause_m593958446 (UniWebView_t424341801 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnShowTransitionFinished(System.String)
extern "C"  void UniWebView_InternalOnShowTransitionFinished_m299738864 (UniWebView_t424341801 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnHideTransitionFinished(System.String)
extern "C"  void UniWebView_InternalOnHideTransitionFinished_m4133142859 (UniWebView_t424341801 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnAnimateToFinished(System.String)
extern "C"  void UniWebView_InternalOnAnimateToFinished_m1295518112 (UniWebView_t424341801 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnAddJavaScriptFinished(UniWebViewNativeResultPayload)
extern "C"  void UniWebView_InternalOnAddJavaScriptFinished_m2215693805 (UniWebView_t424341801 * __this, UniWebViewNativeResultPayload_t996691953 * ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnEvalJavaScriptFinished(UniWebViewNativeResultPayload)
extern "C"  void UniWebView_InternalOnEvalJavaScriptFinished_m2122182360 (UniWebView_t424341801 * __this, UniWebViewNativeResultPayload_t996691953 * ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnPageFinished(UniWebViewNativeResultPayload)
extern "C"  void UniWebView_InternalOnPageFinished_m3414119570 (UniWebView_t424341801 * __this, UniWebViewNativeResultPayload_t996691953 * ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnPageStarted(System.String)
extern "C"  void UniWebView_InternalOnPageStarted_m1188099516 (UniWebView_t424341801 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnPageErrorReceived(UniWebViewNativeResultPayload)
extern "C"  void UniWebView_InternalOnPageErrorReceived_m304156435 (UniWebView_t424341801 * __this, UniWebViewNativeResultPayload_t996691953 * ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnMessageReceived(System.String)
extern "C"  void UniWebView_InternalOnMessageReceived_m3316942950 (UniWebView_t424341801 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnWebViewKeyDown(System.Int32)
extern "C"  void UniWebView_InternalOnWebViewKeyDown_m4008097831 (UniWebView_t424341801 * __this, int32_t ___keyCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalOnShouldClose()
extern "C"  void UniWebView_InternalOnShouldClose_m2412606297 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView::InternalWebContentProcessDidTerminate()
extern "C"  void UniWebView_InternalWebContentProcessDidTerminate_m4034422559 (UniWebView_t424341801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
