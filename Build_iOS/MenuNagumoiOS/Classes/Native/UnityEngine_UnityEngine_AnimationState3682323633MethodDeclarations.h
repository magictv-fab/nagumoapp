﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationState
struct AnimationState_t3682323633;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"

// System.Boolean UnityEngine.AnimationState::get_enabled()
extern "C"  bool AnimationState_get_enabled_m1514601936 (AnimationState_t3682323633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
extern "C"  void AnimationState_set_enabled_m1879429625 (AnimationState_t3682323633 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_weight(System.Single)
extern "C"  void AnimationState_set_weight_m1582950610 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WrapMode UnityEngine.AnimationState::get_wrapMode()
extern "C"  int32_t AnimationState_get_wrapMode_m2705936773 (AnimationState_t3682323633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationState_set_wrapMode_m3837047690 (AnimationState_t3682323633 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_time()
extern "C"  float AnimationState_get_time_m2290731302 (AnimationState_t3682323633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C"  void AnimationState_set_time_m3547497437 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
extern "C"  void AnimationState_set_normalizedTime_m1757568870 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m4187319075 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_length()
extern "C"  float AnimationState_get_length_m2089699583 (AnimationState_t3682323633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_layer(System.Int32)
extern "C"  void AnimationState_set_layer_m2805758051 (AnimationState_t3682323633 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
extern "C"  void AnimationState_AddMixingTransform_m4158051049 (AnimationState_t3682323633 * __this, Transform_t1659122786 * ___mix0, bool ___recursive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform)
extern "C"  void AnimationState_AddMixingTransform_m2145392148 (AnimationState_t3682323633 * __this, Transform_t1659122786 * ___mix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationState::get_name()
extern "C"  String_t* AnimationState_get_name_m1230036347 (AnimationState_t3682323633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
extern "C"  void AnimationState_set_blendMode_m2249435534 (AnimationState_t3682323633 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
