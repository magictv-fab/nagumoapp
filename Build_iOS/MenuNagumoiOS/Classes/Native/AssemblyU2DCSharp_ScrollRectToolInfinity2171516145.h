﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScrollRectToolInfinity/SelectChildren
struct SelectChildren_t2120299545;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectToolInfinity
struct  ScrollRectToolInfinity_t2171516145  : public MonoBehaviour_t667441552
{
public:
	// System.Single ScrollRectToolInfinity::snapMinVel
	float ___snapMinVel_3;
	// UnityEngine.RectTransform ScrollRectToolInfinity::rect
	RectTransform_t972643934 * ___rect_4;
	// UnityEngine.UI.ScrollRect ScrollRectToolInfinity::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_5;
	// System.Single ScrollRectToolInfinity::desaceleration
	float ___desaceleration_6;

public:
	inline static int32_t get_offset_of_snapMinVel_3() { return static_cast<int32_t>(offsetof(ScrollRectToolInfinity_t2171516145, ___snapMinVel_3)); }
	inline float get_snapMinVel_3() const { return ___snapMinVel_3; }
	inline float* get_address_of_snapMinVel_3() { return &___snapMinVel_3; }
	inline void set_snapMinVel_3(float value)
	{
		___snapMinVel_3 = value;
	}

	inline static int32_t get_offset_of_rect_4() { return static_cast<int32_t>(offsetof(ScrollRectToolInfinity_t2171516145, ___rect_4)); }
	inline RectTransform_t972643934 * get_rect_4() const { return ___rect_4; }
	inline RectTransform_t972643934 ** get_address_of_rect_4() { return &___rect_4; }
	inline void set_rect_4(RectTransform_t972643934 * value)
	{
		___rect_4 = value;
		Il2CppCodeGenWriteBarrier(&___rect_4, value);
	}

	inline static int32_t get_offset_of_scrollRect_5() { return static_cast<int32_t>(offsetof(ScrollRectToolInfinity_t2171516145, ___scrollRect_5)); }
	inline ScrollRect_t3606982749 * get_scrollRect_5() const { return ___scrollRect_5; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_5() { return &___scrollRect_5; }
	inline void set_scrollRect_5(ScrollRect_t3606982749 * value)
	{
		___scrollRect_5 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_5, value);
	}

	inline static int32_t get_offset_of_desaceleration_6() { return static_cast<int32_t>(offsetof(ScrollRectToolInfinity_t2171516145, ___desaceleration_6)); }
	inline float get_desaceleration_6() const { return ___desaceleration_6; }
	inline float* get_address_of_desaceleration_6() { return &___desaceleration_6; }
	inline void set_desaceleration_6(float value)
	{
		___desaceleration_6 = value;
	}
};

struct ScrollRectToolInfinity_t2171516145_StaticFields
{
public:
	// ScrollRectToolInfinity/SelectChildren ScrollRectToolInfinity::selectChildren
	SelectChildren_t2120299545 * ___selectChildren_2;

public:
	inline static int32_t get_offset_of_selectChildren_2() { return static_cast<int32_t>(offsetof(ScrollRectToolInfinity_t2171516145_StaticFields, ___selectChildren_2)); }
	inline SelectChildren_t2120299545 * get_selectChildren_2() const { return ___selectChildren_2; }
	inline SelectChildren_t2120299545 ** get_address_of_selectChildren_2() { return &___selectChildren_2; }
	inline void set_selectChildren_2(SelectChildren_t2120299545 * value)
	{
		___selectChildren_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectChildren_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
