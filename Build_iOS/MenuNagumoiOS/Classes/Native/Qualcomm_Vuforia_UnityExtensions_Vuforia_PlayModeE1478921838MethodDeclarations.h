﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
struct NullPlayModeEditorUtility_t1478921838;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro3794639002.h"

// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String)
extern "C"  void NullPlayModeEditorUtility_DisplayDialog_m2384374929 (NullPlayModeEditorUtility_t1478921838 * __this, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamProfile/ProfileCollection Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::LoadAndParseWebcamProfiles(System.String)
extern "C"  ProfileCollection_t3794639002  NullPlayModeEditorUtility_LoadAndParseWebcamProfiles_m3177070801 (NullPlayModeEditorUtility_t1478921838 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::RestartPlayMode()
extern "C"  void NullPlayModeEditorUtility_RestartPlayMode_m1739987973 (NullPlayModeEditorUtility_t1478921838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::.ctor()
extern "C"  void NullPlayModeEditorUtility__ctor_m3751633857 (NullPlayModeEditorUtility_t1478921838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
