﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SwipeGestureEvent
struct  SwipeGestureEvent_t2968962937  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SwipeGestureEvent::minSwipeDistance
	FsmFloat_t2134102846 * ___minSwipeDistance_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeLeftEvent
	FsmEvent_t2133468028 * ___swipeLeftEvent_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeRightEvent
	FsmEvent_t2133468028 * ___swipeRightEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeUpEvent
	FsmEvent_t2133468028 * ___swipeUpEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeDownEvent
	FsmEvent_t2133468028 * ___swipeDownEvent_13;
	// System.Single HutongGames.PlayMaker.Actions.SwipeGestureEvent::screenDiagonalSize
	float ___screenDiagonalSize_14;
	// System.Single HutongGames.PlayMaker.Actions.SwipeGestureEvent::minSwipeDistancePixels
	float ___minSwipeDistancePixels_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SwipeGestureEvent::touchStarted
	bool ___touchStarted_16;
	// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.SwipeGestureEvent::touchStartPos
	Vector2_t4282066565  ___touchStartPos_17;

public:
	inline static int32_t get_offset_of_minSwipeDistance_9() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___minSwipeDistance_9)); }
	inline FsmFloat_t2134102846 * get_minSwipeDistance_9() const { return ___minSwipeDistance_9; }
	inline FsmFloat_t2134102846 ** get_address_of_minSwipeDistance_9() { return &___minSwipeDistance_9; }
	inline void set_minSwipeDistance_9(FsmFloat_t2134102846 * value)
	{
		___minSwipeDistance_9 = value;
		Il2CppCodeGenWriteBarrier(&___minSwipeDistance_9, value);
	}

	inline static int32_t get_offset_of_swipeLeftEvent_10() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___swipeLeftEvent_10)); }
	inline FsmEvent_t2133468028 * get_swipeLeftEvent_10() const { return ___swipeLeftEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_swipeLeftEvent_10() { return &___swipeLeftEvent_10; }
	inline void set_swipeLeftEvent_10(FsmEvent_t2133468028 * value)
	{
		___swipeLeftEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___swipeLeftEvent_10, value);
	}

	inline static int32_t get_offset_of_swipeRightEvent_11() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___swipeRightEvent_11)); }
	inline FsmEvent_t2133468028 * get_swipeRightEvent_11() const { return ___swipeRightEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_swipeRightEvent_11() { return &___swipeRightEvent_11; }
	inline void set_swipeRightEvent_11(FsmEvent_t2133468028 * value)
	{
		___swipeRightEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___swipeRightEvent_11, value);
	}

	inline static int32_t get_offset_of_swipeUpEvent_12() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___swipeUpEvent_12)); }
	inline FsmEvent_t2133468028 * get_swipeUpEvent_12() const { return ___swipeUpEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_swipeUpEvent_12() { return &___swipeUpEvent_12; }
	inline void set_swipeUpEvent_12(FsmEvent_t2133468028 * value)
	{
		___swipeUpEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___swipeUpEvent_12, value);
	}

	inline static int32_t get_offset_of_swipeDownEvent_13() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___swipeDownEvent_13)); }
	inline FsmEvent_t2133468028 * get_swipeDownEvent_13() const { return ___swipeDownEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_swipeDownEvent_13() { return &___swipeDownEvent_13; }
	inline void set_swipeDownEvent_13(FsmEvent_t2133468028 * value)
	{
		___swipeDownEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___swipeDownEvent_13, value);
	}

	inline static int32_t get_offset_of_screenDiagonalSize_14() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___screenDiagonalSize_14)); }
	inline float get_screenDiagonalSize_14() const { return ___screenDiagonalSize_14; }
	inline float* get_address_of_screenDiagonalSize_14() { return &___screenDiagonalSize_14; }
	inline void set_screenDiagonalSize_14(float value)
	{
		___screenDiagonalSize_14 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistancePixels_15() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___minSwipeDistancePixels_15)); }
	inline float get_minSwipeDistancePixels_15() const { return ___minSwipeDistancePixels_15; }
	inline float* get_address_of_minSwipeDistancePixels_15() { return &___minSwipeDistancePixels_15; }
	inline void set_minSwipeDistancePixels_15(float value)
	{
		___minSwipeDistancePixels_15 = value;
	}

	inline static int32_t get_offset_of_touchStarted_16() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___touchStarted_16)); }
	inline bool get_touchStarted_16() const { return ___touchStarted_16; }
	inline bool* get_address_of_touchStarted_16() { return &___touchStarted_16; }
	inline void set_touchStarted_16(bool value)
	{
		___touchStarted_16 = value;
	}

	inline static int32_t get_offset_of_touchStartPos_17() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t2968962937, ___touchStartPos_17)); }
	inline Vector2_t4282066565  get_touchStartPos_17() const { return ___touchStartPos_17; }
	inline Vector2_t4282066565 * get_address_of_touchStartPos_17() { return &___touchStartPos_17; }
	inline void set_touchStartPos_17(Vector2_t4282066565  value)
	{
		___touchStartPos_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
