﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Header_toggle_buttons
struct Header_toggle_buttons_t797597032;

#include "codegen/il2cpp-codegen.h"

// System.Void Header_toggle_buttons::.ctor()
extern "C"  void Header_toggle_buttons__ctor_m3674943747 (Header_toggle_buttons_t797597032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Header_toggle_buttons::Start()
extern "C"  void Header_toggle_buttons_Start_m2622081539 (Header_toggle_buttons_t797597032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Header_toggle_buttons::Update()
extern "C"  void Header_toggle_buttons_Update_m3980968554 (Header_toggle_buttons_t797597032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Header_toggle_buttons::setToggleOn(System.Int32)
extern "C"  void Header_toggle_buttons_setToggleOn_m4098240551 (Header_toggle_buttons_t797597032 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Header_toggle_buttons::setMyToggles()
extern "C"  void Header_toggle_buttons_setMyToggles_m2333791634 (Header_toggle_buttons_t797597032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
