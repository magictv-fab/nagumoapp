﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.PDF417ResultMetadata
struct PDF417ResultMetadata_t2693798038;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.PDF417.PDF417ResultMetadata::set_SegmentIndex(System.Int32)
extern "C"  void PDF417ResultMetadata_set_SegmentIndex_m3569692598 (PDF417ResultMetadata_t2693798038 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417ResultMetadata::set_FileId(System.String)
extern "C"  void PDF417ResultMetadata_set_FileId_m3025143845 (PDF417ResultMetadata_t2693798038 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.PDF417.PDF417ResultMetadata::get_OptionalData()
extern "C"  Int32U5BU5D_t3230847821* PDF417ResultMetadata_get_OptionalData_m1865883728 (PDF417ResultMetadata_t2693798038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417ResultMetadata::set_OptionalData(System.Int32[])
extern "C"  void PDF417ResultMetadata_set_OptionalData_m3816933663 (PDF417ResultMetadata_t2693798038 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417ResultMetadata::set_IsLastSegment(System.Boolean)
extern "C"  void PDF417ResultMetadata_set_IsLastSegment_m96336134 (PDF417ResultMetadata_t2693798038 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.PDF417ResultMetadata::.ctor()
extern "C"  void PDF417ResultMetadata__ctor_m4293902107 (PDF417ResultMetadata_t2693798038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
