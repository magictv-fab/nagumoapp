﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ActivateGameObject
struct  ActivateGameObject_t1848802188  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ActivateGameObject::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ActivateGameObject::activate
	FsmBool_t1075959796 * ___activate_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ActivateGameObject::recursive
	FsmBool_t1075959796 * ___recursive_11;
	// System.Boolean HutongGames.PlayMaker.Actions.ActivateGameObject::resetOnExit
	bool ___resetOnExit_12;
	// System.Boolean HutongGames.PlayMaker.Actions.ActivateGameObject::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ActivateGameObject::activatedGameObject
	GameObject_t3674682005 * ___activatedGameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_activate_10() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___activate_10)); }
	inline FsmBool_t1075959796 * get_activate_10() const { return ___activate_10; }
	inline FsmBool_t1075959796 ** get_address_of_activate_10() { return &___activate_10; }
	inline void set_activate_10(FsmBool_t1075959796 * value)
	{
		___activate_10 = value;
		Il2CppCodeGenWriteBarrier(&___activate_10, value);
	}

	inline static int32_t get_offset_of_recursive_11() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___recursive_11)); }
	inline FsmBool_t1075959796 * get_recursive_11() const { return ___recursive_11; }
	inline FsmBool_t1075959796 ** get_address_of_recursive_11() { return &___recursive_11; }
	inline void set_recursive_11(FsmBool_t1075959796 * value)
	{
		___recursive_11 = value;
		Il2CppCodeGenWriteBarrier(&___recursive_11, value);
	}

	inline static int32_t get_offset_of_resetOnExit_12() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___resetOnExit_12)); }
	inline bool get_resetOnExit_12() const { return ___resetOnExit_12; }
	inline bool* get_address_of_resetOnExit_12() { return &___resetOnExit_12; }
	inline void set_resetOnExit_12(bool value)
	{
		___resetOnExit_12 = value;
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_activatedGameObject_14() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___activatedGameObject_14)); }
	inline GameObject_t3674682005 * get_activatedGameObject_14() const { return ___activatedGameObject_14; }
	inline GameObject_t3674682005 ** get_address_of_activatedGameObject_14() { return &___activatedGameObject_14; }
	inline void set_activatedGameObject_14(GameObject_t3674682005 * value)
	{
		___activatedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___activatedGameObject_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
