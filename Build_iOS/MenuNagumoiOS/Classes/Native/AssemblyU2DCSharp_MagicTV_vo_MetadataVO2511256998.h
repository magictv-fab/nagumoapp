﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.MetadataVO
struct  MetadataVO_t2511256998  : public VOWithId_t2461972856
{
public:
	// System.String MagicTV.vo.MetadataVO::key
	String_t* ___key_1;
	// System.String MagicTV.vo.MetadataVO::value
	String_t* ___value_2;
	// System.String MagicTV.vo.MetadataVO::tabela
	String_t* ___tabela_3;
	// System.String MagicTV.vo.MetadataVO::tabela_id
	String_t* ___tabela_id_4;

public:
	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(MetadataVO_t2511256998, ___key_1)); }
	inline String_t* get_key_1() const { return ___key_1; }
	inline String_t** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(String_t* value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier(&___key_1, value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(MetadataVO_t2511256998, ___value_2)); }
	inline String_t* get_value_2() const { return ___value_2; }
	inline String_t** get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(String_t* value)
	{
		___value_2 = value;
		Il2CppCodeGenWriteBarrier(&___value_2, value);
	}

	inline static int32_t get_offset_of_tabela_3() { return static_cast<int32_t>(offsetof(MetadataVO_t2511256998, ___tabela_3)); }
	inline String_t* get_tabela_3() const { return ___tabela_3; }
	inline String_t** get_address_of_tabela_3() { return &___tabela_3; }
	inline void set_tabela_3(String_t* value)
	{
		___tabela_3 = value;
		Il2CppCodeGenWriteBarrier(&___tabela_3, value);
	}

	inline static int32_t get_offset_of_tabela_id_4() { return static_cast<int32_t>(offsetof(MetadataVO_t2511256998, ___tabela_id_4)); }
	inline String_t* get_tabela_id_4() const { return ___tabela_id_4; }
	inline String_t** get_address_of_tabela_id_4() { return &___tabela_id_4; }
	inline void set_tabela_id_4(String_t* value)
	{
		___tabela_id_4 = value;
		Il2CppCodeGenWriteBarrier(&___tabela_id_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
