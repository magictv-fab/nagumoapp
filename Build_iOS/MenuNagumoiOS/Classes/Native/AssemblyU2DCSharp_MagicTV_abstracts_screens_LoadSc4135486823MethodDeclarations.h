﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.LoadScreenBar
struct LoadScreenBar_t4135486823;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.abstracts.screens.LoadScreenBar::.ctor()
extern "C"  void LoadScreenBar__ctor_m2922966637 (LoadScreenBar_t4135486823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::prepare()
extern "C"  void LoadScreenBar_prepare_m989306994 (LoadScreenBar_t4135486823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::Start()
extern "C"  void LoadScreenBar_Start_m1870104429 (LoadScreenBar_t4135486823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::setMessage(System.String)
extern "C"  void LoadScreenBar_setMessage_m3945449446 (LoadScreenBar_t4135486823 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::setPercentLoaded(System.Single)
extern "C"  void LoadScreenBar_setPercentLoaded_m3550668428 (LoadScreenBar_t4135486823 * __this, float ___total0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::hide()
extern "C"  void LoadScreenBar_hide_m651453913 (LoadScreenBar_t4135486823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::HideNow()
extern "C"  void LoadScreenBar_HideNow_m547574335 (LoadScreenBar_t4135486823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenBar::show()
extern "C"  void LoadScreenBar_show_m965796052 (LoadScreenBar_t4135486823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
