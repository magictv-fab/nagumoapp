﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC
struct  U3CISendStatusU3Ec__IteratorC_t4229773480  : public Il2CppObject
{
public:
	// System.String OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<acao>__0
	String_t* ___U3CacaoU3E__0_0;
	// System.String OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::id
	String_t* ___id_1;
	// System.Int32 OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::status
	int32_t ___status_2;
	// System.String OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<json>__1
	String_t* ___U3CjsonU3E__1_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<headers>__2
	Dictionary_2_t827649927 * ___U3CheadersU3E__2_4;
	// System.Byte[] OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<pData>__3
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__3_5;
	// UnityEngine.WWW OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_6;
	// System.Int32 OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::$PC
	int32_t ___U24PC_7;
	// System.Object OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::$current
	Il2CppObject * ___U24current_8;
	// System.String OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<$>id
	String_t* ___U3CU24U3Eid_9;
	// System.Int32 OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::<$>status
	int32_t ___U3CU24U3Estatus_10;

public:
	inline static int32_t get_offset_of_U3CacaoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CacaoU3E__0_0)); }
	inline String_t* get_U3CacaoU3E__0_0() const { return ___U3CacaoU3E__0_0; }
	inline String_t** get_address_of_U3CacaoU3E__0_0() { return &___U3CacaoU3E__0_0; }
	inline void set_U3CacaoU3E__0_0(String_t* value)
	{
		___U3CacaoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CacaoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___status_2)); }
	inline int32_t get_status_2() const { return ___status_2; }
	inline int32_t* get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(int32_t value)
	{
		___status_2 = value;
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_3() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CjsonU3E__1_3)); }
	inline String_t* get_U3CjsonU3E__1_3() const { return ___U3CjsonU3E__1_3; }
	inline String_t** get_address_of_U3CjsonU3E__1_3() { return &___U3CjsonU3E__1_3; }
	inline void set_U3CjsonU3E__1_3(String_t* value)
	{
		___U3CjsonU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__2_4() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CheadersU3E__2_4)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__2_4() const { return ___U3CheadersU3E__2_4; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__2_4() { return &___U3CheadersU3E__2_4; }
	inline void set_U3CheadersU3E__2_4(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__3_5() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CpDataU3E__3_5)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__3_5() const { return ___U3CpDataU3E__3_5; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__3_5() { return &___U3CpDataU3E__3_5; }
	inline void set_U3CpDataU3E__3_5(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_6() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CwwwU3E__4_6)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_6() const { return ___U3CwwwU3E__4_6; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_6() { return &___U3CwwwU3E__4_6; }
	inline void set_U3CwwwU3E__4_6(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eid_9() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CU24U3Eid_9)); }
	inline String_t* get_U3CU24U3Eid_9() const { return ___U3CU24U3Eid_9; }
	inline String_t** get_address_of_U3CU24U3Eid_9() { return &___U3CU24U3Eid_9; }
	inline void set_U3CU24U3Eid_9(String_t* value)
	{
		___U3CU24U3Eid_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eid_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Estatus_10() { return static_cast<int32_t>(offsetof(U3CISendStatusU3Ec__IteratorC_t4229773480, ___U3CU24U3Estatus_10)); }
	inline int32_t get_U3CU24U3Estatus_10() const { return ___U3CU24U3Estatus_10; }
	inline int32_t* get_address_of_U3CU24U3Estatus_10() { return &___U3CU24U3Estatus_10; }
	inline void set_U3CU24U3Estatus_10(int32_t value)
	{
		___U3CU24U3Estatus_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
