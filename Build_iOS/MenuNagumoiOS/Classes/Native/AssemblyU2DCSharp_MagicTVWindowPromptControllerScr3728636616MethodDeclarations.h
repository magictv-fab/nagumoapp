﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowPromptControllerScript/ClickOkPromptDelegate
struct ClickOkPromptDelegate_t3728636616;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTVWindowPromptControllerScript/ClickOkPromptDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ClickOkPromptDelegate__ctor_m316678815 (ClickOkPromptDelegate_t3728636616 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowPromptControllerScript/ClickOkPromptDelegate::Invoke(System.String)
extern "C"  void ClickOkPromptDelegate_Invoke_m3701103593 (ClickOkPromptDelegate_t3728636616 * __this, String_t* ___textMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTVWindowPromptControllerScript/ClickOkPromptDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ClickOkPromptDelegate_BeginInvoke_m415002358 (ClickOkPromptDelegate_t3728636616 * __this, String_t* ___textMessage0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowPromptControllerScript/ClickOkPromptDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ClickOkPromptDelegate_EndInvoke_m380877871 (ClickOkPromptDelegate_t3728636616 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
