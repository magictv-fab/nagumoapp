﻿using UnityEngine;
using System.Collections;

public class PopupManager : MonoBehaviour {

    public float time = 3f;
    public float lifeTime = 5f;
    public iTween.EaseType easetype = iTween.EaseType.easeInOutSine;
    public GameObject fundo;
    public bool automaticClose = true;

    private void Awake()
    {
        transform.localScale = Vector3.zero;

    }

    void OnEnable () 
    {
		transform.localScale = Vector3.zero;

		if (fundo)
			fundo.SetActive(true);

        Hashtable ht = new Hashtable();
        ht.Add("scale", Vector3.one);
        ht.Add("time", time);
        ht.Add("easetype", easetype);
        ht.Add("oncompletetarget", gameObject);

		if (automaticClose)
			ht.Add("oncomplete", "OnComplete");

        iTween.ScaleTo(gameObject, ht);
	}
	
    public void OnComplete()
    {
		Hashtable ht = new Hashtable();
        ht.Add("scale", Vector3.zero);
		ht.Add("time", time);
        ht.Add("delay", lifeTime);
		ht.Add("easetype", easetype);
		ht.Add("oncompletetarget", gameObject);
		ht.Add("oncomplete", "Disable");
		ht.Add("oncompletedelay", 1f);

		iTween.ScaleTo(gameObject, ht);       
    }

    public void Disable()
    {
		if (fundo)
			fundo.SetActive(false);
        
        gameObject.SetActive(false);
    }
	
}
