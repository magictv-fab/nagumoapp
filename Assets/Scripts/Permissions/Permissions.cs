﻿using UnityEngine;
using System.Collections;

public class Permissions : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Permission();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Permission()
	{
#if UNITY_ANDROID
		AndroidJavaClass androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo= androidJC.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass jc = new AndroidJavaClass("com.grafite.androidpermissionplugin.Permission");
		jc.CallStatic("checkPermission", jo);
#endif
	}
}
