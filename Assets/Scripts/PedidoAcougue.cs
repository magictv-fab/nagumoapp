﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class PedidoAcougue : MonoBehaviour
{
    public static AcougueData acougueData;
    public static PedidoAcougue instance;
    public static int posicao;

    public static List<string> titleLst = new List<string>();
    public static List<int> idLst = new List<int>();

    public static List<CortesData[]> cortesLst = new List<CortesData[]>();
    public static List<Texture2D> textureLst = new List<Texture2D>();
    public static List<Sprite> imgLst = new List<Sprite>();
    public static List<bool> favoritouLst = new List<bool>();

    public GameObject loadingObj, popup;
    public GameObject itemPrefab;
    public GameObject containerItens;
    public static List<GameObject> itensList = new List<GameObject>();
    public Dropdown dropdownTipoDeCorte;
    public Dropdown dropdownPeso;
    public Text positionText;
    public InputField quantidade, observacoes;
    public Text textPeso;

    public PedidoData pedidoData = new PedidoData();

    private ScrollRect scrollRect;
    private int carnesCount;
    public static GameObject currentItem;
    public GameObject selectedItem;
    public Text quantidadeOfertasText;
    public UnityEngine.UI.Extensions.HorizontalScrollSnap horizontalScrollSnap;
    public Text carneTitle;
    private int categoria_carne = 1;
    private static Dictionary<string, int> corteIdDict = new Dictionary<string, int>();
    private static string currentJsonTxt;

    // Use this for initialization
    void Start()
    {
        instance = this;

        StartCoroutine(ILoadCarnes());

        scrollRect = GetComponent<ScrollRect>();

    }

    public void BtnSelectPeso(string peso)
    {
        textPeso.transform.parent.gameObject.SetActive(true);
        textPeso.text = peso;
    }

    public void BtnTipoCarne(int tipo)
    {
        
        if (this.categoria_carne != tipo)
        {
            this.categoria_carne = tipo;
            StartCoroutine(ILoadCarnes());
        }

    }

    public void BtnSearch(string search)
    {
        StartCoroutine(ILoadCarnes(search));
        //PopUp("Em desenvolvimento");
    }

    public void ShowNumberSelected(int value)
    {
        if (titleLst.Count > 0)
        {
            quantidadeOfertasText.text = (value + 1).ToString() + " de " + titleLst.Count.ToString();
        }
        else
        {
            quantidadeOfertasText.text = "";
        }
    }

    public void InstantiateNextItem()
    {
        if (carnesCount >= titleLst.Count)
            return;

        var obj = Instantiate(itemPrefab);
        obj.transform.SetParent(containerItens.transform, false);
        obj.transform.localScale = Vector3.one;
        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        CarneValues carneValues = obj.GetComponent<CarneValues>();

        carneValues.title = titleLst[carnesCount];
        carneValues.texture = textureLst[carnesCount];
        carneValues.img.sprite = imgLst[carnesCount];
        carneValues.id = idLst[carnesCount];
        carneValues.tiposCortesLst = new List<CortesData>(cortesLst[carnesCount]);

        foreach (CortesData corteData in cortesLst[carnesCount])
        {
            carneValues.optionDatas.Add(new Dropdown.OptionData(corteData.corte));

            if(!corteIdDict.ContainsKey(corteData.corte))
                corteIdDict.Add(corteData.corte, corteData.id_corte);
        }

        obj.transform.name = titleLst[carnesCount];

        obj.transform.SetAsFirstSibling();
        currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

        itensList.Add(obj);

        carnesCount++;
    }

    public void SendStatus(int status)
    {
        StartCoroutine(ISendStatus(selectedItem.GetComponent<OfertaValues>().id, status));
    }

    public void BtnAccept()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * 2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onAllow.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(1);
    }

    public void BtnRecuse()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * -2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onDeny.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(0);
    }

    private IEnumerator ISendStatus(string id, int status)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"status_oferta\":\"" + status + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_carnes.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ILoadCarnes(string search = "")
    {
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"categoria_carne\":" + categoria_carne + ",\"loja\":" + MagicTV.in_apps.InAppManager.lojaCode + "}";

        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.urlAcougue + "/pegar_carnes.php", pData, headers);

        if (search != "")
        {
            json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"pesquisa\":\"" + search + "\",\"loja\":" + MagicTV.in_apps.InAppManager.lojaCode + "\"}";
           
            headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());
            www = new WWW(ServerControl.urlAcougue + "/pesquisar_carnes.php", pData, headers);

            this.categoria_carne = -1;
        }


        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            //verifica se o txt que chegou eh diferente do que tem ja.
            if (currentJsonTxt != www.text)
            {
                Debug.Log("Tem atualizacao de Ofertas!");
                currentJsonTxt = www.text;

                //Atualiza:
                if (loadingObj)
                    loadingObj.SetActive(true);

                //LoaderPublishAnim.instance.EndLoad();

                foreach (GameObject obj in itensList)
                    Destroy(obj);

                itensList.Clear();
                titleLst.Clear();
                cortesLst.Clear();
                idLst.Clear();
                favoritouLst.Clear();
                imgLst.Clear();
                textureLst.Clear();

                carnesCount = 0;
                posicao = 0;

                try
                {
                    string message = SimpleJSON.EscapeString(www.text);

                    //popula obj json.
                    acougueData = JsonUtility.FromJson<AcougueData>(message);

                    //posicao da fila.
                    posicao = acougueData.posicao + 1;

                    //passa as informacoes para as listas.
                    foreach (CarnesData carneData in acougueData.carnes)
                    {
                        titleLst.Add(carneData.nome_produto);
                        idLst.Add(carneData.id_carne);
                        cortesLst.Add(carneData.cortes);
                    }

                    //tratamento caso não tenha ofertas.
                    if (acougueData.carnes.Length > 0)
                    {
                        StartCoroutine(ILoadImgs());
                    }
                    else
                    {
                        if (loadingObj)
                            loadingObj.SetActive(false);
                    }
                }
                catch
                {
                    PopUp(www.text);
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
            else
            {
                //se o txt for igual so baixa os que ja tem.
                //tratamento se n tiver ofertas.
                if (acougueData != null)
                {
                    StartCoroutine(LoadLoadeds());
                }
                else
                {
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
        }

        //atualiza posicao da fila.
        positionText.text = posicao.ToString();
    }

    public static string GetDateString(string date)
    {
        System.DateTime theDate = System.DateTime.Parse(date);
        return theDate.ToString("dd/MM/yyyy");

    }

    private IEnumerator ILoadImgs()
    {
        foreach (CarnesData carneData in acougueData.carnes)
        {
            WWW www = new WWW(ServerControl.urlIMGAcougue + "/" + carneData.imagem);
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                Texture2D texture = www.texture;
                //texture.LoadImage(www.bytes);
                //texture.Apply();
                Sprite spt = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                imgLst.Add(spt);
                textureLst.Add(texture);
            }
            else
            {
                imgLst.Add(new Sprite());
                textureLst.Add(new Texture2D(100, 100));

                Debug.LogError("Erro: " + www.error);
                Debug.LogError("url: " + ServerControl.urlIMGAcougue + "/" + carneData.imagem);
            }
        }

        StartCoroutine(LoadLoadeds());
    }

    private IEnumerator LoadLoadeds()
    {
        if (loadingObj)
            loadingObj.SetActive(true);

        scrollRect = horizontalScrollSnap._scroll_rect;

        //inicializa com 2 cartoes de oferta.
        //for (int i = 0; i < 2; i++)
        for (int i = 0; i < acougueData.carnes.Length; i++)
        {
            InstantiateNextItem();
        }

        if (acougueData.carnes.Length > 0)
            if (quantidadeOfertasText)
            quantidadeOfertasText.text = "1 de " + acougueData.carnes.Length;

        yield return new WaitForEndOfFrame();

        //scrollRect.content = containerItens.transform.GetChild(containerItens.transform.childCount - 1).GetComponent<RectTransform>();

        if (horizontalScrollSnap)
        {
            horizontalScrollSnap.InitialiseChildObjectsFromScene();
            horizontalScrollSnap.UpdateLayout();
            scrollRect.content.localPosition = Vector3.zero;
            if (currentItem)
                scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scrollRect.content.rect.width + currentItem.GetComponent<RectTransform>().rect.width / 2f);
        }

        // StartCoroutine(ILoadOfertas());

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);

        if (popup)
        {
            popup.GetComponentInChildren<Text>().text = txt;
            popup.SetActive(true);
        }
    }

    public void BtnAvancar()
    {
        if(textPeso.text == "100")
        {
            PopUp("Você precisa escolher o peso!");
            return;
        }

        if (!dropdownTipoDeCorte.captionText.transform.parent.gameObject.activeSelf)
        {
            PopUp("Você precisa escolher o tipo de corte!");
            return;
        }

        pedidoData.nome = carneTitle.text;
        pedidoData.info = "Peso: " + textPeso.text + "\n" + 
            "Tipo de Corte: " + dropdownTipoDeCorte.options[dropdownTipoDeCorte.value].text + "\n" +
            "Unidade: Atibaia Alvinólois \nHorario / data retirada: \n" + System.DateTime.Now.ToString("dd/MM/yyyy") + " às " + System.DateTime.Now.ToString("HH:mm");
        
        pedidoData.peso = textPeso.text;
        pedidoData.timeStamp = "";//GetTimestamp(DateTime.Now);
        pedidoData.observacoes = observacoes.text;
        pedidoData.idCorte = corteIdDict[dropdownTipoDeCorte.options[dropdownTipoDeCorte.value].text];

        if(!string.IsNullOrEmpty(quantidade.text))
            pedidoData.quantidade = int.Parse(quantidade.text);

        PedidosRevisao.pedidosLst.Add(pedidoData);
        Application.LoadLevel("Acougue_Revise");
    }


    public static string GetTimestamp(DateTime value)
    {
        return value.ToString("yyyyMMddHHmmssffff");
    }
}