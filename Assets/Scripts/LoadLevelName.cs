﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevelName : MonoBehaviour
{

    public static string loadedScene;
    public static bool isAcougue;   //usado para mostrar a ui de ra do acougue na tela de ra.

    public GameObject disableObj;
    public GameObject enableAcougue;

    private bool back;
    private string name;

    // Use this for initialization
    void Start()
    {

        if (isAcougue)
        {
            enableAcougue.SetActive(true);
            isAcougue = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void BackButton()
    {
        SceneManager.UnloadScene(SceneManager.GetSceneAt(SceneManager.sceneCount - 1).name);
        Analytics.LevelChange("Menu_Nagumo");
    }

    public void LoadLevel(string name)
    {
        this.name = name;
        if (!back)
        {
            back = true;
            StartCoroutine(GoLevel());
        }
    }

    public void DoceNovembroLevel(int indexPremio)
    {
        SharerPhoto.awardIndex = indexPremio;
		LoadLevel("Compartilhar2");
    }

    public void LoadLevelAcougue()
    {
        LoadLevelName.isAcougue = false;
        this.name = "Acougue_Pedido";
        if (!back)
        {
            back = true;
            StartCoroutine(GoLevel());
        }
    }

    public void LoadLevelCenaMain(bool isAcougue)
    {
        LoadLevelName.isAcougue = isAcougue;
        this.name = "cena_load_main";
        if (!back)
        {
            back = true;
            StartCoroutine(GoLevel());
        }
    }

    public IEnumerator GoLevel()
    {

        yield return new WaitForEndOfFrame();


        AsyncOperation async = Application.LoadLevelAsync(this.name);


        yield return async;


    }

    public void LoadLevelAdditive(string name)
    {
        LoadLevelName.isAcougue = false;

        this.name = name;
        loadedScene = name;

        StartCoroutine(GoLevelAdditive());
    }

    public IEnumerator GoLevelAdditive()
    {

        yield return new WaitForEndOfFrame();

        AsyncOperation async = SceneManager.LoadSceneAsync(this.name, LoadSceneMode.Additive);

        yield return async;

        if (disableObj)
            disableObj.SetActive(false);

    }

    public void Exit()
    {
        Application.Quit();
    }

}
