﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

    public float velocity = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.forward * 100f * Time.deltaTime * velocity);
	}
}
