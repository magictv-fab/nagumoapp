﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GerarCupom : MonoBehaviour {

    public int cuponPontosValue = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ButtonGerar()
    {
        //CuponsControllerNagumo.instance.pontos -= cuponPontosValue;

        if (CuponsControllerNagumo.instance.loadingObj)
            CuponsControllerNagumo.instance.loadingObj.SetActive(true);
        
        StartCoroutine(IGerar());
    }

    private IEnumerator IGerar()
    {
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"pontos\":" + cuponPontosValue + "}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/gerar_cupom.php", pData, headers);

        Debug.Log("URL " + www.url);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            CuponsControllerNagumo.instance.PopUp(www.error);
            if (CuponsControllerNagumo.instance.loadingObj)
                CuponsControllerNagumo.instance.loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            try
            {
                //[0] = code. [1] = message;
                string code = www.text.Split(',')[0];
                string message = www.text.Remove(0, code.Length + 1);
                Debug.Log(code);
                Debug.Log(message);

                if (code.Contains("200"))
                {
                    //popula obj json.
                    CuponData cuponNagumoData = JsonUtility.FromJson<CuponData>(message);
                    CuponsControllerNagumo.instance.pontos = cuponNagumoData.pts;
                   
                    if (CuponsControllerNagumo.instance.loadingObj)
                        CuponsControllerNagumo.instance.loadingObj.SetActive(false);
                    
                    CuponsControllerNagumo.instance.ClearAll();
                    CuponsControllerNagumo.instance.Instantiation();
                    CuponsControllerNagumo.instance.UpdateTxt();
                }
                else
                {
                    CuponsControllerNagumo.instance.PopUp(message);
                    if (CuponsControllerNagumo.instance.loadingObj)
                        CuponsControllerNagumo.instance.loadingObj.SetActive(false);
                }
            }
            catch
            {
                CuponsControllerNagumo.instance.PopUp(www.text);
                if (CuponsControllerNagumo.instance.loadingObj)
                    CuponsControllerNagumo.instance.loadingObj.SetActive(false);
            }
        }
    }
}
