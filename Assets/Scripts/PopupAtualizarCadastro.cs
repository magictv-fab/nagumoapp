﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PopupAtualizarCadastro : MonoBehaviour {

	// Use this for initialization

	void Awake () {
        if(PlayerPrefs.GetInt("atualizeCadastro") == 0)
        {
            gameObject.SetActive(true);
            gameObject.SetActiveRecursively(true);
        }
        else
        {
            
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FecharPopup()
    {
        gameObject.SetActive(false);
        PlayerPrefs.SetInt("atualizeCadastro",1);
        PlayerPrefs.Save();

        SceneManager.LoadScene("Atualizar", LoadSceneMode.Additive);
    }
}
