﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Vuforia;
using MagicTV.globals;
using MagicTV.in_apps;
using MagicTV.processes;

public class AutoFocusAndroid : MonoBehaviour {

	#if UNITY_ANDROID

	private bool whiles = true;
	private int i = 1;
	private bool newStatus;

//	public void AutoCheckFocus(string msg)
//	{
//		GameObject AutoFocusCheck = GameObject.FindGameObjectWithTag("AutoFocusChecker");
//		Text autoText = AutoFocusCheck.GetComponentInChildren<Text>();
//		Debug.Log(autoText.text);
//
//		autoText.text = msg; 
//
//	}
	
	private IEnumerator AutoFocus()
	{
		while (whiles == true) {
			yield return new WaitForSeconds (2);

			GameObject MaTvMa = GameObject.Find("MagicTVMain");
			newStatus = PresentationController.checkTracking;


			if(newStatus == true)
			{
//				AutoCheckFocus("Auto Focus Off" + i++ + " " + newStatus);
				i = 1;
			} else {
				CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

//				AutoCheckFocus("Auto Focus On " + i++ + " " + newStatus);
			}

		}
	}

	void Start () {

		StartCoroutine(AutoFocus());

	}
	
	#endif

}
