﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropDownRemoveLastItem : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //remove o ultimo item para colocar ele como titulo do dropdown.
        GetComponent<Dropdown>().options.Remove(GetComponent<Dropdown>().options[GetComponent<Dropdown>().options.Count - 1]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
