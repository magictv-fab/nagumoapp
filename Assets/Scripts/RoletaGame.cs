﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum RoletaPointAngle
{
    point_1,
    point_2,
    point_3,
    point_5,
    point_10
}

public class RoletaGame : MonoBehaviour
{

    public static RoletaGame instance;

    public Text jogadasTxt, restaJogadasTxt;
    public bool offline;
    public int points = 1;
    public float time = 3f;
    public float speed = 10f;
    public List<float> pointAngles = new List<float>();
    public iTween.EaseType easetype = iTween.EaseType.linear;
    public GameObject roletaGO;
    public List<GameObject> popups = new List<GameObject>();
    public float popupTime = 5f;
    public Dictionary<int, int> ptsIndex = new Dictionary<int, int>();

    private bool inGame = false;
    private int startSpins = 0;

	// Use this for initialization
	void Start () 
    {
        ptsIndex.Add(1, 0);
        ptsIndex.Add(2, 1);
        ptsIndex.Add(3, 2);
        ptsIndex.Add(5, 3);
        ptsIndex.Add(10, 4);

        instance = this;

        startSpins = Login.userData.spins_disponiveis;

        UpdateText();
    }
	
	// Update is called once per frame
	public void UpdateText ()
    {
        jogadasTxt.text = Login.userData.spins_disponiveis.ToString("00");
        restaJogadasTxt.text = startSpins.ToString("00");
	}

    public void BtnPlay()
    {
        if(Login.userData.spins_disponiveis <= 0)
        {
            ServerControl.instance.PopUp("Você não possui jogadas disponíveis");
            return;
        }

        if (!inGame)
        {
            //para esperar resposta do servidor.
            offline = true;
            TurnRoletaWaiting();
            StartCoroutine(ServerControl.instance.IPlayRoleta());
        }

        UpdateText();

    }

    public void TurnRoletaWaiting()
    {
        inGame = true;

        Hashtable ht = new Hashtable();
        ht.Add("rotation", Vector3.forward * 720f);
        ht.Add("easetype", iTween.EaseType.linear);
        ht.Add("speed", speed);
        ht.Add("islocal", true);
        ht.Add("oncompletetarget", this.gameObject);

        if (offline)
            ht.Add("oncomplete", "TurnRoletaWaiting");
        else
            ht.Add("oncomplete", "TurnRoleta");

        iTween.RotateTo(roletaGO, ht);
    }

    public void TurnRoleta()
    {
        inGame = true;

        Hashtable ht = new Hashtable();
        ht.Add("rotation", (Vector3.forward * 720f) + (Vector3.forward * pointAngles[ptsIndex[points]]));
        ht.Add("easetype", easetype);
        ht.Add("time", time);
        ht.Add("islocal", true);
        ht.Add("oncompletetarget", this.gameObject);
        ht.Add("oncomplete", "TurnRoletaComplete");

        iTween.RotateTo(roletaGO, ht);
    }

    public void TurnRoletaComplete()
    {
        Debug.Log("Complete");
        StartCoroutine(ShowPopup());
    
        UpdateText();
    }

    private IEnumerator ShowPopup()
    {
        popups[ptsIndex[points]].SetActive(true);
        yield return new WaitForSeconds(popupTime);

        Hashtable ht = new Hashtable();
        ht.Add("scale", Vector3.one * 0.001f);
        ht.Add("easetype", iTween.EaseType.easeInBack);
        ht.Add("time", 0.7f);

        iTween.ScaleTo(popups[ptsIndex[points]], ht);

        yield return new WaitForSeconds(1f);

        iTween.Stop(popups[ptsIndex[points]]);

        popups[ptsIndex[points]].SetActive(false);
        popups[ptsIndex[points]].transform.localScale = Vector3.one;
        popups[ptsIndex[points]].transform.localPosition = Vector3.zero;
        inGame = false;
    }
}
