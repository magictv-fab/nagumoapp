﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using MagicTV.globals;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

    public static UserData userData = new UserData()
        {
#if UNITY_EDITOR
        nome = "Guilherme Sabino",
//            cpf = "32676057847",
			cpf = "39849025816",

            telefone = "11949429550",
            idade = 30,
            email = "uniteiro3d@gmail.com",
            senha = "1234",
            dt_nasc = "22/02/1987",
            pontos_disponiveis = 120,
            spins_disponiveis = 2,
            cupons_disponiveis = 5,
            foto = "teste.jpg",
            cep = "01540040"
#endif
    };

    public static bool isLogged = false;
    public static Texture2D photo_profile;

    public InputField loginInput, senhaInput;
    public GameObject loadingObj;
	public GameObject popup;

    public static InAppAnalytics _analytics;


    private bool back;

    void Awake()
    {
        if (PlayerPrefs.GetInt("login") == 1)
        {
            loginInput.text = PlayerPrefs.GetString("cpf");
            senhaInput.text = PlayerPrefs.GetString("senha");

            StartCoroutine(ILogin());

        } 
    }

	// Use this for initialization
	void Start () 
    {
        if(loginInput)
        {
            if (PlayerPrefs.GetString("cpf") != "")
            {
               // loginInput.text = PlayerPrefs.GetString("cpf");

            }
        }

        _analytics = InAppAnalytics.GetAnalytics("magic-tv-v-" + MagicApplicationSettings.AppVersionNumber, "UA-106553532-1");
        _analytics.init();
        _analytics.Open();
	}

	private void OnApplicationQuit()
	{
        _analytics.SendEvent("magictv", "stop");
        _analytics.Close();
         Debug.Log("Fechou");
	}

	// Update is called once per frame
	void Update () {
	
	}

    public void ButtonCupons()
    {
        if (isLogged)
            Application.LoadLevel("Cupons");
        else
            Application.LoadLevel("Login");
    }

    public void ButtonContas()
    {
		if (isLogged)
			Application.LoadLevel("Atualizar");
		else
			Application.LoadLevel("Login");        
    }

    public void BtnEsqueceuSenha()
    {
        
    }

	public void LoginButon()
	{
        if((loginInput.text == "miguel" || loginInput.text == "fabiano") && (senhaInput.text == "miguel" || senhaInput.text == "fabiano"))
        {
            GoLevel();
            return;
        }
		StartCoroutine(ILogin());
	}

	public void EsqueceuSenhaButon()
	{
        loginInput.text = loginInput.text.Replace(" ", "");

        Debug.Log(loginInput.text);

        if (loginInput.text == "")
        {
            PopUp("Digite seu cpf ou cnpj");
        }
        else
        {
            StartCoroutine(IEsqueceuSenha());
        }
	}

    private IEnumerator ISendJsonEsqueceuSenha()
    {

        if (loadingObj)
            loadingObj.SetActive(true);

        string json = "{\"cpf\":\"" + loginInput.text + "\"}";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/reenviaemail.php", pData, headers);

        yield return www;

        if (loadingObj)
            loadingObj.SetActive(false);

        if (!string.IsNullOrEmpty(www.error))
        {
            PopUp(www.error);
        }
        else
        {
            Debug.Log(www.text);
            PopUp(www.text);
        }

    }

    private IEnumerator ILogin()
    {
        loginInput.text = loginInput.text.Replace(" ","");

		string device_id = SystemInfo.deviceUniqueIdentifier;

		string mac_adress = Cadastro.GetMacAddress();

		string json = "{\"login\":\"" + loginInput.text + "\",\"senha\":\"" + Base64Encode(senhaInput.text) + "\",\"device_id\":\"" + device_id+  "\",\"mac_address\":\"" + mac_adress+ "\"}";

        Debug.Log("Json: " + json);

        Debug.Log("Fiz Request");

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/login.php", pData, headers);

        //WWW www = new WWW(ServerControl.url + "/login.php?"+ "login="+loginInput.text+"&senha="+ Base64Encode(senhaInput.text));

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if(!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
			if (loadingObj)
				loadingObj.SetActive(false);
		}
        else
        {
            Debug.Log(www.text);

            try
            {
                //[0] = code. [1] = message;
                string code = www.text.Split(',')[0];
                string message = www.text.Remove(0, code.Length + 1);
				Debug.Log(code);
				Debug.Log(message);

                if (code.Contains("200"))
                {
                    PlayerPrefs.SetInt("login", 1);
                    PlayerPrefs.SetString("cpf", loginInput.text);
                    PlayerPrefs.SetString("senha", senhaInput.text);
                    isLogged = true;

                    Debug.Log("Logado!");

                    //popula obj json.
                    userData = JsonUtility.FromJson<UserData>(message);
                    userData.senha = senhaInput.text;

					//Baixa a foto do server, se houver
					StartCoroutine(ILoadImgProfile(userData.foto));

					//Chamado dentro da Coroutine (Para esperar a requisição terminar)
//                    SceneManager.LoadScene("Menu_Nagumo");

                    // (PlayerPrefs.GetInt("primeiro_cadastro", 0) == 0 && (userData.rua == "" || userData.bairro == "" || userData.cidade == "" || userData.estado == ""))
                    if (userData.dt_nasc == "" || userData.sobrenome == "" || userData.email == "" || userData.telefone == "" || userData.sexo == 2 || userData.cep == "" || userData.bairro == "" || userData.cidade == "" || userData.estado == "")
                    {
                        //SceneManager.LoadScene("Atualizar", LoadSceneMode.Additive);
                    
                        //aqui na proxima tela aparece um popup e obriga o usuario a ir para a tela de atualizacao.
                        PlayerPrefs.SetInt("atualizeCadastro", 0);
                        PlayerPrefs.Save();
                    }    
                    //PlayerPrefs.SetInt("primeiro_cadastro", 1);
                    //PlayerPrefs.Save();


                }else{
                        PopUp(message);
					if (loadingObj)
						loadingObj.SetActive(false);
                }
           }
            catch
			 {
				 PopUp(www.text);
				 if (loadingObj)
					 loadingObj.SetActive(false);
			 }
		}
    }

	private IEnumerator ILoadImgProfile(string photoName)
	{
		//Se vier vazio o campo foto, abre direto a tela principal
		if(string.IsNullOrEmpty(photoName)){
			
			SceneManager.LoadScene("Menu_Nagumo");
			yield break;
		}

		//Passa a imagem do server para a Instancia do Game
		WWW www = new WWW(ServerControl.urlIMGProfile + "/" + photoName);
		yield return www;

		if (string.IsNullOrEmpty(www.error))
		{

			if(www.texture.width != 0 && www.texture.height != 0 )
			{

				Debug.Log("Baixado imagem do server");
				Debug.Log("Caminho: "+ ServerControl.urlIMGProfile + "/" + photoName);

				Login.photo_profile = www.texture;

				if (GetInfoMenu.instance)
				{
					if (GetInfoMenu.instance.fotoImg)
					{
						GetInfoMenu.instance.fotoImg.sprite = GetInfoMenu.CropImage(www.texture);
					}
				}					

			}else Debug.Log("User não tinha foto no server");

		}else{
			Debug.Log(www.error);
		}

		//Independente se baixou ou não, abre o App
		SceneManager.LoadScene("Menu_Nagumo");
	}




	private IEnumerator IEsqueceuSenha()
	{
		WWW www = new WWW(ServerControl.url + "/reenvia_senha.php?" + "login=" + loginInput.text);

		if (loadingObj)
			loadingObj.SetActive(true);

		yield return www;

		if (loadingObj)
			loadingObj.SetActive(false);

        if (!string.IsNullOrEmpty(www.error))
        {
            PopUp(www.error);
        }
        else
        {
            PopUp(www.text);
        }

	}

	public void PopUp(string txt)
	{
		Debug.Log("Popup: " + txt);

        if(txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }

		popup.GetComponentInChildren<Text>().text = txt;
		popup.SetActive(true);
	}

    public void ButtonLoadGame()
    {
        Application.LoadLevel("Game_Doce_Novembro_2018");
    }

	public void Close()
	{
		if (!back)
		{
			back = true;
			StartCoroutine(GoLevel());
		}
	}

	public IEnumerator GoLevel()
	{

		yield return new WaitForEndOfFrame();


		AsyncOperation async = Application.LoadLevelAsync("cena_load_main");


		yield return async;


	}

	public string Md5Sum(string strToEncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);

		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);

		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";

		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}

		return hashString.PadLeft(32, '0');
	}

	public static string Base64Encode(string plainText)
	{
		var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
		return System.Convert.ToBase64String(plainTextBytes);
	}
}
