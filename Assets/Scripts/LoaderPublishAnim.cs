﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class LoaderPublishAnim : MonoBehaviour {

    public static LoaderPublishAnim instance;

    public Image image;
    public float yMax = 448f;
    public GameObject container;
    public float multiplier = 2f;
    public UnityEvent onLoad;

    private bool isLoading;
    private Vector3 startPos, startDiff;

	// Use this for initialization
	void Start () 
    {
        instance = this;
        startPos = transform.localPosition;
        startDiff = startPos - container.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(transform.localPosition.y < yMax && !isLoading)
        {
            OnLoad();
        }

        if (isLoading)
        {
            transform.Rotate(Vector3.forward * 100f * Time.deltaTime);
        }
        else
        {
            transform.localPosition = startDiff + container.transform.localPosition * multiplier;

            float alpha = Mathf.InverseLerp(startPos.y, yMax, transform.localPosition.y);
            image.color = new Color(1f, 1f, 1f, alpha);
        }
	}

    public void OnLoad()
    {
        isLoading = true;

        transform.localPosition = Vector3.up * yMax;
        onLoad.Invoke();
        image.color = new Color(1f, 1f, 1f, 1f);
    }

    public void EndLoad()
    {
        isLoading = false;

        transform.localPosition = startPos;
        image.color = new Color(1f, 1f, 1f, 0.1f);
    }
}
