﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Cupons : MonoBehaviour {

    public Image container;
    public Sprite cupom1, cupom2, cupom3, cupom5, cupom10, cupom50, cupom100, cupom200, cupom500;
	public RawImage qrCodeImage;
    public int cupomValue;
	public Text codeText, dateText;
    public string code = "1234567890";
    public GameObject usedObj, expiredObj;
    public bool used;
    public Image barcodeSprite;
    public string dataExpira = "";
	private int cupomValueTabloid = 0;

	private QRCodeEncodeController e_qrController;

    // Use this for initialization
	void Start()
	{
        codeText.text = code;

        e_qrController = GetComponent<QRCodeEncodeController>();   
		e_qrController.onQREncodeFinished += QREncodeFinished;//Add Finished Event
		
        Encode();

        //se foi usado liga esse gameobject.
        usedObj.SetActive(used);

        switch(cupomValue)
        {
			case 1:
                container.sprite = cupom1;
				break;
			case 2:
                container.sprite = cupom2;
				break;
			case 3:
                container.sprite = cupom3;
				break;
			case 5:
                container.sprite = cupom5;
				break;

            case 10:
                container.sprite = cupom10;
				cupomValueTabloid = 10;
                break;

            case 50:
                container.sprite = cupom50;
				cupomValueTabloid = 50;
                break;

            case 100:
                container.sprite = cupom100;
				cupomValueTabloid = 100;
                break;

            case 200:
                container.sprite = cupom200;
				cupomValueTabloid = 200;
                break;

            case 500:
                container.sprite = cupom500;
				cupomValueTabloid = 500;
                break;
        }
    }

	public void SetOfertasTabloidByCupomValue(){
	    
		PlayerPrefs.SetInt("CupomTabloid", cupomValueTabloid);

	}

	// Update is called once per frame
	void Update()
	{

	}

	void QREncodeFinished(Texture2D tex)
	{
		if (tex != null && tex != null)
		{

			int width = tex.width;
			int height = tex.height;
			float aspect = width * 1.0f / height;
            //qrCodeImage.GetComponent<RectTransform>().sizeDelta = new Vector2(170, 170.0f / aspect);
            //qrCodeImage.texture = tex;
            barcodeSprite.sprite = Sprite.Create(tex, new Rect(0, 0, width, height), new Vector2(0.5f, 0.5f), 100);
		}
		else
		{

		}
	}

	public void Encode()
	{
		if (e_qrController != null)
		{
			string valueStr = code;
			int errorlog = e_qrController.Encode(valueStr);
			//infoText.color = Color.red;
			if (errorlog == -13)
			{
				//infoText.text = "Must contain 12 digits,the 13th digit is automatically added !";

			}
			else if (errorlog == -8)
			{
				//infoText.text = "Must contain 7 digits,the 8th digit is automatically added !";

			}
			else if (errorlog == -128)
			{
				//infoText.text = "Contents length should be between 1 and 80 characters !";
                //
			}
			else if (errorlog == -1)
			{
				//infoText.text = "Please select one code type !";
			}
			else if (errorlog == 0)
			{
				//infoText.color = Color.green;
				//infoText.text = "Encode successfully !";
			}

		}
	}
}
