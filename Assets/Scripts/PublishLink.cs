﻿using UnityEngine;
using System.Collections;
using System;

public class PublishLink : MonoBehaviour {

    public static string url;
    public string optionalUrl = "";
    private string htmlTxt = "<marquee><h3 style=\"text-align:center\"><span style=\"font-family: Menlo; color: #0000ff;\"> Sem&nbsp;conex&atilde;o&nbsp;com&nbsp;a&nbsp;internet.</span></h3><marquee>";
    public UniWebView uniWebView; 

	// Use this for initialization
	void Start () {
        //uniWebView.SetBackButtonEnabled(false);
        //uniWebView.LoadHTMLString(htmlTxt, url, true);

        if (optionalUrl != "")
            url = optionalUrl;

       StartCoroutine(checkInternetConnection((isConnected) => {
            uniWebView.gameObject.SetActive(true);
            if (isConnected)
                uniWebView.Load(url);
            else
                uniWebView.LoadHTMLString(htmlTxt, "", true);
        }));

    }
	
    IEnumerator checkInternetConnection(Action<bool> action)
    {
         WWW www = new WWW("http://google.com");
         yield return www;
         if (www.error != null) {
             action (false);
         } else {
             action (true);
         }
     } 
}
