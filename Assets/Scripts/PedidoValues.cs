﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PedidoValues : MonoBehaviour {

    public PedidoData pedido;   //para saber a posicao na lista na hora da revisao.
    public string nomeCarne, peso, tipoCarne, unidade, horarioData, info, codePedido;
    public int quantidade = 1;
    public Sprite sprite;
    public Text infoText;
    public Text titleText;
    public Image image;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void BtnExcluir()
    {
        PedidosRevisao.pedidosLst.Remove(pedido);
        Destroy(gameObject);
    }

}
