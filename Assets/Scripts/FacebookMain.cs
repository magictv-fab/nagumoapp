﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Facebook.Unity;

public class FacebookMain : MonoBehaviour {

    public static FacebookData facebookData;
    public static FacebookMain instance;

    private Texture2D sharerInitTexture;
    private string linkOferta, linkImg, title, info;

	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FacebookAccount()
    {
        if(!FB.IsInitialized)
            FB.Init(this.OnInitComplete, this.OnHideUnity);
        else if (!FB.IsLoggedIn)
            CallFBLogin();
        else
            GetInfo();  
            
    }

    private void CallFBLogin()
    {
        FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends", "user_birthday"}, this.HandleResult);
    }

    private void CallFBLoginForPublish()
    {
        // It is generally good behavior to split asking for read and publish
        // permissions rather than ask for them all at once.
        //
        // In your own game, consider postponing this call until the moment
        // you actually need it.
        //FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, this.HandleResult);
        FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends", "user_birthday" }, this.HandleResult);
    }

    private void CallFBLogout()
    {
        FB.LogOut();
    }

    private void OnInitComplete()
    {
        if (!FB.IsLoggedIn)
            CallFBLogin();
        else
            GetInfo();
    }

    private void OnInitCompleteSharer()
    {
        if (!FB.IsLoggedIn)
            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends", "user_birthday" }, this.HandleResultLoginSharerLink);
        else
            Sharer(sharerInitTexture);
    }

    private void OnInitCompleteSharerLink()
    {
        if (!FB.IsLoggedIn)
            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends", "user_birthday" }, this.HandleResultLoginSharerLink);
        else
            SharerLink(linkOferta, linkImg, title, info);
    }

    private void GetInfo()
    {
        FB.API("/me/?fields=email,name,birthday,gender", HttpMethod.GET, delegate (IGraphResult result) {
            if (string.IsNullOrEmpty(result.Error))
            {
                Debug.Log("Recebido: " + result.RawResult);
                facebookData = JsonUtility.FromJson<FacebookData>(result.RawResult);
                Debug.Log("Nome: " + facebookData.name);
                Application.LoadLevel("Cadastro");
            }
            else
            {
                Debug.LogWarning("received error=" + result.Error);
            }
        });        
    }

    private void OnHideUnity(bool isGameShown)
    {

    }

    protected void HandleResult(IResult result)
    {
        if (string.IsNullOrEmpty(result.Error))
        {
            GetInfo();
        }
        else
        {
            Debug.LogWarning("received error=" + result.Error);
        }
    }

    public void SharerLink(string linkOferta, string linkImg, string title, string info)
    {
        Debug.Log("linkOferta: " + linkOferta);
        Debug.Log("linkImg: " + linkImg);
        Debug.Log("title: " + title);
        Debug.Log("info: " + info);

        if(FB.IsLoggedIn)
        {
            FB.ShareLink(new System.Uri(linkOferta), title, info, new System.Uri(linkImg),(IShareResult result) => {
                if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
                {
                    Debug.Log("ShareLink Error: " + result.Error);
                }
                else if (!string.IsNullOrEmpty(result.PostId))
                {
                    // Print post identifier of the shared content
                    Debug.Log(result.PostId);
                }
                else
                {
                    // Share succeeded without postID
                    Debug.Log("ShareLink success!");
                    StartCoroutine(ISendJsonEnviaSeCompartilhou());
                } 
            });
        }
        else
        {
            this.linkImg = linkImg;
            this.linkOferta = linkOferta;
            this.title = title;
            this.info = info;

            FB.Init(this.OnInitCompleteSharerLink, this.OnHideUnity);
        }
    }

    private IEnumerator ISendJsonEnviaSeCompartilhou()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW("http://dashboard-magictv.com.br/nagumoplay/webservices/bonificacao_pontos/compartilhamentos_publicacoes.php", pData, headers);

        yield return www;


        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError(www.error);
        }
        else
        {
            Debug.Log(www.text);
        }

    }

    public void Sharer(Texture2D texture)
    {
        if (FB.IsInitialized)
        {
            byte[] dataToSave = texture.EncodeToPNG();

            var wwwForm = new WWWForm();
            wwwForm.AddBinaryData("image", dataToSave, "Promocao_Nagumo.png");

            FB.API("me/photos", HttpMethod.POST, HandleResultSharer, wwwForm);
        }
        else
        {
            sharerInitTexture = texture;
            FB.Init(this.OnInitCompleteSharer, this.OnHideUnity);

        }
    }

    protected void HandleResultSharer(IResult result)
    {
        if (string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("HandResultSharer: " + result.RawResult);
        }
        else
        {
            Debug.LogWarning("received error=" + result.Error);
        }
    }

    protected void HandleResultLoginSharer(IResult result)
    {
        if (string.IsNullOrEmpty(result.Error))
        {
            Sharer(sharerInitTexture);

            Debug.Log("HandResultSharer: " + result.RawResult);
        }
        else
        {
            Debug.LogWarning("received error=" + result.Error);
        }
    }

    protected void HandleResultLoginSharerLink(IResult result)
    {
        if (string.IsNullOrEmpty(result.Error))
        {
            SharerLink(linkOferta, linkImg, title, info);

            Debug.Log("HandResultSharerLink: " + result.RawResult);
        }
        else
        {
            Debug.LogWarning("received error=" + result.Error);
        }
    }
}

public class FacebookData
{
    public string name;
    public string id;
    public string email;
    public string birthday;
    public string gender;
}
