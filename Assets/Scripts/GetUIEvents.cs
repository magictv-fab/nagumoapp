﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class GetUIEvents : MonoBehaviour, IPointerClickHandler {

    public UnityEvent onPointerClick;

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnPointerClick(PointerEventData tst) 
    {
        onPointerClick.Invoke();
    }
}
