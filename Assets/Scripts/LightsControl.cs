﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightsControl : MonoBehaviour {

    public List<GameObject> lights = new List<GameObject>();
    public float velocity = 1f;
    public Color lightOnColor = Color.yellow;
    public Color emissionColor;
    public List<Color> emissionColorLst = new List<Color>();

    private Color startMaterialColor, startEmissionColor;

    private void Awake()
    {
		startMaterialColor = lights[0].GetComponent<Renderer>().material.color;
        startEmissionColor = lights[0].GetComponent<Renderer>().material.GetColor("_EmissionColor");
	}

    private void Start()
    {
        Turn();                 
    }

    public void Turn()
    {
        Stop();
		StartCoroutine(ITurn());

	}

	public void BlinkAll(int repeat = 1)
	{
		Stop();
        StartCoroutine(IBlinkAll(repeat));

	}

    private IEnumerator ITurn () 
    {
        for (int i = 0; i < emissionColorLst.Count; i++)
        {
            foreach (var obj in lights)
            {
                Reset();
                obj.GetComponent<Renderer>().material.color = lightOnColor;
                obj.GetComponent<Renderer>().material.SetColor("_EmissionColor", emissionColorLst[i]);
                yield return new WaitForSeconds(velocity);
            }
        }
		StartCoroutine(ITurn());

	}

    public void Stop()
    {
        StopAllCoroutines();
    }

    public void LoadLights(float value)
    {
        Stop();
        Reset();

        float percent = Mathf.Lerp(0, lights.Count, value);

        for (int i = 0; i < percent; i++)
        {
            lights[i].GetComponent<Renderer>().material.color = lightOnColor;
			lights[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", emissionColor);
		}
    }

    public IEnumerator IBlinkAll(int repeat = 1)
	{

        for (int i = 0; i < repeat; i++)
        {
            foreach (var obj in lights)
            {
                obj.GetComponent<Renderer>().material.color = lightOnColor;
				obj.GetComponent<Renderer>().material.SetColor("_EmissionColor", emissionColor);
			}

            yield return new WaitForSeconds(velocity);

            Reset();
			yield return new WaitForSeconds(velocity);

		}

        Turn();
	}

    public void Reset()
    {
		foreach (var obj in lights)
		{
			obj.GetComponent<Renderer>().material.color = startMaterialColor;
            obj.GetComponent<Renderer>().material.SetColor("_EmissionColor", startEmissionColor);
		}        
    }

	// Update is called once per frame
	void Update () {
	
	}
}
