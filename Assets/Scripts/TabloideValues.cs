﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TabloideValues : MonoBehaviour {

    public string id;
    public string txt;
    public string link;
    public Sprite spt;
    public Text txtTxt;
    public Image imgImg;

	// Use this for initialization
	void Start () 
    {
        imgImg.sprite = spt;
        txtTxt.text = txt;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GoLink()
    {
        if (string.IsNullOrEmpty(link))
            return;

       // string newLink = link.Contains("http://") ? link : "http://" + link;
        Application.OpenURL(link);
    }

}
