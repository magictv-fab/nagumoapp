﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GetInfoMenu : MonoBehaviour 
{
    public static GetInfoMenu instance;
    public Image fotoImg;
    public Text nomeTxt, girosTxt, pontosTxt, cuponsTxt;

    // Use this for initialization
    void Start()
    {
        instance = this;

        if (Login.isLogged)
        {
            if (Login.userData != null)
            {
                string nome = Login.userData.nome;

                if (Login.userData.nome.Contains(" "))
                    nome = Login.userData.nome.Split(' ')[0];

                nomeTxt.text = nome;
                pontosTxt.text = Login.userData.pontos_disponiveis.ToString();
                cuponsTxt.text = Login.userData.cupons_disponiveis.ToString();
                girosTxt.text = Login.userData.spins_disponiveis.ToString();

                //inicia download da foto se ja nao tiver baixado.
                if (Login.photo_profile == null && !string.IsNullOrEmpty(Login.userData.foto))
                    StartCoroutine(ILoadImg());
                else
                {
                    fotoImg.sprite = CropImage(Login.photo_profile);
                }
            }
        }
    }
	
    private IEnumerator ILoadImg()
    {
        WWW www = new WWW(ServerControl.urlIMGProfile + "/" + Login.userData.foto);

        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Login.photo_profile = www.texture;
            //fotoImg.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
            fotoImg.sprite = CropImage(www.texture);
        }
        else
        {
            Debug.Log("Erro ao baixar foto");
        }
    }

    public static Sprite CropImage(Texture2D texture)
    {
        int min = Mathf.Min(texture.width, texture.height);
        int max = Mathf.Max(texture.width, texture.height);

        int startX = min != texture.width?(max - min) / 2:0;
        int startY = min != texture.height?(max - min) / 2:0;

        return Sprite.Create(texture, new Rect(startX,startY,min,min), Vector2.zero); 
    }
}
