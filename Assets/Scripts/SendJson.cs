﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SendJson : MonoBehaviour {
    
    public InputField jsonInput;
    public Text resp;

    private string url = "";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SendJsonBtn()
    {
        StartCoroutine(ISendJson());   
    }

    private IEnumerator ISendJson()
    {
        string json = jsonInput.text;
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		byte[] pData = System.Text.Encoding.ASCII.GetBytes(json.ToCharArray());

		WWW www = new WWW("http://magictv.hospedagemdesites.ws/nagumoplay/webservices/magicjson.php", pData, headers);

		yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.text);
            resp.text = www.text;
        }
        else
        {
            Debug.Log(www.error);
            resp.text = www.error;
        }
    }
}
