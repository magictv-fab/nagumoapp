﻿using UnityEngine;
using System.Collections;

public class DestruidorDeCubo : MonoBehaviour {
    public float TimeToDie = 3f;

    float timeLived = 0f;
	// Use this for initialization
	void Start () {
        timeLived = 0f;
	}
	
	// Update is called once per frame
	void Update () {
        timeLived += Time.deltaTime;
        if (timeLived >= TimeToDie)
        {
            DestroyImmediate(gameObject);
        }
	}
}
