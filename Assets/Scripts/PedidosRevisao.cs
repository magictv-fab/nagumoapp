﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class PedidosRevisao : MonoBehaviour {

    public static List<PedidoData> pedidosLst = new List<PedidoData>();
    public static string idPedido;

    public GameObject content;
    public GameObject itemPrefab;
    public GameObject loadingObj;
    public GameObject popup;
    public GameObject popupPassarPeso;

	// Use this for initialization
	void Start () 
    {
        //instancia os pedidos criados e inseridos neessa variavel estatica q armazena os pedidos.
        foreach(PedidoData pedidoData in pedidosLst)
        {
            var obj = Instantiate(itemPrefab);
            obj.transform.parent = content.transform;
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localScale = Vector3.one;
            obj.transform.SetAsFirstSibling();

            PedidoValues pValues = obj.GetComponent<PedidoValues>();
            pValues.nomeCarne = pedidoData.nome;
            pValues.info = pedidoData.info;
            pValues.sprite = pedidoData.sprite;
            pValues.pedido = pedidoData;
            pValues.image.sprite = pedidoData.sprite;
            pValues.titleText.text = pedidoData.nome;
            pValues.infoText.text = pedidoData.info;
        }
	}


    public void BtnAvancar()
    {
        if (pedidosLst.Count > 0)
        {
            popupPassarPeso.SetActive(true);
        }
    }

    public void AceitaPassarPeso(bool aceitaPassarPeso)
    {
        StartCoroutine(IEnviarPedido(aceitaPassarPeso));
    }

    private IEnumerator IEnviarPedido(bool aceitaPassarPeso)
    {
        PedidoEnvioData pedidoEnvioData = new PedidoEnvioData();
        pedidoEnvioData.pedidos = pedidosLst.ToArray();
        pedidoEnvioData.loja = MagicTV.in_apps.InAppManager.lojaCode;
        pedidoEnvioData.nota = 0;
        pedidoEnvioData.login = Login.userData.cpf;
        pedidoEnvioData.senha = Login.Base64Encode(Login.userData.senha);
        pedidoEnvioData.passarPeso = aceitaPassarPeso;

        string json = JsonUtility.ToJson(pedidoEnvioData);

        Debug.Log("Json Envio Pedido: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW("http://dashboard-magictv.com.br/nagumoplay/webservices/acougue/salvar_pedido.php", pData, headers);

        if (loadingObj)
           loadingObj.SetActive(true);

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log("Resp: " + www.text);
            try
            {
                string message = SimpleJSON.EscapeString(www.text);
                message = message.Replace("200,{'id_pedido':'", "").Replace("'}", "");

                Debug.Log(message);
                idPedido = message;

                //MeuPedidoAcougue.pedidosLst = new List<PedidoData>(pedidosLst);
                //quando envia limpa a lista e envia ela para a tela meupedidoacougue.
                pedidosLst.Clear();

                //coloca o codigo recebido no pedido.
                pedidoEnvioData.codePedido = idPedido;
                MeuPedidoAcougue.pedidoEnvioDatasLst.Add(pedidoEnvioData);

                Application.LoadLevel("Acougue_Confirmacao");

            }
            catch (System.Exception ex)
            {
                PopUp(ex.ToString());
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
        }
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);

        popup.GetComponentInChildren<Text>().text = txt;
        popup.SetActive(true);
    }
}
