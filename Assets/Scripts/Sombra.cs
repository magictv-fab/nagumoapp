﻿using UnityEngine;
using System.Collections;

public class Sombra : MonoBehaviour 
{
    public GameObject target;
    public float bias = 1f;
    private bool ok;

	// Use this for initialization
    IEnumerator Start () {
        yield return new WaitForSeconds(4f);
        ok = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (ok)
            return;
        
        float dist = Vector3.Distance(transform.position, target.transform.position);
        transform.localScale = Vector3.one * dist * bias;
	}
}
