﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MagicTV.globals.events;
using MagicTV.globals;
using MagicTV.processes;

public class TesteApresentacoes : MonoBehaviour {
	protected string[] tracksIds;
	public List<string> TrackingsIds;
	public List<string> Labels;
	protected bool _hasTracks;

	string currentWindow = "root";

	// Use this for initialization
	void Start () {
		this.tracksIds = Labels.ToArray ();
		this._hasTracks = (tracksIds.Length > 0 && Labels != null && Labels.Count == TrackingsIds.Count);
	}
	
	// Update is called once per frame
	void OnGUI () {
		if(!this._hasTracks){
			return;
		}
		if(PresentationController.Instance == null){
			return;
		}
		if(this.currentWindow == "root"){
			this.showAllButtonsTracks ();
			return ;
		}
		this.showBackButton ();
	}

	void showBackButton ()
	{
		if( GUI.Button ( new Rect(50,10,230,80), "GO BACK")){
			PresentationController.Instance.StopLastPresentation();
			this.currentWindow = "root";
		}
	}

	void showAllButtonsTracks ()
	{
		GUILayout.BeginArea (new Rect(10, 100, Screen.width-20, Screen.height-200));
		int selected = GUILayout.SelectionGrid (-1 , tracksIds , 3);
		if(selected >= 0){
			this.currentWindow = TrackingsIds[selected];
			PresentationController.Instance.OnTrackingFound(TrackingsIds[selected]);

		}
		GUILayout.EndArea ();
	}
}
