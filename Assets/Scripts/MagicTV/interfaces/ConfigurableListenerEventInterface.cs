﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Configurable listener event interface.
/// Interface para qualquer classe que seja configuravel por string
/// 
/// Na prática, no magicTV basta implementar essa classe, que é possível configurar-se via metadata
/// </summary>
public interface ConfigurableListenerEventInterface{

	string getUniqueName();
	void configByString (string metadata);
	void Reset ();
	GameObject getGameObjectReference();
}
