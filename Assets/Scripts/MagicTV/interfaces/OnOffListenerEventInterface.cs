﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Configurable listener event interface.
/// Interface para qualquer classe que seja configuravel por string
/// 
/// Na prática, no magicTV basta implementar essa classe, que é possível configurar-se via metadata
/// </summary>
public interface OnOffListenerEventInterface{

	string getUniqueName();
	void turnOn ();
	void turnOff ();
	void ResetOnOff ();
	bool isActive();
	GameObject getGameObjectReference();
}
