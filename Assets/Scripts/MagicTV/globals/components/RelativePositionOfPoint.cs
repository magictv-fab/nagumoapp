﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @author : Renato Miawaki
/// @version : 1.0
/// 
/// Relative position of point.
/// Otimo para ser utilizado com zoom de camera para encontrar a configuração do zoom
/// </summary>
public class RelativePositionOfPoint : MonoBehaviour , ConfigurableListenerEventInterface{
	/// <summary>
	/// The reference internal point. Precisa ser um objeto interno.
	/// </summary>
	public GameObject referenceInternalPoint;
	protected static RelativePositionOfPoint Instance;
	public static RelativePositionOfPoint GetInstance(){
		return Instance;
	}

	public Vector3 DebugSetRelativePoint;
	public float DebugPercentDistance;
	public Vector3 DebugGlobalPointResult;
	public bool DebugCalcNow ;

	void Update(){
		if(DebugCalcNow){
			DebugCalcNow = false ;
			DebugGlobalPointResult = GetRelativePoint(DebugSetRelativePoint, DebugPercentDistance);
		}
	}
	// Use this for initialization
	void Start () {
		Instance = this;
		//Caso não passe um obj de referencia, ele cria um
		if(referenceInternalPoint == null){
			referenceInternalPoint = new GameObject("referenceInternalPoint");
			referenceInternalPoint.transform.parent = this.transform;
		}
	}
	public Vector3 DebugRecivedVector;
	/// <summary>
	/// Gets the relative point.
	/// distance = 1 the same point of pivot
	/// distance = 0 the same point of myPoint
	/// distance = -1 get back the same distance of pivot 
	/// </summary>
	/// <returns>The relative point.</returns>
	/// <param name="myPoint">My point.</param>
	/// <param name="distance">Distance.</param>
	public Vector3 GetRelativePoint( Vector3 myPoint, float percentDistance ){
		DebugRecivedVector = myPoint;
		//pega a distancia total 
		float totalCurrentDistance = Vector3.Distance (myPoint, this.transform.position);
		//agora encontra a relação para usar na regra de 3
//		totalCurrentDistance = 100f;
		this.transform.LookAt (myPoint);


        //TODO: Double Invert para manter a compatiblilidade das apresentacoes que estao no ar
        //linha abaixo dever'a ser removida e a proxima descomentada
        //this.setDistanceOfReferencePoint ((1-percentDistance)*totalCurrentDistance);
        this.setDistanceOfReferencePoint(percentDistance * totalCurrentDistance);

        return this.referenceInternalPoint.transform.position;
	}
	public float DebugPercent ;
	/// <summary>
	/// Sets the distance of reference point.
	/// </summary>
	/// <param name="f">F.</param>
	void setDistanceOfReferencePoint (float f)
	{
		DebugPercent = f;
		//sempre vai em direção ao forward, aumenta o z
		this.referenceInternalPoint.transform.localPosition = new Vector3(0,0, f);
	}
	/// <summary>
	/// Envie o ponto considerado como pivot, o ponto de relaçao e passe a distancia em porcentagem de 0 a 1 para ter de volta o ponto relativo
	/// Gets the relative point of pivot.
	/// </summary>
	/// <returns>The relative point of pivot.</returns>
	/// <param name="pivotPoint">Pivot point.</param>
	/// <param name="myPoint">My point.</param>
	/// <param name="percentDistance">Percent distance.</param>
	public Vector3 GetRelativePointOfPivot( Vector3 pivotPoint, Vector3 myPoint, float percentDistance ){
		this.transform.position = pivotPoint;
		return GetRelativePoint (myPoint, percentDistance);
	}


	public string getUniqueName(){
		return this.gameObject.name;
	}
	public void configByString (string metadata){

	}
	public void Reset (){

	}
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}

}
