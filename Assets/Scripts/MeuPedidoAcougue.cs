﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MeuPedidoAcougue : MonoBehaviour {

    public static List<PedidoEnvioData> pedidoEnvioDatasLst = new List<PedidoEnvioData>();

    public GameObject content;
    public GameObject itemPrefab;
    public GameObject loadingObj;
    public GameObject popup;
    public GameObject pedidoRealizado, pedidoEmProducao, pedidoPronto;
    public Text textPosicao;

    private List<PedidoData> pedidosAvaliarLst = new List<PedidoData>();

    // Use this for initialization
    void Start()
    {
        //instancia os pedidos criados e inseridos neessa variavel estatica q armazena os pedidos.
        if(false)
        foreach (PedidoEnvioData pedidoEnvioData in pedidoEnvioDatasLst)
        {
            var obj = Instantiate(itemPrefab);
            obj.transform.parent = content.transform;
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localScale = Vector3.one;
            obj.transform.SetAsFirstSibling();

            PedidoValues pValues = obj.GetComponent<PedidoValues>();
            pValues.titleText.text = pedidoEnvioData.codePedido;

            string info = "";

            foreach (PedidoData pedidoData in pedidoEnvioData.pedidos)
            {
                info += pedidoData.info + " \n";
            }

            pValues.infoText.text = info;

            pedidoRealizado.SetActive(true);
        }

        StartCoroutine(ICheckPedido());
    }

    private IEnumerator ICheckPedido(bool isUpdate = false)
    {
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";

        Debug.Log("Json Envio Pedido: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW("http://dashboard-magictv.com.br/nagumoplay/webservices/acougue/meu_pedido.php", pData, headers);

        //if (loadingObj)
        //    loadingObj.SetActive(true);

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log("Resp: " + www.text);
            try
            {
                string message = SimpleJSON.EscapeString(www.text);

                MeuPedidoData meuPedidoData = JsonUtility.FromJson<MeuPedidoData>(message);
                textPosicao.text = meuPedidoData.posicao.ToString();

                int avaliou = 0;
                Dictionary<int, int> statusDict = new Dictionary<int, int>();


                foreach (PedidoData pedidoData in meuPedidoData.pedidos)
                {
                    string info = "";

                    //se o pedido ainda n foi entregue ou cancelado aparece.
                    if (pedidoData.status_pedido < 3)
                    {
                        foreach (CarnesData carneData in pedidoData.carnes)
                        {
                            info += "\nTipo de carne: " + carneData.nome_produto + "\n";
                            info += "Peso: " + carneData.peso + "\n";
                            info += "Tipo de corte: " + carneData.corte + "\n";
                            info += "Unidade: " + pedidoData.nome_loja + "\n";

                            if(pedidoData.carnes.Length > 1)
                            {
                                if(pedidoData.carnes[pedidoData.carnes.Length - 1] != carneData)
                                {
                                    info += "............................................................................................\n\n";
                                }
                            }
                        }

                        //info = info.Remove(info.Length - 1);

                        if(!isUpdate)
                            InstantiateObj("#" + pedidoData.id_pedido, info);

                    }
                    else
                    {
                        avaliou = pedidoData.avaliou;
                        Debug.Log("avaliou> " + avaliou);
                    }

                    //adiciona o status do pedido e se ele o avaliou.
                    if(!statusDict.ContainsKey(pedidoData.status_pedido))
                        statusDict.Add(pedidoData.status_pedido, pedidoData.avaliou);
                    else
                        statusDict[pedidoData.status_pedido] = pedidoData.avaliou;


                    if(pedidoData.avaliou == 0)
                    {
                        pedidoData.idPedido = pedidoData.id_pedido;

                        if(!pedidosAvaliarLst.Contains(pedidoData))
                            pedidosAvaliarLst.Add(pedidoData);
                    }
                }

                //status dos pedidos
                pedidoRealizado.SetActive(false);
                pedidoPronto.SetActive(false);
                pedidoEmProducao.SetActive(false);


                //verifica quem esta como entregue e nao avaliou ainda.
                if (statusDict.ContainsKey(0))
                {
                    pedidoRealizado.SetActive(true);
                }
                else if (statusDict.ContainsKey(1))
                {
                    pedidoEmProducao.SetActive(true);
                }
                else if (statusDict.ContainsKey(2))
                {
                    textPosicao.text = "";
                    pedidoPronto.SetActive(true);
                }
                else if (statusDict.ContainsKey(3))
                {
                    //significa q esta entregue e ele nao o avaliou ainda.
                    if (statusDict[3] == 0)
                    {
                        AcougueAvaliacao.pedidoDataLst = new List<PedidoData>(pedidosAvaliarLst);
                        if (loadingObj)
                            loadingObj.SetActive(true);
                        Application.LoadLevel("Acougue_avaliacao");
                    }
                }

            }
            catch (System.Exception ex)
            {
                PopUp(ex.ToString());
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
        }

        loadingObj.SetActive(false);

        yield return new WaitForSeconds(30f);

        StartCoroutine(ICheckPedido(true));
    }

    private IEnumerator IConfirmarRetirada()
    {
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";

        Debug.Log("Json Envio Pedido: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW("http://dashboard-magictv.com.br/nagumoplay/webservices/acougue/confirma_retirada.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log("Resp: " + www.text);
            AcougueAvaliacao.pedidoDataLst = new List<PedidoData>(pedidosAvaliarLst);
            Application.LoadLevel("Acougue_avaliacao");   
        }
    }

    public void ButtonAvaliar()
    {
        StartCoroutine(IConfirmarRetirada());
    }

    public void InstantiateObj(string code, string info)
    {
        var obj = Instantiate(itemPrefab);
        obj.transform.parent = content.transform;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;
        obj.transform.SetAsFirstSibling();

        PedidoValues pValues = obj.GetComponent<PedidoValues>();
        pValues.titleText.text = code;
        pValues.infoText.text = info;
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);

        popup.GetComponentInChildren<Text>().text = txt;
        popup.SetActive(true);
    }
}
