﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AcougueAvaliacao : MonoBehaviour {

    public static List<PedidoData> pedidoDataLst = new List<PedidoData>();

    public Text codePedido, infoPedido;
    public GameObject content;
    public GameObject itemPrefab;
    public GameObject loadingObj;
    public GameObject popup;
    public List<GameObject> estrelaLst = new List<GameObject>();

	// Use this for initialization
	void Start () {
        foreach (PedidoData pedidoData in pedidoDataLst)
        {
            string info = "";

            foreach (CarnesData carneData in pedidoData.carnes)
            {
                info += "Tipo de carne: " + carneData.nome_produto + "\n";
                info += "Peso: " + carneData.peso + "\n";
                info += "Tipo de corte: " + carneData.corte + "\n";
                info += "Unidade: " + pedidoData.nome_loja + "\n";
            }

            info = info.Remove(info.Length - 1);

            codePedido.text = "#" + pedidoData.id_pedido;
            infoPedido.text = info;

            //InstantiateObj("#" + pedidoData.id_pedido, info);
        }
	
	}

    public void InstantiateObj(string code, string info)
    {
        var obj = Instantiate(itemPrefab);
        obj.transform.parent = content.transform;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;
        obj.transform.SetAsFirstSibling();

        PedidoValues pValues = obj.GetComponent<PedidoValues>();
        pValues.titleText.text = code;
        pValues.infoText.text = info;
    }

    public void AvaliarPedido()
    {
        StartCoroutine(IAvaliarPedido());
    }

    private IEnumerator IAvaliarPedido()
    {
        PedidoEnvioData pedidoEnvioData = new PedidoEnvioData();
        pedidoEnvioData.pedidos = pedidoDataLst.ToArray();
        pedidoEnvioData.login = Login.userData.cpf;
        pedidoEnvioData.senha = Login.Base64Encode(Login.userData.senha);
        pedidoEnvioData.loja = MagicTV.in_apps.InAppManager.lojaCode;

        pedidoEnvioData.nota = 0;

        foreach(GameObject go in estrelaLst)
        {
            if (go.activeSelf)
                pedidoEnvioData.nota++;
        }

        string json = JsonUtility.ToJson(pedidoEnvioData);

        Debug.Log("Json Envio Pedido Avaliacao: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW("http://dashboard-magictv.com.br/nagumoplay/webservices/acougue/avaliacao_pedido.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log("Resp Avaliacao: " + www.text);
            try
            {
                string message = SimpleJSON.EscapeString(www.text);

                //AvaliacaoRespData avaliacaoRespData = JsonUtility.FromJson<AvaliacaoRespData>(message);

                //PopUp(avaliacaoRespData.resposta);

                Debug.Log(message);
                Application.LoadLevel("Menu_Nagumo");


            }
            catch (System.Exception ex)
            {
                PopUp(ex.ToString());
                if (loadingObj)
                    loadingObj.SetActive(false);
            }

           // yield return new WaitForSeconds(5f);

           // Application.LoadLevel("Acougue_Pedido");
        }
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);

        popup.GetComponentInChildren<Text>().text = txt;
        popup.SetActive(true);
    }
}

[System.Serializable]
public class AvaliacaoRespData{
    public string resposta;
}
