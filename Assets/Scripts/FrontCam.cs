﻿using UnityEngine;
using System.Collections;

public class FrontCam : MonoBehaviour {

	public WebCamTexture mCamera = null;
	public float bias = 0.1f;



	public GameObject frase, btnCam, btnFechar;

	private bool back;

	WebCamDevice[] devices;


	// Take a shot immediately
	IEnumerator Start()
	{
		//		openCamera();
		StartCoroutine (openCamera());
		yield return null;
	}


	IEnumerator openCamera(){

		// We should only read the screen after all rendering is complete
		yield return new WaitForEndOfFrame();

		try{


			WebCamDevice device = WebCamTexture.devices[1];
			mCamera = new WebCamTexture(device.name);
			gameObject.GetComponent<Renderer>().material.mainTexture = mCamera;
			mCamera.Play();


		}catch{

			Debug.Log("Erro ao abrir camera frontal ... abrindo a de trás");

			WebCamDevice device = WebCamTexture.devices[0];
			mCamera = new WebCamTexture(device.name);
			gameObject.GetComponent<Renderer>().material.mainTexture = mCamera;
			mCamera.Play();
			
		}





		#if UNITY_IOS
		transform.localEulerAngles = new Vector3(0,0,-90);
		#endif






	}


	public void BackButton()
	{
		mCamera.Stop();
		Close();
	}

	public void Close()
	{
		if (!back)
		{
			back = true;
			StartCoroutine(GoLevel());
		}
	}

	public void OnDisable()
	{
		mCamera.Stop();
	}

	public void OnDestroy()
	{
		mCamera.Stop();
	}

	public IEnumerator GoLevel()
	{

		mCamera.Stop();

		yield return new WaitForEndOfFrame();


		AsyncOperation async = Application.LoadLevelAsync("Menu_Nagumo");


		yield return async;


	}

	public void hideHUD(){
		frase.SetActive(false);
		btnCam.SetActive(false);
		btnFechar.SetActive(false);
	}
	public void showHUD(){
		frase.SetActive(true);
		btnCam.SetActive(true);
		btnFechar.SetActive(true);
	}

	public IEnumerator RemoveHUD()
	{

		hideHUD();
		yield return new WaitForSeconds(3f);
		showHUD();
	}

	public void fakeScreenShot(){
		StartCoroutine(RemoveHUD());
		mCamera.Pause();

	}


}
