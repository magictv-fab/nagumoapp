﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public class TabloideManager : MonoBehaviour 
{
    public ScrollRect scrollRect;
    public static TabloidesData tabloidesData;
    public static List<string> textoLst = new List<string>(), linkLst = new List<string>();
    public static List<Sprite> imgLst = new List<Sprite>();
    public GameObject loadingObj, popup;
    public GameObject itemPrefab, containerItens;
    public GameObject currentItem;
    public int loadUtil = 10;

	public int precoCupom = 10;

    private int objCounter;
    private static string currentJsonTxt;
    private int imgLoadedCount;

    private void Start()
    {

		precoCupom = PlayerPrefs.GetInt("CupomTabloid");

        StartCoroutine(ILoadPublish());

    }

    public void GetScrollPos(Vector2 pos)
    {
        //Debug.Log(pos);
        if (pos.y < 0 && !loadingObj.activeSelf && imgLst.Count < tabloidesData.tabloides.Length)
        {
            Debug.Log("Carrega imagens");
            loadingObj.SetActive(true);
            LoadImgs();
        }
    }

    public void InstantiateNextItem()
    {

        var obj = Instantiate(itemPrefab);
        obj.transform.SetParent(containerItens.transform, false);
        obj.transform.localScale = Vector3.one * 0.87f;
        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


        TabloideValues tabloideValues = obj.GetComponent<TabloideValues>();

        tabloideValues.txt = textoLst[objCounter];
//		tabloideValues.txt = "";

        tabloideValues.spt = imgLst[objCounter];
        tabloideValues.link = linkLst[objCounter];

        //obj.transform.SetAsFirstSibling();

		string valor = tabloideValues.txt;

		string resultString = Regex.Match(valor, @"\d+").Value;

		print(resultString);



		int valorItem = int.Parse(resultString);

		if(valorItem >= precoCupom){
			Debug.Log("Destroy: " + tabloideValues.txt);
			//Destroy(obj);
			obj.SetActive(false);
		}

		tabloideValues.txt = " ";

        currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

        objCounter++;

    }

    private IEnumerator ILoadPublish()
    {
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_tabloides.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            //verifica se o txt que chegou eh diferente do que tem ja.
            if (currentJsonTxt != www.text)
            {
                Debug.Log("Tem atualizacao de Ofertas!");
                currentJsonTxt = www.text;

                //Atualiza:
                textoLst.Clear();
                imgLst.Clear();
                linkLst.Clear();

                objCounter = 0;

                try
                {
                    string message = www.text;
                    //   Debug.Log(message);

                    //popula obj json.
                    tabloidesData = JsonUtility.FromJson<TabloidesData>(message);

                    //passa as informacoes para as listas.
                    foreach (TabloideData tabloideData in tabloidesData.tabloides)
                    {
                        textoLst.Add("R$ " + tabloideData.valor.ToString(".00"));
                        linkLst.Add(tabloideData.link);
                    }

                    StartCoroutine(ILoadImgs());
                }
                catch
                {
                    PopUp(www.text);
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
            else
            {
                StartCoroutine(LoadLoadeds());
            }
        }

    }

    public string DecodeFromUtf8(this string utf8String)
    {
        // copy the string as UTF-8 bytes.
        byte[] utf8Bytes = new byte[utf8String.Length];
        for (int i = 0; i < utf8String.Length; ++i)
        {
            //Debug.Assert( 0 <= utf8String[i] && utf8String[i] <= 255, "the char must be in byte's range");
            utf8Bytes[i] = (byte)utf8String[i];
        }

        return Encoding.UTF8.GetString(utf8Bytes, 0, utf8Bytes.Length);
    }

    private IEnumerator ILoadImgs()
    {

        //foreach (TabloideData tabloideData in tabloidesData.tabloides)
        for (int i = 0; i < loadUtil; i ++)
        {
            if (tabloidesData.tabloides.Length < imgLoadedCount + i)
                break;
            
            WWW www = new WWW(ServerControl.urlIMGTabloides + "/" + tabloidesData.tabloides[imgLoadedCount].imagem);
                
            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                Sprite spt = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
                imgLst.Add(spt);
            }

            Debug.Log("Baixou imagem: " + tabloidesData.tabloides[imgLoadedCount].imagem);

            //conta o total de imagens carregadas.
            imgLoadedCount++;
        }

        StartCoroutine(LoadLoadeds());
    }

    public void LoadImgs()
    {
        StartCoroutine(ILoadImgs());
    }

    private IEnumerator LoadLoadeds()
    {
        if (loadingObj)
            loadingObj.SetActive(true);
    
        for (int i = objCounter; i < imgLst.Count; i++)
                InstantiateNextItem();

        yield return new WaitForEndOfFrame();

        if (loadingObj)
            loadingObj.SetActive(false);
    
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);

        if (popup)
        {
            popup.SetActive(true);

            popup.GetComponentInChildren<Text>().text = txt;
        }
    }
}
