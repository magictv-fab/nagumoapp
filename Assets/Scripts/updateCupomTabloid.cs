﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class updateCupomTabloid : MonoBehaviour {

    public Image container;

    public Sprite  cupom10, cupom50, cupom100, cupom200, cupom500;

    public int cupomValue;
	// Use this for initialization

    void Awake(){
        cupomValue = PlayerPrefs.GetInt("CupomTabloid");

        Debug.Log("Valor tabloid" + cupomValue);
    }



	void Start () {



	
        switch (cupomValue)
        {
            case 10:
                container.sprite = cupom10;
                break;
            case 50:
                container.sprite = cupom50;
                break;
            case 100:
                container.sprite = cupom100;
                break;
            case 200:
                container.sprite = cupom200;
                break;
            case 500:
                container.sprite = cupom500;
                break;
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
