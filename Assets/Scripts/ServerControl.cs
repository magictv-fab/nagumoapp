﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ServerControl : MonoBehaviour
{
    public static ServerControl instance;

    //public static string url = "http://www.dashboard-magictv.com.br/nagumoplay/webservices/docenovembro2018testes.php";
    public static string url = "http://www.dashboard-magictv.com.br/nagumoplay/webservices/bonificacao_pontos";
	public static string urlCRM = "http://www.dashboard-magictv.com.br/nagumoplay/webservices/crm";



    public static string urlIMGOfertas = "http://www.dashboard-magictv.com.br/nagumoplay/site/assets/img/ofertas";
    public static string urlIMGPublicacoes = "http://www.dashboard-magictv.com.br/nagumoplay/site/assets/img/publicacoes";
    public static string urlIMGTabloides = "http://www.dashboard-magictv.com.br/nagumoplay/site/assets/img/tabloide";
    public static string urlIMGProfile = "http://www.dashboard-magictv.com.br/nagumoplay/site/assets/img/participantes";
    public static string urlAcougue = "http://dashboard-magictv.com.br/nagumoplay/webservices/acougue";
    public static string urlIMGAcougue = "http://dashboard-magictv.com.br/nagumoplay/acougue/assets/img/produtos_acougue";

	public delegate void OnPlayDelegate(PointsAngle status);
    public event OnPlayDelegate OnPlay;
	public GameObject popup;

	private LightsControl lightControl;
    private GameMain gameMain;
    private string alertMessage;

    // Use this for initialization
    void Start()
    {
        instance = this;

        gameMain = GetComponent<GameMain>();
        lightControl = GetComponent<LightsControl>();

         // if(Login.isLogged)
    	//gameMain.jogadasRestantes = Login.userData.spins;
    }

    public void Play()
    {
        StartCoroutine(IPlay());

        /*Jogo Fake Random */
//         Array values = Enum.GetValues(typeof(PointsAngle));
//         System.Random random = new System.Random();
//         PointsAngle randomBar = (PointsAngle)values.GetValue(random.Next(values.Length));
//
//         OnPlay(randomBar);
    }

    private IEnumerator IPlay()
    {
        WWW www = null;
        JsonPlayRequest jsonPlayRequest = new JsonPlayRequest();
        if (Login.isLogged)
        {
            jsonPlayRequest.cpf = Login.userData.cpf;
            jsonPlayRequest.id_participante = Login.userData.id_participante;
            jsonPlayRequest.senha = Base64Encode(Login.userData.senha);

            string json = JsonUtility.ToJson(jsonPlayRequest, true);

            Debug.Log(json);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());
			
            www = new WWW(ServerControl.url + "/jogar.php", pData, headers);
		}


       // WWW www = new WWW("http://www.magictv.com.br/nagumoplay/server.php");
        //WWW www = new WWW("http://www.cutehdwallpapers.com/uploads/large/4k-ultra-hd-wallpapers/cool-4k-ultra-hd-wallpapers.jpg");
        //yield return www;

        StopCoroutine("ConectandoLetreiro");
        StartCoroutine("ConectandoLetreiro", "Conectando...");

        float fakeProgress = 0;
        do
        {
            if (www.progress == 0)
            {
                fakeProgress += Time.deltaTime * 0.1f;
                fakeProgress = Mathf.Clamp(fakeProgress, 0, 0.4f);
            }
            else
            {
                fakeProgress = 0;
            }
            //Debug.Log(www.progress);
            lightControl.LoadLights(www.progress + fakeProgress);
            yield return new WaitForEndOfFrame();
        }
        while (!www.isDone);

        lightControl.LoadLights(www.progress + fakeProgress);
        StopCoroutine("ConectandoLetreiro");


        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.text);

            PointsAngle state = PointsAngle.lose;

            try
            {
                //converte o json recebido para o objeto.
                JsonPlay jsonPlay = JsonUtility.FromJson<JsonPlay>(www.text);
                gameMain.jogadasRestantes = jsonPlay.jogadas;
                Login.userData.spins = jsonPlay.jogadas;
                //insere o cupom caso tenha ganho.
                if (jsonPlay.cupom != "")
                {
                    CuponData cuponData = new CuponData();
                    cuponData.id = jsonPlay.cupom;
                    cuponData.valor = jsonPlay.valor;

                    //cria uma lista para add o cupom.
                    List <CuponData> lst = new List<CuponData>();
                    if (Login.userData.cupons.Length > 0)
                    {
                        lst.AddRange(Login.userData.cupons);
                    }
                    lst.Add(cuponData);

                    Login.userData.cupons = lst.ToArray();
                }

                switch (jsonPlay.pts)
                {
                    case 500:
                        state = PointsAngle.pts500;
                        break;
                    case 200:
                        state = PointsAngle.pts200;
                        break;
                    case 100:
                        state = PointsAngle.pts100;
                        break;
                    case 50:
                        state = PointsAngle.pts50;
                        break;
                    case 10:
                        state = PointsAngle.pts10;
                        break;
                    default:
                        state = PointsAngle.lose;
                        break;
                }

                if (OnPlay != null)
                {
                    OnPlay(state);
                }

            }
            catch
            {
                gameMain.Stop();

				PopUp(www.text);
            }
        }
        else
        {
            Debug.Log(www.error);
            gameMain.onGame = false;
            StartCoroutine("ConectandoLetreiro", "Problema na conexão!");
        }

    }

    public IEnumerator IPlayRoleta()
    {
        WWW www = null;
        JsonPlayRequest jsonPlayRequest = new JsonPlayRequest();
      //  if (Login.isLogged)
        {
            jsonPlayRequest.cpf = Login.userData.cpf;
            //jsonPlayRequest.id_participante = Login.userData.id_participante;
            jsonPlayRequest.senha = Base64Encode(Login.userData.senha);

            string json = JsonUtility.ToJson(jsonPlayRequest, true);

            Debug.Log(json);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

            www = new WWW(ServerControl.url + "/jogar.php", pData, headers);
        }


        float fakeProgress = 0;
        do
        {
            if (www.progress == 0)
            {
                fakeProgress += Time.deltaTime * 0.1f;
                fakeProgress = Mathf.Clamp(fakeProgress, 0, 0.4f);
            }
            else
            {
                fakeProgress = 0;
            }
            //Debug.Log(www.progress);
            //lightControl.LoadLights(www.progress + fakeProgress);
            yield return new WaitForEndOfFrame();
        }
        while (!www.isDone);

        //lightControl.LoadLights(www.progress + fakeProgress);
       // StopCoroutine("ConectandoLetreiro");


        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.text);

            try
            {
                //converte o json recebido para o objeto.
                JsonPlay jsonPlay = JsonUtility.FromJson<JsonPlay>(www.text);
                //gameMain.jogadasRestantes = jsonPlay.jogadas;
                Login.userData.spins = jsonPlay.jogadas;
                //insere o cupom caso tenha ganho.
                if (jsonPlay.cupom != "")
                {
                    CuponData cuponData = new CuponData();
                    cuponData.id = jsonPlay.cupom;
                    cuponData.valor = jsonPlay.valor;

                    //cria uma lista para add o cupom.
                    List<CuponData> lst = new List<CuponData>();
                    if (Login.userData.cupons.Length > 0)
                    {
                        lst.AddRange(Login.userData.cupons);
                    }

                    lst.Add(cuponData);

                    Login.userData.cupons = lst.ToArray();


                 }

                Login.userData.pontos_disponiveis += jsonPlay.pts;
                Login.userData.spins_disponiveis = jsonPlay.jogadas;
                RoletaGame.instance.UpdateText();

                RoletaGame.instance.offline = false;
                RoletaGame.instance.points = jsonPlay.pts;


            }
            catch
            {
                PopUp(www.text);
            }
        }
        else
        {
            Debug.Log(www.error);
            PopUp(www.error);
        }

    }


    private IEnumerator ConectandoLetreiro(string value)
    {
        while (true)
        {
            gameMain.letreiroDigital.text = value;
            yield return new WaitForSeconds(0.3f);
            gameMain.letreiroDigital.text = "";
            yield return new WaitForSeconds(0.3f);

        }
    }

    // Update is called once per frame
    void Update()
    {

    }

	public void PopUp(string txt)
	{
		Debug.Log("Popup: " + txt);

		popup.GetComponentInChildren<Text>().text = txt;
		popup.SetActive(true);
	}

	public static string Base64Encode(string plainText)
	{
		var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
		return System.Convert.ToBase64String(plainTextBytes);
	}
}

[System.Serializable]
public class JsonPlay
{
    public int pts = 0;
    public int jogadas = 0;
    public string cupom = "";
    public int valor;

}

[Serializable]
public class JsonPlayRequest
{
    public int id_participante;
    public string senha, cpf;
}