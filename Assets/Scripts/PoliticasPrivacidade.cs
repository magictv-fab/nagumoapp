﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoliticasPrivacidade : MonoBehaviour {

    public List<GameObject> politicas = new List<GameObject>();

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FechaTudo()
    {
        foreach(GameObject obj in politicas){
            obj.SetActive(false);
        }
    }

    public void Inversor( GameObject inverterObj )
    {
        foreach (GameObject obj in politicas)
        {
            if (inverterObj != obj)
                obj.SetActive(false);
            else
                obj.SetActive(!obj.activeSelf);
        }

    }
}
