﻿using UnityEngine;
using System.Collections;

public class PopupSugestaoOfertas : MonoBehaviour {

	// Use this for initialization

	void OnEnable () {
        if(PlayerPrefs.GetInt("ativouOfertaX") == 0)
        {
            gameObject.SetActive(true);
            gameObject.SetActiveRecursively(true);
        }
        else
        {
            gameObject.SetActive(false); 
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FecharPopup()
    {
        gameObject.SetActive(false);
        PlayerPrefs.SetInt("ativouOfertaX",1);
        PlayerPrefs.Save();

    }
}
