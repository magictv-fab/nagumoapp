﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class OfertasManager : MonoBehaviour 
{
    public static OfertasData ofertasData;
    public static OfertasManager instance;

    public static List<string> titleLst = new List<string>(),
                        valueLst = new List<string>(),
                        infoLst = new List<string>(),
                        idLst = new List<string>(),
                        validadeLst = new List<string>(),
                        linkLst = new List<string>();

    public List<OfertaValues> ofertaValuesLst = new List<OfertaValues>();
    public static List<Texture2D> textureLst = new List<Texture2D>();
    public static List<Sprite> imgLst = new List<Sprite>();
    public static List<bool> favoritouLst = new List<bool>();

    public GameObject loadingObj, popup;
    public GameObject itemPrefab;
    public GameObject containerItens;
    public static List<GameObject> itensList = new List<GameObject>();

    public GameObject favoritouPrimeiraVezObj;
    public GameObject popupSemConexao;

    private ScrollRect scrollRect;
    private int ofertaCounter;
    public static GameObject currentItem;
    public GameObject selectedItem;
    public Text quantidadeOfertasText;
    public UnityEngine.UI.Extensions.HorizontalScrollSnap horizontalScrollSnap;

    private static string currentJsonTxt;
    private const int loadMaxItens = 10;
    private static int loadedItensCount;
    private RectTransform rectContainer;
    private bool onRectUpdate;
    private static int loadedImgsCount;    //serve para contar as imagens q ja foram carregadas.

	// Use this for initialization
	void Start () 
    {
        rectContainer = containerItens.GetComponent<RectTransform>();

        instance = this;

        StartCoroutine(ILoadOfertas());

        scrollRect = GetComponent<ScrollRect>();

	}

    public void ShowNumberSelected(int value)
    {
        if (titleLst.Count > 0)
        {
            quantidadeOfertasText.text = (value + 1).ToString() + " de " + titleLst.Count.ToString();
        }
        else
        {
           // quantidadeOfertasText.text = (value).ToString() + " de " + titleLst.Count.ToString();
            quantidadeOfertasText.text = "";
        }
    }

    public void InstantiateNextItem()
    {
        if (ofertaCounter >= titleLst.Count)
            return;

        var obj = Instantiate(itemPrefab);
        obj.transform.SetParent(containerItens.transform, false);
        obj.transform.localScale = Vector3.one;
        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        ItemMoveDrag itemMoveDrag = obj.GetComponent<ItemMoveDrag>();
        itemMoveDrag.onSelected.AddListener(InstantiateNextItem);

        OfertaValues ofertaValues = obj.GetComponent<OfertaValues>();

        ofertaValues.title.text = titleLst[ofertaCounter];
        ofertaValues.value.text = valueLst[ofertaCounter];
        ofertaValues.info.text = infoLst[ofertaCounter];
        ofertaValues.img.sprite = imgLst[ofertaCounter];
        ofertaValues.id = idLst[ofertaCounter];
        ofertaValues.favoritou = favoritouLst[ofertaCounter];
        ofertaValues.validade.text = validadeLst[ofertaCounter];
        ofertaValues.link = linkLst[ofertaCounter];

        ofertaValues.popupSemConexao = popupSemConexao;

        //obj.transform.SetAsFirstSibling();
        currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

        ofertaCounter++;

        ofertaValuesLst.Add(ofertaValues);
    }

    public void SendStatus(int status)
    {
        StartCoroutine(ISendStatus(selectedItem.GetComponent<OfertaValues>().id, status));   
    }

    public void BtnAccept()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * 2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onAllow.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(1);
    }

    public void BtnRecuse()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * -2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onDeny.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(0);
    }

    private IEnumerator ISendStatus(string id, int status)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"status_oferta\":\"" + status + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/salva_ofertas.php", pData, headers);

    //    if (loadingObj)
      //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ILoadOfertas()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_ofertas.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            //verifica se o txt que chegou eh diferente do que tem ja.
            if (currentJsonTxt != www.text)
            {
                Debug.Log("Tem atualizacao de Ofertas!");
                currentJsonTxt = www.text;
               
                loadedItensCount = 0;
                loadedImgsCount = 0;

                //Atualiza:
                if (loadingObj)
                    loadingObj.SetActive(true);

                //LoaderPublishAnim.instance.EndLoad();

                foreach (GameObject obj in itensList)
                    Destroy(obj);

                itensList.Clear();

                titleLst.Clear();
                valueLst.Clear();
                infoLst.Clear();
                idLst.Clear();
                favoritouLst.Clear();
                imgLst.Clear();
                linkLst.Clear();
                textureLst.Clear();
                validadeLst.Clear();
                ofertaValuesLst.Clear();

                ofertaCounter = 0;

                try
                {
                    string message = SimpleJSON.EscapeString(www.text);

                    //popula obj json.
                    ofertasData = JsonUtility.FromJson<OfertasData>(message);

                    //passa as informacoes para as listas.
                    foreach (OfertaData ofertaData in ofertasData.ofertas)
                    {
                        titleLst.Add(ofertaData.titulo);
                        if(ofertaData.desconto <= 0)
                        {
                            valueLst.Add("Pague: " + ofertaData.pague.ToString() + " e leve: " + ofertaData.leve.ToString());
                        }
                        else
                        {
                            valueLst.Add("Desconto de: " + ofertaData.desconto.ToString() + "%");
                        }
                        infoLst.Add(ofertaData.texto);

                        idLst.Add(ofertaData.id_oferta.ToString());
                        favoritouLst.Add(ofertaData.aderiu);
                        if (ofertaData.unidade > 0)
                            validadeLst.Add("Válido para até: " + ofertaData.unidade + " unidade(s) - " + GetDateString(ofertaData.datafinal));
                        else
                        {
                            if (ofertaData.peso >= 1000)
                            {
                                validadeLst.Add("Válido para até: " + (ofertaData.peso * 0.001f).ToString("0.###").Replace('.', ',') + " Kg - " + GetDateString(ofertaData.datafinal));
                            }
                            else
                            {
                                validadeLst.Add("Válido para até: " + ofertaData.peso + " grama(s) - " + GetDateString(ofertaData.datafinal));
                            }
                        }

                        linkLst.Add(ofertaData.link);

                        imgLst.Add(new Sprite());
                        textureLst.Add(new Texture2D(100, 100));
                    }

                    //tratamento caso não tenha ofertas.
                    if (ofertasData.ofertas.Length > 0)
                    {

                        //StartCoroutine(ILoadImgs());
                        StartCoroutine(LoadLoadeds());

                    }
                    else
                    {
                        if (loadingObj)
                            loadingObj.SetActive(false);
                    }
                }
                catch
                {
                    PopUp(www.text);
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
            else
            {
                //se o txt for igual so baixa os que ja tem.
                //tratamento se n tiver ofertas.
                if (ofertasData != null)
                {
                    StartCoroutine(LoadLoadeds());
                }
                else
                {
                    if (loadingObj)
                        loadingObj.SetActive(false);                    
                }
            }
        }
    }

	public void Update()
	{
        if(Input.GetKeyDown(KeyCode.Space))
        {
            LoadMoreItens();
        }

        if (ofertaValuesLst.Count > loadMaxItens && !onRectUpdate)
        {
            if (Mathf.Abs(containerItens.transform.localPosition.x) > (631 * loadedItensCount))
            {
                onRectUpdate = true;
                LoadMoreItens();
            }
        }
	}

	public void LoadMoreItens()
    {
        //StartCoroutine(ILoadImgs());
    }

    public static string GetDateString(string date)
    {
        System.DateTime theDate = System.DateTime.Parse(date);
        return theDate.ToString("dd/MM/yyyy");

    }

    public static string GetDateTimeString(string date)
    {
        System.DateTime theDate = System.DateTime.Parse(date);
        return theDate.ToString("dd/MM/yyyy - HH:mm");

    }

    private IEnumerator ILoadImgs()
    {

            Debug.Log("Carrega imagens.");

            for (int i = loadedItensCount; i < ofertasData.ofertas.Length; i++)
            {
                if (loadedItensCount >= imgLst.Count)
                    break;

                WWW www = new WWW(ServerControl.urlIMGOfertas + "/" + ofertasData.ofertas[loadedItensCount].imagem);
                yield return www;

                //yield return new WaitForSeconds(1f);

                if (string.IsNullOrEmpty(www.error))
                {
                    Texture2D texture = www.texture;
                    Sprite spt = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

                    imgLst[loadedItensCount] = spt;
                    textureLst[loadedItensCount] = texture;

                    if (ofertaValuesLst.Count > loadedItensCount)
                        ofertaValuesLst[loadedItensCount].img.sprite = spt;

                    loadedImgsCount++;
                }
                else
                {
                    Debug.LogError("Erro: " + www.error);
                    Debug.LogError("url: " + ServerControl.urlIMGOfertas + "/" + ofertasData.ofertas[loadedItensCount].imagem);
                }

                loadedItensCount++;
                onRectUpdate = false;
            }

    }

    private IEnumerator LoadLoadeds()
    {
        if (loadingObj)
            loadingObj.SetActive(true);
        
        scrollRect = GetComponent<ScrollRect>();

        for (int i = 0; i < ofertasData.ofertas.Length; i++)
        {
            InstantiateNextItem();
        }

        StartCoroutine(ILoadImgs());

        if(ofertasData.ofertas.Length > 0)
             if (quantidadeOfertasText)
                 quantidadeOfertasText.text = "1 de " + ofertasData.ofertas.Length;

        yield return new WaitForEndOfFrame();

        if (horizontalScrollSnap)
        {
            horizontalScrollSnap.InitialiseChildObjectsFromScene();
            horizontalScrollSnap.UpdateLayout();
            scrollRect.content.localPosition = Vector3.zero;
            if(currentItem)
                scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scrollRect.content.rect.width + currentItem.GetComponent<RectTransform>().rect.width / 2f);
        }

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);
        if (txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }
        if (popup)
        {
            popup.GetComponentInChildren<Text>().text = txt;
            popup.SetActive(true);
        }
    }


}

