﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PointsAngle
{
    lose = -1,
    pts100 = 0,
	pts200 = 72,
	pts10 = 144,
	pts50 = 216,
	pts500 = 288
}

public class GameMain : MonoBehaviour
{
    public GameObject buttonPlay;
    public PointsAngle premio;
    public List<GameObject> roletaLst = new List<GameObject>();
    public iTween.EaseType turnEaseType;
    public AudioClip playSound;
    public AudioClip winSound;
    public AudioClip loseSound;
    public float minTime = 4f;
    public float delayTime = 1.5f;
    public TextMesh letreiroDigital;
    public int jogadasRestantes = 3;
	public bool onGame;
    public List<GameObject> alertLst = new List<GameObject>();

	private ServerControl serverControl;
    private LightsControl lightControl;
    private AudioSource audioSource;
    private int counterStop;
    private Color startButtonColor;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        lightControl = GetComponent<LightsControl>();
        serverControl = GetComponent<ServerControl>();

        serverControl.OnPlay += PlayServer;
    }

    IEnumerator corrigeVoltaRoleta(){
        yield return new WaitForSeconds(1.5f);

        var roleta = roletaLst[0];
        iTween.RotateBy(roleta, iTween.Hash("z", .8, "easeType", "easeInOutBack", "delay", 2.5f));

        var roleta1 = roletaLst[1];
        iTween.RotateBy(roleta1, iTween.Hash("z", .6, "easeType", "easeInOutBack", "delay", 2.5f));


        var roleta2 = roletaLst[2];
        iTween.RotateBy(roleta2, iTween.Hash("z", .2, "easeType", "easeInOutBack", "delay", 2.5f));
    }

    // Use this for initialization
    IEnumerator Start () 
    {

        StartCoroutine(corrigeVoltaRoleta());

        //pega a cor do botao e guarda.
        startButtonColor = buttonPlay.GetComponent<Renderer>().materials[2].GetColor("_EmissionColor");

        //espera atualizar spins do servidor.
        //comentado para demo: 
        letreiroDigital.text = "Conectando...";
        UpdateData.onUpdate = true;

        jogadasRestantes = 0;

        while (UpdateData.onUpdate)
            yield return new WaitForEndOfFrame();

        //comentado para demo: 
        jogadasRestantes = Login.userData.spins;
        // jogadasRestantes = 99;
		Letreiro();

    }

	public void BtnPlay()
	{
        if (jogadasRestantes <= 0 || onGame)
			return;

        buttonPlay.GetComponent<Renderer>().materials[2].SetColor("_EmissionColor", Color.red);


		onGame = true;
        serverControl.Play();
    }

    void PlayServer(PointsAngle state)
    {
        premio = state;
        Debug.Log(premio.ToString());
        Play();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            Play();
    }

    public void Play()
    {
        Letreiro();

        //zera contador de paradas.
        counterStop = 0;
        int counter = 2;
        //guarda o primeiro numero para nao ganhar acidentalmente.
        PointsAngle rndRoleta = RndEnum();

        foreach(var obj in roletaLst)
        {
            Hashtable ht = new Hashtable();

            //verifica se perdeu ou se tem premio.
            if (premio == PointsAngle.lose)
            {
                PointsAngle rnd = RndEnum();
                while (rnd == rndRoleta || rnd == PointsAngle.pts100) 
                    rnd = RndEnum();

                ht.Add("rotation", new Vector3(0, 90f, (360f * 10 + (int)rnd)));

                rndRoleta = rnd;
                Debug.Log("Random: " + rnd);
			}
            else
            {
                ht.Add("rotation", new Vector3(0, 90f, (360f * 10 + (int)premio)));
            }

            ht.Add("time", minTime + (counter-- * delayTime));
            ht.Add("easetype", turnEaseType);
            ht.Add("islocal", true);
            ht.Add("oncomplete", "RoletaStop");
            ht.Add("oncompletetarget", gameObject);

            iTween.RotateTo(obj, ht);
        }

        //botao.
        if (playSound)
            audioSource.PlayOneShot(playSound);

        //rodando.
        audioSource.Play();
    }

    public void RoletaStop()
    {
        if (counterStop >= 2)
        {
            //volta a cor do botao.
            buttonPlay.GetComponent<Renderer>().materials[2].SetColor("_EmissionColor", startButtonColor);

			onGame = false;
            audioSource.Stop();
            if (premio == PointsAngle.lose)
            {
                Lose();
            }
            else
            {
                Win();
			}

		}
        counterStop++;
    }

    private PointsAngle RndEnum()
    {
        System.Array values = PointsAngle.GetValues(typeof(PointsAngle));
        System.Random random = new System.Random();
        return (PointsAngle)values.GetValue(random.Next(values.Length));
    }

    private void Letreiro()
    {
        if (letreiroDigital)
        {
            
            letreiroDigital.text = jogadasRestantes + (jogadasRestantes==1?" Jogada Restante":" Jogadas Restantes");
        }
    }


    private void Win()
    {
		audioSource.PlayOneShot(winSound);
        lightControl.BlinkAll(7);

        switch(premio)
        {
            case PointsAngle.pts10:
                alertLst[1].SetActive(true);
                break;
            case PointsAngle.pts50:
                alertLst[2].SetActive(true);
                break;
            case PointsAngle.pts100:
                alertLst[3].SetActive(true);
                break;
            case PointsAngle.pts200:
                alertLst[4].SetActive(true);
                break;
            case PointsAngle.pts500:
                alertLst[5].SetActive(true);
                break;
        }
  	}

    public void Stop()
    {
        onGame = false;
        buttonPlay.GetComponent<Renderer>().materials[2].SetColor("_EmissionColor", startButtonColor);
	}

    private void Lose()
    {
        alertLst[0].SetActive(true);
		audioSource.PlayOneShot(loseSound);
        lightControl.Turn();
	}
}
