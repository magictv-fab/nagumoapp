﻿using UnityEngine;
using System.Collections;

public class CagadorDeCubo : MonoBehaviour {
    public GameObject Cubo;

	// Use this for initialization
	void Start () {
	
	}

    public static CagadorDeCubo Instance;

    public CagadorDeCubo()
    {
        Instance = this;
    }

    public static void CagaCubo(Vector3 position)
    {
        var c = (GameObject)GameObject.Instantiate(Instance.Cubo);
        c.transform.position = position;
    }

	// Update is called once per frame
	void Update () {

	}
}