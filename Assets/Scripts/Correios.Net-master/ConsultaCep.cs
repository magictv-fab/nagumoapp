﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

public class ConsultaCep : MonoBehaviour
{

    public string BuscaCep(string cep)
    {
        HttpWebRequest requisicao = (HttpWebRequest)WebRequest.Create(
                "http://www.buscacep.correios.com.br/servicos/dnec/consultaLogradouroAction.do?Metodo=listaLogradouro&CEP=" + cep + "&TipoConsulta=cep");
        HttpWebResponse resposta = (HttpWebResponse)requisicao.GetResponse();

        int cont;
        byte[] buffer = new byte[1000];
        StringBuilder sb = new StringBuilder();
        string temp;

        Stream stream = resposta.GetResponseStream();
         
        do
        {
            cont = stream.Read(buffer, 0, buffer.Length);
                    temp = Encoding.Default.GetString(buffer, 0, cont).Trim();
            sb.Append(temp);

        } while (cont > 0);

        string pagina = sb.ToString();

        if (pagina.IndexOf("<font color=\"black\">CEP NAO ENCONTRADO</font>") >= 0)
        {
            Debug.Log( "<b style=\"color:red\">CEP não localizado.</b>");
        }
        else
        {
            string logradouro = Regex.Match(pagina,
        "<td width=\"268\" style=\"padding: 2px\">(.*)</td>").Groups[1].Value;
            string bairro = Regex.Matches(pagina,
        "<td width=\"140\" style=\"padding: 2px\">(.*)</td>")[0].Groups[1].Value;
            string cidade = Regex.Matches(pagina,
        "<td width=\"140\" style=\"padding: 2px\">(.*)</td>")[1].Groups[1].Value;
            string estado = Regex.Match(pagina,
        "<td width=\"25\" style=\"padding: 2px\">(.*)</td>").Groups[1].Value;
            string resultado = string.Format(
        "Logradouro: {0} <br/> Bairro: {1} <br/> Cidade: {2} <br/> Estado: {3}",
        logradouro, bairro, cidade, estado);
            //litResultado.Text = resultado;
        }

        return pagina;
    }
}
