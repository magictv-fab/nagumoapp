﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class FavoritosManager : MonoBehaviour
{
    public static PublicationsData publicationsData;
    public static FavoritosManager instance;

    public static List<string> titleLst = new List<string>(),
                        valueLst = new List<string>(),
                        infoLst = new List<string>(),
                        idLst = new List<string>();

    public static List<Sprite> imgLst = new List<Sprite>();

    public GameObject loadingObj, popup;
    public GameObject itemPrefab;
    public GameObject containerItens;
    public static List<GameObject> itensList = new List<GameObject>();

    private ScrollRect scrollRect;
    private int ofertaCounter;
    public GameObject currentItem;
    private GameObject selectedItem;
    private static string currentJsonTxt;

    // Use this for initialization
    void Start()
    {
        instance = this;

        StartCoroutine(ILoadPublish());      
    }

    public void InstantiateNextItem()
    {
        if (ofertaCounter >= titleLst.Count)
            return;

        var obj = Instantiate(itemPrefab);
        obj.transform.SetParent(containerItens.transform, false);
        obj.transform.localScale = Vector3.one * 0.87f;
        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        //ItemMoveDrag itemMoveDrag = obj.GetComponent<ItemMoveDrag>();
        // itemMoveDrag.onSelected.AddListener(InstantiateNextItem);

        OfertaValues ofertaValues = obj.GetComponent<OfertaValues>();

        //ofertaValues.title.text = titleLst[ofertaCounter];
        ofertaValues.value.text = valueLst[ofertaCounter];
        ofertaValues.info.text = infoLst[ofertaCounter];
        ofertaValues.img.sprite = imgLst[ofertaCounter];
        ofertaValues.id = idLst[ofertaCounter];
        ofertaValues.saveImageSource = imgLst[ofertaCounter].texture;

        obj.transform.SetAsFirstSibling();
        currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

        // if (ofertaCounter < titleLst.Count - 1)
        ofertaCounter++;
        //  else
        //   ofertaCounter = 0;
    }

    public void SendStatus(int status)
    {
        StartCoroutine(ISendStatus(selectedItem.GetComponent<OfertaValues>().id, status));
    }

    public void Favoritar(string id)
    {
        StartCoroutine(IFavoritar(id));
    }

    public void BtnAccept()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * 2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onAllow.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(1);
    }

    public void BtnRecuse()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * -2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onDeny.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(0);
    }

    public void DesFavoritar(string id)
    {
        StartCoroutine(ExcluirFavorito(id));
    }

    private IEnumerator ExcluirFavorito(string id)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_publicacao\":\"" + id + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/excluir_favorito_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator IFavoritar(string id)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_publicacao\":\"" + id + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/salvar_publicacao_favoritos_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ISendStatus(string id, int status)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"status_oferta\":\"" + status + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/salvar_publicacao_favoritos_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ILoadPublish()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_publicacao_favoritos_participante.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);


            //verifica se o txt que chegou eh diferente do que tem ja.
            if (currentJsonTxt != www.text)
            {
                Debug.Log("Tem atualizacao de Ofertas!");
                currentJsonTxt = www.text;

                //Atualiza:
                if (loadingObj)
                    loadingObj.SetActive(true);

                //LoaderPublishAnim.instance.EndLoad();

                foreach (GameObject obj in itensList)
                    Destroy(obj);

                itensList.Clear();

                titleLst.Clear();
                valueLst.Clear();
                infoLst.Clear();
                idLst.Clear();
                imgLst.Clear();

                ofertaCounter = 0;


                try
                {
                    string message = SimpleJSON.EscapeString(www.text);
                    //   Debug.Log(message);

                    //popula obj json.
                    publicationsData = JsonUtility.FromJson<PublicationsData>(message);

                    //passa as informacoes para as listas.
                    foreach (PublicationData publicationData in publicationsData.publicacoes)
                    {
                        titleLst.Add(publicationData.titulo);
                        valueLst.Add(publicationData.datainicial);
                        infoLst.Add(publicationData.texto);
                        idLst.Add(publicationData.id_publicacao.ToString());

                        //if (titleLst.Count >= 11)
                        //    break;
                    }

                    StartCoroutine(ILoadImgs());
                }
                catch
                {
                    PopUp(www.text);
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
            else
            {
                StartCoroutine(LoadLoadeds());
            }
        }
    }

    private IEnumerator ILoadImgs()
    {
        foreach (PublicationData publicationData in publicationsData.publicacoes)
        {
            WWW www = new WWW(ServerControl.urlIMGPublicacoes + "/" + publicationData.imagem);
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                Sprite spt = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
                imgLst.Add(spt);
            }
        }

        StartCoroutine(LoadLoadeds());
    }

    private IEnumerator LoadLoadeds()
    {
        if (loadingObj)
            loadingObj.SetActive(true);

        scrollRect = GetComponent<ScrollRect>();

        for (int i = 0; i < titleLst.Count; i++)
        {
            InstantiateNextItem();
        }

        yield return new WaitForEndOfFrame();

        if (loadingObj)
            loadingObj.SetActive(false);
    }


    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);

        if (popup)
        {
            popup.GetComponentInChildren<Text>().text = txt;
            popup.SetActive(true);
        }
    }
}

