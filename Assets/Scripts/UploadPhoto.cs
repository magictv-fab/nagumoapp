﻿// C# file names: "FileUpload.cs"
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class UploadPhoto : MonoBehaviour
{
    public Image image;
    string imgPath = "";
    string uploadUrl = "";

	public GameObject loadingObj;

    IEnumerator IUploadPhotoToServer(string localFileName, string uploadURL)
    {
        imgPath = localFileName;
        this.uploadUrl = uploadURL; 

        WWW localFile = new WWW(localFileName);
        yield return localFile;

        if (localFile.error == null)
            Debug.Log("Loaded file successfully");
        else
        {
            Debug.Log("Open file error: " + localFile.error);
            yield break; // stop the coroutine here
        }
        WWWForm postForm = new WWWForm();
        // version 1
        postForm.AddBinaryData("foto_perfil", localFile.texture.EncodeToJPG(), "foto_perfil.jpg", "image/jpg");
		postForm.AddField("login", Login.userData.cpf);
		postForm.AddField("senha", Login.Base64Encode(Login.userData.senha));
		

        // version 2
        //postForm.AddBinaryData("theFile", localFile.bytes, localFileName, "text/plain");
        WWW upload = new WWW(uploadURL, postForm);
        yield return upload;

        if (upload.error == null)
        {
            Debug.Log("upload done :" + upload.text);

            Login.photo_profile = localFile.texture;

            //GetInfoMenu.instance.fotoImg.sprite = Sprite.Create(Login.photo_profile, new Rect(0, 0, Login.photo_profile.width, Login.photo_profile.height), Vector2.zero);
            if (GetInfoMenu.instance)
            {
                if (GetInfoMenu.instance.fotoImg)
                {
                    GetInfoMenu.instance.fotoImg.sprite = GetInfoMenu.CropImage(localFile.texture);
                }
            }

            if(image)
            {
                image.sprite = GetInfoMenu.CropImage(localFile.texture);
            }
        }
        else
            Debug.Log("Error during upload: " + upload.error);
    }
		

    void UploadFile(string localFileName, string uploadURL)
    {
        StartCoroutine(IUploadPhotoToServer(localFileName, uploadURL));
    }


	public void	UploadPhotoToServer(){
		
		UploadFile(imgPath, ServerControl.url + "/foto_perfil.php");

	}

	public void BtnUploadPhotoAuto(){
		
		Debug.Log("Upload from library");

		NativeImagePicker.FromLibrary(new NativeImagePicker.CallbackImagePicked((string filepath)=>{

			Debug.Log("path " + filepath);

			//Manda direto para o server (Usando as informações do Login)
			StartCoroutine(IUploadPhotoToServer(filepath , ServerControl.url + "/foto_perfil.php"));

		}));
	}

    public void BtnUploadLocalPhoto()
    {
        Debug.Log("Upload from library");

        NativeImagePicker.FromLibrary(new NativeImagePicker.CallbackImagePicked((string filepath)=>{
			
        Debug.Log("path " + filepath);
		//Acesso global ao caminho da foto
		imgPath = filepath;
		//Mostra para o user (local)
		StartCoroutine(loadImageByWWW(filepath));

        }));
    }


	// Use this for initialization
	IEnumerator loadImageByWWW (string path) {

		if (loadingObj)
			loadingObj.SetActive(true);

		WWW localFile = new WWW(path);
		yield return localFile;

		if (loadingObj)
			loadingObj.SetActive(false);

		if (localFile.error == null){
			Debug.Log("Loaded file successfully");
			if(image)
			{
				image.sprite = GetInfoMenu.CropImage(localFile.texture);
			}

		}
			
		else
		{
			Debug.Log("Open file error: " + localFile.error);
			yield break; // stop the coroutine here
		}

		Login.photo_profile = localFile.texture;

		if (GetInfoMenu.instance)
		{
			if (GetInfoMenu.instance.fotoImg)
			{
				GetInfoMenu.instance.fotoImg.sprite = GetInfoMenu.CropImage(localFile.texture);
			}
		}




	}
		

}