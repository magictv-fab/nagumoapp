﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class UserData {

    public string nome, sobrenome, cpf, telefone, email, senha, foto, cep;
    public string rua;
    public string bairro;
    public int numero;
    public string complemento;
    public string estado;
    public string cidade;
    public CuponData[] cupons;
    public int id_participante, id,spins, idade, sexo, pontos, pontos_disponiveis, spins_disponiveis, cupons_disponiveis;
    public float saldo_acumulado;
    public string dt_nasc;
}

[System.Serializable]
public class CuponData
{
    public int valor, pts, promocao;
    public string id, dataExpira;
    public bool used;
}

[System.Serializable]
public class OfertasData
{
    public OfertaData[] ofertas;
}

[System.Serializable]
public class OfertaData
{
    public int id_oferta, desconto, pague, leve, unidade;
    public int peso;
    public string imagem, titulo, ean, texto, datainicial, datafinal, link;
    public string dataAquisicao = "";
    public bool aderiu;
}

[System.Serializable]
public class OfertasCRMDataServer
{
	public OfertasCRMDataGroup[] ofertas;
}

[System.Serializable]
public class OfertasCRMDataGroup
{
	public OfertaCRMData[] preco;
	public OfertaCRMData[] depor;
}

[System.Serializable]
public class OfertasCRMDataExclusivas
{
	public OfertaCRMData[] ofertas;
}
	

[System.Serializable]
public class OfertaCRMData
{
	public int id_oferta, tipo, unidade , desconto, pague, leve;
	public long id_crm;
	public int peso;
	public float preco , de , por;
	public string imagem, titulo, texto, datainicial, datafinal, link, ean;
	public string dataAquisicao = "";
	public bool aderiu;
}

[System.Serializable]
public class PublicationsData
{
    public PublicationData[] publicacoes;
}

[System.Serializable]
public class PublicationData
{
    public int id_publicacao;
    public bool favoritou;
    public string imagem, titulo, texto, datainicial, link;
}

[System.Serializable]
public class TabloidesData
{
    public TabloideData[] tabloides;
}

[System.Serializable]
public class TabloideData
{
    public float valor;
    public string imagem, texto, link;
    public int id_tabloide;
}

[System.Serializable]
public class AcougueData
{
    public CarnesData[] carnes;
    public int posicao;

}

[System.Serializable]
public class CarnesData
{
    public string nome_produto, corte, observacao, peso;
    public string imagem;
    public int id_carne;
    public CortesData[] cortes;
}

[System.Serializable]
public class CortesData
{
    public string corte;
    public int id_corte;
}

[System.Serializable]
public class PedidoData
{
    public string nome, timeStamp, peso, observacoes, nome_loja;
    public int idCarne, idCorte, quantidade, status_pedido, avaliou, id_pedido, idPedido; //campos com o mesmo nome mas o cara de web fica nervoso se for questionado entao esta ai.
    public CarnesData[] carnes;

    [System.NonSerialized]
    public string info;
    [System.NonSerialized]
    public Sprite sprite;
}

[System.Serializable]
public class PedidoEnvioData
{
    public string login;
    public string senha;
    public int loja, nota = 0;
    public bool passarPeso;
    public PedidoData[] pedidos;

    [System.NonSerialized]
    public string codePedido;
}

[System.Serializable]
public class MeuPedidoData
{
    public int posicao;
    public PedidoData[] pedidos;
}
