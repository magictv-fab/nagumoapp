﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AutoPreencherDropDown : MonoBehaviour {

    public string text;
    public string[] separator;

    private Dropdown dropdown;

    // Use this for initialization
    void Start()
    {
        dropdown = GetComponent<Dropdown>();

        foreach (string txt in text.Split(separator, System.StringSplitOptions.RemoveEmptyEntries))
        {
            Dropdown.OptionData optionData = new Dropdown.OptionData(txt);
            
            dropdown.options.Add(optionData);
        }
    }
	// Update is called once per frame
	void Update () {
	
	}
}
