﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateData : MonoBehaviour {

    public static bool onUpdate = false;

    public GameObject loadingObj;

	// Use this for initialization
	void Start () {
        StartCoroutine(ILogin());
        Login.userData.spins = 3;
	}
	

    /// <summary>
    /// Chamado para atualizar os campos de spins e cupons.
    /// </summary>
    private IEnumerator ILogin()
    {
        onUpdate = true;

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Base64Encode(Login.userData.senha) + "\"}";

        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/login.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            //PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            try
            {
                //[0] = code. [1] = message;
                string code = www.text.Split(',')[0];
                string message = www.text.Remove(0, code.Length + 1);
                Debug.Log(code);
                Debug.Log(message);

                if (code.Contains("200"))
                {

                    //popula obj json.
                    UserData userData = JsonUtility.FromJson<UserData>(message);
                    Login.userData.cupons = userData.cupons;
                    // Login.userData.spins = userData.spins;

                    Debug.Log("Update OK");

                    if (loadingObj)
                        loadingObj.SetActive(false);

                }
                else
                {
                   // PopUp(message);
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
            catch
            {
               // PopUp(www.text);
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
        }

        onUpdate = false;
    }

    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }
}
