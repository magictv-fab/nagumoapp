﻿using UnityEngine;
using System.Collections;

public class ButtonSendMessage : MonoBehaviour {

    public string methodName = "Play";
    public GameObject listenerObj;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnMouseUp()
    {
        listenerObj.SendMessage(methodName);
    }
}
