using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Net.NetworkInformation;
using System;
using System.Text.RegularExpressions;
using System.Globalization;
using Correios.Net.Http;
using Correios.Net;
using System.Net;
using System.Text;
using System.IO;
using System.Linq;

public class Cadastro : MonoBehaviour
{

    public InputField nomeInput, sobreNomeInput, cpfInput, emailInput, reemailInput, telefoneInput, idadeInput, senhaInput, resenhaInput, novaSenhaInput, cepInput, ruaInput, bairroInput, numeroInput, complementoInput, cidadeInput, estadoInput, dddInput;
	public Toggle politicaPriv, politicaFeidl;
    public Dropdown sexoDd, estadoDd;
    public GameObject popup;
    public GameObject loadingObj;
    public Image foto;

    private JsonCadastro jsonCadastro = new JsonCadastro();
    private bool onConnection;
    private bool back;
    private string lastedChar;
    private DateTime tmpDateTime;
    private string levelName;

    // Use this for initialization
    void Start()
    {
		Debug.Log("OnConnection value= "+ onConnection);
        levelName = SceneManager.GetSceneAt(SceneManager.sceneCount - 1).name;
        //Debug.Log(ConvertUSToBrFormat("1987-02-22"));

//        if (foto && Login.photo_profile != null)
//            foto.sprite = GetInfoMenu.CropImage(Login.photo_profile);

        if(FacebookMain.facebookData != null)
        {
            if(!String.IsNullOrEmpty(FacebookMain.facebookData.name))
            {
                string fbNome = FacebookMain.facebookData.name.Contains(" ") ? FacebookMain.facebookData.name.Split(' ')[0] : FacebookMain.facebookData.name;
                string fbSobrenome = FacebookMain.facebookData.name.Contains(" ") ? FacebookMain.facebookData.name.Split(' ')[1] : "";

                nomeInput.text = fbNome;
                sobreNomeInput.text = fbSobrenome;
            }
            if (!String.IsNullOrEmpty(FacebookMain.facebookData.email))
            {
                emailInput.text = FacebookMain.facebookData.email;
                reemailInput.text = FacebookMain.facebookData.email;
            }
            if (!String.IsNullOrEmpty(FacebookMain.facebookData.birthday))
            {
                idadeInput.text = FacebookMain.facebookData.birthday;
            }
            if (!String.IsNullOrEmpty(FacebookMain.facebookData.gender))
            {
                if (FacebookMain.facebookData.gender == "male")
                    sexoDd.value = 0;
                else
                    sexoDd.value = 1;
            }
        }


        Debug.Log(GetMacAddress());

        if (levelName == "Atualizar")
        {
			//Só mostra a foto se for atualizar cadastro
			if (foto && Login.photo_profile != null)
				foto.sprite = GetInfoMenu.CropImage(Login.photo_profile);

            FeedForm();
        }
        else
        {
            PlayerPrefs.SetInt("login", 0);
			// Caso contrário, seta a foto como nula
			foto = null;
			Login.photo_profile = null;
        }

        //gambiarra para remover o ultimo item e deixar ele como um dropdowm descente.
        onChangeSexo(0);
        //EstadoSelect("São Paulo - SP");
    }

    //pega o estado pelo nome e seleciona ele no dropdown.
    public void EstadoSelect(string name)
    {
        name = name.Split(' ').Last().ToLower();
        var listaEstados = estadoDd.options;
        var item = listaEstados.FirstOrDefault(estado => estado.text.Split(' ').Last().ToLower() == name.ToLower());
        estadoDd.value = listaEstados.IndexOf(item);
    }

    public static string ConvertUSToBrFormat(string dt)
    {
        Debug.Log("convert: " + dt);
        try
        {
            return DateTime.ParseExact(dt, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
        }
        catch
        {
            return "";
        }
    }

    public static string ConvertBRToUSFormat(string dt)
    {
        Debug.Log("convert: " + dt);
        try
        {
            return DateTime.ParseExact(dt, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
        }
        catch
        {
            return "";
        }
    }

    public void Logout()
    {
        PlayerPrefs.SetInt("login", 0);
        Login.isLogged = false;
        Login.userData = null;
        Application.LoadLevel(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Close();
    }

    public void OnValidateInput(string text)
    {
        Debug.Log(text);
        //I Want date format like (dd/mm/yyyy)
        if (text.Length <= 10)
        {
            
            switch (text.Length)
            {
                case 2: //User typed 'dd'
                    if (lastedChar != "/") 
                    {
                        idadeInput.text = text + "/";
                        idadeInput.caretPosition = idadeInput.text.Length;
                    }
                    else
                    {
                        lastedChar = idadeInput.text[idadeInput.text.Length - 1].ToString();

                    }
                    break;
                case 3: //User typed 'dd'
                    if (!text.Contains("/"))
                    {
                        idadeInput.text = text.Insert(2,"/");
                        idadeInput.caretPosition = idadeInput.text.Length;
                    }
                    break;
                case 5: //User typed 'dd/mm'
                   
                    if (lastedChar != "/")
                    {
                        idadeInput.text = text + "/";
                        idadeInput.caretPosition = idadeInput.text.Length;
                    }
                    break;
                case 6: //User typed 'dd'
                    if (lastedChar != "/") 
                        if (text[5].ToString() != "/")
                        {
                            idadeInput.text = text.Insert(5, "/");
                            idadeInput.caretPosition = idadeInput.text.Length;
                        }
                    break;
                default:
                    
                    break;
            }

        }
        else
        {
            idadeInput.text = text.Remove(text.Length - 1);
        }

        if(idadeInput.text.Length > 0)
              lastedChar = idadeInput.text[idadeInput.text.Length - 1].ToString();
            //else
            
    }

    public void FeedForm()
    {

        if (!string.IsNullOrEmpty(Login.userData.nome))
        {
            //string fbNome = Login.userData.nome.Contains(" ") ? Login.userData.nome.Split(' ')[0] : Login.userData.nome;
            //string fbSobrenome = Login.userData.nome.Contains(" ") ? Login.userData.nome.Split(' ')[1] : "";

            nomeInput.text = Login.userData.nome;
            //nomeInput.text = Login.userData.nome;
        }
        else
        {
            nomeInput.GetComponent<Image>().color = Color.yellow;
        }

        if (!string.IsNullOrEmpty(Login.userData.sobrenome))
        {
            //string fbNome = Login.userData.nome.Contains(" ") ? Login.userData.nome.Split(' ')[0] : Login.userData.nome;
            //string fbSobrenome = Login.userData.nome.Contains(" ") ? Login.userData.nome.Split(' ')[1] : "";

            sobreNomeInput.text = Login.userData.sobrenome;
            //nomeInput.text = Login.userData.nome;
        }
        else
        {
            sobreNomeInput.GetComponent<Image>().color = Color.yellow;
        }

        if (!string.IsNullOrEmpty(Login.userData.cpf)) cpfInput.text = Login.userData.cpf;

        if (!string.IsNullOrEmpty(Login.userData.email))
            emailInput.text = Login.userData.email;
        else 
            emailInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.telefone))
        {
            if (Login.userData.telefone.StartsWith("0"))
                telefoneInput.text = Login.userData.telefone.Substring(3);
            else
                telefoneInput.text = Login.userData.telefone.Substring(2);
        }
        else
            telefoneInput.GetComponent<Image>().color = Color.yellow;
 
        if (!string.IsNullOrEmpty(Login.userData.telefone))
        {
            if(Login.userData.telefone.StartsWith("0"))
                dddInput.text = Login.userData.telefone.Remove(3);
            else
                dddInput.text = Login.userData.telefone.Remove(2);
        }
        else
            dddInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.cep))
            cepInput.text = Login.userData.cep;
        else
            cepInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.rua))
            ruaInput.text = Login.userData.rua;
        else
            ruaInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.bairro))
            bairroInput.text = Login.userData.bairro;
        else
            bairroInput.GetComponent<Image>().color = Color.yellow;

        if (Login.userData.numero > 0)
            numeroInput.text = Login.userData.numero.ToString();
        else
            numeroInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.complemento))
            complementoInput.text = Login.userData.complemento;
        else
            complementoInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.cidade))
            cidadeInput.text = Login.userData.cidade;
        else
            cidadeInput.GetComponent<Image>().color = Color.yellow;

        if (!string.IsNullOrEmpty(Login.userData.estado))
            EstadoSelect(Login.userData.estado);
        /*    estadoInput.text = Login.userData.estado;
        else
            estadoInput.GetComponent<Image>().color = Color.yellow;
*/
        if (string.IsNullOrEmpty(Login.userData.dt_nasc))
            idadeInput.GetComponent<Image>().color = Color.yellow;

        if(Login.userData.dt_nasc != "")
            idadeInput.text = ConvertUSToBrFormat(Login.userData.dt_nasc);
       
        else if (Login.userData.idade > 0)
            idadeInput.text = Login.userData.idade.ToString();   
        
        sexoDd.value = Login.userData.sexo;


    }

    public void SearchCep(string cep)
    {
        if(cep == "")
        {
            cep = cepInput.text;    
        }

        if (cep == "")
            return;

//        StartCoroutine(IBuscaCep(cep));
		StartCoroutine(IBuscaCEPAPI(cep));
    }


	private IEnumerator IBuscaCEPAPI(string cep){


		WWW wwwCep = new WWW("https://viacep.com.br/ws/"+cep+"/json/");
		yield return wwwCep;

		Debug.Log(wwwCep.text);

		//Sem erro ...
		if(string.IsNullOrEmpty(wwwCep.error)){

			try{

				CEP cepAPI = JsonUtility.FromJson<CEP>(wwwCep.text);

				cidadeInput.text = cepAPI.localidade;
				ruaInput.text = cepAPI.logradouro;
				EstadoSelect(cepAPI.uf);
				bairroInput.text = cepAPI.bairro;

			}catch{

				PopUp("CEP não encontrado.");
			}

		}else{
				PopUp("Problema na conexão com a internet.");
		}
		
	}

    private IEnumerator IBuscaCep(string cep)
    {

        WWW wwwCep = new WWW("https://buscarcep.com.br/?cep=" + cep + "&formato=string&chave=Chave_Gratuita_BuscarCep&identificador=CLIENTE1");
        yield return wwwCep;

			if(string.IsNullOrEmpty(wwwCep.error))
        {
            try
            {
                //Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                Encoding iso = Encoding.GetEncoding("iso-8859-1");
                Encoding utf8 = Encoding.GetEncoding("iso-8859-1");
                byte[] isoBytes = Encoding.Convert(utf8, iso, wwwCep.bytes);
                string result = iso.GetString(isoBytes);

                Debug.Log(result);

                string[] resp = result.Split('&');
                foreach (var field in resp)
                {
                    //Debug.Log(field);
                    if (field.Split('=').Length > 1)
                    {
                        var chave = field.Split('=')[0];
                        var valor = field.Split('=')[1];

                        if (chave == "cidade")
                        {
                            cidadeInput.text = valor;
                        }
                        else if (chave == "logradouro")
                        {
                            ruaInput.text = valor;
                        }
                        else if (chave == "uf")
                        {
                            //estadoInput.text = valor;
                            EstadoSelect(valor);
                        }
                        else if (chave == "bairro")
                        {
                            bairroInput.text = valor;
                        }
                        else if(chave == "resultado")
                        {
                            if(valor == "-2")
                                PopUp("Formato de cep inválido!");
                            else if (valor == "-1")
                                PopUp("cep não encontrado!");
                        }
                    }
                }
            }
            catch
            {
                PopUp("CEP não encontrado.");
            }
        }
        else
        {
            PopUp("Problema na conexão com a internet.");
        }
    }

    private bool ValidadeDate(string date)
    {
        return DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tmpDateTime);
    }

    public void SendJson()
    {
		

		if (!CheckData() || onConnection){

			return;

		}
        //seta a acao q ira fazer, cadastrar ou editar.
        if (levelName == "Atualizar")
        {
            jsonCadastro.acao = "atualizar";
            jsonCadastro.senha = Base64Encode(Login.userData.senha);
        }
        else
        {
            jsonCadastro.acao = "cadastrar";
            jsonCadastro.senha = Base64Encode(senhaInput.text);
        }

        jsonCadastro.nome = nomeInput.text;
        jsonCadastro.sobrenome = sobreNomeInput.text;
        jsonCadastro.cpf = cpfInput.text;
        //if(idadeInput.enabled)
         //   jsonCadastro.idade = Mathf.Abs(int.Parse(idadeInput.text));
        jsonCadastro.email = emailInput.text;
        if (telefoneInput.text == "")
            jsonCadastro.telefone = "00000000";
        else
            jsonCadastro.telefone = dddInput.text + telefoneInput.text;
        
        if (sexoDd.enabled)
            jsonCadastro.sexo = sexoDd.value;
        jsonCadastro.device_id = SystemInfo.deviceUniqueIdentifier;
        jsonCadastro.mac_address = GetMacAddress();

        if (cepInput.text != "")
            jsonCadastro.cep = cepInput.text;

        if (ruaInput.text != "")
            jsonCadastro.rua = ruaInput.text;

        if (bairroInput.text != "")
            jsonCadastro.bairro = bairroInput.text;

        if (numeroInput.text != "")
            jsonCadastro.numero = int.Parse(numeroInput.text);

        if (complementoInput.text != "")
            jsonCadastro.complemento = complementoInput.text;

        if (cidadeInput.text != "")
            jsonCadastro.cidade = cidadeInput.text;

        if (estadoDd.captionText.text != "Selecione")
            jsonCadastro.estado = estadoDd.captionText.text;

        if (novaSenhaInput)
        {
            jsonCadastro.novaSenha = Base64Encode(novaSenhaInput.text);

            //se o campo nova senha estiver vazio envia a senha antiga no campo novasenha.
            if (novaSenhaInput.text == "")
                jsonCadastro.novaSenha = jsonCadastro.senha;
        }

        //data.
        jsonCadastro.dt_nasc = ConvertBRToUSFormat(idadeInput.text);
        string json = JsonUtility.ToJson(jsonCadastro, true);
        Debug.Log(json);

//		onConnection = true;

        StartCoroutine(ISendJson(json));
        onConnection = true;
    }

    public void onChangeSexo(int value)
    {
        if(sexoDd.options.Count > 2)
            sexoDd.options.Remove(sexoDd.options[2]);
    }

    private bool CheckData()
    {
		if(politicaPriv && politicaFeidl)
		{

			if(!politicaPriv.isOn){
				PopUp("Favor aceitar os termos da Política de Privacidade");
				return false;
			}

			if(!politicaFeidl.isOn){
				PopUp("Favor aceitar os termos do Programa de Fidelidade");
				return false;
			}	

		}

        if (nomeInput.text == "")
        {
            PopUp("Preencha corretamente o nome ou razão social!");
        }
        else if (sobreNomeInput.text == "")
        {
            PopUp("Preencha corretamente o sobrenome ou o nome fantasia!");
        }
        else if (!IsCpf(cpfInput.text) && !IsCnpj(cpfInput.text) && levelName == "Cadastro")
        {
            if (senhaInput.text == "")
            {
                PopUp("Digite sua senha!");
                return false;
            }
            PopUp("CPF ou CNPJ inválido, verifique!");
        }

		/*
        else if (dddInput.text == "")
        {
            PopUp("Preencha o campo DDD!");
        }
        else if (telefoneInput.text == "")
        {
            PopUp("Preencha o campo Telefone!");
        }


        else if (cepInput.text == "")
        {
            PopUp("Preencha corretamente o seu CEP!");
        }

        else if (ruaInput.text == "")
        {
            PopUp("Preencha corretamente o campo Rua!");
        }
        else if (bairroInput.text == "")
        {
            PopUp("Preencha corretamente o campo Bairro!");
        }
        else if (numeroInput.text == "")
        {
            PopUp("Preencha corretamente o campo Nº!");
        }

		*/
//        else if (estadoDd.captionText.text == "Selecione")
//        {
//            PopUp("Selecione o seu Estado!");
//        }
		/*
        else if (emailInput.text == "")
        {
            PopUp("Preencha o campo E-mail!");
        }
		/*
        /*else if (emailInput.text != reemailInput.text/* && Application.loadedLevelName == "Cadastro")
		{
            PopUp("Os dois campos de e-mail devem estar iguais, verifique!");
		}*/

//        else if (sexoDd.value == 2 /*&& levelName == "Cadastro" && Application.platform == RuntimePlatform.Android && false*/)
//        {
//            PopUp("Favor selecionar o gênero, verifique!");
//        }

        else if (!IsValid(emailInput.text)/* && levelName == "Cadastro"*/)
        {
            PopUp("E-mail inválido!");
        }

//        else if (!ValidadeDate(idadeInput.text)/* && Application.loadedLevelName == "Cadastro" && Application.platform == RuntimePlatform.Android && false*/)
//        {
//            PopUp("Preencha corretamente a data de nascimento!");
//        }

        else if (senhaInput.text != resenhaInput.text && levelName == "Cadastro")
        {
            PopUp("Os dois campos da senha devem ser iguais, verifique!");
        }
        else if (novaSenhaInput.text != resenhaInput.text && levelName == "Atualizar")
        {
            PopUp("Os dois campos da senha devem ser iguais, verifique!");
        }
        else if (senhaInput.text.Length < 4 && levelName == "Cadastro")
        {
            PopUp("Digite no minimo 4 digitos na senha!");
        }

        else if (novaSenhaInput.text != "" && novaSenhaInput.text.Length < 4 && levelName == "Atualizar")
        {
            PopUp("Digite no minimo 4 digitos na senha!");
        }

		else if(levelName == "Atualizar"){
			if((novaSenhaInput.text != "" || senhaInput.text != "") && senhaInput.text != Login.userData.senha && levelName == "Atualizar")
			{
				PopUp("Sua senha atual está errada!");
			}else
				{
					return true;
				}
		}
			        
        else
        {
            return true;
        }

        return false;
    }

    public bool IsValid(string emailaddress)
    {
        if (emailaddress == "")
            return true;
        
        Debug.Log(emailaddress);
        return  System.Text.RegularExpressions.Regex.IsMatch(emailaddress, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{1,4}|[0-9]{1,3})(\]?)$");
    }

    private IEnumerator ISendJson(string json)
    {
        if (loadingObj)
            loadingObj.SetActive(true);
		

		//Manda dados JSON

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        //headers.Add("charset", "utf-8");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/../cadastro.php", pData, headers);

        yield return www;

        Debug.Log("WWW url: " + www.url);

        onConnection = false;

        if (loadingObj)
            loadingObj.SetActive(false);

        if (!string.IsNullOrEmpty(www.error))
        {
            PopUp(www.error);
        }
        else
        {
            Debug.Log(www.text);
            PopUp(www.text);

            if (www.text.Contains("sucesso"))
            {
                //atualiza objeto userData instanciado no login.
                if (levelName == "Atualizar")
                {
                }
                else
                {
                    Login.userData = new UserData();
                }

                Login.userData.nome = jsonCadastro.nome;
                Login.userData.sobrenome = jsonCadastro.sobrenome;
                Login.userData.cpf = jsonCadastro.cpf;
                Login.userData.email = jsonCadastro.email;
                Login.userData.telefone = jsonCadastro.telefone;
                Login.userData.idade = jsonCadastro.idade;
                Login.userData.sexo = jsonCadastro.sexo;
                Login.userData.dt_nasc = jsonCadastro.dt_nasc;
                Login.userData.cep = jsonCadastro.cep;
                Login.userData.rua = jsonCadastro.rua;
                Login.userData.bairro = jsonCadastro.bairro;
                Login.userData.numero = jsonCadastro.numero;
                Login.userData.cidade = jsonCadastro.cidade;
                Login.userData.estado = jsonCadastro.estado;
                Login.userData.complemento = jsonCadastro.complemento;

                if (levelName == "Atualizar")
                {
                    if (novaSenhaInput.text != "")
                        Login.userData.senha = novaSenhaInput.text;
                    //else if (senhaInput.text != "")
                    //        Login.userData.senha = senhaInput.text;

                }
                else
                {
                    if(senhaInput.text != "")
                        Login.userData.senha = senhaInput.text;
                }
                Login.isLogged = true;


				//Manda foto

				//Veja se o user tem foto
				//Textura criada quando clicou no btn da foto
				Texture2D localFile = Login.photo_profile;

				if(localFile == null){

					Debug.Log("Não escolheu foto");

				}else{
					Debug.Log("Upload da foto");

					//Já recebeu a confirmação do server, espera um pouco para mandar a foto
					yield return new WaitForSeconds(1f);

					//Form para envio da foto
					WWWForm postForm = new WWWForm();

					postForm.AddBinaryData("foto_perfil", localFile.EncodeToJPG(), "foto_perfil.jpg", "image/jpg");
					//Campos do form (tela)

					//Se for atualizar, pega a senha interna
					if (levelName == "Atualizar")
					{
						postForm.AddField("login", Login.userData.cpf);
						postForm.AddField("senha", Login.Base64Encode(Login.userData.senha));
					}
					//Se não usa a senha que foi digitada no campo
					else
					{
						postForm.AddField("login",	jsonCadastro.cpf);
						postForm.AddField("senha", 	Login.Base64Encode(senhaInput.text));
					}	

					Debug.Log("Upload da foto:");
					Debug.Log("User CPF par foto: " + jsonCadastro.cpf);
					Debug.Log("User senha para cadastrar foto:" + Login.Base64Encode(senhaInput.text));
					Debug.Log("User senha para atualizar foto:" + Login.Base64Encode(Login.userData.senha));

					//Url para upload da foto
					string uploadURL = ServerControl.url + "/foto_perfil.php";

					//Comeca uma nova requisicao
					ClosePopUp();


					if (loadingObj)
						loadingObj.SetActive(true);
					
					WWW upload = new WWW(uploadURL, postForm);
					yield return upload;

					if (loadingObj)
						loadingObj.SetActive(false);

					if (!string.IsNullOrEmpty(upload.error))
					{
						PopUp(upload.error);
					}
					else
					{
						Debug.Log(upload.text);
						PopUp(upload.text);
					}

//					if (upload.error == null)
//					{
//						Debug.Log("upload done :" + upload.text);
//					}
//					else
//						Debug.Log("Error during upload: " + upload.error);
				}


//                yield return new WaitForSeconds(3f);
				//Tempo para user ler popUp
				yield return new WaitForSeconds(1f);

				ClosePopUp();


                StartCoroutine(GoLevel());
            }

        }
    }

	public void ClosePopUp(){
		popup.SetActive(false);
	}

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);
        if (txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }
        else if (txt.Contains("Dados alterados com sucesso"))
        {
            popup.GetComponentInChildren<Button>().transform.parent.gameObject.SetActive(false);
        }
        else if (txt.Contains("Cadastro realizado com sucesso"))
        {
            popup.GetComponentInChildren<Button>().gameObject.SetActive(false);
        }

        popup.GetComponentInChildren<Text>().text = txt;
        popup.SetActive(true);
    }

    public void ButtonCupons()
    {
        if (loadingObj)
            loadingObj.SetActive(true);

        if (Login.isLogged)
            Close();
        else
            Application.LoadLevel("Login");
    }

    public void Close()
    {
        if (!back)
        {
            if (loadingObj)
                loadingObj.SetActive(true);

            back = true;

            if (Login.isLogged)
                StartCoroutine(GoLevel());
            else
                Application.LoadLevel("Login");

        }
    }

    public IEnumerator GoLevel()
    {
        if (loadingObj)
            loadingObj.SetActive(true);

        yield return new WaitForEndOfFrame();


        if (Login.isLogged)
        {
            //AsyncOperation async = Application.LoadLevelAsync("Menu_Nagumo");

            if (Application.loadedLevelName == "Atualizar")
            {
                //yield return async;
                UnityEngine.SceneManagement.SceneManager.UnloadScene("Atualizar");
            }
            else
            {
                Application.LoadLevel("Menu_Nagumo");
            }
        }
        else
        {
            Application.LoadLevel(0);
        }

    }

    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    public static string GetMacAddress()
    {
#if UNITY_ANDROID
        AndroidJavaObject mWiFiManager = null;

        try
        {
            string macAddr = "";
            if (mWiFiManager == null)
            {
                using (AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    mWiFiManager = activity.Call<AndroidJavaObject>("getSystemService", "wifi");
                }
            }
            macAddr = mWiFiManager.Call<AndroidJavaObject>("getConnectionInfo").Call<string>("getMacAddress");
            return macAddr;
        }
        catch
        {
            Debug.Log("erro mac address");    
        }
#else
        var macAdress = "";
		NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

		foreach (NetworkInterface adapter in nics)
		{
		    PhysicalAddress address = adapter.GetPhysicalAddress();
			if (address.ToString() != "")
			{
				macAdress = address.ToString();
				return macAdress;
			}
		}
#endif

        return "20:00:00:00:00:00";
    }

    public static bool IsCnpj(string cnpj)
    {
        int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        int soma;
        int resto;
        string digito;
        string tempCnpj;
        cnpj = cnpj.Trim();
        cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
        if (cnpj.Length != 14)
            return false;
        tempCnpj = cnpj.Substring(0, 12);
        soma = 0;
        for (int i = 0; i < 12; i++)
            soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
        resto = (soma % 11);
        if (resto < 2)
            resto = 0;
        else
            resto = 11 - resto;
        digito = resto.ToString();
        tempCnpj = tempCnpj + digito;
        soma = 0;
        for (int i = 0; i < 13; i++)
            soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
        resto = (soma % 11);
        if (resto < 2)
            resto = 0;
        else
            resto = 11 - resto;
        digito = digito + resto.ToString();
        return cnpj.EndsWith(digito);
    }

    public static bool IsCpf(string cpf)
    {
        int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
        int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
        string tempCpf;
        string digito;
        int soma;
        int resto;
        cpf = cpf.Trim();
        cpf = cpf.Replace(".", "").Replace("-", "");
        if (cpf.Length != 11)
            return false;
        tempCpf = cpf.Substring(0, 9);
        soma = 0;

        for (int i = 0; i < 9; i++)
            soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
        resto = soma % 11;
        if (resto < 2)
            resto = 0;
        else
            resto = 11 - resto;
        digito = resto.ToString();
        tempCpf = tempCpf + digito;
        soma = 0;
        for (int i = 0; i < 10; i++)
            soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
        resto = soma % 11;
        if (resto < 2)
            resto = 0;
        else
            resto = 11 - resto;
        digito = digito + resto.ToString();
        return cpf.EndsWith(digito);
    }
}

[System.Serializable]
public class JsonCadastro
{
    public string acao;
    public string nome;
    public string sobrenome;
	public string cpf;
	public string email;
	public string telefone;
	public int idade;
	public int sexo; // 0 masculino, 1 feminino.
	public string senha;
	public string novaSenha;
	public string device_id;
	public string mac_address;
    public string dt_nasc;
    public string cep;
    public string rua;
    public string bairro;
    public int numero;
    public string complemento;
    public string estado;
    public string cidade;
}

[System.Serializable]
public class CEP
{
	public string cep;
	public string logradouro;
	public string complemento;
	public string bairro;
	public string localidade;
	public string uf;

}


