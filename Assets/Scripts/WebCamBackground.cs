﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebCamBackground : MonoBehaviour {

    public WebCamTexture camTexture;

    public void Start()
    {
        //set up camera
        WebCamDevice[] devices = WebCamTexture.devices;
        string backCamName = "";
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log("Device:" + devices[i].name + "IS FRONT FACING:" + devices[i].isFrontFacing);

            if (!devices[i].isFrontFacing)
            {
                backCamName = devices[i].name;
            }
        }

        camTexture = new WebCamTexture(backCamName, 1000, 1000, 30);
        camTexture.Play();
        GetComponent<Renderer>().material.mainTexture = camTexture;
    }

    private void OnApplicationQuit()
    {
        camTexture.Stop();
    }

    private void OnDestroy()
    {
        camTexture.Stop();
    }

    private void OnDisable()
    {
        camTexture.Stop();
    }
}
