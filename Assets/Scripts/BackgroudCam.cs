﻿using UnityEngine;
using System.Collections;
 using UnityEngine.UI;

public class BackgroudCam : MonoBehaviour
{

    public WebCamTexture mCamera = null;
    public float bias = 0.1f;


    public int _cameraIndex=0;

    private bool back;

    WebCamDevice[] devices;

    
	// Take a shot immediately
	IEnumerator Start()
	{
//		openCamera();
		StartCoroutine (openCamera());
		yield return null;
	}


	IEnumerator openCamera(){
		
		// We should only read the screen after all rendering is complete
		yield return new WaitForEndOfFrame();


		if(_cameraIndex == 1){

			WebCamDevice device = WebCamTexture.devices[_cameraIndex];
			mCamera = new WebCamTexture(device.name);
			gameObject.GetComponent<Renderer>().material.mainTexture = mCamera;
			mCamera.Play();

		}else{



			mCamera = new WebCamTexture(480, 640);

			gameObject.GetComponent<Renderer>().material.mainTexture = mCamera;
			mCamera.Play();
			//transform.localScale = new Vector3(mCamera.width, mCamera.height) * bias;
		}

		
	}
	/*
    // Use this for initialization
	void Start()
    {

        if(_cameraIndex == 1){

            WebCamDevice device = WebCamTexture.devices[_cameraIndex];
            mCamera = new WebCamTexture(device.name);
            gameObject.GetComponent<Renderer>().material.mainTexture = mCamera;
            mCamera.Play();
                        
        }else{


            mCamera = new WebCamTexture(480, 640);

            gameObject.GetComponent<Renderer>().material.mainTexture = mCamera;
            mCamera.Play();
            //transform.localScale = new Vector3(mCamera.width, mCamera.height) * bias;
        }
			


#if UNITY_IOS
        transform.localEulerAngles = new Vector3(0,180,-90);
#endif

    }
	*/


    public void BackButton()
    {
        mCamera.Stop();
        Close();
    }

	public void Close()
	{
		if (!back)
		{
			back = true;
			StartCoroutine(GoLevel());
		}
	}

	public void OnDisable()
	{
        mCamera.Stop();
	}

	public void OnDestroy()
	{
        mCamera.Stop();
	}

	public IEnumerator GoLevel()
	{

        mCamera.Stop();

		yield return new WaitForEndOfFrame();


		AsyncOperation async = Application.LoadLevelAsync("Menu_Nagumo");


		yield return async;


	}

            public void fakeScreenShot(){                        
            mCamera.Pause();
        }


}
