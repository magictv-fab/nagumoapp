﻿using UnityEngine;
using System.Collections;
using MagicTV.abstracts ;
using MagicTV.globals;
using MagicTV.globals.events;
using MagicTV.vo;
using MagicTV.abstracts.device;
using System.Linq;
using System.Collections.Generic;
using MagicTV.utils;
using ARM.components;
using InApp;
using MagicTV.download;
using MagicTV.processes;
using MagicTV.abstracts.screens;

public class CloseBundle : MonoBehaviour {

	public void Close()
	{
		AppRootEvents.GetInstance ().RaiseStopPresentation ();
	
		StartCoroutine(Dest());
	}

	private IEnumerator Dest()
	{
		yield return new WaitForSeconds(0.5f);
		Destroy(transform.parent.parent.parent.gameObject);
	}
}
