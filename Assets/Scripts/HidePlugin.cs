﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class HidePlugin : MonoBehaviour{


	public void HideUnity()
	{			

		Debug.Log ("UNITY: CHAMOU a funcao \"close\" na current activity Android"); 


		#if (UNITY_IOS || UNITY_IPHONE)


		#else
		try
		{

			//AndroidJavaClass androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			//AndroidJavaObject jo2= androidJC.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaClass jc = new AndroidJavaClass("br.com.magictv.palmeirastv.UnityPlayerActivity");

			jc.CallStatic("close");
			jc.Call("close");
		}
		catch
		{
			Debug.LogError("UNITY: Nao conseguiu chamar o metodo close no endereco: br.com.magictv.palmeirastv.UnityPlayerActivity , tente colocar ele estatico");
			//Application.Quit();
		}
		#endif

	}
}
