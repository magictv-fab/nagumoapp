﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class ItemMoveDrag : MonoBehaviour {

    public Vector3 velocity;
    public float minVelToSelect = 0.1f;
    public float rotFactor = 0.05f;
    public iTween.EaseType easetype = iTween.EaseType.easeOutSine;
    public float easetypeTime = 0.85f;
    public Image allowImage, denyImage;
    public UnityEvent onAllow, onDeny, onSelected;

    private RectTransform rectTransform;
    private Vector3 mousePosition;
    private bool onSelectArea; // se esta na area de selecao ou nao.
    private Vector2 lastPosition;
    private ScrollRect scrollRect;
    private bool isSelected;

	// Use this for initialization
	void Start () 
    {
        rectTransform = GetComponent<RectTransform>();
        scrollRect = GetComponentInParent<ScrollRect>();

        //scrollRect.content = rectTransform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //inclinacao.
        float rot = rectTransform.anchoredPosition.x;

        //inclinacao para um lado ou para outro.
        if (Input.GetMouseButtonDown(0))
        {
            mousePosition = Input.mousePosition;
        }

        transform.localEulerAngles = new Vector3(0, 0, rot * rotFactor * -Mathf.Sign(Screen.height / 2.5f - mousePosition.y));

        //verifica se esta na area para selecao.
        onSelectArea = Mathf.Abs(rectTransform.anchoredPosition.x) > Screen.width * 0.5f;

        //verifica se o usuario soltou o dedo e se aceitou ou recusou.
        if(Input.GetMouseButtonUp(0) && !isSelected)
        {
            if(Mathf.Abs(velocity.x) > minVelToSelect || onSelectArea)
            {
                if (!OfertasManager.currentItem)
                {
                    OfertasManager.currentItem = OfertasManager.instance.containerItens.transform.GetChild(0).gameObject;
                }

                OfertasManager.instance.selectedItem = OfertasManager.currentItem;


                if(rectTransform.anchoredPosition.x < 0)
                {
                    if(onDeny != null)
                    {
                        onDeny.Invoke();    
                    }

                    Debug.Log("Recusou");
                    OfertasManager.instance.SendStatus(0);

                }
                else if(rectTransform.anchoredPosition.x > 0)
                {
                    if(onAllow != null)
                    {
                        onAllow.Invoke();    
                    }

                    OfertasManager.instance.SendStatus(1);

                    Debug.Log("Aceitou");
                }

                //animacao.
                SelectAnim();

                //tratativas da selecao.
                OnSelect();
            }
        }

        //fade nas imagens.
        allowImage.color = new Color(1f, 1f, 1f, Mathf.InverseLerp(0, Screen.width * 0.25f, rectTransform.anchoredPosition.x));
        denyImage.color = new Color(1f, 1f, 1f, Mathf.InverseLerp(0, Screen.width * -0.25f, rectTransform.anchoredPosition.x));

        //calcula a velocidade.
        velocity = (lastPosition - rectTransform.anchoredPosition) * Time.deltaTime;
        lastPosition = rectTransform.anchoredPosition;
    }

    public void OnSelect()
    {
        if (transform.parent.childCount <= 0)
            return;
        
            isSelected = true;

            //seleciona o primeiro item para ser o alvo do toque.
        if (transform.parent.childCount <= 1)
             scrollRect.content = transform.parent.GetChild(transform.parent.childCount - 1).GetComponent<RectTransform>();
        else
             scrollRect.content = transform.parent.GetChild(transform.parent.childCount - 2).GetComponent<RectTransform>();

        //tira esse item do container.
        transform.SetParent(transform.parent.parent, false);

        if (onSelected != null)
            onSelected.Invoke();        
    }

    public void SelectAnim()
    {

        Hashtable ht = new Hashtable();
        ht.Add("position", transform.position + transform.right * (Screen.width * 3.5f) * Mathf.Sign(rectTransform.anchoredPosition.x));
        ht.Add("speed", easetypeTime * 100f);
        ht.Add("easetype", easetype);
        ht.Add("oncompletetarget", this.gameObject);
        ht.Add("oncomplete", "DestroyNow");

        iTween.MoveTo(gameObject, ht);
    }

    public void DestroyNow()
    {
        Destroy(gameObject);   
    }
}
