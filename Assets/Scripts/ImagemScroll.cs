﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ImagemScroll : MonoBehaviour 
{
    public Dropdown dropdown;
    public ScrollRect scrollRect;
    public List<GameObject> infoRegionLst = new List<GameObject>();

	// Use this for initialization
	void Start () 
    {
       // dropdown.OnPointerClick()    
	}
	

	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnSelect(int value)
    {
        for (int i = 0; i < infoRegionLst.Count; i++)
        {
            infoRegionLst[i].SetActive(value == i);
        }

        scrollRect.content = infoRegionLst[value].GetComponent<RectTransform>();
        scrollRect.CalculateLayoutInputVertical();
    }

    public void HideAll()
    {
        //gambiarra para quando clicar no mesmo ele nao ignorar o OnChangeValue.
       // dropdown.spriteState.pressedSprite ==;

        for (int i = 0; i < infoRegionLst.Count; i++)
        {
            infoRegionLst[i].SetActive(false);
        } 
    }
}
