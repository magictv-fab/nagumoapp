﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CuponsControllerNagumo : MonoBehaviour {

    public static CuponsControllerNagumo instance;
    public List<GameObject> cuponsLst = new List<GameObject>();
    public Text saldoTxt, infoTxt;
    public int pontos;
    public GameObject content;
    public GameObject loadingObj, popup;

    private List<GameObject> cuponsInstantiatedsLst = new List<GameObject>();

	// Use this for initialization
	void Start () 
    {
        instance = this;
        pontos = Login.userData.pontos_disponiveis;
        Instantiation();

        if(pontos < 10)
        {
            infoTxt.text = "Você ainda não possui pontos suficientes para gerar um cupom.";
        }

        UpdateTxt();
    }
	
    public void ClearAll()
    {
        foreach (GameObject go in cuponsInstantiatedsLst)
            Destroy(go);

        cuponsInstantiatedsLst.Clear();
    }

	public void Instantiation()
    {
        if (pontos >= 100)
        {
            Inst(0);
            Inst(1);
            Inst(2);
            Inst(3);
            Inst(4);
        }
        else if (pontos >= 50)
        {
            Inst(0);
            Inst(1);
            Inst(2);
            Inst(3);
        }
        else if (pontos >= 30)
        {
            Inst(0);
            Inst(1);
            Inst(2);
        }
        else if (pontos >= 20)
        {
            Inst(0);
            Inst(1);
        }
        else if (pontos >= 10)
        {
            Inst(0);
        }
    }

    private void Inst(int index)
    {
        var obj = Instantiate(cuponsLst[index]);
        obj.transform.parent = content.transform;
        obj.transform.localScale = Vector3.one * 0.9f;

        cuponsInstantiatedsLst.Add(obj);
    }

    // Update is called once per frame
	public void UpdateTxt () 
    {
        saldoTxt.text = pontos.ToString() + " pontos";
	}

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);
        if (txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }
        popup.SetActive(true);

        popup.GetComponentInChildren<Text>().text = txt;
    }
}
