﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CuponsControl : MonoBehaviour {

    public bool showActiveCupons;
    public GameObject container, cuponPrefab, loadingObj, cuponDoceNovembroPrefab;
    public List<GameObject> cuponsLst = new List<GameObject>();
    public Dictionary<string, GameObject> codeObjDict = new Dictionary<string, GameObject>();
    public GameObject semCuponsImg, semCuponsResgatados;

    private float timeOfUpdate = 30;
    private GameObject obj;

    IEnumerator Start()
    {
        UpdateData.onUpdate = true;

        while (UpdateData.onUpdate)
            yield return new WaitForEndOfFrame();
        
        //instancia e trata o cupon.
        for (int i = 0; i < Login.userData.cupons.Length; i++)
        {
            if (showActiveCupons && !Login.userData.cupons[i].used)
            {
                //instancia o cupom referente a promocao dele.
//                if (Login.userData.cupons[i].promocao == 0)
//                    obj = Instantiate(cuponDoceNovembroPrefab);
//                else
                obj = Instantiate(cuponDoceNovembroPrefab);
                
                obj.transform.parent = container.transform;
                obj.transform.SetAsFirstSibling();
                obj.transform.localScale = Vector3.one;

                obj.GetComponent<Cupons>().code = Login.userData.cupons[i].id;
                obj.GetComponent<Cupons>().used = Login.userData.cupons[i].used;
                obj.GetComponent<Cupons>().cupomValue = Login.userData.cupons[i].valor;
                obj.GetComponent<Cupons>().dateText.text = System.DateTime.Parse(Login.userData.cupons[i].dataExpira).ToString("dd/MM/yyyy");

                cuponsLst.Add(obj);
                codeObjDict.Add(Login.userData.cupons[i].id, obj);

                if (semCuponsImg)
                    semCuponsImg.SetActive(false);

                if (System.DateTime.Parse(Login.userData.cupons[i].dataExpira) < System.DateTime.Today)
                {
                    obj.GetComponent<Cupons>().expiredObj.SetActive(true);
                }

            }
            else if (/*!showActiveCupons &&*/ Login.userData.cupons[i].used)
            {
                //instancia o cupom referente a promocao dele.
//                if (Login.userData.cupons[i].promocao == 0)
//                    obj = Instantiate(cuponDoceNovembroPrefab);
//                else
                obj = Instantiate(cuponDoceNovembroPrefab);

                obj.transform.parent = container.transform;
                obj.transform.localScale = Vector3.one;

                obj.GetComponent<Cupons>().code = Login.userData.cupons[i].id;
                obj.GetComponent<Cupons>().used = Login.userData.cupons[i].used;
                obj.GetComponent<Cupons>().cupomValue = Login.userData.cupons[i].valor;
                obj.GetComponent<Cupons>().dateText.text = System.DateTime.Parse(Login.userData.cupons[i].dataExpira).ToString("dd/MM/yyyy");

                cuponsLst.Add(obj);
                codeObjDict.Add(Login.userData.cupons[i].id, obj);

                if (semCuponsResgatados)
                    semCuponsResgatados.SetActive(false);

                obj.GetComponent<Cupons>().usedObj.SetActive(true);
            }

            yield return new WaitForEndOfFrame();
		}



        StartCoroutine(UpdateDt());

    }

    private IEnumerator UpdateDt()
    {
        yield return new WaitForSeconds(timeOfUpdate);
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Base64Encode(Login.userData.senha) + "\"}";

        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/login.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            //PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            //[0] = code. [1] = message;
            string code = www.text.Split(',')[0];
            string message = www.text.Remove(0, code.Length + 1);
            Debug.Log(code);
            Debug.Log(message);

            if (code.Contains("200"))
            {
                //popula obj json.
                UserData userData = JsonUtility.FromJson<UserData>(message);
                Login.userData.cupons = userData.cupons;
                //Login.userData.spins = userData.spins;

                for (int i = 0; i < userData.cupons.Length; i++)
                {
                    if (userData.cupons[i].used)
                    {
                        codeObjDict[userData.cupons[i].id].GetComponent<Cupons>().usedObj.SetActive(true);
                        //desativa se o cupom foi usado se estiver na tela dos cupons ativos.
                       // if(showActiveCupons)
                        //if(codeObjDict.ContainsKey(userData.cupons[i].id))
                         //   codeObjDict[userData.cupons[i].id].SetActive(false);
                    }
                    else if(System.DateTime.Parse(userData.cupons[i].dataExpira) < System.DateTime.Today)
                    {
                        codeObjDict[userData.cupons[i].id].GetComponent<Cupons>().expiredObj.SetActive(true);
                    }

                    yield return new WaitForEndOfFrame();
                }

                Debug.Log("Update OK");
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
            else
            {
                // PopUp(message);
                if (loadingObj)
                    loadingObj.SetActive(false);
            }

        }      

        StartCoroutine(UpdateDt());
    }

    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }
}
