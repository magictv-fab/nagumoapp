﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Linq;

public class PublishManager : MonoBehaviour
{
    public static PublicationsData publicationsData;
    public static PublishManager instance;

    public List<GameObject> toggleList = new List<GameObject>();

    public List<string> titleLst = new List<string>(),
                        valueLst = new List<string>(),
                        infoLst = new List<string>(),
                        idLst = new List<string>(),     
                        linkLst = new List<string>(),     
                        urlImgLst = new List<string>();

    public List<bool> favoritouLst = new List<bool>();

    public List<Sprite> imgLst = new List<Sprite>();

    public GameObject loadingObj, popup;
    public GameObject itemPrefab;
    public GameObject containerItens;
    public List<GameObject> itensList = new List<GameObject>();
    public UnityEngine.UI.Extensions.HorizontalScrollSnap horizontalScrollSnap;
    public ScrollRect scrollRect;

    private int ofertaCounter;
    public GameObject currentItem;
    private GameObject selectedItem;

    //para comparar se tem atualizacao.
    private static string currentJsonTxt;

    // Use this for initialization
    void Start()
    {
        instance = this;

        titleLst.Clear();
        valueLst.Clear();
        infoLst.Clear();
        imgLst.Clear();

        StartCoroutine(ILoadPublish());

        //para o update.
        LoaderPublishAnim.instance.onLoad.AddListener(LoadPublishUpdate);

    }

    public void InstantiateNextItem()
    {
        if (ofertaCounter >= titleLst.Count)
            return;

        var obj = Instantiate(itemPrefab);
        obj.transform.SetParent(containerItens.transform, false);
        obj.transform.localScale = Vector3.one * 0.87f;
        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        OfertaValues ofertaValues = obj.GetComponent<OfertaValues>();

        int indexInvertido = titleLst.Count - ofertaCounter - 1;

        ofertaValues.value.text = valueLst[indexInvertido];
        ofertaValues.info.text = infoLst[indexInvertido];
        ofertaValues.img.sprite = imgLst[indexInvertido];
        ofertaValues.saveImageSource = imgLst[indexInvertido].texture;
        ofertaValues.id = idLst[indexInvertido];
        ofertaValues.favoritou = favoritouLst[indexInvertido];
        ofertaValues.imgLink = urlImgLst[indexInvertido];
        ofertaValues.link = linkLst[indexInvertido];

        obj.transform.SetAsFirstSibling();
        currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

        itensList.Add(obj);

        //liga o toggle embaixo.
        toggleList[ofertaCounter].SetActive(true);

        ofertaCounter++;
    }

    public void SendStatus(int status)
    {
        StartCoroutine(ISendStatus(selectedItem.GetComponent<OfertaValues>().id, status));
    }

    public void Favoritar(string id)
    {
        StartCoroutine(IFavoritar(id));
    }

    public void DesFavoritar(string id)
    {
        StartCoroutine(ExcluirFavorito(id));
    }

    public void BtnAccept()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * 2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onAllow.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(1);
    }

    public void BtnRecuse()
    {
        if (!currentItem)
        {
            currentItem = containerItens.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * -2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onDeny.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(0);
    }

    private IEnumerator IFavoritar(string id)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_publicacao\":\"" + id + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/salvar_publicacao_favoritos_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ISendStatus(string id, int status)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"status_oferta\":\"" + status + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/salvar_publicacao_favoritos_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ExcluirFavorito(string id)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_publicacao\":\"" + id + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/excluir_favorito_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ILoadPublish()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_publicacao_favoritos.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            currentJsonTxt = www.text;

            try
            {
                string message = SimpleJSON.EscapeString(www.text);
                Debug.Log(message);

                //popula obj json.
                publicationsData = JsonUtility.FromJson<PublicationsData>(message);

                //passa as informacoes para as listas.
                foreach (PublicationData publicationData in publicationsData.publicacoes)
                {
                    titleLst.Add(publicationData.titulo);
                    valueLst.Add(publicationData.datainicial);
                    infoLst.Add(publicationData.texto);
                    idLst.Add(publicationData.id_publicacao.ToString());
                    favoritouLst.Add(publicationData.favoritou);
                    linkLst.Add(publicationData.link);

                    if (titleLst.Count >= 12)
                        break;
                }

                StartCoroutine(ILoadImgs());
            }
            catch(System.Exception ex)
            {
                PopUp(ex.ToString());
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
        }
    }

    public void LoadPublishUpdate()
    {
        StartCoroutine(ILoadPublishUpdate());
    }

    public IEnumerator ILoadPublishUpdate()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_publicacao_favoritos.php", pData, headers);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            //verifica se o txt que chegou eh diferente do que tem ja.
            if(currentJsonTxt != www.text)
            {
                Debug.Log("Tem atualizacao!");
                currentJsonTxt = www.text;

                //Atualiza:
                if (loadingObj)
                    loadingObj.SetActive(true);

                LoaderPublishAnim.instance.EndLoad();

                foreach (GameObject obj in itensList)
                    Destroy(obj);

                itensList.Clear();

                titleLst.Clear();
                valueLst.Clear();
                infoLst.Clear();
                idLst.Clear();
                favoritouLst.Clear();
                imgLst.Clear();
                linkLst.Clear();

                ofertaCounter = 0;

                try
                {
                    string message = SimpleJSON.EscapeString(www.text);
                    Debug.Log(message);

                    //popula obj json.
                    publicationsData = JsonUtility.FromJson<PublicationsData>(message);

                    //passa as informacoes para as listas.
                    foreach (PublicationData publicationData in publicationsData.publicacoes)
                    {
                        titleLst.Add(publicationData.titulo);
                        valueLst.Add(publicationData.datainicial);
                        infoLst.Add(publicationData.texto);
                        idLst.Add(publicationData.id_publicacao.ToString());
                        favoritouLst.Add(publicationData.favoritou);
                        linkLst.Add(publicationData.link);

                        if (titleLst.Count >= 12)
                            break;
                    }

                    StartCoroutine(ILoadImgs());
                }
                catch (System.Exception ex)
                {
                    PopUp(ex.ToString());
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }

        }

        LoaderPublishAnim.instance.EndLoad();
    }

    private IEnumerator ILoadImgs()
    {
        foreach (PublicationData publicationData in publicationsData.publicacoes)
        {
            WWW www = new WWW(ServerControl.urlIMGPublicacoes + "/" + publicationData.imagem);
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                Sprite spt = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
                imgLst.Add(spt);
                urlImgLst.Add(www.url);
            }
        }


        //scrollRect = GetComponent<ScrollRect>();

        //inicializa com 2 cartoes de oferta.
        for (int i = 0; i < titleLst.Count; i++)
        {
            InstantiateNextItem();
        }

        yield return new WaitForEndOfFrame();

        if (horizontalScrollSnap)
        {
            horizontalScrollSnap.InitialiseChildObjectsFromScene();
            horizontalScrollSnap.UpdateLayout();
            scrollRect.content.localPosition = Vector3.zero;
            scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scrollRect.content.rect.width + currentItem.GetComponent<RectTransform>().rect.width / 2f);
        }


        // scrollRect.content = containerItens.transform.GetChild(containerItens.transform.childCount - 1).GetComponent<RectTransform>();

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);


        if (txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }

        if (popup)
        {
            popup.GetComponentInChildren<Text>().text = txt;
            popup.SetActive(true);
        }
    }

    public void Logout()
    {
        PlayerPrefs.SetInt("login", 0);
        Login.isLogged = false;
        Login.userData = null;
        Login.photo_profile = null;
        Application.LoadLevel(0);
        Login._analytics.SendEvent("level-", "login");
    }

    private void OrderInverter()
    {
        titleLst.Sort();
        valueLst.Sort();
        infoLst.Sort();
        idLst.Sort();
        favoritouLst.Sort();
        imgLst.Sort();
        linkLst.Sort();
        urlImgLst.Sort();
    }

    private void OnApplicationQuit()
    {
        Login._analytics.SendEvent("magictv", "stop");
        Login._analytics.Close();
        Debug.Log("Fechou");
    }
}
