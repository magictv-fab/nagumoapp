﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class VerticalSwap : MonoBehaviour
{

    public float minAxisY = 0.1f;
    public float maxAxisX = 0.5f;
    public float multiplier = 2f;
    public ScrollRect scrollRect;
    public UnityEvent onScroll;

    private Vector3 startPosition;
    private bool onScrollFlag, endUpdate;

    // Use this for initialization
    void Start()
    {
        startPosition = transform.localPosition;
        LoaderPublishAnim.instance.onLoad.AddListener(EndUpdate);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && !endUpdate)
        {
#if UNITY_EDITOR

            float velocity = Input.GetAxis("Mouse Y") * multiplier;

            if (((velocity < minAxisY/4f || transform.localPosition.y < startPosition.y)
                 && Mathf.Abs(Input.GetAxis("Mouse X")) < maxAxisX) || onScrollFlag)
            {
                Vector3 pos = transform.localPosition;
                pos.y += velocity;
                transform.localPosition = pos;

                if (scrollRect)
                    scrollRect.horizontal = false;

                if (!onScrollFlag)
                {
                    onScroll.Invoke();
                    onScrollFlag = true;
                }
            }
                
#else
            Touch touch = Input.touches[0];

            /*if ((Input.GetAxis("Mouse Y") < minAxisY || transform.localPosition.y < startPosition.y) 
                && Input.GetAxis("Mouse X") < maxAxisX)
            {*/
            if (touch.phase == TouchPhase.Moved || onScrollFlag)
            {
                float velocity = (touch.deltaPosition.y) * multiplier;

                if (((velocity < minAxisY || transform.localPosition.y < startPosition.y)
                     && Mathf.Abs(Input.GetAxis("Mouse X")) < maxAxisX) || onScrollFlag)
                {
                    Vector3 pos = transform.localPosition;
                    pos.y += velocity ;
                    transform.localPosition = pos;

                    if (scrollRect)
                        scrollRect.horizontal = false;

                    if (!onScrollFlag)
                    {
                        onScroll.Invoke();
                        onScrollFlag = true;
                    }
                }
            }
#endif
        }
        else 
        {
            transform.localPosition = startPosition;
            if (Input.GetMouseButtonUp(0))
            {
                if (scrollRect)
                {
                    scrollRect.horizontal = true;

                    onScrollFlag = false;
                    endUpdate = false;
                }
            }
        }
	}

    //quem puxa e a animacao que gira a imagem.
    public void EndUpdate()
    {
        endUpdate = true;
    }
}
