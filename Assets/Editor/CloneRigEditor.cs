﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

//Referências
//http://forum.unity3d.com/threads/display-a-list-class-with-a-custom-editor-script.227847/
//http://docs.unity3d.com/Manual/editor-PropertyDrawers.html

[ExecuteInEditMode]
[CustomEditor (typeof(CloneRigColliders), true)]
public class CloneRigEditor : Editor
{
		private CloneRigColliders typedEditableTarget;
		private CloneRigEditor instance;

		private void DrawButtons ()
		{
				//  NGUIEditorTools.BeginContents ();
				//  if (NGUIEditorTools.DrawHeader ("Utils")) {
						if (GUILayout.Button ("do it. Just, DO IT!")) {
								typedEditableTarget.CloneCollider();
						}
				//  }
				//  NGUIEditorTools.EndContents ();
		}

		public override void OnInspectorGUI ()
		{
				base.OnInspectorGUI();
				typedEditableTarget = (CloneRigColliders)target;
				//  ScalableUIAutoAtacher.SetScalablePropertiesScripts ((UIRect)target);

				//  NGUIEditorTools.SetLabelWidth (80f);
				EditorGUILayout.Space ();

				//  serializedObject.Update ();

				//  EditorGUI.BeginDisabledGroup (!ShouldDrawProperties ());
				//  DrawCustomProperties ();
				//  EditorGUI.EndDisabledGroup ();
				//  DrawFinalProperties ();

				DrawButtons ();

				serializedObject.ApplyModifiedProperties ();
		}

		protected virtual void OnEnable ()
		{
				instance = this;

				//  if (serializedObject.isEditingMultipleObjects) {
				//  		mAnchorType = AnchorType.Advanced;
				//  } else
				//  		ReEvaluateAnchorType ();
						
				//  ScalableUIAutoAtacher.SetScalablePropertiesScripts ((UIRect)target);
				//  typedEditableTarget = (UIRect)target;
				//  if (ARM.gui.utils.ScreenOrientation.Orientation == ARM.gui.utils.ScreenOrientationType.Landscape) {
				//  		typedEditableTarget.ScalablePropertyLandScapeRef.PasteCurrentPropertiesToTargetNow ();
				//  } else {
				//  		typedEditableTarget.ScalablePropertyPortraitRef.PasteCurrentPropertiesToTargetNow ();
				//  }
		}
		
//				EditorGUILayout.ColorField
//				EditorGUILayout.CurveField
//				EditorGUILayout.BoundsField
//				EditorGUILayout.EnumMaskField
//				EditorGUILayout.EnumPopup
//				EditorGUILayout.FloatField
//				EditorGUILayout.IntField
//				EditorGUILayout.IntPopup
//				EditorGUILayout.IntSlider
//				EditorGUILayout.LayerField
//				EditorGUILayout.MaskField
//				EditorGUILayout.MinMaxSlider
//				EditorGUILayout.ObjectField
//				EditorGUILayout.PasswordField
//				EditorGUILayout.PropertyField
//				EditorGUILayout.TagField
//				EditorGUILayout.TextArea
//				EditorGUILayout.TextField
//				EditorGUILayout.Vector2Field
//				EditorGUILayout.Vector3Field
//				EditorGUILayout.Vector4Field
}