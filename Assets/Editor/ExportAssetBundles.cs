using UnityEngine;
using UnityEditor;

public class ExportAssetBundles
{



		[MenuItem("Assets/Build Streamed Scene - IOS")]
		static void  CustomEditorBuildSettingsSceneIOS ()
		{
				UnityEditor.EditorBuildSettingsScene[] allScenes = EditorBuildSettings.scenes;

	
				string[] scenesToAdd = new string[allScenes.Length];
				// loop through all the scenes
				int count = 0;
				foreach (UnityEditor.EditorBuildSettingsScene scene in allScenes) {
//			scene.path;		
						BuildPipeline.PushAssetDependencies ();
						scenesToAdd [count++] = scene.path;
				}

				// Bring up save panel
				string path = EditorUtility.SaveFilePanel ("Save Resource", "", "New Resource", "unity3d");

				BuildPipeline.BuildStreamedSceneAssetBundle (scenesToAdd, path, BuildTarget.iOS, BuildOptions.BuildAdditionalStreamedScenes); 
//		var levels : String[] = ["Assets/Level1.unity"];
//		BuildPipeline.BuildStreamedSceneAssetBundle( levels, "Streamed-Level1.unity3d", BuildTarget.WebPlayer); 
		}

		[MenuItem("Assets/Build Streamed Scene - WEB")]
		static void  CustomEditorBuildSettingsSceneWeb ()
		{
				UnityEditor.EditorBuildSettingsScene[] allScenes = EditorBuildSettings.scenes;
		
		
				string[] scenesToAdd = new string[allScenes.Length];
				// loop through all the scenes
				int count = 0;
				foreach (UnityEditor.EditorBuildSettingsScene scene in allScenes) {
						//			scene.path;		
						BuildPipeline.PushAssetDependencies ();
						scenesToAdd [count++] = scene.path;
				}
		
				// Bring up save panel
				string path = EditorUtility.SaveFilePanel ("Save Resource", "", "New Resource", "unity3d");
		
				BuildPipeline.BuildStreamedSceneAssetBundle (scenesToAdd, path, BuildTarget.WebPlayer, BuildOptions.BuildAdditionalStreamedScenes); 
				//		var levels : String[] = ["Assets/Level1.unity"];
				//		BuildPipeline.BuildStreamedSceneAssetBundle( levels, "Streamed-Level1.unity3d", BuildTarget.WebPlayer); 
		}

		[MenuItem("Assets/Build AssetBundle From Selection IOS - Track dependencies")]
		static void ExportResourceIOS ()
		{
       GameObject go = (GameObject)Selection.activeObject;
				// Bring up save panel
				string path = EditorUtility.SaveFilePanel ("Save Resource", "", go.name	+ "_IOS", "unity3d");
       
				if (path.Length != 0) {
						// Build the resource file from the active selection.
						Object[] selection = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
						BuildPipeline.BuildAssetBundle (Selection.activeObject, selection, path, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, BuildTarget.iOS);
						Selection.objects = selection;
				}
		}

		[MenuItem("Assets/Build AssetBundle From Selection ANDROID - Track dependencies")]
		static void ExportResourceAndroid ()
		{
		GameObject go = (GameObject)Selection.activeObject;
				// Bring up save panel
				string path = EditorUtility.SaveFilePanel ("Save Resource", "",  go.name	+ "_Android", "unity3d");
		
				if (path.Length != 0) {
						// Build the resource file from the active selection.
						Object[] selection = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
			
						BuildPipeline.BuildAssetBundle (Selection.activeObject, selection, path, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, BuildTarget.Android);
						Selection.objects = selection;
				}
		}


		[MenuItem("Assets/Build AssetBundle From Selection WEB - Track dependencies")]
		static void ExportResource ()
		{
		
				// Bring up save panel
				string path = EditorUtility.SaveFilePanel ("Save Resource", "", "AssetBundle_WEB", "unity3d");
		
				if (path.Length != 0) {
						// Build the resource file from the active selection.
						Object[] selection = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
			
						BuildPipeline.BuildAssetBundle (Selection.activeObject, selection, path, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets);
						Selection.objects = selection;
				}
		}




		[MenuItem("Assets/Build AssetBundle From Selection - No dependency tracking")]
		static void ExportResourceNoTrack ()
		{
       
				// Bring up save panel
				string path = EditorUtility.SaveFilePanel ("Save Resource", "", "New Resource", "unity3d");
       
				if (path.Length != 0) {
						// Build the resource file from the active selection.
						BuildPipeline.BuildAssetBundle (Selection.activeObject, Selection.objects, path);
				}
		}
}