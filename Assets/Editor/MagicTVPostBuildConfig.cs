﻿using UnityEngine;
using System.Collections;

using System.Linq;
using System.Collections.Generic;

using UnityEditor;
using System.IO;


using UnityEditor.Callbacks;
using System;

public class PostBuildConfig : MonoBehaviour {

    /// Processbuild Function

    [PostProcessBuild]
    // <- this is where the magic happens
    public static void OnPostProcessBuild(BuildTarget target, string path)
    {
        // FIX de iOS não precisam mais existir devido a versao Unity-5.2.1f1
        // return ;
#if UNITY_ANDROID
            
#endif
        // 1: Check this is an iOS build before running
#if UNITY_IPHONE
        {
            
            //  string xcodeprojPath = Application.dataPath;
            //  xcodeprojPath = xcodeprojPath.Substring(0, xcodeprojPath.Length - 16);
            //  string device = "iOSandIpad";
            
            
            //  xcodeprojPath = path ;
            //  xcodeprojPath = path + "/Unity-iPhone.xcodeproj";
            
            
            //          Debug.Log("We found xcodeprojPath to be : "+xcodeprojPath) ;
            Debug.Log("OnPostProcessBuild - START to: " + path);
            updateXcodeProject(path);
        }
#else
        // 3: We do nothing if not iPhone
        Debug.Log("OnPostProcessBuild - Warning: This is not an iOS build") ;
#endif
        Debug.Log("OnPostProcessBuild - STOP");
    }

    // MAIN FUNCTION
    // xcodeproj_filename - filename of the Xcode project to change
    // frameworks - list of Apple standard frameworks to add to the project
    public static void updateXcodeProject(string xcodeprojPath)
    {

        // INFO.plist changes
             string info_plist_file = xcodeprojPath + "/Info.plist";
             List<string> info_plist_lines = System.IO.File.ReadAllLines(info_plist_file).ToList();
             string info_plist_text = System.IO.File.ReadAllText(info_plist_file);
             

            bool hasDeepLink = info_plist_text.Contains("CFBundleURLTypes") ;
            if (!hasDeepLink){
                string addData = "<key>CFBundleURLTypes</key>\n<array>\n\t<dict>\n\t\t<key>CFBundleURLSchemes</key>\n\t\t\t<array>\n\t\t\t\t<string>magicTV</string>\n   \t\t\t</array>\n\t</dict>\n</array>";
                info_plist_lines.Insert( info_plist_lines.IndexOf("</plist>")-1 , addData);            
            }else{
                Debug.Log("OnPostProcessBuild - Warning: DEEPLINK have already been added to XCode project");
            }    
             
            bool hasSecurity = info_plist_text.Contains("NSAppTransportSecurity") ; 
            if (!hasSecurity){
                string addData = "<key>NSAppTransportSecurity</key><dict><key>NSAllowsArbitraryLoads</key><true/></dict>";
                info_plist_lines.Insert( info_plist_lines.IndexOf("</plist>")-1 , addData);            
            }else{
                 Debug.Log("OnPostProcessBuild - Warning: NSAppTransportSecurity have already been added to XCode project");
            }

            bool hasFullScreen = info_plist_text.Contains("UIRequiresFullScreen") ; 
            if (!hasFullScreen){
                string addData = "<key>UIRequiresFullScreen</key><true/>";
                info_plist_lines.Insert( info_plist_lines.IndexOf("</plist>")-1 , addData);            
            }else{
                 Debug.Log("OnPostProcessBuild - Warning: UIRequiresFullScreen have already been added to XCode project");
            }
            

            System.IO.File.WriteAllLines(info_plist_file, info_plist_lines.ToArray());
            

          // Unity-iPhone.xcodeproj  changes
            string xcodeproj = xcodeprojPath + "/Unity-iPhone.xcodeproj/project.pbxproj";
            List<string> xcodeproj_lines = System.IO.File.ReadAllLines(xcodeproj).ToList();
            string xcodeproj_text = System.IO.File.ReadAllText(xcodeproj);
            bool hasEnableBitcode = info_plist_text.Contains("ENABLE_BITCODE") ;

            if (!hasEnableBitcode){
                string addData = "ENABLE_BITCODE = NO; ";

                // xcodeproj_lines.Where(x=>x.Contains("buildSettings")).ToList().ForEach(
                //     (value,index)=> 
                //         {
                //             xcodeproj_lines.Insert( x.Position+1 , addData) ;
                //         }
                //     )
                //     );


                // DEBUG buildSettings
                xcodeproj_lines.Insert( 
                     xcodeproj_lines.IndexOf(
                        xcodeproj_lines.Where(x=>x.Contains("buildSettings")).FirstOrDefault() ) +1 , addData);   

                //RELEASE buildSettings
                 xcodeproj_lines.Insert( 
                     xcodeproj_lines.LastIndexOf(
                        xcodeproj_lines.Where(x=>x.Contains("buildSettings")).LastOrDefault() ) +1            , addData);           
            }else{
                 Debug.Log("OnPostProcessBuild - Warning: ENABLE_BITCODE have already been added to XCode project");
            }

             System.IO.File.WriteAllLines(xcodeproj, xcodeproj_lines.ToArray());

    
    }

  
}
