﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public class OfertasManagerCRM_Exclusivas : MonoBehaviour {

	public static OfertasCRMDataExclusivas ofertasDataServer;
	public static OfertasManagerCRM_Exclusivas instance;

	[Header("Controlador para mostrar as scrollViews ")]
	public CRM_ScrollViewsManager _CRM_ScrollViewManager;

	public static List<string> titleLst = new List<string>(),
	valueLst = new List<string>(),
	infoLst = new List<string>(),
	idLst = new List<string>(),
	idcrmLst = new List<string>(),
	validadeLst = new List<string>(),
	linkLst = new List<string>();

	public List<OfertasValuesCRM> ofertaValuesLst = new List<OfertasValuesCRM>();
	public static List<Texture2D> textureLst = new List<Texture2D>();
	public static List<Sprite> imgLst = new List<Sprite>();
	public static List<bool> favoritouLst = new List<bool>();

	public GameObject loadingObj, popup;
	public GameObject itemPrefab;
	public GameObject containerItens;
	public static List<GameObject> itensList = new List<GameObject>();

	public GameObject favoritouPrimeiraVezObj;
	public GameObject popupSemConexao;

	private ScrollRect scrollRect;
	private int ofertaCounter;
	public static GameObject currentItem;
	public GameObject selectedItem;
	//    public Text quantidadeOfertasText;
	public UnityEngine.UI.Extensions.HorizontalScrollSnap horizontalScrollSnap;

	private static string currentJsonTxt;
	private const int loadMaxItens = 10;
	private static int loadedItensCount;
	private RectTransform rectContainer;
	private bool onRectUpdate;
	private static int loadedImgsCount;    //serve para contar as imagens q ja foram carregadas.

	CultureInfo myCulture;


	// Use this for initialization
	void Start () 
	{
		//Seta a CultureInfo para usar nos preços. Padrão Brasil
		myCulture = new CultureInfo("pt-BR");

		rectContainer = containerItens.GetComponent<RectTransform>();

		instance = this;

		StartCoroutine(ILoadOfertas());

		scrollRect = GetComponent<ScrollRect>();

	}

	public void ShowNumberSelected(int value)
	{
		if (titleLst.Count > 0)
		{
			//            quantidadeOfertasText.text = (value + 1).ToString() + " de " + titleLst.Count.ToString();
			Debug.Log("<color=cyan>Quantidade de Ofertas</color>"+titleLst.Count.ToString());
		}
		else
		{           
			//            quantidadeOfertasText.text = "";
		}
	}

	public void InstantiateNextItem()
	{
		if (ofertaCounter >= titleLst.Count)
			return;
		
//		if (ofertaCounter >= 10)
//			return;

		if(!containerItens.activeInHierarchy)return;

		var obj = Instantiate(itemPrefab);
		obj.transform.SetParent(containerItens.transform, false);
		obj.transform.localScale = Vector3.one * 1.1f;
		//		obj.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);

		obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

		ItemMoveDrag itemMoveDrag = obj.GetComponent<ItemMoveDrag>();
		itemMoveDrag.onSelected.AddListener(InstantiateNextItem);

		OfertasValuesCRM ofertaValuesCRM = obj.GetComponent<OfertasValuesCRM>();



		try
		{
			ofertaValuesCRM.title.text = titleLst[ofertaCounter];
			ofertaValuesCRM.value.text = valueLst[ofertaCounter];
			ofertaValuesCRM.info.text = infoLst[ofertaCounter];
			ofertaValuesCRM.img.sprite = imgLst[ofertaCounter];
			ofertaValuesCRM.id = idLst[ofertaCounter];
			ofertaValuesCRM.id_crm = idcrmLst[ofertaCounter];
			ofertaValuesCRM.favoritou = favoritouLst[ofertaCounter];
			ofertaValuesCRM.validade.text = validadeLst[ofertaCounter];
			ofertaValuesCRM.link = linkLst[ofertaCounter];

			//			ofertaValuesCRM.popupSemConexao = popupSemConexao;

		}catch{

			Debug.Log("<color=red>Erro ao passar infos para Prefab</color>");

		}


		//obj.transform.SetAsFirstSibling();
		currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

		ofertaCounter++;

		ofertaValuesLst.Add(ofertaValuesCRM);
	}

	public void SendStatus(int status)
	{
		StartCoroutine(ISendStatus(selectedItem.GetComponent<OfertasValuesCRM>().id, status));   
	}

	public void BtnAccept()
	{
		if (!currentItem)
		{
			currentItem = containerItens.transform.GetChild(0).gameObject;
		}

		selectedItem = currentItem;

		currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * 2f;
		ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

		itemMoveDrag.onAllow.Invoke();
		itemMoveDrag.SelectAnim();
		itemMoveDrag.OnSelect();

		SendStatus(1);
	}

	public void BtnRecuse()
	{
		if (!currentItem)
		{
			currentItem = containerItens.transform.GetChild(0).gameObject;
		}

		selectedItem = currentItem;

		currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * -2f;
		ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

		itemMoveDrag.onDeny.Invoke();
		itemMoveDrag.SelectAnim();
		itemMoveDrag.OnSelect();

		SendStatus(0);
	}

	private IEnumerator ISendStatus(string id, int status)
	{
		string acao = "e"; // "escrever"
		string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"status_oferta\":\"" + status + "\",\"acao\":\"" + acao + "\"}";
		Debug.Log("Json: " + json);

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

//		WWW www = new WWW(ServerControl.url + "/salva_ofertas.php", pData, headers);
		WWW www = new WWW(ServerControl.urlCRM + "/ofertas_ativaveis_getset.php", pData, headers);
		//    if (loadingObj)
		//      loadingObj.SetActive(true);

		yield return www;
	}

	private IEnumerator ILoadOfertas()
	{
		string acao = "l";
		string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) +  "\",\"acao\":\""+ acao + "\"}";
		Debug.Log("Json: " + json);

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

		WWW www = new WWW(ServerControl.urlCRM + "/ofertas_ativaveis_getset.php", pData, headers);

		if (loadingObj)
			loadingObj.SetActive(true);

		yield return www;

		Debug.Log("Url: " + www.url);

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.Log("Erro: " + www.error);

//			PopUp(www.error);
			if (loadingObj)
				loadingObj.SetActive(false);
		}
		else
		{
			Debug.Log(www.text);

			//verifica se o txt que chegou eh diferente do que tem ja.
			if (currentJsonTxt != www.text)
			{
				Debug.Log("Tem atualizacao de Ofertas!");
				currentJsonTxt = www.text;

				loadedItensCount = 0;
				loadedImgsCount = 0;

				//Atualiza:
				if (loadingObj)
					loadingObj.SetActive(true);

				//LoaderPublishAnim.instance.EndLoad();

				foreach (GameObject obj in itensList)
					Destroy(obj);

				itensList.Clear();

				titleLst.Clear();
				valueLst.Clear();
				infoLst.Clear();
				idLst.Clear();
				idcrmLst.Clear();
				favoritouLst.Clear();
				imgLst.Clear();
				linkLst.Clear();
				textureLst.Clear();
				validadeLst.Clear();
				ofertaValuesLst.Clear();

				ofertaCounter = 0;

				try
				{
					string message = SimpleJSON.EscapeString(www.text);

					//popula obj json.
					ofertasDataServer = JsonUtility.FromJson<OfertasCRMDataExclusivas>(message);

					int count = 0;

					//passa as informacoes para as listas.
					foreach (OfertaCRMData ofertaCRMData in ofertasDataServer.ofertas)
					{
						
						titleLst.Add(ofertaCRMData.titulo);
						if(ofertaCRMData.desconto <= 0)
                        {
							valueLst.Add("Pague: " + ofertaCRMData.pague.ToString() + " e leve: " + ofertaCRMData.leve.ToString());
                        }
                        else
                        {
//							valueLst.Add("Desconto de: " + ofertaCRMData.desconto.ToString() + "%");
							valueLst.Add(ofertaCRMData.desconto.ToString() + "%");

                        }

						valueLst.Add(ofertaCRMData.preco.ToString("C", myCulture));

						infoLst.Add(ofertaCRMData.texto);

						idLst.Add(ofertaCRMData.id_oferta.ToString());
						idcrmLst.Add(ofertaCRMData.id_crm.ToString());

						favoritouLst.Add(ofertaCRMData.aderiu);

						if (ofertaCRMData.unidade > 0)
							validadeLst.Add("Válido para até: " + ofertaCRMData.unidade + " unidade(s) - " + GetDateString(ofertaCRMData.datafinal));
						else
						{
							if (ofertaCRMData.peso >= 1000)
							{
								validadeLst.Add("Válido para até: " + (ofertaCRMData.peso * 0.001f).ToString("0.###").Replace('.', ',') + " Kg - " + GetDateString(ofertaCRMData.datafinal));
							}
							else
							{
								validadeLst.Add("Válido para até: " + ofertaCRMData.peso + " grama(s) - " + GetDateString(ofertaCRMData.datafinal));
							}
						}

						linkLst.Add(ofertaCRMData.link);

						imgLst.Add(new Sprite());
						textureLst.Add(new Texture2D(100, 100));
					}
							
					Debug.Log("Quantidade de Ofertas Exclusivas-> "+titleLst.Count.ToString());
									
					//tratamento caso não tenha ofertas.
					if (ofertasDataServer.ofertas.Length > 0)
					{						
						StartCoroutine(LoadLoadeds());
					}
					else
					{
						Debug.Log("<color=red>Me tira da view EXCLUSIVAS</color>");
						//Se não tiver nada , me tira do scrollview
						//Desconta caso já tiver excluído algum child
						_CRM_ScrollViewManager.removeMeFromScrollSnap((0 - _CRM_ScrollViewManager.getRemoved()) );

						if (loadingObj)
							loadingObj.SetActive(false);
					}						
				}
				catch
				{
					if (ofertasDataServer.ofertas.Length == 0)
						PopUp("Sem ofertas para mostrar");

				
//					PopUp(www.text);
					if (loadingObj)
						loadingObj.SetActive(false);
				}
			}
			else
			{
				
				//tratamento caso não tenha ofertas.
				if (ofertasDataServer.ofertas.Length == 0)
				{
					Debug.Log("<color=red>Me tira da view EXCLUSIVAS</color>");
					//Se não tiver nada , me tira do scrollview
					//Desconta caso já tiver excluído algum child
					_CRM_ScrollViewManager.removeMeFromScrollSnap((0 - _CRM_ScrollViewManager.getRemoved()));
				}

				//se o txt for igual so baixa os que ja tem.
				//tratamento se n tiver ofertas.
				if (ofertasDataServer != null)
				{
					StartCoroutine(LoadLoadeds());
				}
				else
				{
					if (loadingObj)
						loadingObj.SetActive(false);                    
				}
			}


			//Já Fez o download dessa parte
			_CRM_ScrollViewManager.downloadDone();

		}
	}

	public void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			LoadMoreItens();
		}

		if (ofertaValuesLst.Count > loadMaxItens && !onRectUpdate)
		{
			if (Mathf.Abs(containerItens.transform.localPosition.x) > (631 * loadedItensCount))
			{
				onRectUpdate = true;
				LoadMoreItens();
			}
		}
	}

	public void LoadMoreItens()
	{
		//StartCoroutine(ILoadImgs());
	}

	public static string GetDateString(string date)
	{
		System.DateTime theDate = System.DateTime.Parse(date);
		return theDate.ToString("dd/MM/yyyy");

	}

	public static string GetDateTimeString(string date)
	{
		System.DateTime theDate = System.DateTime.Parse(date);
		return theDate.ToString("dd/MM/yyyy - HH:mm");

	}

	private IEnumerator ILoadImgs()
	{

		Debug.Log("Carrega imagens.");

		for (int i = loadedItensCount; i < ofertasDataServer.ofertas.Length; i++)
		{

			if (loadedItensCount >= imgLst.Count)
				break;

//			if (ofertaCounter >= 10)
//				return;

			WWW www = new WWW(ofertasDataServer.ofertas[loadedItensCount].imagem);
			yield return www;

			//yield return new WaitForSeconds(1f);

			if (string.IsNullOrEmpty(www.error))
			{
				Texture2D texture = www.texture;
				Sprite spt = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

				imgLst[loadedItensCount] = spt;
				textureLst[loadedItensCount] = texture;

				if (ofertaValuesLst.Count > loadedItensCount)
					ofertaValuesLst[loadedItensCount].img.sprite = spt;

				loadedImgsCount++;
			}
			else
			{
				Debug.LogError("Erro: " + www.error);
				Debug.LogError("url: " +ofertasDataServer.ofertas[loadedItensCount].imagem);
			}

			loadedItensCount++;
			onRectUpdate = false;
		}

	}

	private IEnumerator LoadLoadeds()
	{
		if (loadingObj)
			loadingObj.SetActive(true);

		scrollRect = GetComponent<ScrollRect>();

		for (int i = 0; i < ofertasDataServer.ofertas.Length; i++)
		{
			InstantiateNextItem();
		}

		StartCoroutine(ILoadImgs());

		//        if(ofertasData.ofertas.Length > 0)
		//             if (quantidadeOfertasText)
		//                 quantidadeOfertasText.text = "1 de " + ofertasData.ofertas.Length;

		yield return new WaitForEndOfFrame();

		if (horizontalScrollSnap)
		{
			horizontalScrollSnap.InitialiseChildObjectsFromScene();
			horizontalScrollSnap.UpdateLayout();
//			scrollRect.content.localPosition = Vector3.zero;
//			if(currentItem)
//				scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scrollRect.content.rect.width + currentItem.GetComponent<RectTransform>().rect.width / 2f);
		}

		if (loadingObj)
			loadingObj.SetActive(false);
	}

	public void PopUp(string txt)
	{
		Debug.Log("Popup: " + txt);
		if (txt.Contains("resolve host"))
		{
			txt = "Sem conexão com a internet.";
		}
		if (popup)
		{
			popup.GetComponentInChildren<Text>().text = txt;
			popup.SetActive(true);
		}
	}
}
