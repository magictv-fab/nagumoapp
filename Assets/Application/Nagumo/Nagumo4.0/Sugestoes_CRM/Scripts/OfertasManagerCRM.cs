﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


public class OfertasManagerCRM : MonoBehaviour {

	public static OfertasCRMDataServer ofertasDataServer;
    public static OfertasManagerCRM instance;

	[Header("Nome do Clinete na ScrollView")]
	public Text nomeDoCliente;

    public static List<string> titleLst = new List<string>(),
                        	   valueLst = new List<string>(),
                       		   infoLst = new List<string>(),
                        	   idLst = new List<string>(),
							   idcrmLst = new List<string>(),
                        	   validadeLst = new List<string>(),
                        	   linkLst = new List<string>(),

								titleDEPOR = new List<string>(),
								valueDEPORLst = new List<string>(),
								valueDEPOR_DELst = new List<string>(),
								valueDEPORDE_PORLst = new List<string>(),
								infoDEPORLst = new List<string>(),
								idDEPORLst = new List<string>(),
								idDEPORcrmLst = new List<string>(),
								validadeDEPORLst = new List<string>(),
								linkDEPORLst = new List<string>();

	public List<OfertasValuesCRM> ofertaValuesLst = new List<OfertasValuesCRM>();
	public List<OfertasValuesCRM> ofertaDEPORValuesLst = new List<OfertasValuesCRM>();

    public static List<Texture2D> textureLst = new List<Texture2D>();
    public static List<Sprite> imgLst = new List<Sprite>();

	public static List<Texture2D> textureDEPORLst = new List<Texture2D>();
	public static List<Sprite> imgDEPORLst = new List<Sprite>();
//    public static List<bool> favoritouLst = new List<bool>();

	public GameObject loadingObj, popup;
	[Header("OFERTAS PRECO 2")]
    public GameObject itemPrefabPreco2;
    public GameObject containerItensPreco2;
    public static List<GameObject> itensList = new List<GameObject>();


	[Header("OFERTAS DE-POR")]
	public GameObject itemPrefab_DE_POR;
	public GameObject containerItens_DE_POR;


	[Header("Controlador para mostrar as scrollViews ")]
	public CRM_ScrollViewsManager _CRM_ScrollViewManager;


	[Header("Interno")]
    public GameObject favoritouPrimeiraVezObj;
    public GameObject popupSemConexao;

    public ScrollRect scrollRect;
	private int ofertaPrecoCounter;
	private int ofertaDePorCounter;
    public static GameObject currentItem;
    public GameObject selectedItem;
//    public Text quantidadeOfertasText;
    public UnityEngine.UI.Extensions.HorizontalScrollSnap horizontalScrollSnap;
	public UnityEngine.UI.Extensions.HorizontalScrollSnap horizontalScrollSnapHEADER;
	public Header_toggle_buttons togglesbuttonscript;


    private static string currentJsonTxt;
    private const int loadMaxItens = 10;
    private static int loadedItensCount;
	private static int loadedItensDEPORCount;

    private RectTransform rectContainer;
    private bool onRectUpdate;
    private static int loadedImgsCount;    //serve para contar as imagens q ja foram carregadas.

	CultureInfo myCulture;


    // Use this for initialization
    void Start () 
    {

		if(nomeDoCliente) nomeDoCliente.text = Login.userData.nome;


		//Seta a CultureInfo para usar nos preços. Padrão Brasil
		myCulture = new CultureInfo("pt-BR");

        rectContainer = containerItensPreco2.GetComponent<RectTransform>();

        instance = this;

        StartCoroutine(ILoadOfertas());

        scrollRect = GetComponent<ScrollRect>();

    }

    public void ShowNumberSelected(int value)
    {
        if (titleLst.Count > 0)
        {
//            quantidadeOfertasText.text = (value + 1).ToString() + " de " + titleLst.Count.ToString();
			Debug.Log("<color=cyan>Quantidade de Ofertas</color>"+titleLst.Count.ToString());
        }
        else
        {           
//            quantidadeOfertasText.text = "";
        }
    }


	public void InstantiateNextItemDePor(){

//		Debug.Log("<color>IS</color>"+containerItens_DE_POR.activeInHierarchy);
		if(!containerItens_DE_POR.activeInHierarchy)return;

		if (ofertaDePorCounter >= 10)
			return;


		//		if (ofertaCounter >= titleLst.Count)
		//			return;

		var obj = Instantiate(itemPrefab_DE_POR);
		obj.transform.SetParent(containerItens_DE_POR.transform, false);
		obj.transform.localScale = Vector3.one * 1.1f;

		obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

		ItemMoveDrag itemMoveDrag = obj.GetComponent<ItemMoveDrag>();
		itemMoveDrag.onSelected.AddListener(InstantiateNextItemPreco2);

		OfertasValuesCRM ofertaValuesCRM = obj.GetComponent<OfertasValuesCRM>();



		try
		{
			ofertaValuesCRM.title.text = titleDEPOR[ofertaDePorCounter];
			ofertaValuesCRM.value.text = valueDEPORLst[ofertaDePorCounter];
			//De
			ofertaValuesCRM.DE.text = valueDEPOR_DELst[ofertaDePorCounter];
			//Por
			ofertaValuesCRM.POR.text = valueDEPORDE_PORLst[ofertaDePorCounter];

			ofertaValuesCRM.info.text = infoDEPORLst[ofertaDePorCounter];
			ofertaValuesCRM.img.sprite = imgDEPORLst[ofertaDePorCounter];
			ofertaValuesCRM.id = idDEPORLst[ofertaDePorCounter];
			ofertaValuesCRM.id_crm = idDEPORcrmLst[ofertaDePorCounter];

			ofertaValuesCRM.validade.text = validadeDEPORLst[ofertaDePorCounter];
			ofertaValuesCRM.link = linkDEPORLst[ofertaDePorCounter];

		}catch{
			
			Debug.Log("<color=red>Erro ao passar infos para Prefab</color>");
		}			
		//obj.transform.SetAsFirstSibling();
		currentItem = containerItens_DE_POR.transform.GetChild(containerItens_DE_POR.transform.childCount - 1).gameObject;
		ofertaDePorCounter++;

		ofertaDEPORValuesLst.Add(ofertaValuesCRM);
		
	}

    public void InstantiateNextItemPreco2()
    {
		//Se já estiver desativado, é por que não tem oferta, entao nao faz nada
		if(!containerItensPreco2.activeInHierarchy)return;


        if (ofertaPrecoCounter >= 10)
            return;
		

//		if (ofertaCounter >= titleLst.Count)
//			return;

        var obj = Instantiate(itemPrefabPreco2);
        obj.transform.SetParent(containerItensPreco2.transform, false);
        obj.transform.localScale = Vector3.one * 1.1f;
//		obj.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);

        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        ItemMoveDrag itemMoveDrag = obj.GetComponent<ItemMoveDrag>();
        itemMoveDrag.onSelected.AddListener(InstantiateNextItemPreco2);

		OfertasValuesCRM ofertaValuesCRM = obj.GetComponent<OfertasValuesCRM>();



		try
		{
			ofertaValuesCRM.title.text = titleLst[ofertaPrecoCounter];
			ofertaValuesCRM.value.text = valueLst[ofertaPrecoCounter];
			ofertaValuesCRM.info.text = infoLst[ofertaPrecoCounter];
			ofertaValuesCRM.img.sprite = imgLst[ofertaPrecoCounter];
			ofertaValuesCRM.id = idLst[ofertaPrecoCounter];
			ofertaValuesCRM.id_crm = idcrmLst[ofertaPrecoCounter];
//			ofertaValuesCRM.favoritou = favoritouLst[ofertaCounter];
			ofertaValuesCRM.validade.text = validadeLst[ofertaPrecoCounter];
			ofertaValuesCRM.link = linkLst[ofertaPrecoCounter];
				
//			ofertaValuesCRM.popupSemConexao = popupSemConexao;

		}catch{
			
			Debug.Log("<color=red>Erro ao passar infos para Prefab</color>");
			
		}

		
		//obj.transform.SetAsFirstSibling();
		currentItem = containerItensPreco2.transform.GetChild(containerItensPreco2.transform.childCount - 1).gameObject;
		
		ofertaPrecoCounter++;
		
		ofertaValuesLst.Add(ofertaValuesCRM);
    }

    public void SendStatus(int status)
    {
		StartCoroutine(ISendStatus(selectedItem.GetComponent<OfertasValuesCRM>().id, status));   
    }

     void BtnAccept()
    {
        if (!currentItem)
        {
            currentItem = containerItensPreco2.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * 2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onAllow.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(1);
    }

     void BtnRecuse()
    {
        if (!currentItem)
        {
            currentItem = containerItensPreco2.transform.GetChild(0).gameObject;
        }

        selectedItem = currentItem;

        currentItem.GetComponent<RectTransform>().anchoredPosition = Vector2.right * -2f;
        ItemMoveDrag itemMoveDrag = currentItem.GetComponent<ItemMoveDrag>();

        itemMoveDrag.onDeny.Invoke();
        itemMoveDrag.SelectAnim();
        itemMoveDrag.OnSelect();

        SendStatus(0);
    }

    private IEnumerator ISendStatus(string id, int status)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"status_oferta\":\"" + status + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/salva_ofertas.php", pData, headers);

    //    if (loadingObj)
      //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ILoadOfertas()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.urlCRM + "/pegar_ofertas_exibiveis.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
			Debug.Log("<color=red>Erro</color>" + www.error);

//            PopUp(www.error);

            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            //verifica se o txt que chegou eh diferente do que tem ja.
            if (currentJsonTxt != www.text)
            {
                Debug.Log("Tem atualizacao de Ofertas!");
                currentJsonTxt = www.text;
               
                loadedItensCount = 0;
				loadedItensDEPORCount = 0;
                loadedImgsCount = 0;

                //Atualiza:
                if (loadingObj)
                    loadingObj.SetActive(true);

                //LoaderPublishAnim.instance.EndLoad();

                foreach (GameObject obj in itensList)
                    Destroy(obj);

                itensList.Clear();


				//Preco Lists
                titleLst.Clear();
                valueLst.Clear();
                infoLst.Clear();
                idLst.Clear();
				idcrmLst.Clear();
                imgLst.Clear();
                linkLst.Clear();
                textureLst.Clear();
                validadeLst.Clear();
                ofertaValuesLst.Clear();
//              favoritouLst.Clear();

				//DE POR Lists
				ofertaDEPORValuesLst.Clear();
				titleDEPOR.Clear();
				valueDEPORLst.Clear();
				valueDEPOR_DELst.Clear();
				idDEPORLst.Clear();
				idDEPORcrmLst.Clear();
				linkDEPORLst.Clear();
				validadeDEPORLst.Clear();
				valueDEPORDE_PORLst.Clear();
				imgDEPORLst.Clear();
				textureDEPORLst.Clear();

                ofertaPrecoCounter = 0;
				ofertaDePorCounter = 0;

//
				try
				{
					string message = SimpleJSON.EscapeString(www.text);
					
					//popula obj json.
					ofertasDataServer = JsonUtility.FromJson<OfertasCRMDataServer>(message);
					
					int count = 0;					

					Debug.Log("TEm : "+ ofertasDataServer.ofertas[0].preco.Length + " Ofertas de PReco");
					Debug.Log("TEm : "+ ofertasDataServer.ofertas[1].depor.Length + " Ofertas DE POR");

					//Só guarda se tiver oferta ...

					foreach(OfertaCRMData ofertaCRMDataPreco in ofertasDataServer.ofertas[0].preco)
					{
						titleLst.Add(ofertaCRMDataPreco.titulo);
						valueLst.Add(ofertaCRMDataPreco.preco.ToString("C", myCulture));
						infoLst.Add(ofertaCRMDataPreco.texto);

						idLst.Add(ofertaCRMDataPreco.id_oferta.ToString());
						idcrmLst.Add(ofertaCRMDataPreco.id_crm.ToString());

						if (ofertaCRMDataPreco.unidade > 0)
							validadeLst.Add("Válido para até: " + ofertaCRMDataPreco.unidade + " unidade(s) - " + GetDateString(ofertaCRMDataPreco.datafinal));
						else
						{
							if (ofertaCRMDataPreco.peso >= 1000)
							{
								validadeLst.Add("Válido para até: " + (ofertaCRMDataPreco.peso * 0.001f).ToString("0.###").Replace('.', ',') + " Kg - " + GetDateString(ofertaCRMDataPreco.datafinal));
							}
							else
							{
								validadeLst.Add("Válido para até: " + ofertaCRMDataPreco.peso + " grama(s) - " + GetDateString(ofertaCRMDataPreco.datafinal));
							}
						}

						linkLst.Add(ofertaCRMDataPreco.link);

						imgLst.Add(new Sprite());
						textureLst.Add(new Texture2D(100, 100));
					}
						
					//Avisa que fez o download dessa parte
					_CRM_ScrollViewManager.downloadDone();

					//tratamento caso não tenha ofertas.
					if (ofertasDataServer.ofertas[0].preco.Length > 0)
					{
						//StartCoroutine(ILoadImgs());
						StartCoroutine(LoadLoadeds(0));
					}
					else
					{
						Debug.Log("<color=red>Me tira da view PRECO</color>");
						//Se não tiver nada , me tira do scrollview
						_CRM_ScrollViewManager.removeMeFromScrollSnap(1);
					
						if (loadingObj)
							loadingObj.SetActive(false);
					}

										
					foreach(OfertaCRMData ofertaCRMDataDePor in ofertasDataServer.ofertas[1].depor)
					{
						
						titleDEPOR.Add(ofertaCRMDataDePor.titulo);
						valueDEPORLst.Add(ofertaCRMDataDePor.preco.ToString("C", myCulture));
						infoDEPORLst.Add(ofertaCRMDataDePor.texto);

						valueDEPOR_DELst.Add("De: " +ofertaCRMDataDePor.de.ToString("C", myCulture));

						valueDEPORDE_PORLst.Add("Por: "+ ofertaCRMDataDePor.por.ToString("C", myCulture));


						idDEPORLst.Add(ofertaCRMDataDePor.id_oferta.ToString());
						idDEPORcrmLst.Add(ofertaCRMDataDePor.id_crm.ToString());

						if (ofertaCRMDataDePor.unidade > 0)
							validadeDEPORLst.Add("Válido para até: " + ofertaCRMDataDePor.unidade + " unidade(s) - " + GetDateString(ofertaCRMDataDePor.datafinal));
						else
						{
							if (ofertaCRMDataDePor.peso >= 1000)
							{
								validadeDEPORLst.Add("Válido para até: " + (ofertaCRMDataDePor.peso * 0.001f).ToString("0.###").Replace('.', ',') + " Kg - " + GetDateString(ofertaCRMDataDePor.datafinal));
							}
							else
							{
								validadeDEPORLst.Add("Válido para até: " + ofertaCRMDataDePor.peso + " grama(s) - " + GetDateString(ofertaCRMDataDePor.datafinal));
							}
						}

						linkDEPORLst.Add(ofertaCRMDataDePor.link);

						imgDEPORLst.Add(new Sprite());
						textureDEPORLst.Add(new Texture2D(100, 100));
					}


					//tratamento caso não tenha ofertas.
					if (ofertasDataServer.ofertas[1].depor.Length > 0)
					{
						//StartCoroutine(ILoadImgs());
						StartCoroutine(LoadLoadeds(1));
					}
					else
					{
						Debug.Log("<color=red>Me tira da view DE-POR</color>");
						//Se não tiver nada , me tira do scrollview
						_CRM_ScrollViewManager.removeMeFromScrollSnap(2);						

						if (loadingObj)
							loadingObj.SetActive(false);
					}

					//Fez o download
					_CRM_ScrollViewManager.downloadDone();

                }
                catch
                {
                    PopUp(www.text);
//					Debug.Log(www.text);
                    if (loadingObj)
                        loadingObj.SetActive(false);
                }
            }
            else
            {
                //se o txt for igual so baixa os que ja tem.
                //tratamento se n tiver ofertas.
                if (ofertasDataServer != null)
                {
                    StartCoroutine(LoadLoadeds(0));
					StartCoroutine(LoadLoadeds(1));
                }
                else
                {
                    if (loadingObj)
                        loadingObj.SetActive(false);                    
                }
            }
        }


		/************************************************************************************************/

		//Atualiza o scrollSnap do header com os Child que tem alguma oferta
//		Debug.Log("INICIA COM OS CHILDREEN QUE SOBRARAM!!!!!!!");
		horizontalScrollSnapHEADER.InitialiseChildObjectsFromScene();
		horizontalScrollSnapHEADER.UpdateLayout();

		//Atualiza os toggles (Para acender)
		togglesbuttonscript.setMyToggles();

		/************************************************************************************************/



    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            LoadMoreItens();
        }

        if (ofertaValuesLst.Count > loadMaxItens && !onRectUpdate)
        {
            if (Mathf.Abs(containerItensPreco2.transform.localPosition.x) > (631 * loadedItensCount))
            {
                onRectUpdate = true;
                LoadMoreItens();
            }
        }
    }

    public void LoadMoreItens()
    {
        //StartCoroutine(ILoadImgs());
    }

    public static string GetDateString(string date)
    {
        System.DateTime theDate = System.DateTime.Parse(date);
        return theDate.ToString("dd/MM/yyyy");

    }

    public static string GetDateTimeString(string date)
    {
        System.DateTime theDate = System.DateTime.Parse(date);
        return theDate.ToString("dd/MM/yyyy - HH:mm");

    }

	 // 0 = Preco , 1 = De por
	private IEnumerator ILoadImgs(int index)
{

	if(index == 0)
	{

		Debug.Log("Carrega imagens Tipo PRECO.");			
		//Carrega só 10 por enquanto
		for (int i = loadedItensCount; i < 10/*ofertasDataServer.ofertas.Length*/; i++)
		{

			if (loadedItensCount >= imgLst.Count)
				break;

			//			if (ofertaCounter >= 10)
			//				break;
	
			WWW www = new WWW(ofertasDataServer.ofertas[0].preco[loadedItensCount].imagem);
			//              WWW www = new WWW(ofertasDataServer.ofertas[0].preco[loadedItensCount].imagem);
			yield return www;
			//yield return new WaitForSeconds(1f);

			if (string.IsNullOrEmpty(www.error))
			{
				Texture2D texture = www.texture;
				Sprite spt = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

				imgLst[loadedItensCount] = spt;
				textureLst[loadedItensCount] = texture;

				if (ofertaValuesLst.Count > loadedItensCount)
					ofertaValuesLst[loadedItensCount].img.sprite = spt;

				loadedImgsCount++;
			}
			else
			{
					Debug.LogError("Erro: " + www.error);
				Debug.LogError("url: " +ofertasDataServer.ofertas[0].preco[loadedItensCount].imagem);									
			}

			loadedItensCount++;
			onRectUpdate = false;			
		}

	}else
			
	if(index == 1)
		{		

//				yield break;
				Debug.Log("Carrega imagens Tipo DE POR.");			

				for (int i = loadedItensDEPORCount; i < 10/*ofertasDataServer.ofertas.Length*/; i++)
			{

				if (loadedItensDEPORCount >= imgDEPORLst.Count)
				break;


				WWW wwwDePor = new WWW(ofertasDataServer.ofertas[1].depor[loadedItensDEPORCount].imagem);
				yield return wwwDePor;

				if (string.IsNullOrEmpty(wwwDePor.error))
				{
					Texture2D texture = wwwDePor.texture;
					Sprite spt = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

					imgDEPORLst[loadedItensDEPORCount] = spt;
					textureDEPORLst[loadedItensDEPORCount] = texture;

					if (ofertaDEPORValuesLst.Count > loadedItensDEPORCount)
						ofertaDEPORValuesLst[loadedItensCount].img.sprite = spt;

					loadedItensDEPORCount++;
				}
				else
				{
					Debug.LogError("Erro: " + wwwDePor.error);
					Debug.LogError("url: " +ofertasDataServer.ofertas[1].depor[loadedItensDEPORCount].imagem);				
				}

				loadedItensDEPORCount++;
				onRectUpdate = false;

			}

		}
       

}

	private IEnumerator LoadLoadeds(int index)
    {
        if (loadingObj)
            loadingObj.SetActive(true);
        
        scrollRect = GetComponent<ScrollRect>();

		// 0  = PRECO
		if(index == 0 ){
			
			for (int i = 0; i < ofertasDataServer.ofertas[0].preco.Length; i++)
			{
				InstantiateNextItemPreco2();
			}

			StartCoroutine(ILoadImgs(0));

		}

		// 1  = DE POR
		if(index == 1)
		{
			for (int i = 0; i < ofertasDataServer.ofertas[1].depor.Length; i++)
			{
				InstantiateNextItemDePor();
			}

			StartCoroutine(ILoadImgs(1));

		}


        yield return new WaitForEndOfFrame();

        if (horizontalScrollSnap)
        {
            horizontalScrollSnap.InitialiseChildObjectsFromScene();
            horizontalScrollSnap.UpdateLayout();
//            scrollRect.content.localPosition = Vector3.zero;
//            if(currentItem)
//                scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scrollRect.content.rect.width + currentItem.GetComponent<RectTransform>().rect.width / 2f);
        }

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);
        if (txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }
        if (popup)
        {
            popup.GetComponentInChildren<Text>().text = txt;
            popup.SetActive(true);
        }
    }
}
