﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;

public class CRM_ScrollViewsManager : MonoBehaviour {

	//TODO
	/*

Tentar usar uma variavel statica para comandar a hora de dar o comando 		

.InitialiseChildObjectsFromScene(); 
e
.UpdateLayout();

pois os dois scripts (Exclusivas e CRM) roam ao mesmo tempo ,isso está atrapalhando. 
Tentar fazer com que rode esse comando apenas depois dos downloads das classes ...



	*/
	//Contador de downloads
	public static int dowloaded, removed;
	//Bool para fazer o update do layout
	private bool initScroollSnaps;

	public Text nomeDoCliente;


	/// <summary>
	/// Start this instance.
	/// Será responsável por verificar se, quando não houver algum tipo de oferta para esse cliente,
	/// nem mostra a scrollView correspondente.
	/// Ou seja, o cliente poderá ver apenas 2 ou scrollviews etc ...
	/// 
	/// </summary>
	/// 
	/// 

	public UnityEngine.UI.Extensions.HorizontalScrollSnap _horizontalScrollSnapOfertas, _horizontalScrollSnapHeader;
	public Header_toggle_buttons togglesbuttonscript;
	// Use this for initialization
	void Start () {

		if(nomeDoCliente){

			string texto = nomeDoCliente.text;
			string frase = texto.Replace("_", Login.userData.nome);

			nomeDoCliente.text = frase;
		}

	}

	public int getRemoved(){
		return removed;
	}

	void OnEnable(){
		removed = 0;
	}

	public void removeMeFromScrollSnap(int myIndex)
	{
		//Conta que removeu
		removed ++;

//		Debug.Log("<color>INDEX-></color>"+myIndex);
		//Scroll Views
		GameObject objRemoved;
		_horizontalScrollSnapOfertas.RemoveChild(myIndex , out objRemoved);
		objRemoved.SetActive(false);
		//Header
		GameObject headerRemoved;
		_horizontalScrollSnapHeader.RemoveChild(myIndex , out headerRemoved);
		headerRemoved.SetActive(false);
	}

	public void downloadDone(){

//		Debug.Log("<color>Terminou alguem</color>");
		dowloaded ++;

		//3 views completaram downloads ?
		if(dowloaded >= 3)
		{			
			Debug.Log("<color=cyan>UPDATE LAYOUTS</color>");
			//Update Scroll views
			_horizontalScrollSnapOfertas.InitialiseChildObjectsFromScene();
			_horizontalScrollSnapOfertas.UpdateLayout();

			//Update Scroll Views Header
			_horizontalScrollSnapHeader.InitialiseChildObjectsFromScene();
			_horizontalScrollSnapHeader.UpdateLayout();
			//Update Toggles Header
			togglesbuttonscript.setMyToggles();

			//Zera contador
			dowloaded = 0;

			//Forca comecar na pagina '0' mesmo que só tenha 1 pagina (Para 'acender' o toggle)
			_horizontalScrollSnapOfertas.GoToScreen(0);
		}

	}


	public void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Log("<color=cyan>UPDATE LAYOUTS</color>");
			//Update Scroll views
			_horizontalScrollSnapOfertas.InitialiseChildObjectsFromScene();
			_horizontalScrollSnapOfertas.UpdateLayout();

			//Update Scroll Views Header
			_horizontalScrollSnapHeader.InitialiseChildObjectsFromScene();
			_horizontalScrollSnapHeader.UpdateLayout();
			//Update Toggles Header
			togglesbuttonscript.setMyToggles();

			//Zera o contador para a proxima vez
			removed = 0;
		}
			
	}



}
