﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class ScrollRectEnsureVisible : MonoBehaviour {

	public RectTransform maskTransform;

	private ScrollRect mScrollRect;
	private RectTransform mScrollTransform;
	private RectTransform mContent;

	[Header("Array para sincronizar com o Horizontal Script")]
	public GameObject[] scrollTransformItem;


	[Header("Horizontal script Helper")]
	public ScrollSnapBase _horizontalSnapScript;

	//Forca iniciar na pagina 1 (Bug antes de Instanciar automatico)
	void Start () 
	{
		StartCoroutine(forceSnap());

	}

	IEnumerator forceSnap()
	{
		if(!_horizontalSnapScript)yield break;

		yield return new WaitForSeconds(.1f);
		Debug.Log("Forcando Snap");
		_horizontalSnapScript.GoToScreen(0);
	}

	//Script chamado do scrollSnap script
	public void getIndexScroll(){
		//Só faz se tiver ref do script
		if(!_horizontalSnapScript)return;

//		Debug.Log("Centralizando no item:  "+ _horizontalSnapScript._currentPage+ "da array de Rect");
		var gameObj = scrollTransformItem[_horizontalSnapScript._currentPage];

		//Centraliza de acordo com a lista de gameObject usando o index que o script de snap possui
		CenterOnItem(gameObj.GetComponent<RectTransform>());

		//Toggle On para o objeto selecionado
		gameObj.GetComponent<Toggle>().isOn = true;
	}


	public void CenterOnItem(RectTransform target)
	{
		// Item is here
		var itemCenterPositionInScroll = GetWorldPointInWidget(mScrollTransform, GetWidgetWorldPoint(target));
		// But must be here
		var targetPositionInScroll = GetWorldPointInWidget(mScrollTransform, GetWidgetWorldPoint(maskTransform));
		// So it has to move this distance
		var difference = targetPositionInScroll - itemCenterPositionInScroll;
		difference.z = 0f;

		//clear axis data that is not enabled in the scrollrect
		if (!mScrollRect.horizontal)
		{
			difference.x = 0f;
		}
		if (!mScrollRect.vertical)
		{
			difference.y = 0f;
		}

		var normalizedDifference = new Vector2(
			difference.x / (mContent.rect.size.x - mScrollTransform.rect.size.x),
			difference.y / (mContent.rect.size.y - mScrollTransform.rect.size.y));

		var newNormalizedPosition = mScrollRect.normalizedPosition - normalizedDifference;
		if (mScrollRect.movementType != ScrollRect.MovementType.Unrestricted)
		{
			newNormalizedPosition.x = Mathf.Clamp01(newNormalizedPosition.x);
			newNormalizedPosition.y = Mathf.Clamp01(newNormalizedPosition.y);
		}

		mScrollRect.normalizedPosition = newNormalizedPosition;
	}
	private void Awake()
	{

		StartCoroutine(forceSnap());


		mScrollRect = GetComponent<ScrollRect>();
		mScrollTransform = mScrollRect.transform as RectTransform;
		mContent = mScrollRect.content;
		Reset();
	}
	private void Reset()
	{
		if (maskTransform == null)
		{
			var mask = GetComponentInChildren<Mask>(true);
			if (mask)
			{
				maskTransform = mask.rectTransform;
			}
			if (maskTransform == null)
			{
				var mask2D = GetComponentInChildren<RectMask2D>(true);
				if (mask2D)
				{
					maskTransform = mask2D.rectTransform;
				}
			}
		}
	}
	private Vector3 GetWidgetWorldPoint(RectTransform target)
	{
		//pivot position + item size has to be included
		var pivotOffset = new Vector3(
			(0.5f - target.pivot.x) * target.rect.size.x,
			(0.5f - target.pivot.y) * target.rect.size.y,
			0f);
		var localPosition = target.localPosition + pivotOffset;
		return target.parent.TransformPoint(localPosition);
	}
	private Vector3 GetWorldPointInWidget(RectTransform target, Vector3 worldPoint)
	{
		return target.InverseTransformPoint(worldPoint);
	}
}
