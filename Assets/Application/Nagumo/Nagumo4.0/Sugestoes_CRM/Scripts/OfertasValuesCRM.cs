﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class OfertasValuesCRM : MonoBehaviour {

	public Text title, info, value , DE, POR, validade, unidades, aquisicao;
	public Image img, favoritoImg;
	public Button btn;
	public string id, id_crm;
	public Texture2D saveImageSource;
	public bool favoritou;
	public Sprite favoritoSprite, desfavoritouSprite;
	public GameObject favoritouObj;
	public GameObject btnFavoritado, btnFavoritar, popupSemConexao;
	public bool showPopup = true;
	public string link, imgLink;

	// Use this for initialization
	void Start()
	{
		if (favoritou)
		{
			if(favoritoImg)
				favoritoImg.sprite = favoritoSprite;
			if (favoritouObj)
				favoritouObj.SetActive(true);
			if (btnFavoritado)
			{
				btnFavoritado.SetActive(true);
				btnFavoritar.SetActive(false);
			}
		}
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void GoLink()
	{
		if (string.IsNullOrEmpty(link))
			return;

		//link = "out:" + link;

		//caso tenha out na frente eh para abrir em outra janela.
		if(link.Contains("out:"))
		{
			string newLink = link.Substring(4);
			Debug.Log("abre o link " + newLink);

			Application.OpenURL(newLink);
		}
		else
		{
			PublishLink.url = link;

			LoadLevelName loadLevelName = GetComponent<LoadLevelName>();
			loadLevelName.LoadLevelAdditive("Publicacao_Link");            
		}

	}

	public void SaveImage()
	{
		ScreenshotManager.SaveImage(saveImageSource, "Oferta_Nagumo_" + id, "jpg");
		if (PublishManager.instance != null && showPopup)
			PublishManager.instance.PopUp("A imagem foi salva em seu rolo de câmera.");
		else
			FavoritosManager.instance.PopUp("A imagem foi salva em seu rolo de câmera.");
	}

	public void RemoverOferta()
	{
		MinhasOfertasManager.instance.MinhaOfertaRemove(id);
		gameObject.SetActive(false);
	}

	public void Favoritar()
	{

		if (!favoritou)
		{
			if(favoritoImg)
				favoritoImg.sprite = favoritoSprite;

			if (PublishManager.instance != null && showPopup)
			{
				PublishManager.instance.Favoritar(id);
				PublishManager.instance.PopUp("A publicação foi salva em favoritos.");
			}
			else
			{
				FavoritosManager.instance.Favoritar(id);
				FavoritosManager.instance.PopUp("A publicação foi salva em favoritos.");
			}
		}
		else
		{
			if(favoritoImg)
				favoritoImg.sprite = desfavoritouSprite;

			if (PublishManager.instance != null && showPopup)
			{
				PublishManager.instance.DesFavoritar(id);
				PublishManager.instance.PopUp("A publicação foi excluida de favoritos.");
			}
			else
			{
				FavoritosManager.instance.DesFavoritar(id);
				FavoritosManager.instance.PopUp("A publicação foi excluida de favoritos.");
				gameObject.SetActive(false);
			}
		}


		favoritou = !favoritou;

		if (favoritouObj)
			favoritouObj.SetActive(favoritou);
	}

	public void BtnAtivarDesativar()
	{

		if (!favoritou)
		{
			StartCoroutine(IFavoritar(id));
		}
		else
		{
			// DesFavoritar(id);
		}


		//favoritou = !favoritou;

	}

	public void DesFavoritar(string id)
	{
		StartCoroutine(ExcluirFavorito(id));
	}

	private IEnumerator ExcluirFavorito(string id)
	{

		string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\"}";
		Debug.Log("Json: " + json);

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

		WWW www = new WWW(ServerControl.url + "/excluir_oferta_participante.php", pData, headers);

		//    if (loadingObj)
		//      loadingObj.SetActive(true);

		yield return www;

		if (!string.IsNullOrEmpty(www.text))
			Debug.Log(www.text); 
	}

	private IEnumerator IFavoritar(string id)
	{

		string acao = "e"; // "escrever"
		string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\",\"id_crm\":\"" + id_crm+ "\",\"status_oferta\":\"" + 1 + "\",\"acao\":\"" + acao + "\"}";
		Debug.Log("<color=blue>Json: " + json);

//		yield break;

		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

		//		WWW www = new WWW(ServerControl.url + "/salva_ofertas.php", pData, headers);
		WWW www = new WWW(ServerControl.urlCRM + "/ofertas_ativaveis_getset.php", pData, headers);

		Debug.Log(ServerControl.urlCRM + "/ofertas_ativaveis_getset.php");

		//    if (loadingObj)
		//      loadingObj.SetActive(true);
		yield return www;

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.Log(www.text);
			popupSemConexao.SetActive(true);
		}
		else
		{
			if (favoritouObj)
			{
				favoritouObj.SetActive(true);
				OfertasManagerCRM_Exclusivas.instance.favoritouPrimeiraVezObj.SetActive(true);
			}

			if (btnFavoritado)
			{
				btnFavoritado.SetActive(true);
				btnFavoritar.SetActive(false);
			}
		}
	}

	public void Compartilhar()
	{
		string sharerLink = "http://dashboard-magictv.com.br/nagumoplay/webservices/bonificacao_pontos/facebook.php?id=" + id;
		FacebookMain.instance.SharerLink(sharerLink, imgLink, "Oferta Nagumo!", info.text??"");

		return;

		if (PublishManager.instance != null && showPopup)
			PublishManager.instance.PopUp("Sistema de publicação no Facebook está em desenvolvimento...");
		else
			FavoritosManager.instance.PopUp("Sistema de publicação no Facebook está em desenvolvimento...");
	}
}
