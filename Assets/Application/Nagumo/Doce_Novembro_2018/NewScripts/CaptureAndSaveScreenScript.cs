﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CaptureAndSaveScreenScript : MonoBehaviour {

	string x = "0";
	string y = "0";
	string width = "0";
	string height = "0";


	int marginImage = 100;
	int marginImageBottom = 50;


	Texture2D tex;

	CaptureAndSave snapShot ;

	string log="Log";
	void Start()
	{
		snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
	}

	void OnEnable()
	{
		CaptureAndSaveEventListener.onError += OnError;
		CaptureAndSaveEventListener.onSuccess += OnSuccess;
	}

	void OnDisable()
	{
		CaptureAndSaveEventListener.onError += OnError;
		CaptureAndSaveEventListener.onSuccess += OnSuccess;
	}

	void OnError(string error)
	{
		log += "\n"+error;
		Debug.Log ("Error : "+error);
	}

	void OnSuccess(string msg)
	{
		log += "\n"+msg;
		Debug.Log ("Success : "+msg);
	}

	public void saveOnFolder(){


		#if UNITY_IOS

		Debug.Log("Salvando iOS");
//		snapShot.CaptureAndSaveToAlbum(0,
//			(int)(Screen.height/4 - marginImageBottom),
//			(int) Screen.width,
//			(int)(Screen.height/2 + marginImage),
//			ImageType.JPG);

		tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

		ScreenshotManager.SaveImage(tex, "Meu_premio", "jpg");


		return;

		#endif



		Debug.Log("Salvando Android");
		snapShot.CaptureAndSaveToAlbum(0,
			(int)(Screen.height/4 - marginImageBottom),
			(int) Screen.width,
			(int)(Screen.height/2 + marginImage),
			ImageType.JPG);

	}



}
