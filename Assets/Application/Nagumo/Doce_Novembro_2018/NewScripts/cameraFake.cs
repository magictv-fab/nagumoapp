﻿using UnityEngine;
using System.Collections;
 using UnityEngine.UI;

public class cameraFake : MonoBehaviour {

	public WebCamTexture mCamera = null;
    public float bias = 0.1f;


    public int _cameraIndex=0;

    private bool back;

    WebCamDevice[] devices;

    public RawImage image;


    // Use this for initialization
    void Start()
    {

        if(_cameraIndex == 1){

            WebCamDevice device = WebCamTexture.devices[_cameraIndex];
            mCamera = new WebCamTexture(device.name);
            image.material.mainTexture = mCamera;
            mCamera.Play();
                        
        }else{



            mCamera = new WebCamTexture(480, 640);

            image.material.mainTexture = mCamera;
            mCamera.Play();
            //transform.localScale = new Vector3(mCamera.width, mCamera.height) * bias;
        }





#if UNITY_IOS
        transform.localEulerAngles = new Vector3(0,180,-90);
#endif

    }

    public void BackButton()
    {
        mCamera.Stop();
        Close();
    }

	public void Close()
	{
		if (!back)
		{
			back = true;
			StartCoroutine(GoLevel());
		}
	}

	public void OnDisable()
	{
        mCamera.Stop();
	}

	public void OnDestroy()
	{
        mCamera.Stop();
	}

	public IEnumerator GoLevel()
	{

        mCamera.Stop();

		yield return new WaitForEndOfFrame();


		AsyncOperation async = Application.LoadLevelAsync("Menu_Nagumo");


		yield return async;


	}

            public void fakeScreenShot(){ 

				// StartCoroutine(delay());
                       
            mCamera.Pause();
        }

	IEnumerator delay()	{
		
		 yield return new WaitForSeconds(2f);
		 mCamera.Pause();
	}
}
