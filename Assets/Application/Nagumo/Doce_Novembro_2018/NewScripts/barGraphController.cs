﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class barGraphController : MonoBehaviour {

	public Text saldoAcumulado, valorRestante;
	public Image fillBar;

	// public ReactTransformControll reactTransformControll;

	float total = 100;


	void Start()
	{		
		//Procura o GameObject que receberá os valors para mexer no seu ReactTransform
		// reactTransformControll = Object.FindObjectOfType<ReactTransformControll>();
	}

	// Update is called once per frame
	void Update () {

		/*
		if (coolingDown == true)
        {
            //Reduce fill amount over 30 seconds
            cooldown.fillAmount -= 1.0f / waitTime * Time.deltaTime;
        }
		*/

	}

	public void updateBarGraphText(float valor){

		//Atualiza "seta"
		saldoAcumulado.text = "R$ "+ valor.ToString();
		//Atualiza valor restante
		valorRestante.text = "R$ " + (total - valor).ToString("f2");
		//Atualiza a fill bar
		updateFillBar(valor);
		// moveSetaTobarGraphValue(valor);
		// reactTransformControll.moveSetaToLocation(valor/100);

	}

	public void updateFillBar(float valor){		

		// fillBar.fillAmount = Mathf.Lerp(fillBar.fillAmount, valor/100, Time.deltaTime);
		fillBar.fillAmount = valor/100;
	}

}
