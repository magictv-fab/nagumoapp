﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateExtratoParticipante : MonoBehaviour
{
    barGraphController BGController;

    public static bool onUpdate = false;

    public GameObject loadingObj;

    // Use this for initialization
    void OnEnable()
    {
        BGController = GetComponent<barGraphController>();
        
        StartCoroutine(ILogin());

    }


    /// <summary>
    /// Chamado para atualizar os campos de spins e cupons.
    /// </summary>
    private IEnumerator ILogin()
    {
        onUpdate = true;
        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Base64Encode(Login.userData.senha) + "\"}";

        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());


        WWW www = new WWW(ServerControl.url + "/pegar_extrato_participante.php", pData, headers);



        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            //PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            try
            {

                //popula obj json.
                Extrato extrato = JsonUtility.FromJson<Extrato>(www.text);                
                Login.userData.saldo_acumulado = extrato.saldo_acumulado;
                Login.userData.spins = extrato.total_spins;
                //Passa para a HUD
                BGController.updateBarGraphText(Login.userData.saldo_acumulado);

                
                Debug.Log("Saldo Acumulado JSON= " + Login.userData.saldo_acumulado);
                Debug.Log("Total Spins JSON= " + Login.userData.spins);


                Debug.Log("Update Extrato OK");

                if (loadingObj)
                    loadingObj.SetActive(false);


            }
            catch
            {

                //Debug.Log("Catch " + Login.userData.saldo_acumulado);
                //Debug.Log("Catch " + Login.userData.spins);

                // PopUp(www.text);
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
        }

        onUpdate = false;
    }

    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }




[System.Serializable]
public class Extrato
{
    public int   total_spins;
    public float saldo_acumulado;
    public float saldo_anterior;
}

}
