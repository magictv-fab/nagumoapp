﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeBackgroundImage : MonoBehaviour {

	public Image backgroundImage;
	public Sprite spriteLoaded;
	public Image loadBar;
	public GameObject scrollGroup;
	public GameObject barGroup;

	private DownloadImages downloadImages;
	private bool startLoad;
	private string focusCover;

	// Use this for initialization
	void Start () 
	{
		downloadImages = GetComponent<DownloadImages>();

		downloadImages.onLoadCompleted += StartLoad;
	}
	
	public void StartLoad () 
	{
		startLoad = true;
		scrollGroup.SetActive(true);
		barGroup.SetActive(false);
		//ScrollRectToolInfinity.selectChildren = null;
		//ScrollRectToolInfinity.selectChildren += ChangeImage;

	}

    private void OnDisable()
    {
        startLoad = false;
        scrollGroup.SetActive(false);
        barGroup.SetActive(true);
    }

    public void ChangeImage(string name)
	{
		name = name.ToLower();
		focusCover = name;
		Debug.Log(name);

		if(!startLoad)
			return;

		if(DownloadImages.bdImages.ContainsKey(name))
		{
			if(DownloadImages.bdImages[name].Count > 0)
			{
				//pega o ultimo arquivo da lista, q eh o ultimo arquivo colocado no servidor.
				spriteLoaded = DownloadImages.bdImages[name][DownloadImages.bdImages[name].Count - 1];
				StopCoroutine("ColorTransition");
				StartCoroutine("ColorTransition", spriteLoaded);
			}
		}

	}

    public void BtnClose()
    {
        gameObject.SetActive(false);
    }

	public void ClickBackgroudCover()
	{
		//if(!startLoad)
			return;

		SelectCover.clickedName = focusCover;
		Debug.Log("Clicado no " + SelectCover.clickedName);
		Application.LoadLevel("EditionSelect");
	}

	private IEnumerator ColorTransition(Sprite sprite)
	{
		float time = 0.3f;
		backgroundImage.CrossFadeColor(new Color(1,1,1,0), time, true, true);

		yield return new WaitForSeconds(time);
		backgroundImage.overrideSprite = sprite;




		backgroundImage.CrossFadeColor(new Color(1,1,1,1f), time, true, true);

	}
}
