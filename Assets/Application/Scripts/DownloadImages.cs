﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class DownloadImages : MonoBehaviour 
{
	public delegate void OnLoadCompleted();

	public static Dictionary<string, List<Sprite>> bdImages = new Dictionary<string, List<Sprite>>();
	public OnLoadCompleted onLoadCompleted;

    private string url = "http://magictv.hospedagemdesites.ws/nagumoplay/site/assets/img/nagumoapp/";
	private string pathFiles;
	private List<WWW> wwws = new List<WWW>();
	private ChangeBackgroundImage changeBackgroundImage;

	void OnEnable () 
	{
		bdImages.Clear();
		changeBackgroundImage = GameObject.FindObjectOfType(typeof(ChangeBackgroundImage)) as ChangeBackgroundImage;

		pathFiles = Application.persistentDataPath;

		//verifica se tem atualizacao no servidor se ja tiver passado por aqui ele ignora.
		if(bdImages.Count <= 0)
		{
			StartCoroutine(CheckFilesUpdate());
		}
		else
		{
			if(onLoadCompleted != null)
				onLoadCompleted();
		}
	}

	public IEnumerator CheckFilesUpdate()
	{
		WWW www = new WWW(url + "getall.php");
		
		yield return www;
		
		if(!string.IsNullOrEmpty(www.error))
		{
			Debug.Log(www.error);
            gameObject.SetActive(false);
		}
		else
		{
			//armazena a informacao de todas imagens do servidor.
			string[] paths = www.text.Split(';');
			List<string> pathsList = new List<string>();
			pathsList.AddRange(paths);

			foreach(string path in paths)
			{
				Debug.Log("path " + path);
				if(path == "")
					break;

				//organiza pasta e nome do arquivo no dicionario.
				string folder = path.Split('/')[1].ToLower();
				string fileName = path.Split('/')[2];
				string pathDirectory = pathFiles + "/" + Path.GetDirectoryName(path);

				//verifica se ja tem a pasta salva localmente, se n existir cria ela.
				if(!Directory.Exists(pathDirectory))
				{
					Directory.CreateDirectory(pathDirectory);
					Debug.Log("Criado diretorio " + pathDirectory);
				}

				//verifica se o arquivo (jpg) existe localmente, se n existir cria ele.
				if(!File.Exists(pathDirectory + "/" + fileName))
				{
					StartCoroutine(Download(url + "capas/" + folder, pathDirectory, fileName));
				}

				//verifica se o arquivo da pasta ainda esta no servidor se n estiver deleta ele.
				foreach(string localFile in Directory.GetFiles(pathDirectory))
				{
					string lFile = Path.GetFileName(localFile);
					string lFileDir = Path.GetDirectoryName(localFile);
					string lDirFile = localFile.Split('/')[localFile.Split('/').Length - 2] + "/" +
									localFile.Split('/')[localFile.Split('/').Length - 1];

					if(!pathsList.Contains("capas/" + lDirFile))
					{
						Debug.Log("Deletado " + localFile);
						if(File.Exists(localFile))
							File.Delete(localFile);
					}
				}
			}

		}

		//verifica se tem download em andamento.
		while(!wwws.TrueForAll(WwwIsDone) && wwws.Count > 0)
		{

			yield return new WaitForEndOfFrame();
		}

		//verifica se a pasta de capas existe se existe carrega os arquivos.
		if(Directory.Exists(pathFiles + "/capas/"))
			StartCoroutine(LoadFiles());
		else
			Debug.LogError("N foram carregados os arquivos para o path local " + pathFiles + "/capas/"); 
	}

	public static bool WwwIsDone(WWW www)
	{
		return www.isDone;
	}

	public IEnumerator Download(string url, string folder, string file)
	{
		Debug.Log("Baixando " + url + "/" + file);

		WWW www = new WWW(url + "/" +file);
		wwws.Add(www);

		while( !www.isDone ) 
		{
			//a barrinha sobe ate a metade no download a outra metade eh load da pasta local.
			changeBackgroundImage.loadBar.fillAmount += (www.progress / wwws.Count) / 2f;
			yield return null;
		}

		if(!string.IsNullOrEmpty(www.error))
		{
			Debug.LogError(www.error);
		}
		else
		{
			File.WriteAllBytes(folder + "/" + file, www.bytes);
		}
	}

	public IEnumerator LoadFiles()
	{
		changeBackgroundImage.loadBar.fillAmount = 0.5f;

		//armazena no dicionario as imagens das pastas.
		foreach(string folderPath in Directory.GetDirectories(pathFiles + "/capas/"))
		{
			//Debug.Log(folderPath);
			string folderName = Path.GetFileName(folderPath);
			//Debug.Log(folderName);

			if(!bdImages.ContainsKey(folderName))
			{
				bdImages.Add(folderName, new List<Sprite>());
			}
			//pega a imagem da pasta local e converte em Texture2D.
			foreach(string filePath in Directory.GetFiles(folderPath))
			{
				string imgName = Path.GetFileName(filePath);

				Debug.Log("lendo de " + filePath);

				byte[] bytes = File.ReadAllBytes(filePath);
#if UNITY_IOS
				Texture2D text = new Texture2D(2, 2, TextureFormat.PVRTC_RGBA4, false);
#else
				Texture2D text = new Texture2D(2, 2);
#endif
				text.LoadImage(bytes);

				//transforma a tesxtura 2d em sprite e guarda na lista.. eh mais rapido assim.
				Sprite sprite = Sprite.Create(text, new Rect(0,0,text.width, text.height), Vector2.one * 0.5f);
                sprite.name = imgName.Replace(".png", "").Replace(".jpg", "").Replace(".jpeg", "").Replace(".bmp", "").Replace(".PNG", "").Replace(".JPEG", "").Replace(".JPG", "").Replace(".BMP", "");
				bdImages[folderName].Add(sprite);

				//barrinha.
				yield return new WaitForEndOfFrame();

				changeBackgroundImage.loadBar.fillAmount += Mathf.InverseLerp(0, Directory.GetFiles(folderPath).Length * Directory.GetDirectories(pathFiles + "/capas/").Length, 1f);
			}
		}

		if(onLoadCompleted != null)
			onLoadCompleted();
	}
}
