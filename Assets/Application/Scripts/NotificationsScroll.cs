﻿using UnityEngine;
using System.Collections;
//using UTNotifications;
using UnityEngine.UI;
using System.Collections.Generic;

public class NotificationsScroll : MonoBehaviour {

	public GameObject notificatonObj;
	public GameObject notificationContainer;

	private NotificationsManager notificationsManager;
	private List<GameObject> objsList = new List<GameObject> ();
    private List<string> idNotificationsLst = new List<string> ();

	// Use this for initialization
	void OnEnable ()
	{
        //Debug.Log(NotificationsManager.ClearString("Teste t.e.s.t.e.s ; :rabit: áâã çççççç %$# * de mensagem grande la laalalalal ala al ala al ala la ala la al ala la al ala ala al ala ala la ala la ala al ala al al"));


//		Manager.Instance.SetBadge (0);

		//InstantiateNotifications ("test", "test", System.DateTime.Now.ToString ());

		Debug.Log ("OnEnable NotificationsScroll");
		notificationsManager = GameObject.FindObjectOfType (typeof (NotificationsManager)) as NotificationsManager;

		string [] notificationsIndexs = PlayerPrefs.GetString ("NotificationsIndex").Split (';');

		foreach (string notificationID in notificationsIndexs)
		{
			if (!string.IsNullOrEmpty (notificationID)) {
				//pega id;title;text;date.
				string [] notificationsData = PlayerPrefs.GetString ("Notification_" + notificationID).Split (';');
				string id = notificationsData [0];
				string title = notificationsData [1];
				string text = notificationsData [2];
				string date = notificationsData [3];

				if (!idNotificationsLst.Contains (id)) {
					InstantiateNotifications (title, text, date);
					idNotificationsLst.Add (id);
				}
			}	
		}

/*		foreach (ReceivedNotification rn in notificationsManager.m_receivedNotifications) {
			//so add se ja n tiver esse id na lista.
			if (!idNotificationsLst.Contains (rn.id)) {
				InstantiateNotifications (rn.title, rn.text, System.DateTime.Now.ToString ());
				idNotificationsLst.Add (rn.id);
			}
		}*/

		notificationContainer.GetComponentInParent <GridLayoutGroup>().CalculateLayoutInputVertical ();
	}
	
	private void InstantiateNotifications (string title, string text, string dateTime)
	{
		Debug.Log ("Instancia notificacion title: " + title);

		var obj = Instantiate (notificatonObj);
		obj.transform.parent = notificationContainer.transform;
		obj.transform.localScale = Vector3.one;
		obj.transform.localPosition = Vector3.zero;
		obj.GetComponent<NotificationObj> ().text = text;
		obj.GetComponent<NotificationObj> ().title = title;
		obj.GetComponent<NotificationObj> ().dateTime = dateTime;

		objsList.Add (obj);
	}
}
