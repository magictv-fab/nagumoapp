﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CoverFocus : MonoBehaviour
{
    public static Transform tmpChild;

    public float scaleFront = 1.3f;
    public float scaleBack = 0.9f;
    public float scalePercent = 100f;
    public Button caralhodebutton;


    private RectTransform rect;
    private Camera camera;
    private InfiniteHorizontalScroll scrollRect;

    // Use this for initialization
    void Start()
    {
        camera = GetComponentInParent<Canvas>().worldCamera;
        if (!tmpChild)
            tmpChild = transform;

        rect = GetComponent<RectTransform>();

        scrollRect = GetComponentInParent<InfiniteHorizontalScroll>();
    }

    // Update is called once per frame
    void OnGUI()
    {
        float t = Time.deltaTime * 10f;


        if (Mathf.Abs((transform.position.x) - camera.transform.position.x) < transform.lossyScale.x * scalePercent)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(scaleFront, scaleFront, 1f), t);
            transform.GetComponent<Image>().color = Color.white;
            transform.localEulerAngles = Vector3.zero;

            if (caralhodebutton)
                caralhodebutton.enabled = true;

            if (ScrollRectToolInfinity.selectChildren != null)
            {
                //quando a velocidade for menor q 70 e for diferente do ultimo selecionado e n estiver tocando a tela.
                if (transform != tmpChild && Mathf.Abs(scrollRect.velocity.x) < 70f && !Input.GetMouseButton(0))
                {
                    tmpChild = transform;
                    ScrollRectToolInfinity.selectChildren(name);
                }
            }

            if (PedidoAcougue.instance != null)
            {
                if (PedidoAcougue.instance.carneTitle)
                {
                    if (PedidoAcougue.instance.carneTitle.text != transform.parent.name)
                    {
                        PedidoAcougue.instance.carneTitle.text = transform.parent.name;
                        //PedidoAcougue.instance.dropdownTipoDeCorte.options.Clear();
                        PedidoAcougue.instance.dropdownTipoDeCorte.options = transform.parent.GetComponent<CarneValues>().optionDatas;
                        //PedidoAcougue.instance.dropdownTipoDeCorte.options.AddRange(transform.parent.GetComponent<CarneValues>().optionDatas);

                        PedidoAcougue.instance.pedidoData.sprite = transform.parent.GetComponent<CarneValues>().img.sprite;

                        PedidoAcougue.instance.pedidoData.idCarne = transform.parent.GetComponent<CarneValues>().id;
                    }
                }
            }
        }
        else
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(scaleBack, scaleBack, 1f), t);
            transform.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            if (caralhodebutton)
                caralhodebutton.enabled = false;
        }

    }
}