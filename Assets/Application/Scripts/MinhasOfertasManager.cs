﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MinhasOfertasManager : MonoBehaviour {

    public static OfertasData ofertasData;
    public static MinhasOfertasManager instance;

    public static List<string> titleLst = new List<string>(),
                        valueLst = new List<string>(),
                        infoLst = new List<string>(),
                        idLst = new List<string>(),
                        validadeLst = new List<string>(),
                        aquisicaoLst = new List<string>();
    public static List<int> unidadesLst = new List<int>();
    public static List<float> pesoLst = new List<float>();
    public static List<Sprite> imgLst = new List<Sprite>();

    public GameObject loadingObj, popup;
    public GameObject itemPrefab;
    public GameObject containerItens;
    public static List<GameObject> itensList = new List<GameObject>();

    private ScrollRect scrollRect;
    private int ofertaCounter;
    private GameObject currentItem;

    private static string currentJsonTxt;
    private static int loadedImgsCount;

	// Use this for initialization
	void Start () 
    {
        instance = this;
        itensList.Clear();
        StartCoroutine(ILoadOfertas());
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void InstantiateOfertas()
    {
        if (ofertaCounter >= titleLst.Count)
            return;

        for (int i = 0; i < titleLst.Count; i++)
        {
            var obj = Instantiate(itemPrefab);
            obj.transform.SetParent(containerItens.transform, false);
            obj.transform.localScale = Vector3.one * 0.9f;
            obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;


            OfertaValues ofertaValues = obj.GetComponent<OfertaValues>();

            ofertaValues.title.text = titleLst[ofertaCounter];
            ofertaValues.value.text = valueLst[ofertaCounter];
            ofertaValues.info.text = infoLst[ofertaCounter];
            //ofertaValues.img.sprite = imgLst[ofertaCounter];
            ofertaValues.id = idLst[ofertaCounter];
            ofertaValues.validade.text = "Validade: " + OfertasManager.GetDateString(validadeLst[ofertaCounter]);
            ofertaValues.aquisicao.text = "Adesão: " + OfertasManager.GetDateTimeString(aquisicaoLst[ofertaCounter]);
            //ofertaValues.unidades.text = "Unidades: " + unidadesLst[ofertaCounter];

            if (unidadesLst[ofertaCounter] > 0)
            {
                ofertaValues.unidades.text = "Unidades: " + unidadesLst[ofertaCounter];
            }
            else
            {
                if (pesoLst[ofertaCounter] >= 1000)
                {
                    ofertaValues.unidades.text = "Peso: " + (pesoLst[ofertaCounter] * 0.001f).ToString("0.###").Replace('.', ',') + " Kg";
                }
                else
                {
                    ofertaValues.unidades.text = "Peso: " + pesoLst[ofertaCounter] + " grama(s)";
                }
            }

            //obj.transform.SetAsFirstSibling();
            currentItem = containerItens.transform.GetChild(containerItens.transform.childCount - 1).gameObject;

            // if (ofertaCounter < titleLst.Count - 1)
            ofertaCounter++;
            itensList.Add(obj);
        }

        StartCoroutine(ILoadImgs());

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    public void MinhaOfertaRemove(string id)
    {
        StartCoroutine(IMinhaOfertaRemove(id));
    }

    private IEnumerator IMinhaOfertaRemove(string id)
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\",\"id_oferta\":\"" + id + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/excluir_oferta_participante.php", pData, headers);

        //    if (loadingObj)
        //      loadingObj.SetActive(true);

        yield return www;
    }

    private IEnumerator ILoadOfertas()
    {

        string json = "{\"login\":\"" + Login.userData.cpf + "\",\"senha\":\"" + Login.Base64Encode(Login.userData.senha) + "\"}";
        Debug.Log("Json: " + json);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] pData = System.Text.Encoding.UTF8.GetBytes(json.ToCharArray());

        WWW www = new WWW(ServerControl.url + "/pegar_ofertas_participante.php", pData, headers);

        if (loadingObj)
            loadingObj.SetActive(true);

        yield return www;

        Debug.Log("Url: " + www.url);

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Erro: " + www.error);

            PopUp(www.error);
            if (loadingObj)
                loadingObj.SetActive(false);
        }
        else
        {
            Debug.Log(www.text);

            if (www.text == "{\"ofertas\":[]}")
            {
                if (loadingObj)
                    loadingObj.SetActive(false);
            }
            else
            {
                //verifica se o txt que chegou eh diferente do que tem ja.
                if (currentJsonTxt != www.text)
                {
                    Debug.Log("Tem atualizacao de Minhas Ofertas!");
                    currentJsonTxt = www.text;

                    //Atualiza:
                    if (loadingObj)
                        loadingObj.SetActive(true);

                    //LoaderPublishAnim.instance.EndLoad();

                    foreach (GameObject obj in itensList)
                        Destroy(obj);

                    itensList.Clear();

                    titleLst.Clear();
                    valueLst.Clear();
                    infoLst.Clear();
                    idLst.Clear();
                    imgLst.Clear();
                    unidadesLst.Clear();
                    pesoLst.Clear();
                    validadeLst.Clear();
                    aquisicaoLst.Clear();

                    ofertaCounter = 0;
                    loadedImgsCount = 0;

                    try
                    {
                        string message = SimpleJSON.EscapeString(www.text);
                        Debug.Log(message);

                        //popula obj json.
                        ofertasData = JsonUtility.FromJson<OfertasData>(message);

                        Debug.Log(ofertasData.ofertas[0].titulo);

                        //passa as informacoes para as listas.
                        foreach (OfertaData ofertaData in ofertasData.ofertas)
                        {
                            titleLst.Add(ofertaData.titulo);
                            //valueLst.Add(ofertaData.desconto.ToString() + "%");

                            if (ofertaData.desconto <= 0)
                            {
                                valueLst.Add("Pague: " + ofertaData.pague.ToString() + " e leve: " + ofertaData.leve.ToString());
                            }
                            else
                            {
                                valueLst.Add("Desconto: " + ofertaData.desconto.ToString() + "%");
                            }
                            infoLst.Add(ofertaData.texto);
                            idLst.Add(ofertaData.id_oferta.ToString());
                            validadeLst.Add(ofertaData.datafinal);
                            aquisicaoLst.Add(ofertaData.dataAquisicao);
                            unidadesLst.Add(ofertaData.unidade);
                            pesoLst.Add(ofertaData.peso);
                        }

                        InstantiateOfertas();
                    }
                    catch
                    {
                        PopUp(www.text);
                        if (loadingObj)
                            loadingObj.SetActive(false);
                    }
                }
                else
                {
                    StartCoroutine(LoadLoadeds());
                }
            }
        }
    }

    private IEnumerator ILoadImgs()
    {
        int i = 0;
        foreach (OfertaData ofertaData in ofertasData.ofertas)
        {
            if (i >= loadedImgsCount)
            {
                WWW www = new WWW(ServerControl.urlIMGOfertas + "/" + ofertaData.imagem);
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    Sprite spt = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
                    imgLst.Add(spt);
                    itensList[i].GetComponent<OfertaValues>().img.sprite = spt;
                    loadedImgsCount++;

                }
            }
            else
            {
                itensList[i].GetComponent<OfertaValues>().img.sprite = imgLst[i];
            }

            i++;
        }


       // scrollRect = GetComponent<ScrollRect>();

        //InstantiateOfertas();

        yield return new WaitForEndOfFrame();

       // scrollRect.content = containerItens.transform.GetChild(containerItens.transform.childCount - 1).GetComponent<RectTransform>();

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    private IEnumerator LoadLoadeds()
    {
        if (loadingObj)
            loadingObj.SetActive(true);

        InstantiateOfertas();

        yield return new WaitForEndOfFrame();

        if (loadingObj)
            loadingObj.SetActive(false);
    }

    public void PopUp(string txt)
    {
        Debug.Log("Popup: " + txt);
        if (txt.Contains("resolve host"))
        {
            txt = "Sem conexão com a internet.";
        }
        if (popup)
        {
            popup.GetComponentInChildren<Text>().text = txt;
            popup.SetActive(true);
        }
    }
}
