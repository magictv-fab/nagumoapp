﻿using UnityEngine;
using System.Collections;

public class ChangeObj : MonoBehaviour {

    public GameObject go1, go2;
    public Vector3 pos1, pos2, rot1, rot2, size1, size2;

    private bool change;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void ButtonChange()
    {
        if(!change)
        {
            go1.transform.localPosition = pos1;
            go1.transform.localEulerAngles = rot1;
            go1.transform.localScale = size1;
			go2.transform.localPosition = pos2;
			go2.transform.localEulerAngles = rot2;
			go2.transform.localScale = size2;
        }
        else
        {
			go2.transform.localPosition = pos1;
			go2.transform.localEulerAngles = rot1;
			go2.transform.localScale = size1;
			go1.transform.localPosition = pos2;
			go1.transform.localEulerAngles = rot2;
			go1.transform.localScale = size2;           
        }

        change = !change;
    }

}
