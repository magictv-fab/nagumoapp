﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
 
public class JC_Slider : Slider
{
    public float StepsIncrement = .1f;
 
    public override void OnMove(AxisEventData eventData)
    {
        if((direction == Direction.RightToLeft || direction == Direction.LeftToRight) &&
            (eventData.moveDir == MoveDirection.Left || eventData.moveDir == MoveDirection.Right))
        {
            this.value += eventData.moveDir == MoveDirection.Left ? -StepsIncrement : StepsIncrement;
            return;
        }
 
        if ((direction == Direction.BottomToTop || direction == Direction.TopToBottom) &&
            (eventData.moveDir == MoveDirection.Down || eventData.moveDir == MoveDirection.Up))
        {
            this.value += eventData.moveDir == MoveDirection.Down ? -StepsIncrement : StepsIncrement;
            return;
        }
     
        base.OnMove(eventData);
    }
 
}