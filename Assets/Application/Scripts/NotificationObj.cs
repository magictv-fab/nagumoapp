﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotificationObj : MonoBehaviour {

	public Text titleText, textText, dateText;
	public string title, text, dateTime;

	// Use this for initialization
	void Start () 
	{
		titleText.text = title;
		textText.text = text;
		dateText.text = dateTime;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
