﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollRectTool : MonoBehaviour {

	public delegate void SelectChildren(string name);
    public static ScrollRectTool instance;

    public bool changeColorScale = true;
	public float snapMinVel = 70f;
	public float rotMultiplayer = 0.1f;
	public SelectChildren selectChildren;
	public float scaleFront = 1.3f;
	public float scaleBack = 0.9f;
	public int sideCells = 2;
	public bool center = true;
	public int backCells;

	private GridLayoutGroup grid;
	private RectTransform rect;
	private ScrollRect scrollRect;
	private Vector2 targetPos;
	private bool done = false;
	private float t = 0;
	private float desaceleration;
	private Transform tmpChild;
	private float fixCelSize;
	private float fixPair;

	public void Start() {
        instance = this;
		grid = GetComponentInChildren<GridLayoutGroup>();
		rect = GetComponent<RectTransform>();
		scrollRect = GetComponentInParent<ScrollRect>();
		// auto adjust the width of the grid to have space for all the childs
		//rect.sizeDelta = new Vector2((transform.childCount + sideCells ) * grid.cellSize.x + (transform.childCount - 1f) * grid.spacing.x, rect.sizeDelta.y);
		desaceleration = scrollRect.decelerationRate;

		fixPair = (1-(transform.childCount%2));

		rect.sizeDelta = new Vector2((transform.childCount + sideCells + fixPair) * grid.cellSize.x + (transform.childCount - 1f) * grid.spacing.x, rect.sizeDelta.y);

		fixCelSize = fixPair * (grid.cellSize.x + grid.spacing.x);

		if(!center)
			targetPos = new Vector2(rect.sizeDelta.x/2, 0);

	}

    void LateUpdate()
	{
		if(fixPair > 0.9f)
		{
			if(transform.localPosition.x < -rect.sizeDelta.x/2f + fixCelSize * 1.1f)
			{
				transform.localPosition = new Vector3(-rect.sizeDelta.x/2f + fixCelSize * 1.1f, transform.localPosition.y);
				done = false;
			}

		}
	}

	public void Update() {
		t = Time.deltaTime * 15f;
		if (t > 1f) {
			t = 1f;
		}


        if (Input.GetMouseButton(0))
        {
            TouchDown();
        }

        if (Input.GetMouseButtonUp(0))
        {
            done = false;
        }

		if (Mathf.Abs(scrollRect.velocity.x) > snapMinVel) {
			scrollRect.decelerationRate = desaceleration;
			TouchUp();
		}
		else if (!done) {

			if(transform.childCount > 1)
				rect.localPosition = Vector2.Lerp(rect.localPosition, targetPos, t);

			if (Mathf.Abs(rect.localPosition.x - targetPos.x) < 0.05f) 
			{
				rect.localPosition = targetPos;
				scrollRect.decelerationRate = Mathf.Lerp(scrollRect.decelerationRate, 0, Time.deltaTime * 100f);
				done = true;
			}
		}

		Vector2 tempPos = new Vector2(Mathf.Round((rect.localPosition.x) / (grid.cellSize.x + grid.spacing.x)) * (grid.cellSize.x + grid.spacing.x) * -1f, 0);

        if (changeColorScale)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                if (Mathf.Abs((child.localPosition.x - (grid.spacing.x / 2) * fixPair) - tempPos.x) < 0.05f)
                {
                        child.localScale = Vector3.Lerp(child.localScale, new Vector3(scaleFront, scaleFront, 1f), t);
                        child.GetComponentInChildren<Image>().color = Color.white;
                        child.localEulerAngles = Vector3.zero;
                    if (child != tmpChild)
                    {
                        tmpChild = child;
                        if (selectChildren != null)
                            selectChildren(child.name);

                        //if(child == transform.GetChild(transform.childCount - 1))
                        //	scrollRect.StopMovement();
                    }
                }
                else
                {
                        child.localScale = Vector3.Lerp(child.localScale, new Vector3(scaleBack, scaleBack, 1f), t);
                        child.GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0.5f);
                    //child.localEulerAngles = Vector3.up * (child.position.x) * rotMultiplayer;
                }
            }
        }
	}
	public void TouchDown() 
	{
		done = true;

	}
	public void TouchUp() 
	{
		float newX = Mathf.Round((rect.localPosition.x)/ (grid.cellSize.x + grid.spacing.x)) * (grid.cellSize.x + grid.spacing.x);
		//float newX = (Mathf.Round(rect.localPosition.x / (grid.cellSize.x + grid.spacing.x)) - ((1 - (transform.childCount%2)) * 0.5f)) * (grid.cellSize.x + grid.spacing.x);
		newX = Mathf.Sign(newX) * Mathf.Min(Mathf.Abs(newX), (rect.rect.width - scrollRect.GetComponent<RectTransform>().rect.width) / 2f);
		//newX = Mathf.Sign(newX) * Mathf.Min(Mathf.Abs(newX), ((rect.rect.width +((1 - (transform.childCount%2)) * ((grid.cellSize.x + grid.spacing.x)/2))) / 2) + (grid.cellSize.x + grid.spacing.x)/2);
		targetPos = new Vector2(newX, 0);
		done = false;
	}

}
