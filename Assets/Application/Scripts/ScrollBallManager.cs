﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollBallManager : MonoBehaviour 
{
    public ScrollRect scrollRect;

    public float xPos, xSize;

    private Scrollbar scrollbar;

	// Use this for initialization
	void Start () 
    {
        scrollbar = GetComponent<Scrollbar>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        xPos = scrollRect.content.localPosition.x + scrollRect.content.rect.width / 2f;
        xSize = scrollRect.content.rect.width;

        scrollbar.value = Mathf.InverseLerp(0, xSize, xPos);
	}
}
