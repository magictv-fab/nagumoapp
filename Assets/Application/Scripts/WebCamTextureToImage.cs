﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WebCamTextureToImage : MonoBehaviour {

	WebCamTexture webCamTexture; 


	// Use this for initialization
	void Start () 
	{
		WebCamDevice wcdevice = new WebCamDevice();
		webCamTexture = new WebCamTexture(wcdevice.name);

		webCamTexture.Play();

		#if UNITY_IOS || UNITY_IPHONE
			transform.localScale = new Vector3(webCamTexture.width, webCamTexture.height) * 1f;
		#else
			transform.localScale = new Vector3(webCamTexture.width, webCamTexture.height) * 0.01f;
		#endif

	}

	void OnDestroy()
	{
		webCamTexture.Stop();
	}

	// Update is called once per frame
	void Update () 
	{
		if(webCamTexture.isPlaying)
			GetComponent<RawImage>().texture = (Texture)webCamTexture;

	}
}
