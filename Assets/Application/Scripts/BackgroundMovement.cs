﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackgroundMovement : MonoBehaviour {

	private float acelerometerY;
	public float cornerX;

	// Use this for initialization
	void Start () 
	{
		Input.gyro.enabled = true;

		acelerometerY = Input.gyro.rotationRate.y;
		cornerX = (transform.root.GetComponent<RectTransform>().rect.width - GetComponent<RectTransform>().rect.width) / 2;

	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localPosition += Vector3.right * -Input.gyro.rotationRate.y * 1f;

		Vector3 pos = transform.localPosition;
		pos.x = Mathf.Clamp(pos.x, cornerX, -cornerX);
		transform.localPosition = pos;
	}
}
