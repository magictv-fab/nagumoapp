﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonAlterarSenha : MonoBehaviour {

    public GameObject seta, groupSenhas;
    public RectTransform rectTransform;

    private bool openClose;
    private int rectCloseY = 1759;
    private int rectOpenY = 2059;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Action()
    {
        if(openClose)
        {
            seta.transform.localEulerAngles = Vector3.forward * 90;

            rectTransform.sizeDelta = new Vector2(0, rectCloseY);
        }
        else
        {
            seta.transform.localEulerAngles = Vector3.forward * -90;

            rectTransform.sizeDelta = new Vector2(0, rectOpenY);
        }

        openClose = !openClose;
        groupSenhas.SetActive(openClose);

    }
}
