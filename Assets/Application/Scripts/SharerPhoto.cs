﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SharerPhoto : MonoBehaviour {

    public static int awardIndex;
    public List<GameObject> frameLst = new List<GameObject>();
    public Texture2D tex;
    public Image imageRefCut;

    private bool take;

    public GameObject parabensPanel;
    public GameObject loadingPanel;

	// Use this for initialization
	void Awake () 
    {
        frameLst[awardIndex].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Start(){


	}

    public void TakePicture()
    {
        take = true;
    }

    public void OnPostRender()
    {
        if(take)
        {
            take = false;

            var rect = imageRefCut.rectTransform.rect;



            int width = Mathf.RoundToInt(rect.width);
            int height = Mathf.RoundToInt(rect.height);
            int startX = Mathf.RoundToInt(rect.x);
            int startY = Mathf.RoundToInt(rect.y);

//            Debug.Log(startX);

			Debug.Log("Width= " + width);
			Debug.Log("height= " + height);
			Debug.Log("startX= " + startX);
			Debug.Log("startY= " + startY);

            tex = new Texture2D(width, height, TextureFormat.RGB24, false);

            Rect rex = new Rect(startX, startY, width, height);

            tex.ReadPixels(rex, 0, 0, false);
            tex.Apply();

            // Encode texture into PNG
            var bytes = tex.EncodeToPNG();
            //Destroy(tex);

			Debug.Log("Salvando by Rect");
            System.IO.File.WriteAllBytes(Application.dataPath + "SavedScreen.png", bytes);

            Debug.Log(Application.dataPath);
        }
    }

    IEnumerator delay(){
         yield return new WaitForSeconds(2f);
        parabensPanel.SetActive(true);
        loadingPanel.SetActive(false);
    }

    public void showSharePanel(){

        StartCoroutine("delay");

    }
}
