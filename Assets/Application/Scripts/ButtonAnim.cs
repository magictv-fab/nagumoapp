﻿using UnityEngine;
using System.Collections;

public class ButtonAnim : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Hashtable ht = new Hashtable ();
		ht.Add ("scale", Vector3.one * 1.04f);
		ht.Add ("time", 0.5f);
		ht.Add ("easetype", iTween.EaseType.easeInOutSine);
		ht.Add ("looptype", iTween.LoopType.pingPong);

		iTween.ScaleTo (gameObject, ht);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
