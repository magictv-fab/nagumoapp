﻿using UnityEngine;
using System.Collections;

public class ActiveRoletas : MonoBehaviour {

    public float time = 5f;
    public bool active = false;

    void Start()
    {
        StartCoroutine(Go());
    }
	// Use this for initialization
    IEnumerator Go () 
    {
        yield return new WaitForSeconds(time);
        gameObject.GetComponent<Renderer>().enabled = active;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
