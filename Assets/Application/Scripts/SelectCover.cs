﻿using UnityEngine;
using System.Collections;

public class SelectCover : MonoBehaviour {

	public static string clickedName = "ofertas";
	public static string clickedEdition = "";
	
	public void ClickCover(GameObject sender)
	{
		clickedName = sender.name.ToLower();
		Debug.Log("Clicado no " + clickedName);
		Application.LoadLevel("EditionSelect");
	}

	public void ClickEdition()
	{
		clickedEdition = name; 

		Debug.Log("edicao selecionada " + clickedEdition);

		//GameObject.Find("Debug Revisao TxT").GetComponent<UnityEngine.UI.Text>().text = "Revisão: " + clickedEdition;

		Application.LoadLevel("cena_load_main");
	}
}
