﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollRectToolInfinity : MonoBehaviour {

	public delegate void SelectChildren(string name);

	public static SelectChildren selectChildren;

	public float snapMinVel = 70f;

	private RectTransform rect;
	private ScrollRect scrollRect;
	private float desaceleration;


	void Start() 
	{
		rect = GetComponent<RectTransform>();
		scrollRect = GetComponentInParent<ScrollRect>();

		desaceleration = scrollRect.decelerationRate;
	}
	

	public void Update() 
	{
		if (Mathf.Abs(scrollRect.velocity.x) > snapMinVel || Input.GetMouseButton(0)) 
		{
			scrollRect.decelerationRate = desaceleration;
		}
		else if(selectChildren != null && CoverFocus.tmpChild != null)
		{
			//pega a posicao do gameObject em foco e subtrai pela metade da tela (pois vai alinhar no centro). 
			//scrollRect.decelerationRate = 0;
			Vector3 pos = rect.localPosition;
			pos.x = Mathf.Lerp(pos.x, pos.x - (CoverFocus.tmpChild.position.x - Screen.width/2f), Time.deltaTime * 10);
			rect.localPosition = pos;
		}

	}


}
