﻿using UnityEngine;
using System.Collections;

public class TweenMove : MonoBehaviour {

    public iTween.EaseType easetype;
    public Vector3 position;
    public float time = 3f;

    private Vector3 startPositon;
    private bool flipflop;

    private void Start()
    {
        startPositon = transform.localPosition;
    }

    public void SlideIn()
    {
        Hashtable ht = new Hashtable();

        if(flipflop)
           ht.Add("position", startPositon);
        else
            ht.Add("position", position);

        ht.Add("speed", time);
        ht.Add("islocal", true);
        ht.Add("easetype", easetype);

        iTween.MoveTo(gameObject, ht);

        flipflop = !flipflop;
    }

    public void SlideOut()
    {
        Hashtable ht = new Hashtable();
        ht.Add("position", position);
        ht.Add("time", time);
        ht.Add("islocal", true);
        ht.Add("easetype", easetype);

        iTween.MoveTo(gameObject, ht);
    }

    private void OnEnable()
    {

    }

    // Update is called once per frame
    void Update () {
	
	}
}
