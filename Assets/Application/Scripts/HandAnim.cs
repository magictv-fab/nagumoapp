﻿using UnityEngine;
using System.Collections;

public class HandAnim : MonoBehaviour
{

	// Use this for initialization
	void Start () 
	{
		Hashtable ht = new Hashtable ();
		ht.Add ("rotation", Vector3.forward * 20f);
		ht.Add ("time", 1f);
		ht.Add ("easetype", iTween.EaseType.easeInOutExpo);
		ht.Add ("looptype", iTween.LoopType.pingPong);

		iTween.RotateTo (gameObject, ht);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
