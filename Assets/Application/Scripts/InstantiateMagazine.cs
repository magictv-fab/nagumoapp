﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InstantiateMagazine : MonoBehaviour {

	public GameObject magazineCoverObj;

    private void OnDisable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }      
    }

    public void OnEnable () 
	{
		if(!DownloadImages.bdImages.ContainsKey(SelectCover.clickedName))
		{
			Debug.LogError("N tem capa para a revista " + SelectCover.clickedName);
			return;
		}

		foreach(Sprite coverSprite in DownloadImages.bdImages[SelectCover.clickedName])
		{
			var obj = Instantiate(magazineCoverObj);
			obj.transform.parent = transform;
			obj.name = coverSprite.name;
			obj.GetComponent<Image>().sprite = coverSprite;
		}

        GetComponent<GridLayoutGroup>().CalculateLayoutInputHorizontal();
        ScrollRectTool.instance.Start();

	}
	
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Back();
		}
	}

	public void Back()
	{
		Application.LoadLevel("TitleSelect");
	}
}
