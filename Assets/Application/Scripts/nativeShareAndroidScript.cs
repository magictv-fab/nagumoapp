﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class nativeShareAndroidScript : MonoBehaviour {

    public GameObject CanvasShareObj;

 
    private bool isProcessing = false;
    private bool isFocus = false;

	int marginImage = 100;
	int marginImageBottom = 50;
 
    public void ShareBtnPress()
    {
        if (!isProcessing)
        {
//            CanvasShareObj.SetActive(true);
            StartCoroutine(ShareScreenshot());
        }
    }
 
    IEnumerator ShareScreenshot()
    {
        isProcessing = true;
 
        yield return new WaitForEndOfFrame();


		Texture2D tex = new Texture2D(Screen.width, Screen.height/2 + marginImage);	
		tex.ReadPixels(new Rect(0,Screen.height/4 - marginImageBottom , Screen.width, Screen.height/2 + marginImage),0,0);
		tex.Apply();


		string destination = Path.Combine(Application.persistentDataPath, "meupremio.png");
		Debug.Log(destination);

		// Save picture on file
		byte[] bytes = tex.EncodeToJPG();
		File.WriteAllBytes(destination, bytes);  
        
 
        yield return new WaitForSeconds(2f);
 
        if (!Application.isEditor)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            //AndroidJavaClass uriClass = new AndroidJavaClass("com.instagram.android");
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");



            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"),
                uriObject);
            
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"),
                "Veja o meu prêmio");
            
            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser",
                intentObject, "Compartilhe sua foto");
            currentActivity.Call("startActivity", chooser);
 
            yield return new WaitForSeconds(2f);
        }
 
        yield return new WaitUntil(() => isFocus);
//        CanvasShareObj.SetActive(false);



        isProcessing = false;
    }
 
    private void OnApplicationFocus(bool focus)
    {
        isFocus = focus;


        //StartCoroutine(ShowPanelOk());

        //PopupShareOk.SetActive(true);
    }



    IEnumerator ShowPanelOk(){
        
        yield return new WaitForSeconds(5f);


    }
}
