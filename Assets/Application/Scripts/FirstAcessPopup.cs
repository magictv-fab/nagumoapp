﻿using UnityEngine;
using System.Collections;

public class FirstAcessPopup : MonoBehaviour {
    

	// Use this for initialization
	void Awake () {
        if (PlayerPrefs.GetInt("firstPopup") == 1)
        {
            gameObject.SetActive(false);
        }
	}
	
    public void Close()
    {
        gameObject.SetActive(false);
        PlayerPrefs.SetInt("firstPopup", 1);
    }


	// Update is called once per frame
	void Update () {
	
	}
}
