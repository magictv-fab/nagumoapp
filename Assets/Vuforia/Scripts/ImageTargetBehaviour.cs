/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Connected Experiences, Inc.
==============================================================================*/

using System.Collections.Generic;
using UnityEngine;
using MagicTV.processes;

namespace Vuforia
{
	/// <summary>
	/// This class serves both as an augmentation definition for an ImageTarget in the editor
	/// as well as a tracked image target result at runtime
	/// </summary>
	public class ImageTargetBehaviour : ImageTargetAbstractBehaviour,ITrackableEventHandler
	{
		private TrackableBehaviour mTrackableBehaviour;
		
		bool _registred = false ;
		
		void Start ()
		{
			
			tryToRegisterEvents ();
		}
		
		
		void tryToRegisterEvents ()
		{
			if (_registred) {
				return;
			}
			if (!mTrackableBehaviour) {
				mTrackableBehaviour = GetComponent<TrackableBehaviour> ();
			}
			if (!_registred && mTrackableBehaviour && PresentationController.Instance != null) {
				mTrackableBehaviour.RegisterTrackableEventHandler (this);
				//						PresentationController.Instance.RegisterAREvents ();
				//						mTrackableBehaviour.RegisterTrackableEventHandler (onTrackChange);
				_registred = true;
			}
		}
		
		#region ITrackableEventHandler implementation
		
		
		public void OnTrackableStateChanged (Status previousStatus, Status newStatus)
		{
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
				newStatus == TrackableBehaviour.Status.TRACKED ||
				newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
				
				PresentationController.Instance.OnTrackingFound (mTrackableBehaviour.TrackableName);
			} else {
				PresentationController.Instance.OnTrackingLost (mTrackableBehaviour.TrackableName);
			}
		}
		
		
		#endregion
		
		void Update ()
		{
			if (!_registred) {
				
				tryToRegisterEvents ();
			}
		}
	}
}