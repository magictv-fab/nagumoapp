﻿using UnityEngine;
using System.Collections;

public class WallSmall : MonoBehaviour {

	// This script is for the walls in middle, single and double obstacles
	// This scripts make them move from right side of screen to left side,
	// if the game is running

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!GameScene.gameOver && GameScene.gameStart){
			
			Vector3 moveDir = new Vector3(-1, 0, 0);
			gameObject.transform.Translate(moveDir * GameScene.speedWall * Time.deltaTime);

			if(gameObject.transform.position.x< -10){
				GameScene.makeNewWall = true;
				Destroy(gameObject);
			}

		}
	}

}








