﻿using UnityEngine;
using System.Collections;

// This script is attached to TouchControl gameObject. It has a collider covering the whole screen,
// so when anywhere in the screen is clicked or tapped, this code is run.

public class HeliControl : MonoBehaviour {
	
	private bool down;

	// Use this for initialization
	void Start () {
		down = false;
		GameScene.btnPlay.GetComponent<GUIText>().text = "Começar";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// When screen is touched
	void OnMouseDown() {
		down = true;
		// If it is first touch, then we start the game
		if(!GameScene.gameOver && !GameScene.gameStart){
			GameScene.initGame = true;
			GameScene.gameStart = true;
		}

		// If game is over and screen is tuched, we launch the level again
		if(GameScene.gameOver){
			Application.LoadLevel(Application.loadedLevel);
		}

		// When touched, we make up variable true and down false
		GameScene.leftUp = true;
		GameScene.leftDown = false;
	}

	// When screen is released
	void OnMouseUp() { 
		down = false;
		GameScene.leftUp = false;
		GameScene.leftDown = true;
			
	}

}





