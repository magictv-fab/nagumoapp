﻿using UnityEngine;
using System.Collections;

public class WallMovement : MonoBehaviour {

	// This script is for the movement of walls at upper border and lower border of screens,
	// We just keep them scrolliong from right side of screen to left side.
	// If a wall is gone off the screen, we place it beside the next wall so that it keeps on looping

	private GameObject WallUpL;
	private GameObject WallUpR;
	private GameObject WallDownL;
	private GameObject WallDownR;

	public static float diffY;

	// Use this for initialization
	void Start () {
	
		WallUpL = GameObject.Find("Wall Up L");
		WallUpR = GameObject.Find("Wall Up R"); 
		WallDownL = GameObject.Find("Wall Down L");
		WallDownR = GameObject.Find("Wall Down R");

		diffY = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(!GameScene.gameOver && GameScene.gameStart){

			Vector3 moveDir = new Vector3(-1, 0, 0);
			gameObject.transform.Translate(moveDir * GameScene.speedWall * Time.deltaTime);

			if(WallUpL.transform.position.x < -21){
				WallUpL.transform.position = new Vector2(WallUpR.transform.position.x + 21, 0 - diffY);
			}

			else if(WallUpR.transform.position.x < -21){
				WallUpR.transform.position = new Vector2(WallUpL.transform.position.x + 21, 0 - diffY);
			}

			else if(WallDownL.transform.position.x < -21){
				WallDownL.transform.position = new Vector2(WallDownR.transform.position.x + 21, -13 + diffY);
			}

			else if(WallDownR.transform.position.x < -21){
				WallDownR.transform.position = new Vector2(WallDownL.transform.position.x + 21, -13 + diffY);
			}


		}
	}



}








