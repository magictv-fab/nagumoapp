﻿using UnityEngine;
using System.Collections;

public class CollisionCheck : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Check if the gameObject this script is attached to has collided with the helicopter
	void OnTriggerEnter2D(Collider2D c) {
		// Debug.Log("collided1");
		if(c.gameObject.name == "copter" && !GameScene.gameOver){
			GameScene.gameOver = true;
		}
	}

}



