﻿using UnityEngine;
using System.Collections;

public class GameScene : MonoBehaviour {

	public GameObject copterL;

	public static bool leftUp;
	public static bool leftDown;

	public static bool gameOver;
	public static bool gameStart;

	public static float speedDown = 2.5f;
	public static float speedWall;

	public static GameObject btnPlay;

	public GameObject WallSingle;
	public GameObject WallDouble;
	private GameObject lastWall;

	public GameObject scoreText;
	public GameObject bestText;

	private int scoreNum;
	private int bestNum;
	
	public static bool makeNewWall;
	public static bool initGame;

	private float diffY1, diffY2;

	private static int difficultyNum;

	private Animator anim;

	public AudioClip heliClip;
	public static AudioSource heliSource;

	public AudioClip explodeClip;
	public static AudioSource explodeSource;

	// Use this for initialization
	void Start () {

		// Initialize variables
		Time.timeScale = 1;

		leftUp = false;
		leftDown = false;

		initGame = false;
		gameStart = false;
		gameOver = false;

		btnPlay = GameObject.Find("btn-play");

		makeNewWall = false; // this will be used to check and spawn new walls

		diffY1 = 2.5f; 	// this is used for positioning of single walls in middle between range (-2.5 , 2.5);
		diffY2 = 1; 	// this is used for positioning of double walls in middle between range (-1 , 1);
		difficultyNum = 1;	// starting difficulty, changes every few seconds with coroutine method, changeDifficulty()
		speedWall = 3.5f; // Speed of walls from right to left, it can also be increased when changing difficulty

		scoreNum = 0;
		scoreText.GetComponent<GUIText>().text = "Pontos: " + scoreNum;

		bestNum = PlayerPrefs.GetInt("best", 0); // get best score value stored from local storage
		bestText.GetComponent<GUIText>().text = "Recorde: " + bestNum;

		// here we select a random number, either 1 or 2. 
		// if its 1, single wall is spawned, if its 2, double wall is spawned.
		// randomWallY1 and randomWallY2 are variables for random positioning in vertical position

		int randomWall = Random.Range(1, 3);
		float randomWallY1, randomWallY2;
		GameObject wallMiddle1, wallMiddle2;
		if(randomWall <= 1){
			randomWallY1 = Random.Range(-diffY1, diffY1);
			randomWallY2 = Random.Range(-diffY1, diffY1);
			wallMiddle1 = Instantiate(WallSingle, new Vector3(6, randomWallY1, 0), Quaternion.identity) as GameObject;
			wallMiddle2 = Instantiate(WallSingle, new Vector3(wallMiddle1.transform.position.x + 12, randomWallY2, 0), Quaternion.identity) as GameObject;
		}else{
			randomWallY1 = Random.Range(-diffY2, diffY2);
			randomWallY2 = Random.Range(-diffY2, diffY2);
			wallMiddle1 = Instantiate(WallDouble, new Vector3(6, randomWallY1, 0), Quaternion.identity) as GameObject;
			wallMiddle2 = Instantiate(WallDouble, new Vector3(wallMiddle1.transform.position.x + 12, randomWallY2, 0), Quaternion.identity) as GameObject;
		}
		lastWall = wallMiddle2;

		anim = copterL.GetComponent<Animator>();
		anim.SetTrigger("Play"); // Play helicopter animation at start

		// get helicopter sound, make it loop
		//  heliSource = gameObject.AddComponent<AudioSource>();
		//  heliSource.playOnAwake = false;
		//  heliSource.rolloffMode = AudioRolloffMode.Linear;
		//  heliSource.clip = heliClip;
		//  heliSource.loop = true;
		//  heliSource.Play();

		// get explode sound to play when game over
		//  explodeSource = gameObject.AddComponent<AudioSource>();
		//  explodeSource.playOnAwake = false;
		//  explodeSource.rolloffMode = AudioRolloffMode.Linear;
		//  explodeSource.clip = explodeClip;

	}
	
	// Update is called once per frame
	void Update () {
		// when game starts, 
		// we make initgame variable true, and this part starts the game methods
		// then we make initgame false again so that it runs only once
		if(gameStart && initGame){
			initGame = false;
			GameScene.btnPlay.GetComponent<GUIText>().text = "";
			StartCoroutine(changeDifficulty());
			StartCoroutine(changeScore());
		}

		// we move helicopter only if game is running 
		if(!gameOver && gameStart){
			if(leftUp){
				Vector3 moveDir = new Vector3(0, 1, 0);
				copterL.transform.Translate(moveDir * speedDown * Time.deltaTime);
			}else if(leftDown){
				Vector3 moveDir = new Vector3(0, -1, 0);
				copterL.transform.Translate(moveDir * speedDown * Time.deltaTime);
			}
		}

		// if makeNewWall variable is true, we create some more walls, same method as in start()
		if(makeNewWall){
			makeNewWall = false;

			int randomWall = Random.Range(1, 3);
			float randomWallY1, randomWallY2;
			GameObject wallMiddle1, wallMiddle2;
			if(randomWall <= 1){
				randomWallY1 = Random.Range(-diffY1, diffY1);
				randomWallY2 = Random.Range(-diffY1, diffY1);
				wallMiddle1 = Instantiate(WallSingle, new Vector3(lastWall.transform.position.x + 12, randomWallY1, 0), Quaternion.identity) as GameObject;
				wallMiddle2 = Instantiate(WallSingle, new Vector3(wallMiddle1.transform.position.x + 12, randomWallY2, 0), Quaternion.identity) as GameObject;
			}else{
				randomWallY1 = Random.Range(-diffY2, diffY2);
				randomWallY2 = Random.Range(-diffY2, diffY2);
				wallMiddle1 = Instantiate(WallDouble, new Vector3(lastWall.transform.position.x + 12, randomWallY1, 0), Quaternion.identity) as GameObject;
				wallMiddle2 = Instantiate(WallDouble, new Vector3(wallMiddle1.transform.position.x + 12, randomWallY2, 0), Quaternion.identity) as GameObject;
			}
			lastWall = wallMiddle2;

		}

		// If gameOver is true, we stop game, sounds, show game over text, etc
		if(gameOver && Time.timeScale == 1){
			//  heliSource.Stop();
			//  explodeSource.Play();
			Time.timeScale = 0;
			btnPlay.GetComponent<GUIText>().text = "Game Over!";
			if(scoreNum > bestNum){
				bestNum = scoreNum;
				bestText.GetComponent<GUIText>().text = "Recorde: " + bestNum;
				PlayerPrefs.SetInt("best", bestNum);
				anim.SetTrigger("Stop");
			}
		}

	}

	// Changes score of game every 1 seconds until game is running
	IEnumerator changeScore(){
		while(gameStart && !gameOver){
			yield return new WaitForSeconds(1);
			scoreNum += 1;
			scoreText.GetComponent<GUIText>().text = "Pontos: " + scoreNum;
		} 
	}

	// Changes game difficulty every few seconds, 4 secs here.
	IEnumerator changeDifficulty(){
		while(difficultyNum <= 10 && !gameOver){
			yield return new WaitForSeconds(4);
			difficultyNum += 1;
//			diffY1 += 0.1f;
			diffY2 += 0.1f;
			WallMovement.diffY += 0.1f;
			speedWall += 0.05f;
		}
	}



}






