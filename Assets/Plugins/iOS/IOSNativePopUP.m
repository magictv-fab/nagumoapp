#import <Foundation/Foundation.h>
 
 void showNativeAlert( const char* title, const char* message)
 {
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithUTF8String: title]
     message:[NSString stringWithUTF8String: message]
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     
    [alert release];
     alert.tag = 3491832;
}
// void alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
// {
    // if (alertView.tag == 3491832)
    // {
     	// UnitySendMessage("OSNativePopup", "buttonBackCall", "AEEEE");       
    // }
// }