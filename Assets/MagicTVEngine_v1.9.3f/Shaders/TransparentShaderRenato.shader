Shader "Renato/Transparent Chroma" {
Properties {
                _MainTex ("Base (RGB)", 2D) = "white" {}
                _Color ("Mask Color", Color)  = (1.0, 0.0, 0.0, 1.0)
                _Sens ("Sens", Float) = 0.5
                _Smooth ("Smoothing", Float) = 0.5
                _Cutoff ("Recorte", Float) = 0.5
                _SensChroma ("Sensibilidade do Croma", Float) = .1285714
				_ColorChroma ("Chroma", Color) = (0, 1.0, 0)
				
        }
        SubShader {
                Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
                LOD 100
                ZTest Always Cull Back ZWrite On Lighting Off Fog { Mode off }
                CGPROGRAM
                #pragma surface surf Lambert alpha
 
				struct Input {
                    float2 uv_MainTex;
                };
 
                sampler2D _MainTex;
                float4 _Color;
                float _Sens;
				float _Smooth;
				
				float _SensChroma;
				half3 _ColorChroma;
 				float _Cutoff;
 				
                void surf (Input IN, inout SurfaceOutput o) {
                		
                        half4 c = tex2D (_MainTex, IN.uv_MainTex);
 
                        float maskY = 0.2989 * _Color.r + 0.5866 * _Color.g + 0.1145 * _Color.b;
						float maskCr = 0.7132 * (_Color.r - maskY);
						float maskCb = 0.5647 * (_Color.b - maskY);
						
						float Y = 0.2989 * c.r + 0.5866 * c.g + 0.1145 * c.b;
						float Cr = 0.7132 * (c.r - Y);
						float Cb = 0.5647 * (c.b - Y);
						
						
						
						
							
						                        float aR = abs(c.r - _ColorChroma.r) < _SensChroma ? abs(c.r - _ColorChroma.r) : 1;
						                        float aG = abs(c.g - _ColorChroma.g) < _SensChroma ? abs(c.g - _ColorChroma.g) : 1;
						                        float aB = abs(c.b - _ColorChroma.b) < _SensChroma ? abs(c.b - _ColorChroma.b) : 1;                        
						                        
						                        float a = (aR + aG + aB) / 3; 
                        
                        
						
						float blendValue = smoothstep(_Sens, _Sens + _Smooth, distance(float2(Cr, Cb), float2(maskCr, maskCb)));
						
						if(a < _Cutoff){
                        						o.Alpha = 0;
//                        	o.Alpha = 1.0 * blendValue;
                        }else{
                        	o.Alpha = 1.0 * blendValue;
                        }
						o.Emission = c.rgb * blendValue;
                }
                ENDCG
        }
        FallBack "Diffuse"	
}