// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:3138,x:34017,y:32439,varname:node_3138,prsc:2|emission-1156-RGB,clip-5367-OUT;n:type:ShaderForge.SFN_Tex2d,id:1156,x:32680,y:32528,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:_node_1156,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:10e1453c231887544824dc466ab9b71b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ComponentMask,id:9453,x:32939,y:32348,varname:node_9453,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-1156-RGB;n:type:ShaderForge.SFN_ComponentMask,id:7623,x:32926,y:32617,varname:node_7623,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1156-RGB;n:type:ShaderForge.SFN_ComponentMask,id:7850,x:32927,y:32900,varname:node_7850,prsc:2,cc1:2,cc2:-1,cc3:-1,cc4:-1|IN-1156-RGB;n:type:ShaderForge.SFN_Slider,id:2836,x:33156,y:32656,ptovrint:False,ptlb:Green,ptin:_Green,varname:node_2836,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:8478,x:33156,y:32817,ptovrint:False,ptlb:Red,ptin:_Red,varname:node_8478,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1246,x:33156,y:33029,ptovrint:False,ptlb:Blue,ptin:_Blue,varname:node_1246,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.004431567,max:1;n:type:ShaderForge.SFN_Multiply,id:5630,x:33552,y:32595,varname:node_5630,prsc:2|A-9453-OUT,B-2836-OUT;n:type:ShaderForge.SFN_Multiply,id:918,x:33567,y:32771,varname:node_918,prsc:2|A-7623-OUT,B-8478-OUT;n:type:ShaderForge.SFN_Multiply,id:333,x:33567,y:32956,varname:node_333,prsc:2|A-7850-OUT,B-1246-OUT;n:type:ShaderForge.SFN_Add,id:5367,x:33799,y:32708,varname:node_5367,prsc:2|A-5630-OUT,B-918-OUT,C-333-OUT;proporder:1156-2836-8478-1246;pass:END;sub:END;*/

Shader "Shader Forge/Chroma_SF" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _Green ("Green", Range(0, 1)) = 0
        _Red ("Red", Range(0, 1)) = 0
        _Blue ("Blue", Range(0, 1)) = 0.004431567
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Green;
            uniform float _Red;
            uniform float _Blue;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                clip(((_Texture_var.rgb.g*_Green)+(_Texture_var.rgb.r*_Red)+(_Texture_var.rgb.b*_Blue)) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = _Texture_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Green;
            uniform float _Red;
            uniform float _Blue;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                clip(((_Texture_var.rgb.g*_Green)+(_Texture_var.rgb.r*_Red)+(_Texture_var.rgb.b*_Blue)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
