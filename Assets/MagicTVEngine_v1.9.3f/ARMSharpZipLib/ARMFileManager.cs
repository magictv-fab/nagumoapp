﻿using UnityEngine;
using System.Collections;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using System;

namespace ARM.utils.io {
	public partial class ARMFileManager : MonoBehaviour {

		/// <summary>
		/// Uncompresses the file and folders
		/// </summary>
		/// <param name="file">File.</param>
		/// <param name="pathToUnzip">Path to unzip.</param>
		public static bool UncompressFile (string zipFilePath, string pathToUnzip ) {
			bool success = false ;
			//cria a pasta caso já não esteja criada
			ARMFileManager.CreateFolderRecursivelyForPath (pathToUnzip);
			using (ZipInputStream s = new ZipInputStream(File.OpenRead( zipFilePath ))) {
				
				ZipEntry theEntry;
				while ((theEntry = s.GetNextEntry()) != null) {
					
					// //Debug.LogWarning (theEntry.Name);
					
					string directoryName = Path.GetDirectoryName ( theEntry.Name ) ;
					string fileName = Path.GetFileName ( theEntry.Name ) ;
					
					// create directory
					if (directoryName.Length > 0) {

						Directory.CreateDirectory (pathToUnzip + directoryName);

					}
					
					if (fileName != String.Empty) {
						using ( FileStream streamWriter = File.Create( pathToUnzip + theEntry.Name)) {
							int size = 2048;
							byte[] data = new byte[2048];
							while (true) {
								size = s.Read (data, 0, data.Length);
								if (! ( size > 0 ) ) {
									break;
								}
								streamWriter.Write (data, 0, size);
							}
							success = true ;
						}
					}
				}
			}
			return success ;
		}
		/// <summary>
		/// Uncompresses the file and delete de zip file after finished
		/// </summary>
		/// <param name="zipFilePath">Zip file path.</param>
		/// <param name="pathToUnzip">Path to unzip.</param>
		/// <param name="deleteZipAfterFinished">If set to <c>true</c> delete zip after finished.</param>
		public static bool UncompressFile (string zipFilePath, string pathToUnzip , bool deleteZipAfterFinished ) {

			return UncompressFile ( zipFilePath, pathToUnzip ) ;
			if (deleteZipAfterFinished) {
				//deletando arquivo
				ARMFileManager.DeleteFile (zipFilePath);
			}
		}
	}
}