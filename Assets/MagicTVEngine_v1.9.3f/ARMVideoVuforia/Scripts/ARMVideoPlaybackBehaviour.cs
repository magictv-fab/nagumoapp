/*==============================================================================
/*============================================================================== 
 * Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved. 
 * Confidential and Proprietary – Qualcomm Connected Experiences, Inc. 
 * ==============================================================================*/

using UnityEngine;
using ARM.animation;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
/// <summary>
/// The VideoPlaybackBehaviour manages the appearance of a video that can be superimposed on a target.
/// Playback controls are shown on top of it to control the video. 
/// </summary>
using ARM.utils.io;



public class ARMVideoPlaybackBehaviour : ViewTimeControllAbstract
{

	public GameObject buttonGame;


	#region PUBLIC_MEMBER_VARIABLES
	public string m_path = null;
	public bool loop = false;
	public VideoPlayerHelper.MediaState ARMCurrentState = VideoPlayerHelper.MediaState.NOT_READY;
    #endregion // PUBLIC_MEMBER_VARIABLES

    #region PRIVATE_MEMBER_VARIABLES
	private VideoPlayerHelper mVideoPlayer = null;
	private bool mIsInternalComponentInited = false;
	private bool mIsPrepared = false;
	private Texture2D mVideoTexture = null;
	public bool firstPlay = false;
	[SerializeField]
	[HideInInspector]
	private Texture
		mKeyframeTexture = null;
	private VideoPlayerHelper.MediaType mMediaType = VideoPlayerHelper.MediaType.ON_TEXTURE;
	private VideoPlayerHelper.MediaState mCurrentState = VideoPlayerHelper.MediaState.NOT_READY;
	private float mSeekPosition = 0.0f;
	private float mPlayPosition = 0.0f;
	private bool isPlayableOnTexture;
	private int debug_status;

	protected bool initCalled = false;

    #endregion // PRIVATE_MEMBER_VARIABLES



	public delegate void OnVoidEvent ();
	OnVoidEvent onYouCanShowMe;

	public void AddYouCanShowMe (OnVoidEvent eventHandler)
	{
		onYouCanShowMe += eventHandler;
	}

	protected void RaiseYouCanShowMe ()
	{
		if (onYouCanShowMe != null) {
			onYouCanShowMe ();
		}
	}

    #region PROPERTIES

	/// <summary>
	/// Returns the video player
	/// </summary>
	public VideoPlayerHelper VideoPlayer {
		get { return mVideoPlayer; }
	}

	/// <summary>
	/// Returns the current playback state
	/// </summary>
	public VideoPlayerHelper.MediaState CurrentState {
		get { return mCurrentState; }
	}

	/// <summary>
	/// Type of playback (on-texture only, fullscreen only, or both)
	/// </summary>
	public VideoPlayerHelper.MediaType MediaType {
		get { return mMediaType; }
		set { mMediaType = value; }
	}

	/// <summary>
	/// Texture displayed before video playback begins
	/// </summary>
	public Texture KeyframeTexture {
		get { return mKeyframeTexture; }
		set { mKeyframeTexture = value; }
	}

    #endregion // PROPERTIES


    #region UNITY_MONOBEHAVIOUR_METHODS

	public override void Start ()
	{
		base.Start ();
		this.onFinished += Stop;
		// Init ();
	}

	public void Init ()
	{
		//				this.onFinished += Stop`
		//		base.Hide();
		Debug.Log ("ARMVideoPlaybackBehaviour Init");

		mCurrentState = VideoPlayerHelper.MediaState.NOT_READY;

		// A filename or url must be set in the inspector
		if (m_path == null || m_path.Length == 0) {
			Debug.Log ("Please set a video url in the Inspector");
			mCurrentState = VideoPlayerHelper.MediaState.ERROR;
		}

		HandleStateChange (VideoPlayerHelper.MediaState.NOT_READY);
		mCurrentState = VideoPlayerHelper.MediaState.NOT_READY;
		// Create the video player and set the filename
		mVideoPlayer = new VideoPlayerHelper ();
		mVideoPlayer.SetFilename (m_path);

		// Initialize the video texture
		InitVideoTexture ();

		// Flip the plane as the video texture is mirrored on the horizontal
		transform.localScale = new Vector3 (-1 * Mathf.Abs (transform.localScale.x), transform.localScale.y, transform.localScale.z);
		ARMCurrentState = mCurrentState;

		//  IniciaVideo();

		//  PreparaVideo ();
		initCalled = true;
	}

	// Lock onFinished event to be sent only once per play
	bool _onFinishedEventTrigger;

	void OnRenderObject ()
	{
		if (!initCalled) {
			return;
		}
		IniciaVideo ();

		PreparaVideo ();

		if (mIsInternalComponentInited && mIsPrepared) {
			if (!_isReady) {
				Debug.Log (" ARMVideoPlaybackBehaviour  ~~~~ OnRenderObject RaiseOnReady");
				RaiseOnReady ();
			}
			mCurrentState = (isPlayableOnTexture) ? mVideoPlayer.UpdateVideoData () : mVideoPlayer.GetStatus ();
			if (mCurrentState == VideoPlayerHelper.MediaState.REACHED_END) {
				if (!loop) {
					if (_onFinishedEventTrigger) {
						this.onFinished ();
						_onFinishedEventTrigger = false;
					}
					return;
				}
				Loop ();
			}
		}
	}

	void OnDestroy ()
	{
		// Deinit the video
		if (mVideoPlayer == null) {
			return;
		}
		mVideoPlayer.Deinit ();
	}

    #endregion // UNITY_MONOBEHAVIOUR_METHODS


    #region PRIVATE_METHODS

	private void IniciaVideo ()
	{
		if (mIsInternalComponentInited)
			return;

		// Initialize the video player
		if (mVideoPlayer.Init () == false) {
			Debug.Log ("Could not initialize video player");
			return;
		}

		// Load the video
		if (mVideoPlayer.Load (m_path, mMediaType, false, 0) == false) {
			Debug.Log ("Could not load video '" + m_path + "' for media type " + mMediaType);
			return;
		}

		//				if (!ARMFileManager.Exitst (m_path)) {
		//						Debug.Log ("Could not load video '" + m_path + "' FILE DOES NOT EXISTS ");
		//						return;
		//				}


		mIsInternalComponentInited = true;
	}


	private void PreparaVideo ()
	{
		if (mIsPrepared)
			return;

		// Get the video player status
		VideoPlayerHelper.MediaState state = mVideoPlayer.GetStatus ();
		Debug.Log (string.Format (" ~~~~~ PreparaVideo    state:{0}  ", state));
		if (state == VideoPlayerHelper.MediaState.ERROR) {
			Debug.Log ("Could not load video '" + m_path + "' for media type " + mMediaType);
			HandleStateChange (VideoPlayerHelper.MediaState.ERROR);
			this.enabled = false;
		} else if (state < VideoPlayerHelper.MediaState.NOT_READY) {
			// Video player is ready
			// Can we play this video on a texture?
			isPlayableOnTexture = mVideoPlayer.IsPlayableOnTexture ();
			if (isPlayableOnTexture) {
				// Pass the video texture id to the video player
				int nativeTextureID = mVideoTexture.GetNativeTextureID ();
				mVideoPlayer.SetVideoTextureID (nativeTextureID);

				// Seek ahead if necessary
				if (mSeekPosition > 0) {
					mVideoPlayer.SeekTo (mSeekPosition);
				}
			}

			// Video is prepared, ready for playback
			mIsPrepared = true;
		}
	}

	// Initialize the video texture
	private void InitVideoTexture ()
	{
		// Create texture of size 0 that will be updated in the plugin (we allocate buffers in native code)
		mVideoTexture = new Texture2D (0, 0, TextureFormat.RGB565, false);
		mVideoTexture.filterMode = FilterMode.Bilinear;
		mVideoTexture.wrapMode = TextureWrapMode.Clamp;
		GetComponent<Renderer> ().material.mainTexture = mVideoTexture;
		GetComponent<Renderer> ().material.mainTextureScale = new Vector2 (1f, 1f);
	}

	private void Loop ()
	{
		mSeekPosition = 0.1f;
		mVideoPlayer.Play (false, 0.1f);
	}

    #endregion // PRIVATE_METHODS


    #region PUBLIC_METHODS

	public override void Play ()
	{

		Debug.Log("##########");

		Debug.Log ("~~~~Play " + mPlayPosition);
		mPlayPosition = mVideoPlayer.GetCurrentPosition ();
		ARMCurrentState = VideoPlayerHelper.MediaState.PLAYING;

		mVideoPlayer.Play (false, mVideoPlayer.GetCurrentPosition ());

		Debug.LogError ("Play " + mPlayPosition);
		if (mPlayPosition < 0.2f) {
			Debug.LogError ("mPlayPosition < " + mPlayPosition);
			InvokeRepeating ("DelayParaOPrimeiroPlay", 0.1f, 0.3f);
			return;
		}
        
		BasePlay ();
	}

	public void DelayParaOPrimeiroPlay ()
	{
		float current = mVideoPlayer.GetCurrentPosition ();
		float comparador = mPlayPosition + 0.2f;
		Debug.LogError ("DelayParaOPrimeiroPlay " + mPlayPosition);
		if (comparador <= current) {
			Debug.LogError ("comparador " + comparador + " / " + current);
			BasePlay ();
		}
	}

	private void BasePlay ()
	{
		Debug.Log (" ! ! ARMVideoPlaybackBehaviour 	BasePlay");
		_onFinishedEventTrigger = true;
		RaiseYouCanShowMe ();
		base.Play ();
		CancelInvoke ();
	}

	public override void Stop ()
	{
        
		//		base.Hide();
		ARMCurrentState = VideoPlayerHelper.MediaState.STOPPED;
		base.Stop ();

		if (!mIsInternalComponentInited)
			return;

		mSeekPosition = 0.1f;
		mVideoPlayer.SeekTo (mSeekPosition);

		VideoPause ();
	}

	public override void Pause ()
	{

		//		base.Hide();
		ARMCurrentState = VideoPlayerHelper.MediaState.PAUSED;
		base.Pause ();

		if (!mIsInternalComponentInited)
			return;

		VideoPause ();
	}

	public void VideoPause ()
	{

		// Handle pause event natively
		mVideoPlayer.Pause ();

		// Store the playback position for later
		mSeekPosition = mVideoPlayer.GetCurrentPosition ();
	}


	// Handle video playback state changes
	private void HandleStateChange (VideoPlayerHelper.MediaState newState)
	{
		// If the movie is playing or paused render the video texture
		// Otherwise render the keyframe
		if (newState == VideoPlayerHelper.MediaState.PLAYING ||
			newState == VideoPlayerHelper.MediaState.PAUSED) {
			Material mat = GetComponent<Renderer> ().material;
			mat.mainTexture = mVideoTexture;
			mat.mainTextureScale = new Vector2 (1, 1);
		} else {
			if (mKeyframeTexture != null) {
				Material mat = GetComponent<Renderer> ().material;
				mat.mainTexture = mKeyframeTexture;
				mat.mainTextureScale = new Vector2 (1, -1);
			}
		}

		// Display the appropriate icon, or disable if not needed
		switch (newState) {
		case VideoPlayerHelper.MediaState.READY:
		case VideoPlayerHelper.MediaState.REACHED_END:
		case VideoPlayerHelper.MediaState.PAUSED:
		case VideoPlayerHelper.MediaState.STOPPED:
                //  mIconPlane.GetComponent<Renderer>().material.mainTexture = m_playTexture;
                //  mIconPlaneActive = true;
			break;

		case VideoPlayerHelper.MediaState.NOT_READY:
		case VideoPlayerHelper.MediaState.PLAYING_FULLSCREEN:
                //  mIconPlane.GetComponent<Renderer>().material.mainTexture = m_busyTexture;
                //  mIconPlaneActive = true;
			break;

		case VideoPlayerHelper.MediaState.ERROR:
                //  mIconPlane.GetComponent<Renderer>().material.mainTexture = m_errorTexture;
                //  mIconPlaneActive = true;
			break;

		default:
                //  mIconPlaneActive = false;
			break;
		}

		if (newState == VideoPlayerHelper.MediaState.PLAYING_FULLSCREEN) {
			// Switching to full screen, disable QCARBehaviour (only applicable for iOS)
			VuforiaBehaviour qcarBehaviour = (VuforiaBehaviour)FindObjectOfType (typeof(VuforiaBehaviour));
			qcarBehaviour.enabled = false;
		} else if (mCurrentState == VideoPlayerHelper.MediaState.PLAYING_FULLSCREEN) {
			// Switching away from full screen, enable QCARBehaviour (only applicable for iOS)
			//  QCARBehaviour qcarBehaviour = (QCARBehaviour) FindObjectOfType(typeof(QCARBehaviour));
			//  qcarBehaviour.enabled = true;

			//  StartCoroutine ( ResetToPortraitSmoothly() );
		}
	}

    #endregion // PUBLIC_METHODS
}
