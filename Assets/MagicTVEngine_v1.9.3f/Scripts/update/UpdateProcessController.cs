﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts ;

using MagicTV.abstracts.comm ;

using ARM.abstracts.internet ;

using MagicTV.abstracts.device ;

using ARM.components ;

using ARM.abstracts ;

using MagicTV.vo ;

using MagicTV.vo.result ;

using MagicTV.globals ;
using MagicTV.globals.events ;
using MagicTV.vo.request;
using ARM.abstracts.components;
using MagicTV.abstracts.screens;


namespace MagicTV.update
{
	/// <summary>
	/// O processo de update encapsulado nessa classe e subclasses
	/// </summary>
	public class UpdateProcessController : ProgressEventsAbstract
	{

		/** <CONFIG> **/

		public float progressCacheWeight = 0.9f ;

		public float progressUpdateWeight = 0.05f ;

		public float progressDeviceInfoWeight = 0.05f ;

		/** </CONFIG> **/

		/** <COMPONENTES> **/

		/// <summary>
		/// Para centralizar comunicação com servidor
		/// </summary>
		public ServerCommunicationAbstract serverCommunication ;

		public DeviceFileInfoAbstract deviceFileInfo ;

		public DeviceInfoAbstract deviceInfo ;

		public DeviceCacheFileManagerAbstract cacheManager ;

		public LoadScreenAbstract loadScreen ;

		public InternetInfoAbstract internetInfo ;

		/** </COMPONENTES> **/

		private GroupComponentsManager groupComponent ;

		private bool _initing = false ;

		private ResultRevisionVO _resultInfoToSave ;

		protected bool _serverCommRunning = false ;
				
				
		public override void prepare ()
		{

			if (this._initing) {
				return;
			}




			//Debug.LogWarning ("U UpdateProgressController . init ");
			this._initing = true;
			//Iniciando os componentes

			this._currentProgress = 0f;

			this.groupComponent = new GroupComponentsManager ();
			
			GeneralComponentsAbstract[] components = new GeneralComponentsAbstract[ 6 ] {
				serverCommunication,
				deviceFileInfo,
				cacheManager,
				deviceInfo,
				loadScreen,
				internetInfo
			};
			
			this.groupComponent.SetComponents (components);

			this.cacheManager.weight = this.progressCacheWeight;

			if (! this.groupComponent.isComponentIsReady ()) {
				//Debug.LogWarning ("U UpdateProgressController . init ~ the group is not ready ");
				this.groupComponent.AddCompoentIsReadyEventHandler (this.prepareInitialData);
				this.groupComponent.prepare ();
				return;
			}
						
			//Debug.LogWarning ("U UpdateProgressController . init ~ the group IS READY ! GO GO GO! ");
			this.prepareInitialData ();
		}

		void hideProgress ()
		{
//			Debug.LogError (" UpdateProgress . hideProgress  ");
			//this.loadScreen.hide ();
		}

		void showProgress (float progress)
		{

			////Debug.LogWarning (" UpdateProgress . showProgress ~ progress " + progress);
			this.loadScreen.setPercentLoaded (progress);
		}

		protected void prepareInitialData ()
		{
			if (this.loadScreen != null) {
				//se tiver tela de screen, avisa ela que é com ela mesmo a relacão de mostrar o percent load
				this.AddProgressEventhandler (showProgress);
				this.AddCompleteEventhandler (hideProgress);
				
			}
			InternetInfoAbstract.isWifi = false;	
			//tem o que ser baixado
			if (this.internetInfo.hasWifi ()) {
				//iniciando sem o botao de cancelar, pois tem wifi
				InternetInfoAbstract.isWifi = true ;
			}
			//Debug.LogWarning ("U UpdateProgressController . prepareInitialData ");

			//procura a revisão atual
			ResultRevisionVO currentRevision = this.deviceFileInfo.getCurrentRevision ();

			//Debug.LogError ("U UpdateProgressController . prepareInitialData:: " + currentRevision);
			//seta a revisão atual no contexto global
			AppRoot.revision = (currentRevision != null) ? currentRevision.revision : ServerRequestParamsUpdateConstants.REVISION_NOREV;


			AppRoot.user_agent = this.deviceInfo.GetUserAgent ();

			this.RaiseComponentIsReady ();

		}

		public override void Play ()
		{
						
			if (! this.isComponentIsReady ()) {
				this.AddCompoentIsReadyEventHandler (Play);
				this.prepare ();
				return;
			}

			if (this._serverCommRunning) {
				return;
			}
			this.RemoveCompoentIsReadyEventHandler (Play);
						
			this._serverCommRunning = true;

			//assincrono
//						if (!DeviceInfo.Instance.hasNotificationToken ()) {
			//se não tem ainda notification token, precisa iniciar o processo de atualização
			DeviceEvents.GetInstance ()._onSendTokenToServer += sendTokenToServer;
            Debug.Log("DESCOMENTAR E COLOCAR O NAMESPACE NOTIFICATION Q EU TIREI PRA BOTAR O PLUGIN DE NOTIFICATION");
//			MagicTVNotificationManager.Instance.Play ();
//						}
			//iniciando processo

			this.RaiseProgress (this._currentProgress);
			//inicia o processo conforme desenhado e depois de pronto chamar o metodo abaixo
			this.serverCommunication.AddUpdateRequestCompleteEventHandler (UpdateRequestEventHandler);
			this.serverCommunication.AddUpdateRequestErrorEventHandler (UpdateErrorEventHandler);

			this.serverCommunication.RequestUpdate ();


		}
		private string _tokenToSave ;
		/// <summary>
		/// Sends the token to server.
		/// To use push notification ios and android
		/// Ver url to send
		/// </summary>
		/// <param name="value">Value.</param>
		void sendTokenToServer (string value)
		{
			this._tokenToSave = value;
			if (!this.serverCommunication.isComponentIsReady ()) {
				this.serverCommunication.AddCompoentIsReadyEventHandler (doSendTokenToServer);			
				return;
			}
			doSendTokenToServer ();
		}

		void doSendTokenToServer ()
		{
			this.serverCommunication.onTokenRequestComplete += tokenSaved;
			this.serverCommunication.sendToken (this._tokenToSave);

		}

		/// <summary>
		/// Realmente foi salvo o token
		/// </summary>
		/// <param name="success">If set to <c>true</c> success.</param>
		void tokenSaved (bool success)
		{
			if (success) {
				DeviceEvents.GetInstance ().RaiseTokenSavedIntoServer (this._tokenToSave);
			}
		}

		/** CONEXÃO ROLOU **/
		protected void UpdateRequestEventHandler (RevisionInfoVO resultInfo)
		{
						
			if (resultInfo == null) {
				Debug.LogWarning ("ResultInfo is null");
				this.EndProcess ();
				return;
			}
			//pegar as infos do servidor 5% do processo
			this._currentProgress = this.progressUpdateWeight;
			this.RaiseProgress (this._currentProgress);

			this.serverCommunication.RemoveUpdateRequestCompleteEventHandler ( UpdateRequestEventHandler ) ;

			this._serverCommRunning = false;
			//Debug.LogWarning ("[UPC] UpdateProcessController . UpdateRequestEventHandler . " + resultInfo.result.bundles);
			//chama o inicio do processo interno no mobile

			this.StartMobileProcess (resultInfo);

		}
		public bool deleteImmediately ;
		protected void StartMobileProcess (RevisionInfoVO resultInfo)
		{


			if (resultInfo == null) {
				this.EndProcess ();
				return;
			}
			if (resultInfo.result == null) {
				this.EndProcess ();
				return;
			}
			//pin recebido, propagando a informação
			//this.commitPin (resultInfo.result.pin);

			//Debug.LogError ("#### UpdateProcess . commit ");
			ReturnDataVO resultSave = this.deviceFileInfo.commit (resultInfo.result);
			
			if (! resultSave.success) {
				//revisão não foi salva, 
				AppRootEvents.LogError ("U  UpdateProcessController.UpdateRequestEventHandler", "Erro ao salvar a revisão:" + resultSave.message);
				
			}
			if ( this.deleteImmediately) {
				this.deleteFiles ();
			}
			//TODO: TESTAR VERSAO PROD X DEV

			this._resultInfoToSave = this.deviceFileInfo.getUncachedRevision (); // PROD: pegar so o necessário


//			this._resultInfoToSave = resultInfo.result ; // DEV: force de downloade de tudo

			if (this._resultInfoToSave == null || this._resultInfoToSave.bundles.Length == 0) {
				//nao tem novidades
				this.EndProcess ();
				return;
			}

			//tem itens a serem baixados - 10% do processo todo
			this._currentProgress = this.progressUpdateWeight + this.progressDeviceInfoWeight;
			this.RaiseProgress (this._currentProgress);

			this.StartDownloadProcess ();
		}
		/// <summary>
		/// Commits the pin.
		/// Se já tiver no player prefs, pega o valor salvo
		/// </summary>
		/// <param name="resultInfo">Result info.</param>
		void commitPin (string pin)
		{
			if (DeviceInfo.Instance != null) {
				string pinSaved = DeviceInfo.Instance.getPin ();
				if (pinSaved == null || pinSaved == "") {
					//salva se já não tiver salvo
					DeviceInfo.Instance.setPin (pin);
				}
								
			}
		}

		/// <summary>
		/// Deletes the files local and info DB.
		/// </summary>
		void deleteFiles ()
		{
			//busca arquivos a serem deletados
			UrlInfoVO[] urlToDelete = this.deviceFileInfo.getUrlInfoVOToDelete ();
			if (urlToDelete != null && urlToDelete.Length > 0) {
				//se tem arquivos deletados
				foreach (UrlInfoVO u in urlToDelete) {
					//pede para deletar o arquivo físico local
					if (this.cacheManager.Delete (u).success) {
						//se conseguir deletar o arquivo físico, deleta do banco
						this.deviceFileInfo.delete (u);
						
					}
				}
			}
		}

		protected void StartDownloadProcess ()
		{
			ARMDebug.LogTime ("UpdateProcessController StartDownloadProcess  ");
            //			InternetInfoAbstract.isWifi = this.internetInfo.hasWifi ();	
            //			//tem o que ser baixado
            //			if (InternetInfoAbstract.isWifi) {
            //				//iniciando sem o botao de cancelar, pois tem wifi
            //				if (this.loadScreen != null) {
            ////					this.loadScreen.ChangeToPerspective (MagicTV.globals.Perspective.PROGRESS_BAR_WITHOUT_CANCEL);
            //					this.loadScreen.show ();
            //										
            //				}
            //				doStartCacheProcess ();
            //				return;
            //			}
            //não tem wifi
            //			if (this.loadScreen != null) {
            //				//mas tem a tela do user
            ////				this.loadScreen.ChangeToPerspective (MagicTV.globals.Perspective.ASK_TO_DOWNLOAD);
            //				this.loadScreen.AddLoadConfirmedEventHandler (doStartCacheProcess);
            //				this.loadScreen.show ();
            //				return;
            //			}
            this.loadScreen.show();
            //se chegou aqui é pq não tem a tela do user pra perguntar nada, então... 
            doStartCacheProcess ();

		}
		/// <summary>
		/// realmente inicia o processo de download da internet
		/// </summary>
		void doStartCacheProcess ()
		{
			ARMDebug.LogTime ("UpdateProcessController doStartCacheProcess  ");
			this.cacheManager.AddCompleteEventhandler (cacheCompleteEventHandler);
			this.cacheManager.AddProgressEventhandler (cacheProgressEventHandler);
			
			this.cacheManager.SetFileList (this._resultInfoToSave);
			
			this.cacheManager.StartCache ();
		}

		protected void cacheCompleteEventHandler ()
		{
			Debug.LogWarning ("U UpdateProgressController . cacheCompleteEventHandler ");
			this.deviceFileInfo.Save ();
			//terminou o cache, dispara o evento
			this.RaiseCompleteAndTotalProgress ();
			this.EndProcess ();
		}

		/// <summary>
		/// Downloads the progress event handler.
		/// </summary>
		/// <param name="subTotal">Sub total.</param>
		protected void cacheProgressEventHandler (float subTotal)
		{
			//Debug.LogWarning ("[U] UpdateProgress . cacheProgressEventHandler . subTotal:" + subTotal);
			this._currentProgress = this.progressUpdateWeight + this.progressDeviceInfoWeight + this.cacheManager.GetCurrentProgressWithWeight ();
			this.RaiseProgress (this._currentProgress);

		}

		/** ERRO NA CONEXÃO **/
		protected void UpdateErrorEventHandler (string errorMessage)
		{

//			AppRootEvents.LogError( "U UpdateProcess . UpdateErrorEventHandler " , errorMessage ) ;

			this.serverCommunication.RemoveUpdateRequestErrorEventHandler (UpdateErrorEventHandler);

			this._serverCommRunning = false;

			this.EndProcess ();

		}
		protected void EndProcess ()
		{

			AppRootEvents.GetInstance ().GetDevice ().RaiseDoPinRecived (DeviceInfo.Instance.getPin ());
			DeviceInfo.Instance.SaveCurrentInfoVersionControlId ();
			this.deviceFileInfo.Save ();
			this._currentProgress = 1f;
			this.RaiseProgress (this._currentProgress);

			this.RaiseComplete ();

		}
	}
}