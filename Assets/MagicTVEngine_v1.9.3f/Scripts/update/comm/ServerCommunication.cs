using UnityEngine;
using System.Collections;
using MagicTV.abstracts.comm;

using MagicTV.vo;

using MagicTV.globals;

using MagicTV.vo.request;
using MagicTV.globals.events;

/***
{
	"jsonrpc": "2.0",
	"method": "update",
	"params": {
		"device_id": "dsaoudh0p28ey21vedweydg023gh",
		"app": "magic-tv",
		"user_agent": "MagicTV/iOS",
		"connection": "3G",
		"revision": "NOREV"
	},
	"id": 1
}



http://grafitelabs.com.br/magictvserver/src/public/api/call?jsonrpc=2.0&method=update&params%5Bdevice_id%5D=dsaoudh0p28ey21vedweydg023gh&params%5Bapp%5D=magic-tv&params%5Buser_agent%5D=MagicTV%2FiOS&params%5Bconnection%5D=WIFI&params%5Brevision%5D=NOREV&id=1989898

***/
/// <summary>
/// Server communication.
/// @author : Renato Seiji Miawaki
/// @version : 1.0
/// 
/// 
/// 
/// </summary>
using MagicTV.utils.chroma.vuforia;
using LitJson;
using ARM.abstracts.internet;
using ARM.utils.cron;
using MagicTV.download;
using MagicTV.gui;
using MagicTV.abstracts.device;


namespace MagicTV.update.comm
{

	public class ServerCommunication : ServerCommunicationAbstract
	{
		public string urlToGetJson;

		//		public string backupurlToGetInfo = "http://www.magicapi.com.br/api/call";
		//		public string backupUrlToGetPing = "http://www.magicapi.com.br/api/call";
//				public string backupUrlToGetJson = "http://www.magicapi.com.br/admin_arm/riachuelo_image_integration/";
		//		private bool firstAtempt;

		public static ServerCommunication Instance;

		public int delayMilisecondsToDownloadImages = 0;

		//EVENTOS DE SUCCESS DE SEND METADATA
		public delegate void OnBooleanEventHandler (bool success);

		protected OnBooleanEventHandler onUpdateMetadataRequestComplete;

		public void AddUpdateMetadataEventHandler (OnBooleanEventHandler method)
		{
			this.onUpdateMetadataRequestComplete += method;
		}


		public void RemoveUpdateMetadataEventHandler (OnBooleanEventHandler method)
		{
			this.onUpdateMetadataRequestComplete -= method;
		}
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseUpdateMetadataComplete (bool success)
		{
			//se disparou o evento, é porque está pronto

			if (this.onUpdateMetadataRequestComplete != null) {
				this.onUpdateMetadataRequestComplete (success);
			}
		}

		void Start ()
		{
			Instance = this;
		}

		public override void prepare ()
		{
			this.AddUpdateRequestCompleteEventHandler (checkImageUpdate);

			this.RaiseComponentIsReady ();
		}

		public override void RequestUpdate ()
		{
			// faz a requisição no servidor
			pingServer ();

			RequestRegisterDeviceOnceTime ();
			//getServerResult ();

		}
		/// <summary>
		/// Registrando o device no servidor assincronamente
		/// Requests the register device once time.
		/// 
		/// </summary>
		void RequestRegisterDeviceOnceTime ()
		{
			int saved = PlayerPrefs.GetInt ("device_id_saved");

			if (saved > 0) {

				return;
			}
			MudaString ();
			ServerRequestUpdateVO request = new ServerRequestUpdateVO ();
			ServerRequestParamsDeviceVO parametros = new ServerRequestParamsDeviceVO ();

			parametros.device_id = AppRoot.device_id;
			parametros.app = MagicApplicationSettings.Instance.app;
			parametros.connection = AppRoot.connection;
			parametros.revision = AppRoot.revision;
			parametros.user_agent = AppRoot.user_agent;
			//TODO: o valor do token na verdade era pra ser para o push
			parametros.token = AppRoot.device_id;

			request.param = parametros;

			request.method = "device";

			string requestString = "?method=" + request.method + "&jsonrpc=" + request.jsonrpc + "&id=" + request.id + "&" + parametros.ToURLString ();


			requestString = MagicApplicationSettings.Instance.urlToRegisterDevice + requestString;
			//requestString
			Debug.LogError( " @@@@@ REQUEST String = " + requestString);
			WWW www = new WWW (requestString);
			StartCoroutine (WaitForRequestDeviceRegister (www));
			//
		}

		void MudaString ()
		{
			string agent = AppRoot.user_agent;
			agent = agent.Replace ("MagicTV/", string.Empty);
			MagicApplicationSettings.Instance.urlToGetInfo = MagicApplicationSettings.Instance.urlToGetInfo.Trim().Replace ("$type", agent);
			MagicApplicationSettings.Instance.urlToGetPing = MagicApplicationSettings.Instance.urlToGetPing.Trim().Replace ("$type", agent);
			MagicApplicationSettings.Instance.urlToRegisterDevice = MagicApplicationSettings.Instance.urlToRegisterDevice.Trim();
			urlToGetJson = urlToGetJson.Trim().Replace ("$type", agent);

		}

		private IEnumerator WaitForRequestDeviceRegister (WWW www)
		{
			yield return www;

			//salvo
			this.dispatchResultDeviceJson (www);
		}
		private void dispatchResultDeviceJson (WWW www)
		{
			Debug.LogError ("Device Registrado, resposta :" + www.text);

			if (www.error == null) {
				Debug.LogError (".ping JSON DEVICE result: WWW !: " + www.text);

				try {
					ServerRequestMobile result = LitJson.JsonMapper.ToObject<ServerRequestMobile> (www.text);
					//					//pin recebido, propagando a informação
					PlayerPrefs.SetInt ("device_id_saved", result.id);
					this.commitPin (result.id.ToString ());

				} catch (JsonException ex) {
					Debug.LogError ("ERRO PARSE JSON DEVICE result: WWW !: " + www.text +" >> " + ex.Message);
				}
			}

		}
		void commitPin (string pin)
		{
			if (DeviceInfo.Instance != null) {
				string pinSaved = DeviceInfo.Instance.getPin ();
				if (pinSaved == null || pinSaved == "") {
					//salva se já não tiver salvo
					DeviceInfo.Instance.setPin (pin);
				}

			}
		}


		/// <summary>
		/// Checks the image update depois dessa mesma classe disparar que terminou de fazer o update.
		/// </summary>
		/// <param name="revisionInfoVO">Revision info V.</param>
		void checkImageUpdate (RevisionInfoVO revisionInfoVO)
		{
			this.doCheckImageUpdate ();
		}

		void doCheckImageUpdate ()
		{
			//está no wifi?
			if (InternetInfoAbstract.isWifi) {
				//terminou de baixar as informações e está no wifi
//				pingJSON ();
				return;
			}
			Debug.LogError ("Sem wifi não baixa as imagens (e nem confere se tem o que ser baixado)");
		}

		protected void RaiseUpdateJsonRequestError (string t)
		{
			//nesse caso nao faz nada em json
			Debug.LogWarning (t);
		}

		public bool requestIsFinished ;
		protected IEnumerator currentRoutine ;

		protected WWW pingServer ()
		{
			//faz o ping, ao retornar resultado, compara com a versão atual pra ver se 
			ARMDebug.LogTime ("ServerCommunication getServerResult  ");
			ServerRequestUpdateVO request = new ServerRequestUpdateVO ();

			request.param = new ServerRequestParamsUpdateVO ();

			request.param.device_id = AppRoot.device_id;
			request.param.app = MagicApplicationSettings.Instance.app;
			request.param.connection = AppRoot.connection;
			request.param.revision = AppRoot.revision;
			request.param.user_agent = AppRoot.user_agent;
			request.method = "ping";
			string requestString = "?method=" + request.method + "&jsonrpc=" + request.jsonrpc + "&id=" + request.id + "&" + request.param.ToURLString ();

			MudaString ();
			requestString = MagicApplicationSettings.Instance.urlToGetPing + requestString;
			Debug.LogError ("!!!!!! PING:" + requestString);
			//iniciando a contagem de timeout
			requestIsFinished = false;
			EasyTimer.ClearInterval (this._timerTimeout);
			this._timerTimeout = EasyTimer.SetTimeout ( checkPingTimeoutIsFinished, MagicApplicationSettings.Instance.timeoutToleranceMiliseconds ) ;
			WWW wwwPing = new WWW (requestString);

			currentRoutine = WaitForRequestPing (wwwPing);
			StartCoroutine (currentRoutine);

			return wwwPing;
		}

		void checkPingTimeoutIsFinished ()
		{
			if(!this.requestIsFinished){

				//TIMEOUT de ping

				if(currentRoutine != null){
					StopCoroutine( currentRoutine ) ;
				}
				//Abre o dialogo para perguntar se quer tentar de novo:

				MobileNativeDialog dialog = new MobileNativeDialog ("Verifique sua conexão com a internet", "Deseja tentar novamente?", "Sim", "Não");
				dialog.OnComplete += OnDialogPingClose;
			}
		}

		void OnDialogPingClose (MNDialogResult obj)
		{
			//parsing result
			switch (obj) {
			case MNDialogResult.YES:
				pingServer ();
				break;
			case MNDialogResult.NO:
				RaiseUpdateRequestError (" Timeout PING "+MagicApplicationSettings.Instance.timeoutToleranceMiliseconds);
				break;
			}
		}

		private IEnumerator WaitForRequestPing (WWW www)
		{
			yield return www;

			this.dispatchResultPing (www);
		}

		private void dispatchResultPing (WWW www)
		{
			requestIsFinished = true ;
			if (www.error == null) {
				Debug.LogWarning (".ping result: WWW !: " + www.text);
				try {
					RevisionInfoPingVO result = LitJson.JsonMapper.ToObject<RevisionInfoPingVO> (www.text);
					PingRequestComplete (result);
				} catch (JsonException ex) {
					RaiseUpdateRequestError (".ping parser error");
				}
				return;
			}

			Debug.LogError ("[ SC ] ~~~~  ServerCommunication . dispatchResult WWW PING Error: " + www.error + " > " + www.url);
			//						AppRootEvents.LogError ("ServerCommunication", "" + www.error);

			#if UNITY_ANDROID
			MobileNativeDialog dialog = new MobileNativeDialog ("Verifique sua conexão com a internet", "Deseja tentar novamente?", "Sim", "Não");
			dialog.OnComplete += OnDialogResponde;
			Debug.LogError("Verifique sua conexão com a internet");
			#endif

			RaiseUpdateRequestError (www.error);
		}

		#if UNITY_ANDROID
		void OnDialogResponde (MNDialogResult obj)
		{
			//parsing result
			switch (obj) {
			case MNDialogResult.YES:
				Application.LoadLevel (Application.loadedLevelName);
				break;
			case MNDialogResult.NO:
				RaiseUpdateRequestError (" Timeout UPDATE "+MagicApplicationSettings.Instance.timeoutToleranceMiliseconds);
				break;
			}
		}
		
		#endif

		int _timerTimeout;

		private void PingRequestComplete (RevisionInfoPingVO result)
		{

			if (result == null || result.error != null || result.result.current == null) {
				//é null, não pega
				//RaiseUpdateRequestError ("ping não recebido, não atualiza");
				//por enquanto, que não subimos o server, se não veio nada no ping, busca da maneira antiga
				getServerResult ();
				return;
			}
			//			Debug.LogWarning (".ping PingRequestComplete " + result.result.current);
			int current = DeviceInfo.Instance.GetCurrentInfoVersionControlId ();
			//			Debug.LogError (".ping PingRequestComplete LOCAL.current_version " + current);
			if (current == 0) {
				//não tinha uma versão local, pega info e grava

				DeviceInfo.Instance.SetCurrentInfoVersionControlId (result.result.current);
				//				Debug.LogWarning (".ping PingRequestComplete não tinha uma versão local, pega info para gravar ||| " + result.result.current);
				//pega
				getServerResult ();
				return;
			}
			if (result.result.current > current) {
				//				Debug.LogWarning (".ping PingRequestComplete servidor tem novidades. ATUALIZA ");
				//salva
				DeviceInfo.Instance.SetCurrentInfoVersionControlId (result.result.current);
				//pega

				getServerResult ();
				return;
			}
			//			Debug.LogError (".ping PingRequestComplete SEM NOVIDADES ");
			//faz o ping de imagens 
			doCheckImageUpdate ();
			//se chegou aqui é pq está na versão atual, entao da por concluido
			RaiseUpdateRequestError ("Nada por baixar. Não é um erro. E sim um ping concluído percebendo que está ok.");
		}



		protected WWW getServerResult ()
		{
			ARMDebug.LogTime ("ServerComunication . antes da requisicao");
			Debug.LogError ("ServerCommunication getServerResult  ");
			ServerRequestUpdateVO request = new ServerRequestUpdateVO ();

			request.param = new ServerRequestParamsUpdateVO ();

			request.param.device_id = AppRoot.device_id;
			request.param.app = MagicApplicationSettings.Instance.app;
			request.param.connection = AppRoot.connection;
			request.param.revision = AppRoot.revision;
			request.param.user_agent = AppRoot.user_agent;

			//alvo:
			//http://grafitelabs.com.br/magictvserver/src/public/api/call?jsonrpc=2.0&method=update&params%5Bdevice_id%5D=dsaoudh0p28ey21vedweydg023gh&params%5Bapp%5D=magic-tv&params%5Buser_agent%5D=MagicTV%2FiOS&params%5Bconnection%5D=WIFI&params%5Brevision%5D=NOREV&id=1989898
			//http://grafitelabs.com.br/magictvserver/src/public/api/call?method=update&jsonrpc=2.0&id=0&paramsparams%5Bdevice_id%5D=01553F15-7A76-59FF-AAAD-F80593D4D723&params%5Bapp%5D=magic-tv&params%5Buser_agent%5D=3G&params%5Bconnection%5D=3G&params%5Brevision%5D=NOREV
			//http://grafitelabs.com.br/magictvserver/src/public/api/call?method=update&jsonrpc=2.0&id=0&params%7b%22REVISION_NOREV%22%3a%22NOREV%22%2c%22UserAgent_Android%22%3a%22MagicTV%2fAndroid%22%2c%22UserAgent_iOS%22%3a%22MagicTV%2fiOS%22%2c%22ConnectionsType_3G%22%3a%223G%22%2c%22ConnectionsType_WiFI%22%3a%22WIFI%22%2c%22revision%22%3a%22NOREV%22%2c%22device_id%22%3a%22MobileIdUnico%22%2c%22app%22%3a%22magic-tv%22%2c%22user_agent%22%3anull%2c%22connection%22%3a%223G%22%7d

			string requestString = "?method=" + request.method + "&jsonrpc=" + request.jsonrpc + "&id=" + request.id + "&" + request.param.ToURLString ();

			MudaString () ;
			requestString = MagicApplicationSettings.Instance.urlToGetInfo + requestString ;
			ARMDebug.LogTime ("ServerComunication . antes da requisicao");
			Debug.LogError ("!!!!!! REQUEST DE APRESENTAÇÕES " + requestString);

			requestIsFinished = false;
			EasyTimer.ClearInterval (this._timerTimeout);
			this._timerTimeout =  EasyTimer.SetTimeout ( checkTimeoutIsFinished, MagicApplicationSettings.Instance.timeoutToleranceMiliseconds ) ;
			WWW www = new WWW ( requestString ) ;
			
			currentRoutine = WaitForRequest (www) ;
			StartCoroutine (currentRoutine);

			return www;

		}
		void checkTimeoutIsFinished ()
		{
			if(!this.requestIsFinished){
				
				//TIMEOUT de ping
				
				if( currentRoutine != null ){
					StopCoroutine( currentRoutine ) ;
				}
				//Abre o dialogo para perguntar se quer tentar de novo:
				
				MobileNativeDialog dialog = new MobileNativeDialog ("Verifique sua conexão com a internet", "Deseja tentar novamente?", "Sim", "Não");
				dialog.OnComplete += OnDialogClose;
				Debug.LogError("Verifique sua conexão com a internet");
			}
		}
		
		void OnDialogClose (MNDialogResult obj)
		{
			//parsing result
			switch (obj) {
			case MNDialogResult.YES:
				getServerResult ();
				break;
			case MNDialogResult.NO:
				RaiseUpdateRequestError (" Timeout UPDATE "+MagicApplicationSettings.Instance.timeoutToleranceMiliseconds);
				break;
			}
		}
		private IEnumerator WaitForRequest (WWW www)
		{
			ARMDebug.LogTime ("ServerCommunication WaitForRequest ANTES do  yield ");

			yield return www;

			ARMDebug.LogTime ("ServerCommunication WaitForRequest DEPOIS do yield ");

			this.dispatchResult (www);


		}

		private void dispatchResult (WWW www)
		{
			requestIsFinished = true;
			ARMDebug.LogTime ("ServerComunication . depois da requisicao");
			Debug.LogError ("(!) SERVER RESULT: WWW Ok!: " + www.text);
			if (www.error == null) {

								ARMDebug.LogTime ("ServerCommunication dispatchResult ANTES de parsear o json ");
				try {

					RevisionInfoVO result = LitJson.JsonMapper.ToObject<RevisionInfoVO> (www.text);
										ARMDebug.LogTime ("ServerCommunication dispatchResult DEPOSI de parsear o json ");
					//fake
					//RevisionInfoVO result = LitJson.JsonMapper.ToObject <RevisionInfoVO> ("{ \"jsonrpc\": \"2.0\", \"id\": \"1\", \"result\": {\"revision\": 5,\"name\": \"Santander - Revisão 1\",\"short_description\": \".\",\"long_description\": \".\",\"bundles\": [{\"id\": 8,\"context\": \"InAppVideoPresentation_SantanderCard1\",\"published_at\": null,\"expired_at\": null,\"files\": [{\"id\": 10,\"type\": \"audio.mp3\",\"urls\": [{\"id\": 21,\"type\": \"low\",\"url\": \"https://magic-tv-sao-paulo.s3.amazonaws.com/X1ncR0BsPOIRmb8hRCdN.mp3\",\"size\": 1974226}]},{\"id\": 11,\"type\": \"video.ogv\",\"urls\": [{\"id\": 23,\"type\": \"low\",\"url\": \"https://magic-tv-sao-paulo.s3.amazonaws.com/fkd6Dk22zCTD5VdDfC3B.ogv\",\"size\": 42719857}]},{\"id\": 12,\"type\": \"tracking.zip\",\"urls\": [{\"id\": 24,\"type\": \"high\",\"url\": \"https://magic-tv-sao-paulo.s3.amazonaws.com/YBMhiEQoAcTtTnIpmhfR.zip\",\"size\": 58022}]}],\"metadata\": [{\"id\": 6,\"key\": \"GoogleAnalytics\",\"value\": \"UA-56074992-1\"},{\"id\": 7,\"key\": \"transform_position\",\"value\": \"0|0|0\"},{\"id\": 9,\"key\": \"transform_rotation\",\"value\": \"-24.90253|-60.26068|36.3987\"},{\"id\": 8,\"key\": \"transform_scale\",\"value\": \"1.538023|1|1.300505\"}]}],\"metadata\": [{\"id\": 1,\"key\": \"GoogleAnalytics\",\"value\": \"UA-52603038-6\"} ] }}");
					//								RevisionInfoVO result = LitJson.JsonMapper.ToObject <Rev
					Debug.LogError ("request json parsed");
					RaiseUpdateRequestComplete (result);

				} catch (JsonException ex) {
					Debug.LogError ("json parser error no SERVER RESULT ");
					RaiseUpdateRequestError ("parser error");
				}
				return;
			}

			Debug.LogError ("[ SC ] ~~~~  ServerCommunication . dispatchResult WWW Error: " + www.error + " > " + www.url);
			//						AppRootEvents.LogError ("ServerCommunication", "" + www.error);

			RaiseUpdateRequestError (www.error);
		}



		public override WWW sendToken (string token)
		{
			ServerRequestUpdateVO request = new ServerRequestUpdateVO ();
			ServerMetadataParamsVO parametros = new ServerMetadataParamsVO ();


			parametros.device_id = AppRoot.device_id;
			parametros.app = MagicApplicationSettings.Instance.app;
			parametros.connection = AppRoot.connection;
			parametros.revision = AppRoot.revision;
			parametros.user_agent = AppRoot.user_agent;
			parametros.metas.Add (new MetadataVO () { key = "token", value = token });
			/*
						"device_id": "android-dsaoudh0p28ey21vedweydg023gh",
						"app": "magic-tv",
						"user_agent": "MagicTV/Android",
						"token": "token_gerado_pela_store"
							 * */
			request.param = parametros;

			string requestString = "?method=device&jsonrpc=" + request.jsonrpc + "&device_id=" + request.id + "&" + request.param.ToURLString ();
			MudaString ();
			requestString = MagicApplicationSettings.Instance.urlToGetInfo + requestString;
			Debug.LogError (requestString);
			WWW www = new WWW (requestString);
			StartCoroutine (WaitForRequestToken (www));

			return www;
		}

		private IEnumerator WaitForRequestToken (WWW www)
		{
			yield return www;

			this.dispatchResultToken (www);
		}

		public WWW sendTransformInfo (GameObject gm, string bundle_id)
		{
			if (gm == null || gm.transform == null) {
				return null;
			}
			ServerRequestUpdateVO request = new ServerRequestUpdateVO ();
			ServerMetadataParamsVO parametros = new ServerMetadataParamsVO ();

			parametros.device_id = AppRoot.device_id;
			parametros.app = MagicApplicationSettings.Instance.app;
			parametros.connection = AppRoot.connection;
			parametros.revision = AppRoot.revision;
			parametros.user_agent = AppRoot.user_agent;

			Vector3 pos = gm.transform.localPosition;
			parametros.metas.Add (new MetadataVO () { key = "TransformPosition", value = pos.x + "%7C" + pos.y + "%7C" + pos.z });
			Vector3 scale = gm.transform.localScale;
			parametros.metas.Add (new MetadataVO () { key = "TransformScale", value = scale.x + "%7C" + scale.y + "%7C" + scale.z });
			Vector3 rotation = gm.transform.localRotation.eulerAngles;
			parametros.metas.Add (new MetadataVO () { key = "TransformRotation", value = rotation.x + "%7C" + rotation.y + "%7C" + rotation.z });


			request.param = parametros;

			string requestString = "?method=metadata&params%5Bbundle_id%5D=" + bundle_id + "&jsonrpc=" + request.jsonrpc + "&id=" + request.id + "&" + request.param.ToURLString ();
			MudaString ();
			requestString = MagicApplicationSettings.Instance.urlToGetInfo + requestString;
			Debug.LogError (requestString);
			WWW www = new WWW (requestString);
			StartCoroutine (WaitForRequestMetas (www));

			return www;
		}
		/// <summary>
		/// Sends the vuforia chromaqui info.
		/// </summary>
		/// <returns>The vuforia chromaqui info.</returns>
		/// <param name="material_name">Material_name.</param>
		/// <param name="bundle_id">Bundle_id.</param>
		/// <param name="float1">Float1.</param>
		/// <param name="float2">Float2.</param>
		/// <param name="float3">Float3.</param>
		/// <param name="float4">Float4.</param>
		public WWW sendVuforiaChromaquiInfo (string material_name, string bundle_id, float float1, float float2, float float3, float float4)
		{
			if (material_name == null || bundle_id == null) {
				return null;
			}
			ServerRequestUpdateVO request = new ServerRequestUpdateVO ();
			ServerMetadataParamsVO parametros = new ServerMetadataParamsVO ();

			parametros.device_id = AppRoot.device_id;
			parametros.app = MagicApplicationSettings.Instance.app;
			parametros.connection = AppRoot.connection;
			parametros.revision = AppRoot.revision;
			parametros.user_agent = AppRoot.user_agent;
			//por padrao é VuforiaChromaKey
			string label1 = VuforiaChromaKeyConfig.GetLabelFloat1 ();
			string label2 = VuforiaChromaKeyConfig.GetLabelFloat2 ();


			parametros.metas.Add (new MetadataVO () { key = label1, value = float1.ToString() });

			parametros.metas.Add (new MetadataVO () { key = label2, value = float2.ToString() });

			if (float3 != null && float4 != null) {
				string label3 = VuforiaChromaKeyConfig.GetLabelFloat3 ();
				string label4 = VuforiaChromaKeyConfig.GetLabelFloat4 ();
				parametros.metas.Add (new MetadataVO () { key = label3, value = float3.ToString() });
				parametros.metas.Add (new MetadataVO () { key = label4, value = float4.ToString() });
			}
			request.param = parametros;

			string requestString = "?method=metadata&params%5Bbundle_id%5D=" + bundle_id + "&jsonrpc=" + request.jsonrpc + "&id=" + request.id + "&" + request.param.ToURLString ();
			MudaString ();
			requestString = MagicApplicationSettings.Instance.urlToGetInfo + requestString;
			Debug.LogError (requestString);
			WWW www = new WWW (requestString);
			StartCoroutine (WaitForRequestMetas (www));

			return www;
		}
		private IEnumerator WaitForRequestMetas (WWW www)
		{
			yield return www;

			this.dispatchResultMeta (www);

		}
		private void dispatchResultToken (WWW www)
		{
			if (www.error == null) {

				Debug.LogWarning (www.text);
				RaiseUpdateTokenComplete (true);
				return;
			}
			Debug.LogError (www.error);
			Debug.Log (www.error);
			RaiseUpdateTokenComplete (false);
		}
		private void dispatchResultMeta (WWW www)
		{


			if (www.error == null) {

				Debug.LogWarning (www.text);
				RaiseUpdateMetadataComplete (true);
				return;
			}
			Debug.LogError (www.error);
			Debug.Log (www.error);
			RaiseUpdateMetadataComplete (false);
		}



	}
}