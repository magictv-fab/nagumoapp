﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MagicTV.in_apps;
using MagicTV.globals.events;
using MagicTV.processes;
using MagicTV.abstracts.device;

public class DebugTracks : MonoBehaviour, OnOffListenerEventInterface {
	public static DebugTracks Instance;
	public bool active;

	public bool showMenu;
	public DeviceFileInfoAbstract fileInfo;

	// Use this for initialization
	DebugTracks () {
		Instance = this;
	}
	// Update is called once per frame
	void OnGUI () {
		if(active){
			if(fileInfo != null && fileInfo.tracks.Count > 0){
				if(this.showMenu){
					this.drawMenu();
					drawBackButton();
				} else {
					drawShowButton();
				}
			}

		}
	}
	public int currentPage = 0 ;
	public int totalButtonsPerPage = 7 ;
	void drawMenu(){

		int totalLimit = totalButtonsPerPage * (currentPage + 1);
		int initialLimit = totalButtonsPerPage * currentPage ;
		int line = 0;
		for (int i = initialLimit; i < totalLimit; i++) {
			line++;
			if(fileInfo.tracks.Count - 1 > i ){
				if( GUI.Button( new Rect( Screen.width/4, (Screen.height/10)*line+(Screen.height/10), (Screen.width/2), Screen.height/10), fileInfo.tracks[i] ) ){
					//AppRootEvents.GetInstance().GetAR ().RaiseTrackIn(fileInfo.tracks[i]);
					PresentationController.Instance.StopLastPresentation();
					PresentationController.Instance.OnTrackingFound(fileInfo.tracks[i]);
				}
			}
		}
		GUI.Button (new Rect( Screen.width/2 - (Screen.width/4)/2, (Screen.height/10), (Screen.width/4), Screen.height/10), "["+this.currentPage+"]");
		if( currentPage > 0 && GUI.Button( new Rect( 0, (Screen.height/10), (Screen.width/4), Screen.height/10), "<" ) ){
			currentPage -- ;
		}
		if( currentPage < Mathf.FloorToInt( fileInfo.tracks.Count/totalButtonsPerPage ) && GUI.Button( new Rect( Screen.width - (Screen.width/4), (Screen.height/10), (Screen.width/4), Screen.height/10), ">" ) ){
			currentPage ++ ;
		}
	}
	void drawShowButton(){
		//botao voltar
		if( GUI.Button( new Rect( 0, 0, (Screen.width/4), Screen.height/10), "MOSTRAR" ) ){
			this.showMenu = true;
		}
	}
	void drawBackButton(){
		//botao voltar
		if( GUI.Button( new Rect( 0, 0, (Screen.width/4), Screen.height/10), "FECHAR" ) ){
			this.showMenu = false;
		}
	}


	public string getUniqueName(){
		return this.gameObject.name;
	}
	public void turnOn (){
		this.active = true;
	}
	public void turnOff (){
		this.active = false;
	}
	public void ResetOnOff (){
		//se ligou não desliga mais. Feche o app, isso é pra debug
//		this.active = false;
	}
	public bool isActive(){
		return this.active;
	}
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}
}
