﻿using UnityEngine;
using System.Collections;
using ARM.animation;
using MagicTV.vo;

public class ExchangeQuality : GenericStartableComponentControllAbstract
{
    private static ExchangeQuality _instance;
    private int _quality;
    public ExchangeQuality()
    {
        _instance = this;
    }
    public static ExchangeQuality GetInstance()
    {
        return _instance;
    }

    public override void prepare()
    {
        _quality = 3;
        _quality = (Benchmark.Instance != null) ?
            (Benchmark.Instance.GetCurrentQuality() != null) ? Benchmark.Instance.GetCurrentQuality().quality : _quality
            : _quality;
        this.changeUnityQuality(_quality);
    }

    private void changeUnityQuality(int _quality)
    {
        //A qualidade do Unity varia de 1 a 5, sendo 5 a melhor de todas
        if (_quality == UrlInfoConstants.QUALITY_HIGH)
        {
            QualitySettings.SetQualityLevel(5);
        }
        if (_quality == UrlInfoConstants.QUALITY_MID)
        {
            QualitySettings.SetQualityLevel(3);
        }
        if (_quality == UrlInfoConstants.QUALITY_LOW)
        {
            QualitySettings.SetQualityLevel(1);
        }
        this.RaiseComponentIsReady();
    }

    public void externalChangeUnityQuality(int quality)
    {
        this.changeUnityQuality(quality);
    }

    public override void Play()
    {
        base.Play();
        if (!this._isFinished)
        {
            this.RaiseOnFinishedEventHandler();
        }
    }
    public override void Stop()
    {
        base.Stop();
    }
    public override void Pause()
    {
        base.Pause();
    }


}
