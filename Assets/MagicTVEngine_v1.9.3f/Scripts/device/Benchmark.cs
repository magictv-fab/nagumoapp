﻿using UnityEngine;
using System.Collections;
using MagicTV.abstracts;


/// <summary>
///
/// </summary>
using MagicTV.abstracts.device;
using MagicTV.vo;
using System.Collections.Generic;


public class Benchmark : BenchmarkAbstract
{
	private string _userDeviceQuality = "UserDeviceQuality";
	private string _deviceModel;
	private int _graphicsMemorySize;
	private int _processorCount;
	private bool _supportShadows;
	private int _systemMemorySize;
	private BenchmarkInfo _benchmarkInfo;
	
	// Nessa versão decidimos que o iPhone 4s é sempre low quality, 
	// porém o ideal seria criar uma List<string> contendo todos que
	// já sabemos ser low ou talvez atualizar vindo direto do server
	private List<string> _lowPerformanceDevice = new List<string> ();
	private List<string> _midPerformanceDevice = new List<string> ();
	/**
	 * @"i386"      on 32-bit Simulator
@"x86_64"    on 64-bit Simulator
@"iPod1,1"   on iPod Touch
@"iPod2,1"   on iPod Touch Second Generation
@"iPod3,1"   on iPod Touch Third Generation
@"iPod4,1"   on iPod Touch Fourth Generation
@"iPhone1,1" on iPhone
@"iPhone1,2" on iPhone 3G
@"iPhone2,1" on iPhone 3GS
@"iPad1,1"   on iPad
@"iPad2,1"   on iPad 2
@"iPad3,1"   on 3rd Generation iPad
@"iPhone3,1" on iPhone 4 (GSM)
@"iPhone3,3" on iPhone 4 (CDMA/Verizon/Sprint)
@"iPhone4,1" on iPhone 4S
@"iPhone5,1" on iPhone 5 (model A1428, AT&T/Canada)
@"iPhone5,2" on iPhone 5 (model A1429, everything else)
@"iPad3,4" on 4th Generation iPad
@"iPad2,5" on iPad Mini
@"iPhone5,3" on iPhone 5c (model A1456, A1532 | GSM)
@"iPhone5,4" on iPhone 5c (model A1507, A1516, A1526 (China), A1529 | Global)
@"iPhone6,1" on iPhone 5s (model A1433, A1533 | GSM)
@"iPhone6,2" on iPhone 5s (model A1457, A1518, A1528 (China), A1530 | Global)
@"iPad4,1" on 5th Generation iPad (iPad Air) - Wifi
@"iPad4,2" on 5th Generation iPad (iPad Air) - Cellular
@"iPad4,4" on 2nd Generation iPad Mini - Wifi
@"iPad4,5" on 2nd Generation iPad Mini - Cellular
@"iPhone7,1" on iPhone 6 Plus
@"iPhone7,2" on iPhone 6
	 * */
	public override void prepare ()
	{
		if (this._lowPerformanceDevice.Count == 0) {
			
			this._lowPerformanceDevice.Add ("iPhone3,1");
			this._lowPerformanceDevice.Add ("iPhone3,2");
			this._lowPerformanceDevice.Add ("iPhone3,3");
			this._lowPerformanceDevice.Add ("iPhone4");
			this._lowPerformanceDevice.Add ("iPhone4,1");
			this._lowPerformanceDevice.Add ("iPad2,1");
			this._lowPerformanceDevice.Add ("iPad2,2");
			this._lowPerformanceDevice.Add ("iPad2,3");
			this._lowPerformanceDevice.Add ("iPad2,4");
			this._lowPerformanceDevice.Add ("iPad2,5");
			this._lowPerformanceDevice.Add ("iPad2,6");
			this._lowPerformanceDevice.Add ("iPad2,7");
			this._lowPerformanceDevice.Add ("iPad2,8");
			
		}
		
		if (this._midPerformanceDevice.Count == 0) {
			
			this._midPerformanceDevice.Add ("iPhone5,1");
			this._midPerformanceDevice.Add ("iPhone5,2");
			this._midPerformanceDevice.Add ("iPhone5,3");
			this._midPerformanceDevice.Add ("iPhone5,4");
			this._midPerformanceDevice.Add ("iPad3,1");
			this._midPerformanceDevice.Add ("iPad3,2");
			this._midPerformanceDevice.Add ("iPad3,3");
			this._midPerformanceDevice.Add ("iPad3,4");
			
		}
		_deviceModel = SystemInfo.deviceModel; // Qual o modelo do celular
		_graphicsMemorySize = SystemInfo.graphicsMemorySize; // Quantidade de memória RAM do aparelho
		_processorCount = SystemInfo.processorCount; // Quantidade de processadores. Neste momento admitimos que um celular com apenas 1 é low quality
		_supportShadows = SystemInfo.supportsShadows; // Nao utilizado. Futuramente criar método para ligar e desligar sombras no futuro (?)
		_systemMemorySize = SystemInfo.systemMemorySize; // Quantidade de memória presente no sistema
		if (PlayerPrefs.GetInt (_userDeviceQuality) == 0) {
			this.testDeviceBenchmark ();
		}
		_benchmarkInfo = new BenchmarkInfo (PlayerPrefs.GetInt (_userDeviceQuality));
		//		Debug.LogWarning ("BENCHMARK PREPARE : User Device Quality = " + PlayerPrefs.GetInt (_userDeviceQuality));
		this.RaiseComponentIsReady ();
	}
	
	public override void retestQuality ()
	{
		this.testDeviceBenchmark ();
	}
	
	private void testDeviceBenchmark ()
	{
		int _quality = UrlInfoConstants.QUALITY_HIGH;
		if (_deviceModel != null && _lowPerformanceDevice.IndexOf (_deviceModel) >= 0) {
			_quality = UrlInfoConstants.QUALITY_LOW;
		}
		if (_deviceModel != null && _midPerformanceDevice.IndexOf (_deviceModel) >= 0) {
			_quality = UrlInfoConstants.QUALITY_MID;
		}
		if (_processorCount != null && _processorCount == 1) {
			_quality = UrlInfoConstants.QUALITY_LOW;
		}	
		if (_graphicsMemorySize != null && _graphicsMemorySize <= 512) {
			_quality = UrlInfoConstants.QUALITY_LOW;
		}
		if (_systemMemorySize != null && _systemMemorySize <= 512) {
			_quality = UrlInfoConstants.QUALITY_LOW;
		}
		PlayerPrefs.SetInt (_userDeviceQuality, _quality);
		PlayerPrefs.Save ();
		//		PlayerPrefs.SetInt (_userDeviceQuality, _quality);
		//		Debug.LogWarning (" Benchmark : testDeviceQuality ");
	}
	
	public override void Play ()
	{
		base.Play ();
		if (!this._isFinished) {
			this.RaiseOnFinishedEventHandler ();
		}
	}
	public override void Stop ()
	{
		base.Stop ();
	}
	public override void Pause ()
	{
		base.Pause ();
	}
	
	public override BenchmarkInfo GetCurrentQuality ()
	{
		return this._benchmarkInfo;
		Debug.LogWarning ("GetQuality BenchmarkInfo ");
	}
}
