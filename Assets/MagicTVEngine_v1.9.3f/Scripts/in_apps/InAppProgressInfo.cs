﻿using UnityEngine;
using System.Collections;
using ARM.interfaces;
using MagicTV.vo;


namespace MagicTV.in_apps
{

		public delegate void OnInAppCommonEventHandler (BundleVO vo) ;
		public delegate void OnInAppProgressEventHandler (float progress) ;

		public class InAppProgressInfo : ARMProgressInterface<BundleVO, OnInAppCommonEventHandler,OnInAppProgressEventHandler>
		{

				protected bool _isComplete = false;
				protected float _currentProgress = 0f;
				/// <summary>
				/// The total bytes to download.
				/// Não esqueça de modificar esse valor sempre que necessário
				/// </summary>
				public float TotalBytesToDownload = 0f;
				
				private OnInAppCommonEventHandler onComplete;
				private OnInAppCommonEventHandler onError;
				private OnInAppProgressEventHandler onProgress;

			
		#region ARMProgressInterface implementation

				public bool isComplete ()
				{
						return _isComplete;
				}

				public void AddCompleteEventhandler (OnInAppCommonEventHandler eventHandler)
				{
						onComplete += eventHandler;
				}

				public void RemoveCompleteEventhandler (OnInAppCommonEventHandler eventHandler)
				{
						onComplete -= eventHandler;
				}

				public void RaiseComplete (BundleVO var)
				{
						_isComplete = true;
						if (onComplete != null) {
								onComplete (var);
						}
				}

				public void AddProgressEventhandler (OnInAppProgressEventHandler eventHandler)
				{
						onProgress += eventHandler;
				}

				public void RemoveProgressEventhandler (OnInAppProgressEventHandler eventHandler)
				{
						onProgress -= eventHandler;
				}

				public void RaiseProgress (float progress)
				{

						_currentProgress = progress;

						if (onProgress != null) {
								onProgress (progress);
						}
				}

				public float GetCurrentProgress ()
				{
						return _currentProgress;
				}

				public void AddErrorEventhandler (OnInAppCommonEventHandler eventHandler)
				{
						onError += eventHandler;
				}

				public void RemoveErrorEventhandler (OnInAppCommonEventHandler eventHandler)
				{
						onError -= eventHandler;
				}

				public void RaiseError (BundleVO var)
				{
						if (onError != null) {
								onError (var);
						}
				}

		#endregion

		}
}
