﻿using UnityEngine;
using System.Collections;
using InApp;
using ARM.utils;
using ARM.transform.filters;
using MagicTV.processes;
using MagicTV.globals;
using MagicTV.globals.events;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// Configura o transform de container de tracking
/// This class is Singleton
/// @author: Leandro Leal
/// @version: 1.0
/// </summary>


namespace InApp.SimpleEvents
{
	public class TrackingTransformConfig : ConfigurableListenerEventAbstract
	{
		private static TrackingTransformConfig _instance;
		protected Vector3 _initialPosition;
		protected Vector3 _initialRotation;
		protected Vector3 _initialScale;
		protected Vector3 _position;
		protected Vector3 _rotation;
		protected Vector3 _scale;

		public GameObject trackingContainer;


		public TrackingTransformConfig ()
		{
			_instance = this;
		}
		
		public static TrackingTransformConfig GetInstance ()
		{
			return _instance;
		}
		
		private void Start ()
		{

			if (this.trackingContainer == null) {
				return;
			}

			this._initialPosition = cloneVector3 (this.trackingContainer.transform.position);
			this._initialRotation = cloneVector3 (this.trackingContainer.transform.localRotation.eulerAngles);
			this._initialScale = cloneVector3 (this.trackingContainer.transform.localScale);
		}
		
		/// <summary>
		/// Configura o transform do container de trackings
		/// </summary>
		/// <param name="config">Config.</param>
		public override void configByString (string config)
		{

//			this.trackingContainer = GameObject.Find ("tracks");
			if (this.trackingContainer == null) {
				Debug.LogError ("TRACKING : Container de tracking veio nulo na config");
				return;
			}
			if (config != "" && config != null) {
				Vector3[] vectors = PipeToArrayOfVector3.Parse (config);
				if (vectors.Length > 0) {
					this._position = vectors [0];
				}
				if (vectors.Length > 1) {
					this._rotation = vectors [1];
				}
				if (vectors.Length > 2) {
					this._scale = vectors [2];
				}
			}

			this.trackingContainer.transform.position = this._position;
//			Debug.LogWarning (" VALORES ROTATION x : " + _rotation.x + " y: " + _rotation.y + " z: " + _rotation.z);
			this.trackingContainer.transform.rotation = Quaternion.Euler (this.cloneVector3 (this._rotation));
//			Debug.LogWarning (" VALORES ROTATION DO CONTAINTERx : " + this.trackingContainer.transform.rotation.eulerAngles.x + " y: " + this.trackingContainer.transform.rotation.eulerAngles.y + " z: " + this.trackingContainer.transform.rotation.eulerAngles.z);
			this.trackingContainer.transform.localScale = this._scale;
			
		}
		protected Vector3 cloneVector3 (Vector3 vec)
		{
			return new Vector3 (vec.x, vec.y, vec.z);
		}
		
		public override void Reset ()
		{
//			return;
			if (this.trackingContainer == null) {
				return;
			}
			this.trackingContainer.transform.position = cloneVector3 (this._initialPosition);
			this.trackingContainer.transform.localRotation = Quaternion.Euler (cloneVector3 (this._initialRotation));
			this.trackingContainer.transform.localScale = cloneVector3 (this._initialScale);
		}
		
	}
}