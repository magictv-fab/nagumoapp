﻿using UnityEngine;
using System.Collections;
using InApp;

/// <summary>
/// Configurar o zoom do que está na lista de filtros do ARCamera ZoomFilter
/// @author: Leandro Leal
/// @version: 1.0
/// </summary>
using ARM.transform.filters;
using System.Collections.Generic;
using System.Linq;

namespace InApp.SimpleEvents
{
	public class ZoomConfig : ConfigurableListenerEventAbstract
	{
		private static ZoomConfig _instance;
		public bool DebugMode;
		public ZoomConfig ()
		{
			_instance = this;
		}

		public static ZoomConfig GetInstance ()
		{
			return _instance;
		}

		//		// Variáveis que podem ser mudadas através de metadata
		public CameraZoomFilter cameraZoomFilter;
		public float percentZoom = 0f;
		public float scaleDeltaMinimum = 35f;
		public float scaleDeltaMaximum = 100f;
		public float deltaZoomRatio = 1000f;
		public float zoomMax = 0.8f;
		public float zoomMin = 0f;

		// Variáveis com valor default e que serão aplicadas no reset
		protected float _initialPercentZoom;
		protected float _initialScaleDeltaMinimum;
		protected float _initialScaleDeltaMaximum;
		protected float _initialDeltaZoomRatio;
		protected float _initialZoomMax;
		protected float _initialZoomMin;


		private void Start ()
		{
			if(this.cameraZoomFilter==null){
				return;
			}
//			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.ZOOM_CONFIG, configByString);
//			InAppEventDispatcher.GetInstance ().AddToResetList (Reset);
			if (cameraZoomFilter == null) {
				return;
			}
			this._initialPercentZoom = this.cameraZoomFilter.percentZoom;
			this._initialScaleDeltaMinimum = this.cameraZoomFilter.ScaleDeltaMinimum;
			this._initialScaleDeltaMaximum = this.cameraZoomFilter.ScaleDeltaMaximum;
			this._initialDeltaZoomRatio = this.cameraZoomFilter.deltaZoomRatio;
			this._initialZoomMax = this.cameraZoomFilter.zoomMax;
			this._initialZoomMin = this.cameraZoomFilter.zoomMin;

		}


		public override void configByString (string config)
		{
			if(this.cameraZoomFilter==null){
				return;
			}
			if (config != "" && config != null) {
				List<float> zoomConfigs = PipeToList.ParseReturningFloats (config);
				if (zoomConfigs.Count () == 6) {
					this.percentZoom = zoomConfigs [0];
					this.scaleDeltaMinimum = zoomConfigs [1];
					this.scaleDeltaMaximum = zoomConfigs [2];
					this.deltaZoomRatio = zoomConfigs [3];
					this.zoomMax = zoomConfigs [4];
					this.zoomMin = zoomConfigs [5];
				}
			}
			this.cameraZoomFilter.percentZoom = this.percentZoom;
			this.cameraZoomFilter.ScaleDeltaMinimum = this.scaleDeltaMinimum;
			this.cameraZoomFilter.ScaleDeltaMaximum = this.scaleDeltaMaximum;
			this.cameraZoomFilter.deltaZoomRatio = this.deltaZoomRatio;
			this.cameraZoomFilter.zoomMax = this.zoomMax;
			this.cameraZoomFilter.zoomMin = this.zoomMin;

		}

		public override void Reset ()
		{
			if(this.cameraZoomFilter==null){
				return;
			}
			this.cameraZoomFilter.percentZoom = this._initialPercentZoom;
			this.cameraZoomFilter.ScaleDeltaMinimum = this._initialScaleDeltaMinimum;
			this.cameraZoomFilter.ScaleDeltaMaximum = this._initialScaleDeltaMaximum;
			this.cameraZoomFilter.deltaZoomRatio = this._initialDeltaZoomRatio;
			this.cameraZoomFilter.zoomMax = this._initialZoomMax;
			this.cameraZoomFilter.zoomMin = this._initialZoomMin;
		}

	}
}
