﻿using UnityEngine;
using System.Collections;
using InApp;

/// <summary>
/// 
/// Configura eventos genericos para qualquer componente do tipo configurable, menos esse
/// 
/// @author: Renato Miawaki
/// @version: 1.0
/// </summary>
using System.Collections.Generic;
using System.Linq;

namespace InApp.SimpleEvents
{
	public class GenericConfig : ConfigurableListenerEventAbstract
	{
		public GameObject[] gameObjectsToFind;
		protected List<ConfigurableListenerEventInterface> configurables = new List<ConfigurableListenerEventInterface>();

		public Dictionary<string, ConfigurableListenerEventInterface> _configurableComponents = new Dictionary<string, ConfigurableListenerEventInterface>();

		public List<GameObject> DebugConfigurableComponents = new List<GameObject>();
		//protected 
		
		void Start(){
			if( gameObjectsToFind == null ||  gameObjectsToFind.Length == 0){
				return;
			}
			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.GENERIC, configByString);
			InAppEventDispatcher.GetInstance ().AddToResetList (Reset);
			//lista os itens configuraveis
			foreach( GameObject go in gameObjectsToFind ){
				ConfigurableListenerEventInterface[] configurablesTemp = go.GetComponentsInChildren<ConfigurableListenerEventInterface>(); ; //GameObject.FindObjectsOfType<ConfigurableListenerEventInterface>();
				if(configurablesTemp.Length > 0){
					configurables.AddRange(configurablesTemp);
				}
			}


			if(configurables != null && configurables.Count > 0 ){
				foreach( ConfigurableListenerEventInterface conf in configurables ){
					if( conf.getUniqueName() == this.getUniqueName()){
						continue;
					}
					_configurableComponents.Add (conf.getUniqueName(), conf);
					DebugConfigurableComponents.Add (conf.getGameObjectReference());
				}
			}
		}
		public override void configByString (string metadado)
		{
			char[] splitchar = { '|' };
			string[] metadataSplitted = metadado.Split (splitchar);
			if( metadataSplitted == null || metadataSplitted.Length == 0){
				//Não colocou nenhum pipe não da pra usar essa classe pra config
				return;
			}
			//primeiro pipe é o nome do gameObject na tela que seja do tipo GonfigurableListEventAbstract
			string gameObjectName = metadataSplitted [0];
			if ( this._configurableComponents.ContainsKey(gameObjectName) ) {
				//tem  o key, entao da pra configurar, o restante do config é o config que a apresentação já recebe normalmente, 
				//consulte como usar cada componente no wiki
				List<string> metadataListSplitted = metadataSplitted.ToList ();
				
				metadataListSplitted.RemoveAt (0);
				string metadata = string.Join (new string (splitchar), metadataListSplitted.ToArray ());

				this._configurableComponents[gameObjectName].configByString(metadata);
			}
		}
		public override void Reset ()
		{
			if(configurables != null && configurables.Count > 0 ){
				foreach( ConfigurableListenerEventInterface conf in configurables ){
					if( conf.getUniqueName() != this.getUniqueName() ){
						//isso gera duplo reset, façam o tratamento para duplo reset em cada componente
						conf.Reset();
					}
				}
			}
		}

	}
}