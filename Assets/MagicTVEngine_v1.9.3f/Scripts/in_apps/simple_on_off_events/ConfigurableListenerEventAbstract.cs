﻿using UnityEngine;
using System.Collections;

public abstract class ConfigurableListenerEventAbstract : MonoBehaviour, ConfigurableListenerEventInterface
{
	public string getUniqueName(){
		return this.gameObject.name;
	}
	public abstract void configByString (string metadata);
	public abstract void Reset ();
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}
}
