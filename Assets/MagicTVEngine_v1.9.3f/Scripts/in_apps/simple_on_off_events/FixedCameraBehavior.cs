﻿using UnityEngine;
using System.Collections;
using InApp;
using ARM.utils;
using ARM.transform.filters;
using MagicTV.processes;
/// <summary>
/// Fixed camera behavior.
/// This class is Singleton
/// @author: Leandro Leal
/// @version: 1.0
/// 
/// 
/// @author: Renato Seiji Miawaki
/// @version: 1.1
/// Pegando config de posição e rotação do evento
/// 
/// @author: Leandro Leal
/// @version: 1.2
/// Adicionado FixedPositionAngleFilter
/// Trabalha com o filtro e não com mainCamera
/// 
/// @author: Renato Seiji Miawaki
/// @version: 1.2.1
/// Removi o mainCamera pois não é mais necessário nessa versao 1.2.x
/// 
/// @author: Renato Seiji Miawaki | Leandro Leal
/// @version: 1.3
/// Mesclado as classes Detacheble e Detached em uma só classe
/// 
/// 
/// @author: Renato Seiji Miawaki | Leandro Leal
/// @version: 1.4
/// FIX detacheble e também 
/// UPGRADE para conseguir ficar fluido com smooth trackin trackout
/// </summary>
using MagicTV.globals;
using MagicTV.globals.events;
using System.Collections.Generic;
using ARM.abstracts;

namespace InApp.SimpleEvents
{
	public class FixedCameraBehavior : ConfigurableListenerEventAbstract
	{

		private static FixedCameraBehavior _instance;
		public FixedPositionAngleFilter fixedFilter;


		public PresentationController _presentationController;

		public FixedCameraBehavior ()
		{
			_instance = this;
		}

		public static FixedCameraBehavior GetInstance ()
		{
			return _instance;
		}

		public Vector3 cameraFixedPosition = Vector3.zero;
		public Vector3 cameraFixedRotation = Vector3.zero;
		//por padrão é true no config a não ser que algum evento o desligue
		protected bool _isCameraFixed = true;
		public bool isCameraFixed {
			get {
				return _isCameraFixed;
			}
			set {
				_isCameraFixed = value;
				if (this.fixedFilter != null) {
					//ativa a o filtro se ativar esse booleano por vias externas, ou inativa se for o caso
					this.fixedFilter._active = _isCameraFixed;
				}
			}
		}
		
		protected Vector3 _inicialCameraFixedPosition ;
		protected Vector3 _inicialCameraFixedRotation ;
		private void Start ()
		{
			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.FIXED_CAMERA, configByString);
			InAppEventDispatcher.GetInstance ().AddToResetList (Reset);
			_inicialCameraFixedPosition = cloneVector3 (cameraFixedPosition);
			_inicialCameraFixedRotation = cloneVector3 (cameraFixedRotation);
			AppRootEvents.GetInstance ().onRaiseStopPresentation += CloseLockedPresentation;
			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.DETACHED_FROM_TRACKING, turnOn);
			InAppEventDispatcher.GetInstance ().AddToResetList (Reset);
			AppRootEvents.GetInstance ().GetAR ().onTrackIn += onToggleArOn;
			AppRootEvents.GetInstance ().GetAR ().onTrackOut += onToggleArOut;
		}
		protected Vector3 cloneVector3 (Vector3 vec)
		{
			return new Vector3 (vec.x, vec.y, vec.z);
		}
		protected bool _smoothModeFirstTime;
		void onToggleArOn (string s)
		{
			if(this._smoothMode){
				//agora que liga o smooth, delay na primeira vez?
				changeSmoothMode(_smoothMode);
			}
			if (this._alwaysLock) {
				return;
			}
			if (this.fixedFilter != null) {
				//ao perder o track, liga o filtro, e vice versa
				this.fixedFilter._active = false ;
				this.fixedFilter.byPass = true ;
			}
		}

		void onToggleArOut (string s)
		{
			if (this._alwaysLock) {
				return;
			}
			if (this.fixedFilter != null) {
				//perdeu o track, liga o filtro
				this.fixedFilter._active = true ;
				this.fixedFilter.byPass = false ;
			}
		}
		/// <summary>
		/// The _always lock.
		/// Significa que perdendo ou encontrando o track ele continua LOCK
		/// </summary>
		protected bool _alwaysLock = false;

		/// <summary>
		/// 
		/// Ao chamar esse metodo quer dizer que quer que seja do tipo relacionado ao track para ligar ou desligar
		/// 
		/// </summary>
		public void turnOn (string mode)
		{
			if(mode == "" || mode == null){
				return;
			}
			List<string> infos = PipeToList.Parse (mode);
			this._isCameraFixed = true;

			if (this._presentationController != null) {
				//faz pular o losing track, nao perco o track nunca
				this._presentationController.lockRun = true;
			}
			this._alwaysLock = false;
			if (infos.Count > 0 && infos[0] == "awaysLocked") {
				this._alwaysLock = true;
				this.fixedFilter.byPass = false;
				this.fixedFilter._active = true;
			}
			_smoothMode = false;
			if (infos.Count > 1 && infos [1] == "smooth") {
				_smoothMode = true;
			}
			//chama o botao de fechar da gui (pede gentilmente para que ele apareça)
			AppRootEvents.GetInstance ().GetScreen ().RaiseShowCloseButton ();
		}
		public SmoothPlugin pluginSmoothOfSwing;
		protected bool _smoothMode ;
		void changeSmoothMode (bool b)
		{
			if(pluginSmoothOfSwing != null){
				//pluginSmoothOfSwing.initialToleranceTime;
				pluginSmoothOfSwing._active = b ;
				pluginSmoothOfSwing.byPass = !b ;
			}
		}

		/// <summary>
		/// Fixa a camera (trava) na posição que está, ou na posição configurada
		/// </summary>
		/// <param name="config">Config.</param>
		public override void configByString (string config)
		{

			if (config != "" && config != null) {
				Vector3[] vectors = PipeToArrayOfVector3.Parse (config);
				if (vectors.Length > 0) {
					this.cameraFixedPosition = vectors [0];
				}
				if (vectors.Length > 1) {
					this.cameraFixedRotation = vectors [1];
				}
			}
			this.fixedFilter.positionToLock = this.cameraFixedPosition ;
			this.fixedFilter.rotationToLock = this.cameraFixedRotation ;
			this.fixedFilter._active = _isCameraFixed ;

		}

		public void CloseLockedPresentation ()
		{
//			Debug.LogError ("CloseLockedPresentation");
			if (this._presentationController != null) {
				this._presentationController.lockRun = false;
			}
			Reset ();
		}   
		public override void Reset ()
		{
//			Debug.LogError ("ResetFixed");
//			this._presentationController.lockRun = false;
			this.fixedFilter._active = false;
			//por padrão é true no config a não ser que algum evento o desligue
			this._isCameraFixed = true;
			cameraFixedPosition = cloneVector3 (_inicialCameraFixedPosition);
			cameraFixedRotation = cloneVector3 (_inicialCameraFixedRotation);
			changeSmoothMode (false);
		}

	}
}