﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
/// <summary>
/// Global config.
/// Configura
/// </summary>
public class RenderSettiongsConfig : MonoBehaviour , ConfigurableListenerEventInterface{
	public float _initialAmbientIntensity;

	public Color _initialAmbientSkyColor;

	public Color _initialAmbientEquatorColor;

	public Color _initialAmbientLight;

	public Color _initialAmbientGroundColor;

	public float _initialReflectionIntensity;

	public int _initialReflectionBounces;

	public float _initialHaloStrength;

	public string DebugMetadata ;
	public bool DebugSendNow ;
	void Start(){
		this._initialAmbientIntensity = RenderSettings.ambientIntensity;
		this._initialAmbientSkyColor = RenderSettings.ambientSkyColor;
		this._initialAmbientEquatorColor = RenderSettings.ambientEquatorColor;
		this._initialAmbientLight = RenderSettings.ambientLight;
		this._initialAmbientGroundColor = RenderSettings.ambientGroundColor;
		this._initialReflectionIntensity = RenderSettings.reflectionIntensity;
		this._initialReflectionBounces = RenderSettings.reflectionBounces;
		this._initialHaloStrength = RenderSettings.haloStrength;
	}
	void Update(){
		if(DebugSendNow){
			DebugSendNow = false ;
			configByString(this.DebugMetadata);
		}
	}
	public string getUniqueName(){
		return this.gameObject.name;
	}
	/// <summary>
	/// Configs the by string.
	/// 
	/// </summary>
	/// <param name="metadata">Metadata.</param>
	public void configByString (string metadata){
		if(metadata == "" | metadata == null){
			return;
		}
		List<string> configs = PipeToList.Parse (metadata);
		if(configs.Count < 2){
			return;
		}
		string property 	= configs [0];
		string valueString 	= configs [1];
		if( property != "" ){

			MethodInfo m = this.GetType().GetMethod("Config_"+property);
			Debug.LogError ("metadata : "+metadata+" : "+m);
			if(m == null){
				//metodo nao existe
				return;
			}
			try{
				Debug.LogError ("chamando metodo passando "+valueString+"  ... e o methodo é "+"Config_"+property);
				m.Invoke(this, new object[] { valueString } ) ;
			} catch( UnityException e ){
				//algum erro para dar invoke
				Debug.LogError ("erro ao configurar GlobalConfig ");
			}
				//			RenderSettings.ambientIntensity = 1f ;
				//			RenderSettings.ambientSkyColor = new Color() ;
				//			RenderSettings.ambientEquatorColor = new Color() ;
				//			RenderSettings.ambientLight = new Color() ;
				//			RenderSettings.ambientGroundColor = new Color() ;
				//			RenderSettings.reflectionIntensity = 0f ;
				//			RenderSettings.reflectionBounces = 1 ;
				//			RenderSettings.haloStrength = 0f ;

		}
	}
	//Colors
	public void Config_ambientSkyColor(string value){
		RenderSettings.ambientSkyColor = this.getColorByString (value, this._initialAmbientSkyColor);
	}
	public void Config_ambientEquatorColor(string value){
		RenderSettings.ambientEquatorColor = this.getColorByString (value, this._initialAmbientEquatorColor);
	}
	public void Config_ambientLight(string value){
		RenderSettings.ambientLight = this.getColorByString (value, this._initialAmbientLight);
	}
	public void Config_ambientGroundColor(string value){
		RenderSettings.ambientGroundColor = this.getColorByString (value, this._initialAmbientGroundColor);
	}
	//floats
	public void Config_ambientIntensity( string value ){
		RenderSettings.ambientIntensity = getMaxMinFloat(value, 0f, 1f) ;
	}
	public void Config_reflectionIntensity( string value ){
		RenderSettings.reflectionIntensity = getMaxMinFloat(value, 0f, 1f) ;
	}
	public void Config_haloStrength( string value ){
		RenderSettings.haloStrength = getMaxMinFloat(value, 0f, 1f) ;
	}

	public void Config_reflectionBounces( string value ){
		int val;
		if (! int.TryParse (value, out val)) {
			return;
		}
		//entre 1 e 5
		if(val > 5){
			val = 5;
		}
		if(val < 1){
			val = 1;
		}
		RenderSettings.reflectionBounces = val;
	}
	public void Reset (){
		RenderSettings.ambientIntensity = this._initialAmbientIntensity;
		RenderSettings.ambientSkyColor = this._initialAmbientSkyColor;
		RenderSettings.ambientEquatorColor = this._initialAmbientEquatorColor;
		RenderSettings.ambientLight = this._initialAmbientLight;
		RenderSettings.ambientGroundColor = this._initialAmbientGroundColor ;
		RenderSettings.reflectionIntensity = this._initialReflectionIntensity ;
		RenderSettings.reflectionBounces = this._initialReflectionBounces ;
		RenderSettings.haloStrength = this._initialHaloStrength ;
	}
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}


	protected float getMaxMinFloat(string value, float min, float max){
		float val;
		if (! float.TryParse (value, out val)) {
			return min;
		}
		if(val > max){
			val = max;
		}
		if(val < min){
			val = min;
		}
		return val;
	}

	protected Color getColorByString(string colorString, Color defaultColor){
		Vector3? vec = PipeToVector3.Parse (colorString);
		if(vec==null){
			return defaultColor;
		}
		Vector3 v = (Vector3)vec;
		return new Color ( v.x, v.y, v.z, 1);
	}
}
