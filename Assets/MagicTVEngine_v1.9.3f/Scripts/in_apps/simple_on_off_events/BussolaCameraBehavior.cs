﻿using UnityEngine;
using System.Collections;
using InApp;
using ARM.transform.filters.abstracts;

namespace InApp.SimpleEvents
{
	public class BussolaCameraBehavior : ConfigurableListenerEventAbstract
	{
		
		private static BussolaCameraBehavior _instance;

		public GameObject objectToMove ;

		public bool DebugIsOn;

		public bool screenButtonToDebug;
		public BussolaCameraBehavior ()
		{
			_instance = this;
		}
		//void OnGUI(){
		//	if(screenButtonToDebug){
		//		if(!this.DebugIsOn){
		//			if( GUI.Button (new Rect(0,0,300,200), "ON")){
		//				TurnOn();	
		//			}
		//		}
		//		if(this.DebugIsOn){
		//			if( GUI.Button (new Rect(0,0,300,200), "OFF")){
		//				TurnOff();	
		//			}
		//		}
		//	}
		//}
		public static BussolaCameraBehavior GetInstance ()
		{
			return _instance;
		}
		
		public TransformPositionAngleFilterAbstract filterToOn;

		private void Start ()
		{
			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.GIROSCOPE_INCLINATION, TurnOn);
			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.GIROSCOPE_INCLINATION_CONFIGURABLE, configByString);
			InAppEventDispatcher.GetInstance ().AddToResetList (Reset);
//			Debug.LogWarning ("BussolaCamera");
		}
		/// <summary>
		/// Ons the recive config.
		/// </summary>
		/// <param name="config">Config.</param>
		public override void configByString (string config)
		{
			Debug.LogWarning ("BussolaCameraBehavior : onReciveConfigPosition = " + config);
			if (objectToMove != null) {
				Vector3? position = PipeToVector3.Parse (config);
				if (position != null) {
					objectToMove.transform.position = (Vector3)position;
				}
			}
		}
		
		// Segue os transform positions 
		public void TurnOn ()
		{
			Debug.LogError ("Bussola Ligando!");
			filterToOn.byPass = false;
			filterToOn._active = true;
			DebugIsOn = true;

		}
		public void TurnOff ()
		{
			filterToOn.byPass = true;
			filterToOn._active = false;
			DebugIsOn = false;
		}
		
		public override void Reset ()
		{
			TurnOff ();

			if (objectToMove != null) {
				objectToMove.transform.position = Vector3.zero;
			}
		}

	}
}