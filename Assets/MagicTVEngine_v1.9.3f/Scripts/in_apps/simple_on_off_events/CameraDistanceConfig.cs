﻿using UnityEngine;
using System.Collections;
using InApp;
using ARM.utils;
using ARM.transform.filters;
using MagicTV.processes;
using MagicTV.globals;
using MagicTV.globals.events;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// Configura a distancia near e far da camera atraves de um event que vem do admin.
/// This class is Singleton
/// @author: Leandro Leal
/// @version: 1.0
/// </summary>


namespace InApp.SimpleEvents
{
	public class CameraDistanceConfig : ConfigurableListenerEventAbstract
	{
		private static CameraDistanceConfig _instance;
		public Camera mainCamera;
		public float cameraNear;
		public float cameraFar;

		public CameraDistanceConfig ()
		{
			_instance = this;
		}

		public static CameraDistanceConfig GetInstance ()
		{
			return _instance;
		}
	
		private void Start ()
		{
			InAppEventDispatcher.GetInstance ().AddEventListener (InAppEventDispatcher.EventNames.CAMERA_DISTANCE_CONFIG, configByString);
			InAppEventDispatcher.GetInstance ().AddToResetList (Reset);
			this.cameraNear = this.mainCamera.nearClipPlane;
			this.cameraFar = this.mainCamera.farClipPlane;
		}

		/// <summary>
		/// Aqui eu trato o metadado para separar o near e o far da camera e configurar posteriormente
		/// </summary>
		/// <param name="config">Config.</param>
		public override void configByString (string metadado)
		{
//			Debug.LogError ("CameraDistanceConfig = " + metadado); 
			if (metadado != "" && metadado != null) {
				List<string> metadados = PipeToList.Parse (metadado);
				if (metadados.Count () > 1) {
					float _cameraNear = float.Parse (metadados [0]);
					float _cameraFar = float.Parse (metadados [1]);

					if (_cameraNear >= _cameraFar) {
						return;
					}

					if (_cameraFar < 5000000000f) {
						_cameraFar = 5000000000f;
					}

					if (_cameraNear < 0.01f) {
						_cameraNear = 0.01f;
					}

					mainCamera.nearClipPlane = _cameraNear;
					mainCamera.farClipPlane = _cameraFar;
				}
			}

		}

		public override void Reset ()
		{
//			Debug.LogError ("ResetCameraDistanceConfig");
			this.mainCamera.nearClipPlane = 0.01f;
			this.mainCamera.farClipPlane = this.cameraFar;
		}

	}
}