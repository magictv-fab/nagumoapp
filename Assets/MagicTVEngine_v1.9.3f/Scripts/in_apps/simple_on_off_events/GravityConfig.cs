﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GravityConfig : MonoBehaviour , ConfigurableListenerEventInterface{


	
	public string getUniqueName(){
		return this.gameObject.name;
	}
	public void configByString (string metadata){
		Vector3? gravity = PipeToVector3.Parse (metadata);
		if(gravity!= null){
			Physics.gravity = (Vector3) gravity ;
		}
	}
	public void Reset (){
		Physics.gravity = new Vector3 (0, -9.81f, 0);
	}
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}
}
