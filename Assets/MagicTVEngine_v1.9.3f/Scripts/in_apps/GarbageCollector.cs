﻿using System.Collections;
using ARM.animation;
using System.Collections.Generic;


/// <summary>
/// 
/// Garbage colector.
/// v2.0
/// Renato Seiji Miawaki
/// 
/// Nessa versão troquei de trackName por name, porém provavelmente não vai funcionar, mas resolve o erro
/// v2.1
/// Renato Seiji Miawaki
/// 
/// </summary>
using UnityEngine;
using System;

namespace MagicTV.in_apps
{
		public class GarbageCollector : MonoBehaviour
		{
		
				public List<GarbageItemVO> _presentations = new List<GarbageItemVO> () ;

		
				public float tolerance = 10f	;
				public int maxItens = 4 ;
				//TODO: Memória máxima em Megas, caso ultrapasse, não adiciona item sem tirar outro, sendo o mínimo 1 item na memória
				public float maxMemoryUsage = 200 ;

				public int debugTotalItens = 0 ;
				public string debugFirstItemName = "" ;
				public float debugFirstItemTime ;



				public void AddToIncinerationList (ViewTimeControllAbstract presentation)
				{

						//não cabe dois na lista, mandou matar um e já existe, mata ele

						GarbageItemVO item = this.getGarbageItem (presentation.name);
			
						if (item != null) {
								//já está na lista, zera o tempo como se estivesse entrando agora pela primeira vez - esqueci pq tirei entao coloquei de novo para testar
								item.timerCount = 0f;
								return;
						}
						if (this._presentations.Count >= maxItens && this._presentations.Count > 0) {

								this.doDestroy (this._presentations [0]);

						}
						item = new GarbageItemVO (){ presentation = presentation, timerCount = 0f };
						this._presentations.Add (item);
				}
		
				void LateUpdate ()
				{
						if (this.tolerance > 0 && this._presentations != null && this._presentations.Count > 0) {

								this._presentations.ForEach (v => v.timerCount += Time.deltaTime);
								List<GarbageItemVO> itens = this._presentations.FindAll (v => v.timerCount >= this.tolerance);
								if (itens != null && itens.Count > 0) {
										this.doDestroy (itens [0]);
										return;
								}
								//			this._presentations.FindAll ( v => v.timerCount >= this.tolerance )
								//				.ForEach( v => this.doDestroy( v ) ) ;
				
						}
						this.debugTotalItens = this._presentations.Count;
						if (this.debugTotalItens > 0 && this._presentations [0].presentation != null) {
								this.debugFirstItemName = this._presentations [0].presentation.name;
								this.debugFirstItemTime = this._presentations [0].timerCount;
						}

				}
				/// <summary>
				/// Gets the presentation in to trash.
				/// </summary>
				/// <returns>The presentation.</returns>
				/// <param name="trackingName">Tracking name.</param>
				public GarbageItemVO getGarbageItem (string trackingName)
				{
						if (this._presentations == null) {
								return null;
						}
						GarbageItemVO item = this._presentations.Find (v => v.presentation.name == trackingName);
						if (item != null) {
								Debug.LogWarning ("[G 1.0 ] GarabageColector . getGarbageItem . encontrou");
								return item;
						}
						Debug.LogWarning ("[G 1.1 ] GarabageColector . getGarbageItem NÃO encontrou item com trackname : " + trackingName);
						return null;
				}
		
				public ViewTimeControllAbstract getPresentation (string trackName)
				{
			
						GarbageItemVO item = this.getGarbageItem (trackName);
			
						if (item != null) {
								if (item.isDied) {
										return null;
								}
								return item.presentation;
						}
						return null;
			
				}
				/// <summary>
				/// Clears all.
				/// Mata todas as apresentações
				/// </summary>
				public void clearAll ()
				{
						return;
						this._presentations.ForEach (v => this.doDestroy (v));



				}	
				private List<GameObject> trashs = new List<GameObject> () ;
				void doDestroy (GarbageItemVO item)
				{
						if (item == null) {
								return;
						}
						if (item.presentation == null) {
								Debug.LogWarning ("[GC] 22.1 doDestroy ~ deletando item " + item.presentation.name);
								this._presentations.Remove (item);
								return;
						}
						Debug.LogWarning ("[GC] 22.0 doDestroy ~ deletando item " + item.presentation.name);
						item.isDied = true;
						//item.presentation.onHide (  ) ;
						item.presentation.Hide ();
						this.trashs.Add (item.presentation.gameObject);
						//GameObject.DestroyImmediate ( item.presentation.gameObject ) ;

						this._presentations.Remove (item);

				}
		
				public void RemoveFromList (ViewTimeControllAbstract presentation)
				{

						GarbageItemVO item = this.getGarbageItem (presentation.name);
						if (item != null) {
								try {
										this._presentations.Remove (item);
								} catch (UnityException e) {
										Debug.LogError (" Algum erro de null unity " + e.ToString ());
								} catch (Exception e) {
										Debug.LogError (" Algum erro de null system " + e.ToString ());
								} catch {
										Debug.LogError (" Algum erro de null ");
								}
								return;
						}
				}
		}
}