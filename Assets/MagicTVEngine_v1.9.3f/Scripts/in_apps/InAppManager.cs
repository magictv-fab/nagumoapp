using UnityEngine;
using System.Collections;
using MagicTV.abstracts ;
using MagicTV.globals;
using MagicTV.globals.events;
using MagicTV.vo;
using MagicTV.abstracts.device;
using System.Linq;
using System.Collections.Generic;
using MagicTV.utils;
using ARM.components;
using InApp;
using MagicTV.download;
using MagicTV.processes;
/// <summary>
/// In app manager.
///
/// Nessa versão o fluxo mudou em comparação a anterior.
/// @author 	: Renato Seiji Miawaki
/// @version 	: 1.3
///
/// Nessa versão o evento de botao fechar da dispose na apresentação atual aberta
/// @author 	: Renato Seiji Miawaki
/// @version 	: 1.4
///
/// </summary>
using MagicTV.abstracts.screens;


namespace MagicTV.in_apps
{
	public class InAppManager : InAppManagerAbstract
	{
        public static int lojaCode = 1;

		bool _chekStartVuforia = false;

		#region Debug
		public string DebugTrackSimulate;
		public bool DebugEventTrackInSimulate;
		public bool DebugEventTrackOutSimulate;
		void Update ()
		{
			if (DebugEventTrackInSimulate) {
				DebugEventTrackInSimulate = false;
				if (DebugTrackSimulate != "" && DebugTrackSimulate != null) {
					this.Play (DebugTrackSimulate);
					return;
				}
			}
			if (DebugEventTrackOutSimulate) {
				DebugEventTrackOutSimulate = false;
				this.Stop ();
			}
		}
		#endregion
		private static GameObject _inAppMagicTVDemo;

		public DeviceFileInfoAbstract deviceFileInfo ;

		protected Dictionary<string, GroupInAppAbstractComponentsManager> _inAppMemoryCache;
		protected Dictionary<string, GroupInAppAbstractComponentsManager> _inAppMemoryCacheByInAppsOpened;
		/// <summary>
		/// The AR camera.
		/// </summary>
		//  public GameObject camera ;
		/// <summary>
		/// The loader.
		/// </summary>
		public GameObject loader;
		public GameObject luzes;
		

		public string inAppName;
		public bool addInAppNow;
				
		//Arraste os InAppAbstract do Resources para essa lista
		public InAppAbstract[] InAppTypes;
		//a lista de inApps do Resources cacheada
		protected Dictionary<string, InAppAbstract> _inAppTypesList;
		public override void prepare ()
		{

			//fazer um pool de InApp s e pegar desse pull
			_inAppMemoryCache = new Dictionary<string, GroupInAppAbstractComponentsManager> ();
			_inAppTypesList = new Dictionary<string, InAppAbstract> ();
			for (int i = 0; i < InAppTypes.Length; i++) {
				_inAppTypesList.Add (InAppTypes [i].GetInAppName (), InAppTypes [i]);
			}
//			Debug.LogWarning ("I InAppManager . init");

			AppRootEvents.GetInstance ().onRaiseStopPresentation += KillCurrentPresentation;

			//precisa que o file info esteja pronto
			if (! this.deviceFileInfo.isComponentIsReady ()) {
				this.deviceFileInfo.AddCompoentIsReadyEventHandler (this.RaiseComponentIsReady);
				this.deviceFileInfo.prepare ();
				return;
			}
			//removido : Renato
			//WindowControlListScript.Instance.WindowInAppListWaiting.Hide ();
			if (this.luzes != null) {
				this.luzes.SetActive (false);
			}
			this.RaiseComponentIsReady ();

		}

		void KillCurrentPresentation ()
		{
			this.doDisposeCurrent (this._lastAppName);
		}
		/// <summary>
		/// Shows the loader.
		/// Antiga borboleta
		/// </summary>
		public void showLoader ()
		{
			if (this.loader != null) {
				this.loader.SetActive (true);
			}
		}
		/// <summary>
		/// Hides the loader.
		/// Esconde a antiga borboleta
		/// </summary>
		public void hideLoader ()
		{
			if (this.loader != null) {
				this.loader.SetActive (false);
			}
		}

		/// <summary>
		/// Gets the in app by bundle.
		/// </summary>
		/// <returns>The in app by bundle.</returns>
		/// <param name="bundle">Bundle.</param>
		protected InAppAbstract getInAppByBundle (BundleVO bundle)
		{

			//InAppVideoPresentation inApp = (InAppVideoPresentation)Instantiate (Resources.Load<InAppVideoPresentation> (resourceName [0]));
			string prefabName = BundleHelper.GetPrefabNameByBundle (bundle);
//			Debug.LogWarning ("XXXXXXX InAppManager . getInAppByBundle - prefabName:" + prefabName);
			if (prefabName == null || prefabName == "") {
				AppRootEvents.LogError ("InAppManager", "tentou encontrar o nome do prefab mas veio null .");
				return null;
			}
			InAppAbstract resource = getPresentationPrefab (prefabName);
			if (resource == null) {
				AppRootEvents.LogError ("InAppManager", "tentou instanciar um prefab de nome (" + prefabName + ") que não foi encontrado.");
				return null;
			}
			InAppAbstract inApp = (InAppAbstract)Instantiate (resource);
			inApp.AddCompoentIsReadyEventHandler (LoadScreenCircle.Instance.HideNow);
			inApp.SetBundleVO (bundle);

			inApp.name = "inApp_" + bundle.id;
			if (_currentAnalytics != null) {
				//remove os listeners do atual
				_currentAnalytics.stop ();
			}
						
			//verifica se tem id do google
			string analytics = utils.BundleHelper.GetMetadataSingleValueByKey ("GoogleAnalytics", bundle.metadata);
			if (analytics != null) {
				//tem dados de analytics
				_currentAnalytics = InAppAnalytics.GetAnalytics (bundle.name, analytics);
				if (_currentAnalytics != null) {
					_currentAnalytics.init ();
					inApp.SetAnalytics (_currentAnalytics);
				}
			}

			return inApp;
		}
		/// <summary>
		/// Gets the presentation prefab.
		/// </summary>
		/// <returns>The presentation prefab.</returns>
		/// <param name="prefabName">Prefab name.</param>
		InAppAbstract getPresentationPrefab (string prefabName)
		{
			if (_inAppTypesList.ContainsKey (prefabName)) {
				return _inAppTypesList [prefabName];
			}
			return null;
		}

		/// <summary>
		/// Gets the in app by string name.
		///
		/// a string recebida é o nome lá no track, então precisa parsear e verificar o contexto tb o bundle extado
		/// </summary>
		/// <returns>The in app by string.</returns>
		/// <param name="appName">App name.</param>
		protected InAppAbstract[] getInAppByString (string appContext)
		{
			Debug.Log ("*appContext   " + appContext);
			//			Debug.LogWarning ("*******[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . getInAppByStribng 1 " + appContext);
			//teria que verificar se já não tem o app na memória do garbage e etc, mas por enquanto ignora isso
			//teria que descobrir qual e o tipo de InApp pela string, mas está chumbado que é sempre vídeo por enquanto
			BundleVO[] bundles = this.deviceFileInfo.getBundles (appContext);

			if (bundles == null) {
				return null;
			}
			string idgroup = "";
			foreach (BundleVO inTemp in bundles) {
				idgroup += inTemp.id + "_";
			}
//			Debug.LogError (" idgroup ::: " + idgroup);
			//pode ser nesse ponto do processo iniciar o processo de donwload e etc
			if (bundles == null || bundles.Length <= 0) {
				Debug.Log ("bundles veio null");
				return null;
			}
//			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") " + bundles.Length + " InAppManager . getInAppByStribng 2 " + appContext);
			//modificação do fluxo para definir que o download é de responsabilidade do InAppManager e não do InApp individualmente

			//1 - perceber que não foi baixado os UrlInfoVO
			//2 - Chamar a janela de Confirm
			//3 - Pausa o fluxo ( e ignora trackings recebidos )
			//4 - joga pra frente esse processo e só faz acontecer se CONFIRM = true
			//5 - Inicia o processo de download passando todos os UrlInfo envolvidos e chama a janela de download para exibir a %

			InAppAbstract[] inapps = new InAppAbstract[bundles.Length];

			var onDemandDownloader = new InAppOnDemandDownloadManager (bundles);

			if (onDemandDownloader.HasFileToDownload ()) {
				Debug.Log ("Vai Baixar");
				onDemandDownloader.Download ();
				return null;
			}
	
			bool hasLight = false;
			if (this.luzes) {
				this.luzes.SetActive (false);
			}
//			Debug.LogWarning ("------------[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . getInAppByStribng 3 " + appContext);
//						List<InAppAbstract> inApps = new List<InAppAbstract> ();
			List<string> eventNames = new List<string> ();

			this.Reset ();
			for (int i = 0; i <  bundles.Length; i++) {

				BundleVO bundle = bundles [i];
				inapps [i] = getInAppByBundle (bundle);
				if (!hasLight) {
					string light = BundleHelper.GetMetadataSingleValueByKey ("light", bundle.metadata);
					if (light != "" && light != null) {
						hasLight = bool.Parse (light);
					}
				}
				if (hasLight) {
					this.luzes.SetActive (true);
				}

				string[] eventName = BundleHelper.GetMetadataValuesByKey ("Event", bundle.metadata);
				if (eventName != null) {
					eventNames = eventName.ToList ();
					InAppEventDispatcher.GetInstance ().treatToRaiseEvents (eventNames);
				}

				if (i == 0) {
					TransformEvents.RaiseBundleID (int.Parse (bundle.id));
					TransformEvents.RaiseInApp (inapps [i]);
				}
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . getInAppByStribng 4 ! " + appContext);
			return inapps;
		}

		GroupInAppAbstractComponentsManager _currentPresentation ;
		InAppAnalytics _currentAnalytics;
		string _lastAppName;
		string _appNameToRunNow ;
		public override void Play (string appName)
		{

			ARM.device.DeviceControll.vibrate ();

//						Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . Play 1 " + appName);
			Debug.Log ("[ Play ] (" + this._inAppMemoryCache.ContainsKey (appName) + ") para o appName " + appName);




			if (!this._inAppMemoryCache.ContainsKey (appName)) {
				InAppAbstract[] inApps = this.getInAppByString (appName);
				if (inApps == null || inApps.Length == 0) {
					Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . Play | Nao encontrou o app  " + appName);
					return;
				}

				GameObject gameObjGroup = new GameObject (appName);
				GroupInAppAbstractComponentsManager group = gameObjGroup.AddComponent<GroupInAppAbstractComponentsManager> ();

				group.SetComponents (inApps);
				this._inAppMemoryCache.Add (appName, group);

			} 
//						Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . Play 2 " + appName);
			if (_currentPresentation != null) {
				_currentPresentation.Stop ();
			}
//						Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . Play 3 " + appName);
			this.disposeCurrent (appName);
			//Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . Play 4 " + appName);
			//depois de verificar, seta que o lastAppName é o atual
			this._lastAppName = appName;
			this._appNameToRunNow = appName;
			this.doPlay ();
			ScreenEvents.GetInstance ().showCurrentCaption ();

            if (appName.Contains("nagumo_acougue"))
            {
                Debug.Log("Vai para o acougue");

                lojaCode = int.Parse(appName.Split('_')[2]);

                Debug.Log("Na loja: " + lojaCode);

                //Application.LoadLevel("Game"
                LoadLevelName loadLevelName = gameObject.AddComponent<LoadLevelName>() as LoadLevelName;

                loadLevelName.LoadLevelAcougue();
            }


			if (appName.Contains("Tracking_Doce_Novembro_2018"))
			{
				Debug.Log("Vai para o Game");

				Application.LoadLevel("Doce_Novembro_2");

			}

		}
		/// <summary>
		/// Reset all the simpleOnOffEvents
		/// </summary>
		public void Reset ()
		{
//			Debug.LogError ("RESET!!!!"); 
			InAppEventDispatcher.GetInstance ().raiseReset ();
		}

		/// <summary>
		/// Disposes the current.
		/// Esboço de garbage collector
		/// </summary>
		/// <param name="appName">App name.</param>
		void disposeCurrent (string appName)
		{
		
//			Debug.LogError ("NOME DO APP = " + appName); 
			if (this._lastAppName != null && this._lastAppName != appName) {
				this.doDisposeCurrent (appName);
			}
		}

		void doDisposeCurrent (string appName)
		{
			if (this._inAppMemoryCache.ContainsKey (this._lastAppName)) {
				//					this.Reset ();
//				Debug.LogError ("[I] InAppManager . dispose ~ " + this._lastAppName);
				this._inAppMemoryCache [this._lastAppName].Dispose ();
				this._inAppMemoryCache.Remove (this._lastAppName);
//				if (_currentPresentation != null) {
//					_currentPresentation.Dispose ();
//					Debug.LogWarning ("Deu dispose do group");
//				}
				this._lastAppName = null;
			
				//  Destroy(_currentPresentation.gameObject);
				//  _currentPresentation = null;
			}

		}
		void doPlay ()
		{
			if (this._appNameToRunNow == null) {
				//não quer mais
				return;
			}
			if (!this._inAppMemoryCache.ContainsKey (this._appNameToRunNow)) {
				Debug.LogWarning ("InAppManager . não tem???");
				return;
			}
//			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . doPlay 1 " + this._appNameToRunNow);
			_currentPresentation = this._inAppMemoryCache [this._appNameToRunNow];
			if (!_currentPresentation.isComponentIsReady ()) {
//				Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . doPlay 1.1 " + this._appNameToRunNow);
				showLoader ();
				_currentPresentation.AddCompoentIsReadyEventHandler (doPlay);
				//Invoke ("prepareCurrent", 4f);
				_currentPresentation.prepare ();
				return;
			}
//			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . doPlay 2 " + this._appNameToRunNow);
			hideLoader ();
//			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . doPlay 3 " + this._appNameToRunNow);
			_currentPresentation.Run ();
//			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppManager . doPlay 4 " + this._appNameToRunNow);
			if (_currentAnalytics != null) {
				_currentAnalytics.Open ();
			}
		}
		void prepareCurrent ()
		{
			_currentPresentation.prepare ();
		}


		public override void Stop (string appName)
		{
          //  if (GameObject.FindObjectOfType(typeof(PresentationController)))

            try
            {
                PresentationController pc = GameObject.FindObjectOfType(typeof(PresentationController)) as PresentationController;

                if (pc.buttonGame)
                    pc.buttonGame.SetActive(true);
            }
            catch
            {
                Debug.Log("Erro no Presentation Controller.. n achou");
            }

			Debug.Log (" InAppManager Stop ");
			hideLoader ();
			if (appName == this._appNameToRunNow) {
				this._appNameToRunNow = null;
			}
			if (_currentAnalytics != null) {
				_currentAnalytics.Close ();
			}


			if (_inAppMemoryCache != null && _inAppMemoryCache.ContainsKey (appName)) {
				Debug.Log (" _inAppMemoryCache IF ");
				_inAppMemoryCache [appName].Stop ();

				return;
			}

			if (_currentPresentation != null) {
				Debug.Log (" _currentPresentation != null");
				_currentPresentation.Stop ();
			}
		}

		public override void Stop ()
		{
			base.Stop ();

//			QCARManager.Instance.ARCamera.gameObject.SetActive (true);

		}
		public override void Pause ()
		{
			base.Pause ();
		}
		public override void Play ()
		{
			base.Play ();
		}

	}
}
