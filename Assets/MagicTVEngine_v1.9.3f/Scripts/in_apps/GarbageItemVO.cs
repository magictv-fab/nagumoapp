﻿using UnityEngine;
using System.Collections;
using ARM.animation;

/// <summary>
/// Garbage item VO
/// @author : Renato Seiji Miawaki
/// </summary>
namespace MagicTV.in_apps
{
		public class GarbageItemVO
		{

				public ViewTimeControllAbstract presentation ;
		
				public float timerCount = 0f ;

				public bool isTrash = false ;

				public bool isDied = false ;

		}
}