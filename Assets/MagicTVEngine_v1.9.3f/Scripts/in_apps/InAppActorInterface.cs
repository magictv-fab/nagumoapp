﻿
namespace InApp
{
    interface InAppActorInterface
    {

        void Play();
        void Pause();
        
        void Hide();

        void Show();

        // tipo um reset
        void RewindAndPlay();
        
        // tipo um stop
        void RewindAndPause();

        // CustomAction inicialmente para
        // AssetBundle - ir p/ animacao especifica.  - setar variáveis do animator controller
        // Audio - PlauOneShot 
        void CustomAction(string action);

    }
}
