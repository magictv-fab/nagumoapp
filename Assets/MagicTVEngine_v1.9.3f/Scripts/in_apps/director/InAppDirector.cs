﻿using UnityEngine;
using System.Collections;

namespace InApp
{

    // Liste aqui os eventos que vc pretende ligar e desligar
    public enum DirectorActions
    {
        PLAY ,
        PAUSE,
        STOP,
        RESET,
        SHOW,
        HIDE
    }

    public class InAppDirector : MonoBehaviour
    {



        // Use this for initialization
        void Start()
        {
             
            
        }

        void Test(string s)
        {
            Debug.Log(s);
            DirectorMeta x = DirectorMeta.Parse(s);
            Debug.Log(x.BundleID);
            Debug.Log(x.Action);
            Debug.Log(x.Delay);

        }

        // Update is called once per frame
        void Update()
        {

        }

    }
}
