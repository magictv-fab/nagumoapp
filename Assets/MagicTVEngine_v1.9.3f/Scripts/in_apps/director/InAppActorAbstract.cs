﻿using UnityEngine;
using System;
using MagicTV.abstracts;
using MagicTV.vo;
using MagicTV.utils;
using MagicTV.processes;
using MagicTV.globals.events;


namespace InApp
{
	public abstract class InAppActorAbstract : InAppAbstract, InAppActorInterface
	{
		public static bool centerExit;
		public static ScreenOrientation defaultOrientation;
		
		protected bool _autoPlayOnRun = true;
		protected bool _autoHideOnStop = true;
		protected bool _resetOnStop ;
		protected bool _loop;
		
		void Awake()
		{
			defaultOrientation = Screen.orientation;
		}
		
		protected void ActorRun ()
		{
			Debug.Log ("Actor RUN " + this.gameObject.name);
			if (_autoPlayOnRun) {
				Play ();
			}
		}
		
		protected void ActorStop ()
		{
			Debug.Log ("Actor STOP " + this.gameObject.name);
			if (_autoHideOnStop) {
				
				
				Hide ();
			}
			if (_resetOnStop) {
				RewindAndPause ();
			} else {
				Pause ();
			}
			

		}
		
		protected void applyActorConfig (BundleVO bundle)
		{
			//			Debug.LogError (bundle);
			Debug.Log ("* Metadata bundle: " + bundle.ToString ());
			
			string stringResetOnStop = BundleHelper.GetMetadataSingleValueByKey ("ResetOnStop", bundle.metadata);
			if (stringResetOnStop != null) {
				bool p_resetOnStop;
				if (Boolean.TryParse (stringResetOnStop, out p_resetOnStop)) {
					_resetOnStop = p_resetOnStop;
				}
			}
			
			string stringAutoPlay = BundleHelper.GetMetadataSingleValueByKey ("PlayOnRun", bundle.metadata);
			
			if (stringAutoPlay != null) {
				try {
					_autoPlayOnRun = Convert.ToBoolean (stringAutoPlay);
				} catch (Exception e) {
					Debug.LogError (e);
				}
			}
			
			
			string stringHideOnStop = BundleHelper.GetMetadataSingleValueByKey ("HideOnStop", bundle.metadata);
			if (stringHideOnStop != null) {
				try {
					_autoHideOnStop = Convert.ToBoolean (stringHideOnStop);
				} catch (Exception e) {
					Debug.LogError (e);
				}
			}
			
			
			
			string stringLoop = BundleHelper.GetMetadataSingleValueByKey ("Loop", bundle.metadata);
			if (stringLoop != null) {
				bool loop;
				if (Boolean.TryParse (stringLoop, out loop)) {
					_loop = loop;
				}
			}
			//verifica se trava para continuar a apresentação caso perca o track - isso é aplicado a todos os
			string continueOnLostTrack = BundleHelper.GetMetadataSingleValueByKey ("ContinueOnLostTrack", bundle.metadata);
			bool continueOnLostTrackBool;
			if (continueOnLostTrack != null) {
				
				Boolean.TryParse (continueOnLostTrack, out continueOnLostTrackBool);
				
				if (continueOnLostTrackBool)
				{
					PresentationController.Instance.lockRun = true;
					//actor manda ligar, quem desliga é o mesmo que apertou o X
					AppRootEvents.GetInstance ().GetScreen ().RaiseShowCloseButton ();
				}
			} 
			
			//FB verificar o metadado que coloca o x no centro.
			string centerExitValue = BundleHelper.GetMetadataSingleValueByKey ("CenterExit", bundle.metadata);
			
			//FB limpa cache ref a CenterExit
			centerExit = false;
			
			if (centerExitValue != null) 
			{
				Boolean.TryParse (centerExitValue, out centerExit);
				//PresentationController.Instance.lockRun = centerExit;
				if(centerExit)
					AppRootEvents.GetInstance ().GetScreen ().RaiseShowCloseButton ();
				
			}
			
			//FB verificar o metadado que muda a orientacao.
			string orientationLock = BundleHelper.GetMetadataSingleValueByKey ("OrientationLock", bundle.metadata);
			if (orientationLock != null) 
			{
				//tudo minuscula n importando como foi escrita.
				orientationLock = orientationLock.ToLower();
				
				switch(orientationLock)
				{
				case "autorotation":
					Screen.orientation = ScreenOrientation.AutoRotation;
					break;
				case "landscape":
					Screen.orientation = ScreenOrientation.Landscape;
					break;
				case "portrait":
					Screen.orientation = ScreenOrientation.Portrait;
					break;
				case "landscapeleft":
					Screen.orientation = ScreenOrientation.LandscapeLeft;
					break;
				case "landscaperight":
					Screen.orientation = ScreenOrientation.LandscapeRight;
					break;
				case "portraitupsidedown":
					Screen.orientation = ScreenOrientation.PortraitUpsideDown;
					break;
				}
				
				//FB cadastra metodo para ser chamado quando perder o tracking ou/se fechar a apresentacao.
				AppRootEvents.GetInstance ().onRaiseStopPresentation += DefautOrientation;
			}
			
		}
		
		public void DefautOrientation()
		{
			Debug.Log ("defaut orientation");
			Screen.orientation = defaultOrientation;
			
		}
		
		abstract public void Play ();
		abstract public void Pause ();
		
		abstract public void Hide ();
		
		abstract public void Show ();
		
		abstract public void RewindAndPause ();
		
		abstract public void RewindAndPlay ();
		
		// CustomAction inicialmente para
		// AssetBundle - ir p/ animacao especifica.  - setar variáveis do animator controller
		// Audio - PlauOneShot 
		abstract public void CustomAction (string action);
		
	}
}
