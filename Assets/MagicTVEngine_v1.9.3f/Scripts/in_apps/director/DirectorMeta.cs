﻿using UnityEngine;
using System.Collections;
using System;
namespace InApp
{
    public class DirectorMeta
    {
         
        public int BundleID;

        public DirectorActions Action;

        public float Delay;



        public static DirectorMeta Parse(string data)
        {
            DirectorMeta directorMeta = new DirectorMeta();
            string[] dataValues = data.Split('|');
            Debug.LogError(dataValues[0]);
            directorMeta.BundleID = int.Parse(dataValues[0]);
            try
            {
                directorMeta.Action = (DirectorActions)Enum.Parse(typeof(DirectorActions), dataValues[1]);
            }
            catch (ArgumentException)
            {
                Debug.LogError(dataValues[1] + " não é um DirectorAction válido");
            }

            if (dataValues.Length > 2)
            {
                directorMeta.Delay = float.Parse(dataValues[2]);
            }

            return directorMeta;

        }
    }
}

