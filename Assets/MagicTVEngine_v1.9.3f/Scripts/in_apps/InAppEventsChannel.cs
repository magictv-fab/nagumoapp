﻿using UnityEngine;
using System.Collections;

/// <summary>
/// In app events channel.
/// Canal de comunicação entre inapps
/// 
/// @author : Renato Seiji Miawaki
/// </summary>
using System.Collections.Generic;
using System;
using ARM.events;


namespace MagicTV.in_apps
{
		public class InAppEventsChannel
		{
				protected Dictionary<string, float> _floatValues = new Dictionary<string, float> ();
				/// <summary>
				/// Gets the event channel.
				/// 
				/// </summary>
				/// <returns>The event channel.</returns>
				/// <param name="channelName">Channel name.</param>
				public float GetFloatValue (string key)
				{
						if (this._floatValues.ContainsKey (key)) {
								return this._floatValues [key];
						}
						return 0f;
				}
				/// <summary>
				/// Sets the float value.
				/// </summary>
				/// <param name="key">Key.</param>
				/// <param name="value">Value.</param>
				public void SetFloatValue (string key, float value)
				{
						if (this._floatValues.ContainsKey (key)) {
								this._floatValues [key] = value;
								return;
						}
						this._floatValues.Add (key, value);
				}
				protected Dictionary<string, string> _stringValues = new Dictionary<string, string> ();
				/// <summary>
				/// Gets the event channel.
				/// 
				/// </summary>
				/// <returns>The event channel.</returns>
				/// <param name="channelName">Channel name.</param>
				public string GetStringValue (string key)
				{
						if (this._stringValues.ContainsKey (key)) {
								return this._stringValues [key];
						}
						return "";
				}
				/// <summary>
				/// Sets the string value.
				/// </summary>
				/// <param name="key">Key.</param>
				/// <param name="value">Value.</param>
				public void SetStringValue (string key, string value)
				{
						if (this._stringValues.ContainsKey (key)) {
								this._stringValues [key] = value;
								return;
						}
						this._stringValues.Add (key, value);
				}
				protected Dictionary<string, int> _intValues = new Dictionary<string, int> ();
				public int GetIntValue (string key)
				{
						if (this._intValues.ContainsKey (key)) {
								return this._intValues [key];
						}
						return 0;
				}
				/// <summary>
				/// Sets the string value.
				/// </summary>
				/// <param name="key">Key.</param>
				/// <param name="value">Value.</param>
				public void SetIntValue (string key, int value)
				{
						if (this._intValues.ContainsKey (key)) {
								this._intValues [key] = value;
								return;
						}
						this._intValues.Add (key, value);
				}
				/// <summary>
				/// The void events pull.
				/// </summary>
				protected Dictionary<string, VoidEventDispatcher> _voidEvents = new Dictionary<string, VoidEventDispatcher> ();
				/// <summary>
				/// Gets the void event handler.
				/// </summary>
				/// <param name="key">Key.</param>
				public VoidEventDispatcher GetVoidEventHandler (string key)
				{
						if (!this._voidEvents.ContainsKey (key)) {
								this._voidEvents.Add (key, new VoidEventDispatcher ());
						}
						return this._voidEvents [key];
				}
				/// <summary>
				/// The generic events.
				/// </summary>
				protected Dictionary<string, GenericParameterEventDispatcher> _genericEvents = new Dictionary<string, GenericParameterEventDispatcher> ();
				/// <summary>
				/// Gets the generic parameter event handler.
				/// </summary>
				/// <param name="key">Key.</param>
				public GenericParameterEventDispatcher GetGenericParameterEventHandler (string key)
				{
						if (!this._genericEvents.ContainsKey (key)) {
								this._genericEvents.Add (key, new GenericParameterEventDispatcher ());
						}
						return this._genericEvents [key];
				}

		}
}