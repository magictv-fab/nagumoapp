﻿using UnityEngine;
using System;
using System.Collections;
using MagicTV.globals;
using MagicTV.abstracts;
using MagicTV.vo;
using System.Linq;
using System.Collections.Generic;
using MagicTV.utils;
using ARM.utils.request;

using ARM.utils.io;

/// <summary>
/// In app audio presentation.
/// 
/// @version : 1.0
/// @author : Renato Seiji Miawaki
/// 
/// 
/// </summary>
/// 
using ARM.utils.cron;

using InApp;
namespace MagicTV.in_apps
{
	public class InAppAudioPresentation :  InAppActorAbstract
	{


		public const string AUDIO_TYPE = "audio.wav";

		ARMSingleRequest _audioRequest;
		AudioSource _audioSource;
		bool _audioComponentIsReady = false;
		UrlInfoVO _audioUrlInfoVO;
		bool _doPrepareAlreadyCalled = false;
		public string debugAudioUrl;
		

		


		void Start ()
		{
			startAudioSourceComponent ();
		}
		/// <summary>
		/// Run this app. Show and play if needs...
		/// Verifica se já fez download do conteúdo, caso tenha o bundle, se não tiver, faz agora
		/// </summary>
		public override void DoRun ()
		{

			if (!_isComponentReady) {
				this.AddCompoentIsReadyEventHandler (Run);
				this.prepare ();
				return;
			}
			if (_audioSource == null) {
				return;
			}

			ActorRun ();

		}

		void startAudioSourceComponent ()
		{
			if (this._audioSource == null) {
				this._audioSource = this.gameObject.AddComponent<AudioSource> ();
				this._audioSource.playOnAwake = false;
			}
		}

		public override void SetBundleVO (BundleVO bundle)
		{
			this._vo = bundle;
			base.SetBundleVO (bundle);
			foreach (FileVO fVo in this._vo.files) {
				//verifica se é do tipo audio
				if (fVo.type == AUDIO_TYPE) {
					_audioUrlInfoVO = BundleHelper.getSingleURLInfo (fVo);
					debugAudioUrl = _audioUrlInfoVO.local_url;
				}

			}

            applyActorConfig(this._vo);
			
			startAudioSourceComponent ();
			this._audioSource.loop = _loop;

		}

		/// <summary>
		/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
		/// Depois de pronta, dispare o evento RaiseComponentIsReady
		/// </summary>
		public override void prepare ()
		{

			if (_doPrepareAlreadyCalled == true) {
				return;
			}
			_doPrepareAlreadyCalled = true;
			if (this._audioUrlInfoVO == null) {

				return;
			}
			prepareAudio ();
		}

        #region Audio component management

		void prepareAudio ()
		{

			if (this._audioUrlInfoVO == null) {
				this._audioComponentIsReady = true;
				return;
			}

			ARMRequestVO audioVo = new ARMRequestVO (url: "file://" + BundleHelper.GetPersistentPath (_audioUrlInfoVO.local_url));

			_audioRequest = ARMSingleRequest.GetNewInstance (audioVo);
			_audioRequest.Events.AddCompleteEventhandler (onAudioComplete);

			_audioRequest.Load ();
		}
		public float DebugTimeAudio;
		public float DebugCurrentTimeAudio;
		void Update ()
		{
			if (this._isComponentReady) {
				DebugTimeAudio = this._audioSource.time;
				DebugCurrentTimeAudio = this._audioSource.clip.length;
			}
		}


		void onAudioComplete (ARMRequestVO vo)
		{
			startAudioSourceComponent ();
			//audio = this.gameObject.GetComponent<AudioSource> ();
			_audioSource.clip = vo.Request.GetAudioClip (false);

			RaiseComponentIsReady ();
		}
        #endregion

		public override void DoStop ()
		{
			if (_audioSource == null) {
				return;
			}
            ActorStop();

        }

		public override void Dispose ()
		{
			GameObject.DestroyImmediate (this.gameObject);
		}
		public override string GetInAppName ()
		{
			return "InAppAudioPresentation";
		}


        #region InAppActorInterface


		public override void Play ()
		{

			_audioSource.Play ();
		}
		public override void Pause ()
		{
			_audioSource.Pause ();
		}
		public override void RewindAndPause ()
		{

			_audioSource.Stop ();
		}

		public override void RewindAndPlay ()
		{

			RewindAndPause ();
			Play ();
		}
		public override void Hide ()
        {
            _audioSource.volume = 0f;
            return;
		}

		public override void Show ()
        {
			_audioSource.volume = 1f;
			return;
		}


		// CustomAction inicialmente para
		// AssetBundle - ir p/ animacao especifica.  - setar variáveis do animator controller
		// Audio - PlauOneShot 
		public override void CustomAction (string action)
		{
			Debug.Log ("CustomAction(string action)" + action);
			if (action == "PlayOneShot") {
				_audioSource.PlayOneShot (_audioSource.clip, 1);
                return;
            }

            string[] actionVars = action.Split('|');

            if (actionVars[0] == "PlayOneShot") {
                _audioSource.PlayOneShot(_audioSource.clip, float.Parse(actionVars[1]));
                return;
            }
		}
        #endregion

	}
}
