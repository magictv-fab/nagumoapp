using System.Collections.Generic;
using UnityEngine;
using ARM.components;
using ARM.display;
using ARM.utils.request;
using MagicTV.abstracts;
using MagicTV.utils;
using MagicTV.vo;
using ARM.utils.cron;
using ARM.utils;
using InApp;
using MagicTV.abstracts.screens;
using System;
using MagicTV.processes;


namespace MagicTV.in_apps
{
	public class InAppAssetBundlePresentation : InAppActorAbstract
	{

		protected PlayMakerFSM _FSM;

		// referência de AssetBundle para reutilizar na memória - necessário para não dar erros
		protected static Dictionary<string, AssetBundle> _AssetBundleLib;
		//  public BundleVO _vo;

		private ARMSingleRequest _requestModel3D;
		private string _localFileSystemAssetPath;

		protected string _externalLink;
		protected int _delayToOpenLink = 0;
		bool _doPrepareAlreadyCalled = false;

		protected GameObject _loadedModel3d;


		void Start ()
		{
			_FSM = gameObject.GetComponent<PlayMakerFSM> ();

		}


		/// <summary>
		/// [1] Método chamado pela InAppManager.
		/// Sets the bundle VO.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		public override void SetBundleVO (BundleVO bundle)
		{
			base.SetBundleVO (bundle);
			if (_AssetBundleLib == null) {
				_AssetBundleLib = new Dictionary<string, AssetBundle> ();
			}

			this._vo = bundle;
			foreach (FileVO fVo in this._vo.files) {
				if (fVo.type == "assetbundle.unity3d") {
					this._localFileSystemAssetPath = BundleHelper.GetPersistentPath (BundleHelper.GetFileDownloadPath (_vo, BundleHelper.getSingleURLInfo (fVo)));
				}
			}
			applyActorConfig (bundle);
			transformFromVO (bundle);
		}


		/// <summary> 3
		/// Runs ARM3DSimpleco0ntroller logo após o componente estar ready.
		/// </summary>
		void prepareModel3D (string assetPath)
		{

			ARMRequestVO _model3D_requestVO = new ARMRequestVO ("file://" + assetPath);
			//						_model3D_requestVO.UseUnityCache = true;
			//						_model3D_requestVO.UnityCacheVersion = ;

			AssetBundle assetToInstantiate;

			if (_AssetBundleLib.TryGetValue (ARM.utils.io.ARMFileManager.GetGetFileName (_model3D_requestVO.Url), out assetToInstantiate)) {
				// não sei como nem pq pode vir null;
				if (assetToInstantiate != null) {
					instantiateLoadedAssetBundle (assetToInstantiate);

					return;
				}
			}

			this._requestModel3D = ARMSingleRequest.GetNewInstance (_model3D_requestVO);
			this._requestModel3D.Events.AddCompleteEventhandler (onAssetBundleLoaded);
			this._requestModel3D.Load ();
		}

		void onAssetBundleLoaded (ARMRequestVO vo)
		{
			AssetBundle model_asset = vo.Request.assetBundle;
			Debug.LogWarning (vo.Url);
			_AssetBundleLib.Add (ARM.utils.io.ARMFileManager.GetGetFileName (vo.Url), vo.Request.assetBundle);
			instantiateLoadedAssetBundle (vo.Request.assetBundle);
//			LoadScreen.Instance.hide ();


			vo.Request.Dispose ();
			Caching.CleanCache ();

		}



		void instantiateLoadedAssetBundle (AssetBundle model_asset)
		{
			try {
				_loadedModel3d = (GameObject)Instantiate (model_asset.mainAsset);
			} catch (Exception e) {
//				Debug.LogError(""Atenção", "Por favor, atualize sua apresentação.", "ok")
				MobileNativeMessage dialog = new MobileNativeMessage ("Atenção", "Apresentação não compatível com sua versão do aplicativo.", "OK") ;
				PresentationController.Instance.StopLastPresentation() ;
				return;

			}
			this.checkLink (_loadedModel3d);
			//seta o model 3d

			//transforma a escala
			var originalScale = _loadedModel3d.transform.localScale;
			var originalPosition = _loadedModel3d.transform.localPosition;
			var originalRotation = _loadedModel3d.transform.localRotation;
			_loadedModel3d.transform.parent = transform;
			_loadedModel3d.transform.localScale = originalScale;
			_loadedModel3d.transform.localRotation = originalRotation;
			_loadedModel3d.transform.localPosition = originalPosition;


			configInternalApps ();


			Hide ();
			//parece estranho, mas ele veirica se o outro metodo já setou como ready
			//isso acontece quando não existe componentes a serem preparados
			if (this._isComponentReady) {
				//Debug.LogError("ESTE BABACA fez merda");
				//this.RaiseComponentIsReady ();
			}
		}

		bool _FSMIsReady;

		public void FSMIsReady ()
		{
			_FSMIsReady = true;
			Debug.LogWarning (" FSM is REady");
			if (!_hasInternalApps || (_hasInternalApps && _InternalAppsAreReady)) {
				Debug.LogWarning (" Component is Ready");
				EasyTimer.SetInterval (RaiseComponentIsReady, 500);
			}
            
		}

		bool _hasInternalApps;
		bool _InternalAppsAreReady;

		void InternalAppsReady ()
		{
			Debug.LogWarning (" InternalAppsReady");
			_InternalAppsAreReady = true;
			if (_FSMIsReady) {
				EasyTimer.SetInterval (RaiseComponentIsReady, 500);
			}
		}

		void checkLink (GameObject model3d)
		{
			if (this._vo != null && this._vo.metadata != null) {
				//verifica se tem o metadata de link externo
				for (int i = 0; i < this._vo.metadata.Length; i++) {
					MetadataVO metaVO = this._vo.metadata [i];
					if (metaVO.key == "external_link") {
						this.setExternalTo (metaVO.value, model3d);
						continue;
					}
					if (metaVO.key == "external_link_delay") {
						int val = int.Parse (metaVO.value);
						if (val > 0) {
							_delayToOpenLink = val;
						}
						continue;
					}
				}
			}
		}

		void setExternalTo (string value, GameObject model3D)
		{
			if (value == "" || value == null) {
				return;
			}
			this._externalLink = value;

			Collider[] collidersMain = model3D.GetComponents<Collider> ();
			Collider[] colliders = model3D.GetComponentsInChildren<Collider> ();
			if (colliders.Length == 0) {
				_hasClick = false;
				return;
			}
			this._clickGOInstanceID = new List<int> ();
			for (int i = 0; i < colliders.Length; i++) {
				this._clickGOInstanceID.Add (colliders [i].gameObject.GetInstanceID ());
			}
			for (int i = 0; i < collidersMain.Length; i++) {
				this._clickGOInstanceID.Add (collidersMain [i].gameObject.GetInstanceID ());
			}

			_hasClick = true;

		}
		void openLink ()
		{
			if (this._analytics != null) {
				this._analytics.CustomEvent ("link_press", this._externalLink);
			}
			if (this._externalLink != null) {
				if (this._delayToOpenLink > 0) {

					EasyTimer.SetTimeout (this.doOpenLink, this._delayToOpenLink);
					return;
				}
				doOpenLink ();
				return;
			}
		}

		void doOpenLink ()
		{
			if (this._analytics != null) {
				this._analytics.CustomEvent ("link_open", this._externalLink);
			}
			Application.OpenURL (this._externalLink);
		}



		void LateUpdate ()
		{
			checkClick ();

		}

		List<int> _clickGOInstanceID;

		bool _hasClick;

		bool _clickLocked;

		void UnlockClick ()
		{
			_clickLocked = false;
		}

		void checkClick ()
		{
			if (!_hasClick || _clickLocked) {
				return;
			}
			//		Debug.LogWarning (Input.GetMouseButton (0));

			if (Input.touchCount == 0 && Input.GetMouseButton (0) == false) {
				//						_canDrag = false;
				return;
			}
			RaycastHit hit;
			Ray ray = getRay ();
			if (Physics.Raycast (ray, out hit)) {
				Debug.DrawLine (ray.origin, hit.point);
				if (this._clickGOInstanceID.IndexOf (hit.collider.gameObject.GetInstanceID ()) >= 0) {

					openLink ();

					_clickLocked = true;
					//se tiver delay para abrir link espera mais também para habilitar o botao
					EasyTimer.SetTimeout (UnlockClick, Mathf.Max (3000, _delayToOpenLink));

				}
			}
			return;

		}

		Ray getRay ()
		{
			if (Input.touchCount == 1) {
				return Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
			}
			return Camera.main.ScreenPointToRay (Input.mousePosition);
		}


		/// <summary>
		/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
		/// Depois de pronta, dispare o evento RaiseComponentIsReady
		/// </summary>
		public override void prepare ()
		{
			if (_doPrepareAlreadyCalled == true) {
				return;
			}
			_doPrepareAlreadyCalled = true;
			if (string.IsNullOrEmpty (_localFileSystemAssetPath) || !System.IO.File.Exists (_localFileSystemAssetPath)) {
				Debug.LogError ("Caminho do asset 3D não existe.");
				return;
			}
//			EasyTimer.SetTimeout (LoadScreenCircle.Instance.HideNow, 1000);
			this.prepareModel3D (_localFileSystemAssetPath);
		}

		/// <summary>
		/// Runs ARM3DSimplecontroller logo após o componente estar ready.
		/// </summary>
		public override void DoRun ()
		{

			Debug.Log ("AssetBundle RUN");
			ActorRun ();


		}

		public override void DoStop ()
		{
			Debug.Log ("AssetBundle STOP");
			ActorStop ();
		}

		public override void Dispose ()
		{

			foreach (KeyValuePair<string, AssetBundle> entry in _AssetBundleLib) {
				// do something with entry.Value or entry.Key
				entry.Value.Unload (true);

			}
			_AssetBundleLib.Clear ();
			//GameObject.DestroyImmediate (this._model3D.gameObject);
			//this._model3D.Dispose ();
			GameObject.DestroyImmediate (this.gameObject);
		}

		public override string GetInAppName ()
		{
			return "InAppAssetBundlePresentation";
		}


        #region InAppActorInterface


		public override void Play ()
		{
			Debug.Log ("AssetBundle Play");
			Show ();
			if (subInternalInApps != null) {
				subInternalInApps.Run ();
			}
			if (_FSM != null) {
				_FSM.SendEvent ("OnPlay");
			}
		}
		public override void Pause ()
		{
			Debug.Log ("AssetBundle Pause");
			if (_FSM != null) {
				_FSM.SendEvent ("OnPause");
			}
		}
		public override void RewindAndPause ()
		{
			Debug.Log ("AssetBundle Rewind");
			if (_FSM != null) {
				_FSM.SendEvent ("OnRewind");
			}
		}

		public override void RewindAndPlay ()
		{
			Debug.Log ("AssetBundle Rewind+Play");
			if (_FSM != null) {
				_FSM.SendEvent ("OnRewind");
			}
			Play ();
		}
		public override void Hide ()
		{
			Debug.Log ("AssetBundle HIDE");
			if (_FSM != null) {
				_FSM.SendEvent ("OnHide");
			}
			

			SimpleView.Hide (this.gameObject);
			if (subInternalInApps == null)
				return;
			subInternalInApps.Stop ();
			//  SimpleView.setChildrenOnOff(false, this.gameObject);
		}

		public override void Show ()
		{
			Debug.Log ("AssetBundle SHOW");
			if (_FSM != null) {
				_FSM.SendEvent ("OnShow");
			}
			SimpleView.Show (this.gameObject);
			//  SimpleView.setChildrenOnOff(true, this.gameObject);
		}

		GroupInAppAbstractComponentsManager subInternalInApps;
		public InAppAbstract[] DebubInternalInApps;
		void configInternalApps ()
		{

			if (_loadedModel3d == null) {
				//				this._isComponentReady = true;
				return;
			}
			//se tiver um vuforia video presentation internamente, entao seta o assetbundle
			InAppAbstract[] inApps = _loadedModel3d.GetComponentsInChildren<InAppAbstract> ();
			DebubInternalInApps = inApps;
			if (inApps == null || !(inApps.Length > 0)) {
                
				return;
			}

			_hasInternalApps = true;

			var go = new GameObject (this.GetInAppName () + "_itens");
			//go.transform.parent = this.transform;
			this.subInternalInApps = go.AddComponent<GroupInAppAbstractComponentsManager> ();
			this.subInternalInApps.keepGameObjectHierarchy = true;
			this.subInternalInApps.SetComponents (inApps);

			BundleVO voForInApps = new BundleVO ();

			voForInApps.metadata = new MetadataVO[3];
			voForInApps.metadata [0] = new MetadataVO { key = "PlayOnRun", value = "False" };
			voForInApps.metadata [1] = new MetadataVO { key = "ResetOnStop", value = "False" };
			voForInApps.metadata [2] = new MetadataVO { key = "HideOnStop", value = "False" };

			voForInApps.files = _vo.files;

			this.subInternalInApps.AddCompoentIsReadyEventHandler (InternalAppsReady);
			
			this.subInternalInApps.SetBundleVO (voForInApps);
			//			this.subInternalInApps.AddCompoentIsReadyEventHandler (this.RaiseComponentIsReady);
			//			this.subInternalInApps.AddQuitEventHandler (this.RaiseQuit);
			this.subInternalInApps.prepare ();

			
		}


		// CustomAction inicialmente para
		// AssetBundle - ir p/ animacao especifica.  - setar variáveis do animator controller
		// Audio - PlauOneShot 
		public override void CustomAction (string action)
		{

		}

		void OnDestroy ()
		{
			if (subInternalInApps == null) {
				return;
			}
			GameObject.Destroy (subInternalInApps.gameObject);
		}
        #endregion
	}
}
