﻿using UnityEngine;
using System.Collections;

public class ClickableBehaviour : MonoBehaviour
{
		public string _externalLink;
		public float _unlockClickTimer ;
		bool _hasClick;
		bool _clickLocked;
		void Start ()
		{
				if (_externalLink != null) {
						_hasClick = true;
						if (_unlockClickTimer == null || _unlockClickTimer <= 0f) {
								_unlockClickTimer = 3f;
						}
				}
		}

		void OpenLink ()
		{
				if (_externalLink != null) {
						Application.OpenURL (_externalLink);
				}
		}

		void checkClick ()
		{
				if (!_hasClick || _clickLocked) {
						return;				
				}
				//		Debug.LogWarning (Input.GetMouseButton (0));
		
				if (Input.touchCount == 0 && Input.GetMouseButton (0) == false) {
						//						_canDrag = false;
						return;		
				}
				RaycastHit hit;
				Ray ray = getRay ();
				if (Physics.Raycast (ray, out hit)) {
						Debug.DrawLine (ray.origin, hit.point);
						if (gameObject.GetInstanceID () == hit.collider.gameObject.GetInstanceID ()) {
				
								OpenLink ();
				
								_clickLocked = true;
								Invoke ("UnlockClick", _unlockClickTimer);
								
				
						}
				}
				return;
		
		}

		void UnlockClick ()
		{
				_clickLocked = false;
		}
		Ray getRay ()
		{
				if (Input.touchCount == 1) {
						return Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				}
				return Camera.main.ScreenPointToRay (Input.mousePosition);
		}

		void LateUpdate ()
		{
				checkClick ();
		
		}

}
