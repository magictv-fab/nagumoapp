﻿using UnityEngine;
using MagicTV.abstracts;
using MagicTV.vo;
using MagicTV.utils;
using ARM.utils.io;

/// <summary>
/// In app video presentation.
/// 
/// @version : 1.0
/// @author : Renato Seiji Miawaki
/// 
/// 
/// Baixando o conteúdo dinamico
/// @version : 1.1
/// @author : Alan Lucian
/// 
/// 
/// Agora com prepare to run
/// @version : 1.2
/// @author : Renato Seiji Miawaki
/// 
/// </summary>
/// fref
using ARM.utils.cron;
using ARM.display;
using System;
using MagicTV.processes;

using MagicTV.utils.chroma.vuforia;
using MagicTV.utils.chroma.generic;
using MagicTV.globals.events;
using InApp;


namespace MagicTV.in_apps
{
	public class InAppVuforiaVideoPresentation : InAppActorAbstract
	{

		public const string VIDEO_TYPE_MP4 = "video.mp4";
		public const string VIDEO_TYPE_3GP = "video.3gp";
		public const string VIDEO_TYPE_M4V = "video.m4v";


		const float VIDEO_WEIGHT = 0.90f;


		protected Vector3 _originalScale;


		protected bool _canPrepare = false;

		protected bool _needPrepare = false;

		//public float 

		public ARMVideoPlaybackBehaviour _videoController;
		public GameObject VideoPlaneRender;
		bool _doPrepareAlreadyCalled = false;



		UrlInfoVO _videoUrlInfoVO;
		bool _videoComponentIsReady = false;
		const float _videoProgress = 0f;

		SimpleView SimpleView;

		bool _isDisposed;
		/// <summary>
		/// The loading flag.
		/// </summary>
		bool _loading;


		/// <summary>
		/// Luz direcional interna
		/// </summary>
		public Light luz;




		void Awake ()
		{
			getVideoComponent ();


		}

		void getVideoComponent ()
		{
			if (_videoController == null) {
				Debug.Log (" ! ! InAppVuforiaVideoPresentation 	getVideoComponent");
				_videoController = gameObject.GetComponentInChildren<ARMVideoPlaybackBehaviour> ();

			}
			_videoController.AddYouCanShowMe (Show);
		}

		public bool _hideOnStart = true;

		void Start ()
		{
			if (_hideOnStart) { 
				Hide ();
			}
		}

		/// <summary>
		/// Run this app. Show and play if needs...
		/// Verifica se já fez download do conteúdo, caso tenha o bundle, se não tiver, faz agora
		/// </summary>
		public override void DoRun ()
		{

//  #if UNITY_EDITOR
						
						
//  			if (this.luz != null) {
//  				this.luz.gameObject.SetActive (false);	
//  			}
//  			return;
//  #endif
			if (_isDisposed) {
				return;
			}
			if (!_isComponentReady) {
				if (this._doPrepareAlreadyCalled) {
					this.AddCompoentIsReadyEventHandler (Run);
				}
				//								this.prepare ();
				return;
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . Run 1 ");
			// SimpleView.Show (this.gameObject);
			if (this.luz != null) {
				this.luz.gameObject.SetActive (true);
			}
			//this.transform.localScale = new Vector3 (_originalScale.x, _originalScale.y, _originalScale.z);

			ActorRun ();

			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . Run 2 ");


		}




		public override void SetBundleVO (BundleVO bundle)
		{
			base.SetBundleVO (bundle);
			if (ARMVVPChromakeyGUIConfig.Instance != null) {
				ARMVVPChromakeyGUIConfig.Instance.bundleID = bundle.id;
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . SetBundleVO ");
			this._vo = bundle;

			foreach (FileVO fVo in this._vo.files) {


				//verifica se é do tipo vídeo
				if (fVo.type == VIDEO_TYPE_M4V || fVo.type == VIDEO_TYPE_MP4 || fVo.type == VIDEO_TYPE_3GP) {
					_videoUrlInfoVO = BundleHelper.getSingleURLInfo (fVo);
					break;
				}
			}

			if (_videoUrlInfoVO == null) {
				Debug.LogError (" InAppVuforiaVideoPresentation INVALID File Type OR File type NOT FOUND");
				return;
			}

			applyChromakey ();
			applyActorConfig (bundle);

           




			transformFromVO (this._vo);
			//verifica se precisa de luz, caso tenha luz atachada
			this.lightConfig ();

			VideoDebugEvents.AddDebugFloat1EventHandler (debugFloat1Change);
			VideoDebugEvents.AddDebugFloat2EventHandler (debugFloat2Change);
			VideoDebugEvents.AddDebugFloat3EventHandler (debugFloat3Change);
			VideoDebugEvents.AddDebugFloat4EventHandler (debugFloat4Change);
			//Setando que já tem bundle e por isso já consegue se preparar para fazer donwload?
			this._canPrepare = true;
			if (this._needPrepare) {
				doPrepare ();
			}

		}



		void lightConfig ()
		{
			if (this.luz != null) {
				string HasLight = BundleHelper.GetMetadataSingleValueByKey ("HasLight", this._vo.metadata);
				if (HasLight != null) {
					if (HasLight == "1") {
						return;
					}
					bool has = false;
					bool.TryParse (HasLight, out has);
					if (has) {
						return;
					}
				}
				this.luz.gameObject.SetActive (false);
				this.luz = null;
			}
		}

		/// <summary>
		/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
		/// Depois de pronta, dispare o evento RaiseComponentIsReady
		/// </summary>
		public override void prepare ()
		{

#if UNITY_EDITOR
						
			Hide ();
			this.RaiseComponentIsReady ();
			return;
#endif
			if (_doPrepareAlreadyCalled == true) {
				return;
			}
			this._needPrepare = true;
			if (!this._canPrepare) {
				//evita fazer 2x
				return;
			}
			Hide ();
			//_originalScale = new Vector3 (this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);

			doPrepare ();


		}


		void doPrepare ()
		{
			if (this._isDisposed) {
				Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . _isDisposed ");
				return;
			}
			if (_doPrepareAlreadyCalled == true) {
				Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . _doPrepareAlreadyCalled ");
				return;
			}
			if (this._loading) {
				return;
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . doPrepare 1 ");
			this._loading = true;
			_doPrepareAlreadyCalled = true;
			prepareVideo ();
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . doPrepare 2 ");


		}

        #region Video management
		void prepareVideo ()
		{
			if (this._isDisposed) {
				Debug.Log ("initVuforiaVideoComponent Video  ~~~~ IS DISPOSED");
				return;
			}
			//Fluxo para donwload fora do inApp
			initVideoComponent ();
		}


		//id do loop
		protected int _timer;
		void initVideoComponent ()
		{

			if (this._isDisposed) {
				Debug.Log ("initVuforiaVideoComponent Video  ~~~~ IS DISPOSED");
				return;
			}
			Debug.Log ("initVuforiaVideoComponent Video  ~~~~ " + BundleHelper.GetPersistentPath (_videoUrlInfoVO.local_url));
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . initVideoComponent ");





			// o caminho do arivo usado é relativo dentro da streaming assets
			_videoController.m_path = BundleHelper.GetPersistentPath (_videoUrlInfoVO.local_url);
			Debug.Log (string.Format ("~~~{0} --- {1}", _videoController.m_path, ARMFileManager.Exists (_videoController.m_path)));

			//seta o valor passado em loop
			if (!_videoController.loop) {
				_videoController.loop = _loop;
			}



			if (!ARMFileManager.Exists (_videoController.m_path)) {
				return;
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . ARMFileManager.Exitst ");
			_videoController.Init ();

			_videoController.AddOnReady (setVideoComponentActivated);


			if (_autoHideOnStop && _loop == false) {
				_videoController.onFinished += Hide;
			}



			_videoController.onFinished += onFinishHandler;


			if (_videoController.CurrentState == VideoPlayerHelper.MediaState.ERROR) {
				initVideoComponent ();
			}


		}



		void setVideoComponentActivated ()
		{
			if (_isDisposed) {
				return;
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . setVideoComponentActivated ");
			_videoComponentIsReady = true;
			checkComponentsPrepared ();
			transformFromVO (this._vo);
		}

		void debugFloat1Change (float p)
		{

			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VuforiaChromaKeyConfig.addSensibilidade (material, p);
				this.RaiseFloat1 ();
				return;
			}
		}

		void RaiseFloat1 ()
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VideoDebugEvents.RaiseChangeDebugFloat1 (VuforiaChromaKeyConfig.getSensibilidade (material));
				return;
			}
		}


		void debugFloat2Change (float p)
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VuforiaChromaKeyConfig.addRecorte (material, p);
				this.RaiseFloat2 ();
				return;
			}
		}

		void RaiseFloat2 ()
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VideoDebugEvents.RaiseChangeDebugFloat2 (VuforiaChromaKeyConfig.getRecorte (material));
				return;
			}
		}

		void debugFloat3Change (float p)
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VuforiaChromaKeyConfig.addSmooth (material, p);
				this.RaiseFloat3 ();
				return;
			}

		}

		void RaiseFloat3 ()
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VideoDebugEvents.RaiseChangeDebugFloat3 (VuforiaChromaKeyConfig.getSmooth (material));
				return;
			}
		}



		void debugFloat4Change (float p)
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VuforiaChromaKeyConfig.addSesibilidadeFina (material, p);
				this.RaiseFloat4 ();
				return;
			}

		}

		void RaiseFloat4 ()
		{
			Material material = VideoPlaneRender.GetComponent<Renderer> ().material;
			if (material_name == "VuforiaChromaKey") {
				VideoDebugEvents.RaiseChangeDebugFloat4 (VuforiaChromaKeyConfig.getSensibilidadeFina (material));
				return;
			}
		}

		public string material_name = "VuforiaChromaKey";
		void applyChromakey ()
		{

			if (this._isDisposed) {
				return;
			}

			string chroma_color = BundleHelper.GetMetadataSingleValueByKey ("ChromaColor", this._vo.metadata);
			if (chroma_color == null) {

				return;
			}

			Debug.Log ("ApplyChromakey ");
			//seta o tipo de Shader default caso nenhum outro seja setado

			//verifica se foi setado algum outro tipo de shader
			string chromaShader = utils.BundleHelper.GetMetadataSingleValueByKey ("ChromaShader", this._vo.metadata);


			if (chromaShader != null) {
				material_name = chromaShader;

			}
			Debug.Log ("VIDEO_CROMA_ --> " + material_name);
			Material chroma_material = (Material)Resources.Load (material_name, typeof(Material));
			if (chroma_material == null) {
				//				NGUIDebug.Log ("  - - - - -  NAO ACHOU O material " + material_name);
				Debug.LogError (" chroma_material NOT FOUND Error ");
				VideoDebugEvents.RaiseDebugShader ("error");
				return;
			}
			VideoDebugEvents.RaiseDebugShader (material_name);
			//verifica se é de algum tipo já conhecido e especial

			VideoPlaneRender.GetComponent<Renderer> ().material = chroma_material;

			if (material_name == "VuforiaChromaKey") {
				//apenas para esse tipo de chroma key da pra configurar assim
				VuforiaChromaKeyConfig.configChroma (VideoPlaneRender.GetComponent<Renderer> ().material, this._vo.metadata);
				this.RaiseFloat1 ();
				this.RaiseFloat2 ();
				this.RaiseFloat3 ();
				this.RaiseFloat4 ();
				return;

			}
			//daqui pra baixo ele setou o chroma de algum tipo generico
			//agora questão de configuração
			string chromaConfigJson = utils.BundleHelper.GetMetadataSingleValueByKey ("ChromaConfigJson", this._vo.metadata);
			if (chromaConfigJson != null) {
				//chroma config
				GenericChromaKeyConfig.configChroma (VideoPlaneRender.GetComponent<Renderer> ().material, this._vo.metadata, chromaConfigJson);
				this.RaiseFloat1 ();
				this.RaiseFloat2 ();
				return;
			}

		}

        #endregion

		void checkComponentsPrepared ()
		{
			if (this._isDisposed) {
				return;
			}
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . checkComponentsPrepared 1 ");
			if (_videoComponentIsReady) {

				this._loading = false;
				Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . checkComponentsPrepared 2 ");
				RaiseComponentIsReady ();
				return;

			}
		}



		public override void DoStop ()
		{


			if (this._isDisposed) {
				return;
			}
			ActorStop ();

		}

		public override void Dispose ()
		{
			if (this._isDisposed) {
				return;
			}
			VideoDebugEvents.RemoveDebugFloat1EventHandler (debugFloat1Change);
			VideoDebugEvents.RemoveDebugFloat2EventHandler (debugFloat2Change);
			if (this._timer >= 0) {
				EasyTimer.ClearInterval (this._timer);
				this._timer = -1;
			}
			//						this.RemoveCompoentIsReadyEventHandler (this.Dispose);
			this._isDisposed = true;
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . Dispose 2 ");
			GameObject.Destroy (this.gameObject);
			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") InAppVuforiaVideoPresentation . Dispose 3 ");
		}
		public override string GetInAppName ()
		{
			return "InAppVuforiaVideoPresentation";
		}

		void onFinishHandler ()
		{
			AnaltyticsOnFinish ();
		}



        #region InAppActorInterface


		public override void Play ()
		{
			Show ();
#if UNITY_EDITOR
			Debug.LogWarning ("Vuforia Video Play  " + gameObject.name);
			return;
#endif
			_videoController.Play ();
			AnaltyticsOnPlay ();
		}
		public override void Pause ()
		{
#if UNITY_EDITOR
			Debug.LogWarning ("Vuforia Video Pause  " + gameObject.name);
			return;
#endif
			_videoController.Pause ();
			AnaltyticsOnPause ();
		}
		public override void RewindAndPause ()
		{
#if UNITY_EDITOR
			Debug.LogWarning ("Vuforia Video RewindAndPause  " + gameObject.name);
			return;
#endif
			_videoController.Stop ();
		}

		public override void RewindAndPlay ()
		{


#if UNITY_EDITOR
			Debug.LogWarning ("Vuforia Video RewindAndPlay  ");
			return;
#endif
			if (_videoController.isPlaying ()) { 
				_videoController.Stop ();
			}
			Play ();
		}
		public override void Hide ()
		{

			Debug.LogWarning ("Vuforia Video Hide  ");
			SimpleView.Hide (this.gameObject);
			if (this.luz != null) {
				this.luz.gameObject.SetActive (false);
			}
			//this.transform.localScale = new Vector3 (0, 0, 0);
		}

		public override void Show ()
		{
			Debug.LogWarning ("Vuforia Video Show  ");
			_hideOnStart = false;
			SimpleView.Show (this.gameObject);
			if (this.luz != null) {
				this.luz.gameObject.SetActive (true);
			}

		}


		// CustomAction inicialmente para
		// AssetBundle - ir p/ animacao especifica.  - setar variáveis do animator controller
		// Audio - PlauOneShot 
		public override void CustomAction (string action)
		{
			Debug.LogWarning ("Vuforia Video CustomAction  " + action);
		}
        #endregion



        #region analytics
        // o PAUSE é chamado mais que o play, no analytics isso fica evidente
        bool _pauseLock = true;
		void AnaltyticsOnFinish ()
		{
			if (_analytics == null) {
				return;
			}
			_analytics.CustomEvent (GetAnalyticsCategory (), "End");
		}
		void AnaltyticsOnPause ()
		{
			if (_analytics == null || _pauseLock) {
				return;
			}
			_pauseLock = true ;
			_analytics.CustomEvent (GetAnalyticsCategory (), "Pause");
		}
		void AnaltyticsOnPlay ()
		{
			_pauseLock = false;
			if (_analytics == null) {
				return;
			}
			_analytics.CustomEvent (GetAnalyticsCategory (), "Play");
		}
		protected override string GetAnalyticsCategory ()
		{
			return "Video";
		}
        #endregion

	}
}
