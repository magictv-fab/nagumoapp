using UnityEngine;
using MagicTV.globals;
using MagicTV.globals.events;
using MagicTV.vo;
using System.Linq;
using MagicTV.download;
using MagicTV.abstracts.screens;
using ARM.utils.cron;
using MagicTV.abstracts;

namespace MagicTV.in_apps
{
	/// <summary>
	/// In app On Demand Download manager.
	///
	/// Classe ResponsÃ¡vel por fazer o download em tempo real.
	/// @author     : Marcelo Zani
	/// @version    : 1.3
	///
	/// </summary>
	public class InAppOnDemandDownloadManager
	{
		private BundleVO[] _bundles;
		public InAppOnDemandDownloadManager (BundleVO[] bundles)
		{
			_bundles = bundles;
		}

		public bool HasFileToDownload ()
		{
			ARMDebug.LogTime ("HasFileToDownload . INIT");

			var result = false;
			foreach (BundleVO bundle in _bundles) {

				foreach (FileVO file in bundle.files) {
					ARMDebug.LogTime ("HasFileToDownload ....? ");

					UrlInfoVO filteredByQualityUrl = BenchmarkAbstract.Instance.GetCurrentQuality ().FilterFilesByQuality (file);
					if (filteredByQualityUrl != null && filteredByQualityUrl.status == MagicTV.vo.UrlInfoConstants.STATUS_FILE_WAITING) {
						result = true;
//						Debug.LogWarning ("InAppOnDemandDownloadManager : HasFileToDownload");
						break;
					}						
				}
//				}

				//faz o loop em files
				//para cada file, passa no metodo FilterFilesByQuality
				//e ai sim ve o url se ele esta status == MagicTV.vo.UrlInfoConstants.STATUS_FILE_WAITING
//				if (bundle.files.Any (f => f.urls.Any (u => u.status == MagicTV.vo.UrlInfoConstants.STATUS_FILE_WAITING))) {
//					result = true;
//					break;
//				}
			}
			ARMDebug.LogTime ("HasFileToDownload DONE !!  result:  " + result);
			return result;
		}

		public void Download ()
		{
			if (!HasFileToDownload ())
				return;
			AppRootEvents.GetInstance ().GetAR ().AddChangeARToggleHandler (VuforiaChangeState);
			AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOff ();
		}

		protected static MagicTVAbstractWindowConfirmControllerScript confirm;

		protected void CreateConfirmWindow ()
		{
			//acesso modificado para donwload sem confirmação
			return;
			if (confirm != null) {
								
				return;
			}
						
			confirm = WindowControlListScript.Instance.InstantiateConfirmControllerScript ();

			confirm.SetButtonOkText ("Sim");
			confirm.SetButtonCancelText ("Não");
			confirm.SetMessageText (string.Format ("O contetúdo a ser exibido nescessita ser baixado da internet, deseja prosseguir?\r\nTamanho do download: {0:0.00} Mb", GetDownloadSize ()));
			confirm.SetTitleText ("Download de Conteúdo");
			confirm.OnClickOk += WindowConfirmSuccess;
			confirm.OnClickCancel += WindowConfirmFailed;
			WindowControlListScript.Instance.AddPopUpToQueue (confirm.gameObject);
		}

		protected float GetDownloadSize ()
		{
			float bundleSize = 0;

			foreach (BundleVO bundle in _bundles) {
				foreach (FileVO file in bundle.files) {
					foreach (UrlInfoVO url in file.urls) {
						bundleSize += url.size;
					}
				}
			}

			return bundleSize / 1024 / 1024;
		}

		protected void WindowConfirmSuccess ()
		{
			try {
				AppRootEvents.GetInstance ().GetAR ().RemoveChangeARToggleHandler (VuforiaChangeState);
				AppRoot.canTrack = false;
                LoadScreenCircle.Instance.setPercentLoaded(0f);
                LoadScreenCircle.Instance.show ();
//				ScreenEvents.GetInstance ().RaiseChangeToPerspective (Perspective.PROGRESS_WITH_CANCEL);
                LoadScreenCircle.Instance.AddLoadCanceledEventHandler (DownloadCanceled);
				DeviceCacheFileManager.Instance.AddCompleteEventhandler (DownloadComplete);
				DeviceCacheFileManager.Instance.AddProgressEventhandler (DownloadProgress);
				DeviceCacheFileManager.Instance.DoStartCacheDownload (_bundles);
			} finally {
//								AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOn ();
			}
		}

		protected void WindowConfirmFailed ()
		{
			DownloadComplete ();
		}

		bool _started;

		void DoStartDownload ()
		{
			if (!this._started) {
				this._started = true;
				WindowConfirmSuccess ();
			}
		}

		public void VuforiaChangeState (bool active)
		{
			AppRootEvents.GetInstance ().GetAR ().RemoveChangeARToggleHandler (VuforiaChangeState);
			if (!active) {
//								if (AppRoot.connection != AppRoot.CONNECTION_TYPE_WIFI) {
//										CreateConfirmWindow ();
//										return;
//								}
								
				DoStartDownload ();
			}
		}

		void DownloadProgress (float progress)
		{
			LoadScreenCircle.Instance.setPercentLoaded (progress);
		}

		void DownloadCanceled ()
		{
			//por enquanto finge que nao ta mais baixando
			Debug.LogError (" Cancenlar ");
			AppRoot.canTrack = false;
			DeviceCacheFileManager.Instance.Cancel ();
			DownloadComplete ();
		}

//				void DownloadError (string message)
//				{
//						removeListeners ();
//
//				}

		public void DownloadComplete ()
		{
			removeListeners ();
						
			EasyTimer.SetTimeout (turnAROn, 100);

		}

		void turnAROn ()
		{
			EasyTimer.SetTimeout (LoadScreenCircle.Instance.HideNow, 1500);

            AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOn ();
			AppRoot.canTrack = true;
		}

		protected void removeListeners ()
		{
			AppRootEvents.GetInstance ().GetAR ().RemoveChangeARToggleHandler (VuforiaChangeState);
			DeviceCacheFileManager.Instance.RemoveCompleteEventhandler (DownloadComplete);
		}
	}
}