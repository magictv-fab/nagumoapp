﻿using UnityEngine;
using System.Collections;
using MagicTV.globals;
using MagicTV.abstracts;
using MagicTV.vo;
using System.Linq;
using System.Collections.Generic;
using MagicTV.utils;
using ARM.utils.request;

using ARM.utils.io;

/// <summary>
/// In app audio presentation.
/// 
/// @version : 1.0
/// @author : Renato Seiji Miawaki
/// 
/// 
/// </summary>
/// 
using ARM.utils.cron;
using ARM.display;


namespace MagicTV.in_apps
{
		public class InAppImagePresentation : InAppAbstract
		{
				public const string IMAGE_TYPE = "image" ;
				bool _doPrepareAlreadyCalled = false ;
				UrlInfoVO _urlInfoVO ;
				public bool resetOnStop;
				bool _componentIsReady ;
				bool _useCache;
	
				public string externalUrl ;
				public bool _debugLoadNow;
				/// <summary>
				/// Run this app. Show and play if needs...
				/// Verifica se já fez download do conteúdo, caso tenha o bundle, se não tiver, faz agora
				/// </summary>
				public override void DoRun ()
				{
						if (!_isComponentReady) {
								this.AddCompoentIsReadyEventHandler (Run);
								this.prepare ();
								return;
						}
						SimpleView.Show (this.gameObject);
				}

				public override void SetBundleVO (BundleVO bundle)
				{
						this._vo = bundle;
			
						foreach (FileVO fVo in this._vo.files) {
								//verifica se é do tipo audio
								if (fVo.type == IMAGE_TYPE) {
										_urlInfoVO = BundleHelper.getSingleURLInfo (fVo);
								}
						}
						applyConfig (this._vo);
						transformFromVO (this._vo);
						
				}
				void applyConfig (BundleVO bundle)
				{
						string cache = utils.BundleHelper.GetMetadataSingleValueByKey ("cache", bundle.metadata);
						if (cache != null) {
								bool.TryParse (cache, out _useCache);
						}
						externalUrl = utils.BundleHelper.GetMetadataSingleValueByKey ("externalUrl", bundle.metadata);
				}
				public GameObject _plane ;
				/// <summary>
				/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
				/// Depois de pronta, dispare o evento RaiseComponentIsReady
				/// </summary>
				public override void prepare ()
				{
						SimpleView.Hide (this.gameObject);
						if (_doPrepareAlreadyCalled == true) {
								return;
						}
						Debug.LogError ("prepare");
						_doPrepareAlreadyCalled = true;
						
						prepareFile ();
				}

		#region Audio management
				ARMSingleRequest _fileRequest ;
				void prepareFile ()
				{
						string stringPath = this.externalUrl;
						
						if (_urlInfoVO != null) {
								if (this.externalUrl == null) {
										//se foi enviado o external url, ele da preferencia pro external, e faz uns replaces de sistem
										stringPath = _urlInfoVO.url;
								}
						}
						string tagPin = "{#pin}";
						stringPath = stringPath.Replace (tagPin, DeviceInfo.Instance.getPin ());
						Debug.LogError ("strinShowgPath : " + stringPath);
						ARMRequestVO requestVo = new ARMRequestVO (stringPath);
						_fileRequest = ARMSingleRequest.GetNewInstance (requestVo);
						_fileRequest.Events.AddCompleteEventhandler (onFileComplete);
						_fileRequest.Load ();
				}
				
				void Update ()
				{
						if (_debugLoadNow) {
								_debugLoadNow = false;
								ARMRequestVO requestVo = new ARMRequestVO (this.externalUrl);
								requestVo.UseUnityCache = this._useCache;
								_fileRequest = ARMSingleRequest.GetNewInstance (requestVo);
								_fileRequest.Events.AddCompleteEventhandler (onFileComplete);
								_fileRequest.Load ();
						}
				}
				void onFileComplete (ARMRequestVO vo)
				{
//						Texture2D texture = new Texture2D (1, 1);
						Texture2D t = new Texture2D (2, 2, TextureFormat.DXT1, false);
						this._plane.gameObject.GetComponent<Renderer>().material.mainTexture = t;
						vo.Request.LoadImageIntoTexture (t);
						RaiseComponentIsReady ();
				}
		#endregion

				public override void DoStop ()
				{
						if (this.resetOnStop) {
								return;
						}
						SimpleView.Hide (this.gameObject);
				}

				public override void Dispose ()
				{
						GameObject.DestroyImmediate (this.gameObject);
				}
				public override string GetInAppName ()
				{
						return "InAppImagePresentation";
				}
			
		}
}
