﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using ARM.animation;

/*
@author 	: Leal
@version 	: 1.0

Esta classe faz a ligação entre os eventos que vem em forma de metadado do tipo "Event"/"Nome do evento" e que sejam do tipo ON/OFF, 
sendo que todas classes acionadas (SimpleOnOffEvent) devem ter um método RESET

/// @author: Leandro Leal
/// @version: 1.2
/// Adicionado Dispatch de eventos com string
/// Agora as classes podem receber strings configuráveis. O primeiro termo da string deve ser o nome do evento a ser chamado e o restante a sua configuração
*/
namespace InApp
{
	public class InAppEventDispatcher : MonoBehaviour
	{
		protected Dictionary<EventNames, InAppEventDispatcherOnOff> eventDictionary;
		private static InAppEventDispatcher _instance;

		public InAppEventDispatcher ()
		{
			eventDictionary = new Dictionary<EventNames, InAppEventDispatcherOnOff> ();
			_instance = this;
		}
		public static InAppEventDispatcher GetInstance ()
		{
			return _instance;
		}

		// Liste aqui os eventos que vc pretende ligar e desligar
		public enum EventNames
		{
			FIXED_CAMERA, // Fixa a câmera
			FIXED_GROUP_OF_PRESENTATIONS, //Fixa um grupo de apresentações
			LIGHT, // Liga a luz
			DETACHED_FROM_TRACKING, // Este plugin torna a apresentação livre da perda do tracking
			LOCK_SCREEN_ORIENTATION_PORTRAIT, // trava a orientação da tela em portrait
			LOCK_SCREEN_ORIENTATION_LANDSCAPE, // trava a orientação da tela em portrait 
			GIROSCOPE_INCLINATION, // ativa a bussola
			GIROSCOPE_INCLINATION_CONFIGURABLE, // ativa e configura a bussola
			CAMERA_DISTANCE_CONFIG, // Configura o far e o near da camera atraves de metadado
			GENERIC, // Configura tudo que é Generico
			NOT_FOUND, // Quando não acha, esse eh o default
			RESET // Desliga todos os eventos	
		}
		public delegate void InAppEventDispatcherWithString (string metadado);
		protected InAppEventDispatcherWithString actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE;
		protected InAppEventDispatcherWithString actionReferenceFIXED_CAMERA;
		protected InAppEventDispatcherWithString actionReferenceDETACHED_FROM_TRACKING;
		protected InAppEventDispatcherWithString actionReferenceCAMERA_DISTANCE_CONFIG;
		protected InAppEventDispatcherWithString actionReferenceGENERIC;

		public delegate void InAppEventDispatcherOnOff ();

		protected InAppEventDispatcherOnOff actionReferenceFIXED_GROUP_OF_PRESENTATIONS;
		protected InAppEventDispatcherOnOff actionReferenceLIGHT;
		protected InAppEventDispatcherOnOff actionReferenceRESET;

		protected InAppEventDispatcherOnOff actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT;
		protected InAppEventDispatcherOnOff actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE;
		protected InAppEventDispatcherOnOff actionReferenceNOT_FOUND;
		protected InAppEventDispatcherOnOff actionReferenceGIROSCOPE_INCLINATION;

		// metodo que transforma string em enum
		public EventNames getEventNameByString (string eventName)
		{
			switch (eventName) {
			case "FixedCamera":
				return EventNames.FIXED_CAMERA;
			case "FixedPresentationGroup":
				return EventNames.FIXED_GROUP_OF_PRESENTATIONS;
			case "Light":
				return EventNames.LIGHT;
			case "DetachedFromTracking":
				return EventNames.DETACHED_FROM_TRACKING;
			case "LockScreeenOrientationPortrait":
				return EventNames.LOCK_SCREEN_ORIENTATION_PORTRAIT;
			case "LockScreenLandscape":
				return EventNames.LOCK_SCREEN_ORIENTATION_LANDSCAPE;
			case "Bussola":
				return EventNames.GIROSCOPE_INCLINATION;
			case "BussolaConfig":
				return EventNames.GIROSCOPE_INCLINATION_CONFIGURABLE;
			case "CameraDistance":
				return EventNames.CAMERA_DISTANCE_CONFIG;
			case "Generic":
				return EventNames.GENERIC;
			case "Reset":
				return EventNames.RESET;
			default:
				return EventNames.NOT_FOUND;
			}
		}
		public void AddEventListener (EventNames eventName, InAppEventDispatcherWithString action)
		{
			if (eventName == EventNames.GIROSCOPE_INCLINATION_CONFIGURABLE) {
				actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE += action;
			}
			if (eventName == EventNames.FIXED_CAMERA) {
				actionReferenceFIXED_CAMERA += action;
			}
			if (eventName == EventNames.DETACHED_FROM_TRACKING) {
				actionReferenceDETACHED_FROM_TRACKING += action;
			}
			if (eventName == EventNames.CAMERA_DISTANCE_CONFIG) {
				actionReferenceCAMERA_DISTANCE_CONFIG += action;
			}
			if (eventName == EventNames.GENERIC) {
				actionReferenceGENERIC += action;
			}
		}

		// Adicione aqui os próximos eventos que forem sendo criados
		public void AddEventListener (EventNames eventName, InAppEventDispatcherOnOff action)
		{

			if (eventName == EventNames.FIXED_GROUP_OF_PRESENTATIONS) {
				actionReferenceFIXED_GROUP_OF_PRESENTATIONS += action;
			}
			if (eventName == EventNames.LIGHT) {
				actionReferenceLIGHT += action;
			}


			if (eventName == EventNames.LOCK_SCREEN_ORIENTATION_LANDSCAPE) {
				actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE += action;
			}
			if (eventName == EventNames.LOCK_SCREEN_ORIENTATION_PORTRAIT) {
				actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT += action;
			}	
			if (eventName == EventNames.GIROSCOPE_INCLINATION) {
				actionReferenceGIROSCOPE_INCLINATION += action;
			}
		}

		public void raiseInAppEvent (EventNames eventName, string metadado)
		{
			
			if (eventName == EventNames.GIROSCOPE_INCLINATION_CONFIGURABLE) {
				if (actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE != null) {
					actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE (metadado);
				}
			}
			if (eventName == EventNames.FIXED_CAMERA) {
				if (actionReferenceFIXED_CAMERA != null) {
					actionReferenceFIXED_CAMERA (metadado);
				}
			}
			if (eventName == EventNames.DETACHED_FROM_TRACKING) {
				if (actionReferenceDETACHED_FROM_TRACKING != null) {
					actionReferenceDETACHED_FROM_TRACKING (metadado);
				}
			}
			if (eventName == EventNames.CAMERA_DISTANCE_CONFIG) {
				if (actionReferenceCAMERA_DISTANCE_CONFIG != null) {
					actionReferenceCAMERA_DISTANCE_CONFIG (metadado);
				}
			}
			if (eventName == EventNames.GENERIC) {
				if (actionReferenceGENERIC != null) {
					actionReferenceGENERIC (metadado);
				}
			}
		}

		public void raiseInAppEvent (EventNames eventName)
		{
			if (eventName == EventNames.FIXED_GROUP_OF_PRESENTATIONS) {
				if (actionReferenceFIXED_GROUP_OF_PRESENTATIONS != null) {
					actionReferenceFIXED_GROUP_OF_PRESENTATIONS ();
					
				}
			}
			if (eventName == EventNames.LIGHT) {
				if (actionReferenceLIGHT != null) {
					actionReferenceLIGHT ();
				}
			}		
			if (eventName == EventNames.LOCK_SCREEN_ORIENTATION_PORTRAIT) {
				if (actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT != null) {
					actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT ();
				}
			}
			if (eventName == EventNames.LOCK_SCREEN_ORIENTATION_LANDSCAPE) {
				if (actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE != null) {
					actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE ();
				}
			}
			if (eventName == EventNames.GIROSCOPE_INCLINATION) {
				if (actionReferenceGIROSCOPE_INCLINATION != null) {
					actionReferenceGIROSCOPE_INCLINATION ();
				}
			}

		}

		public void treatToRaiseEvents (List<string> metadatas)
		{
			char[] splitchar = { '|' };
			string[] metadataSplitted;
			string eventName;
			string metadata;

//			Debug.LogWarning ("InAppEventDispatcher : raiseCOMStringCOUNTTTT " + metadatas.Count ());

			foreach (string notTreatedData in metadatas) {
				metadataSplitted = notTreatedData.Split (splitchar);
				eventName = metadataSplitted [0];
				if (metadataSplitted.Count () == 1) {
					raiseInAppEvent (getEventNameByString (eventName));
					Debug.LogWarning (" InAppEventDispatcher : raiseSemString " + eventName);
					continue;
				}
				if (metadataSplitted.Count () > 1) {
					List<string> metadataListSplitted = metadataSplitted.ToList ();

					metadataListSplitted.RemoveAt (0);
					metadata = string.Join (new string (splitchar), metadataListSplitted.ToArray ());
					raiseInAppEvent (getEventNameByString (eventName), metadata);
					Debug.LogWarning (" InAppEventDispatcher : raiseCOMSTRING " + eventName + " metadata: " + metadata);
				}
			}
		}

		// Aqui é somente o RESET
		public void AddToResetList (InAppEventDispatcherOnOff action)
		{
			actionReferenceRESET += action;
		}

		public void raiseReset ()
		{
			if (actionReferenceRESET != null) {
				actionReferenceRESET ();
			}
		}
	}
}