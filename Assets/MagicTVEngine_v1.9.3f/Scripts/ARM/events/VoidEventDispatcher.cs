﻿using UnityEngine;
using System.Collections;


/// <summary>
/// VoidEventDispatcher
/// 
/// Disparador de eventos void
/// 
/// @author: Renato Seiji Miawaki
/// 
/// </summary>
namespace ARM.events
{
		public class VoidEventDispatcher
		{

				public delegate void VoidEvent ();
				protected VoidEvent _voidEvent ;
				public void AddOnVoidEventHandler (VoidEvent method)
				{
						this._voidEvent += method;
				}
				public void RemoveOnVoidEventHandler (VoidEvent method)
				{
						this._voidEvent -= method;
				}
				public void Raise ()
				{
						if (this._voidEvent == null) {
								return;
						}
						this._voidEvent ();
				}
		}
}