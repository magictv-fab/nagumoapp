﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Generic parameter event dispatcher.
/// 
/// @author: Renato Seiji Miawaki
/// 
/// </summary>
namespace ARM.events
{
		public class GenericParameterEventDispatcher
		{

				public delegate void GenericEvent (GenericParameterVO parameter);
				protected GenericEvent _genericEvent ;
				public void AddOnGenericEventHandler (GenericEvent method)
				{
						this._genericEvent += method;
				}
				public void RemoveOnGenericEventHandler (GenericEvent method)
				{
						this._genericEvent -= method;
				}
				public void Raise (GenericParameterVO parameter)
				{
						if (this._genericEvent == null) {
								return;
						}
						this._genericEvent (parameter);
				}
		}
}