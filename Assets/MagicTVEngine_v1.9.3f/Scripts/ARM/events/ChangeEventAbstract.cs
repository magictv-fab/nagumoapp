﻿using UnityEngine;
using System.Collections;
/**
 * @author Renato Seiji Miawaki
 * */
/// <summary>
/// Para toda e qualquer classe que pretenda disparar evento de mudanças passando um float de 0 a 1 simbolizando a % da mudança
/// </summary>
namespace ARM.events{

	public abstract class ChangeEventAbstract : MonoBehaviour {

		public delegate void OnChangeEventHandler( float percentChange ) ;

		protected OnChangeEventHandler onChangeEvent ;
		
		public void addChangeEventListener( OnChangeEventHandler eventHandler ){
			this.onChangeEvent += eventHandler ;
		}
		public void removeChangeEventListener( OnChangeEventHandler eventHandler ){
			this.onChangeEvent -= eventHandler ;
		}
		/// <summary>
		/// 0 a 1 float simboliznado a % de modificação
		/// </summary>
		/// <param name="percentChange">Percent change.</param>
		protected void RaiseChageEvent( float percentChange ){
			if( this.onChangeEvent != null ){
				this.onChangeEvent( percentChange ) ;
			}
		}
	}
}