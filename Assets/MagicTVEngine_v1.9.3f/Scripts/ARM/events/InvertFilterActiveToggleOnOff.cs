﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;
using ARM.transform.filters.abstracts;

/// <summary>
/// @author : Renato Seiji Miawaki
/// @version : 1.0
/// 
/// Quando um elemnto liga ele da evento de toggle de desligar
/// Invert filter active toggle on off.
/// </summary>
namespace ARM.events
{
	public class InvertFilterActiveToggleOnOff : ToggleOnOffAbstract
	{

		public bool ligado = false ;

		protected bool _lastValue = false ;
		protected bool _hasFilter ;
		public TransformPositionAngleFilterAbstract filterToInvertActive ;
		void Start ()
		{
			_hasFilter = (filterToInvertActive != null);
		}
		void Update ()
		{
			if (_hasFilter) {
				ligado = !(filterToInvertActive.byPass || !filterToInvertActive._active);
				if (ligado != _lastValue) {
					_lastValue = ligado;
					if (ligado) {
						Debug.LogWarning ("onTurnOffEvent");
						this.onTurnOffEvent ();
					} else {
						Debug.LogWarning ("onTurnOnEvent");
						this.onTurnOnEvent ();
					}
				}

			}
		}
	}
}
