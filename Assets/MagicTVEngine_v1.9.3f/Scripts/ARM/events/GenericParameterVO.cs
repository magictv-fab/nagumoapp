﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generic parameter VO
/// 
/// Para passar parametros genericos
/// 
/// @author: Renato Seiji Miawaki
/// 
/// </summary>
namespace ARM.events
{
		public class GenericParameterVO
		{
				public int id;
				public string type;

				public int intValue;
				public string stringValue;
				public float floatValue;

		}
}