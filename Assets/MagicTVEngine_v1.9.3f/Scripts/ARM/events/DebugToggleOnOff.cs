﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;

namespace ARM.events{
		public class DebugToggleOnOff : ToggleOnOffAbstract {

		public bool ligado = false;
		protected bool _lastValue = false;

		void Update(){
						if (ligado != _lastValue) {
								_lastValue = ligado;
								if (ligado) {
										this.onTurnOnEvent ();
								} else {
										this.onTurnOffEvent ();
								}
						}
		}
}
}
