﻿using UnityEngine;
using System.Collections;

using ARM.abstracts;
using System.Collections.Generic;

namespace ARM.events
{
	public abstract class BaseDelayToggleEventAbstract : ToggleOnOffAbstract, OnOffListenerEventInterface, ConfigurableListenerEventInterface
	{
		//EM SEGUNDOS
		public float delayToOn = 1f ;
		public float delayToOff = 1f ;
		public bool _active;

		/**
		 * ON 
		 * */
		private float _totalTimeElapsedOn = 0f ;
		private bool _wantToOn = false ;
		private bool _timerEnabledOn = false ;
		
		
		/**
		 * OFF 
		 * */
		private float _totalTimeElapsedOff = 0f ;
		private bool _wantToOff = false ;
		private bool _timerEnabledOff = false ;
		protected List<float> _inicialConfig = new List<float> ();


	
		public string getUniqueName ()
		{
			return this.gameObject.name;
		}
		public void turnOn ()
		{
			this._active = true;
		}
		public void turnOff ()
		{
			this._active = false;
		}
		public void ResetOnOff ()
		{
			this._active = true;
		}
		public bool isActive ()
		{
			return this._active = true;
		}
		public GameObject getGameObjectReference ()
		{
			return this.gameObject;
		}
		protected void _turnToOn ()
		{
			this._wantToOff = false;
			if (! (this.delayToOn > 0f)) {
				this.onTurnOnEvent ();
				return;
			}

			this._wantToOn = true;
			this._totalTimeElapsedOn = 0f; 
			
			return;
			
			
		}
		
		protected void _turnToOff ()
		{
			this._wantToOn = false;
			if (! (this.delayToOff > 0f)) {
				this.onTurnOffEvent ();
				return;
			}
			
			this._wantToOff = true;
			this._totalTimeElapsedOff = 0f; 
			
			return;
		}
		public void configByFloatList (List<float> config)
		{		
			//DelayOn|DelayToOn|DelayToOff
			if (config.Count < 1) {
				return;
			}
			if (config.Count > 0) {
				this.delayToOn = config [0];
			}
			if (config.Count > 1) {
				this.delayToOff = config [1];
			}
		}

		void Update ()
		{
			//is it active?
			if (!this._active) {
				return;
			}

			//ON
			this._timerEnabledOn = (this.delayToOn > 0 && this._wantToOn && this._totalTimeElapsedOn < this.delayToOn);
			if (this._timerEnabledOn) {
				//conta to tempo
				this._totalTimeElapsedOn += Time.deltaTime;
				if (this._totalTimeElapsedOn > this.delayToOn) {
					this._wantToOn = false;
					this._totalTimeElapsedOn = 0f;
					this.onTurnOnEvent ();
				}
			}
			
			//OFF
			this._timerEnabledOff = (this.delayToOff > 0 && this._wantToOff && this._totalTimeElapsedOff < this.delayToOff);
			if (this._timerEnabledOff) {
				//conta to tempo
				this._totalTimeElapsedOff += Time.deltaTime;
				if (this._totalTimeElapsedOff > this.delayToOff) {
					this._wantToOff = false;
					this._totalTimeElapsedOff = 0f;
					this.onTurnOffEvent ();
				}
			}
		}

		public void configByString (string metadata)
		{
			//ordem dos floats 
			//shakeTolerance|movementTolerance|toleranceChangePercent|(int)historyRange|(bool)onGUIDebug
			List<float> configs = PipeToList.ParseReturningFloats (metadata);
			configByFloatList (configs);
		}

		public void Reset ()
		{
			Debug.LogError ("Resetando o delay ");
			this.configByFloatList (this._inicialConfig);
		}
	}
}