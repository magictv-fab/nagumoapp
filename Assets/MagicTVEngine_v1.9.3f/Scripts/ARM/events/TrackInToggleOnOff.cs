﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;
using MagicTV.globals.events;
/// <summary>
/// @author: Renato Seiji Miawaki
/// Track in toggle on off.
/// </summary>
namespace ARM.events{
	public class TrackInToggleOnOff : ToggleOnOffAbstract {
		public bool ResetOnTrackin = true;
		//abaixo só para debug
		void Start(){
			AppRootEvents.GetInstance ().GetAR ().onTrackIn += trackInHandler;
			AppRootEvents.GetInstance ().GetAR ().onTrackOut += trackOutHandler;
		}

		void trackOutHandler (string s)
		{
			this.onTurnOffEvent ();
		}

		void trackInHandler (string s)
		{
			if (ResetOnTrackin) {
				this.onTurnOffEvent ();
			}
			this.onTurnOnEvent ();
		}

}
}
