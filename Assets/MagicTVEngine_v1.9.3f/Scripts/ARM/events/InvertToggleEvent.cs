﻿using UnityEngine;
using System.Collections;

using ARM.abstracts;
/**
 * 
 * @author Renato Miawaki
 * 
 * Inverte o evento de toggle recebido
 * quando o ON é disparado, este plugin dispara o OFF
 * quando o OFF é disparado, este plugin dispara o ON
 * */
namespace ARM.events{
	public class InvertToggleEvent : ToggleOnOffAbstract {

		public ToggleOnOffAbstract toggle ;

		// Use this for initialization
		void Start () {
			if( this.toggle != null ){
				this.toggle.addTurnOnChange( this.onTurnOffEvent ) ;
				this.toggle.addTurnOffChange( this.onTurnOnEvent ) ;
			}
		}
	}
}