﻿using UnityEngine;
using System.Collections;

using ARM.abstracts ;

/// <summary>
/// Converte o que é evento de mudança em On e Off
/// ATENÇÃO	Essa classe dispara o On e logo em segiuda o Off
/// 
/// Então para ter um tempo de delay para o Off, coloque esse plugin em um DelayToggleEvent 
/// </summary>
namespace ARM.events{
	public class ChangeEventToggleOnOff : BaseDelayToggleEventAbstract {
		/// <summary>
		/// O plugin de evento de change
		/// </summary>
		public ChangeEventAbstract PluginChange ;

		protected bool _hasPluginChange = false ;

		void Start(){

			if( this.PluginChange != null ){

				this._hasPluginChange = true ;

				this.PluginChange.addChangeEventListener( this.changedEventHandler ) ;

			}

		}

		protected void changedEventHandler( float percentChanged ){

			this._turnToOn () ;
			this._turnToOff () ;

		}
	}
}