﻿using UnityEngine;
using System.Collections;

using ARM.abstracts;
/// <summary>
/// Delay toggle event. Que recebe um plugin de toggle como parametro
/// Se precisar extender e fazer e ter as configurações de delay utilize BaseDelayToggleEventAbstract
/// </summary>
namespace ARM.events
{

	public class DelayToggleEvent : BaseDelayToggleEventAbstract
	{

		public ToggleOnOffAbstract toggle ;

		// Use this for initialization
		void Start ()
		{

			_inicialConfig.Add (this.delayToOn);
			_inicialConfig.Add (this.delayToOff);
			if (this.toggle != null) {
				this.toggle.addTurnOnChange (this._turnToOn);
				this.toggle.addTurnOffChange (this._turnToOff);
			}
		}

	}
}