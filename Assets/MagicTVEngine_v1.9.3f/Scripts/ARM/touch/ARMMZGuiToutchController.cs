﻿using System;
using UnityEngine;

public enum TouchControllerState
{
    Idle,
    Moving,
    Rotating,
    Scalling
}
;

[RequireComponent(typeof(BoxCollider))]
public class ARMMZGuiToutchController : MonoBehaviour
{
    private TouchControllerState _currentState = TouchControllerState.Idle;

    private Vector2 _startClick;
    private Vector2 _start1Click;
    private Vector2 _start2Click;

    private Vector2 _startRotationClick;

    private Vector3 _screenPoint;
    private Vector3 _offset;
    private float _scaleAcurracy = 1f;

    public float RotationAngleTolerance = 55f;
    public float ScaleMaximumTolerance = 4f;
    public float ScaleMinimumTolerance = 0.5f;
    public bool TapToSelect = false;

    public static ARMMZGuiToutchController SelectedObject;

    // Use this for initialization
    void Start()
    {
        _currentState = TouchControllerState.Idle;
    }

    void CheckTouchObject(Vector2 pos)
    {
        if (SelectedObject == this)
            return;

        Ray ray = Camera.main.ScreenPointToRay(pos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && (hit.collider.gameObject == gameObject))
        {
            Debug.DrawLine(ray.origin, hit.point);
            OnSelect();
            Click();
        }
    }

    private float _angle2Touches = 0;
    private int _touchCount = 0;
    // Update is called once per frame
    void Update()
    {
        if (CheckTouching())
        {
            CheckTouchObject(GetLastTouch());
        }

        //checa se não tem toque
        if ((Input.touchCount <= 0) && (!Input.GetMouseButton(0) && !Input.GetMouseButton(1)))
        {
            _currentState = TouchControllerState.Idle;
            _scaleAcurracy = 1f;
            if (SelectedObject == this)
                SelectedObject = null;
        }

        if (SelectedObject != this)
            return;

        var scrool = Input.GetAxis("Mouse ScrollWheel");
        if (scrool != 0)
        {
            OnScroll(scrool);
        }

        //Decidindo entre continuar a escala ou rotação
        if (Input.touchCount == 2)
        {
            if (_touchCount == 1)
            {
                {
                    _start1Click = Input.GetTouch(0).position;
                    _start2Click = Input.GetTouch(1).position;
                }
            }

            _angle2Touches = CalculateAngleBetweenTouchs();
            CalculateScaleRate(Input.GetTouch(0).position, Input.GetTouch(1).position);

            if ((_scaleAcurracy >= 1.05f) || (_scaleAcurracy <= 0.95f))
            {
                _currentState = TouchControllerState.Scalling;
            }

            if (Mathf.Abs(_angle2Touches) >= 30f)
            {
                _currentState = TouchControllerState.Rotating;
            }
        }

        if (Input.GetMouseButton(1) && Input.touchCount == 0)
        {
            _currentState = TouchControllerState.Rotating;
        }

        if (_currentState == TouchControllerState.Moving)
        {
            Move();
        }

        if (_currentState == TouchControllerState.Rotating)
        {
            Rotate(_angle2Touches);
        }

        if (_currentState == TouchControllerState.Scalling)
        {
            transform.localScale = transform.localScale * _scaleAcurracy;
            CheckScaleTolerance();
        }

        _touchCount = Input.touchCount;
    }

    private bool CheckTouching()
    {
        return (((Input.touchCount > 0) && (Input.touchCount < 2)) ||
                Input.GetMouseButton(0) ||
                Input.GetMouseButton(1) ||
                Input.GetMouseButtonUp(0) ||
                Input.GetMouseButtonUp(1) ||
                Input.GetMouseButtonDown(0) ||
                Input.GetMouseButtonDown(1));
    }

    private void CheckScaleTolerance()
    {
        float x = transform.localScale.x;
        float y = transform.localScale.y;
        float z = transform.localScale.z;

        x = (x < ScaleMinimumTolerance) ? ScaleMinimumTolerance
  : (x > ScaleMaximumTolerance) ? ScaleMaximumTolerance : x;

        y = (y < ScaleMinimumTolerance) ? ScaleMinimumTolerance
  : (y > ScaleMaximumTolerance) ? ScaleMaximumTolerance : y;

        z = (z < ScaleMinimumTolerance) ? ScaleMinimumTolerance
  : (z > ScaleMaximumTolerance) ? ScaleMaximumTolerance : z;

        var newRestrictedScale = new Vector3(x, y, z);
        transform.localScale = newRestrictedScale;
    }

    void Click()
    {
        if (!TapToSelect)
        {
            OnSelect();
        }

        if (SelectedObject != this)
            return;

        //seta as varias para iniciar o drag
        if (Input.GetMouseButton(0) || (Input.touchCount == 1))
        {
            _currentState = TouchControllerState.Moving;
            _startClick = GetLastTouch();
            _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            _offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(_startClick.x, _startClick.y, _screenPoint.z));
        }
    }

    float CalculateAngleBetweenTouchs()
    {
        DetectTouchMovement.Calculate();
        var angle = -DetectTouchMovement.turnAngleDelta * 10;
        return angle;
    }

    void Rotate(float angle)
    {
        if (!CheckTouching())
            return;
        if (Input.GetMouseButton(1))
        {
            angle = ((_startRotationClick.x - GetLastTouch().x) * Time.deltaTime * 40);
            _startRotationClick = GetLastTouch();
        }
        transform.Rotate(Vector3.up, angle * Time.deltaTime * 25);
    }

    void CalculateScaleRate(Vector2 newPoint1, Vector2 newPoint2)
    {
        var dist1 = Vector2.Distance(_start2Click, _start1Click);
        var dist2 = Vector2.Distance(newPoint2, newPoint1);

        _scaleAcurracy = 1f + ((dist2 - dist1) * Time.deltaTime * 0.25f);

        _start1Click = newPoint1;
        _start2Click = newPoint2;
    }

    void OnScroll(float delta)
    {
        _currentState = TouchControllerState.Scalling;
        _scaleAcurracy = 1 + delta;
    }

    void Move()
    {
        if (((_startClick == GetLastTouch()) || (_currentState != TouchControllerState.Moving))
                || !((Input.touchCount == 1) || (Input.GetMouseButton(0))))
            return;
        var lastValidClick = GetLastTouch();
        var curScreenPoint = new Vector3(lastValidClick.x, lastValidClick.y, _screenPoint.z);

        var newPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);

        newPosition += _offset;

        newPosition = new Vector3(newPosition.x, transform.position.y, newPosition.z);

        transform.position = newPosition;
    }

    public static Vector2 GetLastTouch()
    {
        var mousePosition = new Vector2(0, 0);
        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if ((Input.touchCount > 0) && (Input.touchCount < 2))
        {
            mousePosition = Input.touches[Input.touchCount - 1].position;
        }
        return mousePosition;
    }

    void OnSelect()
    {
        if (SelectedObject == this)
            return;
        SelectedObject = this;
        _currentState = TouchControllerState.Idle;
        _scaleAcurracy = 1f;
    }
}