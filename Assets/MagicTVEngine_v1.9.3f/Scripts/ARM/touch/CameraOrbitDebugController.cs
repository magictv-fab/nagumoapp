﻿using UnityEngine;
using System.Collections;

public class CameraOrbitDebugController : MonoBehaviour
{
    public Vector3 orbitPivot;
    public float upAngle = 1f;
    public float leftAngle = 2f;
    public float backAngle = 0f;
	
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(orbitPivot, Vector3.up, upAngle);
        transform.RotateAround(orbitPivot, Vector3.left, leftAngle);
		transform.RotateAround(orbitPivot, Vector3.back, backAngle);
        transform.LookAt(orbitPivot);
    }
}
