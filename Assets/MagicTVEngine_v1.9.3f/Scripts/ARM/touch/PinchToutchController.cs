﻿using System;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class PinchToutchController : MonoBehaviour
{
	public float touchAngle;
	public float zoom;
	public float touchDistance;

	public float deltaAngle;
	public float deltaAngleTolerance = 20f;
	public float deltaAngleToleranceReset = 0f;
	public float deltaZoom;

	public bool fixAxisVertical;
	public bool fixAxisHorizontal;
	public bool fixAxisDepth;

	public bool canPan;
	public bool canZoom;
	public bool canRotate;

	private Vector2 _startClick;

	private Vector2 _startRotationClick;

	private Vector3 _screenPoint;
	private Vector3 _offset;
	private float _scaleAcurracy = 1f;

    public float PanSpeedFactor = 10f;
    public float RotateSpeedFactor = 50f;
	public float RotateDeltaMinimum = 2f;
	public float ScaleDeltaMinimum = 65f;
	public float ScaleDeltaMaximum = 100f;
	public float ScaleSpeedFactor = 2f;
	public float ScaleMaximumTolerance = 4f;
	public float ScaleMinimumTolerance = 0.5f;
	public bool tapToSelect = false;
    public bool PanRelativeToWorld = true;

	public static PinchToutchController SelectedObject;

	public GameObject currentCamera;

	public GameObject cameraToControl;
	public GameObject cameraToReference;
	public Vector3 cameraToControlTargetPivot = Vector3.zero;
	public Vector3 cameraToReferenceTargetPivot = Vector3.zero;
	public float cameraToControlInitalTargetDistance = 0f;
	public float cameraToReferenceInitalTargetDistance = 0f;
	public float cameraToControlTargetDistance = 0f;
	public float cameraToReferenceTargetDistance = 0f;
	public float cameraToControlCurrentPercentage = 0f;
	public float cameraToReferenceCurrentPercentage = 0f;
	public float cameraCurrentZoom = 0f;
	public float cameraZoom = 0f;
	public float cameraZoomBonus = 0f;
	public float cameraRelativeRatio;

	public float camerasDistance = 0f;
	public float camerasMaxDistance = 0f;
	public float camerasMinDistance = -10f;
	public TouchControllerState currentState = TouchControllerState.Idle;

	// Use this for initialization
	void Start ()
	{
		//cameraToReference = Camera.main;
		if (cameraToReference == null) {
			if ((StaticPinchCameraZoomFilter.Instance != null) && (StaticPinchCameraZoomFilter.Instance.CameraToPinchControl != null)) {
				cameraToReference = StaticPinchCameraZoomFilter.Instance.CameraToPinchControl;
			} else {
				cameraToReference = Camera.main.gameObject;
			}
		}
		currentCamera = (cameraToControl == null) ? cameraToReference : cameraToControl;
		if (HasCameraToFollow () && cameraToControl != null) {
			camerasDistance = Vector3.Distance (cameraToReference.transform.position, cameraToControl.transform.position);
			cameraToControlInitalTargetDistance = Vector3.Distance (cameraToControl.transform.position, cameraToControlTargetPivot);
			cameraToReferenceInitalTargetDistance = Vector3.Distance (cameraToReference.transform.position, cameraToReferenceTargetPivot);
			if (cameraToControlInitalTargetDistance == 0f) {
				cameraRelativeRatio = 1f;
				return;
			}
			cameraRelativeRatio = cameraToReferenceInitalTargetDistance / cameraToControlInitalTargetDistance;
			if (cameraRelativeRatio <= 0f)
				cameraRelativeRatio = 1f;
		}
		currentState = TouchControllerState.Idle;
		_startClick = Vector2.zero;
	}


	void LateUpdate ()
	{
		CameraFollow ();
	}

	private float _angle2Touches = 0;
	private int _touchCount = 0;
	// Update is called once per frame
	void Update ()
	{
		if (SelectedObject != this)
			return;

		var scrool = Input.GetAxis ("Mouse ScrollWheel");
		if (scrool != 0) {
			OnScroll (scrool);
		}

		if ((SelectedObject != this) && tapToSelect)
			return;

		deltaZoom = 0;
		deltaAngle = 0;

		if (Input.touchCount == 1) {
			Touch touch1 = Input.touches [0];
			if (touch1.phase == TouchPhase.Moved) {
				currentState = TouchControllerState.Moving;
			} else {
				if (currentState != TouchControllerState.Moving)
					_startClick = GetLastTouch ();
			}
		}

		if (Input.GetMouseButton (0) && (currentState != TouchControllerState.Moving)) {
			currentState = TouchControllerState.Moving;
			_startClick = GetLastTouch ();
		}

		//Decidindo entre continuar a escala ou rotação
		if (Input.touchCount == 2) {
			Touch touch1 = Input.touches [0];
			Touch touch2 = Input.touches [1];

			if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved) {
				var newTouchDistance = Vector2.Distance (touch1.position, touch2.position);
				deltaZoom = (newTouchDistance - touchDistance) * (Screen.dpi / 100);
				if (Mathf.Abs (deltaZoom) >= ScaleDeltaMaximum)
					deltaZoom = 0;
				touchDistance = newTouchDistance;

				float newTouchAngle = Angle (touch1.position, touch2.position);
				deltaAngle = Mathf.DeltaAngle (touchAngle, newTouchAngle);
				touchAngle = newTouchAngle;
			}

			//Seta Rotate
			if ((currentState == TouchControllerState.Idle) || (currentState == TouchControllerState.Moving)) {
				if (Mathf.Abs (deltaAngle) >= RotateDeltaMinimum) {
					currentState = TouchControllerState.Rotating;
				}

				//Seta Scale 
				if ((Mathf.Abs (deltaZoom) >= ScaleDeltaMinimum) ||
					((Mathf.Abs (deltaZoom) > 0) && (currentState != TouchControllerState.Rotating))) {
					currentState = TouchControllerState.Scalling;
				}
			}
		}

		//mouse
		if (Input.GetMouseButton (1) && Input.touchCount == 0) {
			currentState = TouchControllerState.Rotating;
		}

		if (currentState == TouchControllerState.Moving) {
			Move ();
		}

		if (currentState == TouchControllerState.Rotating) {
			Rotate ();
		}

		if (currentState == TouchControllerState.Scalling) {
			Scale ();
		}

		_touchCount = Input.touchCount;

		if ((Input.touchCount == 0) && (!Input.GetMouseButton (0))) {
			OnMouseUp ();
		}
	}

	bool HasCameraToFollow ()
	{
		return ((cameraToControl != null) && (cameraToReference != null));
	}

	private void CameraFollow ()
	{
		if (!HasCameraToFollow ())
			return;
		if (cameraRelativeRatio <= 0f)
			return;

		cameraToControlTargetDistance = Vector3.Distance (cameraToControl.transform.position, cameraToControlTargetPivot);
		cameraToReferenceTargetDistance = Vector3.Distance (cameraToReference.transform.position, cameraToReferenceTargetPivot);
		//camera no 0 0 0 e pivot da camera no 0 0 0
		if (cameraToReferenceTargetDistance == 0f)
			cameraToReferenceTargetDistance = 1f;
		cameraToControlCurrentPercentage = GetPercentage (cameraToControlInitalTargetDistance, cameraToControlTargetDistance);
		cameraToReferenceCurrentPercentage = GetPercentage (cameraToReferenceInitalTargetDistance, cameraToReferenceTargetDistance);
		cameraCurrentZoom = 1 - cameraToReferenceCurrentPercentage;

		//starting math relating the two cameras
		//relative world point
		var relX = (cameraToReference.transform.position.x - cameraToReferenceTargetPivot.x) / cameraRelativeRatio;
		var rely = (cameraToReference.transform.position.y - cameraToReferenceTargetPivot.y) / cameraRelativeRatio;
		var relz = (cameraToReference.transform.position.z - cameraToReferenceTargetPivot.z) / cameraRelativeRatio;

		var newPosition = new Vector3 (relX, rely, relz) + cameraToControlTargetPivot;

		//zoom
		var _zoom = (cameraZoom + cameraZoomBonus);
		_zoom = Mathf.Clamp (_zoom, camerasMinDistance, camerasMaxDistance);
		cameraToControl.transform.position = newPosition + (cameraToReference.transform.forward * (_zoom * cameraToReferenceTargetDistance));

		cameraToControl.transform.rotation = cameraToReference.transform.rotation;
	}

	private float GetPercentage (float wholeNumber, float currentNumber)
	{
		if (wholeNumber == 0f)
			return 1f;
		return Mathf.Clamp ((currentNumber / wholeNumber), 0f, float.PositiveInfinity);
	}

	static public float Angle (Vector2 pos1, Vector2 pos2)
	{
		Vector2 from = pos2 - pos1;
		Vector2 to = new Vector2 (1, 0);

		float result = Vector2.Angle (from, to);
		Vector3 cross = Vector3.Cross (from, to);

		if (cross.z > 0) {
			result = 360f - result;
		}

		return result;
	}

	private void CheckScaleTolerance ()
	{
		float x = transform.localScale.x;
		float y = transform.localScale.y;
		float z = transform.localScale.z;

		x = (x < ScaleMinimumTolerance) ? ScaleMinimumTolerance : (x > ScaleMaximumTolerance) ? ScaleMaximumTolerance : x;

		y = (y < ScaleMinimumTolerance) ? ScaleMinimumTolerance : (y > ScaleMaximumTolerance) ? ScaleMaximumTolerance : y;

		z = (z < ScaleMinimumTolerance) ? ScaleMinimumTolerance : (z > ScaleMaximumTolerance) ? ScaleMaximumTolerance : z;

		var newRestrictedScale = new Vector3 (x, y, z);
		transform.localScale = newRestrictedScale;
	}

	void Rotate ()
	{
		if (!canRotate)
			return;
		if (currentState != TouchControllerState.Rotating)
			return;
#if UNITY_EDITOR
        if ((Input.GetMouseButton(1)) && (!(Input.touchCount > 1)))
        {
            deltaAngle = ((_startRotationClick.x - GetLastTouch().x) * Time.deltaTime * 40);
            _startRotationClick = GetLastTouch();
        }
#endif
		if (Mathf.Abs (deltaAngle) > 0) {
			if (deltaAngle >= deltaAngleTolerance) {
				deltaAngle = deltaAngleToleranceReset;
			}
			transform.Rotate (Vector3.up, deltaAngle * Time.deltaTime * RotateSpeedFactor);
		}
	}

	public void OnScroll (float scrollValue)
	{
		currentState = TouchControllerState.Scalling;
		deltaZoom = scrollValue;
		if (HasCameraToFollow ())
			deltaZoom = deltaZoom * 5;
		Scale ();
	}

	void Scale ()
	{
		if (!canZoom)
			return;
		if (currentState != TouchControllerState.Scalling)
			return;

		if (HasCameraToFollow ()) {
			camerasDistance += deltaZoom * (ScaleSpeedFactor / 50);
			cameraZoom += deltaZoom * (ScaleSpeedFactor / 19500);
		} else {
			_scaleAcurracy = 1 + (deltaZoom * (ScaleSpeedFactor / 100));
			transform.localScale = transform.localScale * _scaleAcurracy;
			CheckScaleTolerance ();
		}
	}

	void Move ()
	{
		if (!canPan)
			return;
		if (currentState != TouchControllerState.Moving)
			return;

		if (!((Input.touchCount == 1) || (Input.GetMouseButton (0))))
			return;

		var lastValidClick = GetLastTouch ();

		var newPosition = transform.position + (Vector3.up * (((lastValidClick.y - _startClick.y) * Time.deltaTime) / PanSpeedFactor));

		var crossCan = (Vector3.Dot (Vector3.right, currentCamera.transform.right) < 0) ? -1 : 1;

		newPosition = newPosition + (Vector3.right * (((lastValidClick.x - _startClick.x) * Time.deltaTime) / PanSpeedFactor) * (crossCan));

		_startClick = lastValidClick;


        if (PanRelativeToWorld)
        {
            //relativo ao mundo
            //projetando os eixos relativos da posição original nos vetores padrões
            var currentDotVertical = Vector3.Dot(transform.position, Vector3.up);
            var currentDotHorizontal = Vector3.Dot(transform.position, Vector3.right);
            var currentDotDepth = Vector3.Dot(transform.position, Vector3.forward);

            //projetando os eixos relativos da nova posição original nos vetores padrões
            var dotVertical = Vector3.Dot(newPosition, Vector3.up);
            var dotHorizontal = Vector3.Dot(newPosition, Vector3.right);
            var dotDepth = Vector3.Dot(newPosition, Vector3.forward);

            if (fixAxisVertical)
            {
                dotVertical = currentDotVertical;
            }

            if (fixAxisHorizontal)
            {
                dotHorizontal = currentDotHorizontal;
            }

            if (fixAxisDepth)
            {
                dotDepth = currentDotVertical;
            }

            //projetando os eixos dos vetores padrões para a nova posição do gameobject
            transform.position = (Vector3.up * dotVertical) + (Vector3.right * dotHorizontal) + (Vector3.forward * dotDepth);
        }
        else
        {
            //  relativo a camera
            var currentDotVertical = Vector3.Dot(transform.position, Camera.main.transform.up);
            var currentDotHorizontal = Vector3.Dot(transform.position, Camera.main.transform.right);
            var currentDotDepth = Vector3.Dot(transform.position, Camera.main.transform.forward);

            var dotVertical = Vector3.Dot(newPosition, Camera.main.transform.up);
            var dotHorizontal = Vector3.Dot(newPosition, Camera.main.transform.right);
            var dotDepth = Vector3.Dot(newPosition, Camera.main.transform.forward);

            if (fixAxisVertical)
            {
                dotVertical = currentDotVertical;
            }

            if (fixAxisHorizontal)
            {
                dotHorizontal = currentDotHorizontal;
            }

            if (fixAxisDepth)
            {
                dotDepth = currentDotVertical;
            }

            transform.position = (Camera.main.transform.up * dotVertical) + (Camera.main.transform.right * dotHorizontal) + (Camera.main.transform.forward * dotDepth);
        }
	}

	public static Vector2 GetLastTouch ()
	{
		var mousePosition = new Vector2 (0, 0);
		if (Input.GetMouseButton (0) || Input.GetMouseButton (1)) {
			mousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		}
		if ((Input.touchCount > 0) && (Input.touchCount < 2)) {
			mousePosition = Input.touches [Input.touchCount - 1].position;
		}
		return mousePosition;
	}

	void OnMouseDown ()
	{
		OnSelect ();

		if (SelectedObject != this)
			return;

		//seta as variaves para iniciar o drag
		//if (Input.GetMouseButton(0) || (Input.touchCount == 1))
		//{
		//    currentState = TouchControllerState.Moving;
		//    _startClick = GetLastTouch();
		//    _screenPoint = currentCamera.WorldToScreenPoint(gameObject.transform.position);
		//    _offset = transform.position - currentCamera.ScreenToWorldPoint(new Vector3(_startClick.x, _startClick.y, _screenPoint.z));
		//}
	}

	void OnMouseUp ()
	{
		currentState = TouchControllerState.Idle;
		//  _scaleAcurracy = 1f;
		//  deltaZoom = 0;
		//  deltaAngle = 0;
		//  touchDistance = 0;
		//  touchAngle = 0;
	}

	void OnSelect ()
	{
		if (SelectedObject == this)
			return;
		SelectedObject = this;
		currentState = TouchControllerState.Idle;
		_scaleAcurracy = 1f;
	}
}