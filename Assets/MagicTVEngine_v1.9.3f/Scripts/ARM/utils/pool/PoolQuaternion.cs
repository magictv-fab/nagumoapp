﻿using UnityEngine;
using System.Collections;

namespace ARM.utils{
	
	public class PoolQuaternion {
		
		private PoolFloat _x ;
		
		private PoolFloat _y ;
		
		private PoolFloat _z ;

		private PoolFloat _w ;

		private int 	_currentIndex 	= 0 ;
		private uint 	_maxSize 		= 0 ;
		private int 	_currentSize 	= 0 ;
		
		protected Quaternion[] itens ;
		public PoolQuaternion( uint size ){
			this._maxSize = size ;
			
			this._x 			= new PoolFloat ( this._maxSize ) ;
			this._y 			= new PoolFloat ( this._maxSize ) ;
			this._z 			= new PoolFloat ( this._maxSize ) ;
			this._w 			= new PoolFloat ( this._maxSize ) ;
			
			this.reset ();
		}
		
		public void addItem( Quaternion item ){
			this.itens [ this._currentIndex ] = item ;
			this._currentIndex++;
			if( this._currentSize < this._maxSize && this._currentIndex < this._maxSize ){
				this._currentSize ++ ;
			}
			this._x.addItem ( item.x );
			
			this._y.addItem ( item.y );
			
			this._z.addItem ( item.z );

			this._w.addItem ( item.w );

			_currentIndex = _currentIndex % (int) this._maxSize ;
		}
		public int getCurrentSize(){
			return this._currentSize ;
		}
		public bool isFull(){
			return ( this.getCurrentSize() >= this._maxSize ) ;
		}
		public void reset(){
			this._currentSize 	= 0 ;
			this._currentIndex 	= 0 ;
			this.itens 			= new Quaternion[ this._maxSize ] ;
			this._x.reset() ;
			this._y.reset() ;
			this._z.reset() ;
			this._w.reset() ;
		}
		public Quaternion[] getItens(){
			return this.itens ;
		}
		public Quaternion getMedian(){
			return new Quaternion( this._x.getMedian(), this._y.getMedian(), this._z.getMedian(), this._w.getMedian() ) ;
		}
		
		public Quaternion getMean(){



			return new Quaternion( this._x.getMean(), this._y.getMean(), this._z.getMean() , this._w.getMean() ) ;

		}
		public Quaternion getLastAngle(){
			return this.itens [ this._currentIndex ] ;
		}
		public Quaternion[] getLastsAngles ( uint length ) {
			Quaternion[] list = new Quaternion[ length ] ;
			uint count = 0 ;
			int index = this._currentIndex ;
			while( count < length ){
				index = (int) this._currentIndex - ( int ) count ;
				if( index < 0 ){
					index = this.itens.Length - 1 ;
				}
				list[ count++ ] = this.itens[ index ];
			}
			return list ;
		}
	}
}