﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
namespace ARM.utils{
	public class VectorCompare : MonoBehaviour  , IComparer< Vector3 >{
		public int Compare(Vector3 x, Vector3 y) {
			float distanceA = x.sqrMagnitude ;
			float distanceB = y.sqrMagnitude ;
			if( distanceA > distanceB ){
				return 1 ;
			} else if( distanceB > distanceA ){
				return -1 ;
			}
			return 0 ;
		}
	}
}