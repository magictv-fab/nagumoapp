﻿using UnityEngine;
using System.Collections;

namespace ARM.utils{

	public class PoolVector3 {

		private PoolFloat _x ;
		
		private PoolFloat _y ;
		
		private PoolFloat _z ;

		private int 	_currentIndex 	= 0 ;
		private uint 	_maxSize 		= 0 ;
		private int 	_currentSize 	= 0 ;

		protected Vector3[] itens ;
		public PoolVector3( uint size ){
			this._maxSize = size ;

			this._x 			= new PoolFloat ( this._maxSize ) ;
			this._y 			= new PoolFloat ( this._maxSize ) ;
			this._z 			= new PoolFloat ( this._maxSize ) ;

			this.reset ();
		}

		public void addItem( Vector3 item ){
			this.itens [ this._currentIndex ] = item ;
			this._currentIndex++;
			if( this._currentSize < this._maxSize && this._currentIndex < this._maxSize ){
				this._currentSize ++ ;
			}
			this._x.addItem ( item.x );
			
			this._y.addItem ( item.y );
			
			this._z.addItem ( item.z );

			_currentIndex = _currentIndex % (int) this._maxSize ;
		}
		public int getCurrentSize(){
			return this._currentSize ;
		}
		public bool isFull(){
			return ( this.getCurrentSize() >= this._maxSize ) ;
		}
		public void reset(){
			this._currentSize 	= 0 ;
			this._currentIndex 	= 0 ;
			this.itens 			= new Vector3[ this._maxSize ] ;
			this._x.reset() ;
			this._y.reset() ;
			this._z.reset() ;
		}
		public Vector3[] getItens(){
			return this.itens ;
		}
		public Vector3 getMedian(){
			return new Vector3( this._x.getMedian(), this._y.getMedian(), this._z.getMean() ) ;
		}
		
		public Vector3 getMean(){
			return new Vector3( this._x.getMean(), this._y.getMean(), this._z.getMean() ) ;
		}
		public Vector3 getLastPosition(){
			return this.itens [ this._currentIndex ] ;
		}
		public Vector3[] getLastsPosition ( uint length ) {
			Vector3[] list = new Vector3[ length ] ;
			uint count = 0 ;
			int index = this._currentIndex ;
			while( count < length ){
				index = (int) this._currentIndex - ( int ) count ;
				if( index < 0 ){
					index = this.itens.Length - 1 ;
				}
				list[ count++ ] = this.itens[ index ];
			}
			return list ;
		}
	}
}