﻿using UnityEngine;
using System.Collections;

namespace ARM.utils{

	public class DevicePosition {

		// Use this for initialization
		public static void start () {
			Input.gyro.enabled = true;
		}
		
		// Update is called once per frame
		public static Vector3 getVector () {
			Vector3 rotation = Input.gyro.attitude.eulerAngles ;
			rotation.x *= -1 ;
			rotation.y *= -1 ;
			return rotation ;




		}
		public static Quaternion getQuaternion(){
			return Quaternion.Euler ( DevicePosition.getVector() ) ;
		}
	}
}