﻿using UnityEngine;
using System.Collections;



public class MathScreen {

	protected const float CENTIMETER_2_INCHE_RATIO = 2.54f ;

	/* ***************** CM 2 PX ***************** */

	/* ********* Vector2 ********* */
	public static Vector2 cm2px( Vector2 cm , ARM.interfaces.FilterVector2Interface filter ){
		cm = MathScreen.cm2px( cm ) ;
		return filter.filter ( cm );
	}
	public static Vector2 cm2px( Vector2 cm ){
		cm.x = MathScreen.cm2px ( cm.x ) ;
		cm.y = MathScreen.cm2px ( cm.y ) ;
		return cm ;
	}
	/* ********* FLOAT ********* */
	public static float cm2px( float cm , ARM.interfaces.FilterFloatInterface filter ){
		return MathScreen.inches2px( MathScreen.cm2inches( cm ) , filter );
	}
	/**
	 * pass centimeter in real world and recive pixel for screen
	 * */
	public static float cm2px( float cm ){
		return MathScreen.inches2px( MathScreen.cm2inches ( cm ) );
	}
	/* ***************** INCHES 2 PX ***************** */

	/* ********* Vector2 ********* */
	public static Vector2 inches2px( Vector2 inches , ARM.interfaces.FilterVector2Interface filter ){
		inches = MathScreen.inches2px( inches ) ;
		return filter.filter ( inches );
	}
	public static Vector2 inches2px( Vector2 inches ){
		inches.x = MathScreen.inches2px ( inches.x ) ;
		inches.y = MathScreen.inches2px ( inches.y ) ;
		return inches ;
	}

	/* ********* FLOAT ********* */
	public static float inches2px( float inches , ARM.interfaces.FilterFloatInterface filter ){
		inches = MathScreen.inches2px ( inches );
		return filter.filter ( inches );
	}
	/**
	 * pass inches in real world and recive pixel for screen
	 * */
	public static float inches2px( float inches ){
		return inches / Screen.dpi ;
	}


	/* ***************** CONVERSOR ***************** */
	/**
	 * pass inches and return in centimeters
	 * */
	public static float inches2cm( float inches ){
		return inches * CENTIMETER_2_INCHE_RATIO ;
	}
	/**
	 * pass centimeter and recive inches relative value
	 * */
	public static float cm2inches( float cm ){
		return cm / CENTIMETER_2_INCHE_RATIO ;
	}
}
