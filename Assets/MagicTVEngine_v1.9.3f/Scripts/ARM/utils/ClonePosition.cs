﻿using UnityEngine;
using System.Collections;

namespace ARM.utils {
	public class ClonePosition : MonoBehaviour {
		public GameObject someOne ;
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if( someOne != null ){
				this.transform.position = someOne.transform.position ;
				this.transform.rotation = someOne.transform.rotation ;
			}
		}
	}
}