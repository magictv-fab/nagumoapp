﻿using UnityEngine;
using System.Collections;
/**
 * @author Renato Seiji Miawaki
 * */
namespace ARM.utils.bytes{
	public abstract class ByteListAbstract : MonoBehaviour {

		protected uint _maxByte = 255 ;
		public virtual uint maxByteValue{
			get{
				return this._maxByte;
			}
		}

		public bool IsReady = false ;

		public delegate void OnReadyEventHandler();
		protected OnReadyEventHandler onReadyEvent ;
		
		public void addOnReadyEventListener( OnReadyEventHandler eventHandler ){
			this.onReadyEvent += eventHandler ;
		}
		public void removeOnReadyEventListener( OnReadyEventHandler eventHandler ){
			this.onReadyEvent -= eventHandler ;
		}
		protected void raiseOnReadyEvent(){
			IsReady = true ;
			if( this.onReadyEvent != null ){
				this.onReadyEvent() ;
			}
		}


		/**
		 * Implementar
		 * */
		public abstract byte[] getBytes() ;
	}
}