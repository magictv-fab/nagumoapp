﻿using UnityEngine;
using System.Collections;
using ARM.utils.bytes;
public class ByteCompareEvents : ARM.events.ChangeEventAbstract {

	public ByteListAbstract ByteListSource ;

	/// <summary>
	/// The capture frame rate.
	/// </summary>
	public float CaptureRate = 0.5f;


	/// <summary>
	/// The _history bytes to compare on the next iteration.
	/// </summary>
	private byte[] _historyBytes ;

	// Use this for initialization
	void Start () {
		ByteListSource.addOnReadyEventListener ( startByteComparison );
	}


	void startByteComparison (){
		InvokeRepeating( "TrackCameraMovements" , 0.1f ,  CaptureRate ); 
	}

	/// <summary>
	/// Tracks the camera movements.
	/// </summary>
	void TrackCameraMovements(){
		//		PASSO1: pegar apenas pontos relevantes 
		byte[] newBytesPoI  = ByteListSource.getBytes();
		//			//Debug.LogError( newBytesPoI.Length);
		//		PASSO2: rasterizar % de mudança nos pixes extraidos de PoI
		if( _historyBytes != null ){
			float diference = ByteCompare.Compare( _historyBytes  , newBytesPoI , ByteListSource.maxByteValue ) ;

			base.RaiseChageEvent( diference ) ;
			//Debug.Log ( " Camera movement: " + diference  );
		}
		_historyBytes = newBytesPoI ;
	}

}
