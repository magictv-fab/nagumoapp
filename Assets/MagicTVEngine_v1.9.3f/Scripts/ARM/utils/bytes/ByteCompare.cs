﻿using UnityEngine;
using System.Collections;
using System;
public class ByteCompare  {




	/// <summary>
	/// Compares the 2 byte[] and returns a percentage of diference.
	/// </summary>
	/// <returns>The byte po i.</returns>
	/// <param name="list1">Old data.</param>
	/// <param name="list2">New data.</param>
	public static float Compare( byte[] list1, byte[] list2, uint byteMaxValue ){

		float bytePercentualWeight = (float) ( (float)100/(float) byteMaxValue  ) ; 

		if (list1.Length != list2.Length) {
			//Debug.LogWarning("Parameter list1 must have the same size of list2" );
			
			if( list1.Length < list2.Length ){
//				list1  = fillDataWith( list1 , ImagePoICount );
				Array.Resize (ref list1, list2.Length );
			}
			if(list2.Length < list1.Length ){
				Array.Resize (ref list2, list1.Length );
			}
		}

		int listSize = list2.Length ;

		string mainLog = "list1[i].lengg " + list1.Length + " --- ";
		
		float media = 0f ;
		
		for (int i = 0; i < listSize; i++) {
			
			// % of diference between 2 bytes
			float localDif =   Mathf.Abs(  list1[i] - list2[i] )  * bytePercentualWeight ; 
			
			string s = "list1[i] " + list1[i] + "\n" ;
			s += "list2[i] " + list2[i] + "\n" ;
			s += "localDif " + localDif + "\n" ;
			s += "PixelBytePercentualWeight " + bytePercentualWeight.ToString() + "\n" ;
			//			//Debug.LogError(s);
			
			media += localDif ;
			
			mainLog+= i +""+list1[i]+"|"+list2[i]+"|" +Mathf.Abs(  list1[i] - list2[i] )+ "|"+bytePercentualWeight+"|" + localDif + "\n" ;
		}
		media /= 100; 
		
		//		//Debug.LogWarning(mainLog);
		//		//Debug.LogWarning("MEDIA:  "+media+" ~~ " );
		
		//		//Debug.LogWarning (medianCalculator.getCurrentSize());
		//		//Debug.LogWarning (medianCalculator.isFull ());
		//		return medianCalculator.getMedian ();
		return media/100 ;
	}

}
