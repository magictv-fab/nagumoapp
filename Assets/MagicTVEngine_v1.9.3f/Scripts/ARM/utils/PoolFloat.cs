﻿using UnityEngine;
using System.Collections;

namespace ARM.utils{

	public class PoolFloat {
		private float[] itens ;
		
		private int _currentIndex = 0 ;
		private int _currentSize = 0 ;
		private uint _maxSize ;
		public PoolFloat( uint size ){

			this._maxSize = size ;
			this.reset () ;
		}
		public void addItem( float item ){
			this.itens [ this._currentIndex ] = item ;
			_currentIndex++;
			if( this._currentSize < this._maxSize && this._currentIndex < this._maxSize ){
				this._currentSize ++ ;
			}
			_currentIndex = _currentIndex % (int) this._maxSize ;
		}
		
		public int getCurrentSize(){
			return this._currentSize ;
		}
		public float[] getItens(){
			return this.itens ;
		}
		public float getMedian(){
			return ARM.utils.Math.getMedian ( this.getItens () ) ;
		}

		public float getMean(){
			return ARM.utils.Math.getMean ( this.getItens () ) ;
		}

		
		public float getMeanAngle(){
			return ARM.utils.Math.getMeanAngle ( this.getItens () ) ;
		}

		public void reset(){
			this._currentSize 	= 0 ;
			this._currentIndex 	= 0 ;
			this.itens 			= new float[ this._maxSize ] ;

		}
		
		public bool isFull(){
			return ( this.getCurrentSize() >= this._maxSize ) ;
		}
		public float getLastPosition(){
			return this.itens [ this._currentIndex ] ;
		}
		public float[] getLastsPosition ( uint length ) {
			float[] list = new float[ length ] ;
			uint count = 0 ;
			int index = this._currentIndex ;
			while( count < length ){
				index = (int) this._currentIndex - ( int ) count ;
				if( index < 0 ){
					index = this.itens.Length - 1 ;
				}
				list[ count++ ] = this.itens[ index ];
			}
			return list ;
		}
	}
}