﻿// Smooth Follow from Standard Assets
	// Converted to C# because I fucking hate UnityScript and it's inexistant C# interoperability
	// If you have C# code and you want to edit SmoothFollow's vars ingame, use this instead.
	using UnityEngine;
using System.Collections;

public class SmoothFollow : MonoBehaviour {
	// The target we are following
	public GameObject target;
	// The distance in the x-z plane to the target

	// Place the script in the Camera-Control group in the component menu
	[AddComponentMenu("Camera-Control/Smooth Follow")]
	
	void Update () {
		// Early out if we don't have a target
		if (!target) return;
		
		this.transform.position = target.transform.position;
		this.transform.rotation = target.transform.rotation;
	}
}