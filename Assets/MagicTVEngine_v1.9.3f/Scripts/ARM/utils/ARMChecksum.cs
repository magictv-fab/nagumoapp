﻿using UnityEngine;
using System.Collections;

using System.Security.Cryptography;
using ARM.utils.io;


namespace ARM.utils
{
		public class ARMChecksum
		{

				public static bool CheckFileMD5 (string filePath, string md5Checksum)
				{

						if (!ARMFileManager.Exists (filePath)) {
								return false;
						}

						byte[] fileBytes = ARMFileManager.GetBytes (filePath);
						return (MD5Checksum (fileBytes) == md5Checksum);

				}

				public static string MD5Checksum (byte[] data)
				{

						// encrypt bytes
						MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider ();
						byte[] hashBytes = md5.ComputeHash (data);

						// Convert the encrypted bytes back to a string (base 16)
						var hashString = "";

						for (var i = 0; i < hashBytes.Length; i++) {
								hashString += System.Convert.ToString (hashBytes [i], 16).PadLeft (2, "0" [0]);
						}

						return hashString.PadLeft (32, "0" [0]);
				}


		}
}
