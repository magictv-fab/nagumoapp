﻿using UnityEngine;
using System.Collections;
using System;
using ARM.animation;

namespace ARM.utils.cron
{
	/// <summary>
	/// Timeout item. Para loops que terminam instantaneamente
	/// </summary>
	public class TimeoutItem : ViewTimeControllAbstract
	{

		public int id;

		public float delay ;

		public override void Play ()
		{
			base.Play ();
		}
		// Update is called once per frame
		void Update ()
		{
			if (this._isPlaying) {
				//soma
				this.delay -= Time.deltaTime;
				if (this.delay <= 0) {
					//acabou
					Stop ();
					this._onFinished ();
				}

			}
		}
		public override void Stop ()
		{
			if(!_isPlaying){
				return;
			}
			base.Stop ();
			EasyTimer.ClearInterval (this.id);
		}
	}
}