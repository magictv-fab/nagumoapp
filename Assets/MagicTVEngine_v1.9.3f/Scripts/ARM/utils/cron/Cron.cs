﻿using UnityEngine;
using System.Collections;
using ARM.utils;

namespace ARM.utils {

	public class Cron  {


		// nome do PREFAB que está na pasta Resources
		const string CRON_PREFAB_NAME = "CronWatcher";


		private static ARM.utils.CronWatcher _CronWatcherReference ;
		
		protected static ARM.utils.CronWatcher  _CronWatcher {
			get{
				if( _CronWatcherReference == null ){

					// carrega o PREFAB
					GameObject cron =  Resources.Load( CRON_PREFAB_NAME ) as GameObject ; 
					if( cron == null){
						//Debug.LogError (" Falha ao carregar o CronWatcher" );
						return _CronWatcherReference;
					}
					// isntancia o PREFAB sendo um MonoBehaviour ele passa a poder  executar as funções de loop do UNITY
					GameObject.Instantiate( cron );

					//Debug.LogWarning(  "Cron Instanciado ");

					// pego a classe com os eventos
					_CronWatcherReference = cron.GetComponent<CronWatcher>();

				}
				return _CronWatcherReference ;

			}
			
		}


		//acesso públic para adicionar na lista do Update
		public static void addUpdateListener( CronWatcher.OnCronEventHandler listener){
			_CronWatcher.addUpdateListener (listener );
		}

		//acesso públic para adicionar na lista do LateUpdate
		public static void addLateUpdateListener( CronWatcher.OnCronEventHandler listener){
			_CronWatcher.addLateUpdateListener (listener );
		}
	}
}
