﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARM.utils.cron
{
	/// <summary>
	/// Easy timer. To Start and Stop interval async loops
	/// 
	/// AUTHOR: Renato Seiji Miawaki
	/// </summary>
	public class EasyTimer : MonoBehaviour
	{
		protected static int _id = 0 ;
		protected static Dictionary<int, GameObject> timers = new Dictionary<int, GameObject> ();
		/// <summary>
		/// Sets the timeout. igual do js
		/// Baseado em GameObject pois Invoke da problema em IOS, evite
		/// </summary>
		/// <returns>The timeout.</returns>
		/// <param name="method">Method.</param>
		/// <param name="delayInMilliseconds">Delay in milliseconds.</param>
		/// <param name="timerName">Timer name.</param>
		public static int SetTimeout (ARM.animation.ViewTimeControllAbstract.OnFinishedEvent method, int delayInMilliseconds, string timerName = "")
		{
			_id++;

			GameObject go = new GameObject ("_Timer_" + _id + "_" + timerName);


			timers.Add (_id, go);

			TimeoutItem tm = go.AddComponent< TimeoutItem > ();
			tm.id = _id;
			tm.onFinished += method;
			tm.delay = delayInMilliseconds / 1000;
			tm.Play ();

			return _id;
		}
		public static TimeoutItem GetTimeoutItem (int id)
		{
			if (timers.ContainsKey (id)) {
				return timers [id].GetComponent<TimeoutItem> ();
			}
			return null;
		}
		/// <summary>
		/// Sets the interval. igual do JS
		/// </summary>
		/// <returns>The interval.</returns>
		/// <param name="method">Method.</param>
		/// <param name="delayInMilliseconds">Delay in milliseconds.</param>
		/// <param name="timerName">Timer name.</param>
		public static int SetInterval (ARM.animation.ViewTimeControllAbstract.OnFinishedEvent method, int delayInMilliseconds, string timerName = "")
		{

			_id++;
			
			GameObject go = new GameObject ("_Timer_" + _id + "_" + timerName);
			
			
			timers.Add (_id, go);
			
			IntervalItem tm = go.AddComponent< IntervalItem > ();
			tm.id = _id;
			tm.onFinished += method;
			tm.delay = delayInMilliseconds / 1000;
			tm.Play ();
			
			return _id;
		}
		/// <summary>
		/// Clears the interval. If Exists
		/// </summary>
		/// <param name="id">Identifier.</param>
		public static void ClearInterval (int id)
		{
			if (timers.ContainsKey (id)) {
				GameObject go = timers [id] ;
				timers.Remove (id);
				IntervalItem tempInterval = go.GetComponent<IntervalItem> ();
				if (tempInterval != null && tempInterval.isPlaying()) {
					tempInterval.Stop ();
				}
				GameObject.DestroyImmediate (go);
			}
		}
	}

}