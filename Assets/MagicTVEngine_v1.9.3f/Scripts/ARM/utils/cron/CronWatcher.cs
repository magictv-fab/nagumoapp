﻿using UnityEngine;
using System.Collections;



namespace ARM.utils {

	public class CronWatcher : MonoBehaviour {

		public  delegate void OnCronEventHandler() ;


		protected static event OnCronEventHandler onUpdate ;
		protected static event OnCronEventHandler onLateUpdate ;


		public void addUpdateListener( OnCronEventHandler listener ){

			onUpdate += listener;
		}

		public void addLateUpdateListener( OnCronEventHandler listener ){


			onLateUpdate += listener;
		}
		

		void Update () {

			if (onUpdate != null) {

				onUpdate ();
			}
		}


		void LateUpdate () {

			if (onLateUpdate != null) {

				onLateUpdate ();
			}
		}
	}
}
