﻿using UnityEngine;
using System.Collections;
using System;
using ARM.animation;

/// <summary>
/// 
/// Interval item. para loops que não param
/// 
/// @AUTHOR: Renato Seiji Miawaki 
/// 
/// </summary>

namespace ARM.utils.cron{

	public class IntervalItem : ViewTimeControllAbstract {

		public int id;

		public float delay ;

		public override void Play ()
		{
			base.Play ();
		}
		// Update is called once per frame
		void Update () {
				if( this._isPlaying ){
					
					//soma
					this.delay -= Time.deltaTime ;
					if( this.delay < 0 ){
						this._onFinished() ;
						Stop();
					}

				}
		}
		public override void Stop ()
		{
			if(!_isPlaying){
				return;
			}
			base.Stop ();
			EasyTimer.ClearInterval (this.id);
		}
	}
}