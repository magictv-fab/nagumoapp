﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;


namespace ARM.utils.download
{

	public class SingleGenericDownloader : GenericDownloadManager {

		#region GenericDownloader Instance and GO
			private const string DOWNLOADER_GAMEOBJECT_NAME = "_SingleGenericDownloaderGO";
			protected static GameObject _selfGameObject;
			
			protected static GameObject SelfGameObject {
				get {
					if (_selfGameObject == null) {
						_selfGameObject = GameObject.Find (DOWNLOADER_GAMEOBJECT_NAME);
						if (_selfGameObject != null)
							DestroyImmediate (_selfGameObject.gameObject);
						
						_selfGameObject = new GameObject (DOWNLOADER_GAMEOBJECT_NAME);

						SelfInstance = _selfGameObject.AddComponent<SingleGenericDownloader> ();
						
					}
					return _selfGameObject;
				}
			}
			
			protected static SingleGenericDownloader _selfInstance;
			
			protected static SingleGenericDownloader SelfInstance {
				get {
					if (_selfInstance == null) {
						_selfInstance = SelfGameObject.GetComponent<SingleGenericDownloader> ();
					}
					return _selfInstance;
				}
				set {
					_selfInstance = value;
				}
			}
		#endregion

		#region Public Events
			public static void AddErrorEventhandlerStatic( OnErrorEventHandler eventHandler ){
				SelfInstance.AddErrorEventhandler( eventHandler );
			}
			public static void AddDownloadCompleteEventHandler( OnDownloadCompleteEventHandler eventHandler){
				SelfInstance.AddDownloadCompleteEventhandler (eventHandler);		
			}
			public static void AddProgressEventhandlerStatic(OnProgressEventHandler enventHandler){
				SelfInstance.AddProgressEventhandler( enventHandler );
			}
			
			public static void AddBusyEventHandler( OnCompleteEventHandler eventHandler ){
				SelfInstance.onBusy += eventHandler;		
			}
		#endregion

			
		public static void LoadStatic ( string url = null, int extraAttempts = 0 ){
			SelfInstance.doLoad (url, extraAttempts );

		}

		protected void doLoad( string url = null, int extraAttempts = 0  ){
			//Debug.Log (" SingleGenericDownloader doLoad ");
			if( _isDownloading == true ){
				//Debug.Log("RaiseBusy");
				RaiseBusy();
				return;
			}
			_isDownloading = true;
			this.Load (url, extraAttempts) ;	
		}
		#region singleDownload Process Lock
			private OnCompleteEventHandler onBusy;
		
			private void RaiseBusy(){
				if (onBusy != null) {
					onBusy();
				}		
			}

			protected bool _isDownloading = false ;

			protected override void RaiseComplete ()
			{
				_isDownloading = false ;
				Dispose ();
				base.RaiseComplete ();
			}
		#endregion

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}
	}
}