using UnityEngine;
using System.Collections;


namespace ARM.utils.download
{

		public class GameObjectDownloader : DownloaderAbstract
		{



				int _totalAttempts = 0 ;

				int _attemptCount = 0 ;

				WWW _loader;

				
				string requestUrl ;
				/// <summary>
				/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
				/// Depois de pronta, dispare o evento RaiseComponentIsReady
				/// </summary>
				public override void prepare ()
				{
						this.RaiseComponentIsReady ();
				}

				override public void Load (string url = null, int attempts = 0)
				{
						_totalAttempts = attempts;
						requestUrl = url;
						//Debug.Log ( string.Format( "GODownloader::Load {0}" , url ));
						StartCoroutine (doLoad ());
						 
				}
				/// <summary>
				/// Do the load.
				/// </summary>
				/// <returns>The load.</returns>
				/// <param name="requestUrl">Request URL.</param>
				IEnumerator doLoad ()
				{

						using (_loader = new  WWW ( requestUrl )) {
								yield return _loader;
								this.errorCheck ();
								this.onComplete ();
						} 
				}

				bool _isDisposed;

				/// <summary>
				/// Releases all resource used by the <see cref="ARM.utils.download.GameObjectDownloader"/> object.
				/// and Delete downloader object
				/// </summary>
				/// <remarks>Call <see cref="Dispose"/> when you are finished using the
				/// <see cref="ARM.utils.download.GameObjectDownloader"/>. The <see cref="Dispose"/> method leaves the
				/// <see cref="ARM.utils.download.GameObjectDownloader"/> in an unusable state. After calling <see cref="Dispose"/>,
				/// you must release all references to the <see cref="ARM.utils.download.GameObjectDownloader"/> so the garbage
				/// collector can reclaim the memory that the <see cref="ARM.utils.download.GameObjectDownloader"/> was occupying.</remarks>
				override public void Dispose ()
				{
						_isDisposed = true;
						_loader.Dispose ();
						DestroyImmediate (this.gameObject);
				}
				void OnDestroy ()
				{
						_isDisposed = true;
						_loader.Dispose ();
				}
				override public bool IsComplete ()
				{
						return _isComplete;
				}

				override public WWW GetRequest ()
				{
						return _loader;
				}

				void errorCheck ()
				{
						if (_isComplete) {
								return;
						}
			
						if (_loader != null && _loader.error != null) {
						
								if (_attemptCount < _totalAttempts) {
										_attemptCount++;

										//Debug.LogWarning ( string.Format( "WWW extra download attempt {0} from {1} \n\turl:{2}\n\terror:{3}", _attemptCount, _totalAttempts , requestUrl, _loader.error ) ) ;

										StartCoroutine (doLoad ());
										return;
								}

								//Debug.LogError ("WWW download had an error:" + _loader.error + "\n When loading:"+requestUrl) ;
								this.RaiseError ("WWW download had an error:" + _loader.error + "\n When loading:" + requestUrl);
						}
				}
	
				void onComplete ()
				{
						_isComplete = _loader.isDone;
						this.RaiseDownloadComplete (_loader);
						this.RaiseComplete ();
				}
		
				void Update ()
				{
						this.errorCheck ();						
						if (!_isComplete && _loader != null) {
								
//						//Debug.LogWarning(  string.Format( " GO Downloader: PROGRESS:{0}", _loader.progress ) );
								this.RaiseProgress (_loader.progress);
								
								if (_loader.isDone) {
//									//Debug.LogError( " GO Downloader: onComplete do DOne" );
										this.onComplete ();
								} 
								return;
						}
						if (_loader == null) {
								Debug.Log ("GameObjectloader . Update ~ Era pra ter acabado o download mas _loader está null");
								return;
						}
						if (_isDisposed) {
								//Debug.Log (string.Format (
							//"GameObjectloader . Update ~ Era prA TER ACABADO BUT...  DISPOSED!"));
								return;
						}

						//Debug.Log (string.Format (
										//"GameObjectloader . Update ~ Era prA TER ACABADO BUT...  isDone({0}) bytesDownloaded({1})  size({2})", 
										//_loader.isDone,
										//_loader.bytesDownloaded,
										//_loader.size
						//));
						
				}
		}
}