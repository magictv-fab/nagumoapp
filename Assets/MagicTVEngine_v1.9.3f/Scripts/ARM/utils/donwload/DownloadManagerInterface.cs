﻿using UnityEngine;
using System.Collections;

namespace ARM.utils.download
{
		public interface DownloadManagerInterface
		{
		
				/// <summary>
				/// Load the specified requestUrl.
				/// </summary>
				/// <param name="requestUrl">Request URL.</param>
				void Load (string requestUrl = null, int attempts = 0 ) ;

				/// <summary>
				/// Releases all resource used by the <see cref="ARM.utils.download.DownloadManagerInterface"/> object.
				/// </summary>
				/// <remarks>Call <see cref="Dispose"/> when you are finished using the
				/// <see cref="ARM.utils.download.DownloadManagerInterface"/>. The <see cref="Dispose"/> method leaves the
				/// <see cref="ARM.utils.download.DownloadManagerInterface"/> in an unusable state. After calling
				/// <see cref="Dispose"/>, you must release all references to the
				/// <see cref="ARM.utils.download.DownloadManagerInterface"/> so the garbage collector can reclaim the memory that
				/// the <see cref="ARM.utils.download.DownloadManagerInterface"/> was occupying.</remarks>
				void Dispose () ;

				/// <summary>
				/// Determines whether this instance is complete.
				/// </summary>
				/// <returns><c>true</c> if this instance is complete; otherwise, <c>false</c>.</returns>
				bool IsComplete () ;

				/// <summary>
				/// Gets the request.
				/// </summary>
				/// <returns>The request.</returns>
				WWW GetRequest () ;
		}
}