﻿using UnityEngine;
using System.Collections;

namespace ARM.utils.download
{
		/// <summary>
		/// Download events abstract.
		/// </summary>
		public abstract class DownloaderAbstract : ARM.abstracts.ProgressEventsAbstract, DownloadManagerInterface
		{
		
				public delegate void OnDownloadCompleteEventHandler (WWW request) ;

				/// <summary>
				/// The on download complete.
				/// </summary>
				private OnDownloadCompleteEventHandler onDownloadComplete;

				/// <summary>
				/// Raises the download complete.
				/// </summary>
				/// <param name="request">Request.</param>
				virtual protected void RaiseDownloadComplete (WWW  request)
				{
						if (onDownloadComplete != null) {
								onDownloadComplete (request);
						}	
				}

				/// <summary>
				/// Adds the download complete eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				virtual public void AddDownloadCompleteEventhandler (OnDownloadCompleteEventHandler eventHandler)
				{
						onDownloadComplete += eventHandler;	
				}

				/// <summary>
				/// Removes the download complete eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				virtual public void RemoveDownloadCompleteEventhandler (OnDownloadCompleteEventHandler eventHandler)
				{
						onDownloadComplete -= eventHandler;	
				}

				/// <summary>
				/// Load the specified url.
				/// </summary>
				/// <param name="url">URL.</param>
				abstract public void Load (string url = null, int attempts = 0 );

				/// <summary>
				/// Determines whether this instance is complete.
				/// </summary>
				/// <returns><c>true</c> if this instance is complete; otherwise, <c>false</c>.</returns>
				abstract public bool IsComplete ();


				/// <summary>
				/// Releases all resource used by the <see cref="ARM.utils.download.DownloadEventsAbstract"/> object.
				/// </summary>
				/// <remarks>Call <see cref="Dispose"/> when you are finished using the
				/// <see cref="ARM.utils.download.DownloadEventsAbstract"/>. The <see cref="Dispose"/> method leaves the
				/// <see cref="ARM.utils.download.DownloadEventsAbstract"/> in an unusable state. After calling <see cref="Dispose"/>,
				/// you must release all references to the <see cref="ARM.utils.download.DownloadEventsAbstract"/> so the garbage
				/// collector can reclaim the memory that the <see cref="ARM.utils.download.DownloadEventsAbstract"/> was occupying.</remarks>
				abstract public void Dispose ();

				/// <summary>
				/// Gets the request.
				/// </summary>
				/// <returns>The request.</returns>
				abstract public WWW GetRequest ();

		}
}
