using UnityEngine;
using System.Collections;


namespace ARM.utils.download
{

		public class GenericDownloadManager : DownloaderAbstract
		{
				private static int realInstanceId = 0;

				protected static int _instanceID {
						get { 
								return realInstanceId++;
						}
				}

				GameObject _downloaderInstanceGO;
				GameObjectDownloader _downloader;
				//A variavel _isComplete já existe no parent.parent
				//bool _isComplete = false;

				override public void Load (string url = null, int extraAttempts = 0)
				{

						// gera o GO e instancia e zaz!
						if (_downloader == null) {
								int currentCount = _instanceID; 
								_downloaderInstanceGO = GameObject.Find ("_downloaderInstanceGO_" + currentCount);
								if (_downloaderInstanceGO != null)
										GameObject.DestroyImmediate (_downloaderInstanceGO.gameObject);
								_downloaderInstanceGO = new GameObject ("_downloaderInstanceGO" + currentCount);
								
								

								_downloader = _downloaderInstanceGO.AddComponent<GameObjectDownloader> ();

								_downloader.AddCompleteEventhandler (RaiseComplete);
								_downloader.AddProgressEventhandler (RaiseProgress);
								_downloader.AddErrorEventhandler (RaiseError);
								_downloader.AddDownloadCompleteEventhandler (RaiseDownloadComplete);
								_downloader.weight = weight;
								_downloader.Load (url);
						}

				}

//				virtual private void onProgress (float progress)
//				{
//						this.RaiseProgress (progress);
//				}
//
//				virtual  private void onError (string message)
//				{
//						this.RaiseError (message);
//				}
//
//				virtual private void onComplete ()
//				{
//						this.RaiseComplete ();
//				}
//
//				virtual private void onDownloadComplete (WWW request)
//				{
//						this.RaiseDownloadComplete (request);
//				}
//
				public override void Dispose ()
				{
						if (_downloader != null) {
								_downloader.Dispose ();
						}
				}

				override public bool IsComplete ()
				{
						return _isComplete;
				}

				override public WWW GetRequest ()
				{
						if (_downloader != null) {
								return _downloader.GetRequest ();
						}
						return null;
				}

				public override void prepare ()
				{
					base.RaiseComponentIsReady ();
				}

		}
}