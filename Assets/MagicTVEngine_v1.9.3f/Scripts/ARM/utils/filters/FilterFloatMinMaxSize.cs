﻿using UnityEngine;
using System.Collections;

using ARM.interfaces ;

namespace ARM.utils.filters{

	public class FilterFloatMinMaxSize : FilterFloatInterface {

		public FilterFloatMinMaxSize( ){
			//
		}

		public float filter( float value ){
			return value;
		}
	}

}