﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq ;
namespace ARM.utils
{
	public static class Math
	{

		public static float getMedian (int[] array)
		{
			int[] tempArray = array;
			int count = tempArray.Length;
			
			Array.Sort (tempArray);

			float medianValue = 0;
			
			if (count % 2 == 0) {
				// count is even, need to get the middle two elements, add them together, then divide by 2
				int middleElement1 = tempArray [(count / 2) - 1];
				int middleElement2 = tempArray [(count / 2)];
				medianValue = (middleElement1 + middleElement2) / 2;
			} else {
				// count is odd, simply get the middle element.
				medianValue = tempArray [(count / 2)];
			}

			return medianValue;
		}
		/// <summary>
		/// Gets the mean angle.
		/// </summary>
		/// <returns>The mean angle.</returns>
		/// <param name="array">Array.</param>
		public static float getMeanAngle (float[] array)
		{
			if (array.Length == 1) {
				return array [0];
			}
//			float mean = 0f ;

			//normalizando cada item
			//cria a lista com os floats mais presentes em um dos 4 setores ( 0->90 | 90->180 | 180->270 | 270->360 )
			int[] quadrants = new int[4] {0, 0, 0, 0};



			int currentQuadrant = 0;
			for (int i = 0; i < array.Length; i++) {

				array [i] = normalizeAngle (array [i]);

				currentQuadrant = Mathf.FloorToInt (array [i] / 90);
				//adiciona 
				quadrants [currentQuadrant] ++;

			}

			return 0f;
		}
		public static float getMedianAngle (float[] array)
		{
			if (array.Length == 1) {
				return array [0];
			}
			//fazer de dois em dois
			float medianResult = normalizeAngle (array [0]);

			for (int i = 1; i < array.Length; i++) {
				float current = array [i];
				if (getQuadrantOfAngle (medianResult) == getQuadrantOfAngle (current)) {
					//estao no mesmo quadrante entao é a media simples
					medianResult = (medianResult + current) / 2;
				}
//				float temp = medianResult - array[ i ] ;

				
			}
			return 0f;
		}
		public static int getQuadrantOfAngle (float angle)
		{
			angle = normalizeAngle (angle);
			return Mathf.FloorToInt (angle / 90);
		}
		public static float normalizeAngle (float angle)
		{
			while (angle >= 360) {
				angle -= 360;
			}
			while (angle < 0) {
				angle += 360;
			}
			return angle;
		}
		public static float getMedian (float[] array)
		{
			float[] tempArray = array;
			int count = tempArray.Length;
			
			Array.Sort (tempArray);

			float medianValue = 0;

			if (count % 2 == 0) {
				// count is even, need to get the middle two elements, add them together, then divide by 2
				float middleElement1 = tempArray [(count / 2) - 1];
				float middleElement2 = tempArray [(count / 2)];
				medianValue = (middleElement1 + middleElement2) / 2;
			} else {
				// count is odd, simply get the middle element.
				medianValue = tempArray [(count / 2)];
			}
			
			return medianValue;
		}
		
		public static Vector3 getMedian (Vector3[] array)
		{

			Vector3[] tempArray = array;

			int count = tempArray.Length;

			float[] arrayX = new float[ count ];
			float[] arrayY = new float[ count ];
			float[] arrayZ = new float[ count ];

			for (int i = 0; i < count; i++) {
				Vector3 item = tempArray [i];
				arrayX [i] = item.x;
				arrayY [i] = item.y;
				arrayX [i] = item.z;
			}

			Vector3 medianValue = new Vector3 ();

			medianValue.x = getMedian (arrayX);
			medianValue.y = getMedian (arrayY);
			medianValue.z = getMedian (arrayZ);

			return medianValue;
		}
		
		public static float getMean (float[] positions)
		{
			
			if (positions.Length == 0)
				return 0f;
			
			float x = 0f;
			foreach (float pos in positions) {
				x += pos;
			}
			return (float)(x / positions.Length);
		}

		public static Vector3 getMean (Vector3[] positions)
		{

			if (positions.Length == 0)
				return Vector3.zero;

			float x = 0f;
			float y = 0f;
			float z = 0f;
			foreach (Vector3 pos in positions) {
				x += pos.x;
				y += pos.y;
				z += pos.z;
			}
			return new Vector3 (x / positions.Length, y / positions.Length, z / positions.Length);
		}
		
		public static Quaternion getMean (Quaternion[] positions)
		{
			
			if (positions.Length == 0)
				return Quaternion.identity;
			
			float x = 0f;
			float y = 0f;
			float z = 0f;
			foreach (Quaternion pos in positions) {
				x += pos.x;
				y += pos.y;
				z += pos.z;
			}
			return new Quaternion (x / positions.Length, y / positions.Length, z / positions.Length, 0);
		}
		public static double getStandardDeviation (List<double> doubleList)
		{  

			double average = doubleList.Average ();  
			double sumOfDerivation = 0;  
			foreach (double value in doubleList) {  
				sumOfDerivation += (value) * (value);  
			}  

			double sumOfDerivationAverage = sumOfDerivation / (doubleList.Count - 1);

			return System.Math.Sqrt (sumOfDerivationAverage - (average * average));  
		}
		/// <summary>
		/// Inverts the position.
		/// </summary>
		/// <returns>The position.</returns>
		/// <param name="position">Position.</param>
		public static Vector3 InvertPosition (Vector3 position)
		{
			return new Vector3 (){
				x=-position.x,
				y=-position.y,
				z=-position.z};
		}
		/// <summary>
		/// Inverts the rotation.
		/// </summary>
		/// <returns>The rotation.</returns>
		/// <param name="rotation">Rotation.</param>
		public static Quaternion InvertRotation (Quaternion rotation)
		{
			return Quaternion.Euler (new Vector3 (){
				x=-rotation.eulerAngles.x,
				y=-rotation.eulerAngles.y,
				z=-rotation.eulerAngles.z
			});
		}
	}
}
