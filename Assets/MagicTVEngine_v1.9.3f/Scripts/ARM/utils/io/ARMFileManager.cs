using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;




/// <summary>
/// ARM file manager.
/// 
/// @version 1.1
/// @author Renato Miawaki
/// </summary>
namespace ARM.utils.io
{
	public partial class ARMFileManager : MonoBehaviour
	{
		
		// import a single C-function from our plugin
		[DllImport ("__Internal")]
		private static extern void _MTV_RemoveFromBackup (string filePath);
		
		public static bool WriteBites (string filePath, byte[] bytes)
		{
			//Debug.Log (" -- " + ARMFileManager.RemoveLastPathNode (filePath));
			
			ARMFileManager.CreateFolderRecursivelyForPath (ARMFileManager.RemoveLastPathNode (filePath));
			//System.IO.File.Open ( filePath, FileMode.OpenOrCreate);	
			System.IO.File.WriteAllBytes ( filePath, bytes ) ;

			//  FIX quando no EDITOR do uniyu na perspectiva IOS ele entra no UNITY_IPHONE  só deve entrar no compilado
			#if UNITY_EDITOR
						return ARMFileManager.Exists (filePath);
			#endif

			#if UNITY_IPHONE
						_MTV_RemoveFromBackup (filePath);
			#endif
			
			
			
			return ARMFileManager.Exists (filePath);
		}
		
		
		public static string RemoveLastPathNode (string path)
		{
			string[] nodes = path.Split (new Char [] {'/'});
			Array.Resize<string> (ref nodes, (nodes.Length - 1));
			return string.Join ("/", nodes);		                    
			
			
		}
		
		public static bool DeleteFolder (string str)
		{
			System.IO.Directory.Delete (str, true);
			//TODO fazer verificação de permissao antes e retornar os equivalentes
			return true;
		}
		
		public static bool DeleteFile (string filePath)
		{
			if (filePath == "" || filePath == null || !ARMFileManager.Exists (filePath)) {
				return true;
			}
			
			if (Directory.Exists (filePath)) {
				return false;
			}
			//			System.Security.AccessControl.
			//			System.IO.File.getac
			//			//Debug.LogWarning ( "ARMFileManager . DeleteFile ~ filePath "+filePath ) ;
			System.IO.File.Delete (filePath);		
			//			throw new System.NotImplementedException ();
			//TODO fazer verificação de permissao antes e retornar os equivalentes
			return true;
		}
		
		public static void CreateFolderRecursivelyForPath (string path)
		{
			if (System.IO.Directory.Exists (path)) {
				return;			
			}
			System.IO.Directory.CreateDirectory (path);
			
		}
		
		public static bool Exists (string filePath)
		{	
			//Debug.Log ("Exitst :" + filePath);
			return (System.IO.File.Exists (filePath) || System.IO.Directory.Exists (filePath));
		}
		
		public static string GetPersitentDataPath ()
		{
			
			
			#if UNITY_IPHONE
						return Application.persistentDataPath + "/Raw";
			#endif

			#if UNITY_EDITOR
			return Application.persistentDataPath ;
			#endif

			#if UNITY_ANDROID
						//			return  "jar:file://" + Application.persistentDataPath + "!/assets/";return  
						return  Application.persistentDataPath;
			#endif
			
			return Application.persistentDataPath;
		}
		public static string GetGetFileName (string filePath)
		{
			return Path.GetFileName (filePath);
		}
		public static string GetFileNameWithoutExtention (string fileNameOrPath)
		{
			return Path.GetFileNameWithoutExtension (fileNameOrPath);
		}
		
		public static string GetExtentionOfFile (string local_url)
		{
			return Path.GetExtension (local_url);
		}
		/// <summary>
		/// Gets the directory of file path.
		/// Retorna a pasta do arquivo
		/// </summary>
		/// <returns>The directory of file path.</returns>
		/// <param name="fullPath">Full path.</param>
		public static string GetDirectoryOfFilePath (string fullPath)
		{
			return GetDirectoryOfFilePath (fullPath, false);
		}
		public static string GetDirectoryOfFilePath (string fullPath, bool justParent)
		{
			//prepara um retorno padrao
			string returnedFolderName = fullPath;

			char[] splitchar = { '/' };
			//transforma as folders em lista
			List<string> string_values = fullPath.Split (splitchar).ToList ();

			if (fullPath.IndexOf (".") >= 0) {
				//tem arquivo, entao remove o ultimo item pois ele é o arquivo
				string_values.RemoveAt (string_values.Count - 1);

			}
			//retorno padrão para caso não queria só o ultimo
			returnedFolderName = string.Join ("/", string_values.ToArray ());
			if (justParent) {
				//quer o ultimo? entao pega apenas o último
				returnedFolderName = string_values [string_values.Count - 1];
			}
			return returnedFolderName;
		}
		public static string GetParentFolder (string path)
		{
			//remove o nome do arquivo, caso exista
			return GetDirectoryOfFilePath (path, true);
		}
		public static string[] GetChildDirectoryNames (string folder)
		{
			if (! Exists (folder)) {
				return null;
			}
			DirectoryInfo directoryInfo = new DirectoryInfo (folder);
			DirectoryInfo[] directories = directoryInfo.GetDirectories ();
			if (directories != null && directories.Length > 0) {
				return (from dir in directories  select dir.Name).ToArray ();
			}
			return null;
		}
		
		public static FileInfo[] GetFilesOfDirectory (string folder, string pattern)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo (folder);
			return directoryInfo.GetFiles (pattern);
		}
		public static FileInfo[] GetFilesOfDirectory (string folder)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo (folder);
			return directoryInfo.GetFiles ();
		}
		
		public static string[] GetFilesPathOfDirectory (string folder)
		{
			return Directory.GetFiles (folder);
		}
		
		public static byte[] GetBytes (string filePath)
		{
			if (!Exists (filePath)) {
				return null;
			}
			return System.IO.File.ReadAllBytes (filePath);
		}
	}
}
