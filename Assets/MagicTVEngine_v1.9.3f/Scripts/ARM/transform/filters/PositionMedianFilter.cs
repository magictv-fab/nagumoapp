﻿using UnityEngine;
using System.Collections;

using System;
using ARM.transform.filters.abstracts.histories;

using ARM.utils;
using ARM.utils.cron;

namespace ARM.transform.filters
{
	public class PositionMedianFilter : HistoryPositionFilterAbstract
	{

		public int maxSizeCache = 40;

		PoolVector3 _historyPosition;

		bool _hasItem = false;
		bool _loaded = false;
		public bool isFull = false;

		private bool _calcInProgress = false;

		private bool _lastMeanValueHasValue = false ;
		private Vector3 __lastMeanValue;
		private Vector3 _lastMeanValue {
			get { return __lastMeanValue;}
			set {
				_lastMeanValueHasValue = true;
				__lastMeanValue = value;
			}
		}


		//  public override bool byPass = false;

		bool _saving = false;
		// Use this for initialization
		void Start ()
		{
			if (this.maxSizeCache < 0) {
				this.maxSizeCache = 0;
				return;
			}
			this._historyPosition = new PoolVector3 ((uint)maxSizeCache);
			this._loaded = true;

		}

		public override void reset ()
		{
			this._historyPosition.reset ();
		}

		void Update ()
		{
			isFull = this._historyPosition.isFull ();
		}

		protected override Vector3 _doFilter (Vector3 last, Vector3 nextValue)
		{

			if (this.byPass) {
				return nextValue;
			}
			if (!this._hasItem) {
				return nextValue;
			}
			
			//o filtro só funciona se o pool tiver cheio
			if (!this._historyPosition.isFull ()) {
				return nextValue;
			}
			
			if (this._calcInProgress) {
				return this.lastMeanValue;
			}

			this._calcInProgress = true;
			
			this._lastMeanValue = this._historyPosition.getMedian ();
			
			backToWork ();
			
			return this.lastMeanValue;
		}
		
		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{

			return nextValue;
		}

		private Vector3 lastMeanValue {
			get {
				if (this._lastMeanValueHasValue == false) {
					//inicia como zero pra evitar erro
					this._lastMeanValue = Vector3.zero;
					//verifica se tem itens na lista

					if (this._historyPosition.getItens ().Length > 0) {
						//seta o primeiro item como mean value pois ainda está calculando
						this._lastMeanValue = this._historyPosition.getItens () [0];
					}
				}
				return this._lastMeanValue;
			}
		}


		void backToWork ()
		{

			this._calcInProgress = false;

		}



		public override void save (Vector3 position)
		{

			if (!this._loaded) {
				return;
			}
			if (this._saving) {

				return;

			}

			this._saving = true;
			this._historyPosition.addItem (position); 
			this._hasItem = true;

			backToSave ();

		}

		void backToSave ()
		{
			this._saving = false;
		}

	}
}