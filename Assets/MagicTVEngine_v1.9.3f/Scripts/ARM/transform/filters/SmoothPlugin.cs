﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

using ARM.device;

/**
 * @autor Renato Seiji Miawaki
 * 
 * @version 1.1.1
 * 
 * @description 
 * Tolerance time pass corigido pra nao ficar somando
 * 
 * */
/**
 * @autor Renato Seiji Miawaki
 * 
 * @version 1.1
 * 
 * @description 
 * Initial Tolerance Time funciona mesmo sem o toggle plugin
 * 
 * */

/**
 * @autor Renato Seiji Miawaki
 * 
 * @version 1.0
 * 
 * @description 
 * Essa classe é para união de plugins, sendo que essa classe em si não filtra nada.
 * 
 * 
 * */
using System.Collections.Generic;


namespace ARM.transform.filters
{
	public class SmoothPlugin : TransformPositionAngleFilterAbstract, ConfigurableListenerEventInterface
	{
		public AccelerometerEventDispacher accelerationShakeEvent ;
		public float initialToleranceTime = 1f;
		private float _toleranceTimePassed = 0f;
		public float tweenSpeedAngle = 0.2f;
		public float tweenSpeedPosition = 0.2f;

		protected float _resetTweenSpeedAngle = 0.2f;
		protected float _resetTweenSpeedPosition = 0.2f;
		protected float _resetInitialToleranceTime = 1f;

		protected bool _moveQuick = false;

		void Start ()
		{
			//Debug.LogWarning ("Smooth log");

			if (this.accelerationShakeEvent != null) {
				this.accelerationShakeEvent.addOnShakeInitListener (this._shakeInit);
				this.accelerationShakeEvent.addOnShakeEndListener (this._shakeEnd);
			}
			this._resetTweenSpeedAngle = (float) initialToleranceTime;
			this._resetTweenSpeedPosition = (float) tweenSpeedAngle;
			this._resetInitialToleranceTime = (float) tweenSpeedPosition;
		}

		protected void _shakeInit ()
		{
			this._moveQuick = true;
			this._toleranceTimePassed = 0f;
		}

		protected void _shakeEnd ()
		{

			this._toleranceTimePassed = 0f;
		}

		protected override void _On ()
		{	
			//Debug.LogWarning ("Metodo _On"); 
			this._toleranceTimePassed = 0f;
			this._active = false;

						
		}

		void Update ()
		{
			turnOffQuickMovement ();
		}

		protected void turnOffQuickMovement ()
		{
			//se nao estiver ativo, e ele tiver o plugin de track para poder verificar quando ativa e inativa...


			if (!this._active && this.initialToleranceTime > 0f && this._toleranceTimePassed < this.initialToleranceTime) {
				this._toleranceTimePassed += Time.deltaTime;
				if (this._toleranceTimePassed >= this.initialToleranceTime) {
					this.initialToleranceTime = 0f;
					this._active = true;
					this._moveQuick = false;
//										//Debug.LogWarning ("Metodo turnOffQuickMovement - if _toleranceTimePassed"); 
				}
				return;
			}
		}

		protected override Quaternion _doFilter (Quaternion last, Quaternion newPosition)
		{
			//Debug.Log ("SmoothPlugin Filtrando ");

			float distance = Vector3.Distance( last.eulerAngles, newPosition.eulerAngles ) ;
			if (this.tweenSpeedAngle > 0 && distance > 0.01f) {

				newPosition = Quaternion.Lerp (last, newPosition, this.tweenSpeedAngle);
			}

			return newPosition;
		}

		protected override Vector3 _doFilter (Vector3 last, Vector3 newPosition)
		{
			float distance = Vector3.Distance( last, newPosition ) ;
			if(distance < 0.01f){
				return newPosition;
			}
			return Vector3.Lerp (last, newPosition, this.tweenSpeedPosition);
		}
		/// <summary>
		/// Configs the by string.
		/// </summary>
		/// <param name="metadata">Metadata.</param>
		public void configByString (string metadata){
			List<float> floats = PipeToList.ParseReturningFloats (metadata);
			if(floats.Count > 0){
				initialToleranceTime = floats[0];
			}
			if(floats.Count > 1){
				tweenSpeedAngle = floats[1];
			}
			if(floats.Count > 2){
				tweenSpeedPosition = floats[2];
			}
		}
		public void Reset (){
			initialToleranceTime = this._resetTweenSpeedAngle;
			tweenSpeedAngle = this._resetTweenSpeedPosition;
			tweenSpeedPosition = this._resetInitialToleranceTime;
		}

	}
}