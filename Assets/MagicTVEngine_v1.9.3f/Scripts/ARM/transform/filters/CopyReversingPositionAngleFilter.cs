﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/// <summary>
/// @author Renato Seiji Miawaki
/// @version 1.0
/// @date Julho/2015
/// Plugin Filter to Copy position and/or angle.
/// 
/// </summary>
using ARM.utils;

public class CopyReversingPositionAngleFilter : CopyPositionAngleFilter
{
	protected override Vector3 doCopyPosition (Vector3 pos)
	{
		return Math.InvertPosition (pos);
	}
	protected override Quaternion doCopyRotation (Quaternion rot)
	{
		return Math.InvertRotation (rot);
	}
}
