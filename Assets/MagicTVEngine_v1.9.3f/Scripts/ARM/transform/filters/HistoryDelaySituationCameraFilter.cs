﻿using UnityEngine;
using System.Collections;
using ARM.device.abstracts;
using ARM.camera.abstracts;
using ARM.transform.filters.abstracts;

namespace ARM.transform.filters
{
	public class HistoryDelaySituationCameraFilter : TransformPositionAngleFilterAbstract
	{

		protected Quaternion _lastTargetAngle;
		protected Vector3 _lastTargetPosition;

		//Plugin que verifica se o tracking iniciou ou terminou, para modificar a flag que permite que ele se mova abruptamente
		public TrackEventDispatcherAbstract trackerEventPlugin;

		public bool _isActive;

		public float delayTime = 0.5f;

		private float _totalTimeElapsed = 0f;

		private bool _timer = false;

		public bool _timerEnabled = false;
		// Use this for initialization
		void Start ()
		{
			if (this.trackerEventPlugin == null) {
				throw new System.Exception ("ERRO DE CONFIGURAÇÃO DO HistoryDelaySituationCameraFilter ! adicione um TrackEventDispatcherAbstract ");
			}
			this.trackerEventPlugin.onTrackIn += trackIn;
			this.trackerEventPlugin.onTrackOut += trackOut;

		}

		protected void trackIn ()
		{
			Debug.LogWarning ("TrackIn");
		}

		protected void trackOut ()
		{

			Debug.LogWarning ("TrackOut");
			this._totalTimeElapsed = 0f;
			if (!this._timer) {
				this._timer = true;
			}
		}
		// Update is called once per frame
		void Update ()
		{
			_isActive = _active;
			this._timerEnabled = (this._timer && this.delayTime > 0);
//						Debug.LogWarning ("_timerEnabled: " + _timerEnabled);
			if (this._timerEnabled) {

				this._totalTimeElapsed += Time.deltaTime;
//								UnityEngine.Debug.LogWarning ("contando o tempo " + this._totalTimeElapsed);
//								Debug.LogWarning ("Active: " + _active);
				if (this._totalTimeElapsed >= this.delayTime) {
					//estorou o tempo limite
					this._timer = false;
				}
			}
		}

		protected override Quaternion _doFilter (Quaternion current, Quaternion next)
		{
			save (next);

			if (!this._timerEnabled) {
				return current;
			}
//						Debug.LogWarning ("Save do Quaternion _doFilter: " +next);
			return filter (next);
		}

		public Quaternion filter (Quaternion supoustCameraAngle)
		{
			//se não está com o timer ativo, não filtra, retorna o suposto
			if (!this._timerEnabled) {
				return supoustCameraAngle;
			}
			if (this._lastTargetAngle == null) {
				return supoustCameraAngle;
			}
			//chegou aqui está no timer, e deve filtrar.
			//Ou seja, manter a ultima registrada
			return this._lastTargetAngle;
		}

		public  Vector3 filter (Vector3 supoustCameraPosition)
		{
			//se não está com o timer ativo, não filtra, retorna o suposto
			if (!this._timerEnabled) {
				return supoustCameraPosition;
			}
			if (this._lastTargetPosition == null) {
				return supoustCameraPosition;
			}
			
			//chegou aqui está no timer, e deve filtrar.
			//Ou seja, manter a ultima registrada
			return this._lastTargetPosition;


		}

		protected override Vector3 _doFilter (Vector3 currentPosition, Vector3 newPosition)
		{
			save (newPosition);
//						Debug.LogWarning ("Save do Quaternion _doFilter: " +newPosition);
			if (!this._timerEnabled) {
				return currentPosition;
			}
			return filter (newPosition);
		}


		
		public void save (Quaternion angle)
		{
			if (this._timerEnabled) {
				return;
			}
			this._lastTargetAngle = angle;
		}

		public void save (Vector3 position)
		{
			if (this._timerEnabled) {
				return;
			}
			this._lastTargetPosition = position; 
		}
	}
}