﻿using UnityEngine;
using System.Collections;

using System;

using ARM.transform.filters.abstracts.histories;

using ARM.utils;

namespace ARM.transform.filters
{
	public class AngleMeanFilter : HistoryAngleFilterAbstract
	{

		public int maxSizeCache = 40;

		PoolQuaternion _history ;

		bool _hasItem = false;
		bool _loaded = false;
		// Use this for initialization

		public bool isFull = false;
				
		protected int _currentIndex = 0 ;
				
		void Start ()
		{
						
			if (this.maxSizeCache < 0) {
				this.maxSizeCache = 0;
				return;
			}
			this._history = new PoolQuaternion ((uint)this.maxSizeCache);
			this._loaded = true;
					
		}
		
		// Update is called once per frame

		public override void reset ()
		{
			this._history.reset ();
		}

		protected override Quaternion _doFilter (Quaternion currentPosition, Quaternion newValue)
		{
			if (this.byPass) {
				return newValue;
			}
			save (newValue);
			if (!this._hasItem) {
				return newValue;
			}


			if (!this._history.isFull ()) {
				return newValue;
			}
			return this._history.getMean ();
		}
		protected override Vector3 _doFilter (Vector3 currentValue, Vector3 nextValue)
		{
			return nextValue;
		}

		public override void save (Quaternion position)
		{

			if (!this._loaded) {
				return;
			}
						
			this._history.addItem (position);
			this.isFull = this._history.isFull ();
			this._hasItem = true;
		}

	}
}