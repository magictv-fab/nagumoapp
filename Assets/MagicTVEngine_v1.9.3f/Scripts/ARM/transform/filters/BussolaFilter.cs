﻿using UnityEngine;
using System.Collections;

using ARM.camera.abstracts;
using ARM.utils;
using ARM.transform.filters.abstracts;
using System.Collections.Generic;
/// <summary>
/// Bussola filter.
/// @author : Renato Seiji Miawaki
/// @version : 1.1
/// 
/// Configuravel por string utilizando pipe
/// </summary>
namespace ARM.transform.filters
{
	/// <summary>
	/// Realinha o eixo Y em direção da bussola, não controla os demais angulos
	/// </summary>
	public class BussolaFilter : TransformPositionAngleFilterAbstract, ConfigurableListenerEventInterface
	{

		public bool debugMode = false ;
		public float busolaDebugAngle = 0 ;
		public float outRotationAngle = 0f;
		public float outInclinationAngle = 0f;
		public float calibration = 0f;
		public Vector3 referencePoint = Vector3.zero;
		protected float _resetCalibration = 0f;
		// Use this for initialization
		void Start ()
		{
			_resetCalibration = calibration;
			Input.compass.enabled = true;

			this._init ();
						
		}
		public float roll;
		void Update(){
			Quaternion referenceRotation = Quaternion.identity;
			Quaternion deviceRotation = DeviceRotation.Get();
			Quaternion eliminationOfXY = Quaternion.Inverse(
				Quaternion.FromToRotation(referenceRotation * Vector3.forward, 
			                          deviceRotation * Vector3.forward)
				);
			Quaternion rotationZ = eliminationOfXY * deviceRotation;
			roll = rotationZ.eulerAngles.z;
		}
		public float magneticHeading = 0f;
		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{
			if (nextValue == null) {
				return nextValue;
			}
			Vector3 vec = nextValue.eulerAngles;
			vec.y = busolaDebugAngle ;
			if (Input.compass.enabled) {
				vec.y = Input.compass.magneticHeading ;
			}
			magneticHeading = vec.y;
			outRotationAngle = vec.y + calibration ;
			vec.y = outRotationAngle ;

			return Quaternion.Euler (vec);

		}
		public Vector3 DebugPositionNext ;
		/// <summary>
		/// não filtra posição, retorna o que recebeu
		/// </summary>
		/// <returns>The filter.</returns>
		/// <param name="last">Last.</param>
		/// <param name="nextValue">Next value.</param>
		protected override Vector3 _doFilter (Vector3 last, Vector3 nextValue)
		{
			return nextValue;
		}

		public Quaternion getCameraInclination ()
		{
			outInclinationAngle = Input.compass.magneticHeading;
			Debug.Log ("... outInclinationAngle " + outInclinationAngle);
			return Quaternion.Euler (0, Input.compass.magneticHeading, 0);
		}

		void OnGUI ()
		{
			if (! debugMode) {
				return;
			}
			GUILayout.Label ("gyro.gravity: " + Input.gyro.gravity
				+ "\n gyro.rotationRate: " + Input.gyro.rotationRate
				+ "\n gyro.attitude > " + Input.gyro.attitude  
				+ "\n compass.enabled:" + Input.compass.enabled
				+ "\n compass.magneticHeading:" + Input.compass.magneticHeading 
				+ "\n gyro.Enabled:" + Input.gyro.enabled 
				+ "\n timestamp:" + new Compass ().timestamp
				+ "\n compass.rawVector: " + Input.compass.rawVector.ToString ()
				+ "\n acceleration.magnitude: " + Input.acceleration.magnitude
				+ "\n acceleration.normalized: " + Input.acceleration.normalized
				+ "\n acceleration.sqrMagnitude: " + Input.acceleration.sqrMagnitude
				+ "\n acceleration: " + Input.acceleration
			    + "\n roll : "+roll
			);
		}
		/// <summary>
		/// Configs the by string.
		/// 
		/// </summary>
		/// <param name="metadata">Metadata.</param>
		public void configByString (string metadata){
			Vector3? vec = PipeToVector3.Parse (metadata);
			if(vec != null){
				referencePoint = (Vector3) vec;
			}
		}
		public void Reset (){
			referencePoint = Vector3.zero;
		}
	}
}