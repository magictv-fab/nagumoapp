﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters;

public class StaticPinchCameraZoomFilter : MonoBehaviour, ConfigurableListenerEventInterface
{
    public static  StaticPinchCameraZoomFilter Instance;

    public GameObject CameraToPinchControl;

    public float ZoomTotal;

    public float ZoomPinch;

    public float ZoomBonus;

    public float InitalCamerasDistance;

    public StaticPinchCameraZoomFilter()
    {
        Instance = this;
    }

    void LateUpdate()
    {

    }

	
	public string getUniqueName(){
		return this.gameObject.name;
	}
	public void configByString (string metadata){
		
	}
	public void Reset (){
		
	}
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}
}