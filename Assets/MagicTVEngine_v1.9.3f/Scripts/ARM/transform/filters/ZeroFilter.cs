﻿using UnityEngine;
using System.Collections;

using ARM.camera.abstracts;
using ARM.utils;
using ARM.transform.filters.abstracts;
using System.Collections.Generic;
/// <summary>
/// Bussola filter.
/// @author : Renato Seiji Miawaki | Marcelo Zani | Leal
/// @version : 1.0
/// 
/// Retorna zero para iniciar uma camada de filtros sempre com valor absoluto
/// </summary>
namespace ARM.transform.filters
{
	/// <summary>
	/// Realinha o eixo Y em direção da bussola, não controla os demais angulos
	/// </summary>
	public class ZeroFilter : TransformPositionAngleFilterAbstract, ConfigurableListenerEventInterface
	{
		public bool keepHistory;
		public GameObject arCamera;

		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{
			if(keepHistory){
				this._lastHistoryRotation = nextValue;
			}
			return this.arCamera.transform.rotation;

		}
		/// <summary>
		/// Retorna zero
		/// </summary>
		/// <returns>The filter.</returns>
		/// <param name="last">Last.</param>
		/// <param name="nextValue">Next value.</param>
		protected override Vector3 _doFilter (Vector3 last, Vector3 nextValue)
		{
			if(keepHistory){
				this._lastHistoryPosition = nextValue;
			}
			return arCamera.transform.position;
		}

		protected Quaternion _lastHistoryRotation ;

		protected Vector3 _lastHistoryPosition ;

		public override Quaternion filterCurrent (Quaternion cur)
		{
			if(!keepHistory){
				return cur;
			}
			return _lastHistoryRotation;
		}
		public override Vector3 filterCurrent (Vector3 cur)
		{
			if(!keepHistory){
				return cur;
			}
			return _lastHistoryPosition;
		}


		/// <summary>
		/// Configs the by string.
		/// </summary>
		/// <param name="metadata">Metadata.</param>
		public void configByString (string metadata){
			if(metadata==null || metadata==""){
				return;
			}
			bool b = false;
			bool.TryParse (metadata, out b);
			if (b) {
				this.keepHistory = b;
			}
		}
		public void Reset (){
			//padrão é false
			this.keepHistory = false;
		}
	}
}