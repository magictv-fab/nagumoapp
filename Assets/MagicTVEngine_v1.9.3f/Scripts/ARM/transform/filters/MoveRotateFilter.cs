﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/// <summary>
/// @author Renato Seiji Miawaki
/// @version 1.0
/// @date Agosto/2015
/// Plugin Filter to Move position and/or angle.
/// 
/// </summary>

public class MoveRotateFilter : TransformPositionAngleFilterAbstract
{
	public GameObject gameObjectToMove ;
	/// <summary>
	/// Se true, pega a global position, false usa localPosition
	/// </summary>
	public bool readGlobalPosition ;
	/// <summary>
	/// The write global position.
	/// </summary>
	public bool writeGlobalPosition ;
	/// <summary>
	/// The copy position. Se false ignora a posição.
	/// </summary>
	public bool movePosition = true ;
	/// <summary>
	/// The copy angle. Se false ignora o angulo.
	/// </summary>
	public bool moveAngle = true ;
	/// <summary>
	/// Se true só atualiza no LateUpdate
	/// </summary>
	public bool _changeOnLateUpdate = false ;

	/// <summary>
	/// The force to move position.
	/// </summary>
	public Vector3 forceToMovePosition = Vector3.zero;

	void Start(){
		if(this.gameObjectToMove==null){
			//seta ele mesmo caso não seja setado alguém
			this.gameObjectToMove = this.gameObject;
		}
	}
	public Vector3 forceToRotate = Vector3.zero;

	/// <summary>
	/// The transform this object too.
	/// Se true, muda a posição desse GameObject também, além de retornar o valor filtrado
	/// </summary>
	public bool TransformThisObjectToo = false ;

	Quaternion rotationToSend;

	Vector3 positionToSend;

	// Use this for initialization

	protected override Quaternion _doFilter (Quaternion currentValue, Quaternion nextValue)
	{
		if (!moveAngle) {
			return nextValue;
		}
		if (this.rotationToSend == null) {
			return nextValue;
		}
		return this.rotationToSend;
	}
	public Vector3 debugCurrent;
	public Vector3 debugNext;
	protected override Vector3 _doFilter (Vector3 currentValue, Vector3 nextValue)
	{
		debugCurrent = currentValue;
		debugNext = nextValue;
		if (!movePosition) {
			return nextValue;
		}
		if (this.positionToSend == null) {
			return nextValue;
		}
		return this.positionToSend;
	}
	// Update is called once per frame
	void Update ()
	{
		if (! this._changeOnLateUpdate) {
			doFollow ();
		}

	}
	void LateUpdate ()
	{
		if (this._changeOnLateUpdate) {
			doFollow ();
		}
	}
	

	void doFollow ()
	{
		if (!this._active) {
			return;
		}

		if (!movePosition && !moveAngle) {
			return;
		}
		if (gameObjectToMove != null) {
			Vector3 pos = this.gameObjectToMove.transform.localPosition;
			if (readGlobalPosition) {
				pos = this.gameObjectToMove.transform.position;
			}
			Quaternion rot = this.gameObjectToMove.transform.localRotation;
			if (readGlobalPosition) {
				rot = this.gameObjectToMove.transform.rotation;
			}
			if (movePosition) {
				this.positionToSend = this.doMovePosition (pos);
			}
			if (moveAngle) {
				this.rotationToSend = this.doRotate (rot);
			}
			//transforma a própria posição do objeto para debug
			if (this.TransformThisObjectToo) {
				if (this.writeGlobalPosition) {

					this.transform.position = this.positionToSend;
					this.transform.rotation = this.rotationToSend;
				} else {
					this.transform.localPosition = this.positionToSend;
					this.transform.localRotation = this.rotationToSend;
				}
			}
		}
	}

	protected virtual Quaternion doRotate (Quaternion rot)
	{
		if(forceToMovePosition != Vector3.zero){
			Vector3 r = rot.eulerAngles;
			return Quaternion.Euler( new Vector3( r.x + forceToMovePosition.x,r.y + forceToMovePosition.y, r.z + forceToMovePosition.z) ) ;
		}
		return new Quaternion( rot.x, rot.y, rot.z, rot.w ) ;
	}

	protected virtual Vector3 doMovePosition (Vector3 pos)
	{
		if(forceToMovePosition != Vector3.zero){
			return new Vector3( pos.x + forceToMovePosition.x,pos.y + forceToMovePosition.y,pos.z + forceToMovePosition.z) ;
		}
		return new Vector3( pos.x, pos.y, pos.z );
	}
}
