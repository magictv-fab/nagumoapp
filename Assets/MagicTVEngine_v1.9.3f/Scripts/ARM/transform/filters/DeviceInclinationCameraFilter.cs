﻿using UnityEngine;
using System.Collections;

using ARM.transform.filters.abstracts;
using System.Collections.Generic;

namespace ARM.transform.filters
{
	public class DeviceInclinationCameraFilter : TransformPositionAngleFilterAbstract, ConfigurableListenerEventInterface
	{

		private int sizeFilter = 10 ;
		private Vector3[] _filters;
		private Vector3 _filtersum = Vector3.zero;
		private int posFilter = 0;
		private int countSamples = 0;
		private Vector3 lastSample = Vector3.zero;

		public Vector3 calibration = Vector3.zero;
		// Use this for initialization
		void Start ()
		{
			_filtersum = this.transform.localRotation.eulerAngles;
						
			_init ();

		}

		protected Vector3 MovAverage (Vector3 sample)
		{

			if (countSamples == 0) {
				_filters = new Vector3[sizeFilter];
			}

			_filtersum += sample - _filters [posFilter];
			// push
			_filters [posFilter++] = sample;

			if (posFilter > countSamples) {
				countSamples = posFilter;
			}
			posFilter = posFilter % sizeFilter;

			return _filtersum / countSamples;

		}
		/// <summary>
		/// NÃO FILTRA POSIÇÃO
		/// </summary>
		/// <returns>The filter.</returns>
		/// <param name="currentValue">Current value.</param>
		/// <param name="nextValue">Next value.</param>
		protected override Vector3 _doFilter (Vector3 currentValue, Vector3 nextValue)
		{
			return nextValue;
		}
		public Vector3 DebugRotationLast;
		public Vector3 DebugRotationNext;
		
		public Vector3 DebugRotationOut;
		/// <summary>
		/// Rotaciona nos eixos, mas não na bussola. Adicione a bussola para funcionar
		/// </summary>
		/// <returns>The filter.</returns>
		/// <param name="currentValue">Current value.</param>
		/// <param name="supoustCameraPosition">Supoust camera position.</param>

		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{
			DebugRotationLast = last.eulerAngles ;
			DebugRotationNext = nextValue.eulerAngles ;
			
			Vector3 vec = -MovAverage (Input.acceleration.normalized);
//			Vector3 vec = - Input.acceleration.normalized ;
			vec.x = -vec.x;
			this.transform.up = vec;
			Vector3 vecReturn = this.transform.localRotation.eulerAngles + this.calibration;
			DebugRotationOut = vecReturn;
			return Quaternion.Euler (vecReturn);
						
		}
		public void configByString (string metadata){
			Vector3? v = PipeToVector3.Parse (metadata);
			if(v==null){
				return;
			}
			this.calibration = (Vector3)v;
			List<string> metas = PipeToList.Parse (metadata);
			if(metas.Count > 3){
				if( metas[3] == "True"){
					this.DebugFiltering = true;
				}
			}
		}
		public void Reset (){
			this.calibration = Vector3.zero;
			this.DebugFiltering = false;
		}
		void OnGUI ()
		{
			if (this.DebugFiltering) {
				GUI.Box (new Rect (300, 200, 200, 200), " " + Input.compass.magneticHeading.ToString ());
			}
		}
	}
}