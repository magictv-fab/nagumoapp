﻿using UnityEngine;
using System.Collections;

/// <summary>
/// @autor : Renato Seiji Miawaki
/// @version : 1.0
/// Bussola plugin rotation calibrate.
/// </summary>
using ARM.abstracts;

/// <summary>
/// @author: Renato Seiji Miawaki
/// 
/// @version: 1.0
/// 
/// Bussola plugin rotation calibrate.
/// </summary>
using ARM.utils.cron;
using ARM.transform.filters.abstracts;
using MagicTV.globals.events;


namespace ARM.transform.filters.plugins
{
	public class BussolaPluginRotationCalibrate : MonoBehaviour, OnOffListenerEventInterface
	{
		/// <summary>
		/// The bussola filter to change calibration property value.
		/// </summary>
		public BussolaFilter bussolaFilter ;
		public TransformPositionAngleFilterAbstract objectReferenceToToggleOn;
		protected bool _bussolaFilterActive ;
		/// <summary>
		/// Reference to default angle
		/// </summary>
		public float rotationReference = 0f ;

		/// <summary>
		/// The time to auto off.
		/// After this time, in seconds, this plugin autoOff 
		/// </summary>
		public float timeToAutoOff = 0.1f;

		/// <summary>
		/// Só filtra se tiver ativo
		/// </summary>
		public bool _isActive ; 
		public bool byPass;

		protected bool _hasObjects ;
		private int _timeout;
		// Use this for initialization
		void Start ()
		{
			_hasObjects = (bussolaFilter != null);
			if(_hasObjects){
				_bussolaFilterActive = bussolaFilter._active;
			}
			AppRootEvents.GetInstance().onRaiseStopPresentation += reset;
		}

		void reset ()
		{
			this.objectReferenceToToggleOn._active = false;
			this.objectReferenceToToggleOn.byPass = true;

		}

		void _turnOff ()
		{
			this._isActive = false ;
		}

		void _turnOn ()
		{
			this._isActive = true;

			if (this.timeToAutoOff > 0) {
				EasyTimer.ClearInterval ( _timeout ) ;
				_timeout = EasyTimer.SetTimeout ( _turnOff, Mathf.FloorToInt (this.timeToAutoOff * 1000), "BussolaAutoTurnOffTimeout");
			}
		}
	
		// Update is called once per frame
		void Update ()
		{

			if (_hasObjects) {
				this.checkToggle();
				this.calibrateBussola ();
			}
		}

		void checkToggle ()
		{
			if(_bussolaFilterActive == objectReferenceToToggleOn._active){
				return;
			}
			_bussolaFilterActive = objectReferenceToToggleOn._active;
			if(_bussolaFilterActive){
				_turnOn();
				return;
			}
			_turnOff ();
		}

		public bool DebugCalibrando ;
		void calibrateBussola ()
		{
			DebugCalibrando = false;
			if (!_isActive) {
				return;
			}
			if(byPass){
				return;
			}
			DebugCalibrando = true;
			//fazendo a calibragem automática baseado na rotação do ARCamera
			//calibrar para o ponto zero
			bussolaFilter.calibration = -bussolaFilter.magneticHeading;
			//bussolaFilter.calibration = rotationReference - bussolaFilter.outRotationAngle;

		}
		public void turnOn(){
			byPass = false;
		}
		public void turnOff(){
			byPass = true;
		}
		public string getUniqueName(){
			return this.gameObject.name;
		}
		public void ResetOnOff (){
			byPass = false;
		}
		public GameObject getGameObjectReference(){
			return this.gameObject;
		}
		/// <summary>
		/// Retorna true se esse componente estiver ativo
		/// </summary>
		/// <returns><c>true</c>, if active was ised, <c>false</c> otherwise.</returns>
		public bool isActive(){
			return (this._isActive && !this.byPass);
		}
	}
}