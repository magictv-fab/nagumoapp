﻿using UnityEngine;
using System.Collections;

using ARM.transform.filters.abstracts;
using ARM.transform.filters.abstracts.histories;

/**
 * Faz o filtro com os plugins que tem.
 * Essa classe só junta 2 plugins para ser acessado de forma única no SmartCamera
 * */
namespace ARM.transform.filters
{

	public class HistorySituationCamera : TransformPositionAngleFilterAbstract
	{

		public HistoryPositionFilterAbstract positionFilter;

		public HistoryAngleFilterAbstract angleFilter;

		
		public bool _hasPositionFilter = false;
		public bool _hasAngleFilter = false;
		//CONTROLAR A FREQUENCIA INTERNA E FILTRAR NO UPDATE

		public bool _isActive = false ;
		// Use this for initialization
		void Start ()
		{

			if (this.positionFilter != null) {
				this._hasPositionFilter = true;
			}
			if (this.angleFilter != null) {
				this._hasAngleFilter = true;
			}
			this._init ();

		}
		void Update ()
		{
			this._isActive = this._active;
		}
		protected override void _Off ()
		{
			//Debug.Log (" HistorySituationCamera . _Off  ");
			this.resetFilters ();
			base._Off ();
		}

		public void resetFilters ()
		{
			//Debug.Log (" HistorySituationCamera . resetFilter  ");
			if (this._hasPositionFilter) {
				//Debug.Log (" HistorySituationCamera . resetFilter this._hasPositionFilter ");
				this.positionFilter.reset ();
			}
			if (this._hasAngleFilter) {
				//Debug.Log (" HistorySituationCamera . resetFilter this._hasAngleFilter ");
				this.angleFilter.reset ();
			}
		}


		protected override Vector3 _doFilter (Vector3 last, Vector3 nextValue)
		{
						
			if (this._hasPositionFilter) {
				this.positionFilter.save (nextValue);
				return this.positionFilter.filter (last, nextValue);
			}
			return nextValue;
		}

		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{
						
			if (this._hasAngleFilter) {
				this.angleFilter.save (nextValue);
				return this.angleFilter.filter (last, nextValue);
			}
			return nextValue;
		}
	}
}