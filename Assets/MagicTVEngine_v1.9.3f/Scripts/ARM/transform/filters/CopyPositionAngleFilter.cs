﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/// <summary>
/// @author Renato Seiji Miawaki
/// @version 1.0
/// @date Julho/2015
/// Plugin Filter to Copy position and/or angle.
/// 
/// </summary>

public class CopyPositionAngleFilter : TransformPositionAngleFilterAbstract
{
	public GameObject gameObjectToCopy ;
	/// <summary>
	/// Se true, pega a global position, false usa localPosition
	/// </summary>
	public bool readGlobalPosition ;
	/// <summary>
	/// The write global position.
	/// </summary>
	public bool writeGlobalPosition ;
	/// <summary>
	/// The copy position. Se false ignora a posição.
	/// </summary>
	public bool copyPosition = true ;
	/// <summary>
	/// The copy angle. Se false ignora o angulo.
	/// </summary>
	public bool copyAngle = true ;
	/// <summary>
	/// Se true só atualiza no LateUpdate
	/// </summary>
	public bool _changeOnLateUpdate = false ;

	/// <summary>
	/// The transform this object too.
	/// Se true, muda a posição desse GameObject também, além de retornar o valor filtrado
	/// </summary>
	public bool TransformThisObjectToo = false ;

	Quaternion rotationToSend;

	Vector3 positionToSend;

	// Use this for initialization

	protected override Quaternion _doFilter (Quaternion currentValue, Quaternion nextValue)
	{
		if (!copyAngle) {
			return nextValue;
		}
		if (this.rotationToSend == null) {
			return nextValue;
		}
		return this.rotationToSend;
	}
	public Vector3 debugCurrent;
	public Vector3 debugNext;
	protected override Vector3 _doFilter (Vector3 currentValue, Vector3 nextValue)
	{
		debugCurrent = currentValue;
		debugNext = nextValue;
		if (!copyPosition) {
			return nextValue;
		}
		if (this.positionToSend == null) {
			return nextValue;
		}
		return this.positionToSend;
	}
	// Update is called once per frame
	void Update ()
	{
		if (! this._changeOnLateUpdate) {
			doFollow ();
		}

	}
	void LateUpdate ()
	{
		if (this._changeOnLateUpdate) {
			doFollow ();
		}
	}
	

	void doFollow ()
	{
		if (!this._active) {
			return;
		}

		if (!copyPosition && !copyAngle) {
			return;
		}
		if (gameObjectToCopy != null) {
			Vector3 pos = this.gameObjectToCopy.transform.localPosition;
			if (readGlobalPosition) {
				pos = this.gameObjectToCopy.transform.position;
			}
			Quaternion rot = this.gameObjectToCopy.transform.localRotation;
			if (readGlobalPosition) {
				rot = this.gameObjectToCopy.transform.rotation;
			}
			if (copyPosition) {
				this.positionToSend = this.doCopyPosition (pos);
			}
			if (copyAngle) {
				this.rotationToSend = this.doCopyRotation (rot);
			}
			//transforma a própria posição do objeto para debug
			if (this.TransformThisObjectToo) {
				if (this.writeGlobalPosition) {

					this.transform.position = this.positionToSend;
					this.transform.rotation = this.rotationToSend;
				} else {
					this.transform.localPosition = this.positionToSend;
					this.transform.localRotation = this.rotationToSend;
				}
			}
		}
	}

	protected virtual Quaternion doCopyRotation (Quaternion rot)
	{
		return new Quaternion( rot.x, rot.y, rot.z, rot.w ) ;
	}

	protected virtual Vector3 doCopyPosition (Vector3 pos)
	{
		return new Vector3( pos.x, pos.y, pos.z );
	}
}
