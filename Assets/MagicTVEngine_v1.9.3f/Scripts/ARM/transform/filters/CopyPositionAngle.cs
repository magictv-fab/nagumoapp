﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @author Renato Seiji Miawaki
/// @version 1.0
/// @date Julho/2015
/// Copy position and/or angle.
/// 
/// </summary>
public class CopyPositionAngle : MonoBehaviour
{
	public GameObject gameObjectToCopy ;
	/// <summary>
	/// Se true, pega a global position, false usa localPosition
	/// </summary>
	public bool globalPosition ;

	/// <summary>
	/// The copy position. Se false ignora a posição.
	/// </summary>
	public bool copyPosition = true ;
	/// <summary>
	/// The copy angle. Se false ignora o angulo.
	/// </summary>
	public bool copyAngle = true ;
	/// <summary>
	/// Se true só atualiza no LateUpdate
	/// </summary>
	public bool _changeOnLateUpdate = false;
	// Use this for initialization
	void Start ()
	{
		//
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (! this._changeOnLateUpdate) {
			doFollow ();
		}

	}
	void LateUpdate ()
	{
		if (this._changeOnLateUpdate) {
			doFollow ();
		}
	}
	void doFollow ()
	{
		if (!copyPosition && !copyAngle) {
			return;
		}
		if (gameObjectToCopy != null) {
			if (globalPosition) {
				if (copyPosition) {
					this.transform.position = this.gameObjectToCopy.transform.position;
				}
				if (copyAngle) {
					this.transform.rotation = this.gameObjectToCopy.transform.rotation;
				}
				return;
			}
			if (copyPosition) {
				this.transform.localPosition = this.gameObjectToCopy.transform.localPosition;
			}
			if (copyAngle) {
				this.transform.localRotation = this.gameObjectToCopy.transform.localRotation;
			}
		}
	}
}
