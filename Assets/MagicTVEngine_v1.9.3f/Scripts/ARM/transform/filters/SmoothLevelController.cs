﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System.Linq;
using ARM.device;

namespace ARM.transform.filters
{


	/// <summary>
	/// A idéia é o smooth ficar mais sensivel ( mais devagar ) se o mobile estiver parado
	/// E o smooth ficar mais rápido, se o device estiver se movendo, e quanto mais se mover mais rápido ele permite
	/// Smooth level controller.
	/// </summary>
	public class SmoothLevelController : MonoBehaviour
	{

		public SmoothPlugin smoothFollowToControl ;

		public AccelerometerEventDispacher accelerometerEventDispatcher ;

		private bool _hasAccelerometer = false ;

		private bool _hasSmooth = false ;

		public float angleRatio = 1f ;

		public float positionRatio = 1f ;

		public float angleMinSmooth = 0.1f ;
		public float angleMaxSmooth = 0.01f ;

		public float positionMinSmooth = 0.1f ;
		public float positionMaxSmooth = 0.01f ;
		
		protected float _difMinMaxPosition = 0f ;
		protected float _difMinMaxAngle = 0f ;
		
		private float _defaultSpeedPosition = 0f ;
		private float _defaultSpeedAngle = 0f ;

		public float debugMag = 0f;
		// Use this for initialization
		void Start ()
		{

			this._hasSmooth = (this.smoothFollowToControl != null);

			this._hasAccelerometer = (this.accelerometerEventDispatcher != null);

			if (this._hasSmooth && this._hasAccelerometer) {

				this.accelerometerEventDispatcher.addOnMoveListener (OnDeviceMoveEventHandler);

				this.accelerometerEventDispatcher.addOnMoveEndListener (ReturnDefaultEventHandler);

				this._defaultSpeedAngle = this.smoothFollowToControl.tweenSpeedAngle;
				this._defaultSpeedPosition = this.smoothFollowToControl.tweenSpeedPosition;

			}
			this._difMinMaxPosition = Mathf.Abs (this.positionMaxSmooth - this.positionMinSmooth);
			this._difMinMaxAngle = Mathf.Abs (this.angleMaxSmooth - this.angleMinSmooth);
		}

		void ReturnDefaultEventHandler ()
		{
			this.smoothFollowToControl.tweenSpeedAngle = this._defaultSpeedAngle;
			this.smoothFollowToControl.tweenSpeedPosition = this._defaultSpeedPosition;

		}

		void OnDeviceMoveEventHandler (Vector3 totalMove)
		{

			//limite de 1
			List<float> mags = new List<float> (){ Mathf.Abs ( totalMove.x ), Mathf.Abs ( totalMove.y ) , Mathf.Abs ( totalMove.z ) };

			float magnitude = mags.Max ();

			this.debugMag = magnitude;

			float shakeAngle = magnitude * this.angleRatio;
			float shakePosition = magnitude * this.positionRatio;

			//muda o tween de angulo
			float angle = this.angleMinSmooth + shakeAngle;
			if (angle > this.angleMaxSmooth) {
				angle = this.angleMaxSmooth;
			}
			this.smoothFollowToControl.tweenSpeedAngle = angle;

			//muda o speed de posição
			float position = this.positionMinSmooth + shakePosition;
			if (position > this.positionMaxSmooth) {
				position = this.positionMaxSmooth;
			}
			this.smoothFollowToControl.tweenSpeedPosition = position;

		}
		
		// Update is called once per frame
		void Update ()
		{
			
		}

	}

}