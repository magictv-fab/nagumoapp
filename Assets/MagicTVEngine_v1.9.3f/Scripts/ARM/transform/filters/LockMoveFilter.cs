﻿using UnityEngine;
using System.Collections;

using ARM.transform.filters.abstracts ;

namespace ARM.transform.filters {

	public class LockMoveFilter : TransformPositionAngleFilterAbstract {
		
		protected bool _lastAngleHasValue;
		protected Quaternion _lastAngleValue;
		protected Quaternion _lastAngle {
			get {return _lastAngleValue;}
			set{
				_lastAngleHasValue = true;
				_lastAngleValue = value;
			}
		}
		
		protected bool _lastPositionHasValue;
		protected Vector3 _lastPositionValue;
		protected Vector3 _lastPosition {
			get {return _lastPositionValue;}
			set{
				_lastPositionHasValue = true ;
				_lastPositionValue = value ;
			}
		}

		public bool isWorking = false ;

		protected override void _init ()
		{
			//nesse plugin não faz sentido iniciar como true por default, então força para false
			this.activeByDefault = false ;
			base._init ();
		}

		protected override void _On ()
		{
			base._On ();
		}

		protected override void _Off ()
		{
			base._Off ();
		}
		public override Quaternion filter (Quaternion currentValue, Quaternion nextValue)
		{
			if( this._active && this._lastAngleHasValue != false ){
				//está inativo, da LOCK mostrando a ultima posição quando ele estava ativo
				this.isWorking = false ;

				return this._lastAngle ;
			}
			this.isWorking = true ;
			this._lastAngle =  base.filter (currentValue, nextValue);
			return this._lastAngle ;
		}

		public override Vector3 filter (Vector3 currentValue, Vector3 nextValue)
		{
			if( this._active && this._lastPositionHasValue != false ){
				//está inativo, da LOCK mostrando a ultima posição quando ele estava ativo
				return this._lastPosition ;
			}

			this._lastPosition =  base.filter (currentValue, nextValue);

			return this._lastPosition ;
		}
		protected override Vector3 _doFilter ( Vector3 last, Vector3 nextValue ) {

			return nextValue;

		}
		
		protected override Quaternion _doFilter ( Quaternion last, Quaternion nextValue ) {

			return nextValue;

		}
	}

}