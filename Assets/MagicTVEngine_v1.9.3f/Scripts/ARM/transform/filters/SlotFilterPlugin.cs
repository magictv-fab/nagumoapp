﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/**
 * @autor Renato Seiji Miawaki
 * 
 * @version 1.0
 * 
 * @description 
 * Essa classe é para união de plugins, sendo que essa classe em si não filtra nada.
 * 
 * 
 * */

/* 
 * 
 * @autor Renato Seiji Miawaki
* 
* @version 1.1
* 
* @description 
* Modificações para aplicar, ou não, a modificação dos filtros desse slot, no gameobject em que ele está atachado
* Quando for usar só como um grupo de filtros, não precisa modificar o objeto atachado, já quando for o objetivo modificar o objeto
* aí sim precisa
* 
* 
* */
namespace ARM.transform.filters
{
	public class SlotFilterPlugin : TransformPositionAngleFilterAbstract
	{

		
		
		public Vector3 _lastPosition ;
		public Quaternion _lastAngle ;
		public bool applyInThisObject = true ;
		public bool returnThisValues ;
		bool _isLoaded = false ;

		Vector3 position;

		Quaternion rotation;

		public Vector3 filteredNextValuePosition ;
		public Quaternion filteredNextValueRotation ;

		void Start ()
		{
			this._init ();
			this._isLoaded = true;
		}

		protected override Vector3 _doFilter (Vector3 last, Vector3 nextValue)
		{
			if(returnThisValues){
				if(this._lastPosition != null && this._isLoaded ){
					return this._lastPosition;
				}
			}
			this.filteredNextValuePosition = nextValue ;
			return nextValue ;
		}

		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{
			if(returnThisValues){
				if(this._lastAngle != null && this._isLoaded ){
					return this._lastAngle;
				}
			}
			this.filteredNextValueRotation = nextValue;
			return nextValue;
		}
		public Vector3 DebugRotationLast;
		public Vector3 DebugRotationNext;
		
		public Vector3 DebugRotationOut;

		public Vector3 DebugPositionLast;

		public Vector3 DebugPositionNew;

		void LateUpdate ()
		{
			if (! this._isLoaded || !this.applyInThisObject) {
				return;
			}
			if (this._lastAngle != null) {
				DebugRotationLast = this._lastAngle.eulerAngles;
				DebugRotationNext = this.transform.localRotation.eulerAngles;
			}
			if(this._lastPosition != null){
				DebugPositionLast = this._lastPosition;
				DebugPositionNew = this.transform.position;
			}
			this.position = filter ( this._lastPosition, this.transform.position ) ;

			this.rotation = filter ( this._lastAngle, this.transform.rotation ) ;

			if (this.applyInThisObject) {
				this.transform.position = this.position;
				this.transform.rotation = this.rotation;
			}

			this._lastPosition = new Vector3( this.position.x, this.position.y, this.position.z );
			this._lastAngle = new Quaternion( this.rotation.x, this.rotation.y, this.rotation.z, this.rotation.w);

		}
	}
}