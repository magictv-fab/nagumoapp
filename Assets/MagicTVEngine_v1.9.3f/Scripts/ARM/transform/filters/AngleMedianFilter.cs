﻿using UnityEngine;
using System.Collections;

using System;

using ARM.transform.filters.abstracts.histories;

using ARM.utils;
using System.Collections.Generic;

namespace ARM.transform.filters
{
	public class AngleMedianFilter : HistoryAngleFilterAbstract, ConfigurableListenerEventInterface
	{

		public int maxSizeCache = 40;

		private int _intialMaxSizeCache ;
		PoolQuaternion _history;

		bool _hasItem = false;
		public bool _loaded = false;
		// Use this for initialization

		public bool isFull = false;


		void Start ()
		{
			this._intialMaxSizeCache = this.maxSizeCache;
			if (this.maxSizeCache < 0) {
				this.maxSizeCache = 0;
				return;
			}
			this._history = new PoolQuaternion ((uint)maxSizeCache);
			this._loaded = true;
		}
		
		// Update is called once per frame
		void Update ()
		{
			if(this._history != null){
				isFull = this._history.isFull ();
			}
		}

		public override void reset ()
		{
			this._history.reset ();
		}
		//		void OnGUI () {
		//
		//			GUI.Box (new Rect (0, 0, 140, 50), this._history.getLastPosition().ToString() );
		//
		//			GUI.Box (new Rect (0, 60, 140, 50), this._history.getMedian().ToString() );
		//
		//			GUI.Box (new Rect (0, 120, 140, 50), this._history.getCurrentSize().ToString() );
		//
		//		}
		protected override Quaternion _doFilter (Quaternion currentPosition, Quaternion newValue)
		{
			if (this.byPass) {
				return newValue;
			}
			save (newValue);
			if (!this._hasItem) {
				return newValue;
			}


			if (!this._history.isFull ()) {
				return newValue;
			}
			Quaternion mediana = this._history.getMedian ();
			
			return mediana;

		}
		protected override Vector3 _doFilter (Vector3 currentValue, Vector3 nextValue)
		{
			return nextValue;
		}
		public override void save (Quaternion position)
		{

			if (!this._loaded) {
				return;
			}
			if(this._history==null){
				return;
			}
			this.isFull = this._history.isFull ();
			this._history.addItem (position);
			this._hasItem = true;
		}
		public void configByString (string metadata){
			List<int> ints = PipeToList.ParseReturningInts (metadata);
			if(ints.Count > 0){
				this.maxSizeCache = ints[0] ;
				changeHistorySize(this.maxSizeCache);
			}
		}
		public void Reset (){
			if(this.maxSizeCache == this._intialMaxSizeCache){
				return;
			}
			this.maxSizeCache = this._intialMaxSizeCache;
			changeHistorySize(this.maxSizeCache);
		}
		protected void changeHistorySize( int size ){

			this._loaded = false;
			bool lastBypass = this.byPass;
			this.byPass = false;
			this._history = new PoolQuaternion ((uint)size);

			this._loaded = true;
			this.byPass = lastBypass;
		}

	}
}