﻿using UnityEngine;
using System.Collections;

namespace ARM.transform.filters.interfaces {
		public interface TransformPositionAngleFilterInterface{
		
				Vector3 filter ( Vector3 last, Vector3 nextValue ) ;

				Quaternion filter ( Quaternion last, Quaternion nextValue ) ;

		}
}