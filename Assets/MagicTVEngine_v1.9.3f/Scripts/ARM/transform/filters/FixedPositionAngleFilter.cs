﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/* 
 * 
 * @autor Renato Miawaki
* 
* @version 1.0
* 
* @description 
* Trava a posição copnforme positionToLock e rotationToLock
* 
* */
/* 
 * 
 * @autor Renato Miawaki
* 
* @version 1.1
* 
* @description 
* Agora é configuravel
* 
* */
namespace ARM.transform.filters
{
	public class FixedPositionAngleFilter : TransformPositionAngleFilterAbstract, ConfigurableListenerEventInterface
	{

		public Vector3 positionToLock = Vector3.zero ;
		public Vector3 rotationToLock = Vector3.zero ;

		protected Quaternion _lastHistoryRotation ;
		
		protected Vector3 _lastHistoryPosition ;

		public bool keepHistory ;

		void Start ()
		{
			this._init () ;
		}

		protected override Vector3 _doFilter (Vector3 last, Vector3 nextValue)
		{
			if( keepHistory ) {
				this._lastHistoryPosition = nextValue ;
			}
			return positionToLock ;
		}

		protected override Quaternion _doFilter (Quaternion last, Quaternion nextValue)
		{
			if( keepHistory ) {
				this._lastHistoryRotation = nextValue ;
			}
			return Quaternion.Euler( rotationToLock ) ;
		}

		
		public override Quaternion filterCurrent (Quaternion cur)
		{
			if(!keepHistory){
				return cur;
			}
			return _lastHistoryRotation;
		}
		public override Vector3 filterCurrent (Vector3 cur)
		{
			if(!keepHistory){
				return cur;
			}
			return _lastHistoryPosition;
		}
		public void configByString (string metadata){
			if(metadata == ""){
				return;
			}
			Vector3[] config = PipeToArrayOfVector3.Parse (metadata);
			if(config.Length > 0){
				this.positionToLock = config[0];
			}
			if(config.Length > 1){
				this.rotationToLock = config[1];
			}
		}
		public void Reset (){
			this.positionToLock = Vector3.zero;
			this.rotationToLock = Vector3.zero;
		}
	}
}