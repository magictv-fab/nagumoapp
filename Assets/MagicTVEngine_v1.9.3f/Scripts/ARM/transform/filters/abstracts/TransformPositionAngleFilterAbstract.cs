﻿using UnityEngine;
using System.Collections;

using ARM.transform.filters.interfaces;

using ARM.abstracts;
/**
 * @autor Renato Seiji Miawaki
 * 
 * @version 1.0
 * 
 * @description 
 * Essa classe abstrada seria para plugins de filtros recursivos para filtrar posição e angulo de um GameObject
 * 
 * Com isso da para fazer combinações parecidas com circuitos eletricos e criar a própria lógica para o funcionamento dos plugins,
 * sobrepostos ou em fila e cada um com sua regra de ligar ou desligar e a hierarquia fica por conta da order ou nivel de profundidade
 * recursiva da disposição dos plugins
 * 
 * as classes que extendem essa abstrata precisa implementar _doFilter Vector3 e Quaternion
 * 
 * */
namespace ARM.transform.filters.abstracts
{
	public abstract class TransformPositionAngleFilterAbstract : MonoBehaviour, TransformPositionAngleFilterInterface, OnOffListenerEventInterface
	{
		/// <summary>
		/// The alias for debug and see on UnityEditor
		/// </summary>
		public string Alias = "";
		public TransformPositionAngleFilterAbstract[] filters;

		public bool _hasFilter = false;

		public ToggleOnOffAbstract toggle;

		protected bool _hasToggle = false;

		public bool _active = true;

		protected bool _started = false;

		public bool byPass = false;

		public bool activeByDefault = true;

		private bool _initCalled = false ;

		private static int _countFilters = 0 ;
		void Start ()
		{	
			_init ();	
		}
		protected virtual void _init ()
		{
			if (this._initCalled) {
				return;
			}
			this._initCalled = true;
			defineAliasIfNotExists ();
			if (this.filters != null && this.filters.Length > 0) {
				this._hasFilter = true;
			}
			this._active = activeByDefault;
			if (this.toggle != null) {
				this.toggle.addTurnOnChange (this._On);
				this.toggle.addTurnOffChange (this._Off);
			}
		}

		void defineAliasIfNotExists ()
		{
			if (Alias == "") {
				Alias = "undefined_" + _countFilters.ToString ();
			}
			//soma a variavel estática a privada para não ter nomes repetidos
			_countFilters++;
		}

		protected virtual void _On ()
		{

			this._active = true;

		}

		protected virtual void _Off ()
		{

			this._active = false;

		}

		public bool DebugFiltering;	

		public virtual Vector3 filterCurrent( Vector3 cur ){
			return cur;
		}
		public virtual Quaternion filterCurrent( Quaternion cur ){
			return cur;
		}

		public bool filterThisFirstChilds = true;
		/**
				 * Metodo do filtro já implementado para toggle e filtros
				 * Filtra movimento
				 * */
		public virtual Vector3 filter (Vector3 currentValue, Vector3 nextValue)
		{
			if (this.byPass) {
				return nextValue;
			}

			if (!this._active) {
				return nextValue;
			}
			if (filterThisFirstChilds) {
				nextValue = this._doFilter (currentValue, nextValue);
			}
			if (!this._hasFilter) {
				return nextValue;
			}

			for (uint i = 0; i < this.filters.Length; i++) {

				TransformPositionAngleFilterAbstract plugin = this.filters [i];

				nextValue = plugin.filter (currentValue, nextValue) ;

				currentValue = plugin.filterCurrent( currentValue ) ;

			}
			if (!filterThisFirstChilds) {
				nextValue = this._doFilter (currentValue, nextValue);
			}
			return nextValue;
		}
		protected abstract Vector3 _doFilter (Vector3 currentValue, Vector3 nextValue) ;

				
		/**
				 * Metodo do filtro já implementado para toggle e filtros
				 * Filtra angulo
				 * */
		public virtual Quaternion filter (Quaternion currentValue, Quaternion nextValue)
		{
			if (this.byPass) {
				return nextValue;
			}
						

			if (!this._active) {
				return nextValue;
			}
			//aplica o filtro local
			if (filterThisFirstChilds) {
				nextValue = this._doFilter (currentValue, nextValue);
			}
			if (!this._hasFilter) {
				return nextValue;
			}
			this.DebugFiltering = true;
			//angulos
			//aplica os filtros filhos
			for (uint i = 0; i < this.filters.Length; i++) {

				TransformPositionAngleFilterAbstract plugin = this.filters [i];

				nextValue = plugin.filter (currentValue, nextValue);

				currentValue = plugin.filterCurrent( currentValue );
			}
			if (!filterThisFirstChilds) {
				nextValue = this._doFilter (currentValue, nextValue);
			}
			return nextValue;
		}

		protected abstract Quaternion _doFilter (Quaternion currentValue, Quaternion nextValue) ;


		public string getUniqueName ()
		{
			return this.gameObject.name;
		}
		
		public void turnOn ()
		{
			this.byPass = false;
			this._active = true;
		}
		public void turnOff ()
		{
			this.byPass = true;
			this._active = false;
		}
		public void ResetOnOff ()
		{
			if (this.activeByDefault) {
				turnOn ();
				return;
			} 
			turnOff ();
		}
		public GameObject getGameObjectReference ()
		{
			return this.gameObject;
		}
		/// <summary>
		/// Retorna true se esse componente estiver ativo
		/// </summary>
		/// <returns><c>true</c>, if active was ised, <c>false</c> otherwise.</returns>
		public virtual bool isActive ()
		{
			return (this._active && !this.byPass);
		}
	}
}