﻿using UnityEngine;
using System.Collections;

/**
 * Para classes que fazem filtros baseados em histórico, utilizando Vector3
 * */
namespace ARM.transform.filters.abstracts.histories
{

	public abstract class HistoryPositionFilterAbstract : TransformPositionAngleFilterAbstract
	{


		public abstract void save (Vector3 position) ;

		public abstract void reset () ;
	}
}