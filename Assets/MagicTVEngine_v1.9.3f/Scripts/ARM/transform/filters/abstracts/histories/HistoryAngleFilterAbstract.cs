﻿using UnityEngine;
using System.Collections;
/**
 * Para classes que fazem filtros baseados em histórico, utilizando quaternion
 * */
namespace ARM.transform.filters.abstracts.histories
{
	public abstract class HistoryAngleFilterAbstract : TransformPositionAngleFilterAbstract
	{

		public abstract void save (Quaternion angle) ;

		public abstract void reset () ;

	}
}