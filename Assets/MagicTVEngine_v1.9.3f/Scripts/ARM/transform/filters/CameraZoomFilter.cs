﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/* 
 * 
 * @autor Marcelo Zani
* 
* @version 1.0
* 
* @description 
* 
* 
* */
namespace ARM.transform.filters
{

    public class CameraZoomFilter : TransformPositionAngleFilterAbstract
    {

        public Vector3 positionToZoom;

        //esquema proposto do renato:
        /// <summary>
        /// The percent zoom.
        /// 1 é no target
        /// 0 é na camera
        /// -1 é longe da camera a mesma distancia do target
        /// 2 é passando do target um tantao
        /// 
        /// LEAL: Se puder já setar essa variavel conforme o metadado, aí estamos totalmente completos em relação ao zoom. Defina default como 1f.
        /// 
        /// </summary>
        public float percentZoom = 0f;//<<<<<< AQUI PEPERONE, MUDA ESSA VARIAVEL CONFORME O GESTO DO USUARIO E JÁ ERA O ZOOM ESTÁ APLICADO E PRONTO

        public float ScaleDeltaMinimum = 35f;
        public float ScaleDeltaMaximum = 100f;

        public TouchControllerState currentState = TouchControllerState.Idle;

        public float touchDistance;
        public float deltaZoom;

        public float deltaZoomRatio = 1000f;

        public float zoomMax = 0.8f;
        public float zoomMin = 0f;

        void Update()
        {
            if (zoomMin > zoomMax)
            {
                zoomMin = zoomMax;
            }

            if (Input.touchCount == 2)
            {
                Touch touch1 = Input.touches[0];
                Touch touch2 = Input.touches[1];

                if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                {
                    var newTouchDistance = Vector2.Distance(touch1.position, touch2.position);
                    deltaZoom = (newTouchDistance - touchDistance) * (Screen.dpi / 100);
                    if (Mathf.Abs(deltaZoom) >= ScaleDeltaMaximum)
                        deltaZoom = 0;
                    touchDistance = newTouchDistance;

                    float newTouchAngle = PinchToutchController.Angle(touch1.position, touch2.position);
                }

                //Seta Scale 
                if ((Mathf.Abs(deltaZoom) >= ScaleDeltaMinimum) ||
                    ((Mathf.Abs(deltaZoom) > 0) && (currentState == TouchControllerState.Scalling)))
                {
                    currentState = TouchControllerState.Scalling;
                }
            }
            else
            {
                currentState = TouchControllerState.Idle;
                touchDistance = 0f;
                deltaZoom = 0f;
            }

            if (currentState == TouchControllerState.Scalling)
            {
                percentZoom += deltaZoom / deltaZoomRatio;
            }

            percentZoom = Mathf.Clamp(percentZoom, zoomMin, zoomMax);
        }

        void Start()
        {
            this._init();
            percentZoom = 0f;
        }

        protected override Vector3 _doFilter(Vector3 last, Vector3 nextValue)
        {
            var nextValueComPlugin = filtroComPlugin(nextValue);
            // colocar aqui a relação
            //nextValue = this.positionToZoom;

            //proposta do renato
            return nextValueComPlugin;
        }

        Vector3 filtroComPlugin(Vector3 nextValue)
        {
            if (RelativePositionOfPoint.GetInstance() != null)
            {
                return RelativePositionOfPoint.GetInstance().GetRelativePoint(nextValue, 1 - this.percentZoom);
            }
            //xiii, o plugin nao ta na cena
            Debug.LogError("Falta o plugin RelativePositionOfPoint na cena.");
            return nextValue;
        }

        protected override Quaternion _doFilter(Quaternion last, Quaternion nextValue)
        {
            //TODO: colocar aqui a relação
            return nextValue;
        }
    }
}
