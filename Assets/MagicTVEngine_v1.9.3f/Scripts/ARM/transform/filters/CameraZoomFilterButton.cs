﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/* 
 * 
 * @autor Marcelo Zani
* 
* @version 1.0
* 
* @description 
* 
* 
* */
namespace ARM.transform.filters
{

	public class CameraZoomFilterButton : TransformPositionAngleFilterAbstract
    {

        public Vector3 positionToZoom;

        //esquema proposto do renato:
        /// <summary>
        /// The percent zoom.
        /// 1 é no target
        /// 0 é na camera
        /// -1 é longe da camera a mesma distancia do target
        /// 2 é passando do target um tantao
        /// 
        /// LEAL: Se puder já setar essa variavel conforme o metadado, aí estamos totalmente completos em relação ao zoom. Defina default como 1f.
        /// 
        /// </summary>
        public float percentZoom = 0f;//<<<<<< AQUI PEPERONE, MUDA ESSA VARIAVEL CONFORME O GESTO DO USUARIO E JÁ ERA O ZOOM ESTÁ APLICADO E PRONTO


        void Start()
        {
            this._init();
            percentZoom = 0f;
        }
		public Vector3 DebugResultVector ;
        protected override Vector3 _doFilter(Vector3 last, Vector3 nextValue)
        {
            // colocar aqui a relação
            //nextValue = this.positionToZoom;

            //proposta do renato
			Vector3 v = this.filtroComPlugin(last);
			DebugResultVector = v;
			return v;
            return nextValue;
        }
		void OnGUI(){
			if (GUI.Button (new Rect (0, 0, Screen.width / 2, Screen.height / 4), "-")) {
				this.percentZoom -= 0.01f;
			}
			if (GUI.Button (new Rect (Screen.width / 2, 0, Screen.width / 2, Screen.height / 4), "+")) {
				this.percentZoom += 0.01f;
			}
		}

        Vector3 filtroComPlugin(Vector3 nextValue)
        {

            if (RelativePositionOfPoint.GetInstance() != null)
            {
                return RelativePositionOfPoint.GetInstance().GetRelativePoint(nextValue, 1 - this.percentZoom);
            }
            //xiii, o plugin nao ta na cena
            Debug.LogError("Falta o plugin RelativePositionOfPoint na cena.");
            return nextValue;
        }

        protected override Quaternion _doFilter(Quaternion last, Quaternion nextValue)
        {
            //TODO: colocar aqui a relação
            return nextValue;
        }
    }
}
