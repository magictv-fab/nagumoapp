﻿using UnityEngine;
using System.Collections;
using ARM.transform.filters.abstracts;

/// <summary>
/// @author Renato Seiji Miawaki
/// @version 1.0
/// @date AGosto/2015
/// Plugin Filter to Paste position and/or angle after LateUpdate of this object .
/// 
/// </summary>

public class PastePositionAngleFilter : MonoBehaviour
{
	public GameObject objectToPaste ;
	/// <summary>
	/// Se true, pega a global position, false usa localPosition
	/// </summary>
	public bool readGlobalPosition ;
	/// <summary>
	/// The write global position.
	/// </summary>
	public bool writeGlobalPosition ;
	/// <summary>
	/// The copy position. Se false ignora a posição.
	/// </summary>
	public bool copyPosition = true ;
	/// <summary>
	/// The copy angle. Se false ignora o angulo.
	/// </summary>
	public bool copyAngle = true ;
	/// <summary>
	/// Se true só atualiza no LateUpdate
	/// </summary>
	public bool _changeOnLateUpdate = false ;

	public bool _active = true ; 
	// Update is called once per frame
	void Update ()
	{
		if (! this._changeOnLateUpdate) {
			doFollow ();
		}

	}
	void LateUpdate ()
	{
		if (this._changeOnLateUpdate) {
			doFollow ();
		}
	}

	void doFollow ()
	{
		if (!this._active) {
			return;
		}

		if (!copyPosition && !copyAngle) {
			return;
		}
		Vector3 pos = this.transform.localPosition;
		if (readGlobalPosition) {
			pos = this.transform.position;
		}
		Quaternion rot = this.transform.localRotation;
		if (readGlobalPosition) {
			rot = this.transform.rotation;
		}
		if(this.objectToPaste){
			if (this.writeGlobalPosition) {
				
				this.objectToPaste.transform.position = pos;
				this.objectToPaste.transform.rotation = rot;
			} else {
				this.objectToPaste.transform.localPosition = pos;
				this.objectToPaste.transform.localRotation = rot;
			}
		}
	}

	protected virtual Quaternion doCopyRotation (Quaternion rot)
	{
		return new Quaternion( rot.x, rot.y, rot.z, rot.w ) ;
	}

	protected virtual Vector3 doCopyPosition (Vector3 pos)
	{
		return new Vector3( pos.x, pos.y, pos.z );
	}
}
