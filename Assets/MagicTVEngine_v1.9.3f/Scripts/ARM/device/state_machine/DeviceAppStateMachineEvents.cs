﻿using UnityEngine;
using System.Collections;
namespace AMR.device
{
		public class DeviceAppStateMachineEvents : MonoBehaviour
		{

		#region Auto generate GO
				private const string SELF_GAMEOBJECT_NAME = "_DeviceAppStateMachineEventsGO";
				private static GameObject _selfGameObject ;
				protected static GameObject SelfGameObject {
						get {

								if (_selfGameObject == null) {
										_selfGameObject = GameObject.Find (SELF_GAMEOBJECT_NAME);
										if (_selfGameObject != null) {
												DestroyImmediate (_selfGameObject);
										}
										_selfGameObject = new GameObject (SELF_GAMEOBJECT_NAME);
										_selfGameObject.transform.parent = null;
										IntanceDeviceAppStateMachineEvents = _selfGameObject.AddComponent<DeviceAppStateMachineEvents> ();
								}

								return _selfGameObject;
						}
				}

		#endregion

		#region static Instance 

				protected static DeviceAppStateMachineEvents _selfInstance ;

				protected static DeviceAppStateMachineEvents IntanceDeviceAppStateMachineEvents {
						get {
								if (_selfInstance == null) {
										_selfInstance = SelfGameObject.GetComponent<DeviceAppStateMachineEvents> ();
								}
								return _selfInstance;
						}
						set {
								_selfInstance = value;
						}
				}

		#endregion

				public delegate void OnPauseEventHandler () ;

		#region PauseEvent
				protected OnPauseEventHandler onPause ;

				public static void AddPauseEventHandler (OnPauseEventHandler eventHandler)
				{
						IntanceDeviceAppStateMachineEvents.onPause += eventHandler;
				}

				public static void RemovePauseEventHandler (OnPauseEventHandler eventHandler)
				{
						IntanceDeviceAppStateMachineEvents.onPause -= eventHandler;
				}

				protected void RaisePause ()
				{
						if (onPause != null) {
								onPause ();
						}

				}
		#endregion


		#region ResumeEvent
				protected OnPauseEventHandler onResume ;
			
				public static void AddResumeEventHandler (OnPauseEventHandler eventHandler)
				{
						IntanceDeviceAppStateMachineEvents.onResume += eventHandler;
				}
			
				public static void RemoveResumeEventHandler (OnPauseEventHandler eventHandler)
				{
						IntanceDeviceAppStateMachineEvents.onResume -= eventHandler;
				}
			
				protected void RaiseResume ()
				{
						if (onResume != null) {
								onResume ();	
						}
				
				}
		#endregion



		#region AwakeEvent
				public delegate void OnAwakeEventHandler () ;
			
				protected OnAwakeEventHandler onAwake ;
			
				public static void AddAwakeEventHandler (OnAwakeEventHandler eventHandler)
				{
						IntanceDeviceAppStateMachineEvents.onAwake += eventHandler;
				}
			
				public void RemoveAwakeEventHandler (OnAwakeEventHandler eventHandler)
				{
						IntanceDeviceAppStateMachineEvents.onAwake -= eventHandler;
				}
			
				protected void RaiseAwake ()
				{
						if (onAwake != null) {
								onAwake ();	
						}
				}

		#endregion

		#region IOS awake SystemControll

				public static int IOSPauseToleranceInMilliseconds {
						set{ IntanceDeviceAppStateMachineEvents._IOSPauseToleranceInMilliseconds = value;}
						get{ return IntanceDeviceAppStateMachineEvents._IOSPauseToleranceInMilliseconds; }
				}

				protected int _IOSPauseToleranceInMilliseconds = 2000000 ; //X minutes

				private bool _currentFocus;

				private System.DateTime _pauseTime ;

				void startBackGroundTimerCount ()
				{
						_pauseTime = System.DateTime.Now;

				}

				void checkIOSWasOnBackground ()
				{
						System.DateTime now = System.DateTime.Now;

						//		//Debug.Log ( "SAV:" +_pauseTime.ToLongTimeString());
						//		//Debug.Log ( "NOW:" + now.ToLongTimeString());



						System.TimeSpan diff = (now - _pauseTime);
				
						//			//Debug.Log (diff.TotalMilliseconds);

						if (diff.TotalMilliseconds >= _IOSPauseToleranceInMilliseconds) {
								RaiseAwake ();
						}

				}
		#endregion

				// Use this for initialization
				void Start ()
				{
		
				}
		
				// Update is called once per frame
				void Update ()
				{
		
				}



				void OnApplicationPause (bool pauseStatus)
				{
						if (pauseStatus) {
								RaisePause ();
								#if UNITY_IPHONE
								startBackGroundTimerCount ();
								#endif
								return;
						}

						RaiseResume ();

						#if UNITY_IPHONE
						checkIOSWasOnBackground ();
						#endif

				}



				/// <summary>
				/// Raises the application awake event.  DON'T WORK ON IOS
				/// </summary>
				/// <param name="focusStatus">If set to <c>true</c> focus status.</param>
				void OnApplicationFocus (bool focusStatus)
				{
						// se voltou do focus, e tava false , pra evitar qndo o app abre pela primeira vez
						if (focusStatus && _currentFocus == false) {
								RaiseAwake ();
						}
						_currentFocus = focusStatus;
				}
		}
}
