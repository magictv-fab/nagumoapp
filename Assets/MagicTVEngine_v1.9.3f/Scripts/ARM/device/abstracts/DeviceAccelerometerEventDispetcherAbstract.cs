﻿using UnityEngine;
using System.Collections;
using ARM.events ;
/**
 * 
 * @author Renato Miawaki
 * @version 1.0
 * 
 * */
namespace ARM.device.abstracts
{

	public abstract class DeviceAccelerometerEventDispetcherAbstract:ChangeEventAbstract
	{

		/// <summary>
		/// The tolerance change percent, 1 is 100% and 0 is 0.
		/// </summary>
		public float toleranceChangePercent = 0f ;

		/// <summary>
		/// The cached last vector recived to comare changes.
		/// </summary>
		protected Vector3 _lastVector {
			get { return _lastVectorValue;}
			set {
				_lastVectorHasValue = true;
				_lastVectorValue = value;
			}
		}

		protected Vector3 _lastVectorValue ;
		protected bool _lastVectorHasValue = false ;

		public delegate void OnVoidEventHandler ();

		public delegate void OnMoveHandler (Vector3 factor);

		/**
		 * Quando um shake inicia
		 * */
		private event OnVoidEventHandler onShakeInit ;
		
		/**
		 * Quando um shake para
		 * */
		private event OnVoidEventHandler onShakeEnd ;

		
		/**
		 * Quando um movimento inicia
		 * */
		private event OnVoidEventHandler onMoveInit ;
		
		/**
		 * Quando um movimento para
		 * */
		private event OnVoidEventHandler onMoveEnd ;

		
		/**
		 * Enquanto um movimento está acontecendo
		 * */
		private event  OnMoveHandler onMove ;

		/**
		 * ADD Listeners
		 * */
		public virtual void addOnMoveListener (OnMoveHandler listener)
		{
			this.onMove += listener;
		}
		public virtual void addOnMoveInitListener (OnVoidEventHandler listener)
		{
			this.onMoveInit += listener;
		}
		public virtual void addOnMoveEndListener (OnVoidEventHandler listener)
		{
			this.onMoveEnd += listener;
		}
		public virtual void addOnShakeInitListener (OnVoidEventHandler listener)
		{
			this.onShakeInit += listener;
		}
		public virtual void addOnShakeEndListener (OnVoidEventHandler listener)
		{
			this.onShakeEnd += listener;
		}

		/**
		 * REMOVE Listeners
		 * */
		public virtual void removeOnMoveListener (OnMoveHandler listener)
		{
			this.onMove -= listener;
		}
		public virtual void removeOnMoveInitListener (OnVoidEventHandler listener)
		{
			this.onMoveInit -= listener;
		}
		public virtual void removeOnMoveEndListener (OnVoidEventHandler listener)
		{
			this.onMoveEnd -= listener;
		}
		public virtual void removeOnShakeInitListener (OnVoidEventHandler listener)
		{
			this.onShakeInit -= listener;
		}
		public virtual void removeOnShakeEndListener (OnVoidEventHandler listener)
		{
			this.onShakeEnd -= listener;
		}
		/**
		 * Para uso da classe que extendeu essa
		 * */
		protected void _onShakeInit ()
		{
			//verifica se possuí listeners
			if (this.onShakeInit != null) {
				this.onShakeInit ();
			}
		}
		
		protected void _onShakeEnd ()
		{
			//verifica se possuí listeners
			if (this.onShakeEnd != null) {
				this.onShakeEnd ();
			}
		}


		
		protected void _onMoveInit ()
		{
			//verifica se possuí listeners
			if (this.onMoveInit != null) {
				this.onMoveInit ();
			}
		}
		
		protected void _onMoveEnd ()
		{
			//verifica se possuí listeners
			if (this.onMoveEnd != null) {
				this.onMoveEnd ();
			}
		}

		protected void _onMove (Vector3 factor)
		{
			//verifica se possuí listeners
			if (this.onMove != null) {

				this.onMove (factor);

			}

			if (this._lastVectorHasValue == false) {
				
				this._lastVector = factor;
				return;

			}
			//comparando
			float percentChange = 0;
			percentChange += Mathf.Abs (factor.x - this._lastVector.x) / 2;
			percentChange += Mathf.Abs (factor.y - this._lastVector.y) / 2;
			percentChange += Mathf.Abs (factor.z - this._lastVector.z) / 2;
			percentChange /= 3;
			DebugPercentChange = percentChange;
			if (percentChange > 0 && percentChange > this.toleranceChangePercent) {

				percentChange = (percentChange > 1) ? 1 : percentChange;

				this.RaiseChageEvent (percentChange);
			}
		}
		public float DebugPercentChange = 0f;
	}

}