using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ARM.abstracts.internet ;

using ARM.abstracts ;

namespace ARM.device.internet
{
		public class InternetInfo : InternetInfoAbstract
		{
				private NetworkReachability wifiReachability;
				private Dictionary<NetworkReachability, Action> _wifiReachabilityDictionary;
		
				public Dictionary<NetworkReachability, Action> wifiReachabilityDictionary {
						get {
								if (_wifiReachabilityDictionary != null) {
										return _wifiReachabilityDictionary;
								}
								_wifiReachabilityDictionary = new Dictionary<NetworkReachability, Action> () {
					
					{ NetworkReachability.NotReachable, NotReachable },
					{ NetworkReachability.ReachableViaCarrierDataNetwork, ReachableViaCarrierDataNetwork },
					{ NetworkReachability.ReachableViaLocalAreaNetwork, ReachableViaLocalAreaNetwork }
				};
								return _wifiReachabilityDictionary;
						}
				}

				public override void prepare ()
				{
						//FAKE forçando que não está no wifi
//						this._hasWifi = false ;
//						this.RaiseComponentIsReady () ;
//						return ;
						//iniciar  e pegar todas as infos da internet antes de disparar o evento de que está pronto para uso
						testWifi () ;
						//Debug.LogWarning ( "InternetInfo . init " );
				}

				public override void testWifi ()
				{
						//verifica o tipo da conexão do usuário e dispara o evento
						this.wifiReachability = Application.internetReachability;
						onConnectionHandler (wifiReachability);
						//Debug.LogWarning ("InternetInfo . testWifi -  wifiReachability: " + wifiReachability);

				}

				public void onConnectionHandler (NetworkReachability wifiReachability)
				{
						
						if (wifiReachabilityDictionary.ContainsKey (wifiReachability)) {
								wifiReachabilityDictionary [wifiReachability] ();
						}
						//Debug.LogWarning ( "InternetInfo . onConnectionHandler " );
						RaiseComponentIsReady ();
				}

				public void NotReachable ()
				{
						RaiseWifiInfo (false);
				}

				public void ReachableViaCarrierDataNetwork ()
				{
						RaiseWifiInfo (false);
				}

				public void ReachableViaLocalAreaNetwork ()
				{
						RaiseWifiInfo (true);
				}
		}
}