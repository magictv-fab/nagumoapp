﻿using UnityEngine;
using System.Collections;
using System;
using ARM.abstracts;
using ARM.utils;


namespace ARM.device.accelerometer
{

	public class AccelerometerEventToggleOnOff: ToggleOnOffAbstract
	{

		public AccelerometerEventDispacher accelerometerEvent ;

		void Start ()
		{
		
			if (this.accelerometerEvent != null) {

				this.accelerometerEvent.addOnMoveInitListener (this._onMoveInit);
				this.accelerometerEvent.addOnMoveEndListener (this._onMoveEnd);
				return;
			}

			this.onTurnOnEvent ();
		}
		protected void _onMoveInit ()
		{

			this.onTurnOnEvent ();
		}
		protected void _onMoveEnd ()
		{

			this.onTurnOffEvent ();
		}
	}
}