﻿using UnityEngine;
using System.Collections;
using System;
using ARM.device.abstracts;
using ARM.utils;
using System.Collections.Generic;

/// <summary>
/// Accelerometer event dispacher.
/// @author Renato Seiji Miawaki
/// @version 1.1
/// 
/// Agora a tolerancia de x, y, z são separados
/// </summary>
namespace ARM.device {

	public class AccelerometerEventDispacher: DeviceAccelerometerEventDispetcherAbstract, OnOffListenerEventInterface, ConfigurableListenerEventInterface {

		public bool onGUIDebug ;

		public float shakeTolerance  = 0.1462f  ;

		public float movementToleranceX = 0.0662f ;
		public float movementToleranceY = 0.0662f ;
		public float movementToleranceZ = 0.0662f ;

		public uint _historyRange  = 30 ;

		protected float _time  = 0;

		protected float _clockTimer = 100 ;

		protected PoolVector3 _poolVector3;

		//contains if devide is stable
		protected bool _isShaking 	= false ;
		protected bool _isMoving 	= false ;

		public bool _active = true ;

		protected List<float> _inicialConfig = new List<float>();
		void Start () {

			_poolVector3 = new PoolVector3 ( _historyRange  );

			//ordem dos floats shakeTolerance|movementToleranceX|movementToleranceY|movementToleranceZ|toleranceChangePercent|(int)historyRange|(bool)onGUIDebug
			_inicialConfig.Add (this.shakeTolerance);
			_inicialConfig.Add (this.movementToleranceX);
			_inicialConfig.Add (this.movementToleranceY);
			_inicialConfig.Add (this.movementToleranceZ);
			_inicialConfig.Add (this.toleranceChangePercent);
			_inicialConfig.Add ((float) this._historyRange);
			_inicialConfig.Add (0f);

		}

		void LateUpdate (){
			if(!_active){
				//desligado, não faz nada
				return;
			}
			 _time += ( Time.deltaTime );

			if (_time*1000 >= _clockTimer) {
				_time   = 0;

				this.onTime() ;

			}

		}

		public float xDiff ;
		public float yDiff ;
		public float zDiff ;

		void onTime(){
			_poolVector3.addItem (Input.acceleration);


			bool shakingStable = true;
			bool movementStable = true;

			Vector3 calcBase = _poolVector3.getMedian ();

			xDiff = Mathf.Abs(   Input.acceleration.x - calcBase.x ) ;
			yDiff = Mathf.Abs(   Input.acceleration.y - calcBase.y ) ;
			zDiff = Mathf.Abs(   Input.acceleration.z - calcBase.z ) ;


			if ( xDiff > movementToleranceX ) {
				movementStable = false ;
			}
			if ( yDiff > movementToleranceY ) {
				movementStable = false ;
			}

			if ( zDiff > movementToleranceZ ) {
				movementStable = false ;
			}
			if (!movementStable) {
				//para ter shake, precisa ter movimento
				if (xDiff > shakeTolerance || yDiff > shakeTolerance || zDiff > shakeTolerance) {
					shakingStable = false;
				}
			}
			if (! shakingStable && !_isShaking) {
				_onShakeInit();
				_isShaking = true;

			}else if (shakingStable && _isShaking) {
				_onShakeEnd();
				_isShaking = false;
			}

			if (! movementStable && !_isMoving) {

				_onMoveInit();
				_isMoving = true;
				
			}else if (movementStable && _isMoving) {
				_onMoveEnd();
				_isMoving = false;
				
				
			}

			if (_isMoving) {
				Vector3 _movementDirection  = new Vector3( Input.acceleration.x - calcBase.x , Input.acceleration.y - calcBase.y ,Input.acceleration.z - calcBase.z);
				_onMove( _movementDirection ) ;
			}

//			NGUI//Debug.Log (  _poolVector3.getMean().x ) ;
		}
		public float inicialX = 20f;
		public float inicialY = 20f;
		public float larguraBotao = 350f;
		public float alturaBotao = 85f;
		void OnGUI(){
		
			if (!onGUIDebug) {
				return ;
			}
			if(_poolVector3 == null){
				return;
			}

			Vector3 calcBase = _poolVector3.getMedian () ;

				GUI.Label ( 
			           new Rect (inicialX, inicialY+alturaBotao*2+40, Screen.width, Screen.height/3), 
			           ( _isShaking ? "!!! IS SHAKING !!!! ": "" ) +  "\n" +
			           ( _isMoving ? "~~ IS MOVING ~~~ ": "" ) + "\n : getMedian : " +
			           "\nx:" + calcBase.x + 
			           "\ny:" + calcBase.y + 
			           "\nz:" + calcBase.z  
				) ;

			if (GUI.RepeatButton (new Rect (inicialX, inicialY, larguraBotao, alturaBotao), "+shake")) {
					shakeTolerance+= 0.0001f ;				
				}

			GUI.Label (new Rect (larguraBotao+inicialX+20, inicialY, larguraBotao, alturaBotao), (_isShaking?1:0) + " " +  shakeTolerance) ;

			if (GUI.RepeatButton (new Rect (larguraBotao+inicialX+20, inicialY, larguraBotao, alturaBotao), "-shake")) {
					shakeTolerance-= 0.0001f ;
				}


			if (GUI.RepeatButton (new Rect (inicialX, inicialY+alturaBotao+20, larguraBotao, alturaBotao), "+mov X")) {
				movementToleranceX+= 0.0001f ;				
			}
			
			GUI.Label (new Rect (larguraBotao+inicialX+20, inicialY+alturaBotao+20, larguraBotao, alturaBotao), " " + movementToleranceX) ;
			
			if (GUI.RepeatButton (new Rect (larguraBotao+inicialX+20, inicialY+alturaBotao+20, larguraBotao, alturaBotao), "-mov X")) {
				movementToleranceX-= 0.0001f ;
			}

		}


		public string getUniqueName(){
			return this.gameObject.name;
		}
		public void turnOn (){
			this._active = true ;
		}
		public void turnOff (){
			this._active = false ;
		}
		public void ResetOnOff (){
			this._active = true ;
		}
		public bool isActive(){
			return this._active = true ;
		}
		public GameObject getGameObjectReference(){
			return this.gameObject ;
		}
		/// <summary>
		/// Configs the by string.
		/// ordem dos floats shakeTolerance|movementTolerance|toleranceChangePercent|(int)historyRange|(bool)onGUIDebug
		/// </summary>
		/// <param name="metadata">Metadata.</param>
		public void configByString (string metadata){
			//ordem dos floats 
			//shakeTolerance|movementTolerance|toleranceChangePercent|(int)historyRange|(bool)onGUIDebug
			List<float> configs = PipeToList.ParseReturningFloats (metadata);
			configByFloatList (configs);
		}
		public void configByFloatList(List<float> config ){
			//ordem dos floats shakeTolerance|movementToleranceX|movementToleranceY|movementToleranceZ|toleranceChangePercent|(int)historyRange|(bool)onGUIDebug
			if(config.Count < 1){
				return;
			}
			if(config.Count > 0){
				this.shakeTolerance = config[0] ;
			}
			if(config.Count > 1){
				this.movementToleranceX = config[1] ;
			}
			if(config.Count > 2){
				this.movementToleranceY = config[2] ;
			}
			if(config.Count > 3){
				this.movementToleranceZ = config[3] ;
			}
			if(config.Count > 4){
				this.toleranceChangePercent = config[4] ;
			}
			if(config.Count > 5){
				this._historyRange = (uint) config[5] ;
			}
			if(config.Count > 6){
				this.onGUIDebug = ( config[6] > 0.1f )?true:false ;
			}
		}
		public void Reset (){
			this.configByFloatList (this._inicialConfig);
		}
	}
}