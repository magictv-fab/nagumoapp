﻿using UnityEngine;
using System.Collections;

/// <summary>
/// View time controll abstract.
/// No MagicTV não tem trackingName pois não é assim que funciona
/// 
/// @author: Renato Seiji Miawaki
/// 
/// </summary>
using ARM.abstracts;


namespace ARM.animation
{
		public abstract class ViewTimeControllAbstract : AViewAbstract, GenericTimeControllInterface
		{
				
				public delegate void OnReadyEventHandlerandler ();
				OnReadyEventHandlerandler _onReady ;

				public void AddOnReady (OnReadyEventHandlerandler action)
				{
						ARMDebug.LogWarning ("[ 1 ] ViewTimeControll . AddOnReady " + action);
						this._onReady += action;
				}

				protected void RaiseOnReady ()
				{
						this._isReady = true;
						Debug.LogWarning ("[ 0 ] ViewTimeControll . raise onReady " + this._onReady);
						if (this._onReady != null) {
								
								this._onReady ();
						}
				}

				protected bool _isReady = false ;

				public bool isReady {
						get {
								return this._isReady;
						}
				}



				//Evento a ser chamado quando termina a animação ou seja lá o que for
				public delegate void OnFinishedEvent () ;
				public OnFinishedEvent onFinished ;

				//variavel aconselhada para se modificar 
				protected bool _isPlaying = false ;
				//Basta modificar o valor da variavel e o metodo já funciona
				public bool isPlaying ()
				{
						return this._isPlaying;
				}
				//Sobreescrever esses metodos, mas não esquecer de chamar o base.Play() ; ao executar o seu próprio Play
				public virtual void Play ()
				{
						this._isPlaying = true;
				}
				public virtual void Stop ()
				{
						this._isPlaying = false;
				}
				public virtual void Pause ()
				{
						this._isPlaying = false;
				}
				protected void _onFinished ()
				{
						if (this.onFinished != null) {
								this.onFinished ();
						}
				}
		}
}

