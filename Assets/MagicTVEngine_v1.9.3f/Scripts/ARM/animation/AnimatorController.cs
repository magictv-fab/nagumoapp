using UnityEngine;
using System.Collections;
using ARM.animation;
using System.Linq;
using ARM.utils.cron;

namespace ARM.animation
{
		public class AnimatorController : ViewTimeControllAbstract
		{
				public Animator anim;
				public bool RestartOnHide = false;
				private bool _isReadyToPlay = false;
				public bool autoPlay;

				public bool _playNow;

				public override void Start ()
				{
						if (!_isReadyToPlay) {
								prepareToPlay ();
						}
						if (this.autoPlay) {
								Play ();
						}
				}

				void Update ()
				{
						if (this._playNow) {
								this._playNow = false;
								this.Play ();
						}
				}

				private void prepareToPlay ()
				{
//						this.Hide ();
						this.onFinished += Stop;
						this._isReadyToPlay = true;
						
				}

				public override void Play ()
				{
						if (!_isReadyToPlay) {
								prepareToPlay ();
						}
						if (anim != null) {
								anim.speed = 1;
								anim.SetBool ("StartAnim", true);	
						}
						this.Show ();
						base.Play ();
				}

				public override void Pause ()
				{
//						//ARMDebug.LogWarning (" Pause Animator Controller ");
						if (anim != null) {
								anim.speed = 0;
						}
						base.Pause ();
				}

				public override void Stop ()
				{
//						//ARMDebug.LogWarning (" Stop Animator Controller ");
						if (anim != null) {
								anim.SetBool ("StartAnim", false);
						}
						base.Stop ();
				}

				public override void Hide ()
				{
						base.Hide ();
						if (RestartOnHide) {
								Stop ();			
								return;
						}
						this.Pause ();
						
				}

				public override void Show ()
				{
//						//ARMDebug.LogWarning ("AnimatorController . Show");
						base.Show ();
				}

			
		}
}