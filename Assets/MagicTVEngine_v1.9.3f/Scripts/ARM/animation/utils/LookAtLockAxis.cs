﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 
/// Look at lock axis.
/// 
/// Utilize essa classe para fazer algum elemento 3d utilizar o lookAt porém com algum eixo travado
/// </summary>

namespace ARM.animation.utils
{
		public class LookAtLockAxis : MonoBehaviour
		{

	
				public bool lockX = false ;
				public bool lockY = false ;
				public bool lockZ = false ;

				public float calibrationX = 0f ;
				public float calibrationY = 0f ;
				public float calibrationZ = 0f ;

				public GameObject targetToLook ;
				/// <summary>
				/// The look to camera and ignore the targetToLook
				/// </summary>
				public bool lookToCamera;

				public Vector3 debugRotation ;
				public Quaternion debugRotationQuaternion ;
				protected bool _hasTarget ;
				// Use this for initialization
				void Start ()
				{
						this._hasTarget = (this.targetToLook != null);
						if (this.lookToCamera) {
								this._hasTarget = true;
								this.targetToLook = Camera.main.gameObject;
						}
				}
	
				// Update is called once per frame
				void Update ()
				{
						if (this._hasTarget) {

								Vector3 current = this.transform.localRotation.eulerAngles;

								this.transform.LookAt (this.targetToLook.transform.position);

								Vector3 newRotation = this.transform.localRotation.eulerAngles;

								this.debugRotationQuaternion = this.transform.localRotation;
								this.debugRotation = newRotation;

								newRotation.x += this.calibrationX;
								newRotation.y += this.calibrationY;
								newRotation.z += this.calibrationZ;



								if (this.lockX) {
										newRotation.x = current.x;
								}
								if (this.lockY) {
										newRotation.y = current.y;
								}
								if (this.lockZ) {
										newRotation.z = current.z;
								}


								this.transform.localRotation = Quaternion.Euler (this.normalizeVector3 (newRotation));

						}
				}

				private Vector3 normalizeVector3 (Vector3 newRotation)
				{
						newRotation.x = this.limitAngle (newRotation.x);
						newRotation.y = this.limitAngle (newRotation.y);
						newRotation.z = this.limitAngle (newRotation.z);

						return newRotation;
				}
				private float limitAngle (float angle)
				{
						while (angle > 360) {
								angle -= 360;
						}
						while (angle < 0) {
								angle += 360;
						}
						return angle;
				}
		}
}