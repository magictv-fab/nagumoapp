﻿using UnityEngine;
using System.Collections;

public delegate void OnEvent() ;

namespace ARM.animation{
	public class GenericTimeControllAbstract : MonoBehaviour, GenericTimeControllInterface {
		//Evento a ser chamado quando termina a animação ou seja lá o que for

		public OnEvent onFinished ;
		public OnEvent onStart ;
		//variavel aconselhada para se modificar 
		protected bool _isPlaying = false ;
		//Basta modificar o valor da variavel e o metodo já funciona
		public bool isPlaying() {
			return this._isPlaying ;
		}
		//Sobreescrever esses metodos, mas não esquecer de chamar o base.Play() ; ao executar o seu próprio Play
		public virtual void Play (){
			this._isPlaying = true;
		}
		public virtual void Stop (){
			this._isPlaying = false;
		}
		public virtual void Pause(){
			this._isPlaying = false;
		}
		protected void _onFinished(){
			if ( this.onFinished != null ) {
				this.onFinished ();
			}
		}
		protected void _onStart(){
			if ( this.onStart != null ) {
				this.onStart ();
			}
		}
	}
}

