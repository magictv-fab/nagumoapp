﻿namespace ARM.animation{
	interface GenericTimeControllInterface {

		void Play ();
		void Stop ();
		void Pause() ;
		bool isPlaying() ;

	}
}