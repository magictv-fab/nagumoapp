﻿namespace ARM.animation{
	interface AViewInterface {
		
		void Show ();
		void Hide ();
	}
}