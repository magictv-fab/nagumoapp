﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;

/// <summary>
/// 
/// @author 	: Renato Seiji Miawaki
/// @version 	: 1.0
/// 
/// </summary>
using ARM.abstracts.components;


namespace ARM.animation
{
		/// <summary>
		/// Componentes que podem ser iniciado, Play e ao mesmo tempo precisam ser iniciado
		/// </summary>
		public abstract class GenericStartableComponentControllAbstract : GeneralComponentsAbstract, GenericTimeControllInterface
		{
				//Evento a ser chamado quando termina seja lá o que for

				protected OnEvent onFinished ;

				protected bool _isFinished ;

				public bool isFinished ()
				{
						return _isFinished;
				}

				public void AddOnFinishedEventHandler (OnEvent method)
				{
						this.onFinished += method;
				}
		
				public void RemoveOnFinishedEventHandler (OnEvent method)
				{
						this.onFinished -= method;
				}
				protected void RaiseOnFinishedEventHandler ()
				{
						this._isFinished = true;
						if (this.onFinished != null) {
								this.onFinished ();
						}
				}


				protected OnEvent onStart ;


				//variavel aconselhada para se modificar 
				protected bool _isPlaying = false ;
				//Basta modificar o valor da variavel e o metodo já funciona
				public bool isPlaying ()
				{
						return this._isPlaying;
				}
				/// <summary>
				/// Sobreescrever esses metodos, mas não esquecer de chamar o base.Play() ; ao executar o seu próprio Play
				/// No caso, lembre também de ver se o componente está pronto para uso, pois agora é um componente
				/// </summary>
				public virtual void Play ()
				{
						this._isPlaying = true;
				}
				public virtual void Stop ()
				{
						this._isPlaying = false;
				}
				public virtual void Pause ()
				{
						this._isPlaying = false;
				}

				protected void RaiseOnStartEventHandler ()
				{
						if (this.onStart != null) {
								this.onStart ();
						}
				}


				/// <summary>
				/// Encapsuled process event component to init him
				/// </summary>
				/// <param name="component">Component.</param>
				/// <param name="nextStepMethod">Next step method.</param>
				protected void addProcessInitStep (GeneralComponentsAbstract component, OnEvent nextStepMethod)
				{
			
						if (component != null) {
				
								//em paralelo inicia os componentes
								if (! component.isComponentIsReady ()) {
					
										component.AddCompoentIsReadyEventHandler (nextStepMethod);
					
										component.prepare ();
					
										return;
								}
				
								nextStepMethod ();
				
						}
				}

				/// <summary>
				/// Encapsuled process event component
				/// </summary>
				/// <param name="component">Component.</param>
				/// <param name="nextStepMethod">Next step method.</param>
				protected void  addProcessStartStep (GenericStartableComponentControllAbstract component, OnEvent nextStepMethod)
				{
			
						if (component != null) {

								if (! component.isFinished ()) {

										component.AddOnFinishedEventHandler (nextStepMethod);

										component.Play ();

										return;
								}

								nextStepMethod ();
				
						}
				}


		}
}

