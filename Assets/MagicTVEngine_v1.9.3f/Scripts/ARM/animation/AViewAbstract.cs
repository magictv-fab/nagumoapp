﻿using UnityEngine;
using System.Collections;
using ARM.animation;
using ARM.display;

/// <summary>
/// A View Abstract.
/// To extend and turn to have show and hide method
/// </summary>
namespace ARM.abstracts
{
		public abstract class AViewAbstract : MonoBehaviour, AViewInterface
		{
//				SimpleView _simpleView;
				protected void init ()
				{
//						_simpleView = gameObject.AddComponent<SimpleView>();
				}
				// Use this for initialization
				public virtual void Start ()
				{
						init ();
				}
				public virtual void Show ()
				{
						SimpleView.Show (this.gameObject);
				}
				public virtual void Hide ()
				{
						SimpleView.Hide (this.gameObject);
				}
		}
}