﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts ;

using ARM.abstracts ;
using ARM.abstracts.components;

namespace ARM.abstracts.internet
{

	public abstract class InternetInfoAbstract : GeneralComponentsAbstract
	{

		public static bool isWifi ;

		public delegate void OnWifiInfoEventHandler (bool has);
		
		protected OnWifiInfoEventHandler onWifiInfo ;
		
		public void AddWifiInfoEventHandler (OnWifiInfoEventHandler method)
		{
			this.onWifiInfo += method;
		}
		
		
		public void RemoveWifiInfoEventHandler (OnWifiInfoEventHandler method)
		{
			this.onWifiInfo -= method;
		}
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseWifiInfo (bool has)
		{
			//se disparou o evento, é porque está pronto
			this._hasWifi = true;
			if (this.onWifiInfo != null) {
				this.onWifiInfo (has);
			}
		}
		/// <summary>
		/// Mude para true essa boleana quando estiver ready para uso
		/// </summary>
		protected bool _hasWifi = false ;
		public bool hasWifi ()
		{
			return this._hasWifi;
		}



		public abstract void testWifi () ;


	}

}