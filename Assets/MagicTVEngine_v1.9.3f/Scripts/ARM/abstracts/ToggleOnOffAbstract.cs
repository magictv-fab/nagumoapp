﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @author: Renato Seiji Miawaki
/// Toggle on off abstract.
/// </summary>
namespace ARM.abstracts
{
	public abstract class ToggleOnOffAbstract : MonoBehaviour, OnOffListenerEventInterface
	{

		public delegate void TurnChange () ;
		private TurnChange onTurnOn ;
		private TurnChange onTurnOff ;

		public bool activeByDefault = true;
		/// <summary>
		/// Diz sobre este combonente, se ele está ativo e funcionando ou não
		/// </summary>
		protected bool _isActive = true ;

		void Awake(){
			_isActive = activeByDefault;
		}
		public bool DebugIsOn ;
		// Use this for initialization
		public virtual void addTurnOnChange (TurnChange method)
		{
			this.onTurnOn += method;
		}
		public virtual void removeTurnOnChange (TurnChange method)
		{
			this.onTurnOn -= method;
		}
		protected void onTurnOnEvent ()
		{
			if(!_isActive){
				return;
			}
			DebugIsOn = true;
			if (this.onTurnOn != null) {
				this.onTurnOn ();
			}
		}
		public virtual void addTurnOffChange (TurnChange method)
		{
			this.onTurnOff += method;
		}
		public virtual void removeTurnOffChange (TurnChange method)
		{
			this.onTurnOff -= method;
		}
		protected void onTurnOffEvent ()
		{
			if(!_isActive){
				return;
			}
			DebugIsOn = false;
			if (this.onTurnOff != null) {
				this.onTurnOff ();
			}
		}

		/** 
		 * ABAIXO REFERECE A ESTE COMPONENTE ESTAR LIGADO OU DESLIGADO E NÃO AO EVENTO
		  */
		public string getUniqueName(){
			return this.gameObject.name;
		}
		public void turnOn (){
			this._isActive = true;
		}
		public void turnOff (){
			this._isActive = false;
		}
		public void ResetOnOff (){
			this._isActive = this.activeByDefault;
		}
		public bool isActive(){
			return this._isActive;
		}
		public GameObject getGameObjectReference(){
			return this.gameObject;
		}
	}
}