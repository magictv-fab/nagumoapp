using UnityEngine;
using System.Collections;

using ARM.interfaces ;
/// <summary>
/// General components abstract.
/// Todo componente para evento de init e evento de avisar que está "ready"
/// </summary>
namespace ARM.abstracts.components {

	public abstract class GeneralComponentsObjectAbstract : GeneralComponentInterface {

		protected OnEvent onCompoentIsReady ;

		public void AddCompoentIsReadyEventHandler( OnEvent method ){
			this.onCompoentIsReady += method ;
		}

		
		public void RemoveCompoentIsReadyEventHandler( OnEvent method ){
			this.onCompoentIsReady -= method ;
		}
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseComponentIsReady(){
			//se disparou o evento, é porque está pronto
			this._isComponentReady = true ;
			if( this.onCompoentIsReady != null ){
				this.onCompoentIsReady() ;
			}
		}
		/// <summary>
		/// Mude para true essa boleana quando estiver ready para uso
		/// </summary>
		protected bool _isComponentReady = false ;
		public bool isComponentIsReady(){
			return this._isComponentReady ;
		}
		/// <summary>
		/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
		/// Depois de pronta, dispare o evento RaiseComponentIsReady
		/// </summary>
		public abstract void prepare() ;
	}
}