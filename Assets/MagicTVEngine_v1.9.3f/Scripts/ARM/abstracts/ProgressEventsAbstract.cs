﻿using UnityEngine;
using System.Collections;
using ARM.abstracts.components;
using ARM.animation;

namespace ARM.abstracts
{
		/// <summary>
		/// Progress events abstract. For all classes who is posible to dispetch progress and start and stop de process
		/// </summary>
		public abstract class ProgressEventsAbstract : GenericStartableComponentControllAbstract
		{
				public delegate void OnProgressEventHandler (float progress) ;

				private OnProgressEventHandler onProgress;


				public delegate void OnErrorEventHandler (string message) ;

				private OnErrorEventHandler onError;
				protected bool _isComplete ;

				public delegate void OnCompleteEventHandler () ;

				private OnCompleteEventHandler onComplete;

				/// <summary>
				/// O Peso que esse loading representa, por padrao é 1
				/// </summary>
				public float weight = 1f ;
				protected float _currentProgress = 0f ;

				public ProgressEventsAbstract () {
					
					this.weight = Mathf.Clamp (this.weight, 0f, 1f);
					
				}
				/// <summary>
				/// retorna o progresso setado desconsiderando o peso
				/// </summary>
				/// <returns>The current progress.</returns>
				public float GetCurrentProgress () {
					
					return this._currentProgress;
					
				}
				/// <summary>
				/// Retorna o progresso adicionando o peso
				/// </summary>
				/// <returns>The current progress with weight.</returns>
				public float GetCurrentProgressWithWeight () {

						return this._currentProgress * this.weight;

				}
				
				/// <summary>
				/// Raises the progress without weight
				/// </summary>
				/// <param name="progress">Progress.</param>
				virtual protected void RaiseProgress (float progress) {
						
						this._currentProgress = progress ;

						if (onProgress != null) {

							onProgress (progress) ;

						}
						
				}

				public void AddProgressEventhandler (OnProgressEventHandler eventHandler)
				{

						onProgress += eventHandler ;	

				}

				public void RemoveProgressEventhandler (OnProgressEventHandler eventHandler)
				{

						onProgress -= eventHandler ;

				}

				virtual protected void RaiseError (string message)
				{
						if (onError != null) {
								onError (message);
						}	
				}

				public void AddErrorEventhandler (OnErrorEventHandler eventHandler)
				{
						onError += eventHandler;	
				}

				public void RemoveErrorEventhandler (OnErrorEventHandler eventHandler)
				{
						onError -= eventHandler;	
				}

				virtual protected void RaiseCompleteAndTotalProgress () {
						this.RaiseProgress (1f);
						this.RaiseComplete ();
				}

				virtual protected void RaiseComplete () {
						//ARMDebug.LogTime ( "ProgressEvent :: "+this.gameObject.name );
						this._isComplete = true;

						if (onComplete != null) {
								onComplete ();
						}	
				}

				public void AddCompleteEventhandler (OnCompleteEventHandler eventHandler) {
						onComplete += eventHandler;	
				}

				public void RemoveCompleteEventhandler (OnCompleteEventHandler eventHandler)
				{
						onComplete -= eventHandler;	
				}

				public bool isComplete ()
				{
						return this._isComplete;
				}
	
		}
}