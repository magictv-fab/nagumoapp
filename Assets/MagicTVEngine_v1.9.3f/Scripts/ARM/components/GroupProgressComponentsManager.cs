using UnityEngine;
using System.Collections;

using ARM.abstracts ;

namespace ARM.components{

	public class GroupProgressComponentsManager : ProgressEventsAbstract {

		protected GroupComponentsManager groupComponentManager ;

		protected ProgressEventsAbstract[] _components ;

		public GroupProgressComponentsManager(){
			this.groupComponentManager = new GroupComponentsManager () ;
		}

		public void SetProgressComponents( ProgressEventsAbstract[] components ){

			this._components = components ;

			this.groupComponentManager.SetComponents ( components ) ;

			this._isComponentReady = this.groupComponentManager.isComponentIsReady() ;

			if( ! this._isComponentReady ){

				this.groupComponentManager.AddCompoentIsReadyEventHandler( this.InitLoaderListener );
				this.groupComponentManager.prepare() ;

			}

			this.InitLoaderListener ();

		}

		protected void InitLoaderListener(){
			float weigthTotal = 0f ;
			for( int i = 0 ; i < this._components.Length ; i ++ ){
				this._components[ i ].AddProgressEventhandler( progressEvent ) ;
				this._components[ i ].AddCompleteEventhandler( completeEvent ) ;
				if( this._components[ i ].weight >= 1  ){
					//peso demasiado, calcula automático
					this._components[ i ].weight = 1/this._components.Length ;
				}
				weigthTotal += this._components[ i ].weight ;
			}

		}

		protected void completeEvent(){

			for( int i = 0 ; i < this._components.Length ; i ++ ){
				if( ! this._components[ i ].isComplete() ){
					return ;
				}
			}

			this.RaiseComplete () ;
		}

		protected void progressEvent( float subTotal ){
			float total = 0f ;
			for( int i = 0 ; i < this._components.Length ; i ++ ){
				total += this._components[ i ].GetCurrentProgressWithWeight() ;
			}
			if( total > 1 ){
				total = 1 ;
			}
			this.RaiseProgress ( total ) ;
		}

		public override void prepare ()
		{
			//
		}

	}

}