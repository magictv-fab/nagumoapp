using UnityEngine;
using System.Collections;
using ARM.abstracts;
using ARM.interfaces;
using ARM.abstracts.components;
using MagicTV.abstracts;
using MagicTV.vo;
using MagicTV.in_apps;
using ARM.utils.cron;
using System.Linq;

namespace ARM.components
{

	/// <summary>
	///
	/// Group in app abstract components manager.
	///
	/// @author : Renato Seiji Miawaki
	/// @versao : 1.0.1
	///
	/// </summary>
	public class GroupInAppAbstractComponentsManager : InAppAbstract
	{
		protected BundleVO _bundleVO;
		protected InAppAbstract[] _components;
		public int DebugTotalComponents;
		public bool keepGameObjectHierarchy;

		public GameObject[] _debugInsideInApps;
		/// <summary>
		/// Adicione a lista para ser gerenciada
		/// </summary>
		/// <param name="components">Components.</param>
		public void SetComponents (InAppAbstract[] components)
		{

			if (components == null) {
				return;
			}

			Debug.Log ("GC GroupComponentsManager . SetComponents ( " + components.Length + ")  componentes:" + components);

			_debugInsideInApps = new GameObject[components.Length];
			for (int i = 0; i < components.Length; i++) {
				if (components [i] == null) {
					//evitando null point
					Debug.LogError ("GroupInAppAbstractComponentManager . SetComponents ~ componente NULL !!!!!! ");
					continue;
				}
				_debugInsideInApps [i] = components [i].gameObject;
			}

			this._components = components;
			DebugTotalComponents = this._components.Length;
			if (this._isComponentReady) {
				return;
			}

		}

		/// <summary>
		/// Checks all component is ready to use and Raise de event.
		/// </summary>
		protected void checkAllIsReady ()
		{
			if (this._components == null) {
				return;
			}

			for (int i = 0; i < this._components.Length; i++) {
				if (this._components [i] != null) {
					this._components [i].RemoveCompoentIsReadyEventHandler (checkAllIsReady);
					if (!this._components [i].isComponentIsReady ()) {
                      
						this._components [i].AddCompoentIsReadyEventHandler (checkAllIsReady);
						if (!keepGameObjectHierarchy) {
							this._components [i].gameObject.transform.parent = this.gameObject.transform;
						}
						this._components [i].prepare ();
						return;
					} else {
						//component is ready move on
					}
				}
			}
			this.RaiseComponentIsReady ();
		}
		bool _preparing;
		public override void prepare ()
		{
			if (_preparing) {
				return;
			}
			_preparing = true;
			if (this._components == null) {
				this.RaiseComponentIsReady ();
				return;
			}
			if (!(this._components.Length > 0)) {
				this.RaiseComponentIsReady ();
				return;
			}
			if (this._isComponentReady) {
				return;
			}
			checkAllIsReady ();
		}
		/// <summary>
		/// Dos the dispose (in delay? remove comment).
		/// </summary>
		void doDispose ()
		{
			//EasyTimer.SetTimeout (removeGameObject, 400);
			removeGameObject ();
		}
		/// <summary>
		/// Removes the game object.
		/// </summary>
		void removeGameObject ()
		{
			GameObject.DestroyImmediate (this.gameObject);
		}

		public override void Dispose ()
		{
//			Debug.LogError ("DISPOSE"); 
			if (this._components == null) {
				this.doDispose ();
				return;
			}
			if (!(this._components.Length > 0)) {
				this.doDispose ();
				return;
			}
			if (!this._isComponentReady) {
				this.doDispose ();
				return;
			}

			for (int i = 0; i < this._components.Length; i++) {
				if (this._components [i] != null) {
					this._components [i].Dispose ();
				}
			}
			this.doDispose ();
		}

		public override void DoRun ()
		{
			if (this._components == null) {
				return;
			}
			if (!(this._components.Length > 0)) {
				return;
			}
			if (!this._isComponentReady) {
				return;
			}

			for (int i = 0; i < this._components.Length; i++) {
				if (this._components [i] == null) {
					Debug.LogWarning ("GroupInAppAbstractComponentsManager " + i + "  . Run : não encontrou esse componente");

					continue;
				}
				this._components [i].Run ();
			}
		}
		public override void DoStop ()
		{
			if (this._components == null) {
				return;
			}
			if (!(this._components.Length > 0)) {
				return;
			}
			if (!this._isComponentReady) {
				return;
			}
			for (int i = 0; i < this._components.Length; i++) {
				if (this._components [i] == null) {
					Debug.LogWarning ("GroupInAppAbstract . Stop ~ Mais um componente que veio null na lista");
					continue;
				}
				this._components [i].Stop ();
			}
		}
		/// <summary>
		/// Sets the bundleVO to internal bundles
		/// Se o seu InApp já estiver com bundle setado, não use esse metodo para o grupo pois ele vai setar novamente
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		public override void SetBundleVO (BundleVO bundle)
		{
			this._bundleVO = bundle;
			if (!(this._components.Length > 0)) {
				return;
			}
			//repassa pra todos os in apps
			for (int i = 0; i < this._components.Length; i++) {
				if (this._components [i] == null) {
					Debug.LogWarning ("GroupInAppAbstractComponentsManager . SetBundle : não encontrou esse componente");
					continue;
				}
				this._components [i].SetBundleVO (bundle);
			}
		}
		public override string GetInAppName ()
		{
			return "Its a Group!";
		}

	}
}
