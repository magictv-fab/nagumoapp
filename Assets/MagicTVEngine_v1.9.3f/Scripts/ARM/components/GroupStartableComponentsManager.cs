using UnityEngine;
using System.Collections;

using ARM.abstracts ;

using ARM.interfaces ;
using ARM.abstracts.components;
using ARM.animation;

namespace ARM.components{

	public class GroupStartableComponentsManager : GenericStartableComponentControllAbstract {

		protected GenericStartableComponentControllAbstract[] _components ;

		/// <summary>
		/// Adicione a lista para ser gerenciada
		/// </summary>
		/// <param name="components">Components.</param>
		public void SetComponents( GenericStartableComponentControllAbstract[] components ){

			if( components == null ){
				return ;
			}

			this._components = components ;

		}
		/// <summary>
		/// Checks all component is ready to use and Raise de event. 
		/// </summary>
		protected void checkAllIsReady(){
			if( this._components == null ){
				return ;
			}
			for( int i = 0 ; i < this._components.Length ; i ++ ){

				if( ! this._components[ i ].isComponentIsReady() ){
					return ;
				}

			}
			this.RaiseComponentIsReady() ;
		}

		public override void prepare () {

			if( this._components == null ){
				this.RaiseComponentIsReady();
				return;
			}
			if( ! ( this._components.Length > 0 ) ){
				this.RaiseComponentIsReady();
				return;
			}

			checkAllIsReady () ;

			if( this._isComponentReady ){
				return ;
			}

			for( int i = 0 ; i < this._components.Length ; i ++ ){

				this._components[ i ].AddCompoentIsReadyEventHandler( this.checkAllIsReady ) ;

				this._components[ i ].prepare() ;

			}

		}
		private int _nextIndex = 0 ;
		public override void Play () {
			if( this._components == null ){
				return ;
			}
			if( ! ( this._components.Length > 0 ) ){
				return;
			}
			base.Play () ;
			if( ! this.isComponentIsReady() ){
				this.AddCompoentIsReadyEventHandler( Play ) ;
				this.prepare () ;
				return ;
			}
			if( this._nextIndex < this._components.Length ){
				this._components[ this._nextIndex ].AddOnFinishedEventHandler( this.Play ) ;
				
				this._components[ this._nextIndex ].Play() ;
				this._nextIndex++ ;
				return ;
			}
			//chegou aqui, terminou
			this.RaiseOnFinishedEventHandler () ;
		}
		public override void Stop () {
			base.Stop () ;
			if ( this._nextIndex < this._components.Length ) {
				this._components [ this._nextIndex ].Stop () ;
				if( this._nextIndex > 0 ){
					this._nextIndex -- ;
				}
			}
		}
		public override void Pause () {
			base.Pause () ;
			if ( this._nextIndex < this._components.Length ) {
				this._components [ this._nextIndex ].Pause () ;
				if( this._nextIndex > 0 ){
					this._nextIndex -- ;
				}
			}
		}
	}

}