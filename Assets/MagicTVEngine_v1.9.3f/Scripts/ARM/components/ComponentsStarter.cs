using UnityEngine;
using System.Collections;

using ARM.interfaces ;
/// <summary>
/// ComponentsStarter
/// Une todos os componentes para que ele já de start de todos e is ready quando estão todos prontos
/// </summary>
using ARM.abstracts.components;
using System.Collections.Generic;


namespace ARM.components
{

		public class ComponentsStarter : GeneralComponentsAbstract
		{


				public List<GeneralComponentsAbstract> componentsToInit = new List<GeneralComponentsAbstract> () ;

				protected GeneralComponentsAbstract[] components ;

				public void AddComponent (GeneralComponentsAbstract component)
				{

						this.componentsToInit.Add (component);
						this.components = this.componentsToInit.ToArray ();

				}

				public override void prepare ()
				{
						//inicia todos os componantes relacionados
						//Debug.LogError ( "ComponentsStarter . init " ) ;
						if (this.components == null) {
								this.components = this.componentsToInit.ToArray ();
						}
						if (this.components != null) {

								foreach (GeneralComponentsAbstract comp in this.components) {
										if (comp.isComponentIsReady ()) {
												continue;
										}
										comp.AddCompoentIsReadyEventHandler (checkAllIsComplete);
										comp.prepare ();
								}
						}
				}

				void checkAllIsComplete ()
				{
						//Debug.LogWarning ("ComponentsStarter . checkAllIsComplete ~ this.componentsToInit: " + this.componentsToInit);
						if (this.components != null) {

								foreach (GeneralComponentsAbstract comp in this.components) {
										if (! comp.isComponentIsReady ()) {
												return;
										}
								}
						}
						//here all is complete
						this.RaiseComponentIsReady ();
				}
		}
}