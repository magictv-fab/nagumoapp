using UnityEngine;
using System.Collections;

using ARM.abstracts ;

using ARM.interfaces ;
using ARM.abstracts.components;

namespace ARM.components{

	public class GroupComponentsManager : GeneralComponentsObjectAbstract {

		protected GeneralComponentInterface[] _components ;

		/// <summary>
		/// Adicione a lista para ser gerenciada
		/// </summary>
		/// <param name="components">Components.</param>
		public void SetComponents( GeneralComponentInterface[] components ){

			if( components == null ){
				return ;
			}

			//Debug.Log( "GC GroupComponentsManager . SetComponents ( "+components.Length+")  componentes:"+components ) ;

			this._components = components ;

			checkAllIsReady () ;

			if( this._isComponentReady ){
				return ;
			}
			////Debug.Log( "("+this._components.Length+")  componentes:"+this._components ) ;
			for( int i = 0 ; i < this._components.Length ; i ++ ){

				////Debug.Log( "["+i+"]  componente:"+this._components[ i ] ) ;

				this._components[ i ].AddCompoentIsReadyEventHandler( checkAllIsReady ) ;

			}

		}
		/// <summary>
		/// Checks all component is ready to use and Raise de event. 
		/// </summary>
		protected void checkAllIsReady(){
			if( this._components == null ){
				return ;
			}
			for( int i = 0 ; i < this._components.Length ; i ++ ){

				if( ! this._components[ i ].isComponentIsReady() ){
					return ;
				}

			}
			this.RaiseComponentIsReady() ;
		}

		public override void prepare () {

			if( this._components == null ){
				this.RaiseComponentIsReady();
				return;
			}
			if( ! ( this._components.Length > 0 ) ){
				this.RaiseComponentIsReady();
				return;
			}
			checkAllIsReady () ;
			if( this._isComponentReady ){
				return ;
			}

			for( int i = 0 ; i < this._components.Length ; i ++ ){
				this._components[ i ].prepare() ;
			}

		}
	}

}