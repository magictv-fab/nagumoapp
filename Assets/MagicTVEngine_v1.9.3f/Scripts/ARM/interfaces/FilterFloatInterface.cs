﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces{
	public interface FilterFloatInterface  {
		float filter( float value ) ;
	}
}