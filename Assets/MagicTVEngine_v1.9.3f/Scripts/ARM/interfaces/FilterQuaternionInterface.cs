﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces {
	public interface FilterQuaternionInterface  : GenericFilterInterface< Quaternion > {
		new Quaternion filter( Quaternion value ) ;
		new Quaternion filter( Quaternion currentValue, Quaternion nextValue ) ;


	}
}
