﻿using UnityEngine;
using System.Collections;

public interface GenericFilterInterface< TypeClass > {

	TypeClass filter ( TypeClass value ) ;

	
	TypeClass filter ( TypeClass currentValue, TypeClass nextValue ) ;
}
