﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces{
	public interface HistoryVector3Interface {
		
		void save( Vector3 angle ) ;

		Vector3[] getLasts() ;

		Vector3 getMedian() ;

		Vector3 getMean () ;
	}
}