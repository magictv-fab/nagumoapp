﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces{
	public interface HistoryQuaternionInterface {

		void save( Quaternion angle ) ;

		Quaternion[] getLasts( uint length ) ;
		
		Quaternion[] getLasts() ;

		Quaternion getLast() ;

		Quaternion getMedian() ;
		
		Quaternion getMean () ;

	}
}