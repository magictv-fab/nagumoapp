﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces{
	public interface FilterIntInterface  {
		int filter( int vector ) ;
	}
}