﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces{
	public interface FilterVector2Interface : GenericFilterInterface< Vector2 >  {
		new Vector2 filter( Vector2 vector ) ;

		new Vector2 filter( Vector2 current, Vector2 next ) ;

	}
}