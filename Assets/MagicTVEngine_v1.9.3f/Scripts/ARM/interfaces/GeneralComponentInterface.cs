using UnityEngine;
using System.Collections;

namespace ARM.interfaces{

	public delegate void OnComponentIsReadyEventHandler();

	public interface GeneralComponentInterface {

		void AddCompoentIsReadyEventHandler( OnEvent method ) ;

		void RemoveCompoentIsReadyEventHandler( OnEvent method ) ;

		bool isComponentIsReady() ;
		/// <summary>
		/// Implemente o metodo init para fazer tudo que precisa fazer para que sua classe esteja pronta para uso. 
		/// Depois de pronta, dispare o evento RaiseComponentIsReady
		/// </summary>
		void prepare() ;

	}
}