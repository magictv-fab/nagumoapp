﻿using UnityEngine;
using System.Collections;

namespace ARM.interfaces{
	public interface FilterVector3Interface : GenericFilterInterface < Vector3 > {
		new Vector3 filter( Vector3 value ) ;
		new Vector3 filter( Vector3 currentValue, Vector3 nextValue ) ;
	}
}