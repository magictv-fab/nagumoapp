﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Color change.
/// Classe para modificar a cor de um elemento para um array de cores setadas
/// 
/// @author : Renato Seiji Miawaki
/// @version : 1.0
/// 
/// </summary>
namespace ARM.display
{
		public class ColorChange : MonoBehaviour
		{

				public Material materialToChangeColor ;
				public RotateObject objetctToRotate ;
				protected int _indexColor = 0 ;
				public Vector3[] colors ;

				/// <summary>
				/// The _color tochange. The target color
				/// </summary>
				protected Vector3 _colorTochange ;
				protected Vector3 _lastColorChanged ;

				// Use this for initialization
				void Start ()
				{
						//ARMDebug.LogWarning ("ColorChange . start");
						if (this.materialToChangeColor != null && this.objetctToRotate != null && this.colors.Length > 0) {
								//ARMDebug.LogWarning ("ColorChange . start !!! ");
								this.objetctToRotate.AddOnRotateCompleteEventHandler (changeColor);
								this._lastColorChanged = this.colorToVector3 (this.materialToChangeColor.color);
						}
						changeColor ();
				}

				private Vector3 colorToVector3 (Color cor)
				{
						return  new Vector3 (cor.r, cor.g, cor.b);
				}

				void changeColor ()
				{
						//ARMDebug.LogWarning ("ColorChange . change color mudando cor");
						if (this.colors.Length == 0) {
								return;
						}

						this._indexColor++;
						if (this._indexColor >= this.colors.Length) {
								this._indexColor = 0;
						}

						//ARMDebug.LogWarning ("ColorChange . change color mudando cor MESMO");
						this._colorTochange = this.colors [this._indexColor];
				}

				void Update ()
				{
						Vector3 currentColor = Vector3.Lerp (this._lastColorChanged, this._colorTochange, Time.deltaTime * 3);
						this._lastColorChanged = currentColor;
						Color cor = new Color (this._lastColorChanged.x, this._lastColorChanged.y, this._lastColorChanged.z);
						if (this.materialToChangeColor != null)	
								this.materialToChangeColor.color = cor;
				}
		}
}