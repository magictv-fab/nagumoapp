﻿using UnityEngine;
using System.Collections;

namespace ARM.display
{
		/// <summary>
		/// Class utilitária Simple view:
		/// Serve para dar hide e show nas presentations que trabalham com render e collider.
		/// V 1.0
		/// Author: Leal
		/// 
		/// Atenção: Não extender essa classe
		/// </summary>
		public class SimpleView : MonoBehaviour
		{
				private bool _state;
				private bool _onOff;

				/// <summary>
				/// Shows the presentation.
				/// </summary>
				public static void Show (GameObject presentation)
				{
			
						Renderer[] rendererComponents = presentation.GetComponentsInChildren<Renderer> (true);
						Collider[] colliderComponents = presentation.GetComponentsInChildren<Collider> (true);
			
			
						changeState (presentation, rendererComponents, colliderComponents, true);
				}

				/// <summary>
				/// Hides the presentation.
				/// </summary>
				public static void Hide (GameObject presentation)
				{
						Renderer[] rendererComponents = presentation.GetComponentsInChildren<Renderer> (true);
						Collider[] colliderComponents = presentation.GetComponentsInChildren<Collider> (true);
			
						changeState (presentation, rendererComponents, colliderComponents, false);
				}

				static void  changeState (GameObject presentation, Renderer[] rendererComponents, Collider[] colliderComponents, bool _state)
				{
						// Enable rendering
						if (rendererComponents != null && rendererComponents.Length > 0) {
								foreach (Renderer component in rendererComponents) {
										component.enabled = _state;
								}
						}
			
						// Enable colliders:
						if (colliderComponents != null && colliderComponents.Length > 0) {
								foreach (Collider component in colliderComponents) {
										component.enabled = _state;
								}
						}


						if (presentation == null) {
								return;
						}

						Renderer render = presentation.GetComponent<Renderer> ();

						if (render != null) {
								render.enabled = _state;
						}
				}

				public static void setChildrenOnOff (bool _onOff, GameObject presentation)
				{
						if (presentation != null && presentation.gameObject != null) {
								presentation.gameObject.SetActive (_onOff);
						}
				}
		}
}