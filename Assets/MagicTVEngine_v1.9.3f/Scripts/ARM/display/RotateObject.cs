﻿using UnityEngine;
using System.Collections;


namespace ARM.display
{
		public class RotateObject : MonoBehaviour
		{

				public delegate void OnRotateComplete ();

				protected OnRotateComplete _onRotateComplete ;

				public float speedy = 2f ;

				public Vector3 _currentRotation ;
				public void AddOnRotateCompleteEventHandler (OnRotateComplete method)
				{
						this._onRotateComplete += method;
				}
	
				public void RemoveOnRotateCompleteEventHandler (OnRotateComplete method)
				{
						this._onRotateComplete -= method;
				}
				protected void RaiseOnRotateComplete ()
				{
						if (this._onRotateComplete != null) {
								this._onRotateComplete ();
						}
				}
				// Use this for initialization
				void Start ()
				{

						this._currentRotation = this.transform.localRotation.eulerAngles;
				}
	
				// Update is called once per frame
				void Update ()
				{
						Vector3 vec = this._currentRotation;
						vec.y += Time.deltaTime * speedy;
						if (vec.y > 360) {
								vec.y -= 360;
								//ARMDebug.LogWarning( "RotateObject Update Rodou" );
								this.RaiseOnRotateComplete ();
						}
						this._currentRotation = vec;
						this.transform.localRotation = Quaternion.Euler (vec);
				}
		}
}