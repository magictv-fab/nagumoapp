﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class AmIVisible : MonoBehaviour
{

		public delegate void OnChangeVisible (bool m);

		public OnChangeVisible ChangeVisible;

		public bool visible;

		void OnBecameVisible ()
		{
				RaiseChangeVisible (true);
		}

		void OnBecameInvisible ()
		{
				RaiseChangeVisible (false);
		}

		private void RaiseChangeVisible (bool visible)
		{
				this.visible = visible;
				if (ChangeVisible != null)
						ChangeVisible (visible);
		}
}
