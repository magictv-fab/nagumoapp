﻿using UnityEngine;
using System.Collections;
using System.Linq;
using MagicTV.abstracts ;
using MagicTV.abstracts.screens ;

/// <summary>
/// Tela onde escolhe qual projeto deve ser aberto
/// No caso do nosso primeiro projeto é só um projeto
/// executar o metodo this.RaiseProjectChosen(  "nomeDoProjeto" ) ; 
/// assim que o usuario escolher o projeto (no caso do prototipo, ao clicar em iniciar abre sempre o mesmo projeto, o único)
/// </summary>
using MagicTV.globals.events;


namespace MagicTV.gui.screens
{
		public class InAppGUIScreen : MenuScreenAbstract
		{

				public override void prepare ()
				{ 
						if (WindowControlListScript.Instance != null) {
							//PRINTSCREEN
							
							((MagicTVWindowInAppGUIControllerScript)WindowControlListScript.Instance.WindowInAppGUI).onScreenShotComplete += AppRootEvents.GetInstance().GetScreen().RaisePrintScreenCompleted;
										
							((MagicTVWindowInAppGUIControllerScript)WindowControlListScript.Instance.WindowInAppGUI).onClickBack += AppRootEvents.GetInstance().GetScreen().RaiseGoBackCalled;
							
							//Update
							WindowControlListScript.Instance.WindowInAppGUI.onUpdateClick += UpdateMagicTVContent;
						}
						ContentEvents.GetInstance ().onUpdateComplete += UpdateComplete;
						
						//Clear Cache
						if (WindowControlListScript.Instance != null) {
							((MagicTVWindowInAppGUIControllerScript)WindowControlListScript.Instance.WindowInAppGUI).onClickClearCache += RaiseClearCacheCalled ;

							((MagicTVWindowInAppGUIControllerScript)WindowControlListScript.Instance.WindowInAppGUI).onClickReset += RaiseResetCalled ;
						}

						this.RaiseComponentIsReady ();
				}
				void UpdateMagicTVContent ()
				{
					ContentEvents.GetInstance ().RaiseUpdate ();
				}
				void UpdateComplete ()
				{
					WindowControlListScript.Instance.WindowInAppGUI.UpdateDone ();
				}
				public override void hide ()
				{
						//Debug.Log ("InAppGUIScreen HIDE");
						WindowControlListScript.Instance.WindowInAppGUI.Hide ();
				}
		
				public override void show ()
				{
						//Debug.Log ("InAppGUIScreen show");
						WindowControlListScript.Instance.WindowInAppGUI.Show ();
				}
		}
}