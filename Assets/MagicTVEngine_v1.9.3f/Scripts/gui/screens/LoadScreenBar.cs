
using UnityEngine;
using System.Collections;
using System.Linq;
using MagicTV.abstracts;
using MagicTV.abstracts.screens;
using MagicTV.globals;

/// <summary>
/// Tela para mostrar o processo de carregamento do projeto
/// </summary>
using ARM.utils.cron;


namespace MagicTV.abstracts.screens
{
	public class LoadScreenBar : LoadScreenAbstract
	{
		protected Perspective _currentPerspective;

		public static LoadScreenBar Instance;

		public LoadScreenBar ()
		{
			Instance = this;
		}

		public override void prepare ()
		{
			this.RaiseComponentIsReady ();
		}

		void Start ()
		{
			// Abaixo é o acesso à classe GenericWindow com o nome da screen em questão

			// Na teoria esse cancel funciona quando o user clica em cancelar o download

			//this.RaiseComponentIsReady ();
		}


		/// <summary>
		/// Sets the message to show for user
		/// </summary>
		/// <param name="message">Message.</param>
		public override void setMessage (string message)
		{
			//Por enquanto não fazer mais que isso
			//Debug.LogWarning ( message ) ;
		}

		/// <summary>
		/// Sets the percent loaded.
		/// Lembrando que o total é 1 e o mínimo  é 0
		/// </summary>
		/// <param name="total">Total.</param>
		public override void setPercentLoaded (float total)
		{

			WindowControlListScript.Instance.WindowSplashDownloadProgress.Progress = total;
		}

		int _timeoutToHide ;
		/// <summary>
		/// Hide this instance.
		/// </summary>
		public override void hide ()
		{
//            Debug.LogError("HIDEOU a tela inicial de download");
			WindowControlListScript.Instance.WindowSplashDownloadProgress.Hide ();
		}

		public override void HideNow ()
		{
			// Aqui nao tem nada, pq na teoria nao existe esse hideNow nessa classe.
		}

		/// <summary>
		/// Show this instance.
		/// </summary>
		public override void show ()
		{
//			WindowControlListScript.Instance.WindowInAppGUI.Show ();
			WindowControlListScript.Instance.WindowSplashDownloadProgress.Show ();
			//WindowControlListScript.Instance.WindowInAppListWaiting.Hide ();
		}
	}
}
