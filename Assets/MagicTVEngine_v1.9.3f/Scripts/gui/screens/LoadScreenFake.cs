using UnityEngine ;
using System.Collections ;
using System.Linq ;
using MagicTV.abstracts ;
using MagicTV.abstracts.screens ;
using MagicTV.globals ;
/// <summary>
/// Tela para mostrar o processo de carregamento do projeto
/// </summary>
namespace MagicTV.abstracts.screens
{
	public class LoadScreenFake : LoadScreenAbstract
	{
				
		public bool _active = false ;
				
		Perspective _currentPerspective ;

		string _message = "" ;
		public int totalInt = 0;
		protected float _showedTotal = 0f ;
		public override void prepare ()
		{
			this.RaiseComponentIsReady ();	

		}
				
		void OnGUI ()
		{
					
			if (this._active == true) {
						
				GUI.Box (new Rect (100, 10, 600, 30), "LoadScreenFake " + this._currentPerspective);
				GUI.Box (new Rect (100, 100, 600, 30), "mensagem do sistema:" + this._message);
				if (this._currentPerspective == Perspective.JUST_SHOW_PROGRESS_WITHOUT_CANCEL || this._currentPerspective == Perspective.PROGRESS_WITH_CANCEL) {
					this._showedTotal = Mathf.Lerp (this._showedTotal, (float)this.totalInt, Time.deltaTime);
					GUI.Box (new Rect (100, 200, 600, 30), "porcentagem: " + this.totalInt + "%");
					GUI.Box (new Rect (100, 230, 6 * (this._showedTotal), 10), "");
				}
				if (this._currentPerspective == Perspective.PROGRESS_WITH_CANCEL) {
					if (GUI.Button (new Rect (10, 310, 200, 30), "CANCELAR!")) {
						this.RaiseLoadCanceled ();
					}
				}
				if (this._currentPerspective == Perspective.ASK_TO_DOWNLOAD) {
					if (GUI.Button (new Rect (100, 200, 200, 30), "download?")) {
						this.RaiseLoadConfirmed ();
					}
					if (GUI.Button (new Rect (400, 200, 200, 30), "Pular")) {
						this.RaiseLoadCanceled ();
					}
				}
			}
		}
				
		/// <summary>
		/// Sets the message to show for user
		/// </summary>
		/// <param name="message">Message.</param>
		public override void setMessage (string message)
		{
			//Por enquanto não fazer mais que isso
			//Debug.LogWarning (message);
		}

		/// <summary>
		/// Sets the percent loaded.
		/// Lembrando que o total é 1 e o mínimo  é 0
		/// </summary>
		/// <param name="total">Total.</param>
		public override void setPercentLoaded (float total)
		{

			this.totalInt = Mathf.RoundToInt (total * 100);
					

		}
		public override void HideNow ()
		{
			//
		}

		/// <summary>
		/// Hide this instance.
		/// </summary>
		public override void hide ()
		{
			this._active = false;
						
		}

		/// <summary>
		/// Show this instance.
		/// </summary>
		public override void show ()
		{
			this._active = true;
		}
	}
}