using UnityEngine;
using System.Collections;
using System.Linq;
using MagicTV.abstracts ;
using MagicTV.abstracts.screens ;

/// <summary>
/// Tela onde escolhe qual projeto deve ser aberto
/// No caso do nosso primeiro projeto é só um projeto
/// executar o metodo this.RaiseProjectChosen(  "nomeDoProjeto" ) ; 
/// assim que o usuario escolher o projeto (no caso do prototipo, ao clicar em iniciar abre sempre o mesmo projeto, o único)
/// </summary>
using MagicTV.globals;
using MagicTV.vo;
using MagicTV.abstracts.device;
using MagicTV.globals.events ;

namespace MagicTV.gui.screens
{
		public class HowToScreen : GeneralScreenAbstract
		{

				public override void prepare ()
				{
						AppRootEvents.GetInstance ().GetScreen ().AddShowPageCalledEventHandler (this.show);

//						WindowControlListScript.Instance.WindowHowToPopUp.onHide += this.checkSelected;
						this.RaiseComponentIsReady ();
				}

				void checkSelected ()
				{
						//verifica se está checkada a opção de nunca mais visualizar
				}

				public override void hide ()
				{

//						WindowControlListScript.Instance.WindowHowToPopUp.Hide ();

				}
		
				public override void show ()
				{
//						WindowControlListScript.Instance.WindowHowToPopUp.Show ();
				}
		}
}