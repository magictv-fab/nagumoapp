
using UnityEngine;
using System.Collections;
using System.Linq;
using MagicTV.abstracts;
using MagicTV.abstracts.screens;
using MagicTV.globals;

/// <summary>
/// Tela para mostrar o processo de carregamento do projeto
/// </summary>
using ARM.utils.cron;
using MagicTV.globals.events;
using Vuforia;

namespace MagicTV.abstracts.screens
{
	public class InitialSplashScreen : LoadScreenAbstract
	{
		protected Perspective _currentPerspective;

		public static InitialSplashScreen Instance;

		public InitialSplashScreen ()
		{
			Instance = this;
		}

		public override void prepare ()
		{
			this.RaiseComponentIsReady ();
		}

		void Start ()
		{
			// Abaixo é o acesso à classe GenericWindow com o nome da screen em questão

			// Na teoria esse cancel funciona quando o user clica em cancelar o download

			//this.RaiseComponentIsReady ();
			WindowControlListScript.Instance.WindowDownloadProgress.onClickCancelDownload += this.RaiseLoadCanceled;

            //AppRootEvents.GetInstance().GetStateMachine().AddChangeToStateEventHandler(onStateChange);

            //						((RiachueloMagicTVWindowInAppGUIControllerScript)WindowControlListScript.Instance.WindowInAppGUI).onClickBack +=


            //						WindowControlListScript.Instance.WindowSplashDownloadAvaible.onClickConfirmDownload += this.RaiseLoadConfirmed;
            //						WindowControlListScript.Instance.WindowSplashDownloadAvaible.onClickCancelDownload += this.RaiseLoadCanceled;

        }

		/// <summary>
		/// Quando mudar o processo para updating, damos hide da InitialSplash
		/// </summary>
		/// <param name="state">State.</param>
		private void onStateChange (StateMachine state)
		{
			if (state == StateMachine.OPEN) {
//				this.hide ();
			}
		}

		/// <summary>
		/// Sets the message to show for user
		/// </summary>
		/// <param name="message">Message.</param>
		public override void setMessage (string message)
		{
			//Por enquanto não fazer mais que isso

			//Debug.LogWarning ( message ) ;
		}

		/// <summary>
		/// Sets the percent loaded.
		/// Lembrando que o total é 1 e o mínimo  é 0
		/// </summary>
		/// <param name="total">Total.</param>
		public override void setPercentLoaded (float total)
		{
			//int totalInt = Mathf.RoundToInt( total*100 ) ;
//            Debug.LogWarning("LoadScreen . setPercentLoad ~ " + total);
			WindowControlListScript.Instance.WindowDownloadProgress.Progress = total;
			WindowControlListScript.Instance.WindowSplashDownloadProgress.Progress = total;
		}

		int _timeoutToHide ;
		/// <summary>
		/// Hide this instance.
		/// </summary>
		public override void hide ()
		{
			//Esse é a bolinha
//			EasyTimer.ClearInterval (this._timeoutToHide);
//			this._timeoutToHide = EasyTimer.SetTimeout (HideNow, 10000);
//			WindowControlListScript.Instance.WindowDownloadProgress.Hide ();
			//						WindowControlListScript.Instance.WindowSplashDownloadAvaible.Hide ();
//			WindowControlListScript.Instance.WindowSplashDownloadProgress.Hide ();
			//
			WindowControlListScript.Instance.WindowInAppListWaiting.Hide ();
		}
		public override void HideNow ()
		{
			EasyTimer.ClearInterval (this._timeoutToHide);
		}


		/// <summary>
		/// Show this instance.
		/// </summary>
		public override void show ()
		{
			//WindowControlListScript.Instance.WindowInAppGUI.Show ();
			//						WindowControlListScript.Instance.WindowDownloadProgress.Show ();
//			WindowControlListScript.Instance.WindowInAppListWaiting.Hide ();
		}
	}
}
