using UnityEngine;
using System.Collections;

using ARM.device;

using MagicTV.abstracts.screens;
using MagicTV.abstracts;
using MagicTV.globals;
using MagicTV.globals.events;
using MagicTV.processes;
using ARM.components;
using ARM.interfaces;
using ARM.abstracts.components;
using System;
using ARM.utils.cron;

namespace MagicTV.gui
{
	public class GUILoader : GUILoaderAbstract
	{
		
		public LoadScreenAbstract _loadScreen ;

        public InitialSplashScreen _initialSplashScreen;

		public MenuScreenAbstract _menuScreen ;
        GroupComponentsManager group;
        //  void OnGUI(){
        //  	if(GUILayout.Button("GO")){
        //  		Debug.Log("TEST FOCE UPDATE");
        //  		ContentEvents.GetInstance().RaiseUpdate();
        //  	}
        //  }

        public override void prepare ()
		{


			ScreenEvents.GetInstance().AddSetPINEventHandler( setGUIPin );

			//ligar o evento de tracking com vibrate -> DeviceControll.vibrate ;
			AppRootEvents.GetInstance ().GetScreen ().onRaiseShowCloseButton += showButtonClose;
			AppRootEvents.GetInstance ().GetScreen ().onRaiseHideCloseButton += hideButtonClose;
			
			if (WindowControlListScript.Instance != null) {
				((GenericWindowControllerScript)WindowControlListScript.Instance.WindowScreenClose).onHide += StopPresentation;
			}

			//preparando as telas internas
			 group = new GroupComponentsManager ();

			GeneralComponentsAbstract[] comps = new GeneralComponentsAbstract[ 2 ] {
				this._loadScreen, 
				this._menuScreen
			};

			group.SetComponents ( comps );
			if (group.isComponentIsReady()) {

                //estão todos prontos, ready
                
                RaiseComponentIsReady();
                return;
			}

			group.AddCompoentIsReadyEventHandler (this.initInternalComponentListeners);
			group.prepare ();
			return;
		}

		void setGUIPin( string pin){
			WindowControlListScript.Instance.WindowAbout.SetPINText(pin);
		}

		void initInternalComponentListeners(){
            group.RemoveCompoentIsReadyEventHandler(this.initInternalComponentListeners);
            this._menuScreen.onResetCalled += ResetMagicTV ;
			this._menuScreen.onClearCacheCalled += ClearMagicTVCache ;
			this._menuScreen.onPrintScreenCalled += AppRootEvents.GetInstance ().GetScreen ().RaisePrintScreenCalled;

			this.RaiseComponentIsReady ();
		}

		void StopPresentation ()
		{
//			Debug.Log (" GUI Loader - StopPresentation - ");
			AppRootEvents.GetInstance ().RaiseStopPresentation ();
		}

		
		void showButtonClose ()
		{
			//mostrar o botao fechar
			if (WindowControlListScript.Instance != null) {
				((GenericWindowControllerScript)WindowControlListScript.Instance.WindowScreenClose).Show ();
			}
		}
		void hideButtonClose ()
		{
			//esconder o botao fechar
			if (WindowControlListScript.Instance != null) {
				((GenericWindowControllerScript)WindowControlListScript.Instance.WindowScreenClose).Hide ();
			}
		}
		void ResetMagicTV(){
			AppRootEvents.GetInstance ().GetDevice ().RaiseResetClicked();
		}

		void ClearMagicTVCache ()
		{

            Debug.LogWarning("GUI Chamando Clear MagicTV....");
			//AppRootEvents.GetInstance ().GetDevice ().RaiseResetClicked (); //<<< atualizei aqui mas mesmo assim comentei, pois agora a intenção é a de apagar os arquivos locais e não reset
			AppRootEvents.GetInstance ().GetDevice ().RaiseWantToClearCache();

		}



		public override LoadScreenAbstract getLoadScreen ()
		{
//			 _TODO: Fazer alguma maneira que busque uma LoadScreenAbstract
			return this._loadScreen;
		}

        public override LoadScreenAbstract getInitialSplashScreen()
        {
            return this._initialSplashScreen;
        }

        public override MenuScreenAbstract getMenuScreen(){
			return this._menuScreen;
		}

	}
}