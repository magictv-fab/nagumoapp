﻿using UnityEngine;
using System.Collections;
using MagicTV.abstracts ;
using MagicTV.globals;
using MagicTV.globals.events;
using MagicTV.vo;
using MagicTV.abstracts.device;
using System.Linq;
using System.Collections.Generic;
using MagicTV.utils;
using ARM.components;
using InApp;
using MagicTV.download;
using MagicTV.processes;
using MagicTV.abstracts.screens;


namespace MagicTV.gui

{
	public class CenterExitButton : MonoBehaviour 
	{
		public GameObject buttonCenter, buttonOriginExit;
		private const float delay = 2f;

		void Start () 
		{
//			AppRootEvents.GetInstance ().GetScreen ().onRaiseShowCloseButton += Show;
		}

		void LateUpdate()
		{
			if(PresentationController.tracking)
				buttonCenter.SetActive (false);
		}

		void Update () 
		{

			//verifica se foi clicado (tocado na tela) e liga o GO do botao X com delay para o botao desaparecer.
			if (Input.GetMouseButtonDown (0) && InAppActorAbstract.centerExit && !GameObject.Find("WindowInAppPopUp") && !PresentationController.tracking && PresentationController.checkTracking) 
			{
				buttonCenter.SetActive (true);
				buttonOriginExit.SetActive (false);

				StartCoroutine (Delay ());
			}
		}

		private IEnumerator Delay()
		{
			yield return new WaitForSeconds (delay);
			buttonCenter.SetActive (false);

		}

	}
}