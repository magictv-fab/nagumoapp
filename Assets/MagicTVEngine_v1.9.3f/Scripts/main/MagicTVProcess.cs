using UnityEngine;
using System.Collections;
using ARM.abstracts;
using ARM.animation;
using MagicTV.globals;
using AMR.device;
using MagicTV.globals.events;
using Vuforia;

namespace MagicTV
{
	/// <summary>
	/// Magic TV process .
	/// @author : Renato Seiji Miawaki
	/// 
	/// Encapsulating and putting in order the main events
	/// 
	/// </summary>



	public class MagicTVProcess : GenericStartableComponentControllAbstract
	{


		/// <summary>
		/// The init process component.
		/// Instantiate and prepare all components and wait for all is ready
		/// 
		/// 
		/// </summary>
		public GenericStartableComponentControllAbstract initProcessComponent;
		public GenericStartableComponentControllAbstract updateProcessComponent;

		public GenericStartableComponentControllAbstract magicPresentationsController;

		protected InAppAnalytics _analytics;

		
//		public string Version = "1.0.0";
		public bool autoPlayMode = true;

		public bool debugMode = false;

		// Use this for initialization
		void Awake ()
		{


			//Debug.LogError ("0 MainMagicTV Awake !!!!! ");

			if (this.autoPlayMode) {
				this.AddCompoentIsReadyEventHandler (this.Play);
			}
			prepare ();

			VuforiaBehaviour.Instance.RegisterVuforiaStartedCallback (OnVuforiaStarted);


			if (WindowControlListScript.Instance != null && WindowControlListScript.Instance.WindowInAppListWaiting != null) {
				WindowControlListScript.Instance.WindowInAppListWaiting.Show ();
			}

		}
		public string _lastTrackFoundedEvent;
		void checkTrackoutDebug (string s)
		{
			if(s == "debug"){
				debugMode = false ;
			}
		}

		void checkTrackinDebug (string s)
		{
			_lastTrackFoundedEvent = s;
			if(s == "debug"){
				debugMode = true ;
			}
		}

		void DebugClearCache ()
		{
			//seta como is restarting para que inicie a cena main automaticamente pois está só limpando o cache

			AppRootEvents.GetInstance ().GetDevice ().RaiseReset ();

		}

		void OnGUI ()
		{

			if (this.debugMode) {

				if (GUI.Button (new Rect (10, 100, 100, 100), "RESET")) {
					DebugClearCache ();
				}//endif
				GUI.Label (new Rect( 10, 200, Screen.width, Screen.height/3 ), "Version: "+ MagicApplicationSettings.AppVersionNumber +"  \n GOOGLE ID:"+MagicApplicationSettings.Instance.GoogleAnalitcsID) ;
			}//endif debug
		}
		public bool noPromptOnEditor = true;
		public void ClearCacheProcess(){
			if(this._resetClicked == true){
				//já está no cache
				return;
			}
			Debug.LogError ("ClearCacheProcess");
			#if UNITY_EDITOR
				Debug.LogWarning ("No Editor é sem vaselina - ClearCache ....");    
				OnDialogCloseCache (MNDialogResult.YES);
				return;
			#endif
			
			//TODO: Prompt se realmente quer limpar o cache
			MobileNativeDialog dialog = new MobileNativeDialog ("Liberar Espaço no Dispositivo", "Liberar espaço no dispositivo.\nEsta ação mantém a boa performance do seu aplicativo.\n", "Sim", "Não");

			
			dialog.OnComplete += OnDialogCloseCache;
		}
		private void OnDialogCloseCache (MNDialogResult result)
		{
			Debug.LogError ("OnDialogCloseCache");
			//parsing result
			switch (result) {
			case MNDialogResult.YES:
				//Comando escutado pelo device cache file manager que apaga só as apresentações baixadas e atualiza as informações relativas a isso

				AppRootEvents.GetInstance ().GetDevice ().RemoveClearCacheCompletedEventHandler(clearCacheCompleted);
				AppRootEvents.GetInstance ().GetDevice ().RemoveClearCacheCompletedEventHandler(nothingToClear);

				AppRootEvents.GetInstance ().GetDevice ().AddClearCacheCompletedEventHandler(clearCacheCompleted);
				AppRootEvents.GetInstance ().GetDevice ().AddClearCacheNothingToDeleteEventHandler(nothingToClear);
				AppRootEvents.GetInstance ().GetDevice ().RaiseClearCache ();
				break;
			case MNDialogResult.NO:
				Debug.Log ("No button pressed");
				break;
			}
		}

		void nothingToClear ()
		{
			AppRootEvents.GetInstance ().GetDevice ().RemoveClearCacheCompletedEventHandler(nothingToClear);
			AppRootEvents.GetInstance ().GetDevice ().AddClearCacheNothingToDeleteEventHandler(nothingToClear);
			MobileNativeMessage dialog = new MobileNativeMessage ("Liberar Espaço no Dispositivo", "Não existem mais apresentações para apagar.");
		}

		void clearCacheCompleted ()
		{
			AppRootEvents.GetInstance ().GetDevice ().RemoveClearCacheCompletedEventHandler(clearCacheCompleted);
			MobileNativeMessage dialog = new MobileNativeMessage ("Liberar Espaço no Dispositivo", "Todas as apresentações baixadas foram apagadas com sucesso.");
		}

		bool _resetClicked;

		public void ResetProcess ()
		{
			this._resetClicked = true;
			Debug.LogError ("ResetProcess");

 #if UNITY_EDITOR
				Debug.LogWarning ("No Editor é sem vaselina - ClearCacheProcess ....");    
				OnDialogClose (MNDialogResult.YES);
				return;
#endif

			//TODO: Prompt se realmente quer limpar o cache
			MobileNativeDialog dialog = new MobileNativeDialog ("RESET", "Deseja resetar todo o sistema?", "Sim", "Não");
			dialog.OnComplete += OnDialogClose;
		}

		private void OnDialogClose (MNDialogResult result)
		{
			Debug.LogError ("OnDialogClose");
			//parsing result
			switch (result) {
			case MNDialogResult.YES:
				//evento de limpar cache
				doResetAll();

				break;
			case MNDialogResult.NO:
				Debug.Log ("No button pressed");
				break;
			}
			this._resetClicked = false;
		}

		void doResetAll(){
			//comando ouvido pelo DeviceCacheFileManager que apaga todas as pastas relacionadas
			AppRootEvents.GetInstance ().GetDevice ().RaiseReset ();
			Debug.LogWarning ("[MAIN] RESET AND RESTARTING MAGIC TV");
			Benchmark.Instance.retestQuality ();
			DeviceInfo.Instance.SetCurrentInfoVersionControlId (1);
			DeviceInfo.Instance.SaveCurrentInfoVersionControlId ();
			AppRootEvents.GetInstance ().GetAR ().AddChangeARToggleHandler (doRestartLevel);
			AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOff () ;
		}
		void doRestartLevel (bool b)
		{
			Debug.Log ("[ MAIN ] . doRestartLevel " + b);
			//if (! b) {

			AppRootEvents.GetInstance ().GetAR ().RemoveChangeARToggleHandler (doRestartLevel);
			Application.LoadLevel (Application.loadedLevelName);


			//}
		}

		private void OnVuforiaStarted ()
		{
			bool focusModeSet = CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
			if (!focusModeSet) {
				Debug.Log ("Failed to set focus mode (unsupported mode).");
			}

		}
		public override void prepare ()
		{
			AppRootEvents.GetInstance().GetAR().onTrackIn += checkTrackinDebug;
			AppRootEvents.GetInstance().GetAR().onTrackOut += checkTrackoutDebug;

			_analytics = InAppAnalytics.GetAnalytics ("magic-tv-v-" + MagicApplicationSettings.AppVersionNumber, MagicApplicationSettings.Instance.GoogleAnalitcsID);
			_analytics.init ();

			_analytics.Open ();
			AppRootEvents.GetInstance ().GetDevice ().AddWantToClearCacheEventHandler (ClearCacheProcess);
			AppRootEvents.GetInstance ().GetDevice ().AddResetClickedEventHandler (ResetProcess);
			this.RaiseComponentIsReady ();
		}

		public override void Play ()
		{

			base.Play ();
			AppRootEvents.GetInstance ().GetStateMachine ().RaiseChangeToState (StateMachine.INIT);
			//after complete, call initProcessComplete
			this.addProcessInitStep (this.initProcessComponent, this.initProcessComplete);
			//Debug.LogWarning ("[ MTV ] MagicTVProcess . Play ");
		}

		void initProcessComplete ()
		{
			//now all components and initial tasks is done, so start de upgrade process
			//when process step is complete or canceled, call de processStepComplete method
			// *******  START UPDATE PROCESS
			//Debug.LogWarning ("[ MTV ] MagicTVProcess . initProcessComplete ");
			AppRootEvents.GetInstance ().GetStateMachine ().RaiseChangeToState (StateMachine.UPDATE);
			this.addProcessStartStep (this.updateProcessComponent, this.updateProcessStepComplete);

		}

		void updateProcessStepComplete ()
		{
			if (DeviceInfo.Instance != null) {
				_analytics.SendEvent (DeviceInfo.Instance.GetUserAgent (), "user agent");
			}
			_analytics.SendEvent (AppRoot.device_id, "device id");
			//now, the open process is done, so, start de app
			AppRootEvents.GetInstance ().GetStateMachine ().RaiseChangeToState (StateMachine.OPEN);
			// *******  START MAGIC TV APP
			this.addProcessStartStep (this.magicPresentationsController, this.magicPresentationIsDone);


			//Debug.LogWarning ("[ MTV ] MagicTVProcess . updateProcessStepComplete ");
		}


		/// <summary>
		/// Magics the presentation is ready.
		/// </summary>
		void magicPresentationIsDone ()
		{

			//here the magic tv app has ben closed or call is done for some reason
			_analytics.SendEvent ("done magictv " + AppRoot.device_id, "done");

			//Debug.LogWarning ("MagicTVProcess . magicPresentationIsReady ");

			this.Stop ();
		}

		/// <summary>
		/// Stop this instance and ALL components.
		/// </summary>
		public override void Stop ()
		{
			base.Stop ();
			this.initProcessComponent.Stop ();
			this.updateProcessComponent.Stop ();
			this.magicPresentationsController.Stop ();
			_analytics.SendEvent ("magictv", "stop");
		}
		public override void Pause ()
		{
			base.Pause ();
			//there is no pause
		}

	}
}