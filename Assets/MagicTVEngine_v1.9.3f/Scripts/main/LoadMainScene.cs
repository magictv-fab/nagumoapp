﻿using UnityEngine;
using System.Collections;
using MagicTV.globals.events;

public class LoadMainScene : MonoBehaviour
{
    public string LevelName = "main";
    public static bool started = false;
    public Material magicTVSkyBox;

    // Use this for initialization
    void Start()
    {

        if (magicTVSkyBox != null)
        {
            RenderSettings.skybox = magicTVSkyBox;
        }
        //passar essa parte para o onload da cena
        if (started)
        {
            //se veio do clear cache, entao inicia automaticamente
            DoStartMagic();
            return;
        }
		MagicApplicationSettings.Instance.onAppReady += AppStarted;
    }

	void AppStarted(){
		Debug.Log(" Starteia powa! ");
		if(!started){
			DoStartMagic();
		}
	}

	protected void DoStartMagic(){
		//Seta que não precisa mas iniciar automatico pois não veio do clear cache
		WindowControlListScript.Instance.WindowInAppGUI.Hide();

		if (PlayerPrefs.GetInt ("firstStart") == 0)
		{
			StartCoroutine(FirstStart());
		} 
		else 
		{
			PlayerPrefs.SetInt ("isRestarting", 0);
			PlayerPrefs.Save ();
			Debug.Log ("DO StartMagic Chamado");
			started = true;
			Application.LoadLevelAdditiveAsync (LevelName);
			//Application.LoadLevelAdditiveAsync ("FaceTrackerARSample 1");
		}
	}

	private IEnumerator FirstStart()
	{
		PlayerPrefs.SetInt ("isRestarting", 0);
		PlayerPrefs.Save ();
		Debug.Log ("DO StartMagic Chamado");
		started = true;

		WebCamTexture cam = new WebCamTexture ();

		yield return new WaitForSeconds (3f);
		((FirstAccess)GameObject.FindObjectOfType(typeof(FirstAccess))).screenFirst.SetActive(true);
	}

	public void ButtonStart()
	{
		PlayerPrefs.SetInt ("firstStart", 1);
		((FirstAccess)GameObject.FindObjectOfType(typeof(FirstAccess))).screenFirst.SetActive(false);
		((FirstAccess)GameObject.FindObjectOfType(typeof(FirstAccess))).loadScreen.SetActive(true);
		PlayerPrefs.SetInt ("isRestarting", 0);
		PlayerPrefs.Save ();
		Debug.Log ("DO StartMagic Chamado");
		started = true;
		Application.LoadLevelAdditiveAsync (LevelName);
//		StartCoroutine (StartApp ());
	}


	private IEnumerator StartApp()
	{
		AsyncOperation async = Application.LoadLevelAdditiveAsync(LevelName);
		yield return async;
		((FirstAccess)GameObject.FindObjectOfType(typeof(FirstAccess))).screenFirst.SetActive(false);
	}


	public void ResetPlayerPrefs()
	{
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.Save ();
	}

	public void Stompagic()
	{
        StopMagic();
    }
    public void StopMagic()
    {
        started = false;
        //AppRootEvents.GetInstance ().RaiseStopPresentation ();
        //AppRootEvents.GetInstance ().GetAR ().AddChangeARToggleHandler (onChange);
        //AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOff ();
        Debug.Log("StopMagic Chamado");
        //limpar a GUI para não congelar a imagem
        //WindowControlListScript.Instance.WindowInAppListWaiting.Show ();
        Application.LoadLevel(0);
    }

	public void BackScene(string scene)
	{
		started = false;
		//AppRootEvents.GetInstance ().RaiseStopPresentation ();
		//AppRootEvents.GetInstance ().GetAR ().AddChangeARToggleHandler (onChange);
		//AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOff ();
		Debug.Log("StopMagic Chamado");
		//limpar a GUI para não congelar a imagem
		//WindowControlListScript.Instance.WindowInAppListWaiting.Show ();
		Application.LoadLevel(0);
	}

    void onChange(bool b)
    {
        if (!b)
        {
            AppRootEvents.GetInstance().GetAR().RemoveChangeARToggleHandler(onChange);
            Application.LoadLevel(0);
        }

    }
}
