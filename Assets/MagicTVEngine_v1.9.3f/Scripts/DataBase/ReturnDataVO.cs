﻿using UnityEngine;
using System.Collections;

public class ReturnDataVO
{

		public bool success ;
		public DataTable result ;
		public string message ;
		public bool hasResult ;

		public override string ToString ()
		{
			return "[ResultRevisionVO] " + LitJson.JsonMapper.ToJson (this);
		}
}
