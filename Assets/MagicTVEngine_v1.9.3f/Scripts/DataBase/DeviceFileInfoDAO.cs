using UnityEngine;
using System.Collections;
using System;

using MagicTV.abstracts;
using MagicTV.vo;
using MagicTV.vo.result;

using System.Reflection;
using System.Text;

using ARM.abstracts;
using System.Collections.Generic;
using System.Linq;
using ARM.abstracts.components;
using MagicTV.globals;
using MagicTV.globals.events;
using ARM.utils.io;

public class DeviceFileInfoDAO : GeneralComponentsAbstract
{
    private SqliteDatabase SQLiteDB;

    protected delegate void OnEvent();

    private OnEvent _onClick;
    public int ativo = 0;
    public bool showDebug = false;

    public override void prepare()
    {
		this.RaiseComponentIsReady ();
        //				UpdateDataBase ();
    }

    void Update()
    {
        if (!isComponentIsReady() && SQLiteDB != null)
        {
            SQLiteDB.Open();
            if (SQLiteDB.isReady)
            {
                //Debug.LogError ("DeviceInfoDAO . _isComponentReady ");
                _isComponentReady = true;
                RaiseComponentIsReady();
            }
        }
    }

    public void UpdateDataBase()
    {
        //Debug.Log ("DeviceFileInfoDAO . Debug On ");
        SQLiteDB.MigrateDataBaseToPersistentData("magic_tv.db");
        string query;
        query = "DELETE FROM keyvalue ;";
        SQLiteDB.ExecuteQuery(query);
        ResetdataBase();
    }

    public void ResetdataBase()
    {
        string query;
        query = "DELETE FROM revision_result ;";
        SQLiteDB.ExecuteQuery(query);
        query = "DELETE FROM bundle ;";
        SQLiteDB.ExecuteQuery(query);
        query = "DELETE FROM bundle_file ;";
        SQLiteDB.ExecuteQuery(query);
        query = "DELETE FROM app_metadata ;";
        SQLiteDB.ExecuteQuery(query);
        query = "DELETE FROM bundle_metadata ;";
        SQLiteDB.ExecuteQuery(query);
        query = "DELETE FROM bundle_file_url ;";
        SQLiteDB.ExecuteQuery(query);
    }


    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/
    /* ----------------------------------- Select --------------------------------------*/


    public ResultRevisionVO SelectCurrentResultRevisionVO()
    {
        ResultRevisionVO resultRevisionVO = new ResultRevisionVO();
        ReturnDataVO _ReturnDataVO = SQLiteDB.ExecuteQuery("Select * FROM revision_result ORDER BY revision DESC LIMIT 1");
        //				//Debug.LogError ("[DAO] DeviceFileInfoDAO . SelectCurrentResultRevisionVO ~ " + _ReturnDataVO.result.Rows.Count);
        if (_ReturnDataVO.result.Rows.Count > 0)
        {
            resultRevisionVO.revision = _ReturnDataVO.result.Rows[0]["revision"].ToString();
            resultRevisionVO.name = _ReturnDataVO.result.Rows[0]["name"].ToString();
            resultRevisionVO.long_description = _ReturnDataVO.result.Rows[0]["long_description"].ToString();
            resultRevisionVO.short_description = _ReturnDataVO.result.Rows[0]["short_description"].ToString();
            resultRevisionVO.bundles = SelectBundlesVOByRevision(Convert.ToInt32(resultRevisionVO.revision));
            resultRevisionVO.metadata = SelectAppMetadataVOByRevisionId(Convert.ToInt32(resultRevisionVO.revision));
        }
        return resultRevisionVO;
    }

    public ResultRevisionVO SelectResultRevisionVOByRevision(int revision)
    {
        ResultRevisionVO resultRevisionVO = new ResultRevisionVO();
        ReturnDataVO _ReturnDataVO = SelectResultRevision(new string[] { "revision" }, new string[] { "=" }, new string[] { revision.ToString() });
        if (_ReturnDataVO.result.Rows.Count > 0)
        {
            resultRevisionVO.revision = _ReturnDataVO.result.Rows[0]["revision"].ToString();
            resultRevisionVO.name = _ReturnDataVO.result.Rows[0]["name"].ToString();
            resultRevisionVO.long_description = _ReturnDataVO.result.Rows[0]["long_description"].ToString();
            resultRevisionVO.short_description = _ReturnDataVO.result.Rows[0]["short_description"].ToString();
            resultRevisionVO.bundles = SelectBundlesVOByRevision(Convert.ToInt32(resultRevisionVO.revision));
            resultRevisionVO.metadata = SelectAppMetadataVOByRevisionId(Convert.ToInt32(resultRevisionVO.revision));
        }
        //				//Debug.Log ("D SelectCurrentResultRevisionVO . commit " + resultRevisionVO);
        return resultRevisionVO;
    }

    public BundleVO[] SelectBundlesVOByRevision(int revision)
    {
        ReturnDataVO _ReturnDataVO = SelectBundle(new string[] { "revision_id" }, new string[] { "=" }, new string[] { revision.ToString() });
        DataTable resposta = _ReturnDataVO.result;
        BundleVO[] _BundlesVO = new BundleVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            BundleVO bundle_vo = new BundleVO();
            bundle_vo.id = resposta.Rows[i]["id"].ToString();
            bundle_vo.name = resposta.Rows[i]["name"].ToString();
            bundle_vo.context = resposta.Rows[i]["context"].ToString();
            //formato data já na vo
            if (resposta.Rows[i]["published_at"] != null)
            {
                string published_at = resposta.Rows[i]["published_at"].ToString();
                if (published_at != "")
                {
                    try {
                        bundle_vo.published_at = DateTime.Parse(published_at);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Parse de DateTime Published AT: "+ published_at);
                    }
                }
            }
            if (resposta.Rows[i]["expired_at"] != null)
            {
                string expired_at = resposta.Rows[i]["expired_at"].ToString();
                if (expired_at != "")
                {
                    try
                    {
                        bundle_vo.expired_at = DateTime.Parse(expired_at);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Parse de DateTime Expired AT: " + expired_at);
                    }
                }
            }
            bundle_vo.revision_id = resposta.Rows[i]["revision_id"].ToString();
            bundle_vo.files = SelectFilesVOByBundleId(Convert.ToInt32(bundle_vo.id));
            bundle_vo.metadata = SelectBundleMetadataVOByBundleId(Convert.ToInt32(bundle_vo.id));
            _BundlesVO[i] = bundle_vo;
        }
        return _BundlesVO;
    }

    public FileVO[] SelectFilesVOByBundleId(int bundle_id)
    {
        ReturnDataVO _ReturnDataVO = SelectFile(new string[] { "bundle_id" }, new string[] { "=" }, new string[] { bundle_id.ToString() });
        DataTable resposta = _ReturnDataVO.result;
        FileVO[] _FileVO = new FileVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            FileVO file_vo = new FileVO();
            file_vo.id = resposta.Rows[i]["id"].ToString();
            file_vo.bundle_id = resposta.Rows[i]["bundle_id"].ToString();
            file_vo.type = resposta.Rows[i]["type"].ToString();
            file_vo.urls = SelectUrlInfoVOByFilesId(Convert.ToInt32(file_vo.id));
            _FileVO[i] = file_vo;
        }
        return _FileVO;
    }

    public UrlInfoVO[] SelectUrlInfoVOByFilesId(int file_id)
    {
        ReturnDataVO _ReturnDataVO = SelectUrlInfo(new string[] { "bundle_file_id", "active" }, new string[] { "=", "=" }, new string[] {
                        file_id.ToString (),
                        "1"
                });
        DataTable resposta = _ReturnDataVO.result;
        UrlInfoVO[] _UrlInfoVO = new UrlInfoVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            UrlInfoVO url_info_vo = new UrlInfoVO();
            url_info_vo.id = resposta.Rows[i]["id"].ToString();
            url_info_vo.bundle_file_id = resposta.Rows[i]["bundle_file_id"].ToString();
            url_info_vo.type = resposta.Rows[i]["type"].ToString();
            url_info_vo.md5 = resposta.Rows[i]["md5"].ToString();
            url_info_vo.url = resposta.Rows[i]["url"].ToString();
            url_info_vo.size = Convert.ToInt32(resposta.Rows[i]["size"].ToString());
            url_info_vo.local_url = resposta.Rows[i]["local_url"].ToString();
            url_info_vo.status = Convert.ToInt32(resposta.Rows[i]["status"].ToString());
            _UrlInfoVO[i] = url_info_vo;
        }
        return _UrlInfoVO;
    }

    public BundleVO SelectBundlesVOById(int id)
    {
        ReturnDataVO _ReturnDataVO = SelectBundle(new string[] { "id" }, new string[] { "=" }, new string[] { id.ToString() });
        DataTable resposta = _ReturnDataVO.result;
        BundleVO bundle_vo = new BundleVO();
        if (_ReturnDataVO.hasResult)
        {
            bundle_vo.id = resposta.Rows[0]["id"].ToString();
            bundle_vo.name = resposta.Rows[0]["name"].ToString();
            bundle_vo.context = resposta.Rows[0]["context"].ToString();
            if (resposta.Rows[0]["published_at"] != null)
            {
                string published_at = resposta.Rows[0]["published_at"].ToString();
                if (published_at != "")
                {
                    bundle_vo.published_at = DateTime.Parse(published_at);
                    ;
                }
            }
            if (resposta.Rows[0]["expired_at"] != null)
            {
                string expired_at = resposta.Rows[0]["expired_at"].ToString();
                if (expired_at != "")
                {
                    bundle_vo.expired_at = DateTime.Parse(expired_at);
                    ;
                }
            }
            bundle_vo.revision_id = resposta.Rows[0]["revision_id"].ToString();
            bundle_vo.files = SelectFilesVOByBundleId(Convert.ToInt32(bundle_vo.id));
            bundle_vo.metadata = SelectBundleMetadataVOByBundleId(Convert.ToInt32(bundle_vo.id));
        }
        return bundle_vo;
    }

    public FileVO SelectFilesVOById(int id)
    {
        ReturnDataVO _ReturnDataVO = SelectFile(new string[] { "id" }, new string[] { "=" }, new string[] { id.ToString() });
        DataTable resposta = _ReturnDataVO.result;

        if (_ReturnDataVO.hasResult)
        {
            FileVO file_vo = new FileVO();
            file_vo.id = resposta.Rows[0]["id"].ToString();
            file_vo.type = resposta.Rows[0]["type"].ToString();
            file_vo.bundle_id = resposta.Rows[0]["bundle_id"].ToString();
            file_vo.urls = SelectUrlInfoVOByFilesId(Convert.ToInt32(file_vo.id));
            return file_vo;
        }
        return null;
    }

    public MetadataVO[] SelectBundleMetadataVOByBundleId(int id)
    {
        return SelectMetadataVOByTableAndTableId("bundle_metadata", id);
    }

    public MetadataVO[] SelectAppMetadataVOByRevisionId(int id)
    {
        return SelectMetadataVOByTableAndTableId("app_metadata", id);
    }

    public MetadataVO[] SelectMetadataVOByTableAndTableId(string tabela, int id)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        if (tabela == "bundle_metadata")
        {
            _ReturnDataVO = SelectBundleMetadata(new string[] { "tabela", "tabela_id" }, new string[] { "=", "=" }, new string[] {
                        tabela,
                        id.ToString ()
                    });
        }
        if (tabela == "app_metadata")
        {
            _ReturnDataVO = SelectAppMetadata(new string[] { "tabela", "tabela_id" }, new string[] { "=", "=" }, new string[] {
                        tabela,
                        id.ToString ()
                    });
        }
        DataTable resposta = _ReturnDataVO.result;
        int count = 0;
        if (resposta.Rows != null)
        {
            count = resposta.Rows.Count;
        }
        MetadataVO[] _MetadataVO = new MetadataVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            MetadataVO metadata = new MetadataVO();
            metadata.id = resposta.Rows[i]["id"].ToString();
            metadata.key = resposta.Rows[i]["key"].ToString();
            metadata.value = resposta.Rows[i]["value"].ToString();
            metadata.tabela = resposta.Rows[i]["tabela"].ToString();
            metadata.tabela_id = resposta.Rows[i]["tabela_id"].ToString();
            _MetadataVO[i] = metadata;
        }
        return _MetadataVO;
    }

    public MetadataVO SelectAppMetadataVOByMetadataVO(MetadataVO obj)
    {
        ReturnDataVO _ReturnDataVO = SelectAppMetadata(new string[] { "id", "tabela_id", "tabela" }, new string[] {
                        "=",
                        "=",
                        "="
                }, new string[] {
                        obj.id.ToString (),
                        obj.tabela_id.ToString (),
                        obj.tabela.ToString ()
                });
        DataTable resposta = _ReturnDataVO.result;

        if (_ReturnDataVO.hasResult)
        {
            MetadataVO metadata = new MetadataVO();
            metadata.id = resposta.Rows[0]["id"].ToString();
            metadata.key = resposta.Rows[0]["key"].ToString();
            metadata.value = resposta.Rows[0]["value"].ToString();
            metadata.tabela = resposta.Rows[0]["tabela"].ToString();
            metadata.tabela_id = resposta.Rows[0]["tabela_id"].ToString();
            return metadata;
        }
        return null;
    }

    public MetadataVO SelectBundleMetadataVOByMetadataVO(MetadataVO obj)
    {
        ReturnDataVO _ReturnDataVO = SelectBundleMetadata(new string[] { "id", "tabela_id", "tabela" }, new string[] {
                        "=",
                        "=",
                        "="
                }, new string[] {
                obj.id.ToString (),
                obj.tabela_id.ToString (),
                obj.tabela.ToString ()
            });
        DataTable resposta = _ReturnDataVO.result;

        if (_ReturnDataVO.hasResult)
        {
            MetadataVO metadata = new MetadataVO();
            metadata.id = resposta.Rows[0]["id"].ToString();
            metadata.key = resposta.Rows[0]["key"].ToString();
            metadata.value = resposta.Rows[0]["value"].ToString();
            metadata.tabela = resposta.Rows[0]["tabela"].ToString();
            metadata.tabela_id = resposta.Rows[0]["tabela_id"].ToString();
            return metadata;
        }
        return null;
    }


    public MetadataVO SelectBundleMetadataVOById(int id)
    {
        ReturnDataVO _ReturnDataVO = SelectBundleMetadata(new string[] { "id" }, new string[] { "=" }, new string[] { id.ToString() });
        DataTable resposta = _ReturnDataVO.result;
        MetadataVO metadata = new MetadataVO();
        if (_ReturnDataVO.hasResult)
        {
            metadata.id = resposta.Rows[0]["id"].ToString();
            metadata.key = resposta.Rows[0]["key"].ToString();
            metadata.value = resposta.Rows[0]["value"].ToString();
            metadata.tabela = resposta.Rows[0]["tabela"].ToString();
            metadata.tabela_id = resposta.Rows[0]["tabela_id"].ToString();
        }
        return metadata;
    }

    public MetadataVO SelectAppMetadataVOById(int id)
    {
        ReturnDataVO _ReturnDataVO = SelectAppMetadata(new string[] { "id" }, new string[] { "=" }, new string[] { id.ToString() });
        DataTable resposta = _ReturnDataVO.result;
        MetadataVO metadata = new MetadataVO();
        if (_ReturnDataVO.hasResult)
        {
            metadata.id = resposta.Rows[0]["id"].ToString();
            metadata.key = resposta.Rows[0]["key"].ToString();
            metadata.value = resposta.Rows[0]["value"].ToString();
            metadata.tabela = resposta.Rows[0]["tabela"].ToString();
            metadata.tabela_id = resposta.Rows[0]["tabela_id"].ToString();
        }
        return metadata;
    }

    public UrlInfoVO SelectUrlInfoVOById(int id)
    {
        ReturnDataVO _ReturnDataVO = SelectUrlInfo(new string[] { "id" }, new string[] { "=" }, new string[] { id.ToString() });
        DataTable resposta = _ReturnDataVO.result;

        if (_ReturnDataVO.hasResult)
        {
            UrlInfoVO url_info_vo = new UrlInfoVO();
            url_info_vo.id = resposta.Rows[0]["id"].ToString();
            url_info_vo.bundle_file_id = resposta.Rows[0]["bundle_file_id"].ToString();
            url_info_vo.type = resposta.Rows[0]["type"].ToString();
            url_info_vo.url = resposta.Rows[0]["url"].ToString();
            url_info_vo.md5 = resposta.Rows[0]["md5"].ToString();
            url_info_vo.size = Convert.ToInt32(resposta.Rows[0]["size"].ToString());
            url_info_vo.local_url = resposta.Rows[0]["local_url"].ToString();
            url_info_vo.status = Convert.ToInt32(resposta.Rows[0]["status"].ToString());
            return url_info_vo;
        }
        return null;
    }


    public UrlInfoVO[] SelectDeactiveUrlInfoVO()
    {
        ReturnDataVO _ReturnDataVO = SelectUrlInfo(new string[] { "active" }, new string[] { "=" }, new string[] { "0" });
        DataTable resposta = _ReturnDataVO.result;
        UrlInfoVO[] _UrlInfoVO = new UrlInfoVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            UrlInfoVO url_info_vo = new UrlInfoVO();
            url_info_vo.id = resposta.Rows[i]["id"].ToString();
            url_info_vo.bundle_file_id = resposta.Rows[i]["bundle_file_id"].ToString();
            url_info_vo.type = resposta.Rows[i]["type"].ToString();
            url_info_vo.url = resposta.Rows[i]["url"].ToString();
            url_info_vo.md5 = resposta.Rows[i]["md5"].ToString();
            url_info_vo.size = Convert.ToInt32(resposta.Rows[i]["size"].ToString());
            url_info_vo.local_url = resposta.Rows[i]["local_url"].ToString();
            url_info_vo.status = Convert.ToInt32(resposta.Rows[i]["status"].ToString());
            _UrlInfoVO[i] = url_info_vo;
        }
        return _UrlInfoVO;
    }

    public KeyValueVO SelectKeyValueVOById(int id)
    {
        ReturnDataVO _ReturnDataVO = SelectKeyValue(new string[] { "id" }, new string[] { "=" }, new string[] { id.ToString() });
        DataTable resposta = _ReturnDataVO.result;

        if (_ReturnDataVO.hasResult)
        {
            KeyValueVO metadata = new KeyValueVO();
            metadata.id = resposta.Rows[0]["id"].ToString();
            metadata.key = resposta.Rows[0]["key"].ToString();
            metadata.value = resposta.Rows[0]["value"].ToString();
            return metadata;
        }
        return null;
    }

    public KeyValueVO[] SelectKeyValueVOByKey(string key)
    {
        ReturnDataVO _ReturnDataVO = SelectKeyValue(new string[] { "key" }, new string[] { "=" }, new string[] { key.ToString() });
        DataTable resposta = _ReturnDataVO.result;
        KeyValueVO[] _MetadataVO = new KeyValueVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            KeyValueVO metadata = new KeyValueVO();
            metadata.id = resposta.Rows[i]["id"].ToString();
            metadata.key = resposta.Rows[i]["key"].ToString();
            metadata.value = resposta.Rows[i]["value"].ToString();
            _MetadataVO[i] = metadata;
        }
        return _MetadataVO;
    }

    public KeyValueVO[] SelectKeyValueVO(KeyValueVO vo)
    {
        List<string> keys = new List<string>();
        List<string> conditional = new List<string>();
        List<string> values = new List<string>();
        if (!string.IsNullOrEmpty(vo.key) && vo.id != null)
        {
            keys.Add("id");
            conditional.Add("=");
            values.Add(vo.id);
        }
        if (!string.IsNullOrEmpty(vo.key) && vo.key != null)
        {
            keys.Add("key");
            conditional.Add("=");
            values.Add(vo.key);
        }
        if (!string.IsNullOrEmpty(vo.value) && vo.value != null)
        {
            keys.Add("value");
            conditional.Add("=");
            values.Add(vo.value);
        }
        ReturnDataVO _ReturnDataVO = SelectKeyValue(keys.ToArray(), conditional.ToArray(), values.ToArray());
        DataTable resposta = _ReturnDataVO.result;
        KeyValueVO[] _MetadataVO = new KeyValueVO[resposta.Rows.Count];
        for (int i = 0; i < resposta.Rows.Count; i++)
        {
            KeyValueVO metadata = new KeyValueVO();
            metadata.id = resposta.Rows[i]["id"].ToString();
            metadata.key = resposta.Rows[i]["key"].ToString();
            metadata.value = resposta.Rows[i]["value"].ToString();
            _MetadataVO[i] = metadata;
        }
        return _MetadataVO;
    }

    private ReturnDataVO SelectResultRevision(string[] col, string[] operation, string[] values)
    {
        return SelectWhere("revision_result", new string[] { "*" }, col, operation, values);
    }

    private ReturnDataVO SelectBundle(string[] col, string[] operation, string[] values)
    {
        return SelectWhere("bundle", new string[] { "*" }, col, operation, values);
    }

    private ReturnDataVO SelectFile(string[] col, string[] operation, string[] values)
    {
        //				return SelectAll ("bundle_file");
        return SelectWhere("bundle_file", new string[] { "*" }, col, operation, values);
    }

    private ReturnDataVO SelectAppMetadata(string[] col, string[] operation, string[] values)
    {
        return SelectWhere("app_metadata", new string[] { "*" }, col, operation, values);
    }

    private ReturnDataVO SelectBundleMetadata(string[] col, string[] operation, string[] values)
    {
        return SelectWhere("bundle_metadata", new string[] { "*" }, col, operation, values);
    }

    private ReturnDataVO SelectUrlInfo(string[] col, string[] operation, string[] values)
    {
        return SelectWhere("bundle_file_url", new string[] { "*" }, col, operation, values);
    }

    private ReturnDataVO SelectKeyValue(string[] col, string[] operation, string[] values)
    {
        return SelectWhere("keyvalue", new string[] { "*" }, col, operation, values);
    }


    public ReturnDataVO SelectAll(string tableName)
    {

        string query = "SELECT *  FROM " + tableName;
        return SQLiteDB.ExecuteQuery(query);
    }

    public ReturnDataVO SelectWhere(string tableName, string[] items, string[] col, string[] operation, string[] values)
    {
        if (col.Length != operation.Length || operation.Length != values.Length)
        {
            throw new SqliteException("col.Length != operation.Length != values.Length");
        }
        string query = "SELECT " + items[0];
        for (int i = 1; i < items.Length; ++i)
        {
            query += ", " + items[i];
        }
        if (col.Length < 1)
        {
            query += " FROM " + tableName;
        }
        else
        {
            query += " FROM " + tableName + " WHERE " + col[0] + operation[0] + "'" + values[0] + "' ";
            for (int i = 1; i < col.Length; ++i)
            {
                query += " AND " + col[i] + operation[i] + "'" + values[i] + "' ";
            }
        }

        return SQLiteDB.ExecuteQuery(query);
    }


    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/
    /* ----------------------------------- Insert --------------------------------------*/



    private ReturnDataVO InsertResultRevisionVO(ResultRevisionVO revisionResult)
    {
        ResetdataBase();
        string query = "INSERT INTO revision_result (revision, name, short_description, long_description) VALUES ( '" + revisionResult.revision + "', '" + revisionResult.name + "', '" + revisionResult.short_description + "', '" + revisionResult.long_description + "'  ); ";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO InsertBundleVO(BundleVO bundle)
    {
        string plus_coluna = "";
        string plus_valor = "";
        plus_coluna += "published_at, ";
        plus_coluna += "expired_at, ";
        if (bundle.published_at.ToString() != "")
        {
            plus_valor += "'" + bundle.published_at.ToString() + "', ";
        }
        else
        {
            plus_valor += "null, ";
        }
        if (bundle.expired_at.ToString() != "")
        {
            plus_valor += "'" + bundle.expired_at.ToString() + "', ";
        }
        else
        {
            plus_valor += "null, ";
        }
        string query = "INSERT INTO bundle (id, name, context, " + plus_coluna + " revision_id) VALUES ( '" + bundle.id + "', '" + bundle.name + "', '" + bundle.context + "', " + plus_valor + "'" + bundle.revision_id + "'  ); ";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO InsertFileVO(FileVO file)
    {
        string query = "INSERT INTO bundle_file (id, type, bundle_id) VALUES ( '" + file.id + "', '" + file.type + "', '" + file.bundle_id + "'  ); ";
        ReturnDataVO retorno = SQLiteDB.ExecuteQuery(query);
        return retorno;
    }

    private ReturnDataVO InsertBundleMetadataVO(MetadataVO metadata)
    {
        string query = "INSERT INTO bundle_metadata (id, key, value, tabela, tabela_id) VALUES ( '" + metadata.id + "', '" + metadata.key + "', '" + metadata.value + "', '" + metadata.tabela + "', '" + metadata.tabela_id + "'  ); ";
        //				Debug.LogError( query );
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO InsertAppMetadataVO(MetadataVO metadata)
    {
        string query = "INSERT INTO app_metadata (id, key, value, tabela, tabela_id) VALUES ( '" + metadata.id + "', '" + metadata.key + "', '" + metadata.value + "', '" + metadata.tabela + "', '" + metadata.tabela_id + "'  ); ";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO InsertUrlInfoVO(UrlInfoVO urlInfo)
    {
        string query = "INSERT INTO bundle_file_url (id, active, status, type, size, url, local_url, bundle_file_id, md5) VALUES ( '" + urlInfo.id + "', 1, '" + urlInfo.status + "', '" + urlInfo.type + "', '" + urlInfo.size + "', '" + urlInfo.url + "', '" + urlInfo.local_url + "', '" + urlInfo.bundle_file_id + "', '" + urlInfo.md5 + "'  ); ";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO InsertKeyValueVO(KeyValueVO keyvalue)
    {
        string query = "INSERT INTO keyvalue (key, value) VALUES ( '" + keyvalue.key + "', '" + keyvalue.value + "' ) ";
        return SQLiteDB.ExecuteQuery(query);
    }

    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/
    /* ----------------------------------- DeleteVO --------------------------------------*/


    public ReturnDataVO DeleteVO(ResultRevisionVO vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        DeleteVO(vo.bundles);
        DeleteAppMetadataVO(vo.metadata);

        string query = "Delete FROM revision_result WHERE revision = " + vo.revision;
        _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(BundleVO vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        DeleteVO(vo.files);
        DeleteBundleMetadataVO(vo.metadata);

        string query = "Delete FROM bundle WHERE id = " + vo.id;
        _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteAppMetadataVO(MetadataVO vo)
    {
        string query = "Delete FROM app_metadata WHERE id = " + vo.id + " AND tabela_id = " + vo.tabela_id + " AND tabela = '" + vo.tabela_id + "'";
        ReturnDataVO _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteBundleMetadataVO(MetadataVO vo)
    {
        string query = "Delete FROM bundle_metadata WHERE id = " + vo.id + " AND tabela_id = " + vo.tabela_id + " AND tabela = '" + vo.tabela_id + "'";
        ReturnDataVO _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(FileVO vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        _ReturnDataVO = DeleteVO(vo.urls);

        string query = "Delete FROM bundle_file WHERE id = " + vo.id;
        _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(UrlInfoVO vo)
    {
        string query = "Delete FROM bundle_file_url WHERE id = " + vo.id;
        ReturnDataVO _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(KeyValueVO vo)
    {
        string query = "Delete FROM keyvalue WHERE id = " + vo.id;
        ReturnDataVO _ReturnDataVO = SQLiteDB.ExecuteQuery(query);
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(BundleVO[] vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        foreach (BundleVO _obj in vo)
        {
            _ReturnDataVO = DeleteVO(_obj);
        }
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteAppMetadataVO(MetadataVO[] vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        foreach (MetadataVO _obj in vo)
        {
            _ReturnDataVO = DeleteAppMetadataVO(_obj);
        }
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteBundleMetadataVO(MetadataVO[] vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        foreach (MetadataVO _obj in vo)
        {
            _ReturnDataVO = DeleteBundleMetadataVO(_obj);
        }
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(FileVO[] vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        foreach (FileVO _obj in vo)
        {
            _ReturnDataVO = DeleteVO(_obj);
        }
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteVO(UrlInfoVO[] vo)
    {
        ReturnDataVO _ReturnDataVO = new ReturnDataVO();
        foreach (UrlInfoVO _obj in vo)
        {
            _ReturnDataVO = DeleteVO(_obj);
        }
        return _ReturnDataVO;
    }

    public ReturnDataVO DeleteWhereIdIsDifferent(string tableName, string[] values)
    {
        string query = "Delete ";
        if (values.Length < 1)
        {
            query += " FROM " + tableName;
        }
        else
        {
            query += " FROM " + tableName + " WHERE " + " id != '" + values[0] + "' ";
            for (int i = 1; i < values.Length; ++i)
            {
                query += " AND " + " id != '" + values[i] + "' ";
            }
        }
        return SQLiteDB.ExecuteQuery(query);
    }

    public ReturnDataVO DeleteWhereAppMetadataIdIsDifferent(string table, string[] values)
    {
        string query = "Delete ";
        if (values == null || values.Length < 1)
        {
            query += " FROM app_metadata WHERE tabela = '" + table + "' ";
        }
        else
        {
            query += " FROM app_metadata WHERE tabela = '" + table + "' AND " + " id != '" + values[0] + "' ";
            for (int i = 1; i < values.Length; ++i)
            {
                query += " AND " + " id != '" + values[i] + "' ";
            }
        }
        return SQLiteDB.ExecuteQuery(query);
    }

    public ReturnDataVO DeleteWhereBundleMetadataIdIsDifferent(string table, string[] values)
    {
        string query = "Delete ";
        if (values == null || values.Length < 1)
        {
            query += " FROM bundle_metadata WHERE tabela = '" + table + "' ";
        }
        else
        {
            query += " FROM bundle_metadata WHERE tabela = '" + table + "' AND " + " id != '" + values[0] + "' ";
            for (int i = 1; i < values.Length; ++i)
            {
                query += " AND " + " id != '" + values[i] + "' ";
            }
        }
        return SQLiteDB.ExecuteQuery(query);
    }

    public ReturnDataVO DeactiveFileUrlWhereIdIsDifferent(string tableName, string[] values)
    {

        string query = "Update " + tableName + " SET active = 0";
        if (values.Length > 0)
        {
            query += " WHERE " + " id != '" + values[0] + "' ";
            for (int i = 1; i < values.Length; ++i)
            {
                query += " AND " + " id != '" + values[i] + "' ";
            }
        }

        return SQLiteDB.ExecuteQuery(query);
    }

    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/
    /* ----------------------------------- Update --------------------------------------*/



    private ReturnDataVO UpdateResultRevisionVO(ResultRevisionVO revisionResult)
    {
        string query = "UPDATE revision_result SET revision = '" + revisionResult.revision + "', name = '" + revisionResult.name + "', short_description = '" + revisionResult.short_description + "', long_description = '" + revisionResult.long_description + "'  WHERE revision = " + revisionResult.revision + "; ";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO UpdateBundleVO(BundleVO bundle)
    {
        string query = "UPDATE bundle SET id = '" + bundle.id + "', name = '" + bundle.name + "', context = '" + bundle.context + "', published_at = '" + bundle.published_at + "', expired_at = '" + bundle.expired_at + "'  WHERE id = " + bundle.id + ";";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO UpdateFileVO(FileVO file)
    {
        string query = "UPDATE bundle_file SET id = '" + file.id + "', type = '" + file.type + "' WHERE id = " + file.id + ";";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO UpdateAppMetadataVO(MetadataVO metadata)
    {
        string query = "UPDATE app_metadata SET tabela = '" + metadata.tabela + "', tabela_id = '" + metadata.tabela_id + "', id = '" + metadata.id + "', key = '" + metadata.key + "', value = '" + metadata.value + "' WHERE id = " + metadata.id + " AND tabela_id = " + metadata.tabela_id + " AND tabela = '" + metadata.tabela + "' ;";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO UpdateBundleMetadataVO(MetadataVO metadata)
    {
        string query = "UPDATE bundle_metadata SET tabela = '" + metadata.tabela + "', tabela_id = '" + metadata.tabela_id + "', id = '" + metadata.id + "', key = '" + metadata.key + "', value = '" + metadata.value + "' WHERE id = " + metadata.id + " AND tabela_id = " + metadata.tabela_id + " AND tabela = '" + metadata.tabela + "' ;";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO UpdateUrlInfoVO(UrlInfoVO urlInfo)
    {
        string query_url_local = "";
        if (urlInfo.local_url != null)
        {
            query_url_local = ", local_url = '" + urlInfo.local_url + "', status = '" + urlInfo.status + "'";
        }
        string query = "UPDATE bundle_file_url SET active = 1, type = '" + urlInfo.type + "', size = '" + urlInfo.size + "', url = '" + urlInfo.url + "'" + query_url_local + " WHERE id = " + urlInfo.id + "; ";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO DeactiveAllUrls()
    {
        string query = "UPDATE bundle_file_url SET active = 0";
        return SQLiteDB.ExecuteQuery(query);
    }

    private ReturnDataVO UpdateKeyValueVO(KeyValueVO metadata)
    {
        string query = "UPDATE keyvalue SET id = '" + metadata.id + "', key = '" + metadata.key + "', value = '" + metadata.value + "';";
        return SQLiteDB.ExecuteQuery(query);
    }

    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/
    /* ----------------------------------- Commit --------------------------------------*/



    public ReturnDataVO CommitResultRevisionVO(ResultRevisionVO _obj)
    {
        ResultRevisionVO resultRevisionVO = SelectResultRevisionVOByRevision(Convert.ToInt32(_obj.revision));
        if (resultRevisionVO.revision == "" || Convert.ToInt32(resultRevisionVO.revision) < 1)
        {
            //						//Debug.LogError( "Cadastra Revision" + _obj.ToString());
            ReturnDataVO _ReturnDataVO = InsertResultRevisionVO(_obj);
            return _ReturnDataVO;
        }

        return UpdateResultRevisionVO(_obj);
    }

    public ReturnDataVO CommitBandleVO(BundleVO _obj)
    {
        BundleVO bundleVO = SelectBundlesVOById(Convert.ToInt32(_obj.id));
        ////Debug.LogWarning ("DeviceFileInfo . CommitBandleVO " + bundleVO.id);
        if (bundleVO.id == "" || Convert.ToInt32(bundleVO.id) < 1)
        {
            return InsertBundleVO(_obj);
        }

        return UpdateBundleVO(_obj);
    }

    public ReturnDataVO CommitFileVO(FileVO _obj)
    {
        FileVO fileVO = SelectFilesVOById(Convert.ToInt32(_obj.id));
        //				//Debug.LogError ("[DAO]  CommitFileVO . recebi:" + _obj.ToString () + " | no banco veio:" + fileVO);
        if (fileVO == null)
        {
            return InsertFileVO(_obj);
        }
        return UpdateFileVO(_obj);

    }

    public ReturnDataVO CommitBundleMetadataVO(MetadataVO _obj)
    {
        MetadataVO _MetadataVO = SelectBundleMetadataVOByMetadataVO(_obj);
        if (_MetadataVO == null)
        {
            return InsertBundleMetadataVO(_obj);
        }

        return UpdateBundleMetadataVO(_obj);
    }

    public ReturnDataVO CommitAppMetadataVO(MetadataVO _obj)
    {
        MetadataVO _MetadataVO = SelectAppMetadataVOByMetadataVO(_obj);
        if (_MetadataVO == null)
        {
            return InsertAppMetadataVO(_obj);
        }

        return UpdateAppMetadataVO(_obj);
    }

    public ReturnDataVO CommitUrlInfoVO(UrlInfoVO _obj)
    {
        UrlInfoVO urlInfoVO = SelectUrlInfoVOById(Convert.ToInt32(_obj.id));
        //				Debug.LogError ("[DAO]  CommitUrlInfoVO . recebi : " + _obj.ToString () + " | no banco veio:" + urlInfoVO);
        if (urlInfoVO == null)
        {
            return InsertUrlInfoVO(_obj);
        }
        //deletar o arquivo físico
        if (urlInfoVO.status == UrlInfoConstants.STATUS_FILE_CACHED && urlInfoVO.md5 != _obj.md5)
        {
            // Debug.LogError("!! DELETANDO "+Application.persistentDataPath + urlInfoVO.local_url);
            ARMFileManager.DeleteFile(Application.persistentDataPath + urlInfoVO.local_url);
            urlInfoVO.status = UrlInfoConstants.STATUS_FILE_WAITING;
            urlInfoVO.url = _obj.url;

            urlInfoVO.local_url = "";
            return UpdateUrlInfoVO(urlInfoVO);
        }
        return UpdateUrlInfoVO(_obj);
    }

    public ReturnDataVO CommiteKeyValueVO(KeyValueVO _obj)
    {
        KeyValueVO _KeyValueVO = SelectKeyValueVOById(Convert.ToInt32(_obj.id));
        if (_KeyValueVO == null)
        {
            return InsertKeyValueVO(_obj);
        }
        return UpdateKeyValueVO(_obj);
    }

    void UpdateAllUrlsToDelete()
    {
        DeactiveAllUrls();
    }

    public ReturnDataVO Commit(ResultRevisionVO _revisionResult)
    {
        //				//Debug.LogError ("Cadastra Revision" + _revisionResult.bundles.ToString () + " | total:" + _revisionResult.bundles.Length);
        if (_revisionResult == null || _revisionResult.revision == null)
        {
            UpdateAllUrlsToDelete();
            return new ReturnDataVO() { success = true };
        }
        //				//Debug.LogError ("Commit Revision");
        string[] ids_bundle = null;
        string[] ids_meta = null;
        string[] ids_files = null;
        string[] ids_urls = null;
        string[] ids_bundles_meta = null;

        ReturnDataVO _ReturnDataVO = CommitResultRevisionVO(_revisionResult);
        if (!_ReturnDataVO.success)
        {
            //						//Debug.LogError ("[ERRROLOOOO] Commit Revision message" + _ReturnDataVO.message);
            return _ReturnDataVO;
        }
        ids_bundle = _revisionResult.bundles.Select(b => b.id).ToArray();
        ids_meta = _revisionResult.metadata.Select(b => b.id).ToArray();
        ids_files = _revisionResult.bundles.SelectMany(b => b.files.Select(f => f.id)).ToArray();
        ids_urls = _revisionResult.bundles.SelectMany(b => b.files.SelectMany(f => f.urls.Select(u => u.id))).ToArray();
        ids_bundles_meta = _revisionResult.bundles.SelectMany(b => b.metadata.Select(m => m.id)).ToArray();

        //				ids_files.ToList().ForEach(l => //Debug.LogError("bundles - "+l)); 
        //				ids_files.ToList().ForEach(l => //Debug.Log("files - "+l)); 
        //				ids_urls.ToList().ForEach(l => //Debug.Log("urls - "+l)); 
        //				ids_meta.ToList().ForEach(l => //Debug.Log("metas - "+l)); 

        foreach (BundleVO _bundle in _revisionResult.bundles)
        {
            _bundle.revision_id = _revisionResult.revision;
            _ReturnDataVO = CommitBandleVO(_bundle);
            if (!_ReturnDataVO.success)
            {
                return _ReturnDataVO;
            }
            foreach (FileVO _file in _bundle.files)
            {
                _file.bundle_id = _bundle.id;
                _ReturnDataVO = CommitFileVO(_file);
                if (!_ReturnDataVO.success)
                {
                    return _ReturnDataVO;
                }
                foreach (UrlInfoVO _url_info in _file.urls)
                {
                    _url_info.bundle_file_id = _file.id;
                    _ReturnDataVO = CommitUrlInfoVO(_url_info);
                    if (!_ReturnDataVO.success)
                    {
                        return _ReturnDataVO;
                    }
                }
            }

            foreach (MetadataVO _metadata in _bundle.metadata)
            {
                _metadata.tabela = "bundle_metadata";
                _metadata.tabela_id = _bundle.id;
                _metadata.id = _metadata.id;
                _ReturnDataVO = CommitBundleMetadataVO(_metadata);
                if (!_ReturnDataVO.success)
                {
                    return _ReturnDataVO;
                }
            }
        }

        foreach (MetadataVO _metadata in _revisionResult.metadata)
        {
            _metadata.tabela = "app_metadata";
            _metadata.tabela_id = _revisionResult.revision;
            _metadata.id = _metadata.id;
            _ReturnDataVO = CommitAppMetadataVO(_metadata);
            if (!_ReturnDataVO.success)
            {
                return _ReturnDataVO;
            }
        }


        //mudei pra cima antes de adicionar
        DeleteWhereIdIsDifferent("bundle", ids_bundle);
        DeactiveFileUrlWhereIdIsDifferent("bundle_file_url", ids_urls);
        DeleteWhereAppMetadataIdIsDifferent("app_metadata", ids_meta);
        DeleteWhereBundleMetadataIdIsDifferent("bundle_metadata", ids_bundles_meta);
        DeleteWhereIdIsDifferent("bundle_file", ids_files);
        //				//Debug.LogError ("[!!!] Cadastra Revision   " + _revisionResult.bundles [0].context);

        return _ReturnDataVO;
    }
}
