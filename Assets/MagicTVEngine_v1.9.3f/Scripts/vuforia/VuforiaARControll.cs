using UnityEngine;
using System.Collections;
using  MagicTV.globals.events;
using Vuforia;

public class VuforiaARControll : ARControllInterface
{

	protected static VuforiaARControll _instance;

	public static VuforiaARControll Instance {
		get {
			if (_instance == null) {
				_instance = new VuforiaARControll ();
			}
			return _instance;
		}
		set {
			_instance = value;
		}
	}

	protected ObjectTracker _vuforiaImageTracker;

	protected ObjectTracker VuforiaImageTracker {
		get {
			if (_vuforiaImageTracker == null) {
				_vuforiaImageTracker = TrackerManager.Instance.GetTracker<ObjectTracker> ();
			}
			return _vuforiaImageTracker;
		}
	}

	bool _chekStartVuforia;

	/// <summary>
	/// Starts the vuforiaStage.
	/// </summary>
	public void Enable ()
	{

//				Debug.LogWarning (" InAppManager . startVuforia ");
		// if (this.camera != null) {
		// 		this.camera.SetActive (true);
		// }
		
		// não  podemos mais desligar A Camera, entaão vamos só desligar os trackings ;)
		_chekStartVuforia = true;
		if (!VuforiaImageTracker.IsActive) {
			VuforiaImageTracker.Start (); 
		}
		AppRootEvents.GetInstance ().GetAR ().RaiseARToggle (true);

	}

	/// <summary>
	/// Destroies the gameobject vuforiaStage.
	/// </summary>
	public void Disable ()
	{
		// TODO: destruir o gameobject VuforiaStage
		_chekStartVuforia = false;
		if (VuforiaImageTracker != null) {
			VuforiaImageTracker.Stop ();
		}
		AppRootEvents.GetInstance ().GetAR ().RaiseARToggle (false);
	}
}
