﻿using UnityEngine;
using System.Collections;

public class DebugMedianVector3 : MonoBehaviour {

	public Vector4 vec1 = Vector3.zero ;
	
	public Vector4 vec2 = Vector3.zero ;

	public Vector4 vec3 = Vector3.zero ;

	public Vector4 vec4 = Vector3.zero ;
	
	public Vector4 median = Vector3.zero ;

	public Vector4 mean = Vector3.zero ;

	protected ARM.utils.PoolQuaternion pool ;

	bool _loaded = false ;
	// Use this for initialization
	void Start () {
		this.pool = new ARM.utils.PoolQuaternion ( 4 ) ;
		this._loaded = true;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if( this._loaded ){
			this.pool.reset();
			this.pool.addItem( Quaternion.Euler( vec1 ) );
			this.pool.addItem( Quaternion.Euler( vec2 ) );
			this.pool.addItem( Quaternion.Euler( vec3 ) );
			this.pool.addItem( Quaternion.Euler( vec4 ) );

			this.median = this.pool.getMedian().eulerAngles ;
			
			this.mean = this.pool.getMedian().eulerAngles ;

		}
	}
}
