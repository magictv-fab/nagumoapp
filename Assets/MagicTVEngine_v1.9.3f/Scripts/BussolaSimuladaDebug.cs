﻿using UnityEngine;
using System.Collections;

using ARM.transform.filters;

public class BussolaSimuladaDebug : MonoBehaviour
{

	public BussolaFilter bussola ;
	public bool byPass = false ;

	private Vector2[] sequenciaDeComportamento ;
	public int currentIndex = 0 ;
	public Vector2 currentVector;
	private float _timePassed = 0f ;
	// Use this for initialization
	void Start ()
	{

		//x é o angulo e y é o tempo que ele vai esperar pra mudar de angulo, em segundos
		sequenciaDeComportamento = new Vector2[13]{
			new Vector2{x=0f, y=1f},
			new Vector2{x=1f, y=0.5f},
			new Vector2{x=0f, y=1f},
			new Vector2{x=-1f, y=0.2f},
			new Vector2{x=1f, y=0.5f},
			new Vector2{x=0f, y=0.2f},
			new Vector2{x=1f, y=0.5f},
			new Vector2{x=10f, y=2f},
			new Vector2{x=12f, y=0.5f},
			new Vector2{x=10f, y=1f},
			new Vector2{x=10.5f, y=0.4f},
			new Vector2{x=280f, y=1.3f},
			new Vector2{x=281f, y=1f}
		};
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (this.byPass) {
			return;
		}
		if (this.bussola == null) {
			Debug.LogError ("Simulador de bussola desligando por falta da bussola");
			this.byPass = true;
			return;
		}
		if (this.sequenciaDeComportamento != null && this.sequenciaDeComportamento.Length > 0) {
			if (this.currentIndex >= this.sequenciaDeComportamento.Length) {
				this.currentIndex = 0;
			}
			this.currentVector = this.sequenciaDeComportamento [this.currentIndex];
			this.bussola.busolaDebugAngle = this.currentVector.x;
		}
		this.clock ();
	}
	protected void clock ()
	{
		//this._timePassed += Time.deltaTime ;
		//if( this._timePassed > this.currentVector.y ){
		this.currentIndex ++;
		this._timePassed = 0;
		//}
	}
}
