﻿using UnityEngine;
using System.Collections;

public class PlayMovieTexture : MonoBehaviour
{
    public GameObject playTarget;
    public bool playNow;
    public bool playAudioNow;
    public bool isLoop;
    public float delayAudio = 0f;
    public AudioSource audioSource;
#if (UNITY_EDITOR || UNITY_WEBPLAYER)
    MovieTexture movTex;
#endif
    void Start()
    {
#if (UNITY_EDITOR || UNITY_WEBPLAYER)
        movTex = (MovieTexture)playTarget.GetComponent<Renderer>().material.mainTexture;
#endif
    }
    public void PlayMovieOneShot()
    {
#if (UNITY_EDITOR || UNITY_WEBPLAYER)
        if (movTex == null)
        {
            Debug.LogError("Cade o MovieTexture?");
            return;
        }
        movTex.loop = isLoop;
        movTex.Stop();
#endif
        if (playAudioNow && audioSource != null)
        {
            audioSource.Stop();
            audioSource.time = 0f;
            if (delayAudio < 0)
            {
                audioSource.time = this.delayAudio * -1;
                audioSource.Play();
            }
            else if (delayAudio > 0)
            {
                Invoke("doPlayAudio", this.delayAudio);
            }
            audioSource.Play();
        }
#if (UNITY_EDITOR || UNITY_WEBPLAYER)
        movTex.Play();
#endif
    }
    void doPlayAudio()
    {
        audioSource.Play();
    }
    void Update()
    {
#if (UNITY_EDITOR || UNITY_WEBPLAYER)
        if (playNow && movTex != null)
        {
            playNow = false;
            PlayMovieOneShot();
        }
#endif
    }
}