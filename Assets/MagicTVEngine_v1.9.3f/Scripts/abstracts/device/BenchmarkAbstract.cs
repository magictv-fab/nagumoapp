﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;
using MagicTV.abstracts.device;
using ARM.abstracts.components;
using ARM.animation;


/// <summary>
/// Benchmark abstract.
/// To analyze the graphics capability of the device and return to inform what is the recommended capacity of app assets to download
/// 
/// Cab e ao benchmark perceber que já fez a requisição e sabe qual a qualidade do device atual para 
/// não ser executado novamente caso não precise
/// 
/// @author Renato Miawaki
/// 
/// </summary>
namespace MagicTV.abstracts
{

	public abstract class BenchmarkAbstract : GenericStartableComponentControllAbstract
	{

		public static BenchmarkAbstract Instance ;

		public BenchmarkAbstract ()
		{
			//The last instance of benchmark is the only one
			Instance = this;
		}

		public abstract BenchmarkInfo GetCurrentQuality () ;

		public abstract void retestQuality ();
	}

}