﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MagicTV.vo;

/// <summary>
/// Benchmark info.
///
/// </summary>
using ARM.utils.io;


namespace MagicTV.abstracts.device
{
	
	public class BenchmarkInfo
	{
		
		protected int _quality ;
		
		public int quality {
			get {
				return this._quality;
			}
		}
		
		public BenchmarkInfo (int quality)
		{

			this._quality = quality;
//			Debug.LogWarning ("BenchmarInfo : Quality = " + quality);
		}
		/// <summary>
		/// Filters the files by quality.
		/// Metodo de utilidade para filtrar o que vai ser baixado para o device atual
		/// não retorna os arquivos zip
		/// </summary>
		/// <returns>The files by quality.</returns>
		/// <param name="file">File.</param>
		public UrlInfoVO FilterFilesByQuality (FileVO file)
		{
			if (file.urls == null || file.urls.Length == 0) {
				//Sem urls, sem filtro
				return null;
			}

			if (file.urls.Length == 1) {
				//Se só tem uma (1) url, então nem tem o que discutir, é essa. FATO.
				return filterZip (file.urls [0]);
			}
			UrlInfoVO bestUrl = file.urls [0];

			int bestDistance = Mathf.Abs (UrlInfoConstants.getQualityIntByType (bestUrl.type) - this._quality);

			if (bestDistance == 0 && filterZip (bestUrl) != null) {
				//se não for zip e for a melhor qualidade, retorna ela
				return  bestUrl;
			}
			
			foreach (UrlInfoVO url in file.urls) {

				//a qualidade dessa url, nesse loop, é essa aqui ó
				int quality = UrlInfoConstants.getQualityIntByType (url.type);

				if (quality == this._quality && filterZip (url) != null) {
					//se a qualidade é exatamente essa, pronto, achei o melhor
					return url;
				}

				int distance = Mathf.Abs (UrlInfoConstants.getQualityIntByType (url.type) - this._quality);

				if (distance < bestDistance) {
					bestUrl = url;
				}

				if (distance == bestDistance) {
					if (UrlInfoConstants.getQualityIntByType (bestUrl.type) < quality) {
						bestUrl = url;
					}
				}
			}
			return filterZip (bestUrl);
		}
		protected UrlInfoVO filterZip (UrlInfoVO url)
		{
			string extension = ARMFileManager.GetExtentionOfFile (url.local_url);
			if (extension == ".zip") {
				//ignorando zips - para track não funciona questão de qualidade high low
				return null;	
			}
			return url;
		}
	}
}
