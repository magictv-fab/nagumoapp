﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts ;
using MagicTV.vo ;
using MagicTV.vo.result ;

using ARM.abstracts ;

/// <summary>
/// 
/// Device file info abstract.
/// 
/// Salva o estado dos arquivos no mobile
/// 
/// </summary>
using ARM.abstracts.components;
using System.Collections.Generic;


namespace MagicTV.abstracts.device
{

	public abstract class DeviceFileInfoAbstract : GeneralComponentsAbstract
	{
		/// <summary>
		/// The tracks list string.
		/// Geralmente para debug
		/// </summary>
		public List<string> tracks = new List<string>();

		protected static DeviceFileInfoAbstract instance;
		public static DeviceFileInfoAbstract GetInstance ()
		{
			return instance;
		}
		public DeviceFileInfoAbstract(){
			instance = this;
		}
		public abstract ResultRevisionVO getRevisionByRevisionId (string revision_id) ;

		public abstract ResultRevisionVO getCurrentRevision () ;

		public abstract ReturnDataVO commit ( ResultRevisionVO revisionResult ) ;
		public abstract ReturnDataVO commit ( UrlInfoVO urlInfoVO ) ;

		/// <summary>
		/// Retorna todos os arquivos que ainda não foram baixados para baixar e informações da revisão
		/// </summary>
		/// <returns>The uncached revision.</returns>
		public abstract ResultRevisionVO getUncachedRevision () ;

		public abstract BundleVO[] getBundles (string appType) ;

		public abstract UrlInfoVO[] getUrlInfoVOToDelete ();

		public abstract UrlInfoVO[] getAllUrlInfoVO ();

		public abstract UrlInfoVO[] getCachedUrlInfoVO ();

		public abstract ReturnDataVO delete (UrlInfoVO obj) ;
		
		public abstract ReturnDataVO delete (UrlInfoVO[] objs) ;

		public abstract void populateIndexedBundles ();

		/// <summary>
		/// Persist infos of ResultRevisionVO.
		/// </summary>
		public abstract void Save();
	}

}