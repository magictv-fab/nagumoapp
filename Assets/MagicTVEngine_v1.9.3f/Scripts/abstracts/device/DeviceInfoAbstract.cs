﻿using UnityEngine;
using System.Collections;

using ARM.abstracts.components;
using MagicTV.abstracts ;
using MagicTV.vo ;
using MagicTV.vo.result ;

using ARM.abstracts ;

/// <summary>
/// 
/// Device file info abstract.
/// 
/// Salva o estado dos arquivos no mobile
/// 
/// </summary>
namespace MagicTV.abstracts.device
{

	public abstract class DeviceInfoAbstract : GeneralComponentsAbstract
	{


		public abstract string GetUserAgent ();
		public abstract string getPin ();
		public abstract void setPin (string pin);


		public abstract string getNotificationToken () ;
		public abstract void setNotificationToken (string token) ;
		public abstract bool hasNotificationToken ();
		public abstract int GetCurrentInfoVersionControlId ();
		public abstract void SetCurrentInfoVersionControlId (int version);
		/// <summary>
		/// Saves the current info version.
		/// Dont persist data
		/// </summary>
		public abstract void SaveCurrentInfoVersionControlId ();
	}

}