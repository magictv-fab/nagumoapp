﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts;
using ARM.abstracts.internet ;

using ARM.abstracts ;

using MagicTV.vo ;
using MagicTV.vo.result ;

/// <summary>
/// Download manager abstract.
/// </summary>
namespace MagicTV.abstracts.device{

	public abstract class DeviceCacheFileManagerAbstract : ProgressEventsAbstract {

		public abstract void SetFileList( ResultRevisionVO revisionInfo );

		public abstract void StartCache() ;
		
		public abstract ReturnResultVO Delete( ResultRevisionVO revision );
		public abstract ReturnResultVO Delete( BundleVO bundle );
		public abstract ReturnResultVO Delete( BundleVO[] bundles );
		public abstract ReturnResultVO Delete( FileVO file );
		public abstract ReturnResultVO Delete( FileVO[] files );
		public abstract ReturnResultVO Delete( UrlInfoVO url );
		public abstract ReturnResultVO Delete( UrlInfoVO[] urls );

	}

}