﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts ;
using ARM.abstracts ;
/// <summary>
/// Toda tela deveria implementar ou extender essa classe, ou ter uma classe que controle a tela e extenda essa classe
/// </summary>
using ARM.abstracts.components;


namespace MagicTV.abstracts.screens{
	public abstract class GeneralScreenAbstract : GeneralComponentsAbstract {

		protected bool _isVisible = false ;
		public bool isVisible(){
			return this._isVisible ;
		}

		public abstract void show() ;
		public abstract void hide() ;


	}
}