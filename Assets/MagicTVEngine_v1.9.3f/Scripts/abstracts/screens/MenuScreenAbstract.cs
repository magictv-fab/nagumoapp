﻿using UnityEngine ;
using System.Collections ;

using MagicTV.abstracts ;
using MagicTV.globals ;


/// <summary>
/// Tela para mostrar o processo de carregamento do projeto
/// </summary>
namespace MagicTV.abstracts.screens
{
	public abstract class MenuScreenAbstract : GeneralScreenAbstract
	{

		public delegate void OnVoidEventHandler ();

		public OnVoidEventHandler onPrintScreenCalled;
		/// <summary>
		/// Raises the print screen called.
		/// Ele gostaria de tirar printscreen.
		/// Apertou o botão
		/// </summary>
		protected void RaisePrintScreenCalled ()
		{
			//se disparou o evento, quer tirar o print
			
			if (this.onPrintScreenCalled != null) {
				
				this.onPrintScreenCalled ();
				
			}
		}



		public OnVoidEventHandler onClearCacheCalled;
		
		/// <summary>
		/// Eventos de ClearCache
		/// </summary>
		protected void RaiseClearCacheCalled ()
		{
			//se disparou o evento, é para fazer o clear cache
			
			if (this.onClearCacheCalled != null) {
				
				this.onClearCacheCalled ();
				
			}
		}

		public OnVoidEventHandler onResetCalled;

		/// <summary>
		/// Eventos de Reset
		/// </summary>
		protected void RaiseResetCalled ()
		{
			//se disparou o evento, é para fazer o reset
			
			if (this.onResetCalled != null) {

				this.onResetCalled ();

			}
		}
	}
}