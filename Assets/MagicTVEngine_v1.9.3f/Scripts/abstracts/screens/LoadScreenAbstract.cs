﻿using UnityEngine ;
using System.Collections ;

using MagicTV.abstracts ;
using MagicTV.globals ;


/// <summary>
/// Tela para mostrar o processo de carregamento do projeto
/// </summary>
namespace MagicTV.abstracts.screens
{
	public abstract class LoadScreenAbstract : GeneralScreenAbstract
	{

		public delegate void OnLoadAnswerEventHandler ();
		
		protected OnLoadAnswerEventHandler onLoadCanceled ;
		protected OnLoadAnswerEventHandler onLoadConfirmed ;
		
		public void AddLoadCanceledEventHandler (OnLoadAnswerEventHandler method)
		{
			this.onLoadCanceled += method;
		}
		
		public void RemoveLoadCanceledEventHandler (OnLoadAnswerEventHandler method)
		{
			this.onLoadCanceled -= method;
		}

		protected OnLoadAnswerEventHandler onLoadConfirm ;
		
		public void AddLoadConfirmedEventHandler (OnLoadAnswerEventHandler method)
		{
			this.onLoadConfirmed += method;
		}
		
		public void RemoveLoadConfirmedEventHandler (OnLoadAnswerEventHandler method)
		{
			this.onLoadConfirmed -= method;
		}		

		// Enum que da troca de telas no download
				
		
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseLoadCanceled ()
		{
			//se disparou o evento, é porque está pronto
			
			if (this.onLoadCanceled != null) {

				this.onLoadCanceled ();

			}

		}
		public abstract void HideNow ();
		
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseLoadConfirmed ()
		{

			//se disparou o evento, é porque está pronto
			
			if (this.onLoadConfirmed != null) {

				this.onLoadConfirmed ();

			}
		}

		/// <summary>
		/// Sets the message to show for user
		/// </summary>
		/// <param name="message">Message.</param>
		public abstract void setMessage (string message) ;
		/// <summary>
		/// Sets the percent loaded.
		/// Lembrando que o total é 1 e o mínimo  é zero
		/// </summary>
		/// <param name="total">Total.</param>
		public abstract void setPercentLoaded (float total) ;
	}
}