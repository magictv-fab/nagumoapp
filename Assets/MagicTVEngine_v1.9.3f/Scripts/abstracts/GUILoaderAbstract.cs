﻿using UnityEngine;
using System.Collections;

using ARM.abstracts;

using MagicTV.abstracts.screens;
/// <summary>
/// GUI loader abstract.
/// É a classe que centraliza o carregamento de GUIs (só a estrutura básica) de todas as telas e menus
/// </summary>
using ARM.abstracts.components;


namespace MagicTV.abstracts
{
    public abstract class GUILoaderAbstract : GeneralComponentsAbstract
    {

        public abstract LoadScreenAbstract getLoadScreen();

        public abstract LoadScreenAbstract getInitialSplashScreen();

        public abstract MenuScreenAbstract getMenuScreen();

    }
}