﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts ;
using MagicTV.vo ;
using ARM.abstracts ;

/// <summary>
/// Server communication abstract.
/// Interface* basica para conversa com servidor
/// </summary>
using ARM.abstracts.components;


namespace MagicTV.abstracts.comm
{

	public abstract class ServerCommunicationAbstract : GeneralComponentsAbstract
	{

		public delegate void OnUpdateRequestCompleteEventHandler (RevisionInfoVO revisionInfoVO);

		public delegate void OnBool (bool success);
		
		protected OnUpdateRequestCompleteEventHandler onUpdateRequestComplete ;

		public OnBool onTokenRequestComplete ;
		public void AddUpdateRequestCompleteEventHandler (OnUpdateRequestCompleteEventHandler method)
		{
			this.onUpdateRequestComplete += method;
		}
		
		
		public void RemoveUpdateRequestCompleteEventHandler (OnUpdateRequestCompleteEventHandler method)
		{
			this.onUpdateRequestComplete -= method;
		}
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseUpdateRequestComplete (RevisionInfoVO revisionInfoVO)
		{
			//se disparou o evento, é porque está pronto
			
			if (this.onUpdateRequestComplete != null) {
				this.onUpdateRequestComplete (revisionInfoVO);
			}
		}


		public abstract WWW sendToken (string token);

		public delegate void OnUpdateRequestErrorEventHandler (string descriptionError);
		
		protected OnUpdateRequestErrorEventHandler onUpdateRequestError ;
		
		public void AddUpdateRequestErrorEventHandler (OnUpdateRequestErrorEventHandler method)
		{
			this.onUpdateRequestError += method;
		}
		
		
		public void RemoveUpdateRequestErrorEventHandler (OnUpdateRequestErrorEventHandler method)
		{
			this.onUpdateRequestError -= method;
		}
		/// <summary>
		/// Quando por algum motivo, qualquer, a conexão com o servidor nào acontece
		/// </summary>
		protected void RaiseUpdateRequestError (string descriptionError)
		{
			//se disparou o evento, é porque está pronto
			if (this.onUpdateRequestError != null) {
				this.onUpdateRequestError (descriptionError);
			}
		}

		protected void RaiseUpdateTokenComplete (bool success)
		{
			if (onTokenRequestComplete != null) {
				onTokenRequestComplete (success);
			}
		}

		public abstract void RequestUpdate () ;

	}

}