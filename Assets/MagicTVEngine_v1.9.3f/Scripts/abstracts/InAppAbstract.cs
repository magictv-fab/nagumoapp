﻿using UnityEngine;
using System.Collections;

using ARM.abstracts ;
using ARM.abstracts.components;
using MagicTV.vo;
using ARM.interfaces;
using MagicTV.in_apps;
using ARM.utils.io;
using ARM.utils;
using MagicTV.utils;
using MagicTV.globals;
using System;
using ARM.utils.cron;
using System.Collections.Generic;
using MagicTV.globals.events;

namespace MagicTV.abstracts
{
	/// <summary>
	/// In app abstract.
	/// É qualquer app interno que seja passivel de se apresentar.
	/// e também seu pré loader
	/// Também é de sua total responsabilidade o donwload de seu conteúdo, conforme seu bundle recebido
	/// 
	/// </summary>
	public abstract class InAppAbstract : GeneralComponentsAbstract
	{
		/// <summary>
		/// The lights
		/// </summary>

		private int _delayToRun = 0 ;
		private bool _waitingToRun = false;
		private bool _delayFinished ;
		private int _timerRunningId;
		private bool _delayResetOnStop = true ;
		private int _delayPaused = 0;

		public List<String> DebugMetas = new List<string> ();
		#region quit events		
		protected Action onQuit ;
		
		public void AddQuitEventHandler (Action method)
		{
			this.onQuit += method;
		}

		public void RemoveQuitEventHandler (Action method)
		{
			this.onQuit -= method;
		}
		/// <summary>
		/// Em sua classe, chame esse metodo para disparar o evento
		/// </summary>
		protected void RaiseQuit ()
		{
			//se disparou o evento, é porque está pronto
			if (this.onQuit != null) {
				this.onQuit ();
			}
		}

		#endregion



		/// <summary>
		/// Transforms from BundleVO
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected void transformFromVO (BundleVO bundle)
		{
			string transform_position = utils.BundleHelper.GetMetadataSingleValueByKey ("TransformPosition", bundle.metadata);
			if (transform_position != null) {
				transform.position = getVectorFromMetaString (transform_position);								
			}
			
			string transform_rotation = utils.BundleHelper.GetMetadataSingleValueByKey ("TransformRotation", bundle.metadata);
			if (transform_rotation != null) {
				transform.rotation = Quaternion.Euler (getVectorFromMetaString (transform_rotation));								
			}
			
			string transform_scale = utils.BundleHelper.GetMetadataSingleValueByKey ("TransformScale", bundle.metadata);
			if (transform_scale != null) {
				transform.localScale = getVectorFromMetaString (transform_scale, new Vector3 (1, 1, 1));
			}
			string legenda = utils.BundleHelper.GetMetadataSingleValueByKey ("Caption", bundle.metadata);
			if (!string.IsNullOrEmpty(legenda)) {
				Debug.LogError ("Caption Abstract ");
                ScreenEvents.GetInstance().ResetCaption();
                this.captionMetadataConfig (legenda);
			}
		}
		/// <summary>
		/// Captions the metadata config.
		/// Configuração de legendas através do metadata
		/// </summary>
		/// <param name="legenda">Legenda.</param>
		void captionMetadataConfig (string legenda)
		{

			List<string> textos = PipeToList.Parse (legenda);
			if (textos.Count > 0) {
				ScreenEvents.GetInstance ().SetCaption (textos [0]);

				bool hasConfig = false;
				//seta valor padrão
				int size = 60;
				//seta se é autosize ou não
				bool autoSize = false;
				if (textos.Count > 1) {
					//tem config
					hasConfig = true;
					size = int.Parse (textos [1]);
				}
				if (textos.Count > 2) {
					//tem config
					hasConfig = true;
					autoSize = bool.Parse (textos [2]);
				}
				if (hasConfig) {
					ScreenEvents.GetInstance ().ConfigCaption (size, autoSize);
				}
			}
		}

		/// <summary>
		/// Gets the vector from meta string no padrão do server
		/// </summary>
		/// <returns>The vector from meta string.</returns>
		/// <param name="data">Data.</param>
		/// <param name="defaultValue">Default value.</param>
		protected Vector3 getVectorFromMetaString (string data, Vector3 defaultValue = default(Vector3))
		{
			char[] splitchar = { '|' };
			string[] position_values = data.Split (splitchar);
			if (position_values.Length == 3) {
				return new Vector3 (float.Parse (position_values [0]), float.Parse (position_values [1]), float.Parse (position_values [2]));
			}
			return defaultValue;
		}

		protected BundleVO _vo;
				
		/// <summary>
		/// Checks the local file.
		/// </summary>
		/// <returns><c>true</c>, if local file was checked, <c>false</c> otherwise.</returns>
		/// <param name="file_destination">File_destination.</param>
		/// <param name="md5">Md5.</param>
		protected bool checkLocalFile (string file_destination, string md5)
		{
			if (md5 == null || md5 == "") {
				//nesse ponto, como não veio o md5, ignora o checksum
				return ARMFileManager.Exists (file_destination);
			}
			return ARMChecksum.CheckFileMD5 (file_destination, md5);
		}
		/// <summary>
		/// Saves the file and update database.
		/// Para consolidar o carquivo, salvar ele local no device e atualizar a info dele no banco de dados
		/// </summary>
		/// <returns>The file and update database.</returns>
		/// <param name="urlInfoVO">URL info V.</param>
		/// <param name="bytes">Bytes.</param>
		/// <param name="file_type">File_type.</param>
		protected ReturnDataVO saveFileAndUpdateDatabase (ref UrlInfoVO urlInfoVO, byte[] bytes, string file_type)
		{
			
			//define a pasta de forma encapsulada para que todos os lugares crie no mesmo padrão
			string file_destination = BundleHelper.GetPersistentPath (BundleHelper.GetFileDownloadPath (this._vo, urlInfoVO));
			if (! ARM.utils.io.ARMFileManager.WriteBites (file_destination, bytes)) {
				//erro ao escrever arquivo!
				return new ReturnDataVO (){ success=false, message="Erro ao salvar arquivo no device" };	
			}
			if (!checkLocalFile (file_destination, urlInfoVO.md5)) {
				//lida com o erro ao checkar a consistencia ou integridade do arquivo
				return new ReturnDataVO (){ success=false, message="Arquivo corrompido!" };	
			}
			//até agora tudo bem
			Debug.LogWarning ("(In) InAppAbstract : saveFileAndUpdateDatabase ~ file_destination:" + file_destination);
			urlInfoVO.local_url = file_destination;
			urlInfoVO.status = UrlInfoConstants.STATUS_FILE_CACHED;
			ReturnDataVO retorno = AppRoot.deviceFileInfo.commit (urlInfoVO);
			AppRoot.deviceFileInfo.Save ();
			return retorno ;
		}
		public abstract string GetInAppName ();
        
		/// <summary>
		/// Sets the bundleVO to Load
		/// Se o bundle já estiver carregado, já sete que está completo
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		public virtual void SetBundleVO (BundleVO bundle)
		{
			this.debugMetasBundles (bundle.metadata);
			TransformEvents.AddOnRotationEventHandler (changeRotationFromGUI);
			TransformEvents.AddOnPositionEventHandler (changePositionFromGUI);
			TransformEvents.AddOnScaleEventHandler (changeScaleFromGUI);
			_vo = bundle;
			this.CheckDelayToRun (bundle);
		}

		void debugMetasBundles (MetadataVO[] metadata)
		{
			if (metadata == null) {
				return;
			}
			if (metadata.Length == 0) {
				return;
			}
			//para debug coloca todos os metadatas recebidos nessa string
			foreach (MetadataVO m in metadata) {
				DebugMetas.Add (m.key + ">" + m.value);
			}

		}

		public string GetBundleId ()
		{
			if (_vo == null) {
				return "";
			}
			return _vo.id;
		}
		/// <summary>
		/// Checks the delay to run and save in _delayToRun
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		void CheckDelayToRun (BundleVO bundle)
		{
			//verifica se precisa de delay to Run
			string delayToRun = BundleHelper.GetMetadataSingleValueByKey ("delay_to_run", bundle.metadata);
			if (delayToRun != "" && delayToRun != null) {
				int delay = int.Parse (delayToRun); 
				if (delay > 0) {
					this._delayToRun = delay;
				}
			}

			string resetOnStop = BundleHelper.GetMetadataSingleValueByKey ("delay_reset_on_stop", bundle.metadata);
			if (resetOnStop != "" && resetOnStop != null) {
				this._delayResetOnStop = bool.Parse (resetOnStop); 

			}
		}

		void changeRotationFromGUI (Vector3 value)
		{
			// throw new NotImplementedException();
			transform.Rotate (value);
		}
		void changePositionFromGUI (Vector3 value)
		{
			// throw new NotImplementedException();
			transform.position += value;
		}

		void changeScaleFromGUI (Vector3 value)
		{
			// throw new NotImplementedException();
			transform.localScale += value;
		}

		/// <summary>
		/// Run this app. Show and play if needs...
		/// </summary>
		public void Run ()
		{
			if (_delayToRun > 0) {
				//foi configurado que tem delay para dar play
				//precisa verificar se já está no pause
				if (this._delayPaused > 0) {
					this.StartDelay ();
					return;
				}
				if (this._delayFinished) {
					this.DoRun ();
					return;
				}
				//não está pausado, verifica se já não
				this.StartDelay ();
				return;
			}
			this.DoRun ();

		}

		void StartDelay ()
		{
			//faz o disconto pra caso tenha sido pausado
			int delay = (this._delayPaused > 0) ? this._delayPaused : this._delayToRun;
			this._waitingToRun = true;
			this._timerRunningId = EasyTimer.SetTimeout (this.DelayFinished, delay);
		}

		void StopDelay ()
		{
						
			if (_timerRunningId >= 0) {
				EasyTimer.ClearInterval (_timerRunningId);
			}
			this._waitingToRun = false;
			this._timerRunningId = -1;
			this._delayPaused = 0;
		}
		void PauseDelay ()
		{
			if (_timerRunningId >= 0) {
				//se está esperando para dar run
				if (this._waitingToRun) {
					//pause e guarda o tempo que faltava terminar
					TimeoutItem tmi = EasyTimer.GetTimeoutItem (_timerRunningId);
					tmi.Pause ();
					this._delayPaused = Mathf.FloorToInt (tmi.delay * 1000);
				}
			}
		}
		void DelayFinished ()
		{
			StopDelay ();
			if (!this._delayResetOnStop) {
				_delayFinished = true;
			}
			this.DoRun ();
		}



		public abstract void DoRun ();


		/// <summary>
		/// Stop this app. Hide and stop if needs... But dont dispose 
		/// </summary>
		public void Stop ()
		{
						
			if (this._delayToRun > 0) {
				//foi configurado um tempo para parar
				//por padrão da pause
				PauseDelay ();
				if (_delayResetOnStop) {
					//foi dito que ao parar, da reset
					StopDelay ();
				}
								
								
			}
			this.DoStop ();
		}
		public abstract void DoStop ();

		/// <summary>
		/// Releases all resource used by the <see cref="MagicTV.abstracts.InAppAbstract"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="MagicTV.abstracts.InAppAbstract"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="MagicTV.abstracts.InAppAbstract"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the
		/// <see cref="MagicTV.abstracts.InAppAbstract"/> so the garbage collector can reclaim the memory that the
		/// <see cref="MagicTV.abstracts.InAppAbstract"/> was occupying.</remarks>
		public abstract void Dispose () ;
				
		//public abstract ARMProgressInterface<BundleVO, OnQuitEventHandler, OnProgress > GetProgressInfo ();

		#region Analytics
		protected InAppAnalytics _analytics;
		protected virtual string GetAnalyticsCategory ()
		{ 
			return "InApp";
		}
		public virtual void SetAnalytics (InAppAnalytics analytics)
		{
			_analytics = analytics; 
		}
		#endregion
				
	}
}