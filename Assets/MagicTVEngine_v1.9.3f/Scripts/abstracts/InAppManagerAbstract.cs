﻿using UnityEngine;
using System.Collections;

using ARM.abstracts ;
using MagicTV.vo;
using ARM.abstracts.components;
using ARM.animation;


namespace MagicTV.abstracts{
	public abstract class InAppManagerAbstract : GenericStartableComponentControllAbstract {

		/// <summary>
		/// Play by the specified app name.
		/// </summary>
		/// <param name="appName">App name.</param>
		public abstract void Play( string appName ) ;

		/// <summary>
		/// Stop the specified app name .
		/// </summary>
		/// <param name="appName">App name.</param>
		public abstract void Stop( string appName ) ;

	}
}