﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CloneRigColliders : MonoBehaviour
{
    public GameObject ColliderRigToClone;

    public void CloneCollider()
    {
        Debug.LogWarning("Clonando Rig de Colliders... de : " + ColliderRigToClone.name + " para: " + gameObject.name + "...");
        //  var colliders = ColliderRigToClone.GetComponentsInChildren<CapsuleCollider>();
        //  foreach (var item in colliders)
        //  {
        //      Debug.Log(item.gameObject.name);
        //  }
        nodeChildrens(gameObject, ColliderRigToClone);
    }

    protected void nodeChildrens(GameObject parent, GameObject parentReference)
    {
        SphereCollider sc_ref = parentReference.GetComponent<SphereCollider>();
        CapsuleCollider cc_ref = parentReference.GetComponent<CapsuleCollider>();

        if (sc_ref != null)
        {
            SphereCollider sc = parent.GetComponent<SphereCollider>();
            if (sc == null) sc = parent.AddComponent<SphereCollider>();
            sc.center = new Vector3(sc_ref.center.x, sc_ref.center.y, sc_ref.center.z);
            sc.radius = sc_ref.radius;
        }

        if (cc_ref != null)
        {
            CapsuleCollider cc = parent.GetComponent<CapsuleCollider>();
            if (cc == null) cc = parent.AddComponent<CapsuleCollider>();
            cc.center = new Vector3(cc_ref.center.x, cc_ref.center.y, cc_ref.center.z);
            cc.radius = cc_ref.radius;
            cc.height = cc_ref.height;
            cc.direction = cc_ref.direction;
        }

        for (int i = 0; i < parent.transform.childCount; i++)
        {
            var currentGO = parent.transform.GetChild(i);
            Transform GO_ref = null;

            //checando a referencia up
            for (int ii = i; ii < parentReference.transform.childCount; ii++)
            {
                var currentGO_ref = parentReference.transform.GetChild(ii);
                if ((currentGO.name == currentGO_ref.name) || (currentGO.childCount == currentGO_ref.childCount))
                {
                    GO_ref = currentGO_ref;
                    break;
                }
            }

            if (GO_ref == null)
            {
                var indexDown = (i >= parentReference.transform.childCount) ? parentReference.transform.childCount-1 : i;
                //checando a referencia down
                for (int ii = indexDown; ii >= 0; ii--)
                {
                    var currentGO_ref = parentReference.transform.GetChild(ii);
                    if ((currentGO.name == currentGO_ref.name) || (currentGO.childCount == currentGO_ref.childCount))
                    {
                        GO_ref = currentGO_ref;
                        break;
                    }
                }
            }

            if (GO_ref != null)
            {
                Debug.Log("referencia encontrada : " + currentGO.name + " para " + GO_ref.name);
                nodeChildrens(currentGO.gameObject, GO_ref.gameObject);
            }
        }
    }
}
