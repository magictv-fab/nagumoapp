﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MagicTV.globals.events;

public class DebugsConfigGui : MonoBehaviour , OnOffListenerEventInterface{
	public bool _active;
	// Use this for initialization
	protected string currentWindow ;
	protected List<string> historyWindows = new List<string>() ;

	/// <summary>
	/// se true, tem itens e compontentes para debugar
	/// </summary>
	protected bool _hasConfigurablesDebug;

	/// <summary>
	/// se true, tem componetnes debug pra testar
	/// </summary>
	protected bool _hasOnOffsDebug;


	/// <summary>
	/// Os componentes para acesso e controle
	/// </summary>
	protected List<ConfigurableListenerEventInterface> configurables = new List<ConfigurableListenerEventInterface>();
	protected List<OnOffListenerEventInterface> onOffs = new List<OnOffListenerEventInterface>();

	public Dictionary<string, ConfigurableListenerEventInterface> _configurableComponents = new Dictionary<string, ConfigurableListenerEventInterface>();
	public Dictionary<string, OnOffListenerEventInterface> _onOffs = new Dictionary<string, OnOffListenerEventInterface>();

	public GameObject[] gameObjectsToFind;

	public List<GameObject> DebugOnOffComponents = new List<GameObject>();
	public List<GameObject> DebugConfigurableComponents = new List<GameObject>();

	protected string[] _buttonsOnsNames;
	protected string[] _buttonsConfigNames ;
	void Start () {
		//navegação inicial
		this.currentWindow = this.getUniqueName ();
		//procura os game objects para debug quando da o start
		//lista os itens configuraveis

		foreach (GameObject go in gameObjectsToFind) {
			ConfigurableListenerEventInterface[] configurablesTemp = go.GetComponentsInChildren<ConfigurableListenerEventInterface> ();
			; //GameObject.FindObjectsOfType<ConfigurableListenerEventInterface>();
			if (configurablesTemp.Length > 0) {
				configurables.AddRange (configurablesTemp);
			}
		}
		List<string> temp = new List<string> ();

		if (configurables != null && configurables.Count > 0) {
			this._hasConfigurablesDebug = true;
			foreach (ConfigurableListenerEventInterface conf in configurables) {
				if (conf.getUniqueName () == this.getUniqueName ()) {
					continue;
				}
				temp.Add (conf.getUniqueName ());
				_configurableComponents.Add (conf.getUniqueName (), conf);
				DebugConfigurableComponents.Add (conf.getGameObjectReference ());
			}
		}
		_buttonsConfigNames = temp.ToArray ();
		temp.Clear ();
		//lista os onOffs
		foreach (GameObject go in gameObjectsToFind) {
			OnOffListenerEventInterface[] onOffsTemp = go.GetComponentsInChildren<OnOffListenerEventInterface> ();
			; //GameObject.FindObjectsOfType<ConfigurableListenerEventInterface>();
			if (onOffsTemp.Length > 0) {
				onOffs.AddRange (onOffsTemp);
			}
		}
		
		if (onOffs != null && onOffs.Count > 0) {
			this._hasOnOffsDebug = true;
			foreach (OnOffListenerEventInterface ons in onOffs) {
				if (ons.getUniqueName () == this.getUniqueName ()) {
					continue;
				}
				temp.Add (ons.getUniqueName ());
				_onOffs.Add (ons.getUniqueName (), ons);
				DebugOnOffComponents.Add (ons.getGameObjectReference ());
			}
		}
		this._buttonsOnsNames = temp.ToArray ();

		//Dispara o evento global para os demais interessados
		AppRootEvents.GetInstance ().GetAR ().onTrackIn += trackIn ;
		AppRootEvents.GetInstance ().GetAR ().onTrackOut += trackOut ;
	}
	
	void trackIn (string s)
	{
		if(s == "debug"){
			this._active = true ;
		}
	}
	
	void trackOut (string s)
	{
		if(s == "debug"){
			//this._active = false ;
		}
	}

	bool _isRootWindow ;
	void OnGUI(){
		if(!_active){
			return;
		}
		_isRootWindow = (this.currentWindow == this.getUniqueName()) ;
		GUILayout.Label( "[ "+this.currentWindow+" ]" ) ;
		//se for root já para aqui
		if(this._isRootWindow){
			this.showRootWindow();
			return;
		}
		//botao voltar
		if(GUILayout.Button ("VOLTAR")){
			this.HistoryBack();
			return;
		}
		//passou, não é root, entra na atmosfera de cada um dos debugs
		if(this._hasOnOffsDebug ){
			if(this.currentWindow == "onOffs"){
				this.showOnOffDebugButton();
				return;
			}
			//verificando navegaçõa interna da lista
			if( this._onOffs.ContainsKey(this.currentWindow) ){
				//está nesse item
				//TODO: fazer o debug desse cara aparecer, cada um tem um esquema
				OnOffListenerEventInterface onObj = this._onOffs[this.currentWindow];
				if(onObj.isActive()){
					if( GUILayout.Button ( onObj.getUniqueName()+"[TURN OFF]" )){
						onObj.turnOff ();
						return;
					}
					return ;
				}
				if( GUILayout.Button ( onObj.getUniqueName()+"[TURN ON]" )){
					onObj.turnOn ();
					return;
				}
				return;
			}
			return;
		}
		//chegou aqui, deve estar em configs
		if(this._hasConfigurablesDebug ){
			if(this.currentWindow == "configs"){
				this.showConfigurablesDebugButton();
				return;
			}
			Vector3 teste = new Vector3();

			//verificando navegaçõa interna da lista
			if( this._configurableComponents.ContainsKey(this.currentWindow) ){
				return;
				//está nesse item
				//TODO: fazer o debug desse cara aparecer, cada um tem um esquema
				ConfigurableListenerEventAbstract conf = this._configurableComponents[this.currentWindow].getGameObjectReference().GetComponent< ConfigurableListenerEventAbstract >();
				if(conf!= null){
					
				}
				return;
			}
		}
		//se chegou aqui está bem errado. Botao reset pra voltar toda navegação
		if( GUILayout.Button ( "BACK ALL")){
			this.currentWindow = this.getUniqueName();
			this.historyWindows.Clear();
		}
	}



	/// <summary>
	/// Shows the root window.
	/// </summary>
	void showRootWindow ()
	{
		if(this._hasOnOffsDebug){
			if( GUILayout.Button ( "OnOffs ("+this._onOffs.Count+") ")){
				saveHistory("onOffs");
			}
		}
		if(this._hasConfigurablesDebug){
			if( GUILayout.Button ( "Configs ("+this._configurableComponents.Count+") ")){
				saveHistory("configs");
			}
		}
	}

	/// <summary>
	/// Shows the configurables debug buttons.
	/// </summary>
	void showConfigurablesDebugButton ()
	{

		foreach( ConfigurableListenerEventInterface obj in this.configurables ){
			if( GUILayout.Button ( obj.getUniqueName()+" [conf]" )){
				saveHistory(obj.getUniqueName());
				return;
			}
		}
	}


	void showOnOffDebugButton ()
	{
		int count = 0;
		int column = 1;
		float largura = 180f;


		int selected = GUILayout.SelectionGrid (-1 , this._buttonsOnsNames , 2);
		if(selected >= 0){
			if( this._onOffs.ContainsKey( this._buttonsOnsNames[selected] ) ){
				this.saveHistory( this._onOffs[ this._buttonsOnsNames[selected] ].getUniqueName() );
			}
		}
		return;

	}

	public string getUniqueName(){
		return this.gameObject.name;
	}
	public void turnOn (){
		this._active = true;
	}
	public void turnOff (){
		this._active = false;
	}
	public void ResetOnOff (){
//		turnOff ();
	}
	public GameObject getGameObjectReference(){
		return this.gameObject;
	}
	//navega pra frente
	protected void saveHistory(string current){
		historyWindows.Add (current);
		this.currentWindow = current;
	}
	//navega pra trás
	protected void HistoryBack(){
		if(historyWindows.Count <= 1){
			historyWindows.Clear ();
			this.currentWindow = this.getUniqueName();
			return;
		}
		this.currentWindow = this.historyWindows[historyWindows.Count-1];
		historyWindows.RemoveAt (historyWindows.Count-1);
	}
	/// <summary>
	/// Retorna true se esse componente estiver ativo
	/// </summary>
	/// <returns><c>true</c>, if active was ised, <c>false</c> otherwise.</returns>
	public bool isActive(){
		return this._active ;
	}
}
