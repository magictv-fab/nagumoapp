using UnityEngine;
using System.Collections;
using ARM.animation;


/// <summary>
/// Fake just for tests
/// </summary>
public class FakeSystemAccessControll : GenericStartableComponentControllAbstract {

	public override void prepare ()
	{
		this.RaiseComponentIsReady () ;
	}
	public override void Play ()
	{
		base.Play ();
		if( ! this._isFinished ){
			this.RaiseOnFinishedEventHandler () ;
		}
	}
	public override void Stop ()
	{
		base.Stop ();
	}
	public override void Pause ()
	{
		base.Pause ();
	}


}
