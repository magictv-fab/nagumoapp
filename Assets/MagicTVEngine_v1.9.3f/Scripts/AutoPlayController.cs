﻿using UnityEngine;
using System.Collections;
using ARM.animation;
using System.Linq;

namespace MagicTV.in_apps.animation
{
		public class AutoPlayController : AnimatorController
		{
				private AmIVisible _amIVisible;

				protected new void Start ()
				{
						_amIVisible = gameObject.GetComponent<AmIVisible> ();

						if (_amIVisible != null) {
								_amIVisible.ChangeVisible += HandleOnChangeVisible;
								base.Start ();
								this.Play ();

						}
				}

				void HandleOnChangeVisible (bool m)
				{
						if (!m) {
								this.Stop ();
								return;
						}

						this.Play ();
				}
		}
}