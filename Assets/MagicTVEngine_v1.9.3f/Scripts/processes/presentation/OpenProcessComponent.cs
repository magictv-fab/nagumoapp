﻿using UnityEngine;
using System.Collections;
using ARM.utils.download;
using ARM.utils.io;
using System.Linq;
using System.IO;
using System.Collections.Generic;


/// <summary>
/// 
/// @author: Renato Seiji Miawaki
/// @version: 1.0
/// 
/// </summary>
using ARM.components;
using ARM.animation;
using MagicTV.globals.events;
using MagicTV.globals;
using MagicTV.abstracts.screens;
using MagicTV.utils;
using System;
using Vuforia;


namespace MagicTV.processes.presentation
{
	/// <summary>
	/// Open process component.
	/// Process to open Presentation Controller
	/// </summary>
	public class OpenProcessComponent : GenericStartableComponentControllAbstract
	{

		#region dependent component

		public GeneralScreenAbstract inAppGui ;
		public GameObject trackingContainer;
		#endregion



		//	
		// Update is called once per frame
		protected List<ObjectTracker> _imageTarget = new List<ObjectTracker> ();
		public TrackerManager trackerManager ;
		/// <summary>
		/// All the folders to load tracks
		/// </summary>
		string[] foldersTracks ;
		/// <summary>
		/// The track folder.
		/// </summary>
		string _trackFolder;

		int _indexLoadDataset = 0;

		bool _startLoadDataset;

		public override void prepare ()
		{

			//Debug.Log ("OpenProcessComponent . INIT ");
			checkTrackingFolders ();
				    
			this.RaiseComponentIsReady ();
			//start the components

		}


		/// <summary>
		/// Loads all data set in folder folderTrack
		/// </summary>
		void loadAllDataSet ()
		{
			if (this._isFinished) {
//								//Debug.Log ("\t\t\t\t ~~~~~~~~  _isFinished");
				return;
			}

			if (foldersTracks == null || foldersTracks.Length == 0) {
//								Debug.LogWarning ("\t\t\t\t ~~~~~~~~  OpenProcessComponent . loadAllDataSet . não tem track para carregar");
				this.step2 ();
				return;
			}
			createDataList ();
						

		}
		List<string> dataXmlList;
		void createDataList ()
		{

			dataXmlList = new List<string> ();
			for (int i = 0; i < foldersTracks.Length; i++) {
				string folder = this._trackFolder + foldersTracks [i];
								
				if (ARMFileManager.Exists (folder)) {
					//Debug.LogWarning ("OpenProcess . createDataList [" + PresentationController.getTotalTime () + "] " + folder + " EXISTE!");		
					FileInfo[] files = ARMFileManager.GetFilesOfDirectory (folder, "*.xml");
					if (files != null) {
						foreach (FileInfo file in files) {
							//TODO: precisa conferir se exite o dat e o xml
							//Debug.LogWarning ("OpenProcess . createDataList [" + PresentationController.getTotalTime () + "] " + folder + " adding XML " + folder + "/" + file.Name);		
							dataXmlList.Add (folder + "/" + file.Name);
						}
					}
				}
			}
			startLoadDatasetFolder ();

		}

		void startLoadDatasetFolder ()
		{
			//Debug.LogWarning ("OpenProcess . startLoadDatasetFolder [" + PresentationController.getTotalTime () + "] " + this.dataXmlList.Count + " dataXML!");		
			if (this.dataXmlList.Count > 0) {
				this._indexLoadDataset = 0;
				this._startLoadDataset = true;
				return;
			}
			this.step2 ();
						
		}
		void Update ()
		{
			if (this._startLoadDataset) {
				loadNextDataset ();
			}
		}

		void loadNextDataset ()
		{
						
			if (this._indexLoadDataset >= this.dataXmlList.Count) {
				this._startLoadDataset = false;
				step2 ();
				return;
			}
			string xmlFile = this.dataXmlList [this._indexLoadDataset];
						
			loadLocalDataset (xmlFile, true);
						
			this._indexLoadDataset++;
						
		}

		/// <summary>
		/// Loads the local dataset of folder
		/// </summary>
		/// <param name="xmlFilePath">Xml file path.</param>
		void loadLocalDataset (string xmlFilePath, bool persistTrack = false)
		{
			trackerManager = TrackerManager.Instance;
			ObjectTracker imageTracker = trackerManager.GetTracker<ObjectTracker> ();

			this._imageTarget.Add (imageTracker);

			DataSet dataset = imageTracker.CreateDataSet ();
			dataset.Load (
							xmlFilePath, 
						VuforiaUnity.StorageType.STORAGE_ABSOLUTE
			);

			imageTracker.ActivateDataSet (dataset);
			imageTracker.PersistExtendedTracking (persistTrack);

		}




		public override void Play ()
		{
			base.Play ();
			checkTrackingFolders ();

			AppRootEvents.GetInstance ().GetAR ().AddChangeARToggleHandler (loadTracks);
			AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOn ();
						
		}

		private void checkTrackingFolders ()
		{
			this._trackFolder = UrlHelper.GetFolderToTrack ();
			Debug.LogError("UU PASTA DE TRACKINGS " + this._trackFolder);
				foldersTracks = ARMFileManager.GetChildDirectoryNames (this._trackFolder);
			if (foldersTracks == null){
				Debug.LogError("UU e achamos nada, debug null filho de uma puta!");
				return;
			}
			Debug.LogError("UU e achamos" + foldersTracks.Length);
		}

		void loadTracks (bool b)
		{
			if (! b) {
				return;
			}
			AppRootEvents.GetInstance ().GetAR ().RemoveChangeARToggleHandler (loadTracks);
			try {
				loadAllDataSet ();
			} catch (UnityException e) {
				Debug.LogError ("UnityException ao carregar dataset !!! " + e);
			} catch (Exception e) {
				Debug.LogError ("UnityException ao carregar dataset !!! " + e);
			}
						
						
						
		}
		void step2 ()
		{
			this.organizeTracks ();
		}
		public ImageTargetBehaviour[] DebugImgTargetsObj;
		public GameObject DebugRoot;
		void organizeTracks ()
		{
			//cria a "pasta" para colocar os trackings
			GameObject tracks = new GameObject ("tracks");
			if (trackingContainer != null) {
				tracks.gameObject.transform.parent = trackingContainer.transform;
			}
			ImageTargetBehaviour[] allObjects = UnityEngine.Object.FindObjectsOfType<ImageTargetBehaviour> ();
			//um por um, coloca ele na pasta
			for (int i = 0; i < allObjects.Length; i++) {
				ImageTargetBehaviour go = allObjects [i];
				if (go.gameObject.activeInHierarchy) {
					//trocando a pasta
					go.gameObject.transform.parent = tracks.transform;
					//trocando o nome
					go.gameObject.name = "Track " + i;
				}
			}

            InitialSplashScreen.Instance.hide();
            LoadScreenBar.Instance.hide();

            if (this.inAppGui != null) {
				this.inAppGui.show ();
			}
//			Debug.LogWarning ("OpenProcess . step2 [" + PresentationController.getTotalTime () + "]");
			this.RaiseOnFinishedEventHandler ();
		}
		
		public override void Stop ()
		{
			AppRootEvents.GetInstance ().GetAR ().RaiseWantToTurnOff ();

			base.Stop ();
			//remove tudo???
			//por enquanto não
		}
		public override void Pause ()
		{
			//base.Pause ();

		}
	}
}


