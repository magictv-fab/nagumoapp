using UnityEngine;
using System.Collections;
using ARM.animation;


/// <summary>
/// 
/// @author: Renato Seiji Miawaki
/// @version: 1.0
/// 
/// </summary>
using ARM.components;
using ARM.abstracts.components;
using MagicTV.abstracts;
using ARM.abstracts;
using ARM.abstracts.internet;
using MagicTV.abstracts.screens;
using MagicTV.globals;
using MagicTV.globals.events;


namespace MagicTV.processes
{
    /// <summary>
    /// Init process component.
    /// Class to call initial processes
    /// </summary>
    public class UpdateProcessComponent : GenericStartableComponentControllAbstract
    {

        #region dependent component



        //public LoadScreenAbstract loadScreen ;

        public ProgressEventsAbstract updateProcess;
        #endregion

        //protected GroupComponentsManager groupComponent ;

        public override void prepare()
        {

            //start the components

            if (this.updateProcess == null)
            {
                //Debug.LogError ("UpdateProcessComponent . init ! sem componente para atualização, está pronto ");


                this.RaiseComponentIsReady();
                return;
            }


            ContentEvents.GetInstance().onUpdate += Play;
            this.updateProcess.AddCompleteEventhandler(downloadCompleted);


            if (!this.updateProcess.isComponentIsReady())
            {

                this.updateProcess.AddCompoentIsReadyEventHandler(this.RaiseComponentIsReady);
                this.updateProcess.prepare();
                return;
            }

            this.RaiseComponentIsReady();

        }


        public override void Play()
        {
            base.Play();
            if (this.updateProcess == null)
            {
                this.RaiseOnFinishedEventHandler();
                return;
            }
            this.updateProcess.Play();
        }

        void downloadCompleted()
        {
            ContentEvents.GetInstance().RaiseUpdateComplete();
            this.RaiseOnFinishedEventHandler();
            Stop();
        }

        public override void Stop()
        {
            base.Stop();
            if (this.updateProcess == null)
            {
                return;
            }


            this.updateProcess.Stop();
            //não tem stop ainda
        }
        public override void Pause()
        {
            if (this.updateProcess == null)
            {
                return;
            }
            this.updateProcess.Pause();
            //base.Pause ();
            //nao tem pause
        }
    }
}