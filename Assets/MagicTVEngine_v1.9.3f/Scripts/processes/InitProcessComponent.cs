using ARM.animation;


using ARM.components;
using  MagicTV.globals.events;


/// <summary>
/// 
/// 
/// @author: Renato Seiji Miawaki
/// @version: 1.0
/// 
/// </summary>
namespace MagicTV.processes
{
	/// <summary>
	/// Init process component.
	/// Class to call initial processes
	/// </summary>
	public class InitProcessComponent : GenericStartableComponentControllAbstract
	{

		#region dependent component

		public ComponentsStarter _componentStarter ;

		#endregion

		public override void prepare ()
		{

			
			AppRootEvents.GetInstance ().GetAR ().AddOnWantToTurnOnEventHandler (VuforiaARControll.Instance.Enable);
			AppRootEvents.GetInstance ().GetAR ().AddOnWantToTurnOffEventHandler (VuforiaARControll.Instance.Disable);
			
			if (_componentStarter == null) {
				//Debug.LogError ( "InitProcessComponent . sete o componente para iniciar | " ) ;
				return;
			}
			
			addProcessInitStep (_componentStarter, componentComplete);
		}
		

		void componentComplete ()
		{
			//Debug.LogError ( "InitProcessComponent . complete " ) ;
			
			RaiseComponentIsReady ();

		}
	}
}