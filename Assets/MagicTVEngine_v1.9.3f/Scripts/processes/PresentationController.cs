﻿using UnityEngine;
using System.Collections;
using ARM.animation;
using InApp;

/// <summary>
///
/// @author: Renato Seiji Miawaki
/// @version: 1.0
///
/// </summary>
using MagicTV.processes.presentation;
using MagicTV.abstracts;
using ARM.components;
using ARM.abstracts.components;
using MagicTV.globals.events;

/// <summary>
/// Presentation controller.
///
///
///
/// @author : Renato Seiji Miawaki
///
/// </summary>
using System;
using MagicTV.globals;
using MagicTV.in_apps;


namespace MagicTV.processes
{
	/// <summary>
	/// Presentation Controller
	/// Controll the presentation and process to show "in apps"
	/// </summary>
	public class PresentationController : GenericStartableComponentControllAbstract
	{
        public GameObject buttonGame;
		public static bool tracking = false;
		public static bool checkTracking = false;
		
		public bool lockRun = false;
		public static long startTime = 0;
		public static long lastTotal = 0;
		public static long getTotalTime ()
		{
			
			long total = (DateTime.Now.Ticks - startTime) / 1000;
			
			long subTotal = (total - lastTotal);
			
			
			//			Debug.Log ("[TIME SUBTOTAL] desde a ultima consulta:" + subTotal);
			lastTotal = total;
			return total;
		}
		public bool debugNow;
		/// <summary>
		/// Nome do ultimo track para evitar de disparar multiplas vezes o mesmo track
		/// </summary>
		public string _lastTrackName;
		public static PresentationController Instance;
		void Start ()
		{
			Instance = this;
		}
		/// <summary>
		/// TRACK IN
		/// </summary>
		public void OnTrackingFound (string trackingName)
		{
			tracking = true;

			AppRootEvents.GetInstance ().GetAR ().RaiseTrackIn (trackingName);
			if (!AppRoot.canTrack) {
				return;
			}
			if (this._lastTrackName == trackingName) {
				Debug.LogError ("ON TRACKING FOUND =  "); 
				
				return;
			}

			if (GameObject.Find("Button Game Funcional"))
			{
				buttonGame = GameObject.Find("Button Game Funcional");
				buttonGame.SetActive(false);
			}


            if (trackingName == "nagumo_ofertas" || trackingName == "test_forma_play")
            {
                CarrosselController.instance.carrocel.SetActive(true);
            }

			PresentationController.startTime = DateTime.Now.Ticks;
			//			Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") PresentationController . OnTrackingFound ");
			//Time.timeScale = 1f;
			this.lockRun = false;
			
			if (!String.IsNullOrEmpty (this._lastTrackName)) {
				AppRootEvents.GetInstance ().GetScreen ().RaiseHideCloseButton ();
				ScreenEvents.GetInstance ().ResetCaption ();
				//				this.inAppManager.Reset ();
				//				this.inAppManager.Stop (_lastTrackName);
			}
			
			this._lastTrackName = trackingName;
			
			this.inAppManager.Play (trackingName);
			//Dispara o evento global para os demais interessados
			checkTracking = true;
			
			//Debug.Log (" \t\t\t ~ ~ OnTrackingFound " + trackingName);
		}
		
		public void OnTrackingLost (string trackingName)
		{
			if (buttonGame)
			{
				//buttonGame.SetActive(true);
			}

			tracking = false;

			AppRootEvents.GetInstance ().GetAR ().RaiseTrackOut (trackingName);
			Debug.Log (" On TrackingLost ");
			

			if (this.lockRun) {
				return;
			}
			ScreenEvents.GetInstance ().SetCaption ("");
			//anular somente quando for o last? ou sempre anula?
			if (this._lastTrackName == trackingName) {
				//por enquanto anulando só quando está null
				this._lastTrackName = null;
			}
			
			//Dispara o evento global para os demais interessados
			
			//Debug.LogWarning ("[ TIME DEBUG ] (" + PresentationController.getTotalTime () + ") PresentationController . OnTrackingLost ");
			
			//Time.timeScale = .1f;
			//Debug.Log ("*** Trackable " + trackingName + " lost");
			checkTracking = false;
			this.inAppManager.Stop (trackingName);
			
			
		}
		
		#region dependent component
		
		public GenericStartableComponentControllAbstract openProcessComponent ;
		
		public GUILoaderAbstract guiLoader ;
		
		public InAppManager inAppManager ;
		
		#endregion
		
		protected GroupComponentsManager groupComponent ;
		
		public override void prepare ()
		{
			//Debug.LogWarning (" ~ ~ PresentationController . init ");
			//start the components
			if (this.groupComponent == null) {
				this.groupComponent = new GroupComponentsManager ();
				
				GeneralComponentsAbstract[] components = new GeneralComponentsAbstract[ 3 ] {
					inAppManager,
					guiLoader,
					openProcessComponent
				};
				this.groupComponent.SetComponents (components);
			}
			
			
			if (! this.groupComponent.isComponentIsReady ()) {
				//				Debug.LogError (" Deu ruim x?");s
				this.groupComponent.AddCompoentIsReadyEventHandler (this.prepare);
				this.groupComponent.prepare ();
				return;
			}
			AREvents.GetInstance ().AddChangeARToggleHandler (ARChanged);
			//Ao clicar na screen deve mandar parar a apresentação atual
			AppRootEvents.GetInstance ().onRaiseStopPresentation += StopLastPresentation;
			this.RaiseComponentIsReady ();
			
		}
		
		void ARChanged (bool b)
		{
			InAppActorAbstract.centerExit = false;
			this.Reset ();
		}
		void Reset ()
		{
			this._lastTrackName = "";
			
		}
		
		public override void Play ()
		{
            if (GameObject.Find("Button Game Funcional"))
            {
                buttonGame = GameObject.Find("Button Game Funcional");
                buttonGame.SetActive(false);
            }

            base.Play ();
			//Debug.LogWarning ("PresentationController . Play ");
			step1 ();
		}
		/// <summary>
		/// Liga o ARCamera
		/// </summary>
		void step1 ()
		{
			//primeira coisa inicia o processo de abertura
			if (! this.openProcessComponent.isFinished ()) {
				//								this.openProcessComponent.AddOnFinishedEventHandler (RegisterAREvents);
				this.openProcessComponent.Play ();
				return;
			}
			//						RegisterAREvents ();
		}
		/// <summary>
		/// 
		/// Stops the last presentation.
		/// </summary>
		public void StopLastPresentation ()
		{
			
			if (this._lastTrackName == "" || this._lastTrackName == null) {
				
				
				return;
			}

			checkTracking = false;
			tracking = false;
            
            ScreenEvents.GetInstance ().ResetCaption ();
			this.lockRun = false;
			this.OnTrackingLost (this._lastTrackName);

            CarrosselController.instance.carrocel.SetActive(false);


		}
		
		public override void Stop ()
		{
            tracking = false;
			base.Stop ();
			
		}
		public override void Pause ()
		{
			base.Pause ();
			
		}
	}
}
