﻿using UnityEngine;
using System.Collections;
using ARM.abstracts;
using MagicTV.vo;
/// <summary>
/// 
/// @author : Renato Miawaki
/// @version : 1.0
/// 
/// Download images queue.
/// 
/// </summary>
using ARM.utils.request;
using MagicTV.utils;
using System.Collections.Generic;
using ARM.utils.io;
using ARM.utils.download;


public class DownloadImagesQueue : ProgressEventsAbstract
{
	protected RequestImagesItemVO[] _images ;
	protected int totalBytes = 0 ;
	protected float totalBytesLoaded = 0f;
	protected List<string> _paths = new List<string> ();

	public void SetDownloadImagesList (RequestImagesItemVO[] images_url)
	{
		_images = images_url;
	}
	public override void prepare ()
	{
//		Debug.LogError ("prepare " + _images.Length);
		//chame o prepare após SetDownloadImageList, não antes
		//lembrando que o servidor deve informar o size do arquivo
		if (_images == null || _images.Length == 0) {
			//tem itens a serem feito downlaod
			this.RaiseComponentIsReady ();
		}
		//anota o total
		foreach (RequestImagesItemVO image in _images) {
			if (image.size != null) {
				int subTotalBytes = int.Parse (image.size);
				this.totalBytes += subTotalBytes;
			}

		}
//		Debug.LogError ("totalBytes " + this.totalBytes);
		this.RaiseComponentIsReady ();
	}
	public override void Play ()
	{
		if (this._isPlaying) {
			//já está executando
			return;
		}
		base.Play ();
		this.doStartDownload ();
	}
	protected int currentDownloadIndex = 0 ;
	/// <summary>
	/// Dos the download.
	/// </summary>
	void doStartDownload ()
	{
//		Debug.LogError ("doStartDownload");
		//inicia ou continua o download
		if (!this._isPlaying) {
			return;
		}
		if (this._images == null) {
			//não tem nada
			this.RaiseCompleteAndTotalProgress ();
			return;
		}
		if (this._images.Length <= currentDownloadIndex) {
			//acabou
			this.RaiseCompleteAndTotalProgress ();
			return;
		}
		RequestImagesItemVO currentImage = this._images [currentDownloadIndex];

//		Debug.LogError ("Iniciando loading de 1 imagem [ " + currentDownloadIndex + " ] .... " + currentImage.url);
		Download (currentImage.url);
	}
	void Download (string url)
	{ 

		StartCoroutine (FinishDownload (url)); //In C#, you have to explicitly start a corotine. 
	}

	IEnumerator FinishDownload (string url)
	{ 
		WWW hs_get = new WWW (url);	
		yield return hs_get;
//		Debug.LogError (" finish download ?" + url);
		onAssetBundleLoaded (hs_get.bytes);
	} 

	void errorDownloadImage (ARMRequestVO vo)
	{
//		Debug.LogError ("Erro ao carregar a imagem " + vo.Url);
	}

	void onProgressItem (float progress)
	{
		//soma com o total já efetuado com o recebido e dispara a porcentagem carregada
		if (this.totalBytes <= 0) {
			return;
		}
		this.RaiseProgress ((this.totalBytesLoaded + progress) / this.totalBytes);
	}
	public void clearCurrentCachedImages ()
	{
		if (ARMFileManager.Exists (GetSplashPathFolder ())) {
			ARM.utils.io.ARMFileManager.DeleteFolder (GetSplashPathFolder ());
		}

	}
	string GetSplashPathFolder ()
	{
		return BundleHelper.GetPersistentPath ("splashImages");
		;
	}
	void onAssetBundleLoaded (byte[] bytesLoaded)
	{
		//terminou um item, passa pro próximo
		//size atual consolidado
		RequestImagesItemVO currentImage = this._images [currentDownloadIndex];

//		Debug.LogError ("[" + currentDownloadIndex + "] currentImage.size: " + currentImage.size);
		if (currentImage.size != null) {

			this.totalBytesLoaded += int.Parse (currentImage.size);

		}
		this.currentDownloadIndex++;
		//reinicia o loop para download

		string folder = GetSplashPathFolder ();
//		Debug.LogError ("[" + this.currentDownloadIndex + "] imagem baixada, cria a pasta caso não exista " + folder);
		ARMFileManager.CreateFolderRecursivelyForPath (folder);
		string filePath = folder + currentDownloadIndex + ".jpg";

//		Debug.LogError ("imagem baixada, salva a imagem " + filePath);
		if (ARM.utils.io.ARMFileManager.WriteBites (filePath, bytesLoaded)) {
			Debug.LogError ("sucesso ao salvar, adiciona na lista de salvas");
			_paths.Add (filePath);
		}
		//abaixo apenas para debug

		this.doStartDownload ();
	}

	public string[] GetImagensLoaded ()
	{
		return _paths.ToArray ();
	}

	public override void Pause ()
	{
		
		if (!this._isPlaying) {
			//não da para parar algo que não iniciou
			return;
			
		}
		base.Pause ();
	}
	public override void Stop ()
	{
		if (!this._isPlaying) {
			//não da para parar algo que não iniciou
			return;
		}
		base.Stop ();
	}


}
