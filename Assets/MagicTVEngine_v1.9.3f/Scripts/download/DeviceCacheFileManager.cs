using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ARM.abstracts.internet;
using ARM.utils.io;
using MagicTV.abstracts.device;
using MagicTV.globals;

using System.Linq;
using MagicTV.globals.events;
using MagicTV.utils;
using MagicTV.vo;
using MagicTV.vo.result;

/// <summary>
/// Download manager. Faz a camada entre o processo de download com gravar no device o que já foi ou não baixado.
/// Também decide o nome em que cada coisa será baixada baseada no contexto e etc
///
/// @author: Renato Miawaki
///
/// </summary>


namespace MagicTV.download
{

    public class DeviceCacheFileManager : DeviceCacheFileManagerAbstract
    {

        public InternetInfoAbstract internetInfo;
       

        protected ResultRevisionVO _revisionToCache;
        public DeviceFileInfoAbstract _deviceFileInfo;
        private ConfirmResultVO _lastMessageVO;

        public static DeviceCacheFileManager Instance;

        public DeviceCacheFileManager()
        {
            Instance = this;
        }

        public override void prepare()
        {
            if (this._isComponentReady)
            {
                return;
            }

            //Debug.LogWarning ("DC DeviceCacheFileManager . init ");
            //
            if (!this.internetInfo.isComponentIsReady())
            {
                this.internetInfo.AddCompoentIsReadyEventHandler(this.prepare);
                this.internetInfo.prepare();
                return;
            }
            this.internetInfo.RemoveCompoentIsReadyEventHandler(this.prepare);
            //colocando-se para contexto global
            AppRoot.cacheFileManager = this;
			//se chamar o reset ele apaga realmente tudo
			AppRootEvents.GetInstance().GetDevice().RemoveResetEventHandler(reset);
            AppRootEvents.GetInstance().GetDevice().AddResetEventHandler(reset);
			//se quiser apagar apenas os arquivos baixados chame o limpar apresentações
			AppRootEvents.GetInstance().GetDevice().RemoveClearCacheEventHandler(deleteLocalFiles);
			AppRootEvents.GetInstance().GetDevice().AddClearCacheEventHandler(deleteLocalFiles);
            this.RaiseComponentIsReady();

        }
		public List<string> DebugUrlCachedToDelete = new List<string>();
		/// <summary>
		/// Deletes the local files.
		/// Apenas os arquivos
		/// </summary>
		void deleteLocalFiles ()
		{

			UrlInfoVO[] urls = _deviceFileInfo.getCachedUrlInfoVO();
			Debug.LogError ("Delete Local Files Called "+urls.Length);
			DebugUrlCachedToDelete = urls.Select ( u => u.local_url ).ToList<string>();
			if( urls.Length > 0 ){
				foreach( UrlInfoVO u in urls ){
					if(u.status < 1){
                        Debug.Log(" Status < 1 ");
						continue;
					}
					if(string.IsNullOrEmpty(u.local_url)){
                        Debug.Log(" IsNullOrEmpty u.local_url " + u.local_url);
						continue;
					}
                    Debug.Log(" ARMFileManager.Exists " + ARMFileManager.GetPersitentDataPath( ) + u.local_url );
                    string file_path = ARMFileManager.GetPersitentDataPath( ) +  "/" +u.local_url ;
					if (ARMFileManager.Exists( file_path))
					{
						try{
							ARMFileManager.DeleteFile(file_path);
							Debug.LogError ("Deletando "+file_path +" !! ");
							u.local_url = "";
							u.status = 0 ;
							_deviceFileInfo.commit (u);
						} catch( IOException e ){
							Debug.LogError ("Erro ao apagar arquivo local " +file_path +" >>> "+e.Message);
						}
					}
				}

                _deviceFileInfo.Save ();
				AppRootEvents.GetInstance ().GetDevice ().RaiseClearCacheCompleted();
				AppRootEvents.GetInstance ().RaiseStopPresentation();
				return;
			}
			AppRootEvents.GetInstance ().GetDevice ().RaiseClearCacheNothingToDeleteEventHandler ();

		}

        void reset()
        {

			MagicApplicationSettings.Instance.ResetHard();

        }

        public override void SetFileList(ResultRevisionVO revisionInfo)
        {
            this._revisionToCache = revisionInfo;
        }



        public override void StartCache()
        {
            //Debug.LogWarning ("DC DeviceCacheFileManager . StartCache ");
            if (this._revisionToCache == null)
            {
                //se nem tem o que baixar, então o processo está completo
                //Debug.LogError ("DC DeviceCacheFileManager . StartCache ||| se nem tem o que baixar, então o processo está completo !");
                this.RaiseCompleteAndTotalProgress();
                return;
            }

            if (this.internetInfo.hasWifi())
            {
                //tem wifi, manda baixar direto
                this.StartScreenDownloadNoCancel();
                return;
            }

            //Ele nào está no wifi, espera a confirmação do usuario para efetuar o download

            //Debug.LogWarning ("DC DeviceCacheFileManager . StartCache Ele nào está no wifi, espera a confirmação do usuario para efetuar o download");

            UserEvents.GetInstance().AddConfirmEventHandler(userConfirm);

            this._lastMessageVO = new ConfirmResultVO
            {
                message = "Existem atualizações e recomenda-se que esteja utilizando WIFI. Deseja fazer o download agora?"
            };

            UserEvents.GetInstance().RaiseConfirmCalled(this._lastMessageVO);

        }

        protected void userConfirm(ConfirmResultVO vo)
        {
            if (this._lastMessageVO != null && vo.id == this._lastMessageVO.id)
            {
                //resposta para essa classe
                if (vo.confirm)
                {
                    StartScreenDownloadCancel();
                    return;
                }
                cancelDownload();
                return;
            }
        }

        protected void cancelDownload()
        {
            Debug.LogError(" Cancenlar ");
            BulkBundleDownloader.Cancel();
            this.RaiseCompleteAndTotalProgress();
            WindowControlListScript.Instance.WindowInAppGUI.UpdateDone();
        }
        public void Cancel()
        {
            Debug.LogError(" Cancenlar ");
            this.cancelDownload();
        }
        /// <summary>
        /// Starts the screen download no cancel option.
        /// </summary>
        protected void StartScreenDownloadNoCancel()
        {
            Debug.LogError("StartScreenDownloadNoCancel ");

            //			ScreenEvents.GetInstance ().RaiseChangeToPerspective (Perspective.PROGRESS_BAR_WITHOUT_CANCEL);
            WindowControlListScript.Instance.WindowInAppGUI.UpdateStart();
            DoStartCache();
        }
        /// <summary>
        /// Starts the screen download with cancel option.
        /// </summary>
        protected void StartScreenDownloadCancel()
        {
            Debug.LogError("StartScreenDownloadCancel ");
            //			ScreenEvents.GetInstance ().RaiseChangeToPerspective (Perspective.PROGRESS_WITH_CANCEL);
            WindowControlListScript.Instance.WindowInAppGUI.UpdateDone();
            DoStartCache();

        }

        void onComplete()
        {
			//Salva pois alterou dados relacionados em URL
			AppRoot.deviceFileInfo.Save () ;
            AppRoot.deviceFileInfo.populateIndexedBundles() ;
            WindowControlListScript.Instance.WindowInAppGUI.UpdateDone() ;
            RaiseComplete() ;
        }

        void onProgress(float progress)
        {
            //o melhor era remover os listeners
            RaiseProgress(progress);
        }

        /// <summary>
        /// Ons the item complete download.
        /// </summary>
        /// <param name="urlToSave">URL to save.</param>
        void onItemComplete(UrlInfoVO urlToSave, WWW www)
        {
            if (AppRoot.deviceFileInfo != null && AppRoot.deviceFileInfo.isComponentIsReady())
            {

                //verifica a tipagem
                Debug.LogWarning("[DCF] file type finished:" + ARMFileManager.GetExtentionOfFile(urlToSave.local_url));

                string extension = ARMFileManager.GetExtentionOfFile(urlToSave.local_url);

                if (extension == ".zip")
                {
                    // Debug.LogError ("[CDF] Achei um zip");
                    UrlHelper.UnzipFolder(urlToSave);
                }


                // if (extension == InAppVuforiaVideoPresentation.VIDEO_TYPE_M4V || extension == InAppVuforiaVideoPresentation.VIDEO_TYPE_MP4 || extension == InAppVuforiaVideoPresentation.VIDEO_TYPE_3GP) {
                //         string streamingPath = System.IO.Path.Combine (Application.streamingAssetsPath, urlToSave.local_url);
                //         if (!ARMFileManager.Exitst (streamingPath)) {
                //                 ARMFileManager.CreateFolderRecursivelyForPath (ARMFileManager.RemoveLastPathNode (streamingPath));
                //                 System.IO.File.Move (BundleHelper.GetPersistentPath (urlToSave.local_url), streamingPath);
                //         }
                // }

                //o tratamento de se é mp3 coloquei no bulk pois se colocar aqui trava pois come muito processamento

                //TODO: CheckSUM

                ReturnDataVO resultCommit = AppRoot.deviceFileInfo.commit(urlToSave);

				//Salvando após mudar info de URL
//				AppRoot.deviceFileInfo.Save () ;
                if (!resultCommit.success)
                {
                    Debug.LogError("[DCF] DevicecacheFileManager . onItemComplete commitERROR " + resultCommit.message);
                }
                return;
            }
            //Debug.LogWarning ("D DevicecacheFileManager . onItemComplete ERROR . Erro ao atualizar info de URL pois não foi atribuido o componente. Verificar a referencia no Stage.");
        }
        /// <summary>
        /// Gets the folder to track.
        /// </summary>
        /// <returns>The folder to track.</returns>
        /// <param name="id">Identifier.</param>

        protected void DoStartCache()
        {
            DoStartCacheDownload(_revisionToCache.bundles);
        }

        public void DoStartCacheDownload(IEnumerable<BundleVO> bundleList)
        {
            //Debug.LogWarning ("[!!!!] DeviceCacheFileManager . DoStartCache ~ total:" + _revisionToCache.bundles.Length);
            BulkBundleDownloader.AddCompleteEventhandlerStatic(onComplete);
            BulkBundleDownloader.AddProgressEventhandlerStatic(onProgress);


            //listener do evento de item de download completo e ligar com onItemComplete
            BulkBundleDownloader.AddFileDownloadCompleteEventHandler(onItemComplete);
            BulkBundleDownloader.addBundle(bundleList);

            BulkBundleDownloader.startQueue();
        }


        public override ReturnResultVO Delete(ResultRevisionVO revision)
        {
            return Delete(revision.bundles);
        }

        /// <summary>
        /// Delete the files of the specified bundle.
        /// </summary>
        /// <param name="bundle">Bundle.</param>
        public override ReturnResultVO Delete(BundleVO bundle)
        {
            ReturnResultVO result = Delete(bundle.files);
            if (result.success == false)
            {
                return result;
            }

            string streamingPath = System.IO.Path.Combine(Application.streamingAssetsPath, BundleHelper.GetBundleLocalPath(bundle.context, bundle.id));

            if (ARMFileManager.Exists(streamingPath))
            {
                ARMFileManager.DeleteFolder(streamingPath);
            }

            ARMFileManager.DeleteFolder(BundleHelper.GetPersistentPath(BundleHelper.GetBundleLocalPath(bundle.context, bundle.id)));
            return result;
        }
        /// <summary>
        /// Delete the specified bundles.
        /// </summary>
        /// <param name="bundles">Bundles.</param>
        public override ReturnResultVO Delete(BundleVO[] bundles)
        {

            //                      return DeleteGenericCacheList<BundleVO> ( bundles );

            ReturnResultVO result = new ReturnResultVO();
            result.success = true;
            foreach (BundleVO u in bundles)
            {
                ReturnResultVO r = Delete(u);
                if (!r.success == false)
                {
                    result.success = r.success;
                }
            }
            return result;
        }
        public override ReturnResultVO Delete(FileVO file)
        {
            return Delete(file.urls);
        }
        public override ReturnResultVO Delete(FileVO[] files)
        {

            //                      return DeleteGenericCacheList<FileVO> ( files );
            ReturnResultVO result = new ReturnResultVO();
            result.success = true;
            foreach (FileVO u in files)
            {
                ReturnResultVO r = Delete(u);
                if (r.success == false)
                {
                    result.success = r.success;
                }
            }
            return result;
        }
        /// <summary>
        /// Delete the specified url.
        /// </summary>
        /// <param name="url">URL.</param>
        public override ReturnResultVO Delete(UrlInfoVO url)
        {
            //                      UrlInfoVO data = (UrlInfoVO) url ;
            ReturnResultVO result = new ReturnResultVO();
            UrlHelper.DeleteZippedFile(url);

            result.success = ARMFileManager.DeleteFile(BundleHelper.GetPersistentPath(url.local_url));
            return result;
        }
        public override ReturnResultVO Delete(UrlInfoVO[] urls)
        {
            ReturnResultVO result = new ReturnResultVO();
            result.success = true;
            foreach (UrlInfoVO u in urls)
            {
                ReturnResultVO r = Delete(u);
                if (r.success == false)
                {
                    result.success = r.success;
                }
            }

            return result;
        }

    }

}
