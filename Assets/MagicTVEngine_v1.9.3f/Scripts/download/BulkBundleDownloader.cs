using System;
using UnityEngine;
using System.Collections;
using ARM.utils.download;
using MagicTV.vo;
using ARM.abstracts;
using System.Collections.Generic;
using System.Linq;
using MagicTV.utils;
using MagicTV.globals;
using ARM.utils.io;
using MagicTV.globals.events;
using ARM.utils.cron;
using MagicTV.abstracts;

/// <summary>
/// @author: Alan Lucian
/// @version 1.0
/// 
/// 
/// @author: Renato Miawaki
/// @version 1.1
/// 
/// 
/// @author: Leandro Leal
/// @version 1.2
/// 
/// 
/// @author: Renato Miawaki
/// @version 1.2.1
/// Arrumada a questão de zip filtrar por qualidade
/// 
/// Bulk bundle downloader.
/// </summary>
namespace MagicTV.download
{
	public class BulkBundleDownloader :  ProgressEventsAbstract
	{

        #region customEvents
		public delegate void  OnFileDonwloadCompleteEventHandler (UrlInfoVO vo,WWW www);
		protected OnFileDonwloadCompleteEventHandler onFileDownloadComplete;

		public static void AddFileDownloadCompleteEventHandler (OnFileDonwloadCompleteEventHandler eventHandler)
		{
			InstanceBulkBundleDownloader.onFileDownloadComplete += eventHandler;
		}
        #endregion



        #region gerenciamento de bundle para  download
		protected List<BundleVO> _bundles;
		protected int _currentBundle;


		protected float _bulkTotalProgress = 0f;

		public static void addBundle (IEnumerable<BundleVO> bundleList)
		{
			if (bundleList == null) {
				//Debug.LogWarning (" BB Downloader: Download Bundle List  is null ");
				return;
			}
			InstanceBulkBundleDownloader._bundles.AddRange (bundleList);
		}

		public static void addBundle (BundleVO bundle)
		{

			if (bundle == null) {
				//Debug.LogWarning (" BB Downloader: Download Bundle  is null ");
				return;
			}

			InstanceBulkBundleDownloader._bundles.Add (bundle);

		}

		protected List<BundleDownloadItem> _itensToDownload;

		List<BundleDownloadItem> filterBundleToFileList (IEnumerable<BundleVO> bundleList)
		{


			//TODO: tirar daqui a questao de ser só ZIP

			//tirar esse Where para voltar a baixar tudo;
			// float bundleSize = bundleList.SelectMany (x => x.files).Where (f => f.type == "tracking.zip").SelectMany (h => h.urls).Sum (x => x.size);

			// float bundleSize = bundleList.SelectMany (x => x.files).SelectMany (h => h.urls).Sum (x => x.size);
			float bundleSize = 0;
			List<BundleDownloadItem> downloadList = new List<BundleDownloadItem> ();

			foreach (BundleVO bundle in bundleList) {
				foreach (FileVO file in bundle.files) {
					//Precisa criar um processo separado ssó para os zips
					UrlInfoVO[] urlZips = file.getByExtension (".zip");
//					Debug.LogError ("Encontrou " + urlZips.Length + " ZIPS");
					foreach (UrlInfoVO urlZipItem in urlZips) {
						bundleSize += urlZipItem.size;
						BundleDownloadItem bundleDownloadItemZip = new BundleDownloadItem {
							fileDestination = BundleHelper.GetFileDownloadPath(bundle, urlZipItem ),
							fileWeight = urlZipItem.size / 1,
							UrlInfoVO = urlZipItem
						};
						//adiciona cada zip na lista de download
						downloadList.Add (bundleDownloadItemZip);
					}
					UrlInfoVO filteredByQualityUrl = BenchmarkAbstract.Instance.GetCurrentQuality ().FilterFilesByQuality (file);
					if (filteredByQualityUrl != null) {
						bundleSize += filteredByQualityUrl.size;
						BundleDownloadItem bundleDownloadItem = new BundleDownloadItem {
							fileDestination = BundleHelper.GetFileDownloadPath(bundle, filteredByQualityUrl ),
							fileWeight = filteredByQualityUrl.size / 1,
							UrlInfoVO = filteredByQualityUrl
							
						};
						downloadList.Add (bundleDownloadItem);
					}

//					Debug.LogWarning ("BULKBUNDLE bundle" + bundleSize);
				}
			}
//			Debug.LogError ("Itens para download " + downloadList.Count);
			for (int i = 0; i < downloadList.Count; i++) {
				downloadList [i].fileWeight = downloadList [i].UrlInfoVO.size / bundleSize;
			}
			return downloadList;
			//TODO:Fazer isso do modo tradicional, loop e etc
//			List<BundleDownloadItem> downloadList = (
//                        from b in bundleList
//                        from f in b.files
//                        from u in f.urls
//                        select new BundleDownloadItem {
//                                fileDestination = BundleHelper.GetFileDownloadPath(b, u ),
//                                fileWeight = u.size / bundleSize,
//                                UrlInfoVO = u
//
//                        }
//                        ).ToList ();

		}

        #endregion

        #region donwload system


		public static void startQueue ()
		{
			InstanceBulkBundleDownloader.doStartQueue ();
		}

		protected void doStartQueue ()
		{


			InstanceGenericDownload.AddDownloadCompleteEventhandler (onDonwloadComplete);
			InstanceGenericDownload.AddProgressEventhandler (onProgressEventHandler);
			InstanceGenericDownload.AddErrorEventhandler (onErrorEventHandler);
			_itensToDownload = filterBundleToFileList (_bundles);
						
			_cancelBulk = false;
//
//						//Debug.Log (_itensToDownload[0].fileDestination);


			_bulkTotalProgress = 0f;
			downloadItem ();
		}

		protected void downloadItem ()
		{

			if (_itensToDownload.Count == 0) {
				RaiseCompleteAndTotalProgress ();
				Destroy (gameObject);
				GC.Collect ();
				GC.WaitForPendingFinalizers ();
				return;
			}


			//Debug.Log (string.Format ("BB Downloader downloadItem  origin:{0} \n fileWeight{1}: ", _itensToDownload.First ().UrlInfoVO.url, _itensToDownload.First ().fileWeight));


			InstanceGenericDownload.weight = _itensToDownload.First ().fileWeight;

			InstanceGenericDownload.Load (_itensToDownload.First ().UrlInfoVO.url, AppRoot.TOTAL_DOWNLOAD_ATTEMPTS_ON_ERROR);

		}

        #endregion

		static bool _cancelBulk;

		public static void Cancel ()
		{
			Debug.LogError (" Cancenlar ");
			_cancelBulk = true;
			InstanceGenericDownload.Dispose ();
			EasyTimer.SetTimeout (InstanceBulkBundleDownloader.Reset, 600);
						
		}
		protected void Reset ()
		{

			_bundles = new List<BundleVO> ();
			_currentBundle = 0;
			_bulkTotalProgress = 0f;
						
						
		}

		public override void prepare ()
		{


			//						throw new System.NotImplementedException ();

		}



		public static int alert_i = 0;
		public static int alert_a = 0;


        #region EventsHandler

		void onErrorEventHandler (string message)
		{
			Debug.LogError (string.Format ("  BB Downloader ERROR -  {0}", message));
			RaiseError (message);
		}


		void onDonwloadComplete (WWW request)
		{
			if (_cancelBulk) {
//								dispatchItemComplete (request);
				return;
			}
			if (request.error != null) {
//								Debug.LogError ("BulkBandleDonwloader . onDownloadComplete - Erro : " + request.error);
				if(alert_i == alert_a){
					alert_a = 30 + alert_a;
					Debug.LogError ("Erro ao fazer download. Verifique sua internet. i=" + alert_i + " == a=" + alert_a);
					AppRootEvents.GetInstance ().GetScreen ().SendAlert ("Erro ao fazer download. Verifique sua internet.");
				}
				alert_i++;
				dispatchItemComplete (request);
				return;
			}
			//Debug.Log (" BB Downloader . onDonwloadComplete ");
			string filePath = BundleHelper.GetPersistentPath (_itensToDownload.First ().fileDestination);
			if (ARMFileManager.Exists (filePath)) {
				ARMFileManager.DeleteFile (filePath);
			}
			ARM.utils.io.ARMFileManager.WriteBites (filePath, request.bytes);
			UrlInfoVO urlInfoVO = _itensToDownload.First ().UrlInfoVO;
			string s = _itensToDownload.First ().fileDestination;
			urlInfoVO.local_url = _itensToDownload.First ().fileDestination;
			urlInfoVO.status = UrlInfoConstants.STATUS_FILE_CACHED;
			//processo caso a extensão seja mp3
			string extension = ARMFileManager.GetExtentionOfFile (urlInfoVO.local_url);
			//tratamento para mp3, pois mp3 é horrivel para o aplicativo
			if (extension == ".mp3") {
				//AudioClip audio = new AudioClip ();
				UrlHelper.ConvertMp3ToWav (urlInfoVO, request);
			}
			//Debug.Log (urlInfoVO);
			if (onFileDownloadComplete != null) {
				onFileDownloadComplete (urlInfoVO, request);
			}

			dispatchItemComplete (request);
		}

		void dispatchItemComplete (WWW request)
		{
			_bulkTotalProgress += InstanceGenericDownload.GetCurrentProgressWithWeight ();
			InstanceGenericDownload.Dispose ();
			if (_cancelBulk) {
				return;
			}
			_itensToDownload.RemoveAt (0);
			
			request.Dispose ();

						
								
			
			downloadItem ();
			
			GC.Collect ();
			GC.WaitForPendingFinalizers ();
		}

		void onProgressEventHandler (float progress)
		{


			float data = _bulkTotalProgress + InstanceGenericDownload.GetCurrentProgressWithWeight ();

//					//Debug.Log ( string.Format("BB Downloader onProgressEventHandler  _bulkTotalProgress:{0} " , data  ));

			RaiseProgress (data);
		}
        #endregion


        #region Bulk Instance and GO
		private const string BULK_GAMEOBJECT_NAME = "_BulkBundleDownloaderGO";
		private static GameObject _currentBulkBundleDownloaderGO;

		protected static GameObject BulkBundleDownloaderGO {
			get {
				if (_currentBulkBundleDownloaderGO == null) {
					_currentBulkBundleDownloaderGO = GameObject.Find (BULK_GAMEOBJECT_NAME);
					if (_currentBulkBundleDownloaderGO != null)
						GameObject.DestroyImmediate (_currentBulkBundleDownloaderGO.gameObject);
					_currentBulkBundleDownloaderGO = new GameObject (BULK_GAMEOBJECT_NAME);
					_currentBulkBundleDownloaderGO.transform.parent = null;
					InstanceBulkBundleDownloader = _currentBulkBundleDownloaderGO.AddComponent<BulkBundleDownloader> ();
					InstanceBulkBundleDownloader.Reset ();
				}
				return _currentBulkBundleDownloaderGO;
			}
		}

		protected static BulkBundleDownloader _instanceBulkBundleDownloader;

		protected static BulkBundleDownloader InstanceBulkBundleDownloader {
			get {
				if (_instanceBulkBundleDownloader == null) {
					_instanceBulkBundleDownloader = BulkBundleDownloaderGO.GetComponent<BulkBundleDownloader> ();
				}
				return _instanceBulkBundleDownloader;
			}
			set { _instanceBulkBundleDownloader = value; }
		}
        #endregion

        #region GenericDownloader Instance and GO
		private const string DOWNLOADER_GAMEOBJECT_NAME = "_GenericDownloadManagerGO";
		protected static GameObject _currentGerericDonwloadManagerGO;

		protected static GameObject GenericDownloadManagerGO {
			get {
				if (_currentGerericDonwloadManagerGO == null) {
					_currentGerericDonwloadManagerGO = GameObject.Find (DOWNLOADER_GAMEOBJECT_NAME);
					if (_currentGerericDonwloadManagerGO != null)
						DestroyImmediate (_currentGerericDonwloadManagerGO.gameObject);

					_currentGerericDonwloadManagerGO = new GameObject (DOWNLOADER_GAMEOBJECT_NAME);
					_currentGerericDonwloadManagerGO.transform.parent = BulkBundleDownloaderGO.gameObject.transform;
					InstanceGenericDownload = _currentGerericDonwloadManagerGO.AddComponent<GenericDownloadManager> ();

				}
				return _currentGerericDonwloadManagerGO;
			}
		}

		protected static GenericDownloadManager _instanceGenericDownload;

		protected static GenericDownloadManager InstanceGenericDownload {
			get {
				if (_instanceGenericDownload == null) {
					_instanceGenericDownload = GenericDownloadManagerGO.GetComponent<GenericDownloadManager> ();
				}
				return _instanceGenericDownload;
			}
			set {
				_instanceGenericDownload = value;
			}
		}
        #endregion

        #region Public Events
		public static void AddErrorEventhandlerStatic (OnErrorEventHandler eventHandler)
		{
			InstanceBulkBundleDownloader.AddErrorEventhandler (eventHandler);
		}
		public static void AddCompleteEventhandlerStatic (OnCompleteEventHandler eventHandler)
		{
			InstanceBulkBundleDownloader.AddCompleteEventhandler (eventHandler);
		}
		public static void AddProgressEventhandlerStatic (OnProgressEventHandler enventHandler)
		{
			InstanceBulkBundleDownloader.AddProgressEventhandler (enventHandler);
		}
        #endregion



	}
}