﻿using UnityEngine;
using System.Collections;

using ARM.utils.io;

using System.Linq;
using System.Collections.Generic;

using MagicTV.vo;

/// <summary>
/// URL helper.
/// Methods of MagicTV project, utils and relected to URL and files
/// 
/// @author: Renato Miawaki
/// 
/// </summary>
using ARM.utils.audio;


namespace MagicTV.utils
{
		public class UrlHelper
		{
				/// <summary>
				/// Deletes the zipped file. And the unziped files of this zip
				/// </summary>
				/// <param name="url">URL.</param>
				public static void DeleteZippedFile (UrlInfoVO url)
				{
						
						if (ARMFileManager.GetExtentionOfFile (url.local_url) == ".zip") {
								//se é zip é track???
								//atualmente só confere se é um zip referente a track, mas futuramente haverão outros tipos de arquivos zipados
								string trackFolder = UrlHelper.GetFolderToTrack (url);
								if (ARMFileManager.Exists (trackFolder)) {
										//é zip e ele está lá no folder de track, deleta o folder todo
										ARMFileManager.DeleteFolder (trackFolder);
								}
						}
				}
				/// <summary>
				/// Return folder for unzip tracks on system
				/// </summary>
				/// <returns>String reperent The folder local url to unzip track.</returns>
				/// <param name="file">File.</param>
				public static string GetFolderToTrack (UrlInfoVO file = null)
				{
						string folder = ARMFileManager.GetPersitentDataPath () + "/tracks/";
						if (file == null) {
								return folder;
						}
						return folder + file.id + "/";
				}
				/// <summary>
				/// Unzips the folder and decids where it needs to unzip
				/// </summary>
				/// <param name="url">URL.</param>
				public static void UnzipFolder (UrlInfoVO url)
				{
						//Debug.LogWarning ("UrlHelper . UnzipFolder : local file " + url.local_url);
						//Debug.LogWarning ("UrlHelper . UnzipFolder : FOLDER to unzip : " + UrlHelper.GetFolderToTrack (url));

						//atualmente está interpretando que se é zip é sempre de track, mas basta tratar isso futuramente
						ARMFileManager.UncompressFile (BundleHelper.GetPersistentPath (url.local_url), UrlHelper.GetFolderToTrack (url));
				}
				/// <summary>
				/// Converts the mp3 to wav e deleta o mp3 enviado
				/// </summary>
				/// <param name="url">URL.</param>
				/// <param name="www">Www.</param>
				public static void ConvertMp3ToWav (UrlInfoVO url, WWW www)
				{
						//deleta o mp3 recebido
						ARMFileManager.DeleteFile (BundleHelper.GetPersistentPath (url.local_url));
						//renomeia para .wav
						url.local_url = url.local_url.Replace (".mp3", ".wav");
						SavWav.Save (BundleHelper.GetPersistentPath (url.local_url), www.GetAudioClip (false));

				}

		}
}