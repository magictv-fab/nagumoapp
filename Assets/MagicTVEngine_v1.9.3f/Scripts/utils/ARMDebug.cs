﻿#define DEBUG_LOG_ERROR
#define DEBUG_LOG_WARN
#define DEBUG_LOG_INFO
#define DEBUG_LOG_TIME

using System;
using UnityEngine;

public static class ARMDebug  {

	[System.Diagnostics.Conditional("DEBUG_LOG_INFO")]
	public static void Log ( object o ) {
		//Debug.Log ( o );
	}

	[System.Diagnostics.Conditional("DEBUG_LOG_WARN")]	
	public static void LogWarning ( object o ) {
		//Debug.LogWarning ( o );
	}

	[System.Diagnostics.Conditional("DEBUG_LOG_ERROR")]
	public static void LogError ( object o ) {
		//Debug.LogError ( o );
	}



	public static long startTime = 0;
	public static long lastTotal = 0;
	public static long getCurrentMiliseconds(){
		return System.DateTime.Now.Ticks / 10;

	}
	public static void ResetCount(){
		if(startTime == 0){
			startTime = getCurrentMiliseconds() ;
		}
	}
	[System.Diagnostics.Conditional("DEBUG_LOG_TIME")]
	public static void LogTime (string logText)
	{
        return;
		ResetCount ();
		long total = ( getCurrentMiliseconds() - startTime) / 1000;
			long subTotal = (total - lastTotal);
			
			lastTotal = total;
			// return total;
			Debug.LogError ( string.Format( "TIME INTERVAL [{0}-{1}] : {2} ", subTotal, total, logText ) );
	}
}
