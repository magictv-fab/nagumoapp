﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @autor: Renato Seiji Miawaki
/// 
/// string using Pipe to vector3.
/// exemple:
/// 1|1|1
/// </summary>
public static class PipeToVector3
{
	/// <summary>
	/// Parse the specified string_with_pipe to Vector3.
	/// </summary>
	/// <param name="string_with_pipe">String_with_pipe.</param>
	public static Vector3? Parse (string string_with_pipe)
	{
		if (string_with_pipe != "" && string_with_pipe != null) {
			char[] splitchar = { '|' };
			string[] position_values = string_with_pipe.Split (splitchar);
			if (position_values.Length >= 3) {
				return new Vector3 (float.Parse (position_values [0]), float.Parse (position_values [1]), float.Parse (position_values [2]));
			}
		}
		return null;
	}
}
