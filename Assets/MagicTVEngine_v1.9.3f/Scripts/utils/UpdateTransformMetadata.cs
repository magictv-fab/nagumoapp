﻿using UnityEngine;
using MagicTV.abstracts;
using MagicTV.globals.events;
using MagicTV.update.comm;

public class UpdateTransformMetadata : MonoBehaviour
{


		

		public int bundle_id;
		// Use this for initialization

		public Rect buttonSize = new Rect (200, 200, 150, 150);

		public GameObject objetoToSendTransform;
		public string result;
		protected bool _enviando;
    void setBundleID(int value)
    {
		bundle_id = value;
    }
    void setInAppGO(InAppAbstract value)
    {
        objetoToSendTransform = value.gameObject;
    }
		void Start ()
		{
				//reset bundle pra evitar que envie para o bundle id errado, sete na hora que for usar
				
				this.bundle_id = 0;

				TransformEvents.AddOnBundleIDEventHandler(setBundleID);
				TransformEvents.AddOnInAppEventHandler(setInAppGO);
		}
		



		// Update is called once per frame
		void ___OnGUI ()
		{

				if (this._enviando) {
						GUI.Box (buttonSize, "enviando");
						return;
				}
				if (GUI.Button (buttonSize, "enviar bundle " + this.bundle_id)) {

						this.sendPosition ();
				}
		}

		void completeSend (bool success)
		{
				ServerCommunication.Instance.RemoveUpdateMetadataEventHandler (completeSend);
				_enviando = false;
				result = "Erro ao enviar, veja o log.";
				if (success) {
						result = "enviado com sucesso";
						
				}
				AppRootEvents.GetInstance ().GetScreen ().SendAlert (result);
				

		}

		public void sendPosition ()
		{
				result = "";
				if (! (this.bundle_id > 0)) {
						Debug.LogError ("Vai enviar sem bundle id?????");
						
						AppRootEvents.GetInstance ().GetScreen ().SendAlert ("Vai enviar sem bundle id?????");
						return;
				}
				if (objetoToSendTransform == null) {
						Debug.LogError ("Precisa escolher um GameObject pra dar send");
						AppRootEvents.GetInstance ().GetScreen ().SendAlert ("Precisa escolher um GameObject pra dar send");
						return;
				}
				//enviar dado
				_enviando = true;
				ServerCommunication.Instance.AddUpdateMetadataEventHandler (completeSend);
				ServerCommunication.Instance.sendTransformInfo (this.objetoToSendTransform, this.bundle_id.ToString ());

		}
}
