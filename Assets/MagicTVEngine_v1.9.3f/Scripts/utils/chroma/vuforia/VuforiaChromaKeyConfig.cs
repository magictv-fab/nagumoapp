﻿using UnityEngine;
using System.Collections;
using MagicTV.vo;
/// <summary>
/// MMT blue chroma key config.
/// configurador de chromakey para quem quiser configurar chormakey baseado em meta datas
/// </summary>
namespace MagicTV.utils.chroma.vuforia
{
		public class VuforiaChromaKeyConfig
		{
				/// <summary>
				/// Configs the chroma.
				/// </summary>
				/// <param name="material">Material.</param>
				/// <param name="metas">Metas.</param>
				public static void configChroma (Material material, MetadataVO[] metas)
				{

						//Chroma
						//Recorte
						//Sensibilidade
						string chroma_color = utils.BundleHelper.GetMetadataSingleValueByKey ("ChromaColor", metas);

						if (chroma_color == "Green") {
								//setando o verde padrao
								material.SetColor ("_ColorChroma", Color.green);
								material.SetColor ("_Color", Color.green);

						
						} else if (chroma_color == "Blue") {
								material.SetColor ("_ColorChroma", Color.blue);
								material.SetColor ("_Color", Color.blue);

						} else {
								//pega char por char caso tenha enviado a cor específica separada por |
								char[] c = new char[]{'|'};
								string[] values = chroma_color.Split (c);
								if (values.Length == 4) {
										material.SetVector ("_Color", new Vector4 (
												float.Parse (values [0]), 
												float.Parse (values [1]), 
												float.Parse (values [2]), 
												float.Parse (values [3])
										));
										material.SetVector ("_ColorChroma", new Vector4 (
						float.Parse (values [0]), 
						float.Parse (values [1]), 
						float.Parse (values [2]), 
						float.Parse (values [3])
										));
								}
						}
						string chroma_Recorte = utils.BundleHelper.GetMetadataSingleValueByKey (GetLabelFloat1 (), metas);
						//_CutOff
						if (chroma_Recorte != null) {
								float chroma_chroma_Recorte_value;
								if (float.TryParse (chroma_Recorte, out chroma_chroma_Recorte_value)) {
										changeRecorte (material, chroma_chroma_Recorte_value);
								}
						} else {
								//valor padrao
								changeRecorte (material, 0.38f);
						}
						//_Sens
						string chroma_Sensibilidade = utils.BundleHelper.GetMetadataSingleValueByKey (GetLabelFloat2 (), metas);
						if (chroma_Sensibilidade != null) {
								float chroma_Sensibilidade_value;
								if (float.TryParse (chroma_Sensibilidade, out chroma_Sensibilidade_value)) {
										changeSensibilidade (material, chroma_Sensibilidade_value);
								}
						} else {
								changeSensibilidade (material, 0.44f);
						}

			
						string chroma_SensibilidadeFina = utils.BundleHelper.GetMetadataSingleValueByKey (GetLabelFloat4 (), metas);
						if (chroma_SensibilidadeFina != null) {
								float chroma_Sensibilidade_Fina_value;
								if (float.TryParse (chroma_SensibilidadeFina, out chroma_Sensibilidade_Fina_value)) {
										changeSensibilidadeFina (material, chroma_Sensibilidade_Fina_value);
								}
						} else {
								changeSensibilidadeFina (material, -0.071428f);
						}
						string chroma_Smooth = utils.BundleHelper.GetMetadataSingleValueByKey (GetLabelFloat3 (), metas);
						if (chroma_Smooth != null) {
								float chroma_Smooth_value;
								if (float.TryParse (chroma_Smooth, out chroma_Smooth_value)) {
										changeSmooth (material, chroma_Smooth_value);
								}
						} else {
								changeSmooth (material, 0.11f);
						}
				}
				/// <summary>
				/// Changes the sensibilidade fina.
				/// </summary>
				/// <param name="material">Material.</param>
				/// <param name="value">Value.</param>
				public static void changeSensibilidadeFina (Material material, float value)
				{
						material.SetFloat (GetLabelFloat4 (), value);
				}
				public static void addSesibilidadeFina (Material material, float value)
				{
						float current = material.GetFloat (GetLabelFloat4 ());
						changeSensibilidadeFina (material, current + value);
				}
				public static float getSensibilidadeFina (Material material)
				{
						return material.GetFloat (GetLabelFloat4 ());
				}
				public static string GetLabelFloat4 ()
				{
						return "_SensChroma";
				}

				/// <summary>
				/// Changes the smooth.
				/// </summary>
				/// <param name="material">Material.</param>
				/// <param name="value">Value.</param>
				public static void changeSmooth (Material material, float value)
				{
						material.SetFloat (GetLabelFloat3 (), value);
				}
				public static void addSmooth (Material material, float value)
				{
						float current = material.GetFloat (GetLabelFloat3 ());
						changeSmooth (material, current + value);
				}
				public static float getSmooth (Material material)
				{
						return material.GetFloat (GetLabelFloat3 ());
				}
				public static string GetLabelFloat3 ()
				{
						return "_Smooth";
				}
		
				/// <summary>
				/// Changes the recorte.
				/// </summary>
				/// <param name="material">Material.</param>
				/// <param name="value">Value.</param>
				public static void changeRecorte (Material material, float value)
				{
						material.SetFloat (GetLabelFloat1 (), value);
				}
				public static void addRecorte (Material material, float value)
				{
						float current = material.GetFloat (GetLabelFloat1 ());
						changeRecorte (material, current + value);
				}
				public static float getRecorte (Material material)
				{
						return material.GetFloat (GetLabelFloat1 ());
				}
				public static string GetLabelFloat1 ()
				{
						return "_Cutoff";
				}
				/// <summary>
				/// Changes the sensibilidade.
				/// </summary>
				/// <param name="material">Material.</param>
				/// <param name="value">Value.</param>
				public static void changeSensibilidade (Material material, float value)
				{
						material.SetFloat (GetLabelFloat2 (), value);
				}
		
				public static void addSensibilidade (Material material, float value)
				{
						float current = material.GetFloat (GetLabelFloat2 ());
						changeSensibilidade (material, current + value);
				}
				
				public static float getSensibilidade (Material material)
				{
						return material.GetFloat (GetLabelFloat2 ());
				}
				
				public static string GetLabelFloat2 ()
				{
						return "_Sens";
				}


		}
}