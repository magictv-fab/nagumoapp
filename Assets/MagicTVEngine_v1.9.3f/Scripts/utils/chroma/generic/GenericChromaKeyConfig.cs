﻿using UnityEngine;
using System.Collections;
using MagicTV.vo;
/// <summary>
/// Generic chroma key config.
/// configurador de chromakey para quem quiser configurar chormakey baseado em meta datas livres
/// </summary>
using System;


namespace MagicTV.utils.chroma.generic
{
		public class GenericChromaKeyConfig
		{
				/// <summary>
				/// Configs the chroma.
				/// </summary>
				/// <param name="material">Material.</param>
				/// <param name="metas">Metas.</param>
				public static void configChroma (Material material, MetadataVO[] metas, string json)
				{

						
						string chroma_color = utils.BundleHelper.GetMetadataSingleValueByKey ("ChromaColor", metas);

						if (chroma_color == "Green") {
								//setando o verde padrao
								material.SetColor ("Chroma", Color.green);
						} else {
								//pega char por char caso tenha enviado a cor específica separada por |
								char[] c = new char[]{'|'};
								string[] values = chroma_color.Split (c);
								if (values.Length == 4) {
										material.SetVector ("Chroma", new Vector4 (
												float.Parse (values [0]), 
												float.Parse (values [1]), 
												float.Parse (values [2]), 
												float.Parse (values [3])
										));
								}
						}
						GenericChromaKeyConfig.setChromaConfigJson (json, material);
						
				}
				/// <summary>
				/// Sets the chroma config json.
				/// </summary>
				/// <param name="chromaConfigJson">Chroma config json.</param>
				public static void setChromaConfigJson (string chromaConfigJson, Material material)
				{
						try {
								KeyTypeValue[] keyValues = LitJson.JsonMapper.ToObject <KeyTypeValue[]> (chromaConfigJson);
								setChromaKeyValues (keyValues, material);
						} catch (Exception e) {
								//deu algum erro. não faz nada
								Debug.LogError ("GenericChromaKeyConfig . setChromaConfigJson " + e);
						}
				}
				/// <summary>
				/// Sets the chroma key values for many itens.
				/// </summary>
				/// <param name="keyValues">Key values.</param>
				public static void setChromaKeyValues (KeyTypeValue[] keyValues, Material material)
				{
						if (keyValues == null || keyValues.Length == 0) {
								return;
						}
						for (int i = 0; i < keyValues.Length; i++) {
								KeyTypeValue k = keyValues [i];
								setChromaKeyValue (k, material);
				
						}
			
				}
				/// <summary>
				/// Sets the chroma key value.
				/// </summary>
				/// <param name="k">K.</param>
				public static void setChromaKeyValue (KeyTypeValue k, Material material)
				{
						if (k == null) {
								return;
						}
						if (k.type == KeyTypeValue.TYPE_INT) {
								material.SetInt (k.key, k.getIntValue ());
								return;
						}
			
						if (k.type == KeyTypeValue.TYPE_FLOAT) {
								material.SetFloat (k.key, k.getValueFloat ());
								return;
						}
			
				}
		}
}