﻿using UnityEngine;
using System.Collections;

using ARM.utils.io;

using System.Linq;
using System.Collections.Generic;

using MagicTV.vo;

namespace MagicTV.utils
{
	public class BundleHelper
	{
		/// <summary>
		/// Gets the single URL info.
		/// TODO: Deveria levar em consideração a capacidade do device
		/// </summary>
		/// <returns>The single URL info.</returns>
		/// <param name="fVO">FileVO object values</param>
		public static UrlInfoVO getSingleURLInfo (FileVO fVO)
		{
			//TODO :  pegar a url mais indicada de acordo com  a qualidade que o Device suporta
			if (fVO.urls.Length <= 0) {
				return null;
			}
			return fVO.urls [0];

						
		}

		public static string GetPersistentPath (string path)
		{
			return string.Format ("{0}/{1}", ARMFileManager.GetPersitentDataPath (), path);
		}
		/// <summary>
		/// Gets the file download path.
		/// </summary>
		/// <returns>The file download path.</returns>
		/// <param name="bundleVO">Bundle V.</param>
		/// <param name="urlInfoVO">URL info V.</param>
		public static string GetFileDownloadPath (BundleVO  bundleVO, UrlInfoVO urlInfoVO)
		{

			return string.Format ("{0}/{1}", BundleHelper.GetBundleLocalPath (bundleVO.context, bundleVO.id), ARMFileManager.GetGetFileName (urlInfoVO.url));

		}
		/// <summary>
		/// Gets the prefab name by bundle.
		/// Modificar aqui caso mude a lógica de como encontrar o prefab a partir do bundle
		/// </summary>
		/// <returns>The prefab name by bundle.</returns>
		/// <param name="bundle">Bundle.</param>
		public static string GetPrefabNameByBundle (BundleVO bundle)
		{
			return BundleHelper.GetMetadataSingleValueByKey ("Template", bundle.metadata);
		}

		public static string GetBundleLocalPath (string bundleContext, string bundleId)
		{
			return string.Format ("{0}/{1}", bundleContext, bundleId);
		}
		/// <summary>
		/// Gets the metadata Array of string values by key.
		/// </summary>
		/// <returns>The metadata values by key.</returns>
		/// <param name="key">Key.</param>
		public static string[] GetMetadataValuesByKey (string key, MetadataVO[] metadata)
		{
			if (metadata != null && metadata.Length > 0) {
				
				string[] values = (from mt in metadata
				                   where mt.key == key
				                   select mt.value).ToArray ();
				return values;
			}
			return null;
		}
		/// <summary>
		/// Gets the metadata single value by key.
		/// </summary>
		/// <returns>The metadata single value by key.</returns>
		/// <param name="key">Key.</param>
		public static string GetMetadataSingleValueByKey (string key, MetadataVO[] metadata)
		{
			string[] values = GetMetadataValuesByKey (key, metadata);
			if (values != null && values.Length > 0) {
				return values [0];
			}
			return null;
		}

	}
}