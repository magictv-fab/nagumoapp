﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @autor: Renato Seiji Miawaki
/// 
/// string using Pipe to vector3.
/// exemple:
/// 1|1|1
/// </summary>
using System.Collections.Generic;


public static class PipeToArrayOfVector3
{
	/// <summary>
	/// Parse the specified string_with_pipe to Vector3 array.
	/// Retorna um array baseado em uma string
	/// 1|2|3|4|5|6|7
	/// o exemplo acima resulta em um array com 2 Vector3 de valores
	/// 1,2,3
	/// 4,5,6
	/// 
	/// o 6 e 7 são ignorados pois não chegaram a ter os 3 parametros
	/// </summary>
	/// <param name="string_with_pipe">String_with_pipe.</param>
	public static Vector3[] Parse (string string_with_pipe)
	{
		List<Vector3> vectors = new List<Vector3> ();
		if (string_with_pipe != "" && string_with_pipe != null) {
			char[] splitchar = { '|' };
			string[] position_values = string_with_pipe.Split (splitchar);

			int totalVectors = Mathf.FloorToInt (position_values.Length / 3);
			for (int i = 0; i < totalVectors; i++) {
				int initialIndex = i * 3;
				if (position_values.Length >= initialIndex + 3) {
					vectors.Add (new Vector3 (float.Parse (position_values [initialIndex]), float.Parse (position_values [initialIndex + 1]), float.Parse (position_values [initialIndex + 2])));
				}
			}
		}
		return vectors.ToArray ();
	}
}
