﻿using UnityEngine;
using System.Collections;

public class ToggleCamera : MonoBehaviour {

	public void cameraOn () {
		this.gameObject.SetActive(false);
	}

	public void cameraOff () {
		this.gameObject.SetActive(false);
	}
}
