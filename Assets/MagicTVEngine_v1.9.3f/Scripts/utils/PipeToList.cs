﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @autor: Renato Seiji Miawaki
/// 
/// string using Pipe to vector3.
/// exemple:
/// 1|1|1
/// </summary>
using System.Collections.Generic;


public static class PipeToList
{
	/// <summary>
	/// Parse the specified string_with_pipe.
	/// </summary>
	/// <param name="string_with_pipe">String_with_pipe.</param>
	public static List<string> Parse (string string_with_pipe)
	{
		List<string> parsedStrings = new List<string> ();
		if (string_with_pipe != "" && string_with_pipe != null) {
			char[] splitchar = { '|' };
			string[] string_values = string_with_pipe.Split (splitchar);
			foreach (string metadado in string_values) {
				parsedStrings.Add (metadado);
			}
		}
		return parsedStrings;
	}

	/// <summary>
	/// Esse método retorna uma lista de floats
	/// </summary>
	/// <returns>The returning floats.</returns>
	/// <param name="string_with_pipe">String_with_pipe.</param>
	public static List<float> ParseReturningFloats (string string_with_pipe)
	{
		List<float> parsedFloats = new List<float> ();
		if (string_with_pipe != "" && string_with_pipe != null) {
			char[] splitchar = { '|' };
			string[] string_values = string_with_pipe.Split (splitchar);
			foreach (string metadado in string_values) {
				parsedFloats.Add (float.Parse (metadado));
			}
		}
		return parsedFloats;
	}
	public static List<int> ParseReturningInts(string string_with_pipe){
		List<int> parseInts = new List<int> ();
		if (string_with_pipe != "" && string_with_pipe != null) {
			char[] splitchar = { '|' };
			string[] string_values = string_with_pipe.Split (splitchar);
			foreach (string metadado in string_values) {
				int temp;
				if(int.TryParse (metadado, out temp)){
					parseInts.Add (temp);
				}
			}
		}
		return parseInts;
	}
}