using UnityEngine;
using System.Collections;

namespace ARM.utils.request
{
		public class ARMRequestVO
		{
				
			#region request Config

				public bool UseUnityCache = false;
				public int UnityCacheVersion = 0;
				public string Url;
				/// <summary>
				/// numero de tentativas que essea requisicao vai ser executada antes de disparar o evento de erro
				/// </summary>
				public int TotalAttempts = 1;
				public WWWForm PostData;
				public float Weight = 1f ;
				public IRequestParameter MetaData;
			#endregion		

			#region RequesData ( after Start );
				public int CurrentAttempt ;
				public WWW Request;
			#endregion

				public ARMRequestVO (string url, IRequestParameter metaData)
				{
						this.Url = url;
						this.MetaData = metaData;
				}

				public ARMRequestVO (string url)
				{
						this.Url = url;

				}
		}

}
