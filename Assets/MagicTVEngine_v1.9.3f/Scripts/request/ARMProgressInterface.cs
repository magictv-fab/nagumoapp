using UnityEngine;
using System.Collections;
using ARM.abstracts ;



namespace ARM.interfaces
{
		

		public interface ARMProgressInterface<ReturnType, CommonEventHandler, ProgressEventHandler>
		{
				
			
		#region Complete 
				bool isComplete ();

				/// <summary>
				/// Adds the complete eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				void AddCompleteEventhandler (CommonEventHandler eventHandler);
				/// <summary>
				/// Removes the complete eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				void RemoveCompleteEventhandler (CommonEventHandler eventHandler);
				/// <summary>
				/// Raises the complete.
				/// </summary>
				/// <param name="var">Variable.</param>
				void RaiseComplete (ReturnType var);
		#endregion


		#region progress 
//				delegate void OnProgressEventHandler (float progress) ;
				/// <summary>
				/// Adds the progress eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				void AddProgressEventhandler (ProgressEventHandler eventHandler);
				/// <summary>
				/// Removes the progress eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				void RemoveProgressEventhandler (ProgressEventHandler eventHandler);
				
				
				void RaiseProgress (float progress);
				/// <summary>
				/// Gets the current progress.
				/// </summary>
				/// <returns>The current progress.</returns>
				float GetCurrentProgress ();
		#endregion

		#region error


				
				/// <summary>
				/// Adds the error eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				void AddErrorEventhandler (CommonEventHandler eventHandler);
				/// <summary>
				/// Removes the error eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				void RemoveErrorEventhandler (CommonEventHandler eventHandler);
				/// <summary>
				/// Raises the error.
				/// </summary>
				/// <param name="var">Variable.</param>
				void RaiseError (ReturnType var);
		#endregion
		}

}
