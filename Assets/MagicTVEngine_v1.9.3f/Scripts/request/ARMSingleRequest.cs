﻿using UnityEngine;
using System.Collections;

namespace ARM.utils.request
{
		public class ARMSingleRequest: MonoBehaviour
		{

				bool _isComplete;
				public ARMRequestEvents Events ;

				public string log_url;
				public float log_progress;

				protected bool _isPrepared ;

				bool _started = false ;

				public delegate void OnStartErrorEventHandler (string message) ;
				OnStartErrorEventHandler onStartError;


//				protected ARMRequestVO<VOMetaData> __vo ;
				public ARMRequestVO VO ;
//				{
//						get {
//
//								return __vo;
//						}
//						set {
//								if (_started) {
//										RaiseStartErrorEventHandler ("Trying to set Request VO when Request already started");
//										return;
//								}
//								__vo = value;
//						}
//				}


			#region Static Public Access

				protected static int _instanceID = 0;
				public static ARMSingleRequest GetNewInstance (ARMRequestVO requestVO)
				{
						ARMSingleRequest request = generateGameObjectWithComponent ();
						request.Init ();
						request.setData (requestVO);
						return request;
				}

//				public static ARMSingleRequest GetNewInstance (string url)
//				{
//						ARMSingleRequest request = generateGameObjectWithComponent ();
//						request.Init ();
//						request.setData (url);
//						return request;
//				}
			#endregion


		#region GameObject functions

				protected static ARMSingleRequest generateGameObjectWithComponent ()
				{
						GameObject go = new GameObject ("_ARMRequestGameObject_" + _instanceID++);
//						return go.AddComponent (typeof(T));
						return  go.AddComponent<ARMSingleRequest> ();

				}




				void Init ()
				{
						Events = new ARMRequestEvents ();
				}

				public void Load ()
				{
						VO.CurrentAttempt = 1;
						StartCoroutine (doLoad ());

				}

				/// <summary>
				/// Do the load.
				/// </summary>
				/// <returns>The load.</returns>
				/// <param name="requestUrl">Request URL.</param>
				IEnumerator doLoad ()
				{
						if (VO.UseUnityCache) {
								while (!Caching.ready) {
										yield return null;
								}
						}
						using (VO.Request = getWWWRequest()) {
								yield return VO.Request;
								this.errorCheck ();
								this.onCompleteCheck ();
						}
				}




				void Update ()
				{

						this.errorCheck ();
						if (!_isComplete && VO.Request != null) {

								//Debug.LogWarning (string.Format (" GO Downloader: PROGRESS:{0}  DONE:{1}", VO.Request.progress, VO.Request.isDone));
//								float X = VO.Request.progress * VO.Weight;
//								log_progress = X;
								Events.RaiseProgress (VO.Request.progress);


								this.onCompleteCheck ();

								return;
						}

				}

				/// <summary>
				/// Releases all resource used by the <see cref="ARM.utils.download.GameObjectDownloader"/> object.
				/// and Delete downloader object
				/// </summary>
				/// <remarks>Call <see cref="Dispose"/> when you are finished using the
				/// <see cref="ARM.utils.download.GameObjectDownloader"/>. The <see cref="Dispose"/> method leaves the
				/// <see cref="ARM.utils.download.GameObjectDownloader"/> in an unusable state. After calling <see cref="Dispose"/>,
				/// you must release all references to the <see cref="ARM.utils.download.GameObjectDownloader"/> so the garbage
				/// collector can reclaim the memory that the <see cref="ARM.utils.download.GameObjectDownloader"/> was occupying.</remarks>
				public void Dispose ()
				{
						VO.Request.Dispose ();
						Destroy (this.gameObject);

				}

		#endregion

		#region Request DATA

				public void SetWeight (float weight)
				{
						if (_started) {
								RaiseStartErrorEventHandler ("Trying to set postData when Request already started");
								return;
						}
						VO.Weight = weight;
				}

				public void SetPostData (WWWForm postData)
				{
						if (_started) {
								RaiseStartErrorEventHandler ("Trying to set postData when Request already started");
								return;
						}
						VO.PostData = postData;
				}

				public void setData (ARMRequestVO requestVO)
				{
						VO = requestVO;
						log_url = VO.Url;
				}



				protected WWW getWWWRequest ()
				{

						if (VO.UseUnityCache) {

								return WWW.LoadFromCacheOrDownload (VO.Url, VO.UnityCacheVersion);
						}

						if (VO.PostData != null) {
								return new WWW (VO.Url, VO.PostData);
						}

						return new WWW (VO.Url);

				}

				public bool IsComplete ()
				{
						return _isComplete;
				}

				public ARMRequestVO GetVO ()
				{
						return VO;
				}

		#endregion

		#region EVENTS
				void errorCheck ()
				{
						if (_isComplete) {
								return;
						}

						if (VO.Request != null && VO.Request.error != null) {
								if (VO.CurrentAttempt < VO.TotalAttempts) {

										VO.CurrentAttempt++;

										//Debug.LogWarning (string.Format ("WWW extra download attempt {0} from {1} \n\turl:{2}\n\terror:{3}", VO.CurrentAttempt, VO.TotalAttempts, VO.Url, VO.Request.error));

										StartCoroutine (doLoad ());
										return;
								}

								//Debug.LogError ("WWW download had an error:" + VO.Request.error + "\n When loading:" + VO.Url);
								ARMRequestVO n = VO;
								Events.RaiseError (n);
						}
				}

				void onCompleteCheck ()
				{
						if (!_isComplete && VO.Request.isDone) {
								_isComplete = VO.Request.isDone;
								Events.RaiseComplete (VO);
								this.Dispose ();
						}

				}

				protected void RaiseStartErrorEventHandler (string message)
				{
						if (onStartError != null) {
								onStartError (message);
						}
				}

				public void AddStartErrorEventHandler (OnStartErrorEventHandler eventHandler)
				{
						onStartError += eventHandler;
				}


				public void RemoveStartErrorEventHandler (OnStartErrorEventHandler eventHandler)
				{
						onStartError -= eventHandler;
				}
		#endregion
		}
}
