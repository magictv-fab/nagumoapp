using UnityEngine;
using System.Collections;
using ARM.abstracts ;
using ARM.interfaces;

namespace ARM.utils.request
{
		public delegate void OnRequestEventHandler (ARMRequestVO vo) ;
		public delegate void OnProgressEventHandler (float progress) ;

		public class ARMRequestEvents: ARMProgressInterface <ARMRequestVO ,OnRequestEventHandler, OnProgressEventHandler  >
		{
			#region Complete 

				/// <summary>
				/// The on request complete.
				/// </summary>
				private OnRequestEventHandler onRequestComplete;

				/// <summary>
				/// Raises the request complete.
				/// </summary>
				/// <param name="request">Request.</param>

				public void RaiseComplete (ARMRequestVO vo)
				{
						this._isComplete = true;
						if (onRequestComplete != null) {
								onRequestComplete (vo);
						}	
				}

				/// <summary>
				/// Adds the request complete eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				public void AddCompleteEventhandler (OnRequestEventHandler eventHandler)
				{
						onRequestComplete += eventHandler;	
				}


				/// <summary>
				/// Removes the request complete eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				public void RemoveCompleteEventhandler (OnRequestEventHandler eventHandler)
				{
						onRequestComplete -= eventHandler;	
				}

				public bool isComplete ()
				{
						return this._isComplete;
				}

			#endregion
			


			#region on Error
				/// <summary>
				/// The on request fail.
				/// </summary>
				private OnRequestEventHandler onRequestFail;

				public void RaiseError (ARMRequestVO  vo)
				{
						if (onRequestFail != null) {
								onRequestFail (vo);
						}	
				}
				/// <summary>
				/// Adds the error eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				public void AddErrorEventhandler (OnRequestEventHandler eventHandler)
				{
						onRequestFail += eventHandler;	
				}
				/// <summary>
				/// Removes the error eventhandler.
				/// </summary>
				/// <param name="eventHandler">Event handler.</param>
				public void RemoveErrorEventhandler (OnRequestEventHandler eventHandler)
				{
						onRequestFail -= eventHandler;	
				}
			#endregion

			#region Progress
				private OnProgressEventHandler onProgress;
		
				protected bool _isComplete ;

				/// <summary>
				/// O Peso que esse loading representa, por padrao é 1
				/// </summary>
				public float weight = 1f ;
				protected float _currentProgress = 0f ;
		
				
				/// <summary>
				/// retorna o progresso setado desconsiderando o peso
				/// </summary>
				/// <returns>The current progress.</returns>
				public float GetCurrentProgress ()
				{
						return this._currentProgress;
				}
				/// <summary>
				/// Retorna o progresso adicionando o peso
				/// </summary>
				/// <returns>The current progress with weight.</returns>
				public float GetCurrentProgressWithWeight ()
				{
			
						return this._currentProgress * this.weight;
			
				}

				public void RaiseProgress (float progress)
				{
						this._currentProgress = progress;
						if (onProgress != null) {
								onProgress (progress);
						}	
				}
		
				public void AddProgressEventhandler (OnProgressEventHandler eventHandler)
				{
						onProgress += eventHandler;	
				}
		
				public void RemoveProgressEventhandler (OnProgressEventHandler eventHandler)
				{
						onProgress -= eventHandler;	
				}
			#endregion
				

		}
}
