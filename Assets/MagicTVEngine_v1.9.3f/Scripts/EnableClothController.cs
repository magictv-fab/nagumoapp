﻿using UnityEngine;
using System.Collections;

public class EnableClothController : MonoBehaviour {
	public Cloth cloth;

    public void EnableCloth()
    {
		cloth.enabled = true;
	}
}
