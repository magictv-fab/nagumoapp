﻿using UnityEngine;
using System.Collections;

public class PlaySoundController : MonoBehaviour {
    public AudioSource audioSource;

    public void PlaySound()
    {
        audioSource.Play();
    }
}
