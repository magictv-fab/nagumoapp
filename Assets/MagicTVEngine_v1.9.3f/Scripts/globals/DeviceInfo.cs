using UnityEngine;
using System.Collections;
using MagicTV.abstracts ;
using MagicTV.abstracts.device ;
using MagicTV.vo ;
using MagicTV.vo.result ;

using MagicTV.globals ;

public class DeviceInfo : DeviceInfoAbstract
{

	public static DeviceInfo Instance;

	const string PLAYER_PREFS_REGISTERED_VAR = "notification_token";
	const string PLAYER_PREFS_CURRENT_REQUEST_VERSION_VAR = "current_request_version";
	void Start ()
	{
		Instance = this;
	}
	// Use this for initialization
	public override void prepare ()
	{

		Instance = this;

		AppRoot.deviceInfo = this;
		//Debug.LogWarning ("DeviceInfo . init ");

		//TODO: verificar o que precisa pra pegar as infos que podem ser consultadas
		this.RaiseComponentIsReady ();
	}
	/// <summary>
	/// Gets the user agent.
	/// Aqui determina se a requisição do servidor vai para android ou ios. Se confundir isso tudo da errado
	/// </summary>
	/// <returns>The user agent.</returns>
	public override string GetUserAgent ()
	{
		//retorna o UserAgent entre as duas opções em

		// AppRoot.USER_AGENT_TYPE_ANDROID : AppRoot.USER_AGENT_TYPE_ANDROID ;

		#if UNITY_ANDROID
			return AppRoot.USER_AGENT_TYPE_ANDROID ;
		#endif
		return AppRoot.USER_AGENT_TYPE_IOS;


	}
	protected string lastPing ;
	public override string getPin ()
	{
		if(lastPing == null || lastPing == ""){
			lastPing = PlayerPrefs.GetString ("pin");
		}
		return lastPing;
	}

	public override void setPin (string value)
	{
		PlayerPrefs.SetString ("pin", value);
		PlayerPrefs.Save ();
	}
	public override bool hasNotificationToken ()
	{
		return PlayerPrefs.HasKey (PLAYER_PREFS_REGISTERED_VAR);
	}
	public override string getNotificationToken ()
	{
		return PlayerPrefs.GetString (PLAYER_PREFS_REGISTERED_VAR);
	}
	public override void setNotificationToken (string token)
	{
		PlayerPrefs.SetString (PLAYER_PREFS_REGISTERED_VAR, token);
		PlayerPrefs.Save ();
	}
		
	public override int GetCurrentInfoVersionControlId ()
	{
		return PlayerPrefs.GetInt (PLAYER_PREFS_CURRENT_REQUEST_VERSION_VAR);
	}
	protected static int _lastInfoVersionControlIdToSave ;
	/// <summary>
	/// Sets the current info version control identifier to Save Letter
	/// Remember to call SaveCurrentInfoVersionControlId
	/// </summary>
	/// <param name="val">Value.</param>
	public override void SetCurrentInfoVersionControlId (int val)
	{
		_lastInfoVersionControlIdToSave = val;

	}
	/// <summary>
	/// Saves the current info version.
	/// Dont persist data when you set, becouse you need wait to confirm
	/// </summary>
	public override void SaveCurrentInfoVersionControlId ()
	{
		if (_lastInfoVersionControlIdToSave > 0) {
			PlayerPrefs.SetInt (PLAYER_PREFS_CURRENT_REQUEST_VERSION_VAR, _lastInfoVersionControlIdToSave);
			PlayerPrefs.Save ();
		}
	}
}
