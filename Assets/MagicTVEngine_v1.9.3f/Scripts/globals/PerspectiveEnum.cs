﻿using UnityEngine;
using System.Collections;

namespace MagicTV.globals
{
		public enum Perspective
		{ 
				JUST_SHOW_PROGRESS_WITHOUT_CANCEL,
				PROGRESS_WITH_CANCEL,
				ASK_TO_DOWNLOAD,
				BLANK,
				PROGRESS_BAR_WITHOUT_CANCEL
		}
}