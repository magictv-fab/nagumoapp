﻿using UnityEngine;
using System.Collections;
using MagicTV.abstracts;
using MagicTV.abstracts.device;
using MagicTV.vo;
using MagicTV.vo.result;

using MagicTV.globals;
using System;
using System.Collections.Generic;
using System.Linq;
using MagicTV.globals.events;
using ARM.utils.io;

public class DeviceFileInfo : DeviceFileInfoAbstract
{

	private ResultRevisionVO _ResultRevisionVO;
	public DeviceFileInfoDAO _DeviceFileInfoDAO;

	protected Dictionary<string, List<BundleVO> > _indexedBundles;
	protected Dictionary<string, BundleVO[]> _indexedBundlesArray;
	private static DeviceFileInfo instance;
//
//	public static DeviceFileInfo GetInstance ()
//	{
//		return instance;
//	}
	/// <summary>
	/// Initializes a new instance of the <see cref="DeviceFileInfo"/> class.
	/// </summary>
	private DeviceFileInfo ()
	{
		this._indexedBundles = new Dictionary<string, List<BundleVO> > ();
		this._indexedBundlesArray = new Dictionary<string, BundleVO[]> ();
		instance = this;
	}
	protected string hashVarName = "hash_image";
	/// <summary>
	/// Gets the last image hash.
	/// Se retornar string vazia é pq não tem e precisa baixar a imagem pela primeira vez
	/// </summary>
	/// <returns>The last image hash.</returns>
	public string getLastImageHash ()
	{
		string prefs = PlayerPrefs.GetString (hashVarName);
		if (prefs != null && prefs != "") {
			return prefs;
		}
		return "";
	}
	public void setLastImageHash (string hash)
	{
		PlayerPrefs.SetString (hashVarName, hash);
		PlayerPrefs.Save ();
	}
	void OnDestroy ()
	{
		instance = null;
	}
	// Use this for initialization
	public override void prepare ()
	{
		AppRoot.deviceFileInfo = this;
		AppRootEvents.GetInstance ().GetStateMachine ().AddChangeToStateEventHandler (onStateChangeToOpen, StateMachine.OPEN);
		_DeviceFileInfoDAO.AddCompoentIsReadyEventHandler (this.RaiseComponentIsReady);
		_DeviceFileInfoDAO.prepare ();
		populateIndexedBundles ();
		//Debug.LogError ("DeviceFileInfo . init ");
	}


	public void onStateChangeToOpen ()
	{
		//evento mudou para open
		//Debug.LogWarning ("[DFI] DeviceFileInfo . onStateChangeToOpen ~ cacheando DB ");
		if (this._DeviceFileInfoDAO) {
			this.cacheDbInfo ();
		}
	}

	void cacheDbInfo ()
	{
		//TODO: cachear toda info do banco || this.getCurrentRevision ();

	}

	/// <summary>
	/// Gets the revision by revision identifier.
	/// </summary>
	/// <returns>The revision by revision identifier.</returns>
	/// <param name="revision_id">Revision_id.</param>
	public override ResultRevisionVO getRevisionByRevisionId (string revision_id)
	{
		return _DeviceFileInfoDAO.SelectResultRevisionVOByRevision (Convert.ToInt32 (revision_id));
	}
	/// <summary>
	/// Gets the URL info VO to delete .
	/// </summary>
	/// <returns>The URL info VO to delete.</returns>
	public override UrlInfoVO[] getUrlInfoVOToDelete ()
	{
		return _DeviceFileInfoDAO.SelectDeactiveUrlInfoVO ();
	}
	/// <summary>
	/// Delete the specified obj forever
	/// </summary>
	/// <param name="obj">Object.</param>
	public override ReturnDataVO delete (UrlInfoVO obj)
	{
		return _DeviceFileInfoDAO.DeleteVO (obj);
	}
	/// <summary>
	/// Delete the specified list of objs forever
	/// </summary>
	/// <param name="objs">Objects.</param>
	public override ReturnDataVO delete (UrlInfoVO[] objs)
	{
		return _DeviceFileInfoDAO.DeleteVO (objs);
	}

	/// <summary>
	/// Gets the current revision.
	/// </summary>
	/// <returns>The current revision.</returns>
	public override ResultRevisionVO getCurrentRevision ()
	{
		_ResultRevisionVO = _DeviceFileInfoDAO.SelectCurrentResultRevisionVO ();
//				Debug.LogError ("DeviceFileInfo . getCurrentRevision_ResultRevisionVO: " + _ResultRevisionVO.ToString ());
		return _ResultRevisionVO;
	}

	/// <summary>
	/// Saves the revision.
	/// </summary>
	/// <returns>The revision.</returns>
	/// <param name="revisionResult">Revision result.</param>
	public override ReturnDataVO commit (ResultRevisionVO revisionResult)
	{
//				Debug.LogError ("D DeviceFileInfo . commit - revisionResult " + revisionResult.ToString());
		ReturnDataVO returnDataVO = _DeviceFileInfoDAO.Commit (revisionResult);
            
		// returnDataVO.result = getCurrentRevision ();

		_ResultRevisionVO = getCurrentRevision ();
//				populateIndexedBundles ();
		return returnDataVO;
	}

	/// <summary>
	/// Saves the revision.
	/// </summary>
	/// <returns>The revision.</returns>
	/// <param name="revisionResult">Revision result.</param>
	public override ReturnDataVO commit (UrlInfoVO urlInfo)
	{
//				//Debug.LogError ("DeviceFileInfo . commit: urlInfo" + urlInfo.ToString ());
		return _DeviceFileInfoDAO.CommitUrlInfoVO (urlInfo);
	}

	public ReturnDataVO commit (KeyValueVO obj)
	{
		return _DeviceFileInfoDAO.CommiteKeyValueVO (obj);
	}
	/// <summary>
	/// Gets the bundles.
	/// Recebe uma string do sistema e este é o local que deve decidir como converter essa string em bundles
	/// e essa string atualmente é um trackingID
	/// </summary>
	/// <returns>The bundles.</returns>
	/// <param name="trackingID">Tracking I.</param>
	public override BundleVO[] getBundles (string trackingID)
	{
//				Debug.LogWarning ("[DFI] DeviceFileInfo . getBundles ~ trackingID : " + trackingID + " >>> " + this._indexedBundles.Count ());
		if (this._indexedBundles.Count () == 0) {
			this.populateIndexedBundles ();
		}
		if (this._indexedBundlesArray.ContainsKey (trackingID)) {
//			Debug.LogWarning ("[DFI] DeviceFileInfo . getBundles ~ trackingID : " + trackingID + " !!!! ");
			return this._indexedBundlesArray [trackingID];
		}
//				Debug.LogWarning ("[DFI] DeviceFileInfo . getBundles ~ trackingID : NULL ");
		return null;
	}

	/// <summary>
	/// Populates the indexed bundles.
	/// </summary>
	public override void populateIndexedBundles ()
	{
		ResultRevisionVO revision = this.getCurrentRevision ();

		List<MetadataVO> trackings = revision.bundles.SelectMany (b => b.metadata).Where (m => m.key == "TrackingID").Distinct ().ToList ();

		_indexedBundles.Clear ();
				
		trackings.ForEach (
                        t => addBundle (t.value, revision.bundles.Where (b => b.metadata.Any (m => m.id == t.id)).ToList ())
		);
	}

	void addBundle (string value, List<BundleVO> list)
	{

		tracks.Add (value);
		if (_indexedBundles.ContainsKey (value)) {
			for (int i = 0; i < list.Count; i++) {
				_indexedBundles [value].Add (list [i]);
			}
			this.cacheBundle (value);
			return;

		}
		_indexedBundles.Add (value, list);
		cacheBundle (value);

		return;
	}

	void cacheBundle (string value)
	{
//		Debug.Log ("CacheBundle:" + value);
		if (_indexedBundlesArray.ContainsKey (value)) {
			_indexedBundlesArray [value] = _indexedBundles [value].ToArray ();
			return;
		}
		_indexedBundlesArray.Add (value, _indexedBundles [value].ToArray ());
	}

	public bool _debugClicked;
	void Update ()
	{
		//
		if (this._debugClicked) {
			this._debugClicked = false;
			//fazer algo no teste unitário
		}
	}

	/// <summary>
	/// Retorna todos os arquivos que ainda não foram baixados para baixar e informações da revisão
	/// </summary>
	/// <returns>The uncached revision.</returns>
	public override ResultRevisionVO getUncachedRevision ()
	{
//				_ResultRevisionVO = getCurrentRevision();
//				//Debug.LogError ("DeviceFileInfo . getUncachedRevision: " + _ResultRevisionVO.ToString ());
//				//Debug.LogError ("DeviceFileInfo . getUncachedRevision: " + _ResultRevisionVO.ToString ());
		_ResultRevisionVO = this.getCurrentRevision ();

		//Filtra somente os arquivos que ainda nao foram baixados
		ResultRevisionVO result = new ResultRevisionVO {
                        revision = _ResultRevisionVO.revision,
                        name = _ResultRevisionVO.name,
                        short_description = _ResultRevisionVO.short_description,
                        long_description = _ResultRevisionVO.long_description,
                        bundles = (from bx in _ResultRevisionVO.bundles
                                   //where bx.files.Any(bxf => bxf.type == "tracking.zip")
                                   select new BundleVO {
                                           id = bx.id,
                                           name = bx.name,
                                           revision_id = bx.revision_id,
                                           context = bx.context,
                                           files = (from fx in bx.files
                                                    where fx.type == "tracking.zip"
                                                    select new FileVO {
                                                            id = fx.id,
                                                            bundle_id = fx.bundle_id,
                                                            type = fx.type,
                                                            urls = (from ux in fx.urls
                                                                    where string.IsNullOrEmpty(ux.local_url)
                                                                    select new UrlInfoVO {
                                                                            id = ux.id,
                                                                            bundle_file_id = ux.bundle_file_id,
                                                                            type = ux.type,
                                                                            md5 = ux.md5,
                                                                            url = ux.url,
                                                                            size = ux.size,
                                                                            local_url = ux.local_url,
                                                                            status = ux.status
                                                                    }).ToArray()
                                                    }).ToArray(),
                                           metadata = (from mx in bx.metadata
                                                       select new MetadataVO {
                                                               tabela = "bundle_metadata",
                                                               tabela_id = bx.id,
                                                               id = mx.id,
                                                               key = mx.key,
                                                               value = mx.value,
                                                       }).ToArray(),
                                           published_at = bx.published_at,
                                           expired_at = bx.expired_at
                                   }).ToArray(),
                        metadata = (from mx in _ResultRevisionVO.metadata
                                    select new MetadataVO {
                                            tabela = "app_metadata",
                                            tabela_id = _ResultRevisionVO.revision,
                                            id = mx.id,
                                            key = mx.key,
                                            value = mx.value,
                                    }).ToArray()
                };

		// Remove bundles sem arquivos e sem urls
		result.bundles = result.bundles.Where (b => b.files.Count (f => f.urls.Count () > 0) > 0).ToArray ();
		//Debug.LogError ("DeviceFileInfo . getUncachedRevision filter: " + result.ToString ());
		return result;
	}
	protected string KEY_TOTAL_IMAGE = "totalImages";
	protected string KEY_LOCAL_IMAGE_URL_ = "localImageUrl_";
	protected string TAG_LAST_INDEX_IMAGE_SORTED = "lastIndexImageSorted" ;
	public void Reset ()
	{
		//this.clearImages ();
	}
	public string[] initialImages;
	public void setImagesSplash (string[] imagesUrl)
	{
		//limpa os valores atuais para setar os novos valores
		this.clearImages ();
		//salva cada item no user prefs
		for (int i = 0; i < imagesUrl.Length; i++) {
			PlayerPrefs.SetString (KEY_LOCAL_IMAGE_URL_ + i, imagesUrl [i]);
			PlayerPrefs.Save ();
		}
		//salva o total no user prefs 
		PlayerPrefs.SetInt (KEY_TOTAL_IMAGE, imagesUrl.Length);
		PlayerPrefs.Save ();
	}

	public void clearImages ()
	{
		int total = this.getTotalImages ();
		while (total > 0) {
			if (this.getImageByIndex (total) != "") {
				PlayerPrefs.DeleteKey (KEY_LOCAL_IMAGE_URL_ + total);
			}
			total--;
		}
		PlayerPrefs.SetInt (KEY_TOTAL_IMAGE, 0);
		PlayerPrefs.SetInt (TAG_LAST_INDEX_IMAGE_SORTED, 0);
		PlayerPrefs.Save ();
	}
	/// <summary>
	/// Gets the total images.
	/// </summary>
	/// <returns>The total images.</returns>
	protected int getTotalImages ()
	{
		int total = PlayerPrefs.GetInt (KEY_TOTAL_IMAGE);
		if (total <= 0) {
			if (initialImages == null || initialImages.Length == 0) {
				return 0;
			}
			//não tem imagem salva, tenta usar as imagens padrão como total
//			PlayerPrefs.SetInt (KEY_TOTAL_IMAGE, initialImages.Length);
//			PlayerPrefs.Save ();
		}
		return PlayerPrefs.GetInt (KEY_TOTAL_IMAGE);
	}

	/// <summary>
	/// Gets the image splash local url.
	/// </summary>
	/// <returns>The image splash.</returns>
	public string getImageSplash ()
	{
		int lastIndex = PlayerPrefs.GetInt (TAG_LAST_INDEX_IMAGE_SORTED);
		lastIndex++;
		if (lastIndex >= this.getTotalImages ()) {
			lastIndex = 0 ;
		}
		PlayerPrefs.SetInt (TAG_LAST_INDEX_IMAGE_SORTED, lastIndex);
		PlayerPrefs.Save ();
		return this.getImageByIndex (lastIndex);
	}
	protected string getImageByIndex (int index)
	{
		string localUrl = PlayerPrefs.GetString (KEY_LOCAL_IMAGE_URL_ + index);
		if (localUrl != "" && localUrl != null) {

			return localUrl;
		}
		//não achou, deve estar setado no array de imagens iniciais
		if (this.initialImages != null && this.initialImages.Length > index) {
			return this.initialImages [index];
		}
		return "";
	}
	public override UrlInfoVO[] getAllUrlInfoVO ()
	{
		Debug.LogError ("Não implementado");
		return new UrlInfoVO[0];
	}
	public override UrlInfoVO[] getCachedUrlInfoVO ()
	{
		Debug.LogError ("Não implementado");
		return new UrlInfoVO[0];
	}
	public override void Save ()
	{
		//TODO: salva o que está na memória
		
	}
}
