﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// In app analytics.
/// 
/// @author : Renato Miawaki
/// 
/// @version : 1.0
/// </summary>
using MagicTV.processes;


namespace MagicTV.globals
{
	public class InAppAnalytics
	{
		static Dictionary<string,InAppAnalytics> instances = new Dictionary<string, InAppAnalytics> () ;
		/// <summary>
		/// uso privado, pega uma  new instance.
		/// </summary>
		/// <returns>The new instance.</returns>
		/// <param name="appName">App name.</param>
		/// <param name="trackingID">Tracking I.</param>
		public static InAppAnalytics GetNewInstance (string appName, string trackingID)
		{
			return new InAppAnalytics (appName, trackingID);
		}
		/// <summary>
		/// uso público, pega uma isntancia única, se já existir retorna a mesma de um pool
		/// Gets the analytics.
		/// </summary>
		/// <returns>The analytics.</returns>
		/// <param name="appName">App name.</param>
		/// <param name="trackingID">Tracking I.</param>
		public static InAppAnalytics GetAnalytics (string appName, string trackingID)
		{
			if (! AnalyticsExists (appName)) {
						
				instances.Add (appName, GetNewInstance (appName, trackingID));
						
			}
				
			return instances [appName];
		}
		/// <summary>
		/// just string appName? ok, lets try find
		/// </summary>
		/// <returns>The analytics.</returns>
		/// <param name="appName">App name.</param>
		public static InAppAnalytics GetAnalytics (string appName)
		{
			if (AnalyticsExists (appName)) {
				return instances [appName];
			}
			return null;
		}
		/// <summary>
		/// Analyticses the exists.s
		/// </summary>
		/// <returns><c>true</c>, if exists was analyticsed, <c>false</c> otherwise.</returns>
		/// <param name="appName">App name.</param>
		public static bool AnalyticsExists (string appName)
		{
			return (instances.ContainsKey (appName));
		}

		string _appName;
		private Analytics _Analytics ;
		/// <summary>
		/// construtora
		/// </summary>
		/// <param name="appName">App name.</param>
		/// <param name="trackingID">Tracking I.</param>
		public InAppAnalytics (string appName, string trackingID)
		{
			this._appName = appName;
			GameObject go = new GameObject ("InAppAnalytics_" + appName + "_" + trackingID);
			this._Analytics = go.AddComponent<Analytics> ();
			this._Analytics.trackingID = trackingID;
			this._Analytics.appName = appName;
			this._Analytics.Init ();
			this._Analytics.changeScreen ("Tela Login");

		}


		/// <summary>
		/// Chamne o init, para iniciar o listeners com a GUI
		/// </summary>
		public void init ()
		{
			if (WindowControlListScript.Instance != null) {
				WindowControlListScript.Instance.WindowInAppGUI.onClickCamera += WindowInAppGUIonClickCamera;
			}
			if (WindowControlListScript.Instance != null && WindowControlListScript.Instance.WindowInstructions != null) {
				WindowControlListScript.Instance.WindowInstructions.onShow += WindowInstructionsOnShow;
			} 
//						WindowControlListScript.Instance.WindowHelp.onClickPrint += WindowHelpOnClickPrint;
//						WindowControlListScript.Instance.WindowHelp.onClickYouTubeVideo += WindowHelpOnClickYouTubeVideo;
//						WindowControlListScript.Instance.WindowHelp.onShow += WindowHelpOnShow;
		
//						WindowControlListScript.Instance.WindowAbout.onShow += WindowAboutOnShow;
//						WindowControlListScript.Instance.WindowAbout.onClickEmail += WindowAboutOnClickEmail;
//						WindowControlListScript.Instance.WindowAbout.onClickFacebook += WindowAboutOnClickFacebook;
//						WindowControlListScript.Instance.WindowAbout.onClickSite += WindowAboutOnClickSite;
//						WindowControlListScript.Instance.WindowAbout.onClickTwitter += WindowAboutOnClickTwitter;
//						WindowControlListScript.Instance.WindowAbout.onClickYouTubeChannel += WindowAboutOnClickYouTubeChannel;
		}
		/// <summary>
		/// Chame stop para parar o listener com a GUI
		/// </summary>
		public void stop ()
		{
			if (WindowControlListScript.Instance.WindowInAppGUI == null) {
				return;
			}
			WindowControlListScript.Instance.WindowInAppGUI.onClickCamera -= WindowInAppGUIonClickCamera;
		
			//  WindowControlListScript.Instance.WindowInstructions.onShow -= WindowInstructionsOnShow;
		
//						WindowControlListScript.Instance.WindowHelp.onClickPrint -= WindowHelpOnClickPrint;
//						WindowControlListScript.Instance.WindowHelp.onClickYouTubeVideo -= WindowHelpOnClickYouTubeVideo;
//						WindowControlListScript.Instance.WindowHelp.onShow -= WindowHelpOnShow;
//		
//						WindowControlListScript.Instance.WindowAbout.onShow -= WindowAboutOnShow;
//						WindowControlListScript.Instance.WindowAbout.onClickEmail -= WindowAboutOnClickEmail;
//						WindowControlListScript.Instance.WindowAbout.onClickFacebook -= WindowAboutOnClickFacebook;
//						WindowControlListScript.Instance.WindowAbout.onClickSite -= WindowAboutOnClickSite;
//						WindowControlListScript.Instance.WindowAbout.onClickTwitter -= WindowAboutOnClickTwitter;
//						WindowControlListScript.Instance.WindowAbout.onClickYouTubeChannel -= WindowAboutOnClickYouTubeChannel;
		}



		#region Eventos para serem usados encapsulando o modo de trackear no analytics

		public void CustomEvent (string category, string eventName)
		{
			_Analytics.sendEvent (this._appName, category, eventName);
			
		}
		string _lastTrackNameAnalytics;
		public void Open ()
		{
			if (PresentationController.Instance != null && _lastTrackNameAnalytics != PresentationController.Instance._lastTrackName) {
				//last trackname
				_lastTrackNameAnalytics = PresentationController.Instance._lastTrackName;
				//UPDATE tracking 
				_Analytics.sendEvent (PresentationController.Instance._lastTrackName, "TrackFinded", "trackEncontrado");
			}
			_Analytics.sendEvent (this._appName, "Open", "app aberto");
		}
		/// <summary>
		/// Sends the personal event.
		/// </summary>
		/// <param name="action">Action.</param>
		/// <param name="label">Label.</param>
		public void SendEvent (string action, string label)
		{
			_Analytics.sendEvent (this._appName, action, label);
		}
		public void Close ()
		{
			_Analytics.sendEvent (this._appName, "Close", "app fechado");
		}
	
		public void WindowInAppGUIonClickCamera ()
		{
			_Analytics.sendEvent (this._appName, "ScreenShot", "Foto");
		}
	
		public void WindowInstructionsOnShow ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Instruções");
		}
	
		public void WindowHelpOnShow ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Ajuda");
		}
	
		public void WindowHelpOnClickPrint ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Ajuda Impressão");
				
		}
	
		public void WindowHelpOnClickYouTubeVideo ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Ajuda You Tube");
				
		}
	
		public void WindowAboutOnShow ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Sobre");
		}
	
		public void WindowAboutOnClickEmail ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Sobre E-mail");
				
		}
	
		public void WindowAboutOnClickFacebook ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Sobre Facebook");
				
		}
	
		public void WindowAboutOnClickSite ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Sobre Site");
				
		}
	
		public void WindowAboutOnClickTwitter ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Sobre Twitter");
				
		}
	
		public void WindowAboutOnClickYouTubeChannel ()
		{
			_Analytics.sendEvent (this._appName, "Menu", "Sobre You Tube");
				
		}
		#endregion
	}
}
