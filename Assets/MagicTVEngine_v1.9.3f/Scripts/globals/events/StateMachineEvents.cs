﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts.screens ;
using System.Collections.Generic ;

namespace MagicTV.globals.events
{

		/// <summary>
		/// 
		/// State magine events.
		/// 
		/// LOADING, 
		///	WAIT_FOR_USER_CHOICE, 
		///	IN_APP 
		///
		/// </summary>
		public class StateMagineEvents
		{

				/*** EVENTO GLOBAL DE MUDANÇA DE PERSPECTIVA ***/
		
				public delegate void OnChangeToStateEventHandler (StateMachine p);
				public delegate void OnChangeToAnStateEventHandler ();
				protected OnChangeToStateEventHandler onChangeToState ;

				protected OnChangeToAnStateEventHandler onChangeToStateInit ;
				protected OnChangeToAnStateEventHandler onChangeToStateUpdate ;
				protected OnChangeToAnStateEventHandler onChangeToStateOpen ;


				protected Dictionary<StateMachine, OnChangeToAnStateEventHandler> eventsList;

				public StateMagineEvents ()
				{
						this.eventsList = new Dictionary<StateMachine, OnChangeToAnStateEventHandler> ();
						this.eventsList.Add (StateMachine.INIT, onChangeToStateInit);
						this.eventsList.Add (StateMachine.UPDATE, onChangeToStateUpdate);
						this.eventsList.Add (StateMachine.OPEN, onChangeToStateOpen);
				}

				public void AddChangeToStateEventHandler (OnChangeToStateEventHandler action)
				{
						this.onChangeToState += action;
				}
		
				public void RemoveChangeToStateEventHandler (OnChangeToStateEventHandler action)
				{
						this.onChangeToState -= action;
				}

		
				public void AddChangeToStateEventHandler (OnChangeToAnStateEventHandler action, StateMachine state)
				{
						if (this.eventsList.ContainsKey (state)) {
								this.eventsList [state] += action;
						}
				}
		
				public void RemoveChangeToStateEventHandler (OnChangeToAnStateEventHandler action, StateMachine state)
				{
						if (this.eventsList.ContainsKey (state)) {
								this.eventsList [state] -= action;
						}
				}

				public void RaiseChangeToState (StateMachine state)
				{
						if (onChangeToState != null) {
								onChangeToState (state);
						}
						//faz o raise por tipo
						if (this.eventsList.ContainsKey (state)) {
								if (this.eventsList [state] != null) {
										this.eventsList [state] ();
								}
						}
				}

				private static StateMagineEvents _instance ;

				public static StateMagineEvents GetInstance ()
				{
						if (StateMagineEvents._instance == null) {
								StateMagineEvents._instance = new StateMagineEvents ();
						}
						return StateMagineEvents._instance; 
				}



		}

}