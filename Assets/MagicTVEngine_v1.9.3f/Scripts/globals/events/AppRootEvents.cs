﻿using UnityEngine;
using System.Collections;
using MagicTV.in_apps;
using System.Collections.Generic;

/// <summary>
/// App root events.
/// 
/// @version : 1.0
/// @author : Renato Seiji Miawaki
/// 
/// </summary>
namespace MagicTV.globals.events
{
	/// <summary>
	/// Proxy de todas as classes de eventos globais
	/// </summary>
	public class AppRootEvents : MonoBehaviour
	{
		public AppRootEvents ()
		{
			_instance = this;
		}
		public OnVoidEventHandler onDownloadAllCalled ;
		public void RaiseDownloadAll ()
		{
			if (onDownloadAllCalled == null) {
				return;
			}
			onDownloadAllCalled ();
		}

		public static void LogError (string context, string message)
		{
			//TODO: enviar para o servidor o erro, enviar em fila, um por um.
			Debug.LogError (context + "  " + message);
		}
		public delegate void OnVoidEventHandler ();
		/// <summary>
		/// Para centralizar e fazer o meio de campo entre a GUI e a PresentationController
		/// The on raise stop presentation.
		/// </summary>
		public OnVoidEventHandler onRaiseStopPresentation ;
		public void RaiseStopPresentation ()
		{

			if (onRaiseStopPresentation != null) {
			
//				Debug.LogError (" RaiseStopPresentation ");
				onRaiseStopPresentation ();
			}
		}

		private static AppRootEvents _instance;

		public static AppRootEvents GetInstance ()
		{
			return _instance;
		}

		public AREvents GetAR ()
		{
			return AREvents.GetInstance ();
		}

		public ScreenEvents GetScreen ()
		{
			return ScreenEvents.GetInstance ();
		}


		public DeviceEvents GetDevice ()
		{
			return DeviceEvents.GetInstance ();
		}

		protected Dictionary<string, InAppEventsChannel> _channels = new Dictionary<string, InAppEventsChannel> ();

		/// <summary>
		/// Gets the event channel.
		/// 
		/// </summary>
		/// <returns>The event channel.</returns>
		/// <param name="channelName">Channel name.</param>
		public InAppEventsChannel GetEventChannel (string channelName)
		{
			if (!this._channels.ContainsKey (channelName)) {
				this._channels.Add (channelName, new InAppEventsChannel ());
			}
			return this._channels [channelName];
		}
		/// <summary>
		/// Removes the event channel listener.
		/// </summary>
		/// <param name="channelName">Channel name.</param>
		public void RemoveEventChannel (string channelName)
		{
			if (this._channels.ContainsKey (channelName)) {
				this._channels [channelName] = null;
				this._channels.Remove (channelName);
			}
		}
		/// <summary>
		/// Gets the state machine.
		/// </summary>
		/// <returns>The state machine.</returns>
		public StateMagineEvents GetStateMachine ()
		{
			return StateMagineEvents.GetInstance ();
		}
		/// <summary>
		/// Prepara todos os PACIVOS para os eventos globais
		/// </summary>
		/// <param name="main">Main.</param>
		public static void prepare (MagicTVProcess main)
		{
			//DeviceEvents.GetInstance ().RaiseDoShake ();
			AMR.device.DeviceAppStateMachineEvents.AddResumeEventHandler (DeviceEvents.GetInstance ().RaiseResume);
			AMR.device.DeviceAppStateMachineEvents.AddPauseEventHandler (DeviceEvents.GetInstance ().RaisePause);
			AMR.device.DeviceAppStateMachineEvents.AddAwakeEventHandler (DeviceEvents.GetInstance ().RaiseAwake);
		}

	}

}