﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts.screens ;
using MagicTV.vo.result ;

namespace MagicTV.globals.events{

	public class UserEvents {

		/*** EVENTO GLOBAL DE MUDANÇA DE CONFIRMAÇÃO DE PROMPT DE CONFIRMAÇÃO ***/

		public delegate void OnConfirmEventHandler( ConfirmResultVO p );
		protected OnConfirmEventHandler onConfirm ;

		public void AddConfirmEventHandler( OnConfirmEventHandler action ){
			this.onConfirm += action ;
		}
		
		public void RemoveConfirmEventHandler( OnConfirmEventHandler action ){
			this.onConfirm -= action ;
		}

		public void RaiseConfirm( ConfirmResultVO result ){
			if( onConfirm != null ){
				onConfirm( result ) ;
			}
		}


		/*** EVENTO GLOBAL DE PEDIDO PARA ABRIR PROMPTY DE CONFIRMAÇÃO DO USUARIO ***/
		
		protected OnConfirmEventHandler onConfirmCalled ;
		
		public void AddConfirmCalledEventHandler( OnConfirmEventHandler action ){
			this.onConfirmCalled += action ;
		}
		
		public void RemoveConfirmCalledEventHandler( OnConfirmEventHandler action ){
			this.onConfirmCalled -= action ;
		}
		
		public void RaiseConfirmCalled( ConfirmResultVO result ){
			if( onConfirmCalled != null ){
				onConfirmCalled( result ) ;
			}
		}

		private static UserEvents _instance ;

		public static UserEvents GetInstance(){
			if( UserEvents._instance == null ){
				UserEvents._instance = new UserEvents() ;
			}
			return UserEvents._instance ; 
		}



	}

}