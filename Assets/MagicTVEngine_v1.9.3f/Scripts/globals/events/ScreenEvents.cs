﻿using UnityEngine;
using System.Collections;

using MagicTV.abstracts.screens ;

using System.Collections.Generic ;
/// <summary>
/// Screen events globais
/// </summary>
namespace MagicTV.globals.events
{

	public class ScreenEvents: MonoBehaviour
	{
		public delegate void OnVoidEventHandler ();
		public delegate void OnStringEventHandler (string path);
		public delegate void OnIntBoolEventHandler (int i,bool b);
		protected OnVoidEventHandler onAlertMessageClose;
		public string captionText;

		public void SendAlert (string message, string title = "Alert", string buttonLabel = "OK", OnVoidEventHandler onCloseEventHandler  = null)
		{
			MobileNativeMessage msga = new MobileNativeMessage (title, message, buttonLabel);

			if (onCloseEventHandler != null) {
				msga.OnComplete += OnAlertClose;
				onAlertMessageClose += onCloseEventHandler;
			}

		}
		private void OnAlertClose ()
		{
			if (onAlertMessageClose != null) {
				onAlertMessageClose ();
				onAlertMessageClose = null;
			}
		}

		public void showCurrentCaption ()
		{
			Debug.LogError ("Caption SHOW CURRENT: " + this.captionText);
			this.SetCaption (captionText);
		}

		/// <summary>
		/// 
		/// The on set caption.
		/// Para legendas na GUI
		/// 
		/// </summary>
		public OnStringEventHandler OnSetCaption;
		/// <summary>
		/// Sets the caption.
		/// Legendas na GUI
		/// </summary>
		/// <param name="text">Text.</param>
		public void SetCaption (string text)
		{
			if (OnSetCaption != null) {
				OnSetCaption (text);
				if (text != "") {
					this.captionText = text;
				}
				//Debug.LogError ("Caption: " + this.captionText);
			}
		}

		public void ResetCaption ()
		{
			SetCaption ("");
			ResetConfigCaption ();
			this.captionText = "";
		}

 
		public OnIntBoolEventHandler OnConfigCaption;
		/// <summary>
		/// Configs the caption.
		/// Size é o tamanho da fonte
		/// autoSize contradiz o tamanho e seta o tamanho da fonte automático
		/// </summary>
		/// <param name="size">Size.</param>
		/// <param name="autoSize">If set to <c>true</c> auto size.</param>
		public void ConfigCaption (int size, bool autoSize)
		{
			if (OnConfigCaption != null) {
				OnConfigCaption (size, autoSize);
			}
		}
		public void ResetConfigCaption ()
		{
			ConfigCaption (50, false);
		}
		/*** EVENTO PARA MOSTRAR A PAGINA DE COMO USAR ***/

		protected OnVoidEventHandler onShowHelpPage ;
		public void RaiseShowHelpPage ()
		{
			if (this.onShowHelpPage != null) {
				this.onShowHelpPage ();
			}
		}
		public void AddShowPageCalledEventHandler (OnVoidEventHandler action)
		{
			this.onShowHelpPage += action;
		}
		
		public void RemoveShowPageCalledEventHandler (OnVoidEventHandler action)
		{
			this.onShowHelpPage -= action;
		}


		/*** EVENTO GLOBAL DE MUDANÇA DE PERSPECTIVA ***/

		public delegate void OnChangeToPerspectiveEventHandler (Perspective p);
		public delegate void OnChangeAnToPerspectiveEventHandler ();
		protected OnChangeToPerspectiveEventHandler onChangeToPerspective ;
		
		protected OnChangeAnToPerspectiveEventHandler onChangeToPerspectiveAskDownload ;
		protected OnChangeAnToPerspectiveEventHandler onChangeToPerspectiveBlank ;
		protected OnChangeAnToPerspectiveEventHandler onChangeToPerspectiveProgressWithoutCancel ;
		protected OnChangeAnToPerspectiveEventHandler onChangeToPerspectiveProgressWithCancel ;
		protected OnChangeAnToPerspectiveEventHandler onChangeToPerspectiveProgressBarWithouCancel ;

		protected Dictionary< Perspective, OnChangeAnToPerspectiveEventHandler> eventsList;
		
		public ScreenEvents ()
		{
            _instance = this;
           
            //			this.eventsList = new Dictionary< Perspective, OnChangeAnToPerspectiveEventHandler> ();
            //			this.eventsList.Add (Perspective.BLANK, this.onChangeToPerspectiveBlank);
            //			this.eventsList.Add (Perspective.ASK_TO_DOWNLOAD, this.onChangeToPerspectiveAskDownload);
            //			this.eventsList.Add (Perspective.JUST_SHOW_PROGRESS_WITHOUT_CANCEL, this.onChangeToPerspectiveProgressWithoutCancel);
            //			this.eventsList.Add (Perspective.PROGRESS_WITH_CANCEL, this.onChangeToPerspectiveProgressWithCancel);
            //			this.eventsList.Add (Perspective.PROGRESS_BAR_WITHOUT_CANCEL, this.onChangeToPerspectiveProgressBarWithouCancel);
        }
		void Awake(){
			// OnSetCaption += WindowControlListScript.Instance.SetCaption;
			// OnConfigCaption += WindowControlListScript.Instance.SetConfig;
		}
//		public void AddChangeToPerspectiveEventHandler (OnChangeToPerspectiveEventHandler action)
//		{
//			this.onChangeToPerspective += action;
//		}
//		
//		public void RemoveChangeToPerspectiveEventHandler (OnChangeToPerspectiveEventHandler action)
//		{
//			this.onChangeToPerspective -= action;
//		}
//
//		public void AddChangeToPerspectiveEventHandler (OnChangeAnToPerspectiveEventHandler action, Perspective perspective)
//		{
//			if (this.eventsList.ContainsKey (perspective)) {
//				this.eventsList [perspective] += action;
//			}
//		}
//		
//		public void RemoveChangeToPerspectiveEventHandler (OnChangeAnToPerspectiveEventHandler action, Perspective perspective)
//		{
//			if (this.eventsList.ContainsKey (perspective)) {
//				this.eventsList [perspective] -= action;
//			}
//		}
//
//		public void RaiseChangeToPerspective (Perspective perspective)
//		{
//			if (onChangeToPerspective != null) {
//				onChangeToPerspective (perspective);
//			}
//			if (this.eventsList.ContainsKey (perspective)) {
//				if (this.eventsList [perspective] != null) {
//					this.eventsList [perspective] ();
//				}
//			}
//		}
		/*** EVENTO GLOBAL DE PRINT SCREEN DONE ***/
		

		
		protected OnStringEventHandler onPrintScreenCompleted ;
		
		public void AddPrintScreenCompletedEventHandler (OnStringEventHandler action)
		{
			this.onPrintScreenCompleted += action;
		}
		
		public void RemovePrintScreenCompletedEventHandler (OnStringEventHandler action)
		{
			this.onPrintScreenCompleted -= action;
		}
		
		public void RaisePrintScreenCompleted (string path)
		{
			if (this.onPrintScreenCompleted != null) {
				this.onPrintScreenCompleted (path);
			}
		}
		#region CloseButton

		public OnVoidEventHandler onRaiseShowCloseButton ;
		public void RaiseShowCloseButton ()
		{
			if (this.onRaiseShowCloseButton != null) {
				this.onRaiseShowCloseButton ();
			}
		}

		public OnVoidEventHandler onRaiseHideCloseButton ;
		public void RaiseHideCloseButton ()
		{
			if (this.onRaiseHideCloseButton != null) {
				this.onRaiseHideCloseButton ();
			}
		}
		#endregion
		#region PrintScreenEvents
		/*** EVENTO GLOBAL DE PRINT SCREEN ***/


		protected OnVoidEventHandler onPrintScreenCalled ;


		public void AddPrintScreenCalledEventHandler (OnVoidEventHandler action)
		{
			this.onPrintScreenCalled += action;
		}
			
		public void RemovePrintScreenCalledEventHandler (OnVoidEventHandler action)
		{
			this.onPrintScreenCalled -= action;
		}
			
		public void RaisePrintScreenCalled ()
		{
			if (this.onPrintScreenCalled != null) {
				this.onPrintScreenCalled ();
			}
		}

		#endregion

		/*** EVENTO GLOBAL DE Colocar PIN na tela ***/

		protected OnStringEventHandler onSetPINEventHandler ;
		
		public void AddSetPINEventHandler (OnStringEventHandler action)
		{
			this.onSetPINEventHandler += action;
		}
		
		public void RemoveSetPINEventHandler (OnStringEventHandler action)
		{
			this.onSetPINEventHandler -= action;
		}
		
		public void RaiseSetPINEventHandler ( string pin)
		{
			if (this.onSetPINEventHandler != null) {
				this.onSetPINEventHandler ( pin );
			}
		}

		/*** EVENTO GLOBAL DE BOTAO VOLTAR ***/

		protected OnVoidEventHandler onGoBackEventHandler ;
		
		public void AddGoBackEventHandler (OnVoidEventHandler action)
		{
			this.onGoBackEventHandler += action;
		}
		
		public void RemoveGoBackEventHandler (OnVoidEventHandler action)
		{
			this.onGoBackEventHandler -= action;
		}
		
		public void RaiseGoBackCalled ()
		{
			if (this.onGoBackEventHandler != null) {
				this.onGoBackEventHandler ();
			}
		}
		/*** SINGLETON ***/
		private static ScreenEvents _instance ;

		public static ScreenEvents GetInstance ()
		{
			return ScreenEvents._instance; 
		}
	}

}