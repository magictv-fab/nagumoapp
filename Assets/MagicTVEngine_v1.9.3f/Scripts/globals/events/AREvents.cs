using UnityEngine;
using System.Collections;

namespace MagicTV.globals
{
    public class AREvents : MonoBehaviour
    {

        /*** EVENTOS GLOBAIS RELACIONADOS A CHAMAR O PLUGIN DE REALIDADE AUMENTADA ***/

        public delegate void OnARToggleOnOff(bool b);
		public delegate void OnStringEvent ( string s );

        protected OnARToggleOnOff onARToggle;


        public delegate void OnARWantToDoSomething();

        protected OnARWantToDoSomething onWantToTurnOn;
        protected OnARWantToDoSomething onWantToTurnOff;


        /// <summary>
		/// Avisos se ligou ou desligou
		/// Nada tem a ver com trackin ou trackout
        /// </summary>
        /// <param name="action">Action.</param>
        public void AddChangeARToggleHandler(OnARToggleOnOff action)
        {
            this.onARToggle += action;

        }

        public void RemoveChangeARToggleHandler(OnARToggleOnOff action)
        {
            this.onARToggle -= action;
        }

        public void RaiseARToggle(bool b)
        {
            if (onARToggle != null)
            {
                onARToggle(b);
            }
        }

        /*************************************/
		public OnStringEvent onTrackIn;
		public void RaiseTrackIn(string trackName){
			if(onTrackIn!=null){
				onTrackIn(trackName);
			}
		}
		public OnStringEvent onTrackOut;
		public void RaiseTrackOut(string trackName){
			if(onTrackOut!=null){
				onTrackOut(trackName);
			}
		}
        /***  Eventos de ligar e desligar  ****/
        public void AddOnWantToTurnOnEventHandler(OnARWantToDoSomething action)
        {
            this.onWantToTurnOn += action;
        }

        public void RemoveOnWantToTurnOnEventHandler(OnARWantToDoSomething action)
        {
            this.onWantToTurnOn -= action;
        }

        public void RaiseWantToTurnOn()
        {
            if (onWantToTurnOn != null)
            {
                onWantToTurnOn();
            }
        }

        public void AddOnWantToTurnOffEventHandler(OnARWantToDoSomething action)
        {
            this.onWantToTurnOff += action;
        }

        public void RemoveOnWantToTurnOffEventHandler(OnARWantToDoSomething action)
        {
            this.onWantToTurnOff -= action;
        }

        public void RaiseWantToTurnOff()
        {
            if (onWantToTurnOff != null)
            {
                onWantToTurnOff();
            }
        }
        /***********************/


        /*** Para instanciar ***/
        private static AREvents _instance;

        public AREvents()
        {
            _instance = this;
        }

        public static AREvents GetInstance()
        {
            return _instance;
        }


    }
}