using UnityEngine;
using System.Collections;

namespace MagicTV.globals.events
{
    public class DeviceEvents : MonoBehaviour
    {

        /*** EVENTOS GLOBAIS RELACIONADOS A CHAMAR O PLUGIN DE REALIDADE AUMENTADA ***/


        public delegate void OnSomething();
        public delegate void OnString(string value);


        //				public delegate void OnARTurnedOnOff ();



        #region shakeEvents


        protected OnSomething doVibrate;

        /***  Eventos de ligar e desligar  ****/
        public void AddVibrateEventHandler(OnSomething action)
        {
            this.doVibrate += action;
        }

        public void RemoveVibrateEventHandler(OnSomething action)
        {
            this.doVibrate -= action;
        }

        public void RaiseDoVibrate()
        {
            if (doVibrate != null)
            {
                doVibrate();
            }
        }
        #endregion

        #region notificationToken 


        public OnString _onSendTokenToServer;
        public void RaiseSendTokenToServer(string token)
        {
            if (_onSendTokenToServer != null)
            {
                _onSendTokenToServer(token);
            }
        }

        public OnString _onTokenSavedIntoServer;
        public void RaiseTokenSavedIntoServer(string token)
        {
            if (_onTokenSavedIntoServer != null)
            {
                _onTokenSavedIntoServer(token);
            }
        }
        #endregion

        #region Device StateMachine 
        protected OnString onPinRecieved;
        public void AddOnPinRecieved(OnString method)
        {
            this.onPinRecieved += method;
        }
        public void RemoveOnPinRecieved(OnString method)
        {
            this.onPinRecieved -= method;
        }
        public void RaiseDoPinRecived(string pin)
        {
            if (onPinRecieved != null)
            {
                onPinRecieved(pin);
            }
        }


        protected OnSomething onReset;
        public void AddResetEventHandler(OnSomething method)
        {
            this.onReset += method;
        }
        public void RemoveResetEventHandler(OnSomething method)
        {
            this.onReset -= method;
        }
        public void RaiseReset()
        {
            if (this.onReset != null)
            {
                this.onReset();
            }
        }

		/// <summary>
		/// The on clear cache complete nothing to delete.
		/// Evento para quando não tem nada para deletar e chamou o delete
		/// </summary>
		protected OnSomething onClearCacheCompleteNothingToDelete;
		
		public void AddClearCacheNothingToDeleteEventHandler(OnSomething method)
		{
			RemoveClearCacheNothingToDeleteEventHandler (method);
			this.onClearCacheCompleteNothingToDelete += method;
		}
		public void RemoveClearCacheNothingToDeleteEventHandler(OnSomething method)
		{
			this.onClearCacheCompleteNothingToDelete -= method;
		}
		public void RaiseClearCacheNothingToDeleteEventHandler()
		{
			if (this.onClearCacheCompleteNothingToDelete != null)
			{
				this.onClearCacheCompleteNothingToDelete();
			}
		}

		/// <summary>
		/// The on clear cache complete.
		/// Para quando termina de limpar o cache local ele avisa
		/// </summary>
		protected OnSomething onClearCacheComplete;

		public void AddClearCacheCompletedEventHandler(OnSomething method)
		{
			RemoveClearCacheCompletedEventHandler (method);
			this.onClearCacheComplete += method;
		}
		public void RemoveClearCacheCompletedEventHandler(OnSomething method)
		{
			this.onClearCacheComplete -= method;
		}
		public void RaiseClearCacheCompleted()
		{
			if (this.onClearCacheComplete != null)
			{
				this.onClearCacheComplete();
			}
		}



		/// <summary>
		/// Evento para pedir para apagar o cache local, não apaga tudo, apenas os arquivos baixados
		/// </summary>
		protected OnSomething onClearCache;
		public void AddClearCacheEventHandler(OnSomething method)
		{
			this.onClearCache += method;
		}
		public void RemoveClearCacheEventHandler(OnSomething method)
		{
			this.onClearCache -= method;
		}
		public void RaiseClearCache()
		{
			if (this.onClearCache != null)
			{
				this.onClearCache();
			}
		}


		/// <summary>
		/// Abaixo a intenção de apagar os arquivos locais, ou seja, pode ser interceptado por uma janela
		/// não é reset
		/// </summary>
		protected OnSomething onWantToClearCache;
		public void AddWantToClearCacheEventHandler(OnSomething method)
		{
			this.onWantToClearCache += method;
		}
		public void RemoveWantToClearCacheEventHandler(OnSomething method)
		{
			this.onWantToClearCache -= method;
		}
		public void RaiseWantToClearCache()
		{
			if (this.onWantToClearCache != null)
			{
				this.onWantToClearCache();
			}
		}



		/// <summary>
		/// Abaixo a intenção de reset, ou seja, pode ser interceptado por uma janela
		/// </summary>
		protected OnSomething onResetClicked;
		public void AddResetClickedEventHandler(OnSomething method)
		{
			this.onResetClicked += method;
		}
		public void RemoveResetClickedEventHandler(OnSomething method)
		{
			this.onResetClicked -= method;
		}
		public void RaiseResetClicked()
		{
			if (this.onResetClicked != null)
			{
				this.onResetClicked();
			}
		}

        protected OnSomething onResume;

        public void RemoveResumeEventHandler(OnSomething action)
        {
            this.onResume -= action;
        }

        public void AddResumeEventHandler(OnSomething action)
        {
            this.onResume += action;
        }
        public void RaiseResume()
        {
            if (onResume != null)
            {
                onResume();
            }

        }

        protected OnSomething onPause;
        public void RemovePauseEventHandler(OnSomething action)
        {
            this.onPause -= action;
        }

        public void AddPauseEventHandler(OnSomething action)
        {
            this.onPause += action;
        }
        public void RaisePause()
        {
            if (onPause != null)
            {
                onPause();
            }
        }



        protected OnSomething onAwake;
        public void RemoveAwakeEventHandler(OnSomething action)
        {
            this.onAwake -= action;
        }

        public void AddAwakeEventHandler(OnSomething action)
        {
            this.onAwake += action;
        }
        public void RaiseAwake()
        {
            if (onAwake != null)
            {
                onAwake();
            }
        }

        #endregion
        /***********************/


        #region sigleton instance
        /*** Para instanciar ***/
        private static DeviceEvents _instance;

        public DeviceEvents()
        {
            _instance = this;
        }

        public static DeviceEvents GetInstance()
        {
            return _instance;
        }
        #endregion
    }
}