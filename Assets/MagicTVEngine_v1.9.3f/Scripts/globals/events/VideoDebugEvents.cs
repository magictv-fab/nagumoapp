﻿using UnityEngine;
using System.Collections;

namespace MagicTV.globals.events
{
		public static class VideoDebugEvents
		{

				public delegate void OnFloatEventHandler (float p);
				public delegate void OnStringEventHandler (string v);
				public delegate void OnMetaEventHandler (string bundleID, string materialName, float float1,float float2);
				/// <summary>
				/// The event1 group.
				/// </summary>
				public static OnFloatEventHandler event1 ;
		
				public static void AddDebugFloat1EventHandler (OnFloatEventHandler action)
				{
						event1 += action;
				}
		
				public static void RemoveDebugFloat1EventHandler (OnFloatEventHandler action)
				{
						event1 -= action;
				}
		
				public static void RaiseDebugFloat1 (float result)
				{
						if (event1 != null) {
								event1 (result);
						}
				}
				/// <summary>
				/// The string shader name
				/// </summary>
				public static OnStringEventHandler stringEvent ;
		
				public static void AddChangeDebugShader (OnStringEventHandler action)
				{
						stringEvent += action;
				}
		
				public static void RemoveChangeDebugShader (OnStringEventHandler action)
				{
						stringEvent -= action;
				}
		
				public static void RaiseDebugShader (string result)
				{
						if (stringEvent != null) {
								stringEvent (result);
						}
				}

				
				/// <summary>
				/// The string shader name
				/// </summary>
				public static OnMetaEventHandler onSaveEvent ;
		
				public static void AddSaveEventHandler (OnMetaEventHandler action)
				{
						onSaveEvent += action;
				}
		
				public static void RemoveSaveEventHandler (OnMetaEventHandler action)
				{
						onSaveEvent -= action;
				}
		
				public static void RaiseSave (string bundleID, string materialName, float float1,float float2)
				{
						if (onSaveEvent != null) {
								onSaveEvent (bundleID, materialName, float1,float2);
						}
				}


				/// <summary>
				/// The event2 group
				/// </summary>
				public static OnFloatEventHandler event2 ;
		
				public static void AddDebugFloat2EventHandler (OnFloatEventHandler action)
				{
						event2 += action;
				}
		
				public static void RemoveDebugFloat2EventHandler (OnFloatEventHandler action)
				{
						event2 -= action;
				}
		
				public static void RaiseDebugFloat2 (float result)
				{
						if (event2 != null) {
								event2 (result);
						}
				}

				/// <summary>
				/// The event3 group
				/// </summary>
				public static OnFloatEventHandler event3 ;
		
				public static void AddDebugFloat3EventHandler (OnFloatEventHandler action)
				{
						event3 += action;
				}
		
				public static void RemoveDebugFloat3EventHandler (OnFloatEventHandler action)
				{
						event3 -= action;
				}
		
				public static void RaiseDebugFloat3 (float result)
				{
						if (event3 != null) {
								event3 (result);
						}
				}

				/// <summary>
				/// The event4 group
				/// </summary>
				public static OnFloatEventHandler event4 ;
		
				public static void AddDebugFloat4EventHandler (OnFloatEventHandler action)
				{
						event4 += action;
				}
		
				public static void RemoveDebugFloat4EventHandler (OnFloatEventHandler action)
				{
						event4 -= action;
				}
		
				public static void RaiseDebugFloat4 (float result)
				{
						if (event4 != null) {
								event4 (result);
						}
				}

				///ABAIXO , QUEM SERA ALTERADO QUE DEVE DAR RAISE

				/// <summary>
				/// The CHANGE event1 group
				/// </summary>
				public static OnFloatEventHandler change1 ;
		
				public static void AddChangeDebugFloat1EventHandler (OnFloatEventHandler action)
				{
						change1 += action;
				}
		
				public static void RemoveChangeDebugFloat1EventHandler (OnFloatEventHandler action)
				{
						change1 -= action;
				}
		
				public static void RaiseChangeDebugFloat1 (float result)
				{
						if (change1 != null) {
								change1 (result);
						}
				}


				

				/// <summary>
				/// The CHANGE event2 group
				/// </summary>
				public static OnFloatEventHandler change2 ;
		
				public static void AddChangeDebugFloat2EventHandler (OnFloatEventHandler action)
				{
						change2 += action;
				}
		
				public static void RemoveChangeDebugFloat2EventHandler (OnFloatEventHandler action)
				{
						change2 -= action;
				}
		
				public static void RaiseChangeDebugFloat2 (float result)
				{
						if (change2 != null) {
								change2 (result);
						}
				}

				/// <summary>
				/// The CHANGE event3 group
				/// </summary>
				public static OnFloatEventHandler change3 ;
		
				public static void AddChangeDebugFloat3EventHandler (OnFloatEventHandler action)
				{
						change3 += action;
				}
		
				public static void RemoveChangeDebugFloat3EventHandler (OnFloatEventHandler action)
				{
						change3 -= action;
				}
		
				public static void RaiseChangeDebugFloat3 (float result)
				{
						if (change3 != null) {
								change3 (result);
						}
				}


				/// <summary>
				/// The CHANGE event4 group
				/// </summary>
				public static OnFloatEventHandler change4 ;
		
				public static void AddChangeDebugFloat4EventHandler (OnFloatEventHandler action)
				{
						change4 += action;
				}
		
				public static void RemoveChangeDebugFloat4EventHandler (OnFloatEventHandler action)
				{
						change4 -= action;
				}
		
				public static void RaiseChangeDebugFloat4 (float result)
				{
						if (change4 != null) {
								change4 (result);
						}
				}
		}

}