﻿using UnityEngine;
using System.Collections;
using System;
namespace MagicTV.globals.events
{
	public class ContentEvents
	{

        
		public Action onUpdate;

		public void RaiseUpdate ()
		{
			if (onUpdate != null) {
//				Debug.LogError ("ContentEvents : UPDATE ");
				onUpdate ();
			}
		}

		public Action onUpdateComplete;

		public void RaiseUpdateComplete ()
		{
			if (onUpdateComplete != null) {
				onUpdateComplete ();
			}
		}
        
		/*** Para instanciar ***/
		private static ContentEvents _instance;

		public static ContentEvents GetInstance ()
		{
			if (ContentEvents._instance == null) {
				ContentEvents._instance = new ContentEvents ();
			}
			return ContentEvents._instance;
		}

	}
}