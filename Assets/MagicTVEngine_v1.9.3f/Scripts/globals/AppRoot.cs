using UnityEngine;
using System.Collections;
using MagicTV.abstracts.device;
using MagicTV.globals.events ;
//#if UNITY_IOS
//using System.Net.NetworkInformation;
//#endif

/// <summary>
/// App root e suas propriedades na memória
/// </summary>
namespace MagicTV.globals
{

	public class AppRoot
	{

		string info;

		protected static StateMachine _stateMachine ;
		/// <summary>
		/// The state machine.
		/// </summary>
		public static StateMachine stateMachine {
			get {
				return _stateMachine;
			}
			set {
				//if( _stateMachine != value ){
				_stateMachine = value;
				//Debug.LogWarning( "Ap AppRoot . stateMagine - set "+value ) ;
				AppRootEvents.GetInstance ().GetStateMachine ().RaiseChangeToState (value);
				//}
			}
		}

//		private bool _needToShowWhoTo = false ;
//		public bool needToShowWhoTo{
//			get{ return _needToShowWhoTo; }
//			set{  
//				if( value == true ){
//
//				}
//				_needToShowWhoTo = value ;
//			}
//		}

		protected static string _device_id ;
		
		public const string CONNECTION_TYPE_WIFI = "WIFI";
		public const string CONNECTION_TYPE_3G = "3G";
		/// <summary>
		/// The connection type
		/// </summary>
		public static string connection = "3G";
		/// <summary>
		/// The can track.
		/// </summary>
		public static bool canTrack = true;

		public const int TOTAL_DOWNLOAD_ATTEMPTS_ON_ERROR = 3 ;//TotalDownloadAttempsOnError

		/// <summary>
		/// The current last revision.
		/// </summary>
		public static string revision ;


		public const string USER_AGENT_TYPE_ANDROID = "MagicTV/Android";
		public const string USER_AGENT_TYPE_IOS = "MagicTV/iOS";

		//acesso direto a deviceInfo
		public static DeviceInfoAbstract deviceInfo ;

		/// <summary>
		/// The user_agent.
		/// </summary>
		public static string user_agent ;

		public static DeviceCacheFileManagerAbstract cacheFileManager;

		public static DeviceFileInfoAbstract deviceFileInfo ;

		public static string device_id {
			get { 
				return AppRoot.getDeviceId ();
			}
		}
				
		/// <summary>
		/// Gets the device identifier.
		/// </summary>
		/// <returns>The device identifier.</returns>
		public static string getDeviceId ()
		{
			if (AppRoot._device_id == null) {
                //				//pega o unique id do device
                //
                //				AppRoot._device_id = getMacAddress ();
                //				Debug.LogError (" MACADDRESS " + AppRoot._device_id);
                //				if (AppRoot._device_id == "") {
                
                // AppRoot._device_id = ExternalIntegration.Instance.getUniqueID();
                if (string.IsNullOrEmpty(AppRoot._device_id))
                {
                	AppRoot._device_id = SystemInfo.deviceUniqueIdentifier;    
				}
                //				}		
            }
			return AppRoot._device_id;
		}
//		static string macAddress = "";
		// Esse método retorna somente o primeiro numero do MacAddress
		// ******DESCONTINUADO*************
//		public static string getMacAddress ()
//		{
////			AndroidJavaObject mWiFiManager;
//			if (macAddress != "") {
//				return macAddress;
//			}
//
//			#if UNITY_IOS 
//			NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces ();
//
//			foreach (NetworkInterface adapter in nics) {
//				PhysicalAddress address = adapter.GetPhysicalAddress ();
//				if (address.ToString () != "") {
//					macAddress = address.ToString ();
//					return macAddress;
//				}
//			}
//			#endif
//
//			#if UNITY_ANDROID	
//
//			if (mWiFiManager == null) {
//				using (AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayerNativeActivity").GetStatic<AndroidJavaObject>("currentActivity")) {
//					mWiFiManager = activity.Call<AndroidJavaObject> ("getSystemService", new object[] {  "wifi" });
//				}
//			}
//			macAddress = mWiFiManager.Call<AndroidJavaObject> ("getConnectionInfo").Call<string> ("getMacAddress");
//			return macAddress;
//
////			AndroidJavaObject jo = new AndroidJavaObject ("magictv.getSystemService");
////			jo.Call ("getConnectionInfo", new object[] {  "wifi" });
//////			Debug.Log ("UNITY PRINTSCREEN " + path);
//
//
//			#endif
//
//
////			return macAddress = getAndroidMacAddress ();
//
//			return macAddress;
//		}

		public static void setDeviceId (string device)
		{
			if (AppRoot._device_id != null) {
				//Debug.LogWarning( "AppRoot . setDeviceId ~ DeviceId deve ser setado sómente uma vez. " ) ;
				return;
			}
			AppRoot._device_id = device;
		}

	}

}