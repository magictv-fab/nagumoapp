﻿using UnityEngine;
using System.Collections;
using MagicTV.abstracts.device;
using MagicTV.vo;

/// <summary>
/// Device file info json.
/// Mesma implementação de banco só que baseado em json e classes
/// @version: 1.0
/// @author: Renato Seiji Miawaki
/// </summary>
using System.Collections.Generic;
using MagicTV.utils;
using System.Linq;
using ARM.utils.io;
using System.IO;
using LitJson;


namespace MagicTV.globals
{
	public class DeviceFileInfoJson : DeviceFileInfoAbstract
	{
		public string _localFolderOfJson = "deviceInfo";
		protected string _fileName = "revision.json";
		protected Dictionary<string, List<BundleVO> > _indexedBundles;
		protected Dictionary<string, BundleVO[]> _indexedBundlesArray;
		ResultRevisionVO _currentRevisionResult ;
		bool _prepareAllreadyCalled;

		public DeviceFileInfoJson(){
			instance = this;
		}

		public override void prepare ()
		{
			if(this._isComponentReady){
				this.RaiseComponentIsReady ();
				return;
			}
			if(_prepareAllreadyCalled){
				Debug.LogError ("DeviceFileInfoJson . Prepare Allready Called");	
				return;
			}
			_prepareAllreadyCalled = true;
			Debug.LogError ("DeviceFileInfoJson . prepare");
			
			AppRoot.deviceFileInfo = this;
			if (this._indexedBundles == null) {
				this._indexedBundles = new Dictionary<string, List<BundleVO> > ();
				this._indexedBundlesArray = new Dictionary<string, BundleVO[]> ();
			}

			//TODO: pegar o arquivo json local e por na memória :) _currentRevisionResult
			this.getResultRevisionFromFile ();
		}
		string getJsonLocalFileUrl(){
			return BundleHelper.GetPersistentPath (_localFolderOfJson + "/" + _fileName) ;
		}
		string getJsonLocalFolder(){
			return BundleHelper.GetPersistentPath (_localFolderOfJson ) ;
		}
		void getResultRevisionFromFile ()
		{
			string file = this.getJsonLocalFileUrl();
			Debug.LogError ("!!!!!!!!!@@@@@@@@@@@ !!!!!!! DeviceFileInfoJson . getResultRevisionFromFile "+file);
			
			if (ARMFileManager.Exists (file)) {
				//le o conteudo
				Debug.LogError ("!!!!!!!!!@@@@@@@@@@@ !!!!!!! DeviceFileInfoJson . getResultRevisionFromFile le o conteudo ");
				WWW wfile = new WWW("file://"+file);
				StartCoroutine(DoReadFile(wfile));
				return;
			}
			Debug.LogError ("!!!!!!!!!@@@@@@@@@@@ !!!!!!! DeviceFileInfoJson . getResultRevisionFromFile COMPLETE ");
			//Revisão nao existente localmente
			this.RaiseComponentIsReady ();
		}
		private IEnumerator DoReadFile (WWW www)
		{
			yield return www;
			
			this.FinishedRead ( www ) ;
		}
		/// <summary>
		/// Terminou de ler o json interno
		/// </summary>
		/// <param name="www">www.</param>
		void FinishedRead (WWW www)
		{
			Debug.LogError ("!!!!!!!!!@@@@@@@@@@@ !!!!!!! DeviceFileInfoJson . FinishedRead "+www.text);
			if(www.error == null){
				try {
					
					SetCurrentRevision(LitJson.JsonMapper.ToObject<ResultRevisionVO> (www.text) );
					populateIndexedBundles () ;
				} catch (JsonException ex) {
					Debug.LogError (".INFO parser error");
				}
			}
			Debug.LogError ( "this._currentRevisionResult:" + this._currentRevisionResult );
			this.RaiseComponentIsReady ();
		}

		#region implemented abstract members of DeviceFileInfoAbstract


		public override void populateIndexedBundles ()
		{
			ResultRevisionVO revision = this.getCurrentRevision ();
			if(revision == null){
				return;
			}
			List<MetadataVO> trackings = revision.bundles.SelectMany (b => b.metadata).Where (m => m.key == "TrackingID").Distinct ().ToList ();
			
			_indexedBundles.Clear ();
			
			trackings.ForEach (
				t => addBundle (t.value, revision.bundles.Where (b => b.metadata.Any (m => m.id == t.id)).ToList ())
				);
		}

		void addBundle (string value, List<BundleVO> list)
		{
			tracks.Add (value);
			if (_indexedBundles.ContainsKey (value)) {
				for (int i = 0; i < list.Count; i++) {
					_indexedBundles [value].Add (list [i]);
				}
				this.cacheBundle (value);
				return;
				
			}
			_indexedBundles.Add (value, list);
			cacheBundle (value);
			
			return;
		}
		void cacheBundle (string value)
		{
			if (_indexedBundlesArray.ContainsKey (value)) {
				_indexedBundlesArray [value] = _indexedBundles [value].ToArray ();
				return;
			}
			_indexedBundlesArray.Add (value, _indexedBundles [value].ToArray ());
		}
		#endregion

		public override ReturnDataVO commit (ResultRevisionVO revisionResult)
		{
			Debug.LogError ("DeviceFileInfoJson . commit "+revisionResult.revision);

			//esse é O CARA que recebe o grande commit a se comparar e atualizar.
			//precisa apagar arquivos físicos, ou marcarem para ser apagado.
			//anota quais as urls que precisam ser deletadas
			this.setUrlsToDelete  ( this._currentRevisionResult, ref revisionResult ); 
			//poe na memória, mas antes atualiza as urls pra ver o que já baixou
			SetCurrentRevision(this.getFilteredRevision( revisionResult ));
			// this._currentRevisionResult = this.getFilteredRevision( revisionResult ) ;

			Debug.LogError ( "this._currentRevisionResult:"+this._currentRevisionResult );
			return new ReturnDataVO (){ success=true };
		}
		List<UrlInfoVO> _urlsToDelete;
		/// <summary>
		/// Sets the urls to delete.
		/// Seta como status -1 as urls que precisam ser deletadas pois já não fazem parte do mesmo grupo
		/// </summary>
		/// <param name="current">Current.</param>
		/// <param name="newOne">New one.</param>
		void setUrlsToDelete (ResultRevisionVO current, ref ResultRevisionVO newOne)
		{
			_urlsToDelete = new List<UrlInfoVO>() ;
			UrlInfoVO[] currentsUrls = this.revisionToUrls( current ) ;
			UrlInfoVO[] newUrls = this.revisionToUrls( newOne ) ;
			//agora faz o diff entre uma url e outras, comparando pelo id

			foreach( UrlInfoVO u in currentsUrls ){
				//verifica se a url local está na lista das novas
				if( filterUrlFromId( newUrls, u.id ) == null ){
					//se não está na lista das novas, deveria ser deletado

					u.status = -1 ;
					//atualiza a revisao enviada que tem essa url pra deletar
					upgradeUrlIntoRevision ( u, ref newOne );
					_urlsToDelete.Add (u) ;
				}
			}

		}

		protected UrlInfoVO filterUrlFromId( UrlInfoVO[] urls, string id ){
			if( urls == null ){
				return null ;
			}
			if( urls.Length == 0 ){
				return null ;
			}
			return urls.Where (u => u.id == id).DefaultIfEmpty(null).Single () ;

		}
		/// <summary>
		/// Gets the filtered revision.
		/// Esse metodo consiste em pegar a revisão enviada e verificar se tem urls enviadas relacionadas com as atuais
		/// </summary>
		/// <returns>The filtered revision.</returns>
		/// <param name="revisionResult">Revision result.</param>
		ResultRevisionVO getFilteredRevision (ResultRevisionVO revisionResult)
		{
			if(this._currentRevisionResult == null){
				//não tem nada gravado na revisão atual
				return revisionResult ;
			}
			//TODO: verificar os arquivos locais para setar o que já foi baixado em relação ao que já está na memória
			//precisa fazer um diff pra manter as informações
			UrlInfoVO[] currentsUrls = this.revisionToUrls( this._currentRevisionResult ) ;
			foreach( BundleVO b in revisionResult.bundles ) {
				foreach( FileVO f in b.files ){
					for (int ui = 0; ui < f.urls.Length; ui++) {
						//atualiza na memoria da nova revisão a url local info caso seja necessaria, pois se o arquivo já foi baixado, não precisa mais setar como baixado
						Debug.LogError("###### + " + f.urls[ui] );
						f.urls[ui] = getUrlLocalInfoFiltered( f.urls[ui], currentsUrls );
						Debug.LogError("###### + " + f.urls[ui] );
					}

				}
			}
			

			//precisa deletar as urls que local existem mas online não

			return revisionResult ;
		}

		UrlInfoVO[] revisionToUrls (ResultRevisionVO revisionResult)
		{

			if(revisionResult == null){
				
				return new UrlInfoVO[0];
			}
			
			return revisionResult.bundles.SelectMany(b => b.files.SelectMany(f => f.urls)).ToArray();

			// pra quem não tem fé  eé desconfiado
			List<UrlInfoVO> urls = new List<UrlInfoVO> ();
			foreach( BundleVO b in revisionResult.bundles ) {
				
				foreach( FileVO f in b.files ){
					
					for (int ui = 0; ui < f.urls.Length; ui++) {
						urls.Add (f.urls[ui]);
					}
					
				}
			}
			return urls.ToArray ();
			//Abaixo o modo como deveria ser se fosse tudo lindo...
			
		}

		protected UrlInfoVO getUrlLocalInfoFiltered( UrlInfoVO novaUrl, UrlInfoVO[] currents){

			UrlInfoVO u = filterUrlFromId ( currents, novaUrl.id ) ;
			if( u == null ){
				return novaUrl ;
			}
			if(novaUrl.md5 != u.md5){
				//o arquivo tem o mesmo id, porém não é o mesmo, então não é para manter o local
				return novaUrl ;
			}
			//esse não é novo, precisa retornar com as infos que já tem local
			return u.Clone ();
		}
		/// <summary>
		/// Commit the specified urlInfoVO.
		/// Aqui deve-se atualizar o UrlInfoVO na revisão atual
		/// Precisa decidir o momento de se persistir essa informação
		/// </summary>
		/// <param name="urlInfoVO">URL info V.</param>
		public override ReturnDataVO commit (UrlInfoVO urlInfoVO)
		{
			ReturnDataVO retorno = new ReturnDataVO();
			if(this._currentRevisionResult == null){
				return retorno ;
			}
			//var bla = ( from r in this._currentRevisionResult where 1 select r   );
			upgradeUrlIntoRevision ( urlInfoVO, ref this._currentRevisionResult );
			retorno.success = true ;

			return retorno;
		}
		/// <summary>
		/// Upgrades the URL into revision.
		/// </summary>
		/// <param name="urlInfoVO">URL info V.</param>
		/// <param name="revision">Revision.</param>
		void upgradeUrlIntoRevision (UrlInfoVO urlInfoVO, ref ResultRevisionVO revision)
		{
			//url = urlInfoVO.Clone ();
			foreach( BundleVO b in revision.bundles ) {
				foreach( FileVO f in b.files ){
					if( f.id == urlInfoVO.bundle_file_id ){
						for (int ui = 0; ui < f.urls.Length; ui++) {
							if( f.urls[ui].id == urlInfoVO.id ){
								f.urls[ui] = urlInfoVO.Clone() ;
								return;
							}
						}
						//não encontrou essa url na lista precisa adicionar no pensamento de commit
						List<UrlInfoVO> urlsTemp = f.urls.ToList ();
						urlsTemp.Add (urlInfoVO);
						f.urls = urlsTemp.ToArray() ;
					}
				}
			}

		}

		/// <summary>
		/// Delete the specified obj.
		/// Aqui ele só deleta da memória, não consolida automaticamente
		/// </summary>
		/// <param name="obj">Object.</param>
		public override ReturnDataVO delete (UrlInfoVO obj)
		{
			//procurando pelo ponteiro
			if(this._currentRevisionResult == null){
				return new ReturnDataVO (){ success = false };
			}

			//pesquisa o File que tenha essa url
			FileVO file = this.getFileById (obj.bundle_file_id);
			List<UrlInfoVO> urls = new List<UrlInfoVO> () ;
			foreach( UrlInfoVO u in file.urls ){
				if(u.id != obj.id){
					urls.Add ( u ) ;
				}
			}
			//verificar se é ponteiro e se ele altera a variavel diretamente
			file.urls = urls.ToArray ();
			foreach( BundleVO b in this._currentRevisionResult.bundles ) {
				for (int ui = 0; ui < b.files.Length; ui++) {
					if(b.files[ui].id == file.id){
						b.files[ui] = file;
					}
				}
			}
			return new ReturnDataVO (){ success = true };
		}

		FileVO getFileById (string bundle_file_id)
		{
			return this._currentRevisionResult.bundles.SelectMany ( b => b.files ).Where (f => f.id == bundle_file_id).ToList<FileVO>().DefaultIfEmpty( null ).First() ;
		}

		public override ReturnDataVO delete (UrlInfoVO[] objs)
		{
			if( objs == null || objs.Length == 0 ){
				return new ReturnDataVO (){ success = false };
			}
			foreach(UrlInfoVO u in objs){
				delete (u);
			}
			Save ();
			return new ReturnDataVO (){ success = true };
		}
		public override BundleVO[] getBundles (string appType)
		{
			if (this._indexedBundles.ContainsKey (appType)) {
				return this._indexedBundles[appType].ToArray() ;
			}
			return new BundleVO[0];
		}
		public override ResultRevisionVO getCurrentRevision ()
		{
			return this._currentRevisionResult ;
		}
		public override ResultRevisionVO getRevisionByRevisionId (string revision_id)
		{
			throw new System.NotImplementedException ();
		}
		public override ResultRevisionVO getUncachedRevision ()
		{
			ResultRevisionVO _ResultRevisionVO = this.getCurrentRevision ();
			
			//Filtra somente os arquivos que ainda nao foram baixados
			ResultRevisionVO result = new ResultRevisionVO {
				revision = _ResultRevisionVO.revision,
				name = _ResultRevisionVO.name,
				short_description = _ResultRevisionVO.short_description,
				long_description = _ResultRevisionVO.long_description,
				bundles = (from bx in _ResultRevisionVO.bundles
				           //where bx.files.Any(bxf => bxf.type == "tracking.zip")
				           select new BundleVO {
					id = bx.id,
					name = bx.name,
					revision_id = bx.revision_id,
					context = bx.context,
					files = (from fx in bx.files
					         where fx.type == "tracking.zip"
					         select new FileVO {
						id = fx.id,
						bundle_id = fx.bundle_id,
						type = fx.type,
						urls = (from ux in fx.urls
						        where string.IsNullOrEmpty(ux.local_url) && ux.status != -1 //-1 é para deletar, então não pode pedir pra baixar
						        select new UrlInfoVO {
							id = ux.id,
							bundle_file_id = ux.bundle_file_id,
							type = ux.type,
							md5 = ux.md5,
							url = ux.url,
							size = ux.size,
							local_url = ux.local_url,
							status = ux.status
						}).ToArray()
					}).ToArray(),
					metadata = (from mx in bx.metadata
					            select new MetadataVO {
//						tabela = "bundle_metadata",
//						tabela_id = bx.id,
						id = mx.id,
						key = mx.key,
						value = mx.value,
					}).ToArray(),
					published_at = bx.published_at,
					expired_at = bx.expired_at
				}).ToArray(),
				metadata = (from mx in _ResultRevisionVO.metadata
				            select new MetadataVO {
//					tabela = "app_metadata",
//					tabela_id = _ResultRevisionVO.revision,
					id = mx.id,
					key = mx.key,
					value = mx.value,
				}).ToArray()
			};
			
			// Remove bundles sem arquivos e sem urls
			result.bundles = result.bundles.Where (b => b.files.Count (f => f.urls.Count () > 0) > 0).ToArray ();
			//Debug.LogError ("DeviceFileInfo . getUncachedRevision filter: " + result.ToString ());
			return result;
		}
		/// <summary>
		/// Gets the URL info VO to delete.
		/// TODO: Testar, mas lembrar que precisa mudar para -1 o status caso queira deletar
		/// </summary>
		/// <returns>The URL info VO to delete.</returns>
		public override UrlInfoVO[] getUrlInfoVOToDelete ()
		{
			if(_urlsToDelete == null || _urlsToDelete.Count() == 0){
				return new UrlInfoVO[0];
			}
			return _urlsToDelete.ToArray () ;
			//no pensamento em memória, não precisa mais anotar qual é que precisa deletar, e sim, deleta-lo
			//this.revisionToUrls( this._currentRevisionResult ).Where( u => u.status == -1 ).ToArray<UrlInfoVO>() ;
		}
		/// <summary>
		/// Retorna todas as urls Info vo
		/// </summary>
		/// <returns>The all URL info V.</returns>
		public override UrlInfoVO[] getAllUrlInfoVO (){
			return this.revisionToUrls( this._currentRevisionResult ) ;
		}
		/// <summary>
		/// retorna as url infos vos setadas como baixadas
		/// </summary>
		/// <returns>The cached URL info V.</returns>
		public override UrlInfoVO[] getCachedUrlInfoVO (){
			return this.revisionToUrls (this._currentRevisionResult).Where ( u => u.status > 0 ).ToArray()  ;
		}
		protected void SetCurrentRevision( ResultRevisionVO revisionVO){
			Debug.LogError( "  ~@~~@~~@~~@~~@~  Setando o Revision (procurando o x9) = " + (revisionVO == null ? "VEIO NULL" : "TEM COISA AI" ) );
			_currentRevisionResult = revisionVO;
		}

		public override void Save ()
		{
			//TODO: salva o que está na memória em arquivo local
			if(this._currentRevisionResult != null){
				populateIndexedBundles () ;
				string folder = this.getJsonLocalFolder() ;
				ARMFileManager.CreateFolderRecursivelyForPath( folder ) ;
				string contentJson = LitJson.JsonMapper.ToJson( this._currentRevisionResult ) ;
				//ARMFileManager.DeleteFile( this.getJsonLocalFileUrl() ) ;

				if (File.Exists (this.getJsonLocalFileUrl())) {
					System.IO.File.Delete (this.getJsonLocalFileUrl());		
				}
				System.IO.File.WriteAllBytes ( this.getJsonLocalFileUrl(), System.Text.Encoding.UTF8.GetBytes( contentJson ) ) ;
				//ARMFileManager.WriteBites( this.getJsonLocalFileUrl(), System.Text.Encoding.UTF8.GetBytes( contentJson ) ) ;
			}
		}
	}
}