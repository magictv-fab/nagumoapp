using UnityEngine;
using System.Collections;
using MagicTV.abstracts ;
using MagicTV.abstracts.device ;
using MagicTV.vo ;
using MagicTV.vo.result ;

using MagicTV.globals ;
using System;
using System.Collections.Generic;
using System.Linq;
using ARM.utils.io;
using MagicTV.utils;

public class FakeDeviceFileInfo : DeviceFileInfoAbstract
{
	#region implemented abstract members of DeviceFileInfoAbstract

	public override void populateIndexedBundles ()
	{
		throw new NotImplementedException ();
	}

	#endregion

	public override UrlInfoVO[] getAllUrlInfoVO ()
	{
		throw new NotImplementedException ();
	}
	public override UrlInfoVO[] getCachedUrlInfoVO ()
	{
		throw new NotImplementedException ();
	}
		

	// Use this for initialization
	public override void prepare ()
	{
		AppRoot.deviceFileInfo = this;
		this.RaiseComponentIsReady ();
	}


	/// <summary>
	/// Gets the revision by revision identifier.
	/// </summary>
	/// <returns>The revision by revision identifier.</returns>
	/// <param name="revision_id">Revision_id.</param>
	public override ResultRevisionVO getRevisionByRevisionId (string revision_id)
	{
		return null;
	}

	/// <summary>
	/// Gets the URL info VO to delete .
	/// </summary>
	/// <returns>The URL info VO to delete.</returns>
	public override UrlInfoVO[] getUrlInfoVOToDelete ()
	{
		return null;
	}
	/// <summary>
	/// Delete the specified obj forever
	/// </summary>
	/// <param name="obj">Object.</param>
	public override ReturnDataVO delete (UrlInfoVO obj)
	{
		return null;
	}
	/// <summary>
	/// Delete the specified list of objs forever
	/// </summary>
	/// <param name="objs">Objects.</param>
	public override ReturnDataVO delete (UrlInfoVO[] objs)
	{
		return null;
	}

	ResultRevisionVO _ResultRevisionVO ;

	/// <summary>
	/// Gets the current revision.
	/// </summary>
	/// <returns>The current revision.</returns>
	public override ResultRevisionVO getCurrentRevision ()
	{

		return _ResultRevisionVO;
	}

	/// <summary>
	/// Saves the revision.
	/// </summary>
	/// <returns>The revision.</returns>
	/// <param name="revisionResult">Revision result.</param>
	public override ReturnDataVO commit (ResultRevisionVO revisionResult)
	{
		_ResultRevisionVO = revisionResult;
		return new ReturnDataVO (){ success=true };
	}
		
	/// <summary>
	/// Saves the revision.
	/// </summary>
	/// <returns>The revision.</returns>
	/// <param name="revisionResult">Revision result.</param>
	public override ReturnDataVO commit (UrlInfoVO urlInfo)
	{
//				//Debug.LogError ("DeviceFileInfo . commit: urlInfo" + urlInfo.ToString ());
		return new ReturnDataVO (){ success=true };
	}

	public ReturnDataVO commit (KeyValueVO obj)
	{
		return new ReturnDataVO (){ success=true };
	}

	public override BundleVO[] getBundles (string appContext)
	{
				
		BundleVO[] _BundlesVO = (from bx in _ResultRevisionVO.bundles
		                         where bx.context == appContext
		                         select bx).ToArray ();
		return _BundlesVO;
	}
	
	
	/// <summary>
	/// Retorna todos os arquivos que ainda não foram baixados para baixar e informações da revisão
	/// </summary>
	/// <returns>The uncached revision.</returns>
	public override ResultRevisionVO getUncachedRevision ()
	{
		//TODO: ver se precisa baixar tudo ou nada
		if (_ResultRevisionVO != null) {
			if (_ResultRevisionVO.bundles != null && _ResultRevisionVO.bundles.Length > 0) {
				if (ARMFileManager.Exists (BundleHelper.GetBundleLocalPath (_ResultRevisionVO.bundles [0].context, _ResultRevisionVO.bundles [0].id))) {
					return null;
				}
			}
		}
			
		return _ResultRevisionVO;
	}
	public void setImagesSplash (string[] imagesUrl)
	{
		//
	}
	public string getImageSplash ()
	{
		return "";
	}
	public override void Save ()
	{
		//TODO: salva o que está na memória
		
	}
}
