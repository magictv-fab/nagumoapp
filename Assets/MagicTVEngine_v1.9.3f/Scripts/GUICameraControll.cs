﻿using UnityEngine;
using System.Collections;

public class GUICameraControll : MonoBehaviour
{
    public PinchToutchController _pinch;

    float _zoomFactor = 0f;
    float _rotateFactor = 0f;

    float _verticalFactor;
    float _sideFactor;

    Vector3 _originalPos;
    Quaternion _originaRot;

    // Use this for initialization
    void Start()
    {
        _originalPos = _pinch.gameObject.transform.position;
        _originaRot = _pinch.gameObject.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        //		
        if (_zoomFactor != 0f)
        {
            //_pinch.currentState = TouchControllerState.Scalling;
            //_pinch.deltaZoom += _zoomFactor;
            _pinch.OnScroll(_zoomFactor);
        }

        if (_rotateFactor != 0f)
        {
            _pinch.gameObject.transform.Rotate(Vector3.up * _rotateFactor * 2);
        }

        if (_verticalFactor != 0f)
        {
            _pinch.gameObject.transform.position = new Vector3(_pinch.gameObject.transform.position.x,
                                                                  _pinch.gameObject.transform.position.y + _verticalFactor,
                                                                  _pinch.gameObject.transform.position.z
            );
        }

        if (_sideFactor != 0f)
        {
            _pinch.gameObject.transform.position = new Vector3(_pinch.gameObject.transform.position.x + _sideFactor,
                                                                  _pinch.gameObject.transform.position.y,
                                                                  _pinch.gameObject.transform.position.z
            );
        }

    }

    public void ResetAll()
    {
        _pinch.gameObject.transform.position = _originalPos;
        _pinch.gameObject.transform.rotation = _originaRot;

        Application.LoadLevel(Application.loadedLevel);
    }

    public void Stop()
    {
        _zoomFactor = 0f;
        _verticalFactor = 0f;
        _sideFactor = 0f;
        _rotateFactor = 0f;
    }

    public void RotateLeft()
    {
        _rotateFactor = 1f;
    }

    public void RotateRight()
    {
        _rotateFactor = -1f;
    }

    public void ZoomIN()
    {
        _zoomFactor = 1f;
    }

    public void ZoomOUT()
    {
        _zoomFactor -= 1f;
    }

    public void MoveUP()
    {
        _verticalFactor = 0.1f;
    }
    public void MoveDown()
    {
        _verticalFactor = -0.1f;
    }

    public void MoveLeft()
    {
        _sideFactor = 0.1f;
    }
    public void MoveRight()
    {
        _sideFactor = -0.1f;
    }
}