﻿using UnityEngine;
using System.Collections;

/// <summary>
/// @author : Renato Seiji Miawaki
/// 
/// RequestImagesItemVO seria a informação de cada imagem para download vinda do webservice de imagens
/// </summary>
namespace MagicTV.vo
{
	public class RequestImagesItemVO
	{

		/// <summary>
		/// A URL da imagem no servidor
		/// </summary>
		public string url ;

		/// <summary>
		/// O tamanho da imagem em bytes no servidor
		/// </summary>
		public string size ;

		/// <summary>
		/// O tempo que uma imagem precisa ficar até que apareça outra, na apresentação de slides
		/// </summary>
		public string tempo ;
		
		public override string ToString ()
		{
			return "[RequestImagesItemVO] " + LitJson.JsonMapper.ToJson (this);
		}
	}
}