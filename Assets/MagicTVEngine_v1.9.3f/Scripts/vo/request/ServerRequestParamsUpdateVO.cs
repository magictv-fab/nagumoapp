﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Server request parameters update VO
/// v2
/// </summary>
namespace MagicTV.vo.request
{
		public class ServerRequestParamsUpdateVO
		{
				private string _revision ;
				public string revision {
						get {
								if (this._revision == null) {
										return ServerRequestParamsUpdateConstants.REVISION_NOREV;
								}
								return this._revision;
						}
						set {
								this._revision = value;
						}
				}

				public string device_id ;
				public string app ;

				public string api = "v2.0";
				
				public string user_agent ;

				public string connection ;

				/// <summary>
				/// Tos the URL string to request.
				/// </summary>
				/// <returns>The URL string.</returns>
				public virtual string ToURLString ()
				{
						//alvo:
						return   "params%5Bdevice_id%5D=" + this.device_id + "&params%5Bapp%5D=" + this.app + "&params%5Buser_agent%5D=" + this.user_agent + "&params%5Bapi%5D=" + this.api + "&params%5Bconnection%5D=" + this.connection + "&params%5Bconnection%5D=" + this.connection + "&params%5Brevision%5D=" + this.revision;
				}
		}
}