﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Server request parameters update VO
/// 
/// v1
/* 
{
"device_id": "dsaoudh0p28ey21vedweydg023gh",
"app": "ID do APP - Constante",
"user_agent": "MagicTV/Android" | "MagicTV/iOS",
"connection": "3G" | "WIFI",
"revision": "ID da última revisão" | "NOREV"
}
*/
///
/// v2
/* 
{
    "jsonrpc": "2.0",
    "method": "update",
    "params": {
        "device_id": "dsaoudh0p28ey21vedweydg023gh",
        "app": "mtv-lite",
        "user_agent": "MagicTV/Android",
        "connection": "3G",
        "revision": "NOREV",
        "api": "v2.0",
        "roles": [""]
    },
    "id": 1
}
*/
/// </summary>
using System.Collections.Generic;


namespace MagicTV.vo.request
{
		public class ServerMetadataParamsVO:ServerRequestParamsUpdateVO
		{
				
				public List<MetadataVO> metas = new List<MetadataVO> ();
				/// <summary>
				/// Tos the URL string to request.
				/// </summary>
				/// <returns>The URL string.</returns>
				public override string ToURLString ()
				{
						string resultString = "";
						if (metas.Count > 0) {
								for (int i = 0; i < metas.Count; i++) {
										
										resultString += "&params%5Bmetadata%5D%5B" + i + "%5D%5Bkey%5D=" + metas [i].key + "&params%5Bmetadata%5D%5B" + i + "%5D%5Bvalue%5D=" + metas [i].value;

								}
						}
						return base.ToURLString () + resultString;
				}
		}
}