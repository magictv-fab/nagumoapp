﻿using UnityEngine;
using System.Collections;

/***

{
  "jsonrpc": "2.0",
  "method": "update",
  "params": {
    "device_id": "dsaoudh0p28ey21vedweydg023gh",
    "app": "ID do APP - Constante",
    "user_agent": "MagicTV/Android" | "MagicTV/iOS",
    "connection": "3G" | "WIFI",
    "revision": "ID da última revisão" | "NOREV"
  },
  "id": 1
}

 * **/
namespace MagicTV.vo.request
{

		public class ServerRequestUpdateVO
		{

				public string jsonrpc = "2.0" ;
				public string method = "update" ;
				
				[JsonProperty( PropertyName = "params" )]
				public ServerRequestParamsUpdateVO
						param;

				public int id ;

				protected static int _id = 1;
				protected static int getId ()
				{
						return ServerRequestUpdateVO._id ++;
				}
				public ServerRequestUpdateVO ()
				{
						this.id = ServerRequestUpdateVO.getId ();
				}

		}
}