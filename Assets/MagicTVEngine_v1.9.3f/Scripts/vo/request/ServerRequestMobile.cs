﻿using UnityEngine;
using System.Collections;

/***

{
  "jsonrpc": "2.0",
  "method": "update",
  "params": {
    "device_id": "dsaoudh0p28ey21vedweydg023gh",
    "app": "ID do APP - Constante",
    "user_agent": "MagicTV/Android" | "MagicTV/iOS",
    "connection": "3G" | "WIFI",
    "revision": "ID da última revisão" | "NOREV"
  },
  "id": 1
}

 * **/
namespace MagicTV.vo.request
{

	public class ServerRequestMobile
		{
		//{"jsonrpc":"2.0","id":4797,"result":true}
				public string jsonrpc = "2.0" ;
				public bool result ;
				public int id ;

		}
}