﻿using UnityEngine;
using System.Collections;

namespace MagicTV.vo.request
{
		
	public class ServerRequestDeviceVO
	{
			
		public string jsonrpc = "2.0" ;
		public int id ;
		public bool result;
	}
}
