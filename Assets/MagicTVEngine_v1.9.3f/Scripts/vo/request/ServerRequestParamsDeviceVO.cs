﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Server request parameters update VO
/// v2
/// </summary>
namespace MagicTV.vo.request
{
	public class ServerRequestParamsDeviceVO:ServerRequestParamsUpdateVO
	{
				
		public string token ;

		/// <summary>
		/// Tos the URL string to request.
		/// </summary>
		/// <returns>The URL string.</returns>
		public override string ToURLString ()
		{
				//alvo:
				return   base.ToURLString()+"&params%5Btoken%5D=" + this.token ;
		}

	}
}