﻿using UnityEngine;
using System.Collections;

/***
 {
    "device_id": "dsaoudh0p28ey21vedweydg023gh",
    "app": "ID do APP - Constante",
    "user_agent": "MagicTV/Android" | "MagicTV/iOS",
    "connection": "3G" | "WIFI",
    "revision": "ID da última revisão" | "NOREV"
  }
 ***/
namespace MagicTV.vo.request{
	public class ServerRequestParamsUpdateConstants{

		public const string REVISION_NOREV 			= "NOREV" ;
		public const string UserAgent_Android 		= "MagicTV/Android" ;
		public const string UserAgent_iOS 			= "MagicTV/iOS" ;
		public const string ConnectionsType_3G 		= "3G" ;
		public const string ConnectionsType_WiFI 	= "WIFI" ;

	}
}