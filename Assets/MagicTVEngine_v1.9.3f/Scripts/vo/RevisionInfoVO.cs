﻿using UnityEngine;
using System.Collections;

/// <summary>
/// RevisionInfoVO com o resultado de uma consulta
/// </summary>
namespace MagicTV.vo
{

		public class RevisionInfoVO  : VOWithId
		{

				public string jsonrpc ;
				public ResultRevisionVO result ;

				public ErrorResultVO error ;

		
				public override string ToString ()
				{
						return "[RevisionInfoVO] " + LitJson.JsonMapper.ToJson (this);
				}
		}
}