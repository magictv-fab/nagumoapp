﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ARM.utils.io;

namespace MagicTV.vo
{

	public class FileVO : VOWithId
	{

		public string bundle_id;
		public string type ;
		public UrlInfoVO[] urls = new UrlInfoVO[0];

		public override string ToString ()
		{
			return "[FileVO] { id:" + id + " bundle_id:" + bundle_id + " } total " + urls.Length + " urls ";
		}
		public UrlInfoVO[] getByExtension (string ext)
		{
			List<UrlInfoVO> urlsFiltered = new List<UrlInfoVO> ();

			foreach (UrlInfoVO url in this.urls) {
				string extension = ARMFileManager.GetExtentionOfFile (url.url);
				if (extension == ext) {
					//ignorando zips - para track não funciona questão de qualidade high low
					urlsFiltered.Add (url);
				}
			}
			return urlsFiltered.ToArray ();
		}
	}

}