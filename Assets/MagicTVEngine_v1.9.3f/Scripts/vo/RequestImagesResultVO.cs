﻿using UnityEngine;
using System.Collections;

/// <summary>
/// RevisionInfoVO com o resultado de uma consulta
/// </summary>
namespace MagicTV.vo
{

	public class RequestImagesResultVO
	{

		public string hash ;

		public RequestImagesItemVO[] imagens ;
		
		public override string ToString ()
		{
			return "[RequestImagesResultVO] " + LitJson.JsonMapper.ToJson (this);
		}
	}
}