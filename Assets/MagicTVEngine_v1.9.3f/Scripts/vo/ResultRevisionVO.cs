﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MagicTV.vo
{
		public class ResultRevisionVO
		{

				public string revision ;
				public string name ;
				public string short_description ;
				public string long_description ;
				public BundleVO[] bundles = new BundleVO[0];
				public MetadataVO[] metadata = new MetadataVO[0];
				public string pin ;
				public override string ToString ()
				{
						return "[ResultRevisionVO] " + LitJson.JsonMapper.ToJson (this);
				}
		}

}