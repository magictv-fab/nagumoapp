using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;


/// <summary>
/// Bundle VO é a representaçao de um bundle que pode conter vários arquivos de um mesmo contexto
/// Cada apresentação tem um bundle e nele todos os arquivos e informações referentes a ela
/// </summary>
using System;
using System.Globalization;

namespace MagicTV.vo
{
	

		public class BundleVO : VOWithId
		{

				public BundleVO ()
				{
				}
				
				public string revision_id;
				public string name;
				public string context ;
				public FileVO[] files = new FileVO[0];
				
				public MetadataVO[] metadata = new MetadataVO[0];

				public DateTime? published_at ;
				public DateTime? expired_at ;

				public BundleVO[] children = new BundleVO[0];
				
				public bool isDateActive ()
				{
						//TODO: validar se a data está ativa, ou seja, se hoje está entre published_at e expired_at
//						DateTime datePublished 	= DateTime.Parse ( this.published_at , DateTimeStyles.AssumeUniversal ) ;
//						DateTime dateExpired 	= DateTime.Parse ( this.expired_at ) ;

						//Debug.LogWarning (this.published_at);
//			//Debug.LogWarning ( dateExpired ) ;
//			//Debug.LogWarning ( datePublished ) ;

						return true;

				}
				
				public override string ToString ()
				{
						return "[BundleVO] " + LitJson.JsonMapper.ToJson (this);
				}

		}

}