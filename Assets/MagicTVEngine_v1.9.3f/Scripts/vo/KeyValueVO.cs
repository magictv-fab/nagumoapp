﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MagicTV.vo{
	public class KeyValueVO {

		public string id ;
		public string key ;
		public string value ;

		public override string ToString ()
		{
			return "[ResultRevisionVO] " + LitJson.JsonMapper.ToJson (this);
		}
	}
}