﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Object to key, type and value
/// Passe o valor em float, se quiser diga que o tipo é int
/// </summary>
namespace MagicTV.vo
{
		public class KeyTypeValue
		{

				public const string TYPE_INT = "int";
				public const string TYPE_FLOAT = "float";

				public string type;
				public string key;
				public float value;

				public float getValueFloat ()
				{
						return value;
				}
				public int getIntValue ()
				{
						return Mathf.RoundToInt (value);
				}
		}
}