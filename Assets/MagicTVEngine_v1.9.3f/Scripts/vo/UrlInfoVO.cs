﻿using UnityEngine;
using System.Collections;

namespace MagicTV.vo
{

	public class UrlInfoConstants
	{

		public const int STATUS_FILE_WAITING = 0 ;
		public const int STATUS_FILE_CACHED = 1 ;
		public const int STATUS_FILE_DOWNLOADING = 2 ;

		public const int QUALITY_HIGH = 3 ;

		public const int QUALITY_MID = 2 ;

		public const int QUALITY_LOW = 1 ;


		/// <summary>
		/// Gets the type of the quality int by.
		/// </summary>
		/// <returns>The quality int by type.</returns>
		/// 
		/// TODO: Melhorar o desempenho
		/// 
		/// <param name="type">Type.</param>
		public static int getQualityIntByType (string type)
		{
			int resultInt = 0;
			if (int.TryParse (type, out resultInt)) {
				return resultInt;
			}
			//chegou até aqui não conseguiu fazer o parse automatico
			if (type.Length > 2) {
				type = type.ToLower ();
				if (type == "high") {
					return QUALITY_HIGH;
				}

				
				if (type == "mid") {
					return QUALITY_MID;
				}

				
				if (type == "low") {
					return QUALITY_LOW;
				}
			}
			return 0;
		}
	}

	public class UrlInfoVO : VOWithId
	{

		public string bundle_file_id;
		/// <summary>
		/// The type of url, is the quality of file to donwload
		/// </summary>
		public string type ;

		public string md5 = "";
		public string url ;
		/// <summary>
		/// The size in Bytes
		/// </summary>
		public int size ;

		/// <summary>
		/// Caminho local do arquivo salvo
		/// </summary>
		public string local_url ;

		/// <summary>
		/// Mudar para 1 se o arquivo já estiver salvo local e ZERO para se ainda não foi feito o download, -1 se for para deletar
		/// </summary>
		public int status ;
		public override string ToString ()
		{
			return "[UrlInfoVO] { id:" + id + " , url:" + url + " , local_url : " + local_url + "}";
		}

		public UrlInfoVO Clone ()
		{
			UrlInfoVO u = new UrlInfoVO ();
			u.id = this.id;
			u.url = this.url;
			u.bundle_file_id = this.bundle_file_id;
			u.type = this.type;
			u.md5 = this.md5;
			u.size = this.size;
			u.local_url = this.local_url;
			u.status = this.status;
			return u;
		}
	}

}