﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices ; 

namespace MagicTV.vo.result{
	/// <summary>
	/// Para resultados gerais de resposta boleana, opcional message em string
	/// o id serve para reconhecimento se o evento de resposta é o mesmo do evento da pergunta
	/// </summary>
	public class ConfirmResultVO {
			private static int _autoId = 0 ;
			public string id ;
			public bool confirm ;
			//public dynamic result ;
			public string message ;

			public ConfirmResultVO(){
				_autoId++ ;
				this.id = _autoId.ToString () ;
			}
	}

}