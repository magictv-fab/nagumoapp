﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices ; 

namespace MagicTV.vo.result{
	/// <summary>
	/// Para resultados gerais de retorno imediato
	/// </summary>
	public class ReturnResultVO {

			public bool success ;
			//public dynamic result ;
			public string message ;
	}

}