﻿using UnityEngine;
using System.Collections;

namespace MagicTV.vo{
	/// <summary>
	/// Error results.
	/// </summary>
	public class ErrorResultVO  {

		public int code ;
		public string message ;

	}

}