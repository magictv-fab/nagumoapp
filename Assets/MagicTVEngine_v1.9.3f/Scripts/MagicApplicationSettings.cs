﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.IO;
using ARM.utils.io;
using System.Collections.Generic;
using System;

public class MagicApplicationSettings : MonoBehaviour
{
	public static MagicApplicationSettings Instance;
	static string SOFTWARE_VERSION_NAME = "app_version";
	public static string AppVersionNumber = "1.9.3";
	public static List<string> safeFolders = new List<string> ();
	public string GoogleAnalitcsID ;
	public string urlToGetInfo ;
	public string urlToRegisterDevice ;
	public string urlToGetPing ;
	public string app ;
	public int timeoutToleranceMiliseconds = 5000 ;
	[HideInInspector]
	public bool	AppReady;
	public LoadMainScene LoadMainScene;

	public MagicApplicationSettings ()
	{
		
//
	}

	// Use this for initialization
	void Awake ()
	{

		Instance = this;

		safeFolders.Add (Screenshot.fotoPath);

		string deviceRegisteredVersion = PlayerPrefs.GetString (SOFTWARE_VERSION_NAME);
		
//		if (string.IsNullOrEmpty (deviceRegisteredVersion) || deviceRegisteredVersion != AppVersionNumber) {
//			Debug.Log ("Versao de app sera atualizada");
//			AppReady = false;
//			PlayerPrefs.DeleteAll ();
//			PlayerPrefs.SetString (SOFTWARE_VERSION_NAME, AppVersionNumber);
//			PlayerPrefs.Save ();
//			ResetHard ();
//
//			StartCoroutine (SetAppReady());
//		}
		Debug.Log(" App Is Ready ");
		StartCoroutine (SetAppReady());
	}

	public Action onAppReady;

	IEnumerator SetAppReady()
	{
		yield return new WaitForEndOfFrame ();
		AppReady = true;
		Debug.Log("SetAppReady called");
		if (onAppReady != null)
		{
//			if(PlayerPrefs.GetInt("firstAccess", 1))
			{
				onAppReady();
			}
//			else
//			{
//				LoadMainScene.
//			}
		}
		
	}

	
	public void ClickExit()
	{
		Application.Quit();
	}


	public void ResetHard ()
	{
		Debug.Log (" ResetHard - Apagando arquivos do Sistema por Update do AppVersionNumber || instalacao  ");
		PlayerPrefs.DeleteAll ();

//		ARMDebug.startTime = 0;
		// ARMFileManager.DeleteFolder ( System.IO.Path.Combine(ARMFileManager.GetPersitentDataPath (), "*") );
		// ARMFileManager.DeleteFolder(string str)
	

		try {
			if (ARMFileManager.Exists (ARMFileManager.GetPersitentDataPath ())) {
				FileInfo[] files = ARMFileManager.GetFilesOfDirectory (ARMFileManager.GetPersitentDataPath ());
			
				if (files != null) {
					files = files.Where<FileInfo> (f => safeFolders.Any (s => 
					                                                     !((f.DirectoryName.Contains (s)) && (f.DirectoryName.Contains (Screenshot.fotoPath)))
					                                                     )).ToArray ();
					for (int i = 0; i < files.Length; i++) {
					
						if (ARMFileManager.Exists (files [i].FullName)) {
							string folderOfThisFile = ARMFileManager.GetParentFolder (files [i].FullName);
						
							if (safeFolders.Any (s => !folderOfThisFile.Contains (s))) {
								//se está na lista de safe, apaga a pasta ou o arquivo dentro dela
								ARMFileManager.DeleteFile (files [i].FullName);
							
							
							}
						
						}
					}
				}
			}
		} catch (Exception e) {
			//diretorio nao existe
			Debug.Log ("arquivo de nao existente" + e.Message);
		}
		try {
			string[] folders = ARMFileManager.GetChildDirectoryNames (ARMFileManager.GetPersitentDataPath ());
			Debug.LogError ("FOLDERS:" + folders.Length + " >> " + ARMFileManager.GetPersitentDataPath ());

			folders.Where<string> (f => safeFolders.Any (s =>
			                                           !((f.Contains (s)) && (f.Contains (Screenshot.fotoPath)))
			)).ToArray ();
			
			if (folders != null) {
				for (int ji = 0; ji < folders.Length; ji++) {

					string pathTemp = Path.Combine (ARMFileManager.GetPersitentDataPath (), folders [ji]);

					Debug.Log (pathTemp + "\n" + ARMFileManager.Exists (pathTemp) + "\n " + (safeFolders.Any (s => !pathTemp.Contains (s))));

					if ((ARMFileManager.Exists (pathTemp) && (safeFolders.Any (s => !pathTemp.Contains (s))))) {
						Debug.Log (" vamos deletar " + pathTemp);
						ARMFileManager.DeleteFolder (pathTemp);
					}
				}
			}
		} catch (Exception e) {
			Debug.Log ("arquivo de nao existente path..." + e.Message);
		}
	}
}
