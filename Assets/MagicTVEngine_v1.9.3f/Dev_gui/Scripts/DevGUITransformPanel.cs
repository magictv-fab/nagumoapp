﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MagicTV.update.comm;
using MagicTV.abstracts;
public class DevGUITransformPanel : MonoBehaviour {


	const string TRANSFORM_POSITION = "position";
	const string TRANSFORM_ROTATION = "rotation";
	const string TRANSFORM_SCALE 	= "scale";

	const string PROPERTY_X = "x";
	const string PROPERTY_Y = "y";
	const string PROPERTY_Z = "z";

	Color _selectedColor = Color.red;
	Color _defaultColor = Color.black;

	public Button _positionBtn;
	public Button _rotationBtn;
	public Button _scaleBtn;

	public Button _propertyXBtn;
	public Button _propertyYBtn;
	public Button _propertyZBtn;

	string _currentTransform ;
	string _currentProperty ;
	// Use this for initialization
	InAppAbstract currentInApp;
	IncrementalSliderController _slider;
	void Start () {
		_slider = GetComponent<IncrementalSliderController>();
		_slider.onTransformAxis+= OnTransform;
		TransformEvents.AddOnInAppEventHandler(OnInApp );
	}
	 void OnInApp(InAppAbstract value)
    {
        currentInApp = value;
    }
	// Update is called once per frame
	void Update () {
	
	}

	protected void OnTransform( float value){
		RaiseTransform( getTransformVector( value ) );
		
	}

	
	public void SetProperty( string property){
		_propertyXBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_propertyYBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_propertyZBtn.GetComponentInChildren<Text>().color = _defaultColor;
		if(property == PROPERTY_X){
			_propertyXBtn.GetComponentInChildren<Text>().color = _selectedColor;
			
		}
		if(property == PROPERTY_Y){
			_propertyYBtn.GetComponentInChildren<Text>().color = _selectedColor;
			
		}
		if(property == PROPERTY_Z){
			_propertyZBtn.GetComponentInChildren<Text>().color = _selectedColor;
			
		}
		_currentProperty = property;
	}

	public void SetTransform( string transform){
		_positionBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_rotationBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_scaleBtn.GetComponentInChildren<Text>().color = _defaultColor;
		if(transform == TRANSFORM_POSITION ){
			_positionBtn.GetComponentInChildren<Text>().color = _selectedColor ;
			
		}
		if(transform == TRANSFORM_ROTATION){
			_rotationBtn.GetComponentInChildren<Text>().color = _selectedColor;
			
		}
		if(transform == TRANSFORM_SCALE ){
			_scaleBtn.GetComponentInChildren<Text>().color = _selectedColor;
			
		}
		_currentTransform  = transform ;
	}
	Vector3 getTransformVector( float value ){
		return new Vector3( 
			( _currentProperty == PROPERTY_X ? value : 0f) ,
			( _currentProperty == PROPERTY_Y ? value : 0f) ,
			( _currentProperty == PROPERTY_Z ? value : 0f) 
		);
	}

	void RaiseTransform( Vector3 data) {
		if( _currentTransform == TRANSFORM_POSITION ){
			TransformEvents.RaiseOnPosition( data ) ;
		}

		if( _currentTransform == TRANSFORM_ROTATION ){
			TransformEvents.RaiseOnRotation( data );
		}

		if( _currentTransform == TRANSFORM_SCALE ){
			TransformEvents.RaiseOnScale(data);
		}
	}

}
