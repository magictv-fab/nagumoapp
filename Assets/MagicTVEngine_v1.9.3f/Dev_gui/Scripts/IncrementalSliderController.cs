﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class IncrementalSliderController : MonoBehaviour {

	public delegate void OnTransformAxisEventHandler( float value);

	public OnTransformAxisEventHandler onTransformAxis;

	public Slider _slider;

	public Text _multiplyFactorText;

	float _value;
	// Use this for initialization
	
	
	public void ParseMultiplyValue( float value){
		_multiplyFactorText.text = value.ToString();
	}

	public void ParseValue( float value ){
		_value = value;
		// Debug.Log("--"+value);

		
	}
	void LateUpdate(){
		if(onTransformAxis!=null && _value != 0){
			onTransformAxis( _value * float.Parse(_multiplyFactorText.text ));
		}
	}
	public void OnEndDrag(){
		_slider.value = 0;
	}
	
	
}
