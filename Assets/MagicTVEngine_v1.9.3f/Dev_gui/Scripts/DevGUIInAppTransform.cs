﻿
// using System.Collections;
using UnityEngine;
// using MagicTV.globals.events;
using UnityEngine.UI;
using MagicTV.update.comm;
using MagicTV.abstracts;
public class DevGUIInAppTransform : MonoBehaviour {

	const string TRANSFORM_POSITION = "position";
	const string TRANSFORM_ROTATION = "rotation";
	const string TRANSFORM_SCALE 	= "scale";

	const string PROPERTY_X = "x";
	const string PROPERTY_Y = "y";
	const string PROPERTY_Z = "z";
	

	Color _selectedColor = Color.red;
	Color _defaultColor = Color.black;

	public Button _positionBtn;
	public Button _rotationBtn;
	public Button _scaleBtn;

	public Button _propertyXBtn;
	public Button _propertyYBtn;
	public Button _propertyZBtn;

	public InputField _changeValueInput;
	public float _changeValue = 0.2f;

	string _currentTransform ;
	string _currentProperty ;
	InAppAbstract currentInApp;
// Use this for initialization
    void OnInApp(InAppAbstract value)
    {
        currentInApp = value;
    }
	void Start () {
		_changeValueInput.text = _changeValue.ToString();		
		TransformEvents.AddOnInAppEventHandler(OnInApp );
	}

	public void PauseGUIAction(){
		if(currentInApp==null){
			return;
		}
		currentInApp.Stop();
	}

public void PlayGUIAction(){
		currentInApp.Run();
	}
	public void SetTransform( string transform){
		_positionBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_rotationBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_scaleBtn.GetComponentInChildren<Text>().color = _defaultColor;
		if(transform == TRANSFORM_POSITION ){
			_positionBtn.GetComponentInChildren<Text>().color = _selectedColor ;
		}
		if(transform == TRANSFORM_ROTATION){
			_rotationBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		if(transform == TRANSFORM_SCALE ){
			_scaleBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		_currentTransform  = transform ;
	}

	public void SetProperty( string property){
		_propertyXBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_propertyYBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_propertyZBtn.GetComponentInChildren<Text>().color = _defaultColor;
		if(property == PROPERTY_X){
			_propertyXBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		if(property == PROPERTY_Y){
			_propertyYBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		if(property == PROPERTY_Z){
			_propertyZBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		_currentProperty = property;
	}

	public void RaisePlusButtonEvent(){
		RaiseTransform( getTransformVector( getChangeValue() ) );
		
	}

	public void RaiseMinusButtonEvent(){
		RaiseTransform( getTransformVector( getChangeValue()*-1 ) );
	}

	void RaiseTransform( Vector3 data) {
		if( _currentTransform == TRANSFORM_POSITION ){
			TransformEvents.RaiseOnPosition( data ) ;
		}

		if( _currentTransform == TRANSFORM_ROTATION ){
			TransformEvents.RaiseOnRotation( data );
		}

		if( _currentTransform == TRANSFORM_SCALE ){
			TransformEvents.RaiseOnScale(data);
		}
	}

	Vector3 getTransformVector( float value ){
		return new Vector3( 
			( _currentProperty == PROPERTY_X ? value : 0f) ,
			( _currentProperty == PROPERTY_Y ? value : 0f) ,
			( _currentProperty == PROPERTY_Z ? value : 0f) 
		);
	}

	float getChangeValue(){
		try {   
			return float.Parse( _changeValueInput.text );
		}catch{
			return 0f;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	// a
	}
}
