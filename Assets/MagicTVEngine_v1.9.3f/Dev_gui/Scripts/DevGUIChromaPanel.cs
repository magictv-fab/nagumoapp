﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MagicTV.update.comm;
using MagicTV.abstracts;
using MagicTV.globals.events;
public class DevGUIChromaPanel : MonoBehaviour {

	const string PROPERTY_SMOOTH 		= "smooth";
	const string PROPERTY_SMOOTH_SENS 	= "smooth_sens";
	const string PROPERTY_REC 			= "recorte";
	const string PROPERTY_REC_SENS 		= "recorte_sens";
	
	public Button _smoothBtn;
	public Button _smoothSensBtn;
	public Button _recorteBtn;
	public Button _recorteSensBtn;

string _shaderChoosed;
	float _recorteSens;
	float _recorte;
	float _smooth;
	float _smoothSens;

	Color _selectedColor = Color.red;
	Color _defaultColor = Color.black;
	IncrementalSliderController _slider;

	string _currentProperty ; 

	public string _bundleID;

	// Use this for initialization
	void Start () {
		_slider = GetComponent<IncrementalSliderController>();
		_slider.onTransformAxis+= OnTransform;

		VideoDebugEvents.AddChangeDebugFloat1EventHandler (changed1);
		VideoDebugEvents.AddChangeDebugFloat2EventHandler (changed2);
		VideoDebugEvents.AddChangeDebugFloat3EventHandler (changed3);
		VideoDebugEvents.AddChangeDebugFloat4EventHandler (changed4);
		VideoDebugEvents.AddChangeDebugShader (changedShder);

		TransformEvents.AddOnBundleIDEventHandler(SetBundleID);

	}

	protected void SetBundleID( int value ){
		_bundleID = value.ToString();
	}

	void changedShder (string v)
	{
			_shaderChoosed = v;
	}

	void changed1 (float p)
	{
			_recorteSens = p;
	}

	void changed2 (float p)
	{
			_recorte = p;
	}

	void changed3 (float p)
	{
			_smooth = p;
	}

	void changed4 (float p)
	{
			_smoothSens= p;
	}

	protected void OnTransform( float value){
		if(_currentProperty == PROPERTY_SMOOTH){
			VideoDebugEvents.RaiseDebugFloat3 (value);
		}

		if(_currentProperty == PROPERTY_SMOOTH_SENS){
			VideoDebugEvents.RaiseDebugFloat4 (value);
		}		

		if(_currentProperty == PROPERTY_REC){
			VideoDebugEvents.RaiseDebugFloat2 (value);
		}
		
		if(_currentProperty == PROPERTY_REC_SENS){
			VideoDebugEvents.RaiseDebugFloat1(value);
		}
	}
	
	public void SetProperty( string property){
		

		_smoothBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_smoothSensBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_recorteBtn.GetComponentInChildren<Text>().color = _defaultColor;
		_recorteSensBtn.GetComponentInChildren<Text>().color = _defaultColor;

		if(property == PROPERTY_SMOOTH){
			_smoothBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		if(property == PROPERTY_SMOOTH_SENS){
			_smoothSensBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		if(property == PROPERTY_REC){
			_recorteBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		if(property == PROPERTY_REC_SENS){
			_recorteSensBtn.GetComponentInChildren<Text>().color = _selectedColor;
		}
		_currentProperty = property;
	}

		void doneToSaveChromakey(bool success)
		{
				ServerCommunication.Instance.RemoveUpdateMetadataEventHandler (doneToSaveChromakey);
				
				var alert = WindowControlListScript.Instance.InstantiateAlertControllerScript ();
				alert.SetButtonOkText ("OK");
				string message = (success) ? "Enviado com sucesso" : "Erro ao salvar";
				alert.SetMessageText (message);
				alert.SetTitleText ("Alert");
				
				WindowControlListScript.Instance.AddPopUpToQueue (alert.gameObject);
		}
		public void SaveChromaInfo()
		{
			
				if (  string.IsNullOrEmpty(_bundleID)) {
						return;
				}
				// VideoDebugEvents.RaiseSave (bundleID, GUIMaterialName.text, float.Parse (GUISensibility.text), float.Parse (GUIRecorte.text));

				ServerCommunication.Instance.AddUpdateMetadataEventHandler (doneToSaveChromakey);
			
				ServerCommunication.Instance.sendVuforiaChromaquiInfo (
						_shaderChoosed, 
						_bundleID,
						_recorte, 
						_recorteSens, 
						_smooth, 
						_smoothSens

				);
		}

}
