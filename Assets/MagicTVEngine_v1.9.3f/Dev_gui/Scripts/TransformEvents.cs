﻿using UnityEngine;
using MagicTV.abstracts;
public class TransformEvents  {

	public delegate void OnBundleIDEventHandler( int value );
	public delegate void OnInAppEventHandler( InAppAbstract value );
	public delegate void OnTransformEventHandler( Vector3 value );
	

	#region bundleID
		static OnBundleIDEventHandler onBundleID; 
		public static void AddOnBundleIDEventHandler(  OnBundleIDEventHandler eventHandler){
			onBundleID+= eventHandler;		
		}
		
		public static void RemoveOnBundleIDEventHandler(  OnBundleIDEventHandler eventHandler){
			onBundleID-= eventHandler;		
		}

		public static void RaiseBundleID(  int value ){
			if( onBundleID != null ){
				onBundleID(value);
			}		
		}
	#endregion
	#region InApp
		static OnInAppEventHandler onInApp; 
		public static void AddOnInAppEventHandler(  OnInAppEventHandler eventHandler){
			onInApp+= eventHandler;		
		}
		
		public static void RemoveOnInAppEventHandler(  OnInAppEventHandler eventHandler){
			onInApp-= eventHandler;		
		}

		public static void RaiseInApp(  InAppAbstract value ){
			if( onInApp != null ){
				onInApp(value);
			}		
		}
	#endregion


	#region Position
		static OnTransformEventHandler onPosition;

		public static void AddOnPositionEventHandler(  OnTransformEventHandler eventHandler){
			onPosition+= eventHandler;		
		}
		
		public static void RemoveOnPositionEventHandler(  OnTransformEventHandler eventHandler){
			onPosition-= eventHandler;		
		}

		public static void RaiseOnPosition(  Vector3 value ){
			if( onPosition != null ){
				onPosition(value);
			}		
		}
	#endregion

	#region Rotation
		static OnTransformEventHandler onRotation;

		public static void AddOnRotationEventHandler(  OnTransformEventHandler eventHandler){
			onRotation+= eventHandler;		
		}
		
		public static void RemoveOnRotationEventHandler(  OnTransformEventHandler eventHandler){
			onRotation-= eventHandler;		
		}

		public static void RaiseOnRotation(  Vector3 value ){
			if( onRotation != null ){
				onRotation( value );
			}		
		}
	#endregion

	#region Scale
		static OnTransformEventHandler onScale;

		public static void AddOnScaleEventHandler(  OnTransformEventHandler eventHandler){
			onScale+= eventHandler;		
		}
		
		public static void RemoveOnScaleEventHandler(  OnTransformEventHandler eventHandler){
			onScale-= eventHandler;		
		}

		public static void RaiseOnScale(  Vector3 value ){
			if( onScale != null ){
				onScale( value );
			}		
		}
	#endregion
}
