﻿//  using UnityEngine;
//  using System.Collections;
//  using Vuforia;

//  namespace ARM.vuforia.image
//  {
//  		public class VuforiaImageGrayScale : ARM.utils.bytes.ByteListAbstract, ITrackerEventHandler
//  		{

//  				/// <summary>
//  				/// ImagePoICount ( contador de pixels de interesse  )  dentro da imagem como pontos de referencia para detectar mudanças
//  				/// </summary>
//  				public int ImagePoICount = 100 ;


//  				/// <summary>
//  				/// The m_ pixel format. GRAYSCALE é o único que funciona no IOS
//  				/// </summary>
//  				private Image.PIXEL_FORMAT m_PixelFormat = Image.PIXEL_FORMAT.GRAYSCALE;


//  				/// <summary>
//  				/// CTRL+C OTIMIZAR
//  				/// </summary>
//  				private bool m_RegisteredFormat = false ;
		
//  				/// <summary>
//  				/// CTRL+C OTIMIZAR
//  				/// </summary>
//  				private bool m_LogInfo = true ;
		
//  				private CameraDevice _cam  ;

//  				// Use this for initialization
//  				void Start ()
//  				{
//  //			DataSet.StorageType.
//  						//ImageTracker.DestroyDataSet();

//  						QCARBehaviour qcarBehaviour = (QCARBehaviour)FindObjectOfType (typeof(QCARBehaviour));
//  						if (qcarBehaviour) {
//  								qcarBehaviour.RegisterTrackerEventHandler (this);
//  						}
//  				}
		
//  				/// <summary>
//  				/// Raises the initialized event.
//  				/// Implementado devido a interface
//  				/// </summary>
//  				public void OnInitialized ()
//  				{
//  						//
//  				}
		
//  				/// <summary>
//  				/// Raises the trackables updated event.
//  				/// trecho de código extraido de : https://developer.vuforia.com/resources/dev-guide/unity-camera-image-access
//  				/// OTIMIZAR SE POSSIVEL ( no TIME MTF )
//  				/// </summary>
//  				public void OnTrackablesUpdated ()
//  				{
//  						if (!m_RegisteredFormat) {
//  								CameraDevice.Instance.SetFrameFormat (m_PixelFormat, true);
//  								m_RegisteredFormat = true;
//  						}
//  						if (m_LogInfo) {
//  								_cam = CameraDevice.Instance;
//  								Image image = _cam.GetCameraImage (m_PixelFormat);
//  								if (image == null) {
//  										//ARM//Debug.Log (m_PixelFormat + " image is not available yet");
//  								} else {
//  										string s = m_PixelFormat + " image: \n";
//  										s += "  size: " + image.Width + "x" + image.Height + "\n";
//  										s += "  bufferSize: " + image.BufferWidth + "x" + image.BufferHeight + "\n";
//  										s += "  stride: " + image.Stride;
//  										//ARM//Debug.Log (s);
					
//  										base.raiseOnReadyEvent ();
					
//  										m_LogInfo = false;
//  								}
//  						}
//  				}


//  				public override byte[] getBytes ()
//  				{
//  						if (_cam != null) {
//  								Image image = _cam.GetCameraImage (m_PixelFormat);
//  								return  this.getBytePoI (image.Pixels);
//  						}
//  						return new byte[0];
//  				}


//  				/// <summary>
//  				/// Extrai pontos da imagem para serem analisados
//  				/// </summary>
//  				/// <returns>pontos para análise</returns>
//  				/// <param name="bytePixels">Byte pixels.</param>
//  				private byte[] getBytePoI (byte[] bytePixels)
//  				{
			
//  						int indexJump = Mathf.FloorToInt (bytePixels.Length / ImagePoICount);
			
//  						byte[] result = new byte[ImagePoICount];
//  						;
			
//  						int resultCount = 0;
//  						for (int byteLoopIndex = 0; byteLoopIndex < bytePixels.Length; byteLoopIndex+= indexJump) {
//  								result [resultCount++] = bytePixels [byteLoopIndex];
//  						}
			
//  						return result;
//  				}
//  		}
//  }