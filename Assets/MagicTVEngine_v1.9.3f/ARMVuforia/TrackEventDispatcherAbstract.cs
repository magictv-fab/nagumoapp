﻿using UnityEngine;
using System.Collections;


namespace ARM.camera.abstracts{

	public class TrackEventDispatcherAbstract : MonoBehaviour {
		public delegate void OnTrack ();
		public OnTrack onTrackIn ;
		public OnTrack onTrackOut ;
		protected void _onTrackIn(){
			//verifica se possuí listeners
			if( this.onTrackIn != null ){
				this.onTrackIn() ;
			}
		}
		
		protected void _onTrackOut(){
			//verifica se possuí listeners
			if( this.onTrackOut != null ){
				this.onTrackOut() ;
			}
		}
	}
}