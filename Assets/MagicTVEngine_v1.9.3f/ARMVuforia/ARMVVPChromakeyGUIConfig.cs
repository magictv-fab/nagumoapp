﻿using UnityEngine;
using MagicTV.globals.events;
using UnityEngine.UI;
using MagicTV.update.comm;

public class ARMVVPChromakeyGUIConfig : MonoBehaviour
{
		public float sensibilityChanger = 0.2f;
		public float recorteChanger = 0.2f;

		public float smoothChanger = 0.2f;
		public float smoothSensibilityChanger = 0.2f;

		private float sensibility;
		private float recorte;
		private float smoothSensibility;
		private float smooth;


		string shaderChoiced = "none";

		public  Text GUIMaterialName;
		public  Text GUISensibility;
		public  Text GUIRecorte;


		public InputField GUIFloatValSmoothInput;
		public Text GUISmoothLabel;

		public InputField GUIFloatValSmoothSensibilityInput;
		public Text GUISmoothSensibilityLabel;
		
		public InputField GUIFloatValSensi;
		public InputField GUIFloatrecorte;
		public InputField GUISaveString;

		public string bundleID;

		public static ARMVVPChromakeyGUIConfig Instance;

		void Start ()
		{	
				VideoDebugEvents.AddChangeDebugFloat1EventHandler (changed1);
				VideoDebugEvents.AddChangeDebugFloat2EventHandler (changed2);
				VideoDebugEvents.AddChangeDebugFloat3EventHandler (changed3);
				VideoDebugEvents.AddChangeDebugFloat4EventHandler (changed4);
				VideoDebugEvents.AddChangeDebugShader (changedShder);

				GUIFloatrecorte.text = recorteChanger.ToString ();
				GUIFloatValSensi.text = sensibilityChanger.ToString ();

				GUIFloatValSmoothInput.text = smooth.ToString ();
				GUIFloatValSmoothSensibilityInput.text = smoothSensibility.ToString ();
				Instance = this;
				
		}
		public void GUIResetAction ()
		{
				AppRootEvents.GetInstance ().GetDevice ().RaiseReset ();

		}

		public void GUISetSmooth (string value)
		{
				if (value.Length > 0)
						smooth = float.Parse (value);
		}
		public void GUISetSmoothSensibility (string value)
		{
				if (value.Length > 0)
						smoothSensibility = float.Parse (value);
		}

		public void SetRecorteFromGUI (string value)
		{
				if (value.Length > 0)
						recorteChanger = float.Parse (value);
		}
		public void SetSensibilityFromGUI (string value)
		{
				if (value.Length > 0)
						sensibilityChanger = float.Parse (value);
		}

		public void SetBundleIDFromGUI (string data)
		{
				bundleID = data;
		}

		public void SaveString ()
		{
			
				if (bundleID == null || bundleID == "") {
						return;
				}
				VideoDebugEvents.RaiseSave (bundleID, GUIMaterialName.text, float.Parse (GUISensibility.text), float.Parse (GUIRecorte.text));

				ServerCommunication.Instance.AddUpdateMetadataEventHandler (doneToSaveChromaqui);
			
				ServerCommunication.Instance.sendVuforiaChromaquiInfo (
						GUIMaterialName.text, 
						bundleID,
						float.Parse (GUIRecorte.text), 
						float.Parse (GUISensibility.text), 
						float.Parse (GUISmoothLabel.text), 
						float.Parse (GUISmoothSensibilityLabel.text)

				);
		}

		void doneToSaveChromaqui (bool success)
		{
				ServerCommunication.Instance.RemoveUpdateMetadataEventHandler (doneToSaveChromaqui);
				
				var alert = WindowControlListScript.Instance.InstantiateAlertControllerScript ();
				alert.SetButtonOkText ("OK");
				string message = (success) ? "Enviado com sucesso" : "Erro ao salvar";
				alert.SetMessageText (message);
				alert.SetTitleText ("Alert");
				
				WindowControlListScript.Instance.AddPopUpToQueue (alert.gameObject);
		}

		void changedShder (string v)
		{
				this.shaderChoiced = v;
		}

		void changed1 (float p)
		{
				this.sensibility = p;
		}

		void changed2 (float p)
		{
				this.recorte = p;
		}

		void changed3 (float p)
		{
				this.smooth = p;
		}

		void changed4 (float p)
		{
				this.smoothSensibility = p;
		}

		void OnGUI ()
		{
				GUI.Box (new Rect (0, 0, 50, 50), this.bundleID);
		}

		void LateUpdate ()
		{
				GUIMaterialName.text = shaderChoiced;
				GUIRecorte.text = recorte.ToString ();
				GUISensibility.text = sensibility.ToString ();

				GUISmoothLabel.text = smooth.ToString ();
				GUISmoothSensibilityLabel.text = smoothSensibility.ToString ();
		}

		public void changeSmoothSensiblityValue (bool plusorless)
		{
				if (plusorless) {
						VideoDebugEvents.RaiseDebugFloat4 (-smoothSensibilityChanger);
				}
				if (!plusorless) {
						VideoDebugEvents.RaiseDebugFloat4 (smoothSensibilityChanger);
			
				}
		}

		public void changeSmoothValue (bool plusorless)
		{
				if (plusorless) {
						VideoDebugEvents.RaiseDebugFloat3 (-smoothChanger);
				}
				if (!plusorless) {
						VideoDebugEvents.RaiseDebugFloat3 (smoothChanger);
			
				}
		}


		public void changeSensiblityValue (bool plusorless)
		{
				if (plusorless) {
						VideoDebugEvents.RaiseDebugFloat1 (-sensibilityChanger);
				}
				if (!plusorless) {
						VideoDebugEvents.RaiseDebugFloat1 (sensibilityChanger);
			
				}
		}

		public  void changeRecorteValue (bool plusorless)
		{
				if (plusorless) {
						VideoDebugEvents.RaiseDebugFloat2 (-recorteChanger);
				}
				if (!plusorless) {
						VideoDebugEvents.RaiseDebugFloat2 (recorteChanger);
			
				}
		}
}
