﻿using UnityEngine;
using System.Collections;

using ARM.camera.abstracts ;
using Vuforia;

namespace ARM.camera.ra
{

		public class VuforiaTrackEvent : TrackEventDispatcherAbstract , ITrackableEventHandler
		{
		
				private TrackableBehaviour mTrackableBehaviour;
		
				void Start ()
				{
						mTrackableBehaviour = GetComponent<TrackableBehaviour> ();
						if (mTrackableBehaviour) {
								mTrackableBehaviour.RegisterTrackableEventHandler (this);
						}
				}
				public void OnTrackableStateChanged (
			TrackableBehaviour.Status previousStatus,
			TrackableBehaviour.Status newStatus)
				{
						if (newStatus == TrackableBehaviour.Status.DETECTED ||
								newStatus == TrackableBehaviour.Status.TRACKED ||
								newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
								this._onTrackIn ();
						} else {
								this._onTrackOut ();
						}
				}
		}
}

