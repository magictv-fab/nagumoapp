/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Connected Experiences, Inc.
==============================================================================*/

using UnityEngine;
using Vuforia;

using System.Runtime.InteropServices;
/// <summary>
/// A custom handler that implements the IQCARErrorHandler interface.
/// </summary>
    
public class ARMVuforiaInitializationErrorHandler : MonoBehaviour
{
           
	[DllImport ("__Internal")]   
	private static extern void cameraPrefesShortCut ();
       
	public GameObject[] DisableList;
	public GameObject[] EnaableList;
       
        #region PRIVATE_MEMBER_VARIABLES 

	private string mErrorText = "";
	private bool mErrorOccurred = false;

	private const string WINDOW_TITLE = "QCAR Initialization Error";

protected bool _restart_when_return ;
        #endregion // PRIVATE_MEMBER_VARIABLES

        #region UNTIY_MONOBEHAVIOUR_METHODS

	void Awake ()
	{
		// Check for an initialization error on start.
		VuforiaAbstractBehaviour qcarBehaviour = (VuforiaAbstractBehaviour)FindObjectOfType (typeof(VuforiaAbstractBehaviour));
		if (qcarBehaviour) {
			qcarBehaviour.RegisterVuforiaInitErrorCallback (OnQCARInitializationError);
		}
	}

	void OnGUI ()
	{
		// On error, create a full screen window.
		//  if (mErrorOccurred)
		//  GUI.Window(0, new Rect(0, 0, Screen.width, Screen.height),
		//  DrawWindowContent, WINDOW_TITLE);
	}

	/// <summary>
	/// When this game object is destroyed, it unregisters itself as event handler
	/// </summary>
	void OnDestroy ()
	{
		VuforiaAbstractBehaviour qcarBehaviour = (VuforiaAbstractBehaviour)FindObjectOfType (typeof(VuforiaAbstractBehaviour));
		if (qcarBehaviour) {
			qcarBehaviour.RegisterVuforiaInitErrorCallback (OnQCARInitializationError);
		}
	}

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS

        #region PRIVATE_METHODS

	private void DrawWindowContent (int id)
	{
		// Create text area with a 10 pixel distance from other controls and
		// window border.
		GUI.Label (new Rect (10, 25, Screen.width - 20, Screen.height - 95),
                        mErrorText);

		// Create centered button with 50/50 size and 10 pixel distance from
		// other controls and window border.
		if (GUI.Button (new Rect (Screen.width / 2 - 75, Screen.height - 60, 150, 50), "Close")) {
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#else
                Application.Quit();
			#endif
		}
	}

	private void ShowXCodeConfigButton ()
	{
            
		Debug.Log ("ShowXCodeConfigButton");
		cameraPrefesShortCut ();
	}
	private void RestartApplication(){

		WindowControlListScript.Instance.WindowInAppGUI.ResetClick();
	}
	// Implementation of the IQCARErrorHandler function which sets the
	// error message.
	private void SetErrorCode (VuforiaUnity.InitError  errorCode)
	{
            _restart_when_return = true ;
		//WindowControlListScript.Instance.WindowInAppListWaiting.Hide ();
		WindowControlListScript.Instance.WindowInAppGUI.Hide ();
            
            WindowControlListScript.Instance.WindowErrorMessage.SetButtonLabel("Tentar novamente.");
             WindowControlListScript.Instance.WindowErrorMessage.onClickConfig+= RestartApplication;
                      
		Debug.LogError ("QCAR initialization failed: " + mErrorText);
		switch (errorCode) {
		case VuforiaUnity.InitError.INIT_EXTERNAL_DEVICE_NOT_DETECTED:
			mErrorText =
                        "Este dispositivo não contém o hardware necessário para rodar a tecnologia \n <b>Magic TV</b>.";
			break;
		case VuforiaUnity.InitError .INIT_LICENSE_ERROR_MISSING_KEY:
			mErrorText =
                       "<b>Magic TV</b> Chave de App erro \n[MK]";
			break;
		case VuforiaUnity.InitError.INIT_LICENSE_ERROR_INVALID_KEY:
			mErrorText =
                       "<b>Magic TV</b> Chave de App erro \n[IK]";
			break;
		case VuforiaUnity.InitError.INIT_LICENSE_ERROR_NO_NETWORK_TRANSIENT:
			mErrorText =
                        "Problemas de comunicação com o servidor \n <b>Magic TV</b> \n Favor tente mais tarde.";
			break;
		case VuforiaUnity.InitError.INIT_LICENSE_ERROR_NO_NETWORK_PERMANENT:
			mErrorText =
                        "<b>Magic TV</b> Chave de App erro \n[NNP]";
			break;
		case VuforiaUnity.InitError.INIT_LICENSE_ERROR_CANCELED_KEY:
			mErrorText =
                        "<b>Magic TV</b> Chave de App erro \n[CK]";
			break;
		case VuforiaUnity.InitError.INIT_LICENSE_ERROR_PRODUCT_TYPE_MISMATCH:
			mErrorText =
                        "<b>Magic TV</b> Chave de App erro \n[PTM] ";
			break;
    #if (UNITY_IPHONE || UNITY_IOS)
                case VuforiaUnity.InitError.INIT_NO_CAMERA_ACCESS:
                     mErrorText = "Para ter acesso ao conteúdo \n  é necessário \n permitir o uso da câmera."; 
                    
                        //  "Camera Access was denied to this App. \n" + 
                        //  "When running on iOS8 devices, \n" + 
                        //  "users must explicitly allow the App to access the camera.\n" + 
                        //  "To restore camera access on your device, go to: \n" + 
                        //  "Settings > Privacy > Camera > [This App Name] and switch it ON.";
                       
                        WindowControlListScript.Instance.WindowErrorMessage.onClickConfig+= ShowXCodeConfigButton;
                        WindowControlListScript.Instance.WindowErrorMessage.onClickConfig-= RestartApplication;
                          WindowControlListScript.Instance.WindowErrorMessage.SetButtonLabel("Configurações");
                    break;
    #endif
		case VuforiaUnity.InitError.INIT_DEVICE_NOT_SUPPORTED:
			mErrorText =
                        "Este dispositivo não é compatível com a tecnologia\n" +
				"<b>Magic TV</b>";
			break;
		case VuforiaUnity.InitError.INIT_ERROR:
			mErrorText = "Falha ao iniciar a tecnologia\n" +
				"<b>Magic TV</b>";
			break;
		}
            
           
		
            
		WindowControlListScript.Instance.WindowErrorMessage.Show ();
		if (mErrorText.Length > 0) {
			WindowControlListScript.Instance.WindowErrorMessage.SetErroMessage (mErrorText);
		}
		Screen.orientation = ScreenOrientation.Portrait;
		
		Application.LoadLevelAdditiveAsync("mini-game");

		if (DisableList.Length > 0) {
			for (int i =0; i<DisableList.Length; i++) {
				DisableList [i].SetActive (false);
				GameObject.Destroy(EnaableList [i]);
				
			}
		}
		if (EnaableList.Length > 0) {
			for (int i =0; i<EnaableList.Length; i++) {
				EnaableList [i].SetActive (true);

			}
		}
	}


	// Implementation of the IQCARErrorHandler function which sets if an
	// error has been thrown.
	private void SetErrorOccurred (bool errorOccurred)
	{
		mErrorOccurred = errorOccurred;
	}

        #endregion // PRIVATE_METHODS



        #region QCAR_lifecycle_events

	public void OnQCARInitializationError (VuforiaUnity.InitError initError)
	{
		if (initError != VuforiaUnity.InitError.INIT_SUCCESS) {
			SetErrorCode (initError);
			SetErrorOccurred (true);
		}
	}

        #endregion // QCAR_lifecycle_events
        
	// HOT FIX para IOS 8.x no Iphone 6 e 6+
	// Vuforia as vezes dá erro quando re-abre o app. melhor coisa a se fazer é resetar a aplicação
	void OnApplicationPause (bool pauseState)
	{
#if UNITY_IOS
        if (_restart_when_return && !pauseState) {
			Application.LoadLevel (0);
		}
#endif
    }
}

    
