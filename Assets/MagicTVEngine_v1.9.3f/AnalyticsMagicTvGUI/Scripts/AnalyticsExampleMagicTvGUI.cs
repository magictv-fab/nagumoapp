﻿using UnityEngine;
using System.Collections;

public class AnalyticsExampleMagicTvGUI : MonoBehaviour
{
	private Analytics _Analytics ;
	private string CurrentPageName = " No Tracking " ;
	public string trackingID = "UA-52603038-6" ;
	public string appName = "MagicTV" ;
	
	void Awake(){
		GameObject obj1 = new GameObject("CurrentBundle");
		obj1.transform.parent = null;
		_Analytics = obj1.AddComponent<Analytics>();
		_Analytics.trackingID = trackingID ;
		_Analytics.appName = appName ;
		_Analytics.appVersion = "0.0.1" ;
		_Analytics.Init() ;
	}
	
	void Start(){

		WindowControlListScript.Instance.WindowInAppGUI.onClickCamera += WindowInAppGUIonClickCamera ;
		
		WindowControlListScript.Instance.WindowInstructions.onShow += WindowInstructionsOnShow ;
		
//		WindowControlListScript.Instance.WindowHelp.onClickPrint += WindowHelpOnClickPrint ;
//		WindowControlListScript.Instance.WindowHelp.onClickYouTubeVideo += WindowHelpOnClickYouTubeVideo ;
//		WindowControlListScript.Instance.WindowHelp.onShow += WindowHelpOnShow ;
//		
//		WindowControlListScript.Instance.WindowAbout.onShow += WindowAboutOnShow ;
//		WindowControlListScript.Instance.WindowAbout.onClickEmail += WindowAboutOnClickEmail ;
//		WindowControlListScript.Instance.WindowAbout.onClickFacebook += WindowAboutOnClickFacebook ;
//		WindowControlListScript.Instance.WindowAbout.onClickSite += WindowAboutOnClickSite ;
//		WindowControlListScript.Instance.WindowAbout.onClickTwitter += WindowAboutOnClickTwitter ;
//		WindowControlListScript.Instance.WindowAbout.onClickYouTubeChannel += WindowAboutOnClickYouTubeChannel ;
	}
	

	public void WindowInAppGUIonClickCamera(){
		_Analytics.sendEvent( "ScreenShot", "Foto", CurrentPageName ) ;
	}
	
	public void WindowInstructionsOnShow(){
		_Analytics.sendEvent( "Menu", "Instruções", CurrentPageName ) ;
	}
	
	public void WindowHelpOnShow(){
		_Analytics.sendEvent( "Menu", "Ajuda", CurrentPageName ) ;
	}
	
	public void WindowHelpOnClickPrint(){
		_Analytics.sendEvent( "Menu", "Ajuda", "Impressão" ) ;
		Application.OpenURL( "http://www.grafitelabs.com.br/pdf/magictv_card.pdf" ) ;
	}
	
	public void WindowHelpOnClickYouTubeVideo(){
		_Analytics.sendEvent( "Menu", "Ajuda", "You Tube" ) ;
		Application.OpenURL( "https://www.youtube.com/watch?v=l89O4Q8P824" ) ;
	}
	
	public void WindowAboutOnShow(){
		_Analytics.sendEvent( "Menu", "Sobre", CurrentPageName ) ;
	}
	
	public void WindowAboutOnClickEmail(){
		_Analytics.sendEvent( "Menu", "Sobre", "E-mail" ) ;
		Application.OpenURL ( "mailto:suporte@magictv.com.br" ) ;
	}
	
	public void WindowAboutOnClickFacebook(){
		_Analytics.sendEvent( "Menu", "Sobre", "Facebook" ) ;
		Application.OpenURL ( "http://www.facebook.com/magictvoficial" ) ;
	}
	
	public void WindowAboutOnClickSite(){
		_Analytics.sendEvent( "Menu", "Sobre", "Site" ) ;
		Application.OpenURL ("http://www.magictv.com.br/") ;
	}
	
	public void WindowAboutOnClickTwitter(){
		_Analytics.sendEvent( "Menu", "Sobre", "Twitter" ) ;
		Application.OpenURL ( "https://mobile.twitter.com/MagicTV_oficial" ) ;
	}
	
	public void WindowAboutOnClickYouTubeChannel(){
		_Analytics.sendEvent( "Menu", "Sobre", "You Tube" ) ;
		Application.OpenURL ( "https://www.youtube.com/user/magictvoficial" ) ;
	}
}