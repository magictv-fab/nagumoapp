﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI.Extensions;

public class AutoSwapScroll : MonoBehaviour {

    public HorizontalScrollSnap HorizontalScrollSnap;
    public float time = 5f;
    public float inativityTime = 5f;

    private int counter;
    private int dir = 1;
    private float inativityTimeCtrl;

	private void Awake()
	{
        inativityTimeCtrl = inativityTime;
	}

	private void OnEnable()
	{
        //HorizontalScrollSnap.GoToScreen(1);
	}

	// Use this for initialization
	IEnumerator Start () 
    {
        yield return new WaitForSeconds(time);

        if (HorizontalScrollSnap._currentPage >= HorizontalScrollSnap.ChildObjects.Length - 1)
            dir = -1;
        else if (HorizontalScrollSnap._currentPage <= 0)
            dir = 1;

        //Debug.Log("Vai para pagina: " + (HorizontalScrollSnap.CurrentPage + dir));

        //aguarda o tempo de inatividade.
        while(inativityTimeCtrl < inativityTime)
        {
            yield return new WaitForEndOfFrame();    
        }

        HorizontalScrollSnap.GoToScreen(HorizontalScrollSnap.CurrentPage + dir);

        StartCoroutine(Start());
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (HorizontalScrollSnap._pointerDown)
            inativityTimeCtrl = 0;
        else
            inativityTimeCtrl += Time.deltaTime;
	}
}
