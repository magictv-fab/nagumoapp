﻿using UnityEngine;
using System.Collections;

public class ChangeValuesTest : MonoBehaviour {
	public ReturnConfigTest obj;


	public bool soma;
	public bool diminui;

	public float f;
	public Vector3 v;
	// Use this for initialization
	void Start () {
		if(obj==null){
			return;
		}
		this.f = obj.getFloat ();
		this.v = obj.getVector ();
	}

	void diminuir ()
	{
		this.v.x --;
	}

	void somar ()
	{
		this.v.x ++;
	}
	
	// Update is called once per frame
	void Update () {
		if(soma){
			soma = false;
			somar();
		}
		if(diminui){
			diminui = false ;
			diminuir();
		}
	}
}
