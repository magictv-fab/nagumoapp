﻿using UnityEngine;
using System.Collections;
using System;

public class EndOfApplication : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		#if UNITY_ANDROID
		try {
			AndroidJavaObject jo = new AndroidJavaObject ("magictv.Integration");
			jo.Call ("goBack");
		} catch (Exception e) {
			Debug.Log (e.ToString ());
		}
		#endif
	}
}