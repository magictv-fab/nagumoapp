﻿using UnityEngine;
using System.Collections;
using System;

namespace ARM.gui.utils
{
	[Serializable]
	public enum ScreenOrientationType
	{
		Portrait,
		Landscape}

	;

	[ExecuteInEditMode]
	public class ScreenOrientation : MonoBehaviour
	{
		protected Vector2 _lastSize = Vector2.zero;
		protected static bool _destroying = false;

		public static ScreenOrientationType Orientation {
			get { return GetCurrentOrientationType (); }
		}

				#region Delegates

		public delegate void OnScreenOrientationEventHandler (Vector2 screenSize,ScreenOrientationType screenOrientationType) ;

		protected void RaiseScreenOrientationType (Vector2 screenSize, ScreenOrientationType screenOrientationType)
		{
			if (OnChangeScreenOrientationType != null)
				OnChangeScreenOrientationType (screenSize, screenOrientationType);
			_lastSize = screenSize;
		}

		protected OnScreenOrientationEventHandler OnChangeScreenOrientationType;

				#endregion

				#region Setters Delegates

		public static void AddChangeListener (OnScreenOrientationEventHandler listener)
		{
			if (InstanceScreenOrientation == null) return;
			InstanceScreenOrientation.OnChangeScreenOrientationType += listener;
		}

		public static void RemoveChangeListener (OnScreenOrientationEventHandler listener)
		{
			if (InstanceScreenOrientation == null) return;
			InstanceScreenOrientation.OnChangeScreenOrientationType += listener;
		}

				#endregion

		protected static ARM.gui.utils.ScreenOrientation _instanceScreenOrientation;
				
		protected ScreenOrientation ()
		{
			_instanceScreenOrientation = null;
		}

		protected static ARM.gui.utils.ScreenOrientation InstanceScreenOrientation {
			get {
				if (_instanceScreenOrientation == null) {
					Debug.LogError ("ScreenOrientation agora precisa estar vinculado a um game object manualmente.");
				}
				return _instanceScreenOrientation;
			}
			set { _instanceScreenOrientation = value; }
		}

		protected static ScreenOrientationType GetCurrentOrientationType ()
		{
			return (screenSize.x > screenSize.y) ? ScreenOrientationType.Landscape : ScreenOrientationType.Portrait;
		}

		protected bool CheckOrientationChange ()
		{
			// não houve mudança de orientacão de tela
			//Debug.Log ("Checking Orientation " + _lastSize + " - " + screenSize);
			return _lastSize != screenSize;
		}

		void Update ()
		{
			if (_destroying) {
				return;
			}
			if (CheckOrientationChange ()) {
				RaiseScreenOrientationType (screenSize, GetCurrentOrientationType ());
			}
		}

		void Awake ()
		{
			_instanceScreenOrientation = this;
		}

		void Start ()
		{
			_destroying = false;
		}

		void OnDestroy ()
		{
			_destroying = true;
		}
				
		static int mSizeFrame = -1;
		static System.Reflection.MethodInfo s_GetSizeOfMainGameView;
		static Vector2 mGameSize = Vector2.one;

		static public Vector2 screenSize {
			get {
#if UNITY_EDITOR
								int frame = Time.frameCount;

								if (mSizeFrame != frame || !Application.isPlaying) {
										mSizeFrame = frame;

										if (s_GetSizeOfMainGameView == null) {
												System.Type type = System.Type.GetType ("UnityEditor.GameView,UnityEditor");
												s_GetSizeOfMainGameView = type.GetMethod ("GetSizeOfMainGameView",
														System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
										}
										mGameSize = (Vector2)s_GetSizeOfMainGameView.Invoke (null, null);
								}
								if (mGameSize == Vector2.zero)
										mGameSize = new Vector2 (Screen.width, Screen.height);

								return mGameSize;
#else
				return new Vector2 (Screen.currentResolution.width, Screen.currentResolution.height);
#endif
			}
		}
	}
}