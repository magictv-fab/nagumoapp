﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class SoundController : MonoBehaviour {
	private static SoundController _soundController;

	public SoundController ()
	{
		_soundController = this;
	}

	public static SoundController GetInstance() {
		return _soundController;
	}

	public static AudioSource GetInstanceAudioSource() {
		return GetInstance().GetComponent<AudioSource>();
	}
}
