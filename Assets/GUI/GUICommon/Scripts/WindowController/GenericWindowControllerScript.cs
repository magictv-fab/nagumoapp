﻿using System.Collections.Generic;
using ARM.gui.utils;
using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using UnityEngine.UI;

[ExecuteInEditMode]
public abstract class GenericWindowControllerScript : MonoBehaviour
{
	void Start ()
	{
		Debug.Log("Amarrando os eventos");
		ARM.gui.utils.ScreenOrientation.AddChangeListener (OnChangeScreenOrientationType);
	}

	void OnDestroy ()
	{
		ARM.gui.utils.ScreenOrientation.RemoveChangeListener (OnChangeScreenOrientationType);
	}

	public virtual void Hide ()
	{
		gameObject.SetActive (false);
		if (onHide != null)
			onHide ();
	}

	public virtual void Show ()
	{
		gameObject.SetActive (true);
	}

	void OnEnable(){
		SetCurrentOrientation (ARM.gui.utils.ScreenOrientation.Orientation);
	}

	protected void LabelAnimationStop (Text label)
	{
		var labelAnimation = label.gameObject.GetComponent<Animation> ();
		if (labelAnimation == null)
			return;
		labelAnimation.Stop ();
		labelAnimation.Rewind ();
	}

	protected void LabelAnimationStart (Text label)
	{
		var labelAnimation = label.gameObject.GetComponent<Animation> ();
		if (labelAnimation == null)
			return;
		labelAnimation.Rewind ();
		labelAnimation.Play ();
	}

	public void OnChangeScreenOrientationType (Vector2 screenSize, ScreenOrientationType screenOrientationType)
	{
		Debug.Log ("Orientation Changed Window Controller" + screenOrientationType);
		SetCurrentOrientation (screenOrientationType);
	}

	public List<GameObject> FindChildrensByName (string childrenName)
	{
		var result = new List<GameObject> ();
		if (transform == null || transform.childCount == 0)
			return result;
		var currentTransform = gameObject.transform.GetChild (0);
		for (var i = 0; i < currentTransform.childCount; i++) {
			var currentChild = currentTransform.GetChild (i).gameObject;
			if (currentChild.name.ToLowerInvariant ().Contains (childrenName)) {
				result.Add (currentChild);
			}
		}
		return result;
	}

	public void SetCurrentOrientation (ScreenOrientationType screenOrientationType)
	{
		if (this == null)
			return;
		var landscapes = FindChildrensByName ("landscape");
		var portraits = FindChildrensByName ("portrait");

		foreach (var landscape in landscapes) {
			landscape.SetActive (screenOrientationType == ScreenOrientationType.Landscape);
		}

		foreach (var portrait in portraits) {
			portrait.SetActive (screenOrientationType == ScreenOrientationType.Portrait);
		}
	}

	public Action onShow;
	public Action onHide;
}
