﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using MagicTV.download;

public class Screenshot : MonoBehaviour
{

    #region Auto generate GO

    private const string SELF_GAMEOBJECT_NAME = "_ScreenshotGO";

    private static GameObject _selfGameObject;

    // Utilizar um nome complexo
    public static string fotoPath = "Nagumo Play";

    protected static GameObject SelfGameObject
    {
        get
        {

            if (_selfGameObject == null)
            {
                _selfGameObject = GameObject.Find(SELF_GAMEOBJECT_NAME);
                if (_selfGameObject != null)
                {
                    DestroyImmediate(_selfGameObject);
                }
                _selfGameObject = new GameObject(SELF_GAMEOBJECT_NAME);
                _selfGameObject.transform.parent = null;
                SelfIsntance = _selfGameObject.AddComponent<Screenshot>();
            }

            return _selfGameObject;
        }
    }

    #endregion

    #region static Instance

    protected static Screenshot _selfInstance;

    protected static Screenshot SelfIsntance
    {
        get
        {
            if (_selfInstance == null)
            {
                _selfInstance = SelfGameObject.GetComponent<Screenshot>();
            }
            return _selfInstance;
        }
        set
        {
            _selfInstance = value;
        }
    }

    #endregion

    void Awake()
    {
		if (!MagicApplicationSettings.safeFolders.Any(s => s == fotoPath))
        {
			MagicApplicationSettings.safeFolders.Add(fotoPath);
        }
        ScreenshotManagerGUI.instance.ScreenshotFinishedSaving += ScreenshotSaved;
    }


    #region events

    public delegate void OnCompleteEventHandler(string path);

    protected OnCompleteEventHandler onComplete;

    public static void AddCompleteEventHandler(OnCompleteEventHandler eventHandler)
    {
        SelfIsntance.onComplete += eventHandler;
    }

    public static void RemoveCompleteEventHandler(OnCompleteEventHandler eventHandler)
    {
        SelfIsntance.onComplete -= eventHandler;
    }

    protected void RaiseComplete(string path)
    {
        if (onComplete != null)
        {
            onComplete(path);
        }
    }

    #endregion

    public static void Save()
    {
        Debug.Log("screenshot Save: " + fotoPath);
        SelfIsntance.doSave("Screenshot", fotoPath);
    }

    protected void doSave(string imagePrefix, string albumName)
    {
        StartCoroutine(WaitFrameLock());
        StartCoroutine(ScreenshotManagerGUI.Save(imagePrefix, albumName, true));
    }

    string _path;

    void ScreenshotSaved(string path)
    {
        _path = path;
        RaiseComplete(_path);
    }

    IEnumerator WaitFrameLock()
    {
        yield return new WaitForSeconds(0.5f);
        yield return new WaitForFixedUpdate();
        yield return new WaitForEndOfFrame();
        Debug.Log("WaitFrameLock = false");

		MagicTVWindowInAppGUIControllerScript mtvwiagcs = GameObject.FindObjectOfType(typeof(MagicTVWindowInAppGUIControllerScript)) as MagicTVWindowInAppGUIControllerScript;

		if(mtvwiagcs.canvasObject)
			mtvwiagcs.canvasObject.SetActive(true);
    }

    void OnDestroy() {
        _selfGameObject = null;
        _selfInstance.onComplete = null;
        _selfInstance = null;
        Debug.Log("screenshot limpando statics.........");
    }

}