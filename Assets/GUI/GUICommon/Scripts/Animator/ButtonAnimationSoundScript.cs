﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MagicTV.globals.events;


[RequireComponent (typeof (Animation))]
public class ButtonAnimationSoundScript : MonoBehaviour {

	public AnimationClip animationClip;
	public AudioClip audioClip;
	void Start(){
		Button b = gameObject.GetComponent<Button>();
		if (b == null) return;
		b.onClick.AddListener(OnClick);
	}

	public void OnClick(){
		PlayAnimation ();
		PlaySound ();
	}
	public void StopAnimation(){
		Animation animationController = gameObject.GetComponent<Animation> ();
		animationController.Stop ();
	}

	public void StopSound(){
		AudioSource audioSource = SoundController.GetInstanceAudioSource();
		audioSource.Stop ();
	}

	public void PlayAnimation(){
		if (animationClip != null) {
			Animation animationController = gameObject.GetComponent<Animation> ();
			animationController.Stop ();
			animationController.AddClip (animationClip, animationClip.name);
			animationController.clip = animationClip;
			animationController.Play ();
		}
	}

	public void PlaySound(){
		if (audioClip != null){
			AudioSource audioSource = SoundController.GetInstanceAudioSource();
			audioSource.PlayOneShot(audioClip);
		}
	}
}