﻿using UnityEngine;
using System.Collections;

public class ButtonConfirmExample : MonoBehaviour
{
    public void Click()
    {
        var confirm = WindowControlListScript.Instance.InstantiateConfirmControllerScript();
        confirm.SetButtonOkText("Sim");
        confirm.SetButtonCancelText("Não");
        confirm.SetMessageText("Teste de Confirm, voce quer?");
        confirm.SetTitleText("Confirm");
        confirm.OnClickOk += OkClick;
        confirm.OnClickCancel += CancelClick;
        WindowControlListScript.Instance.AddPopUpToQueue(confirm.gameObject);
    }

    public void OkClick()
    {
    }

    public void CancelClick()
    {
    }
}