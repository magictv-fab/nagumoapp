﻿using UnityEngine;
using System.Collections;

public class ButtonAlertExample : MonoBehaviour
{
    public void Click()
    {
        var alert = WindowControlListScript.Instance.InstantiateAlertControllerScript();
        alert.SetButtonOkText("OK");
        alert.SetMessageText("Teste de Alert!");
        alert.SetTitleText("Alert");
        alert.OnClickOk += OkClick;
        WindowControlListScript.Instance.AddPopUpToQueue(alert.gameObject);
    }

    public void OkClick()
    {

    }
}