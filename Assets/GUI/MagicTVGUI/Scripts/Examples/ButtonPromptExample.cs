﻿using UnityEngine;
using System.Collections;

public class ButtonPromptExample : MonoBehaviour
{
    public void Click()
    {
        var prompt = WindowControlListScript.Instance.InstantiatePromptControllerScript();
        prompt.SetButtonOkText("Sim");
        prompt.SetButtonCancelText("Não");
        prompt.SetMessageText("Teste de Prompt, voce quer?");
        prompt.SetTitleText("Prompt");
        prompt.SetInputPromptText("Click aqui para digitar...");
        prompt.OnClickOk += OkClick;
        prompt.OnClickCancel += CancelClick;
        WindowControlListScript.Instance.AddPopUpToQueue(prompt.gameObject);
    }

    public void OkClick(string textMessage)
    {
        Debug.Log(textMessage);
    }

    public void CancelClick()
    {

    }
}