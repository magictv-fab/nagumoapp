﻿using UnityEngine;
using System.Collections;
using ARM.gui.utils;
using System.Linq;

public class LeftMenuControlScript : MonoBehaviour
{
		public GameObject MenuGameObject;
		public GameObject MenuParentGameObject;
		public GameObject SetaParentGameObject;

		public bool GetStateOpened ()
		{
				return (MenuParentGameObject.transform.localPosition.x >= -0.001f);
		}

		public virtual void Hide ()
		{
				
		}

		public virtual void Show ()
		{
				
		}

		void OnClick ()
		{
				if (GetStateOpened ()) {
						Hide ();
				} else {
						Show ();
				}
		}

		public void OnChangeScreenOrientationType (Vector2 screenSize, ScreenOrientationType screenOrientationType)
		{

		}

		// Use this for initialization
		void Start ()
		{
				ARM.gui.utils.ScreenOrientation.AddChangeListener (OnChangeScreenOrientationType);
		}

		void OnDestroy()
		{
			ARM.gui.utils.ScreenOrientation.RemoveChangeListener (OnChangeScreenOrientationType);
		}
	
		// Update is called once per frame
		void Update ()
		{
		}
}
