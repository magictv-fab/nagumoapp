﻿using UnityEngine;
using System.Collections;

public class MagicTVLeftMenuControlScript : LeftMenuControlScript
{
	public Animation animationController;
	public AnimationClip openAnimationClip;
	public AnimationClip closeAnimationClip;
	public override void Show ()
	{
		//  MenuParentGameObject.transform.localPosition = new Vector3 (0, MenuParentGameObject.transform.localPosition.y, MenuParentGameObject.transform.localPosition.z);
		//  SetaParentGameObject.transform.localRotation = Quaternion.Euler (new Vector3 (0, 0, 180));
	}

	public override void Hide ()
	{
	}

	public void OpenCloseClik ()
	{
		animationController.Stop ();
		animationController.clip = (animationController.clip == openAnimationClip) ? closeAnimationClip : openAnimationClip;
		animationController.Play ();
	}
}