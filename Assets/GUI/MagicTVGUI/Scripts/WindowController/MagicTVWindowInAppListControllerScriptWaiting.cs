﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using ARM.utils.io;
using ARM.utils.request;

public class MagicTVWindowInAppListControllerScriptWaiting : GenericWindowControllerScript
{
	public Image splashImage;
	public void StarClick ()
	{
		if (onClickStart != null) {
			onClickStart ();
		}

		Debug.Log ("Botao Start apertado.");
	}
	public void SetNextImage (string url)
	{
		if (string.IsNullOrEmpty (url)) {
			return;
		}
		if (!ARMFileManager.Exists (url)) {
			return;
		}
//		Debug.LogError (Application.persistentDataPath + url);

		try{
			ARMRequestVO _model3D_requestVO = new ARMRequestVO ("file://" + url);
			//						_model3D_requestVO.UseUnityCache = true;
			//						_model3D_requestVO.UnityCacheVersion = ;
			
			ARMSingleRequest requestModel3D = ARMSingleRequest.GetNewInstance (_model3D_requestVO);
			requestModel3D.Events.AddCompleteEventhandler (onImageLoaded);
			requestModel3D.Load ();
		} catch( Exception e ){
			//
			Debug.LogError(e.Message);
			return;
		}

	}

	void onImageLoaded (ARMRequestVO vo)
	{
		Texture2D t = new Texture2D (vo.Request.texture.width, vo.Request.texture.height, TextureFormat.DXT1, false);
		splashImage.sprite = Sprite.Create (t, new Rect (0, 0, t.width, t.height), Vector2.zero);
		vo.Request.LoadImageIntoTexture (t);
	}

	public Action onClickStart;
}