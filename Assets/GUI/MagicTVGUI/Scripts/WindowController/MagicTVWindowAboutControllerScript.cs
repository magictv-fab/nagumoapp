﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MagicTVWindowAboutControllerScript : GenericWindowControllerScript
{
		public Action onClickFacebook;
		public Action onClickTwitter;
		public Action onClickSite;
		public Action onClickEmail;
		public Action onClickYouTubeChannel;
		public Action onClickClearCache;
//		public Text LabelVersao;
//		public Text LabelMID;
	public Text LabelPINLand;
	public Text LabelPINPort;

		public void FacebookClick ()
		{
				if (onClickFacebook != null) {
						onClickFacebook ();
				}
				Application.OpenURL ("http://www.facebook.com/magictvoficial");

				Debug.Log ("Botao Facebook Apertado.");
		}

		public void TwitterClick ()
		{
				if (onClickTwitter != null) {
						onClickTwitter ();
				}
				Application.OpenURL ("https://mobile.twitter.com/MagicTV_oficial");

				Debug.Log ("Botao Twitter Apertado.");
		}

		public void SiteClick ()
		{
				if (onClickSite != null) {
						onClickSite ();
				}
				Application.OpenURL ("http://www.magictv.com.br/");

				Debug.Log ("Botao Site Apertado.");
		}

		public void EmailClick ()
		{
				if (onClickEmail != null) {
						onClickEmail ();
				}
				Application.OpenURL ("mailto:suporte@magictv.com.br");


				Debug.Log ("Botao Email Apertado.");
		}

		public void YouTubeChannelClick ()
		{
				if (onClickYouTubeChannel != null) {
						onClickYouTubeChannel ();
				}
				Application.OpenURL ("https://www.youtube.com/user/magictvoficial");

				Debug.Log ("Botao YouTube Channel Apertado.");
		}

		public void ClearCacheClick ()
		{
				if (onClickClearCache != null) {
						onClickClearCache ();
				}
				Debug.Log ("Botao Liompar Cache Apertado.");
		}

		

		public void SetPINText (string pin)
		{
				
		LabelPINLand.text = pin;
		LabelPINPort.text = pin;
		}

		public void SetVersionText (string version)
		{
//				LabelMID.text = version;
		}
}