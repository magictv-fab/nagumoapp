﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using UnityEngine.UI;

public class MagicTVWindowSplashDownloadProgressControllerScript : GenericWindowControllerScript
{
    public Button ButtonCancelDownload;

	public Text progressText;
	
	public virtual void UpdateText (float progress)
	{
		progressText.text = (progress * 100).ToString ("0") + " %";
	}

    public void CancelDownLoadClick()
    {
        if (onClickCancelDownload != null)
            onClickCancelDownload();
        Debug.Log("Botao cancelar Download apertado.");
        CanNotCancelDownload();
    }

    public Action onClickCancelDownload;

    private float _progress;

    public float Progress
    {
        get { return _progress; }
        set
        {
            _progress = Mathf.Clamp(value, 0f, 1f);
			UpdateText(_progress);
            GetComponentsInChildren<Slider>().ToList().ForEach(p => p.value = _progress);

		}
	}
	
	public void CanCancelDownload()
    {
        ButtonCancelDownload.enabled = true;
    }

    public void CanNotCancelDownload()
    {
        if (ButtonCancelDownload != null)
            ButtonCancelDownload.enabled = false;
    }
}
