﻿using UnityEngine;
using System.Collections;
using System;

public class ButtonShowWindowTriggerScript : MonoBehaviour
{
		public GenericWindowControllerScript TargetWindow;

		public void OnClick ()
		{
				TargetWindow.Show ();
		}
}
