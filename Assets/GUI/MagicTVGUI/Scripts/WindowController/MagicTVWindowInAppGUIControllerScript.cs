﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using ARM.utils.cron;

public class MagicTVWindowInAppGUIControllerScript : GenericWindowControllerScript
{
	public LeftMenuControlScript MenuContainer;
	public GameObject CameraButton;
	public GameObject InfoButton;
	public GameObject UpdateButton;
	public Action onUpdateClick;
	public GameObject canvasObject;

	public virtual void CameraClick ()
	{

		if(Application.loadedLevelName != "FaceTrackerARSample 1")
		{
			ShowOnlyWaterMark (true);
			if (onClickCamera != null)
				onClickCamera ();
		}


		Debug.Log ("Botao Camera apertado.");

		if(canvasObject)
			canvasObject.SetActive(false);

		StartCoroutine (SaveScreenShot ());

	}

	IEnumerator SaveScreenShot ()
	{
		yield return new WaitForSeconds (0.5f);
		yield return new WaitForFixedUpdate ();
		Screenshot.Save ();
	}

	public void HelpClick ()
	{
		//				WindowControlListScript.Instance.WindowHelp.Show ();
	}

	public void InstructionsClick ()
	{
		WindowControlListScript.Instance.WindowInstructions.Show ();
	}

	public void HowToPopUpClick ()
	{
		//				WindowControlListScript.Instance.WindowHowToPopUp.Show ();
	}

	public void AboutClick ()
	{
		//				WindowControlListScript.Instance.WindowAbout.Show ();

	}

	public void PrintScreenDone ()
	{
		//CancelInvoke ("printScreenDoneLater");
		//Invoke ("printScreenDoneLater", 0.5f);
		Debug.Log ("Screenshot Call......");
		printScreenDoneLater ();
	}

	public void UpdateClick ()
	{
		if (onUpdateClick != null) {
			onUpdateClick ();
		}
	}

	public void UpdateStart ()
	{
		if (UpdateButton != null) {
			ButtonAnimationSoundScript updateBtn = UpdateButton.GetComponent<ButtonAnimationSoundScript> ();
			updateBtn.PlayAnimation ();
		}
	}

	public void UpdateDone ()
	{

		if (UpdateButton != null) {
			ButtonAnimationSoundScript updateBtn = UpdateButton.GetComponent<ButtonAnimationSoundScript> ();
			updateBtn.StopAnimation ();
		}
	}

	private void printScreenDoneLater ()
	{
		ShowOnlyWaterMark (false);
	}

	public virtual void ShowOnlyWaterMark (bool onlyWaterMark)
	{
		if (onlyWaterMark) {
			WindowControlListScript.Instance.WindowScreenShot.Show ();
		} else { 
			WindowControlListScript.Instance.WindowScreenShot.Hide ();
		}

		MenuContainer.MenuParentGameObject.SetActive (!onlyWaterMark);
		MenuContainer.gameObject.SetActive (!onlyWaterMark);
		CameraButton.SetActive (!onlyWaterMark);
		if (InfoButton != null) {
			InfoButton.SetActive (!onlyWaterMark);
		}
		if (UpdateButton != null) {
			UpdateButton.SetActive (!onlyWaterMark);
		}
	}

	public void LoadFaceTrackerScene()
	{
		
//		GameObject.Find("Menu").SetActive(false);
//		GameObject.Find("Luzes").SetActive(false);
//		GameObject.Find("FaceTracking Button").SetActive(false);
//		GameObject.Find("ARUniverse").SetActive(false);
//		Application.LoadLevelAdditiveAsync ("FaceTrackerARSample 1");

		Application.LoadLevel("FaceTrackerARSample 1");
	}

	public override void Show ()
	{
		base.Show ();
		MenuContainer.Hide ();
		ShowOnlyWaterMark (false);
		Screenshot.RemoveCompleteEventHandler (screenshotComplete);
		Screenshot.AddCompleteEventHandler (screenshotComplete);
	}

	private void screenshotComplete (string path)
	{
		Debug.Log ("screen shot done" + path);
		if (onScreenShotComplete != null)
			onScreenShotComplete (path);
		PrintScreenDone ();
	}

	public Action onClickCamera;

	public delegate void ScreenShotCompleteDelegate (string path);

	public GameObject ExitButton;
	public bool isCloseBtnOn = false;


	
	/// <summary>
	/// Abaixo todo o esquema para controle de movimentos para apagar um cache.
	/// Mas externamente isso é irrelevante e é considerado uma intenção normal. Quer ou não quer dar reset
	/// </summary>
	bool _upButtonPressed;
	int _timeout;

	public void OnClearPress ()
	{
		_upButtonPressed = false;
		EasyTimer.ClearInterval (_timeout);
		_timeout = EasyTimer.SetTimeout (checkClearCache, 5000, "clearCacheTimeout");
	}

	public void OnClearUp ()
	{
		EasyTimer.ClearInterval (_timeout);
		_upButtonPressed = true;
		
	}
	
	void checkClearCache ()
	{
		if (!this._upButtonPressed) {
			ResetClick ();
		}
	}

	public ScreenShotCompleteDelegate onScreenShotComplete;


	//Evento para reset
	public Action onClickReset;

	public void ResetClick ()
	{
		if (onClickReset != null)
			onClickReset ();
	}

	public Action onClickClearCache;

	public void ClearCacheClick ()
	{
		if (onClickClearCache != null)
			onClickClearCache ();
	}

	public Action onClickBack;

	public void BackClick ()
	{
		if (onClickBack != null)
			onClickBack ();
	}
}
