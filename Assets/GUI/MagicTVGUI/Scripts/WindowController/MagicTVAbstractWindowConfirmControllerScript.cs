﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class MagicTVAbstractWindowConfirmControllerScript : MagicTVAbstractWindowPopUpControllerScript
{
    public Action OnClickCancel;
    public Text LabelButtonCancel;

    public void SetButtonCancelText(string text)
    {
        LabelButtonCancel.text = text;
    }

    private void RaiseCancel()
    {
        if (OnClickCancel != null)
        {
            OnClickCancel();
        }
        Debug.Log("Botao Cancel Apertado.");
    }

    public void ClickCancel()
    {
        OkButton = false;
        Hide();
    }

    public override void Hide()
    {
        if (!OkButton)
            RaiseCancel();
        base.Hide();
    }
}
