﻿using UnityEngine;
using System.Collections;
using System;

public class MagicTVWindowHelpControllerScript : GenericWindowControllerScript
{
		public Action onClickPrint;
		public Action onClickYouTubeVideo;

		public void PrintClick ()
		{
				if (onClickPrint != null) {
						onClickPrint ();
				}
				Application.OpenURL ("http://www.grafitelabs.com.br/pdf/magictv_card.pdf");

				Debug.Log ("Botao Imprimir Apertado.");
		}

		public void YouTubeClick ()
		{
				if (onClickYouTubeVideo != null) {
						onClickYouTubeVideo ();
				}
				Application.OpenURL ("https://www.youtube.com/watch?v=l89O4Q8P824");

				Debug.Log ("Botao YouTube Video Apertado.");
		}
}