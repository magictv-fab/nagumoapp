﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class MagicTVAbstractWindowPopUpControllerScript : GenericWindowControllerScript
{
    public Action OnClickOk;
    public Text LabelTitle;
    public Text LabelMessage;
    public Text LabelButtonOk;

    protected bool OkButton = false;

    public void ClickOK()
    {
        RaiseClickOk();
        Hide();
    }

    protected void RaiseClickOk()
    {
        OkButton = true;
        if (OnClickOk != null)
        {
            OnClickOk();
        }
        Debug.Log("Botao OK Apertado.");
    }

    public void SetTitleText(string text)
    {
        LabelTitle.text = text;
    }

    public void SetMessageText(string text)
    {
        LabelMessage.text = text;//.PadRight(450, ' ');
        // LabelMessage.alignment = NGUIText.Alignment.Center;
    }

    public void SetButtonOkText(string text)
    {
        LabelButtonOk.text = text;
    }

    public override void Hide()
    {
        DestroyImmediate(gameObject);
    }

    public override void Show()
    {
        base.Show();
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localScale = new Vector3(1, 1, 1);
    }
}
