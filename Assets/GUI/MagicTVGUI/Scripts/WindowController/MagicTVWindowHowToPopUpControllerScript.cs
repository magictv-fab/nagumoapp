﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MagicTVWindowHowToPopUpControllerScript : GenericWindowControllerScript
{
		public Toggle DontShowAnymoreToggle;

		public bool DontShowAnymore {
				get { return DontShowAnymoreToggle.isOn; }
		}
}
