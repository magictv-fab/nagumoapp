﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using MagicTV.globals.events;

public class WindowControlListScript : MonoBehaviour
{
	public GameObject ButtonPrefab;
	public GameObject ButtonTable;
	public GameObject PopUpAlertPrefab;
	public GameObject PopUpConfirmPrefab;
	public GameObject PopUpPromptPrefab;
	public AnimationClip AminationForLabelChangeDefault;
	public bool AutoShowInAppGui = false;
	public Canvas CanvasRoot;
	public static WindowControlListScript Instance;
	public ScreenOrientation freezeScreenRotation;
	public WindowControlListScript ()
	{
		Instance = this;
	}

	private void clearTable ()
	{
		for (int i = 0; i < ButtonTable.transform.childCount; i++) {
			Destroy (ButtonTable.transform.GetChild (i).gameObject);
		}
	}

	void Start ()
	{
		if (AutoShowInAppGui) {
			Instance.WindowInAppGUI.Show ();
		}
		//Screen.orientation = freezeScreenRotation;
		//carrega a imagem
		if (this.materialImagemInicial != null) {

		}

		WindowGuiControlers = WindowInAppGUI.GetComponent<MagicTVWindowInAppGUIControllerScript>();
	}
	
    #region Windows
	public MagicTVWindowInAppGUIControllerScript WindowInAppGUI;

	/// <summary>
	/// The window GUI controlers.
	/// Procura automatico
	/// </summary>
	protected MagicTVWindowInAppGUIControllerScript WindowGuiControlers;
	//    public MagicTVWindowInAppListControllerScript WindowInAppList;

	//    public MagicTVWindowSplashDownloadAvailableControllerScript WindowSplashDownloadAvaible;

	public MagicTVWindowSplashDownloadProgressControllerScript WindowSplashDownloadProgress;

	//    public MagicTVWindowHowToPopUpControllerScript WindowHowToPopUp;

	//    public MagicTVWindowHelpControllerScript WindowHelp;

	public MagicTVWindowInstructionsControllerScript WindowInstructions;

	   public MagicTVWindowAboutControllerScript WindowAbout;

	public MagicTVWindowInAppListControllerScriptWaiting WindowInAppListWaiting;

	public MagicTVWindowErrorMessage WindowErrorMessage;

	public MagicTVWindowSplashDownloadProgressControllerScript WindowDownloadProgress;

	public MagicTVWindowInScreeShotControllerScript WindowScreenShot;

	public MagicTVWindowInAppCloseControllerScript WindowScreenClose;
    #endregion

    #region Links

	public Action onClickYouTubeChannel;

	public Action onClickYouTubeVideo;

	public Action onClickTwitter;

	public Action onClickYouFacebook;

	public Action onClickEmail;

	public Material materialImagemInicial;
    #endregion

    #region Actions

    #endregion

    #region PopUpQueue

	private List<GameObject> popUpControllerList = new List<GameObject> ();
	private MagicTVAbstractWindowPopUpControllerScript _currentPopUp;

	public void Update ()
	{

		if (popUpControllerList == null)
			popUpControllerList = new List<GameObject> ();
		if (popUpControllerList.Any ()) {
			if (popUpControllerList [0] == null) {
				popUpControllerList.RemoveAt (0);
				return;
			}
			_currentPopUp = popUpControllerList [0].GetComponent<MagicTVAbstractWindowPopUpControllerScript> ();
			if (!_currentPopUp.gameObject.activeInHierarchy) {
				_currentPopUp.Show ();
				_currentPopUp.onHide += popUpClose;
			}
		}
	}

	private void popUpClose ()
	{
		_currentPopUp = null;
		if (popUpControllerList.Any ()) {
			popUpControllerList.RemoveAt (0);
		}
	}

	private GameObject _instantiatePopUp (Type popUpType)
	{
		GameObject popUpPrefab = null;
		if (popUpType.IsAssignableFrom (typeof(MagicTVWindowAlertControllerScript)))
			popUpPrefab = PopUpAlertPrefab;
		if (popUpType.IsAssignableFrom (typeof(MagicTVWindowConfirmControllerScript)))
			popUpPrefab = PopUpConfirmPrefab;
		if (popUpType.IsAssignableFrom (typeof(MagicTVWindowPromptControllerScript)))
			popUpPrefab = PopUpPromptPrefab;

		if (popUpPrefab == null)
			return null;

		var popUpInstance = (GameObject)Instantiate (popUpPrefab);
		var rectTransform = popUpInstance.GetComponent<RectTransform> ();
		rectTransform.SetParent (CanvasRoot.transform);
		rectTransform.sizeDelta = new Vector2 (0f, 0f);
		popUpInstance.SetActive (false);

		return popUpInstance;
	}

	public MagicTVWindowAlertControllerScript InstantiateAlertControllerScript ()
	{
		return
            _instantiatePopUp (typeof(MagicTVWindowAlertControllerScript))
                .GetComponent<MagicTVWindowAlertControllerScript> ();
	}

	public MagicTVWindowConfirmControllerScript InstantiateConfirmControllerScript ()
	{
		return
            _instantiatePopUp (typeof(MagicTVWindowConfirmControllerScript))
                .GetComponent<MagicTVWindowConfirmControllerScript> ();
	}

	public MagicTVWindowPromptControllerScript InstantiatePromptControllerScript ()
	{
		return
            _instantiatePopUp (typeof(MagicTVWindowPromptControllerScript))
                .GetComponent<MagicTVWindowPromptControllerScript> ();
	}

	public void AddPopUpToQueue (GameObject popUp)
	{
		popUpControllerList.Add (popUp);
	}

	//private GameObject _preparePopUp(Type popUpType)
	//{
	//    var popUp = InstantiatePopUp(popUpType);
	//    var popUpScript = popUp.GetComponent<MagicTVAbstractWindowPopUpControllerScript>();
	//    popUpScript.SetButtonOKText("OK");
	//    popUpScript.SetMessageText("Message");
	//    popUpScript.SetTitleText("Title");
	//    return popUp;
	//}

    #endregion
}
