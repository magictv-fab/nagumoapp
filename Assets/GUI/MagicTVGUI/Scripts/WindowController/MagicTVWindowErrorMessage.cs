﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
public class MagicTVWindowErrorMessage : GenericWindowControllerScript
{
    
    public Text _ButtonLabel;
    public Text _ErrorMessagePort;
    public Text _ErrorMessageLand;
    public void CongigClick()
    {
        if (onClickConfig != null)
            onClickConfig();
        Debug.Log("Botao Config.");
    }
    
    public void SetErroMessage( string message ){
        _ErrorMessageLand.text = message;
        _ErrorMessagePort.text = message;
    }

    public void SetButtonLabel( string message ){
        _ButtonLabel.text = message;
        
    }

    public Action onClickConfig;
}