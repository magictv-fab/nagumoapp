﻿using System;
using UnityEngine;
using System.Collections;

public class MagicTVWindowAlertControllerScript : MagicTVAbstractWindowPopUpControllerScript
{
    public override void Hide()
    {
        if (!OkButton)
            RaiseClickOk();
        base.Hide();
    }
}
