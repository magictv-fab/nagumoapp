﻿using UnityEngine;
using System.Collections;
using System;

public class MagicTVWindowSplashDownloadAvailableControllerScript : GenericWindowControllerScript
{
		public void ConfirmDownloadClick ()
		{
				if (onClickConfirmDownload != null)
						onClickConfirmDownload ();
				Debug.Log ("Botao Confirm Download apertado.");
		}

		public Action onClickConfirmDownload;

		public void CancelDownLoadClick ()
		{
				if (onClickCancelDownload != null)
						onClickCancelDownload ();
				Debug.Log ("Botao cancelar Download apertado.");
		}

		public Action onClickCancelDownload;
}
