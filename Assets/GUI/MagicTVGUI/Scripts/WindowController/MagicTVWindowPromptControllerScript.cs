﻿using System;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MagicTVWindowPromptControllerScript : MagicTVAbstractWindowConfirmControllerScript
{
    public InputField LabelInputPrompt;

    private string _defaultText = "Click aqui para digitar...";

    public new void ClickOK()
    {
        RaiseClickOk();
        Hide();
    }

    public void SetInputPromptText(string text)
    {
        _defaultText = text;
        LabelInputPrompt.text = text;
    }

    public string GetPromptString()
    {
        return (_defaultText == LabelInputPrompt.text) ? "" : LabelInputPrompt.text;
    }

    public delegate void ClickOkPromptDelegate(string textMessage);

    public new ClickOkPromptDelegate OnClickOk;

    protected new void RaiseClickOk()
    {
        var text = GetPromptString();
        if (String.IsNullOrEmpty(text))
            return;
        OkButton = true;
        if (OnClickOk != null)
        {
            OnClickOk(text);
        }
        Debug.Log("Botao OK Apertado.");
    }
}