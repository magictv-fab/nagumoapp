﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class MagicTVWindowAstraZenecaInAppPassword : GenericWindowControllerScript
{

    public void PasswordChange(string password)
    {
        InputField currentInput = LandscapePasswordInput;
        if (!currentInput.gameObject.activeInHierarchy){
            currentInput = PortraitPasswordInput;
        }
        PasswordValue = currentInput.text;
        if (currentInput == LandscapePasswordInput){
            PortraitPasswordInput.text = PasswordValue;
        }
        else
        {
            LandscapePasswordInput.text = PasswordValue;
        }
    }

    public InputField LandscapePasswordInput;
    public InputField PortraitPasswordInput;

    public void LoginClick()
    {
        if (onClickLogin != null)
            onClickLogin();
        Debug.Log("Botão Login apertado. Senha: " + PasswordValue);
    }

    public Action onClickLogin;

    public string PasswordValue;
}